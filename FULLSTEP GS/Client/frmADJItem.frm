VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmADJItem 
   Caption         =   "Proceso:"
   ClientHeight    =   8745
   ClientLeft      =   0
   ClientTop       =   2715
   ClientWidth     =   11400
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmADJItem.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8745
   ScaleWidth      =   11400
   Begin SSDataWidgets_B.SSDBDropDown sdbddCantidades 
      Height          =   915
      Left            =   1200
      TabIndex        =   66
      Top             =   4080
      Width           =   1185
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJItem.frx":014A
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   53
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Name =   "VALOR"
      Columns(0).Alignment=   1
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).NumberFormat=   "Standard"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1931
      Columns(1).Caption=   "DESC"
      Columns(1).Name =   "DESC"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).NumberFormat=   "Standard"
      Columns(1).FieldLen=   256
      _ExtentX        =   2090
      _ExtentY        =   1614
      _StockProps     =   77
      BackColor       =   8421376
   End
   Begin VB.PictureBox picControlPrecios 
      BackColor       =   &H00A56E3A&
      Height          =   2535
      Left            =   3140
      ScaleHeight     =   2475
      ScaleWidth      =   6495
      TabIndex        =   12
      Top             =   690
      Visible         =   0   'False
      Width           =   6550
      Begin VB.CommandButton cmdOrdernarAtrib 
         Caption         =   "Ordenar"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5325
         TabIndex        =   45
         Top             =   80
         Width           =   975
      End
      Begin VB.CommandButton cmdAplicarPrecio 
         Caption         =   "&Aplicar"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   14
         Top             =   2040
         Width           =   2055
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAplicarPrecio 
         Height          =   1635
         Left            =   150
         TabIndex        =   16
         Top             =   360
         Width           =   6170
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   6
         stylesets.count =   3
         stylesets(0).Name=   "Ponderacion"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16367808
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmADJItem.frx":0166
         stylesets(0).AlignmentPicture=   0
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmADJItem.frx":04B8
         stylesets(1).AlignmentText=   0
         stylesets(2).Name=   "Seleccionada"
         stylesets(2).ForeColor=   16777215
         stylesets(2).BackColor=   8388608
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmADJItem.frx":04D4
         DividerType     =   0
         BevelColorFrame =   -2147483646
         RowSelectionStyle=   1
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         PictureButton   =   "frmADJItem.frx":04F0
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorEven   =   15777984
         BackColorOdd    =   15777984
         RowHeight       =   370
         ExtraHeight     =   265
         Columns.Count   =   6
         Columns(0).Width=   6562
         Columns(0).Caption=   "Atributo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasHeadForeColor=   -1  'True
         Columns(0).HasHeadBackColor=   -1  'True
         Columns(0).HeadForeColor=   16777215
         Columns(0).HeadBackColor=   12615680
         Columns(0).HeadStyleSet=   "Normal"
         Columns(1).Width=   450
         Columns(1).Name =   "INC"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Style=   2
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HeadBackColor=   12615680
         Columns(2).Width=   2752
         Columns(2).Caption=   "Aplicar"
         Columns(2).Name =   "Aplicar"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HeadForeColor=   16777215
         Columns(2).HeadBackColor=   12615680
         Columns(2).HeadStyleSet=   "Normal"
         Columns(3).Width=   582
         Columns(3).Name =   "POND"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   4
         Columns(3).ButtonsAlways=   -1  'True
         Columns(3).HasHeadBackColor=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).HeadBackColor=   12615680
         Columns(3).BackColor=   15777984
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "ID"
         Columns(4).Name =   "ID"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "GR"
         Columns(5).Name =   "GR"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         _ExtentX        =   10883
         _ExtentY        =   2884
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   16367808
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAplicarPrecio 
         BackColor       =   &H00A56E3A&
         Caption         =   "Aplicar al precio :"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   120
         Width           =   4995
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00FFFFFF&
         Height          =   2415
         Left            =   0
         Top             =   60
         Width           =   6465
      End
   End
   Begin VB.PictureBox picControlOptima 
      BackColor       =   &H00A56E3A&
      Height          =   3735
      Left            =   3125
      ScaleHeight     =   3675
      ScaleWidth      =   6510
      TabIndex        =   25
      Top             =   360
      Visible         =   0   'False
      Width           =   6570
      Begin VB.CommandButton cmdAbajo 
         Height          =   255
         Left            =   5670
         MaskColor       =   &H8000000F&
         Picture         =   "frmADJItem.frx":0842
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   240
         Width           =   675
      End
      Begin VB.CommandButton cmdCalcularOptima 
         Caption         =   "&Calcular"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   27
         Top             =   3120
         Width           =   2055
      End
      Begin VB.CommandButton cmdArriba 
         Height          =   255
         Left            =   4980
         Picture         =   "frmADJItem.frx":089C
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   240
         Width           =   675
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgOptima 
         Height          =   2475
         Left            =   180
         TabIndex        =   29
         Top             =   480
         Width           =   6175
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   7
         stylesets.count =   4
         stylesets(0).Name=   "Ponderacion"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16367808
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmADJItem.frx":08F6
         stylesets(0).AlignmentPicture=   0
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmADJItem.frx":0C48
         stylesets(1).AlignmentText=   0
         stylesets(2).Name=   "Interno"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmADJItem.frx":0C64
         stylesets(2).AlignmentPicture=   1
         stylesets(3).Name=   "Seleccionada"
         stylesets(3).ForeColor=   16777215
         stylesets(3).BackColor=   8388608
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmADJItem.frx":0FB6
         DividerType     =   0
         BevelColorFrame =   -2147483646
         AllowDelete     =   -1  'True
         RowSelectionStyle=   1
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         PictureButton   =   "frmADJItem.frx":0FD2
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorEven   =   15777984
         BackColorOdd    =   15777984
         RowHeight       =   370
         ExtraHeight     =   265
         Columns.Count   =   7
         Columns(0).Width=   450
         Columns(0).Name =   "INC"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(0).HasHeadBackColor=   -1  'True
         Columns(0).HeadBackColor=   12615680
         Columns(1).Width=   6191
         Columns(1).Caption=   "Atributo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HeadForeColor=   16777215
         Columns(1).HeadBackColor=   12615680
         Columns(1).HeadStyleSet=   "Normal"
         Columns(2).Width=   2302
         Columns(2).Caption=   "Pertenece"
         Columns(2).Name =   "Pertenece"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HeadForeColor=   16777215
         Columns(2).HeadBackColor=   12615680
         Columns(2).HeadStyleSet=   "Normal"
         Columns(3).Width=   926
         Columns(3).Caption=   "Pond."
         Columns(3).Name =   "PON"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   2
         Columns(3).HasHeadForeColor=   -1  'True
         Columns(3).HasHeadBackColor=   -1  'True
         Columns(3).HeadForeColor=   16777215
         Columns(3).HeadBackColor=   12615680
         Columns(3).HeadStyleSet=   "Normal"
         Columns(4).Width=   582
         Columns(4).Name =   "POND"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Style=   4
         Columns(4).ButtonsAlways=   -1  'True
         Columns(4).HasHeadBackColor=   -1  'True
         Columns(4).HasBackColor=   -1  'True
         Columns(4).HeadBackColor=   12615680
         Columns(4).BackColor=   15777984
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "ID"
         Columns(5).Name =   "ID"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "AMBITO"
         Columns(6).Name =   "AMBITO"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   10892
         _ExtentY        =   4366
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   16367808
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Shape Shape6 
         BorderColor     =   &H00FFFFFF&
         Height          =   3495
         Left            =   60
         Top             =   60
         Width           =   6375
      End
      Begin VB.Label lblCalcularOpt 
         BackColor       =   &H00A56E3A&
         Caption         =   "Calcular �ptima:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   30
         Top             =   180
         Width           =   1875
      End
      Begin VB.Shape Shape5 
         BorderColor     =   &H00FFFFFF&
         Height          =   3615
         Left            =   0
         Top             =   60
         Width           =   6495
      End
   End
   Begin VB.PictureBox picControlVista 
      BackColor       =   &H00A56E3A&
      Height          =   6720
      Left            =   3120
      ScaleHeight     =   6660
      ScaleWidth      =   7350
      TabIndex        =   20
      Top             =   360
      Visible         =   0   'False
      Width           =   7410
      Begin VB.CommandButton cmdRenombrarVista 
         Caption         =   "Renombrar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5520
         TabIndex        =   63
         Top             =   6240
         Width           =   1725
      End
      Begin VB.CommandButton cmdEliminarVista 
         Caption         =   "Eliminar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3720
         TabIndex        =   62
         Top             =   6240
         Width           =   1725
      End
      Begin VB.CommandButton cmdGuardarVistaNueva 
         Caption         =   "D&Guardar en  vista nueva"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1920
         TabIndex        =   61
         Top             =   6240
         Width           =   1725
      End
      Begin VB.CommandButton cmdGuardarVista 
         Caption         =   "D&Guardar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   60
         Top             =   6240
         Width           =   1725
      End
      Begin VB.TextBox txtDecPrec 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3120
         TabIndex        =   15
         Top             =   5745
         Width           =   400
      End
      Begin VB.TextBox txtDecCant 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   6360
         TabIndex        =   19
         Top             =   5745
         Width           =   400
      End
      Begin VB.TextBox txtDecResult 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1455
         TabIndex        =   13
         Top             =   5745
         Width           =   400
      End
      Begin VB.TextBox txtDecPorcen 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4695
         TabIndex        =   17
         Top             =   5745
         Width           =   400
      End
      Begin VB.CheckBox chkOcultarProvSinOfe 
         BackColor       =   &H00A56E3A&
         Caption         =   "Ocultar proveedores sin ofertas"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   3720
         TabIndex        =   44
         Top             =   5160
         Width           =   3495
      End
      Begin VB.CheckBox chkOcultarNoAdj 
         BackColor       =   &H00A56E3A&
         Caption         =   "Ocultar ofertas no adjudicables"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   43
         Top             =   5160
         Width           =   3495
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgOcultarCampos 
         Height          =   1500
         Left            =   120
         TabIndex        =   21
         Top             =   1440
         Width           =   7035
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   5
         stylesets.count =   3
         stylesets(0).Name=   "Ponderacion"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16367808
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmADJItem.frx":1324
         stylesets(0).AlignmentPicture=   0
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmADJItem.frx":1676
         stylesets(1).AlignmentText=   0
         stylesets(2).Name=   "Seleccionada"
         stylesets(2).ForeColor=   16777215
         stylesets(2).BackColor=   8388608
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmADJItem.frx":1692
         DividerType     =   0
         BevelColorFrame =   -2147483646
         RowSelectionStyle=   1
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         PictureButton   =   "frmADJItem.frx":16AE
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorEven   =   15777984
         BackColorOdd    =   15777984
         RowHeight       =   370
         ExtraHeight     =   106
         Columns.Count   =   5
         Columns(0).Width=   450
         Columns(0).Name =   "INC"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(0).HasHeadBackColor=   -1  'True
         Columns(0).HeadBackColor=   12615680
         Columns(1).Width=   5424
         Columns(1).Caption=   "Campo"
         Columns(1).Name =   "CAMPO"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HeadForeColor=   16777215
         Columns(1).HeadBackColor=   12615680
         Columns(1).HeadStyleSet=   "Normal"
         Columns(2).Width=   5424
         Columns(2).Caption=   "Pertenece"
         Columns(2).Name =   "Pertenece"
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HeadForeColor=   16777215
         Columns(2).HeadBackColor=   12615680
         Columns(2).HeadStyleSet=   "Normal"
         Columns(3).Width=   635
         Columns(3).Caption=   "Detal."
         Columns(3).Name =   "POND"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   4
         Columns(3).ButtonsAlways=   -1  'True
         Columns(3).HasHeadForeColor=   -1  'True
         Columns(3).HasHeadBackColor=   -1  'True
         Columns(3).HeadForeColor=   16777215
         Columns(3).HeadBackColor=   12615680
         Columns(3).HeadStyleSet=   "Normal"
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "COD"
         Columns(4).Name =   "COD"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   12409
         _ExtentY        =   2646
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   16367808
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgOcultarProv 
         Height          =   1410
         Left            =   120
         TabIndex        =   22
         Top             =   3600
         Width           =   7035
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   4
         stylesets.count =   3
         stylesets(0).Name=   "Ponderacion"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16367808
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmADJItem.frx":1A00
         stylesets(0).AlignmentPicture=   0
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmADJItem.frx":1D52
         stylesets(1).AlignmentText=   0
         stylesets(2).Name=   "Seleccionada"
         stylesets(2).ForeColor=   16777215
         stylesets(2).BackColor=   8388608
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmADJItem.frx":1D6E
         DividerType     =   0
         BevelColorFrame =   -2147483646
         MultiLine       =   0   'False
         RowSelectionStyle=   1
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         PictureButton   =   "frmADJItem.frx":1D8A
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorEven   =   15777984
         BackColorOdd    =   15777984
         RowHeight       =   370
         ExtraHeight     =   106
         Columns.Count   =   4
         Columns(0).Width=   450
         Columns(0).Name =   "INC"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(0).HasHeadBackColor=   -1  'True
         Columns(0).HeadBackColor=   12615680
         Columns(1).Width=   10795
         Columns(1).Caption=   "Proveedor"
         Columns(1).Name =   "PROVE"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HeadForeColor=   16777215
         Columns(1).HeadBackColor=   12615680
         Columns(1).HeadStyleSet=   "Normal"
         Columns(2).Width=   688
         Columns(2).Caption=   "Detal."
         Columns(2).Name =   "POND"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Style=   4
         Columns(2).ButtonsAlways=   -1  'True
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HeadForeColor=   16777215
         Columns(2).HeadBackColor=   12615680
         Columns(2).HeadStyleSet=   "Normal"
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "COd"
         Columns(3).Name =   "COD"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         _ExtentX        =   12409
         _ExtentY        =   2487
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   16367808
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcVistaDefecto 
         Height          =   255
         Left            =   2115
         TabIndex        =   56
         Top             =   465
         Width           =   4590
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         BevelColorFace  =   13160660
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         Columns.Count   =   4
         Columns(0).Width=   8096
         Columns(0).Caption=   "VISTA"
         Columns(0).Name =   "VISTA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "TIPO"
         Columns(2).Name =   "TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   25
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "USU"
         Columns(3).Name =   "USU"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         _ExtentX        =   8096
         _ExtentY        =   450
         _StockProps     =   93
         Text            =   "Vista 1"
         ForeColor       =   16777215
         BackColor       =   12615680
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcVistaActual 
         Height          =   255
         Left            =   2115
         TabIndex        =   57
         Top             =   120
         Width           =   4590
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         BevelColorFace  =   13160660
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         Columns.Count   =   4
         Columns(0).Width=   8096
         Columns(0).Caption=   "VISTA"
         Columns(0).Name =   "VISTA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "TIPO"
         Columns(2).Name =   "TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "USU"
         Columns(3).Name =   "USU"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         _ExtentX        =   8096
         _ExtentY        =   450
         _StockProps     =   93
         Text            =   "Vista 1"
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7290
         Y1              =   960
         Y2              =   960
      End
      Begin VB.Label lblVistaDefecto 
         BackColor       =   &H00A56E3A&
         Caption         =   "DVista por defecto:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   59
         Top             =   510
         Width           =   1920
      End
      Begin VB.Label lblPlantillaDefecto 
         BackColor       =   &H00A56E3A&
         Caption         =   "DPlantilla"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   58
         Top             =   165
         Width           =   3930
      End
      Begin VB.Label lblNumDec 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DN�mero decimales:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   120
         TabIndex        =   52
         Top             =   5520
         Width           =   1305
      End
      Begin VB.Label lblNumDecResult 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DResultados:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   495
         TabIndex        =   51
         Top             =   5805
         Width           =   855
      End
      Begin VB.Label lblNumDecPrec 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DPrecios:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   2160
         TabIndex        =   50
         Top             =   5805
         Width           =   855
      End
      Begin VB.Label lblNumDecCant 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DCantidades:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   5400
         TabIndex        =   49
         Top             =   5805
         Width           =   840
      End
      Begin VB.Label lblNumDecPorcen 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DPorcentaje:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   3735
         TabIndex        =   48
         Top             =   5805
         Width           =   825
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7290
         Y1              =   5400
         Y2              =   5400
      End
      Begin VB.Line Line9 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7290
         Y1              =   6120
         Y2              =   6120
      End
      Begin VB.Shape Shape3 
         BorderColor     =   &H00FFFFFF&
         Height          =   6620
         Left            =   0
         Top             =   0
         Width           =   7305
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7290
         Y1              =   3000
         Y2              =   3000
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7290
         Y1              =   5040
         Y2              =   5040
      End
      Begin VB.Label lblMostrarProv 
         BackColor       =   &H00A56E3A&
         Caption         =   "Mostrar proveedores"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   24
         Top             =   3120
         Width           =   1635
      End
      Begin VB.Label lblMostrarCampos 
         BackColor       =   &H00A56E3A&
         Caption         =   "Mostrar campos"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   23
         Top             =   1200
         Width           =   1635
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
      Height          =   915
      Left            =   600
      TabIndex        =   55
      Top             =   2640
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJItem.frx":20DC
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   53
      Columns(0).Width=   3201
      Columns(0).Name =   "NOMBRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   405
      Left            =   0
      ScaleHeight     =   405
      ScaleWidth      =   11400
      TabIndex        =   47
      Top             =   8340
      Width           =   11400
      Begin VB.CommandButton cmdGuardar 
         Caption         =   "&Guardar"
         Height          =   345
         Left            =   1110
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Left            =   3270
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdAdjudicar 
         Caption         =   "&Validar"
         Height          =   345
         Left            =   2190
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdHojaComp 
         Caption         =   "Hoja &comp."
         Height          =   345
         Left            =   30
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Enabled         =   0   'False
         Height          =   345
         Left            =   4350
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
   End
   Begin VB.PictureBox PicAbrir 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   220
      Left            =   11460
      Picture         =   "frmADJItem.frx":20F8
      ScaleHeight     =   225
      ScaleWidth      =   225
      TabIndex        =   46
      Top             =   300
      Visible         =   0   'False
      Width           =   220
   End
   Begin VB.PictureBox picCerrar 
      BorderStyle     =   0  'None
      Height          =   220
      Left            =   11460
      Picture         =   "frmADJItem.frx":245F
      ScaleHeight     =   225
      ScaleWidth      =   225
      TabIndex        =   40
      Top             =   60
      Width           =   220
   End
   Begin VB.PictureBox picItem 
      BorderStyle     =   0  'None
      Height          =   1215
      Left            =   0
      ScaleHeight     =   1215
      ScaleWidth      =   11475
      TabIndex        =   31
      Top             =   0
      Width           =   11475
      Begin VB.CommandButton cmdVista 
         Height          =   285
         Left            =   5480
         Picture         =   "frmADJItem.frx":27B5
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Dise�o de vistas"
         Top             =   120
         Width           =   315
      End
      Begin VB.CommandButton cmdOptima 
         BackColor       =   &H00C8D0D4&
         Height          =   285
         Left            =   5120
         Picture         =   "frmADJItem.frx":285B
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   120
         Width           =   315
      End
      Begin VB.CommandButton cmdMostrarGrafico 
         Height          =   285
         Left            =   4760
         Picture         =   "frmADJItem.frx":2B6D
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Mostrar gr�fico"
         Top             =   120
         Width           =   315
      End
      Begin VB.CommandButton cmdPrecios 
         Height          =   285
         Left            =   5120
         Picture         =   "frmADJItem.frx":2FAF
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   450
         Width           =   315
      End
      Begin VB.CommandButton cmdImprimir 
         Enabled         =   0   'False
         Height          =   285
         Left            =   5480
         Picture         =   "frmADJItem.frx":3006
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   450
         UseMaskColor    =   -1  'True
         Width           =   330
      End
      Begin VB.CommandButton cmdDetalle 
         Height          =   285
         Left            =   4760
         Picture         =   "frmADJItem.frx":308B
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Mostrar detalle de �tem"
         Top             =   450
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcItems 
         Height          =   285
         Left            =   30
         TabIndex        =   5
         Top             =   90
         Width           =   4585
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelColorFrame =   0
         BevelColorShadow=   8421504
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   2037
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   11468799
         Columns(1).Width=   6482
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   11468799
         Columns(2).Width=   1905
         Columns(2).Caption=   "A�o/Dest"
         Columns(2).Name =   "DEST"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   11468799
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "GRUPO"
         Columns(3).Name =   "GRUPO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "ID"
         Columns(4).Name =   "ID"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   8087
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   12648447
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgResultados 
         Height          =   950
         Left            =   5980
         TabIndex        =   32
         Top             =   60
         Width           =   5420
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   6
         stylesets.count =   3
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmADJItem.frx":319B
         stylesets(1).Name=   "Red"
         stylesets(1).BackColor=   4744445
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmADJItem.frx":31B7
         stylesets(1).AlignmentText=   1
         stylesets(2).Name=   "Green"
         stylesets(2).BackColor=   10409634
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmADJItem.frx":31D3
         stylesets(2).AlignmentText=   1
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   26
         Columns.Count   =   6
         Columns(0).Width=   1535
         Columns(0).Caption=   "Resultados"
         Columns(0).Name =   "Proceso"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   12632256
         Columns(1).Width=   1905
         Columns(1).Caption=   "Abierto"
         Columns(1).Name =   "Abierto"
         Columns(1).Alignment=   1
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).HeadBackColor=   13168880
         Columns(1).BackColor=   13168880
         Columns(2).Width=   1773
         Columns(2).Caption=   "Consumido"
         Columns(2).Name =   "Consumido"
         Columns(2).Alignment=   1
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).HeadBackColor=   13168880
         Columns(2).BackColor=   13168880
         Columns(3).Width=   1799
         Columns(3).Caption=   "Adjudicado"
         Columns(3).Name =   "Adjudicado"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).HasHeadBackColor=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).HeadBackColor=   13168880
         Columns(3).BackColor=   13168880
         Columns(4).Width=   1588
         Columns(4).Caption=   "Ahorrado"
         Columns(4).Name =   "Ahorrado"
         Columns(4).Alignment=   1
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(4).HasHeadBackColor=   -1  'True
         Columns(4).HasBackColor=   -1  'True
         Columns(4).HeadBackColor=   13168880
         Columns(4).BackColor=   13168880
         Columns(5).Width=   1111
         Columns(5).Caption=   "%"
         Columns(5).Name =   "%"
         Columns(5).Alignment=   1
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(5).HasHeadBackColor=   -1  'True
         Columns(5).HasBackColor=   -1  'True
         Columns(5).HeadBackColor=   13168880
         Columns(5).BackColor=   13168880
         _ExtentX        =   9560
         _ExtentY        =   1676
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblObjetivo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   2680
         TabIndex        =   54
         Top             =   840
         Width           =   1100
      End
      Begin VB.Label lblObjLit 
         Caption         =   "DObjetivo:"
         Height          =   375
         Left            =   2200
         TabIndex        =   53
         Top             =   870
         Width           =   375
      End
      Begin VB.Shape ShapeBotones 
         Height          =   735
         Left            =   4700
         Top             =   60
         Width           =   1185
      End
      Begin VB.Label lblProveLit 
         Caption         =   "Prove Act."
         Height          =   195
         Left            =   3890
         TabIndex        =   42
         Top             =   870
         Width           =   765
      End
      Begin VB.Label lblUDLit 
         Caption         =   "UD"
         Height          =   195
         Left            =   3825
         TabIndex        =   41
         ToolTipText     =   "Unidad"
         Top             =   450
         Width           =   515
      End
      Begin VB.Label lblProve 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4700
         TabIndex        =   39
         Top             =   840
         Width           =   1185
      End
      Begin VB.Label lblPresTot 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   840
         TabIndex        =   38
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label lblPresTotalLit 
         Caption         =   "Pres.total"
         Height          =   195
         Left            =   60
         TabIndex        =   37
         Top             =   870
         Width           =   800
      End
      Begin VB.Label lblCantidad 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   2680
         TabIndex        =   36
         Top             =   420
         Width           =   1100
      End
      Begin VB.Label lblCantLit 
         Caption         =   "Cant."
         Height          =   195
         Left            =   2200
         TabIndex        =   35
         Top             =   450
         Width           =   435
      End
      Begin VB.Label lblPresUni 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   840
         TabIndex        =   34
         Top             =   420
         Width           =   1215
      End
      Begin VB.Label lblPresupuestoLit 
         Caption         =   "Pres.uni."
         Height          =   195
         Left            =   60
         TabIndex        =   33
         Top             =   450
         Width           =   800
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAdj 
      Height          =   4935
      Left            =   0
      TabIndex        =   64
      Top             =   1260
      Width           =   11355
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      GroupHeadLines  =   2
      Col.Count       =   13
      stylesets.count =   19
      stylesets(0).Name=   "ProvActual"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   9693431
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJItem.frx":31EF
      stylesets(1).Name=   "IntroGS"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmADJItem.frx":320B
      stylesets(1).AlignmentText=   2
      stylesets(1).AlignmentPicture=   2
      stylesets(2).Name=   "Yellow"
      stylesets(2).ForeColor=   0
      stylesets(2).BackColor=   13170165
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmADJItem.frx":3458
      stylesets(2).AlignmentText=   1
      stylesets(3).Name=   "Bold"
      stylesets(3).BackColor=   16777215
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmADJItem.frx":3474
      stylesets(4).Name=   "Normal"
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmADJItem.frx":3490
      stylesets(5).Name=   "YellowHead"
      stylesets(5).ForeColor=   0
      stylesets(5).BackColor=   13170165
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmADJItem.frx":34AC
      stylesets(6).Name=   "WhiteHead"
      stylesets(6).ForeColor=   0
      stylesets(6).BackColor=   16777215
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmADJItem.frx":34C8
      stylesets(7).Name=   "BlueHead"
      stylesets(7).ForeColor=   12582912
      stylesets(7).BackColor=   16777215
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmADJItem.frx":34E4
      stylesets(8).Name=   "AtribNoOfe"
      stylesets(8).BackColor=   13160660
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmADJItem.frx":3500
      stylesets(9).Name=   "Red"
      stylesets(9).BackColor=   4744445
      stylesets(9).HasFont=   -1  'True
      BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(9).Picture=   "frmADJItem.frx":351C
      stylesets(9).AlignmentText=   1
      stylesets(10).Name=   "Proveedor"
      stylesets(10).BackColor=   13160660
      stylesets(10).HasFont=   -1  'True
      BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(10).Picture=   "frmADJItem.frx":3538
      stylesets(10).AlignmentText=   0
      stylesets(10).AlignmentPicture=   1
      stylesets(11).Name=   "Blue"
      stylesets(11).ForeColor=   12582912
      stylesets(11).BackColor=   16777215
      stylesets(11).HasFont=   -1  'True
      BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(11).Picture=   "frmADJItem.frx":3554
      stylesets(11).AlignmentText=   1
      stylesets(12).Name=   "Adjudicado"
      stylesets(12).BackColor=   10079487
      stylesets(12).HasFont=   -1  'True
      BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(12).Picture=   "frmADJItem.frx":3570
      stylesets(13).Name=   "Atributo"
      stylesets(13).BackColor=   16777215
      stylesets(13).HasFont=   -1  'True
      BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(13).Picture=   "frmADJItem.frx":358C
      stylesets(14).Name=   "Importe"
      stylesets(14).BackColor=   16701391
      stylesets(14).HasFont=   -1  'True
      BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(14).Picture=   "frmADJItem.frx":35A8
      stylesets(15).Name=   "ItemCerrado"
      stylesets(15).ForeColor=   16777215
      stylesets(15).BackColor=   12632256
      stylesets(15).HasFont=   -1  'True
      BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(15).Picture=   "frmADJItem.frx":35C4
      stylesets(16).Name=   "YellowLeft"
      stylesets(16).ForeColor=   0
      stylesets(16).BackColor=   13170165
      stylesets(16).HasFont=   -1  'True
      BeginProperty stylesets(16).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(16).Picture=   "frmADJItem.frx":35E0
      stylesets(16).AlignmentText=   0
      stylesets(17).Name=   "Green"
      stylesets(17).BackColor=   10409634
      stylesets(17).HasFont=   -1  'True
      BeginProperty stylesets(17).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(17).Picture=   "frmADJItem.frx":35FC
      stylesets(17).AlignmentText=   1
      stylesets(18).Name=   "GrayHead"
      stylesets(18).ForeColor=   0
      stylesets(18).BackColor=   13160660
      stylesets(18).HasFont=   -1  'True
      BeginProperty stylesets(18).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(18).Picture=   "frmADJItem.frx":3618
      DividerStyle    =   0
      BevelColorFrame =   -2147483630
      BevelColorShadow=   -2147483633
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      UseExactRowCount=   0   'False
      SelectTypeCol   =   0
      SelectByCell    =   -1  'True
      CellNavigation  =   1
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   -2147483630
      ForeColorOdd    =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   212
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns.Count   =   13
      Columns(0).Width=   5503
      Columns(0).Caption=   "Proveedor"
      Columns(0).Name =   "PROVE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HeadStyleSet=   "Normal"
      Columns(0).StyleSet=   "Proveedor"
      Columns(1).Width=   1773
      Columns(1).Caption=   "Precio"
      Columns(1).Name =   "PRECIO"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).HeadStyleSet=   "Normal"
      Columns(1).StyleSet=   "Yellow"
      Columns(2).Width=   1773
      Columns(2).Caption=   "CantAdj"
      Columns(2).Name =   "CANTADJ"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HeadStyleSet=   "Normal"
      Columns(2).StyleSet=   "Blue"
      Columns(3).Width=   1588
      Columns(3).Caption=   "CantMax"
      Columns(3).Name =   "CANTMAX"
      Columns(3).Alignment=   1
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HeadStyleSet=   "Normal"
      Columns(3).StyleSet=   "Yellow"
      Columns(4).Width=   2117
      Columns(4).Caption=   "%Ahorro"
      Columns(4).Name =   "AHO_PORCEN"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HeadStyleSet=   "Normal"
      Columns(4).StyleSet=   "Yellow"
      Columns(5).Width=   2117
      Columns(5).Caption=   "Consumido"
      Columns(5).Name =   "CONSUM"
      Columns(5).Alignment=   1
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).HeadStyleSet=   "Normal"
      Columns(5).StyleSet=   "Yellow"
      Columns(6).Width=   2117
      Columns(6).Caption=   "Adjudicado"
      Columns(6).Name =   "ADJ"
      Columns(6).Alignment=   1
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).HeadStyleSet=   "Normal"
      Columns(6).StyleSet=   "Yellow"
      Columns(7).Width=   2117
      Columns(7).Caption=   "Ahorro"
      Columns(7).Name =   "AHO"
      Columns(7).Alignment=   1
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).HeadStyleSet=   "Normal"
      Columns(7).StyleSet=   "Yellow"
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "NUMOFE"
      Columns(8).Name =   "NUMOFE"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "PROVECOD"
      Columns(9).Name =   "PROVECOD"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   2117
      Columns(10).Caption=   "DImporte oferta"
      Columns(10).Name=   "IMP_OFE"
      Columns(10).Alignment=   1
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(10).HeadStyleSet=   "Normal"
      Columns(10).StyleSet=   "Importe"
      Columns(11).Width=   2117
      Columns(11).Caption=   "DAhorro oferta"
      Columns(11).Name=   "AHORRO_OFE"
      Columns(11).Alignment=   1
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(11).HeadStyleSet=   "Normal"
      Columns(11).StyleSet=   "Yellow"
      Columns(12).Width=   2117
      Columns(12).Caption=   "D%Ahorro oferta"
      Columns(12).Name=   "AHORRO_OFE_PORCEN"
      Columns(12).Alignment=   1
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(12).HeadStyleSet=   "Normal"
      Columns(12).StyleSet=   "Yellow"
      UseDefaults     =   0   'False
      _ExtentX        =   20029
      _ExtentY        =   8705
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAdjEsc 
      Height          =   4860
      Left            =   0
      TabIndex        =   65
      Top             =   1260
      Width           =   11940
      _Version        =   196617
      DataMode        =   2
      GroupHeadLines  =   2
      HeadLines       =   3
      Col.Count       =   19
      stylesets.count =   25
      stylesets(0).Name=   "ProvActual"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   9693431
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJItem.frx":3634
      stylesets(1).Name=   "NoHomologado"
      stylesets(1).ForeColor=   192
      stylesets(1).BackColor=   13170165
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmADJItem.frx":3650
      stylesets(1).AlignmentText=   1
      stylesets(2).Name=   "ItemCerradoClip"
      stylesets(2).ForeColor=   16777215
      stylesets(2).BackColor=   12632256
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmADJItem.frx":366C
      stylesets(2).AlignmentPicture=   0
      stylesets(3).Name=   "Item"
      stylesets(3).BackColor=   9876917
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmADJItem.frx":3688
      stylesets(4).Name=   "Yellow"
      stylesets(4).ForeColor=   0
      stylesets(4).BackColor=   13170165
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmADJItem.frx":36A4
      stylesets(4).AlignmentText=   1
      stylesets(5).Name=   "ProveNoAdj"
      stylesets(5).ForeColor=   16777215
      stylesets(5).BackColor=   -2147483633
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmADJItem.frx":36C0
      stylesets(6).Name=   "NoHomologadoClip"
      stylesets(6).ForeColor=   192
      stylesets(6).BackColor=   13170165
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmADJItem.frx":36DC
      stylesets(6).AlignmentPicture=   0
      stylesets(7).Name=   "Normal"
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmADJItem.frx":36F8
      stylesets(8).Name=   "YellowClip"
      stylesets(8).ForeColor=   0
      stylesets(8).BackColor=   13169908
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmADJItem.frx":3714
      stylesets(8).AlignmentPicture=   0
      stylesets(9).Name=   "YellowHead"
      stylesets(9).ForeColor=   0
      stylesets(9).BackColor=   13170165
      stylesets(9).HasFont=   -1  'True
      BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(9).Picture=   "frmADJItem.frx":3730
      stylesets(10).Name=   "Prove"
      stylesets(10).BackColor=   13170165
      stylesets(10).HasFont=   -1  'True
      BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(10).Picture=   "frmADJItem.frx":374C
      stylesets(11).Name=   "BlueHead"
      stylesets(11).ForeColor=   12582912
      stylesets(11).BackColor=   16777215
      stylesets(11).HasFont=   -1  'True
      BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(11).Picture=   "frmADJItem.frx":3768
      stylesets(12).Name=   "Red"
      stylesets(12).BackColor=   4744445
      stylesets(12).HasFont=   -1  'True
      BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(12).Picture=   "frmADJItem.frx":3784
      stylesets(12).AlignmentText=   1
      stylesets(13).Name=   "Proveedor"
      stylesets(13).BackColor=   -2147483633
      stylesets(13).HasFont=   -1  'True
      BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(13).Picture=   "frmADJItem.frx":37A0
      stylesets(13).AlignmentText=   0
      stylesets(13).AlignmentPicture=   1
      stylesets(14).Name=   "Blue"
      stylesets(14).ForeColor=   12582912
      stylesets(14).BackColor=   16777215
      stylesets(14).HasFont=   -1  'True
      BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(14).Picture=   "frmADJItem.frx":37BC
      stylesets(14).AlignmentText=   1
      stylesets(15).Name=   "Adjudicado"
      stylesets(15).BackColor=   10079487
      stylesets(15).HasFont=   -1  'True
      BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(15).Picture=   "frmADJItem.frx":37D8
      stylesets(16).Name=   "ProveedorConAdjuntos"
      stylesets(16).HasFont=   -1  'True
      BeginProperty stylesets(16).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(16).Picture=   "frmADJItem.frx":37F4
      stylesets(16).AlignmentText=   2
      stylesets(16).AlignmentPicture=   2
      stylesets(17).Name=   "HeadBold"
      stylesets(17).HasFont=   -1  'True
      BeginProperty stylesets(17).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(17).Picture=   "frmADJItem.frx":3810
      stylesets(18).Name=   "AdjudicadoClip"
      stylesets(18).BackColor=   10079487
      stylesets(18).HasFont=   -1  'True
      BeginProperty stylesets(18).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(18).Picture=   "frmADJItem.frx":382C
      stylesets(18).AlignmentPicture=   0
      stylesets(19).Name=   "Grey"
      stylesets(19).BackColor=   12632256
      stylesets(19).HasFont=   -1  'True
      BeginProperty stylesets(19).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   675
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(19).Picture=   "frmADJItem.frx":3848
      stylesets(19).AlignmentText=   1
      stylesets(19).AlignmentPicture=   2
      stylesets(20).Name=   "ItemCerrado"
      stylesets(20).ForeColor=   16777215
      stylesets(20).BackColor=   12632256
      stylesets(20).HasFont=   -1  'True
      BeginProperty stylesets(20).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(20).Picture=   "frmADJItem.frx":3864
      stylesets(21).Name=   "YellowLeft"
      stylesets(21).ForeColor=   0
      stylesets(21).BackColor=   13170165
      stylesets(21).HasFont=   -1  'True
      BeginProperty stylesets(21).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(21).Picture=   "frmADJItem.frx":3880
      stylesets(21).AlignmentText=   0
      stylesets(22).Name=   "Green"
      stylesets(22).BackColor=   10409634
      stylesets(22).HasFont=   -1  'True
      BeginProperty stylesets(22).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(22).Picture=   "frmADJItem.frx":389C
      stylesets(22).AlignmentText=   1
      stylesets(23).Name=   "DobleFondo"
      stylesets(23).HasFont=   -1  'True
      BeginProperty stylesets(23).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(23).Picture=   "frmADJItem.frx":38B8
      stylesets(23).AlignmentPicture=   5
      stylesets(24).Name=   "GrayHead"
      stylesets(24).ForeColor=   0
      stylesets(24).BackColor=   -2147483633
      stylesets(24).HasFont=   -1  'True
      BeginProperty stylesets(24).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(24).Picture=   "frmADJItem.frx":101DA
      UseGroups       =   -1  'True
      DividerStyle    =   0
      MultiLine       =   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      UseExactRowCount=   0   'False
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      CellNavigation  =   1
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   26
      CaptionAlignment=   0
      Groups.Count    =   2
      Groups(0).Width =   14340
      Groups(0).HeadStyleSet=   "GrayHead"
      Groups(0).Columns.Count=   19
      Groups(0).Columns(0).Width=   529
      Groups(0).Columns(0).Visible=   0   'False
      Groups(0).Columns(0).Caption=   "ID"
      Groups(0).Columns(0).Name=   "ID"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   582
      Groups(0).Columns(1).Visible=   0   'False
      Groups(0).Columns(1).Caption=   "ART"
      Groups(0).Columns(1).Name=   "ART"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(2).Width=   635
      Groups(0).Columns(2).Visible=   0   'False
      Groups(0).Columns(2).Caption=   "DEST"
      Groups(0).Columns(2).Name=   "DEST"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(3).Width=   714
      Groups(0).Columns(3).Visible=   0   'False
      Groups(0).Columns(3).Caption=   "CERRADO"
      Groups(0).Columns(3).Name=   "CERRADO"
      Groups(0).Columns(3).DataField=   "Column 3"
      Groups(0).Columns(3).DataType=   11
      Groups(0).Columns(3).FieldLen=   256
      Groups(0).Columns(4).Width=   767
      Groups(0).Columns(4).Visible=   0   'False
      Groups(0).Columns(4).Caption=   "UNI"
      Groups(0).Columns(4).Name=   "UNI"
      Groups(0).Columns(4).DataField=   "Column 4"
      Groups(0).Columns(4).DataType=   8
      Groups(0).Columns(4).FieldLen=   256
      Groups(0).Columns(5).Width=   873
      Groups(0).Columns(5).Visible=   0   'False
      Groups(0).Columns(5).Caption=   "INI"
      Groups(0).Columns(5).Name=   "INI"
      Groups(0).Columns(5).DataField=   "Column 5"
      Groups(0).Columns(5).DataType=   8
      Groups(0).Columns(5).FieldLen=   256
      Groups(0).Columns(6).Width=   1005
      Groups(0).Columns(6).Visible=   0   'False
      Groups(0).Columns(6).Caption=   "FIN"
      Groups(0).Columns(6).Name=   "FIN"
      Groups(0).Columns(6).DataField=   "Column 6"
      Groups(0).Columns(6).DataType=   8
      Groups(0).Columns(6).FieldLen=   256
      Groups(0).Columns(7).Width=   1138
      Groups(0).Columns(7).Visible=   0   'False
      Groups(0).Columns(7).Caption=   "PAG"
      Groups(0).Columns(7).Name=   "PAG"
      Groups(0).Columns(7).DataField=   "Column 7"
      Groups(0).Columns(7).DataType=   8
      Groups(0).Columns(7).FieldLen=   256
      Groups(0).Columns(8).Width=   1931
      Groups(0).Columns(8).Visible=   0   'False
      Groups(0).Columns(8).Caption=   "NUMOFE"
      Groups(0).Columns(8).Name=   "NUMOFE"
      Groups(0).Columns(8).DataField=   "Column 8"
      Groups(0).Columns(8).DataType=   8
      Groups(0).Columns(8).FieldLen=   256
      Groups(0).Columns(9).Width=   2778
      Groups(0).Columns(9).Visible=   0   'False
      Groups(0).Columns(9).Caption=   "Proveedor"
      Groups(0).Columns(9).Name=   "PROV"
      Groups(0).Columns(9).DataField=   "Column 9"
      Groups(0).Columns(9).DataType=   8
      Groups(0).Columns(9).FieldLen=   256
      Groups(0).Columns(9).Locked=   -1  'True
      Groups(0).Columns(9).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(9).StyleSet=   "YellowLeft"
      Groups(0).Columns(10).Width=   1958
      Groups(0).Columns(10).Caption=   "Description (Year/Dest)"
      Groups(0).Columns(10).Name=   "DESCR"
      Groups(0).Columns(10).CaptionAlignment=   0
      Groups(0).Columns(10).DataField=   "Column 10"
      Groups(0).Columns(10).DataType=   8
      Groups(0).Columns(10).FieldLen=   256
      Groups(0).Columns(10).Locked=   -1  'True
      Groups(0).Columns(10).HasHeadBackColor=   -1  'True
      Groups(0).Columns(10).HasBackColor=   -1  'True
      Groups(0).Columns(10).HeadBackColor=   -2147483633
      Groups(0).Columns(10).BackColor=   16776960
      Groups(0).Columns(10).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(10).StyleSet=   "YellowLeft"
      Groups(0).Columns(11).Width=   1323
      Groups(0).Columns(11).Caption=   "Quantity"
      Groups(0).Columns(11).Name=   "CANT"
      Groups(0).Columns(11).Alignment=   1
      Groups(0).Columns(11).CaptionAlignment=   2
      Groups(0).Columns(11).DataField=   "Column 11"
      Groups(0).Columns(11).DataType=   8
      Groups(0).Columns(11).NumberFormat=   "Standard"
      Groups(0).Columns(11).FieldLen=   256
      Groups(0).Columns(11).HasHeadForeColor=   -1  'True
      Groups(0).Columns(11).HasHeadBackColor=   -1  'True
      Groups(0).Columns(11).HasForeColor=   -1  'True
      Groups(0).Columns(11).HeadForeColor=   12582912
      Groups(0).Columns(11).HeadBackColor=   16777215
      Groups(0).Columns(11).ForeColor=   12582912
      Groups(0).Columns(11).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(11).StyleSet=   "Blue"
      Groups(0).Columns(12).Width=   2143
      Groups(0).Columns(12).Caption=   "Used amount"
      Groups(0).Columns(12).Name=   "PRECAPE"
      Groups(0).Columns(12).Alignment=   1
      Groups(0).Columns(12).CaptionAlignment=   2
      Groups(0).Columns(12).DataField=   "Column 12"
      Groups(0).Columns(12).DataType=   8
      Groups(0).Columns(12).NumberFormat=   "Standard"
      Groups(0).Columns(12).FieldLen=   256
      Groups(0).Columns(12).Locked=   -1  'True
      Groups(0).Columns(12).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(12).StyleSet=   "Yellow"
      Groups(0).Columns(13).Width=   2752
      Groups(0).Columns(13).Caption=   "Current Supplier"
      Groups(0).Columns(13).Name=   "CODPROVEACTUAL"
      Groups(0).Columns(13).Alignment=   1
      Groups(0).Columns(13).CaptionAlignment=   2
      Groups(0).Columns(13).DataField=   "Column 13"
      Groups(0).Columns(13).DataType=   8
      Groups(0).Columns(13).NumberFormat=   "Standard"
      Groups(0).Columns(13).FieldLen=   256
      Groups(0).Columns(13).Locked=   -1  'True
      Groups(0).Columns(13).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(13).StyleSet=   "Yellow"
      Groups(0).Columns(14).Width=   1905
      Groups(0).Columns(14).Caption=   "Dec. Amount"
      Groups(0).Columns(14).Name=   "IMP"
      Groups(0).Columns(14).Alignment=   1
      Groups(0).Columns(14).CaptionAlignment=   2
      Groups(0).Columns(14).DataField=   "Column 14"
      Groups(0).Columns(14).DataType=   8
      Groups(0).Columns(14).NumberFormat=   "Standard"
      Groups(0).Columns(14).FieldLen=   256
      Groups(0).Columns(14).Locked=   -1  'True
      Groups(0).Columns(14).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(14).StyleSet=   "Yellow"
      Groups(0).Columns(15).Width=   1773
      Groups(0).Columns(15).Caption=   "Saving"
      Groups(0).Columns(15).Name=   "AHORROIMP"
      Groups(0).Columns(15).Alignment=   1
      Groups(0).Columns(15).CaptionAlignment=   2
      Groups(0).Columns(15).DataField=   "Column 15"
      Groups(0).Columns(15).DataType=   8
      Groups(0).Columns(15).NumberFormat=   "Standard"
      Groups(0).Columns(15).FieldLen=   256
      Groups(0).Columns(15).Locked=   -1  'True
      Groups(0).Columns(15).HasHeadBackColor=   -1  'True
      Groups(0).Columns(15).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(16).Width=   2487
      Groups(0).Columns(16).Caption=   "Saving %"
      Groups(0).Columns(16).Name=   "AHORROPORCEN"
      Groups(0).Columns(16).Alignment=   2
      Groups(0).Columns(16).CaptionAlignment=   2
      Groups(0).Columns(16).DataField=   "Column 16"
      Groups(0).Columns(16).DataType=   8
      Groups(0).Columns(16).NumberFormat=   "0.0#\%"
      Groups(0).Columns(16).FieldLen=   256
      Groups(0).Columns(16).Locked=   -1  'True
      Groups(0).Columns(16).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(17).Width=   7011
      Groups(0).Columns(17).Visible=   0   'False
      Groups(0).Columns(17).Caption=   "CANTMAX"
      Groups(0).Columns(17).Name=   "CANTMAX"
      Groups(0).Columns(17).DataField=   "Column 17"
      Groups(0).Columns(17).DataType=   8
      Groups(0).Columns(17).FieldLen=   256
      Groups(0).Columns(18).Width=   7011
      Groups(0).Columns(18).Visible=   0   'False
      Groups(0).Columns(18).Caption=   "HOM"
      Groups(0).Columns(18).Name=   "HOM"
      Groups(0).Columns(18).DataField=   "Column 18"
      Groups(0).Columns(18).DataType=   8
      Groups(0).Columns(18).FieldLen=   256
      Groups(1).Width =   3201
      Groups(1).Visible=   0   'False
      Groups(1).Caption=   "ATRIB_CARAC"
      Groups(1).Columns.Count=   0
      UseDefaults     =   0   'False
      _ExtentX        =   21061
      _ExtentY        =   8572
      _StockProps     =   79
      Caption         =   "sdbgEscalado"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmADJItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'''''  Las clases CItem, CGrupo, CProceso tendr�n importes en moneda del proceso
'''''  Las clases CAdjudicacion, CAdjGrupo, CPrecioItem tendr�n importes en moneda de la oferta
'''''  Se har�n todos los calculos en la moneda original de la oferta y ya al final se hara la
'''''   conversi�n a moneda del proceso
'Constantes:
Private Const ANCHO_FILA = 269.8583
'Valor m�ximo de NVarcha(MAX)
Private Const cLongAtribEsp_Texto As Long = 32471
Private WithEvents cP As cPopupMenu
Attribute cP.VB_VarHelpID = -1
Private ColorWhite As ColorConstants
Private ColorButton As ColorConstants
Private ColorGrey As ColorConstants
Private ColorBlue As ColorConstants
Private m_sLayOut As String 'Guarda el fichero donde est� la configuraci�n inicial de la grid
Private m_sLayOutEsc As String
Private m_sTemp As String
Private m_vEnPedidos() As Variant
Private m_iInd As Integer
Private m_bCambianTodasAdjs As Boolean
Private m_bHayAtribsDeGrupoOProce As Boolean
Private m_ofrmADJGraficos As frmADJGraficos
'Acciones permitidas
Private m_bPreadjudicar As Boolean
Private m_bVal As Boolean
Private m_bOfe As Boolean
Private m_bHojaComparativa As Boolean
Private m_bPermSaltarseVolMaxAdjDir As Boolean
Private m_bModifOfeDeProv As Boolean
Private m_bModifOfeDeUsu As Boolean
Private m_bRestModifOfeProvUsu As Boolean
Private m_bRestModifOfeUsu As Boolean
Private m_bPermitirVistas As Boolean
Private m_bPermitirModifVistas As Boolean
Private m_bIgnBlqEscalacion As Boolean
Public errorcierre
Public g_oOrigen As Form
Public m_oProcesoSeleccionado As CProceso 'Pq se va a usar desde frmADJ y frmRESREU
Public m_oProvesAsig As CProveedores
Public m_oProvesUmbrales As CProveedores 'Proveedores con avisos o restricciones en Variables de calidad
Public g_ogrupo As CGrupo
Public g_oItemSeleccionado As CItem
Public m_oAtribsFormulas As CAtributos
Public m_oAtribsFormulasOld As CAtributos
Public m_oAdjs As CAdjsGrupo 'Aqu� se van manteniendo los valores de los grupos
Public g_oAdjsProveEsc As CAdjudicaciones   'Adjudicaciones calculadas a nivel de proveedor para escalados
Private m_oAdjParcs As CAdjudicaciones
Private m_oAdjsProve As CAdjudicaciones   'Colecci�n para las adjudicaciones totales por proveedor (c�lculos internos)
Private m_oAdjsProveEsc As CAdjudicaciones
Public m_oAdjParcsGrupo As CAdjsGrupo
'C�digo del grupo seleccionado en frmADJ o frmRESREU
Public g_sCodGrupo As String
Public g_dblAbiertoProc As Double
Public g_dblConsumidoProc As Double
Public g_dblAdjudicadoProc As Double
Public g_dblAdjudicadoProcAux As Double     'Para llevar el adjudicado por proveedor
Public g_dblAdjudicadoProcAuxMonProce As Double
Public g_dblAhorradoProc As Double
Public g_dblAhorradoPorcenProc As Double
Private sCantSinAdj  As String
'Formato para el n�mero de decimales
Private m_sFormatoNumber As String
Private m_sFormatoNumberPrecio As String
Private m_sFormatoNumberCant As String
'Clases para la configuraci�n de las vistas
Public g_oVistaSeleccionada As CConfVistaItem
Private m_bCargandoVista As Boolean
Private m_bDblClick As Boolean
'Idiomas
Private m_sPrecio As String
Private m_sCantAdj As String
Private m_sCantidadAdj As String
Private m_sCantMax As String
Private m_sAhorroPorcen As String
Private m_sAhorro As String
Private m_sConsumido As String
Private m_sAdjudicado As String
Private m_sProceso As String
Private m_sLitGrupo As String
Private m_sTodos As String
Private m_sItems As String
Private m_sItem As String
Private m_sTodosLosGrupos As String
Private m_sProve As String
Private m_sPuntos As String
Private m_sPrecUni As String
Private m_sTotalItem As String
Private m_sIdiObtenerHojaComp As String
Private m_sImporteOfe As String
Private m_sAhorroOfe As String
Private m_sAhorroOfePorcen As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_sIdiMayor As String
Private m_sIdiEntre As String
Private m_sIdiMenor As String
Private m_sVistaPlan As String
Private m_sVistaResp As String
Private m_sVistaUsu As String
Private m_sGuarNue As String
Private m_sRenonVista As String
Private m_sElimVista As String
Private m_sVistaIni As String
Private m_sPrecioImporte As String
Private m_sDest As String
Private m_sOtrasCarac As String
Private m_sEscalado As String
Private m_sPresUni As String
Private m_sObjetivo As String
Private m_sAhorUni As String
Private m_sImpAdj As String
Private m_sCantidad As String
Private m_sCantAdjudicada As String
Private m_sAdjudicar As String
Private m_sDesadjudicar As String
Private m_sProveAct As String
Private m_sImporteConsum As String
Private m_sPresUniMedio As String
Private m_sPrecUniMedio As String
Private m_bModError As Boolean
Private m_oIBaseDatos As IBaseDatos
'Variable para indicar si se va a recargar o no la comparativa cuando se vuelva a ella.
Private m_bRecargarOrigen As Boolean
Public m_lIdentificador As Long
Public m_sUsu  As String  'Usuario del que se cargan las vistas
Public m_sUsuVista As String 'Usuario de la vista, para cuando se permitan ver las vistas de otros usuarios
Public g_sNombreVista As String
Private oVistasCombo As CConfVistasProce
Private m_sIdiErrorEvaluacion(2) As String
Private m_bOcultarprove
Private m_bOcultarofertas
Private m_sLitRecalculando As String
Private m_bBorrarAdj As Boolean
Private m_iFilaItem As Integer
Private m_dblCantidadAnterior As Double
Private m_bRecalcular As Boolean
Private m_bRecalcularItem As Boolean
Private m_bCambioCantAdjEsc As Boolean
Private m_iGrupo As Integer
Private m_iRow As Integer
Private m_bRecalculando As Boolean
Private m_bComprobandoActualizarGrid As Boolean
Private m_bTabPressed As Boolean
Public m_dYaCerrado As Double
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sValidarOblSolCompraAdj_Inicial As String
Private m_sValidarOblSolCompraAdj_Final As String
Private m_sMsgError As String
Private Sub AnyadirColumnasParaAtributos()
    Dim oAtributo As CAtributo
    Dim oColumn As SSDataWidgets_B.Column
    Dim iCol As Integer

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iCol = sdbgAdj.Columns.Count
    
    ''Se cargan los atributos para el grupo seleccionado, de los tres ambitos
    'Los de ambito proceso
    If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
    For Each oAtributo In m_oProcesoSeleccionado.ATRIBUTOS
        sdbgAdj.Columns.Add iCol
        Set oColumn = sdbgAdj.Columns(iCol)
        With oColumn
            .Name = "ATRIB" & oAtributo.idAtribProce
            If oAtributo.Tipo = TipoNumerico Then
                .StyleSet = "Yellow"
                .Alignment = ssCaptionAlignmentRight
                If IsNull(oAtributo.PrecioAplicarA) Then
                    If IsNull(oAtributo.NumDecAtrib) Or IsEmpty(oAtributo.NumDecAtrib) Then
                        oAtributo.NumDecAtrib = ObtenerMaxDecAtributos(oAtributo.idAtribProce, "GeneralProc")
                    End If
                    If oAtributo.NumDecAtrib > g_oVistaSeleccionada.DecPrecios Then
                        .NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                    Else
                        .NumberFormat = FormateoNumericoComp(oAtributo.NumDecAtrib)
                    End If
                Else
                    .NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                End If
            Else
                .StyleSet = "YellowLeft"
                If oAtributo.Tipo = TipoString Then
                    .FieldLen = cLongAtribEsp_Texto
                End If
            End If
            .TagVariant = oAtributo.Tipo & "$P"
            .caption = oAtributo.Cod & " - " & oAtributo.Den
            .CaptionAlignment = ssCaptionAlignmentLeft
            If oAtributo.interno Then
                .HeadStyleSet = "IntroGS"
            Else
                .HeadStyleSet = "Normal"
            End If
            .Width = 160
            .Visible = True
            .Locked = True
        End With
        iCol = iCol + 1
    Next
    End If
    'Los de ambito grupo
    If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
    For Each oAtributo In m_oProcesoSeleccionado.AtributosGrupo
        If oAtributo.codgrupo = g_ogrupo.Codigo Or IsNull(oAtributo.codgrupo) Then
            sdbgAdj.Columns.Add iCol
            Set oColumn = sdbgAdj.Columns(iCol)
            With oColumn
                .Name = "ATRIB" & oAtributo.idAtribProce
                If oAtributo.Tipo = TipoNumerico Then
                    .StyleSet = "Yellow"
                    .Alignment = ssCaptionAlignmentRight
                    If IsNull(oAtributo.PrecioAplicarA) Then
                        If IsNull(oAtributo.NumDecAtrib) Or IsEmpty(oAtributo.NumDecAtrib) Then
                            oAtributo.NumDecAtrib = ObtenerMaxDecAtributos(oAtributo.idAtribProce, "GeneralGr", g_ogrupo)
                        End If
                        If oAtributo.NumDecAtrib > g_oVistaSeleccionada.DecPrecios Then
                            .NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                        Else
                            .NumberFormat = FormateoNumericoComp(oAtributo.NumDecAtrib)
                        End If
                    Else
                        .NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                    End If
                Else
                    .StyleSet = "YellowLeft"
                    If oAtributo.Tipo = TipoString Then
                        .FieldLen = cLongAtribEsp_Texto
                    End If
                End If
                .TagVariant = oAtributo.Tipo & "$G"
                .caption = oAtributo.Cod & " - " & oAtributo.Den
                .CaptionAlignment = ssCaptionAlignmentLeft
                If oAtributo.interno Then
                    .HeadStyleSet = "IntroGS"
                Else
                    .HeadStyleSet = "Normal"
                End If
                .Width = 1600
                .Visible = True
                .Locked = True
            End With
            iCol = iCol + 1
        End If
    Next
    End If
    'Los de ambito item
    If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
    For Each oAtributo In m_oProcesoSeleccionado.AtributosItem
        If oAtributo.codgrupo = g_ogrupo.Codigo Or IsNull(oAtributo.codgrupo) Then
            sdbgAdj.Columns.Add iCol
            Set oColumn = sdbgAdj.Columns(iCol)
            With oColumn
                .Name = "ATRIB" & oAtributo.idAtribProce
                If oAtributo.Tipo = TipoNumerico Then
                    .StyleSet = "Yellow"
                    .Alignment = ssCaptionAlignmentRight
                    If IsNull(oAtributo.PrecioAplicarA) Then
                        If IsNull(oAtributo.NumDecAtrib) Or IsEmpty(oAtributo.NumDecAtrib) Then
                            oAtributo.NumDecAtrib = ObtenerMaxDecAtributos(oAtributo.idAtribProce, "Grupo", g_ogrupo)
                        End If
                        If oAtributo.NumDecAtrib > g_oVistaSeleccionada.DecPrecios Then
                            .NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                        Else
                            .NumberFormat = FormateoNumericoComp(oAtributo.NumDecAtrib)
                        End If
                    Else
                        .NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                    End If
                Else
                    .StyleSet = "YellowLeft"
                    If oAtributo.Tipo = TipoString Then
                        .FieldLen = cLongAtribEsp_Texto
                    End If
                End If
                .TagVariant = oAtributo.Tipo & "$I"
                .caption = oAtributo.Cod & " - " & oAtributo.Den
                .CaptionAlignment = ssCaptionAlignmentLeft
                If oAtributo.interno Then
                    .HeadStyleSet = "IntroGS"
                Else
                    .HeadStyleSet = "Normal"
                End If
                .Width = 1600
                .Visible = True
                .Locked = True
            End With
            iCol = iCol + 1
        End If
    Next
    End If

    If m_oProcesoSeleccionado.UsarPonderacion Then
        sdbgAdj.Columns.Add iCol
        Set oColumn = sdbgAdj.Columns(iCol)
        With oColumn
            .Name = "PUNTOS"
            .StyleSet = "Yellow"
            .NumberFormat = m_sFormatoNumber
            .caption = m_sPuntos
            .Alignment = ssCaptionAlignmentRight
            .CaptionAlignment = ssCaptionAlignmentRight
            .HeadStyleSet = "Normal"
            .Width = 1600
            .Visible = True
            .Locked = True
        End With
    End If
    
Set oAtributo = Nothing
Set oColumn = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "AnyadirColumnasParaAtributos", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub
''' <summary>
''' Calcular Ponderacion
''' </summary>
''' <param name="sProve">Prove</param>
''' <returns>Ponderacion</returns>
''' <remarks>Llamada desde: CargarGridAdjudicaciones ; Tiempo m�ximo: 0,2</remarks>
Private Function CalcularPonderacion(ByVal sProve As String) As Double
    Dim vValor As Variant
    Dim oAtribOfer As CAtributoOfertado

    'Si no est� calculada, calcula la ponderaci�n
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oOrigen.g_PonderacionCalculada Then
        g_oOrigen.CalcularPonderacionAtributosOfertados
    End If
    
    'Ahora se suman todos los puntos de las ponderaciones para el proveedor sProve
    If Not m_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados Is Nothing Then
        For Each oAtribOfer In m_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados
            vValor = vValor + NullToDbl0(oAtribOfer.ValorPond)
        Next
    End If
    
    If Not m_oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados Is Nothing Then
        For Each oAtribOfer In m_oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados
            If oAtribOfer.Grupo = g_ogrupo.Codigo Then
                vValor = vValor + NullToDbl0(oAtribOfer.ValorPond)
            End If
        Next
    End If
    
    If Not g_ogrupo.UltimasOfertas.Item(Trim(sProve)) Is Nothing Then
        If Not g_ogrupo.UltimasOfertas.Item(Trim(sProve)).AtribItemOfertados Is Nothing Then
            For Each oAtribOfer In g_ogrupo.UltimasOfertas.Item(Trim(sProve)).AtribItemOfertados
                If oAtribOfer.Item = g_oItemSeleccionado.Id Then
                    vValor = vValor + NullToDbl0(oAtribOfer.ValorPond)
                End If
            Next
        End If
    End If
            
    Set oAtribOfer = Nothing
    CalcularPonderacion = NullToDbl0(vValor)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CalcularPonderacion", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Sub CargarComboItems()
Dim oItem As CItem
Dim oGrupo As CGrupo
Dim bCargaItems As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcItems.RemoveAll
    
    If g_sCodGrupo = "" Then
        'Carga todos los items del proceso
        For Each oGrupo In m_oProcesoSeleccionado.Grupos
            bCargaItems = True
            If m_oProcesoSeleccionado.AdminPublica = True Then
                If m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                    bCargaItems = False
                End If
            End If
            If bCargaItems = True Then
                For Each oItem In oGrupo.Items
                    If oItem.Confirmado = True Then
                        sdbcItems.AddItem oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador) & oItem.proceso.Anyo & "/" & oItem.DestCod & Chr(m_lSeparador) & oItem.grupoCod & Chr(m_lSeparador) & oItem.Id
                    End If
                Next
            End If
        Next
        
    Else
        'Es un grupo en concreto
        For Each oItem In g_ogrupo.Items
            If oItem.Confirmado = True Then
                sdbcItems.AddItem oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador) & oItem.proceso.Anyo & "/" & oItem.DestCod & Chr(m_lSeparador) & oItem.grupoCod & Chr(m_lSeparador) & oItem.Id
            End If
        Next
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CargarComboItems", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>Carga el grid sdbgAdj</summary>
''' <remarks>Llamada desde:chkOcultarNoAdj_Click       chkOcultarProvSinOfe_Click      cmdAplicarPrecio_Click
'''           sdbcVistaActual_CloseUp     sdbgAdj_AfterColUpdate          sdbgOcultarProv_Change
'''           CalcularOptima itemSeleccionado; Tiempo m�ximo: 0,2</remarks>
''' <revision>LTG 24/10/2011</revision>

Private Sub CargarGridAdjudicaciones()
    Dim oAdj As CAdjudicacion
    Dim sFilaGrid As String
    Dim oProve As CProveedor
    Dim dblPrecioOfe As Double
    Dim dblConsumido As Double
    Dim dblPorcenAdj As Double
    Dim dblAdjudicado As Double
    Dim dblCantAdj As Double
    Dim bAdj As Boolean
    Dim idAtrib As Long
    Dim i As Integer
    Dim sTipoAtrib As String
    Dim oAtribOfertado As CAtributoOfertado
    Dim dblConsumidoTotal As Double
    Dim dblAdjudicadoTotal As Double
    Dim dblAhorradoTotal As Double
    Dim dblPond As Double
    Dim iNumOfe As Integer
    Dim sCod As String
    Dim scod1 As String
    Dim dblCantMax As Double
    Dim dImporteOfe As Double
    Dim dAhorroOfe As Double
    Dim dAhorroOfePorcen As Double
    Dim bProvVisible As Boolean
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim dblCambio As Double
    
    'Grid de Adjudicaciones
    'Se va construyendo cada fila para el AddItem de la grid
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAdj.RemoveAll
    
    For Each oProve In m_oProvesAsig
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(g_oItemSeleccionado.grupoCod) Is Nothing Then
                bCargar = False
            End If
        End If
        
        If bCargar Then
            Select Case g_oVistaSeleccionada.ConfVistasItemProv.Item(CStr(oProve.Cod)).Visible
                Case TipoProvVisible.Visible
                    bProvVisible = True
                Case TipoProvVisible.NoVisible
                    bProvVisible = False
                Case TipoProvVisible.DepenVista
                    'Si tiene los checks marcados comprueba si tiene ofertas y si la oferta es adjudicable:
                    bProvVisible = True
                    If g_oVistaSeleccionada.OcultarProvSinOfe Then
                        If Not g_ogrupo.UltimasOfertas Is Nothing Then
                            If g_ogrupo.UltimasOfertas.Item(Trim(CStr(oProve.Cod))) Is Nothing Then
                                bProvVisible = False
                            End If
                        Else
                            bProvVisible = False
                        End If
                    End If
                    If bProvVisible And g_oVistaSeleccionada.OcultarNoAdj Then
                        If Not m_oProcesoSeleccionado.Ofertas Is Nothing Then
                            If m_oProcesoSeleccionado.Ofertas.Item(CStr(oProve.Cod)) Is Nothing Then
                                bProvVisible = False
                            Else
                                If Not g_oOrigen.m_oEstOfes.Item(m_oProcesoSeleccionado.Ofertas.Item(CStr(oProve.Cod)).CodEst).adjudicable Then
                                    bProvVisible = False
                                End If
                            End If
                        Else
                            bProvVisible = False
                        End If
                    End If
            End Select
            
            'Solo se cargan los proveedores que est�n visibles
            If bProvVisible Then
                sFilaGrid = oProve.Cod & " " & oProve.Den 'Proveedores
                If Not g_ogrupo.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                    With g_ogrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                        dblCambio = g_oItemSeleccionado.CambioComparativa(oProve.Cod)
                        
                        iNumOfe = .Num
                        If .Cambio = 0 Then
                            dblPrecioOfe = 0
                        Else
                            If IsNull(.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta) Then
                                dblPrecioOfe = 0
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" 'Precio
                            Else
                                dblPrecioOfe = NullToDbl0(.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta / dblCambio)
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & dblPrecioOfe  'Precio
                            End If
                        End If
    
                        sCod = CStr(g_oItemSeleccionado.Id) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                        Set oAdj = g_ogrupo.Adjudicaciones.Item(sCod)
                        If Not oAdj Is Nothing Then
                            dblPorcenAdj = oAdj.Porcentaje
                            dblCantAdj = (dblPorcenAdj * g_oItemSeleccionado.Cantidad) / 100
                            bAdj = True

                            If g_oItemSeleccionado.Cantidad = 0 Then
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "0" 'Cant Adj.
                            Else
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & IIf(dblCantAdj = 0, "", dblCantAdj) 'Cant Adj.
                            End If
                        Else
                            dblCantAdj = 0
                            dblPorcenAdj = 0
                            bAdj = False
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" 'Cant Adj.
                        End If
                        If m_oProcesoSeleccionado.SolicitarCantMax Then
                            dblCantMax = NullToDbl0(.Lineas.Item(CStr(g_oItemSeleccionado.Id)).CantidadMaxima)
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & IIf(dblCantMax = 0, "", dblCantMax) 'Cant.Max
                        Else
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" 'Cant Max.
                        End If
    
                        If dblPorcenAdj > 0 Then
                            dblConsumido = g_oItemSeleccionado.Cantidad * NullToDbl0(g_oItemSeleccionado.Precio) * (dblPorcenAdj / 100)
                            dblAdjudicado = oAdj.ImporteAdjTot / dblCambio
                            If dblConsumido = 0 Then
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "0"
                            Else
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ((dblConsumido - dblAdjudicado) / Abs(dblConsumido)) * 100 '%Ahorrado
                            End If
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & dblConsumido  'Consumido
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & dblAdjudicado 'Adjudicado
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & (dblConsumido - dblAdjudicado) 'Ahorrado
                        Else
                            dblConsumido = 0
                            dblAdjudicado = 0
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""  '%Ahorrado
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""  'Consumido
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" 'Adjudicado
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" 'Ahorrado
                        End If
                        
                        dblConsumidoTotal = dblConsumidoTotal + dblConsumido
                        dblAdjudicadoTotal = dblAdjudicadoTotal + dblAdjudicado
                        dblAhorradoTotal = dblAhorradoTotal + (dblConsumido - dblAdjudicado)
                        
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & iNumOfe 'Num Oferta
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oProve.Cod 'Prove Cod
                        
                        'Importe oferta
                        If IsNull(.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta) Then
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                        Else
                            If Not oAdj Is Nothing Then
                                If Not IsNull(oAdj.importe) Then
                                    dImporteOfe = oAdj.importe / dblCambio
                                Else
                                    dImporteOfe = NullToDbl0(g_oItemSeleccionado.Cantidad * dblPrecioOfe)
                                End If
                            Else
                                dImporteOfe = NullToDbl0(g_oItemSeleccionado.Cantidad * dblPrecioOfe)
                            End If
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & dImporteOfe
                        End If
                        
                        If IsNull(.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta) Then
                            'Ahorro oferta
                            dAhorroOfe = 0
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                            
                            ' %Ahorro oferta
                            dAhorroOfePorcen = 0
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                        Else
                            'Ahorro oferta
                            dAhorroOfe = (NullToDbl0(g_oItemSeleccionado.Cantidad) * NullToDbl0(g_oItemSeleccionado.Precio)) - dImporteOfe
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & dAhorroOfe
                            
                            ' %Ahorro oferta
                            If (g_oItemSeleccionado.Cantidad * NullToDbl0(g_oItemSeleccionado.Precio)) = 0 Then
                                dAhorroOfePorcen = 0
                            Else
                                If NullToDbl0(g_oItemSeleccionado.Cantidad) * NullToDbl0(g_oItemSeleccionado.Precio) <> 0 Then
                                    dAhorroOfePorcen = (dAhorroOfe / Abs((NullToDbl0(g_oItemSeleccionado.Cantidad) * NullToDbl0(g_oItemSeleccionado.Precio)))) * 100
                                End If
                            End If
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & dAhorroOfePorcen
                        End If
                        
                        'Atributos
                        For i = 10 To sdbgAdj.Cols - 1
                            If sdbgAdj.Columns(i).Name = "PUNTOS" Then
                                dblPond = CalcularPonderacion(oProve.Cod)
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & dblPond
                            Else
                                Select Case Right(sdbgAdj.Columns(i).TagVariant, 1)
                                    Case "P"
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribProcOfertados Is Nothing Then
                                            idAtrib = DevolverIdAtributo(sdbgAdj.Columns(i).Name)
                                            If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(idAtrib)) Is Nothing Then
                                                Set oAtribOfertado = m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(idAtrib))
                                                sTipoAtrib = Left(sdbgAdj.Columns(i).TagVariant, 1)
                                                Select Case sTipoAtrib
                                                    Case TiposDeAtributos.TipoString
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & EliminarTab(NullToStr(oAtribOfertado.valorText))
                                                    Case TiposDeAtributos.TipoNumerico 'Si aplicar a precio + o - le hago el cambio
                                                        If m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(idAtrib)).PrecioFormula = "+" Or m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(idAtrib)).PrecioFormula = "-" Then
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAtribOfertado.valorNum / dblCambio
                                                        Else
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAtribOfertado.valorNum
                                                        End If
                                                    Case TiposDeAtributos.TipoFecha
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAtribOfertado.valorFec
                                                    Case TiposDeAtributos.TipoBoolean
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & DevolverValorBoolean(oAtribOfertado.valorBool)
                                                End Select
                                                Set oAtribOfertado = Nothing
                                            Else
                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                                            End If
                                        Else
                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                                        End If
                                    Case "G"
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribGrOfertados Is Nothing Then
                                            idAtrib = DevolverIdAtributo(sdbgAdj.Columns(i).Name)
                                            scod1 = g_oItemSeleccionado.grupoCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(g_oItemSeleccionado.grupoCod))
                                            scod1 = scod1 & CStr(idAtrib)
                                            
                                            If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(scod1) Is Nothing Then
                                                Set oAtribOfertado = m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(scod1)
                                                sTipoAtrib = Left(sdbgAdj.Columns(i).TagVariant, 1)
                                                Select Case sTipoAtrib
                                                    Case TiposDeAtributos.TipoString
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & EliminarTab(NullToStr(oAtribOfertado.valorText))
                                                    Case TiposDeAtributos.TipoNumerico 'Si aplicar a precio + o - le hago el cambio
                                                            If m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(idAtrib)).PrecioFormula = "+" Or m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(idAtrib)).PrecioFormula = "-" Then
                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAtribOfertado.valorNum / m_oProcesoSeleccionado.Grupos.Item(g_oItemSeleccionado.grupoCod).CambioComparativa(oProve.Cod)
                                                            Else
                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAtribOfertado.valorNum
                                                            End If
                                                    Case TiposDeAtributos.TipoFecha
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAtribOfertado.valorFec
                                                    Case TiposDeAtributos.TipoBoolean
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & DevolverValorBoolean(oAtribOfertado.valorBool)
                                                End Select
                                                Set oAtribOfertado = Nothing
                                            Else
                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                                            End If
                                        Else
                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                                        End If
                                    Case "I"
                                        If Not .AtribItemOfertados Is Nothing Then
                                            idAtrib = DevolverIdAtributo(sdbgAdj.Columns(i).Name)
                                            If Not .AtribItemOfertados.Item(g_oItemSeleccionado.Id & "$" & CStr(idAtrib)) Is Nothing Then
                                                Set oAtribOfertado = .AtribItemOfertados.Item(g_oItemSeleccionado.Id & "$" & CStr(idAtrib))
    
                                                sTipoAtrib = Left(sdbgAdj.Columns(i).TagVariant, 1)
                                                Select Case sTipoAtrib
                                                    Case TiposDeAtributos.TipoString
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & EliminarTab(NullToStr(oAtribOfertado.valorText))
                                                    Case TiposDeAtributos.TipoNumerico 'Si aplicar a precio + o - le hago el cambio
                                                        If m_oProcesoSeleccionado.AtributosItem.Item(CStr(idAtrib)).PrecioFormula = "+" Or m_oProcesoSeleccionado.AtributosItem.Item(CStr(idAtrib)).PrecioFormula = "-" Then
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAtribOfertado.valorNum / dblCambio
                                                        Else
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAtribOfertado.valorNum
                                                        End If
                                                    Case TiposDeAtributos.TipoFecha
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAtribOfertado.valorFec
                                                    Case TiposDeAtributos.TipoBoolean
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & DevolverValorBoolean(oAtribOfertado.valorBool)
                                                End Select
                                                Set oAtribOfertado = Nothing
                                            Else
                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                                            End If
                                        Else
                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                                        End If
                                End Select
                            End If
                        Next
    
                    End With
                Else
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oProve.Cod
                End If
                
                sdbgAdj.AddItem sFilaGrid
            End If
        End If
    Next
    
    CalcularTotalesDeProceso
    CargarGridResultados dblConsumidoTotal, dblAhorradoTotal, dblAdjudicadoTotal

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CargarGridAdjudicaciones", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Carga el grid sdbgAdjEsc</summary>
''' <param name="iNumAtribCaract">N� de atributos de caracter�stica</param>
''' <remarks>Llamada desde: ItemEscaladoSeleccionado; Tiempo m�ximo: 0,2</remarks>

Private Sub CargarGridEscAdjudicaciones(ByVal iNumAtribCaract As Integer)
    Dim dblConsumidoTotal As Double
    Dim dblAhorradoTotal As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAdjEsc.RemoveAll
    sdbgAdjEsc.MoveFirst
    
    If Not g_ogrupo Is Nothing Then
        'A�adimos el item al grid
        AddItem g_oItemSeleccionado, iNumAtribCaract

        'A�adimos los proveedores
        AddProveedores g_oItemSeleccionado
        
        CalcularTotalesDeProceso
        
        dblConsumidoTotal = DevolverImporteConsumido
        dblAhorradoTotal = dblConsumidoTotal - g_oItemSeleccionado.ImporteAdj 'ImporteAdj en moneda proceso
        CargarGridResultados dblConsumidoTotal, dblAhorradoTotal, g_oItemSeleccionado.ImporteAdj
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CargarGridEscAdjudicaciones", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Rellena los datos del grid de resultados</summary>
''' <remarks>Llamada desde: CargarGridEscAdjudicaciones ; Tiempo m�ximo:0</remarks>

Private Sub CargarGridResultados(ByVal dblConsumidoTotal As Double, ByVal dblAhorradoTotal As Double, ByVal dblAdjudicadoTotal As Double)
    Dim dblAbierto As Double
    Dim dblPorcen As Double
    Dim vPrecio As Variant
    Dim vObjetivo As Variant
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_ogrupo.UsarEscalados Then
        PresupuestoYObjetivoItemConEscalados g_ogrupo, g_oItemSeleccionado, m_oProvesAsig, vPrecio, vObjetivo
        
        If Not IsNull(g_oItemSeleccionado.Cantidad) And Not IsEmpty(g_oItemSeleccionado.Cantidad) Then
            dblAbierto = g_oItemSeleccionado.Cantidad * NullToDbl0(vPrecio)
        End If
    Else
        dblAbierto = g_oItemSeleccionado.Cantidad * NullToDbl0(g_oItemSeleccionado.Precio)
    End If
    If dblConsumidoTotal = 0 Then
        dblPorcen = 0
    Else
        dblPorcen = (dblAhorradoTotal / Abs(dblConsumidoTotal)) * 100
    End If
    
    sdbgResultados.RemoveAll
    sdbgResultados.AddItem m_sProceso & Chr(m_lSeparador) & g_dblAbiertoProc & Chr(m_lSeparador) & g_dblConsumidoProc & Chr(m_lSeparador) & g_dblAdjudicadoProc & Chr(m_lSeparador) & g_dblAhorradoProc & Chr(m_lSeparador) & g_dblAhorradoPorcenProc
    If g_ogrupo.Consumido = 0 Then
        sdbgResultados.AddItem m_sLitGrupo & Chr(m_lSeparador) & g_ogrupo.Abierto & Chr(m_lSeparador) & g_ogrupo.Consumido & Chr(m_lSeparador) & g_ogrupo.AdjudicadoTotal & Chr(m_lSeparador) & (g_ogrupo.Consumido - g_ogrupo.AdjudicadoTotal) & Chr(m_lSeparador) & 0
    Else
        sdbgResultados.AddItem m_sLitGrupo & Chr(m_lSeparador) & g_ogrupo.Abierto & Chr(m_lSeparador) & g_ogrupo.Consumido & Chr(m_lSeparador) & g_ogrupo.AdjudicadoTotal & Chr(m_lSeparador) & (g_ogrupo.Consumido - g_ogrupo.AdjudicadoTotal) & Chr(m_lSeparador) & IIf(g_ogrupo.Consumido = 0, 0, ((g_ogrupo.Consumido - g_ogrupo.AdjudicadoTotal) / Abs(g_ogrupo.Consumido)) * 100)
    End If
    sdbgResultados.AddItem m_sItem & Chr(m_lSeparador) & dblAbierto & Chr(m_lSeparador) & dblConsumidoTotal & Chr(m_lSeparador) & dblAdjudicadoTotal & Chr(m_lSeparador) & dblAhorradoTotal & Chr(m_lSeparador) & dblPorcen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CargarGridResultados", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub
 
''' <summary>A�ade un item al grid</summary>
''' <param name="oItem">Item</param>
''' <param name="iNumAtribCarac">N� de atributos de caracter�sticas que se muestran en el grid</param>
''' <remarks>Llamada desde: CargarGridEscAdjudicaciones ; Tiempo m�ximo:0</remarks>
''' <revision>LTG 21/02/2012</revision>
 
Private Sub AddItem(ByVal oItem As CItem, ByVal iNumAtribCarac As Integer)
    Dim oProve As CProveedor
    Dim oAsig As COferta
    Dim oEscalado As CEscalado
    Dim oEscItem As CEscalado
    Dim oatrib As CAtributo
    Dim vImporteAhorroItem As Variant
    Dim sAnyo As String
    Dim sFilaGrid As String
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim sCod As String
    Dim vImporteItem As Variant
    Dim i As Integer
    Dim sPresup As String
    Dim sObjetivo As String
    Dim sCantAdj As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sAnyo = m_sDest & ": " & Right(Year(oItem.FechaInicioSuministro), 2) & "/" & oItem.DestCod
    
    With oItem
        sFilaGrid = .Id & Chr(m_lSeparador) & .ArticuloCod & Chr(m_lSeparador) & .DestCod & Chr(m_lSeparador) & .Cerrado & Chr(m_lSeparador)
        sFilaGrid = sFilaGrid & .UniCod & Chr(m_lSeparador) & .FechaInicioSuministro & Chr(m_lSeparador) & .FechaFinSuministro & Chr(m_lSeparador)
        sFilaGrid = sFilaGrid & .PagCod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador)
        sFilaGrid = sFilaGrid & .ArticuloCod & " " & .Descr & " (" & sAnyo & ")"
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & NullToStr(.Cantidad)
        'Importe consumido
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & IIf(.Consumido = 0, "", .Consumido)
        'Proveedor actual
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & .ProveAct
        'Importe adjudicado
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & IIf(IsNull(.ImporteAdj) Or IsEmpty(.ImporteAdj) Or NullToDbl0(.ImporteAdj) = 0, "", .ImporteAdj)
    End With

    vImporteAhorroItem = 0
    For Each oProve In m_oProvesAsig
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(g_ogrupo.Codigo) Is Nothing Then
                bCargar = False
            End If
        End If
        If bCargar Then
            If Not g_ogrupo.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                Set oAsig = g_ogrupo.UltimasOfertas.Item(Trim(oProve.Cod))

                'Carga las adjudicaciones
                sCod = oAsig.Prove & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAsig.Prove))
                sCod = CStr(oItem.Id) & sCod

                If Not m_oAdjsProve.Item(sCod) Is Nothing Then
                    vImporteAhorroItem = vImporteAhorroItem + m_oAdjsProve.Item(sCod).importe   'Se utiliza la prop. Importe para guardar el ahorro
                    vImporteItem = vImporteItem + m_oAdjsProve.Item(sCod).ImporteAdjTot 'Se utiliza la prop. ImporteAdjTot para guardar el importe seg�n los datos del item
                End If
            End If
        End If
    Next   'Pasa al siguiente proveedor
    Set oProve = Nothing

    'Columnas AHORROIMP, AHORROPORCEN
    If IsNull(vImporteAhorroItem) Or IsEmpty(vImporteAhorroItem) Then
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
    Else
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & IIf(vImporteAhorroItem = 0, "", vImporteAhorroItem)
        If vImporteItem > 0 Then
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & (vImporteAhorroItem / vImporteItem) * 100
        Else
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
        End If
    End If

    'columnas CantMax,HOM
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""

    'Columnas de atributos de caracter�sticas
    For i = 1 To iNumAtribCarac
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
    Next

    'Datos de los escalados
    If Not g_ogrupo.Escalados Is Nothing Then
        If g_ogrupo.Escalados.Count > 0 Then
            For Each oEscalado In g_ogrupo.Escalados
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oEscalado.Id & Chr(m_lSeparador) & oEscalado.Inicial
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & NullToStr(oEscalado.final)

                sPresup = vbNullString
                sObjetivo = vbNullString
                If Not oItem.Escalados Is Nothing Then
                    If Not oItem.Escalados.Item(CStr(oEscalado.Id)) Is Nothing Then
                        Set oEscItem = oItem.Escalados.Item(CStr(oEscalado.Id))
                        
                        sPresup = NullToStr(oItem.PresupuestoEscalado(oEscalado.Id))
                        sObjetivo = NullToStr(oEscItem.Objetivo)

                        Set oEscItem = Nothing
                    End If
                End If
                If sPresup = "" Then
                    sPresup = NullToStr(oItem.Precio)
                End If

                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sPresup

                'Columnas de las adjudicaciones
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sObjetivo
                sCantAdj = NullToStr(CantidadAdjItemEscalado(g_ogrupo, oItem.Id, oEscalado.Id, m_oProvesAsig))
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCantAdj & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""

                'columnas de los atributos de costes/descuentos
                For Each oatrib In g_ogrupo.AtributosItem
                    If Not IsNull(oatrib.PrecioFormula) And Not IsEmpty(oatrib.PrecioFormula) Then
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                    End If
                Next
            Next

            Set oEscalado = Nothing
        End If
    End If
    
    'A�ade la fila a la grid
    sdbgAdjEsc.AddItem sFilaGrid

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "AddItem", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>A�ade los proveedores al grid</summary>
''' <param name="oItem">Item para el que se est�n a�adiendo los proveedores</param>
''' <remarks>Llamada desde: CargarGridEscAdjudicaciones ; Tiempo m�ximo:0</remarks>
Private Sub AddProveedores(ByVal oItem As CItem)
    Dim oProve As CProveedor
    Dim bCargar As Boolean
    Dim scodProve As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For Each oProve In m_oProvesAsig
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            
            Select Case g_oVistaSeleccionada.ConfVistasItemProv.Item(CStr(oProve.Cod)).Visible
                Case TipoProvVisible.Visible
                    bCargar = True
                Case TipoProvVisible.NoVisible
                    bCargar = False
                Case TipoProvVisible.DepenVista
                    'Si tiene los checks marcados comprueba si tiene ofertas y si la oferta es adjudicable:
                    bCargar = True
                    If g_oVistaSeleccionada.OcultarProvSinOfe Then
                        If Not g_ogrupo.UltimasOfertas Is Nothing Then
                            If g_ogrupo.UltimasOfertas.Item(Trim(CStr(oProve.Cod))) Is Nothing Then
                                bCargar = False
                            End If
                        Else
                            bCargar = False
                        End If
                    End If
                    If bCargar And g_oVistaSeleccionada.OcultarNoAdj Then
                        If Not m_oProcesoSeleccionado.Ofertas Is Nothing Then
                            If m_oProcesoSeleccionado.Ofertas.Item(CStr(oProve.Cod)) Is Nothing Then
                                bCargar = False
                            Else
                                If Not g_oOrigen.m_oEstOfes.Item(m_oProcesoSeleccionado.Ofertas.Item(CStr(oProve.Cod)).CodEst).adjudicable Then
                                    bCargar = False
                                End If
                            End If
                        Else
                            bCargar = False
                        End If
                    End If
            End Select
        End If
        
        If bCargar Then
            AddProveedor oItem, oProve
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "AddProveedores", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>A�ade un proveedor al grid</summary>
''' <param name="oItem">Item para el que se est�n a�adiendo los proveedores</param>
''' <param name="sCodProve">C�digo del proveedor a a�adir</param>
''' <param name="iIndex">Indice del grid en que se quiere a�adir el proveedor</param>
''' <remarks>Llamada desde: AddProveedores ; Tiempo m�ximo:0</remarks>
''' <revision>LTG 21/02/2012</revision>
Private Sub AddProveedor(ByVal oItem As CItem, ByVal oProve As CProveedor, Optional ByVal vIndex As Variant)
    Dim scodProve As String
    Dim oAsig As COferta
    Dim oatrib  As CAtributo
    Dim sFilaGrid As String
    Dim sCodAtrib As String
    Dim sValorAtrib As String
    Dim oEscalado As CEscalado
    Dim sCod As String
    Dim strCantidad As String
    Dim strPorcen As String
    Dim strImporteAdj As String
    Dim sAhorro As String
    Dim strCantAdjProve As String
    Dim strImpAdjProve As String
    Dim strAhorroImpProve As String
    Dim strAhorroPorcenProve As String
    Dim strPresUniProve As String
    Dim strPrecUniProve As String
    Dim dblImporteProve As Double
    Dim vPresEsc As Variant
    Dim strAdj As String
    Dim sFormatoNumerico As String
    Dim dCambioOfe As Double
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
    
    Set oAsig = g_ogrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                                            
    sFilaGrid = oProve.Cod & Chr(m_lSeparador) & oItem.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oItem.Cerrado & Chr(m_lSeparador)
    sFilaGrid = sFilaGrid & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador)
    sFilaGrid = sFilaGrid & "" & Chr(m_lSeparador)
    If Not oAsig Is Nothing Then
        sFilaGrid = sFilaGrid & oAsig.Num & Chr(m_lSeparador) & "1" & Chr(m_lSeparador)
        dCambioOfe = oItem.CambioComparativa(oProve.Cod, True, m_oAdjsProve)
    Else
        sFilaGrid = sFilaGrid & "" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador)
        dCambioOfe = 1
    End If
    sFilaGrid = sFilaGrid & oProve.Cod & " - " & oProve.Den
    
    'Datos de las adjudicaciones
    If Not m_oAdjsProve Is Nothing Then
        If Not m_oAdjsProve.Item(CStr(oItem.Id) & scodProve) Is Nothing Then
            strCantAdjProve = NullToStr(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).Adjudicado)
            If IsNull(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdj) Or IsEmpty(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdj) Or NullToDbl0(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdj) = 0 Then
                strImpAdjProve = ""
            Else
                strImpAdjProve = CStr(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdj)
            End If
            If NullToDbl0(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdj) > 0 Then
                strAhorroImpProve = IIf(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).importe = 0, "", m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).importe)
                strPresUniProve = NullToStr(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).PresUnitario)
                If NullToStr(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).Precio) = "" Then
                    strPrecUniProve = ""
                Else
                    strPrecUniProve = NullToStr(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).Precio) / dCambioOfe
                End If
                dblImporteProve = m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdjTot
                If dblImporteProve <> 0 Then
                    strAhorroPorcenProve = CStr((m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).importe / dblImporteProve) * 100)
                End If
            End If
        End If
    End If
    'Columna CANT
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strCantAdjProve
    'Columna PRECAPE: Presupuesto unitario medio
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strPresUniProve
    'Columna PROVEACT: Precio unitario medio
    If Not g_oVistaSeleccionada Is Nothing Then
        sFormatoNumerico = FormateoNumericoComp(g_oVistaSeleccionada.DecCant)
    Else
        sFormatoNumerico = ""
    End If
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & Format(strPrecUniProve, sFormatoNumerico)
    'Columna IMP
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strImpAdjProve
    'Columna AHORROIMP
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strAhorroImpProve
    'Columna AHORROPORCEN
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strAhorroPorcenProve

    'Rellena columna CantMax
    If Not oAsig Is Nothing Then
        If Not IsEmpty(oAsig.Lineas.Item(CStr(oItem.Id)).CantidadMaxima) Or IsNull(oAsig.Lineas.Item(CStr(oItem.Id)).CantidadMaxima) Then
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAsig.Lineas.Item(CStr(oItem.Id)).CantidadMaxima
        Else
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
        End If
        'Columna HOM
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAsig.Lineas.Item(CStr(oItem.Id)).Hom
    Else
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
    End If
    
    'Columnas de atributos de caracter�sticas
    For Each oatrib In g_ogrupo.AtributosItem
        'Son atributos de caracter�stica si no tienen operaci�n asociada
        If IsNull(oatrib.PrecioFormula) Or IsEmpty(oatrib.PrecioFormula) Then
            sValorAtrib = ""
            sCodAtrib = CStr(oItem.Id) & "$" & CStr(oatrib.idAtribProce)
            If Not oAsig Is Nothing Then
                If Not oAsig.AtribItemOfertados Is Nothing Then
                    If Not oAsig.AtribItemOfertados.Item(sCodAtrib) Is Nothing Then
                        Select Case oatrib.Tipo
                            Case TiposDeAtributos.TipoBoolean
                                If Not IsNull(oAsig.AtribItemOfertados.Item(sCodAtrib).valorBool) Then sValorAtrib = DevolverValorBoolean(oAsig.AtribItemOfertados.Item(sCodAtrib).valorBool)
                            Case TiposDeAtributos.TipoFecha
                                If Not IsNull(oAsig.AtribItemOfertados.Item(sCodAtrib).valorFec) Then sValorAtrib = oAsig.AtribItemOfertados.Item(sCodAtrib).valorFec
                            Case TiposDeAtributos.TipoNumerico
                                If Not IsNull(oAsig.AtribItemOfertados.Item(sCodAtrib).valorNum) Then
                                    If oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-" Then
                                        sValorAtrib = oAsig.AtribItemOfertados.Item(sCodAtrib).valorNum / dCambioOfe
                                    Else
                                        sValorAtrib = oAsig.AtribItemOfertados.Item(sCodAtrib).valorNum
                                    End If
                                End If
                            Case TiposDeAtributos.TipoString
                                sValorAtrib = EliminarTab(NullToStr(oAsig.AtribItemOfertados.Item(sCodAtrib).valorText))
                        End Select
                    End If
                End If
            End If
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sValorAtrib
        End If
    Next

    'Asignaciones y Adjudicaciones de escalados
    If Not g_ogrupo.Escalados Is Nothing Then
        If g_ogrupo.Escalados.Count > 0 Then
            For Each oEscalado In g_ogrupo.Escalados
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oEscalado.Id & Chr(m_lSeparador) & oEscalado.Inicial & Chr(m_lSeparador) & oEscalado.final

                sCod = CStr(oItem.Id) & scodProve & CStr(oEscalado.Id)

                'Rellena columna PRES
                If Not oAsig Is Nothing Then
                    If Not oAsig.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)) Is Nothing Then
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAsig.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)).PrecioOferta / dCambioOfe
                    Else
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                    End If
                Else
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                End If
                
                'Columna AHORRO
                sAhorro = vbNullString
                If Not oAsig Is Nothing Then
                    If Not oAsig.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)) Is Nothing Then
                        If Not IsNull(oAsig.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)).PrecioOferta) Then
                            vPresEsc = oItem.PresupuestoEscalado(oEscalado.Id)
                            If Not IsNull(vPresEsc) Then
                                sAhorro = CStr(vPresEsc - (oAsig.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)).PrecioOferta / dCambioOfe))
                            End If
                        End If
                    End If
                End If
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sAhorro
                
                'Columnas ADJCANT, ADJIMP
                strCantidad = vbNullString
                strPorcen = "100"
                strImporteAdj = vbNullString
                strAdj = "0"
                If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                    strAdj = "1"
                    If NullToDbl0(g_ogrupo.Adjudicaciones.Item(sCod).Adjudicado) > 0 Then
                        strCantidad = g_ogrupo.Adjudicaciones.Item(sCod).Adjudicado
                    End If
                    If Not IsNull(g_ogrupo.Adjudicaciones.Item(sCod).ImporteAdj) And Not IsEmpty(g_ogrupo.Adjudicaciones.Item(sCod).ImporteAdj) Then
                        strImporteAdj = g_ogrupo.Adjudicaciones.Item(sCod).ImporteAdj / dCambioOfe
                    End If
                End If

                'Rellena columna AdjCantidad, AdjImp y Adj
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strCantidad
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strImporteAdj
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strAdj

                'columnas de los atributos de costes/descuentos
                For Each oatrib In g_ogrupo.AtributosItem
                    If Not IsNull(oatrib.PrecioFormula) And Not IsEmpty(oatrib.PrecioFormula) Then
                        sValorAtrib = ""
                        sCodAtrib = CStr(oItem.Id) & "$" & CStr(oatrib.idAtribProce)
                        If Not oAsig Is Nothing Then
                            If Not oAsig.AtribItemOfertados Is Nothing Then
                                If Not oAsig.AtribItemOfertados.Item(sCodAtrib) Is Nothing Then
                                    If Not oAsig.AtribItemOfertados.Item(sCodAtrib).Escalados Is Nothing Then
                                        If Not oAsig.AtribItemOfertados.Item(sCodAtrib).Escalados.Item(CStr(oEscalado.Id)) Is Nothing Then
                                            Select Case oatrib.Tipo
                                                Case TiposDeAtributos.TipoNumerico
                                                    If Not IsNull(oAsig.AtribItemOfertados.Item(sCodAtrib).valorNum) Then
                                                        If oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-" Then
                                                            sValorAtrib = oAsig.AtribItemOfertados.Item(sCodAtrib).Escalados.Item(CStr(oEscalado.Id)).valorNum / dCambioOfe
                                                        Else
                                                            sValorAtrib = NullToStr(oAsig.AtribItemOfertados.Item(sCodAtrib).Escalados.Item(CStr(oEscalado.Id)).valorNum)
                                                        End If
                                                    End If
                                            End Select
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sValorAtrib
                    End If
                Next
            Next
            
            Set oEscalado = Nothing
        End If
    End If
    
    If Not IsMissing(vIndex) Then
        sdbgAdjEsc.AddItem sFilaGrid, vIndex
    Else
        sdbgAdjEsc.AddItem sFilaGrid
    End If
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "AddProveedor", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub ConfigurarBotones()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bHojaComparativa Then
        cmdHojaComp.Visible = False
        cmdListado.Left = cmdRestaurar.Left
        cmdRestaurar.Left = cmdAdjudicar.Left
        cmdAdjudicar.Left = cmdGuardar.Left
        cmdGuardar.Left = cmdHojaComp.Left
    End If
    
    If Not m_bPreadjudicar Then
        cmdGuardar.Visible = False
        cmdListado.Left = cmdRestaurar.Left
        cmdRestaurar.Left = cmdAdjudicar.Left
        cmdAdjudicar.Left = cmdGuardar.Left
    End If
    
    If Not m_bVal Then
        cmdAdjudicar.Visible = False
        cmdListado.Left = cmdRestaurar.Left
        cmdRestaurar.Left = cmdAdjudicar.Left
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ConfigurarBotones", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Calcula el importe consumido</summary>
''' <remarks>Llamada desde: CalcularOptima,CalcularOptimaItem,AdjudicarDesadjudicarEscalado; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 02/11/2011</revision>
Private Function DevolverImporteConsumido() As Double
    Dim oProve As CProveedor
    Dim dImporte As Double
    Dim dpres As Double
    Dim dCant As Double
    Dim sCod As String
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim oEscalado As CEscalado
    Dim dblCantidadProveedor As Double
    Dim iNumAdj As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dImporte = 0
    If g_ogrupo.UsarEscalados <> 1 Then dCant = g_oItemSeleccionado.Cantidad
        
    If IsNull(g_oItemSeleccionado.Precio) Then
        dpres = 0
    Else
        dpres = g_oItemSeleccionado.Precio
    End If
        
    If Not g_ogrupo.Adjudicaciones Is Nothing Then
        For Each oProve In m_oProvesAsig
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(g_oItemSeleccionado.grupoCod) Is Nothing Then
                    bCargar = False
                End If
            End If
            If bCargar Then
                If g_ogrupo.UsarEscalados Then
                    iNumAdj = 0
                    For Each oEscalado In g_ogrupo.Escalados
                        sCod = KeyEscalado(oProve.Cod, g_oItemSeleccionado.Id, oEscalado.Id)
                        If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                            
                            dblCantidadProveedor = CantidadProveedor(g_ogrupo, m_oProvesAsig, m_oAdjsProve, oProve.Cod, g_oItemSeleccionado.Id, m_oAdjsProve.Item(CStr(g_oItemSeleccionado.Id) & scodProve).Porcentaje)
                            dImporte = dImporte + ConsumidoProveedor(g_ogrupo, oProve.Cod, g_oItemSeleccionado.Id, dblCantidadProveedor)
                            iNumAdj = iNumAdj + 1
                        End If
                    Next
                    If iNumAdj > 0 Then dImporte = dImporte / iNumAdj
                    
                    Set oEscalado = Nothing
                Else
                    sCod = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    sCod = CStr(g_oItemSeleccionado.Id) & sCod
                
                    If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        dImporte = dImporte + ((NullToDbl0(g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje) * dCant / 100) * dpres)
                    End If
                End If
            End If
        Next
    End If
    
    DevolverImporteConsumido = dImporte
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "DevolverImporteConsumido", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function


Private Function DevolverIdAtributo(ByVal sCaption As String) As Long
'**************************************************************************
'Devuelve el id del atributo guardado en el name de la columna de la grid
'**************************************************************************

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DevolverIdAtributo = Mid(sCaption, 6)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "DevolverIdAtributo", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>Guarda la configuraci�n de la vista actual</summary>
''' <remarks>Llamada desde: cmdGuardarVista_Click,cmdGuardarVistaNueva_Click; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 27/10/2011</revision>
Private Sub GuardarVistaItem()
    Dim oatrib As CConfVistaItemAtrib
    Dim iScroll As Integer
    Dim iNumColsGR1 As Integer
    Dim iNumColsGrupo As Integer
    Dim iEscVisible As Integer
    Dim iNumColsGR As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    g_oVistaSeleccionada.OcultarNoAdj = chkOcultarNoAdj.Value
    g_oVistaSeleccionada.OcultarProvSinOfe = chkOcultarProvSinOfe.Value
    
    If g_ogrupo.UsarEscalados Then
        With sdbgAdjEsc
            iNumColsGR = .Groups(0).Columns.Count + .Groups(1).Columns.Count - 1
            iNumColsGR1 = .Groups(0).Columns.Count - 1
            
            'Si se ha hecho un scroll se scrolla otra vez para dejarlo en la posici�n 0
            If .ColPosition(0) > 0 Then
                iScroll = .Columns(0).Position
                .Scroll -iScroll, 0
                .Update
            End If
            
            g_oVistaSeleccionada.Grupo0Width = .Groups(0).Width  'Anchura grupo ppal
            g_oVistaSeleccionada.GruposEscWidth = .Groups(2).Width
            
            'almacena los widths de las columnas
            g_oVistaSeleccionada.ProveDescrWidth = .Columns("DESCR").Width
            g_oVistaSeleccionada.CantAdjWidth = .Columns("CANT").Width
            g_oVistaSeleccionada.ImporteOfeWidth = .Columns("IMP").Width
            g_oVistaSeleccionada.AhorroOfeWidth = .Columns("AHORROIMP").Width
            g_oVistaSeleccionada.AhorroPorcenWidth = .Columns("AHORROPORCEN").Width
            g_oVistaSeleccionada.PrecioWidth = .Columns("CODPROVEACTUAL").Width
            g_oVistaSeleccionada.ConsumWidth = .Columns("PRECAPE").Width
            
            'almacena la posici�n de las columnas
            g_oVistaSeleccionada.CantAdjPos = .Columns("CANT").Position
            g_oVistaSeleccionada.ImporteOfePos = .Columns("IMP").Position
            g_oVistaSeleccionada.AhorroOfePos = .Columns("AHORROIMP").Position
            g_oVistaSeleccionada.AhorroPorcenPos = .Columns("AHORROPORCEN").Position
            g_oVistaSeleccionada.PrecioPos = .Columns("CODPROVEACTUAL").Position
            g_oVistaSeleccionada.ConsumPos = .Columns("PRECAPE").Position
            
            'almacena la visibilidad de las columnas
            g_oVistaSeleccionada.CantAdjVisible = .Columns("CANT").Visible
            g_oVistaSeleccionada.ImporteOfeVisible = .Columns("IMP").Visible
            g_oVistaSeleccionada.AhorroOfeVisible = .Columns("AHORROIMP").Visible
            g_oVistaSeleccionada.AhorroPorcenVisible = .Columns("AHORROPORCEN").Visible
            g_oVistaSeleccionada.PrecioVisible = .Columns("CODPROVEACTUAL").Visible
            g_oVistaSeleccionada.ConsumVisible = .Columns("PRECAPE").Visible
            
            'almacena los width y posici�n de los atributos de caracter�stica
            For Each oatrib In g_oVistaSeleccionada.ConfVistasItemAtrib
                If Not g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)) Is Nothing Then
                    If IsNull(g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Or IsEmpty(g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Then
                        oatrib.Posicion = .Columns(CStr(oatrib.Atributo)).Position - iNumColsGR1
                        oatrib.Width = .Columns(CStr(oatrib.Atributo)).Width
                        oatrib.Visible = .Columns(CStr(oatrib.Atributo)).Visible
                    End If
                End If
            Next
            
            'Obtiene cual es el 1� escalado que est� visible
            For iEscVisible = 2 To .Groups.Count - 1
                If .Groups(iEscVisible).Visible Then
                    Exit For
                End If
            Next
            If iEscVisible = .Groups.Count Then Exit Sub
            
            'almacena los width y posici�n de las columnas del grupo de escalados
            If .Groups.Count > 2 Then
                iNumColsGrupo = .Groups(iEscVisible).Columns.Count
                
                'Para las columnas de los escalados se usar�: PREC_PROV para PRES y ADJ_PROV para AHORRO
                'almacena los widths de las columnas
                g_oVistaSeleccionada.PrecioEscWidth = .Columns("PRES" & iEscVisible).Width
                g_oVistaSeleccionada.AhorroEscWidth = .Columns("AHORRO" & iEscVisible).Width
                g_oVistaSeleccionada.AdjEscWidth = .Columns("ADJCANT" & iEscVisible).Width
                'almacena los position de las columnas
                g_oVistaSeleccionada.PrecioEscPos = .Columns("PRES" & iEscVisible).Position - iNumColsGR - (iNumColsGrupo * (iEscVisible - 2))
                g_oVistaSeleccionada.AhorroEscPos = .Columns("AHORRO" & iEscVisible).Position - iNumColsGR - (iNumColsGrupo * (iEscVisible - 2))
                g_oVistaSeleccionada.AdjEscPos = .Columns("ADJCANT" & iEscVisible).Position - iNumColsGR - (iNumColsGrupo * (iEscVisible - 2))
                'almacena la visibilidad de las columnas
                g_oVistaSeleccionada.PrecioEscVisible = .Columns("PRES" & iEscVisible).Visible
                g_oVistaSeleccionada.AhorroEscVisible = .Columns("AHORRO" & iEscVisible).Visible
                g_oVistaSeleccionada.AdjEscVisible = .Columns("ADJCANT" & iEscVisible).Visible
                
                'almacena los width de los atributos
                If g_oVistaSeleccionada.Vista <> TipoDeVistaDefecto.vistainicial Then
                    For Each oatrib In g_oVistaSeleccionada.ConfVistasItemAtrib
                        If Not g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)) Is Nothing Then
                            'Son atributos de coste/descuento si tienen operaci�n asociada
                            If Not IsNull(g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) And Not IsEmpty(g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Then
                                oatrib.Posicion = .Columns(oatrib.Atributo & iEscVisible).Position - iNumColsGR - (iNumColsGrupo * (iEscVisible - 2))
                                oatrib.Width = .Columns(oatrib.Atributo & iEscVisible).Width
                            End If
                        End If
                    Next
                End If
            End If
            
            'Almacena el ancho de la fila
            g_oVistaSeleccionada.AnchoFila = .RowHeight
        End With
    Else
        With sdbgAdj
            'Si se ha hecho un scroll se scrolla otra vez para dejarlo en la posici�n 0
            If .ColPosition(0) > 0 Then
                iScroll = .Columns(0).Position
                .Scroll -iScroll, 0
                .Update
            End If
            
            'almacena los widths de las columnas
            g_oVistaSeleccionada.ProveDescrWidth = .Columns("PROVE").Width
            g_oVistaSeleccionada.PrecioWidth = .Columns("PRECIO").Width
            g_oVistaSeleccionada.CantAdjWidth = .Columns("CANTADJ").Width
            g_oVistaSeleccionada.CantMaxWidth = .Columns("CANTMAX").Width
            g_oVistaSeleccionada.AhorroPorcenWidth = .Columns("AHO_PORCEN").Width
            g_oVistaSeleccionada.AhorroWidth = .Columns("AHO").Width
            g_oVistaSeleccionada.ConsumWidth = .Columns("CONSUM").Width
            g_oVistaSeleccionada.AdjudWidth = .Columns("ADJ").Width
            g_oVistaSeleccionada.ImporteOfeWidth = .Columns("IMP_OFE").Width
            g_oVistaSeleccionada.AhorroOfeWidth = .Columns("AHORRO_OFE").Width
            g_oVistaSeleccionada.AhorroOfePorcenWidth = .Columns("AHORRO_OFE_PORCEN").Width
            
            If g_oItemSeleccionado.proceso.UsarPonderacion Then
                g_oVistaSeleccionada.PondWidth = .Columns("PUNTOS").Width
            End If
            
            'almacena los position de las columnas
            g_oVistaSeleccionada.PrecioPos = .Columns("PRECIO").Position
            g_oVistaSeleccionada.CantAdjPos = .Columns("CANTADJ").Position
            g_oVistaSeleccionada.CantMaxPos = .Columns("CANTMAX").Position
            g_oVistaSeleccionada.AhorroPorcenPos = .Columns("AHO_PORCEN").Position
            g_oVistaSeleccionada.AhorroPos = .Columns("AHO").Position
            g_oVistaSeleccionada.ConsumPos = .Columns("CONSUM").Position
            g_oVistaSeleccionada.AdjudPos = .Columns("ADJ").Position
            g_oVistaSeleccionada.ImporteOfePos = .Columns("IMP_OFE").Position
            g_oVistaSeleccionada.AhorroOfePos = .Columns("AHORRO_OFE").Position
            g_oVistaSeleccionada.AhorroOfePorcenPos = .Columns("AHORRO_OFE_PORCEN").Position
            
            'almacena la visibilidad de las columnas
            g_oVistaSeleccionada.PrecioVisible = .Columns("PRECIO").Visible
            g_oVistaSeleccionada.CantAdjVisible = .Columns("CANTADJ").Visible
            g_oVistaSeleccionada.CantMaxVisible = .Columns("CANTMAX").Visible
            g_oVistaSeleccionada.AhorroPorcenVisible = .Columns("AHO_PORCEN").Visible
            g_oVistaSeleccionada.AhorroVisible = .Columns("AHO").Visible
            g_oVistaSeleccionada.ConsumVisible = .Columns("CONSUM").Visible
            g_oVistaSeleccionada.AdjudVisible = .Columns("ADJ").Visible
            g_oVistaSeleccionada.ImporteOfeVisible = .Columns("IMP_OFE").Visible
            g_oVistaSeleccionada.AhorroOfeVisible = .Columns("AHORRO_OFE").Visible
            g_oVistaSeleccionada.AhorroOfePorcenVisible = .Columns("AHORRO_OFE_PORCEN").Visible
            
            If g_oItemSeleccionado.proceso.UsarPonderacion Then
                g_oVistaSeleccionada.PondPos = .Columns("PUNTOS").Position
            End If
            
            'almacena los width y posici�n de los atributos
            For Each oatrib In g_oVistaSeleccionada.ConfVistasItemAtrib
                oatrib.Posicion = .Columns("ATRIB" & oatrib.Atributo).Position
                oatrib.Width = .Columns("ATRIB" & oatrib.Atributo).Width
            Next
            
            'Almacena el ancho de la fila
            g_oVistaSeleccionada.AnchoFila = .RowHeight
        End With
    End If
    
    Screen.MousePointer = vbNormal
    Set oatrib = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "GuardarVistaItem", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Public Sub itemSeleccionado(Optional ByVal bRedimensionar As Boolean)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bRedimensionar Then
        
        PrepararGridOriginal
        ConfiguracionVistaActual g_oOrigen.m_oVistaSeleccionada.Vista, g_oOrigen.m_oVistaSeleccionada.TipoVista, g_oOrigen.m_oVistaSeleccionada.UsuarioVista 'Se lee la vista actual de la BD y si no hay se pone la por defecto
        ConfigurarBotonesVistas g_oOrigen.m_oVistaSeleccionada.TipoVista
        
        AnyadirColumnasParaAtributos 'Crear columnas para atributos del �tem
    
        'Carga la grid de adjs
        CargarGridAdjudicaciones
        
        'Carga los campos y proveedores a ocultar en las grids con la configuraci�n de la vista actual
        CargarVista
    
        'Redimensiona la grid
        RedimensionarGrid
        'Se quitan los proves que no se han de ver
        RedimensionarProves
        
        'Carga la grid de los precios a aplicar y de optima
        CargarAtribAplicarFormulas
        CargarAtribOptima
    Else
        CargarGridAdjudicaciones
    End If
    
    'Ponemos en el combo de vistas los valores que corresponden
    MostrarVistaEnCombos True
    
    'Datos del �tem
    lblPresUni.caption = Format(g_oItemSeleccionado.Precio, m_sFormatoNumberPrecio)
    lblCantidad.caption = Format(g_oItemSeleccionado.Cantidad, m_sFormatoNumberCant)
    lblUDLit.caption = g_oItemSeleccionado.UniCod
    lblPresTot.caption = Format(g_oItemSeleccionado.Presupuesto, m_sFormatoNumberPrecio)
    lblObjetivo.caption = Format(g_oItemSeleccionado.Objetivo, m_sFormatoNumber)
    
    If m_oProcesoSeleccionado.AdminPublica = True Then
        lblProveLit.Visible = False
        lblProve.Visible = False
    Else
        lblProveLit.Visible = True
        lblProve.Visible = True
        lblProve.caption = NullToStr(g_oItemSeleccionado.ProveAct)
        lblProve.ToolTipText = IIf(IsNull(g_oItemSeleccionado.ProveAct) And IsNull(g_oItemSeleccionado.ProveActDen), "", g_oItemSeleccionado.ProveAct & "-" & g_oItemSeleccionado.ProveActDen)
    End If
    
    If g_oOrigen.m_oProcesoSeleccionado.Invitado Then
        m_bPreadjudicar = False
        m_bVal = False
        m_bOfe = False
        m_bHojaComparativa = True
        m_bPermSaltarseVolMaxAdjDir = False
        m_bModifOfeDeProv = False
        m_bModifOfeDeUsu = False
        ConfigurarBotones
    Else
        ConfigurarSeguridad
    End If
    
    If m_oProcesoSeleccionado.Bloqueado Then
        If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
            If g_oItemSeleccionado.Cerrado = True Then
                cmdAdjudicar.Enabled = False
                cmdGuardar.Enabled = False
                m_oProcesoSeleccionado.GuardarProceso = False
                m_bRecargarOrigen = False
            Else
                cmdAdjudicar.Enabled = g_oOrigen.cmdAdjudicar.Enabled
            End If
        ElseIf m_oProcesoSeleccionado.Estado >= conadjudicaciones Then
            cmdAdjudicar.Enabled = False
            cmdGuardar.Enabled = False
            m_oProcesoSeleccionado.GuardarProceso = False
            m_bRecargarOrigen = False
        End If
    Else
        cmdAdjudicar.Enabled = False
        cmdGuardar.Enabled = False
        m_oProcesoSeleccionado.GuardarProceso = False
        m_bRecargarOrigen = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "itemSeleccionado", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Carga los datos del item en el grid</summary>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0</remarks>
Private Sub ItemEscaladoSeleccionado(Optional ByVal bRedimensionar As Boolean)
    Dim iNumAtribCaract As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bRedimensionar Then
        LockWindowUpdate Me.hWnd
        
        PrepararGridOriginalEsc

        ConfiguracionVistaActual g_oOrigen.m_oVistaSeleccionada.Vista, g_oOrigen.m_oVistaSeleccionada.TipoVista, g_oOrigen.m_oVistaSeleccionada.UsuarioVista 'Se lee la vista actual de la BD y si no hay se pone la por defecto
        
        ConfigurarBotonesVistas g_oOrigen.m_oVistaSeleccionada.TipoVista
    
        'Carga de los grupos de atributos y escalados en el grid
        iNumAtribCaract = CrearGruposDinamicos
        
        MostrarSplit
        
        'C�lculo de las adjudicaciones
        Set m_oAdjsProve = CalcularAdjudicacionesInterno(m_oProcesoSeleccionado, g_ogrupo, m_oProvesAsig, m_oAtribsFormulas)
        
        'Carga la grid de adjs
        CargarGridEscAdjudicaciones iNumAtribCaract
        
        'Carga los campos y proveedores a ocultar en las grids con la configuraci�n de la vista actual
        CargarVista

        'Redimensiona la grid
        RedimensionarGridEsc
        'Se quitan los proves que no se han de ver
        RedimensionarProves

        'Carga la grid de los precios a aplicar y de optima
        CargarAtribAplicarFormulas
        CargarAtribOptima
        
        LockWindowUpdate 0&
    Else
        CargarGridEscAdjudicaciones sdbgAdjEsc.Groups.Item("ATRIB_CARAC").Columns.Count
    End If
    
    'Ponemos en el combo de vistas los valores que corresponden
    MostrarVistaEnCombos True
    
    'Datos del �tem
    lblPresUni.caption = Format(g_oItemSeleccionado.Precio, m_sFormatoNumberPrecio)
    lblCantidad.caption = Format(g_oItemSeleccionado.Cantidad, m_sFormatoNumberCant)
    lblUDLit.caption = g_oItemSeleccionado.UniCod
    lblPresTot.caption = Format(g_oItemSeleccionado.Presupuesto, m_sFormatoNumberPrecio)
    lblObjetivo.caption = Format(g_oItemSeleccionado.Objetivo, m_sFormatoNumber)
    
    If m_oProcesoSeleccionado.AdminPublica = True Then
        lblProveLit.Visible = False
        lblProve.Visible = False
    Else
        lblProveLit.Visible = True
        lblProve.Visible = True
        lblProve.caption = NullToStr(g_oItemSeleccionado.ProveAct)
        lblProve.ToolTipText = IIf(IsNull(g_oItemSeleccionado.ProveAct) And IsNull(g_oItemSeleccionado.ProveActDen), "", g_oItemSeleccionado.ProveAct & "-" & g_oItemSeleccionado.ProveActDen)
    End If
    
    If g_oOrigen.m_oProcesoSeleccionado.Invitado Then
        m_bPreadjudicar = False
        m_bVal = False
        m_bOfe = False
        m_bHojaComparativa = True
        m_bPermSaltarseVolMaxAdjDir = False
        m_bModifOfeDeProv = False
        m_bModifOfeDeUsu = False
        ConfigurarBotones
    Else
        ConfigurarSeguridad
    End If
    
    If m_oProcesoSeleccionado.Bloqueado Then
        If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
            If g_oItemSeleccionado.Cerrado = True Then
                cmdAdjudicar.Enabled = False
                cmdGuardar.Enabled = False
                m_oProcesoSeleccionado.GuardarProceso = False
                m_bRecargarOrigen = False
            Else
                cmdAdjudicar.Enabled = g_oOrigen.cmdAdjudicar.Enabled
            End If
        ElseIf m_oProcesoSeleccionado.Estado >= conadjudicaciones Then
            cmdAdjudicar.Enabled = False
            cmdGuardar.Enabled = False
            m_oProcesoSeleccionado.GuardarProceso = False
            m_bRecargarOrigen = False
        End If
    Else
        cmdAdjudicar.Enabled = False
        cmdGuardar.Enabled = False
        m_oProcesoSeleccionado.GuardarProceso = False
        m_bRecargarOrigen = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ItemEscaladoSeleccionado", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Crea en el grid los grupos que no son fijos: atributos y escalados</summary>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo; Tiempo m�ximo:0</remarks>
''' <returns>Entero indicando el n� de columnas de atributos</returns>
Private Function CrearGruposDinamicos() As Integer
    Dim iNumAtribCarac As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    '����Si no, no se muestran datos en el grid?????
    sdbgAdjEsc.AddItem ""
    sdbgAdjEsc.col = 1
    sdbgAdjEsc.RemoveItem 0
    DoEvents
    
    'Creaci�n de grupos para los atributos de caracter�stica
    iNumAtribCarac = CrearGrupoAtributosCaracteristicas
    
    'Creaci�n de grupos para los escalados
    CrearGruposEscalados
    
    CrearGruposDinamicos = iNumAtribCarac
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CrearGruposDinamicos", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>A�ade los grupos de atributos de caracter�sticas al grid</summary>
''' <remarks>Llamada desde: CrearGruposDinamicos; Tiempo m�ximo:0</remarks>
''' <returns>Entero indicando el n� de columnas de atributos</returns>
Private Function CrearGrupoAtributosCaracteristicas() As Integer
    Dim ogroup As SSDataWidgets_B.Group
    Dim oColumn As SSDataWidgets_B.Column
    Dim oatrib As CAtributo
    Dim i As Integer
    Dim bCrearGrupo As Boolean
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bCrearGrupo = False
    With sdbgAdjEsc
        'Columnas (una por cada atributo)
        If Not g_ogrupo.AtributosItem Is Nothing Then
            'Primer bucle para crear el grupo y calcular su anchura (si se crea sin dar la anchura total,
            'al darle una anchura al final redimensiona s�lo la �ltima columna)
            i = 0
            For Each oatrib In g_ogrupo.AtributosItem
                'Son atributos de caracter�stica si no tienen operaci�n asociada
                If IsNull(oatrib.PrecioFormula) Or IsEmpty(oatrib.PrecioFormula) Then
                    i = i + 1
                    bCrearGrupo = True
                End If
            Next
            
            '.Groups.Add 1
            Set ogroup = .Groups(1)
            ogroup.TagVariant = "ATRIB_CARAC"
            ogroup.caption = m_sOtrasCarac
            ogroup.CaptionAlignment = ssCaptionAlignmentCenter
            ogroup.AllowSizing = True
            ogroup.HeadStyleSet = "HeadBold"
            If bCrearGrupo Then
                'oGroup.Visible = (VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial)
                ogroup.Visible = True
                
                i = 0
                For Each oatrib In g_ogrupo.AtributosItem
                    'Son atributos de caracter�stica si no tienen operaci�n asociada
                    If IsNull(oatrib.PrecioFormula) Or IsEmpty(oatrib.PrecioFormula) Then
                        ogroup.Columns.Add i
                        Set oColumn = ogroup.Columns(i)
                        oColumn.Name = oatrib.idAtribProce
                        oColumn.caption = vbCrLf & vbCrLf & oatrib.Cod
                        oColumn.Alignment = ssCaptionAlignmentRight
                        oColumn.CaptionAlignment = ssColCapAlignCenter
                        oColumn.HeadStyleSet = "DobleFondo"
                        DoEvents
                                                
                        i = i + 1
                    End If
                Next
                
                'Dar tama�o al grupo y las columnas
                If ogroup.Columns.Count > 1 Then
                    ogroup.Width = ogroup.Columns.Count * 800
                Else
                    ogroup.Width = 1600
                End If
                For Each oColumn In ogroup.Columns
                    oColumn.Width = 800
                Next
            Else
                ogroup.Visible = False
                ogroup.Columns.RemoveAll
            End If
            
            Set oatrib = Nothing
            Set ogroup = Nothing
        End If
    End With
    
    CrearGrupoAtributosCaracteristicas = i
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CrearGrupoAtributosCaracteristicas", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>A�ade los grupos de escalados al grid</summary>
''' <remarks>Llamada desde: CrearGruposDinamicos; Tiempo m�ximo:0</remarks>
Private Sub CrearGruposEscalados()
    Dim oEscalado As CEscalado
    Dim ogroup As SSDataWidgets_B.Group
    Dim oColumn As SSDataWidgets_B.Column
    Dim oatrib As CAtributo
    Dim sCaption As String
    Dim iGroupIndex
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_ogrupo.Escalados Is Nothing Then
        If g_ogrupo.Escalados.Count > 0 Then
            iGroupIndex = 2
            For Each oEscalado In g_ogrupo.Escalados
                With sdbgAdjEsc
                    .Groups.Add iGroupIndex
                    Set ogroup = .Groups(iGroupIndex)
        
                    sCaption = m_sEscalado & " (" & g_ogrupo.UnidadEscalado & ")" & vbCrLf & oEscalado.Inicial
                    If Not IsEmpty(oEscalado.final) And Not IsNull(oEscalado.final) Then
                        sCaption = sCaption & " - " & oEscalado.final
                    End If
        
                    ogroup.TagVariant = oEscalado.Id
                    ogroup.caption = sCaption
                    ogroup.AllowSizing = True
                    ogroup.CaptionAlignment = ssCaptionAlignmentCenter
                    ogroup.HeadStyleSet = "HeadBold"
        
                    'Columnas
                    ogroup.Columns.Add 0
                    Set oColumn = ogroup.Columns(0)
                    oColumn.caption = "ESC"
                    oColumn.Name = "ESC" & iGroupIndex
                    oColumn.Visible = False
        
                    ogroup.Columns.Add 1
                    Set oColumn = ogroup.Columns(1)
                    oColumn.caption = "INI"
                    oColumn.Name = "INI" & iGroupIndex
                    oColumn.Visible = False
        
                    ogroup.Columns.Add 2
                    Set oColumn = ogroup.Columns(2)
                    oColumn.caption = "FIN"
                    oColumn.Name = "FIN" & iGroupIndex
                    oColumn.Visible = False
        
                    ogroup.Columns.Add 3
                    Set oColumn = ogroup.Columns(3)
                    oColumn.caption = m_sPresUni & vbCrLf & vbCrLf & vbCrLf & m_sPrecioImporte
                    oColumn.Name = "PRES" & iGroupIndex
                    oColumn.HeadStyleSet = "DobleFondo"
                    oColumn.Alignment = ssCaptionAlignmentCenter
                    oColumn.Style = ssStyleEdit
                    
                    ogroup.Columns.Add 4
                    Set oColumn = ogroup.Columns(4)
                    oColumn.caption = m_sObjetivo & vbCrLf & vbCrLf & vbCrLf & m_sAhorUni
                    oColumn.Name = "AHORRO" & iGroupIndex
                    oColumn.HeadStyleSet = "DobleFondo"
                    oColumn.Alignment = ssCaptionAlignmentCenter
                    
                    ogroup.Columns.Add 5
                    Set oColumn = ogroup.Columns(5)
                    oColumn.caption = m_sCantAdjudicada & vbCrLf & vbCrLf & vbCrLf & m_sCantAdjudicada
                    oColumn.Name = "ADJCANT" & iGroupIndex
                    oColumn.HeadStyleSet = "DobleFondo"
                    oColumn.Alignment = ssCaptionAlignmentCenter
                    oColumn.HeadForeColor = ColorBlue
                    
                    ogroup.Columns.Add 6
                    Set oColumn = ogroup.Columns(6)
                    oColumn.caption = "ADJIMP"
                    oColumn.Name = "ADJIMP" & iGroupIndex
                    oColumn.Visible = False
                    
                    ogroup.Columns.Add 7
                    Set oColumn = ogroup.Columns(7)
                    oColumn.caption = "ADJ"
                    oColumn.Name = "ADJ" & iGroupIndex   'oEscalado.Id
                    oColumn.Visible = False
                                        
                    'A�adir columnas de atributos de costes/descuentos
                    i = 1
                    For Each oatrib In g_ogrupo.AtributosItem
                        'Son atributos de costes/descuentos si tienen operaci�n asociada
                        If Not IsNull(oatrib.PrecioFormula) And Not IsEmpty(oatrib.PrecioFormula) Then
                            ogroup.Columns.Add 7 + i
                            Set oColumn = ogroup.Columns(7 + i)
                            oColumn.caption = vbCrLf & vbCrLf & oatrib.Cod
                            oColumn.Name = CStr(oatrib.idAtribProce) & CStr(iGroupIndex)
                            oColumn.HeadStyleSet = "DobleFondo"
                            i = i + 1
                        End If
                    Next
                    
                    'Dar tama�o al grupo y las columnas
                    ogroup.Width = 2400 + ((i - 1) * 800)
                    i = 1
                    For Each oColumn In ogroup.Columns
                        If oColumn.Visible Then
                            'Las 2 primeras columnas m�s grandes
                            If i <= 2 Then
                                oColumn.Width = 1200
                            Else
                                oColumn.Width = 800
                            End If
                            i = i + 1
                        End If
                    Next
        
                    Set oColumn = Nothing
                End With
        
                iGroupIndex = iGroupIndex + 1
            Next
            
            Set oEscalado = Nothing
            Set ogroup = Nothing
            Set oColumn = Nothing
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CrearGruposEscalados", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Comprueba si hay que aplicar atributos de grupo</summary>
''' <param name="sCodGrupo">Codigo del grupo</param>
''' <remarks>Llamada desde: CalcularAdjudicacionesEsc; Tiempo m�ximo:0,1</remarks>

Private Function ComprobarAplicarAtribsItem(ByVal sCodGrupo As String) As Boolean
    Dim bHayAtribs  As Boolean
    Dim oatrib As CAtributo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oAtribsFormulas Is Nothing Then
        ComprobarAplicarAtribsItem = False
        Exit Function
    End If
    
    If m_oAtribsFormulas.Count = 0 Then
        ComprobarAplicarAtribsItem = False
        Exit Function
    End If
    
    bHayAtribs = False
    For Each oatrib In m_oAtribsFormulas
        If oatrib.PrecioAplicarA = TipoAplicarAPrecio.TotalItem Then
            If oatrib.codgrupo = sCodGrupo Or IsNull(oatrib.codgrupo) Then
                ComprobarAplicarAtribsItem = True
                bHayAtribs = True
                Exit For
            End If
        End If
    Next
    If bHayAtribs = False Then
        ComprobarAplicarAtribsItem = False
        Exit Function
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ComprobarAplicarAtribsItem", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Private Sub MostrarDetalleItem()
Dim oAdj As CAdjDeArt
Dim oDestinos As CDestinos
Dim oPagos As CPagos
Dim oUnidades As CUnidades
Dim oArticulo As CArticulo
Dim oAdjudicaciones As CAdjsDeArt

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass

    Set frmItemDetalle.g_oItemSeleccionado = g_oItemSeleccionado
    
    If g_oItemSeleccionado.ArticuloCod <> "" Then
        frmItemDetalle.lblItem.caption = g_oItemSeleccionado.ArticuloCod & " - " & g_oItemSeleccionado.Descr
    Else
        frmItemDetalle.lblItem.caption = NullToStr(g_oItemSeleccionado.ArticuloCod)
    End If

    frmItemDetalle.lblCodDest.caption = g_oItemSeleccionado.DestCod
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    oDestinos.CargarTodosLosDestinos frmItemDetalle.lblCodDest.caption, , True, , , , , , , , True
    If oDestinos.Count <> 0 Then
        frmItemDetalle.lblDenDest.caption = oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If

    frmItemDetalle.lblCodPag.caption = g_oItemSeleccionado.PagCod
    Set oPagos = oFSGSRaiz.generar_CPagos
    oPagos.CargarTodosLosPagos frmItemDetalle.lblCodPag.caption, , True
    If oPagos.Count <> 0 Then
        frmItemDetalle.lblDenPag.caption = oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If

    frmItemDetalle.lblCodUni.caption = g_oItemSeleccionado.UniCod
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    oUnidades.CargarTodasLasUnidades frmItemDetalle.lblCodUni.caption, , True
    If oUnidades.Count <> 0 Then
        frmItemDetalle.lblDenUni.caption = oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If

    frmItemDetalle.lblFecIni.caption = g_oItemSeleccionado.FechaInicioSuministro
    frmItemDetalle.lblFecFin.caption = g_oItemSeleccionado.FechaFinSuministro
    
    g_oItemSeleccionado.CargarEspGeneral
    frmItemDetalle.txtEsp.Text = NullToStr(g_oItemSeleccionado.esp)

    'Realizar la carga de las adjudicaciones anteriores
    'Comprobamos haber si existen adjudicaciones de summit

    If g_oItemSeleccionado.ArticuloCod <> "" Then
        Set oArticulo = oFSGSRaiz.Generar_CArticulo
        oArticulo.Cod = g_oItemSeleccionado.ArticuloCod

        Set oAdjudicaciones = oFSGSRaiz.Generar_CAdjsDeArt
        ' Las actuales
        oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorFecFin, oArticulo, False, True

        For Each oAdj In oAdjudicaciones
           frmItemDetalle.sdbgAdjudicaciones.AddItem oAdj.Descr & Chr(m_lSeparador) & oAdj.ProveCod & Chr(m_lSeparador) & oAdj.Precio * g_oItemSeleccionado.CambioComparativa(oAdj.ProveCod) * CDbl(g_oItemSeleccionado.proceso.Cambio) & Chr(m_lSeparador) & oAdj.Cantidad & Chr(m_lSeparador) & oAdj.Porcentaje & Chr(m_lSeparador) & oAdj.FechaInicioSuministro & Chr(m_lSeparador) & oAdj.FechaFinSuministro & Chr(m_lSeparador) & oAdj.DestCod & Chr(m_lSeparador) & oAdj.UniCod
        Next
    End If

    Screen.MousePointer = vbNormal
    frmItemDetalle.Show 1
    Unload frmItemDetalle

    Set oUnidades = Nothing
    Set oDestinos = Nothing
    Set oPagos = Nothing
    Set oAdjudicaciones = Nothing
    Set oArticulo = Nothing

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "MostrarDetalleItem", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub RedimensionarGrid()
    Dim oatrib As CConfVistaItemAtrib
    Dim iPos As Integer
    
    '''''''''''''''''''''Redimensiona sdbgAdj''''''''''''''''''''

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgAdj
    .Columns("PROVE").Position = 0
    .Columns("PRECIO").Position = g_oVistaSeleccionada.PrecioPos
    .Columns("CANTADJ").Position = g_oVistaSeleccionada.CantAdjPos
    .Columns("CANTMAX").Position = g_oVistaSeleccionada.CantMaxPos
    .Columns("AHO_PORCEN").Position = g_oVistaSeleccionada.AhorroPorcenPos
    .Columns("AHO").Position = g_oVistaSeleccionada.AhorroPos
    .Columns("CONSUM").Position = g_oVistaSeleccionada.ConsumPos
    .Columns("ADJ").Position = g_oVistaSeleccionada.AdjudPos
    
    If IsNull(g_oVistaSeleccionada.ImporteOfePos) Or g_oVistaSeleccionada.ImporteOfePos = 0 Then
        .Columns("IMP_OFE").Position = 10
    Else
        .Columns("IMP_OFE").Position = g_oVistaSeleccionada.ImporteOfePos
    End If
    If IsNull(g_oVistaSeleccionada.AhorroOfePos) Or g_oVistaSeleccionada.AhorroOfePos = 0 Then
        .Columns("AHORRO_OFE").Position = 11
    Else
        .Columns("AHORRO_OFE").Position = g_oVistaSeleccionada.AhorroOfePos
    End If
    If IsNull(g_oVistaSeleccionada.AhorroOfePorcenPos) Or g_oVistaSeleccionada.AhorroOfePorcenPos = 0 Then
        .Columns("AHORRO_OFE_PORCEN").Position = 12
    Else
        .Columns("AHORRO_OFE_PORCEN").Position = g_oVistaSeleccionada.AhorroOfePorcenPos
    End If
    
    If g_oItemSeleccionado.proceso.UsarPonderacion Then
        .Columns("PUNTOS").Position = g_oVistaSeleccionada.PondPos
    End If
    
    .Columns("PROVE").Visible = True
    .Columns("PRECIO").Visible = g_oVistaSeleccionada.PrecioVisible
    .Columns("CANTADJ").Visible = g_oVistaSeleccionada.CantAdjVisible
    If m_oProcesoSeleccionado.SolicitarCantMax = True Then
        .Columns("CANTMAX").Visible = g_oVistaSeleccionada.CantMaxVisible
    Else
        .Columns("CANTMAX").Visible = False
    End If
    .Columns("AHO_PORCEN").Visible = g_oVistaSeleccionada.AhorroPorcenVisible
    .Columns("AHO").Visible = g_oVistaSeleccionada.AhorroVisible
    .Columns("CONSUM").Visible = g_oVistaSeleccionada.ConsumVisible
    .Columns("ADJ").Visible = g_oVistaSeleccionada.AdjudVisible
    .Columns("IMP_OFE").Visible = g_oVistaSeleccionada.ImporteOfeVisible
    .Columns("AHORRO_OFE").Visible = g_oVistaSeleccionada.AhorroOfeVisible
    .Columns("AHORRO_OFE_PORCEN").Visible = g_oVistaSeleccionada.AhorroOfePorcenVisible
    
    If g_oItemSeleccionado.proceso.UsarPonderacion Then
        .Columns("PUNTOS").Visible = g_oVistaSeleccionada.PondVisible
    End If
    
    .Columns("PROVE").Width = g_oVistaSeleccionada.ProveDescrWidth
    .Columns("PRECIO").Width = g_oVistaSeleccionada.PrecioWidth
    .Columns("CANTADJ").Width = g_oVistaSeleccionada.CantAdjWidth
    .Columns("CANTMAX").Width = g_oVistaSeleccionada.CantMaxWidth
    .Columns("AHO_PORCEN").Width = g_oVistaSeleccionada.AhorroPorcenWidth
    .Columns("AHO").Width = g_oVistaSeleccionada.AhorroWidth
    .Columns("CONSUM").Width = g_oVistaSeleccionada.ConsumWidth
    .Columns("ADJ").Width = g_oVistaSeleccionada.AdjudWidth
    .Columns("IMP_OFE").Width = IIf(g_oVistaSeleccionada.ImporteOfeWidth = 0, 1200, g_oVistaSeleccionada.ImporteOfeWidth)
    .Columns("AHORRO_OFE").Width = IIf(g_oVistaSeleccionada.AhorroOfeWidth = 0, 1200, g_oVistaSeleccionada.AhorroOfeWidth)
    .Columns("AHORRO_OFE_PORCEN").Width = IIf(g_oVistaSeleccionada.AhorroOfePorcenWidth = 0, 1200, g_oVistaSeleccionada.AhorroOfePorcenWidth)
        
    If g_oItemSeleccionado.proceso.UsarPonderacion Then
       .Columns("PUNTOS").Width = IIf(g_oVistaSeleccionada.PondWidth = 0, 1600, g_oVistaSeleccionada.PondWidth)
    End If
    
    iPos = 11
    
    'Redimensiona los atributos
    For Each oatrib In g_oVistaSeleccionada.ConfVistasItemAtrib
        .Columns("ATRIB" & oatrib.Atributo).Position = IIf(oatrib.Posicion = 0, iPos, oatrib.Posicion)
        .Columns("ATRIB" & oatrib.Atributo).Visible = oatrib.Visible
        .Columns("ATRIB" & oatrib.Atributo).Width = IIf(oatrib.Width = 0, 1600, oatrib.Width)
                
        iPos = iPos + 1
    Next
    
    'Pone el ancho de las filas
    If g_oVistaSeleccionada.AnchoFila = 0 Or IsNull(g_oVistaSeleccionada.AnchoFila) Then
        .RowHeight = ANCHO_FILA
    Else
        .RowHeight = g_oVistaSeleccionada.AnchoFila
    End If
    End With
    
    FormateoDecimales
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "RedimensionarGrid", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Redimensiona el grid de escalados en funci�n de la vista seleccionada</summary>
''' <remarks>Llamada desde: sdbcVistaActual_CloseUp; Tiempo m�ximo: 0</remarks>
Private Sub RedimensionarGridEsc()
    Dim oatrib As CConfVistaItemAtrib
    Dim oAtributo As CAtributo
    Dim i As Integer
    Dim j As Integer
    Dim iIndex As Integer
    Dim dblGrupoCarWidth As Double
    Dim iNumColsGR  As Integer
    Dim iNumColsGR1 As Integer
    Dim inumColumns As Integer
    Dim sGrupo As String
    Dim sColumna As String
    Dim iOrden As Integer
    Dim sAtributo As String
    Dim oColumn As SSDataWidgets_B.Column
    Dim bGrupoAtribVisible As Boolean
    
    '''''''''''''''''''''Redimensiona sdbgAdjEsc''''''''''''''''''''
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgAdjEsc
        iNumColsGR = .Groups(0).Columns.Count + .Groups(1).Columns.Count - 1
        iNumColsGR1 = .Groups(0).Columns.Count - 1
        
        .Groups(0).Width = g_oVistaSeleccionada.Grupo0Width
        
        'Posici�n volumnas grupo 0
        .Columns("DESCR").Position = 0
        .Columns("CANT").Position = g_oVistaSeleccionada.CantAdjPos
        .Columns("IMP").Position = g_oVistaSeleccionada.ImporteOfePos
        .Columns("AHORROIMP").Position = g_oVistaSeleccionada.AhorroOfePos
        .Columns("AHORROPORCEN").Position = g_oVistaSeleccionada.AhorroPorcenPos
        .Columns("PRECAPE").Position = g_oVistaSeleccionada.ConsumPos
        .Columns("CODPROVEACTUAL").Position = g_oVistaSeleccionada.PrecioPos
        
        'Visibilidad de los campos del grupo 0
        .Columns("DESCR").Visible = True
        .Columns("CANT").Visible = g_oVistaSeleccionada.CantAdjVisible
        .Columns("IMP").Visible = g_oVistaSeleccionada.ImporteOfeVisible
        .Columns("AHORROIMP").Visible = g_oVistaSeleccionada.AhorroOfeVisible
        .Columns("AHORROPORCEN").Visible = g_oVistaSeleccionada.AhorroPorcenVisible
        .Columns("PRECAPE").Visible = g_oVistaSeleccionada.ConsumVisible
        .Columns("CODPROVEACTUAL").Visible = g_oVistaSeleccionada.PrecioVisible
                
        'Tama�o de los campos del grupo 0
        'Se pone el width en el mismo orden en el que est�n los positions,porque sino algunas veces no funciona
        For i = 0 To .Groups(0).Columns.Count - 1
            iIndex = .Groups(0).ColPosition(i)
            Select Case .Groups(0).Columns(iIndex).Name
                Case "DESCR"
                    .Columns("DESCR").Width = g_oVistaSeleccionada.ProveDescrWidth
                Case "CANT"
                    .Columns("CANT").Width = g_oVistaSeleccionada.CantAdjWidth
                Case "IMP"
                    .Columns("IMP").Width = g_oVistaSeleccionada.ImporteOfeWidth
                Case "AHORROIMP"
                    .Columns("AHORROIMP").Width = g_oVistaSeleccionada.AhorroOfeWidth
                Case "AHORROPORCEN"
                    .Columns("AHORROPORCEN").Width = g_oVistaSeleccionada.AhorroPorcenWidth
                Case "PRECAPE"
                    .Columns("PRECAPE").Width = g_oVistaSeleccionada.ConsumWidth
                Case "CODPROVEACTUAL"
                    .Columns("CODPROVEACTUAL").Width = g_oVistaSeleccionada.PrecioWidth
            End Select
        Next
        
        'Formateo num�rico de las columnas del grupo 0 de la grid
        .Columns("AHORROIMP").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
        .Columns("AHORROPORCEN").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPorcen, True)
        .Columns("IMP").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
        .Columns("CANT").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecCant)
        .Columns("PRECAPE").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecCant)
        'No se le indica el formato aqu� porque si notambi�n intentar�a aplicar el formato num�rico al c�digo
        'del proveedor actual (a nivel de item)
        .Columns("CODPROVEACTUAL").NumberFormat = ""    'FormateoNumericoComp(g_oVistaSeleccionada.DecCant)
        
        'Redimensionar el grupo de atributos de caracter�stica
        .Groups(1).Visible = (g_oVistaSeleccionada.Vista <> TipoDeVistaDefecto.vistainicial)
        If g_oVistaSeleccionada.Vista <> TipoDeVistaDefecto.vistainicial Then
            If Not g_ogrupo.AtributosItem Is Nothing Then
                bGrupoAtribVisible = False
                
                'Visibilidad
                For Each oatrib In g_oVistaSeleccionada.ConfVistasItemAtrib
                    If Not g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)) Is Nothing Then
                        If IsNull(g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Or IsEmpty(g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Then
                            .Columns(CStr(oatrib.Atributo)).Visible = oatrib.Visible
                            If .Columns(CStr(oatrib.Atributo)).Visible Then
                                bGrupoAtribVisible = True
                                dblGrupoCarWidth = dblGrupoCarWidth + oatrib.Width
                            End If
                        End If
                    End If
                Next
                .Groups(1).Width = dblGrupoCarWidth
                .Groups(1).Visible = bGrupoAtribVisible
                
                'Tama�o y posici�n
                For Each oatrib In g_oVistaSeleccionada.ConfVistasItemAtrib
                    If Not g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)) Is Nothing Then
                        If IsNull(g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Or IsEmpty(g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Then
                            If oatrib.Posicion = 0 Then oatrib.Posicion = 1
                            .Columns(CStr(oatrib.Atributo)).Position = iNumColsGR1 + oatrib.Posicion
                            If oatrib.Width = 0 Then
                                .Columns(CStr(oatrib.Atributo)).Width = 500
                            Else
                                .Columns(CStr(oatrib.Atributo)).Width = oatrib.Width
                            End If
                        End If
                    End If
                Next
            End If
        Else
            'Visibilidad
            For Each oColumn In .Groups(1).Columns
                oColumn.Visible = False
            Next
            .Groups(1).Visible = False
        End If
        
        'Redimensiona el resto de grupos (para cada escalado)
        For i = 2 To .Groups.Count - 1
            inumColumns = 3     'N� de columnas en cada grupo (3 + n� de atributos aplicables a precio)
            sGrupo = .Groups(i).TagVariant
            
            If Not .Groups(i).Visible Then
                .Groups(i).Visible = True
            End If
            
            .Groups(i).Width = g_oVistaSeleccionada.GruposEscWidth    'tama�o del grupo
                                    
            'Posici�n de los campos:
            iOrden = iNumColsGR + (.Groups(i).Columns.Count * (i - 2))
            
            'Atributos
            If g_oVistaSeleccionada.Vista <> TipoDeVistaDefecto.vistainicial Then
                'Formateo num�rico de los atributos aplicables al precio
                If Not g_ogrupo.AtributosItem Is Nothing Then
                    For Each oAtributo In g_ogrupo.AtributosItem
                        If Not IsNull(g_ogrupo.AtributosItem.Item(CStr(oAtributo.idAtribProce)).PrecioFormula) And Not IsEmpty(g_ogrupo.AtributosItem.Item(CStr(oAtributo.idAtribProce)).PrecioFormula) Then
                            inumColumns = inumColumns + 1
                            
                            If Not IsNull(oAtributo.PrecioAplicarA) Then
                                .Columns(oAtributo.idAtribProce & CStr(i)).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                            ElseIf oAtributo.Tipo = TipoNumerico Then
                                If oAtributo.NumDecAtrib > g_oVistaSeleccionada.DecPrecios Then
                                    .Columns(oAtributo.idAtribProce & sGrupo).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                                Else
                                    .Columns(oAtributo.idAtribProce & sGrupo).NumberFormat = FormateoNumericoComp(oAtributo.NumDecAtrib)
                                End If
                            End If
                        End If
                    Next
                    
                    Set oAtributo = Nothing
                End If
            End If
            
            '-------- Posici�n de las columnas -------------
            For j = 0 To (.Groups(i).Columns.Count - 1)
                sColumna = ""
                iIndex = .Groups(i).ColPosition(j)
                sColumna = Left(.Groups(i).Columns(iIndex).Name, Len(.Groups(i).Columns(iIndex).Name) - Len(CStr(i)))
                Select Case sColumna
                    Case "ESC", "INI", "FIN", "CANTMAX", "ADJIMP", "ADJ"
                        'columnas ocultas
                    Case "ADJCANT"
                        .Columns("ADJCANT" & i).Position = g_oVistaSeleccionada.AdjEscPos + iOrden
                    Case "PRES"
                        .Columns("PRES" & i).Position = g_oVistaSeleccionada.PrecioEscPos + iOrden
                    Case "AHORRO"
                        .Columns("AHORRO" & i).Position = g_oVistaSeleccionada.AhorroEscPos + iOrden
                    Case Else
                        'Es un atributo
                        sAtributo = CStr(Left(.Groups(i).Columns(j).Name, Len(.Groups(i).Columns(j).Name) - Len(CStr(i))))
                        If g_oVistaSeleccionada.ConfVistasItemAtrib.Item(sAtributo).Posicion = 0 Then g_oVistaSeleccionada.ConfVistasItemAtrib.Item(sAtributo).Posicion = 1
                        .Columns(.Groups(i).Columns(j).Name).Position = g_oVistaSeleccionada.ConfVistasItemAtrib.Item(sAtributo).Posicion + iOrden
                End Select
            Next
            
            '-------- Visibilidad de los campos -----------
            .Columns("PRES" & i).Visible = g_oVistaSeleccionada.PrecioEscVisible
            .Columns("AHORRO" & i).Visible = g_oVistaSeleccionada.AhorroEscVisible
            .Columns("ADJCANT" & i).Visible = g_oVistaSeleccionada.AdjEscVisible
            'Atributos
            If g_oVistaSeleccionada.Vista <> TipoDeVistaDefecto.vistainicial Then
                For Each oatrib In g_oVistaSeleccionada.ConfVistasItemAtrib
                    If Not g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)) Is Nothing Then
                        If Not IsNull(g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) And Not IsEmpty(g_ogrupo.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Then
                            .Columns(oatrib.Atributo & i).Visible = oatrib.Visible
                        End If
                    End If
                Next
            Else
                'Es la vista inicial
                For j = 0 To .Groups(i).Columns.Count - 1
                    sColumna = Left(.Groups(i).Columns(j).Name, Len(.Groups(i).Columns(j).Name) - Len(CStr(i)))
                    If sColumna <> "PRES" And sColumna <> "AHORRO" And sColumna <> "ADJCANT" Then
                        .Groups(i).Columns(j).Visible = False
                    End If
                Next j
            End If
            
            '-------- Tama�o de los campos -----------
            For j = 0 To .Groups(i).Columns.Count - 1
                sColumna = ""
                iIndex = .Groups(i).ColPosition(j)
                sColumna = Left(.Groups(i).Columns(iIndex).Name, Len(.Groups(i).Columns(iIndex).Name) - Len(CStr(i)))
                Select Case sColumna
                    Case "ESC", "INI", "FIN", "CANTMAX", "ADJIMP", "ADJ"
                        'campos ocultos
                    Case "ADJCANT"
                        If g_oVistaSeleccionada.AdjEscWidth = 0 Then
                            .Columns("ADJCANT" & i).Width = 1200
                        Else
                            .Columns("ADJCANT" & i).Width = g_oVistaSeleccionada.AdjEscWidth
                        End If
                    Case "PRES"
                        If g_oVistaSeleccionada.PrecioEscWidth = 0 Then
                            .Columns("PRES" & i).Width = 1200
                        Else
                            .Columns("PRES" & i).Width = g_oVistaSeleccionada.PrecioEscWidth
                        End If
                    Case "AHORRO"
                        If g_oVistaSeleccionada.AhorroEscWidth = 0 Then
                            .Columns("AHORRO" & i).Width = 1200
                        Else
                            .Columns("AHORRO" & i).Width = g_oVistaSeleccionada.AhorroEscWidth
                        End If
                    Case Else
                        'Comprueba si la columna es un atributo
                        If g_oVistaSeleccionada.Vista <> TipoDeVistaDefecto.vistainicial Then
                            If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
                                If Not IsNull(g_ogrupo.AtributosItem.Item(sColumna).PrecioFormula) And Not IsEmpty(g_ogrupo.AtributosItem.Item(sColumna).PrecioFormula) Then
                                    If Not g_oVistaSeleccionada.ConfVistasItemAtrib.Item(CStr(sColumna)) Is Nothing Then
                                        If g_oVistaSeleccionada.ConfVistasItemAtrib.Item(CStr(sColumna)).Width = 0 Then
                                            .Columns(sColumna & i).Width = 500
                                        Else
                                            .Columns(sColumna & i).Width = g_oVistaSeleccionada.ConfVistasItemAtrib.Item(CStr(sColumna)).Width
                                        End If
                                    End If
                                End If
                            End If
                        End If
                End Select
            Next j
        Next i
        
        'Pone el ancho de las filas
        If g_oVistaSeleccionada.AnchoFila = 0 Or IsNull(g_oVistaSeleccionada.AnchoFila) Then
            .RowHeight = ANCHO_FILA
        Else
            .RowHeight = g_oVistaSeleccionada.AnchoFila
        End If
    End With
    
    FormateoDecimales
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "RedimensionarGridEsc", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Elimina un proveedor del grid en funci�n de la vista seleccionada</summary>
''' <remarks>Llamada desde: chkOcultarProvSinOfe_Click; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 26/10/2011</revision>
Private Sub RedimensionarProves()
    Dim oGrid As SSDBGrid
    Dim i As Integer
    Dim vbm As Variant
    Dim bVisible As Boolean
    Dim scodProve As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_ogrupo.UsarEscalados Then
        Set oGrid = sdbgAdjEsc
    Else
        Set oGrid = sdbgAdj
    End If

    With oGrid
        .MoveFirst
        
        'Redimensiona los proveedores
        i = .Rows - 1
        While (Not g_ogrupo.UsarEscalados And i >= 0) Or (g_ogrupo.UsarEscalados And i >= 1)
            vbm = .AddItemBookmark(i)
            If g_ogrupo.UsarEscalados Then
                scodProve = .Columns("ID").CellValue(vbm)
            Else
                scodProve = .Columns("PROVECOD").CellValue(vbm)
            End If
            
            If Not g_oVistaSeleccionada.ConfVistasItemProv.Item(scodProve) Is Nothing Then
            
                Select Case g_oVistaSeleccionada.ConfVistasItemProv.Item(scodProve).Visible
                    Case TipoProvVisible.NoVisible
                        .RemoveItem (i)
                    Case TipoProvVisible.DepenVista
                        'Si tiene marcados los checks y no tiene ofertas o no son adjudicables se elimina de la grid
                        bVisible = True
                        If g_oVistaSeleccionada.OcultarProvSinOfe Then
                            If g_ogrupo.UltimasOfertas.Item(Trim(scodProve)) Is Nothing Then
                                .RemoveItem (i)
                                bVisible = False
                            End If
                        End If
                        If bVisible And g_oVistaSeleccionada.OcultarNoAdj Then
                            If Not m_oProcesoSeleccionado.Ofertas.Item(scodProve) Is Nothing Then
                                If Not g_oOrigen.m_oEstOfes.Item(m_oProcesoSeleccionado.Ofertas.Item(scodProve).CodEst).adjudicable Then
                                    .RemoveItem (i)
                                End If
                            Else
                                .RemoveItem (i)
                            End If
                        End If
                End Select
            End If
            i = i - 1
        Wend
    End With
    
    Set oGrid = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "RedimensionarProves", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Configura la vista actual</summary>
''' <remarks>Llamada desde: itemseleccionado,ItemEscaladoSeleccionado; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 28/10/2011</revision>

Private Sub ConfiguracionVistaActual(ByVal iVista As Integer, ByVal udtTipoVista As TipoDeVistaDefecto, ByVal sUsuarioVista As String)
    
    Dim i As Integer
    Dim oatrib As CConfVistaItemAtrib
    Dim sCod As String
    
    'Carga la configuraci�n de la vista que se pasa por par�metro para el usuario y el �tem seleccionado
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set g_oVistaSeleccionada = Nothing
    
    sCod = sUsuarioVista & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUSU - Len(sUsuarioVista))
    
    If Not g_oOrigen.m_oVistaSeleccionadaGr Is Nothing Then
        If g_oOrigen.m_oVistaSeleccionadaGr.Vista <> iVista Or g_oOrigen.m_oVistaSeleccionadaGr.TipoVista <> udtTipoVista Or g_oOrigen.m_oVistaSeleccionadaGr.UsuarioVista <> sUsuarioVista Then Set g_oOrigen.m_oVistaSeleccionadaGr = Nothing
    End If
    
    If Not g_oOrigen.m_oVistaSeleccionadaAll Is Nothing Then
        If g_oOrigen.m_oVistaSeleccionadaAll.Vista <> iVista Or g_oOrigen.m_oVistaSeleccionadaAll.TipoVista <> udtTipoVista Or g_oOrigen.m_oVistaSeleccionadaAll.UsuarioVista <> sUsuarioVista Then Set g_oOrigen.m_oVistaSeleccionadaAll = Nothing
    End If
    
    If g_ogrupo.ConfVistasItem Is Nothing Then
        Set g_ogrupo.ConfVistasItem = oFSGSRaiz.Generar_CConfVistasItem
        If udtTipoVista = TipoDeVistaDefecto.VistaDeResponsable Then
            g_ogrupo.CargarTodasLasConfVistasItem m_oProcesoSeleccionado.responsable.Usuario, iVista, , udtTipoVista
        ElseIf udtTipoVista = VistaDeUsuario Then
            g_ogrupo.CargarTodasLasConfVistasItem oUsuarioSummit.Cod, iVista, , udtTipoVista
        ElseIf udtTipoVista = VistaDePlantilla Then
            g_ogrupo.ConfVistasItem.Add g_ogrupo, iVista, , 1, True, 1000, 2, True, 1000, 3, True, 1000, 7, True, 1200, 6, True, 1200, 5, True, 1200, 4, True, 1200, 3120, m_sUsu, False, False, 13, False, 1005, ANCHO_FILA, 2, 2, 2, 2, 10, True, 1200, 11, True, 1200, 12, True, 1200, udtTipoVista, , , 2500, 7725, 1, True, 1200, 2, True, 1200, 3, True, 1200
        ElseIf udtTipoVista = vistainicial Then
            g_ogrupo.ConfVistasItem.Add g_ogrupo, 0, , 1, True, 1000, 2, True, 1000, 3, True, 1000, 7, True, 1200, 6, True, 1200, 5, True, 1200, 4, True, 1200, 3120, m_sUsu, False, False, 13, False, 1005, ANCHO_FILA, 2, 2, 2, 2, 10, True, 1200, 11, True, 1200, 12, True, 1200, vistainicial, , , 2500, 7725, 1, True, 1200, 2, True, 1200, 3, True, 1200
        ElseIf udtTipoVista = VistaDeOtroUsuario Then
            g_ogrupo.CargarTodasLasConfVistasItem sUsuarioVista, iVista, , udtTipoVista
        End If
        If Not g_ogrupo.ConfVistasItem.Item(CStr(iVista & udtTipoVista & sCod)) Is Nothing Then
            Set g_oVistaSeleccionada = g_ogrupo.ConfVistasItem.Item(CStr(iVista & udtTipoVista & sCod))
        Else
            Set g_oVistaSeleccionada = g_ogrupo.ConfVistasItem.Add(g_ogrupo, iVista, , 1, True, 1000, 2, True, 1000, 3, True, 1000, 7, True, 1200, 6, True, 1200, 5, True, 1200, 4, True, 1200, 3120, m_sUsu, False, False, 13, False, 1005, ANCHO_FILA, 2, 2, 2, 2, 10, True, 1200, 11, True, 1200, 12, True, 1200, udtTipoVista, sUsuarioVista, , 2500, 7725, 1, True, 1200, 2, True, 1200, 3, True, 1200)
        End If
    End If
    
    'Si no hay datos en BD para esa vista,o no hab�a vista por defecto para el �tem se a�ade
    'a la colecci�n y se inserta en BD con los valores por defecto
    If g_ogrupo.ConfVistasItem.Item(CStr(iVista & udtTipoVista & sCod)) Is Nothing Then
        If udtTipoVista = TipoDeVistaDefecto.VistaDeResponsable Then
            g_ogrupo.CargarTodasLasConfVistasItem m_oProcesoSeleccionado.responsable.Usuario, iVista, , udtTipoVista
        ElseIf udtTipoVista = VistaDeUsuario Then
            g_ogrupo.CargarTodasLasConfVistasItem oUsuarioSummit.Cod, iVista, , udtTipoVista
        ElseIf udtTipoVista = VistaDePlantilla Then
            g_ogrupo.ConfVistasItem.Add g_ogrupo, iVista, , 1, True, 1000, 2, True, 1000, 3, True, 1000, 7, True, 1200, 6, True, 1200, 5, True, 1200, 4, True, 1200, 3120, m_sUsu, False, False, 13, False, 1005, ANCHO_FILA, 2, 2, 2, 2, 10, True, 1200, 11, True, 1200, 12, True, 1200, udtTipoVista, sUsuarioVista, , 2500, 7725, 1, True, 1200, 2, True, 1200, 3, True, 1200
        ElseIf udtTipoVista = vistainicial Then
            g_ogrupo.ConfVistasItem.Add g_ogrupo, 0, , 1, True, 1000, 2, True, 1000, 3, True, 1000, 7, True, 1200, 6, True, 1200, 5, True, 1200, 4, True, 1200, 3120, m_sUsu, False, False, 13, False, 1005, ANCHO_FILA, 2, 2, 2, 2, 10, True, 1200, 11, True, 1200, 12, True, 1200, vistainicial, sUsuarioVista, , 2500, 7725, 1, True, 1200, 2, True, 1200, 3, True, 1200
        ElseIf udtTipoVista = VistaDeOtroUsuario Then
            g_ogrupo.CargarTodasLasConfVistasItem sUsuarioVista, iVista, , udtTipoVista
        End If
        If Not g_ogrupo.ConfVistasItem.Item(CStr(iVista & udtTipoVista & sCod)) Is Nothing Then
            Set g_oVistaSeleccionada = g_ogrupo.ConfVistasItem.Item(CStr(iVista & udtTipoVista & sCod))
        Else
            Set g_oVistaSeleccionada = g_ogrupo.ConfVistasItem.Add(g_ogrupo, iVista, , 1, True, 1000, 2, True, 1000, 3, True, 1000, 7, True, 1200, 6, True, 1200, 5, True, 1200, 4, True, 1200, 3120, m_sUsu, False, False, 13, False, 1005, ANCHO_FILA, 2, 2, 2, 2, 10, True, 1200, 11, True, 1200, 12, True, 1200, udtTipoVista, sUsuarioVista, , 2500, 7725, 1, True, 1200, 2, True, 1200, 3, True, 1200)
        End If
    Else
        Set g_oVistaSeleccionada = g_ogrupo.ConfVistasItem.Item(CStr(iVista & udtTipoVista & sCod))
    End If
        
    'Carga las configuraciones de los atributos
    'Se cargan todos los atributos de proceso, del grupo del item y de item
    g_oVistaSeleccionada.CargarConfAtributos
    'Si se carga un atributo en la vistas que no existe en la colecci�n de atributos lo elimina de la colecci�n
    For Each oatrib In g_oVistaSeleccionada.ConfVistasItemAtrib
        If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing And Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing And Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
            If m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(oatrib.Atributo)) Is Nothing And _
                m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(oatrib.Atributo)) Is Nothing And _
                m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)) Is Nothing Then
                    g_oVistaSeleccionada.ConfVistasItemAtrib.Remove (CStr(oatrib.Atributo))
            End If
        End If
    Next
    
    'Carga las configuraciones de los proveedores
    g_oVistaSeleccionada.CargarConfProveedores
    
    'Carga los formatos decimales:
    If g_oVistaSeleccionada.DecResult > 0 Then
        m_sFormatoNumber = "#,##0."
        For i = 1 To g_oVistaSeleccionada.DecResult
            m_sFormatoNumber = m_sFormatoNumber & "0"
        Next i
    Else
        m_sFormatoNumber = "#,##0"
    End If
    
    If g_oVistaSeleccionada.DecPrecios > 0 Then
        m_sFormatoNumberPrecio = "#,##0."
        For i = 1 To g_oVistaSeleccionada.DecPrecios
            m_sFormatoNumberPrecio = m_sFormatoNumberPrecio & "0"
        Next i
    Else
        m_sFormatoNumberPrecio = "#,##0"
    End If
    
    If g_oVistaSeleccionada.DecCant > 0 Then
        m_sFormatoNumberCant = "#,##0."
        For i = 1 To g_oVistaSeleccionada.DecCant
            m_sFormatoNumberCant = m_sFormatoNumberCant & "0"
        Next i
    Else
        m_sFormatoNumberCant = "#,##0"
    End If
    
    g_oVistaSeleccionada.HayCambios = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ConfiguracionVistaActual", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub CargarComboVistas()
    Dim oVista As CConfVistaProce
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcVistaActual.RemoveAll
    sdbcVistaDefecto.RemoveAll

    Set oVistasCombo = Nothing
    Set oVistasCombo = oFSGSRaiz.Generar_CConfVistasProce
   
    
    oVistasCombo.CargarCombosVistas m_oProcesoSeleccionado, oUsuarioSummit.Cod, m_bPermitirVistas
    For Each oVista In oVistasCombo
        Select Case oVista.TipoVista
            Case TipoDeVistaDefecto.vistainicial
                sdbcVistaActual.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & "INI"
                sdbcVistaDefecto.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & "INI"
            Case TipoDeVistaDefecto.VistaDePlantilla
                sdbcVistaActual.AddItem m_sVistaPlan & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
                sdbcVistaDefecto.AddItem m_sVistaPlan & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
            Case TipoDeVistaDefecto.VistaDeResponsable
                sdbcVistaActual.AddItem m_sVistaResp & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
                sdbcVistaDefecto.AddItem m_sVistaResp & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
            Case TipoDeVistaDefecto.VistaDeUsuario
                sdbcVistaActual.AddItem m_sVistaUsu & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
                sdbcVistaDefecto.AddItem m_sVistaUsu & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
            Case TipoDeVistaDefecto.VistaDeOtroUsuario
                sdbcVistaActual.AddItem m_sVistaUsu & " (" & oVista.UsuarioNombre & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
                sdbcVistaDefecto.AddItem m_sVistaUsu & " (" & oVista.UsuarioNombre & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
        End Select
    Next
    sdbcVistaActual.ListAutoPosition = True
    sdbcVistaActual.SelLength = Len(sdbcVistaActual.Text)
    sdbcVistaActual.Refresh
    
    sdbcVistaDefecto.SelStart = 0
    sdbcVistaDefecto.SelLength = Len(sdbcVistaDefecto.Text)
    sdbcVistaDefecto.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CargarComboVistas", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Carga la configuraci�n de la vista actual para el �tem seleccionado y campos de configuraci�n correspondientes</summary>
''' <remarks>Llamada desde: sdbcVistaActual_CloseUp; Tiempo m�ximo:0</remarks>
''' <revision>LTG 21/02/2012</revision>
Private Sub CargarVista()
    Dim oatrib As CConfVistaItemAtrib
    Dim oProv As CConfVistaItemProv
    Dim sPertenece As String
    Dim bProvVisible As Boolean
    Dim bCargar As Boolean
    Dim scodProve As String
    
    'Limpia las grids
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgOcultarCampos.RemoveAll
    sdbgOcultarProv.RemoveAll
    
    If g_oVistaSeleccionada Is Nothing Then
        Exit Sub
    End If
    
    m_bCargandoVista = True
    
    With sdbgOcultarCampos
        If g_ogrupo.UsarEscalados <> 0 Then
            'Ahorro
            .AddItem IIf(g_oVistaSeleccionada.AhorroVisible, "-1", "0") & Chr(m_lSeparador) & m_sAhorro & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Ahorro"
            'Ahorro %
            .AddItem IIf(g_oVistaSeleccionada.AhorroPorcenVisible, "-1", "0") & Chr(m_lSeparador) & m_sAhorroPorcen & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AhorroPorcen"
            'Importe adj.
            .AddItem IIf(g_oVistaSeleccionada.ImporteOfeVisible, "-1", "0") & Chr(m_lSeparador) & m_sImpAdj & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "ImporteOfe"
            'Prove. actual-Prec. Uni. medio
            .AddItem BooleanToSQLBinary(g_oVistaSeleccionada.PrecioVisible) & Chr(m_lSeparador) & m_sProveAct & " - " & m_sPrecUniMedio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "CODPROVEACTUAL"
            'Imp. consum-Pres. Uni. medio
            .AddItem BooleanToSQLBinary(g_oVistaSeleccionada.ConsumVisible) & Chr(m_lSeparador) & m_sImporteConsum & " - " & m_sPresUniMedio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "PRECAPE"
            'Cantidad adj.
            .AddItem IIf(g_oVistaSeleccionada.CantAdjVisible, "-1", "0") & Chr(m_lSeparador) & m_sCantAdjudicada & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "CantAdj"
            'Pres. unitario-Precio/Importes
            .AddItem IIf(g_oVistaSeleccionada.PrecioEscVisible, "-1", "0") & Chr(m_lSeparador) & m_sPresUni & " - " & m_sPrecioImporte & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "PrecioEsc"
            'Objetivo-Ahorro unitario
            .AddItem IIf(g_oVistaSeleccionada.AhorroEscVisible, "-1", "0") & Chr(m_lSeparador) & m_sObjetivo & " - " & m_sAhorUni & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AhorroEsc"
            'Adjudicado escalado
            .AddItem IIf(g_oVistaSeleccionada.AdjEscVisible, "-1", "0") & Chr(m_lSeparador) & m_sCantAdjudicada & " - " & m_sCantAdjudicada & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AdjEsc"
        Else
            'Primero carga los campos fijos:
            'Precio
            .AddItem IIf(g_oVistaSeleccionada.PrecioVisible, "-1", "0") & Chr(m_lSeparador) & m_sPrecio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Precio"
            'Cant Adj.
            .AddItem IIf(g_oVistaSeleccionada.CantAdjVisible, "-1", "0") & Chr(m_lSeparador) & m_sCantAdj & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "CantAdj"
            'Cant Max.
            If m_oProcesoSeleccionado.SolicitarCantMax Then
                .AddItem IIf(g_oVistaSeleccionada.CantMaxVisible, "-1", "0") & Chr(m_lSeparador) & m_sCantMax & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "CantMax"
            End If
            'Consumido
            .AddItem IIf(g_oVistaSeleccionada.ConsumVisible, "-1", "0") & Chr(m_lSeparador) & m_sConsumido & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Consumido"
            'Adjudicado
            .AddItem IIf(g_oVistaSeleccionada.AdjudVisible, "-1", "0") & Chr(m_lSeparador) & m_sAdjudicado & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Adjudicado"
            'Ahorro
            .AddItem IIf(g_oVistaSeleccionada.AhorroVisible, "-1", "0") & Chr(m_lSeparador) & m_sAhorro & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Ahorro"
            '%Ahorro
            .AddItem IIf(g_oVistaSeleccionada.AhorroPorcenVisible, "-1", "0") & Chr(m_lSeparador) & m_sAhorroPorcen & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AhorroPorcen"
            'Importe de la oferta
            .AddItem IIf(g_oVistaSeleccionada.ImporteOfeVisible, "-1", "0") & Chr(m_lSeparador) & m_sImporteOfe & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "ImporteOfe"
            'Ahorro de la oferta
            .AddItem IIf(g_oVistaSeleccionada.AhorroOfeVisible, "-1", "0") & Chr(m_lSeparador) & m_sAhorroOfe & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AhorroOfe"
            '%Ahorro de la oferta
            .AddItem IIf(g_oVistaSeleccionada.AhorroOfePorcenVisible, "-1", "0") & Chr(m_lSeparador) & m_sAhorroOfePorcen & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AhorroOfePorcen"
        End If
    End With
    
    'Ahora carga los atributos en la grid
    If Not g_oVistaSeleccionada.ConfVistasItemAtrib Is Nothing Then
        For Each oatrib In g_oVistaSeleccionada.ConfVistasItemAtrib
            sPertenece = ""
            If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
                If Not m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(oatrib.Atributo)) Is Nothing Then
                    sPertenece = m_sProceso
                Else
                    If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
                        If Not m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(oatrib.Atributo)) Is Nothing Then
                            sPertenece = m_sLitGrupo & ":"
                            If IsNull(m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(oatrib.Atributo)).codgrupo) Then
                                sPertenece = sPertenece & m_sTodos
                            Else
                                sPertenece = sPertenece & m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(oatrib.Atributo)).codgrupo
                            End If
                        Else
                            If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
                                If Not m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)) Is Nothing Then
                                    sPertenece = m_sItems & ":"
                                    If IsNull(m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).codgrupo) Then
                                        sPertenece = sPertenece & m_sTodos
                                    Else
                                        sPertenece = sPertenece & m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).codgrupo
                                    End If
                                End If
                            End If
                        End If
                    Else
                        If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
                            If Not m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)) Is Nothing Then
                                sPertenece = m_sItems & ":"
                                If IsNull(m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).codgrupo) Then
                                    sPertenece = sPertenece & m_sTodos
                                Else
                                    sPertenece = sPertenece & m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).codgrupo
                                End If
                            End If
                        End If
                    End If
                End If
            Else
                If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
                    If Not m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(oatrib.Atributo)) Is Nothing Then
                        sPertenece = m_sLitGrupo & ":"
                        If IsNull(m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(oatrib.Atributo)).codgrupo) Then
                            sPertenece = sPertenece & m_sTodos
                        Else
                            sPertenece = sPertenece & m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(oatrib.Atributo)).codgrupo
                        End If
                    Else
                        If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
                            If Not m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)) Is Nothing Then
                                sPertenece = m_sItems & ":"
                                If IsNull(m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).codgrupo) Then
                                    sPertenece = sPertenece & m_sTodos
                                Else
                                    sPertenece = sPertenece & m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).codgrupo
                                End If
                            End If
                        End If
                    End If
                Else
                    If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
                        If Not m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)) Is Nothing Then
                            sPertenece = m_sItems & ":"
                            If IsNull(m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).codgrupo) Then
                                sPertenece = sPertenece & m_sTodos
                            Else
                                sPertenece = sPertenece & m_oProcesoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).codgrupo
                            End If
                        End If
                    End If
                End If
            End If
            sdbgOcultarCampos.AddItem IIf(oatrib.Visible, "-1", "0") & Chr(m_lSeparador) & oatrib.CodAtributo & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.Atributo
        Next
        'Puntos totales
        If g_oItemSeleccionado.proceso.UsarPonderacion Then
            sdbgOcultarCampos.AddItem IIf(g_oVistaSeleccionada.PondVisible, "-1", "0") & Chr(m_lSeparador) & m_sPuntos & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Puntos"
        End If
    End If
    
    'Carga los proveedores en la grid de ocultar o no los provs
    If Not g_oVistaSeleccionada.ConfVistasItemProv Is Nothing Then
        For Each oProv In g_oVistaSeleccionada.ConfVistasItemProv
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                scodProve = oProv.Proveedor & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Proveedor))
                If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(g_oItemSeleccionado.grupoCod) Is Nothing Then
                    bCargar = False
                End If
            End If
            If bCargar Then
                Select Case oProv.Visible
                    Case TipoProvVisible.Visible
                        bProvVisible = True
                    Case TipoProvVisible.NoVisible
                        bProvVisible = False
                    Case TipoProvVisible.DepenVista
                        'Comprueba si tiene ofertas:
                        bProvVisible = True
                        If g_oVistaSeleccionada.OcultarProvSinOfe = True Then
                            If Not g_ogrupo.UltimasOfertas Is Nothing Then
                                If g_ogrupo.UltimasOfertas.Item(Trim(oProv.Proveedor)) Is Nothing Then
                                    bProvVisible = False
                                End If
                            Else
                                bProvVisible = False
                            End If
                        End If
                        'Comprueba si la oferta es adjudicable:
                        If bProvVisible = True And g_oVistaSeleccionada.OcultarNoAdj = True Then
                            If Not m_oProcesoSeleccionado.Ofertas Is Nothing Then
                                If m_oProcesoSeleccionado.Ofertas.Item(oProv.Proveedor) Is Nothing Then
                                    bProvVisible = False
                                Else
                                    If Not g_oOrigen.m_oEstOfes.Item(m_oProcesoSeleccionado.Ofertas.Item(oProv.Proveedor).CodEst).adjudicable Then
                                        bProvVisible = False
                                    End If
                                End If
                            Else
                                bProvVisible = False
                            End If
                        End If
                End Select
                
                sdbgOcultarProv.AddItem IIf(bProvVisible, "-1", "0") & Chr(m_lSeparador) & oProv.Proveedor & " - " & oProv.DenProve & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oProv.Proveedor
            End If
        Next
    End If
    
    'Chequea o no los checks correspondientes:
    chkOcultarNoAdj.Value = IIf(g_oVistaSeleccionada.OcultarNoAdj = True, 1, 0)
    chkOcultarProvSinOfe.Value = IIf(g_oVistaSeleccionada.OcultarProvSinOfe = True, 1, 0)
        
    'N�mero de decimales a mostrar:
    txtDecCant.Text = g_oVistaSeleccionada.DecCant
    txtDecPorcen.Text = g_oVistaSeleccionada.DecPorcen
    txtDecPrec.Text = g_oVistaSeleccionada.DecPrecios
    txtDecResult.Text = g_oVistaSeleccionada.DecResult
    
    m_bCargandoVista = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CargarVista", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Sub CargarAtribAplicarFormulas()
    'A�ade a la grid de aplicar precio todos los atributos del proceso o del grupo del �tem con f�rmulas
    Dim oatrib As CAtributo
    Dim sAplicar As String
    Dim lMayor As Long
    Dim iOrden As Integer
    Dim bSalir As Boolean
    Dim iOrdenSiguiente As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oAtribsFormulas Is Nothing Then Exit Sub
    If m_oAtribsFormulas.Count = 0 Then Exit Sub
    
    sdbgAplicarPrecio.RemoveAll
    
    bSalir = False
    iOrdenSiguiente = 0
    
    While bSalir = False
        iOrden = 0
        lMayor = 0
            
        For Each oatrib In m_oAtribsFormulas
            If lMayor = 0 And iOrden = 0 Then
                If iOrdenSiguiente = 0 Then
                    lMayor = oatrib.idAtribProce
                    iOrden = oatrib.Orden
                ElseIf oatrib.Orden <= iOrdenSiguiente Then
                    lMayor = oatrib.idAtribProce
                    iOrden = oatrib.Orden
                End If
            ElseIf (oatrib.Orden > iOrden) Then
                If (oatrib.Orden <= iOrdenSiguiente And iOrdenSiguiente > 0) Or iOrdenSiguiente = 0 Then
                    lMayor = oatrib.idAtribProce
                    iOrden = oatrib.Orden
                End If
            End If
        Next
    
        If lMayor = 0 Then Exit Sub
        
        Set oatrib = m_oAtribsFormulas.Item(CStr(lMayor))
        iOrdenSiguiente = oatrib.Orden - 1
        
        If oatrib.codgrupo = g_oItemSeleccionado.grupoCod Or IsNull(oatrib.codgrupo) Then
            If oatrib.PrecioAplicarA = 2 Or oatrib.PrecioAplicarA = 3 Then
                If oatrib.PrecioAplicarA = 2 Then
                    sAplicar = m_sTotalItem 'Total del item
                Else
                    sAplicar = m_sPrecUni 'Precio unitario del item
                End If
                If (oatrib.PrecioDefecto = 1 And IsNull(oatrib.UsarPrec)) Or oatrib.UsarPrec = 1 Then
                    sdbgAplicarPrecio.AddItem oatrib.Cod & "   " & oatrib.Den & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & sAplicar & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & NullToStr(oatrib.codgrupo)
                Else
                    sdbgAplicarPrecio.AddItem oatrib.Cod & "   " & oatrib.Den & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & sAplicar & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & NullToStr(oatrib.codgrupo)
                End If
            End If
        End If
            
        Set oatrib = Nothing
        If iOrden = 1 Then
            bSalir = True
        End If
        
    Wend
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CargarAtribAplicarFormulas", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub OcultarCampo(ByVal Campo As String, bVisible As Boolean)
    Dim oEscalado As CEscalado
    Dim oColumn As Column
    Dim bGrupoVisible As Boolean
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_ogrupo.UsarEscalados Then
        With sdbgAdjEsc
            Select Case Campo
                Case "Ahorro"
                    g_oVistaSeleccionada.AhorroVisible = bVisible
                    .Columns("AHORROIMP").Visible = bVisible
                    .Columns("AHORROIMP").Position = g_oVistaSeleccionada.AhorroPos
                Case "AhorroPorcen"
                    g_oVistaSeleccionada.AhorroPorcenVisible = bVisible
                    .Columns("AHORROPORCEN").Visible = bVisible
                    .Columns("AHORROPORCEN").Position = g_oVistaSeleccionada.AhorroPorcenPos
                Case "ImporteOfe"
                    g_oVistaSeleccionada.ImporteOfeVisible = bVisible
                    .Columns("IMP").Visible = bVisible
                    .Columns("IMP").Position = g_oVistaSeleccionada.ImporteOfePos
                Case "CantAdj"
                    g_oVistaSeleccionada.CantAdjVisible = bVisible
                    .Columns("CANT").Visible = bVisible
                    .Columns("CANT").Position = g_oVistaSeleccionada.CantAdjPos
                Case "PRECAPE"
                    g_oVistaSeleccionada.ConsumVisible = bVisible
                    .Columns("PRECAPE").Visible = bVisible
                    .Columns("PRECAPE").Position = g_oVistaSeleccionada.ConsumPos
                Case "CODPROVEACTUAL"
                    g_oVistaSeleccionada.PrecioVisible = bVisible
                    .Columns("CODPROVEACTUAL").Visible = bVisible
                    .Columns("CODPROVEACTUAL").Position = g_oVistaSeleccionada.PrecioPos
                Case "PrecioEsc"
                    g_oVistaSeleccionada.PrecioEscVisible = bVisible
                    For i = 2 To .Groups.Count - 1
                        .Columns("PRES" & CStr(i)).Visible = bVisible
                    Next
                    Set oEscalado = Nothing
                Case "AhorroEsc"
                    g_oVistaSeleccionada.AhorroEscVisible = bVisible
                    For i = 2 To .Groups.Count - 1
                        .Columns("AHORRO" & CStr(i)).Visible = bVisible
                    Next
                    Set oEscalado = Nothing
                Case "AdjEsc"
                    g_oVistaSeleccionada.AdjEscVisible = bVisible
                    For i = 2 To .Groups.Count - 1
                        .Columns("ADJCANT" & CStr(i)).Visible = bVisible
                    Next
                    Set oEscalado = Nothing
                Case Else
                    'Es un atributo
                    g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Visible = bVisible
                    If Not g_ogrupo.AtributosItem.Item(Campo) Is Nothing Then
                        If IsNull(g_ogrupo.AtributosItem.Item(Campo).PrecioFormula) Or IsEmpty(g_ogrupo.AtributosItem.Item(Campo).PrecioFormula) Then
                            'Es un atributo de caracter�stica
                            .Columns(Campo).Visible = bVisible
                            
                            If bVisible Then
                                If g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Posicion = 0 Then
                                    g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Posicion = .Groups(0).Columns.Count
                                Else
                                    .Columns(Campo).Position = .Groups(0).Columns.Count - 1 + g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Posicion
                                End If
                                If g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Width = 0 Then
                                    .Columns(Campo).Width = 500
                                Else
                                    .Columns(Campo).Width = g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Width
                                End If
                                If Not .Groups(1).Visible Then .Groups(1).Visible = True
                                If .Groups(1).Width < .Columns(Campo).Width Then .Groups(1).Width = .Columns(Campo).Width
                            Else
                                'Ocultar el grupo si no hay columnas visibles
                                bGrupoVisible = False
                                For Each oColumn In .Groups(1).Columns
                                    If oColumn.Visible Then
                                        bGrupoVisible = True
                                        Exit For
                                    End If
                                Next
                                Set oColumn = Nothing
                                .Groups(1).Visible = bGrupoVisible
                            End If
                        Else
                            'es un atributo aplicable a precio
                            For i = 2 To .Groups.Count - 1
                                .Columns(Campo & CStr(i)).Visible = bVisible
                            Next
                            Set oEscalado = Nothing
                        End If
                    End If
            End Select
        End With
    Else
        With sdbgAdj
            Select Case Campo
                Case "Precio"
                    g_oVistaSeleccionada.PrecioVisible = bVisible
                    .Columns("PRECIO").Visible = bVisible
                    .Columns("PRECIO").Position = g_oVistaSeleccionada.PrecioPos
                    
                Case "CantAdj"
                    g_oVistaSeleccionada.CantAdjVisible = bVisible
                    .Columns("CANTADJ").Visible = bVisible
                    .Columns("CANTADJ").Position = g_oVistaSeleccionada.CantAdjPos
                    
                Case "CantMax"
                    g_oVistaSeleccionada.CantMaxVisible = bVisible
                    .Columns("CANTMAX").Visible = bVisible
                    .Columns("CANTMAX").Position = g_oVistaSeleccionada.CantMaxPos
                                
                Case "AhorroPorcen"
                    g_oVistaSeleccionada.AhorroPorcenVisible = bVisible
                    .Columns("AHO_PORCEN").Visible = bVisible
                    .Columns("AHO_PORCEN").Position = g_oVistaSeleccionada.AhorroPorcenPos
                    
                Case "Ahorro"
                    g_oVistaSeleccionada.AhorroVisible = bVisible
                    .Columns("AHO").Visible = bVisible
                    .Columns("AHO").Position = g_oVistaSeleccionada.AhorroPos
                    
                Case "Adjudicado"
                    g_oVistaSeleccionada.AdjudVisible = bVisible
                    .Columns("ADJ").Visible = bVisible
                    .Columns("ADJ").Position = g_oVistaSeleccionada.AdjudPos
                    
                Case "Consumido"
                    g_oVistaSeleccionada.ConsumVisible = bVisible
                    .Columns("CONSUM").Visible = bVisible
                    .Columns("CONSUM").Position = g_oVistaSeleccionada.ConsumPos
                    
                Case "ImporteOfe"
                    g_oVistaSeleccionada.ImporteOfeVisible = bVisible
                    .Columns("IMP_OFE").Visible = bVisible
                    If IsNull(g_oVistaSeleccionada.ImporteOfePos) Then
                        .Columns("IMP_OFE").Position = 10
                    Else
                        .Columns("IMP_OFE").Position = g_oVistaSeleccionada.ImporteOfePos
                    End If
                    
                Case "AhorroOfe"
                    g_oVistaSeleccionada.AhorroOfeVisible = bVisible
                    .Columns("AHORRO_OFE").Visible = bVisible
                    If IsNull(g_oVistaSeleccionada.AhorroOfePos) Then
                        .Columns("AHORRO_OFE").Position = 11
                    Else
                        .Columns("AHORRO_OFE").Position = g_oVistaSeleccionada.AhorroOfePos
                    End If
                    
                Case "AhorroOfePorcen"
                    g_oVistaSeleccionada.AhorroOfePorcenVisible = bVisible
                    .Columns("AHORRO_OFE_PORCEN").Visible = bVisible
                    If IsNull(g_oVistaSeleccionada.AhorroOfePorcenPos) Then
                        .Columns("AHORRO_OFE_PORCEN").Position = 12
                    Else
                        .Columns("AHORRO_OFE_PORCEN").Position = g_oVistaSeleccionada.AhorroOfePorcenPos
                    End If
                    
                Case "Puntos"
                    g_oVistaSeleccionada.PondVisible = bVisible
                    .Columns("PUNTOS").Visible = bVisible
                    .Columns("PUNTOS").Position = g_oVistaSeleccionada.PondPos
                    
                Case Else  'Es un atributo
                    g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Visible = bVisible
                    .Columns("ATRIB" & g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Atributo).Visible = bVisible
                    If g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Posicion = 0 Then
                        g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Posicion = .Columns("ATRIB" & g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Atributo).Position
                    Else
                        .Columns("ATRIB" & g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Atributo).Position = g_oVistaSeleccionada.ConfVistasItemAtrib.Item(Campo).Posicion
                    End If
            End Select
        End With
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "OcultarCampo", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub


''' <summary>
''' Oculta o desoculta los proveedores de la vista que tienen ofertas no adjudicables
''' </summary>
'''
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en check de la vista; Tiempo m�ximo: 0</remarks>
Private Sub chkOcultarNoAdj_Click()
    Dim i As Integer
    Dim vbm As Variant
    Dim bQuitar As Boolean
    Dim j As Integer
    Dim sProve As String
    
    'Oculta las ofertas no adjudicables
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkOcultarNoAdj.Value = vbChecked Then
        g_oVistaSeleccionada.OcultarNoAdj = True
        
        If g_ogrupo.UsarEscalados Then
            With sdbgAdjEsc
                i = .Rows - 1
                While i >= 0
                    bQuitar = False
                    vbm = .AddItemBookmark(i)
                    If .Columns("PROV").CellValue(vbm) = "1" Then
                        sProve = .Columns("ID").CellValue(vbm)
                        'Si para el proveedor la oferta es no adjudicable lo oculta
                        If Not m_oProcesoSeleccionado.Ofertas.Item(sProve) Is Nothing Then
                            If Not g_oOrigen.m_oEstOfes.Item(m_oProcesoSeleccionado.Ofertas.Item(sProve).CodEst).adjudicable Then
                                bQuitar = True
                            End If
                            If bQuitar Then
                                'Deschequea los proveedores sin ofertas que estaban visibles
                                ChequearProve sProve, False
                                
                                .RemoveItem (i)
                            End If
                            
                        End If
                        
                        If bQuitar Then
                            g_oVistaSeleccionada.ConfVistasItemProv.Item(sProve).Visible = TipoProvVisible.NoVisible
                        End If
                    End If
                    
                    i = i - 1
                Wend
                .MoveFirst
            End With
        Else
            With sdbgAdj
                i = .Rows - 1
                While i >= 0
                    bQuitar = False
                    vbm = .AddItemBookmark(i)
                    sProve = .Columns("PROVECOD").CellValue(vbm)
                    'Si para el proveedor la oferta es no adjudicable lo oculta
                    If Not m_oProcesoSeleccionado.Ofertas.Item(sProve) Is Nothing Then
                        If Not g_oOrigen.m_oEstOfes.Item(m_oProcesoSeleccionado.Ofertas.Item(sProve).CodEst).adjudicable Then
                            bQuitar = True
                        End If
                        If bQuitar Then
                            'Deschequea los proveedores sin ofertas que estaban visibles
                            ChequearProve sProve, False
                            
                            .RemoveItem (i)
                        End If
                        
                    End If
                    
                    If bQuitar Then
                        g_oVistaSeleccionada.ConfVistasItemProv.Item(sProve).Visible = TipoProvVisible.NoVisible
                    End If
                    
                    i = i - 1
                    
                Wend
                .MoveFirst
            End With
        End If
    Else  'Visualiza las ofertas no adjudicables
        If Not m_bOcultarofertas Then
            g_oVistaSeleccionada.OcultarNoAdj = False
            
            If m_bCargandoVista = True Then Exit Sub
            
            'chequea los proveedores sin ofertas para hacerlos visibles
            For j = 0 To sdbgOcultarProv.Rows - 1
                bQuitar = False
                vbm = sdbgOcultarProv.AddItemBookmark(j)
                If Not m_oProcesoSeleccionado.Ofertas.Item(sdbgOcultarProv.Columns("COD").CellValue(vbm)) Is Nothing Then
                    If Not g_oOrigen.m_oEstOfes.Item(m_oProcesoSeleccionado.Ofertas.Item(sdbgOcultarProv.Columns("COD").CellValue(vbm)).CodEst).adjudicable Then
                        bQuitar = True
                    Else
                        If g_oVistaSeleccionada.ConfVistasItemProv.Item(sdbgOcultarProv.Columns("COD").CellValue(vbm)).Visible <> NoVisible Then
                            bQuitar = True
                        Else
                            bQuitar = False
                            
                        End If
                    End If
                End If
                If bQuitar = True Then
                    sdbgOcultarProv.Bookmark = vbm
                    sdbgOcultarProv.Columns("INC").Value = "1"
                    sdbgOcultarProv.Update
                End If
                
                'Si tiene ofertas 0 la check no est� marcada el prov.estar� visible:
                If Not (g_oVistaSeleccionada.OcultarProvSinOfe = True And m_oProcesoSeleccionado.Ofertas.Item(sdbgOcultarProv.Columns("COD").CellValue(vbm)) Is Nothing) Then
                    If bQuitar Then
                        g_oVistaSeleccionada.ConfVistasItemProv.Item(sdbgOcultarProv.Columns("COD").CellValue(vbm)).Visible = TipoProvVisible.Visible
                    End If
                End If
                
            Next j
            
            'A�ade a la grid los proveedores con ofertas no adjudicables
            If g_ogrupo.UsarEscalados Then
                CargarGridEscAdjudicaciones sdbgAdjEsc.Groups(1).Columns.Count
            Else
                CargarGridAdjudicaciones
            End If
        Else
            m_bOcultarofertas = False
        End If
    End If

    If m_bCargandoVista = False Then
        g_oVistaSeleccionada.HayCambios = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "chkOcultarNoAdj_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Oculta o desoculta los proveedores de la vista que no tienen ofertas
''' </summary>
'''
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en check de la vista; Tiempo m�ximo: 0</remarks>

Private Sub chkOcultarProvSinOfe_Click()
    Dim i As Integer
    Dim vbm As Variant
    Dim bOcultarFila As Boolean
    Dim j As Integer
    Dim sProve As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bCargandoVista = True Then Exit Sub
    
    'Oculta los proveedores sin ofertas
    If chkOcultarProvSinOfe.Value = vbChecked Then
        g_oVistaSeleccionada.OcultarProvSinOfe = True
             
        If g_ogrupo.UsarEscalados Then
            With sdbgAdjEsc
                i = .Rows - 1
                While i >= 0
                    bOcultarFila = False
                    vbm = .AddItemBookmark(i)
                    If .Columns("PROV").CellValue(vbm) = "1" Then
                        sProve = .Columns("ID").CellValue(vbm)
                        If g_ogrupo.UltimasOfertas.Item(Trim(sProve)) Is Nothing Then
                            'Deschequea los proveedores sin ofertas que estaban visibles
                            bOcultarFila = True
                            
                            ChequearProve sProve, False
            
                            .RemoveItem (i)
                        End If
                        'If g_oVistaSeleccionada.ConfVistasItemProv.Item(sProve).Visible <> TipoProvVisible.NoVisible Then
                            If bOcultarFila Then
                                g_oVistaSeleccionada.ConfVistasItemProv.Item(sProve).Visible = TipoProvVisible.NoVisible
                            End If
                        'End If
                    End If
                    
                    i = i - 1
                Wend
                .MoveFirst
            End With
        Else
            With sdbgOcultarProv
                i = .Rows - 1
                While i >= 0
                    bOcultarFila = False
                    vbm = .AddItemBookmark(i)
                    sProve = .Columns("COD").CellValue(vbm)
                    If g_ogrupo.UltimasOfertas.Item(Trim(.Columns("COD").CellValue(vbm))) Is Nothing Then
                        'Deschequea los proveedores sin ofertas que estaban visibles
                        bOcultarFila = True
                                                      
                        'Buscar el proveedor en sdbgAdj
                        Dim k As Integer
                        Dim vBmk As Variant
                        For k = 0 To sdbgAdj.Rows - 1
                            vBmk = sdbgAdj.AddItemBookmark(k)
                            If sdbgAdj.Columns("PROVECOD").CellValue(vBmk) = sProve Then sdbgAdj.RemoveItem (k)
                        Next
                    End If
                    
                    ChequearProve sProve, Not bOcultarFila
                    If g_oVistaSeleccionada.Vista <> TipoDeVistaDefecto.vistainicial Then
                        g_oVistaSeleccionada.ConfVistasItemProv.Item(sProve).Visible = TipoProvVisible.DepenVista
                    End If
                    
                    i = i - 1
                Wend
                .MoveFirst
            End With
        End If
        
    Else   'Hace visibles los proveedores sin ofertas
        Dim bHacerVisible
        If Not m_bOcultarprove Then
        
            If m_bCargandoVista = True Then Exit Sub
            
            g_oVistaSeleccionada.OcultarProvSinOfe = False
                        
            'chequea los proveedores sin ofertas para hacerlos visibles
            For j = 0 To sdbgOcultarProv.Rows - 1
                bHacerVisible = False
                vbm = sdbgOcultarProv.AddItemBookmark(j)
                If g_ogrupo.UltimasOfertas.Item(Trim(sdbgOcultarProv.Columns("COD").CellValue(vbm))) Is Nothing Then
                    sdbgOcultarProv.Bookmark = vbm
                    sdbgOcultarProv.Columns("INC").Value = "1"
                    sdbgOcultarProv.Update
                    bHacerVisible = True
                End If
                
                'Si la oferta del proveedor no es no adjudicable y la check no est� marcada
                If g_oVistaSeleccionada.OcultarNoAdj Then
                    If Not m_oProcesoSeleccionado.Ofertas Is Nothing Then
                        If Not m_oProcesoSeleccionado.Ofertas.Item(sdbgOcultarProv.Columns("COD").CellValue(vbm)) Is Nothing Then
                            If g_oOrigen.m_oEstOfes.Item(m_oProcesoSeleccionado.Ofertas.Item(sdbgOcultarProv.Columns("COD").CellValue(vbm)).CodEst).adjudicable = True Then
                                If bHacerVisible Then
                                    g_oVistaSeleccionada.ConfVistasItemProv.Item(CStr(sdbgOcultarProv.Columns("COD").CellValue(vbm))).Visible = TipoProvVisible.Visible
                                End If
                            End If
                        Else
                            If bHacerVisible Then
                                g_oVistaSeleccionada.ConfVistasItemProv.Item(CStr(sdbgOcultarProv.Columns("COD").CellValue(vbm))).Visible = TipoProvVisible.Visible
                            End If
                        End If
                    End If
                Else
                    g_oVistaSeleccionada.ConfVistasItemProv.Item(CStr(sdbgOcultarProv.Columns("COD").CellValue(vbm))).Visible = TipoProvVisible.Visible
                End If
            Next j
            
            If g_ogrupo.UsarEscalados Then
                CargarGridEscAdjudicaciones sdbgAdjEsc.Groups(1).Columns.Count
            Else
                CargarGridAdjudicaciones
            End If
        'RedimensionarProves
        Else
            m_bOcultarprove = False
        End If
    End If

    If m_bCargandoVista = False Then
        g_oVistaSeleccionada.HayCambios = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "chkOcultarProvSinOfe_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>Chequea o deschequea un proveedor en el grid de proveedores</summary>
''' <param name="sCod">Cod. proveedor</param>
''' <param name="bCheck">Indica si el proveedor tiene que estar chequeado</param>
''' <remarks>Llamada desde: chkOcultarProvSinOfe_Click; Tiempo m�ximo: 0</remarks>

Public Sub ChequearProve(ByVal sCod As String, ByVal bCheck As Boolean)
    Dim j As Integer
    Dim vbm As Variant
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgOcultarProv
        For j = 0 To .Rows - 1
            If .Columns("COD").CellValue(j) = sCod Then
                vbm = .AddItemBookmark(j)
                .Bookmark = vbm
                If bCheck Then
                    .Columns("INC").Value = -1
                Else
                    .Columns("INC").Value = 0
                End If
                .Update
                Exit For
            End If
        Next j
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ChequearProve", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub cmdAbajo_Click()
    Dim temp As Variant
    Dim temp2 As Variant
    Dim i As Integer
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgOptima.Row < sdbgOptima.Rows - 1 Then
        
        For i = 0 To sdbgOptima.Columns.Count - 1
            temp = sdbgOptima.Columns(i).CellValue(sdbgOptima.RowBookmark(sdbgOptima.Row + 1))
            temp2 = sdbgOptima.Columns(i).CellValue(sdbgOptima.RowBookmark(sdbgOptima.Row))
            sdbgOptima.Columns(i).Value = temp
            sdbgOptima.MoveNext
            sdbgOptima.Columns(i).Value = temp2
            sdbgOptima.MovePrevious
        Next i
        
        sdbgOptima.MoveNext
        sdbgOptima.Update
        sdbgOptima.DoClick
        DoEvents
    
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdAbajo_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdAdjudicar_Click()
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    
    Dim bCierreTotalGr As Boolean
    Dim bCierreTotal As Boolean
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    
    Dim oAtribsEspec As CAtributosEspecificacion
    Dim oAtribEspec As CAtributoEspecificacion
    Dim sAtributosObligatorios As String
    Dim oAtributosObligatorios As CAtributosEspecificacion
    Dim sValidarOblSolCompraAdj As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oAtributosObligatorios = oFSGSRaiz.Generar_CAtributosEspecificacion
    
    'Comprobamos que no han llegado ofertas nuevas
    If m_oProcesoSeleccionado.Ofertas.ComprobarUltimasOfertas Then
        irespuesta = oMensajes.PreguntaContinuarAdjudicacionSinOfertasNuevas
        If irespuesta = vbNo Then
           Exit Sub
        End If
    End If

    'Ahora guardo el campo USAR_PREC y el orden para los atributos
    If Not m_oAtribsFormulas Is Nothing Then
        teserror = m_oAtribsFormulas.ModificarUsarPrec
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
    End If

    'comprobamos que si est� configurado como proceso de un solo pedido, todo est� adjudicado a un mismo proveedor
    If m_oProcesoSeleccionado.DefUnSoloPedido Then
        If Not ComprobarAdjudicadoUnSoloProveedor() Then
            oMensajes.ImposibleAdjudicarVarios
            Exit Sub
        End If
    End If
    'Tarea 3425 - GFA - Validaci�n para saber si tiene solicitud introducida antes de validar la adjudicaci�n
    If m_oProcesoSeleccionado.oblSolCompra = 2 Then
        If Not m_oProcesoSeleccionado.ValidarOblSolCompraAdj(sdbgAdj, sValidarOblSolCompraAdj, m_oProcesoSeleccionado, g_ogrupo.Den, True, g_oItemSeleccionado) Then
            sValidarOblSolCompraAdj = m_sValidarOblSolCompraAdj_Inicial & Chr(13) & Chr(10) & Chr(13) & Chr(10) & _
                                      sValidarOblSolCompraAdj & Chr(13) & Chr(10) & Chr(13) & Chr(10) & _
                                      m_sValidarOblSolCompraAdj_Final
            Call oMensajes.ValidarOblSolCompraAdj(sValidarOblSolCompraAdj)
            Exit Sub
        End If
    End If
    'comprueba si se va a realizar un cierre parcial o si va a ser total de grupo
    bCierreTotal = True
    bCierreTotalGr = True
    For Each oItem In g_ogrupo.Items
        If oItem.Cerrado = False Then
            If oItem.Id <> g_oItemSeleccionado.Id Then
                bCierreTotalGr = False
                bCierreTotal = False
                Exit For
            End If
        End If
    Next
    'Si se va a cerrar el grupo comprueba si es o no un cierre total del proceso
    If bCierreTotalGr = True Then
        For Each oGrupo In m_oProcesoSeleccionado.Grupos
            If oGrupo.Codigo <> g_ogrupo.Codigo Then
                For Each oItem In oGrupo.Items
                    If oItem.Cerrado = False Then
                        bCierreTotal = False
                        Exit For
                    End If
                Next
            End If
        Next
    
    End If

    'si es cierre total validamos los atributos obligatorios del proceso
    If bCierreTotal Then
        Set oAtribsEspec = m_oProcesoSeleccionado.ValidarAtributosEspObligatorios(TValidacionAtrib.TVAdjudicacion)
        If oAtribsEspec.Count > 0 Then
            For Each oAtribEspec In oAtribsEspec
                If Not oAtributosObligatorios.existe(oAtribEspec.Atrib) Then
                    oAtributosObligatorios.addAtributoEsp oAtribEspec
                End If
            Next
        End If
    Else
        'En caso contrario solo validamos los del item seleccionado
        Set oAtribsEspec = m_oProcesoSeleccionado.ValidarAtributosEspObligatoriosItem(TValidacionAtrib.TVAdjudicacion, g_oItemSeleccionado.Id)
        For Each oAtribEspec In oAtribsEspec
            If Not oAtributosObligatorios.existe(oAtribEspec.Atrib) Then
                oAtributosObligatorios.addAtributoEsp oAtribEspec
            End If
        Next
    End If
    
    For Each oAtribEspec In oAtributosObligatorios
        sAtributosObligatorios = sAtributosObligatorios & oAtribEspec.Den & vbCrLf
    Next
    
    Set oAtribsEspec = Nothing
    Set oAtribEspec = Nothing
    Set oAtributosObligatorios = Nothing
    
    If sAtributosObligatorios <> "" Then
        Call oMensajes.AtributosEspObl(sAtributosObligatorios, TVAdjudicacion)
        Exit Sub
    End If
    'Carga la colecci�n con los items a cerrar parcialmente y la pasa al form. de cierre parcial
    If Not PreadjudicarItem(bCierreTotalGr) Then
        Exit Sub
    End If
    
    Set g_oOrigen.m_oAdjParcsGrupo = m_oAdjParcsGrupo

    ValidarParcial bCierreTotal
    
    Exit Sub
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdAdjudicar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>
''' Tras elegir en el grid de "AplicarPrecio" lo q se desea q se aplique (precio y/o atributos) se aplica pulsando
''' este bot�n. Se muestra en pantalla la nueva adjudicaci�n.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub cmdAplicarPrecio_Click()
    Dim i As Integer
    Dim oGrupo As CGrupo
    Dim vbm As Variant
    
    'Si est� parcialmente cerrado no se pueden habilitar-deshbilitar atributos de item (total y/o unitario)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado And g_oItemSeleccionado.Cerrado Then
        oMensajes.ImposibleModificarAtributoAplic
        Exit Sub
    End If
    
    'Si no hay atributos de f�rmulas no hace nada
    If m_oAtribsFormulas Is Nothing Then Exit Sub
    If m_oAtribsFormulas.Count = 0 Then Exit Sub

    'Ahora modifica el campo USAR_PREC en las colecciones de atributos
    sdbgAplicarPrecio.MoveFirst
    For i = 0 To sdbgAplicarPrecio.Rows - 1
        vbm = sdbgAplicarPrecio.AddItemBookmark(i)

        m_oAtribsFormulas.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)).UsarPrec = BooleanToSQLBinary(GridCheckToBoolean(sdbgAplicarPrecio.Columns("INC").CellValue(vbm)))

        If Not m_oAtribsFormulas.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)).AtribTotalGrupo Is Nothing Then
            If Not m_oAtribsFormulas.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)).AtribTotalGrupo.Item(sdbgAplicarPrecio.Columns("GR").CellValue(vbm)) Is Nothing Then
                 m_oAtribsFormulas.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)).AtribTotalGrupo.Item(sdbgAplicarPrecio.Columns("GR").CellValue(vbm)).UsarPrec = BooleanToSQLBinary(GridCheckToBoolean(sdbgAplicarPrecio.Columns("INC").CellValue(vbm)))
            End If
        End If

        If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
            If Not m_oProcesoSeleccionado.ATRIBUTOS.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)) Is Nothing Then
                m_oProcesoSeleccionado.ATRIBUTOS.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)).UsarPrec = BooleanToSQLBinary(GridCheckToBoolean(sdbgAplicarPrecio.Columns("INC").CellValue(vbm)))
            End If
        End If

        If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
            If Not m_oProcesoSeleccionado.AtributosGrupo.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)) Is Nothing Then
                m_oProcesoSeleccionado.AtributosGrupo.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)).UsarPrec = BooleanToSQLBinary(GridCheckToBoolean(sdbgAplicarPrecio.Columns("INC").CellValue(vbm)))
            End If
        End If

        If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
            If Not m_oProcesoSeleccionado.AtributosItem.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)) Is Nothing Then
                m_oProcesoSeleccionado.AtributosItem.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)).UsarPrec = BooleanToSQLBinary(GridCheckToBoolean(sdbgAplicarPrecio.Columns("INC").CellValue(vbm)))
            End If
        End If

        For Each oGrupo In m_oProcesoSeleccionado.Grupos
            If Not oGrupo.AtributosItem Is Nothing Then
                If Not oGrupo.AtributosItem.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)) Is Nothing Then
                    oGrupo.AtributosItem.Item(sdbgAplicarPrecio.Columns("ID").CellValue(vbm)).UsarPrec = BooleanToSQLBinary(GridCheckToBoolean(sdbgAplicarPrecio.Columns("INC").CellValue(vbm)))
                End If
            End If
        Next
    Next

    'Aplica la f�rmulas y recalcula las colecciones para todos los items
    CalcularPrecioConFormulas
    AplicarAtributosTotalItem
    AplicarAtributosTotalGrupo
    CalcularTotalesDeProceso

    CargarGridAdjudicaciones
    
    If m_oProcesoSeleccionado.Bloqueado Then
        If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
            If g_oItemSeleccionado.Cerrado = False Then
                cmdGuardar.Enabled = True
                m_oProcesoSeleccionado.GuardarProceso = True
                m_bRecargarOrigen = True
            End If
        ElseIf m_oProcesoSeleccionado.Estado < conadjudicaciones Then
            cmdGuardar.Enabled = True
            m_oProcesoSeleccionado.GuardarProceso = True
            m_bRecargarOrigen = True
        End If
    End If
    If m_oProcesoSeleccionado.Estado >= conadjudicaciones Then
        m_oProcesoSeleccionado.ModificadoHojaAdj = True
    End If
    
    picControlPrecios.Visible = False
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdAplicarPrecio_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdArriba_Click()
    Dim temp As Variant
    Dim temp2 As Variant
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgOptima.Row > 0 Then
        For i = 0 To sdbgOptima.Columns.Count - 1
            temp = sdbgOptima.Columns(i).CellValue(sdbgOptima.RowBookmark(sdbgOptima.Row - 1))
            temp2 = sdbgOptima.Columns(i).CellValue(sdbgOptima.RowBookmark(sdbgOptima.Row))
            sdbgOptima.Columns(i).Value = temp
            sdbgOptima.MovePrevious
            sdbgOptima.Columns(i).Value = temp2
            sdbgOptima.MoveNext
        Next i
        
        sdbgOptima.MovePrevious
        sdbgOptima.Update
        sdbgOptima.DoClick
        DoEvents
        
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdArriba_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Tras elegir en el grid de "Optima" lo q se desea q se aplique (precio y/o atributos) se calcula la
''' adjudicaci�n optima y la aplica en pantalla.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub cmdCalcularOptima_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
        If g_oItemSeleccionado.Cerrado = True Then
            oMensajes.ImposibleModItem
            Exit Sub
        End If
    End If
'    CalcularOptima
    
    If m_oProcesoSeleccionado.Bloqueado Then
        If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
            If g_oItemSeleccionado.Cerrado = False Then
                cmdGuardar.Enabled = True
                m_oProcesoSeleccionado.GuardarProceso = True
                m_bRecargarOrigen = True
            End If
        ElseIf m_oProcesoSeleccionado.Estado < conadjudicaciones Then
            cmdGuardar.Enabled = True
            m_oProcesoSeleccionado.GuardarProceso = True
            m_bRecargarOrigen = True
        End If
    End If
    If m_oProcesoSeleccionado.Estado >= conadjudicaciones Then
        m_oProcesoSeleccionado.ModificadoHojaAdj = True
    End If
    
    picControlOptima.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdCalcularOptima_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdDetalle_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    MostrarDetalleItem
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdDetalle_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub cmdEliminarVista_Click()
 Dim irespuesta As Integer
 Dim oIBaseDatos As IBaseDatos
 Dim teserror As TipoErrorSummit
 Dim iVistaVieja As Integer
 Dim udtTipoVistaVieja As TipoDeVistaDefecto
 Dim sUsuarioVistaVieja As String
 Dim sCod As String
 Dim bDefecto As Boolean
 
  
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
 irespuesta = oMensajes.PreguntaEliminar(sdbcVistaActual.Text)
 
 iVistaVieja = g_oOrigen.m_oVistaSeleccionada.Vista
 udtTipoVistaVieja = g_oOrigen.m_oVistaSeleccionada.TipoVista
 sUsuarioVistaVieja = g_oOrigen.m_oVistaSeleccionada.UsuarioVista
 
 If irespuesta = vbYes Then
    Set oIBaseDatos = g_oOrigen.m_oVistaSeleccionada
    teserror = oIBaseDatos.EliminarDeBaseDatos
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        oIBaseDatos.CancelarEdicion
        Exit Sub
    Else
        sCod = g_oOrigen.m_oVistaSeleccionada.UsuarioVista & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUSU - Len(g_oOrigen.m_oVistaSeleccionada.UsuarioVista))
        g_oOrigen.EliminarVistaDeColeccion CStr(iVistaVieja & udtTipoVistaVieja & sCod)
        g_ogrupo.ConfVistasItem.Remove CStr(iVistaVieja & udtTipoVistaVieja & sCod)
    End If
    

    g_oOrigen.VistaDespuesDeEliminar iVistaVieja, udtTipoVistaVieja, sUsuarioVistaVieja
    
          
    If CStr(iVistaVieja & udtTipoVistaVieja & sUsuarioVistaVieja) = CStr(g_oOrigen.m_oProcesoSeleccionado.VistaDefectoComp & g_oOrigen.m_oProcesoSeleccionado.VistaDefectoTipo & m_oProcesoSeleccionado.UsuarioVistaDefecto) Then
        g_oOrigen.m_oProcesoSeleccionado.VistaDefectoComp = g_oOrigen.m_oVistaSeleccionada.Vista
        g_oOrigen.m_oProcesoSeleccionado.VistaDefectoTipo = g_oOrigen.m_oVistaSeleccionada.TipoVista
        g_oOrigen.m_oProcesoSeleccionado.NombreVistaDefecto = g_oOrigen.m_oVistaSeleccionada.nombre
        g_oOrigen.m_oProcesoSeleccionado.UsuarioVistaDefecto = g_oOrigen.m_oVistaSeleccionada.UsuarioVista
        
        teserror = g_oOrigen.m_oProcesoSeleccionado.ModificarVistaDefectoProceso(oUsuarioSummit.Cod)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
        
        bDefecto = True
    End If
    
    ConfiguracionVistaActual (g_oOrigen.m_oVistaSeleccionada.Vista), (g_oOrigen.m_oVistaSeleccionada.TipoVista), g_oOrigen.m_oVistaSeleccionada.UsuarioVista

    'Carga los campos y proveedores a ocultar en las grids con la configuraci�n de la vista actual
    CargarVista

    'Redimensiona la grid
    If g_ogrupo.UsarEscalados Then
        RedimensionarGridEsc
    Else
        RedimensionarGrid
    End If
    'Se quitan los proves que no se han de ver
    RedimensionarProves

    MostrarVistaEnCombos bDefecto
        
    ConfigurarBotonesVistas TipoDeVistaDefecto.VistaDeUsuario
    
    Dim udtAmbito As TipoAmbitoProceso
    
    If g_oOrigen.sstabComparativa.Tag = "General" Then
        g_oOrigen.CargarVistaActualProce
        udtAmbito = AmbProceso
    ElseIf g_oOrigen.sstabComparativa.Tag = "ALL" Then
        g_oOrigen.AllSeleccionado
        udtAmbito = AmbItem
    Else
        g_oOrigen.GrupoSeleccionado
        udtAmbito = AmbGrupo
    End If
     
     g_oOrigen.PonerCaptionGrid
    
     g_oOrigen.MostrarVistaEnCombos udtAmbito, bDefecto

 End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdEliminarVista_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
 
End Sub

''' <summary>
''' Guarda la adjudicaci�n
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdGuardar_Click()
    Dim irespuesta As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bCambianTodasAdjs Then
        irespuesta = oMensajes.PreguntaGuardarAdjsDeProcesoCompleto
        If irespuesta = vbNo Then
            Exit Sub
        End If
    End If

    g_oOrigen.m_bCtrlIgnorar_FrmAdjItem = True
    g_oOrigen.m_dCtrlIgnorar_FrmAdjItem_Proc = g_dblAbiertoProc
    g_oOrigen.m_dCtrlIgnorar_FrmAdjItem_Adj = g_dblAdjudicadoProc
    g_oOrigen.cmdGuardar_Click
    g_oOrigen.m_bCtrlIgnorar_FrmAdjItem = False
    
    cmdGuardar.Enabled = m_oProcesoSeleccionado.GuardarProceso
    
    If cmdGuardar.Enabled = False Then
        'm_bRecargarOrigen = False
        m_bCambianTodasAdjs = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdGuardar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdGuardarVista_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oOrigen.m_oVistaSeleccionada Is Nothing Then
        g_oOrigen.GuardarVistaGeneral
    End If
    If Not g_oOrigen.m_oVistaSeleccionadaAll Is Nothing Then
        g_oOrigen.GuardarVistaAll
    End If
    If Not g_oOrigen.m_oVistaSeleccionadaGr Is Nothing Then
        g_oOrigen.GuardarVistaGr
    End If
    If Not g_oVistaSeleccionada Is Nothing Then
        GuardarVistaItem
    End If

    g_oOrigen.GuardarVistas False
    oMensajes.MensajeOKOnly 821, TipoIconoMensaje.Information
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdGuardarVista_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub cmdGuardarVistaNueva_Click()
    
    Dim teserror As TipoErrorSummit
    Dim iVistaVieja As Integer
    Dim udtTipoVistaVieja As TipoDeVistaDefecto
    Dim sCod As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    GuardarVistaItem
    
    frmADJGuardarVista.g_sOrigen = "frmADJItem"
    Set frmADJGuardarVista.m_ofrmADJItem = Me
    frmADJGuardarVista.caption = m_sGuarNue
    frmADJGuardarVista.Show 1
    
    iVistaVieja = g_oOrigen.m_oVistaSeleccionada.Vista
    udtTipoVistaVieja = g_oOrigen.m_oVistaSeleccionada.TipoVista
    sCod = g_oOrigen.m_oVistaSeleccionada.UsuarioVista & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUSU - Len(g_oOrigen.m_oVistaSeleccionada.UsuarioVista))
    
    If frmADJItem.g_sNombreVista <> "" Then
        If g_oOrigen.sstabComparativa.selectedItem.Tag = "General" Then
            g_oOrigen.GuardarVistaGeneral
        ElseIf g_oOrigen.sstabComparativa.selectedItem.Tag = "ALL" Then
            g_oOrigen.GuardarVistaAll
        Else
            g_oOrigen.GuardarVistaGr
        End If
        
        teserror = g_oOrigen.m_oVistaSeleccionada.GuardarVistaNuevaBd(g_sNombreVista, basOptimizacion.gvarCodUsuario, g_oOrigen.m_oVistaSeleccionadaAll)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        Else
            g_oOrigen.EliminarVistaDeColeccion CStr(iVistaVieja & udtTipoVistaVieja & sCod)
            g_ogrupo.ConfVistasItem.Remove CStr(iVistaVieja & udtTipoVistaVieja & sCod)
                    
            oMensajes.MensajeOKOnly 821, TipoIconoMensaje.Information
        End If
                
        ConfiguracionVistaActual teserror.Arg1, VistaDeUsuario, basOptimizacion.gvarCodUsuario
        
        Dim udtAmbito As TipoAmbitoProceso
        
        If g_oOrigen.sstabComparativa.Tag = "General" Then
           udtAmbito = AmbProceso
           g_oOrigen.ConfiguracionVistaActual teserror.Arg1, VistaDeUsuario, basOptimizacion.gvarCodUsuario
        ElseIf g_oOrigen.sstabComparativa.Tag = "ALL" Then
            udtAmbito = AmbItem
            g_oOrigen.ConfiguracionVistaActualAll teserror.Arg1, VistaDeUsuario, basOptimizacion.gvarCodUsuario
        Else
            udtAmbito = AmbGrupo
            g_oOrigen.ConfiguracionVistaActualGr teserror.Arg1, VistaDeUsuario, basOptimizacion.gvarCodUsuario
        End If
        g_oOrigen.PonerCaptionGrid
        
        sdbcVistaActual.Text = m_sVistaUsu & ": " & g_sNombreVista
        sdbcVistaActual.Refresh
        sdbgAdj.caption = m_sVistaUsu & ": " & g_sNombreVista
        sdbgAdjEsc.caption = m_sVistaUsu & ": " & g_sNombreVista
        ConfigurarBotonesVistas TipoDeVistaDefecto.VistaDeUsuario
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdGuardarVistaNueva_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
Private Sub cmdHojaComp_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If gParametrosInstalacion.gsComparativaItem = "" Then
        gParametrosInstalacion.gsComparativaItem = gParametrosGenerales.gsCOMPARATIVAITEMDOT
        g_GuardarParametrosIns = True
    End If
    Dim oEmpresas As CEmpresas
    Dim sCarpeta As String
    Dim sMarcaPlantillas As String
    If InStr(gParametrosInstalacion.gsComparativaItem, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
        'Buscamos la empresa CARPETA_PLANTILLAS
        Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
        sCarpeta = oEmpresas.DevolverCarpetaPlantillaProce_o_Ped(g_oItemSeleccionado.proceso.Anyo, g_oItemSeleccionado.proceso.GMN1Cod, g_oItemSeleccionado.proceso.Cod, 0)
        Set oEmpresas = Nothing
    End If
    If sCarpeta = "" And gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    Set frmRESREUHojaAdj.g_oOrigen = Me
    frmRESREUHojaAdj.caption = m_sIdiObtenerHojaComp
    frmRESREUHojaAdj.sOrigen = "ComparativaItem"
    frmRESREUHojaAdj.txtDetProceDot = Replace(gParametrosInstalacion.gsComparativaItem, sMarcaPlantillas, sCarpeta)
    frmRESREUHojaAdj.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdHojaComp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdMostrarGrafico_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    MDI.mnuPopUpMostrarGrafico(5).Visible = True
    If m_oProcesoSeleccionado.UsarPonderacion Then
        MDI.mnuPopUpMostrarGrafico(6).Visible = True
    Else
        MDI.mnuPopUpMostrarGrafico(6).Visible = False
    End If
    MDI.mnuPopUpMostrarGrafico(7).Visible = True
    MDI.mnuPopUpMostrarGrafico(0).Visible = False
    MDI.mnuPopUpMostrarGrafico(1).Visible = False
    MDI.mnuPopUpMostrarGrafico(2).Visible = False
    MDI.mnuPopUpMostrarGrafico(3).Visible = False
    MDI.mnuPopUpMostrarGrafico(4).Visible = False
    MDI.mnuPopUpMostrarGrafico(8).Visible = (g_ogrupo.UsarEscalados = 1)

    Set MDI.g_ofrmOrigenGraficos = Me
    PopupMenu MDI.POPUPMostrarGrafico, , cmdMostrarGrafico.Left + 50, cmdMostrarGrafico.Top + 250
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdMostrarGrafico_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdOptima_Click()
Dim i As Integer
Dim bm As Variant
Dim bHaySelec As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    'Al entrar si no hay nada seleccionado se chequea por defecto Precio/Importes.
    For i = 0 To sdbgOptima.Rows - 1
        bm = sdbgOptima.GetBookmark(i)
        If sdbgOptima.Columns("INC").CellValue(bm) = -1 Then
            bHaySelec = True
            Exit For
        End If
    Next i

    If Not bHaySelec Then
        sdbgOptima.MoveFirst
        For i = 0 To sdbgOptima.Rows - 1
            If sdbgOptima.Columns("COD").Value = m_sPrecioImporte Then
                sdbgOptima.Columns("INC").Value = -1
                sdbgOptima.Update
                sdbgOptima.DoClick
                Exit For
            End If
            sdbgOptima.MoveNext
            i = i + 1
        Next
    End If
    
    Set MDI.g_ofrmOrigenResponsable = Me
    MDI.mnuOptima(0).Visible = False
    MDI.mnuOptima(1).Visible = False
    PopupMenu MDI.mnuPOPUPOptima
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdOptima_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub cmdOrdernarAtrib_Click()
    'Llama al formulario que muestra el orden de los atributos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmOrdenAtributos.g_sOrigen = "frmADJItem"
    Set frmOrdenAtributos.g_oOrigen = Me
    Set frmOrdenAtributos.g_oProceso = m_oProcesoSeleccionado
    frmOrdenAtributos.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdOrdernarAtrib_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
''' <summary>
''' Calcular Precio Con Formulas
''' </summary>
''' <remarks>Llamada desde: sdbgAdj_AfterColUpdate  CalcularOptimaItem      ModificarOrdenAtributos     cmdAplicarPrecio_Click; Tiempo m�ximo: 0,2</remarks>
Private Sub CalcularPrecioConFormulas()
    Dim oProve As CProveedor
    Dim oAsig As COferta
    Dim dPrecioAplicFormula As Variant
    Dim oatrib As CAtributo
    Dim dValorAtrib As Double
    Dim scod1 As String
    Dim iOrden As Integer
    Dim bSiguiente As Boolean
    Dim lngMinimo As Long
    Dim lAtribAnterior As Long
    Dim bCargar As Boolean
    Dim scodProve As String

    'Calcula el precio v�lido seg�n las f�rmulas a aplicar de atributos de prec. unitario
        
        'Ahora empieza el c�lculo del precio
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        For Each oProve In m_oProvesAsig
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(g_oItemSeleccionado.grupoCod) Is Nothing Then
                    bCargar = False
                End If
            End If
            If bCargar Then
            
                dPrecioAplicFormula = 0
                
                If Not g_ogrupo.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                    Set oAsig = g_ogrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                    
                    If Not oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)) Is Nothing Then
                        'Comprueba si se va a usar el precio 1,2 o 3
                        Select Case oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Usar
                            Case 1
                                dPrecioAplicFormula = oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Precio
                            Case 2
                                dPrecioAplicFormula = oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Precio2
                            Case 3
                                dPrecioAplicFormula = oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Precio3
                        End Select
                    
                    
                        '**** APLICA LOS ATRIBUTOS A APLICAR AL PRECIO UNITARIO DEL ITEM ****
                        lAtribAnterior = 0
                        iOrden = 1
                        bSiguiente = False
                        While bSiguiente = False
                            lngMinimo = 0
                            For Each oatrib In m_oAtribsFormulas
                                'si el atributo aplica la f�rmula al total del item
                                If oatrib.PrecioAplicarA = 3 And oatrib.UsarPrec = 1 Then
                                    If oatrib.Orden = iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                        lngMinimo = oatrib.idAtribProce
                                        Exit For
                                    Else
                                        If oatrib.Orden > iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                            If lngMinimo = 0 Or (oatrib.Orden < m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden) Then
                                                lngMinimo = oatrib.idAtribProce
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                            
                            If (lngMinimo = 0) Then
                                bSiguiente = True
                            ElseIf (m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then
                                bSiguiente = True
                            Else
                                dValorAtrib = 0
                                Set oatrib = m_oAtribsFormulas.Item(CStr(lngMinimo))
                                lAtribAnterior = oatrib.idAtribProce
                                iOrden = oatrib.Orden
                                
                                'si el atributo aplica la f�rmula al precio unitario del item
                                Select Case oatrib.ambito
                                    Case 1  'Proceso
                                        'Comprueba los atributos a aplicar al precio unitario de �mbito proceso
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribProcOfertados Is Nothing Then
                                            If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                                dValorAtrib = NullToDbl0(m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                                            End If
                                        End If
                                            
                                    Case 2 'Grupo
                                        'Comprueba los atributos a aplicar al precio unitario de �mbito grupo
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribGrOfertados Is Nothing Then
                                            If g_ogrupo.Codigo = oatrib.codgrupo Or IsNull(oatrib.codgrupo) Then
                                                scod1 = g_ogrupo.Codigo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(g_ogrupo.Codigo))
                                                scod1 = scod1 & CStr(oatrib.idAtribProce)
                                                If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(scod1) Is Nothing Then
                                                    dValorAtrib = NullToDbl0(m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(scod1).valorNum)
                                                End If
                                            End If
                                        End If
                
                                    Case 3  'Item
                                        If Not oAsig.AtribItemOfertados Is Nothing Then
                                            If Not oAsig.AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                                                dValorAtrib = NullToDbl0(oAsig.AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(oatrib.idAtribProce)).valorNum)
                                            End If
                                        End If
                                End Select
                                If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                    Select Case oatrib.PrecioFormula
                                        Case "+"
                                            dPrecioAplicFormula = dPrecioAplicFormula + dValorAtrib
                                        Case "-"
                                            dPrecioAplicFormula = dPrecioAplicFormula - dValorAtrib
                                        Case "/"
                                            dPrecioAplicFormula = dPrecioAplicFormula / dValorAtrib
                                        Case "*"
                                            dPrecioAplicFormula = dPrecioAplicFormula * dValorAtrib
                                        Case "+%"
                                            dPrecioAplicFormula = dPrecioAplicFormula + (dPrecioAplicFormula * (dValorAtrib / 100))
                                        Case "-%"
                                            dPrecioAplicFormula = dPrecioAplicFormula - (dPrecioAplicFormula * (dValorAtrib / 100))
                                    End Select
                                End If
                                    
                                Set oatrib = Nothing
                                bSiguiente = False
                                iOrden = iOrden + 1
                            End If
                        Wend
                        
                        'Guarda el precio v�lido obtenido en la colecci�n
                        oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta = dPrecioAplicFormula
                    End If
                End If
            End If
        Next oProve
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CalcularPrecioConFormulas", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Aplica los atributos para el total del item</summary>
''' <param name="bOptima"></param>
''' <remarks>Llamada desde: RecalcularCantidades, CalcularOptima, CalcularOptimaItem; Tiempo m�ximo:instantaneo</remarks>
''' <revison>LTG 15/11/2011</revision>
Private Sub AplicarAtributosTotalItem(Optional ByVal bOptima As Boolean)
    Dim oProv As CProveedor
    Dim sCod As String
    Dim scod1 As String
    Dim scod2 As String
    Dim oAsig As COferta
    Dim bSiguiente As Boolean
    Dim dImporteAdj As Double
    Dim dImporteAdjProve As Double
    Dim dImporte As Double
    Dim dImporteProve As Double
    Dim dadj As Double
    Dim dImp As Double
    Dim lngMinimo As Long
    Dim oatrib As CAtributo
    Dim iOrden As Integer
    Dim dValorAtrib As Double
    Dim lAtribAnterior As Long
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim bHayPrecio As Boolean
    Dim oAdjTotProveEsc As CAdjudicacion
    Dim oAdj As CAdjudicacion
    
    'Calcula el importe adj. aplicando las f�rmulas de total del item

    'si el �tem est� cerrado no se aplican lo atributos
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oItemSeleccionado.Cerrado = True Then Exit Sub
    
    dadj = 0
    dImp = 0
    dImporteAdj = 0
    dImporte = 0
    g_oItemSeleccionado.ImporteAdj = 0
    
    For Each oProv In m_oProvesAsig
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
            If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(g_oItemSeleccionado.grupoCod) Is Nothing Then
                bCargar = False
            End If
        End If
        
        If bCargar Then
            dImporteAdjProve = 0
            dImporteProve = 0
        
            sCod = oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
            sCod = CStr(g_oItemSeleccionado.Id) & sCod
    
            Set oAsig = g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod))
            
            If Not oAsig Is Nothing Then
                bHayPrecio = True
                If g_ogrupo.UsarEscalados <> 1 Then bHayPrecio = Not IsNull(oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta)
                
                If bHayPrecio Then
                    If Not g_ogrupo.Adjudicaciones Is Nothing Then
                        If g_ogrupo.UsarEscalados Then
                            If Not g_oAdjsProveEsc.Item(sCod) Is Nothing Then
                                dImporteAdjProve = dImporteAdjProve + NullToDbl0(g_oAdjsProveEsc.Item(sCod).ImporteAdj)
                            End If
                        Else
                            If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                                dImporteAdjProve = dImporteAdjProve + NullToDbl0(oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta) * (g_oItemSeleccionado.Cantidad * g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje / 100)
                            End If
                        End If
                    End If
                    
                    If g_ogrupo.UsarEscalados Then
                        'Importe hipotetico si adjudicas todo el item al proveedor
                        Set oAdjTotProveEsc = CalcularAdjTotalProveedorEsc(oProv.Cod, g_oItemSeleccionado.Id, m_oProcesoSeleccionado, g_ogrupo, oAsig, m_oProvesAsig, m_oAtribsFormulas)
                        If Not oAdjTotProveEsc Is Nothing Then
                            dImporteProve = dImporteProve + oAdjTotProveEsc.ImporteAdj
                        End If
                    Else
                        dImporteProve = dImporteProve + (NullToDbl0(oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta)) * g_oItemSeleccionado.Cantidad
                    End If
                    
                    lAtribAnterior = 0
                    bSiguiente = False
                    iOrden = 1
                    
                    While Not bSiguiente
                        lngMinimo = 0
                        For Each oatrib In m_oAtribsFormulas
                            'si el atributo aplica la f�rmula al total del item
                            If oatrib.codgrupo = g_oItemSeleccionado.grupoCod Or IsNull(oatrib.codgrupo) Then
                                If oatrib.PrecioAplicarA = 2 And oatrib.UsarPrec = 1 And oatrib.FlagAplicar = True Then
                                    If oatrib.Orden = iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                                        lngMinimo = oatrib.idAtribProce
                                        Exit For
                                    Else
                                        If oatrib.Orden > iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                                            If lngMinimo = 0 Then
                                                lngMinimo = oatrib.idAtribProce
                                            ElseIf (oatrib.Orden < m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden) Then
                                                lngMinimo = oatrib.idAtribProce
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        Next
                    
                        If (lngMinimo = 0) Then
                            bSiguiente = True
                        ElseIf (m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then
                            bSiguiente = True
                        Else
                            dValorAtrib = 0
                            Set oatrib = m_oAtribsFormulas.Item(CStr(lngMinimo))
                            lAtribAnterior = oatrib.idAtribProce
                            Select Case oatrib.ambito
                                Case 1
                                    'Proceso
                                    If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod) Is Nothing Then
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados Is Nothing Then
                                            If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                                dValorAtrib = NullToDbl0(m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                                            End If
                                        End If
                                    End If
                        
                                Case 2
                                    'Grupo
                                    If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod) Is Nothing Then
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribGrOfertados Is Nothing Then
                                            scod1 = g_ogrupo.Codigo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(g_ogrupo.Codigo))
                                            scod1 = scod1 & CStr(oatrib.idAtribProce)
                                            If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribGrOfertados.Item(scod1) Is Nothing Then
                                                dValorAtrib = NullToDbl0(m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribGrOfertados.Item(scod1).valorNum)
                                            End If
                                        End If
                                    End If
                        
                                Case 3  'Item
                                    If Not oAsig.AtribItemOfertados Is Nothing Then
                                        If Not oAsig.AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                                            dValorAtrib = NullToDbl0(oAsig.AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(oatrib.idAtribProce)).valorNum)
                                        End If
                                    End If
                            End Select
                            
                            If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                Select Case oatrib.PrecioFormula
                                Case "+"
                                    dImporteAdjProve = dImporteAdjProve + dValorAtrib
                                    dImporteProve = dImporteProve + dValorAtrib
                    
                                Case "-"
                                    dImporteAdjProve = dImporteAdjProve - dValorAtrib
                                    dImporteProve = dImporteProve - dValorAtrib
                                    
                                Case "/"
                                    dImporteAdjProve = dImporteAdjProve / dValorAtrib
                                    dImporteProve = dImporteProve / dValorAtrib
                                    
                                Case "*"
                                    dImporteAdjProve = dImporteAdjProve * dValorAtrib
                                    dImporteProve = dImporteProve * dValorAtrib
                                    
                                Case "+%"
                                    dImporteAdjProve = dImporteAdjProve + (dImporteAdjProve * (dValorAtrib / 100))
                                    dImporteProve = dImporteProve + (dImporteProve * (dValorAtrib / 100))
                                    
                                Case "-%"
                                    dImporteAdjProve = dImporteAdjProve - (dImporteAdjProve * (dValorAtrib / 100))
                                    dImporteProve = dImporteProve - (dImporteProve * (dValorAtrib / 100))
                                End Select
                            End If
                                                
                            Set oatrib = Nothing
                            bSiguiente = False
                            iOrden = iOrden + 1
                    
                        End If
                    Wend
                    
                    If dImporteAdjProve <> 0 Then dImporteAdj = dImporteAdj + (dImporteAdjProve / g_oItemSeleccionado.CambioComparativa(oProv.Cod))
                    If dImporteProve <> 0 Then dImporte = dImporte + (dImporteProve / g_oItemSeleccionado.CambioComparativa(oProv.Cod))
                End If
            End If
    
            'Va recalculando los valores de importe adj. para ese proveedor
            If Not g_ogrupo.Adjudicaciones Is Nothing Then
                If g_ogrupo.UsarEscalados Then
                    Set oAdj = g_oAdjsProveEsc.Item(sCod)
                Else
                    Set oAdj = g_ogrupo.Adjudicaciones.Item(sCod)
                End If
                    
                If Not oAdj Is Nothing Then
                    'Guarda el importe para cada adjudicaci�n sin atributos de total item
                    oAdj.ImporteAdj = ((NullToDbl0(oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta)) * NullToDbl0((g_oItemSeleccionado.Cantidad * oAdj.Porcentaje / 100)))
        
                    If oAdj.Porcentaje > 0 Then
                        dadj = dadj + ((NullToDbl0(oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta)) * NullToDbl0((g_oItemSeleccionado.Cantidad * oAdj.Porcentaje / 100)))
                        dImp = dImp + ((NullToDbl0(oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta)) * NullToDbl0(g_oItemSeleccionado.Cantidad))
                    End If
                    
                    'Resto lo que hay a las adjudicaciones del grupo, si viene de la optima se hace en LimpiarAdjudicaciones
                    scod2 = g_ogrupo.Codigo & oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
                    If Not bOptima Then
                        If Not m_oAdjs.Item(scod2) Is Nothing Then
                            m_oAdjs.Item(scod2).AdjudicadoOfe = m_oAdjs.Item(scod2).AdjudicadoOfe - g_ogrupo.Adjudicaciones.Item(sCod).importe
                            m_oAdjs.Item(scod2).AdjudicadoSin = m_oAdjs.Item(scod2).AdjudicadoSin - g_ogrupo.Adjudicaciones.Item(sCod).ImporteAdjTot
                        End If
                    End If
                    oAdj.ImporteAdjTot = dImporteAdjProve
                    oAdj.importe = dImporteProve
                                
                    'Sumo lo calculado a las adjudicaciones del grupo
                    If Not m_oAdjs.Item(scod2) Is Nothing Then
                        m_oAdjs.Item(scod2).AdjudicadoOfe = m_oAdjs.Item(scod2).AdjudicadoOfe + oAdj.importe
                        m_oAdjs.Item(scod2).AdjudicadoSin = m_oAdjs.Item(scod2).AdjudicadoSin + oAdj.ImporteAdjTot
                    End If
                    
                    g_oItemSeleccionado.ImporteAdj = g_oItemSeleccionado.ImporteAdj + (oAdj.ImporteAdjTot / g_oItemSeleccionado.CambioComparativa(oProv.Cod))
                End If
            End If
        End If
    Next
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "AplicarAtributosTotalItem", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub AplicarAtributosTotalGrupo()
    Dim oProv As CProveedor
    Dim dImporte As Double
    Dim dImporteMonProce As Double
    Dim dAdjudicado As Double
    Dim dAdjudicadoMonProce As Double
    Dim oatrib As CAtributo
    Dim sCod As String
    Dim scod1 As String
    Dim lAtribAnterior As Long
    Dim bSiguiente As Boolean
    Dim dValorAtrib As Double
    Dim iOrden As Integer
    Dim lngMinimo As Long
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim dblCambio As Double
    
    'hay que deshabilitar (USarPrec y Flag) los atributos si me cambian la adjudicaci�n a m�s de un provedor
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_ogrupo.AdjudicadoTotal = 0
                        
    For Each oProv In m_oProvesAsig
        dblCambio = g_ogrupo.CambioComparativa(oProv.Cod)
        
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
            If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(g_oItemSeleccionado.grupoCod) Is Nothing Then
                bCargar = False
            End If
        End If
        If bCargar Then
            dImporte = 0
            dImporteMonProce = 0
            dAdjudicado = 0
            dAdjudicadoMonProce = 0
            
            sCod = g_ogrupo.Codigo & oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
            
            If Not m_oAdjs.Item(sCod) Is Nothing Then
                dImporte = m_oAdjs.Item(sCod).AdjudicadoOfe 'Adjudicado al 100% al grupo sin atributos de grupo
                dImporteMonProce = m_oAdjs.Item(sCod).AdjudicadoOfeMonProce
                dAdjudicado = m_oAdjs.Item(sCod).AdjudicadoSin 'Adjudicado al grupo sin atributos de grupo
                dAdjudicadoMonProce = m_oAdjs.Item(sCod).AdjudicadoSinMonProce
            End If
            
            '**** APLICA LOS ATRIBUTOS A APLICAR AL TOTAL DEL GRUPO ****
            'Ahora aplica las operaciones para los atributos de grupo.Aplica las f�rmulas al importe obtenido
            If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod) Is Nothing Then
                sCod = CStr(g_ogrupo.Codigo) & oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
                If Not m_oAdjs.Item(sCod) Is Nothing Then
                    lAtribAnterior = 0
                    bSiguiente = False
                    iOrden = 1
                    
                    While Not bSiguiente
                        lngMinimo = 0
                        For Each oatrib In m_oAtribsFormulas
                            'si el atributo aplica la f�rmula al total del grupo
                            If oatrib.PrecioAplicarA = 1 Then
                                If Not oatrib.AtribTotalGrupo.Item(g_ogrupo.Codigo) Is Nothing Then
                                    If oatrib.AtribTotalGrupo.Item(g_ogrupo.Codigo).UsarPrec = 1 Then
                                       ' If bSoloUno Then
                                            If oatrib.Orden = iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                                lngMinimo = oatrib.idAtribProce
                                                Exit For
                                            Else
                                                If oatrib.Orden > iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                                    If lngMinimo = 0 Or (oatrib.Orden < m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden) Then
                                                        lngMinimo = oatrib.idAtribProce
                                                    End If
                                                End If
                                            End If
                                    Else
                                        oatrib.AtribTotalGrupo.Item(g_ogrupo.Codigo).FlagAplicar = True
                                    End If
                                End If
                            End If
                        Next
                        
                        If (lngMinimo = 0) Then
                            bSiguiente = True
                        ElseIf (m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then
                            bSiguiente = True
                        Else
                            dValorAtrib = 0
                            Set oatrib = m_oAtribsFormulas.Item(CStr(lngMinimo))
                            lAtribAnterior = oatrib.idAtribProce
                            iOrden = oatrib.Orden
                            Select Case oatrib.ambito
                                Case 1
                                    'Proceso
                                    If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados Is Nothing Then
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                            dValorAtrib = NullToDbl0(m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                                        End If
                                    End If
                                            
                                Case 2
                                    'Grupo
                                    If oatrib.codgrupo = g_ogrupo.Codigo Or IsNull(oatrib.codgrupo) Then
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribGrOfertados Is Nothing Then
                                            scod1 = g_ogrupo.Codigo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(g_ogrupo.Codigo))
                                            scod1 = scod1 & CStr(oatrib.idAtribProce)
                                            If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribGrOfertados.Item(scod1) Is Nothing Then
                                                dValorAtrib = NullToDbl0(m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribGrOfertados.Item(scod1).valorNum)
                                            End If
                                        End If
                                    End If
                            End Select
                            If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                Select Case oatrib.PrecioFormula
                                    Case "+"
                                        dImporte = dImporte + dValorAtrib
                                        dImporteMonProce = dImporteMonProce + (dValorAtrib / dblCambio)
                                        dAdjudicado = dAdjudicado + dValorAtrib
                                        dAdjudicadoMonProce = dAdjudicadoMonProce + (dValorAtrib / dblCambio)
                                        
                                    Case "-"
                                        dImporte = dImporte - dValorAtrib
                                        dImporteMonProce = dImporteMonProce - (dValorAtrib / dblCambio)
                                        dAdjudicado = dAdjudicado - dValorAtrib
                                        dAdjudicadoMonProce = dAdjudicadoMonProce - (dValorAtrib / dblCambio)
                                        
                                    Case "/"
                                        dImporte = dImporte / dValorAtrib
                                        dImporteMonProce = dImporteMonProce / dValorAtrib
                                        dAdjudicado = dAdjudicado / dValorAtrib
                                        dAdjudicadoMonProce = dAdjudicadoMonProce / dValorAtrib
                                        
                                    Case "*"
                                        dImporte = dImporte * dValorAtrib
                                        dImporteMonProce = dImporteMonProce * dValorAtrib
                                        dAdjudicado = dAdjudicado * dValorAtrib
                                        dAdjudicadoMonProce = dAdjudicadoMonProce * dValorAtrib
                                        
                                    Case "+%"
                                        dImporte = dImporte + ((dValorAtrib / 100) * dImporte)
                                        dImporteMonProce = dImporteMonProce + ((dValorAtrib / 100) * dImporteMonProce)
                                        dAdjudicado = dAdjudicado + ((dValorAtrib / 100) * dAdjudicado)
                                        dAdjudicadoMonProce = dAdjudicadoMonProce + ((dValorAtrib / 100) * dAdjudicadoMonProce)
                                        
                                    Case "-%"
                                        dImporte = dImporte - ((dValorAtrib / 100) * dImporte)
                                        dImporteMonProce = dImporteMonProce - ((dValorAtrib / 100) * dImporteMonProce)
                                        dAdjudicado = dAdjudicado - ((dValorAtrib / 100) * dAdjudicado)
                                        dAdjudicadoMonProce = dAdjudicadoMonProce - ((dValorAtrib / 100) * dAdjudicadoMonProce)
                                        
                                End Select
                            End If
                            
                            Set oatrib = Nothing
                            bSiguiente = False
                            iOrden = iOrden + 1
                        End If
                    Wend
                End If
            End If
                
            'Almacena el importe,adjudicado,consumido,ahorrado,ahorradoporcen,ahorro ofe,... para el proveedor
            sCod = g_ogrupo.Codigo & oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
            
            If Not m_oAdjs.Item(sCod) Is Nothing Then
                m_oAdjs.Item(sCod).importe = dImporte
                m_oAdjs.Item(sCod).ImporteMonProce = dImporteMonProce
                m_oAdjs.Item(sCod).Adjudicado = dAdjudicado
                m_oAdjs.Item(sCod).AdjudicadoMonProce = dAdjudicadoMonProce
                m_oAdjs.Item(sCod).Ahorrado = m_oAdjs.Item(sCod).Consumido - dAdjudicado
                m_oAdjs.Item(sCod).AhorradoMonProce = m_oAdjs.Item(sCod).ConsumidoMonProce - dAdjudicadoMonProce
                
                If m_oAdjs.Item(sCod).Consumido = 0 Then
                    m_oAdjs.Item(sCod).AhorradoPorcen = 0
                Else
                    m_oAdjs.Item(sCod).AhorradoPorcen = (m_oAdjs.Item(sCod).Consumido - dAdjudicado) / Abs(m_oAdjs.Item(sCod).Consumido)
                End If
                
                m_oAdjs.Item(sCod).AhorroOfe = m_oAdjs.Item(sCod).AbiertoOfe - dImporte
                m_oAdjs.Item(sCod).AhorroOfeMonProce = m_oAdjs.Item(sCod).AbiertoOfeMonProce - dImporteMonProce
                
                If m_oAdjs.Item(sCod).AbiertoOfe = 0 Then
                    m_oAdjs.Item(sCod).AhorroOfePorcen = 0
                Else
                    m_oAdjs.Item(sCod).AhorroOfePorcen = (m_oAdjs.Item(sCod).AbiertoOfe - dImporte) / Abs(m_oAdjs.Item(sCod).AbiertoOfe)
                End If
                
                g_ogrupo.AdjudicadoTotal = g_ogrupo.AdjudicadoTotal + dAdjudicadoMonProce
            End If
        End If
    Next  'oProve
   
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "AplicarAtributosTotalGrupo", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub cmdPrecios_Click()
Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picControlPrecios.Visible = True
    
    'Actualiza el check que indica si los atributos se est�n aplicando o no:
    sdbgAplicarPrecio.MoveFirst
    
    For i = 0 To sdbgAplicarPrecio.Rows - 1
        'Si se un atributo que afecta al presupuesto unitario,total del item o total de la oferta
        If (IsNull(m_oAtribsFormulas.Item(sdbgAplicarPrecio.Columns("ID").Value).UsarPrec) And (m_oAtribsFormulas.Item(sdbgAplicarPrecio.Columns("ID").Value).PrecioDefecto = 0 Or IsNull(m_oAtribsFormulas.Item(sdbgAplicarPrecio.Columns("ID").Value).PrecioDefecto))) Or m_oAtribsFormulas.Item(sdbgAplicarPrecio.Columns("ID").Value).UsarPrec = 0 Then
            sdbgAplicarPrecio.Columns("INC").Value = "0"
        Else
            If m_oAtribsFormulas.Item(sdbgAplicarPrecio.Columns("ID").Value).FlagAplicar = True Then
                sdbgAplicarPrecio.Columns("INC").Value = "1"
            Else
                sdbgAplicarPrecio.Columns("INC").Value = "0"
            End If
        End If

        sdbgAplicarPrecio.MoveNext
        
    Next i

    sdbgAplicarPrecio.MoveFirst
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdPrecios_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub cmdRenombrarVista_Click()
    Dim teserror As TipoErrorSummit
 
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmADJGuardarVista.m_sNombreVistaAnterior = g_oOrigen.m_oVistaSeleccionada.nombre
    frmADJGuardarVista.g_sOrigen = "frmADJItem"
    Set frmADJGuardarVista.m_ofrmADJItem = Me
    frmADJGuardarVista.caption = m_sRenonVista
    frmADJGuardarVista.Show 1
      
    If g_sNombreVista <> "" Then
       teserror = g_oOrigen.m_oVistaSeleccionada.RenombrarVista(g_sNombreVista)
            
       If teserror.NumError <> TESnoerror Then
           basErrores.TratarError teserror
           Exit Sub
       Else
           oMensajes.MensajeOKOnly 821, TipoIconoMensaje.Information
       End If
       
       If g_oOrigen.m_oVistaSeleccionada.TipoVista = VistaDeOtroUsuario Then
           sdbcVistaActual.Refresh
           sdbcVistaActual.Text = m_sVistaUsu & " (" & NullToStr(g_oOrigen.m_oVistaSeleccionada.UsuarioNombre) & "): " & g_sNombreVista
           sdbcVistaDefecto.Text = m_sVistaUsu & " (" & NullToStr(g_oOrigen.m_oVistaSeleccionada.UsuarioNombre) & "): " & g_sNombreVista
           g_oOrigen.PonerCaptionGrid
           sdbgAdj.caption = m_sVistaUsu & " (" & NullToStr(g_oOrigen.m_oVistaSeleccionada.UsuarioNombre) & "): " & g_sNombreVista
           sdbgAdjEsc.caption = m_sVistaUsu & " (" & NullToStr(g_oOrigen.m_oVistaSeleccionada.UsuarioNombre) & "): " & g_sNombreVista
       Else
       
           sdbcVistaActual.Refresh
           sdbcVistaActual.Text = m_sVistaUsu & ": " & g_sNombreVista
           sdbcVistaDefecto.Text = m_sVistaUsu & ": " & g_sNombreVista
           g_oOrigen.PonerCaptionGrid
           sdbgAdj.caption = m_sVistaUsu & ": " & g_sNombreVista
           sdbgAdjEsc.caption = m_sVistaUsu & ": " & g_sNombreVista
       End If
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdRenombrarVista_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub cmdRestaurar_Click()
    Dim lID As Long
    Dim sGrupo As String
    Dim irespuesta As Integer

    'Si ha habido cambios en la vista pregunta a ver si quiere guardar la vista:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado.Invitado = False Then
        If Not g_oVistaSeleccionada Is Nothing Then
            If g_oVistaSeleccionada.HayCambios = True Then
                If g_oVistaSeleccionada.TipoVista = VistaDeUsuario Then
                    irespuesta = oMensajes.PreguntaGuardarVistas
                    If irespuesta = vbYes Then
                        cmdGuardarVista_Click
                    End If
                End If
            End If
        End If
    End If
    
    g_oOrigen.cmdRestaurar_Click
    
    lID = g_oItemSeleccionado.Id
    sGrupo = g_oItemSeleccionado.grupoCod
    
    'Hay que obtener de nuevo las clases
    Set g_ogrupo = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(sGrupo)
    Set m_oProvesAsig = g_oOrigen.m_oProvesAsig
    Set g_ogrupo.ConfVistasItem = Nothing
    Set g_oItemSeleccionado = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(sGrupo).Items.Item(CStr(lID))
    If g_oItemSeleccionado Is Nothing Then
        oMensajes.DatoEliminado 47
        Screen.MousePointer = vbNormal
        Unload Me
        Exit Sub
    End If
    Set m_oProcesoSeleccionado = g_oOrigen.m_oProcesoSeleccionado
    Set m_oAtribsFormulas = g_oOrigen.m_oAtribsFormulas
    Set m_oAdjs = g_oOrigen.m_oAdjs
    Set g_oAdjsProveEsc = g_oOrigen.m_oAdjsProveEsc
    
    If Not g_ogrupo Is Nothing Then
        If g_ogrupo.UsarEscalados Then
            ItemEscaladoSeleccionado True
        Else
            itemSeleccionado True   'Se recarga la grid
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdRestaurar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdVista_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picControlVista.Visible = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cmdVista_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cP_Click(ItemNumber As Long)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgAdjEsc
        AdjudicarDesadjudicarEscalado .Columns("ID").Value, .Columns("ESC" & .Grp).Value, (.Columns("ADJIMP" & .Grp).Value = "")
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "cP_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
    If Not IsNull(g_oItemSeleccionado.ArticuloCod) Then
        Me.sdbcItems.Value = g_oItemSeleccionado.ArticuloCod & " - " & g_oItemSeleccionado.Descr & " - " & g_oItemSeleccionado.proceso.Anyo & "/" & g_oItemSeleccionado.DestCod
    Else
        Me.sdbcItems.Value = g_oItemSeleccionado.Descr & " - " & g_oItemSeleccionado.proceso.Anyo & "/" & g_oItemSeleccionado.DestCod
    End If
        
    If Not g_oVistaSeleccionada Is Nothing Then
        If g_ogrupo.UsarEscalados Then
             ItemEscaladoSeleccionado True
        Else
            If g_oVistaSeleccionada.Vista = g_oOrigen.m_oVistaSeleccionada.Vista And g_oVistaSeleccionada.TipoVista = g_oOrigen.m_oVistaSeleccionada.TipoVista Then
                itemSeleccionado
            Else
                itemSeleccionado True
            End If
        End If
    Else
        itemSeleccionado True
    End If
    
    cmdGuardar.Enabled = m_oProcesoSeleccionado.GuardarProceso

    If g_ogrupo.UsarEscalados Then
        If Me.Visible Then sdbgAdjEsc.SetFocus
    Else
        If Me.Visible Then sdbgAdj.SetFocus
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Deactivate()
    'Desactiva el formulario en el diccionario del origen
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oOrigen.g_ofrmADJItemEstado.Exists(CStr(m_lIdentificador)) Then
        'A:indica que se el formulario continua abierto
        'N:No recargar las grid del origen
        'R:recargar las grid del origen
        If m_bRecargarOrigen Then
            g_oOrigen.g_ofrmADJItemEstado.Item(CStr(m_lIdentificador)) = "AR"
            m_bRecargarOrigen = False
        Else
            g_oOrigen.g_ofrmADJItemEstado.Item(CStr(m_lIdentificador)) = "AN"
        End If
    End If
    
    GuardarVistaItem
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "Form_Deactivate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub Form_Load()

Dim sItem As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bActivado = False
    m_bDescargarFrm = False
    Set cP = New cPopupMenu
    cP.hWndOwner = Me.hWnd

    Me.Height = 6975
    Me.Width = 11850

    ColorWhite = RGB(255, 255, 255)
    ColorButton = RGB(212, 208, 200)
    ColorGrey = RGB(192, 192, 192)
    ColorBlue = RGB(0, 0, 192)

    CargarRecursos
    
    ConfigurarSeguridad
    
    'Guardo la configuraci�n original de la grid
    m_sTemp = FSGSLibrary.DevolverPathFichTemp
    m_sLayOut = m_sTemp & "LayAdjItem"
    m_sLayOutEsc = m_sTemp & "LayAdjItemEsc"
    sdbgAdj.SaveLayout m_sLayOut, ssSaveLayoutAll
    sdbgAdjEsc.SaveLayout m_sLayOutEsc, ssSaveLayoutAll
    
    PonerFieldSeparator Me
    
    If gParametrosGenerales.gbUsarPonderacion = True Then
        If m_oProcesoSeleccionado.UsarPonderacion = True Then
            sdbgOptima.Columns("PON").Visible = True
        Else
            sdbgOptima.Columns("PON").Visible = False
            sdbgOptima.Columns("COD").Width = sdbgOptima.Columns("COD").Width + sdbgOptima.Columns("PON").Width
        End If
    Else
        sdbgOptima.Columns("PON").Visible = False
        sdbgOptima.Columns("COD").Width = sdbgOptima.Columns("COD").Width + sdbgOptima.Columns("PON").Width
    End If
    
    'Carga la combo de items segun el grupo
    CargarComboItems
    
    If Not IsNull(g_oItemSeleccionado.ArticuloCod) Then
        sItem = g_oItemSeleccionado.ArticuloCod & " - " & g_oItemSeleccionado.Descr
    Else
        sItem = g_oItemSeleccionado.Descr
    End If
    
    If g_sCodGrupo <> "" Then
        Me.caption = m_sProceso & ": " & m_oProcesoSeleccionado.Anyo & "/" & m_oProcesoSeleccionado.GMN1Cod & "/" & m_oProcesoSeleccionado.Cod & "   " & m_sLitGrupo & ": " & g_ogrupo.Codigo & "   " & m_sItem & ": " & sItem
    Else
        Me.caption = m_sProceso & ": " & m_oProcesoSeleccionado.Anyo & "/" & m_oProcesoSeleccionado.GMN1Cod & "/" & m_oProcesoSeleccionado.Cod & "   " & m_sItem & ": " & sItem
    End If
       
    'Habilito los botones segun la pantalla original
    cmdGuardar.Enabled = g_oOrigen.cmdGuardar.Enabled
    cmdAdjudicar.Enabled = g_oOrigen.cmdAdjudicar.Enabled
    
    lblProve.Backcolor = RGB(247, 232, 147)
       
    'Carga de datos
    sdbgAdj.Visible = False
    sdbgAdjEsc.Visible = False
    If g_ogrupo.UsarEscalados Then
        sdbgAdjEsc.Visible = True
        ItemEscaladoSeleccionado True
    Else
        sdbgAdj.Visible = True
        itemSeleccionado True
    End If
    
    CargarComboVistas
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picControlPrecios.Visible = False
    picControlOptima.Visible = False
    picControlVista.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "Form_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height < 2500 Then Exit Sub
    If Me.Width < 2000 Then Exit Sub
    
    picCerrar.Left = Me.Width - 360
    PicAbrir.Left = Me.Width - 360
    picItem.Width = Me.Width - 345
    If picItem.Width > (sdbgResultados.Left + 75) Then
        sdbgResultados.Width = picItem.Width - sdbgResultados.Left - 75
        sdbgResultados.Columns(0).Width = sdbgResultados.Width * 0.16
        sdbgResultados.Columns(1).Width = sdbgResultados.Width * 0.195
        sdbgResultados.Columns(2).Width = sdbgResultados.Width * 0.184
        sdbgResultados.Columns(3).Width = sdbgResultados.Width * 0.184
        sdbgResultados.Columns(4).Width = sdbgResultados.Width * 0.163
        sdbgResultados.Columns(5).Width = sdbgResultados.Width * 0.115
    End If
    
    If picItem.Visible = True Then
        sdbgAdj.Height = Me.Height - picItem.Height - 910
    Else
        sdbgAdj.Height = Me.Height - sdbgAdj.Top - 910
    End If
    sdbgAdjEsc.Height = sdbgAdj.Height
    
    sdbgAdj.Width = Me.Width - 195
    sdbgAdjEsc.Width = sdbgAdj.Width
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub

Private Sub Form_Unload(Cancel As Integer)
      
    'Desactiva el formulario en el diccionario del origen
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oOrigen.g_ofrmADJItemEstado.Exists(CStr(m_lIdentificador)) Then
        'C:indica que se va a cerrar la pantalla
        'N:No recargar las grid del origen
        'R:recargar las grid del origen
        If m_bRecargarOrigen Then
            g_oOrigen.g_ofrmADJItemEstado.Item(CStr(m_lIdentificador)) = "CR"
        Else
            g_oOrigen.g_ofrmADJItemEstado.Item(CStr(m_lIdentificador)) = "CN"
        End If
    End If
    
    Set g_ogrupo = Nothing
    Set g_oItemSeleccionado = Nothing
    Set m_oProcesoSeleccionado = Nothing
    Set m_oProvesAsig = Nothing
    Set m_oAtribsFormulas = Nothing
    Set m_oAdjs = Nothing
    Set g_oVistaSeleccionada = Nothing
    Set m_oAdjParcs = Nothing
    Set m_oAdjParcsGrupo = Nothing
    Set m_oIBaseDatos = Nothing
    Set oVistasCombo = Nothing
    Set m_oAdjsProve = Nothing
    
    If Not m_ofrmADJGraficos Is Nothing Then
        Unload m_ofrmADJGraficos
        Set m_ofrmADJGraficos = Nothing
    End If
    
    g_oOrigen.m_iUBound = g_oOrigen.m_iUBound - 1
        
    g_sCodGrupo = ""
    Set g_oOrigen = Nothing
    Set cP = Nothing
    
    Me.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub PicAbrir_Click()
    'Hace visibles los datos generales de �tem y la grid de resultados
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picItem.Visible = True
    
    'Hace m�s peque�a la grid de adjudicaciones y redimensiona la pantalla
    sdbgAdj.Top = picItem.Height + 50
    sdbgAdj.Height = sdbgAdj.Height - picItem.Height + 200
        
    'Trata los iconos
    picCerrar.Visible = True
    PicAbrir.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "PicAbrir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub PicAbrir_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    PicAbrir.BorderStyle = 1
    PicAbrir.BorderStyle = 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "PicAbrir_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub picCerrar_Click()
    'Hace invisibles los datos generales de �tem y la grid de resultados
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picItem.Visible = False
    
    'Hace m�s grande la grid de adjudicaciones y redimensiona la pantalla
    sdbgAdj.Top = sdbgAdj.Top - picItem.Height + 200
    sdbgAdj.Height = sdbgAdj.Height + picItem.Height - 200
        
    'Trata los iconos
    picCerrar.Visible = False
    
    PicAbrir.Top = 0
    PicAbrir.Visible = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "picCerrar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub picCerrar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picCerrar.BorderStyle = 1
    picCerrar.BorderStyle = 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "picCerrar_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub picItem_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picControlVista.Visible = False

    picControlOptima.Visible = False
    picControlPrecios.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "picItem_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
        
End Sub

Private Sub sdbcItems_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcItems.Value = sdbcItems.Columns("COD").Value & " - " & sdbcItems.Columns("DEN").Value '& " - " & sdbcItems.Columns("DEST").Value
    
    Set g_oItemSeleccionado = m_oProcesoSeleccionado.Grupos.Item(sdbcItems.Columns("GRUPO").Value).Items.Item(CStr(sdbcItems.Columns("ID").Value))
    If sdbcItems.Columns("GRUPO").Value <> g_ogrupo.Codigo Then
        Set g_ogrupo = m_oProcesoSeleccionado.Grupos.Item(sdbcItems.Columns("GRUPO").Value)
        itemSeleccionado True
        'No tendr�a que calcular nada puesto que si cambian los atributos a aplicar ya lo he calculado al momento
    Else
        itemSeleccionado False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbcItems_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub
Private Sub sdbcItems_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    sdbcItems.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbcItems.Rows - 1
            bm = sdbcItems.AddItemBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcItems.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcItems.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbcItems_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>Comprueba si existen adjudicaciones para un proveedor</summary>
''' <param name="ColIndex">columna updatada</param>
''' <remarks>Llamada desde:evento de sdbgAdj; Tiempo m�ximo:instantaneo</remarks>
''' <revison>LTG 15/11/2011</revision>

Private Function ComprobarAdjudicacion(ByVal sProve As String) As Variant
    Dim sCod As Variant
    Dim oAdjs As CAdjudicaciones
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sCod = ""
    If Not g_ogrupo.UltimasOfertas Is Nothing Then
        If Not g_ogrupo.UltimasOfertas.Item(Trim(sProve)) Is Nothing Then
            If Not g_ogrupo.Adjudicaciones Is Nothing Then
                sCod = sProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProve))
                sCod = CStr(g_oItemSeleccionado.Id) & sCod
                    
                If g_ogrupo.UsarEscalados Then
                    Set oAdjs = g_oAdjsProveEsc
                Else
                    Set oAdjs = g_ogrupo.Adjudicaciones
                End If
                
                'Si es nothing, no existe la adj devuelvo str vacio
                If oAdjs.Item(sCod) Is Nothing Then
                    sCod = Null
                End If
                
                Set oAdjs = Nothing
            End If
        End If
    End If
    ComprobarAdjudicacion = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ComprobarAdjudicacion", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Comprobar Oferta
''' </summary>
''' <param name="sProve">Prove</param>
''' <returns>Comprobada o no</returns>
''' <remarks>Llamada desde: sdbgAdj_AfterColUpdate ; Tiempo m�ximo: 0,2</remarks>
Private Function ComprobarOferta(ByVal sProve As String) As Boolean
Dim bBool As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
bBool = False
If Not g_ogrupo.UltimasOfertas Is Nothing Then
    If Not g_ogrupo.UltimasOfertas.Item(Trim(sProve)) Is Nothing Then
        If Not g_ogrupo.UltimasOfertas.Item(Trim(sProve)).Lineas Is Nothing Then
            If Not g_ogrupo.UltimasOfertas.Item(Trim(sProve)).Lineas.Item(CStr(g_oItemSeleccionado.Id)) Is Nothing Then
                bBool = True
            End If
        End If
    End If
End If
ComprobarOferta = bBool
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ComprobarOferta", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function
Private Sub sdbcItems_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sdbcItems.DataFieldToDisplay = "Column 0"
sdbcItems.DataFieldList = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbcItems_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub sdbcVistaActual_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
        
    If CInt(sdbcVistaActual.Columns("COD").Value) = CInt(g_oVistaSeleccionada.Vista) And CInt(sdbcVistaActual.Columns("TIPO").Value) = CInt(g_oVistaSeleccionada.TipoVista) And sdbcVistaActual.Columns("USU").Value = g_oVistaSeleccionada.UsuarioVista Then Exit Sub
    Screen.MousePointer = vbHourglass
    
    ConfiguracionVistaActual sdbcVistaActual.Columns("COD").Value, sdbcVistaActual.Columns("TIPO").Value, sdbcVistaActual.Columns("USU").Value
    
    'Carga los campos y proveedores a ocultar en las grids con la configuraci�n de la vista actual
    CargarVista

    'Redimensiona la grid
    If g_ogrupo.UsarEscalados Then
        RedimensionarGridEsc
    Else
        RedimensionarGrid
    End If
    'Se quitan los proves que no se han de ver
    CargarGridAdjudicaciones
    RedimensionarProves
    
    ConfigurarBotonesVistas sdbcVistaActual.Columns("TIPO").Value
            
    Dim sNombreVista As String
    sNombreVista = DevolverNombreVista(sdbcVistaActual.Text)
    
    Select Case sdbcVistaActual.Columns("TIPO").Value
        Case TipoDeVistaDefecto.vistainicial
            sdbgAdj.caption = m_sVistaIni
            sdbgAdjEsc.caption = m_sVistaIni
            
        Case TipoDeVistaDefecto.VistaDePlantilla
            
            sdbgAdj.caption = m_sVistaPlan & ": " & sNombreVista
            sdbgAdjEsc.caption = m_sVistaPlan & ": " & sNombreVista
        Case TipoDeVistaDefecto.VistaDeResponsable
           
            sdbgAdj.caption = m_sVistaResp & ": " & sNombreVista
            sdbgAdjEsc.caption = m_sVistaResp & ": " & sNombreVista
        Case TipoDeVistaDefecto.VistaDeUsuario
            sdbgAdj.caption = m_sVistaUsu & ": " & sNombreVista
            sdbgAdjEsc.caption = m_sVistaUsu & ": " & sNombreVista
        Case TipoDeVistaDefecto.VistaDeOtroUsuario
            If Not IsMissing(g_oVistaSeleccionada.UsuarioNombre) Then
                sdbgAdj.caption = m_sVistaUsu & " (" & NullToStr(g_oVistaSeleccionada.UsuarioNombre) & "): " & sNombreVista
                sdbgAdjEsc.caption = m_sVistaUsu & " (" & NullToStr(g_oVistaSeleccionada.UsuarioNombre) & "): " & sNombreVista
            Else
                sdbgAdj.caption = m_sVistaUsu & " (" & g_oVistaSeleccionada.UsuarioVista & "): " & sNombreVista
                sdbgAdjEsc.caption = m_sVistaUsu & " (" & g_oVistaSeleccionada.UsuarioVista & "): " & sNombreVista
            End If
    End Select
            
     g_oOrigen.ConfiguracionVistaActual sdbcVistaActual.Columns("COD").Value, sdbcVistaActual.Columns("TIPO").Value, sdbcVistaActual.Columns("USU").Value
     
     If g_oOrigen.sstabComparativa.Tag = "General" Then
         g_oOrigen.CargarVistaActualProce
     ElseIf g_oOrigen.sstabComparativa.Tag = "ALL" Then
         g_oOrigen.AllSeleccionado
     Else
         g_oOrigen.GrupoSeleccionado
     End If
     
     g_oOrigen.PonerCaptionGrid
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbcVistaActual_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub sdbcVistaActual_DropDown()
 Dim oVista As CConfVistaProce
 Dim bTieneVistaInicial As Boolean
 
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcVistaActual.RemoveAll
        
    Set oVistasCombo = Nothing
    Set oVistasCombo = oFSGSRaiz.Generar_CConfVistasProce
    
    oVistasCombo.CargarCombosVistas m_oProcesoSeleccionado, oUsuarioSummit.Cod, m_bPermitirVistas
    
    For Each oVista In oVistasCombo
        If oVista.TipoVista = vistainicial Then
            bTieneVistaInicial = True
            Exit For
        Else
            bTieneVistaInicial = False
        End If
    Next
    
    If Not bTieneVistaInicial Then
        sdbcVistaActual.AddItem m_sVistaIni & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & "INI"
    End If
    
    For Each oVista In oVistasCombo
        Select Case oVista.TipoVista
            Case TipoDeVistaDefecto.vistainicial
                sdbcVistaActual.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
            Case TipoDeVistaDefecto.VistaDePlantilla
                sdbcVistaActual.AddItem m_sVistaPlan & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
            Case TipoDeVistaDefecto.VistaDeResponsable
                sdbcVistaActual.AddItem m_sVistaResp & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
            Case TipoDeVistaDefecto.VistaDeUsuario
                sdbcVistaActual.AddItem m_sVistaUsu & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
            Case TipoDeVistaDefecto.VistaDeOtroUsuario
                sdbcVistaActual.AddItem m_sVistaUsu & " (" & oVista.UsuarioNombre & "): " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
        End Select
    Next
    sdbcVistaActual.ListAutoPosition = True
    sdbcVistaActual.SelLength = Len(sdbcVistaActual.Text)
    sdbcVistaActual.Refresh
    
    Set oVista = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbcVistaActual_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Function DevolverNombreVista(ByVal sNombre As String) As String
Dim sResultado As String
Dim sCaracter As String
Dim iCont As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
 iCont = 0
 sResultado = ""
 
 If sNombre = "" Then
     DevolverNombreVista = ""
     Exit Function
 Else
 
 If sNombre = m_sVistaIni Then
    DevolverNombreVista = m_sVistaIni
    Exit Function
 End If
 
 
    While sCaracter <> ":"
        iCont = iCont + 1
        sCaracter = Mid(sNombre, iCont, 1)
        
    Wend
        sResultado = Mid(sNombre, iCont + 1, Len(sNombre))
    
    DevolverNombreVista = Trim(sResultado)
    
 End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "DevolverNombreVista", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function


Private Sub ConfigurarBotonesVistas(ByVal iTipoVista As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
 If iTipoVista = TipoDeVistaDefecto.VistaDeUsuario Then
    
    cmdGuardarVista.Visible = True
    cmdGuardarVistaNueva.Visible = True
    cmdEliminarVista.Visible = True
    cmdRenombrarVista.Visible = True
 
    cmdGuardarVista.Left = 150
    cmdGuardarVistaNueva.Left = cmdGuardarVista.Left + cmdGuardarVista.Width + 50
    cmdEliminarVista.Left = cmdGuardarVistaNueva.Left + cmdGuardarVistaNueva.Width + 50
    cmdRenombrarVista.Left = cmdEliminarVista.Left + cmdEliminarVista.Width + 50
 ElseIf iTipoVista = TipoDeVistaDefecto.VistaDeOtroUsuario Then
    If m_bPermitirModifVistas Then
        cmdGuardarVista.Visible = True
        cmdGuardarVistaNueva.Visible = True
        cmdEliminarVista.Visible = True
        cmdRenombrarVista.Visible = True
    
        cmdGuardarVista.Left = 150
        cmdGuardarVistaNueva.Left = cmdGuardarVista.Left + cmdGuardarVista.Width + 50
        cmdEliminarVista.Left = cmdGuardarVistaNueva.Left + cmdGuardarVistaNueva.Width + 50
        cmdRenombrarVista.Left = cmdEliminarVista.Left + cmdEliminarVista.Width + 50
    Else
        cmdGuardarVista.Visible = False
        cmdGuardarVistaNueva.Visible = True
        cmdEliminarVista.Visible = False
        cmdRenombrarVista.Visible = False
           
        cmdGuardarVista.Left = 150
        cmdGuardarVistaNueva.Left = 2800
    End If
 ElseIf iTipoVista = TipoDeVistaDefecto.vistainicial Then
    
    cmdGuardarVista.Visible = False
    cmdGuardarVistaNueva.Visible = True
    cmdEliminarVista.Visible = False
    cmdRenombrarVista.Visible = False
    
    cmdGuardarVistaNueva.Left = picControlVista.Width / 2 - cmdGuardarVistaNueva.Width / 2
 
 ElseIf iTipoVista = TipoDeVistaDefecto.VistaDeResponsable Or iTipoVista = TipoDeVistaDefecto.VistaDePlantilla Then
    
    cmdGuardarVista.Visible = False
    cmdGuardarVistaNueva.Visible = True
    cmdEliminarVista.Visible = False
    cmdRenombrarVista.Visible = False
    
    cmdGuardarVistaNueva.Left = 2800
 
 End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ConfigurarBotonesVistas", err, Erl, , m_bActivado)
        Exit Sub
    End If
 
End Sub


Private Sub sdbcVistaActual_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcVistaActual.DataFieldList = "Column 0"
    sdbcVistaActual.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbcVistaActual_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcVistaActual_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcVistaActual.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcVistaActual.Rows - 1
            bm = sdbcVistaActual.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcVistaActual.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcVistaActual.Bookmark = bm
                Exit For
            End If
        Next i
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbcVistaActual_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcVistaDefecto_CloseUp()
Dim teserror As TipoErrorSummit
Dim j As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oOrigen.m_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    If CInt(sdbcVistaDefecto.Columns("COD").Value) = CInt(g_oOrigen.m_oProcesoSeleccionado.VistaDefectoComp) And CInt(sdbcVistaActual.Columns("TIPO").Value) = CInt(g_oOrigen.m_oProcesoSeleccionado.VistaDefectoTipo) Then Exit Sub

    g_oOrigen.m_oProcesoSeleccionado.VistaDefectoComp = sdbcVistaDefecto.Columns("COD").Value
    g_oOrigen.m_oProcesoSeleccionado.VistaDefectoTipo = sdbcVistaDefecto.Columns("TIPO").Value
    g_oOrigen.m_oProcesoSeleccionado.NombreVistaDefecto = DevolverNombreVista(sdbcVistaDefecto.Text)
    If g_oOrigen.m_oProcesoSeleccionado.VistaDefectoTipo = VistaDeOtroUsuario Then
        j = InStr(1, sdbcVistaDefecto.Text, ")", vbTextCompare) - InStr(1, sdbcVistaDefecto.Text, "(") - 1
        g_oOrigen.m_oProcesoSeleccionado.UsuarioNombre = Mid(sdbcVistaDefecto.Text, InStr(1, sdbcVistaDefecto.Text, "(") + 1, j)
    End If
    
    teserror = g_oOrigen.m_oProcesoSeleccionado.ModificarVistaDefectoProceso(oUsuarioSummit.Cod)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If


    If g_oOrigen.sstabComparativa.selectedItem.Tag = "General" Then
        g_oOrigen.MostrarVistaEnCombos AmbProceso, True, True
    ElseIf g_oOrigen.sstabComparativa.selectedItem.Tag = "ALL" Then
        g_oOrigen.MostrarVistaEnCombos AmbItem, True, True
    Else
        g_oOrigen.MostrarVistaEnCombos AmbGrupo, True, True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbcVistaDefecto_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub


Private Sub sdbcVistaDefecto_DropDown()
 Dim oVista As CConfVistaProce
 Dim bTieneVistaInicial As Boolean
 
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcVistaDefecto.RemoveAll
        
    Set oVistasCombo = Nothing
    Set oVistasCombo = oFSGSRaiz.Generar_CConfVistasProce
    
    oVistasCombo.CargarCombosVistas m_oProcesoSeleccionado, oUsuarioSummit.Cod, m_bPermitirVistas
    
    If oVistasCombo.Count <> 0 Then
        For Each oVista In oVistasCombo
            If oVista.TipoVista = vistainicial Then
                bTieneVistaInicial = True
                Exit For
            Else
                bTieneVistaInicial = False
            End If
        Next
        
        If Not bTieneVistaInicial Then
            sdbcVistaDefecto.AddItem m_sVistaIni & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & "INI"
        End If
    
        For Each oVista In oVistasCombo
            Select Case oVista.TipoVista
                Case TipoDeVistaDefecto.vistainicial
                    sdbcVistaDefecto.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
                Case TipoDeVistaDefecto.VistaDePlantilla
                    sdbcVistaDefecto.AddItem m_sVistaPlan & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
                Case TipoDeVistaDefecto.VistaDeResponsable
                    sdbcVistaDefecto.AddItem m_sVistaResp & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
                Case TipoDeVistaDefecto.VistaDeUsuario
                    sdbcVistaDefecto.AddItem m_sVistaUsu & ": " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
                Case TipoDeVistaDefecto.VistaDeOtroUsuario
                    sdbcVistaDefecto.AddItem m_sVistaUsu & " (" & NullToStr(oVista.UsuarioNombre) & "): " & oVista.nombre & Chr(m_lSeparador) & oVista.Vista & Chr(m_lSeparador) & oVista.TipoVista & Chr(m_lSeparador) & oVista.UsuarioVista
            End Select
        Next
    Else
        sdbcVistaDefecto.AddItem m_sVistaIni & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & "INI"
    End If
    
    sdbcVistaDefecto.ListAutoPosition = True
    'sdbcVistaDefecto.SelStart = 0
    sdbcVistaDefecto.SelLength = Len(sdbcVistaDefecto.Text)
    sdbcVistaDefecto.Refresh
    
    Set oVista = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbcVistaDefecto_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcVistaDefecto_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcVistaDefecto.DataFieldList = "Column 0"
    sdbcVistaDefecto.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbcVistaDefecto_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcVistaDefecto_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcVistaDefecto.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcVistaDefecto.Rows - 1
            bm = sdbcVistaDefecto.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcVistaDefecto.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcVistaDefecto.Bookmark = bm
                Exit For
            End If
        Next i
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbcVistaDefecto_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>
''' evento de sdbgAdj que controla q todo sea correcto y ordena recarga lo necesario
''' </summary>
''' <param name="ColIndex">columna updatada</param>
''' <remarks>Llamada desde:evento de sdbgAdj; Tiempo m�ximo:instantaneo</remarks>
''' <revison>LTG 02/11/2011</revision>

Private Sub sdbgAdj_AfterColUpdate(ByVal ColIndex As Integer)
    Dim teserror As TipoErrorSummit
    Dim sCod As Variant
    Dim oProve As CProveedor
    Dim dPorcenTotal As Double
    Dim iNumProvesAdju As Integer
    Dim irespuesta As Integer
    Dim bRestaurar As Boolean
    Dim vbm As Variant
    Dim dblPrecio As Double
    Dim vAtrib As Variant
    Dim oGrupo As CGrupo
    Dim i As Integer
    Dim j As Integer
    Dim oPrecio As CPrecioItem
    Dim oAtribsOfe As CAtributosOfertados
    Dim sOperacion As String
    Dim lIdAtribProce As Long
    Dim oatrib As CAtributo
    Dim vValor As Variant
    Dim dblCambio As Double
    Dim bCalcular As Boolean
    Dim oHayAdjs As CAdjudicaciones
    Dim oIAdj  As IAdjudicaciones
    Dim vPrecio As Variant
    Dim bRecalcularOferta As Boolean
    Dim dblConsumido As Double
    Dim dblAhorrado As Double
    Dim ambito As TipoAmbitoProceso
    Dim lIdAtrib As Long
    
    'tengo que avisar si hay atributos de total proceso o grupo y cambia la adjudicaci�n a otro proveedor, aunque solo haya uno
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bDblClick Then Exit Sub
    
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    m_bModError = False

    'Si est� modificando la cantidad adjudicada:
    If sdbgAdj.Columns(sdbgAdj.col).Name = "CANTADJ" Then
        If Trim(sdbgAdj.ActiveCell.Value) <> "" Then
            If Not IsNumeric(sdbgAdj.ActiveCell.Value) Then
                oMensajes.NoValido m_sCantAdj
                sdbgAdj.CancelUpdate
                Exit Sub
            ElseIf Trim(sdbgAdj.ActiveCell.Value) = 0 Then '    Si la cantidad es cero no se puede adjudicar.Otra cosa es el porcentaje en frmAdj
                If g_oItemSeleccionado.Cantidad = 0 Then
                Else
                    sdbgAdj.CancelUpdate

                    sdbgAdj.Columns("CANTADJ").Value = ""
                End If
            ElseIf g_oItemSeleccionado.Cantidad = 0 Then
                If CDec(NullToDbl0(StrToNull(sdbgAdj.ActiveCell.Value))) <> 0 Then
                    oMensajes.NoValido m_sCantAdj
                    sdbgAdj.CancelUpdate
                    Exit Sub
                End If
            ElseIf g_oItemSeleccionado.Cantidad < 0 Then
                If CDec(NullToDbl0(StrToNull(sdbgAdj.ActiveCell.Value))) > 0 Then
                    oMensajes.NoValido m_sCantAdj
                    sdbgAdj.CancelUpdate
                    Exit Sub
                End If
            Else
                If CDec(NullToDbl0(StrToNull(sdbgAdj.ActiveCell.Value))) < 0 Then
                    oMensajes.NoValido m_sCantAdj
                    sdbgAdj.CancelUpdate
                    Exit Sub
                End If
            End If
        End If
        vbm = sdbgAdj.RowBookmark(sdbgAdj.Row)
    
    '    Si no tiene precio no se puede adjudicar
        If sdbgAdj.Columns("PRECIO").CellValue(vbm) = "" Then
            sdbgAdj.CancelUpdate
            Exit Sub
        End If

        sCod = sdbgAdj.Columns("PROVECOD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdj.Columns("PROVECOD").Value))
        sCod = CStr(g_oItemSeleccionado.Id) & sCod
        
        If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
            If NullToDbl0(StrToNull(sdbgAdj.ActiveCell.Value)) = 0 Then
                'Si esta cerrado le dejo que haga pq no puede guardar
                If m_oProcesoSeleccionado.Estado < conadjudicaciones And m_oProcesoSeleccionado.Pedidos > PPSinPedidos Then
                    m_iInd = 0
                    Erase m_vEnPedidos
                    'Si est� en pedidos o catalogo no se puede cambiar la adjudicaci�n
                    If Not IsNull(g_ogrupo.Adjudicaciones.Item(sCod).pedido) Then
                        ReDim Preserve m_vEnPedidos(3, m_iInd)
                        m_vEnPedidos(0, m_iInd) = Trim(NullToStr(g_ogrupo.Items.Item(CStr(g_oItemSeleccionado.Id)).ArticuloCod) & "  " & NullToStr(g_ogrupo.Items.Item(CStr(g_oItemSeleccionado.Id)).Descr))
                        m_vEnPedidos(1, m_iInd) = g_ogrupo.Adjudicaciones.Item(sCod).pedido
                        m_vEnPedidos(2, m_iInd) = Trim(sdbgAdj.Columns("PROVECOD").Value & "  " & m_oProvesAsig.Item(sdbgAdj.Columns("PROVECOD").Value).Den)
                        m_vEnPedidos(3, m_iInd) = 174 'El �tem esta incluido en los pedidos
                        m_iInd = m_iInd + 1
                    ElseIf g_ogrupo.Adjudicaciones.Item(sCod).Catalogo = 1 Then
                        ReDim Preserve m_vEnPedidos(3, m_iInd)
                        m_vEnPedidos(0, m_iInd) = Trim(NullToStr(g_ogrupo.Items.Item(CStr(g_oItemSeleccionado.Id)).ArticuloCod) & "  " & NullToStr(g_ogrupo.Items.Item(CStr(g_oItemSeleccionado.Id)).Descr))
                        m_vEnPedidos(1, m_iInd) = g_ogrupo.Adjudicaciones.Item(sCod).pedido
                        m_vEnPedidos(2, m_iInd) = Trim(sdbgAdj.Columns("PROVECOD").Value & "  " & m_oProvesAsig.Item(sdbgAdj.Columns("PROVECOD").Value).Den)
                        m_vEnPedidos(3, m_iInd) = 175 'El �tem esta incluido en los pedidos
                        m_iInd = m_iInd + 1
                    End If
                    If m_iInd > 0 Then
                        oMensajes.ImposibleCambiarAdjudicacion m_vEnPedidos, 1
                        sdbgAdj.CancelUpdate
                        Exit Sub
                    End If
                End If
            End If
        End If

    '    Primero comprobamos si la cantidad supera la cantidad m�xima indicada por el proveedor
        If sdbgAdj.ActiveCell.Value <> "" Or sdbgAdj.ActiveCell.Value <> 0 Then
            If m_oProcesoSeleccionado.SolicitarCantMax And sdbgAdj.Columns("CANTMAX").CellValue(vbm) <> "" Then
                If CDec(StrToDbl0(sdbgAdj.ActiveCell.Value)) > CDec(StrToDbl0(sdbgAdj.Columns("CANTMAX").CellValue(vbm))) Then
                    oMensajes.CantidadMaximaProveedorSuperada StrToDbl0(sdbgAdj.Columns("CANTMAX").CellValue(vbm)), sdbgAdj.ActiveCell.Value
                    sdbgAdj.CancelUpdate
                    Exit Sub
                End If
            End If
        
        '   Comprobamos que la cantidad total adjudicada no supere el total
            dPorcenTotal = 0
            iNumProvesAdju = 0
            For Each oProve In m_oProvesAsig
                If oProve.Cod = sdbgAdj.Columns("PROVECOD").Value Then
                    If g_oItemSeleccionado.Cantidad = 0 Then
                        sCod = ComprobarAdjudicacion(oProve.Cod)
                        If Not IsNull(sCod) And sCod <> "" Then
                            If CDec(g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje) > 0 Then
                                iNumProvesAdju = iNumProvesAdju + 1
                                dPorcenTotal = dPorcenTotal + g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje
                            End If
                        End If
                    Else
                        If CDec(StrToDbl0(sdbgAdj.ActiveCell.Value)) <> 0 Then
                            dPorcenTotal = dPorcenTotal + CDec((StrToDbl0(sdbgAdj.ActiveCell.Value) * 100) / g_oItemSeleccionado.Cantidad)
    
                            iNumProvesAdju = iNumProvesAdju + 1
                        End If
                    End If
                Else
                    sCod = ComprobarAdjudicacion(oProve.Cod)
                    If Not IsNull(sCod) And sCod <> "" Then
                        If CDec(g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje) > 0 Then
                            iNumProvesAdju = iNumProvesAdju + 1
                            dPorcenTotal = dPorcenTotal + g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje
                        End If
                    End If
                End If
                
                If CDec(dPorcenTotal) > 100 Then
                    oMensajes.NoValido m_sCantidadAdj
                    sdbgAdj.CancelUpdate
                    Exit Sub
        
                End If
            Next
        
            bRestaurar = False
            'Comprobamos si hay atributos de total item, grupo o proceso si se van a deshabilitar
            If iNumProvesAdju > 1 Then
                If Not AtribsTotalItemComprobar() Then
                    bRestaurar = True
                    irespuesta = oMensajes.PreguntaDeshabilitarAtribsAplicados
                    If irespuesta = vbNo Then
                        sdbgAdj.CancelUpdate
                        Exit Sub
                    End If
                    If m_bHayAtribsDeGrupoOProce = True Then
                        m_bCambianTodasAdjs = True
                    End If
                End If
            End If
            
        End If
        
        
        'Calculamos el valor del porcentaje    'Carga las modificaciones en las colecciones
        sCod = ComprobarAdjudicacion(sdbgAdj.Columns("PROVECOD").CellValue(vbm))
        If sCod <> "" Or IsNull(sCod) Then  'Si es "" no existe ni la coleccion de adjudicaciones
            If IsNull(sCod) Then 'Si es null no existe la adj para ese prove
                If ComprobarOferta(sdbgAdj.Columns("PROVECOD").CellValue(vbm)) Then
                    'dblPrecio en moneda de la oferta para a�adir a Cadjudicaciones
                    dblPrecio = g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").CellValue(vbm))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta
                Else
                    dblPrecio = StrToDbl0(sdbgAdj.Columns("PRECIO").CellValue(vbm))
                End If

                If g_oItemSeleccionado.Cantidad = 0 Then
                    If (100 - dPorcenTotal) = 0 Then
                        oMensajes.NoValido m_sCantidadAdj
                        sdbgAdj.CancelUpdate
                        Exit Sub
                    End If
                    g_ogrupo.Adjudicaciones.Add sdbgAdj.Columns("PROVECOD").CellValue(vbm), sdbgAdj.Columns("NUMOFE").CellValue(vbm), g_oItemSeleccionado.Id, dblPrecio, 100 - dPorcenTotal
                Else
                    g_ogrupo.Adjudicaciones.Add sdbgAdj.Columns("PROVECOD").CellValue(vbm), sdbgAdj.Columns("NUMOFE").CellValue(vbm), g_oItemSeleccionado.Id, dblPrecio, (StrToDbl0(sdbgAdj.ActiveCell.Value) / g_oItemSeleccionado.Cantidad) * 100
                End If
                
                sCod = sdbgAdj.Columns("PROVECOD").CellValue(vbm) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdj.Columns("PROVECOD").CellValue(vbm)))
                sCod = CStr(g_oItemSeleccionado.Id) & sCod
            Else
                RecalcularCantidades True

                If g_oItemSeleccionado.Cantidad = 0 Then
                    If Trim(sdbgAdj.ActiveCell.Value) = "" Then g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje = 0
                Else
                    g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje = CDec((StrToDbl0(sdbgAdj.ActiveCell.Value) / g_oItemSeleccionado.Cantidad) * 100)
                End If
            End If
            
            'Importe adjudicado en moneda de la oferta
            g_ogrupo.Adjudicaciones.Item(sCod).ImporteAdj = CDec(g_ogrupo.Adjudicaciones.Item(sCod).Precio * StrToDbl0(sdbgAdj.Columns("CANTADJ").Value))
            
            If CDec(g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje) = 0 Then
                g_ogrupo.Adjudicaciones.Remove sCod
            End If
            'Al modificar la cantidad adjudicada hay que calcular los nuevos Consumido,Adjudicado,Ahorrado
            RecalcularCantidades
            m_bHayAtribsDeGrupoOProce = False
            
        End If
    
    ElseIf sdbgAdj.Columns(sdbgAdj.col).Name = "PRECIO" Then   'Estamos modificando el precio
        If Trim(sdbgAdj.ActiveCell.Value) <> "" Then
            If Not IsNumeric(sdbgAdj.ActiveCell.Value) Then
                oMensajes.NoValido m_sPrecio
                sdbgAdj.CancelUpdate
                Exit Sub
            End If
            'comprueba si hay atributos de precio unitario aplicados a ese item.
            vAtrib = Null
            vAtrib = ComprobarAtributosPrecAplicados(g_oItemSeleccionado, sdbgAdj.Columns("PROVECOD").Value)
            If Not IsNull(vAtrib) Then
                irespuesta = oMensajes.PreguntaModificarPrecio(vAtrib)
                If irespuesta = vbNo Then
                    sdbgAdj.CancelUpdate
                    Exit Sub
                End If
                bRestaurar = True
            End If
        End If
    
        vbm = sdbgAdj.RowBookmark(sdbgAdj.Row)
        
        Set oPrecio = g_ogrupo.UltimasOfertas.Item(Trim(CStr(sdbgAdj.Columns("PROVECOD").Value))).Lineas.Item(CStr(g_oItemSeleccionado.Id))
        If sdbgAdj.Columns(ColIndex).Value = "" Then
            m_iInd = 0
            Erase m_vEnPedidos
            If EliminarAdjudicacionProve(g_ogrupo.Codigo, g_oItemSeleccionado.Id, sdbgAdj.Columns("PROVECOD").Value) Then
                oPrecio.PrecioOferta = Null
            Else
                If m_iInd > 0 Then
                    oMensajes.ImposibleCambiarAdjudicacion m_vEnPedidos, 1
                    sdbgAdj.CancelUpdate
                    Exit Sub
                End If
            End If
        Else
            vPrecio = oPrecio.PrecioOferta
            oPrecio.PrecioOferta = CDec(StrToDbl0(sdbgAdj.Columns(ColIndex).Value) * NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(CStr(sdbgAdj.Columns("PROVECOD").Value))).Cambio))
        End If
        Select Case oPrecio.Usar
            Case 1
                oPrecio.Precio = oPrecio.PrecioOferta
            Case 2
                oPrecio.Precio2 = oPrecio.PrecioOferta
            Case 3
                oPrecio.Precio3 = oPrecio.PrecioOferta
        End Select
                
        'si tiene atributos de precio unitario aplicados los quita:
        If Not IsNull(vAtrib) And Not IsNull(oPrecio.PrecioOferta) Then
            For i = 0 To UBound(vAtrib, 2)
                m_oAtribsFormulas.Item(CStr(vAtrib(2, i))).UsarPrec = 0
                If Not m_oAtribsFormulas.Item(CStr(vAtrib(2, i))).AtribTotalGrupo Is Nothing Then
                    If Not m_oAtribsFormulas.Item(CStr(vAtrib(2, i))).AtribTotalGrupo.Item(CStr(g_ogrupo.Codigo)) Is Nothing Then
                         m_oAtribsFormulas.Item(CStr(vAtrib(2, i))).AtribTotalGrupo.Item(CStr(g_ogrupo.Codigo)).UsarPrec = 0
                    End If
                End If
                If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
                    If Not m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(vAtrib(2, i))) Is Nothing Then
                        m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(vAtrib(2, i))).UsarPrec = 0
                    End If
                End If
                If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
                    If Not m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(vAtrib(2, i))) Is Nothing Then
                        m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(vAtrib(2, i))).UsarPrec = 0
                    End If
                End If
                If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
                    If Not m_oProcesoSeleccionado.AtributosItem.Item(CStr(vAtrib(2, i))) Is Nothing Then
                        m_oProcesoSeleccionado.AtributosItem.Item(CStr(vAtrib(2, i))).UsarPrec = 0
                    End If
                End If
                For Each oGrupo In m_oProcesoSeleccionado.Grupos
                    If Not oGrupo.AtributosItem Is Nothing Then
                        If Not oGrupo.AtributosItem.Item(CStr(vAtrib(2, i))) Is Nothing Then
                            oGrupo.AtributosItem.Item(CStr(vAtrib(2, i))).UsarPrec = 0
                        End If
                    End If
                Next
                sdbgAplicarPrecio.MoveFirst
                For j = 0 To sdbgAplicarPrecio.Rows - 1
                    If sdbgAplicarPrecio.Columns("ID").Value = CStr(vAtrib(2, i)) Then
                        sdbgAplicarPrecio.Columns("INC").Value = 0
                        Exit For
                    Else
                        sdbgAplicarPrecio.MoveNext
                    End If
                Next j
            Next i
            CalcularPrecioConFormulas
        End If
        
        bRecalcularOferta = False
        
        'Si se ha modificado el precio hay que recalcular la oferta.
        If NullToDbl0(sdbgAdj.Columns(ColIndex).Value) <> NullToDbl0(vPrecio) Then
            bRecalcularOferta = True
        End If
        
        If bRecalcularOferta Then
            oPrecio.Oferta.Recalcular = True
            m_oProcesoSeleccionado.Ofertas.Item(CStr(sdbgAdj.Columns("PROVECOD").Value)).Recalcular = True
            Set oPrecio.Oferta.proceso = m_oProcesoSeleccionado
            frmADJCargar.Show
            frmADJCargar.lblCargando.caption = m_sLitRecalculando
            frmADJCargar.Refresh
        End If
        
        teserror = oPrecio.ModificarPrecio
        
        If bRecalcularOferta Then
            Unload frmADJCargar
        End If
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bModError = True
            sdbgAdj.CancelUpdate
            Exit Sub
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompModPrec, "Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(m_oProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(m_oProcesoSeleccionado.Cod) & " Item:" & CStr(g_oItemSeleccionado.Id)
            'Registramos llamada a recalcular
            If bRecalcularOferta Then
                basSeguridad.RegistrarAccion AccionesSummit.ACCCondOfeRecalculo, "Llamada desde:Comparativa Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(m_oProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(m_oProcesoSeleccionado.Cod)
            End If
        End If
        
        'Si hab�a adjudicaciones las guarda:
        If Not g_ogrupo.Adjudicaciones Is Nothing Then
            sCod = sdbgAdj.Columns("PROVECOD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdj.Columns("PROVECOD").Value))
            sCod = CStr(g_oItemSeleccionado.Id) & sCod
            If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                If IsNull(oPrecio.PrecioOferta) Then
                    g_ogrupo.Adjudicaciones.Remove sCod
                End If
                Set oIAdj = m_oProcesoSeleccionado
                Set oHayAdjs = oIAdj.DevolverAdjudicacionesDeGrupoOItem(sdbgAdj.Columns("PROVECOD").Value, , g_oItemSeleccionado.Id)
                If oHayAdjs.Count > 0 Then
                    If g_oOrigen.GuardarAdjudicacion() = False Then
                        sdbgAdj.CancelUpdate
                        Exit Sub
                    End If
                End If
                Set oHayAdjs = Nothing
                Set oIAdj = Nothing
            End If
        End If
                
        sCod = ComprobarAdjudicacion(sdbgAdj.Columns("PROVECOD").CellValue(vbm))
        If sCod <> "" Or IsNull(sCod) Then 'Si es "" no existe ni la coleccion de adjudicaciones
            If IsNull(sCod) Then 'Si es null no existe la adj para ese prove
                If g_oItemSeleccionado.Cantidad = 0 Then
                    dPorcenTotal = 0
                    For Each oProve In m_oProvesAsig
                        sCod = ComprobarAdjudicacion(oProve.Cod)
                        If Not IsNull(sCod) And sCod <> "" Then
                            If CDec(g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje) > 0 Then
                                dPorcenTotal = dPorcenTotal + g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje
                            End If
                        End If
                    Next
                    If (100 - dPorcenTotal > 0) And (sdbgAdj.Columns("CANTADJ").Value <> "") Then
                        g_ogrupo.Adjudicaciones.Add sdbgAdj.Columns("PROVECOD").CellValue(vbm), sdbgAdj.Columns("NUMOFE").CellValue(vbm), g_oItemSeleccionado.Id, NullToDbl0(oPrecio.PrecioOferta), 100 - dPorcenTotal
                    End If
                Else
                    If (StrToDbl0(sdbgAdj.Columns("CANTADJ").Value) / g_oItemSeleccionado.Cantidad) * 100 <> 0 Then
                        g_ogrupo.Adjudicaciones.Add sdbgAdj.Columns("PROVECOD").CellValue(vbm), sdbgAdj.Columns("NUMOFE").CellValue(vbm), g_oItemSeleccionado.Id, NullToDbl0(oPrecio.PrecioOferta), (StrToDbl0(sdbgAdj.Columns("CANTADJ").Value) / g_oItemSeleccionado.Cantidad) * 100
                    End If
                End If
                sCod = sdbgAdj.Columns("PROVECOD").CellValue(vbm) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdj.Columns("PROVECOD").CellValue(vbm)))
                sCod = CStr(g_oItemSeleccionado.Id) & sCod
            Else
                RecalcularCantidades True
            End If
            
            'Importe adjudicado en moneda de la oferta
            If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                g_ogrupo.Adjudicaciones.Item(sCod).ImporteAdj = NullToDbl0(oPrecio.PrecioOferta) * StrToDbl0(sdbgAdj.Columns("CANTADJ").Value)
            End If
            
            If IsNull(oPrecio.PrecioOferta) Then
                sdbgAdj.Columns("CANTADJ").Value = ""
            End If
                    
            'Al modificar el precio de la oferta hay que calcular los nuevos Consumido,Adjudicado,Ahorrado
            RecalcularCantidades
            m_bHayAtribsDeGrupoOProce = False
            
        End If
        
        Set oPrecio = Nothing
        
    ElseIf Mid(sdbgAdj.Columns(sdbgAdj.col).Name, 1, 5) = "ATRIB" Then
        If m_oProcesoSeleccionado.Ofertas.Item(CStr(sdbgAdj.Columns("PROVECOD").Value)) Is Nothing Then
            sdbgAdj.CancelUpdate
            Exit Sub
        End If
        
        Select Case Right(sdbgAdj.Columns(sdbgAdj.col).TagVariant, 1)
             Case "P"
                ambito = TipoAmbitoProceso.AmbProceso
             Case "G"
                ambito = TipoAmbitoProceso.AmbGrupo
             Case "I"
                ambito = TipoAmbitoProceso.AmbItem
        End Select
        lIdAtrib = Right(sdbgAdj.Columns(sdbgAdj.col).Name, Len(sdbgAdj.Columns(sdbgAdj.col).Name) - 5)
        If Not ComprobarValorAtributo(m_oProcesoSeleccionado, lIdAtrib, sdbgAdj.Columns(sdbgAdj.col).Value, m_sIdiTrue, m_sIdiFalse, m_sIdiMayor, m_sIdiMenor, m_sIdiEntre, ambito) Then  'Comprueba si el valor introducido se corresponde con el tipo del atributo
            sdbgAdj.CancelUpdate
            Exit Sub
        End If
        bRecalcularOferta = False
        lIdAtribProce = Right(sdbgAdj.Columns(sdbgAdj.col).Name, Len(sdbgAdj.Columns(sdbgAdj.col).Name) - 5)
        dblCambio = 1
        
        Select Case Right(sdbgAdj.Columns(sdbgAdj.col).TagVariant, 1)
            Case "P"
                If sdbgAdj.Columns(ColIndex).Value = "" Then
                    sOperacion = "D"
                Else
                    If Not m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados Is Nothing Then
                        If Not m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Item(CStr(lIdAtribProce)) Is Nothing Then
                            If m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Item(CStr(lIdAtribProce)).valorNum <> sdbgAdj.Columns(ColIndex).Value Then
                                bRecalcularOferta = True
                            End If
                            sOperacion = "U"
                        Else
                            sOperacion = "I"
                            bRecalcularOferta = True
                        End If
                    Else
                        sOperacion = "I"
                        bRecalcularOferta = True
                    End If
                End If
                If m_oProcesoSeleccionado.MonCod <> m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).CodMon Then
                    dblCambio = m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).Cambio
                End If
                Set oatrib = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(lIdAtribProce))
        
            Case "G"
                If sdbgAdj.Columns(ColIndex).Value = "" Then
                    sOperacion = "D"
                Else
                    If Not m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados Is Nothing Then
                        sCod = CStr(g_ogrupo.Codigo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(CStr(g_ogrupo.Codigo)))
                        sCod = sCod & CStr(lIdAtribProce)
                        If Not m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Item(sCod) Is Nothing Then
                            If m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Item(sCod).valorNum <> sdbgAdj.Columns(ColIndex).Value Then
                                bRecalcularOferta = True
                            End If
                            sOperacion = "U"
                        Else
                            sOperacion = "I"
                            bRecalcularOferta = True
                        End If
                    Else
                        sOperacion = "I"
                        bRecalcularOferta = True
                    End If
                End If
                If m_oProcesoSeleccionado.MonCod <> m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).CodMon Then
                    dblCambio = m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).Cambio
                End If
                Set oatrib = m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(lIdAtribProce))
                
            Case "I"
                If sdbgAdj.Columns(ColIndex).Value = "" Then
                    sOperacion = "D"
                Else
                    If Not g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados Is Nothing Then
                        If Not g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(lIdAtribProce)) Is Nothing Then
                            If g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(lIdAtribProce)).valorNum <> sdbgAdj.Columns(ColIndex).Value Then
                                bRecalcularOferta = True
                            End If
                            sOperacion = "U"
                        Else
                            sOperacion = "I"
                            bRecalcularOferta = True
                        End If
                    Else
                        sOperacion = "I"
                        bRecalcularOferta = True
                    End If
                End If
                If m_oProcesoSeleccionado.MonCod <> g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).CodMon Then
                    dblCambio = g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).Cambio
                End If
                Set oatrib = g_ogrupo.AtributosItem.Item(CStr(lIdAtribProce))
        End Select

        
        If oatrib.Tipo = TipoBoolean Then
            If UCase(sdbgAdj.Columns(ColIndex).Value) = UCase(m_sIdiTrue) Then
                vValor = True
            Else
                vValor = False
            End If
        Else
            If sdbgAdj.Columns(ColIndex).Value <> "" And (oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-") Then
                vValor = sdbgAdj.Columns(ColIndex).Value * dblCambio
            Else
                vValor = sdbgAdj.Columns(ColIndex).Value
            End If
        End If
    
        Set oAtribsOfe = oFSGSRaiz.Generar_CAtributosOfertados
        
        Select Case oatrib.ambito
            Case AmbProceso
                If bRecalcularOferta Then
                    m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).Recalcular = True
                    Set m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).proceso = m_oProcesoSeleccionado
                    frmADJCargar.Show
                    frmADJCargar.lblCargando.caption = m_sLitRecalculando
                    frmADJCargar.Refresh
                End If
                teserror = oAtribsOfe.GuardarAtributoOfertado(sOperacion, m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value), lIdAtribProce, vValor, , , m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).Cambio)
            Case AmbGrupo
                If bRecalcularOferta Then
                    m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).Recalcular = True
                    Set m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).proceso = m_oProcesoSeleccionado
                    frmADJCargar.Show
                    frmADJCargar.lblCargando.caption = m_sLitRecalculando
                    frmADJCargar.Refresh
                End If
                teserror = oAtribsOfe.GuardarAtributoOfertado(sOperacion, m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value), lIdAtribProce, vValor, g_ogrupo.Id, , m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).Cambio)
            Case AmbItem
                If bRecalcularOferta Then
                    g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).Recalcular = True
                    Set m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).proceso = m_oProcesoSeleccionado
                    frmADJCargar.Show
                    frmADJCargar.lblCargando.caption = m_sLitRecalculando
                    frmADJCargar.Refresh
                End If
                teserror = oAtribsOfe.GuardarAtributoOfertado(sOperacion, g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)), lIdAtribProce, vValor, g_ogrupo.Id, g_oItemSeleccionado.Id, g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).Cambio)
        End Select
        
        If bRecalcularOferta Then
            Unload frmADJCargar
        End If
    
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bModError = True
            sdbgAdj.CancelUpdate
            Exit Sub
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompModOfer, "Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(m_oProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(m_oProcesoSeleccionado.Cod) & " Atrib: " & CStr(oatrib.idAtribProce)
            'Registramos llamada a recalcular
            If bRecalcularOferta Then
                basSeguridad.RegistrarAccion AccionesSummit.ACCCondOfeRecalculo, "Llamada desde:Comparativa Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(m_oProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(m_oProcesoSeleccionado.Cod)
            End If
        End If
        Set oAtribsOfe = Nothing
        
        
        '''Guarda en la colecci�n
        Select Case oatrib.ambito
            Case AmbProceso
                If sOperacion = "D" Then
                    If Not m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados Is Nothing Then
                        If Not m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Item(CStr(lIdAtribProce)) Is Nothing Then
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Remove (CStr(lIdAtribProce))
                        End If
                    End If
                ElseIf sOperacion = "I" Then
                    If m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados Is Nothing Then
                        Set m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados = oFSGSRaiz.Generar_CAtributosOfertados
                    End If
                    Select Case oatrib.Tipo
                        Case TiposDeAtributos.TipoString
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Add lIdAtribProce, m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value), , , , , , , vValor
                        Case TiposDeAtributos.TipoNumerico
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Add lIdAtribProce, m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value), , , , , CDbl(vValor)
                        Case TiposDeAtributos.TipoFecha
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Add lIdAtribProce, m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value), , , , CDate(vValor)
                        Case TiposDeAtributos.TipoBoolean
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Add lIdAtribProce, m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value), , , BooleanToSQLBinary(vValor)
                    End Select
                ElseIf sOperacion = "U" Then
                    Select Case oatrib.Tipo
                        Case TiposDeAtributos.TipoString
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Item(CStr(lIdAtribProce)).valorText = vValor
                        Case TiposDeAtributos.TipoNumerico
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Item(CStr(lIdAtribProce)).valorNum = CDbl(vValor)
                        Case TiposDeAtributos.TipoFecha
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Item(CStr(lIdAtribProce)).valorFec = CDate(vValor)
                        Case TiposDeAtributos.TipoBoolean
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribProcOfertados.Item(CStr(lIdAtribProce)).valorBool = BooleanToSQLBinary(vValor)
                    End Select
                End If
            Case AmbGrupo
                sCod = CStr(g_ogrupo.Codigo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(CStr(g_ogrupo.Codigo)))
                sCod = sCod & CStr(lIdAtribProce)
                If sOperacion = "D" Then
                    If Not m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados Is Nothing Then
                        If Not m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Item(sCod) Is Nothing Then
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Remove (sCod)
                        End If
                    End If
                ElseIf sOperacion = "I" Then
                    If m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados Is Nothing Then
                        Set m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados = oFSGSRaiz.Generar_CAtributosOfertados
                    End If
                    Select Case oatrib.Tipo
                        Case TiposDeAtributos.TipoString
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Add lIdAtribProce, m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value), , g_ogrupo.Codigo, , , , , vValor
                        Case TiposDeAtributos.TipoNumerico
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Add lIdAtribProce, m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value), , g_ogrupo.Codigo, , , CDbl(vValor)
                        Case TiposDeAtributos.TipoFecha
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Add lIdAtribProce, m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value), , g_ogrupo.Codigo, , CDate(vValor)
                        Case TiposDeAtributos.TipoBoolean
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Add lIdAtribProce, m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value), , g_ogrupo.Codigo, BooleanToSQLBinary(vValor)
                    End Select
                ElseIf sOperacion = "U" Then
                    Select Case oatrib.Tipo
                        Case TiposDeAtributos.TipoString
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Item(sCod).valorText = vValor
                        Case TiposDeAtributos.TipoNumerico
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Item(sCod).valorNum = CDbl(vValor)
                        Case TiposDeAtributos.TipoFecha
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Item(sCod).valorFec = CDate(vValor)
                        Case TiposDeAtributos.TipoBoolean
                            m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").Value).AtribGrOfertados.Item(sCod).valorBool = BooleanToSQLBinary(vValor)
                    End Select
            End If
            
        Case AmbItem
            If sOperacion = "D" Then
                If Not g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados Is Nothing Then
                    If Not g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(lIdAtribProce)) Is Nothing Then
                        g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Remove (CStr(g_oItemSeleccionado.Id) & "$" & CStr(lIdAtribProce))
                    End If
                End If
                
            ElseIf sOperacion = "I" Then
                If g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados Is Nothing Then
                    Set g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados = oFSGSRaiz.Generar_CAtributosOfertados
                End If
                Select Case oatrib.Tipo
                    Case TiposDeAtributos.TipoString
                        g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Add lIdAtribProce, g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)), g_oItemSeleccionado.Id, g_ogrupo.Codigo, , , , , vValor
                    Case TiposDeAtributos.TipoNumerico
                        g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Add lIdAtribProce, g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)), g_oItemSeleccionado.Id, g_ogrupo.Codigo, , , CDbl(vValor)
                    Case TiposDeAtributos.TipoFecha
                        g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Add lIdAtribProce, g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)), g_oItemSeleccionado.Id, g_ogrupo.Codigo, , CDate(vValor)
                    Case TiposDeAtributos.TipoBoolean
                        g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Add lIdAtribProce, g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)), g_oItemSeleccionado.Id, g_ogrupo.Codigo, BooleanToSQLBinary(vValor)
                End Select
                
            ElseIf sOperacion = "U" Then
                Select Case oatrib.Tipo
                    Case TiposDeAtributos.TipoString
                        g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(lIdAtribProce)).valorText = vValor
                    Case TiposDeAtributos.TipoNumerico
                        g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(lIdAtribProce)).valorNum = CDbl(vValor)
                    Case TiposDeAtributos.TipoFecha
                        g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(lIdAtribProce)).valorFec = CDate(vValor)
                    Case TiposDeAtributos.TipoBoolean
                        g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(lIdAtribProce)).valorBool = BooleanToSQLBinary(vValor)
                End Select
            End If
        End Select
        
        If oatrib.ambito = AmbProceso Or oatrib.ambito = AmbGrupo Then
            g_oOrigen.m_bActualizarGeneral = True
        End If
        
        'Si nos posicionamos en la columna de un atributo aplicable al precio , y est� aplicado habr� que recargar la grid::
        If Not m_oAtribsFormulas.Item(CStr(lIdAtribProce)) Is Nothing Then
            bCalcular = False
            If oatrib.PrecioAplicarA = TipoAplicarAPrecio.TotalGrupo Then
                If m_oAtribsFormulas.Item(CStr(lIdAtribProce)).AtribTotalGrupo.Item(CStr(g_ogrupo.Codigo)).UsarPrec = 1 Then bCalcular = True
            Else
                If m_oAtribsFormulas.Item(CStr(lIdAtribProce)).UsarPrec = 1 Then bCalcular = True
            End If
            If bCalcular = True Then
                bRestaurar = True
                CalcularPrecioConFormulas
                sCod = ComprobarAdjudicacion(sdbgAdj.Columns("PROVECOD").CellValue(vbm))
                If sCod <> "" Or IsNull(sCod) Then 'Si es "" no existe ni la coleccion de adjudicaciones
                    If Not IsNull(sCod) Then
                        RecalcularCantidades True
                    End If
                    'Al modificar los atributos aplicados hay que calcular los nuevos Consumido,Adjudicado,Ahorrado
                    RecalcularCantidades
                    m_bHayAtribsDeGrupoOProce = False
                End If
                If m_oProcesoSeleccionado.Estado >= ConObjetivosSinNotificarYPreadjudicado And m_oProcesoSeleccionado.Estado < Cerrado Then
                    sCod = sdbgAdj.Columns("PROVECOD").CellValue(vbm) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdj.Columns("PROVECOD").CellValue(vbm)))
                    sCod = CStr(g_oItemSeleccionado.Id) & sCod
                    If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        teserror = m_oProcesoSeleccionado.ActualizarTotalesVisor(g_oOrigen.m_dblAdjudicadoProcReal, g_oOrigen.m_dblConsumidoProcReal)
                    End If
                End If
            End If
        End If
    End If
    
    Set oatrib = Nothing
    
    '''''''''''''''''''
    If m_oProcesoSeleccionado.Bloqueado Then
        If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
            If g_oItemSeleccionado.Cerrado = False Then
                cmdGuardar.Enabled = True
                m_oProcesoSeleccionado.GuardarProceso = True
                m_bRecargarOrigen = True
            End If
        ElseIf m_oProcesoSeleccionado.Estado < conadjudicaciones Then
            cmdGuardar.Enabled = True
            m_oProcesoSeleccionado.GuardarProceso = True
            m_bRecargarOrigen = True
        End If
    End If

    If m_oProcesoSeleccionado.Estado >= conadjudicaciones Then
        m_oProcesoSeleccionado.ModificadoHojaAdj = True
    End If

    sCod = ComprobarAdjudicacion(sdbgAdj.Columns("PROVECOD").CellValue(vbm))
    If sCod <> "" Or IsNull(sCod) Then
        If bRestaurar Then
            CargarGridAdjudicaciones
        End If
        
        dblConsumido = DevolverImporteConsumido
        dblAhorrado = dblConsumido - g_oItemSeleccionado.ImporteAdj 'ImporteAdj en moneda proceso
        CargarGridResultados dblConsumido, dblAhorrado, g_oItemSeleccionado.ImporteAdj
    End If
    
    sdbgAdj.Update
    
    g_oOrigen.cmdGuardar.Enabled = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdj_AfterColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
                    
End Sub

''' <summary>
''' Controlar q no se pueda cambiar la adjudicaci�n de un proceso cerrado pq NO cambia las datos de la pantalla
''' q llama pero si las colecciones
''' </summary>
''' <param name="ColIndex">Indice de la columna cambiada</param>
''' <param name="OldValue">Valor anterior</param>
''' <param name="Cancel">Para deshacer el cambio</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub sdbgAdj_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    'Cuando Cerrado, NO cambia las datos de la pantalla q llama pero si las colecciones? Eso esta mal
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado.Estado >= conadjudicaciones Then
        Cancel = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdj_BeforeColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdj_ColMove(ByVal ColIndex As Integer, ByVal NewPos As Integer, Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If ColIndex = 0 Then Cancel = True
    If NewPos = 0 Then Cancel = True
    
    g_oVistaSeleccionada.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdj_ColMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbgAdj_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_oVistaSeleccionada.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdj_ColResize", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Hacer doble click sobre una columna sirve para ver un detalle de lo q pulses (proveedor) � para adjudicar
''' al 100%
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbgAdj_DblClick()
    Dim i As Integer
    Dim sCod As Variant
    Dim iRow As Integer
    Dim iCol As Integer
    Dim dblImporteAdjRestar As Double 'Importe adjudicado anterior al cambio que se resta de los totales de grupo y proceso
    Dim dblImporteConsRestar As Double 'Importe consumido anterior al cambio que se resta de los totales de grupo y proceso
    Dim scod1 As String
    Dim dblAbierto  As Double
    Dim dblPorcen As Double
    Dim dblPrecio As Double
    Dim dblCantUsado As Double
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgAdj.col = -1 Then Exit Sub
    
    'En columna PROVE ver detalle del proveedor
    If sdbgAdj.Columns(sdbgAdj.col).Name = "PROVE" Then
        MostrarDetalleProveedor (sdbgAdj.Columns("PROVECOD").Value)
        Exit Sub
    End If
    
    If g_oItemSeleccionado.Cerrado Then Exit Sub
        
    'Cuando Cerrado, NO cambia las datos de la pantalla q llama pero si las colecciones? Eso esta mal
    If m_oProcesoSeleccionado.Estado >= conadjudicaciones Then
        Exit Sub
    End If
        
    'Adjudica el 100% del item al proveedor en el que se hace doble click en la col cantidad
    iCol = sdbgAdj.col
    'Si no tiene precio no se puede adjudicar
    If sdbgAdj.Columns("PRECIO").Value = "" Then Exit Sub
         
    'Compruebo que no supere la cantidad m�xima si procede
    If m_oProcesoSeleccionado.SolicitarCantMax Then
        If Trim(sdbgAdj.Columns("CANTMAX").Value) <> "" Then
            If CDec((StrToDbl0(sdbgAdj.Columns("CANTADJ").Value)) > CDec(StrToDbl0(sdbgAdj.Columns("CANTMAX").Value))) Then
                oMensajes.CantidadMaximaProveedorSuperada StrToDbl0(sdbgAdj.Columns("CANTMAX").Value), StrToDbl0(sdbgAdj.Columns("CANTADJ").Value)
                sdbgAdj.CancelUpdate
                Exit Sub
            End If
        End If
    End If
    
    'Si hago dblclick en el que ya esta al 100% no hago nada
    sCod = ComprobarAdjudicacion(sdbgAdj.Columns("PROVECOD").Value)
    If Not IsNull(sCod) And sCod <> "" Then
        If CDec(g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje) = 100 Then
            sdbgAdj.CancelUpdate
            Exit Sub
        End If
    End If
    
    'No hace falta comprobar los atributos de total item pq estamos adjudicando todo a un proveedor
    m_bDblClick = True
    m_iInd = 0
    Erase m_vEnPedidos
    'Primero tengo que quitar los de los dem�s
     dblCantUsado = 0
    'Carga en la colecci�n las modificaciones
    dblImporteAdjRestar = 0 'Ser� la cantidad que tenemos que restar de total de grupo y proceso
    dblImporteConsRestar = 0
    iRow = sdbgAdj.AddItemRowIndex(sdbgAdj.Bookmark)
    For i = 0 To sdbgAdj.Rows - 1
        If i <> iRow Then 'El resto de los proves
            sdbgAdj.Bookmark = sdbgAdj.AddItemBookmark(i)
            If m_oProcesoSeleccionado.Estado < conadjudicaciones And m_oProcesoSeleccionado.Pedidos > PPSinPedidos Then
                If EliminarAdjudicacionProve(g_ogrupo.Codigo, g_oItemSeleccionado.Id, sdbgAdj.Columns("PROVECOD").Value) Then
                    'Resto a los totales de prove lo que voy a desadjudicar
                    If sdbgAdj.Columns("CANTADJ").Value <> "" Then
                        'g_ogrupo est� en moneda proceso
                        g_ogrupo.Consumido = g_ogrupo.Consumido - sdbgAdj.Columns("CONSUM").Value
                        scod1 = g_ogrupo.Codigo & Trim(sdbgAdj.Columns("PROVECOD").Value) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(Trim(sdbgAdj.Columns("PROVECOD").Value)))
                        'm_oAdjs, adjs de grupo est� en moneda oferta
                        If Not m_oAdjs.Item(scod1) Is Nothing Then
                            m_oAdjs.Item(scod1).Consumido = m_oAdjs.Item(scod1).Consumido - CDec(sdbgAdj.Columns("CONSUM").Value * g_oItemSeleccionado.CambioComparativa(Trim(sdbgAdj.Columns("PROVECOD").Value)))
                            m_oAdjs.Item(scod1).ConsumidoMonProce = m_oAdjs.Item(scod1).ConsumidoMonProce - CDec(sdbgAdj.Columns("CONSUM").Value)
                        End If
                    End If
                    sdbgAdj.Columns("CANTADJ").Value = ""
                    sdbgAdj.Columns("AHO").Value = ""
                    sdbgAdj.Columns("AHO_PORCEN").Value = ""
                    sdbgAdj.Columns("CONSUM").Value = ""
                    sdbgAdj.Columns("ADJ").Value = ""
                    
                    sCod = ComprobarAdjudicacion(sdbgAdj.Columns("PROVECOD").Value)
                    If Not IsNull(sCod) And sCod <> "" Then
'                            g_ogrupo.adjudicaciones.Remove sCod
                        LimpiarAdjudicaciones
                    End If
                Else
                    dblCantUsado = dblCantUsado + sdbgAdj.Columns("CANTADJ").Value
                End If
            Else
                'Resto a los totales de prove lo que voy a desadjudicar
                If sdbgAdj.Columns("CANTADJ").Value <> "" Then
                    'g_ogrupo est� en moneda proceso
                    g_ogrupo.Consumido = g_ogrupo.Consumido - sdbgAdj.Columns("CONSUM").Value
                    scod1 = g_ogrupo.Codigo & Trim(sdbgAdj.Columns("PROVECOD").Value) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(Trim(sdbgAdj.Columns("PROVECOD").Value)))
                    'm_oAdjs, adjs de grupo est� en moneda oferta
                    If Not m_oAdjs.Item(scod1) Is Nothing Then
                        m_oAdjs.Item(scod1).Consumido = m_oAdjs.Item(scod1).Consumido - (sdbgAdj.Columns("CONSUM").Value * g_oItemSeleccionado.CambioComparativa(Trim(sdbgAdj.Columns("PROVECOD").Value)))
                        m_oAdjs.Item(scod1).ConsumidoMonProce = m_oAdjs.Item(scod1).ConsumidoMonProce - (sdbgAdj.Columns("CONSUM").Value)
                    End If
                End If
                sdbgAdj.Columns("CANTADJ").Value = ""
                sdbgAdj.Columns("AHO").Value = ""
                sdbgAdj.Columns("AHO_PORCEN").Value = ""
                sdbgAdj.Columns("CONSUM").Value = ""
                sdbgAdj.Columns("ADJ").Value = ""
                
                sCod = ComprobarAdjudicacion(sdbgAdj.Columns("PROVECOD").Value)
                If Not IsNull(sCod) And sCod <> "" Then
                    LimpiarAdjudicaciones
                End If
            End If
        End If
    Next
    sdbgAdj.Bookmark = sdbgAdj.AddItemBookmark(iRow)
    If m_iInd > 0 Then
        oMensajes.ImposibleCambiarAdjudicacion m_vEnPedidos, 1
        m_bDblClick = False
    End If
    If CDec(dblCantUsado) > 0 Then
        dblPorcen = CDec((g_oItemSeleccionado.Cantidad - dblCantUsado) * 100 / g_oItemSeleccionado.Cantidad)
    Else
        dblPorcen = 100
    End If

    If CDec(dblPorcen) > 0 Then
        RecalcularCantidades True
        sCod = ComprobarAdjudicacion(sdbgAdj.Columns("PROVECOD").Value)
        If sCod <> "" Or IsNull(sCod) Then
            If ComprobarOferta(sdbgAdj.Columns("PROVECOD").Value) Then
                'dblPrecio en moneda de la oferta para a�adir a Cadjudicaciones
                dblPrecio = g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta
            Else
                dblPrecio = StrToDbl0(sdbgAdj.Columns("PRECIO").Value)
            End If
            If IsNull(sCod) Then
                g_ogrupo.Adjudicaciones.Add sdbgAdj.Columns("PROVECOD").Value, sdbgAdj.Columns("NUMOFE").Value, g_oItemSeleccionado.Id, dblPrecio, dblPorcen, vCambio:=g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").Value)).Cambio
                sCod = sdbgAdj.Columns("PROVECOD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdj.Columns("PROVECOD").Value))
                sCod = CStr(g_oItemSeleccionado.Id) & sCod
            Else
                g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje = dblPorcen
            End If
            g_ogrupo.Adjudicaciones.Item(sCod).ImporteAdj = CDec(dblPrecio * (g_oItemSeleccionado.Cantidad - dblCantUsado))
        End If
        
        
        sdbgAdj.Bookmark = sdbgAdj.AddItemBookmark(iRow)
        sdbgAdj.col = iCol
        sdbgAdj.Columns("CANTADJ").Value = g_oItemSeleccionado.Cantidad - dblCantUsado
        
        RecalcularCantidades
    
        sdbgAdj.Update
        m_bDblClick = False
        
        If m_oProcesoSeleccionado.Bloqueado Then
            If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
                If g_oItemSeleccionado.Cerrado = False Then
                    cmdGuardar.Enabled = True
                    m_oProcesoSeleccionado.GuardarProceso = True
                    m_bRecargarOrigen = True
                End If
            ElseIf m_oProcesoSeleccionado.Estado < conadjudicaciones Then
                cmdGuardar.Enabled = True
                m_oProcesoSeleccionado.GuardarProceso = True
                m_bRecargarOrigen = True
            End If
        End If
        
        If m_oProcesoSeleccionado.Estado >= conadjudicaciones Then
            m_oProcesoSeleccionado.ModificadoHojaAdj = True
        End If
        
        'Grid de Resultados
        dblAbierto = CDec(g_oItemSeleccionado.Cantidad * g_oItemSeleccionado.Precio)
        sdbgResultados.RemoveAll
        sdbgResultados.AddItem m_sProceso & Chr(m_lSeparador) & g_dblAbiertoProc & Chr(m_lSeparador) & g_dblConsumidoProc & Chr(m_lSeparador) & g_dblAdjudicadoProc & Chr(m_lSeparador) & g_dblAhorradoProc & Chr(m_lSeparador) & g_dblAhorradoPorcenProc
        dblPorcen = 0
        If CDec(g_ogrupo.Consumido) <> 0 Then
            dblPorcen = CDec(((g_ogrupo.Consumido - g_ogrupo.AdjudicadoTotal) / Abs(g_ogrupo.Consumido)) * 100)
        End If
        sdbgResultados.AddItem m_sLitGrupo & Chr(m_lSeparador) & g_ogrupo.Abierto & Chr(m_lSeparador) & g_ogrupo.Consumido & Chr(m_lSeparador) & g_ogrupo.AdjudicadoTotal & Chr(m_lSeparador) & (g_ogrupo.Consumido - g_ogrupo.AdjudicadoTotal) & Chr(m_lSeparador) & dblPorcen
        dblPorcen = 0
        If CDec(StrToDbl0(sdbgAdj.Columns("CONSUM").Value)) <> 0 Then
            dblPorcen = CDec((sdbgAdj.Columns("AHO").Value / Abs(sdbgAdj.Columns("CONSUM").Value)) * 100)
        End If
        sdbgResultados.AddItem m_sItem & Chr(m_lSeparador) & dblAbierto & Chr(m_lSeparador) & sdbgAdj.Columns("CONSUM").Value & Chr(m_lSeparador) & sdbgAdj.Columns("ADJ").Value & Chr(m_lSeparador) & sdbgAdj.Columns("AHO").Value & Chr(m_lSeparador) & dblPorcen
    End If
    m_bDblClick = False
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdj_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
''' <summary>
''' Recalcular Cantidades
''' </summary>
''' <param name="bRestar">Restar o no</param>
''' <remarks>Llamada desde: sdbgAdj_AfterColUpdate ; Tiempo m�ximo: 0,2</remarks>
Private Sub RecalcularCantidades(Optional ByVal bRestar As Boolean)
    Dim sCod As String
    Dim dblVar As Double
    Dim dblCons As Double
    Dim dblCambio As Double
    Dim dImporteOfe As Double
    Dim dAhorroOfe As Double
    Dim dAhorroOfePorcen As Double
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRestar Then
        AplicarAtributosTotalItem
        sCod = sdbgAdj.Columns("PROVECOD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdj.Columns("PROVECOD").Value))
        sCod = g_oItemSeleccionado.Id & sCod
        dblVar = 0
        dblCambio = g_oItemSeleccionado.CambioComparativa(Trim(sdbgAdj.Columns("PROVECOD").Value))
        
        If Not g_ogrupo.Adjudicaciones Is Nothing Then
            If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                'dblVar en moneda oferta
                dblVar = g_ogrupo.Adjudicaciones.Item(sCod).ImporteAdjTot
            End If
        End If
        
        If sdbgAdj.Columns("CANTADJ").Value = "" Then
            sdbgAdj.Columns("ADJ").Value = IIf(dblVar = 0, "", dblVar / dblCambio)
        Else
            sdbgAdj.Columns("ADJ").Value = dblVar / dblCambio
        End If
        'dblVar en moneda proceso
        dblVar = (StrToDbl0(sdbgAdj.Columns("CANTADJ").Value) * g_oItemSeleccionado.Precio)
        If sdbgAdj.Columns("CANTADJ").Value = "" Then
            sdbgAdj.Columns("CONSUM").Value = IIf(dblVar = 0, "", dblVar)
        Else
            sdbgAdj.Columns("CONSUM").Value = dblVar
        End If
        If sdbgAdj.Columns("ADJ").Value <> "" And sdbgAdj.Columns("CONSUM").Value <> "" Then
            sdbgAdj.Columns("AHO").Value = sdbgAdj.Columns("CONSUM").Value - sdbgAdj.Columns("ADJ").Value
            If sdbgAdj.Columns("CONSUM").Value = 0 Then
                sdbgAdj.Columns("AHO_PORCEN").Value = 0
            Else
                sdbgAdj.Columns("AHO_PORCEN").Value = (sdbgAdj.Columns("AHO").Value / Abs(sdbgAdj.Columns("CONSUM").Value)) * 100
            End If
        Else
            sdbgAdj.Columns("AHO").Value = ""
            sdbgAdj.Columns("AHO_PORCEN").Value = ""
        End If
        
        'Calcula el importe oferta,ahorro oferta y ahorro% oferta:
        If sdbgAdj.Columns("PRECIO").Value = "" Then
            sdbgAdj.Columns("IMP_OFE").Value = ""
            sdbgAdj.Columns("AHORRO_OFE").Value = ""
            sdbgAdj.Columns("AHORRO_OFE_PORCEN").Value = ""
        Else
            'Importe oferta:
            dImporteOfe = NullToDbl0(g_oItemSeleccionado.Cantidad * CDbl(sdbgAdj.Columns("PRECIO").Value))
            If Not g_ogrupo.Adjudicaciones Is Nothing Then
                If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                    If Not IsNull(g_ogrupo.Adjudicaciones.Item(sCod).importe) Then
                        dImporteOfe = g_ogrupo.Adjudicaciones.Item(sCod).importe / dblCambio
                    End If
                End If
            End If
            sdbgAdj.Columns("IMP_OFE").Value = dImporteOfe
            
            'Ahorro oferta:
            dAhorroOfe = (g_oItemSeleccionado.Cantidad * NullToDbl0(g_oItemSeleccionado.Precio)) - dImporteOfe
            sdbgAdj.Columns("AHORRO_OFE").Value = dAhorroOfe
                        
            ' %Ahorro oferta:
            If (g_oItemSeleccionado.Cantidad * NullToDbl0(g_oItemSeleccionado.Precio)) = 0 Then
                dAhorroOfePorcen = 0
            Else
                dAhorroOfePorcen = (dAhorroOfe / Abs((g_oItemSeleccionado.Cantidad * NullToDbl0(g_oItemSeleccionado.Precio)))) * 100
            End If
            sdbgAdj.Columns("AHORRO_OFE_PORCEN").Value = dAhorroOfePorcen
        End If
                    

        'Sumo a los totales de prove lo que voy a adjudicar
        'Consumido grupo en moneda proceso
        g_ogrupo.Consumido = g_ogrupo.Consumido + StrToDbl0(sdbgAdj.Columns("CONSUM").Value)
        sCod = g_ogrupo.Codigo & Trim(sdbgAdj.Columns("PROVECOD").Value) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(Trim(sdbgAdj.Columns("PROVECOD").Value)))
        If Not m_oAdjs.Item(sCod) Is Nothing Then
            'Consumido de adj grupo en moneda oferta
            m_oAdjs.Item(sCod).Consumido = m_oAdjs.Item(sCod).Consumido + (StrToDbl0(sdbgAdj.Columns("CONSUM").Value) * m_oAdjs.Item(sCod).Cambio)
            m_oAdjs.Item(sCod).ConsumidoMonProce = m_oAdjs.Item(sCod).ConsumidoMonProce + (StrToDbl0(sdbgAdj.Columns("CONSUM").Value))
        End If
        AplicarAtributosTotalGrupo
        CalcularTotalesDeProceso
        
    Else 'Restamos la cantidad antigua
        sCod = sdbgAdj.Columns("PROVECOD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdj.Columns("PROVECOD").Value))
        sCod = g_oItemSeleccionado.Id & sCod
        If Not g_ogrupo.Adjudicaciones Is Nothing Then
            If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                If g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Then
                    'dblCOns en moneda proceso
                    dblCons = (StrToDbl0(g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje) / 100) * g_oItemSeleccionado.Cantidad * g_oItemSeleccionado.Precio
                    g_ogrupo.Consumido = g_ogrupo.Consumido - dblCons
                End If
            End If
        End If
        sCod = g_ogrupo.Codigo & Trim(sdbgAdj.Columns("PROVECOD").Value) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(Trim(sdbgAdj.Columns("PROVECOD").Value)))
        If Not m_oAdjs.Item(sCod) Is Nothing Then
            'm_oAdjs.Item(scod).Consumido en moneda oferta
            m_oAdjs.Item(sCod).Consumido = m_oAdjs.Item(sCod).Consumido - (dblCons * m_oAdjs.Item(sCod).Cambio)
            m_oAdjs.Item(sCod).ConsumidoMonProce = m_oAdjs.Item(sCod).ConsumidoMonProce - dblCons
        End If
    End If
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "RecalcularCantidades", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Recalcula las cantidades</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="bAdjudicar">Indica si se trata de una adjudicaci�n o desadjudicaci�n</param>
''' <param name="lItem">Id del item que se est� modificando</param>
''' <param name="sProv">C�digo del proveedor</param>
''' <param name="lIdEscalado">Id del escalado</param>
''' <param name="dblCantEsc">Cantidad adjudicada al escalado</param>
''' <param name="dblCantProve">Cantidad adjudicada al proveedor</param>
''' <remarks>Llamada desde: AdjudicarEscalado, DesadjudicarEscalado</remarks>
''' <revision>LTG 02/11/2011</revision>

Private Sub RecalcularCantidadesEscPorEscalado(ByRef oGrupo As CGrupo, ByVal bAdjudicar As Boolean, ByVal lItem As Long, ByVal sProv As String, ByVal lIdEscalado As Long, _
        Optional ByVal dblCantEsc As Variant, Optional ByVal dblCantProve As Variant)
    Dim sCod As String
    Dim scod1 As String
    Dim scodProve As String
    Dim oAdj As CAdjudicacion
    Dim oAdjProve As CAdjudicacion
    Dim oAdjProveReal As CAdjudicacion
    Dim oOferta As COferta
    Dim oEscalado As CEscalado
    Dim oEsc As CEscalado
    Dim oProve As CProveedor
    Dim dblPorcenAnt As Double
    Dim vPrecio As Variant
    Dim vObjetivo As Variant
    Dim dblCantidadEscalado As Double
    Dim dblImporteEscalado As Double
    Dim dblImporteAux As Double
    Dim bHayOtrosEscAdj As Boolean
    Dim dblCantidadProveedor As Double
    Dim dblImporteAdjudicadoProveedor As Double
    Dim dblImporteAdjudicadoProveedorTot As Double
    Dim dbConsumidoProve As Double
    Dim dblImporteAhorroProve As Double
    Dim dblPorcenAhorroProve As Double
    Dim vCantidadItem As Variant
    Dim dblCantidadItem As Double
    Dim dblImporteAdjudicadoItem As Double
    Dim dblImporteAhorroItem As Double
    Dim dblConsumidoItem As Double
    Dim dblPorcenAhorroItem As Double
    Dim dblPorcen As Double
    Dim bDistintos As Boolean
    Dim bAdjudicado As Boolean
    Dim i As Integer
    Dim iRow As Integer
    Dim vBookmark As Variant
    Dim iNumAdjProve As Integer
    Dim iNumAdjEsc As Integer
    Dim dblCambio As Double
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bRecalculando = True
    
    'Obtenci�n de objetos
    scodProve = sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
    sCod = KeyEscalado(sProv, g_oItemSeleccionado.Id, lIdEscalado)
    scod1 = CStr(oGrupo.Codigo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
    Set oEscalado = g_oItemSeleccionado.Escalados.Item(CStr(lIdEscalado))
    'Si existe una adjudicaci�n a nivel de proveedor se elimina
    If Not g_ogrupo.Adjudicaciones.Item(CStr(lItem) & scodProve) Is Nothing Then
        g_ogrupo.Adjudicaciones.Remove (CStr(lItem) & scodProve)
    End If
    Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
    Set oOferta = oGrupo.UltimasOfertas.Item(Trim(sProv))
    Set oAdjProve = m_oAdjsProve.Item(CStr(lItem) & scodProve)
    Set oAdjProveReal = g_oAdjsProveEsc.Item(CStr(lItem) & scodProve)
    
    With g_oItemSeleccionado
        dblPorcenAnt = .PorcenCons
        dblCambio = .CambioComparativa(sProv, True, m_oAdjsProve)
        
        'Modifico las colecciones y luego las grids
        oGrupo.Adjudicado = oGrupo.Adjudicado - .ImporteAdj
        oGrupo.AdjudicadoReal = oGrupo.AdjudicadoReal - .ImporteAdj
        oGrupo.Consumido = oGrupo.Consumido - .Consumido
        
        'Restamos lo adjudicado al grupo para ese prove
        oGrupo.AdjudicadoTotal = oGrupo.AdjudicadoTotal - g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoMonProce
        
        'Si hab�a adjudicaci�n resto los datos anteriores y sumo los nuevos
        If Not oAdjProve Is Nothing Then
            If Not g_oOrigen.m_oAdjs.Item(scod1) Is Nothing Then
                vPrecio = .Precio
                vObjetivo = .Objetivo
                If IsNull(.Precio) Then
                    'Calcular el precio del item en funci�n de las adjudicaciones
                    PresupuestoYObjetivoItemConEscalados oGrupo, g_oItemSeleccionado, m_oProvesAsig, vPrecio, vObjetivo
                End If
                
                'Resto lo anterior, el consumido y el importe adjudicado con atribs de total item
                g_oOrigen.m_oAdjs.Item(scod1).Consumido = g_oOrigen.m_oAdjs.Item(scod1).Consumido - NullToDbl0(NullToDbl0(.Cantidad * dblPorcenAnt) / 100) * NullToDbl0(NullToDbl0(vPrecio) * dblCambio)
                g_oOrigen.m_oAdjs.Item(scod1).ConsumidoMonProce = g_oOrigen.m_oAdjs.Item(scod1).ConsumidoMonProce - (NullToDbl0(.Cantidad * dblPorcenAnt / 100) * NullToDbl0(vPrecio))
                g_oOrigen.m_oAdjs.Item(scod1).ConsumidoReal = g_oOrigen.m_oAdjs.Item(scod1).ConsumidoReal - NullToDbl0(NullToDbl0(.Cantidad * dblPorcenAnt) / 100) * NullToDbl0(NullToDbl0(vPrecio) * dblCambio)
                g_oOrigen.m_oAdjs.Item(scod1).ConsumidoRealMonProce = g_oOrigen.m_oAdjs.Item(scod1).ConsumidoRealMonProce - (NullToDbl0(.Cantidad * dblPorcenAnt / 100) * NullToDbl0(vPrecio))
                If Not oAdjProveReal Is Nothing Then
                    g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSin = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSin - oAdjProveReal.ImporteAdjTot
                    g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinMonProce = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinMonProce - (oAdjProveReal.ImporteAdjTot / dblCambio)
                    g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinReal = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinReal - oAdjProveReal.ImporteAdjTot
                    g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinRealMonProce = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinRealMonProce - (oAdjProveReal.ImporteAdjTot / dblCambio)
                    g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoItems = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoItems - oAdjProveReal.ImporteAdj
                    g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoItemsMonproce = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoItemsMonproce - (oAdjProveReal.ImporteAdj / dblCambio)
                End If
            End If
            'Datos de grupo
            'Importe del item sin atribs de total item
            .ImporteAdj = .ImporteAdj - CDec(oAdjProveReal.ImporteAdjTot / dblCambio)
        End If
        
        'Elimino el total de adjudicaciones de grupo para esta oferta.
        Dim objAdj As CAdjGrupo
        Dim scod2 As String
        For Each objAdj In g_oOrigen.m_oAdjs
            If objAdj.Prove = sProv Then
                scod2 = CStr(objAdj.Grupo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc - g_oOrigen.m_oAdjs.Item(scod2).Adjudicado
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce - g_oOrigen.m_oAdjs.Item(scod2).AdjudicadoMonProce
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal - g_oOrigen.m_oAdjs.Item(scod2).AdjudicadoReal
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce - g_oOrigen.m_oAdjs.Item(scod2).AdjudicadoRealMonProce
            End If
        Next
        
        If bAdjudicar Then
            If oAdj Is Nothing Then
                Set oAdj = oGrupo.Adjudicaciones.Add(sProv, oOferta.Num, .Id, oOferta.Lineas.Item(CStr(.Id)).Escalados.Item(CStr(lIdEscalado)).PrecioOferta, _
                                         100, , oOferta.Lineas.Item(CStr(.Id)).Escalados.Item(CStr(lIdEscalado)).PrecioOferta, _
                                         oOferta.Lineas.Item(CStr(.Id)).Escalados.Item(CStr(lIdEscalado)).Objetivo, , oGrupo.Codigo, , , , lIdEscalado)
            End If
            
            bHayOtrosEscAdj = False
            iNumAdjEsc = 0
            For Each oEsc In oGrupo.Escalados
                sCod = KeyEscalado(sProv, lItem, oEsc.Id)
                If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                    iNumAdjEsc = iNumAdjEsc + 1
                End If
            Next
            bHayOtrosEscAdj = (iNumAdjEsc > 0)
            
            If Not IsMissing(dblCantEsc) Then
                If Not IsNull(dblCantEsc) Then
                    dblCantidadEscalado = dblCantEsc
                End If
            Else
                dblCantidadEscalado = 0
            End If
            oAdj.Adjudicado = dblCantidadEscalado
            If Not IsNull(.Cantidad) And Not IsEmpty(.Cantidad) And dblCantidadEscalado > 0 Then
                oAdj.Porcentaje = (dblCantidadEscalado / .Cantidad) * 100
            ElseIf Not IsNull(.Cantidad) And Not IsEmpty(.Cantidad) And Not IsMissing(dblCantProve) Then
                oAdj.Porcentaje = ((dblCantProve / iNumAdjEsc) / .Cantidad) * 100
            ElseIf Not IsMissing(dblCantProve) Then
                oAdj.Porcentaje = (dblCantProve / iNumAdjEsc) / dblCantProve * 100
            End If
            dblPorcen = 0
            If Not IsNull(.Cantidad) And Not IsEmpty(.Cantidad) Then
                If Not IsMissing(dblCantProve) Then
                    dblPorcen = (dblCantProve / .Cantidad) * 100
                Else
                    dblPorcen = (CantidadAdjItemProvePorEscalado(oGrupo, lItem, sProv) / .Cantidad) * 100
                End If
            End If
            If oAdjProve Is Nothing Then
                Set oAdjProve = m_oAdjsProve.Add(sProv, oOferta.Num, lItem, , dblPorcen, , , , , oGrupo.Codigo)
            Else
                oAdjProve.Porcentaje = dblPorcen
            End If
            If oAdjProveReal Is Nothing Then
                Set oAdjProveReal = g_oAdjsProveEsc.Add(sProv, oOferta.Num, lItem, , dblPorcen, , , , , oGrupo.Codigo)
            Else
                oAdjProveReal.Porcentaje = dblPorcen
            End If
            
            If dblCantidadEscalado > 0 Then
                If Not IsNull(oGrupo.Items.Item(CStr(oAdj.Id)).PresupuestoEscalado(oAdj.Escalado)) Then
                    dblImporteEscalado = dblCantidadEscalado * oOferta.Lineas.Item(CStr(lItem)).Escalados.Item(CStr(lIdEscalado)).PrecioOferta
                Else
                    dblImporteEscalado = 0
                End If
            
                'Datos de la adjudicaci�n
                oAdj.ImporteAdj = dblImporteEscalado
                oAdj.importe = CDec(dblImporteEscalado / oOferta.Cambio)  'En moneda proceso?�
                If ComprobarAplicarAtribsItem(oGrupo.Codigo) Then       'Si esta todo el item a este prove
                    oAdj.ImporteAdjTot = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, oAdj.ImporteAdj, TotalItem, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, .Id, lIdEscalado)
                Else
                    oAdj.ImporteAdjTot = oAdj.ImporteAdj
                End If
            End If
        Else
            oGrupo.Adjudicaciones.Remove sCod
        End If
                        
        If Not bHayOtrosEscAdj Then
            dblCantidadProveedor = 0
            dblImporteAdjudicadoProveedor = 0
            dbConsumidoProve = 0
            dblImporteAhorroProve = 0
            dblPorcenAhorroProve = 0
        Else
            'C�lculos proveedor
            If Not IsNull(.Cantidad) And Not IsEmpty(.Cantidad) Then
                If Not IsMissing(dblCantProve) Then
                    oAdjProve.Porcentaje = (dblCantProve / .Cantidad) * 100
                Else
                    oAdjProve.Porcentaje = (CantidadAdjItemProvePorEscalado(oGrupo, lItem, sProv) / .Cantidad) * 100
                End If
            End If
            If IsMissing(dblCantProve) Then
                dblCantidadProveedor = CantidadProveedor(oGrupo, m_oProvesAsig, m_oAdjsProve, sProv, .Id, m_oAdjsProve.Item(CStr(g_oItemSeleccionado.Id) & scodProve).Porcentaje)
            Else
                dblCantidadProveedor = dblCantProve
            End If
            'Mon oferta
            dblImporteAdjudicadoProveedor = ImporteAdjudicadoProveedor(m_oProcesoSeleccionado, oGrupo.Adjudicaciones, Nothing, oGrupo, oOferta, sProv, lItem, dblCantidadProveedor)
            dblImporteAdjudicadoProveedorTot = ImporteAdjudicadoProveedor(m_oProcesoSeleccionado, oGrupo.Adjudicaciones, m_oAtribsFormulas, oGrupo, oOferta, sProv, lItem, dblCantidadProveedor)
            dbConsumidoProve = ConsumidoProveedor(oGrupo, sProv, .Id, dblCantidadProveedor)   'mon proceso
            dblImporteAhorroProve = dbConsumidoProve - CDec(dblImporteAdjudicadoProveedor / dblCambio)
            
            If dbConsumidoProve > 0 Then
                dblPorcenAhorroProve = (dblImporteAhorroProve / dbConsumidoProve) * 100
            Else
                dblPorcenAhorroProve = 0
            End If
        End If
        
        iNumAdjProve = NumAdjudicacionesProve(oGrupo, sProv, .Id)
        If iNumAdjProve = 0 Then
            m_oAdjsProve.Remove (CStr(lItem) & scodProve)
        End If
        
        If Not oAdjProve Is Nothing Then
            oAdjProve.Adjudicado = dblCantidadProveedor 'Se utiliza la prop. Adjudicado para guardar la cantidad
            If dblImporteAdjudicadoProveedor > 0 Then
                oAdjProve.PresUnitario = PresupuestoProveItemEscAdj(oGrupo, g_oItemSeleccionado, sProv, dblCantidadProveedor) * dblCambio
                oAdjProve.Precio = PrecioProveItemEscAdj(oGrupo, oOferta, g_oItemSeleccionado, sProv, dblCantidadProveedor)
                oAdjProve.ImporteAdj = dblImporteAdjudicadoProveedor 'En moneda oferta
                oAdjProve.importe = dblImporteAhorroProve * dblCambio 'Se utiliza la prop. Importe para guardar el ahorro
                oAdjProve.ImporteAdjTot = dbConsumidoProve * dblCambio 'Se utiliza la prop. ImporteAdjTot para guardar el importe seg�n los datos del item
                
            Else
                oAdjProve.PresUnitario = Null
                oAdjProve.Precio = Null
                oAdjProve.ImporteAdj = 0
                oAdjProve.importe = 0
                oAdjProve.ImporteAdjTot = 0
            End If
        End If
                                                    
        'C�lculos item
        'Sumar las cantidades adjudicadas a los proveedores
        vCantidadItem = .Cantidad
        'Sumar las cantidades adjudicadas a los proveedores
        For Each oProve In m_oProvesAsig
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            sCod = CStr(.Id) & scodProve
            
            If Not m_oAdjsProve Is Nothing Then
                If Not m_oAdjsProve.Item(sCod) Is Nothing Then
                    dblCantidadItem = dblCantidadItem + CDec(NullToDbl0(m_oAdjsProve.Item(sCod).Adjudicado) / .CambioComparativa(oProve.Cod))
                    dblImporteAdjudicadoItem = dblImporteAdjudicadoItem + CDec(NullToDbl0(m_oAdjsProve.Item(sCod).ImporteAdj) / .CambioComparativa(oProve.Cod))
                    dblImporteAhorroItem = dblImporteAhorroItem + CDec(NullToDbl0(m_oAdjsProve.Item(sCod).importe) / .CambioComparativa(oProve.Cod))
                    dblConsumidoItem = dblConsumidoItem + CDec(NullToDbl0(m_oAdjsProve.Item(sCod).ImporteAdjTot) / .CambioComparativa(oProve.Cod))
                End If
            End If
        Next
        If dblConsumidoItem > 0 Then dblPorcenAhorroItem = (dblImporteAhorroItem / dblConsumidoItem) * 100
               
        'Actualizar la colecci�n de adjudicaciones por proveedor (real)
        If Not oAdjProveReal Is Nothing Then
            If iNumAdjProve = 0 Then
                scodProve = oAdjProveReal.ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAdjProveReal.ProveCod))
                g_oAdjsProveEsc.Remove (CStr(lItem) & scodProve)
                
                oAdjProveReal.Adjudicado = 0
                oAdjProveReal.Porcentaje = 0
                oAdjProveReal.ImporteAdj = 0
                oAdjProveReal.importe = 0
                oAdjProveReal.ImporteAdjTot = 0
            Else
                If dblCantidadItem > 0 Then
                    dblPorcen = (dblCantidadProveedor / IIf(IsEmpty(vCantidadItem) Or IsNull(vCantidadItem), dblCantidadItem, vCantidadItem)) * 100
                End If
                
                oAdjProveReal.Adjudicado = dblCantidadProveedor
                oAdjProveReal.Porcentaje = dblPorcen
                oAdjProveReal.ImporteAdj = dblImporteAdjudicadoProveedor
                oAdjProveReal.importe = CDec(dblImporteAdjudicadoProveedor * dblCambio)
                g_oOrigen.m_dblImporteAux = oAdjProveReal.importe
                oAdjProveReal.ImporteAdjTot = dblImporteAdjudicadoProveedorTot
                oAdjProveReal.importe = g_oOrigen.m_dblImporteAux
                
                .ImporteAdj = .ImporteAdj + CDec(oAdjProveReal.ImporteAdjTot / dblCambio) 'Adjudicado del item con atributos
            End If
            
            'Si el item no tiene cantidad puede haber m�s adjudicaciones para otros proveedores. Hay que recalcular los porcentajes
            If IsEmpty(vCantidadItem) Or IsNull(vCantidadItem) Then
                For Each oProve In m_oProvesAsig
                    If oProve.Cod <> sProv Then
                        scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                        If Not g_oAdjsProveEsc.Item(CStr(lItem) & scodProve) Is Nothing And dblCantidadItem > 0 Then
                            m_oAdjsProve.Item(CStr(lItem) & scodProve).Porcentaje = (g_oAdjsProveEsc.Item(CStr(lItem) & scodProve).Adjudicado / dblCantidadItem) * 100
                        End If
                    End If
                Next
            End If
        End If
        
        'los totales objetivo: resto antes de cambiar la cantidad consumida del item
        oGrupo.ObjetivoImp = oGrupo.ObjetivoImp - CDec(.CantidadCons * NullToDbl0(vObjetivo))
        g_oOrigen.m_dblTotalProveObjAll = g_oOrigen.m_dblTotalProveObjAll - CDec(.CantidadCons * NullToDbl0(vObjetivo))
        If NullToDbl0(vObjetivo) = 0 Then
            'Si no has puesto objetivo. No te vale la cantidad a adjudicar pq NO hay precio sobre el que aplicar. NO precio NO importe
            oGrupo.ObjetivoAhorro = 0
            g_oOrigen.m_dblTotalAhorroProveObjAll = 0
        Else
            oGrupo.ObjetivoAhorro = oGrupo.ObjetivoAhorro - (CDec(NullToDbl0(vPrecio) * .CantidadCons) - CDec(NullToDbl0(vObjetivo) * .CantidadCons))
            g_oOrigen.m_dblTotalAhorroProveObjAll = g_oOrigen.m_dblTotalAhorroProveObjAll - (CDec(NullToDbl0(vPrecio) * .CantidadCons) - CDec(NullToDbl0(vObjetivo) * .CantidadCons))
        End If
                  
        PresupuestoYObjetivoItemConEscalados oGrupo, g_oItemSeleccionado, m_oProvesAsig, vPrecio, vObjetivo
          
        'Datos del item: cantidad consumida y consumido
        If IsNull(.Cantidad) Or IsEmpty(.Cantidad) Then
            .CantidadCons = 0
        Else
            If .Cantidad = 0 Then
                .CantidadCons = 0
            Else
                .CantidadCons = .CantidadCons - CDec(.Cantidad * dblPorcenAnt / 100) + CDec(.Cantidad * dblPorcen / 100)
            End If
        End If
        .PorcenCons = .PorcenCons - dblPorcenAnt + dblPorcen
        .Consumido = dblConsumidoItem

        'Calculo los totales objetivo
        oGrupo.ObjetivoImp = oGrupo.ObjetivoImp + CDec(.CantidadCons * NullToDbl0(vObjetivo))
        g_oOrigen.m_dblTotalProveObjAll = g_oOrigen.m_dblTotalProveObjAll + CDec(.CantidadCons * NullToDbl0(vObjetivo))
        If NullToDbl0(.Objetivo) = 0 Then
            'Si no has puesto objetivo. No te vale la cantidad a adjudicar pq NO hay precio sobre el que aplicar. NO precio NO importe
            oGrupo.ObjetivoAhorro = 0
            g_oOrigen.m_dblTotalAhorroProveObjAll = 0
        Else
            g_oOrigen.m_dblTotalAhorroProveObjAll = g_oOrigen.m_dblTotalAhorroProveObjAll + (dblConsumidoItem - (CDec(NullToDbl0(vObjetivo) * .CantidadCons)))
            oGrupo.ObjetivoAhorro = oGrupo.ObjetivoAhorro + (dblConsumidoItem - (CDec(NullToDbl0(vObjetivo) * .CantidadCons)))
        End If
        'Datos del grupo
        oGrupo.Adjudicado = oGrupo.Adjudicado + .ImporteAdj
        oGrupo.AdjudicadoReal = oGrupo.AdjudicadoReal + .ImporteAdj
        oGrupo.Consumido = oGrupo.Consumido + .Consumido
        'Datos de la adjudicacion prove-grupo
        If Not g_oOrigen.m_oAdjs.Item(scod1) Is Nothing Then
            With g_oOrigen.m_oAdjs.Item(scod1)
                'Le sumo lo nuevo
                If IsNull(vCantidadItem) Or IsEmpty(vCantidadItem) Then
                    .Consumido = .Consumido + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPrecio) * dblCambio))
                    .ConsumidoMonProce = .ConsumidoMonProce + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPrecio)))
                    .ConsumidoReal = .ConsumidoReal + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPrecio) * dblCambio))
                    .ConsumidoRealMonProce = .ConsumidoRealMonProce + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPrecio)))
                Else
                    .Consumido = .Consumido + CDec(CDec(g_oItemSeleccionado.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPrecio) * dblCambio))
                    .ConsumidoMonProce = .ConsumidoMonProce + CDec(CDec(g_oItemSeleccionado.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPrecio)))
                    .ConsumidoReal = .ConsumidoReal + CDec(CDec(g_oItemSeleccionado.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPrecio) * dblCambio))
                    .ConsumidoRealMonProce = .ConsumidoRealMonProce + CDec(CDec(g_oItemSeleccionado.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPrecio)))
                End If
                .AdjudicadoSin = .AdjudicadoSin + oAdjProveReal.ImporteAdjTot
                .AdjudicadoSinMonProce = .AdjudicadoSinMonProce + (oAdjProveReal.ImporteAdjTot / dblCambio)
                .AdjudicadoSinReal = .AdjudicadoSinReal + oAdjProveReal.ImporteAdjTot
                .AdjudicadoSinRealMonProce = .AdjudicadoSinRealMonProce + (oAdjProveReal.ImporteAdjTot / dblCambio)
                .AdjudicadoItems = .AdjudicadoItems + oAdjProveReal.ImporteAdj
                .AdjudicadoItemsMonproce = .AdjudicadoItemsMonproce + (oAdjProveReal.ImporteAdj / dblCambio)
                
                bDistintos = False
                bAdjudicado = True
                If Not m_oAtribsFormulas Is Nothing Then bAdjudicado = m_oAtribsFormulas.ComprobarAplicarAtribsGrupo(oGrupo.Codigo, sProv)
                If .AdjudicadoSin <> .AdjudicadoSinReal Then
                    bDistintos = True
                    If bAdjudicado Then
                        dblImporteAux = CDec(oGrupo.AdjudicadoReal * dblCambio)
                        'Voy a calcular el Adjudicado con atribs para el grupo pq si se pueden aplicar es pq est� adjudicado a este proveedor todo el grupo, sino es que no se pueden aplicar
                        .AdjudicadoReal = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, .AdjudicadoSinReal, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)
                        oGrupo.AdjudicadoTotalReal = CDec(dblImporteAux / dblCambio)
                        
                        'en moneda proceso
                        .AdjudicadoRealMonProce = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, .AdjudicadoSinRealMonProce, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                                    oGrupo.Codigo, , , oGrupo.CambioComparativa(sProv))
                    Else
                        .AdjudicadoReal = .AdjudicadoSinReal
                        .AdjudicadoRealMonProce = .AdjudicadoSinRealMonProce
                        oGrupo.AdjudicadoTotalReal = oGrupo.AdjudicadoReal
                    End If
                End If
                If bAdjudicado Then
                    dblImporteAux = CDec(oGrupo.Adjudicado * dblCambio)
                    'Voy a calcular el Adjudicado con atribs para el grupo
                    .Adjudicado = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, .AdjudicadoSin, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)
                    oGrupo.AdjudicadoTotal = oGrupo.AdjudicadoTotal + (.Adjudicado / dblCambio)
                    
                    'En moneda proceso
                    .AdjudicadoMonProce = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, .AdjudicadoSinMonProce, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                            oGrupo.Codigo, , , oGrupo.CambioComparativa(sProv))
                    If Not bDistintos Then
                        .AdjudicadoReal = .Adjudicado
                        .AdjudicadoRealMonProce = .AdjudicadoMonProce
                        oGrupo.AdjudicadoTotalReal = oGrupo.AdjudicadoTotal
                    End If
                Else
                    If Not bDistintos Then
                        .AdjudicadoReal = .AdjudicadoSinReal
                        .AdjudicadoRealMonProce = .AdjudicadoSinRealMonProce
                        oGrupo.AdjudicadoTotalReal = oGrupo.AdjudicadoReal
                    End If
                    .Adjudicado = .AdjudicadoSin
                    .AdjudicadoMonProce = .AdjudicadoSinMonProce
                    oGrupo.AdjudicadoTotal = oGrupo.Adjudicado
                End If
                .Ahorrado = .Consumido - .Adjudicado
                .AhorradoMonProce = .ConsumidoMonProce - .AdjudicadoMonProce
                If .Consumido <> 0 Then
                    .AhorradoPorcen = CDec(.Ahorrado / Abs(.Consumido)) * 100
                Else
                    .AhorradoPorcen = 0
                End If
                
                i = 1
                g_oOrigen.sdbgGruposProve.MoveFirst 'Me coloco en la fila correspondiente al grupo
                While g_oOrigen.sdbgGruposProve.Columns("COD").Value <> oGrupo.Codigo And i <= g_oOrigen.sdbgGruposProve.Rows
                    g_oOrigen.sdbgGruposProve.MoveNext
                    i = i + 1
                Wend
                If g_oOrigen.sdbgGruposProve.Columns("COD").Value = oGrupo.Codigo Then
                    g_oOrigen.sdbgGruposProve.Columns("Consumido" & sProv).Value = NullToStr(CDec(.ConsumidoMonProce))
                    g_oOrigen.sdbgGruposProve.Columns("Adjudicado" & sProv).Value = NullToStr(CDec(.AdjudicadoMonProce))
                    g_oOrigen.sdbgGruposProve.Columns("Ahorrado" & sProv).Value = NullToStr(CDec(.AhorradoMonProce))
                    g_oOrigen.sdbgGruposProve.Columns("Ahorro%" & sProv).Value = NullToStr(.AhorradoPorcen)
                    g_oOrigen.sdbgGruposProve.Update
                End If
                g_oOrigen.sdbgProveGrupos.MoveFirst
                i = 1
                While g_oOrigen.sdbgProveGrupos.Columns("COD").Value <> sProv And i <= g_oOrigen.sdbgProveGrupos.Rows
                    g_oOrigen.sdbgProveGrupos.MoveNext
                    i = i + 1
                Wend
                If g_oOrigen.sdbgProveGrupos.Columns("COD").Value = sProv Then
                    g_oOrigen.sdbgProveGrupos.Columns("Consumido" & oGrupo.Codigo).Value = NullToStr(CDec(.ConsumidoMonProce))
                    g_oOrigen.sdbgProveGrupos.Columns("Adjudicado" & oGrupo.Codigo).Value = NullToStr(CDec(.AdjudicadoMonProce))
                    g_oOrigen.sdbgProveGrupos.Columns("Ahorrado" & oGrupo.Codigo).Value = NullToStr(CDec(.AhorradoMonProce))
                    g_oOrigen.sdbgProveGrupos.Columns("Ahorro%" & oGrupo.Codigo).Value = NullToStr(.AhorradoPorcen)
                    g_oOrigen.sdbgProveGrupos.Update
                End If
                g_oOrigen.sdbgProveGrupos.MoveFirst
            End With
        End If
        
        'Guardo el nuevo total de adjudicaciones de grupo para esta oferta.
        For Each objAdj In g_oOrigen.m_oAdjs
            If objAdj.Prove = sProv Then
                scod2 = CStr(objAdj.Grupo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc = g_oOrigen.m_oAdjs.Item(scod2).Adjudicado
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal = g_oOrigen.m_oAdjs.Item(scod2).AdjudicadoReal
            End If
        Next

        'Suma a los datos del proceso
        g_oOrigen.m_dblConsumidoProc = g_oOrigen.m_dblConsumidoProc + .Consumido
        g_oOrigen.m_dblConsumidoProcReal = g_oOrigen.m_dblConsumidoProcReal + .Consumido
        g_oOrigen.m_dblAdjudicadoSinProc = g_oOrigen.m_dblAdjudicadoSinProc + oGrupo.AdjudicadoTotal
        g_oOrigen.m_dblAdjudicadoSinProcReal = g_oOrigen.m_dblAdjudicadoSinProcReal + oGrupo.AdjudicadoTotalReal
        g_oOrigen.m_dblAdjudicadoProc = g_oOrigen.m_dblAdjudicadoSinProc
        g_oOrigen.m_dblAdjudicadoProcReal = g_oOrigen.m_dblAdjudicadoSinProcReal
        g_oOrigen.AplicarAtributosDeProceso
        g_oOrigen.CargarGridResultados
        
        '************ ACTUALIZACION GRID ESCALADOS *************
        'Buscar la fila del proveedor
        iRow = sdbgAdjEsc.Row
        For i = 1 To m_oProvesAsig.Count
            vBookmark = sdbgAdjEsc.AddItemBookmark(i)
            If sdbgAdjEsc.Columns("ID").CellValue(vBookmark) = sProv Then
                sdbgAdjEsc.Row = i
                Exit For
            End If
        Next
        'Buscar el grupo del escalado
        For i = 2 To sdbgAdjEsc.Groups.Count - 1
            If sdbgAdjEsc.Columns("ESC" & i).Value = lIdEscalado Then Exit For
        Next
        If bAdjudicar Then
            'Escalados
            sdbgAdjEsc.Columns("ADJ" & i).Value = "1"
            If dblCantidadEscalado > 0 Then sdbgAdjEsc.Columns("ADJCANT" & i).Value = dblCantidadEscalado
            sdbgAdjEsc.Columns("ADJIMP" & i).Value = dblImporteEscalado / dblCambio
            'Proveedor
            sdbgAdjEsc.Columns("CANT").Value = CDec(dblCantidadProveedor)
            sdbgAdjEsc.Columns("IMP").Value = IIf(dblImporteAdjudicadoProveedor = 0, "", dblImporteAdjudicadoProveedor / dblCambio)
            If dblImporteAdjudicadoProveedor = 0 And dblImporteAhorroProve = 0 Then
                sdbgAdjEsc.Columns("AHORROIMP").Value = ""
            Else
                sdbgAdjEsc.Columns("AHORROIMP").Value = dblImporteAhorroProve
            End If
            If dblImporteAdjudicadoProveedor = 0 And dblPorcenAhorroProve = 0 Then
                sdbgAdjEsc.Columns("AHORROPORCEN").Value = ""
            Else
                sdbgAdjEsc.Columns("AHORROPORCEN").Value = dblPorcenAhorroProve
            End If
            sdbgAdjEsc.Columns("PRECAPE").Value = oAdjProve.PresUnitario / dblCambio
            If dblImporteAdjudicadoProveedor = 0 And oAdjProve.Precio = 0 Then
                sdbgAdjEsc.Columns("CODPROVEACTUAL").Value = ""
            Else
                sdbgAdjEsc.Columns("CODPROVEACTUAL").Value = oAdjProve.Precio / dblCambio
            End If
        Else
            'escalados
            sdbgAdjEsc.Columns("ADJ" & i).Value = "0"
            sdbgAdjEsc.Columns("ADJCANT" & i).Value = ""
            sdbgAdjEsc.Columns("ADJIMP" & i).Value = ""
            'proveedor
            If dblCantidadProveedor = 0 Then
                sdbgAdjEsc.Columns("CANT").Value = ""
            Else
                sdbgAdjEsc.Columns("CANT").Value = dblCantidadProveedor
            End If
            sdbgAdjEsc.Columns("IMP").Value = IIf(dblImporteAdjudicadoProveedor = 0, "", dblImporteAdjudicadoProveedor)
            If dblImporteAdjudicadoProveedor > 0 Then
                sdbgAdjEsc.Columns("AHORROIMP").Value = dblImporteAhorroProve
                sdbgAdjEsc.Columns("AHORROPORCEN").Value = dblPorcenAhorroProve
                sdbgAdjEsc.Columns("PRECAPE").Value = oAdjProve.PresUnitario
                sdbgAdjEsc.Columns("CODPROVEACTUAL").Value = oAdjProve.Precio
            Else
                sdbgAdjEsc.Columns("AHORROIMP").Value = ""
                sdbgAdjEsc.Columns("AHORROPORCEN").Value = ""
                sdbgAdjEsc.Columns("PRECAPE").Value = ""
                sdbgAdjEsc.Columns("CODPROVEACTUAL").Value = ""
            End If
        End If
        sdbgAdjEsc.Update
        'Item
        iRow = sdbgAdjEsc.Row
        sdbgAdjEsc.Row = 0
        sdbgAdjEsc.Columns("IMP").Value = IIf(dblImporteAdjudicadoItem = 0, "", dblImporteAdjudicadoItem)
        If dblImporteAdjudicadoItem = 0 And dblImporteAhorroItem = 0 Then
            sdbgAdjEsc.Columns("AHORROIMP").Value = ""
        Else
            sdbgAdjEsc.Columns("AHORROIMP").Value = dblImporteAhorroItem
        End If
        If dblImporteAdjudicadoItem = 0 And dblPorcenAhorroItem = 0 Then
            sdbgAdjEsc.Columns("AHORROPORCEN").Value = ""
        Else
            sdbgAdjEsc.Columns("AHORROPORCEN").Value = dblPorcenAhorroItem
        End If
        sdbgAdjEsc.Columns("PRECAPE").Value = IIf(.Consumido = 0, "", .Consumido)
        sdbgAdjEsc.Columns("ADJCANT" & i).Value = NullToStr(CantidadAdjItemEscalado(g_ogrupo, lItem, lIdEscalado, m_oProvesAsig))
        sdbgAdjEsc.Row = iRow
    End With
    
    m_bRecalculando = False
    
    Set oAdj = Nothing
    Set oAdjProve = Nothing
    Set oAdjProveReal = Nothing
    Set oOferta = Nothing
    Set oEscalado = Nothing
    Set oEsc = Nothing
    Set oProve = Nothing
    
    AplicarAtributosTotalGrupo
    CalcularTotalesDeProceso
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "RecalcularCantidadesEscPorEscalado", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Actualiza las colecciones de frmADJ para que los cambios efectuados en el control tengan reflejo en el formulario</summary>
''' <param name="g_ogrupo">Objeto grupo</param>
''' <param name="bAdjudicar">Indica si se trata de una adjudicaci�n o desadjudicaci�n</param>
''' <param name="lItem">Id del item que se est� modificando</param>
''' <param name="sProv">C�digo del proveedor</param>
''' <param name="dblCantProve">Cantidad adjudicada al proveedor</param>
''' <remarks>Llamada desde: AdjudicarEscalado, DesadjudicarEscalado</remarks>
''' <revision>LTG 09/05/2012</revision>

Private Sub RecalcularCantidadesEscPorProveedor(ByRef g_ogrupo As CGrupo, ByVal bAdjudicar As Boolean, ByVal lItem As Long, ByVal sProv As String, ByVal dblCantProve As Variant)
    Dim vCantidadItem As Variant
    Dim dblConsumidoProve As Double
    Dim oAdj As CAdjudicacion
    Dim oAdjProve As CAdjudicacion
    Dim oAdjProveReal As CAdjudicacion
    Dim oOferta As COferta
    Dim oProve As CProveedor
    Dim sCod As String
    Dim scod1 As String
    Dim scodProve As String
    Dim bDistintos As Boolean
    Dim bAdjudicado As Boolean
    Dim i As Integer
    Dim dblImporteAdjudicadoProveedor As Double
    Dim dblImporteAdjudicadoProveedorTot As Double
    Dim dblImporteAhorroProve As Double
    Dim dblPorcenAhorroProve As Double
    Dim iRow As Integer
    Dim dblCantidadItem As Double
    Dim dblImporteAdjudicadoItem As Double
    Dim dblImporteAhorroItem As Double
    Dim dblPorcenAhorroItem As Double
    Dim dblConsumidoItem As Double
    Dim dblPorcenAnt As Double
    Dim dblPorcen As Double
    Dim vPres As Variant
    Dim vObjetivo As Variant
    Dim dblImporteAux As Double
    Dim dblCambio As Double
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bRecalculando = True
    
    'Obtenci�n de objetos
    scodProve = sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
    Set oAdj = g_ogrupo.Adjudicaciones.Item(CStr(lItem) & scodProve)
    Set oOferta = g_ogrupo.UltimasOfertas.Item(Trim(sProv))
    Set oAdjProve = m_oAdjsProve.Item(CStr(lItem) & scodProve)
    Set oAdjProveReal = g_oAdjsProveEsc.Item(CStr(lItem) & scodProve)
    
    scod1 = CStr(g_ogrupo.Codigo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
    
    With g_oItemSeleccionado
        dblPorcenAnt = .PorcenCons
        dblCambio = .CambioComparativa(sProv)
        
        'Modifico las colecciones y luego las grids
        g_oOrigen.m_dblConsumidoProc = g_oOrigen.m_dblConsumidoProc - .Consumido
        g_oOrigen.m_dblConsumidoProcReal = g_oOrigen.m_dblConsumidoProcReal - .Consumido
        g_oOrigen.m_dblAdjudicadoSinProc = g_oOrigen.m_dblAdjudicadoSinProc - g_ogrupo.AdjudicadoTotal
        g_oOrigen.m_dblAdjudicadoSinProcReal = g_oOrigen.m_dblAdjudicadoSinProcReal - g_ogrupo.AdjudicadoTotalReal
        g_ogrupo.Adjudicado = g_ogrupo.Adjudicado - .ImporteAdj
        g_ogrupo.AdjudicadoReal = g_ogrupo.AdjudicadoReal - .ImporteAdj
        g_ogrupo.Consumido = g_ogrupo.Consumido - .Consumido
        
        'Restamos lo adjudicado al grupo para ese prove
        If Not g_oOrigen.m_oAdjs.Item(scod1) Is Nothing Then
            g_ogrupo.AdjudicadoTotal = g_ogrupo.AdjudicadoTotal - (g_oOrigen.m_oAdjs.Item(scod1).Adjudicado / dblCambio)
        End If
        
        'Si hab�a adjudicaci�n resto los datos anteriores y sumo los nuevos
        vPres = g_oItemSeleccionado.PresupuestoProveItemEsc(g_ogrupo.Escalados)
        vObjetivo = ObjetivoProveItemEsc(g_ogrupo, g_oItemSeleccionado)
        
        If Not oAdjProve Is Nothing Then
            If Not g_oOrigen.m_oAdjs.Item(scod1) Is Nothing Then
                'Resto lo anterior, el consumido y el importe adjudicado con atribs de total item
                g_oOrigen.m_oAdjs.Item(scod1).Consumido = g_oOrigen.m_oAdjs.Item(scod1).Consumido - NullToDbl0(NullToDbl0(.Cantidad * dblPorcenAnt) / 100) * NullToDbl0(NullToDbl0(vPres) * dblCambio)
                g_oOrigen.m_oAdjs.Item(scod1).ConsumidoMonProce = g_oOrigen.m_oAdjs.Item(scod1).ConsumidoMonProce - (NullToDbl0(.Cantidad * dblPorcenAnt) / 100 * NullToDbl0(vPres))
                g_oOrigen.m_oAdjs.Item(scod1).ConsumidoReal = g_oOrigen.m_oAdjs.Item(scod1).ConsumidoReal - NullToDbl0(NullToDbl0(.Cantidad * dblPorcenAnt) / 100) * NullToDbl0(NullToDbl0(vPres) * dblCambio)
                g_oOrigen.m_oAdjs.Item(scod1).ConsumidoRealMonProce = g_oOrigen.m_oAdjs.Item(scod1).ConsumidoRealMonProce - (NullToDbl0(.Cantidad * dblPorcenAnt / 100) * NullToDbl0(vPres))
                g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSin = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSin - oAdjProveReal.ImporteAdjTot
                g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinMonProce = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinMonProce - (oAdjProveReal.ImporteAdjTot / dblCambio)
                g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinReal = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinReal - oAdjProveReal.ImporteAdjTot
                g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinRealMonProce = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoSinRealMonProce - (oAdjProveReal.ImporteAdjTot / dblCambio)
                g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoItems = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoItems - oAdjProveReal.ImporteAdj
                g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoItemsMonproce = g_oOrigen.m_oAdjs.Item(scod1).AdjudicadoItemsMonproce - (oAdjProveReal.ImporteAdj / dblCambio)
            End If
            'Datos de grupo
            'Importe del item sin atribs de total item
            .ImporteAdj = .ImporteAdj - CDec(oAdjProveReal.ImporteAdjTot / dblCambio)
        End If
        
        'Elimino el total de adjudicaciones de grupo para esta oferta.
        Dim objAdj As CAdjGrupo
        Dim scod2 As String
        For Each objAdj In g_oOrigen.m_oAdjs
            If objAdj.Prove = sProv Then
                scod2 = CStr(objAdj.Grupo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc - g_oOrigen.m_oAdjs.Item(scod2).Adjudicado
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce - g_oOrigen.m_oAdjs.Item(scod2).AdjudicadoMonProce
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal - g_oOrigen.m_oAdjs.Item(scod2).AdjudicadoReal
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce - g_oOrigen.m_oAdjs.Item(scod2).AdjudicadoRealMonProce
            End If
        Next
        
        If bAdjudicar Then
            dblPorcen = 0
            If Not IsNull(.Cantidad) And Not IsEmpty(.Cantidad) Then
                dblPorcen = (dblCantProve / .Cantidad) * 100
            End If
            
            If oAdj Is Nothing Then
                Set oAdj = g_ogrupo.Adjudicaciones.Add(sProv, oOferta.Num, lItem, , dblPorcen, , , .Objetivo, , g_ogrupo.Codigo)
            Else
                oAdj.Porcentaje = dblPorcen
            End If
            If oAdjProve Is Nothing Then
                Set oAdjProve = m_oAdjsProve.Add(sProv, oOferta.Num, lItem, , dblPorcen, , , , , g_ogrupo.Codigo)
                Set oAdjProveReal = g_oAdjsProveEsc.Add(sProv, oOferta.Num, lItem, , dblPorcen, , , , , g_ogrupo.Codigo)
            Else
                oAdjProve.Porcentaje = dblPorcen
                oAdjProveReal.Porcentaje = dblPorcen
            End If
        Else
            g_ogrupo.Adjudicaciones.Remove CStr(lItem) & scodProve
            m_oAdjsProve.Remove CStr(lItem) & scodProve
            g_oAdjsProveEsc.Remove CStr(lItem) & scodProve
        End If
                                        
        'C�lculos proveedor
        dblImporteAdjudicadoProveedor = ImporteAdjudicadoProveedor(m_oProcesoSeleccionado, g_ogrupo.Adjudicaciones, Nothing, g_ogrupo, oOferta, sProv, lItem, dblCantProve)
        dblImporteAdjudicadoProveedorTot = ImporteAdjudicadoProveedor(m_oProcesoSeleccionado, g_ogrupo.Adjudicaciones, m_oAtribsFormulas, g_ogrupo, oOferta, sProv, lItem, dblCantProve)
        dblConsumidoProve = ConsumidoProveedor(g_ogrupo, sProv, lItem, dblCantProve)
        dblImporteAhorroProve = dblConsumidoProve - dblImporteAdjudicadoProveedor
        If dblConsumidoProve > 0 Then
            dblPorcenAhorroProve = (dblImporteAhorroProve / dblConsumidoProve) * 100
        Else
            dblPorcenAhorroProve = 0
        End If
        
        If Not oAdjProve Is Nothing Then
            oAdjProve.Adjudicado = dblCantProve 'Se utiliza la prop. Adjudicado para guardar la cantidad
            If dblImporteAdjudicadoProveedor > 0 Then
                oAdjProve.PresUnitario = g_oItemSeleccionado.PresupuestoProveItemEsc(g_ogrupo.Escalados)
                oAdjProve.Precio = PrecioProveItemEsc(g_ogrupo, oOferta, g_oItemSeleccionado)
                oAdjProve.ImporteAdj = dblImporteAdjudicadoProveedor
                oAdjProve.importe = dblImporteAhorroProve   'Se utiliza la prop. Importe para guardar el ahorro
                oAdjProve.ImporteAdjTot = dblConsumidoProve  'Se utiliza la prop. ImporteAdjTot para guardar el importe seg�n los datos del item
            Else
                oAdjProve.PresUnitario = Null
                oAdjProve.Precio = Null
                oAdjProve.ImporteAdj = Null
                oAdjProve.importe = Null
                oAdjProve.ImporteAdjTot = Null
            End If
        End If
                                                    
        'C�lculos item
        vCantidadItem = .Cantidad
        'Sumar las cantidades adjudicadas a los proveedores
        For Each oProve In m_oProvesAsig
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            sCod = CStr(lItem) & scodProve
            
            If Not m_oAdjsProve Is Nothing Then
                If Not m_oAdjsProve.Item(sCod) Is Nothing Then
                    dblCantidadItem = dblCantidadItem + NullToDbl0(m_oAdjsProve.Item(sCod).Adjudicado)
                    dblImporteAdjudicadoItem = dblImporteAdjudicadoItem + NullToDbl0(m_oAdjsProve.Item(sCod).ImporteAdj)
                    dblImporteAhorroItem = dblImporteAhorroItem + NullToDbl0(m_oAdjsProve.Item(sCod).importe)
                    dblConsumidoItem = dblConsumidoItem + NullToDbl0(m_oAdjsProve.Item(sCod).ImporteAdjTot)
                End If
            End If
        Next
        If dblConsumidoItem > 0 Then dblPorcenAhorroItem = (dblImporteAhorroItem / dblConsumidoItem) * 100
               
        'Actualizar la colecci�n de adjudicaciones por proveedor (real)
        If Not oAdjProveReal Is Nothing Then
            If bAdjudicar Then
                If dblCantidadItem > 0 Then
                    dblPorcen = (dblCantProve / IIf(IsEmpty(vCantidadItem) Or IsNull(vCantidadItem), dblCantidadItem, vCantidadItem)) * 100
                End If
                
                oAdjProveReal.Adjudicado = dblCantProve
                oAdjProveReal.Porcentaje = dblPorcen
                oAdjProveReal.ImporteAdj = dblImporteAdjudicadoProveedor
                oAdjProveReal.importe = CDec(dblImporteAdjudicadoProveedor * dblCambio)
                g_oOrigen.m_dblImporteAux = oAdjProveReal.importe
                oAdjProveReal.ImporteAdjTot = dblImporteAdjudicadoProveedorTot
                If ComprobarAplicarAtribsItem(g_ogrupo.Codigo) Then     'Si esta todo el item a este prove
                    oAdjProveReal.ImporteAdjTot = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, oAdjProveReal.ImporteAdj, TotalItem, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, g_ogrupo.Codigo, lItem)
                Else
                    oAdjProveReal.ImporteAdjTot = oAdjProveReal.ImporteAdj
                End If
                oAdjProveReal.importe = g_oOrigen.m_dblImporteAux
                
                .ImporteAdj = .ImporteAdj + CDec(oAdjProveReal.ImporteAdjTot / dblCambio) 'Adjudicado del item con atributos
                
                oAdj.Adjudicado = oAdjProveReal.Adjudicado
                oAdj.Porcentaje = oAdjProveReal.Porcentaje
                oAdj.ImporteAdj = oAdjProveReal.ImporteAdj
                oAdj.importe = oAdjProveReal.importe
                oAdj.ImporteAdjTot = oAdjProveReal.ImporteAdjTot
            End If
            
            'Si el item no tiene cantidad puede haber m�s adjudicaciones para otros proveedores. Hay que recalcular los porcentajes
            If IsEmpty(vCantidadItem) Or IsNull(vCantidadItem) Then
                For Each oProve In m_oProvesAsig
                    If oProve.Cod <> sProv Then
                        scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                        If Not m_oAdjsProve.Item(CStr(lItem) & scodProve) Is Nothing And dblCantidadItem > 0 Then
                            m_oAdjsProveEsc.Item(CStr(lItem) & scodProve).Porcentaje = (m_oAdjsProveEsc.Item(CStr(lItem) & scodProve).Adjudicado / dblCantidadItem) * 100
                        End If
                        
                        If Not g_ogrupo.Adjudicaciones.Item(CStr(lItem) & scodProve) Is Nothing And dblCantidadItem > 0 Then
                            g_ogrupo.Adjudicaciones.Item(CStr(lItem) & scodProve).Porcentaje = (m_oAdjsProveEsc.Item(CStr(lItem) & scodProve).Adjudicado / dblCantidadItem) * 100
                        End If
                    End If
                Next
            End If
        End If
        
        'los totales objetivo: resto antes de cambiar la cantidad consumida del item
        g_ogrupo.ObjetivoImp = g_ogrupo.ObjetivoImp - CDec(.CantidadCons * NullToDbl0(vObjetivo))
        g_oOrigen.m_dblTotalProveObjAll = g_oOrigen.m_dblTotalProveObjAll - CDec(.CantidadCons * NullToDbl0(vObjetivo))
        If NullToDbl0(vObjetivo) = 0 Then
            'Si no has puesto objetivo. No te vale la cantidad a adjudicar pq NO hay precio sobre el que aplicar. NO precio NO importe
            g_ogrupo.ObjetivoAhorro = 0
            g_oOrigen.m_dblTotalAhorroProveObjAll = 0
        Else
            g_ogrupo.ObjetivoAhorro = g_ogrupo.ObjetivoAhorro - (CDec(NullToDbl0(vPres) * .CantidadCons) - CDec(NullToDbl0(vObjetivo) * .CantidadCons))
            g_oOrigen.m_dblTotalAhorroProveObjAll = g_oOrigen.m_dblTotalAhorroProveObjAll - (CDec(NullToDbl0(vPres) * .CantidadCons) - CDec(NullToDbl0(vObjetivo) * .CantidadCons))
        End If
          
        'Datos del item: cantidad consumida y consumido
        If IsNull(.Cantidad) Or IsEmpty(.Cantidad) Then
            .CantidadCons = 0
        Else
            If .Cantidad = 0 Then
                .CantidadCons = 0
            Else
                .CantidadCons = .CantidadCons - CDec(.Cantidad * dblPorcenAnt / 100) + CDec(dblCantidadItem * dblPorcen / 100)
            End If
        End If
        .PorcenCons = .PorcenCons - dblPorcenAnt + dblPorcen
        .Consumido = dblConsumidoItem

        'Calculo los totales objetivo
        g_ogrupo.ObjetivoImp = g_ogrupo.ObjetivoImp + CDec(.CantidadCons * NullToDbl0(vObjetivo))
        g_oOrigen.m_dblTotalProveObjAll = g_oOrigen.m_dblTotalProveObjAll + CDec(.CantidadCons * NullToDbl0(vObjetivo))
        If NullToDbl0(.Objetivo) = 0 Then
            'Si no has puesto objetivo. No te vale la cantidad a adjudicar pq NO hay precio sobre el que aplicar. NO precio NO importe
            g_ogrupo.ObjetivoAhorro = 0
            g_oOrigen.m_dblTotalAhorroProveObjAll = 0
        Else
            g_oOrigen.m_dblTotalAhorroProveObjAll = g_oOrigen.m_dblTotalAhorroProveObjAll + (dblConsumidoItem - (CDec(NullToDbl0(vObjetivo) * .CantidadCons)))
            g_ogrupo.ObjetivoAhorro = g_ogrupo.ObjetivoAhorro + (dblConsumidoItem - (CDec(NullToDbl0(vObjetivo) * .CantidadCons)))
        End If
        'Datos del grupo
        g_ogrupo.Adjudicado = g_ogrupo.Adjudicado + .ImporteAdj
        g_ogrupo.AdjudicadoReal = g_ogrupo.AdjudicadoReal + .ImporteAdj
        g_ogrupo.Consumido = g_ogrupo.Consumido + .Consumido
        
        'Datos de la adjudicacion prove-grupo
        If Not g_oOrigen.m_oAdjs.Item(scod1) Is Nothing Then
            With g_oOrigen.m_oAdjs.Item(scod1)
                'Le sumo lo nuevo
                If IsNull(vCantidadItem) Or IsEmpty(vCantidadItem) Then
                    .Consumido = .Consumido + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPres) * dblCambio))
                    .ConsumidoMonProce = .ConsumidoMonProce + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPres)))
                    .ConsumidoReal = .ConsumidoReal + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPres) * dblCambio))
                    .ConsumidoRealMonProce = .ConsumidoRealMonProce + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPres)))
                Else
                    .Consumido = .Consumido + CDec(CDec(g_oItemSeleccionado.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPres) * dblCambio))
                    .ConsumidoMonProce = .ConsumidoMonProce + CDec(CDec(g_oItemSeleccionado.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPres)))
                    .ConsumidoReal = .ConsumidoReal + CDec(CDec(g_oItemSeleccionado.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPres) * dblCambio))
                    .ConsumidoRealMonProce = .ConsumidoRealMonProce + CDec(CDec(g_oItemSeleccionado.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPres)))
                End If
                If dblPorcen > 0 Then
                    .AdjudicadoSin = .AdjudicadoSin + oAdjProveReal.ImporteAdjTot
                    .AdjudicadoSinMonProce = .AdjudicadoSinMonProce + (oAdjProveReal.ImporteAdjTot / dblCambio)
                    .AdjudicadoSinReal = .AdjudicadoSinReal + oAdjProveReal.ImporteAdjTot
                    .AdjudicadoSinRealMonProce = .AdjudicadoSinRealMonProce + (oAdjProveReal.ImporteAdjTot / dblCambio)
                    .AdjudicadoItems = .AdjudicadoItems + oAdjProveReal.ImporteAdj
                    .AdjudicadoItemsMonproce = .AdjudicadoItemsMonproce + (oAdjProveReal.ImporteAdj / dblCambio)
                End If
                .importe = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, .AdjudicadoOfe, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, g_ogrupo.Codigo)
                .ImporteMonProce = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, .AdjudicadoOfeMonProce, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                        g_ogrupo.Codigo, , , g_ogrupo.CambioComparativa(sProv))
                bDistintos = False
                bAdjudicado = True
                If Not m_oAtribsFormulas Is Nothing Then bAdjudicado = m_oAtribsFormulas.ComprobarAplicarAtribsGrupo(g_ogrupo.Codigo, sProv)
                If .AdjudicadoSin <> .AdjudicadoSinReal Then
                    bDistintos = True
                    If bAdjudicado Then
                        dblImporteAux = CDec(g_ogrupo.AdjudicadoReal * dblCambio)
                        'Voy a calcular el Adjudicado con atribs para el grupo pq si se pueden aplicar es pq est� adjudicado a este proveedor todo el grupo, sino es que no se pueden aplicar
                        .AdjudicadoReal = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, .AdjudicadoSinReal, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, g_ogrupo.Codigo)
                        g_ogrupo.AdjudicadoTotalReal = CDec(dblImporteAux / dblCambio)
                        
                        'Moneda de proceso
                        .AdjudicadoRealMonProce = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, .AdjudicadoSinRealMonProce, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                                    g_ogrupo.Codigo, , , g_ogrupo.CambioComparativa(sProv))
                    Else
                        .AdjudicadoReal = .AdjudicadoSinReal
                        .AdjudicadoRealMonProce = .AdjudicadoSinRealMonProce
                        g_ogrupo.AdjudicadoTotalReal = g_ogrupo.AdjudicadoReal
                    End If
                End If
                If bAdjudicado Then
                    dblImporteAux = CDec(g_ogrupo.Adjudicado * dblCambio)
                    'Voy a calcular el Adjudicado con atribs para el grupo
                    .Adjudicado = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, .AdjudicadoSin, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, g_ogrupo.Codigo)
                    g_ogrupo.AdjudicadoTotal = g_ogrupo.AdjudicadoTotal + (.Adjudicado / dblCambio)
                    
                    'En moneda de proceso
                    .AdjudicadoMonProce = AplicarAtributos(m_oProcesoSeleccionado, m_oAtribsFormulas, .AdjudicadoSinMonProce, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                                g_ogrupo.Codigo, , , g_ogrupo.CambioComparativa(sProv))
                    
                    If Not bDistintos Then
                        .AdjudicadoReal = .Adjudicado
                        .AdjudicadoRealMonProce = .AdjudicadoMonProce
                        g_ogrupo.AdjudicadoTotalReal = g_ogrupo.AdjudicadoTotal
                    End If
                Else
                    If Not bDistintos Then
                        .AdjudicadoReal = .AdjudicadoSinReal
                        .AdjudicadoRealMonProce = .AdjudicadoSinRealMonProce
                        g_ogrupo.AdjudicadoTotalReal = g_ogrupo.AdjudicadoReal
                    End If
                    .Adjudicado = .AdjudicadoSin
                    .AdjudicadoMonProce = .AdjudicadoSinMonProce
                    g_ogrupo.AdjudicadoTotal = g_ogrupo.Adjudicado
                End If
                .Ahorrado = .Consumido - .Adjudicado
                .AhorradoMonProce = .ConsumidoMonProce - .AdjudicadoMonProce
                If .Consumido <> 0 Then
                    .AhorradoPorcen = CDec(.Ahorrado / Abs(.Consumido)) * 100
                Else
                    .AhorradoPorcen = 0
                End If
                
                i = 1
                g_oOrigen.sdbgGruposProve.MoveFirst 'Me coloco en la fila correspondiente al grupo
                While g_oOrigen.sdbgGruposProve.Columns("COD").Value <> g_ogrupo.Codigo And i <= g_oOrigen.sdbgGruposProve.Rows
                    g_oOrigen.sdbgGruposProve.MoveNext
                    i = i + 1
                Wend
                If g_oOrigen.sdbgGruposProve.Columns("COD").Value = g_ogrupo.Codigo Then
                    g_oOrigen.sdbgGruposProve.Columns("Consumido" & sProv).Value = NullToStr(CDec(.ConsumidoMonProce))
                    g_oOrigen.sdbgGruposProve.Columns("Adjudicado" & sProv).Value = NullToStr(CDec(.AdjudicadoMonProce))
                    g_oOrigen.sdbgGruposProve.Columns("Ahorrado" & sProv).Value = NullToStr(CDec(.AhorradoMonProce))
                    g_oOrigen.sdbgGruposProve.Columns("Ahorro%" & sProv).Value = NullToStr(.AhorradoPorcen)
                    g_oOrigen.sdbgGruposProve.Update
                End If
                g_oOrigen.sdbgProveGrupos.MoveFirst
                i = 1
                While g_oOrigen.sdbgProveGrupos.Columns("COD").Value <> sProv And i <= g_oOrigen.sdbgProveGrupos.Rows
                    g_oOrigen.sdbgProveGrupos.MoveNext
                    i = i + 1
                Wend
                If g_oOrigen.sdbgProveGrupos.Columns("COD").Value = sProv Then
                    g_oOrigen.sdbgProveGrupos.Columns("Consumido" & g_ogrupo.Codigo).Value = NullToStr(CDec(.ConsumidoMonProce))
                    g_oOrigen.sdbgProveGrupos.Columns("Adjudicado" & g_ogrupo.Codigo).Value = NullToStr(CDec(.AdjudicadoMonProce))
                    g_oOrigen.sdbgProveGrupos.Columns("Ahorrado" & g_ogrupo.Codigo).Value = NullToStr(CDec(.AhorradoMonProce))
                    g_oOrigen.sdbgProveGrupos.Columns("Ahorro%" & g_ogrupo.Codigo).Value = NullToStr(.AhorradoPorcen)
                    g_oOrigen.sdbgProveGrupos.Update
                End If
                g_oOrigen.sdbgProveGrupos.MoveFirst
            End With
        End If
        
        'Guardo el nuevo total de adjudicaciones de grupo para esta oferta.
        For Each objAdj In g_oOrigen.m_oAdjs
            If objAdj.Prove = sProv Then
                scod2 = CStr(objAdj.Grupo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
                
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc + g_oOrigen.m_oAdjs.Item(scod2).Adjudicado
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce + g_oOrigen.m_oAdjs.Item(scod2).AdjudicadoMonProce
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal + g_oOrigen.m_oAdjs.Item(scod2).AdjudicadoReal
                m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce = m_oProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce + g_oOrigen.m_oAdjs.Item(scod2).AdjudicadoRealMonProce
            End If
        Next

        'Suma a los datos del proceso
        g_oOrigen.m_dblConsumidoProc = g_oOrigen.m_dblConsumidoProc + .Consumido
        g_oOrigen.m_dblConsumidoProcReal = g_oOrigen.m_dblConsumidoProcReal + .Consumido
        g_oOrigen.m_dblAdjudicadoSinProc = g_oOrigen.m_dblAdjudicadoSinProc + g_ogrupo.AdjudicadoTotal
        g_oOrigen.m_dblAdjudicadoSinProcReal = g_oOrigen.m_dblAdjudicadoSinProcReal + g_ogrupo.AdjudicadoTotalReal
        g_oOrigen.m_dblAdjudicadoProc = g_oOrigen.m_dblAdjudicadoSinProc
        g_oOrigen.m_dblAdjudicadoProcReal = g_oOrigen.m_dblAdjudicadoSinProcReal
        
        g_oOrigen.AplicarAtributosDeProceso
        g_oOrigen.CargarGridResultados
        
        '************ ACTUALIZACION GRID ESCALADOS *************
        'Buscar la fila del proveedor
        iRow = sdbgAdjEsc.Row
        'Actualizar los datos del proveedor en el grid
        sdbgAdjEsc.Columns("IMP").Value = IIf(dblImporteAdjudicadoProveedor = 0, "", dblImporteAdjudicadoProveedor)
        If dblImporteAdjudicadoProveedor = 0 And dblImporteAhorroProve = 0 Then
            sdbgAdjEsc.Columns("AHORROIMP").Value = ""
        Else
            sdbgAdjEsc.Columns("AHORROIMP").Value = dblImporteAhorroProve
        End If
        If dblImporteAdjudicadoProveedor = 0 And dblPorcenAhorroProve = 0 Then
            sdbgAdjEsc.Columns("AHORROPORCEN").Value = ""
        Else
            sdbgAdjEsc.Columns("AHORROPORCEN").Value = dblPorcenAhorroProve
        End If
        sdbgAdjEsc.Columns("PRECAPE").Value = oAdjProve.PresUnitario
        If dblImporteAdjudicadoProveedor = 0 And oAdjProve.Precio = 0 Then
            sdbgAdjEsc.Columns("CODPROVEACTUAL").Value = ""
        Else
            sdbgAdjEsc.Columns("CODPROVEACTUAL").Value = oAdjProve.Precio
        End If
        sdbgAdjEsc.Update
    
        'Item
        sdbgAdjEsc.Row = 0
        sdbgAdjEsc.Columns("IMP").Value = IIf(dblImporteAdjudicadoItem = 0, "", dblImporteAdjudicadoItem)
        If dblImporteAdjudicadoItem = 0 And dblImporteAhorroItem = 0 Then
            sdbgAdjEsc.Columns("AHORROIMP").Value = ""
        Else
            sdbgAdjEsc.Columns("AHORROIMP").Value = dblImporteAhorroItem
        End If
        If dblImporteAdjudicadoItem = 0 And dblPorcenAhorroItem = 0 Then
            sdbgAdjEsc.Columns("AHORROPORCEN").Value = ""
        Else
            sdbgAdjEsc.Columns("AHORROPORCEN").Value = dblPorcenAhorroItem
        End If
        sdbgAdjEsc.Columns("PRECAPE").Value = IIf(.Consumido = 0, "", .Consumido)
        sdbgAdjEsc.Row = iRow
        
        g_oOrigen.sdbgResultados.MoveFirst
        g_oOrigen.sdbgResultados.MoveNext
        g_oOrigen.sdbgResultados.Columns("Consumido").Value = g_ogrupo.Consumido
        g_oOrigen.sdbgResultados.Columns("Abierto").Value = g_ogrupo.Abierto
        g_oOrigen.sdbgResultados.Columns("Adjudicado").Value = g_ogrupo.AdjudicadoTotal
        g_oOrigen.sdbgResultados.Columns("Ahorrado").Value = g_ogrupo.Consumido - g_ogrupo.AdjudicadoTotal
        If g_ogrupo.Consumido = 0 Then
            g_oOrigen.sdbgResultados.Columns("%").Value = 0
        Else
            g_oOrigen.sdbgResultados.Columns("%").Value = CDec((g_ogrupo.Consumido - g_ogrupo.AdjudicadoTotal) / Abs(g_ogrupo.Consumido)) * 100
        End If
        g_oOrigen.sdbgResultados.MoveFirst
                
        'sdbgtotalesProve y sdbgtotales2
        g_oOrigen.RestaurarTotalesGeneral
    End With

    m_bRecalculando = False

    Set oOferta = Nothing
    Set oAdj = Nothing
    Set oAdjProve = Nothing
    Set oAdjProveReal = Nothing
    Set oProve = Nothing

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "RecalcularCantidadesEscPorProveedor", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdj_HeadClick(ByVal ColIndex As Integer)
    Dim sIdAtrib As String
    Dim oatrib As CAtributo
    
    'si pincha en un campo atributo muestra el detalle del atributo
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If InStr(1, sdbgAdj.Columns(ColIndex).Name, "ATRIB") > 0 Then
        sIdAtrib = Right(sdbgAdj.Columns(ColIndex).Name, Len(sdbgAdj.Columns(ColIndex).Name) - Len("ATRIB"))
    Else
        Exit Sub
    End If
        
    Me.Enabled = False
    
    'Los de ambito proceso
    If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        If Not m_oProcesoSeleccionado.ATRIBUTOS.Item(sIdAtrib) Is Nothing Then
            Set oatrib = m_oProcesoSeleccionado.ATRIBUTOS.Item(sIdAtrib)
        End If
    End If
    'Los de ambito grupo
    If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
        With m_oProcesoSeleccionado.AtributosGrupo
            If Not .Item(sIdAtrib) Is Nothing Then
                If .Item(sIdAtrib).codgrupo = g_ogrupo.Codigo Or IsNull(.Item(sIdAtrib).codgrupo) Then
                    Set oatrib = .Item(sIdAtrib)
                End If
            End If
        End With
    End If
    'Los de ambito item
    If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
        If Not m_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib) Is Nothing Then
            Set oatrib = m_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib)
        End If
    End If
        'Muestra el detalle del atributo
        Set frmDetAtribProce.g_oOrigen = Me
        frmDetAtribProce.g_sOrigen = "frmADJ"
        
        Set frmDetAtribProce.g_oAtributo = oatrib
        
        Set frmDetAtribProce.g_oProceso = m_oProcesoSeleccionado
        
        Set frmDetAtribProce.g_oProveedores = m_oProvesAsig
        
        If frmDetAtribProce.g_oAtributo Is Nothing Then
            Me.Enabled = True
            Exit Sub
        End If
        Set frmDetAtribProce.g_oGrupoSeleccionado = g_ogrupo
        DoEvents
        If Not IsNull(oatrib.codgrupo) Then
            'Comprueba si est� definido a nivel de grupo o de proceso
            frmDetAtribProce.sdbcGrupos.Value = oatrib.codgrupo
        Else
            frmDetAtribProce.sdbcGrupos.Value = m_sProceso
            frmDetAtribProce.sdbcGrupos.Text = m_sProceso
        End If
        
        frmDetAtribProce.sstabGeneral.Tab = 0
        frmDetAtribProce.Show vbModal
    
    
    
    Me.Enabled = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdj_HeadClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdj_LostFocus()
    'comprueba si lo que se ha modificado es un check.Si no es as� no hace nada.Se hace aqui porque con las columnas de tipo checkbox
    'no salta el aftercolupdate
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgAdj.col = -1 Then Exit Sub
    If sdbgAdj.DataChanged = False Then Exit Sub
    If sdbgAdj.Rows = 0 Then Exit Sub
    
    If sdbgAdj.Columns(sdbgAdj.col).Style = ssStyleCheckBox Then
        sdbgAdj_AfterColUpdate (sdbgAdj.col)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdj_LostFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdj_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picControlPrecios.Visible = False
    picControlOptima.Visible = False
    picControlVista.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdj_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Tras moverse de fila/columna realiza los cambios correspondientes en las celdas (locked, cargar combo, etc)
''' </summary>
''' <param name="lastRow">Fila desde la que se mueve</param>
''' <param name="LastCol">Columna desde la que se mueve</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAdj_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim i As Integer
Dim bModifPrecio As Boolean
Dim oatrib As CAtributo
Dim lIdAtribProce As Long
                                        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgAdj.Rows = 0 Then Exit Sub
    If sdbgAdj.col = -1 Then Exit Sub
    If sdbgAdj.Row = -1 Then Exit Sub
    
    If g_oItemSeleccionado.Cerrado = True Then
        For i = 0 To sdbgAdj.Columns.Count - 1
            sdbgAdj.Columns(i).Locked = True
        Next i
    Else
        If Left(sdbgAdj.Columns(sdbgAdj.col).Name, 6) = "PRECIO" Or Mid(sdbgAdj.Columns(sdbgAdj.col).Name, 1, 5) = "ATRIB" Then
            If (Not m_oProcesoSeleccionado Is Nothing) Then
                If Not m_oProcesoSeleccionado.Bloqueado Then
                    sdbgAdj.Columns(sdbgAdj.col).Locked = True
                Else
                    If m_oProcesoSeleccionado.Estado < conadjudicaciones Then
                        'Comprueba si tiene permisos para modificar el precio o los atributos o no:
                        bModifPrecio = True
                        If m_bModifOfeDeProv = True Or m_bModifOfeDeUsu = True Then
                            'Si no hay oferta tampoco se puede modificar
                            If g_ogrupo.UltimasOfertas Is Nothing Then
                                bModifPrecio = False
                            Else
                                If g_ogrupo.UltimasOfertas.Item(Trim(CStr(sdbgAdj.Columns("PROVECOD").Value))) Is Nothing Then
                                    bModifPrecio = False
                                End If
                            End If

                            'Permitir modificar ofertas introducidas por los proveedores:
                            If bModifPrecio = True And m_bModifOfeDeProv = True Then
                                If Not g_ogrupo.UltimasOfertas.Item(Trim(CStr(sdbgAdj.Columns("PROVECOD").Value))) Is Nothing Then
                                    If g_ogrupo.UltimasOfertas.Item(Trim(CStr(sdbgAdj.Columns("PROVECOD").Value))).portal = False And Not m_bModifOfeDeUsu Then
                                        bModifPrecio = False
                                    End If
                                Else
                                    bModifPrecio = False
                                End If
                            End If

                            'Permitir modificar ofertas introducidas por los usuarios de GS
                            If bModifPrecio = True And m_bModifOfeDeUsu = True Then
                                If g_ogrupo.UltimasOfertas.Item(Trim(CStr(sdbgAdj.Columns("PROVECOD").Value))).portal = True And Not m_bModifOfeDeProv Then
                                    bModifPrecio = False
                                Else
                                    If m_bRestModifOfeUsu = True Then 'Restringir la eliminaci�n a las ofertas introducidas por el usuario
                                        If Not g_ogrupo.UltimasOfertas.Item(Trim(CStr(sdbgAdj.Columns("PROVECOD").Value))).Usuario Is Nothing Then
                                            If g_ogrupo.UltimasOfertas.Item(Trim(CStr(sdbgAdj.Columns("PROVECOD").Value))).Usuario.Cod <> oUsuarioSummit.Cod Then
                                                bModifPrecio = False
                                            End If
                                        Else
                                            bModifPrecio = False
                                        End If
                                    End If
                                End If
                            End If

                            'Restringir la modificaci�n a las ofertas de los proveedores asignados al usuario
                            If bModifPrecio = True And m_bRestModifOfeProvUsu = True Then
                                If g_oOrigen.Name = "frmADJ" Then
                                    If Not g_oOrigen.m_bOfeAsigComp Then
                                        If Not g_oOrigen.m_oProveedoresAsigUsu Is Nothing Then
                                            If g_oOrigen.m_oProveedoresAsigUsu.Item(CStr(sdbgAdj.Columns("PROVECOD").Value)) Is Nothing Then
                                                bModifPrecio = False
                                            End If
                                        Else
                                            bModifPrecio = False
                                        End If
                                    End If
                                Else
                                    If Not g_oOrigen.m_oProveedoresAsigUsu Is Nothing Then
                                        If g_oOrigen.m_oProveedoresAsigUsu.Item(CStr(sdbgAdj.Columns("PROVECOD").Value)) Is Nothing Then
                                            bModifPrecio = False
                                        End If
                                    Else
                                        bModifPrecio = False
                                    End If
                                End If
                            End If
                        Else
                            bModifPrecio = False
                        End If
                        
                        
                        
                        If bModifPrecio = True Then
                            sdbgAdj.Columns(sdbgAdj.col).Locked = False
                            
                            If Mid(sdbgAdj.Columns(sdbgAdj.col).Name, 1, 5) = "ATRIB" Then
                                lIdAtribProce = Right(sdbgAdj.Columns(sdbgAdj.col).Name, Len(sdbgAdj.Columns(sdbgAdj.col).Name) - 5)
                                Select Case Right(sdbgAdj.Columns(sdbgAdj.col).TagVariant, 1)
                                    Case "P"
                                        Set oatrib = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(lIdAtribProce))
                                    Case "G"
                                        Set oatrib = m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(lIdAtribProce))
                                    Case "I"
                                        Set oatrib = m_oProcesoSeleccionado.AtributosItem.Item(CStr(lIdAtribProce))
                                End Select
                            
                                'Si vamos a modificar un atributo comprobamos si es de tipo lista:
                                If oatrib.TipoIntroduccion = Introselec Then  'De tipo lista
                                    sdbddValor.RemoveAll
                                    sdbddValor.AddItem ""
                                    If oatrib.Tipo = TipoNumerico Then
                                        sdbddValor.Columns(0).Alignment = ssCaptionAlignmentRight
                                    Else
                                        sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
                                    End If
                                    sdbgAdj.Columns.Item(sdbgAdj.col).DropDownHwnd = sdbddValor.hWnd
                                    sdbddValor.Enabled = True
                                Else  'Libre
                                    If oatrib.Tipo = TipoBoolean Then
                                        sdbddValor.RemoveAll
                                        sdbddValor.AddItem ""
                                        sdbgAdj.Columns.Item(sdbgAdj.col).DropDownHwnd = sdbddValor.hWnd
                                        sdbddValor.Enabled = True
                                    Else
                                        sdbgAdj.Columns.Item(sdbgAdj.col).DropDownHwnd = 0
                                        sdbddValor.Enabled = False
                                    End If
                                End If
                                Set oatrib = Nothing
                            End If
                        Else
                            sdbgAdj.Columns(sdbgAdj.col).Locked = True
                            sdbddValor.Enabled = False
                        End If
                    End If
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdj_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdj_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
With sdbgAdj
    If g_oItemSeleccionado.Cerrado Then
        For i = 0 To .Columns.Count - 1
            .Columns(i).CellStyleSet "ItemCerrado"
            .Columns(i).Locked = True
        Next
        Exit Sub
    Else
        If Trim(.Columns("CANTADJ").CellValue(Bookmark)) <> "" Then
            .Columns("PROVE").CellStyleSet "Adjudicado", .Row
            If StrToDbl0(.Columns("AHO").Value) > 0 Then
                .Columns("AHO").CellStyleSet "Green", .Row
            Else
                .Columns("AHO").CellStyleSet "Red", .Row
            End If
            If StrToDbl0(.Columns("AHO_PORCEN").Value) > 0 Then
                .Columns("AHO_PORCEN").CellStyleSet "Green", .Row
            Else
                .Columns("AHO_PORCEN").CellStyleSet "Red", .Row
            End If
            .Columns("ADJ").CellStyleSet "Adjudicado", .Row
        End If
        
        'El ahorro de la oferta:
        If Trim(.Columns("AHORRO_OFE").CellValue(Bookmark)) <> "" Then
            If StrToDbl0(.Columns("AHORRO_OFE").Value) > 0 Then
                .Columns("AHORRO_OFE").CellStyleSet "Green"
            Else
                .Columns("AHORRO_OFE").CellStyleSet "Red"
            End If
        End If
        
        'El %ahorro de la oferta:
        If Trim(.Columns("AHORRO_OFE_PORCEN").CellValue(Bookmark)) <> "" Then
            If StrToDbl0(.Columns("AHORRO_OFE_PORCEN").Value) > 0 Then
                .Columns("AHORRO_OFE_PORCEN").CellStyleSet "Green"
            Else
                .Columns("AHORRO_OFE_PORCEN").CellStyleSet "Red"
            End If
        End If
    End If
End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdj_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
        
End Sub

Private Sub sdbgAdj_SplitterMove(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     Cancel = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdj_SplitterMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Despues de updatar (en grid) un dato se hacen en cascada las modificaciones asociadas para la bbdd (grabar cambios escalados) y diferentes colecciones
''' </summary>
''' <param name="ColIndex">Columna modificada</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAdjEsc_AfterColUpdate(ByVal ColIndex As Integer)
    Dim teserror As TipoErrorSummit
    Dim vPrecio As Variant
    Dim oEscalado As CEscalado
    Dim oOferta As COferta
    Dim oPrecio As CPrecioItem
    Dim bEspera As Boolean
    Dim vAtrib As Variant
    Dim dPrecioAplicFormula As Double
    Dim irespuesta As Integer
    Dim vPrecioOfer As Variant
    Dim iNumAtribCarac As Integer
    Dim sCod As String
    Dim dblConsumidoTotal As Double
    Dim dblAhorradoTotal As Double
    Dim sIdAtrib As String
    Dim vValor As Variant
    Dim bNuevoPresup As Boolean
    Dim dblCantAdj As Double
    Dim dblCantidad As Double
    Dim bHayEscAdj As Boolean
    Dim oEsc As CEscalado
    Dim oAdj As CAdjudicacion
    Dim bNuevoPrecio As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRecalculando Then Exit Sub
    
    With sdbgAdjEsc
        Select Case .Grp
            Case 0
                Select Case .Columns(ColIndex).Name
                    Case "CANT"
                        If .Columns("PROV").Value = "0" Then
                            If Trim(.ActiveCell.Value) = "" Then
                                g_oItemSeleccionado.Cantidad = Null
                                g_oItemSeleccionado.Presupuesto = Null
                            Else
                                If Not IsEmpty(g_oItemSeleccionado.Cantidad) And Not IsNull(g_oItemSeleccionado.Cantidad) Then m_dblCantidadAnterior = g_oItemSeleccionado.Cantidad
                                g_oItemSeleccionado.Cantidad = CDbl(.ActiveCell.Value)
                                
                                If Not IsNull(g_oItemSeleccionado.Precio) And Not IsEmpty(g_oItemSeleccionado.Precio) Then
                                    g_oItemSeleccionado.Presupuesto = g_oItemSeleccionado.Cantidad * g_oItemSeleccionado.Precio
                                End If
                            End If
                            
                            teserror = m_oProcesoSeleccionado.ModificarCantidadItemEsc(g_ogrupo.Codigo, g_oItemSeleccionado.Id, m_bBorrarAdj)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                .CancelUpdate
                            End If
                        Else
                            dblCantidad = IIf(Trim(.ActiveCell.Value) <> "", .ActiveCell.Value, 0)
                            
                            'Comprobar si hay escalados adjudicados
                            bHayEscAdj = False
                            For Each oEsc In g_ogrupo.Escalados
                                sCod = KeyEscalado(.Columns("ID").Value, .Columns("ART").Value, oEsc.Id)
                                Set oAdj = g_ogrupo.Adjudicaciones.Item(sCod)
                                If Not oAdj Is Nothing Then
                                    bHayEscAdj = True
                                    
                                    RecalcularCantidadesEscPorEscalado g_ogrupo, True, .Columns("ART").Value, .Columns("ID").Value, oAdj.Escalado, , dblCantidad
                                    AdjudicacionModificada
                                End If
                                Set oAdj = Nothing
                            Next
                            Set oEsc = Nothing
                            
                            If Not bHayEscAdj Then
                                sCod = .Columns("ID").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(.Columns("ID").Value))
                                
                                    RecalcularCantidadesEscPorProveedor g_ogrupo, (dblCantidad > 0), .Columns("ART").Value, .Columns("ID").Value, dblCantidad
                                    AdjudicacionModificada
                            End If
                        End If
                        
                        m_bRecargarOrigen = True
                End Select
            Case 1
                'Modificaci�n de atributos de caracter�sticas. S�lo es necesario guardar el nuevo valor
                sIdAtrib = .Columns(.col).Name
                If Not g_ogrupo.AtributosItem.Item(sIdAtrib) Is Nothing Then
                    vValor = g_oOrigen.ModificarValorAtributo(sdbgAdjEsc, .Columns("ID").Value, sIdAtrib, ColIndex)
                    If IsNull(vValor) Then
                        .CancelUpdate
                        Exit Sub
                    End If
                    
                    m_bRecargarOrigen = True
                End If
            Case Else
                Select Case Left(.Columns(ColIndex).Name, 4)
                    Case "PRES"
                        If .Columns("PROV").Value = "0" Then
                            'Modificaci�n del presupuesto del item por escalado
                            
                            If Trim(.ActiveCell.Value) <> "" Then
                                If Not IsNumeric(.ActiveCell.Value) Then
                                    oMensajes.NoValido m_sPresUni
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            End If
                                                        
                            bNuevoPresup = False
                            Set oEscalado = g_oItemSeleccionado.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value))
                            If oEscalado Is Nothing Then
                                Set oEscalado = oFSGSRaiz.Generar_CEscalado
                                oEscalado.Id = .Columns("ESC" & .Columns("ESC" & .Grp).Value).Value
                                oEscalado.Inicial = .Columns("INI" & .Columns("ESC" & .Grp).Value).Value
                                If .Columns("FIN" & .Columns("ESC" & .Grp).Value).Value <> "" Then oEscalado.final = .Columns("FIN" & .Columns("ESC" & .Grp).Value).Value
                                bNuevoPresup = True
                            End If
                            
                            If m_oProcesoSeleccionado.Estado < conadjudicaciones And m_oProcesoSeleccionado.Bloqueado Then
                                g_oOrigen.cmdGuardar.Enabled = True
                                m_oProcesoSeleccionado.GuardarProceso = True
                            ElseIf m_oProcesoSeleccionado.Estado > ParcialmenteCerrado Then
                                m_oProcesoSeleccionado.ModificadoHojaAdj = True
                            End If
                            
                            vPrecio = g_oItemSeleccionado.Precio 'Guardo el anterior para hacer las cuentas
                            If .ActiveCell.Value = "" Then
                                oEscalado.Presupuesto = Null
                            Else
                                oEscalado.Presupuesto = .ActiveCell.Value
                            End If
                            
                            g_oItemSeleccionado.TipoEscalados = g_ogrupo.TipoEscalados
                            teserror = g_oItemSeleccionado.ModificarPresupuestoEscalado(oEscalado.Id, oEscalado.Presupuesto, oEscalado.Inicial, oEscalado.final, bNuevoPresup)
                            If teserror.NumError <> TESnoerror Then
                                If teserror.NumError = TESVolumenAdjDirSuperado Then
                                    basErrores.TratarError teserror
                                    If Not m_oProcesoSeleccionado.PermSaltarseVolMaxAdjDir Then
                                        .CancelUpdate
                                        Exit Sub
                                    Else
                                        basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompModPres, "Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(m_oProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(m_oProcesoSeleccionado.Cod) & " Item:" & .Columns("ID").Value
                                    End If
                                Else
                                    basErrores.TratarError teserror
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            Else
                                If bNuevoPresup Then g_oItemSeleccionado.Escalados.Add oEscalado.Id, oEscalado.Inicial, oEscalado.final, oEscalado.Presupuesto
                                basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompModPres, "Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(m_oProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(m_oProcesoSeleccionado.Cod) & " Item:" & .Columns("ID").Value
                                m_bRecalcular = True
                                m_bRecargarOrigen = True
                            End If
                        Else
                            'Modificaci�n de los precios de la oferta
                            
                            If Trim(.ActiveCell.Value) <> "" Then
                                If Not IsNumeric(.ActiveCell.Value) Then
                                    oMensajes.NoValido m_sPrecioImporte
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            End If
                            
                            If m_oProcesoSeleccionado.Estado < conadjudicaciones And m_oProcesoSeleccionado.Bloqueado Then
                                g_oOrigen.cmdGuardar.Enabled = True
                                m_oProcesoSeleccionado.GuardarProceso = True
                            ElseIf m_oProcesoSeleccionado.Estado > ParcialmenteCerrado Then
                                m_oProcesoSeleccionado.ModificadoHojaAdj = True
                            End If
                            vAtrib = Null
                            
                            Set oOferta = g_ogrupo.UltimasOfertas.Item(Trim(.Columns("ID").Value))
                            'Comprueba que no haya atributos aplicados al precio
                            If Not oOferta Is Nothing Then
                                If Not oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)) Is Nothing Then
                                    If Not oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Escalados.Item(CStr(sdbgAdjEsc.Groups(sdbgAdjEsc.Grp).TagVariant)) Is Nothing Then
                                        With oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Escalados.Item(CStr(sdbgAdjEsc.Groups(sdbgAdjEsc.Grp).TagVariant))
                                            'Si el precio de la oferta es distinto al precio sin atributos es pq hay atribs aplicados
                                            dPrecioAplicFormula = NullToDbl0(.Precio)
                                              
                                            'Si hay atributos aplicados de precio se tienen que desaplicar para cambiar el precio
                                            If CDec(NullToDbl0(dPrecioAplicFormula)) <> CDec(NullToDbl0(.PrecioOferta)) Then
                                                vAtrib = g_oOrigen.DevolverAtributosAplicadosPrecUnitario(g_ogrupo.Codigo, g_oItemSeleccionado.Id, sdbgAdjEsc.Columns("ID").Value)
                                                If Not IsNull(vAtrib) Then
                                                    irespuesta = oMensajes.PreguntaModificarPrecio(vAtrib)
                                                    If irespuesta = vbNo Then
                                                        sdbgAdjEsc.CancelUpdate
                                                        Exit Sub
                                                    End If
                                                End If
                                            End If
                                        End With
                                    End If
                                End If
                            End If
                            
                            If Not oOferta Is Nothing Then
                                bNuevoPrecio = False
                                                                
                                Set oPrecio = oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id))
                                If Not oPrecio.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value)) Is Nothing Then
                                    vPrecio = oPrecio.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value)).PrecioOferta
                                Else
                                    bNuevoPrecio = True
                                    oPrecio.Escalados.Add .Columns("ESC" & .Grp).Value, g_ogrupo.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value)).Inicial, g_ogrupo.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value)).final
                                End If
                                If .Columns(ColIndex).Value <> vbNullString Then
                                    oPrecio.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value)).PrecioOferta = CDec(StrToDbl0(.Columns(ColIndex).Value) * NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(.Columns("ID").Value)).Cambio))
                                Else
                                    oPrecio.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value)).PrecioOferta = Null
                                End If
                                vPrecioOfer = oPrecio.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value)).PrecioOferta
                                oPrecio.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value)).Precio = oPrecio.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value)).PrecioOferta
                                        
                                'Si se ha modificado el precio hay que recalcular la oferta.
                                bEspera = False
                                If NullToDbl0(.Columns(ColIndex).Value) <> NullToDbl0(vPrecio) And (IsNull(g_oItemSeleccionado.Cantidad) Or IsEmpty(g_oItemSeleccionado.Cantidad)) Then
                                    Set oEscalado = g_ogrupo.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value))
                                    If (g_ogrupo.TipoEscalados = ModoDirecto And oEscalado.Inicial = g_oItemSeleccionado.Cantidad) Or _
                                        (g_ogrupo.TipoEscalados = ModoRangos And oEscalado.Inicial <= g_oItemSeleccionado.Cantidad And oEscalado.final >= g_oItemSeleccionado.Cantidad) Then
                                        bEspera = True
                                        oOferta.Recalcular = True
                                        Set oOferta.proceso = m_oProcesoSeleccionado
                                    End If
                                    Set oEscalado = Nothing
                                End If
                                        
                                teserror = oPrecio.ModificarPrecioEscalado(g_ogrupo.TipoEscalados, .Columns("ESC" & .Grp).Value, bNuevoPrecio)
                                                                
                                If teserror.NumError <> TESnoerror Then
                                    basErrores.TratarError teserror
                                    .CancelUpdate
                                    Exit Sub
                                Else
                                    m_bRecargarOrigen = True
                                    
                                    basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompModPrec, "Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(m_oProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(m_oProcesoSeleccionado.Cod) & " Item:" & CStr(.Columns("ART").Value)
                                    'Registramos llamada a recalcular
                                    If bEspera Then
                                        basSeguridad.RegistrarAccion AccionesSummit.ACCCondOfeRecalculo, "Llamada desde:Comparativa Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(m_oProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(m_oProcesoSeleccionado.Cod)
                                    End If
                                End If
                                
                                'si tiene atributos de precio unitario aplicados los quita:
                                If Not IsNull(vAtrib) And Not IsNull(oPrecio.PrecioOferta) Then
                                    'si hab�a atributos de precio unitario aplicados habr� que desaplicarlos
                                    g_oOrigen.DeshabilitarAtribsUnitarioItem g_ogrupo.Codigo, vAtrib
                                    'Aqu� de momento lo calculo todo
                                    g_oOrigen.CalcularPrecioConFormulas
                                    CargarGridEscAdjudicaciones iNumAtribCarac
                                    g_oOrigen.RecalcularTotalesGrupo
                                Else
                                    sCod = KeyEscalado(.Columns("ID").Value, .Columns("ART").Value, .Columns("ESC" & .Grp).Value)
                                    If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                                        RecalcularCantidadesEscPorEscalado g_ogrupo, True, .Columns("ART").Value, .Columns("ID").Value, .Columns("ESC" & .Grp).Value, g_ogrupo.Adjudicaciones.Item(sCod).Adjudicado
                                        
                                        dblConsumidoTotal = DevolverImporteConsumido
                                        dblAhorradoTotal = dblConsumidoTotal - g_oItemSeleccionado.ImporteAdj 'ImporteAdj en moneda proceso
                                        CargarGridResultados dblConsumidoTotal, dblAhorradoTotal, g_oItemSeleccionado.ImporteAdj
                                    Else
                                        Set m_oAdjsProve = CalcularAdjudicacionesInterno(m_oProcesoSeleccionado, g_ogrupo, m_oProvesAsig, m_oAtribsFormulas)
                                    End If
                                End If
                                If m_oProcesoSeleccionado.Estado >= ConObjetivosSinNotificarYPreadjudicado And m_oProcesoSeleccionado.Estado < Cerrado Then
                                    sCod = KeyEscalado(.Columns("ID").Value, .Columns("ART").Value, .Columns("ESC" & .Grp).Value)
                                    If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                                        teserror = m_oProcesoSeleccionado.ActualizarTotalesVisor(g_oOrigen.m_dblAdjudicadoProcReal, g_oOrigen.m_dblConsumidoProcReal)
                                    End If
                                End If
                            End If
                            Set oOferta = Nothing
                        End If
                    Case "AHOR"
                        If .Columns("PROV").Value = "0" Then
                            If Trim(.ActiveCell.Value) <> "" Then
                                If Not IsNumeric(.ActiveCell.Value) Then
                                    oMensajes.NoValido m_sObjetivo
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            End If
                        
                            bNuevoPresup = False
                            'Modificaci�n del objetivo del item por escalado
                            Set oEscalado = g_oItemSeleccionado.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value))
                            If oEscalado Is Nothing Then
                                Set oEscalado = oFSGSRaiz.Generar_CEscalado
                                oEscalado.Id = .Columns("ESC" & .Columns("ESC" & .Grp).Value).Value
                                oEscalado.Inicial = .Columns("INI" & .Columns("ESC" & .Grp).Value).Value
                                If .Columns("FIN" & .Columns("ESC" & .Grp).Value).Value <> "" Then oEscalado.final = .Columns("FIN" & .Columns("ESC" & .Grp).Value).Value
                                bNuevoPresup = True
                            End If

                            If m_oProcesoSeleccionado.Estado < conadjudicaciones And m_oProcesoSeleccionado.Bloqueado Then
                                g_oOrigen.cmdGuardar.Enabled = True
                                m_oProcesoSeleccionado.GuardarProceso = True
                            ElseIf m_oProcesoSeleccionado.Estado > ParcialmenteCerrado Then
                                m_oProcesoSeleccionado.ModificadoHojaAdj = True
                            End If
                            
                            If .ActiveCell.Value = "" Then
                                oEscalado.Objetivo = Null
                            Else
                                oEscalado.Objetivo = CDbl(.ActiveCell.Value)
                            End If

                            teserror = g_oItemSeleccionado.ModificarObjetivoEscalado(oEscalado.Id, oEscalado.Objetivo, Date, bNuevoPresup)
                            If teserror.NumError <> TESnoerror And teserror.NumError <> 25 Then
                                basErrores.TratarError teserror
                                .CancelUpdate
                                Exit Sub
                            Else
                                m_bRecargarOrigen = True
                                If bNuevoPresup Then g_oItemSeleccionado.Escalados.Add oEscalado.Id, oEscalado.Inicial, oEscalado.final, oEscalado.Presupuesto
                                If gParametrosGenerales.giINSTWEB = ConPortal Then
                                    '�?
                                    'm_bActualizarContadoresPortal = True
                                End If
                                basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompFijObjetivos, "Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(m_oProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(m_oProcesoSeleccionado.Cod) & " Item:" & .Columns("ID").Value

                            End If
                        End If
                    Case "ADJC"
                        If .Columns("PROV").Value = "1" Then
                            'Modificaci�n de la cantidad adjudicada a un escalado
                            
                            If Trim(.ActiveCell.Value) <> "" Then
                                If Not IsNumeric(.ActiveCell.Value) Then
                                    oMensajes.NoValido m_sCantAdjudicada
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            End If
                            
                            m_bCambioCantAdjEsc = True
                            m_iRow = .Row
                            
                            If .ActiveCell.Value = "" Then
                                dblCantAdj = 0
                            Else
                                dblCantAdj = CDbl(.ActiveCell.Value)
                            End If
                                                        
                            sCod = KeyEscalado(.Columns("ID").Value, .Columns("ART").Value, .Groups(.Grp).TagVariant)
                            If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                                g_ogrupo.Adjudicaciones.Item(sCod).Adjudicado = dblCantAdj
                                RecalcularCantidadesEscPorEscalado g_ogrupo, True, g_oItemSeleccionado.Id, .Columns("ID").Value, .Groups(.Grp).TagVariant, dblCantAdj
                            Else
                                AdjudicarEscalado .Columns("ID").Value, .Groups(.Grp).TagVariant, True, dblCantAdj
                            End If
                            AdjudicacionModificada
                            
                            m_bRecargarOrigen = True
                        End If
                    Case Else
                        If .Columns("PROV").Value = "1" Then
                            'Modificaci�n de atributos de precio � precio/importe por escalado � objetivo por escalado
                            If Not g_ogrupo.AtributosItem Is Nothing Then
                                Set oEscalado = g_oItemSeleccionado.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value))
                                sIdAtrib = Left(.Columns(.col).Name, Len(.Columns(.col).Name) - Len(CStr(.Grp)))
                                If Not g_ogrupo.AtributosItem.Item(sIdAtrib) Is Nothing Then
                                    vPrecio = g_oOrigen.ModificarValorAtributo(sdbgAdjEsc, .Columns("ID").Value, sIdAtrib, ColIndex, oEscalado.Id)
                                    If IsNull(vPrecio) Then
                                        .CancelUpdate
                                        Exit Sub
                                    Else
                                        If .Columns("ADJCANT" & .Grp).Value = "" Then
                                            dblCantAdj = 0
                                        Else
                                            dblCantAdj = CDbl(.Columns("ADJCANT" & .Grp).Value)
                                        End If
                                        RecalcularCantidadesEscPorEscalado g_ogrupo, True, g_oItemSeleccionado.Id, .Columns("ID").Value, oEscalado.Id, dblCantAdj
                                    End If
                                    
                                    m_bRecargarOrigen = True
                                End If
                            End If
                        End If
                End Select
        End Select
        
        'Cuando hay cambio de la cant. adjudicada a un escalado no se llama al Update porque hay que dar la oportunidad
        'al usaurio de rellenar las cantidades adjudicadas a cada escalado antes de hacer el Update de la l�nea
        If Not m_bCambioCantAdjEsc Then .Update
    End With
        
    Set oEscalado = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_AfterColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>Realiza las acciones necesarias despu�s de que se ha modificado una adjudicaci�n</summary>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>

Private Sub AdjudicacionModificada()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado.Estado < conadjudicaciones And m_oProcesoSeleccionado.Bloqueado Then
        g_oOrigen.cmdGuardar.Enabled = True
        cmdGuardar.Enabled = True
        m_oProcesoSeleccionado.GuardarProceso = True
    ElseIf m_oProcesoSeleccionado.Estado > ParcialmenteCerrado Then
        m_oProcesoSeleccionado.ModificadoHojaAdj = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "AdjudicacionModificada", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Evento que salta al hacer cambios en el grid, Actualizar la fila en edicion
''' </summary>
''' <param name="RtnDispErrMsg">Buffer con los datos y bookmark de la fila </param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAdjEsc_AfterUpdate(RtnDispErrMsg As Integer)
    Dim oEscalado As CEscalado
    Dim oAdj As CAdjudicacion
    Dim iRow As Integer
    Dim bRecalc As Boolean
    Dim i As Integer
    Dim oAsig As COferta
    Dim sAhorro As String
    Dim dblPresup As Double
    Dim oProve As CProveedor
    Dim sCod As String
    Dim dblConsumido As Double
    Dim dblAhorrado As Double
    Dim scodProve As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRecalculando Then Exit Sub
    
    If m_bBorrarAdj Then
        LockWindowUpdate Me.hWnd
                        
        For Each oAdj In g_ogrupo.Adjudicaciones
            If oAdj.Id = g_oItemSeleccionado.Id Then
                DesadjudicarEscalado oAdj.ProveCod, oAdj.Escalado, bRecalc
            End If
        Next
        Set oAdj = Nothing
        
        m_bBorrarAdj = False
        
        LockWindowUpdate 0&
    ElseIf m_bRecalcularItem Then
        LockWindowUpdate Me.hWnd
                                
        For Each oAdj In m_oAdjsProve
            'S�lo es necesario recalcular las adjudicaciones de los proveedores que no tienen cantidad adjudicada para mostrar
            'los datos correctos con respecto a la nueva cantidad de item
            If oAdj.Id = g_oItemSeleccionado.Id Then
                For Each oEscalado In g_ogrupo.Escalados
                    sCod = KeyEscalado(oAdj.ProveCod, g_oItemSeleccionado.Id, oEscalado.Id)
                    If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        RecalcularCantidadesEscPorEscalado g_ogrupo, True, g_oItemSeleccionado.Id, oAdj.ProveCod, oEscalado.Id, g_ogrupo.Adjudicaciones.Item(sCod).Adjudicado
                    End If
                Next
            End If
        Next
        Set oAdj = Nothing
        
        m_bRecalcularItem = False
        
        LockWindowUpdate 0&
    ElseIf m_bCambioCantAdjEsc Then
        LockWindowUpdate g_oOrigen.hWnd
        LockWindowUpdate Me.hWnd
                        
        With sdbgAdjEsc
            iRow = .Row
            .Row = m_iRow
            scodProve = .Columns("ID").Value
            .Row = iRow
                        
            For Each oAdj In g_ogrupo.Adjudicaciones
                If oAdj.Id = g_oItemSeleccionado.Id And (oAdj.ProveCod = scodProve Or oAdj.Adjudicado = 0) Then
                    RecalcularCantidadesEscPorEscalado g_ogrupo, True, g_oItemSeleccionado.Id, oAdj.ProveCod, oAdj.Escalado, oAdj.Adjudicado
                End If
            Next
            Set oAdj = Nothing
        End With
        
        dblConsumido = DevolverImporteConsumido
        dblAhorrado = dblConsumido - g_oItemSeleccionado.ImporteAdj 'ImporteAdj en moneda proceso
        CargarGridResultados dblConsumido, dblAhorrado, g_oItemSeleccionado.ImporteAdj
        
        m_bCambioCantAdjEsc = False
        
        LockWindowUpdate 0&
    ElseIf m_bRecalcular Then
        m_bRecalcular = False
                
        LockWindowUpdate Me.hWnd
        
        With sdbgAdjEsc
            iRow = .Row
            .Row = m_iFilaItem
            
            If .Columns("PRES" & .Groups(m_iGrupo).TagVariant).Value <> "" Then
                dblPresup = .Columns("PRES" & .Groups(m_iGrupo).TagVariant).Value
            End If
                
            'Si hay adjudicaciones hay que recalcularlas
            For Each oProve In m_oProvesAsig
                sCod = KeyEscalado(oProve.Cod, .Columns("ID").Value, .Groups(m_iGrupo).TagVariant)
                If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                    RecalcularCantidadesEscPorEscalado g_ogrupo, True, g_oItemSeleccionado.Id, oProve.Cod, .Groups(m_iGrupo).TagVariant, g_ogrupo.Adjudicaciones.Item(sCod).Adjudicado
                    AdjudicacionModificada
                End If
            Next
            
            'Recalcular objetivos
            i = m_iFilaItem + 1
            .Row = i
            While .Columns("PROV").Value = "1" And i < .Rows
                sAhorro = vbNullString
                
                If dblPresup > 0 Then
                    Set oAsig = g_ogrupo.UltimasOfertas.Item(Trim(.Columns("ID").Value))
                    If Not oAsig Is Nothing Then
                        If Not oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Escalados.Item(CStr(.Groups(m_iGrupo).TagVariant)) Is Nothing Then
                            If Not IsNull(oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Escalados.Item(CStr(.Groups(m_iGrupo).TagVariant)).PrecioOferta) Then
                                sAhorro = CStr(dblPresup - (oAsig.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Escalados.Item(CStr(.Groups(m_iGrupo).TagVariant)).PrecioOferta / g_oItemSeleccionado.CambioComparativa(oProve.Cod, True, m_oAdjsProve)))
                            End If
                        End If
                    End If
                End If
                
                .Columns("AHORRO" & .Groups(m_iGrupo).TagVariant).Value = sAhorro
                
                i = i + 1
                .Row = i
            Wend
            Set oAsig = Nothing
            
            dblConsumido = DevolverImporteConsumido
            dblAhorrado = dblConsumido - g_oItemSeleccionado.ImporteAdj 'ImporteAdj en moneda proceso
            CargarGridResultados dblConsumido, dblAhorrado, g_oItemSeleccionado.ImporteAdj
            
            .Row = iRow
        End With
        
        LockWindowUpdate 0&
    ElseIf m_bCambioCantAdjEsc Then
        If Not m_bRecalculando Then m_bCambioCantAdjEsc = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_AfterUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjEsc_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim vCantidad As Variant
    Dim bComprobarAdj As Boolean
    Dim bHayAdj As Boolean
    Dim oAdj As CAdjudicacion
    Dim dblSumaCantAdj As Double
    Dim dblCantAdj As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRecalculando Then Exit Sub
    
    With sdbgAdjEsc
        Select Case .Grp
            Case 0
                If .Columns(ColIndex).Name = "CANT" Then
                    If .Columns("PROV").Value = "0" Then
                        'Comprobar si el usuario tiene permisos para modificar el item en la apertura
                        If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
                            If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModificarArti)) Is Nothing Then
                                Cancel = True
                                Exit Sub
                            End If
                        End If
                        
                        'Modificaci�n de la cantidad del item
                        If .ActiveCell.Value <> vbNullString Then
                            If Not IsNumeric(.ActiveCell.Value) Or .ActiveCell.Value = "0" Then
                                oMensajes.NoValido m_sCantidad
                                .CancelUpdate
                                Exit Sub
                            End If
                        End If
                        
                        vCantidad = g_ogrupo.Items.Item(.Columns("ID").Value).Cantidad
                                                
                        If Not IsNull(vCantidad) And Not IsEmpty(vCantidad) Then
                            If .Columns("CANT").Value <> vbNullString Then
                                If CDbl(OldValue) > CDbl(.Columns("CANT").Value) Then
                                    bComprobarAdj = True
                                End If
                            Else
                                bComprobarAdj = True
                            End If
                        Else
                            If .Columns("CANT").Value <> vbNullString Then
                                bComprobarAdj = True
                            End If
                        End If
                                                                    
                        If bComprobarAdj Then
                            bHayAdj = False
                            m_bBorrarAdj = False
                            
                            For Each oAdj In g_ogrupo.Adjudicaciones
                                If oAdj.Id = .Columns("ID").Value Then
                                    If Not g_ogrupo.Items.Item(CStr(oAdj.Id)) Is Nothing Then
                                        bHayAdj = True
                                        dblCantAdj = dblCantAdj + NullToDbl0(oAdj.Adjudicado)
                                    End If
                                End If
                            Next
                            Set oAdj = Nothing
                            
                            If bHayAdj Then
                                If Not IsNull(vCantidad) And Not IsEmpty(vCantidad) Then
                                    If dblCantAdj > .Columns("CANT").Value Then
                                        If oMensajes.AdjudicacionesNoCorrespondientes = vbNo Then
                                            Cancel = True
                                        Else
                                            m_bBorrarAdj = True
                                        End If
                                    Else
                                        m_bRecalcularItem = True
                                    End If
                                Else
                                    If dblCantAdj > .Columns("CANT").Value Then
                                        If oMensajes.AdjudicacionesNoCorrespondientes = vbNo Then
                                            Cancel = True
                                        Else
                                            m_bBorrarAdj = True
                                        End If
                                    Else
                                        m_bRecalcularItem = True
                                    End If
                                End If
                            End If
                        End If
                    Else
                        'Si se est� modificando la cantidad del proveedor
                        
                        If Trim(.Columns("CANT").Value) <> "" Then
                            If .ActiveCell.Value <> vbNullString Then
                                If Not IsNumeric(.ActiveCell.Value) Or .ActiveCell.Value = "0" Then
                                    oMensajes.NoValido m_sCantAdjudicada
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            End If
                            
                            'Comprobar que la suma de las cantidades adjudicadas no supera la del item
                            bHayAdj = False
                            For Each oAdj In m_oAdjsProve
                                If oAdj.Grupo = g_ogrupo.Codigo And oAdj.Id = .Columns("ART").Value Then
                                    If oAdj.ProveCod <> .Columns("ID").Value Then
                                        dblSumaCantAdj = dblSumaCantAdj + oAdj.Adjudicado
                                    Else
                                        bHayAdj = True
                                        dblSumaCantAdj = dblSumaCantAdj + .Columns("CANT").Value
                                    End If
                                End If
                            Next
                            Set oAdj = Nothing
                            
                            If Not bHayAdj Then dblSumaCantAdj = dblSumaCantAdj + .Columns("CANT").Value
                            
                            vCantidad = g_ogrupo.Items.Item(.Columns("ART").Value).Cantidad
                            If dblSumaCantAdj > NullToDbl0(vCantidad) Then
                                oMensajes.CantidadItemSuperada
                                Cancel = True
                            End If
                        End If
                    End If
                End If
            Case 1
                'Comprobar el valor del atributo
                Cancel = Not ComprobarValorAtributo(m_oProcesoSeleccionado, .Columns(.col).Name, .Columns(ColIndex).Value, m_sIdiTrue, m_sIdiFalse, m_sIdiMayor, _
                                m_sIdiMenor, m_sIdiEntre, AmbItem)
            Case Else
                Select Case Left(.Columns(ColIndex).Name, Len(.Columns(ColIndex).Name) - Len(CStr(.Grp)))
                    Case "PRES"
                        If .Columns("PROV").Value = "0" Then
                            m_iFilaItem = .Row
                            m_iGrupo = .Grp
                        End If
                End Select
        End Select
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_BeforeColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjEsc_BeforeUpdate(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRecalculando Then Exit Sub
    
    If m_bCambioCantAdjEsc Then
        With sdbgAdjEsc
            If Not ComprobarActualizarFilaGrid(g_ogrupo, .Columns("ART").Value) Then
                Cancel = True
            End If
        End With
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_BeforeUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjEsc_ColMove(ByVal ColIndex As Integer, ByVal NewPos As Integer, Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If ColIndex = 0 Then Cancel = True
    If NewPos = 0 Then Cancel = True
    
    g_oVistaSeleccionada.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_ColMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjEsc_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_oVistaSeleccionada.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_ColResize", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' DblClick grid: mostrar el detalle del item
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAdjEsc_DblClick()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgAdjEsc
        If g_ogrupo.Cerrado = 1 Then Exit Sub
        If .col = -1 Then Exit Sub

        'Si se ha hecho doble click  sobre un item mostrar el detalle del item
        Select Case .Grp
            Case 0
                If .Columns("PROV").Value = "1" Then
                    MostrarDetalleProveedor .Columns("ID").Value
                End If
            Case 1
            Case Else
                If Left(.Columns(.col).Name, 4) = "PRES" And .Columns("PROV").Value = "1" Then
                    AdjudicarDesadjudicarEscalado .Columns("ID").Value, .Columns("ESC" & .Grp).Value, (.Columns("ADJIMP" & .Grp).Value = "")
                End If
        End Select
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjEsc_GrpResize(ByVal GrpIndex As Integer, Cancel As Integer)
    
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    g_oVistaSeleccionada.HayCambios = True
        
    With sdbgAdjEsc
        If GrpIndex > 1 Then
            'Si redimensiona un escalado
            For i = 2 To .Groups.Count - 1
                If i <> GrpIndex Then
                    .Groups(i).Width = .ResizeWidth
                End If
            Next i
        End If
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_GrpResize", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjEsc_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = 9 Then m_bTabPressed = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjEsc_LostFocus()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bCambioCantAdjEsc And Not m_bComprobandoActualizarGrid Then
        With sdbgAdjEsc
            If .Columns("PROV").Value = "1" Then
                If Not ComprobarActualizarFilaGrid(g_ogrupo, .Columns("ART").Value) Then
                    If Me.Visible Then .SetFocus
                End If
            End If
        End With
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_LostFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>Comprobaciones antes de actualizar una fila</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="lIdItem">Id. item</param>
''' <returns>Booleano indicando si se puede actualizar la fila</returns>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>LTG 14/05/2012</revision>

Private Function ComprobarActualizarFilaGrid(ByVal oGrupo As CGrupo, ByVal lIdItem As Long) As Boolean
    
    Dim bAct As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ComprobarActualizarFilaGrid = True
    
    If m_bRecalculando Then Exit Function
    
    m_bComprobandoActualizarGrid = True
    
    bAct = True
                    
    'Comprobaci�n cantidades adjudicadas a todos los escalados
    If Not ComprobarSumaCantAdj(oGrupo, lIdItem) Then
        oMensajes.ExcesoCantAdjEscaladosItem
        bAct = False
    End If
    
    If bAct Then AdjudicacionModificada
    
    m_bComprobandoActualizarGrid = False
    
    ComprobarActualizarFilaGrid = bAct
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ComprobarActualizarFilaGrid", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Private Sub sdbgAdjEsc_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picControlPrecios.Visible = False
    picControlOptima.Visible = False
    picControlVista.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjEsc_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim iIndex As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Button = 2 Then
        With sdbgAdjEsc
            iIndex = .RowContaining(Y)
            If iIndex > -1 Then
                .Row = iIndex

                iIndex = .ColContaining(X, Y)
                If .Columns("PROV").Value = "1" Then
                    'Se posiciona en la fila
                    mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
                    mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
                    DoEvents
                    
                    If Left(.Columns(iIndex).Name, 4) = "PRES" Then
                        CargarMenuAdj .Left + X, .Top + Y
                    End If
                End If
            End If
        End With
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_MouseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
''' <summary>
''' Tras moverse de fila/columna realiza los cambios correspondientes en las celdas (locked, cargar combo, etc)
''' </summary>
''' <param name="lastRow">Fila desde la que se mueve</param>
''' <param name="LastCol">Columna desde la que se mueve</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAdjEsc_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim oatrib As CAtributo
    Dim vBmk As Variant
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgAdjEsc
        If .col > -1 Then
            Select Case .Columns(.col).Group
                Case 0
                    Select Case .Columns(.col).Name
                        Case "CANT"
                            If .Columns("PROV").Value = "0" Then
                                .Columns(.col).Locked = False
                            Else
                                If ProveSinOfes(g_ogrupo.UltimasOfertas, Trim(.Columns("ID").Value), True, g_ogrupo.Escalados) Then
                                    'Si el proveedor no tiene ofertas o Tiene oferta pero sin precios se bloquea
                                    .Columns(.col).Locked = True
                                Else
                                    If CantidadProveedorEditable(g_ogrupo, g_oItemSeleccionado.Id, .Columns("ID").Value) Then
                                        .Columns(.col).Locked = False
                                    Else
                                        .Columns(.col).Locked = True
                                    End If
                                End If
                            End If
                    End Select
                Case 1
                    If .Columns("PROV").Value = 0 Then
                        .Columns(.col).Locked = True
                    Else
                        .Columns(.col).Locked = False
                        
                        'Si vamos a modificar un atributo comprobamos si es de tipo lista:
                        Set oatrib = g_ogrupo.AtributosItem.Item(CStr(.Columns(.col).Name))
                        If oatrib.TipoIntroduccion = Introselec Then  'De tipo lista
                            sdbddValor.RemoveAll
                            sdbddValor.AddItem ""
                            If oatrib.Tipo = TipoNumerico Then
                                sdbddValor.Columns(0).Alignment = ssCaptionAlignmentRight
                            Else
                                sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
                            End If
                            .Columns.Item(.col).DropDownHwnd = sdbddValor.hWnd
                            sdbddValor.Enabled = True
                        Else  'Libre
                            If oatrib.Tipo = TipoBoolean Then
                                sdbddValor.RemoveAll
                                sdbddValor.AddItem ""
                                .Columns.Item(.col).DropDownHwnd = sdbddValor.hWnd
                                sdbddValor.Enabled = True
                            Else
                                .Columns.Item(.col).DropDownHwnd = 0
                                sdbddValor.Enabled = False
                            End If
                        End If
                        Set oatrib = Nothing
                    End If
                Case Else
                    Select Case Left(.Columns(.col).Name, Len(.Columns(.col).Name) - Len(CStr(.Grp)))
                        Case "PRES"
                            If g_ogrupo.UltimasOfertas.Item(Trim(.Columns("ID").Value)) Is Nothing Then
                                'Si el proveedor no tiene ofertas se bloquea
                                .Columns(.col).Locked = True
                            Else
                                If Not m_oProcesoSeleccionado.Bloqueado Then
                                    .Columns(.col).Locked = True
                                Else
                                    .Columns(.col).Locked = False
                                End If
                            End If
                        Case "AHORRO"
                            If .Columns("PROV").Value = "1" Then
                                'El ahorro es calculado
                                .Columns(.col).Locked = True
                            Else
                                .Columns(.col).Locked = False
                            End If
                        Case "ADJCANT"
                            If .Columns("PROV").Value = "0" Then
                                .Columns(.col).Locked = True
                            Else
                                If ProveSinOfes(g_ogrupo.UltimasOfertas, Trim(.Columns("ID").Value), True, g_ogrupo.Escalados) Then
                                    'Si el proveedor no tiene ofertas o Tiene oferta pero sin precios se bloquea
                                    .Columns(.col).Locked = True
                                Else
                                    .Columns(.col).Locked = False
                                End If
                            End If
                    End Select
            End Select
        End If
        
        If Not IsNull(LastRow) Then
            If val(LastRow) > -1 Then
                vBmk = .AddItemBookmark(.Row)
                
                'Cuando se cambia de fila presionando el tab el valor de LastRow es el mismo que el vBmk de .Row (como si no se hubiera cambiado de fila)
                If (val(vBmk) <> val(LastRow)) Or (m_bTabPressed And (val(vBmk) = val(LastRow)) And (.col < val(LastCol))) Then
                    'vBmk = .AddItemBookmark(val(LastRow))
                    If .Columns("PROV").CellValue(LastRow) = "1" Then
                        If Not ComprobarActualizarFilaGrid(g_ogrupo, .Columns("ART").CellValue(LastRow)) Then
                            '.Row = val(LastRow)
                            If m_bTabPressed Then
                                If .Row > 1 Then .Bookmark = .AddItemBookmark(.Row - 1)
                            Else
                                .Bookmark = LastRow
                            End If
                            .col = val(LastCol)
                        End If
                    End If
                End If
            End If
        End If
    End With
    
    If m_bTabPressed Then m_bTabPressed = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjEsc_RowLoaded(ByVal Bookmark As Variant)
    Dim oEscalado As CEscalado
    Dim oProve As CProveedor
    Dim oStyleSetSource As StyleSet
    Dim oStyleSet As StyleSet
    Dim bModifPorPubMatProve As Boolean
    Dim bComprobarAviso As Boolean
    Dim bComprobarContrato  As Boolean
    Dim sStyleName As String
    Dim sCadenaAviso  As String
    Dim sCadenaContrato As String
    Dim i As Integer
    Dim iGroupIndex As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgAdjEsc
        If .Columns("PROV").CellValue(Bookmark) = "1" Then
            AplicarEstiloFila "Prove"
            .Columns("CANT").CellStyleSet "Blue"
        Else
            AplicarEstiloFila "Item"
        End If
    
        If g_oItemSeleccionado.Cerrado Then
            For i = 0 To .Columns.Count - 1
                .Columns(i).CellStyleSet "ItemCerrado"
                .Columns(i).Locked = True
            Next
            Exit Sub
        Else
            If .Columns("PROV").CellValue(Bookmark) = "1" Then
                If Trim(.Columns("CANT").CellValue(Bookmark)) <> "" Then
                    .Columns("DESCR").CellStyleSet "Adjudicado", .Row
                    If .Columns("AHORROIMP").Value <> "" Then
                        If StrToDbl0(.Columns("AHORROIMP").Value) > 0 Then
                            .Columns("AHORROIMP").CellStyleSet "Green", .Row
                        Else
                            .Columns("AHORROIMP").CellStyleSet "Red", .Row
                        End If
                    End If
                    If .Columns("AHORROPORCEN").Value <> "" Then
                        If StrToDbl0(.Columns("AHORROPORCEN").Value) > 0 Then
                            .Columns("AHORROPORCEN").CellStyleSet "Green", .Row
                        Else
                            .Columns("AHORROPORCEN").CellStyleSet "Red", .Row
                        End If
                    End If
                End If
                
                'Marca los proveedores que son el Proveedor actual para el item en curso
                If Not m_oProcesoSeleccionado.AdminPublica Then
                    If .Columns("ID").CellValue(Bookmark) = .Columns("CODPROVEACTUAL").Value And _
                            Trim$(.Columns("CODPROVEACTUAL").Value) <> "" And .Columns("CANT").Value <> "" Then
                        .Columns("CANT").CellStyleSet "ProvActual", .Row
                        .Columns("DESCR").CellStyleSet "ProvActual", .Row
                    End If
                End If
                                      
                bComprobarAviso = False
                If g_oOrigen.m_bComparativaQA Then
                    bComprobarAviso = g_oOrigen.ComprobarAvisoRestriccion(.Columns("ID").CellValue(Bookmark))
                End If
                bComprobarContrato = g_oOrigen.ComprobarContrato(.Columns("ID").CellValue(Bookmark))
                                              
                Set oProve = m_oProvesAsig.Item(.Columns("ID").CellValue(Bookmark))
                sCadenaAviso = ""
                sCadenaContrato = ""
                If bComprobarAviso Then sCadenaAviso = "YAviso"
                If bComprobarContrato Then sCadenaContrato = "YContrato"
                If oProve.NoOfe And Not oProve.HaOfertado Then
                    sStyleName = "NOOFE" & sCadenaAviso & sCadenaContrato
                Else
                    If Not oProve.HaOfertado Then
                        If oProve.Notificado Then
                            sStyleName = "NOTIF" & sCadenaAviso & sCadenaContrato
                        Else
                            If Not bComprobarAviso Then
                                If bComprobarContrato Then
                                    sStyleName = "Contrato"
                                Else
                                    sStyleName = "Proveedor"
                                End If
                            Else
                                sStyleName = "Aviso" & sCadenaContrato
                            End If
                        End If
                    Else
                        'If frmOrigen.MostrarClip(oProve.Cod) Then
                        'En las filas de proveedor se guarda el id del item correspondiente en la columna ART
                        If g_oOrigen.MostrarClipItem(g_ogrupo.Codigo, .Columns("ART").Value, oProve.Cod) Then
                            If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod) Is Nothing Then
                                If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).Usuario Is Nothing Then
                                    If m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).portal Then
                                        sStyleName = "ClipYMundo" & sCadenaAviso & sCadenaContrato
                                    Else
                                        sStyleName = "ClipYPieza" & sCadenaAviso & sCadenaContrato
                                    End If
                                Else
                                    sStyleName = "ProveedorConAdjuntos" & sCadenaAviso & sCadenaContrato
                                End If
                            End If
                        Else
                            If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod) Is Nothing Then
                                If Not m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).Usuario Is Nothing Then
                                    If m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).portal Then
                                        sStyleName = "IntroPORTAL" & sCadenaAviso & sCadenaContrato
                                    Else
                                        sStyleName = "IntroGS" & sCadenaAviso & sCadenaContrato
                                    End If
                                Else
                                    If Not bComprobarAviso Then
                                        If bComprobarContrato Then
                                            sStyleName = "Contrato"
                                        Else
                                            sStyleName = "Proveedor"
                                        End If
                                    Else
                                        sStyleName = "Aviso" & sCadenaContrato
                                    End If
                                End If
                            Else
                                If Not bComprobarAviso Then
                                    If bComprobarContrato Then
                                        sStyleName = "Contrato"
                                    Else
                                        sStyleName = "Proveedor"
                                    End If
                                Else
                                    sStyleName = "Aviso" & sCadenaContrato
                                End If
                            End If
                        End If
                    End If
                End If
                
                If .Columns("CERRADO").Value Then
                    'Si el item est� cerrado el fondo tiene que ser de color gris
                    Set oStyleSetSource = .StyleSets(sStyleName)
                    sStyleName = sStyleName & "YCerrado"
                    Set oStyleSet = .StyleSets(sStyleName)
                    oStyleSet.AlignmentPicture = oStyleSetSource.AlignmentPicture
                    Set oStyleSet.Font = oStyleSetSource.Font
                    oStyleSet.Backcolor = ColorGrey
                    oStyleSet.Forecolor = ColorWhite
                    Set oStyleSet.Picture = oStyleSetSource.Picture
                Else
                    'si la oferta es no adjudicable el nombre del proveedor saldr� en blanco
                    'El cambio de la prop. ForeColor de la celda no funciona. Se coge el estilo, se crea uno
                    'nuevo para el caso de no adjudicable y se asigna a la celda
                    If Not OfertaAdjudicable(.Columns("ID").Value) Then
                        Set oStyleSetSource = .StyleSets(sStyleName)
                        sStyleName = sStyleName & "YNoAdj"
                        Set oStyleSet = .StyleSets(sStyleName)
                        oStyleSet.AlignmentPicture = oStyleSetSource.AlignmentPicture
                        Set oStyleSet.Font = oStyleSetSource.Font
                        oStyleSet.Forecolor = ColorWhite
                        oStyleSet.Backcolor = ColorButton
                        Set oStyleSet.Picture = oStyleSetSource.Picture
                    End If
                End If
                .Columns("DESCR").CellStyleSet sStyleName
                Set oStyleSetSource = Nothing
                Set oStyleSet = Nothing
                                        
                iGroupIndex = 2
                For Each oEscalado In g_ogrupo.Escalados
                    'Marca en gris los items que no se pueden modificar porque el proveedor tiene restricci�n por material.
                    'Solo para multimaterial
                    If gParametrosGenerales.gbMultiMaterial And m_oProcesoSeleccionado.PubMatProve Then
                        bModifPorPubMatProve = True
                        'Comprobemos si los proveedores tienen restringidos los items a los de su material
                        bModifPorPubMatProve = g_oOrigen.m_oProcesos.ComprobarPubMatProve(m_oProcesoSeleccionado.Anyo, m_oProcesoSeleccionado.GMN1Cod, m_oProcesoSeleccionado.Cod, g_ogrupo.Id, .Columns("ART").Value, .Columns.Item("ART").Value)
                        If Not bModifPorPubMatProve Then
                            For i = 0 To .Columns.Count - 1
                                .Columns(i).CellStyleSet "Grey"
                            Next
                        End If
                    End If
        
                    If .Columns("ADJ" & iGroupIndex).Value <> "0" Then
                        If g_oOrigen.MostrarClipItem(g_ogrupo.Codigo, .Columns("ART").Value, .Columns("PROV").Value) Then
                            .Columns("PRES" & iGroupIndex).CellStyleSet "AdjudicadoClip", .Row
                        Else
                            .Columns("PRES" & iGroupIndex).CellStyleSet "Adjudicado", .Row
                        End If
                    Else
                        If g_oOrigen.MostrarClipItem(g_ogrupo.Codigo, .Columns("ART").Value, .Columns("PROV").Value) Then
                            If .Columns("HOM").Value <> 1 And g_ogrupo.Items.Item(.Columns("ART").Value).ArticuloCod <> "" And Not basParametros.gParametrosGenerales.gbPermAdjSinHom Then
                                .Columns("PRES" & iGroupIndex).CellStyleSet "NoHomologadoClip", .Row
                            Else
                                .Columns("PRES" & iGroupIndex).CellStyleSet "YellowClip", .Row
                            End If
                        End If
                    End If
                    
                    'El ahorro de la oferta:
                    If Trim(.Columns("AHORRO" & CStr(iGroupIndex)).CellValue(Bookmark)) <> "" Then
                        If StrToDbl0(.Columns("AHORRO" & CStr(iGroupIndex)).Value) > 0 Then
                            .Columns("AHORRO" & CStr(iGroupIndex)).CellStyleSet "Green"
                        Else
                            .Columns("AHORRO" & CStr(iGroupIndex)).CellStyleSet "Red"
                        End If
                    End If
                    
                    iGroupIndex = iGroupIndex + 1
                Next
                
                Set oEscalado = Nothing
                Set oProve = Nothing
            End If
        End If
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>Indica si una oferta es adjudicable para un proveedor</summary>
''' <param name="sCodProve">C�digo del proveedor</param>
''' <remarks>Llamada desde: sdbgEscalado_RowLoaded</remarks>

Private Function OfertaAdjudicable(ByVal scodProve As String) As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    OfertaAdjudicable = False
        
    If Not m_oProcesoSeleccionado.Ofertas.Item(scodProve) Is Nothing Then
        OfertaAdjudicable = g_oOrigen.m_oEstOfes.Item(CStr(m_oProcesoSeleccionado.Ofertas.Item(scodProve).CodEst)).adjudicable
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "OfertaAdjudicable", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>Aplica un estilo a las columnas del grid</summary>
''' <param name="StyleSet">Nombre del estilo a aplicar</param>
''' <remarks>Llamada desde: sdbgEscalado_RowLoaded</remarks>
''' <remarks></remarks>

Private Sub AplicarEstiloFila(ByVal StyleSet As String)
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For i = 0 To sdbgAdjEsc.Cols - 1
        sdbgAdjEsc.Columns(i).CellStyleSet StyleSet
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "AplicarEstiloFila", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjEsc_SplitterMove(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Cancel = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAdjEsc_SplitterMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAplicarPrecio_BtnClick()
    'si es un campo de atributo muestra la pantalla con la ponderaci�n
    Dim sIdAtrib As String
    Dim bMuestra As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.Enabled = False
    
    sIdAtrib = sdbgAplicarPrecio.Columns("ID").Value
    Set frmDetAtribProce.g_oOrigen = Me
    Set frmDetAtribProce.g_oProceso = m_oProcesoSeleccionado
    frmDetAtribProce.g_sOrigen = "frmADJ"
    Set frmDetAtribProce.g_oProveedores = m_oProvesAsig
    Set frmDetAtribProce.g_oGrupoSeleccionado = g_ogrupo
    
    bMuestra = False
    'Comprueba si es un atributo de proceso
    If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        If Not m_oProcesoSeleccionado.ATRIBUTOS.Item(sIdAtrib) Is Nothing Then
            'Es un atributo de proceso
            Set frmDetAtribProce.g_oAtributo = m_oProcesoSeleccionado.ATRIBUTOS.Item(sIdAtrib)
            frmDetAtribProce.sdbcGrupos.Value = m_sProceso
            frmDetAtribProce.sdbcGrupos.Text = m_sProceso
            bMuestra = True
        End If
    End If
    'Comprueba si es un atributo de grupo
    
    If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
        If Not m_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib) Is Nothing Then
            Set frmDetAtribProce.g_oAtributo = m_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib)
        
            If Not IsNull(m_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib).codgrupo) Then
                'Comprueba si est� definido a nifrmadj.vel de grupo o de proceso
                frmDetAtribProce.sdbcGrupos.Value = m_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib).codgrupo
            Else
                frmDetAtribProce.sdbcGrupos.Value = m_sProceso
                frmDetAtribProce.sdbcGrupos.Text = m_sProceso
            End If
            bMuestra = True
        End If
    End If
    
    'Comprueba si es un atributo de item
    If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
        If Not m_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib) Is Nothing Then
            Set frmDetAtribProce.g_oAtributo = m_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib)
        
            If Not IsNull(m_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib).codgrupo) Then
                'Comprueba si est� definido a nivel de grupo o de proceso
                frmDetAtribProce.sdbcGrupos.Value = m_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib).codgrupo
            Else
                frmDetAtribProce.sdbcGrupos.Value = m_sProceso
                frmDetAtribProce.sdbcGrupos.Text = m_sProceso
            End If
            bMuestra = True
        End If
    End If
    
    'Si tampoco es un atributo de item no muestra la pantalla
    If Not bMuestra Then
        Me.Enabled = True
        Exit Sub
    End If
    
    frmDetAtribProce.sstabGeneral.Tab = 0
    frmDetAtribProce.Show vbModal

    Me.Enabled = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAplicarPrecio_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAplicarPrecio_Change()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oAtribsFormulas.Item(sdbgAplicarPrecio.Columns("ID").Value) Is Nothing Then Exit Sub
    
    'Si est� parcialmente cerrado no se pueden habilitar-deshbilitar atributos de item (total y/o unitario)
    If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado And g_oItemSeleccionado.Cerrado Then
        oMensajes.ImposibleModificarAtributoAplic
        Exit Sub
    End If
    
    'Comprueba si se puede aplicar o no el atributo.
    If GridCheckToBoolean(sdbgAplicarPrecio.Columns("INC").Value) = False Then Exit Sub
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAplicarPrecio_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgOcultarCampos_BtnClick()
   Dim sIdAtrib As String
   Dim bMuestra As Boolean
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.Enabled = False
    
    sIdAtrib = sdbgOcultarCampos.Columns("COD").Value
    
    Set frmDetAtribProce.g_oProceso = m_oProcesoSeleccionado
    Set frmDetAtribProce.g_oProveedores = m_oProvesAsig
    Set frmDetAtribProce.g_oOrigen = Me
    Set frmDetAtribProce.g_oGrupoSeleccionado = g_ogrupo
    frmDetAtribProce.g_sOrigen = "frmADJ"
    bMuestra = False
    'Comprueba si es un atributo de proceso
    If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        With m_oProcesoSeleccionado.ATRIBUTOS
            If Not .Item(sIdAtrib) Is Nothing Then
                'Es un atributo de proceso
                Set frmDetAtribProce.g_oAtributo = .Item(sIdAtrib)
                frmDetAtribProce.sdbcGrupos.Value = m_sProceso
                frmDetAtribProce.sdbcGrupos.Text = m_sProceso
                bMuestra = True
            End If
        End With
    End If
    'Comprueba si es un atributo de grupo
    If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
        With m_oProcesoSeleccionado.AtributosGrupo
            If Not .Item(sIdAtrib) Is Nothing Then
                Set frmDetAtribProce.g_oAtributo = .Item(sIdAtrib)
            
                If Not IsNull(.Item(sIdAtrib).codgrupo) Then
                    'Comprueba si est� definido a nivel de grupo o de proceso
                    frmDetAtribProce.sdbcGrupos.Value = .Item(sIdAtrib).codgrupo
                Else
                    frmDetAtribProce.sdbcGrupos.Value = m_sProceso
                    frmDetAtribProce.sdbcGrupos.Text = m_sProceso
                End If
                bMuestra = True
            End If
        End With
    End If
    
    If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
        If Not m_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib) Is Nothing Then
        
            Set frmDetAtribProce.g_oAtributo = m_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib)
        
            If Not IsNull(m_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib).codgrupo) Then
                'Comprueba si est� definido a nivel de grupo o de proceso
                frmDetAtribProce.sdbcGrupos.Value = m_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib).codgrupo
            Else
                frmDetAtribProce.sdbcGrupos.Value = m_sProceso
                frmDetAtribProce.sdbcGrupos.Text = m_sProceso
            End If
            bMuestra = True
        End If
    End If
    
    If Not bMuestra Then
        Me.Enabled = True
        Exit Sub
    End If
    
    frmDetAtribProce.sstabGeneral.Tab = 0
    frmDetAtribProce.Show vbModal

    Me.Enabled = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgOcultarCampos_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgOcultarCampos_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_ogrupo.AtributosItem Is Nothing Then
        If Not g_ogrupo.AtributosItem.Item(sdbgOcultarCampos.Columns("COD").Value) Is Nothing Then
            'Le asigna el style de la bombilla
            sdbgOcultarCampos.Columns("POND").CellStyleSet "Ponderacion", sdbgOcultarCampos.Row
            Exit Sub
        End If
    End If
    
    If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
        If Not m_oProcesoSeleccionado.AtributosGrupo.Item(sdbgOcultarCampos.Columns("COD").Value) Is Nothing Then
            'Le asigna el style de la bombilla
            sdbgOcultarCampos.Columns("POND").CellStyleSet "Ponderacion", sdbgOcultarCampos.Row
            Exit Sub
        End If
    End If
    
    If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        If Not m_oProcesoSeleccionado.ATRIBUTOS.Item(sdbgOcultarCampos.Columns("COD").Value) Is Nothing Then
            'Le asigna el style de la bombilla
            sdbgOcultarCampos.Columns("POND").CellStyleSet "Ponderacion", sdbgOcultarCampos.Row
            Exit Sub
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgOcultarCampos_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgOcultarProv_BtnClick()
    'Muestra el detalle del proveedor
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    MostrarDetalleProveedor sdbgOcultarProv.Columns("COD").Value
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgOcultarProv_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub MostrarDetalleProveedor(ByVal CodProve As String)
Dim oProves As CProveedores
Dim oProve As CProveedor
Dim oCon As CContacto
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, CodProve, , True
    
    If oProves.Count = 0 Then
        oMensajes.DatoEliminado m_sProve
        Set oProves = Nothing
        Exit Sub
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor CodProve
    
    Set frmProveDetalle.g_oProveSeleccionado = oProve
    
    With frmProveDetalle
        .caption = CodProve & "  " & oProve.Den
        .lblCodPortProve = NullToStr(oProve.CodPortal)
        .g_bPremium = oProve.EsPremium
        .g_bActivo = oProve.EsPremiumActivo
        .lblDir = NullToStr(oProve.Direccion)
        .lblCp = NullToStr(oProve.cP)
        .lblPob = NullToStr(oProve.Poblacion)
        .lblPaiCod = NullToStr(oProve.CodPais)
        .lblPaiDen = NullToStr(oProve.DenPais)
        .lblMonCod = NullToStr(oProve.CodMon)
        .lblMonDen = NullToStr(oProve.DenMon)
        .lblProviCod = NullToStr(oProve.CodProvi)
        .lblProviDen = NullToStr(oProve.DenProvi)
        .lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
        .lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
        .lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
        
        If Trim(oProve.Calif1) <> "" Then
            .lblCal1Den = oProve.Calif1
        End If
        
        If Trim(oProve.Calif2) <> "" Then
            .lblcal2den = oProve.Calif2
        End If
        
        If Trim(oProve.Calif3) <> "" Then
            .lblcal3den = oProve.Calif3
        End If
        
        .lblCal1Val = DblToStr(oProve.Val1, m_sFormatoNumber)
        .lblCal2Val = DblToStr(oProve.Val2, m_sFormatoNumber)
        .lblCal3Val = DblToStr(oProve.Val3, m_sFormatoNumber)
    
        .lblURL = NullToStr(oProve.URLPROVE)
    
        oProve.CargarTodosLosContactos , , , , True, , , , , , , True
        
        For Each oCon In oProve.Contactos
            
            .sdbgContactos.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.Departamento & Chr(m_lSeparador) & oCon.Cargo _
            & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Port & Chr(m_lSeparador)
        
        Next
       
        Set oProves = Nothing
        Set oProve = Nothing
        
        .Show 1
    
    End With
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "MostrarDetalleProveedor", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

Private Sub sdbgOcultarProv_Change()
    'Hace visibles o invisbles los provedores de la grid
    Dim bQuitarCheck As Boolean
    Dim bQuitarCheckAsignables As Boolean
    Dim scodProve As String
    Dim iNumAtrib As Integer
    Dim bVisible As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bQuitarCheck = False
    bQuitarCheckAsignables = False
    
    scodProve = sdbgOcultarProv.Columns("COD").Value
    bVisible = sdbgOcultarProv.Columns("INC").Value
    
    If chkOcultarProvSinOfe.Value = vbChecked Then
        m_bOcultarprove = True
        chkOcultarProvSinOfe.Value = vbUnchecked
    End If
    ActualizarVisibilidadProves scodProve, bVisible
    
    If GridCheckToBoolean(sdbgOcultarProv.Columns("INC").Value) Then
        If g_ogrupo.UsarEscalados Then
            CargarGridEscAdjudicaciones iNumAtrib
        Else
            CargarGridAdjudicaciones
        End If
        
        If g_oVistaSeleccionada.OcultarNoAdj Then
            
            If Not m_oProcesoSeleccionado.Ofertas Is Nothing Then
                If m_oProcesoSeleccionado.Ofertas.Item(sdbgOcultarProv.Columns("COD").Value) Is Nothing Then
                    bQuitarCheckAsignables = False
                Else
                    If Not g_oOrigen.m_oEstOfes.Item(m_oProcesoSeleccionado.Ofertas.Item(sdbgOcultarProv.Columns("COD").Value).CodEst).adjudicable Then
                        bQuitarCheckAsignables = True
                    End If
                End If
            Else
                bQuitarCheckAsignables = False
            End If
                                    
            If bQuitarCheckAsignables Then
                m_bOcultarofertas = True
                chkOcultarNoAdj.Value = vbUnchecked
            End If
        End If
    End If
    RedimensionarProves
    
    g_oVistaSeleccionada.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgOcultarProv_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub ActualizarVisibilidadProves(ByVal sProveCod As String, ByVal bVisible As Boolean)
    Dim sCod As String
    Dim vbm As Variant
    Dim j As Integer
    Dim bVis As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgOcultarProv
        For j = 0 To .Rows - 1
            vbm = .AddItemBookmark(j)
            
            sCod = .Columns("COD").CellValue(vbm)
            If sCod = sProveCod Then
                'Cuando se llama a esta funci�n en el evento Change todav�a no se ha actualizado el valor que devuelve CellValue
                'Se asigna el pasado como par�metro
                bVis = bVisible
            Else
                bVis = .Columns("INC").CellValue(vbm)
            End If
            
            If g_oVistaSeleccionada.Vista <> TipoDeVistaDefecto.vistainicial Then
                If bVis Then
                    g_oVistaSeleccionada.ConfVistasItemProv.Item(sCod).Visible = TipoProvVisible.Visible
                Else
                    g_oVistaSeleccionada.ConfVistasItemProv.Item(sCod).Visible = TipoProvVisible.NoVisible
                End If
            End If
        Next j
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ActualizarVisibilidadProves", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbgOcultarProv_RowLoaded(ByVal Bookmark As Variant)
    'Le asigna el style de la bombilla
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgOcultarProv.Columns("POND").CellStyleSet "Ponderacion", sdbgOcultarProv.Row
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgOcultarProv_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgOptima_BtnClick()
    
    Dim sIdAtrib As String
    Dim bMuestra As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.Enabled = False
    
    sIdAtrib = sdbgOptima.Columns("ID").Value
    
    Set frmDetAtribProce.g_oProceso = m_oProcesoSeleccionado
    Set frmDetAtribProce.g_oProveedores = m_oProvesAsig
    Set frmDetAtribProce.g_oOrigen = Me
    Set frmDetAtribProce.g_oGrupoSeleccionado = g_ogrupo
    frmDetAtribProce.g_sOrigen = "frmADJ"
    bMuestra = False
    'Comprueba si es un atributo de proceso
    If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        If Not m_oProcesoSeleccionado.ATRIBUTOS.Item(sIdAtrib) Is Nothing Then
            'Es un atributo de proceso
            Set frmDetAtribProce.g_oAtributo = m_oProcesoSeleccionado.ATRIBUTOS.Item(sIdAtrib)
            frmDetAtribProce.sdbcGrupos.Value = m_sProceso
            frmDetAtribProce.sdbcGrupos.Text = m_sProceso
            bMuestra = True
        End If
    End If
    'Comprueba si es un atributo de grupo
    If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
        With m_oProcesoSeleccionado.AtributosGrupo
            If Not .Item(sIdAtrib) Is Nothing Then
                Set frmDetAtribProce.g_oAtributo = .Item(sIdAtrib)
            
                If Not IsNull(.Item(sIdAtrib).codgrupo) Then
                    'Comprueba si est� definido a nivel de grupo o de proceso
                    frmDetAtribProce.sdbcGrupos.Value = .Item(sIdAtrib).codgrupo
                Else
                    frmDetAtribProce.sdbcGrupos.Value = m_sProceso
                    frmDetAtribProce.sdbcGrupos.Text = m_sProceso
                End If
                bMuestra = True
            End If
        End With
    End If
    'Comprueba si es un atributo de item
    If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
        With m_oProcesoSeleccionado.AtributosItem
            If Not .Item(sIdAtrib) Is Nothing Then
            
                Set frmDetAtribProce.g_oAtributo = .Item(sIdAtrib)
            
                If Not IsNull(.Item(sIdAtrib).codgrupo) Then
                    'Comprueba si est� definido a nivel de grupo o de proceso
                    frmDetAtribProce.sdbcGrupos.Value = .Item(sIdAtrib).codgrupo
                Else
                    frmDetAtribProce.sdbcGrupos.Value = m_sProceso
                    frmDetAtribProce.sdbcGrupos.Text = m_sProceso
                End If
                bMuestra = True
            End If
        End With
    End If
    
    If Not bMuestra Then
        Me.Enabled = True
        Exit Sub
    End If
        
    frmDetAtribProce.sstabGeneral.Tab = 0
    frmDetAtribProce.Show vbModal

    Me.Enabled = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgOptima_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgOptima_Change()
Dim oatrib As CAtributo
    
    'En la fila del precio no se puede chequear la check de ponderaci�n
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgOptima.Columns("COD").Value = m_sPrecio Then
        If GridCheckToBoolean(sdbgOptima.Columns("PON").Value) = True Then
            sdbgOptima.Columns("PON").Value = "0"
            Exit Sub
        End If
    End If
    
    If GridCheckToBoolean(sdbgOptima.Columns("PON").Value) = True Then
        'Si el atributo seleccionado es de tipo de ponderaci�n=0 (sin ponderaci�n) que no deje checkearlo
        Select Case sdbgOptima.Columns("AMBITO").Value
            Case AmbGrupo
                Set oatrib = m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(sdbgOptima.Columns("ID").Value))
            Case AmbItem
                Set oatrib = m_oProcesoSeleccionado.AtributosItem.Item(CStr(sdbgOptima.Columns("ID").Value))
            Case AmbProceso
                Set oatrib = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(sdbgOptima.Columns("ID").Value))
        End Select
        
        If Not oatrib Is Nothing Then
            If oatrib.TipoPonderacion = SinPonderacion Then
                frmMsgboxOptima.sOrigen = "frmADJ"
                frmMsgboxOptima.sMensaje = "POND"
                frmMsgboxOptima.Show vbModal
                sdbgOptima.Columns("PON").Value = "0"
            End If
        Else
            sdbgOptima.Columns("PON").Value = "0"
        End If
        Set oatrib = Nothing
    End If
    
    
    sdbgOptima.Update

'        MsgBox "No es posible calcular la �ptima con atributos de proceso y atributos de grupo simultaneamente." & vbCrLf & "Seleccione �nicamente atributos del mismo nivel.", vbInformation + vbOKOnly, "FULLSTEP GS"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgOptima_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbgOptima_Click()
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgOptima.SelBookmarks.RemoveAll
    sdbgOptima.SelBookmarks.Add sdbgOptima.Bookmark
    If sdbgOptima.Row = 0 Then
        sdbgOptima.MovePrevious
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgOptima_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
  
End Sub

Private Sub sdbgOptima_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgOptima.MoveFirst
    sdbgOptima.SelBookmarks.Add sdbgOptima.Bookmark
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgOptima_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgOptima_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgOptima.Columns("COD").Value = m_sPrecio Then Exit Sub
    
    sdbgOptima.Columns("POND").CellStyleSet "Ponderacion", sdbgOptima.Row

    If m_oProcesoSeleccionado.AtributosItem Is Nothing Then Exit Sub
    
    If Not m_oProcesoSeleccionado.AtributosItem.Item(sdbgOptima.Columns("ID").CellValue(Bookmark)) Is Nothing Then
        'Si el atributo es interno se mostrar� una bolita roja en la fila:
        If m_oProcesoSeleccionado.AtributosItem.Item(sdbgOptima.Columns("ID").CellValue(Bookmark)).interno = True Then
            sdbgOptima.Columns("COD").CellStyleSet "Interno", sdbgOptima.Row
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgOptima_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbgAplicarPrecio_RowLoaded(ByVal Bookmark As Variant)
    'Le asigna el style de la bombilla
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAplicarPrecio.Columns("POND").CellStyleSet "Ponderacion", sdbgAplicarPrecio.Row
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgAplicarPrecio_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgResultados_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgResultados
        If .Columns("Ahorrado").CellValue(Bookmark) > 0 And .Columns("Ahorrado").CellValue(Bookmark) <> "" Then
            .Columns("Ahorrado").CellStyleSet "Green", .Row
        Else
            If .Columns("Ahorrado").CellValue(Bookmark) < 0 Then .Columns("Ahorrado").CellStyleSet "Red", .Row
        End If
        
        If .Columns("%").CellValue(Bookmark) > 0 And .Columns("%").CellValue(Bookmark) <> "" Then
            .Columns("%").CellStyleSet "Green", .Row
        Else
            If .Columns("%").CellValue(Bookmark) < 0 Then .Columns("%").CellStyleSet "Red", .Row
        End If
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgResultados_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgOcultarCampos_Change()
    'Oculta el campo correspondiente
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    OcultarCampo sdbgOcultarCampos.Columns("COD").Value, GridCheckToBoolean(sdbgOcultarCampos.Columns("INC").Value)
    
    g_oVistaSeleccionada.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbgOcultarCampos_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>Carga los recusrsos de idiomas en los controles del formulario</summary>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0</remarks>

Private Sub CargarRecursos()
    Dim Adores As Ador.Recordset
    Dim sCostesDescuentos As String

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADJITEM, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Adores Is Nothing Then
        lblPresupuestoLit.caption = Adores(0).Value
        m_sPresUni = Adores(0).Value
        Adores.MoveNext
        lblPresTotalLit.caption = Adores(0).Value
        Adores.MoveNext
        lblCantLit.caption = Adores(0).Value
        Adores.MoveNext
        lblProveLit.caption = Adores(0).Value
        Adores.MoveNext
        Me.caption = Adores(0).Value '5
        m_sProceso = Adores(0).Value
        Adores.MoveNext
        sdbgAdj.Columns("PROVE").caption = Adores(0).Value
        sdbgOcultarProv.Columns("PROVE").caption = Adores(0).Value
        m_sProve = Adores(0).Value
        Adores.MoveNext
        sdbgAdj.Columns("PRECIO").caption = Adores(0).Value
        m_sPrecio = Adores(0).Value
        Adores.MoveNext
        sdbgAdj.Columns("CANTADJ").caption = Adores(0).Value
        m_sCantAdj = Adores(0).Value
        Adores.MoveNext
        sdbgAdj.Columns("AHO_PORCEN").caption = Adores(0).Value
        m_sAhorroPorcen = Adores(0).Value
        Adores.MoveNext
        sdbgAdj.Columns("AHO").caption = Adores(0).Value '10
        sdbgResultados.Columns("Ahorrado").caption = Adores(0).Value
        m_sAhorro = Adores(0).Value
        Adores.MoveNext
        sdbgAdj.Columns("CONSUM").caption = Adores(0).Value
        sdbgResultados.Columns("Consumido").caption = Adores(0).Value
        m_sConsumido = Adores(0).Value
        Adores.MoveNext
        sdbgAdj.Columns("ADJ").caption = Adores(0).Value
        sdbgResultados.Columns("Adjudicado").caption = Adores(0).Value
        m_sAdjudicado = Adores(0).Value
        Adores.MoveNext
        sdbgResultados.Columns("Abierto").caption = Adores(0).Value
        Adores.MoveNext
        sdbgResultados.Columns("Proceso").caption = Adores(0).Value
        Adores.MoveNext
        cmdHojaComp.caption = Adores(0).Value '15
        Adores.MoveNext
        cmdAdjudicar.caption = Adores(0).Value
        Adores.MoveNext
        cmdGuardar.caption = Adores(0).Value
        Adores.MoveNext
        cmdRestaurar.caption = Adores(0).Value
        Adores.MoveNext
        cmdListado.caption = Adores(0).Value
        Adores.MoveNext
        sdbcItems.Columns("COD").caption = Adores(0).Value '20
        Adores.MoveNext
        sdbcItems.Columns("NOMBRE").caption = Adores(0).Value
        Adores.MoveNext
        sdbcItems.Columns("DEST").caption = Adores(0).Value
        m_sDest = Adores(0).Value
        Adores.MoveNext
        
        Adores.MoveNext
        lblUDLit.ToolTipText = Adores(0).Value
        Adores.MoveNext
        sdbgAdj.Columns("CANTMAX").caption = Adores(0).Value '25
        m_sCantMax = Adores(0).Value
        Adores.MoveNext
        lblMostrarCampos.caption = Adores(0).Value
        Adores.MoveNext
        sdbgOcultarCampos.Columns("CAMPO").caption = Adores(0).Value
        Adores.MoveNext
        sdbgOcultarCampos.Columns("Pertenece").caption = Adores(0).Value
        sdbgOptima.Columns(2).caption = Adores(0).Value
        Adores.MoveNext
        lblMostrarProv.caption = Adores(0).Value
        Adores.MoveNext
        cmdGuardarVista.caption = Adores(0).Value '30
        Adores.MoveNext
        m_sProceso = Adores(0).Value
        Adores.MoveNext
        m_sLitGrupo = Adores(0).Value 'Grupo
        Adores.MoveNext
        m_sTodos = Adores(0).Value
        Adores.MoveNext
        chkOcultarNoAdj.caption = Adores(0).Value
        Adores.MoveNext
        chkOcultarProvSinOfe.caption = Adores(0).Value '35
        Adores.MoveNext
        m_sItems = Adores(0).Value
        Adores.MoveNext
        m_sTodosLosGrupos = Adores(0).Value
        Adores.MoveNext
        m_sPuntos = Adores(0).Value
        Adores.MoveNext
        m_sItem = Adores(0).Value
        Adores.MoveNext
        m_sPrecUni = Adores(0).Value '40
        Adores.MoveNext
        m_sTotalItem = Adores(0).Value
        Adores.MoveNext
'        lblAplicarPrecio.caption = Adores(0).Value
        Adores.MoveNext
'        sdbgAplicarPrecio.Columns("COD").caption = Adores(0).Value
        sdbgOptima.Columns("COD").caption = Adores(0).Value
        Adores.MoveNext
        sdbgAplicarPrecio.Columns("Aplicar").caption = Adores(0).Value
        Adores.MoveNext
        cmdOrdernarAtrib.caption = Adores(0).Value '45
        Adores.MoveNext
        cmdAplicarPrecio.caption = Adores(0).Value
        Adores.MoveNext
        cmdDetalle.ToolTipText = Adores(0).Value
        Adores.MoveNext
        cmdOptima.ToolTipText = Adores(0).Value
        lblCalcularOpt.caption = Adores(0).Value & ":"
        Adores.MoveNext
        cmdVista.ToolTipText = Adores(0).Value
        Adores.MoveNext
'        cmdPrecios.ToolTipText = Adores(0).Value '50
        Adores.MoveNext
        cmdMostrarGrafico.ToolTipText = Adores(0).Value
        Adores.MoveNext
        m_sCantidadAdj = Adores(0).Value
        Adores.MoveNext
        m_sIdiObtenerHojaComp = Adores(0).Value
        Adores.MoveNext
        cmdCalcularOptima.caption = Adores(0).Value
        Adores.MoveNext
        sdbgOptima.Columns("PON").caption = Adores(0).Value '55
      
        Adores.MoveNext
        lblNumDec = Adores(0).Value   '56: N�mero de decimales:
        Adores.MoveNext
        lblNumDecCant = Adores(0).Value  '57 Cantidades:
        Adores.MoveNext
        lblNumDecPorcen = Adores(0).Value  '58 Porcentajes:
        Adores.MoveNext
        lblNumDecPrec = Adores(0).Value   '59 Precios:
        Adores.MoveNext
        lblNumDecResult = Adores(0).Value  '60 Resultados
        
        Adores.MoveNext
        lblObjLit.caption = Adores(0).Value  '61 obj.
        
        Adores.MoveNext
        m_sImporteOfe = Adores(0).Value   '62 Importe de la oferta
        sdbgAdj.Columns("IMP_OFE").caption = Adores(0).Value
        Adores.MoveNext
        m_sAhorroOfe = Adores(0).Value    '63 Ahorro de la oferta
        sdbgAdj.Columns("AHORRO_OFE").caption = Adores(0).Value
        Adores.MoveNext
        m_sAhorroOfePorcen = Adores(0).Value  '64 %Ahorro de la oferta
        sdbgAdj.Columns("AHORRO_OFE_PORCEN").caption = Adores(0).Value
        
        Adores.MoveNext
        m_sIdiTrue = Adores(0).Value   '65 Si
        Adores.MoveNext
        m_sIdiFalse = Adores(0).Value  '66 No
        Adores.MoveNext
        m_sIdiMayor = Adores(0).Value  '67
        Adores.MoveNext
        m_sIdiMenor = Adores(0).Value  '68
        Adores.MoveNext
        m_sIdiEntre = Adores(0).Value  '69
        Adores.MoveNext
        m_sVistaPlan = Adores(0).Value  '70  Vista de plantilla
        Adores.MoveNext
        m_sVistaResp = Adores(0).Value  '71  Vista de Responsable
        Adores.MoveNext
        m_sVistaUsu = Adores(0).Value  '72  vista de usuario
        Adores.MoveNext
        m_sGuarNue = Adores(0).Value  '73  Guardar vista nueva
        cmdGuardarVistaNueva.caption = Adores(0).Value
        Adores.MoveNext
        m_sRenonVista = Adores(0).Value  '74  Renombrar vista
        cmdRenombrarVista.caption = Adores(0).Value
        Adores.MoveNext
        m_sElimVista = Adores(0).Value  '75  eliminar vista
        cmdEliminarVista.caption = Adores(0).Value
        Adores.MoveNext
        m_sVistaIni = Adores(0).Value '76 Vista inicial
        Adores.MoveNext
        lblPlantillaDefecto.caption = Adores(0).Value '77 vista actual
        Adores.MoveNext
        lblVistaDefecto.caption = Adores(0).Value '78 vista por defecto
        
        Adores.MoveNext
        m_sIdiErrorEvaluacion(1) = Adores(0).Value  '79 Error al realizar el c�lculo:F�rmula inv�lida.
        Adores.MoveNext
        m_sIdiErrorEvaluacion(2) = Adores(0).Value  '80 Error al realizar el c�lculo:Valores incorrectos.
        
        Adores.MoveNext
        m_sLitRecalculando = Adores(0).Value '81 Recalculando ofertas
        Adores.MoveNext
        sdbgAplicarPrecio.Columns("COD").caption = Adores(0).Value  '82 Coste / descuento
        Adores.MoveNext
        sCostesDescuentos = Adores(0).Value '83 Costes / descuentos
        Adores.MoveNext
        lblAplicarPrecio.caption = Adores(0).Value  '84 "###" aplicables a precios o importes
        lblAplicarPrecio.caption = Replace(lblAplicarPrecio.caption, "###", sCostesDescuentos)
        Adores.MoveNext
        m_sPrecioImporte = Adores(0).Value '85 Precio/Importes
        Adores.MoveNext
        cmdCalcularOptima.caption = Adores(0).Value '86  Aceptar
        Adores.MoveNext
        cmdPrecios.ToolTipText = Adores(0).Value '50
        
        Adores.MoveNext
        With sdbgAdjEsc
            .Groups(0).Columns("DESCR").caption = Adores(0).Value & " (" & m_sDest & ")" & vbCrLf & vbCrLf & vbCrLf & m_sProve
            Adores.MoveNext
            m_sCantidad = Adores(0).Value
            Adores.MoveNext
            m_sCantAdjudicada = Adores(0).Value
            Adores.MoveNext
            .Groups(0).Columns("IMP").caption = Adores(0).Value & vbCrLf & vbCrLf & vbCrLf & Adores(0).Value
            m_sImpAdj = Adores(0).Value
            .Groups(0).Columns("AHORROIMP").caption = m_sAhorro & vbCrLf & vbCrLf & vbCrLf & m_sAhorro
            .Groups(0).Columns("AHORROPORCEN").caption = m_sAhorroPorcen & vbCrLf & vbCrLf & vbCrLf & m_sAhorroPorcen
        End With
        Adores.MoveNext
        m_sAdjudicar = Adores(0).Value
        Adores.MoveNext
        m_sDesadjudicar = Adores(0).Value
        Adores.MoveNext
        m_sOtrasCarac = Adores(0).Value
        Adores.MoveNext
        m_sEscalado = Adores(0).Value
        Adores.MoveNext
        m_sObjetivo = Adores(0).Value
        Adores.MoveNext
        m_sAhorUni = Adores(0).Value
        Adores.MoveNext
        With sdbgAdjEsc
            m_sProveAct = Adores(0).Value
            Adores.MoveNext
            m_sImporteConsum = Adores(0).Value
            Adores.MoveNext
            m_sPresUniMedio = Adores(0).Value
            Adores.MoveNext
            m_sPrecUniMedio = Adores(0).Value
            Adores.MoveNext
            .Groups(0).Columns("CANT").caption = Adores(0).Value & vbCrLf & vbCrLf & vbCrLf & m_sCantAdjudicada
            .Groups(0).Columns("PRECAPE").caption = m_sImporteConsum & vbCrLf & vbCrLf & vbCrLf & m_sPresUniMedio
            .Groups(0).Columns("CODPROVEACTUAL").caption = m_sProveAct & vbCrLf & vbCrLf & vbCrLf & m_sPrecUniMedio
        End With
        Adores.MoveNext
        m_sValidarOblSolCompraAdj_Inicial = Adores(0).Value & ":" '103 - Mensaje validaci�n de solicitud de compra introducida
        Adores.MoveNext
        m_sValidarOblSolCompraAdj_Final = Adores(0).Value '276 - Mensaje validaci�n de solicitud de compra introducida
    End If
    
    Adores.Close
    Set Adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
End Sub
Public Sub PrepararGridOriginal()
Dim i As Integer

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgAdj
        .Scroll -.Cols, 1
        .RemoveAll
        .LoadLayout m_sLayOut
        .FieldSeparator = Chr(m_lSeparador)
        
        For i = 1 To .Columns.Count - 1
            'Bloqueo las columnas excepto la de la cantidad
            .Columns(i).Locked = True
        Next i
        .Columns(0).Locked = True
        .Columns("CANTADJ").Locked = False
        .Columns("CANTMAX").Visible = m_oProcesoSeleccionado.SolicitarCantMax
        
        .SplitterPos = 1
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "PrepararGridOriginal", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>Prepara el gris de escalados para cargarlo con los datos de un item</summary>
''' <remarks>Llamada desde: ItemEscaladoSeleccionado; Tiempo m�ximo: 0</remarks>

Public Sub PrepararGridOriginalEsc()
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgAdjEsc
        .Scroll -sdbgAdj.Cols, 1
        .RemoveAll
        .LoadLayout m_sLayOutEsc
        .FieldSeparator = Chr(m_lSeparador)
        
        For i = 1 To .Columns.Count - 1
            'Bloqueo las columnas excepto la de la cantidad
            .Columns(i).Locked = True
        Next i
        .Columns(0).Locked = True
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "PrepararGridOriginalEsc", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Muestra el split del grid</summary>
''' <remarks>Llamada desde: ItemEscaladoSeleccionado ; Tiempo m�ximo:0</remarks>

Private Sub MostrarSplit()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgAdjEsc
        If .Groups.Count > 1 Then
            .SplitterVisible = True
            .SplitterPos = 1
        Else
            .SplitterVisible = False
        End If
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "MostrarSplit", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 10/12/2012</revision>

Private Sub ConfigurarSeguridad()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjPreadjudicar)) Is Nothing) Then
        m_bPreadjudicar = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjValidar)) Is Nothing) Then
        m_bVal = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEConsultar)) Is Nothing) Then
        m_bOfe = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPHojaComp)) Is Nothing) Then
        m_bHojaComparativa = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEPermSaltarseVolMaxAdjDir)) Is Nothing) Then
        m_bPermSaltarseVolMaxAdjDir = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjModifOfeProv)) Is Nothing) Then
        m_bModifOfeDeProv = True
    Else
        m_bModifOfeDeProv = False
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjModifOfeUsu)) Is Nothing) Then
        m_bModifOfeDeUsu = True
    Else
        m_bModifOfeDeUsu = False
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestModifOfeProv)) Is Nothing) Then
        m_bRestModifOfeProvUsu = True
    Else
        m_bRestModifOfeProvUsu = False
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestModifOfeUsu)) Is Nothing) Then
        m_bRestModifOfeUsu = True
    Else
        m_bRestModifOfeUsu = False
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjPermVistasOtros)) Is Nothing) Then
        m_bPermitirVistas = True
    Else
        m_bPermitirVistas = False
    End If
    
    If g_oOrigen.Name = "frmADJ" Then
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjPermModifVistasOtros)) Is Nothing Then
            m_bPermitirModifVistas = True
        Else
            m_bPermitirModifVistas = False
        End If
     Else 'Si viene de Reuniones
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREUPermitirModifVistasOtrosUsuarios)) Is Nothing Then
            m_bPermitirModifVistas = True
        Else
            m_bPermitirModifVistas = False
        End If
    End If
    m_bIgnBlqEscalacion = Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPIgnBlqEscalacion)) Is Nothing
    ConfigurarBotones
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ConfigurarSeguridad", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub CalcularTotalesDeProceso()
    Dim oGrupo As CGrupo
    Dim oatrib As CAtributo
    Dim sCod As String
    Dim bSiguiente As Boolean
    Dim dValorAtrib As Double
    Dim iOrden As Integer
    Dim lAtribAnterior As Long
    Dim lngMinimo As Long
    Dim oProv As CProveedor
    Dim oOferta As COferta
    
    '''  EPB:
    ''' Este m�todo calcular� para el proceso
    ''' el abierto, consumido y adjudicado antes de aplicar los atributos de total de oferta y despues
    ''' Cada vez que se modifique una adjudicaci�n o los atributos a aplicar se tiene que ejecutar,
    ''' calcula los valores de la grid de resultados para el proceso
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_dblAbiertoProc = 0
    g_dblConsumidoProc = 0
    g_dblAdjudicadoProc = 0
    
    For Each oGrupo In m_oProcesoSeleccionado.Grupos
        g_dblAbiertoProc = g_dblAbiertoProc + oGrupo.Abierto
        g_dblConsumidoProc = g_dblConsumidoProc + oGrupo.Consumido
    Next
    
    For Each oProv In m_oProvesAsig
        g_dblAdjudicadoProcAux = 0
        g_dblAdjudicadoProcAuxMonProce = 0
        For Each oGrupo In m_oProcesoSeleccionado.Grupos
            sCod = oGrupo.Codigo & oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
            If Not m_oAdjs.Item(sCod) Is Nothing Then
                g_dblAdjudicadoProcAux = g_dblAdjudicadoProcAux + m_oAdjs.Item(sCod).Adjudicado
                g_dblAdjudicadoProcAuxMonProce = g_dblAdjudicadoProcAuxMonProce + m_oAdjs.Item(sCod).AdjudicadoMonProce
            End If
        Next

        lAtribAnterior = 0
        iOrden = 1
        bSiguiente = False
        While bSiguiente = False
            lngMinimo = 0
            For Each oatrib In m_oAtribsFormulas
                If Not IsNull(oatrib.PrecioFormula) And oatrib.PrecioAplicarA = 0 Then
                    If oatrib.FlagAplicar = True Then
                        If oatrib.UsarPrec = 1 Then
                            If oatrib.Orden = iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                lngMinimo = oatrib.idAtribProce
                                Exit For
                            Else
                                If oatrib.Orden > iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                    If lngMinimo = 0 Or (oatrib.Orden < m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden) Then
                                        lngMinimo = oatrib.idAtribProce
                                    ElseIf oatrib.idAtribProce = lngMinimo Then
                                        bSiguiente = True
                                        Exit For
                                    End If
                                End If
                            End If
                        End If
                    Else
                        oatrib.FlagAplicar = True
                    End If
                End If
            Next
            
            If (lngMinimo = 0) Then
                bSiguiente = True
            Else
                dValorAtrib = 0
                Set oatrib = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(lngMinimo))
                lAtribAnterior = oatrib.idAtribProce
                iOrden = oatrib.Orden
                If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod) Is Nothing Then
                    If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados Is Nothing Then
                        If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                            dValorAtrib = NullToDbl0(m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                            If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                Select Case oatrib.PrecioFormula
                                    Case "+"
                                        g_dblAdjudicadoProcAux = g_dblAdjudicadoProcAux + dValorAtrib
                                        g_dblAdjudicadoProcAuxMonProce = g_dblAdjudicadoProcAuxMonProce + (dValorAtrib / m_oProcesoSeleccionado.CambioComparativa(oProv.Cod))
                                    Case "-"
                                        g_dblAdjudicadoProcAux = g_dblAdjudicadoProcAux - dValorAtrib
                                        g_dblAdjudicadoProcAuxMonProce = g_dblAdjudicadoProcAuxMonProce - (dValorAtrib / m_oProcesoSeleccionado.CambioComparativa(oProv.Cod))
                                    Case "/"
                                        g_dblAdjudicadoProcAux = g_dblAdjudicadoProcAux / dValorAtrib
                                        g_dblAdjudicadoProcAuxMonProce = g_dblAdjudicadoProcAuxMonProce / dValorAtrib
                                    Case "*"
                                        g_dblAdjudicadoProcAux = g_dblAdjudicadoProcAux * dValorAtrib
                                        g_dblAdjudicadoProcAuxMonProce = g_dblAdjudicadoProcAuxMonProce * dValorAtrib
                                    Case "+%"
                                        g_dblAdjudicadoProcAux = g_dblAdjudicadoProcAux + ((dValorAtrib / 100) * g_dblAdjudicadoProcAux)
                                        g_dblAdjudicadoProcAuxMonProce = g_dblAdjudicadoProcAuxMonProce + ((dValorAtrib / 100) * g_dblAdjudicadoProcAuxMonProce)
                                    Case "-%"
                                        g_dblAdjudicadoProcAux = g_dblAdjudicadoProcAux - ((dValorAtrib / 100) * g_dblAdjudicadoProcAux)
                                        g_dblAdjudicadoProcAuxMonProce = g_dblAdjudicadoProcAuxMonProce - ((dValorAtrib / 100) * g_dblAdjudicadoProcAuxMonProce)
                                End Select
                            End If
                        End If
                    End If
                End If
                
                Set oatrib = Nothing
                bSiguiente = False
                iOrden = iOrden + 1
            End If
        Wend

        Set oOferta = m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod)
        If g_dblAdjudicadoProcAux <> 0 Then
            g_dblAdjudicadoProc = g_dblAdjudicadoProc + g_dblAdjudicadoProcAuxMonProce
        End If
    Next

    g_dblAhorradoProc = g_dblConsumidoProc - g_dblAdjudicadoProc
    If g_dblConsumidoProc <> 0 Then
        g_dblAhorradoPorcenProc = (g_dblAhorradoProc / Abs(g_dblConsumidoProc)) * 100
    Else
        g_dblAhorradoPorcenProc = 0
    End If
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CalcularTotalesDeProceso", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub



Public Function AtribsTotalItemComprobar(Optional ByVal bNoComprobar As Boolean) As Boolean
    
    Dim oatrib As CAtributo
    Dim bHayAtribsItem As Boolean
    
    'Para el item seleccionado
    'Devuelve True si es posible aplicar los atributos de total de item al precio o si no hay
    'False si hay atributos y no se pueden aplicar
    
    '1� Comprueba que existen atributos a aplicar al precio
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oAtribsFormulas Is Nothing Then
        AtribsTotalItemComprobar = True
        Exit Function
    End If
    
    If m_oAtribsFormulas.Count = 0 Then
        AtribsTotalItemComprobar = True
        Exit Function
    End If
    
    bHayAtribsItem = False
    For Each oatrib In m_oAtribsFormulas
        If oatrib.codgrupo = g_oItemSeleccionado.grupoCod Or IsNull(oatrib.codgrupo) Then
            If oatrib.PrecioAplicarA <= 2 And oatrib.UsarPrec = 1 Then
                bHayAtribsItem = True
                If oatrib.PrecioAplicarA < 2 Then
                    m_bHayAtribsDeGrupoOProce = True
                End If
                Exit For
            End If
        End If
    Next
    If bHayAtribsItem = False Then
        AtribsTotalItemComprobar = True
        Exit Function
    End If
    
    AtribsTotalItemComprobar = Not bNoComprobar
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "AtribsTotalItemComprobar", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function



Private Sub CargarAtribOptima()
    'A�ade a la grid de optima todos los atributos
    Dim oatrib As CAtributo
    Dim sPertenece As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgOptima.RemoveAll
    
    sdbgOptima.AddItem "" & Chr(m_lSeparador) & m_sPrecioImporte & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sPrecio
    
    'Carga los atributos de proceso
    If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        For Each oatrib In m_oProcesoSeleccionado.ATRIBUTOS
            'Solo cargaremos los atributos que no son costes/descuentos (es decir que no son aplicables).
            If Not (oatrib.Tipo = TipoNumerico And Not IsNull(oatrib.PrecioAplicarA)) Or oatrib.Tipo <> TipoNumerico Then
                If oatrib.TipoPonderacion <> SinPonderacion Or oatrib.Tipo <> TipoString Then
                    sdbgOptima.AddItem "" & Chr(m_lSeparador) & oatrib.Cod & "   " & oatrib.Den & Chr(m_lSeparador) & m_sProceso & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & oatrib.ambito
                End If
            End If
        Next
    End If
    
    'Carga los atributos de grupo
    If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
        For Each oatrib In m_oProcesoSeleccionado.AtributosGrupo
            'Solo cargaremos los atributos que no son costes/descuentos (es decir que no son aplicables).
            If Not (oatrib.Tipo = TipoNumerico And Not IsNull(oatrib.PrecioAplicarA)) Or oatrib.Tipo <> TipoNumerico Then
                If oatrib.TipoPonderacion <> SinPonderacion Or oatrib.Tipo <> TipoString Then
                    sPertenece = m_sLitGrupo & ":"
                    If IsNull(oatrib.codgrupo) Then
                        sPertenece = sPertenece & m_sTodos
                        sdbgOptima.AddItem "" & Chr(m_lSeparador) & oatrib.Cod & "   " & oatrib.Den & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & oatrib.ambito
                    Else
                        If oatrib.codgrupo = g_ogrupo.Codigo Then
                            sPertenece = sPertenece & oatrib.codgrupo
                            sdbgOptima.AddItem "" & Chr(m_lSeparador) & oatrib.Cod & "   " & oatrib.Den & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & oatrib.ambito
                        End If
                    End If
                End If
            End If
        Next
    End If
    
    'Carga los atributos de item
    If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
        For Each oatrib In m_oProcesoSeleccionado.AtributosItem
            'Solo cargaremos los atributos que no son costes/descuentos (es decir que no son aplicables).
            If Not (oatrib.Tipo = TipoNumerico And Not IsNull(oatrib.PrecioAplicarA)) Or oatrib.Tipo <> TipoNumerico Then
                If oatrib.TipoPonderacion <> SinPonderacion Or oatrib.Tipo <> TipoString Then
                    sPertenece = m_sItem & ":"
                    If IsNull(oatrib.codgrupo) Then
                        sPertenece = sPertenece & m_sTodos
                        sdbgOptima.AddItem "" & Chr(m_lSeparador) & oatrib.Cod & "   " & oatrib.Den & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & oatrib.ambito
                    Else
                        If oatrib.codgrupo = g_ogrupo.Codigo Then
                            sPertenece = sPertenece & oatrib.codgrupo
                            sdbgOptima.AddItem "" & Chr(m_lSeparador) & oatrib.Cod & "   " & oatrib.Den & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & oatrib.ambito
                        End If
                    End If
                End If
            End If
        Next
    End If
    
    sdbgOptima.MoveFirst
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CargarAtribOptima", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

''' <summary>Lanza el c�lculo de la �ptima seg�n el tipo seleccionado y el estado del proceso</summary>
''' <remarks>Llamada desde: MDI</remarks>

Public Sub CalcularOptima(ByVal TipoOptima As TipoOptimaProceso)
    If Not g_oItemSeleccionado.Cerrado Then CalcularOptimaItem
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREU", "CalcularOptima", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Calcular la optima por un atributo dentro de la vista de analisis de un item</summary>
''' <remarks>Llamada desde: cmdCalcularOptima_Click; Tiempo m�ximo: 0,1</remarks>
''' <revision>LTG 15/11/2011</revision>

Private Sub CalcularOptimaItem()
    Dim oatrib As CAtributoOfertado
    Dim oProv As CProveedor
    Dim sProv() As String
    Dim bMejor As Boolean
    Dim vValor As Variant
    Dim i As Integer
    Dim j As Integer
    Dim sCod As String
    Dim dporcentaje As Double
    Dim bHayCkeck As Boolean
    Dim iTipo As Integer
    Dim iProv As Integer
    Dim dCantAdj As Double
    Dim dResto As Double
    Dim dadj As Double
    Dim dCantMax As Double
    Dim dCapacidad As Variant
    Dim bEmpatados As Boolean
    Dim k As Integer
    Dim dUltimoCantAdj As Double
    Dim sMensaje As String
    Dim sEmpatados() As String
    Dim bPreferencia As Boolean
    Dim vbm As Variant
    Dim scod2 As Variant
    Dim scod1 As String
    Dim sOp As String
    Dim oOferta As COferta
    Dim lIdEscalado As Long
    Dim dblImporteEsc As Double
    Dim lEsc() As Long
    Dim dblImpOfeEsc() As Double
    Dim sItemEscEmpatados() As String
    Dim dblImporteAux As Double
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bHayCkeck = False
    For i = 0 To sdbgOptima.Rows - 1
        vbm = sdbgOptima.AddItemBookmark(i)
        If sdbgOptima.Columns("INC").CellValue(vbm) = True Then
            bHayCkeck = True
            Exit For
        End If
    Next i
    
    If bHayCkeck = False Then Exit Sub
        
    sMensaje = ""
    
    'Si no est� calculada la ponderaci�n de los atributos se calcula
    If Not g_oOrigen.g_PonderacionCalculada Then
        g_oOrigen.CalcularPonderacionAtributosOfertados
    End If
    
    'Recalculo el consumido sin el item, lo sumo al final
    g_ogrupo.Consumido = g_ogrupo.Consumido - DevolverImporteConsumido
    For Each oProv In m_oProvesAsig
        sCod = g_ogrupo.Codigo & Trim(oProv.Cod) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(Trim(oProv.Cod)))
        
        If Not m_oAdjs.Item(sCod) Is Nothing Then
            scod2 = ComprobarAdjudicacion(oProv.Cod)
            If Not IsNull(scod2) And scod2 <> "" Then
                If ComprobarOferta(oProv.Cod) Then
                    'En moneda oferta
                    If g_ogrupo.UsarEscalados Then
                        m_oAdjs.Item(sCod).Consumido = m_oAdjs.Item(sCod).Consumido - (NullToDbl0(g_oAdjsProveEsc.Item(scod2).Adjudicado) * NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta))
                    Else
                        m_oAdjs.Item(sCod).Consumido = m_oAdjs.Item(sCod).Consumido - ((NullToDbl0(g_ogrupo.Adjudicaciones.Item(scod2).Porcentaje) * g_oItemSeleccionado.Cantidad / 100) * NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta))
                    End If
                End If
            End If
        End If
    Next
        
    'Primero limpiamos las adjudicaciones existentes
    LimpiarAdjudicaciones

    'Calcula la �ptima para el item seleccionado
    bEmpatados = False
    ReDim sProv(1)
    ReDim sItemEscEmpatados(1)
    If g_ogrupo.UsarEscalados Then
        ReDim lEsc(1)
        ReDim dblImpOfeEsc(1)
    End If
    
    For i = 0 To sdbgOptima.Rows - 1
        vbm = sdbgOptima.AddItemBookmark(i)
        If GridCheckToBoolean(sdbgOptima.Columns("INC").CellValue(vbm)) = True Then
            If sdbgOptima.Columns("ID").CellValue(vbm) = m_sPrecio Then
                'Es el precio de la oferta
                bMejor = True
                'Si hay
                If g_ogrupo.UsarEscalados <> 1 And bEmpatados Then
                    'Si hay proveedores empatados mira a ver si alguno tiene mejor precio de la oferta
                    ReDim sEmpatados(1)
                    For k = 1 To UBound(sProv)
                        If ComprobarOferta(sProv(k)) Then
                            If Not IsNull(g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta) Then
                                If sEmpatados(1) = "" Then
                                    ReDim sEmpatados(1)
                                    sEmpatados(1) = sProv(k)
                                    bMejor = True
                                Else
                                    'k es el mejor
                                    If NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).importe / g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).Cambio) < NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sProv(k - 1))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).importe / g_ogrupo.UltimasOfertas.Item(Trim(sProv(k - 1))).Cambio) Then
                                        ReDim sEmpatados(1)
                                        sEmpatados(1) = sProv(k)
                                        bMejor = True
                                    'hay empate
                                    ElseIf NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).importe / g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).Cambio) = NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sProv(k - 1))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).importe / g_ogrupo.UltimasOfertas.Item(Trim(sProv(k - 1))).Cambio) Then
                                        j = UBound(sEmpatados) + 1
                                        ReDim Preserve sEmpatados(j)
                                        sEmpatados(j) = sProv(k)
                                        bMejor = False
                                        bEmpatados = True
                                    End If
                                End If
                            End If
                        End If
                    Next k
                    
                    If UBound(sEmpatados) > 1 And Not bMejor Then
                        ReDim sProv(UBound(sEmpatados))
                        For iProv = 1 To UBound(sEmpatados)
                            sProv(iProv) = sEmpatados(iProv)
                        Next iProv
                        bEmpatados = True
                    Else
                        ReDim sProv(1)
                        sProv(1) = sEmpatados(1)
                        bEmpatados = False
                        bMejor = True
                    End If
                Else
                    'No hay proveedores empatados
                    For Each oProv In m_oProvesAsig
                        'Inicializo el consumido
                        sCod = g_ogrupo.Codigo & Trim(oProv.Cod) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(Trim(oProv.Cod)))
                        If Not m_oAdjs.Item(sCod) Is Nothing Then
                            If ComprobarOferta(oProv.Cod) Then
                                Set oOferta = g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod))
                                
                                If g_ogrupo.UsarEscalados Then
                                    If Not oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Escalados Is Nothing Then
                                        dblImporteEsc = (ImporteOptimoItemEsc(oProv.Cod, g_oItemSeleccionado, oOferta, lIdEscalado, m_oProcesoSeleccionado, m_oAtribsFormulas, dblImporteAux) / oOferta.Cambio)
                                        
                                        If sProv(1) = "" Then
                                            ReDim sProv(1)
                                            sProv(1) = oProv.Cod
                                            lEsc(1) = lIdEscalado
                                            dblImpOfeEsc(1) = dblImporteEsc
                                        Else
                                            If lIdEscalado <> 0 And (dblImporteEsc < dblImpOfeEsc(1) And sProv(1) <> oProv.Cod) Then
                                                ReDim sProv(1)
                                                ReDim lEsc(1)
                                                sProv(1) = oProv.Cod
                                                lEsc(1) = lIdEscalado
                                                dblImpOfeEsc(1) = dblImporteEsc
                                                bMejor = True
                                            ElseIf lIdEscalado <> 0 And dblImporteEsc = dblImpOfeEsc(1) Then
                                                'Si hay proveedores empatados se omitir� y se adjudica al primero de ellos o al de cantidad/rango m�s alto
                                                If sItemEscEmpatados(1) = "" Then
                                                    sItemEscEmpatados(1) = g_oItemSeleccionado.ArticuloCod & " " & g_oItemSeleccionado.Descr
                                                Else
                                                    ReDim Preserve sItemEscEmpatados(UBound(sItemEscEmpatados) + 1)
                                                    sItemEscEmpatados(UBound(sItemEscEmpatados)) = g_oItemSeleccionado.Id & " " & g_oItemSeleccionado.Descr
                                                End If
                                                    
                                                If g_ogrupo.Escalados.Item(CStr(lIdEscalado)).Inicial > g_ogrupo.Escalados.Item(CStr(lEsc(1))).Inicial Then
                                                    ReDim sProv(1)
                                                    ReDim lEsc(1)
                                                    sProv(1) = oProv.Cod
                                                    lEsc(1) = lIdEscalado
                                                    bMejor = False
                                                    bEmpatados = True
                                                End If
                                            End If
                                        End If
                                    End If
                                Else
                                    If Not IsNull(oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta) Then
                                        If sProv(1) = "" Then
                                            ReDim sProv(1)
                                            sProv(1) = oProv.Cod
                                            bMejor = True
                                        Else
                                            'No hay proveedores empatados
                                            If NullToDbl0(oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).importe / oOferta.Cambio) < NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sProv(1))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).importe / g_ogrupo.UltimasOfertas.Item(Trim(sProv(1))).Cambio) And sProv(1) <> oProv.Cod Then
                                                ReDim sProv(1)
                                                sProv(1) = oProv.Cod
                                                bMejor = True
                                            ElseIf NullToDbl0(oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).importe / oOferta.Cambio) = NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sProv(1))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).importe / g_ogrupo.UltimasOfertas.Item(Trim(sProv(1))).Cambio) Then
                                                j = UBound(sProv) + 1
                                                ReDim Preserve sProv(j)
                                                sProv(j) = oProv.Cod
                                                bMejor = False
                                                bEmpatados = True
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            
            Else   'Es un atributo
                vValor = 0
                bMejor = False
                
                If bEmpatados = True Then
                    ReDim sEmpatados(1)
                    'Hay proveedores empatados
                    For k = 1 To UBound(sProv)
                        sOp = ""
                        If Not g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))) Is Nothing Then
                            Select Case sdbgOptima.Columns("AMBITO").CellValue(vbm)
                                Case AmbProceso
                                    If Not m_oProcesoSeleccionado.Ofertas.Item(sProv(k)).AtribProcOfertados Is Nothing Then
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(sProv(k)).AtribProcOfertados.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))) Is Nothing Then
                                            Set oatrib = m_oProcesoSeleccionado.Ofertas.Item(sProv(k)).AtribProcOfertados.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm)))
                                            iTipo = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).Tipo
                                            bPreferencia = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PreferenciaValorBajo
                                            sOp = NullToStr(m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PrecioFormula)
                                        End If
                                    End If
                                Case AmbGrupo
                                    If Not m_oProcesoSeleccionado.Ofertas.Item(sProv(k)).AtribGrOfertados Is Nothing Then
                                        scod1 = g_ogrupo.Codigo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(g_ogrupo.Codigo))
                                        scod1 = scod1 & CStr(sdbgOptima.Columns("ID").CellValue(vbm))
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(sProv(k)).AtribGrOfertados.Item(scod1) Is Nothing Then
                                            Set oatrib = m_oProcesoSeleccionado.Ofertas.Item(sProv(k)).AtribGrOfertados.Item(scod1)
                                            iTipo = m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).Tipo
                                            bPreferencia = m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PreferenciaValorBajo
                                            sOp = NullToStr(m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PrecioFormula)
                                        End If
                                    End If
                                Case AmbItem
                                    If Not g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))) Is Nothing Then
                                        If Not g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).AtribItemOfertados Is Nothing Then
                                            If Not g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(sdbgOptima.Columns("ID").CellValue(vbm))) Is Nothing Then
                                                Set oatrib = g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(sdbgOptima.Columns("ID").CellValue(vbm)))
                                                iTipo = m_oProcesoSeleccionado.AtributosItem.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).Tipo
                                                bPreferencia = m_oProcesoSeleccionado.AtributosItem.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PreferenciaValorBajo
                                                sOp = NullToStr(m_oProcesoSeleccionado.AtributosItem.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PrecioFormula)
                                            End If
                                        End If
                                    End If
                            End Select
                            
                            If Not oatrib Is Nothing Then
                                If GridCheckToBoolean(sdbgOptima.Columns("PON").CellValue(vbm)) = True Then
                                    If Not IsEmpty(oatrib.ValorPond) And Not IsNull(oatrib.ValorPond) Then
                                        'Cogemos el valor de ponderaci�n
                                        If sEmpatados(1) = "" Then
                                            ReDim sEmpatados(1)
                                            sEmpatados(1) = sProv(k)
                                            vValor = oatrib.ValorPond
                                            bMejor = True
                                        ElseIf oatrib.ValorPond > vValor Then
                                            ReDim sEmpatados(1)
                                            vValor = oatrib.ValorPond
                                            sEmpatados(1) = sProv(k)
                                            bMejor = True
                                        ElseIf oatrib.ValorPond = vValor Then
                                            j = UBound(sEmpatados) + 1
                                            ReDim Preserve sEmpatados(j)
                                            sEmpatados(j) = sProv(k)
                                            bMejor = False
                                            bEmpatados = True
                                        End If
                                    End If
                                
                                Else
                                    'Cogemos el valor del atributo
                                    Select Case iTipo
                                        Case TiposDeAtributos.TipoBoolean
                                            If sEmpatados(1) = "" Then
                                                ReDim sEmpatados(1)
                                                sEmpatados(1) = sProv(k)
                                                vValor = oatrib.valorBool
                                                bMejor = True
                                            ElseIf oatrib.valorBool = vValor Then
                                                j = UBound(sEmpatados) + 1
                                                ReDim Preserve sEmpatados(j)
                                                sEmpatados(j) = sProv(k)
                                                bMejor = False
                                                bEmpatados = True
                                            ElseIf (bPreferencia = True And oatrib.valorBool = vbNo) Or _
                                                   (bPreferencia = False And oatrib.valorBool = vbYes) Then
                                                ReDim sEmpatados(1)
                                                vValor = oatrib.valorBool
                                                sEmpatados(1) = sProv(k)
                                                bMejor = True
                                            End If
                                            
                                        Case TiposDeAtributos.TipoFecha
                                            If sEmpatados(1) = "" Then
                                                ReDim sEmpatados(1)
                                                sEmpatados(1) = sProv(k)
                                                vValor = oatrib.valorFec
                                                bMejor = True
                                            ElseIf oatrib.valorFec = vValor Then
                                                j = UBound(sEmpatados) + 1
                                                ReDim Preserve sEmpatados(j)
                                                sEmpatados(j) = sProv(k)
                                                bMejor = False
                                                bEmpatados = True
                                            Else
                                                If bPreferencia Then   'Preferibles m�s reciente
                                                    If oatrib.valorFec < Date Then  'Fecha pasada
                                                        If oatrib.valorFec > vValor Then
                                                            ReDim sEmpatados(1)
                                                            vValor = oatrib.valorFec
                                                            sEmpatados(1) = sProv(k)
                                                            bMejor = True
                                                        End If
                                                    Else   'Fecha futura
                                                        If vValor > Date Then
                                                            If oatrib.valorFec < vValor Then
                                                                ReDim sEmpatados(1)
                                                                vValor = oatrib.valorFec
                                                                sEmpatados(1) = sProv(k)
                                                                bMejor = True
                                                            End If
                                                        End If
                                                    End If
                                                
                                                Else  'bPreferencia=false
                                                   If oatrib.valorFec < Date Then  'Fecha pasada
                                                        If oatrib.valorFec < vValor Then
                                                            ReDim sEmpatados(1)
                                                            vValor = oatrib.valorFec
                                                            sEmpatados(1) = sProv(k)
                                                            bMejor = True
                                                        End If
                                                    Else   'Fecha futura
                                                        If vValor < Date Then
                                                            If oatrib.valorFec > vValor Then
                                                                ReDim sEmpatados(1)
                                                                vValor = oatrib.valorFec
                                                                sEmpatados(1) = sProv(k)
                                                                bMejor = True
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                            
                                        Case TiposDeAtributos.TipoNumerico
                                            If sEmpatados(1) = "" Then
                                                sEmpatados(1) = sProv(k)
                                                If sOp = "+" Or sOp = "-" Then
                                                    vValor = oatrib.valorNum / g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).Cambio
                                                Else
                                                    vValor = oatrib.valorNum
                                                End If
                                                bMejor = True
                                            Else
                                                If sOp = "+" Or sOp = "-" Then
                                                    If (bPreferencia = True And (oatrib.valorNum / g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).Cambio) < vValor) Or _
                                                       (bPreferencia = False And (oatrib.valorNum / g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).Cambio) > vValor) Then
                                                        ReDim sEmpatados(1)
                                                        vValor = oatrib.valorNum / g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).Cambio
                                                        sEmpatados(1) = sProv(k)
                                                        bMejor = True
                                                    ElseIf (oatrib.valorNum / g_ogrupo.UltimasOfertas.Item(Trim(sProv(k))).Cambio) = vValor Then
                                                        j = UBound(sEmpatados) + 1
                                                        ReDim Preserve sEmpatados(j)
                                                        sEmpatados(j) = sProv(k)
                                                        bMejor = False
                                                        bEmpatados = True
                                                    End If
                                                Else
                                                    If (bPreferencia = True And oatrib.valorNum < vValor) Or _
                                                       (bPreferencia = False And oatrib.valorNum > vValor) Then
                                                        ReDim sEmpatados(1)
                                                        vValor = oatrib.valorNum
                                                        sEmpatados(1) = sProv(k)
                                                        bMejor = True
                                                    ElseIf oatrib.valorNum = vValor Then
                                                        j = UBound(sEmpatados) + 1
                                                        ReDim Preserve sEmpatados(j)
                                                        sEmpatados(j) = sProv(k)
                                                        bMejor = False
                                                        bEmpatados = True
                                                    End If
                                                End If
                                            End If
                                    End Select
                                End If
                            End If
                        End If
                    Next
                    If UBound(sEmpatados) > 1 And bMejor = False Then
                        ReDim sProv(UBound(sEmpatados))
                        For iProv = 1 To UBound(sEmpatados)
                            sProv(iProv) = sEmpatados(iProv)
                        Next iProv
                        bEmpatados = True
                    ElseIf sEmpatados(1) <> "" Then
                        ReDim sProv(1)
                        sProv(1) = sEmpatados(1)
                        bEmpatados = False
                        bMejor = True
                    End If

                Else
                    'No hay proveedores empatados
                    For Each oProv In m_oProvesAsig
                        If Not g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)) Is Nothing Then
                            sOp = ""
                            Select Case sdbgOptima.Columns("AMBITO").CellValue(vbm)
                                Case AmbProceso
                                    If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados Is Nothing Then
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))) Is Nothing Then
                                            Set oatrib = m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribProcOfertados.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm)))
                                            iTipo = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).Tipo
                                            bPreferencia = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PreferenciaValorBajo
                                            sOp = NullToStr(m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PrecioFormula)
                                        End If
                                    End If
                                Case AmbGrupo
                                    If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribGrOfertados Is Nothing Then
                                        scod1 = g_ogrupo.Codigo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(g_ogrupo.Codigo))
                                        scod1 = scod1 & CStr(sdbgOptima.Columns("ID").CellValue(vbm))

                                        If Not m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribGrOfertados.Item(scod1) Is Nothing Then
                                            Set oatrib = m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).AtribGrOfertados.Item(scod1)
                                            iTipo = m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).Tipo
                                            bPreferencia = m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PreferenciaValorBajo
                                            sOp = NullToStr(m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PrecioFormula)
                                        End If
                                    End If
                                Case AmbItem
                                    If Not g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)) Is Nothing Then
                                        If Not g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).AtribItemOfertados Is Nothing Then
                                            If Not g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(sdbgOptima.Columns("ID").CellValue(vbm))) Is Nothing Then
                                                Set oatrib = g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).AtribItemOfertados.Item(CStr(g_oItemSeleccionado.Id) & "$" & CStr(sdbgOptima.Columns("ID").CellValue(vbm)))
                                                iTipo = m_oProcesoSeleccionado.AtributosItem.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).Tipo
                                                bPreferencia = m_oProcesoSeleccionado.AtributosItem.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PreferenciaValorBajo
                                                sOp = NullToStr(m_oProcesoSeleccionado.AtributosItem.Item(CStr(sdbgOptima.Columns("ID").CellValue(vbm))).PrecioFormula)
                                            End If
                                        End If
                                    End If
                            End Select
                        
                            If Not oatrib Is Nothing Then
                                If GridCheckToBoolean(sdbgOptima.Columns("PON").CellValue(vbm)) = True Then
                                    If Not IsEmpty(oatrib.ValorPond) And Not IsNull(oatrib.ValorPond) Then
                                        'Cogemos el valor de ponderaci�n
                                        If sProv(1) = "" Then
                                            ReDim sProv(1)
                                            sProv(1) = oProv.Cod
                                            vValor = oatrib.ValorPond
                                            bMejor = True
                                        ElseIf oatrib.ValorPond > vValor Then
                                            ReDim sProv(1)
                                            vValor = oatrib.ValorPond
                                            sProv(1) = oProv.Cod
                                            bMejor = True
                                        ElseIf oatrib.ValorPond = vValor Then
                                            j = UBound(sProv) + 1
                                            ReDim Preserve sProv(j)
                                            sProv(j) = oProv.Cod
                                            bMejor = False
                                            bEmpatados = True
                                        End If
                                    End If
                                Else
                                    'Cogemos el valor del atributo normal
                                    Select Case iTipo
                                        Case TiposDeAtributos.TipoBoolean
                                            If sProv(1) = "" Then
                                                ReDim sProv(1)
                                                sProv(1) = oProv.Cod
                                                vValor = oatrib.valorBool
                                                bMejor = True
                                            ElseIf oatrib.valorBool = vValor Then
                                                j = UBound(sProv) + 1
                                                ReDim Preserve sProv(j)
                                                sProv(j) = oProv.Cod
                                                bMejor = False
                                                bEmpatados = True
                                            ElseIf (bPreferencia = True And oatrib.valorBool = vbNo) Or _
                                                (bPreferencia = False And oatrib.valorBool = vbYes) Then
                                                ReDim sProv(1)
                                                vValor = oatrib.valorBool
                                                sProv(1) = oProv.Cod
                                                bMejor = True
                                            End If
        
                                        Case TiposDeAtributos.TipoFecha
                                            If sProv(1) = "" Then
                                                ReDim sProv(1)
                                                sProv(1) = oProv.Cod
                                                vValor = oatrib.valorFec
                                                bMejor = True
                                            ElseIf oatrib.valorFec = vValor Then
                                                j = UBound(sProv) + 1
                                                ReDim Preserve sProv(j)
                                                sProv(j) = oProv.Cod
                                                bMejor = False
                                                bEmpatados = True
                                            Else
                                                If bPreferencia = True Then  'Preferibles m�s reciente
                                                    If oatrib.valorFec < Date Then  'Fecha pasada
                                                        If oatrib.valorFec > vValor Then
                                                            ReDim sProv(1)
                                                            vValor = oatrib.valorFec
                                                            sProv(1) = oProv.Cod
                                                            bMejor = True
                                                        End If
                                                    Else   'Fecha futura
                                                        If vValor > Date Then
                                                            If oatrib.valorFec < vValor Then
                                                                ReDim sProv(1)
                                                                vValor = oatrib.valorFec
                                                                sProv(1) = oProv.Cod
                                                                bMejor = True
                                                            End If
                                                        End If
                                                    End If
                                                
                                                Else  'bPreferencia=false
                                                   If oatrib.valorFec < Date Then  'Fecha pasada
                                                        If oatrib.valorFec < vValor Then
                                                            ReDim sProv(1)
                                                            vValor = oatrib.valorFec
                                                            sProv(1) = oProv.Cod
                                                            bMejor = True
                                                        End If
                                                    Else   'Fecha futura
                                                        If vValor < Date Then
                                                            If oatrib.valorFec > vValor Then
                                                                ReDim sProv(1)
                                                                vValor = oatrib.valorFec
                                                                sProv(1) = oProv.Cod
                                                                bMejor = True
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                            
                                        Case TiposDeAtributos.TipoNumerico
                                            If sProv(1) = "" Then
                                                sProv(1) = oProv.Cod
                                                If sOp = "+" Or sOp = "-" Then
                                                    vValor = oatrib.valorNum / g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).Cambio
                                                Else
                                                    vValor = oatrib.valorNum
                                                End If
                                                bMejor = True
                                            Else
                                                If sOp = "+" Or sOp = "-" Then
                                                    If (bPreferencia = True And (oatrib.valorNum / g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).Cambio) < vValor) Or _
                                                           (bPreferencia = False And (oatrib.valorNum / g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).Cambio) > vValor) Then
                                                        ReDim sProv(1)
                                                        vValor = oatrib.valorNum / g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).Cambio
                                                        sProv(1) = oProv.Cod
                                                        bMejor = True
                                                    ElseIf (oatrib.valorNum / g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).Cambio) = vValor Then
                                                        j = UBound(sProv) + 1
                                                        ReDim Preserve sProv(j)
                                                        sProv(j) = oProv.Cod
                                                        bMejor = False
                                                        bEmpatados = True
                                                    End If
                                                Else
                                                    If (bPreferencia = True And oatrib.valorNum < vValor) Or _
                                                           (bPreferencia = False And oatrib.valorNum > vValor) Then
                                                        ReDim sProv(1)
                                                        vValor = oatrib.valorNum
                                                        sProv(1) = oProv.Cod
                                                        bMejor = True
                                                    ElseIf oatrib.valorNum = vValor Then
                                                        j = UBound(sProv) + 1
                                                        ReDim Preserve sProv(j)
                                                        sProv(j) = oProv.Cod
                                                        bMejor = False
                                                        bEmpatados = True
                                                    End If
                                                End If
                                            End If
                                    End Select
                                End If
                            End If
                        End If
                    Next
                    
                End If  'bEmpatados=true
                Set oatrib = Nothing
            End If
            
            If bMejor = True Then Exit For
        End If
    Next
    
'zzz tengo que avisar si hay atributos de total proceso o grupo y cambia la adjudicaci�n aunque s�lo haya uno
    'Ahora adjudica el item a los proveedores correspondientes
    If bMejor And UBound(sProv) > 0 Then
        'Adjudica todo lo que se pueda al proveedor
        If ComprobarOferta(sProv(1)) Then
            Set oOferta = g_ogrupo.UltimasOfertas.Item(Trim(sProv(1)))
            If g_ogrupo.UsarEscalados Then
                If Not oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Escalados.Item(CStr(lEsc(1))) Is Nothing Then
                    If Not IsNull(oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Escalados.Item(CStr(lEsc(1))).PrecioOferta) Then
                        sCod = sProv(1) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv(1)))
                        sCod = CStr(g_oItemSeleccionado.Id) & sCod & CStr(lEsc(1))
                                            
                        g_ogrupo.Adjudicaciones.Add sProv(1), oOferta.Num, g_oItemSeleccionado.Id, oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).Escalados.Item(CStr(lEsc(1))).PrecioOferta, 100, , , , , , , , , lEsc(1), , , , oOferta.Cambio
                    End If
                End If
            Else
                If oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).CantidadMaxima = 0 Or IsNull(oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).CantidadMaxima) Then
                    'No tiene cantidad m�xima.Se adjudica todo al proveedor
                    dporcentaje = 100
                Else   'Tiene cantidad m�xima
                    If oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).CantidadMaxima >= g_oItemSeleccionado.Cantidad Then
                        dporcentaje = 100
                    Else
                        dporcentaje = (oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).CantidadMaxima / g_oItemSeleccionado.Cantidad) * 100
                        'Saca un mensaje indicando que no se ha podido adjudicar toda la cantidad del item
                        sMensaje = sMensaje & g_oItemSeleccionado.ArticuloCod & " " & g_oItemSeleccionado.Descr & "   " & sCantSinAdj & (g_oItemSeleccionado.Cantidad - oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).CantidadMaxima) & vbCrLf
                    End If
                End If
                
                If Not IsNull(oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta) Then
                    sCod = CStr(g_oItemSeleccionado.Id) & sProv(1) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv(1)))
                    If g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        If dporcentaje <> 0 Then
                            g_ogrupo.Adjudicaciones.Add sProv(1), oOferta.Num, g_oItemSeleccionado.Id, oOferta.Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta, dporcentaje, vCambio:=oOferta.Cambio
                        End If
                    Else
                        g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje = dporcentaje
                        If dporcentaje = 0 Then
                            g_ogrupo.Adjudicaciones.Remove sCod
                        End If
                    End If
    
                End If
            End If
            
            Set oOferta = Nothing
        End If
        ReDim sProv(1)
        If g_ogrupo.UsarEscalados Then
            ReDim lEsc(1)
            ReDim dblImpOfeEsc(1)
        End If
        
    Else
        Dim irespuesta As Integer
        If UBound(sProv) > 1 Then
            If Not AtribsTotalItemComprobar(True) Then
                irespuesta = oMensajes.PreguntaDeshabilitarAtribsAplicados
                If irespuesta = vbNo Then
                    Exit Sub
                End If
                If m_bHayAtribsDeGrupoOProce = True Then
                    m_bCambianTodasAdjs = True
                End If
            End If
        End If
        If sProv(1) = "" Then
            j = 1
            For Each oProv In m_oProvesAsig
                If ComprobarOferta(oProv.Cod) Then
                    ReDim Preserve sProv(j)
                    sProv(j) = oProv.Cod
                    j = j + 1
                End If
            Next
        End If
        
        'Adjudica todo el proceso en cantidades proporcionales a los proveedores empatados
        j = UBound(sProv)
        dResto = g_oItemSeleccionado.Cantidad
        dCantAdj = ParteEntera(g_oItemSeleccionado.Cantidad / j)
        dCapacidad = 0
        If m_oProcesoSeleccionado.SolicitarCantMax = False Then
            dUltimoCantAdj = g_oItemSeleccionado.Cantidad - ((j - 1) * dCantAdj)
        End If
        
        While dResto > 0
            For iProv = 1 To j
                If dResto <= 0 Then Exit For
                If Not g_ogrupo.UltimasOfertas.Item(Trim(sProv(iProv))) Is Nothing Then
                    dporcentaje = 0  'Porcentaje a adjudicar
                    sCod = CStr(g_oItemSeleccionado.Id) & sProv(iProv) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv(iProv)))
                    If g_ogrupo.UsarEscalados Then sCod = sCod & CStr(lEsc(iProv))
                    
                    'Cantidad que ya est� adjudicada para ese item y proveedor:
                    dadj = 0
                    If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        dadj = (g_oItemSeleccionado.Cantidad * g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje) / 100
                    End If
                    
                    If g_ogrupo.UsarEscalados Then
                        dporcentaje = 100
                        dResto = 0
                    Else
                        If Not m_oProcesoSeleccionado.SolicitarCantMax Then
                            'Si no hay cantidad m�xima
                            dCapacidad = Null
                            If iProv = j Then
                                dporcentaje = (dUltimoCantAdj / g_oItemSeleccionado.Cantidad) * 100
                                dResto = dResto - dUltimoCantAdj
                            Else
                                dporcentaje = (dCantAdj / g_oItemSeleccionado.Cantidad) * 100
                                dResto = dResto - dCantAdj
                            End If
                            
                        Else
                            'Hay cantidad m�xima:
                            'Cantidad m�xima del proveedor
                            dCantMax = NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sProv(iProv))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).CantidadMaxima)
                            If dCantMax = 0 Then
                                dporcentaje = (dCantAdj / g_oItemSeleccionado.Cantidad) * 100
                                dResto = dResto - dCantAdj
                            Else
                                If dCantMax > dadj Then
                                    If dCantMax - dadj >= dCantAdj Then
                                        dporcentaje = (dCantAdj / g_oItemSeleccionado.Cantidad) * 100
                                        dResto = dResto - dCantAdj
                                    Else
                                        dResto = dResto - dCantMax - dadj
                                        dporcentaje = (dCantMax / g_oItemSeleccionado.Cantidad) * 100
                                    End If
                                End If
                            End If
                        End If
                    End If
                    
                    'Lo a�ade a la colecci�n
                    If dporcentaje > 0 Then
                        If g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                            If g_ogrupo.UsarEscalados Then
                                g_ogrupo.Adjudicaciones.Add sProv(iProv), g_ogrupo.UltimasOfertas.Item(Trim(sProv(1))).Num, g_oItemSeleccionado.Id, g_ogrupo.UltimasOfertas.Item(Trim(sProv(1))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).Escalados.Item(CStr(lEsc(1))).PrecioOferta, _
                                    dporcentaje, , , , , , , , , lEsc(1), , , , g_ogrupo.UltimasOfertas.Item(Trim(sProv(1))).Cambio
                            Else
                                g_ogrupo.Adjudicaciones.Add sProv(iProv), g_ogrupo.UltimasOfertas.Item(Trim(sProv(1))).Num, g_oItemSeleccionado.Id, g_ogrupo.UltimasOfertas.Item(Trim(sProv(1))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta, _
                                    dporcentaje, vCambio:=g_ogrupo.UltimasOfertas.Item(Trim(sProv(1))).Cambio
                            End If
                        Else
                            g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje = g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje + dporcentaje
                        End If
                    End If
                End If
            Next
            
            'Si no se ha adjudicado toda la cantidad se distribuye otra vez en partes proporcionales
            If dResto > 0 And Not IsNull(dCapacidad) Then
                j = 0
                dCapacidad = 0
                For iProv = 1 To UBound(sProv)
                    sCod = CStr(g_oItemSeleccionado.Id) & sProv(iProv) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv(iProv)))
                    dCantMax = NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sProv(iProv))).Lineas.Item(CStr(g_oItemSeleccionado.Id)).CantidadMaxima)
                    If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        dadj = (g_oItemSeleccionado.Cantidad * g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje) / 100
                    Else
                        dadj = 0
                    End If
                    If Not IsNull(dCantMax) And dCantMax <> 0 Then
                        'Si tiene cantidad m�xima
                        If dCantMax > dadj Then
                            j = j + 1
                            dCapacidad = dCapacidad + dCantMax - dadj
                        End If
                    Else
                        'No tiene cantidad m�xima
                        j = j + 1
                        dCapacidad = Null
                    End If
                    
                Next
                
                If j > 0 Then
                    dCantAdj = ParteEntera(dResto / j)
                    If dCantAdj = 0 Then
                        dCantAdj = 1
                    End If
                Else
                    dCapacidad = Null
                    'Saca un mensaje indicando que no se ha podido adjudicar toda la cantidad del item
                    sMensaje = sMensaje & g_oItemSeleccionado.ArticuloCod & "  " & g_oItemSeleccionado.Descr & "   " & sCantSinAdj & dResto & vbCrLf
                    dResto = 0
                End If
            End If
            
        Wend
        
        ReDim sProv(1)
        If g_ogrupo.UsarEscalados Then
            ReDim lEsc(1)
            ReDim dblImpOfeEsc(1)
        End If
    End If
               
    Set g_oAdjsProveEsc = CalcularAdjudicacionesGruposEscalados(m_oProcesoSeleccionado, m_oProcesoSeleccionado.Grupos, m_oProvesAsig, m_oAtribsFormulas)
    Set m_oAdjsProve = CalcularAdjudicacionesInterno(m_oProcesoSeleccionado, g_ogrupo, m_oProvesAsig, m_oAtribsFormulas)
    'Aplica las f�rmulas y actualiza las grids
    CalcularPrecioConFormulas
    
    'Recalculo el consumido sumando el nuevo del item
    g_ogrupo.Consumido = g_ogrupo.Consumido + DevolverImporteConsumido
    For Each oProv In m_oProvesAsig
        sCod = g_ogrupo.Codigo & Trim(oProv.Cod) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(Trim(oProv.Cod)))
        If Not m_oAdjs.Item(sCod) Is Nothing Then
            If Not g_ogrupo.Adjudicaciones Is Nothing Then
                If g_ogrupo.UsarEscalados Then
                    If Not g_oAdjsProveEsc.Item(CStr(g_oItemSeleccionado.Id) & sCod) Is Nothing Then
                        If Not g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)) Is Nothing Then
                            If Not g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).Lineas.Item(CStr(g_oItemSeleccionado.Id)) Is Nothing Then
                                m_oAdjs.Item(sCod).Consumido = m_oAdjs.Item(sCod).Consumido + ((NullToDbl0(g_oAdjsProveEsc.Item(CStr(g_oItemSeleccionado.Id) & sCod).Porcentaje) * g_oItemSeleccionado.Cantidad / 100) * NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta))
                            End If
                        End If
                    End If
                Else
                    If Not g_ogrupo.Adjudicaciones.Item(CStr(g_oItemSeleccionado.Id) & sCod) Is Nothing Then
                        If Not g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)) Is Nothing Then
                            If Not g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).Lineas.Item(CStr(g_oItemSeleccionado.Id)) Is Nothing Then
                                m_oAdjs.Item(sCod).Consumido = m_oAdjs.Item(sCod).Consumido + ((NullToDbl0(g_ogrupo.Adjudicaciones.Item(CStr(g_oItemSeleccionado.Id) & sCod).Porcentaje) * g_oItemSeleccionado.Cantidad / 100) * NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(oProv.Cod)).Lineas.Item(CStr(g_oItemSeleccionado.Id)).PrecioOferta))
                            End If
                        End If
                    End If
                End If
            End If
        End If
    Next
            
    AplicarAtributosTotalItem True
    AplicarAtributosTotalGrupo
    CalcularTotalesDeProceso
    
    If g_ogrupo.UsarEscalados Then
        CargarGridEscAdjudicaciones sdbgAdjEsc.Groups(1).Columns.Count
    Else
        CargarGridAdjudicaciones
    End If
    
    If sItemEscEmpatados(1) <> "" Then
        oMensajes.ItemsEscaladosEmpatados sItemEscEmpatados
    End If
    
    m_bHayAtribsDeGrupoOProce = False
    
    If sMensaje <> "" Then
        oMensajes.ImposibleAdjudicar sMensaje
    End If

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CalcularOptimaItem", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Limpia las adjudicaciones</summary>
''' <returns>Adjudicable si o no</returns>
''' <remarks>Llamada desde: sdbgAdj_DblClick, CalcularOptima, CalcularOptimaItem; Tiempo m�ximo: 0,1</remarks>
''' <revision>LTG 15/11/2011</revision>

Private Sub LimpiarAdjudicaciones()
    Dim oProv As CProveedor
    Dim sCod As Variant
    Dim scod2 As Variant
    Dim oEsc As CEscalado
    
    'elimina las adjudicaciones existentes del item
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For Each oProv In m_oProvesAsig
        sCod = ComprobarAdjudicacion(oProv.Cod)
        If Not IsNull(sCod) And sCod <> "" Then
            'Restamos del total adjudicado de grupo
            scod2 = g_ogrupo.Codigo & oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
            If Not m_oAdjs.Item(scod2) Is Nothing Then
                If g_ogrupo.UsarEscalados Then
                    m_oAdjs.Item(scod2).AdjudicadoOfe = m_oAdjs.Item(scod2).AdjudicadoOfe - g_oAdjsProveEsc.Item(sCod).importe
                    m_oAdjs.Item(scod2).AdjudicadoSin = m_oAdjs.Item(scod2).AdjudicadoSin - g_oAdjsProveEsc.Item(sCod).ImporteAdjTot
                Else
                    m_oAdjs.Item(scod2).AdjudicadoOfe = m_oAdjs.Item(scod2).AdjudicadoOfe - g_ogrupo.Adjudicaciones.Item(sCod).importe
                    m_oAdjs.Item(scod2).AdjudicadoSin = m_oAdjs.Item(scod2).AdjudicadoSin - g_ogrupo.Adjudicaciones.Item(sCod).ImporteAdjTot
                End If
            End If
            
            If g_ogrupo.UsarEscalados Then
                g_oAdjsProveEsc.Remove sCod
                
                'Eliminar las adjudicaciones para cada escalado
                For Each oEsc In g_ogrupo.Escalados
                    sCod = KeyEscalado(oProv.Cod, g_oItemSeleccionado.Id, oEsc.Id)
                    If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        g_ogrupo.Adjudicaciones.Remove sCod
                    End If
                Next
            Else
                g_ogrupo.Adjudicaciones.Remove sCod
            End If
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "LimpiarAdjudicaciones", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

''' <summary>
''' Ver si los items son adjudicables y cargar la colecci�n de adjudicaciones parciales m_oAdjParcsGrupo
''' </summary>
''' <param name="bCierreGr">cierre parcial o si va a ser total de grupo</param>
''' <returns>Adjudicable si o no</returns>
''' <remarks>Llamada desde: cmdAdjudicar_Click; Tiempo m�ximo: 0,1</remarks>
''' <revision>LTG 02/11/2011</revision>

Private Function PreadjudicarItem(ByVal bCierreGr As Boolean) As Boolean
    Dim j As Integer
    Dim dPrecio As Double
    Dim iIndice As Integer
    Dim dPorcenAdj As Double
    Dim dCantAdj As Double
    Dim bAdjudicadas As Boolean
    Dim sCod As String
    Dim dAdjudicado As Double
    Dim dAdjudicadoTot As Double
    Dim dConsumido As Double
    Dim dImporte As Double
    Dim sId As String
    Dim vbm As Variant
    Dim vCat As Variant
    Dim vPed As Variant
    Dim oProves As CProveedores
    Dim sCodGMN4 As String
    Dim sProveBloqueo() As String
    Dim sProveAviso() As String
    Dim oProveUmbral As CProveedor
    Dim iAvisoBloqueo As Integer
    Dim bMensajeAviso As Boolean
    Dim bMensajeBloqueo As Boolean
    Dim iResp As Integer
    Dim bHayQA As Boolean
    Dim bAnadir As Boolean
    Dim i As Integer
    Dim iGroupIndex As Integer
    Dim oEscalado As CEscalado
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Function
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error GoTo err_PreadjudicarItem

    'Rellenamos la colecci�n de adjudicaciones
    Set m_oAdjParcs = oFSGSRaiz.Generar_CAdjudicaciones
    Set m_oAdjParcsGrupo = oFSGSRaiz.Generar_CAdjsGrupo
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    bAdjudicadas = False
    sId = CStr(g_oItemSeleccionado.Id)
    iIndice = 0
    
    If Not g_oItemSeleccionado.Cerrado Then
        If g_ogrupo.UsarEscalados Then
            'Se recorre cada linea de la grid
            For j = 1 To sdbgAdjEsc.Rows - 1
                dAdjudicado = 0
                dImporte = 0
                dConsumido = 0
                vbm = sdbgAdjEsc.AddItemBookmark(j)
                If sdbgAdjEsc.Columns("CANT").CellValue(vbm) <> "" Then  'Si hay oferta
                    If g_oItemSeleccionado.Cantidad = 0 Then
                        sCod = ComprobarAdjudicacion(sdbgAdjEsc.Columns("ID").CellValue(vbm))
                        If Not IsNull(sCod) And sCod <> "" Then
                            dPorcenAdj = g_oAdjsProveEsc.Item(sCod).Porcentaje
                        End If
                    Else
                        If StrToDbl0(sdbgAdjEsc.Columns("CANT").CellValue(vbm)) <> 0 Then
                            dCantAdj = CDbl(sdbgAdjEsc.Columns("CANT").CellValue(vbm))
                            dPorcenAdj = (dCantAdj / g_oItemSeleccionado.Cantidad) * 100
    
                            bAdjudicadas = True
                        Else
                            dPorcenAdj = 0
                            dCantAdj = 0
                        End If
                    End If
                
                    dPrecio = NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdjEsc.Columns("ID").CellValue(vbm))).Lineas.Item(sId).PrecioOferta)

                    vCat = 0
                    vPed = Null
                    dAdjudicadoTot = 0
                    sCod = CStr(g_oItemSeleccionado.Id) & sdbgAdjEsc.Columns("ID").CellValue(vbm) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdjEsc.Columns("ID").CellValue(vbm)))
                    If Not g_oAdjsProveEsc.Item(sCod) Is Nothing Then
                        vCat = g_oAdjsProveEsc.Item(sCod).Catalogo
                        vPed = g_oAdjsProveEsc.Item(sCod).pedido
                        dAdjudicadoTot = CDbl(g_oAdjsProveEsc.Item(sCod).ImporteAdjTot)
                    End If
                    If dPorcenAdj <> 0 Then
                        iGroupIndex = 2
                        For Each oEscalado In g_ogrupo.Escalados
                            If sdbgAdjEsc.Columns("ADJ" & iGroupIndex).Value <> "0" Then
                                m_oAdjParcs.Add sdbgAdjEsc.Columns("ID").CellValue(vbm), val(sdbgAdjEsc.Columns("NUMOFE").CellValue(vbm)), g_oItemSeleccionado.Id, dPrecio, dPorcenAdj, iIndice, , , , g_ogrupo.Codigo, vCat, vPed, , oEscalado.Id
                                m_oAdjParcs.Item(CStr(iIndice)).ImporteAdjTot = dAdjudicadoTot
                                iIndice = iIndice + 1
                            End If
                            iGroupIndex = iGroupIndex + 1
                        Next

                        'Carga la coleccion de proveedores y materiales, para comprobar los umbrales
                        If oProves.Item(sdbgAdjEsc.Columns("ID").CellValue(vbm)) Is Nothing Then
                            oProves.Add sdbgAdjEsc.Columns("ID").CellValue(vbm), " "
                            Set oProves.Item(sdbgAdjEsc.Columns("ID").CellValue(vbm)).IMaterialAsignado_GruposMN4 = Nothing
                            Set oProves.Item(sdbgAdjEsc.Columns("ID").CellValue(vbm)).IMaterialAsignado_GruposMN4 = oFSGSRaiz.Generar_CGruposMatNivel4

                            oProves.Item(sdbgAdjEsc.Columns("ID").CellValue(vbm)).Den = oProves.Item(sdbgAdjEsc.Columns("ID").CellValue(vbm)).DevolverDenominacionProve
                        End If

                        sCodGMN4 = g_oItemSeleccionado.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(g_oItemSeleccionado.GMN1Cod))
                        sCodGMN4 = sCodGMN4 & g_oItemSeleccionado.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(g_oItemSeleccionado.GMN2Cod))
                        sCodGMN4 = sCodGMN4 & g_oItemSeleccionado.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(g_oItemSeleccionado.GMN3Cod))
                        sCodGMN4 = sCodGMN4 & g_oItemSeleccionado.GMN4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(g_oItemSeleccionado.GMN4Cod))

                        If oProves.Item(sdbgAdjEsc.Columns("ID").CellValue(vbm)).IMaterialAsignado_GruposMN4.Item(sCodGMN4) Is Nothing Then
                            oProves.Item(sdbgAdjEsc.Columns("ID").CellValue(vbm)).IMaterialAsignado_GruposMN4.Add g_oItemSeleccionado.GMN1Cod, g_oItemSeleccionado.GMN2Cod, g_oItemSeleccionado.GMN3Cod, _
                                 " ", " ", " ", g_oItemSeleccionado.GMN4Cod, " "
                        End If
                    End If
                End If

                'Guarda la clase de adjudicaciones para el grupo a cerrar
                If Not bCierreGr Then
                    If Not g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdjEsc.Columns("ID").CellValue(vbm))) Is Nothing Then
                        If Not g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdjEsc.Columns("ID").CellValue(vbm))).Lineas.Item(sId) Is Nothing Then
                            sCod = sId & sdbgAdjEsc.Columns("ID").CellValue(vbm) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdjEsc.Columns("ID").CellValue(vbm)))

                            If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                                If g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje = 100 Then
                                    dAdjudicado = dAdjudicado + (VarToDbl0(g_ogrupo.Items.Item(sId).ImporteAdj) * g_oItemSeleccionado.CambioComparativa(sdbgAdjEsc.Columns("ID").CellValue(vbm), True, m_oAdjsProve))
                                Else
                                    dAdjudicado = dAdjudicado + ((g_ogrupo.Items.Item(sId).Cantidad * g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje / 100) * (NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdjEsc.Columns("ID").CellValue(vbm))).Lineas.Item(sId).PrecioOferta)))
                                End If

                                dConsumido = dConsumido + (g_ogrupo.Items.Item(sId).Precio * g_oItemSeleccionado.CambioComparativa(sdbgAdjEsc.Columns("ID").CellValue(vbm), True, m_oAdjsProve)) * (g_ogrupo.Items.Item(sId).Cantidad * g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje / 100)
                            End If

                        End If
                        m_oAdjParcsGrupo.Add m_oProcesoSeleccionado, g_ogrupo.Codigo, sdbgAdjEsc.Columns("ID").CellValue(vbm), sdbgAdjEsc.Columns("NUMOFE").CellValue(vbm), dAdjudicado, 0, 0, dConsumido, dImporte, , , , , , _
                            g_oItemSeleccionado.CambioComparativa(sdbgAdjEsc.Columns("ID").CellValue(vbm), True, m_oAdjsProve)
                    End If
                Else
                    'Se cierra el grupo entero
                    sCod = g_ogrupo.Codigo & sdbgAdjEsc.Columns("ID").CellValue(vbm) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdjEsc.Columns("ID").CellValue(vbm)))
                    If Not m_oAdjs.Item(sCod) Is Nothing Then
                        m_oAdjParcsGrupo.Add m_oProcesoSeleccionado, g_ogrupo.Codigo, sdbgAdjEsc.Columns("ID").CellValue(vbm), sdbgAdjEsc.Columns("NUMOFE").CellValue(vbm), m_oAdjs.Item(sCod).AdjudicadoReal, 0, 0, m_oAdjs.Item(sCod).ConsumidoReal, m_oAdjs.Item(sCod).importe, _
                            , , , , , g_oItemSeleccionado.CambioComparativa(sdbgAdjEsc.Columns("ID").CellValue(vbm), True, m_oAdjsProve)
                        g_ogrupo.Cerrado = 1
                    End If
                End If
            Next
        Else
            'Se recorre cada linea de la grid
            For j = 0 To sdbgAdj.Rows - 1
                dAdjudicado = 0
                dImporte = 0
                dConsumido = 0
                vbm = sdbgAdj.AddItemBookmark(j)
                If sdbgAdj.Columns("PRECIO").CellValue(vbm) <> "" Then  'Si hay oferta
                    If g_oItemSeleccionado.Cantidad = 0 Then
                        sCod = ComprobarAdjudicacion(sdbgAdj.Columns("PROVECOD").CellValue(vbm))
                        If Not IsNull(sCod) And sCod <> "" Then
                            dPorcenAdj = g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje
                        End If
                    Else
                        If StrToDbl0(sdbgAdj.Columns("CANTADJ").CellValue(vbm)) <> 0 Then
                            dCantAdj = CDbl(sdbgAdj.Columns("CANTADJ").CellValue(vbm))
                            dPorcenAdj = (dCantAdj / g_oItemSeleccionado.Cantidad) * 100
    
                            bAdjudicadas = True
                        Else
                            dPorcenAdj = 0
                            dCantAdj = 0
                        End If
                    End If
                    
                    dPrecio = NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").CellValue(vbm))).Lineas.Item(sId).PrecioOferta)
                    
                    vCat = 0
                    vPed = Null
                    dAdjudicadoTot = 0
                    sCod = CStr(g_oItemSeleccionado.Id) & sdbgAdj.Columns("PROVECOD").CellValue(vbm) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdj.Columns("PROVECOD").CellValue(vbm)))
                    If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        vCat = g_ogrupo.Adjudicaciones.Item(sCod).Catalogo
                        vPed = g_ogrupo.Adjudicaciones.Item(sCod).pedido
                        dAdjudicadoTot = CDbl(g_ogrupo.Adjudicaciones.Item(sCod).ImporteAdjTot)
                    End If
                    If dPorcenAdj <> 0 Then
                        m_oAdjParcs.Add sdbgAdj.Columns("PROVECOD").CellValue(vbm), val(sdbgAdj.Columns("NUMOFE").CellValue(vbm)), g_oItemSeleccionado.Id, dPrecio, dPorcenAdj, iIndice, , , , g_ogrupo.Codigo, vCat, vPed
                        m_oAdjParcs.Item(CStr(iIndice)).ImporteAdjTot = dAdjudicadoTot
                        iIndice = iIndice + 1
                        
                        'Carga la coleccion de proveedores y materiales, para comprobar los umbrales
                        If oProves.Item(sdbgAdj.Columns("PROVECOD").CellValue(vbm)) Is Nothing Then
                            oProves.Add sdbgAdj.Columns("PROVECOD").CellValue(vbm), " "
                            Set oProves.Item(sdbgAdj.Columns("PROVECOD").CellValue(vbm)).IMaterialAsignado_GruposMN4 = Nothing
                            Set oProves.Item(sdbgAdj.Columns("PROVECOD").CellValue(vbm)).IMaterialAsignado_GruposMN4 = oFSGSRaiz.Generar_CGruposMatNivel4
                            
                            oProves.Item(sdbgAdj.Columns("PROVECOD").CellValue(vbm)).Den = oProves.Item(sdbgAdj.Columns("PROVECOD").CellValue(vbm)).DevolverDenominacionProve
                        End If
    
                        sCodGMN4 = g_oItemSeleccionado.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(g_oItemSeleccionado.GMN1Cod))
                        sCodGMN4 = sCodGMN4 & g_oItemSeleccionado.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(g_oItemSeleccionado.GMN2Cod))
                        sCodGMN4 = sCodGMN4 & g_oItemSeleccionado.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(g_oItemSeleccionado.GMN3Cod))
                        sCodGMN4 = sCodGMN4 & g_oItemSeleccionado.GMN4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(g_oItemSeleccionado.GMN4Cod))
    
                        If oProves.Item(sdbgAdj.Columns("PROVECOD").CellValue(vbm)).IMaterialAsignado_GruposMN4.Item(sCodGMN4) Is Nothing Then
                            oProves.Item(sdbgAdj.Columns("PROVECOD").CellValue(vbm)).IMaterialAsignado_GruposMN4.Add g_oItemSeleccionado.GMN1Cod, g_oItemSeleccionado.GMN2Cod, g_oItemSeleccionado.GMN3Cod, _
                                 " ", " ", " ", g_oItemSeleccionado.GMN4Cod, " "
                        End If
                    End If
                End If
            
                'Guarda la clase de adjudicaciones para el grupo a cerrar
                If Not bCierreGr Then
                    If Not g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").CellValue(vbm))) Is Nothing Then
                        If Not g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").CellValue(vbm))).Lineas.Item(sId) Is Nothing Then
                            sCod = sId & sdbgAdj.Columns("PROVECOD").CellValue(vbm) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdj.Columns("PROVECOD").CellValue(vbm)))
    
                            If Not g_ogrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                                If g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje = 100 Then
                                    dAdjudicado = dAdjudicado + (VarToDbl0(g_ogrupo.Items.Item(sId).ImporteAdj) * g_oItemSeleccionado.CambioComparativa(Trim(sdbgAdj.Columns("PROVECOD").CellValue(vbm))))
                                Else
                                    dAdjudicado = dAdjudicado + ((g_ogrupo.Items.Item(sId).Cantidad * g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje / 100) * (NullToDbl0(g_ogrupo.UltimasOfertas.Item(Trim(sdbgAdj.Columns("PROVECOD").CellValue(vbm))).Lineas.Item(sId).PrecioOferta)))
                                End If
        
                                dConsumido = dConsumido + (g_ogrupo.Items.Item(sId).Precio * g_oItemSeleccionado.CambioComparativa(Trim(sdbgAdj.Columns("PROVECOD").CellValue(vbm)))) * (g_ogrupo.Items.Item(sId).Cantidad * g_ogrupo.Adjudicaciones.Item(sCod).Porcentaje / 100)
                            End If
                            
                        End If
                        m_oAdjParcsGrupo.Add m_oProcesoSeleccionado, g_ogrupo.Codigo, sdbgAdj.Columns("PROVECOD").CellValue(vbm), sdbgAdj.Columns("NUMOFE").CellValue(vbm), dAdjudicado, 0, 0, dConsumido, dImporte, _
                            , , , , , g_oItemSeleccionado.CambioComparativa(Trim(sdbgAdj.Columns("PROVECOD").CellValue(vbm)))
                    End If
                Else
                    'Se cierra el grupo entero
                    sCod = g_ogrupo.Codigo & sdbgAdj.Columns("PROVECOD").CellValue(vbm) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgAdj.Columns("PROVECOD").CellValue(vbm)))
                    If Not m_oAdjs.Item(sCod) Is Nothing Then
                        m_oAdjParcsGrupo.Add m_oProcesoSeleccionado, g_ogrupo.Codigo, sdbgAdj.Columns("PROVECOD").CellValue(vbm), sdbgAdj.Columns("NUMOFE").CellValue(vbm), m_oAdjs.Item(sCod).AdjudicadoReal, 0, 0, _
                            m_oAdjs.Item(sCod).ConsumidoReal, m_oAdjs.Item(sCod).importe, , , , , , g_oItemSeleccionado.CambioComparativa(Trim(sdbgAdj.Columns("PROVECOD").CellValue(vbm)))
                        g_ogrupo.Cerrado = 1
                    End If
                End If
            Next j
        End If
    End If
    If bCierreGr Then g_ogrupo.Cerrado = 1
    
    If Not ValidarNivelEscalacion(oProves) Then
        PreadjudicarItem = False
        Exit Function
    End If
    
    bHayQA = False
    If g_oOrigen.Name = "frmRESREU" Then
        If frmRESREU.cmdComparativaQA.Enabled = True Then bHayQA = True
    ElseIf g_oOrigen.Name = "frmADJ" Then
        If frmADJ.cmdComparativaQA.Enabled = True Then bHayQA = True
    End If
    
    If bHayQA Then
        bMensajeAviso = False
        bMensajeBloqueo = False
        
        ReDim sProveBloqueo(0)
        ReDim sProveAviso(0)
        
        
        For Each oProveUmbral In oProves  'para cada proveedor
            If Not (m_oProvesUmbrales.Item(oProveUmbral.Cod & "#2") Is Nothing) Then
                bMensajeBloqueo = True
                
                bAnadir = True
                For i = 1 To UBound(sProveBloqueo)
                    If sProveBloqueo(i) = oProveUmbral.Cod & " - " & oProveUmbral.Den Then
                        bAnadir = False
                        Exit For
                    End If
                Next
                
                If bAnadir Then
                    ReDim Preserve sProveBloqueo(UBound(sProveBloqueo) + 1)
                    sProveBloqueo(UBound(sProveBloqueo)) = oProveUmbral.Cod & " - " & oProveUmbral.Den
                End If
            End If
            
            If Not (m_oProvesUmbrales.Item(oProveUmbral.Cod & "#1") Is Nothing) Then
                bMensajeAviso = True
                
                bAnadir = True
                For i = 1 To UBound(sProveAviso)
                    If sProveAviso(i) = oProveUmbral.Cod & " - " & oProveUmbral.Den Then
                        bAnadir = False
                        Exit For
                    End If
                Next
                
                If bAnadir Then
                    ReDim Preserve sProveAviso(UBound(sProveAviso) + 1)
                    sProveAviso(UBound(sProveAviso)) = oProveUmbral.Cod & " - " & oProveUmbral.Den
                End If
            End If
        Next
        
        If bMensajeBloqueo Then
            If UBound(sProveBloqueo) > 0 Then
                oMensajes.BloqueoProveedoresUmbrales (sProveBloqueo)
                g_ogrupo.Cerrado = 0
                PreadjudicarItem = False
                Exit Function
            End If
        End If
        If bMensajeAviso Then
            If UBound(sProveAviso) > 0 Then
                iResp = oMensajes.AvisoProveedoresUmbrales(sProveAviso)
                If iResp = vbNo Then
                    g_ogrupo.Cerrado = 0
                    PreadjudicarItem = False
                    Exit Function
                End If
            End If
        End If
    End If
    
    PreadjudicarItem = True
    Exit Function

err_PreadjudicarItem:
    Select Case err.Number
        Case 9
            Select Case iAvisoBloqueo
                Case 1
                    ReDim Preserve sProveAviso(1)
                Case 2
                    ReDim Preserve sProveBloqueo(1)
            End Select
            Resume Next
    End Select

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "PreadjudicarItem", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>Validaciones seg�n nivel de escalaci�n de los proveedores a los que se quieren adjudicar</summary>
''' <param name="oProves">Grupo visible en pantalla</param>
''' <remarks>Llamada desde:GuardarAdjudicacion, PreadjudicarParcialGrupo, PreadjudicarParcialAll, PreadjudicarParcialProce ; Tiempo m�ximo: 0,3</remarks>
Private Function ValidarNivelEscalacion(ByVal oProves As CProveedores) As Boolean
Dim oProve As CProveedor
Dim s As String
Dim irespuesta As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
ValidarNivelEscalacion = False
'Miramos si alguno de los proveedores adjudicados est� entre los que tienen un nivel de escalaci�n no apto
For Each oProve In oProves
    If s <> "" Then
        s = s & ","
    End If
    s = s & StrToSQLNULL(oProve.Cod)
Next
Set oProve = Nothing
s = m_oProcesoSeleccionado.BloqProveNivelEscalacion(s)
If s <> "" Then
    irespuesta = oMensajes.BloqProveNivelEscalacion(Not m_bIgnBlqEscalacion, s, True)
    If irespuesta = vbNo Or Not m_bIgnBlqEscalacion Then
        Exit Function
    End If
End If
ValidarNivelEscalacion = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ValidarNivelEscalacion", err, Erl, , m_bActivado, m_sMsgError)
        Exit Function
    End If
End Function


''' <summary>
''' Cierra el item en curso y despues parcialmente o totalmente el proceso en curso
''' </summary>
''' <param name="bCierreTotal">Cierre parcial o total de proceso</param>
''' <remarks>Llamada desde:cmdAdjudicar_Click; Tiempo m�ximo:0,1</remarks>
Private Sub ValidarParcial(ByVal bCierreTotal As Boolean)
    Dim teserror As TipoErrorSummit

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oAdjParcs.Count = 0 Then
        oMensajes.FaltanDatos (86)
        Exit Sub
    End If
    
    If Not GuardarAdjudicacionesParciales(bCierreTotal) Then Exit Sub
            
    Screen.MousePointer = vbHourglass
    
    If bCierreTotal Then
        g_oOrigen.m_udtOrigBloqueo = 2 'Validando
        g_oOrigen.sdbgResultados.MoveFirst
        If g_oOrigen.Name = "frmRESREU" Then
            g_oOrigen.errorcierre = m_oProcesoSeleccionado.ValidacionYCierre(basOptimizacion.gvarCodUsuario, g_oOrigen.m_oAdjs, g_oOrigen.m_dblAdjudicadoProcReal, CDate(g_oOrigen.sdbcFecReu.Columns(0).Value))
        Else
            g_oOrigen.errorcierre = m_oProcesoSeleccionado.ValidacionYCierre(basOptimizacion.gvarCodUsuario, g_oOrigen.m_oAdjs, g_oOrigen.m_dblAdjudicadoProcReal)
        End If
        g_oOrigen.m_udtOrigBloqueo = 1 'Proceso
        teserror = g_oOrigen.errorcierre
        
        Screen.MousePointer = vbNormal
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            oMensajes.ProcesoValidado
            basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompAdjudicar, "Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & "GMN1:" & m_oProcesoSeleccionado.GMN1Cod & "Proce:" & m_oProcesoSeleccionado.Cod & "Fecha:" & CStr(Date)
            cmdAdjudicar.Enabled = False
            cmdGuardar.Enabled = False
            m_oProcesoSeleccionado.GuardarProceso = False
            m_oProcesoSeleccionado.ModificadoHojaAdj = False
            m_bRecargarOrigen = False
            g_oOrigen.cmdAdjudicar.Enabled = False
            g_oOrigen.cmdGuardar.Enabled = False
            g_oOrigen.cmdHojaAdj.Enabled = False
            If Not gParametrosGenerales.gbHojaAdjsAbierto Then
                g_oOrigen.cmdHojaAdj.Enabled = True
            End If
        End If
        
    Else
        'Cierre parcial del proceso
        g_oOrigen.m_udtOrigBloqueo = 2 'Validando
        If g_oOrigen.Name = "frmRESREU" Then
            errorcierre = m_oProcesoSeleccionado.ValidacionYCierreParcial(basOptimizacion.gvarCodUsuario, m_oAdjParcs, g_oOrigen.m_oAdjParcsGrupo, CDate(g_oOrigen.sdbcFecReu.Columns(0).Value))
        Else
            errorcierre = m_oProcesoSeleccionado.ValidacionYCierreParcial(basOptimizacion.gvarCodUsuario, m_oAdjParcs, g_oOrigen.m_oAdjParcsGrupo)
        End If
        g_oOrigen.m_udtOrigBloqueo = 1 'Proceso
        teserror = errorcierre

        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Screen.MousePointer = vbNormal
        Else
        
            Screen.MousePointer = vbNormal
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
            Else
                oMensajes.ProcesoParcialmenteCerrado
                basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompAdjudicar, "Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & "GMN1:" & m_oProcesoSeleccionado.GMN1Cod & "Proce:" & m_oProcesoSeleccionado.Cod & "Fecha:" & CStr(Date)
                cmdAdjudicar.Enabled = False
                cmdGuardar.Enabled = False
                g_oOrigen.g_bSesionCerradoParc = True
                m_oProcesoSeleccionado.GuardarProceso = False
                m_oProcesoSeleccionado.ModificadoHojaAdj = False
                'm_bRecargarOrigen = False
                g_oItemSeleccionado.Cerrado = True
                sdbgAdj.Refresh
                ActualizarGridMadre
                If Not gParametrosGenerales.gbHojaAdjsAbierto Then
                    g_oOrigen.cmdHojaAdj.Enabled = True
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ValidarParcial", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Sub ActualizarGridMadre()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_sCodGrupo = "" Then
    g_oOrigen.sdbgAll.SelBookmarks.Add g_oOrigen.sdbgAll.Bookmark
Else
    g_oOrigen.sdbgAdj.SelBookmarks.Add g_oOrigen.sdbgAdj.Bookmark
End If
g_oOrigen.ActualizarGrid
g_oOrigen.sdbgAll.SelBookmarks.RemoveAll
g_oOrigen.sdbgAdj.SelBookmarks.RemoveAll
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ActualizarGridMadre", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

''' <summary>
''' Cierra el item en curso
''' </summary>
''' <param name="bCierreTotal">Cierre parcial o total de proceso</param>
''' <returns>Si todas las modificaciones han sido correctas</returns>
''' <remarks>Llamada desde: ValidarParcial; Tiempo m�ximo:0,3</remarks>
Private Function GuardarAdjudicacionesParciales(ByVal bCierreTotal As Boolean) As Boolean
Dim j As Integer
Dim teserror As TipoErrorSummit
Dim vbm As Variant
Dim bAdjudicadas As Boolean
Dim oIAdj  As IAdjudicaciones

Dim bBloqueoEtapa As Boolean
Dim bBloqueoCondiciones As Boolean
Dim oAdj As CAdjudicacion
Dim SolicitudId As Variant
Dim ListaAvisos As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bAdjudicadas = False
    
    'Comprueba si la oferta es adjudicable
    'Las de reuni�n siempre van a ser adjudicables
    For j = 0 To sdbgAdj.Rows - 1
        vbm = sdbgAdj.AddItemBookmark(j)
        
        If StrToDbl0(sdbgAdj.Columns("CANTADJ").CellValue(vbm)) <> 0 Then
            bAdjudicadas = True
            If Not m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").CellValue(vbm)) Is Nothing Then
                If g_oOrigen.Name = "frmADJ" Then
                    If Not g_oOrigen.m_oEstOfes.Item(m_oProcesoSeleccionado.Ofertas.Item(sdbgAdj.Columns("PROVECOD").CellValue(vbm)).CodEst).adjudicable Then
                        oMensajes.OfertaNoAdjudicable
                        GuardarAdjudicacionesParciales = False
                        Exit Function
                    End If
                End If
            End If
        End If
    Next j
    
    If g_ogrupo.UsarEscalados Then
        'PreadjudicarItem indico algo hay q adjudicar.
        bAdjudicadas = True
    End If
    
    Set oIAdj = m_oProcesoSeleccionado
        
    If Not bAdjudicadas Then
        oMensajes.FaltanDatos (86)
        GuardarAdjudicacionesParciales = False
        Exit Function
    Else
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If gParametrosGenerales.gbSolicitudesCompras Then
            bBloqueoEtapa = False
            bBloqueoCondiciones = False
        
            For Each oAdj In m_oAdjParcs
                SolicitudId = g_oItemSeleccionado.SolicitudId
        
                'Se comprueban las condiciones de bloqueo por etapa
                If Not bBloqueoEtapa And Not NoHayParametro(SolicitudId) Then
                    bBloqueoEtapa = BloqueoEtapa(SolicitudId)
                    If bBloqueoEtapa Then
                        oMensajes.MensajeOKOnly 903, Exclamation  'La solicitud se encuentra en una etapa en la que no se permiten adjudicaciones
                        Exit For
                    Else
                        'Se comprueban las condiciones de bloqueo por f�rmula
                        bBloqueoCondiciones = BloqueoCondiciones(SolicitudId, ListaAvisos, m_oAdjParcs) 'Or bBloqueoCondiciones
                        If bBloqueoCondiciones Then
                            Exit For
                        End If
                    End If
                End If
        
            Next

            If bBloqueoEtapa Or bBloqueoCondiciones Then
                GuardarAdjudicacionesParciales = False
                Exit Function
            End If
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If g_oOrigen.Name = "frmADJ" Then
            If Not CtrlIgnorarRestriccAdjDir(bCierreTotal) Then
                GuardarAdjudicacionesParciales = False
                Exit Function
            End If
                
            teserror = oIAdj.RealizarAdjudicacionesParciales(m_oAdjParcs, , True, m_oAdjs, True, g_oOrigen.m_dblConsumidoProcReal, g_oOrigen.m_dblAdjudicadoProcReal, , oUsuarioSummit)
        Else
            teserror = oIAdj.RealizarAdjudicacionesParciales(m_oAdjParcs, CDate(g_oOrigen.sdbcFecReu.Text), True, m_oAdjs, True, g_oOrigen.m_dblConsumidoProcReal, g_oOrigen.m_dblAdjudicadoProcReal, , oUsuarioSummit)
        End If
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            GuardarAdjudicacionesParciales = False
        Else
            GuardarAdjudicacionesParciales = True
        End If
    
    End If
    
    Set oIAdj = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "GuardarAdjudicacionesParciales", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Mostrar Grafico
''' </summary>
''' <param name="iIndex">q grafico</param>
''' <remarks>Llamada desde: mdi/mnuPopUpMostrarGrafico_Click ; Tiempo m�ximo: 0,2</remarks>
Public Sub POPUPMostrarGrafico(ByVal iIndex As Integer)
Dim iNumProv  As Integer
Dim oProve As CProveedor
Dim oProves As CProveedores
Dim bVisible As Boolean
Dim bCargar As Boolean
Dim scodProve As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    iNumProv = 0
    For Each oProve In m_oProvesAsig
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(g_oItemSeleccionado.grupoCod) Is Nothing Then
                bCargar = False
            End If
        End If
        If bCargar Then
    
            Select Case g_oVistaSeleccionada.ConfVistasItemProv.Item(CStr(oProve.Cod)).Visible
                Case TipoProvVisible.Visible
                    iNumProv = iNumProv + 1
                    oProves.Add oProve.Cod, oProve.Den
                Case TipoProvVisible.DepenVista
                    'Si tiene marcados los checks y no tiene ofertas o no son adjudicables no se muestra en el gr�fico
                    bVisible = True
                    If g_oVistaSeleccionada.OcultarProvSinOfe = True Then
                        If g_ogrupo.UltimasOfertas.Item(Trim(CStr(oProve.Cod))) Is Nothing Then
                            bVisible = False
                        End If
                    End If
                    If bVisible = True And g_oVistaSeleccionada.OcultarNoAdj = True Then
                        If Not m_oProcesoSeleccionado.Ofertas.Item(CStr(oProve.Cod)) Is Nothing Then
                            If Not g_oOrigen.m_oEstOfes.Item(m_oProcesoSeleccionado.Ofertas.Item(CStr(oProve.Cod)).CodEst).adjudicable Then
                                bVisible = False
                            End If
                        Else
                            bVisible = False
                        End If
                    End If
                        
                    If bVisible = True Then
                        iNumProv = iNumProv + 1
                        oProves.Add oProve.Cod, oProve.Den
                    End If
            End Select
        End If
    Next
    
    If iNumProv = 0 Then Exit Sub
                    
                
    If Not m_ofrmADJGraficos Is Nothing Then
        Unload m_ofrmADJGraficos
        Set m_ofrmADJGraficos = Nothing
    End If
    Set m_ofrmADJGraficos = New frmADJGraficos
    Set m_ofrmADJGraficos.g_oOrigen = Me
    Set m_ofrmADJGraficos.g_oProvesAsig = oProves
    m_ofrmADJGraficos.m_iNumProv = iNumProv
    Set m_ofrmADJGraficos.g_oAtribsFormulas = m_oAtribsFormulas

    Select Case iIndex
        Case 5
                'Precios por item/proveedor
                m_ofrmADJGraficos.g_iGrafico = 6
                m_ofrmADJGraficos.Show
        Case 6
                'Puntos por item/proveedor
                m_ofrmADJGraficos.g_iGrafico = 7
                m_ofrmADJGraficos.Show
        Case 7
                'EvolucionDePrecios Item
                m_ofrmADJGraficos.g_iGrafico = 8
                m_ofrmADJGraficos.Show
        Case 8
                'Evoluci�n por escalados
                m_ofrmADJGraficos.g_iGrafico = 9
                m_ofrmADJGraficos.Show
    End Select
    
    Set oProves = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "POPUPMostrarGrafico", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Public Sub PopupDescargarGrafico()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set m_ofrmADJGraficos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "PopupDescargarGrafico", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub


Public Sub ModificarOrdenAtributos(ByVal AtributosOrden As CAtributos)
    Dim oAtribOrden As CAtributo
    Dim bAplicado As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If AtributosOrden Is Nothing Then Exit Sub
    
    bAplicado = False
    
    'Modifica el orden de los atributos que hemos cambiado en la pantalla de orde

    For Each oAtribOrden In AtributosOrden
        'Actualiza la clase de los atributos con precios de f�rmulas
        If Not m_oAtribsFormulas.Item(CStr(oAtribOrden.idAtribProce)) Is Nothing Then
            m_oAtribsFormulas.Item(CStr(oAtribOrden.idAtribProce)).Orden = oAtribOrden.Orden
            
            If m_oAtribsFormulas.Item(CStr(oAtribOrden.idAtribProce)).UsarPrec = 1 Then
                bAplicado = True
            End If
        End If
    Next
    
    'Carga otra vez la grid de aplicar precio ordenando los atributos seg�n el orden
    CargarAtribAplicarFormulas
    
    If bAplicado = True Then
        'Aplica los atributos y carga las grids
        CalcularPrecioConFormulas
        'Hace que la ventana de aplicar los atributos no se vea.
        picControlPrecios.Visible = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ModificarOrdenAtributos", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>Asigna el formato a las columnas con datos num�ricos de los grids</summary>
''' <remarks>Llamada desde: Adjudicar; Tiempo m�ximo:0,1</remarks>
''' <revision>LTG 27/10/2011</revision>

Private Sub FormateoDecimales()
    Dim i As Integer
    Dim oatrib As CAtributo
    Dim oEscalado As CEscalado
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_ogrupo.UsarEscalados Then
        With sdbgAdjEsc
            .Columns("CANT").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecCant)
            .Columns("IMP").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
            .Columns("AHORROIMP").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
            .Columns("AHORROPORCEN").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPorcen, True)
            .Columns("PRECAPE").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
            .Columns("CODPROVEACTUAL").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
            
            For i = 2 To .Groups.Count - 1
                .Columns("PRES" & i).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                .Columns("AHORRO" & i).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                .Columns("ADJCANT" & i).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                
                'Formateo num�rico de los atributos aplicables al precio
                If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
                    For Each oatrib In g_ogrupo.AtributosItem
                        If Not IsNull(oatrib.PrecioFormula) And Not IsEmpty(oatrib.PrecioFormula) Then
                            If Not IsNull(oatrib.PrecioAplicarA) Then
                                .Columns(oatrib.idAtribProce & CStr(i)).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                            ElseIf oatrib.Tipo = TipoNumerico Then
                                If oatrib.NumDecAtrib > g_oVistaSeleccionada.DecPrecios Then
                                    .Columns(oatrib.idAtribProce & CStr(i)).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                                Else
                                    .Columns(oatrib.idAtribProce & CStr(i)).NumberFormat = FormateoNumericoComp(oatrib.NumDecAtrib)
                                End If
                            End If
                            
                            Set oEscalado = Nothing
                        End If
                    Next
                End If
            Next
        End With
    Else
        With sdbgAdj
            'Aplica el formato de los decimales a las columnas de la gri:
            .Columns("PRECIO").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
            .Columns("CANTADJ").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecCant)
            .Columns("CANTMAX").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecCant)
            .Columns("AHO_PORCEN").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPorcen, True)
            .Columns("AHO").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
            .Columns("CONSUM").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
            .Columns("ADJ").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
            .Columns("IMP_OFE").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
            .Columns("AHORRO_OFE").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
            .Columns("AHORRO_OFE_PORCEN").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPorcen, True)
            
            'Formateo num�rico para los atributos,dependiendo de si son aplicables o no al precio:
            If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
                For Each oatrib In m_oProcesoSeleccionado.ATRIBUTOS
                    If Not IsNull(oatrib.PrecioAplicarA) Then
                        .Columns("ATRIB" & oatrib.idAtribProce).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                    ElseIf oatrib.Tipo = TipoNumerico Then
                        If oatrib.NumDecAtrib > g_oVistaSeleccionada.DecPrecios Then
                            .Columns("ATRIB" & oatrib.idAtribProce).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                        Else
                            .Columns("ATRIB" & oatrib.idAtribProce).NumberFormat = FormateoNumericoComp(oatrib.NumDecAtrib)
                        End If
                    End If
                Next
            End If
            If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
                For Each oatrib In m_oProcesoSeleccionado.AtributosGrupo
                    If oatrib.codgrupo = g_ogrupo.Codigo Or IsNull(oatrib.codgrupo) Then
                        If Not IsNull(oatrib.PrecioAplicarA) Then
                            .Columns("ATRIB" & oatrib.idAtribProce).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                        ElseIf oatrib.Tipo = TipoNumerico Then
                            If oatrib.NumDecAtrib > g_oVistaSeleccionada.DecPrecios Then
                                .Columns("ATRIB" & oatrib.idAtribProce).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                            Else
                                .Columns("ATRIB" & oatrib.idAtribProce).NumberFormat = FormateoNumericoComp(oatrib.NumDecAtrib)
                            End If
                        End If
                    End If
                Next
            End If
            If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
                For Each oatrib In m_oProcesoSeleccionado.AtributosItem
                    If oatrib.codgrupo = g_ogrupo.Codigo Or IsNull(oatrib.codgrupo) Then
                        If Not IsNull(oatrib.PrecioAplicarA) Then
                            .Columns("ATRIB" & oatrib.idAtribProce).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                        ElseIf oatrib.Tipo = TipoNumerico Then
                            If oatrib.NumDecAtrib > g_oVistaSeleccionada.DecPrecios Then
                                .Columns("ATRIB" & oatrib.idAtribProce).NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPrecios)
                            Else
                                .Columns("ATRIB" & oatrib.idAtribProce).NumberFormat = FormateoNumericoComp(oatrib.NumDecAtrib)
                            End If
                        End If
                    End If
                Next
            End If
        End With
    End If
    
    'Aplica el formato de los decimales a las columnas de la grid de resultados:
    sdbgResultados.Columns("Abierto").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
    sdbgResultados.Columns("Consumido").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
    sdbgResultados.Columns("Adjudicado").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
    sdbgResultados.Columns("Ahorrado").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecResult)
    sdbgResultados.Columns("%").NumberFormat = FormateoNumericoComp(g_oVistaSeleccionada.DecPorcen, True)
                
    If g_oVistaSeleccionada.DecResult > 0 Then
        m_sFormatoNumber = "#,##0."
        For i = 1 To g_oVistaSeleccionada.DecResult
            m_sFormatoNumber = m_sFormatoNumber & "0"
        Next i
    Else
        m_sFormatoNumber = "#,##0"
    End If
    
    If g_oVistaSeleccionada.DecPrecios > 0 Then
        m_sFormatoNumberPrecio = "#,##0."
        For i = 1 To g_oVistaSeleccionada.DecPrecios
            m_sFormatoNumberPrecio = m_sFormatoNumberPrecio & "0"
        Next i
    Else
        m_sFormatoNumberPrecio = "#,##0"
    End If
    
    If g_oVistaSeleccionada.DecCant > 0 Then
        m_sFormatoNumberCant = "#,##0."
        For i = 1 To g_oVistaSeleccionada.DecCant
            m_sFormatoNumberCant = m_sFormatoNumberCant & "0"
        Next i
    Else
        m_sFormatoNumberCant = "#,##0"
    End If
    
    'Datos del �tem
    lblPresUni.caption = Format(g_oItemSeleccionado.Precio, m_sFormatoNumberPrecio)
    lblCantidad.caption = Format(g_oItemSeleccionado.Cantidad, m_sFormatoNumberCant)
    lblPresTot.caption = Format(g_oItemSeleccionado.Presupuesto, m_sFormatoNumberPrecio)
    lblObjetivo.caption = Format(g_oItemSeleccionado.Objetivo, m_sFormatoNumber)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "FormateoDecimales", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub txtDecResult_LostFocus()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsNumeric(txtDecResult.Text) Then
        txtDecResult.Text = g_oVistaSeleccionada.DecResult
        Exit Sub
    End If
    
    'Si el n�mero de decimales es el mismo que ten�a antes no hace nada
    If Trim(txtDecResult.Text) = g_oVistaSeleccionada.DecResult Then
        Exit Sub
    End If
    
    g_oVistaSeleccionada.DecResult = txtDecResult.Text
    
    FormateoDecimales
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "txtDecResult_LostFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub txtDecPrec_LostFocus()
    'Si no es un valor num�rico pone el mismo valor que hab�a antes
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsNumeric(txtDecPrec.Text) Then
        txtDecPrec.Text = g_oVistaSeleccionada.DecPrecios
        Exit Sub
    End If
    
    'Si el n�mero de decimales es el mismo que ten�a antes no hace nada
    If Trim(txtDecPrec.Text) = g_oVistaSeleccionada.DecPrecios Then
        Exit Sub
    End If
    
    g_oVistaSeleccionada.DecPrecios = txtDecPrec.Text
    
    FormateoDecimales
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "txtDecPrec_LostFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub txtDecPorcen_LostFocus()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsNumeric(txtDecPorcen.Text) Then
        txtDecPorcen.Text = g_oVistaSeleccionada.DecPorcen
        Exit Sub
    End If
    
    'Si el n�mero de decimales es el mismo que ten�a antes no hace nada
    If Trim(txtDecPorcen.Text) = g_oVistaSeleccionada.DecPorcen Then
        Exit Sub
    End If
    
    g_oVistaSeleccionada.DecPorcen = txtDecPorcen.Text
    
    FormateoDecimales
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "txtDecPorcen_LostFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub txtDecCant_LostFocus()
    'Si no es un valor num�rico pone el mismo valor que ten�a antes:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsNumeric(txtDecCant.Text) Then
        txtDecCant.Text = g_oVistaSeleccionada.DecCant
        Exit Sub
    End If
    
    'Si el n�mero de decimales es el mismo que ten�a antes no hace nada
    If Trim(txtDecCant.Text) = g_oVistaSeleccionada.DecCant Then
        Exit Sub
    End If
    
    g_oVistaSeleccionada.DecCant = txtDecCant.Text
    
    FormateoDecimales
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "txtDecCant_LostFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Function ObtenerMaxDecAtributos(ByVal idAtrib As Long, Optional ByVal sAmbito As String, Optional ByVal oGrupo As CGrupo) As Long
    Dim lMax As Long
    Dim oOferta As COferta
    Dim sCod As String
    Dim oItem As CItem
    Dim Grupo As CGrupo
    Dim lLong As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lMax = 0
    
    Select Case sAmbito
        Case "ALL"
            For Each Grupo In m_oProcesoSeleccionado.Grupos
                For Each oOferta In Grupo.UltimasOfertas
                    If Not oOferta.AtribItemOfertados Is Nothing Then
                        For Each oItem In Grupo.Items
                            sCod = CStr(oItem.Id) & "$" & CStr(idAtrib)
                            If Not oOferta.AtribItemOfertados.Item(sCod) Is Nothing Then
                                If Not IsNull(oOferta.AtribItemOfertados.Item(sCod).valorNum) Then
                                    lLong = LongParteDecimal(garSimbolos, oOferta.AtribItemOfertados.Item(sCod).valorNum)
                                    If lLong <> 0 Then
                                        If lLong > lMax Then
                                            lMax = lLong
                                        End If
                                    End If
                                End If
                            End If
                        Next oItem
                    End If
                Next
            Next
            
        Case "GeneralProc"
            'Atributos de proceso
            For Each oOferta In m_oProcesoSeleccionado.Ofertas
                If Not oOferta.AtribProcOfertados Is Nothing Then
                    If Not oOferta.AtribProcOfertados.Item(CStr(idAtrib)) Is Nothing Then
                        If Not IsNull(oOferta.AtribProcOfertados.Item(CStr(idAtrib)).valorNum) Then
                            lLong = LongParteDecimal(garSimbolos, oOferta.AtribProcOfertados.Item(CStr(idAtrib)).valorNum)
                            If lLong <> 0 Then
                                If lLong > lMax Then
                                    lMax = lLong
                                End If
                            End If
                        End If
                    End If
                End If
            Next
            
        Case "GeneralGr"
            'Atributos de grupo
            For Each oOferta In m_oProcesoSeleccionado.Ofertas
                If Not oOferta.AtribGrOfertados Is Nothing Then
                    For Each Grupo In m_oProcesoSeleccionado.Grupos
                        sCod = CStr(Grupo.Codigo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(CStr(Grupo.Codigo)))
                        sCod = sCod & CStr(idAtrib)
                        
                        If Not oOferta.AtribGrOfertados.Item(sCod) Is Nothing Then
                            If Not IsNull(oOferta.AtribGrOfertados.Item(sCod).valorNum) Then
                                lLong = LongParteDecimal(garSimbolos, oOferta.AtribGrOfertados.Item(sCod).valorNum)
                                If lLong <> 0 Then
                                    If lLong > lMax Then
                                        lMax = lLong
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            Next
            
        Case "Grupo"
            'Estamos en un grupo.
            'Obtiene los atributos de item del grupo
            For Each oOferta In oGrupo.UltimasOfertas
                If Not oOferta.AtribItemOfertados Is Nothing Then
                    For Each oItem In oGrupo.Items
                        sCod = CStr(oItem.Id) & "$" & CStr(idAtrib)
                        If Not oOferta.AtribItemOfertados.Item(sCod) Is Nothing Then
                            If Not IsNull(oOferta.AtribItemOfertados.Item(sCod).valorNum) Then
                                lLong = LongParteDecimal(garSimbolos, oOferta.AtribItemOfertados.Item(sCod).valorNum)
                                If lLong <> 0 Then
                                    If lLong > lMax Then
                                        lMax = lLong
                                    End If
                                End If
                            End If
                        End If
                    Next oItem
                End If
            Next
    End Select
    
    ObtenerMaxDecAtributos = lMax
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ObtenerMaxDecAtributos", err, Erl, , m_bActivado)
        Exit Function
    End If
    
End Function

''' <summary>
''' Comprobar Atributos Prec Aplicados
''' </summary>
''' <param name="oItem">Item</param>
''' <param name="sProve">Prove</param>
''' <returns>Ok/Ko</returns>
''' <remarks>Llamada desde: sdbgAdj_AfterColUpdate ; Tiempo m�ximo: 0,2</remarks>
Private Function ComprobarAtributosPrecAplicados(ByVal oItem As CItem, ByVal sProve As String) As Variant
    Dim vAtrib() As Variant
    Dim oAsig As COferta
    Dim oatrib As CAtributo
    Dim iAplicado As Integer
    Dim dValorAtrib As Double
    Dim scod1 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iAplicado = 0
    
    'Ahora empieza el c�lculo del precio
    
    If oItem.Confirmado = True Then
        If Not g_ogrupo.UltimasOfertas.Item(Trim(sProve)) Is Nothing Then
            Set oAsig = g_ogrupo.UltimasOfertas.Item(Trim(sProve))
            If Not oAsig.Lineas.Item(CStr(oItem.Id)) Is Nothing Then
                      
                For Each oatrib In m_oAtribsFormulas
                    'si el atributo aplica la f�rmula al total del item
                    If oatrib.PrecioAplicarA = TipoAplicarAPrecio.UnitarioItem And oatrib.UsarPrec = 1 And oatrib.FlagAplicar = True Then
                            
                        Select Case oatrib.ambito
                            Case 1  'Proceso
                                'Comprueba los atributos a aplicar al precio unitario de �mbito proceso
                                If Not m_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados Is Nothing Then
                                    If Not m_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                        dValorAtrib = NullToDbl0(m_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                                        If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                            ReDim Preserve vAtrib(2, iAplicado)
                                            vAtrib(0, iAplicado) = oatrib.Cod
                                            vAtrib(1, iAplicado) = oatrib.Den
                                            vAtrib(2, iAplicado) = oatrib.idAtribProce
                                            iAplicado = iAplicado + 1
                                        End If
                                    End If
                                End If
                                    
                            Case 2 'Grupo
                                'Comprueba los atributos a aplicar al precio unitario de �mbito grupo
                                If Not m_oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados Is Nothing Then
                                    If g_ogrupo.Codigo = oatrib.codgrupo Or IsNull(oatrib.codgrupo) Then
                                        scod1 = g_ogrupo.Codigo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(g_ogrupo.Codigo))
                                        scod1 = scod1 & CStr(oatrib.idAtribProce)
                                        If Not m_oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados.Item(scod1) Is Nothing Then
                                            dValorAtrib = NullToDbl0(m_oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados.Item(scod1).valorNum)
                                            If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                                ReDim Preserve vAtrib(2, iAplicado)
                                                vAtrib(0, iAplicado) = oatrib.Cod
                                                vAtrib(1, iAplicado) = oatrib.Den
                                                vAtrib(2, iAplicado) = oatrib.idAtribProce
                                                iAplicado = iAplicado + 1
                                            End If
                                        End If
                                    End If
                                End If
        
                            Case 3  'Item
                                If Not oAsig.AtribItemOfertados Is Nothing Then
                                    If Not oAsig.AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                                        dValorAtrib = NullToDbl0(oAsig.AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(oatrib.idAtribProce)).valorNum)
                                        If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                            ReDim Preserve vAtrib(2, iAplicado)
                                            vAtrib(0, iAplicado) = oatrib.Cod
                                            vAtrib(1, iAplicado) = oatrib.Den
                                            vAtrib(2, iAplicado) = oatrib.idAtribProce
                                            iAplicado = iAplicado + 1
                                        End If
                                    End If
                                End If
                        End Select
                    End If
                Next
            End If 'Not oAsig.Lineas.Item(CStr(oItem.Id)) Is Nothing
        End If   'Not g_ogrupo.UltimasOfertas.Item(trim(sProve)) Is Nothing
    End If    'oItem.Confirmado = True
    
    If iAplicado = 0 Then
        ComprobarAtributosPrecAplicados = Null
    Else
        ComprobarAtributosPrecAplicados = vAtrib
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ComprobarAtributosPrecAplicados", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Private Sub sdbddValor_DropDown()
Dim oLista As CValoresPond
Dim oElem As CValorPond
Dim oatrib As CAtributo
Dim lIdAtribProce As Long

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgAdj.Columns(sdbgAdj.col).Locked Then
        sdbddValor.Enabled = False
        Exit Sub
    End If

    sdbddValor.RemoveAll
    
    lIdAtribProce = Right(sdbgAdj.Columns(sdbgAdj.col).Name, Len(sdbgAdj.Columns(sdbgAdj.col).Name) - 5)
    Select Case Right(sdbgAdj.Columns(sdbgAdj.col).TagVariant, 1)
        Case "P"
            Set oatrib = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(lIdAtribProce))
        Case "G"
            Set oatrib = m_oProcesoSeleccionado.AtributosGrupo.Item(CStr(lIdAtribProce))
        Case "I"
            Set oatrib = m_oProcesoSeleccionado.AtributosItem.Item(CStr(lIdAtribProce))
    End Select
                                    
          
    If oatrib.TipoIntroduccion = Introselec Then
        Set oLista = oatrib.ListaPonderacion
        For Each oElem In oLista
            sdbddValor.AddItem oElem.ValorLista
        Next
        Set oLista = Nothing
    Else
        If oatrib.Tipo = TipoBoolean Then
            sdbddValor.AddItem m_sIdiTrue
            sdbddValor.AddItem m_sIdiFalse
        End If
    End If
    Set oatrib = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbddValor_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddValor_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbddValor_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAdj.Columns.Item(sdbgAdj.col).Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "sdbddValor_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Function DevolverValorBoolean(vValor As Variant) As String
Dim sResultado As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case vValor
    Case True, 1
        sResultado = m_sIdiTrue
    Case False, 0
        sResultado = m_sIdiFalse
    Case Else
        sResultado = ""
    End Select

    DevolverValorBoolean = sResultado
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "DevolverValorBoolean", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

''' <summary>
''' Si esta definido el proceso como q debe solo crear un pedido directo de esta adjudicaci�n aunque para alg�n
''' proveedor la adjudicaci�n sea cero si es pq la cantidad del item es cero, no debe pasar.
''' </summary>
''' <returns>Si la adjudicaci�n generar�a mas de un pedido directo</returns>
''' <remarks>Llamada desde: Adjudicar; Tiempo m�ximo:0,1</remarks>
Private Function ComprobarAdjudicadoUnSoloProveedor() As Boolean
Dim oProve As CProveedor
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim sProve As String
Dim sCod As String
Dim scod2 As String
Dim bVa As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ComprobarAdjudicadoUnSoloProveedor = True
    For Each oProve In m_oProvesAsig
        For Each oGrupo In m_oProcesoSeleccionado.Grupos
        
            sCod = oGrupo.Codigo & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            
            If Not m_oAdjs.Item(sCod) Is Nothing Then
                '''''''
                bVa = False
                For Each oItem In oGrupo.Items
                    If Not oGrupo.Adjudicaciones Is Nothing Then
                        scod2 = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                        scod2 = CStr(oItem.Id) & scod2
                        If Not oGrupo.Adjudicaciones.Item(scod2) Is Nothing Then
                            If oGrupo.Adjudicaciones.Item(scod2).Porcentaje > 0 Then
                                bVa = True
                                Exit For
                            End If
                        End If
                    End If
                Next
                '''''''
                If bVa Then
                    If sProve = "" Then
                        sProve = oProve.Cod
                    Else
                        
                        If sProve <> oProve.Cod Then
                            ComprobarAdjudicadoUnSoloProveedor = False
                            Exit Function
                        End If
                    End If
                End If
            End If
        Next
    Next

    If sProve = "" Then
        ComprobarAdjudicadoUnSoloProveedor = False
    End If
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ComprobarAdjudicadoUnSoloProveedor", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Public Function EliminarAdjudicacionProve(ByVal sGrupo As String, ByVal lItem As Long, ByVal sProve As String) As Boolean
'Devuelve:  el porcentaje eliminado
Dim oGrupo As CGrupo
Dim sCod As String
Dim dblPorcen As Double

'Elimina las adjudicaciones que no pertenecen a pedidos ni a cat�logo
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    EliminarAdjudicacionProve = False
    dblPorcen = 0
    Set oGrupo = m_oProcesoSeleccionado.Grupos.Item(sGrupo)
        
    sCod = sProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProve))
    sCod = CStr(lItem) & sCod
    If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
        If Not IsNull(oGrupo.Adjudicaciones.Item(sCod).pedido) Then
            ReDim Preserve m_vEnPedidos(3, m_iInd)
            m_vEnPedidos(0, m_iInd) = Trim(NullToStr(oGrupo.Items.Item(CStr(lItem)).ArticuloCod) & "  " & NullToStr(oGrupo.Items.Item(CStr(lItem)).Descr))
            m_vEnPedidos(1, m_iInd) = oGrupo.Adjudicaciones.Item(sCod).pedido
            m_vEnPedidos(2, m_iInd) = Trim(sProve & "  " & m_oProvesAsig.Item(sProve).Den)
            m_vEnPedidos(3, m_iInd) = 174 'El �tem esta incluido en los pedidos
            m_iInd = m_iInd + 1
        ElseIf oGrupo.Adjudicaciones.Item(sCod).Catalogo = 1 Then
            ReDim Preserve m_vEnPedidos(3, m_iInd)
            m_vEnPedidos(0, m_iInd) = Trim(NullToStr(oGrupo.Items.Item(CStr(lItem)).ArticuloCod) & "  " & NullToStr(oGrupo.Items.Item(CStr(lItem)).Descr))
            m_vEnPedidos(1, m_iInd) = oGrupo.Adjudicaciones.Item(sCod).pedido
            m_vEnPedidos(2, m_iInd) = Trim(sProve & "  " & m_oProvesAsig.Item(sProve).Den)
            m_vEnPedidos(3, m_iInd) = 175 'El �tem esta incluido en los pedidos
            m_iInd = m_iInd + 1
        Else
            EliminarAdjudicacionProve = True
        End If
    Else
        EliminarAdjudicacionProve = True
    End If
    Set oGrupo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "EliminarAdjudicacionProve", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>Lleva a cabo la selecci�n de una vista</summary>
''' <param name="bDefecto"></param>
''' <remarks>Llamada desde: ItemSeleccionado, ItemEscaladoSeleccionado; Tiempo m�ximo: 0,1</remarks>

Private Sub MostrarVistaEnCombos(ByVal bDefecto As Boolean)
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bDefecto Then
        Select Case m_oProcesoSeleccionado.VistaDefectoTipo
            Case TipoDeVistaDefecto.vistainicial
                sdbcVistaDefecto.Text = m_sVistaIni
            Case TipoDeVistaDefecto.VistaDePlantilla
                sdbcVistaDefecto.Text = m_sVistaPlan & ": " & m_oProcesoSeleccionado.NombreVistaDefecto
            Case TipoDeVistaDefecto.VistaDeResponsable
                sdbcVistaDefecto.Text = m_sVistaResp & ": " & m_oProcesoSeleccionado.NombreVistaDefecto
            Case TipoDeVistaDefecto.VistaDeUsuario
                sdbcVistaDefecto.Text = m_sVistaUsu & ": " & m_oProcesoSeleccionado.NombreVistaDefecto
            Case TipoDeVistaDefecto.VistaDeOtroUsuario
                sdbcVistaDefecto.Text = m_sVistaUsu & " (" & m_oProcesoSeleccionado.UsuarioNombre & "): " & m_oProcesoSeleccionado.NombreVistaDefecto
                
        End Select
    
        sdbcVistaDefecto.Columns("COD").Value = m_oProcesoSeleccionado.VistaDefectoComp
        sdbcVistaDefecto.Columns("TIPO").Value = m_oProcesoSeleccionado.VistaDefectoTipo
        sdbcVistaDefecto.Columns("USU").Value = m_oProcesoSeleccionado.UsuarioVistaDefecto
    End If
    
    Select Case g_oVistaSeleccionada.TipoVista
        Case TipoDeVistaDefecto.vistainicial
            sdbcVistaActual.Text = m_sVistaIni
            sdbgAdj.caption = m_sVistaIni
            sdbgAdjEsc.caption = m_sVistaIni
        Case TipoDeVistaDefecto.VistaDePlantilla
            sdbcVistaActual.Text = m_sVistaPlan & ": " & g_oOrigen.m_oVistaSeleccionada.nombre
            sdbgAdj.caption = m_sVistaPlan & ": " & g_oOrigen.m_oVistaSeleccionada.nombre
            sdbgAdjEsc.caption = m_sVistaPlan & ": " & g_oOrigen.m_oVistaSeleccionada.nombre
        Case TipoDeVistaDefecto.VistaDeResponsable
            sdbcVistaActual.Text = m_sVistaResp & ": " & g_oOrigen.m_oVistaSeleccionada.nombre
            sdbgAdj.caption = m_sVistaResp & ": " & g_oOrigen.m_oVistaSeleccionada.nombre
            sdbgAdjEsc.caption = m_sVistaResp & ": " & g_oOrigen.m_oVistaSeleccionada.nombre
        Case TipoDeVistaDefecto.VistaDeUsuario
            sdbcVistaActual.Text = m_sVistaUsu & ": " & g_oOrigen.m_oVistaSeleccionada.nombre
            sdbgAdj.caption = m_sVistaUsu & ": " & g_oOrigen.m_oVistaSeleccionada.nombre
            sdbgAdjEsc.caption = m_sVistaUsu & ": " & g_oOrigen.m_oVistaSeleccionada.nombre
        Case TipoDeVistaDefecto.VistaDeOtroUsuario
            sdbcVistaActual.Text = m_sVistaUsu & " (" & g_oOrigen.m_oVistaSeleccionada.UsuarioNombre & "): " & g_oOrigen.m_oVistaSeleccionada.nombre
            sdbgAdj.caption = m_sVistaUsu & " (" & g_oOrigen.m_oVistaSeleccionada.UsuarioNombre & "): " & g_oOrigen.m_oVistaSeleccionada.nombre
            sdbgAdjEsc.caption = m_sVistaUsu & " (" & g_oOrigen.m_oVistaSeleccionada.UsuarioNombre & "): " & g_oOrigen.m_oVistaSeleccionada.nombre
    End Select
    sdbcVistaActual.Columns("COD").Value = g_oVistaSeleccionada.Vista
    sdbcVistaActual.Columns("TIPO").Value = g_oVistaSeleccionada.TipoVista
    sdbcVistaActual.Columns("USU").Value = g_oOrigen.m_oVistaSeleccionada.UsuarioVista
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "MostrarVistaEnCombos", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

Private Function BloqueoEtapa(IdInstancia As Variant) As Boolean
    Dim oInstancia As CInstancia
    Dim Ador As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oInstancia = oFSGSRaiz.Generar_CInstancia
    oInstancia.Id = IdInstancia
    Set Ador = oInstancia.ComprobarBloqueoEtapa()
    
    If Not Ador.EOF Then
        BloqueoEtapa = IIf(Ador.Fields("BLOQUEO_ADJUDICACION").Value = 1, False, True)
    Else
        BloqueoEtapa = False
    End If
    
    Ador.Close
    Set Ador = Nothing
    Set oInstancia = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "BloqueoEtapa", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Se comprueban las condiciones de bloqueo por f�rmula
''' </summary>
''' <param name="IdInstancia">Instancia</param>
''' <param name="ListaAvisos">ByRef. Lista de condiciones de aviso q no cumple</param>
''' <param name="oAdjs">Adjudicaciones a realizar</param>
''' <returns>Si no cumple alguna condici�n de bloqueo. ListaAvisos</returns>
''' <remarks>Llamada desde: GuardarAdjudicacionesParciales; Tiempo m�ximo: 0,1</remarks>
Private Function BloqueoCondiciones(IdInstancia As Variant, ByRef ListaAvisos As String, Optional ByVal oAdjs As CAdjudicaciones) As Boolean
    Dim oInstancia As CInstancia
    Dim iBloqueo As Integer
    Dim bExisteBloqueo As Boolean
    Dim Ador As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oInstancia = oFSGSRaiz.Generar_CInstancia
    oInstancia.Id = IdInstancia
            
    bExisteBloqueo = False
    
    Set Ador = oInstancia.ComprobarBloqueoCond(TipoBloqueo.Adjudicacion)

    If Ador.EOF Then
        BloqueoCondiciones = False
        Ador.Close
        Set Ador = Nothing
        Set oInstancia = Nothing
        Exit Function
    Else
        While Not Ador.EOF And Not bExisteBloqueo
            iBloqueo = oInstancia.ComprobarBloqueoCondiciones(Ador.Fields("ID").Value, Ador.Fields("FORMULA").Value, , oAdjs, m_oProcesoSeleccionado.Anyo, m_oProcesoSeleccionado.Cod, m_oProcesoSeleccionado.GMN1Cod)
            Select Case iBloqueo
                Case 0  'No existe bloqueo
                
                Case 1  'Existe bloqueo
                    If InStr(ListaAvisos, "@" & Ador.Fields("ID").Value & "@") = 0 Then
                        oMensajes.Bloqueo (NullToStr(Ador.Fields("MENSAJE_" & gParametrosInstalacion.gIdioma).Value))
                        ListaAvisos = ListaAvisos & IIf(ListaAvisos = "", "@", "") & Ador.Fields("ID").Value & "@"
                    End If
                    
                    If Ador.Fields("TIPO").Value = TipoAvisoBloqueo.Bloquea Then
                        bExisteBloqueo = True
                    End If
                Case 3  'Error al realizar el c�lculo:F�rmula inv�lida.
                    If InStr(ListaAvisos, "@" & Ador.Fields("ID").Value & "@") = 0 Then
                        oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(1))
                        ListaAvisos = ListaAvisos & IIf(ListaAvisos = "", "@", "") & Ador.Fields("ID").Value & "@"
                    End If
                Case 4  'Error al realizar el c�lculo:Valores incorrectos.
                    If InStr(ListaAvisos, "@" & Ador.Fields("ID").Value & "@") = 0 Then
                        oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(2))
                        ListaAvisos = ListaAvisos & IIf(ListaAvisos = "", "@", "") & Ador.Fields("ID").Value & "@"
                    End If
            End Select
            Ador.MoveNext
        Wend
    End If
    
    BloqueoCondiciones = bExisteBloqueo

    Ador.Close
    Set Ador = Nothing
    Set oInstancia = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "BloqueoCondiciones", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function


''' <summary>
''' Muestra una ventana para la configuraci�n de la �ptima.
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdCalcularOptima; Tiempo m�ximo:0,3</remarks>

Public Sub ConfigurarOptima()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picControlOptima.Visible = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ConfigurarOptima", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>Carga del men� para Adjudicar/Desadjudicar</summary>
''' <param name="X">Coordenada horizontal</param>
''' <param name="Y">Coordenada vertical</param>
''' <remarks>Llamada desde: ; Tiempo m�ximo:0,1</remarks>

Private Sub CargarMenuAdj(ByVal X As Single, ByVal Y As Single)
    Dim oAdj As CAdjudicacion
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cP.clear
    
    'Si es una columna de precio de un escalado mostrar el men� de adjudicar/desadjudicar
    With sdbgAdjEsc
        Set oAdj = g_ogrupo.Adjudicaciones.Item(KeyEscalado(.Columns("ID").Value, .Columns("ART").Value, .Columns("ESC" & .Grp).Value))
    End With
    If Not oAdj Is Nothing Then
        cP.AddItem m_sDesadjudicar, , 2, , , , , "DES"
    Else
        cP.AddItem m_sAdjudicar, , 1, , , , , "ADJ"
    End If
    Set oAdj = Nothing
    
    cP.ShowPopupMenu X, Y
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CargarMenuAdj", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Gestiona la adjudicaci�n/desadjudicaci�n de un escalado</summary>
''' <param name="scodProve">Cod. proveedor</param>
''' <param name="lIdEscalado">Id. escalado</param>
''' <param name="bAdjudicar">Indica si es adjudicaci�n o desadjudicaci�n</param>
''' <param name="dblCantEsc">Cantidad adjudicada al escalado</param>
''' <param name="dblCantProve">Cantidad adjudicada al proveedor</param>
''' <remarks>Llamada desde: sdbgAdjEsc_DblClick</remarks>
''' <revision>LTG 23/02/2012</revision>

Private Sub AdjudicarDesadjudicarEscalado(ByVal scodProve As String, ByVal lIdEscalado As Long, ByVal bAdjudicar As Boolean, Optional ByVal dblCantEsc As Variant, _
        Optional ByVal dblCantProve As Variant)
    Dim bRecalcAdjsItem As Boolean
    Dim dblConsumido As Double
    Dim dblAhorrado As Double
    Dim oAdj As CAdjudicacion
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    LockWindowUpdate Me.hWnd
    
    With sdbgAdjEsc
        If bAdjudicar Then
            'Si no est� adjudicado adjudicar
            If AdjudicarEscalado(scodProve, lIdEscalado, bRecalcAdjsItem, dblCantEsc, dblCantProve) Then
                If m_oProcesoSeleccionado.Estado < conadjudicaciones And m_oProcesoSeleccionado.Bloqueado Then
                    g_oOrigen.cmdGuardar.Enabled = True
                    m_oProcesoSeleccionado.GuardarProceso = True
                ElseIf m_oProcesoSeleccionado.Estado > ParcialmenteCerrado Then
                    m_oProcesoSeleccionado.ModificadoHojaAdj = True
                End If
            End If
        Else
            'Si est� adjudicado desadjudicar
            If DesadjudicarEscalado(scodProve, lIdEscalado, bRecalcAdjsItem, dblCantEsc) Then
                g_oOrigen.cmdGuardar.Enabled = True
            End If
        End If

        AdjudicacionModificada
    End With
    
    If bRecalcAdjsItem Then
        For Each oAdj In g_ogrupo.Adjudicaciones
            If oAdj.Id = g_oItemSeleccionado.Id And (oAdj.ProveCod <> scodProve Or (oAdj.ProveCod = scodProve And oAdj.Escalado <> lIdEscalado)) Then
                If Not IsNull(oAdj.Escalado) Then
                    AdjudicarEscalado oAdj.ProveCod, oAdj.Escalado, bRecalcAdjsItem, oAdj.Adjudicado, dblCantProve
                End If
            End If
        Next
        Set oAdj = Nothing
    End If
    
    'Grid resultados
    dblConsumido = DevolverImporteConsumido
    dblAhorrado = dblConsumido - g_oItemSeleccionado.ImporteAdj 'ImporteAdj en moneda proceso
    CargarGridResultados dblConsumido, dblAhorrado, g_oItemSeleccionado.ImporteAdj
    
    LockWindowUpdate 0&
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "AdjudicarDesadjudicarEscalado", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Realiza la adjudicaci�n de un escalado de un proveedor</summary>
''' <param name="scodProve">Cod. proveedor</param>
''' <param name="IdEscalado">Id del escalado a adjudicar</param>
''' <param name="bRecalcAdjsItem">Indica si hay que recalcular el resto de adjudicaciones del item</param>
''' <param name="dblCantEsc">Cantidad adjudicada al escalado</param>
''' <param name="dblCantProve">Cantidad adjudicada al proveedor</param>
''' <returns>Booleano indicando si se ha producido la adjudicaci�n</returns>
''' <remarks>Llamada desde: sdbgAdjEsc_DblClick</remarks>
''' <revision>LTG 22/02/2012</revision>

Private Function AdjudicarEscalado(ByVal scodProve As String, ByVal IdEscalado As Long, ByRef bRecalcAdjsItem As Boolean, Optional ByVal dblCantEsc As Variant, _
        Optional ByVal dblCantProve As Variant) As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AdjudicarEscalado = False
    
    With sdbgAdjEsc
        If .Columns("CERRADO").Value Then Exit Function

        'Si no hay precio no se puede adjudicar
        If .Columns("PRES" & .Grp).Value = "" Then Exit Function
        
        'Actualizar las colecciones de frmADJ
        RecalcularCantidadesEscPorEscalado g_ogrupo, True, .Columns("ART").Value, scodProve, IdEscalado, dblCantEsc, dblCantProve

        bRecalcAdjsItem = ComprobarRecalcular(dblCantEsc)

        .Update
        
        m_bRecargarOrigen = True
    End With
    
    AdjudicarEscalado = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "AdjudicarEscalado", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>Deshace la la adjudicaci�n de un escalado de un proveedor</summary>
''' <param name="scodProve">Cod. proveedor</param>
''' <param name="IdEscalado">Id del escalado a adjudicar</param>
''' <param name="bRecalcAdjsItem">Indica si hay que recalcular el resto de adjudicaciones del item</param>
''' <param name="dblCantEsc">Cantidad adjudicada al escalado</param>
''' <param name="dblCantProve">Cantidad adjudicada al proveedor</param>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>LTG 22/02/2012</revision>

Private Function DesadjudicarEscalado(ByVal scodProve As String, ByVal IdEscalado As Long, ByRef bRecalcAdjsItem As Boolean, Optional ByVal dblCantEsc As Variant) As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DesadjudicarEscalado = False
    
    With sdbgAdjEsc
        If .Columns("CERRADO").Value Then Exit Function

        'Actualizar las colecciones de frmADJ
        RecalcularCantidadesEscPorEscalado g_ogrupo, False, .Columns("ART").Value, scodProve, IdEscalado

        bRecalcAdjsItem = ComprobarRecalcular(dblCantEsc)

        .Update
        
        m_bRecargarOrigen = True
    End With
    
    DesadjudicarEscalado = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "DesadjudicarEscalado", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>Comprueba si hay que hacer un rec�lculo de las adjudicaciones</summary>
''' <param name="dblCantEsc">Cantidad adjudicada al escalado</param>
''' <returns>Booleano indicando si hay que recalcular</returns>
''' <remarks>Llamada desde: AdjudiocarEscalado, Desadjudicarescalado</remarks>
''' <revision>LTG 16/04/2012</revision>

Private Function ComprobarRecalcular(Optional ByVal dblCantEsc As Variant) As Boolean
    Dim oAdj As CAdjudicacion
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ComprobarRecalcular = False
    
    'Si no hay cantidades adjudicadas hay que recalcular
    If IsMissing(dblCantEsc) Then
        ComprobarRecalcular = True
    ElseIf dblCantEsc = 0 Then
        ComprobarRecalcular = True
    Else
        For Each oAdj In g_ogrupo.Adjudicaciones
            If oAdj.Id = g_oItemSeleccionado.Id Then
                If NullToDbl0(oAdj.Adjudicado) = 0 Then
                    ComprobarRecalcular = True
                    Exit For
                End If
            End If
        Next
        Set oAdj = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "ComprobarRecalcular", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Antes de hacer ValidacionYCierre su comprueba la restricciones de importe/presupuesto maximo
''' </summary>
''' <param name="CierreTotal">true->Cierre Total proceso false->Cierre un item</param>
''' <returns>Si se puede cerrar o no</returns>
''' <remarks>Llamada desde: ValidarParcial ; Tiempo m�ximo: 0,2</remarks>
Private Function CtrlIgnorarRestriccAdjDir(Optional ByVal CierreTotal As Boolean = False) As Boolean
    Dim teserror As TipoErrorSummit
    Dim oAdjGr As CAdjudicacion
    Dim SumaAdjudicado As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    CtrlIgnorarRestriccAdjDir = True
    
    If Not g_oOrigen.m_oProcesoSeleccionado.PermitirAdjDirecta Then Exit Function
    
    If Not CierreTotal Then
        SumaAdjudicado = m_dYaCerrado
        If g_ogrupo.UsarEscalados Then
            SumaAdjudicado = SumaAdjudicado + m_oAdjParcs.Item(1).ImporteAdjTot
        Else
            For Each oAdjGr In m_oAdjParcs
                SumaAdjudicado = SumaAdjudicado + oAdjGr.ImporteAdjTot
            Next
        End If
    Else
        SumaAdjudicado = g_oOrigen.m_dblAdjudicadoProcReal
    End If
    
    If (gParametrosGenerales.gdVOL_MAX_ADJ_DIR > 0) _
    And ((gParametrosGenerales.gdVOL_MAX_ADJ_DIR * g_oOrigen.m_oProcesoSeleccionado.Cambio) < g_dblAbiertoProc) Then
        teserror.NumError = TESVolumenAdjDirSuperado
        teserror.Arg1 = gParametrosGenerales.gdVOL_MAX_ADJ_DIR * g_oOrigen.m_oProcesoSeleccionado.Cambio
        teserror.Arg2 = g_dblAbiertoProc
    
        If g_oOrigen.m_bIgnorarRestricAdjDirecta Then
            If oMensajes.PreguntarAdjDirSuperado(teserror) <> vbYes Then
                CtrlIgnorarRestriccAdjDir = False
                Exit Function
            End If
        Else
            oMensajes.NoValidaAdjDirSuperado teserror
            
            CtrlIgnorarRestriccAdjDir = False
            Exit Function
        End If
    End If
            
    If (gParametrosGenerales.gdIMP_MAX_ADJ_DIR > 0) _
    And ((gParametrosGenerales.gdIMP_MAX_ADJ_DIR * g_oOrigen.m_oProcesoSeleccionado.Cambio) < SumaAdjudicado) Then
    
        teserror.NumError = TESImporteAdjDirSuperado
        teserror.Arg1 = gParametrosGenerales.gdIMP_MAX_ADJ_DIR * g_oOrigen.m_oProcesoSeleccionado.Cambio
        teserror.Arg2 = SumaAdjudicado
    
        If g_oOrigen.m_bIgnorarRestricAdjDirecta Then
            If oMensajes.PreguntarAdjDirSuperado(teserror) <> vbYes Then
                CtrlIgnorarRestriccAdjDir = False
                Exit Function
            End If
        Else
            oMensajes.NoValidaAdjDirSuperado teserror
        
            CtrlIgnorarRestriccAdjDir = False
            Exit Function
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJItem", "CtrlIgnorarRestriccAdjDir", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function
