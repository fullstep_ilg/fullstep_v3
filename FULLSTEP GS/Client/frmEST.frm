VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmEST 
   BackColor       =   &H00E0E0E0&
   ClientHeight    =   7935
   ClientLeft      =   585
   ClientTop       =   2205
   ClientWidth     =   15240
   ControlBox      =   0   'False
   Icon            =   "frmEST.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   OLEDropMode     =   1  'Manual
   Picture         =   "frmEST.frx":000C
   ScaleHeight     =   7935
   ScaleWidth      =   15240
   Begin VB.Frame frameFiltro 
      BackColor       =   &H00E0E0E0&
      Height          =   3195
      Left            =   0
      TabIndex        =   41
      Top             =   500
      Width           =   15345
      Begin VB.CommandButton cmdLimpiar 
         Caption         =   "D&Limpiar"
         Height          =   315
         Left            =   14280
         TabIndex        =   75
         Top             =   2790
         Width           =   840
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "DBuscar"
         Height          =   315
         Left            =   13290
         Style           =   1  'Graphical
         TabIndex        =   74
         TabStop         =   0   'False
         Top             =   2790
         Width           =   825
      End
      Begin VB.Frame frDatos 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   1245
         Index           =   0
         Left            =   60
         TabIndex        =   68
         Top             =   1470
         Width           =   15195
         Begin VB.CommandButton cmdElimAtrBuscar 
            Height          =   300
            Left            =   14760
            Picture         =   "frmEST.frx":034E
            Style           =   1  'Graphical
            TabIndex        =   70
            Top             =   720
            UseMaskColor    =   -1  'True
            Width           =   300
         End
         Begin VB.CommandButton cmdAnyaAtrBuscar 
            Height          =   300
            Left            =   14760
            Picture         =   "frmEST.frx":03E0
            Style           =   1  'Graphical
            TabIndex        =   69
            Top             =   330
            UseMaskColor    =   -1  'True
            Width           =   300
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAtrBuscar 
            Height          =   1245
            Left            =   30
            TabIndex        =   71
            Top             =   0
            Width           =   14625
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   14
            stylesets.count =   3
            stylesets(0).Name=   "Yellow"
            stylesets(0).BackColor=   11862015
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmEST.frx":0462
            stylesets(1).Name=   "Normal"
            stylesets(1).ForeColor=   0
            stylesets(1).BackColor=   16777215
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmEST.frx":047E
            stylesets(2).Name=   "Header"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmEST.frx":049A
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            HeadStyleSet    =   "Header"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   14
            Columns(0).Width=   4551
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(1).Width=   8916
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   1058
            Columns(2).Caption=   "OPER"
            Columns(2).Name =   "OPER"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   7064
            Columns(3).Caption=   "VALOR"
            Columns(3).Name =   "VALOR"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "INTRO"
            Columns(4).Name =   "INTRO"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "IDTIPO"
            Columns(5).Name =   "IDTIPO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "ID_ATRIB"
            Columns(6).Name =   "ID_ATRIB"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "MAXIMO"
            Columns(7).Name =   "MAXIMO"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "MINIMO"
            Columns(8).Name =   "MINIMO"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "VALOR_ATRIB"
            Columns(9).Name =   "VALOR_ATRIB"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "NUM"
            Columns(10).Name=   "NUM"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   2
            Columns(10).FieldLen=   256
            Columns(11).Width=   3200
            Columns(11).Caption=   "AMBITO"
            Columns(11).Name=   "AMBITO"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(11).Locked=   -1  'True
            Columns(11).Style=   3
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "AMBITO_VALOR"
            Columns(12).Name=   "AMBITO_VALOR"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(13).Width=   3200
            Columns(13).Visible=   0   'False
            Columns(13).Caption=   "LISTAEXTERNA"
            Columns(13).Name=   "LISTAEXTERNA"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            _ExtentX        =   25797
            _ExtentY        =   2196
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddOper 
            Height          =   915
            Left            =   1170
            TabIndex        =   72
            Top             =   840
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmEST.frx":04B6
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
            BackColor       =   8421376
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddValorAtrBuscar 
            Height          =   915
            Left            =   3540
            TabIndex        =   73
            Top             =   660
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmEST.frx":04D2
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
            BackColor       =   8421376
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddAmbito 
            Height          =   915
            Left            =   0
            TabIndex        =   76
            Top             =   0
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmEST.frx":04EE
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
            BackColor       =   8421376
         End
      End
      Begin VB.TextBox txtDenominacion 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   11520
         TabIndex        =   17
         Top             =   500
         Width           =   3675
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMes 
         Height          =   255
         Left            =   360
         TabIndex        =   3
         Top             =   540
         Width           =   1125
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1984
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEquipo 
         CausesValidation=   0   'False
         Height          =   285
         Left            =   2500
         TabIndex        =   5
         Top             =   240
         Width           =   1960
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         HeadLines       =   0
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).Picture=   "frmEST.frx":050A
         stylesets(0).AlignmentText=   0
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5318
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3457
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcComprador 
         CausesValidation=   0   'False
         Height          =   285
         Left            =   2500
         TabIndex        =   6
         Top             =   580
         Width           =   1960
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         HeadLines       =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5318
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3457
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtComprador 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1650
         Locked          =   -1  'True
         TabIndex        =   43
         Top             =   580
         Visible         =   0   'False
         Width           =   2770
      End
      Begin VB.CommandButton cmdLimpiarMat 
         Height          =   285
         Left            =   10640
         Picture         =   "frmEST.frx":0526
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   180
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscarMat 
         Height          =   285
         Left            =   11000
         Picture         =   "frmEST.frx":0A68
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   180
         Width           =   315
      End
      Begin VB.CommandButton cmdLimpiarPres 
         Height          =   285
         Left            =   10640
         Picture         =   "frmEST.frx":0DEA
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   820
         Width           =   315
      End
      Begin VB.TextBox txtPresup 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5740
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   820
         Width           =   4870
      End
      Begin VB.CommandButton cmdBuscarPres 
         Height          =   285
         Left            =   11000
         Picture         =   "frmEST.frx":132C
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   820
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscarUO 
         Height          =   285
         Left            =   11000
         Picture         =   "frmEST.frx":16AE
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   500
         Width           =   315
      End
      Begin VB.OptionButton optDesde 
         BackColor       =   &H00E0E0E0&
         Caption         =   "DDesde"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   175
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   915
      End
      Begin VB.OptionButton optTodos 
         BackColor       =   &H00E0E0E0&
         Caption         =   "DTodos"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   175
         Left            =   120
         TabIndex        =   1
         Top             =   150
         Value           =   -1  'True
         Width           =   915
      End
      Begin VB.CheckBox chkResponsable 
         BackColor       =   &H00E0E0E0&
         Caption         =   "DResponsable"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   235
         Left            =   2500
         TabIndex        =   7
         Top             =   900
         Width           =   1695
      End
      Begin VB.TextBox txtEquipo 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1650
         Locked          =   -1  'True
         TabIndex        =   42
         Top             =   240
         Visible         =   0   'False
         Width           =   2770
      End
      Begin VB.CommandButton cmdLimpiarUO 
         Height          =   285
         Left            =   10640
         Picture         =   "frmEST.frx":1A30
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   500
         Width           =   315
      End
      Begin VB.TextBox txtUO 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5740
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   500
         Width           =   4870
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   255
         Left            =   360
         TabIndex        =   4
         Top             =   840
         Width           =   1125
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1984
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
      End
      Begin VB.TextBox txtMaterial 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5745
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   180
         Width           =   4870
      End
      Begin VB.PictureBox picAbrirAtrBuscar 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   135
         Left            =   210
         Picture         =   "frmEST.frx":1F72
         ScaleHeight     =   135
         ScaleWidth      =   135
         TabIndex        =   67
         Top             =   1260
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.PictureBox picCerrarAtrBuscar 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   180
         Left            =   210
         Picture         =   "frmEST.frx":22BA
         ScaleHeight     =   180
         ScaleWidth      =   135
         TabIndex        =   66
         Top             =   1260
         Width           =   135
      End
      Begin VB.Line Line3 
         Index           =   1
         X1              =   90
         X2              =   15270
         Y1              =   1170
         Y2              =   1170
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         Caption         =   "DB�squeda por atributo"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   165
         Index           =   0
         Left            =   360
         TabIndex        =   65
         Top             =   1230
         Width           =   2865
      End
      Begin VB.Label lblDenominacion 
         BackColor       =   &H00E0E0E0&
         Caption         =   "DDenominaci�n:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   11520
         TabIndex        =   64
         Top             =   240
         Width           =   1575
      End
      Begin VB.Line Line4 
         Index           =   2
         X1              =   11400
         X2              =   11400
         Y1              =   120
         Y2              =   1170
      End
      Begin VB.Label lblMaterial 
         BackColor       =   &H00E0E0E0&
         Caption         =   "DMaterial:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4540
         TabIndex        =   48
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label lblPresup 
         BackColor       =   &H00E0E0E0&
         Caption         =   "DPresupuestos:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4540
         TabIndex        =   47
         Top             =   880
         Width           =   1575
      End
      Begin VB.Line Line2 
         X1              =   1560
         X2              =   1560
         Y1              =   120
         Y2              =   1170
      End
      Begin VB.Label lblEquipo 
         BackColor       =   &H00E0E0E0&
         Caption         =   "DEquipo:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1650
         TabIndex        =   46
         Top             =   300
         Width           =   975
      End
      Begin VB.Label lblComprador 
         BackColor       =   &H00E0E0E0&
         Caption         =   "DComprador:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1650
         TabIndex        =   45
         Top             =   640
         Width           =   1095
      End
      Begin VB.Line Line3 
         Index           =   0
         X1              =   4500
         X2              =   4500
         Y1              =   120
         Y2              =   1170
      End
      Begin VB.Label lblUO 
         BackColor       =   &H00E0E0E0&
         Caption         =   "DUnidad org.:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4540
         TabIndex        =   44
         Top             =   560
         Width           =   1515
      End
   End
   Begin VB.PictureBox picToolTip 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   4800
      ScaleHeight     =   285
      ScaleWidth      =   3945
      TabIndex        =   59
      Top             =   3690
      Visible         =   0   'False
      Width           =   3945
      Begin VB.Label lblToolTip 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "DN�mero de proveedores que han presentado ofertas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   0
         TabIndex        =   60
         Top             =   0
         Width           =   3945
      End
   End
   Begin VB.PictureBox picMostrarMenu 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   240
      Left            =   0
      ScaleHeight     =   240
      ScaleWidth      =   1005
      TabIndex        =   56
      Top             =   3690
      Visible         =   0   'False
      Width           =   1010
      Begin VB.PictureBox PicAbrirMenu 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   150
         Left            =   750
         Picture         =   "frmEST.frx":2604
         ScaleHeight     =   150
         ScaleWidth      =   210
         TabIndex        =   58
         Top             =   60
         Width           =   210
      End
      Begin VB.Line Line5 
         Index           =   3
         X1              =   990
         X2              =   990
         Y1              =   300
         Y2              =   0
      End
      Begin VB.Line Line5 
         Index           =   2
         X1              =   0
         X2              =   0
         Y1              =   300
         Y2              =   0
      End
      Begin VB.Label lblMostrarMenu 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Mostrar"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   90
         TabIndex        =   57
         Top             =   30
         Width           =   555
      End
   End
   Begin VB.PictureBox picOcultarMenu 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   240
      Left            =   2320
      ScaleHeight     =   240
      ScaleWidth      =   825
      TabIndex        =   53
      Top             =   3690
      Width           =   830
      Begin VB.PictureBox PicCerrarMenu 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   220
         Left            =   90
         Picture         =   "frmEST.frx":294D
         ScaleHeight     =   225
         ScaleWidth      =   165
         TabIndex        =   55
         Top             =   60
         Width           =   165
      End
      Begin VB.Line Line5 
         Index           =   1
         X1              =   0
         X2              =   0
         Y1              =   300
         Y2              =   0
      End
      Begin VB.Line Line5 
         Index           =   0
         X1              =   810
         X2              =   810
         Y1              =   300
         Y2              =   0
      End
      Begin VB.Label lblOcultarMenu 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Ocultar"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   270
         TabIndex        =   54
         Top             =   30
         Width           =   465
      End
   End
   Begin VB.PictureBox picMostrarFiltro 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   240
      Left            =   10440
      ScaleHeight     =   240
      ScaleWidth      =   1035
      TabIndex        =   49
      Top             =   3690
      Width           =   1040
      Begin VB.PictureBox picAbrirFiltro 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   215
         Left            =   60
         Picture         =   "frmEST.frx":2C97
         ScaleHeight     =   210
         ScaleWidth      =   105
         TabIndex        =   50
         Top             =   60
         Visible         =   0   'False
         Width           =   105
      End
      Begin VB.PictureBox picCerrarFiltro 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   150
         Left            =   60
         Picture         =   "frmEST.frx":2FDF
         ScaleHeight     =   150
         ScaleWidth      =   105
         TabIndex        =   52
         Top             =   60
         Width           =   105
      End
      Begin VB.Line Line4 
         Index           =   1
         X1              =   1020
         X2              =   1020
         Y1              =   300
         Y2              =   0
      End
      Begin VB.Line Line4 
         Index           =   0
         X1              =   0
         X2              =   0
         Y1              =   300
         Y2              =   0
      End
      Begin VB.Label lblMostraFiltro 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Mostrar"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   165
         Left            =   270
         TabIndex        =   51
         Top             =   50
         Width           =   700
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   65535
      Left            =   11655
      Top             =   3690
   End
   Begin VB.PictureBox Picture2 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   0
      Picture         =   "frmEST.frx":3329
      ScaleHeight     =   240
      ScaleWidth      =   15240
      TabIndex        =   33
      TabStop         =   0   'False
      Top             =   7695
      Width           =   15240
      Begin VB.CommandButton cmdMicro 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   9540
         Picture         =   "frmEST.frx":1D76B
         Style           =   1  'Graphical
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   0
         Width           =   285
      End
      Begin VB.CommandButton cmdExcel 
         Height          =   285
         Left            =   9855
         Picture         =   "frmEST.frx":1DAF5
         Style           =   1  'Graphical
         TabIndex        =   61
         Top             =   0
         Width           =   345
      End
      Begin VB.PictureBox PicExclamacion 
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   8955
         Picture         =   "frmEST.frx":1E007
         ScaleHeight     =   255
         ScaleWidth      =   225
         TabIndex        =   40
         Top             =   0
         Width           =   225
      End
      Begin VB.PictureBox PicNuevas 
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   9195
         Picture         =   "frmEST.frx":1E379
         ScaleHeight     =   255
         ScaleWidth      =   300
         TabIndex        =   38
         Top             =   0
         Visible         =   0   'False
         Width           =   300
      End
      Begin VB.Label lblFS 
         BackStyle       =   0  'Transparent
         Caption         =   "FullStep GS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   315
         Left            =   10400
         TabIndex        =   34
         Top             =   0
         Visible         =   0   'False
         Width           =   1065
      End
   End
   Begin VB.CommandButton cmdMinimizar 
      Height          =   225
      Left            =   9330
      Picture         =   "frmEST.frx":1E7B7
      Style           =   1  'Graphical
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   30
      UseMaskColor    =   -1  'True
      Width           =   250
   End
   Begin VB.CommandButton cmdCerrar 
      Height          =   225
      Left            =   9900
      Picture         =   "frmEST.frx":1ECA9
      Style           =   1  'Graphical
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   30
      Width           =   250
   End
   Begin VB.CommandButton cmdMaximizar 
      Height          =   225
      Left            =   9600
      Picture         =   "frmEST.frx":1EEA3
      Style           =   1  'Graphical
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   30
      Width           =   250
   End
   Begin VB.PictureBox picTot 
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   60
      ScaleHeight     =   195
      ScaleWidth      =   2355
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   5385
      Visible         =   0   'False
      Width           =   2355
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Total+"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   300
         TabIndex        =   27
         Top             =   0
         Width           =   1755
      End
   End
   Begin VB.PictureBox picTotCerr 
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   60
      ScaleHeight     =   195
      ScaleWidth      =   2355
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   5085
      Visible         =   0   'False
      Width           =   2355
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Total cerrados+"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   300
         TabIndex        =   25
         Top             =   0
         Width           =   1755
      End
   End
   Begin VB.PictureBox picTotPend 
      BorderStyle     =   0  'None
      FontTransparent =   0   'False
      Height          =   195
      Left            =   60
      ScaleHeight     =   195
      ScaleWidth      =   2355
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   4785
      Visible         =   0   'False
      Width           =   2355
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Total pendientes+"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   300
         TabIndex        =   24
         Top             =   0
         Width           =   1755
      End
   End
   Begin VB.PictureBox picSeparator2 
      BorderStyle     =   0  'None
      FontTransparent =   0   'False
      Height          =   195
      Left            =   60
      ScaleHeight     =   195
      ScaleWidth      =   3015
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   5115
      Width           =   3015
   End
   Begin VB.PictureBox picSeparator1 
      BorderStyle     =   0  'None
      FontTransparent =   0   'False
      Height          =   195
      Left            =   60
      ScaleHeight     =   195
      ScaleWidth      =   3015
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   4815
      Width           =   3015
   End
   Begin VB.PictureBox picMenu 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      DrawMode        =   1  'Blackness
      DrawStyle       =   5  'Transparent
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   3315
      Left            =   0
      ScaleHeight     =   3315
      ScaleWidth      =   3135
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   3945
      Width           =   3135
      Begin SSDataWidgets_B.SSDBGrid sdbgMenu 
         Height          =   2985
         Left            =   120
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   120
         Width           =   2850
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BorderStyle     =   0
         RecordSelectors =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   4
         stylesets.count =   16
         stylesets(0).Name=   "PendComObj"
         stylesets(0).BackColor=   14745599
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmEST.frx":1F075
         stylesets(1).Name=   "PendAdj"
         stylesets(1).BackColor=   14745599
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmEST.frx":1F091
         stylesets(2).Name=   "Bold"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmEST.frx":1F0AD
         stylesets(3).Name=   "Anulado"
         stylesets(3).BackColor=   16777184
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmEST.frx":1F0C9
         stylesets(4).Name=   "Normal"
         stylesets(4).BackColor=   128
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmEST.frx":1F0E5
         stylesets(5).Name=   "SmallFont"
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   5.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmEST.frx":1F101
         stylesets(6).Name=   "SeleccionadoBold"
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmEST.frx":1F11D
         stylesets(6).AlignmentText=   0
         stylesets(6).AlignmentPicture=   1
         stylesets(7).Name=   "EnRecOfe"
         stylesets(7).BackColor=   14745599
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmEST.frx":1F139
         stylesets(8).Name=   "Seleccionado"
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmEST.frx":1F155
         stylesets(8).AlignmentText=   0
         stylesets(8).AlignmentPicture=   1
         stylesets(9).Name=   "Separator"
         stylesets(9).BackColor=   128
         stylesets(9).HasFont=   -1  'True
         BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(9).Picture=   "frmEST.frx":1F35F
         stylesets(9).AlignmentPicture=   0
         stylesets(10).Name=   "AdjYNotificado"
         stylesets(10).BackColor=   16777184
         stylesets(10).HasFont=   -1  'True
         BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(10).Picture=   "frmEST.frx":219F1
         stylesets(11).Name=   "PendAsigProve"
         stylesets(11).BackColor=   16777215
         stylesets(11).HasFont=   -1  'True
         BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(11).Picture=   "frmEST.frx":21A0D
         stylesets(12).Name=   "ParcCerrado"
         stylesets(12).HasFont=   -1  'True
         BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(12).Picture=   "frmEST.frx":21A29
         stylesets(13).Name=   "PendEnvPet"
         stylesets(13).BackColor=   16777215
         stylesets(13).HasFont=   -1  'True
         BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(13).Picture=   "frmEST.frx":21A45
         stylesets(14).Name=   "PendValidar"
         stylesets(14).BackColor=   16777215
         stylesets(14).HasFont=   -1  'True
         BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(14).Picture=   "frmEST.frx":21A61
         stylesets(15).Name=   "AdjSinNotificar"
         stylesets(15).BackColor=   16777184
         stylesets(15).HasFont=   -1  'True
         BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(15).Picture=   "frmEST.frx":21A7D
         DividerType     =   2
         DividerStyle    =   3
         BevelColorHighlight=   16777215
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         StyleSet        =   "Normal"
         ForeColorEven   =   128
         ForeColorOdd    =   128
         BackColorEven   =   128
         BackColorOdd    =   128
         RowHeight       =   370
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   450
         Columns(1).Caption=   "COLOR"
         Columns(1).Name =   "COLOR"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Style=   1
         Columns(2).Width=   3704
         Columns(2).Caption=   "MENU"
         Columns(2).Name =   "MENU"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Style=   4
         Columns(2).ButtonsAlways=   -1  'True
         Columns(3).Width=   979
         Columns(3).Caption=   "NUMPROCE"
         Columns(3).Name =   "NUMPROCE"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).HasForeColor=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).ForeColor=   16777215
         Columns(3).BackColor=   128
         _ExtentX        =   5027
         _ExtentY        =   5265
         _StockProps     =   79
         ForeColor       =   128
         BackColor       =   128
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00000000&
         Visible         =   0   'False
         X1              =   60
         X2              =   1620
         Y1              =   1560
         Y2              =   1560
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid sdbgProcesos 
      Height          =   3315
      Left            =   3180
      TabIndex        =   18
      Top             =   3930
      Width           =   8385
      ScrollBars      =   3
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      stylesets.count =   17
      stylesets(0).Name=   "PendComObj"
      stylesets(0).BackColor=   16744576
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmEST.frx":21A99
      stylesets(1).Name=   "PendAdj"
      stylesets(1).BackColor=   8289982
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmEST.frx":21AB5
      stylesets(2).Name=   "Bold"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmEST.frx":21AD1
      stylesets(3).Name=   "Anulado"
      stylesets(3).BackColor=   8421504
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmEST.frx":21AED
      stylesets(4).Name=   "Normal"
      stylesets(4).BackColor=   -2147483647
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmEST.frx":21B09
      stylesets(5).Name=   "SmallFont"
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmEST.frx":21B25
      stylesets(6).Name=   "ActiveRow"
      stylesets(6).ForeColor=   16777215
      stylesets(6).BackColor=   8388608
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmEST.frx":21B41
      stylesets(7).Name=   "EnRecOfe"
      stylesets(7).BackColor=   12632256
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmEST.frx":21B5D
      stylesets(8).Name=   "CabeceraOrdenAsc"
      stylesets(8).ForeColor=   16777215
      stylesets(8).BackColor=   128
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmEST.frx":21B79
      stylesets(8).AlignmentPicture=   3
      stylesets(9).Name=   "AdjYNotificado"
      stylesets(9).BackColor=   13750691
      stylesets(9).HasFont=   -1  'True
      BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(9).Picture=   "frmEST.frx":21E5F
      stylesets(10).Name=   "PendAsigProve"
      stylesets(10).BackColor=   13171450
      stylesets(10).HasFont=   -1  'True
      BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(10).Picture=   "frmEST.frx":21E7B
      stylesets(11).Name=   "ParcCerrado"
      stylesets(11).HasFont=   -1  'True
      BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(11).Picture=   "frmEST.frx":21E97
      stylesets(12).Name=   "PendEnvPet"
      stylesets(12).BackColor=   8454016
      stylesets(12).HasFont=   -1  'True
      BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(12).Picture=   "frmEST.frx":21EB3
      stylesets(13).Name=   "Header"
      stylesets(13).ForeColor=   0
      stylesets(13).HasFont=   -1  'True
      BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(13).Picture=   "frmEST.frx":21ECF
      stylesets(14).Name=   "PendValidar"
      stylesets(14).BackColor=   8421631
      stylesets(14).HasFont=   -1  'True
      BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(14).Picture=   "frmEST.frx":21EEB
      stylesets(15).Name=   "AdjSinNotificar"
      stylesets(15).BackColor=   12632256
      stylesets(15).HasFont=   -1  'True
      BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(15).Picture=   "frmEST.frx":21F07
      stylesets(16).Name=   "CabeceraOrdenDesc"
      stylesets(16).ForeColor=   16777215
      stylesets(16).BackColor=   128
      stylesets(16).HasFont=   -1  'True
      BeginProperty stylesets(16).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(16).Picture=   "frmEST.frx":21F23
      stylesets(16).AlignmentPicture=   3
      DividerType     =   2
      DividerStyle    =   4
      BevelColorFrame =   0
      BevelColorHighlight=   12632256
      BevelColorShadow=   128
      BevelColorFace  =   128
      CheckBox3D      =   0   'False
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      HeadStyleSet    =   "Header"
      StyleSet        =   "SmallFont"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   14790
      _ExtentY        =   5847
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   4410
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   525
      Left            =   0
      Picture         =   "frmEST.frx":21F3F
      ScaleHeight     =   525
      ScaleWidth      =   15240
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   0
      Width           =   15240
      Begin VB.PictureBox pictureMinimizar 
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   5040
         Picture         =   "frmEST.frx":22601
         ScaleHeight     =   495
         ScaleWidth      =   1470
         TabIndex        =   36
         Top             =   0
         Visible         =   0   'False
         Width           =   1475
         Begin VB.Label lblMinimizar 
            BackStyle       =   0  'Transparent
            Caption         =   "Visor de procesos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   255
            Left            =   0
            TabIndex        =   37
            Top             =   0
            Width           =   1560
         End
      End
      Begin VB.CommandButton cmdRestaurarVentana 
         Height          =   225
         Left            =   9600
         Picture         =   "frmEST.frx":3CA43
         Style           =   1  'Graphical
         TabIndex        =   35
         TabStop         =   0   'False
         Top             =   30
         Visible         =   0   'False
         Width           =   250
      End
      Begin MSComDlg.CommonDialog cmmdGenerarExcel 
         Left            =   11055
         Top             =   45
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label lblSaludo 
         BackStyle       =   0  'Transparent
         Caption         =   "Bienvenido, usuario"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   435
         Left            =   600
         TabIndex        =   32
         Top             =   60
         Width           =   8655
      End
   End
   Begin VB.ListBox lstMultiMaterial 
      BackColor       =   &H80000018&
      Height          =   255
      ItemData        =   "frmEST.frx":3CC3D
      Left            =   5750
      List            =   "frmEST.frx":3CC3F
      TabIndex        =   62
      Top             =   680
      Visible         =   0   'False
      Width           =   4875
   End
   Begin VB.PictureBox picFont 
      Height          =   375
      Left            =   5760
      ScaleHeight     =   315
      ScaleWidth      =   315
      TabIndex        =   63
      Top             =   690
      Visible         =   0   'False
      Width           =   375
   End
End
Attribute VB_Name = "frmEST"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const CTOTABIERTOS = 12
Private Const CSEP1 = 13
Private Const CTOTCERRADOS = 14
Private Const CSEP2 = 15
Private Const CTOT = 16
Private Const cnMultiMatMaxHeight = 2000

''''''''''''''''''''''  VARIABLES P�BLICAS  ''''''''''''''''''''''''''''
'Proceso seleccionado
Public m_oProcesoSeleccionado As cProceso

''''''''''''''''''''''''''''''  VARIABLES PRIVADAS  ''''''''''''''''''''''''''''''
Private m_adoRecordset As New ADODB.Recordset

Private m_sEstadoIntegracion(6) As String 'estado integraci�n de los procesos

Private m_bModomovimiento As Boolean
Private m_bposx As Long
Private m_bposy As Long
Private m_bModoTamanyo As Boolean
Private m_bMostrado As Boolean

Private m_bTotPend As Boolean
Private m_bTotCerr As Boolean
Private m_dtFechaDesDes As Date
Private m_dtFechaHasDes As Date
Private m_dtFechaDesAdj As Date
Private m_dtFechaHasAdj As Date

'Variables de seguridad
Private m_bRAsig As Boolean
Private m_bREqp As Boolean
Private m_bRCompResp As Boolean
Private m_bRMat As Boolean
Private m_bSoloDeAdjDir As Boolean
Private m_bSoloDeAdjEnReunion As Boolean
Private m_bRUsuAper As Boolean
Private m_bRUsuUON As Boolean
Private m_bRUsuDep As Boolean
Private m_bRPerfUON As Boolean

'Variables de seguridad sobre el Buz�n de Ofertas
Private m_bRMatBuzOfe As Boolean
Private m_bRAsigBuzOfe As Boolean
Private m_bRCompRespBuzOfe As Boolean
Private m_bREqpAsigBuzOfe As Boolean
Private m_bROfeEqpBuzOfe As Boolean
Private m_bROfeTodasBuzOfe As Boolean
Private m_bRUsuAperBuzOfe As Boolean
Private m_bRUsuUonBuzOfe As Boolean
Private m_bRUsuDepBuzOfe As Boolean

Private m_iSeleccion As Integer
Private m_oGestorReuniones As CGestorReuniones
Private m_bRespetarRowColChange As Boolean

Private Ador As Ador.Recordset

Private m_dblTotal As Double
Private m_dblTotalAbiertos As Double
Private m_dblTotalCerrados As Double

' MULTILENGUAJE
Private m_sFrmTitulo As String 'Visor de procesos
Private m_sSaludo As String '
Private m_sUsuario As String
Private m_sEstados(13) As String 'estados procesos
Private m_sCaptionsGrid(32) As String ' los caption de la grid

Private m_sAdjDir(2) As String
Private m_stxtToolTipP As String
Private m_stxtToolTipO As String
Private m_stxtToolTipW As String
Private m_stxtOcultar As String
Private m_stxtMostrar As String
Private m_sIdiMeses(12) As String
Private m_slblProgreso As String
Private m_sMensajeExito As String
Private m_sNombreExcel As String

Private m_iDesde As Integer
Private m_iHasta As Integer

'Variable para el Timer
Private m_iIntervalo As Integer
Private miEstado As Integer
Private mbSubasta As Boolean

'Variables para la configuraci�n de campos,orden y filtro del visor
Public g_oConfVisor As CConfVistaVisor
Private m_arrOrden(33) As Variant
Private m_bCargandoFiltro As Boolean

'Para el filtro de UO
Public sUON1 As String
Public sUON2 As String
Public sUON3 As String

'Para los filtros de comprador responsable
Private m_sEqpCod As String
Private m_sComCod As String
Private m_bRespetarCombo As Boolean
Private m_oEqpSeleccionado As CEquipo
Private m_oEquipos As CEquipos
Private m_oCompSeleccionado As CComprador
Private m_oComps As CCompradores

'Presupuestos:
Private m_vPresup1_1 As Variant
Private m_vPresup1_2 As Variant
Private m_vPresup1_3 As Variant
Private m_vPresup1_4 As Variant

Private m_vPresup2_1 As Variant
Private m_vPresup2_2 As Variant
Private m_vPresup2_3 As Variant
Private m_vPresup2_4 As Variant

Private m_vPresup3_1 As Variant
Private m_vPresup3_2 As Variant
Private m_vPresup3_3 As Variant
Private m_vPresup3_4 As Variant

Private m_vPresup4_1 As Variant
Private m_vPresup4_2 As Variant
Private m_vPresup4_3 As Variant
Private m_vPresup4_4 As Variant

Private m_sUON1Pres As String
Private m_sUON2Pres As String
Private m_sUON3Pres As String

'Para el filtro de fecha:
Private sMesAnterior As String
Private sAnyoAnterior As String

Private g_bSoloInvitado As Boolean

Private m_bMicrostrategy As Boolean
Private m_bAvisoContrato As Boolean
Private m_bAvisoNotificacion As Boolean
Private m_vTZHora As Variant

Private m_oGMN1Seleccionados As CGruposMatNivel1
Private m_oGMN2Seleccionados As CGruposMatNivel2
Private m_oGMN3Seleccionados As CGruposMatNivel3
Private m_oGMN4Seleccionados As CGruposMatNivel4

Private m_sDenominacion As String
Private m_bAtrBuscarVisible As Boolean
Private arOper() As Variant
Private msMsgMinMax As String
Private m_bActivado As Boolean
Private m_sIdiOcultarAtrBuscar As String
Private m_sIdiMostrarAtrBuscar As String
''' <summary>
''' Posiciona en la columna del combo para poner ltexto en la grid asociada
''' </summary>
Private Sub sdbddAmbito_CloseUp()
ComboAmbito_CloseUp sdbddAmbito, sdbgAtrBuscar
End Sub

Private Sub sdbddAmbito_InitColumnProps()
'Voy a usar el mismo procedimiento que para el combo de valores porque es igual
ComboValorAtrBuscar_InitColumnProps sdbddAmbito
End Sub
''' <summary>
''' Posiciona en la columna del combo para poner texto en la grid asociada
''' </summary>
Private Sub sdbddAmbito_PositionList(ByVal Text As String)
'Voy a usar lo mismo del ssdbddValoratrbuscar porque es igual
ComboValorAtrBuscar_PositionList sdbddAmbito, Text, sdbgAtrBuscar
End Sub
''' <summary>
''' Muestra u oculta el filtro de atributos
''' <remarks>Llamada desde Label4_Click,picAbrirAtrBuscar_Click,picCerrarAtrBuscar_Click,lblMostraFiltro_Click,ComprobarYAplicarFiltros </remarks>
''' </summary>
Private Sub MostrarAtrBuscar(b As Boolean)
Dim lTop As Long
frDatos(0).Visible = b
If b Then
    If m_bActivado Then
        'En el dise�o est� ampliado as� que la primera vez que se carga en caso de estar guardado
        'el filtro con busqueda por atributos visible, no se ampliar� el frame
        frameFiltro.Height = Label4(0).Top + Label4(0).Height + cmdRestaurar.Height + frDatos(0).Height + 300
        Label4(0).caption = m_sIdiOcultarAtrBuscar
        picAbrirAtrBuscar.Visible = False
        picCerrarAtrBuscar.Visible = True
        cmdRestaurar.Top = frDatos(0).Top + frDatos(0).Height + 100
    End If
Else
    frameFiltro.Height = Label4(0).Top + Label4(0).Height + cmdRestaurar.Height + 100
    Label4(0).caption = m_sIdiMostrarAtrBuscar
    picCerrarAtrBuscar.Visible = False
    picAbrirAtrBuscar.Visible = True
    cmdRestaurar.Top = Label4(0).Top + Label4(0).Height
End If
cmdLimpiar.Top = cmdRestaurar.Top
MostrarFiltro
End Sub

''' <summary>
''' Agrega un atributo nuevo al filtro, por defecto en ambito Proceso
''' </summary>
Private Sub cmdAnyaAtrBuscar_Click()
Dim ofrmATRIB As frmAtribMod
If sdbgAtrBuscar.DataChanged Then sdbgAtrBuscar.Update
Set ofrmATRIB = New frmAtribMod
With ofrmATRIB
    .g_sOrigen = "frmESTArtBuscar"
    .sstabGeneral.Tab = 0
    .sdbgAtributosGrupo.SelectTypeRow = ssSelectionTypeMultiSelectRange
    .g_bSoloSeleccion = True
    .Show vbModal
End With
Set ofrmATRIB = Nothing
Set g_oConfVisor.AtrBuscar = MoverGridAtrBuscarAColleccion(sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0))
End Sub

''' <summary>
''' Elimina el atributo seleccionado para el filtro LO ELIMINAAAA, no elimina solo el valor
''' </summary>
Private Sub cmdElimAtrBuscar_Click()
Dim bm As Variant
If sdbgAtrBuscar.Rows = 0 Then Exit Sub
bm = sdbgAtrBuscar.Bookmark
'Primero movemos los valores de la grid a la colecci�n para que pille bhien los valores
Set g_oConfVisor.AtrBuscar = MoverGridAtrBuscarAColleccion(sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0))
'Ahora borramos el atributo seleccionado
sdbgAtrBuscar.Bookmark = bm
DoEvents
g_oConfVisor.AtrBuscar.Remove sdbgAtrBuscar.Columns("NUM").Value
CargarAtrBuscar sdbgAtrBuscar, g_oConfVisor, m_sAdjDir(1), m_sAdjDir(0), sdbddAmbito, TAtributoAmbito.ProcesoAtr
DoEvents
End Sub
''' <summary>
''' Limpia los valores del filtro
''' </summary>
Private Sub cmdLimpiar_Click()
sdbcEquipo.Text = ""
txtEquipo.Text = ""
txtComprador.Text = ""
chkResponsable.Value = 0
cmdLimpiarMat_Click
cmdLimpiarPres_Click
cmdLimpiarUO_Click
txtDenominacion.Text = ""
sdbgAtrBuscar.RemoveAll
Set g_oConfVisor.AtrBuscar = MoverGridAtrBuscarAColleccion(sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0))
End Sub
''' <summary>
''' Muestra u oculta el filtro de atributos
''' </summary>
Private Sub Label4_Click(Index As Integer)
Select Case Index
    Case 0
        MostrarAtrBuscar Not frDatos(0).Visible
End Select
End Sub

''' <summary>
''' Muestra u oculta el filtro de atributos
''' </summary>
Private Sub picAbrirAtrBuscar_Click()
MostrarAtrBuscar True
End Sub

''' <summary>
''' Muestra u oculta el filtro de atributos
''' </summary>
Private Sub picCerrarAtrBuscar_Click()
MostrarAtrBuscar False
End Sub

''' <summary>
''' Se cargan los valores en el combo
''' </summary>
Private Sub sdbddValorAtrBuscar_DropDown()
ComboValorAtrBuscar_DropDown sdbddValorAtrBuscar, sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0)
End Sub

Private Sub sdbddValorAtrBuscar_InitColumnProps()
ComboValorAtrBuscar_InitColumnProps sdbddValorAtrBuscar
End Sub
''' <summary>
''' Antes de seleccionar el valor se comprueban los atributos
''' </summary>
Private Sub sdbddValorAtrBuscar_PositionList(ByVal Text As String)
ComboValorAtrBuscar_PositionList sdbddValorAtrBuscar, Text, sdbgAtrBuscar
End Sub

''' <summary>
''' Posiciona en la columna del combo para poner ltexto en la grid asociada
''' </summary>
Private Sub sdbddValorAtrBuscar_ValidateList(Text As String, RtnPassed As Integer)
ComboValorAtrBuscar_ValidateList Text, RtnPassed, sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0)
End Sub

Private Sub sdbddOper_InitColumnProps()
'Voy a usar el mismo procedimiento que para el combo de valores porque es igual
ComboValorAtrBuscar_InitColumnProps sdbddOper
End Sub

''' <summary>
''' Posiciona en la columna del combo para poner ltexto en la grid asociada
''' </summary>
Private Sub sdbddOper_PositionList(ByVal Text As String)
'Voy a usar lo mismo del ssdbddValoratrbuscar porque es igual
ComboValorAtrBuscar_PositionList sdbddOper, Text, sdbgAtrBuscar
End Sub

''' <summary>
''' Antes de seleccionar el valor se comprueban que sean validos
''' </summary>
Private Sub sdbddOper_ValidateList(Text As String, RtnPassed As Integer)
ComboOper_ValidateList Text, RtnPassed, sdbgAtrBuscar, arOper
End Sub

''' <summary>
''' Antes de actualizar el valor se comprueban los atributos
''' </summary>
Private Sub sdbgAtrBuscar_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
GridAtrBuscar_BeforeColUpdate sdbgAtrBuscar, ColIndex, OldValue, Cancel, msMsgMinMax
End Sub
''' <summary>
''' Se actualiza el valor de la columna "VALOR_ATRIB"
''' </summary>
Private Sub sdbgAtrBuscar_Change()
    GridAtrBuscar_Change sdbgAtrBuscar
End Sub

''' <summary>
''' Se recuperan los formatos de las columnas
''' </summary>
Private Sub sdbgAtrBuscar_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
GridAtrBuscar_RowColChange sdbgAtrBuscar, LastRow, LastCol, sdbddOper, sdbddValorAtrBuscar, m_sAdjDir(1), m_sAdjDir(0)
End Sub

''' <summary>
''' Acci�n al scrollar en la grid porque se pueden nover todas las filas y se puede perder el valor
''' </summary>
Private Sub sdbgAtrBuscar_Scroll(Cancel As Integer)
GridAtrBuscar_Scroll sdbgAtrBuscar, Cancel
End Sub

''' <summary>
''' Cargar multidioma para visor procesos
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_EST, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        With Ador
            Label1.caption = Ador(0).Value '1
            .MoveNext
            Label2.caption = Ador(0).Value
            .MoveNext
            Label3.caption = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(0) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(1) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(2) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(3) = Ador(0).Value
            sdbgAtrBuscar.Columns("DEN").caption = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(4) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(5) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(6) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(7) = Ador(0).Value
            .MoveNext
            m_sFrmTitulo = Ador(0).Value
            .MoveNext
            m_sSaludo = Ador(0).Value
            .MoveNext
            m_sUsuario = Ador(0).Value
            ' carga array estados proceso
            For i = 1 To 13
                .MoveNext
                m_sEstados(i) = Ador(0).Value '15 - 26
            Next
            
            .MoveNext
            m_sCaptionsGrid(8) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(9) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(10) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(11) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(12) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(13) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(14) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(15) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(16) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(17) = Ador(0).Value
            Ador.MoveNext
            m_sCaptionsGrid(18) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(19) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(20) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(21) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(22) = Ador(0).Value
            .MoveNext
            m_sCaptionsGrid(23) = Ador(0).Value
            
            .MoveNext
            m_stxtToolTipP = Ador(0).Value  '44 N�mero de proveedores del proceso
            .MoveNext
            m_stxtToolTipO = Ador(0).Value  '45 N�mero de proveedores que han presentando ofertas
            .MoveNext
            m_stxtToolTipW = Ador(0).Value  '46 N�mero de proveedores que han ofertado a trav�s del portal
            
            .MoveNext
            optTodos.caption = Ador(0).Value '47 Todos
            .MoveNext
            optDesde.caption = Ador(0).Value '48 Desde
            .MoveNext
            lblEquipo.caption = Ador(0).Value '49 Equipo
            .MoveNext
            lblComprador.caption = Ador(0).Value '50 Equipo
            .MoveNext
            chkResponsable.caption = Ador(0).Value '51 Responsable
            .MoveNext
            lblMaterial.caption = Ador(0).Value '52 Material:
            .MoveNext
            lblUO.caption = Ador(0).Value '53 Unidad org:
            .MoveNext
            lblPresup.caption = Ador(0).Value '54 Presupuestos:
            .MoveNext
            m_stxtOcultar = Ador(0).Value '55 Ocultar
            lblMostraFiltro.caption = Ador(0).Value
            lblMostrarMenu.caption = Ador(0).Value
            lblOcultarMenu.caption = Ador(0).Value
            .MoveNext
            m_stxtMostrar = Ador(0).Value '56 Mostrar
            lblMostrarMenu.caption = Ador(0).Value
            
            For i = 1 To 12
                .MoveNext
                m_sIdiMeses(i) = Ador(0).Value
            Next i
            .MoveNext
            m_sCaptionsGrid(24) = Ador(0).Value 'P
            .MoveNext
            m_sCaptionsGrid(25) = Ador(0).Value 'O
            .MoveNext
            m_sCaptionsGrid(26) = Ador(0).Value 'W
            .MoveNext
            m_sAdjDir(0) = Ador(0).Value 'No
            .MoveNext
            m_sAdjDir(1) = Ador(0).Value 'Si
            .MoveNext
            m_slblProgreso = Ador(0).Value 'Exportando visor a excel
            .MoveNext
            m_sMensajeExito = Ador(0).Value ' La exportacion a excel ha finalizado con �xito
            .MoveNext
            m_sNombreExcel = Ador(0).Value 'Nombre de la hoja excel
            .MoveNext
            m_sCaptionsGrid(27) = Ador(0).Value '(Fec.Val.Apertura)
            .MoveNext
            m_sCaptionsGrid(28) = Ador(0).Value '(%Ahorro)
            .MoveNext
            m_sCaptionsGrid(29) = Ador(0).Value '(Cambio)
            .MoveNext
            m_sCaptionsGrid(30) = Ador(0).Value '(GS->ERP)
            .MoveNext
            m_sEstadoIntegracion(0) = Ador(0).Value  'Pendiente
            .MoveNext
            m_sEstadoIntegracion(1) = Ador(0).Value     'Parcial. En proceso
            .MoveNext
            m_sEstadoIntegracion(2) = Ador(0).Value    'Parcial. OK
            .MoveNext
            m_sEstadoIntegracion(3) = Ador(0).Value    'Parcial. Incorrecto
            .MoveNext
            m_sCaptionsGrid(31) = Ador(0).Value 'R
            .MoveNext
            m_sCaptionsGrid(32) = Ador(0).Value 'Solic vinculada
            .MoveNext
            m_sEstadoIntegracion(4) = Ador(0).Value     'Total. En proceso
            .MoveNext
            m_sEstadoIntegracion(5) = Ador(0).Value    'Total. OK
            .MoveNext
            m_sEstadoIntegracion(6) = Ador(0).Value    'Total. Incorrecto
            .MoveNext
            lblDenominacion.caption = Ador(0).Value & ":"   'Denominaci�n
            .MoveNext
            msMsgMinMax = Ador(0).Value 'Mensaje de fechas baremo entre fechas para las comprobaciones de atributos
            .MoveNext
            m_sIdiMostrarAtrBuscar = Ador(0).Value
            .MoveNext
            sdbgAtrBuscar.Columns("COD").caption = Ador(0).Value
            Ador.MoveNext
            sdbgAtrBuscar.Columns("OPER").caption = Ador(0).Value
            Ador.MoveNext
            sdbgAtrBuscar.Columns("VALOR").caption = Ador(0).Value
            .MoveNext
            m_sIdiOcultarAtrBuscar = Ador(0).Value
            .MoveNext
            cmdRestaurar.caption = Ador(0).Value
            .MoveNext
            cmdLimpiar.caption = Ador(0).Value
            .MoveNext
            sdbgAtrBuscar.Columns("AMBITO").caption = Ador(0).Value
            .MoveNext
            sdbddAmbito.AddItem TAtributoAmbito.ProcesoAtr & Chr(m_lSeparador) & Ador(0).Value
            .MoveNext
            sdbddAmbito.AddItem TAtributoAmbito.GrupoProcesoAtr & Chr(m_lSeparador) & Ador(0).Value
            .MoveNext
            sdbddAmbito.AddItem TAtributoAmbito.ItemProcesoAtr & Chr(m_lSeparador) & Ador(0).Value
            .Close
        End With
    End If
    Label4(0).caption = m_sIdiOcultarAtrBuscar
    Set Ador = Nothing
    
End Sub

Public Sub ComprobarNuevasOfertas()
    Dim dFechaD As Date
    Dim dFechaH As Date
    
    dFechaD = DateAdd("d", -gParametrosInstalacion.giBuzonPeriodo, Date)
    dFechaH = Date
    
    Set Ador = oGestorInformes.DevolVerOfertasRecibidasDesde(dFechaD, dFechaH, , , , , , 0, True, m_bRMatBuzOfe, m_bRAsigBuzOfe, m_bREqpAsigBuzOfe, m_bRCompRespBuzOfe, , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, TipoOrdenacionBuzon.OrdPorProve, , m_bRUsuAperBuzOfe, basOptimizacion.gvarCodUsuario, m_bRUsuUonBuzOfe, m_bRUsuDepBuzOfe, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario)
        
    If Ador Is Nothing Then
        PicNuevas.Visible = False
        Exit Sub
    Else
        PicNuevas.Visible = True
    End If
    
    Ador.Close
    Set Ador = Nothing
End Sub

Private Sub chkResponsable_Click()
    If m_bCargandoFiltro = False Then
        If chkResponsable.Value = vbChecked And sdbcEquipo.Visible = True And sdbcEquipo.Text = "" Then
            m_bCargandoFiltro = True
            chkResponsable.Value = vbUnchecked
            m_bCargandoFiltro = False
            Exit Sub
        End If

        g_oConfVisor.HayCambios = True
    End If
End Sub

Private Sub cmdBuscarMat_Click()
    Dim oFrmSelMat As frmSELMATMultiple
    
    'Abrir la pantalla de selecci�n m�ltiple de material
    Set oFrmSelMat = New frmSELMATMultiple
    oFrmSelMat.m_bModif = True
    oFrmSelMat.SeleccionSimple = True
    oFrmSelMat.RestMatComp = m_bRMat
    Set oFrmSelMat.oGruposMN1Seleccionados = m_oGMN1Seleccionados
    Set oFrmSelMat.oGruposMN2Seleccionados = m_oGMN2Seleccionados
    Set oFrmSelMat.oGruposMN3Seleccionados = m_oGMN3Seleccionados
    Set oFrmSelMat.oGruposMN4Seleccionados = m_oGMN4Seleccionados
    oFrmSelMat.sOrigen = "frmEST"
    oFrmSelMat.Show vbModal
    
    Set m_oGMN1Seleccionados = oFrmSelMat.oGruposMN1Seleccionados
    Set m_oGMN2Seleccionados = oFrmSelMat.oGruposMN2Seleccionados
    Set m_oGMN3Seleccionados = oFrmSelMat.oGruposMN3Seleccionados
    Set m_oGMN4Seleccionados = oFrmSelMat.oGruposMN4Seleccionados
    
    If Not m_oGMN1Seleccionados Is Nothing Then
        If m_oGMN1Seleccionados.Count > 0 Then m_oGMN1Seleccionados.CargarDenGMN1s oUsuarioSummit.idioma
    End If
    If Not m_oGMN2Seleccionados Is Nothing Then
        If m_oGMN2Seleccionados.Count > 0 Then m_oGMN2Seleccionados.CargarDenGMN2s oUsuarioSummit.idioma
    End If
    If Not m_oGMN3Seleccionados Is Nothing Then
        If m_oGMN3Seleccionados.Count > 0 Then m_oGMN3Seleccionados.CargarDenGMN3s oUsuarioSummit.idioma
    End If
    If Not m_oGMN4Seleccionados Is Nothing Then
        If m_oGMN4Seleccionados.Count > 0 Then m_oGMN4Seleccionados.CargarDenGMN4s oUsuarioSummit.idioma
    End If
    
    Unload oFrmSelMat
    Set oFrmSelMat = Nothing
    
    'Mostrar la nueva selecci�n
    MostrarMatSeleccionado
End Sub

Private Sub cmdBuscarPres_Click()
    Dim iNumPresup As Integer
    
    'Si el sistema est� configurado para tranajar con varios presupuestos muestra un men� para seleccionar el presupuesto.
    'Si solo trabaja con uno abre directamente la pantalla de selecci�n de ese pres.
    If gParametrosGenerales.gbUsarPres1 = False And gParametrosGenerales.gbUsarPres2 = False And gParametrosGenerales.gbUsarPres3 = False And gParametrosGenerales.gbUsarPres4 = False Then Exit Sub
    
    MDI.mnuVisorPresup(0).Visible = True
    MDI.mnuVisorPresup(1).Visible = True
    MDI.mnuVisorPresup(2).Visible = True
    MDI.mnuVisorPresup(3).Visible = True
    
    iNumPresup = 4
    If gParametrosGenerales.gbUsarPres1 = False Then
        iNumPresup = iNumPresup - 1
        MDI.mnuVisorPresup(0).Visible = False
    End If
    
    If gParametrosGenerales.gbUsarPres2 = False Then
        MDI.mnuVisorPresup(1).Visible = False
        iNumPresup = iNumPresup - 1
    End If
    
    If gParametrosGenerales.gbUsarPres3 = False Then
        MDI.mnuVisorPresup(2).Visible = False
        iNumPresup = iNumPresup - 1
    End If
    If gParametrosGenerales.gbUsarPres4 = False Then
        MDI.mnuVisorPresup(3).Visible = False
        iNumPresup = iNumPresup - 1
    End If
    
    If iNumPresup = 1 Then
        If gParametrosGenerales.gbUsarPres1 = True Then
            frmSELPresAnuUON.sOrigen = "frmEST1"
            frmSELPresAnuUON.g_iTipoPres = 1
            frmSELPresAnuUON.Show 1
            
        ElseIf gParametrosGenerales.gbUsarPres2 = True Then
            frmSELPresAnuUON.sOrigen = "frmEST2"
            frmSELPresAnuUON.g_iTipoPres = 2
            frmSELPresAnuUON.Show 1
            
        ElseIf gParametrosGenerales.gbUsarPres3 = True Then
            frmSELPresUO.sOrigen = "frmEST3"
            frmSELPresUO.g_iTipoPres = 3
            Screen.MousePointer = vbNormal
            frmSELPresUO.Show 1
            
        ElseIf gParametrosGenerales.gbUsarPres4 = True Then
            frmSELPresUO.sOrigen = "frmEST4"
            frmSELPresUO.g_iTipoPres = 4
            frmSELPresUO.Show 1
        End If
        
    Else
        PopupMenu MDI.mnuPOPUPVisorPres
    End If
End Sub

Private Sub cmdBuscarUO_Click()
    frmSELUO.sOrigen = "frmEST"
    frmSELUO.bRUO = m_bRUsuUON
    frmSELUO.bRPerfUON = m_bRPerfUON
    frmSELUO.Show vbModal
End Sub

Private Sub cmdCerrar_Click()
  
        Unload Me

End Sub

''' <summary>
''' Se generara un excel con el contenido del visor de procesos.
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdExcel_Click()
    Dim sTemp, sTemp1 As String
    Dim fso As Scripting.FileSystemObject
    Dim oFile As File
    Dim lInit As Long
    Dim sFile As String
    Dim sFileTitle As String
    Dim arrVisor() As Variant
    Dim i As Integer
    Dim j As Integer
    Dim adoComm As ADODB.Command
    Dim oExcelAdoConn As ADODB.Connection
    Dim sConnect As String
    Dim sSQL As String
    Dim bDisplayAlerts As Boolean
    Dim appexcel As Object
    Dim wkb  As Object
    Dim oSheet As Object
    Dim adoVisor As New ADODB.Recordset
    Dim oAtrBuscar As CAtributosBuscar
    adoVisor.CursorLocation = adUseClient
    
    On Error GoTo ERROR:

    sTemp = DevolverPathFichTemp & "VisorProcesos.xls"
    Set fso = New Scripting.FileSystemObject
    fso.CopyFile App.Path & "\PlantillaExport.xls", sTemp
    Set oFile = fso.GetFile(sTemp)
    If oFile.Attributes And 1 Then
        oFile.Attributes = oFile.Attributes Xor 1
    End If
    Set oFile = Nothing
    'Coleccion que recoge los atributos por los que se quiere filtrar y el ambito en el que se quiere que se cumplan
    Set oAtrBuscar = MoverGridAtrBuscarAColleccion(sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0))
    Set adoVisor = oGestorInformes.DevolVerProcesosVisor(FiltroProcesosVisor(True), m_sEstadoIntegracion(), oAtrBuscar)

    If adoVisor Is Nothing Then
        oMensajes.NoHayDatos
        Timer1.Enabled = False
        Exit Sub
    Else
        ReDim arrVisor(sdbgProcesos.Rows + 1, sdbgProcesos.Columns.Count)
        
        If sdbgProcesos.Columns("ADJDIR").Visible Then arrVisor(0, g_oConfVisor.AdjDirpos) = sdbgProcesos.Columns("ADJDIR").Name
        If sdbgProcesos.Columns("AHORRO").Visible Then arrVisor(0, g_oConfVisor.AhorroPos) = sdbgProcesos.Columns("AHORRO").Name
        If sdbgProcesos.Columns("ANYO").Visible Then arrVisor(0, g_oConfVisor.AnyoPos) = adoVisor.Fields("ANYO").Name
        If sdbgProcesos.Columns("CONS").Visible Then arrVisor(0, g_oConfVisor.ConsumidoPos) = adoVisor.Fields("CONS").Name
        If sdbgProcesos.Columns("DESCR").Visible Then arrVisor(0, g_oConfVisor.Descrpos) = adoVisor.Fields("DESCR").Name
        If sdbgProcesos.Columns("DESTDEN").Visible Then arrVisor(0, g_oConfVisor.DestinoDenpos) = adoVisor.Fields("DESTDEN").Name
        If sdbgProcesos.Columns("DEST").Visible Then arrVisor(0, g_oConfVisor.Destinopos) = adoVisor.Fields("DEST").Name
        If sdbgProcesos.Columns("EST").Visible Then arrVisor(0, g_oConfVisor.EstadoPos) = adoVisor.Fields("EST").Name
        If sdbgProcesos.Columns("FECAPE").Visible Then arrVisor(0, g_oConfVisor.FecApepos) = adoVisor.Fields("FECAPE").Name
        If sdbgProcesos.Columns("FECCIERRE").Visible Then arrVisor(0, g_oConfVisor.FecCierrePos) = adoVisor.Fields("FECCIERRE").Name
        If sdbgProcesos.Columns("FECFIN").Visible Then arrVisor(0, g_oConfVisor.FecFinPos) = adoVisor.Fields("FECFIN").Name
        If sdbgProcesos.Columns("FECINI").Visible Then arrVisor(0, g_oConfVisor.FecIniPos) = adoVisor.Fields("FECINI").Name
        If sdbgProcesos.Columns("FECLIMOFE").Visible Then arrVisor(0, g_oConfVisor.FecLimOfepos) = adoVisor.Fields("FECLIMOFE").Name
        If sdbgProcesos.Columns("FECNEC").Visible Then arrVisor(0, g_oConfVisor.FecNecPos) = adoVisor.Fields("FECNEC").Name
        If sdbgProcesos.Columns("FECPRES").Visible Then arrVisor(0, g_oConfVisor.FecPresPos) = adoVisor.Fields("FECPRES").Name
        If sdbgProcesos.Columns("GMN1").Visible Then arrVisor(0, g_oConfVisor.GMN1pos) = adoVisor.Fields("GMN1").Name
        If sdbgProcesos.Columns("IMP").Visible Then arrVisor(0, g_oConfVisor.ImportePos) = adoVisor.Fields("IMP").Name
        If sdbgProcesos.Columns("MON").Visible Then arrVisor(0, g_oConfVisor.Monedapos) = adoVisor.Fields("MON").Name
        If sdbgProcesos.Columns("O").Visible Then arrVisor(0, g_oConfVisor.Opos) = adoVisor.Fields("O").Name
        If sdbgProcesos.Columns("PAGO").Visible Then arrVisor(0, g_oConfVisor.Pagopos) = adoVisor.Fields("PAGO").Name
        If sdbgProcesos.Columns("PRES").Visible Then arrVisor(0, g_oConfVisor.PresPos) = adoVisor.Fields("PRES").Name
        If sdbgProcesos.Columns("COD").Visible Then arrVisor(0, g_oConfVisor.Procepos) = adoVisor.Fields("COD").Name
        If sdbgProcesos.Columns("PROVEDEN").Visible Then arrVisor(0, g_oConfVisor.ProveedorDenpos) = adoVisor.Fields("PROVEDEN").Name
        If sdbgProcesos.Columns("PROVE").Visible Then arrVisor(0, g_oConfVisor.ProveedorPos) = adoVisor.Fields("PROVE").Name
        If sdbgProcesos.Columns("P").Visible Then arrVisor(0, g_oConfVisor.Ppos) = adoVisor.Fields("P").Name
        If sdbgProcesos.Columns("RESP").Visible Then arrVisor(0, g_oConfVisor.Responsablepos) = adoVisor.Fields("RESP").Name
        If sdbgProcesos.Columns("W").Visible Then arrVisor(0, g_oConfVisor.Wpos) = adoVisor.Fields("W").Name
        If sdbgProcesos.Columns("R").Visible Then arrVisor(0, g_oConfVisor.Rpos) = adoVisor.Fields("R").Name
        If sdbgProcesos.Columns("FECVALAPERTURA").Visible Then arrVisor(0, g_oConfVisor.FecValAperturapos) = adoVisor.Fields("FECVALAPERTURA").Name
        If sdbgProcesos.Columns("PORCENTAJEAHORRO").Visible Then arrVisor(0, g_oConfVisor.PorcentajeAhorropos) = adoVisor.Fields("PORCENTAJEAHORRO").Name
        If sdbgProcesos.Columns("CAMBIO").Visible Then arrVisor(0, g_oConfVisor.Cambiopos) = adoVisor.Fields("CAMBIO").Name
        If sdbgProcesos.Columns("GSTOERP").Visible Then arrVisor(0, g_oConfVisor.gsToErpPos) = adoVisor.Fields("GSTOERP").Name
        If sdbgProcesos.Columns("VINCULADA").Visible Then arrVisor(0, g_oConfVisor.SolicVinculadaPos) = adoVisor.Fields("VINCULADA").Name
                            
        For i = 1 To adoVisor.RecordCount
            If sdbgProcesos.Columns("ADJDIR").Visible Then arrVisor(i, g_oConfVisor.AdjDirpos) = adoVisor.Fields("ADJDIR").Value
            If sdbgProcesos.Columns("AHORRO").Visible Then arrVisor(i, g_oConfVisor.AhorroPos) = adoVisor.Fields("AHORRO").Value
            If sdbgProcesos.Columns("ANYO").Visible Then arrVisor(i, g_oConfVisor.AnyoPos) = adoVisor.Fields("ANYO").Value
            If sdbgProcesos.Columns("CONS").Visible Then arrVisor(i, g_oConfVisor.ConsumidoPos) = adoVisor.Fields("CONS").Value
            If sdbgProcesos.Columns("DESCR").Visible Then arrVisor(i, g_oConfVisor.Descrpos) = adoVisor.Fields("DESCR").Value
            If sdbgProcesos.Columns("DESTDEN").Visible Then arrVisor(i, g_oConfVisor.DestinoDenpos) = adoVisor.Fields("DESTDEN").Value
            If sdbgProcesos.Columns("DEST").Visible Then arrVisor(i, g_oConfVisor.Destinopos) = adoVisor.Fields("DEST").Value
            If sdbgProcesos.Columns("EST").Visible Then arrVisor(i, g_oConfVisor.EstadoPos) = adoVisor.Fields("EST").Value
            If sdbgProcesos.Columns("FECAPE").Visible Then arrVisor(i, g_oConfVisor.FecApepos) = adoVisor.Fields("FECAPE").Value
            If sdbgProcesos.Columns("FECCIERRE").Visible Then arrVisor(i, g_oConfVisor.FecCierrePos) = adoVisor.Fields("FECCIERRE").Value
            If sdbgProcesos.Columns("FECFIN").Visible Then arrVisor(i, g_oConfVisor.FecFinPos) = adoVisor.Fields("FECFIN").Value
            If sdbgProcesos.Columns("FECINI").Visible Then arrVisor(i, g_oConfVisor.FecIniPos) = adoVisor.Fields("FECINI").Value
            If sdbgProcesos.Columns("FECLIMOFE").Visible Then arrVisor(i, g_oConfVisor.FecLimOfepos) = adoVisor.Fields("FECLIMOFE").Value
            If sdbgProcesos.Columns("FECNEC").Visible Then arrVisor(i, g_oConfVisor.FecNecPos) = adoVisor.Fields("FECNEC").Value
            If sdbgProcesos.Columns("FECPRES").Visible Then arrVisor(i, g_oConfVisor.FecPresPos) = adoVisor.Fields("FECPRES").Value
            If sdbgProcesos.Columns("GMN1").Visible Then arrVisor(i, g_oConfVisor.GMN1pos) = adoVisor.Fields("GMN1").Value
            If sdbgProcesos.Columns("IMP").Visible Then arrVisor(i, g_oConfVisor.ImportePos) = adoVisor.Fields("IMP").Value
            If sdbgProcesos.Columns("MON").Visible Then arrVisor(i, g_oConfVisor.Monedapos) = adoVisor.Fields("MON").Value
            If sdbgProcesos.Columns("O").Visible Then arrVisor(i, g_oConfVisor.Opos) = adoVisor.Fields("O").Value
            If sdbgProcesos.Columns("PAGO").Visible Then arrVisor(i, g_oConfVisor.Pagopos) = adoVisor.Fields("PAGO").Value
            If sdbgProcesos.Columns("PRES").Visible Then arrVisor(i, g_oConfVisor.PresPos) = adoVisor.Fields("PRES").Value
            If sdbgProcesos.Columns("COD").Visible Then arrVisor(i, g_oConfVisor.Procepos) = adoVisor.Fields("COD").Value
            If sdbgProcesos.Columns("PROVEDEN").Visible Then arrVisor(i, g_oConfVisor.ProveedorDenpos) = adoVisor.Fields("PROVEDEN").Value
            If sdbgProcesos.Columns("PROVE").Visible Then arrVisor(i, g_oConfVisor.ProveedorPos) = adoVisor.Fields("PROVE").Value
            If sdbgProcesos.Columns("P").Visible Then arrVisor(i, g_oConfVisor.Ppos) = adoVisor.Fields("P").Value
            If sdbgProcesos.Columns("RESP").Visible Then arrVisor(i, g_oConfVisor.Responsablepos) = adoVisor.Fields("RESP").Value
            If sdbgProcesos.Columns("W").Visible Then arrVisor(i, g_oConfVisor.Wpos) = adoVisor.Fields("W").Value
            If sdbgProcesos.Columns("R").Visible Then arrVisor(i, g_oConfVisor.Rpos) = adoVisor.Fields("R").Value
            If sdbgProcesos.Columns("FECVALAPERTURA").Visible Then arrVisor(i, g_oConfVisor.FecValAperturapos) = adoVisor.Fields("FECVALAPERTURA").Value
            If sdbgProcesos.Columns("PORCENTAJEAHORRO").Visible Then arrVisor(i, g_oConfVisor.PorcentajeAhorropos) = adoVisor.Fields("PORCENTAJEAHORRO").Value
            If sdbgProcesos.Columns("CAMBIO").Visible Then arrVisor(i, g_oConfVisor.Cambiopos) = adoVisor.Fields("CAMBIO").Value
            If sdbgProcesos.Columns("GSTOERP").Visible Then arrVisor(i, g_oConfVisor.gsToErpPos) = adoVisor.Fields("GSTOERP").Value
            If sdbgProcesos.Columns("VINCULADA").Visible Then arrVisor(i, g_oConfVisor.SolicVinculadaPos) = adoVisor.Fields("VINCULADA").Value
            
            adoVisor.MoveNext
        Next

        'Se carga el array en el excel
        Set oExcelAdoConn = New ADODB.Connection
        sConnect = "Provider=MSDASQL.1;" _
                 & "Extended Properties=""DBQ=" & sTemp & ";" _
                 & "Driver={Microsoft Excel Driver (*.xls)};" _
                 & "FIL=excel 8.0;" _
                 & "ReadOnly=0;" _
                 & "UID=admin;"""

        oExcelAdoConn.Open sConnect

        Set adoComm = New ADODB.Command
        Set adoComm.ActiveConnection = oExcelAdoConn
        
        sSQL = " CREATE TABLE [" & m_sNombreExcel & "] ("
        For j = 0 To UBound(arrVisor, 2)
            If arrVisor(0, j) <> "" Then
                Select Case j
                Case g_oConfVisor.AhorroPos, g_oConfVisor.ConsumidoPos, g_oConfVisor.ImportePos, g_oConfVisor.PresPos, g_oConfVisor.PorcentajeAhorropos, g_oConfVisor.Cambiopos
                    sSQL = sSQL & arrVisor(0, j) & " number NULL,"
                Case g_oConfVisor.FecApepos, g_oConfVisor.FecCierrePos, g_oConfVisor.FecFinPos, g_oConfVisor.FecIniPos, g_oConfVisor.FecNecPos, g_oConfVisor.FecPresPos, g_oConfVisor.FecValAperturapos
                    sSQL = sSQL & arrVisor(0, j) & " date NULL,"
                Case g_oConfVisor.FecLimOfepos
                    sSQL = sSQL & arrVisor(0, j) & " datetime NULL,"
                Case Else
                    sSQL = sSQL & arrVisor(0, j) & " memo,"
                End Select
            End If
        Next
        sSQL = Left(sSQL, Len(sSQL) - 1) & ")" 'para quitarle la ,
        oExcelAdoConn.Execute sSQL
        lInit = 0
        
        frmProgreso.ProgressBar.Value = 0
        frmProgreso.ProgressBar.Max = adoVisor.RecordCount + 1
        frmProgreso.lblTransferir = m_slblProgreso
        frmProgreso.Top = 3000
        frmProgreso.Left = 1500
        frmProgreso.Show
        DoEvents
        
        While lInit < adoVisor.RecordCount
           For j = 1 To UBound(arrVisor, 1) - 1
                sSQL = " INSERT INTO [" & m_sNombreExcel & "$] values("
                For i = 0 To UBound(arrVisor, 2)
                    If arrVisor(0, i) <> "" Then
                       
                        Select Case arrVisor(0, i)
                        Case "ADJDIR"
                            sSQL = sSQL & "'" & IIf(arrVisor(j, g_oConfVisor.AdjDirpos) = 1, m_sAdjDir(1), m_sAdjDir(0)) & "',"
                        Case "AHORRO"
                            sSQL = sSQL & DblToSQLNullFloat(garSimbolos, arrVisor(j, g_oConfVisor.AhorroPos)) & ","
                        Case "ANYO"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.AnyoPos))) & "',"
                        Case "CONS"
                             sSQL = sSQL & DblToSQLNullFloat(garSimbolos, arrVisor(j, g_oConfVisor.ConsumidoPos)) & ","
                        Case "DESCR"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.Descrpos))) & "',"
                        Case "DESTDEN"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.DestinoDenpos))) & "',"
                        Case "DEST"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.Destinopos))) & "',"
                        Case "EST"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.EstadoPos))) & "',"
                        Case "FECAPE"
                            If IsNull(arrVisor(j, g_oConfVisor.FecApepos)) Then
                                sSQL = sSQL & "NULL,"
                            Else
                                sSQL = sSQL & "'" & CDate(arrVisor(j, g_oConfVisor.FecApepos)) & "',"
                            End If
                        Case "FECCIERRE"
                            If IsNull(arrVisor(j, g_oConfVisor.FecCierrePos)) Then
                                sSQL = sSQL & "NULL,"
                            Else
                                sSQL = sSQL & "'" & CDate(arrVisor(j, g_oConfVisor.FecCierrePos)) & "',"
                            End If
                        Case "FECFIN"
                            If IsNull(arrVisor(j, g_oConfVisor.FecFinPos)) Then
                                sSQL = sSQL & "NULL,"
                            Else
                                sSQL = sSQL & "'" & CDate(arrVisor(j, g_oConfVisor.FecFinPos)) & "',"
                            End If
                        Case "FECINI"
                            If IsNull(arrVisor(j, g_oConfVisor.FecIniPos)) Then
                                sSQL = sSQL & "NULL,"
                            Else
                                sSQL = sSQL & "'" & CDate(arrVisor(j, g_oConfVisor.FecIniPos)) & "',"
                            End If
                        Case "FECLIMOFE"
                            If IsNull(arrVisor(j, g_oConfVisor.FecLimOfepos)) Then
                                sSQL = sSQL & "NULL,"
                            Else
                                sSQL = sSQL & "'" & CDate(arrVisor(j, g_oConfVisor.FecLimOfepos)) & "',"
                            End If
                        Case "FECNEC"
                            If IsNull(arrVisor(j, g_oConfVisor.FecNecPos)) Then
                                sSQL = sSQL & "NULL,"
                            Else
                                sSQL = sSQL & "'" & CDate(arrVisor(j, g_oConfVisor.FecNecPos)) & "',"
                            End If
                        Case "FECPRES"
                            If IsNull(arrVisor(j, g_oConfVisor.FecPresPos)) Then
                                sSQL = sSQL & "NULL,"
                            Else
                                sSQL = sSQL & "'" & CDate(arrVisor(j, g_oConfVisor.FecPresPos)) & "',"
                            End If
                        Case "GMN1"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.GMN1pos))) & "',"
                        Case "IMP"
                            sSQL = sSQL & DblToSQLNullFloat(garSimbolos, arrVisor(j, g_oConfVisor.ImportePos)) & ","
                        Case "MON"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.Monedapos))) & "',"
                        Case "O"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.Opos))) & "',"
                        Case "PAGO"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.Pagopos))) & "',"
                        Case "PRES"
                            sSQL = sSQL & DblToSQLNullFloat(garSimbolos, arrVisor(j, g_oConfVisor.PresPos)) & ","
                        Case "COD"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.Procepos))) & "',"
                        Case "PROVEDEN"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.ProveedorDenpos))) & "',"
                        Case "PROVE"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.ProveedorPos))) & "',"
                        Case "P"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.Ppos))) & "',"
                        Case "RESP"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.Responsablepos))) & "',"
                        Case "W"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.Wpos))) & "',"
                        Case "R"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.Rpos))) & "',"
                        Case "FECVALAPERTURA"
                            If IsNull(arrVisor(j, g_oConfVisor.FecValAperturapos)) Then
                                sSQL = sSQL & "NULL,"
                            Else
                                sSQL = sSQL & "'" & CDate(arrVisor(j, g_oConfVisor.FecValAperturapos)) & "',"
                            End If
                        Case "PORCENTAJEAHORRO"
                            sSQL = sSQL & DblToSQLNullFloat(garSimbolos, arrVisor(j, g_oConfVisor.PorcentajeAhorropos)) & ","
                        Case "CAMBIO"
                            sSQL = sSQL & DblToSQLNullFloat(garSimbolos, arrVisor(j, g_oConfVisor.Cambiopos)) & ","
                        Case "GSTOERP"
                            sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, g_oConfVisor.gsToErpPos))) & "',"
                        Case "VINCULADA"
                            sSQL = sSQL & "'" & IIf(arrVisor(j, g_oConfVisor.SolicVinculadaPos) = 1, m_sAdjDir(1), m_sAdjDir(0)) & "',"
                        End Select
                    End If
                Next
                sSQL = Left(sSQL, Len(sSQL) - 1) & ")" 'para quitarle la ,
                adoComm.CommandText = sSQL
                adoComm.CommandType = adCmdText
                adoComm.Prepared = True
                adoComm.Execute
                lInit = lInit + 1
                frmProgreso.ProgressBar.Value = frmProgreso.ProgressBar.Value + 1
            Next
        Wend

        Unload frmProgreso

        Set adoComm = Nothing
        
        oExcelAdoConn.Close
        Set oExcelAdoConn = Nothing

        'Dar formato al excel con los anchos de las columnas definidos en g_oConfVisor
        Set appexcel = CreateObject("Excel.Application")
        Set wkb = appexcel.Workbooks.Add(sTemp)
        i = 1
        While wkb.Sheets(m_sNombreExcel).Cells(i).Value <> ""
            wkb.Sheets(m_sNombreExcel).Cells(i).Font.Bold = True
            Select Case wkb.Sheets(m_sNombreExcel).Cells(i).Value
                Case "ANYO"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(0)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.AnyoWidth / 100
                Case "GMN1"
                   wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(1)
                   wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.GMN1Width / 100
                Case "COD"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(2)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.ProceWidth / 100
                Case "DESCR"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(3)
                    wkb.Sheets(m_sNombreExcel).Cells(i).Columns.ColumnWidth = g_oConfVisor.DescrWidth / 100 + 5
                Case "EST"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(4)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.EstadoWidth / 100
                Case "RESP"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(5)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.ResponsableWidth / 100
                 Case "ADJDIR"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(6)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.AdjDirWidth / 100
                 Case "FECPRES"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(7)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.FecPresWidth / 100 + 3
                 Case "FECAPE"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(8)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.FecApeWidth / 100 + 3
                 Case "FECLIMOFE"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(9)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.FecLimOfeWidth / 100 + 3
                 Case "FECCIERRE"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(10)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.FecCierreWidth / 100 + 3
                 Case "FECINI"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(11)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.FecIniWidth / 100 + 3
                 Case "FECFIN"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(12)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.FecFinWidth / 100 + 3
                 Case "PRES"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(13)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.PresWidth / 100
                 Case "CONS"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(14)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.ConsumidoWidth / 100
                 Case "IMP"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(15)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.ImporteWidth / 100
                 Case "MON"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(16)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.MonedaWidth / 100
                 Case "DEST"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(17)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.DestinoWidth / 100
                 Case "DESTDEN"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(18)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.DestinoDenWidth / 100
                 Case "PAGO"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(19)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.PagoWidth / 100
                 Case "PROVE"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(20)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.ProveedorWidth / 100
                 Case "PROVEDEN"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(21)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.ProveedorDenWidth / 100
                 Case "AHORRO"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(22)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.AhorroWidth / 100
                 Case "FECNEC"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(23)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.FecNecWidth / 100
                 Case "P"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(24)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.PWidth / 100
                 Case "O"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(25)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.OWidth / 100
                 Case "W"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(26)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.WWidth / 100
                 Case "R"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(31)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.RWidth / 100
                Case "FECVALAPERTURA"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(27)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.FecValAperturaWidth / 100
                Case "PORCENTAJEAHORRO"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(28)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.PorcentajeAhorroWidth / 100
                Case "CAMBIO"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(29)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.CambioWidth / 100
                Case "GSTOERP"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(30)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.gsToErpWidth / 100
                Case "VINCULADA"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(32)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisor.SolicVinculadaWidth / 100
            End Select
            i = i + 1
        Wend
        
        'Le guardo con otro nombre porque al hacer cambios sale una ventana preguntando si se
        'van a guardar los cambios. Luego, cuando termine todo se borraran los dos excel temporales.
        sTemp1 = sTemp
        sTemp1 = Mid(sTemp, 1, Len(sTemp) - 4) & "1.xls"
        wkb.SaveAs sTemp1
         
        bDisplayAlerts = appexcel.DisplayAlerts
        appexcel.DisplayAlerts = False
                
        For Each oSheet In appexcel.worksheets
            If oSheet.Name <> m_sNombreExcel Then
                oSheet.Delete
                Exit For
            End If
        Next
        fso.DeleteFile sTemp
        wkb.SaveAs sTemp
        appexcel.DisplayAlerts = bDisplayAlerts
        appexcel.Quit
        Set appexcel = Nothing
       
        cmmdGenerarExcel.CancelError = True
        cmmdGenerarExcel.Filter = "Excel |*.xls"
        cmmdGenerarExcel.filename = "VisorProcesos.xls"
        cmmdGenerarExcel.ShowSave
        
        sFile = cmmdGenerarExcel.filename
        sFileTitle = cmmdGenerarExcel.FileTitle
        If sFileTitle = "" Then
            fso.DeleteFile sTemp
            fso.DeleteFile sTemp1
            Set fso = Nothing
            Exit Sub
        Else
            'Mostrar mensaje de proceso finalizado
            fso.CopyFile sTemp, sFile
            oMensajes.MensajeOKOnly m_sMensajeExito, TipoIconoMensaje.Information
            fso.DeleteFile sTemp
            fso.DeleteFile sTemp1
            Set fso = Nothing
            Exit Sub
        End If
    End If
  
    Exit Sub
ERROR:

    If err.Number = cdlCancel Then
        fso.DeleteFile sTemp
        fso.DeleteFile sTemp1
        Set fso = Nothing
        Exit Sub
    Else
        Unload frmProgreso
        oMensajes.MensajeOKOnly err.Description + vbCrLf + oMensajes.CargarTextoMensaje(905), Critical
        If sTemp <> "" Then
            fso.DeleteFile sTemp
        End If
        If sTemp1 <> "" Then
            fso.DeleteFile sTemp1
        End If
        Set fso = Nothing
        Set appexcel = Nothing
        Exit Sub
    End If
End Sub

Private Sub cmdLimpiarMat_Click()
    If txtMaterial.Text = "" Then Exit Sub
    
    txtMaterial.Text = ""
    
    Set m_oGMN1Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
    Set m_oGMN2Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
    Set m_oGMN3Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
    Set m_oGMN4Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
    
    g_oConfVisor.HayCambios = True
End Sub

Private Sub cmdLimpiarPres_Click()
    If txtPresup.Text = "" Then Exit Sub
    
    txtPresup.Text = ""
    
    m_vPresup1_1 = Null
    m_vPresup1_2 = Null
    m_vPresup1_3 = Null
    m_vPresup1_4 = Null
    m_vPresup2_1 = Null
    m_vPresup2_2 = Null
    m_vPresup2_3 = Null
    m_vPresup2_4 = Null
    m_vPresup3_1 = Null
    m_vPresup3_2 = Null
    m_vPresup3_3 = Null
    m_vPresup3_4 = Null
    m_vPresup4_1 = Null
    m_vPresup4_2 = Null
    m_vPresup4_3 = Null
    m_vPresup4_4 = Null
    m_sUON1Pres = ""
    m_sUON2Pres = ""
    m_sUON3Pres = ""

    g_oConfVisor.HayCambios = True
End Sub

Private Sub cmdLimpiarUO_Click()
    If txtUO.Text = "" Then Exit Sub
    
    txtUO.Text = ""
    
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    
    g_oConfVisor.HayCambios = True
End Sub

Private Sub cmdMaximizar_Click()

    Me.WindowState = vbMaximized
    cmdRestaurarVentana.Visible = True
    cmdMaximizar.Visible = False
        
End Sub

''' <summary>
''' Al pulsar el bot�n llamaremos a frmInternet para que nos muestre el listado de Microstrategy que corresponda seg�n el origen (guardado en el TAG del bot�n)
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en el bot�n cmdMicro; Tiempo m�ximo: 0</remarks>
Private Sub cmdMicro_Click()

With frmInternet
    .g_sOrigen = Me.Name
    .g_sNombre = cmdMicro.ToolTipText
    .g_sRuta = cmdMicro.Tag
    '.Show
    If MDI.ActiveForm Is Nothing Then
        Me.WindowState = vbNormal
    Else
        If MDI.ActiveForm.WindowState = vbMaximized Then
            Me.WindowState = vbMaximized
        Else
            Me.WindowState = vbNormal
        End If
    End If
    .SetFocus
End With

End Sub

Private Sub cmdMinimizar_Click()
    'Al restaurar habr� que ponerle los tamanyos de vbnormal
    
    '**************** Minimiza el form *********************
    Me.WindowState = vbNormal  '1� lo pasa a estado normal
    'porque sino no se puede cambiar el tamanyo
    
    Me.Height = 350
    Me.Width = 2400
    'Lo posiciona en el borde inferior derecho
    Me.Top = MDI.Height - 1110
    Me.Left = MDI.Width - Me.Width - 220
    
    
    pictureMinimizar.Visible = True
    Picture2.Visible = False  'el picture inferior
    
    'Cambia el top de los botones
    cmdCerrar.Top = 0
    cmdRestaurarVentana.Top = 0
    cmdMaximizar.Top = 0
    
    cmdCerrar.Left = Me.Width - 400
    cmdMaximizar.Left = Me.Width - 690
     
    cmdExcel.Left = Me.Width - 660
    PicNuevas.Left = cmdExcel - 200
        
    cmdRestaurarVentana.Left = Me.Width - 930
    cmdMinimizar.Left = Me.Width - 950
    
    lblSaludo.Width = cmdMinimizar.Left - 600 - 75
    
    cmdMaximizar.Visible = True
    cmdMinimizar.Visible = False
    cmdRestaurarVentana.Visible = True
    
    lblMinimizar.caption = m_sFrmTitulo
End Sub

Public Sub cmdRestaurar_Click()
    Restaurar
End Sub


Private Sub cmdRestaurarVentana_Click()
    Me.WindowState = vbNormal
    cmdRestaurarVentana.Visible = False
    cmdMaximizar.Visible = True
    
    '*********** Redimensiona el formulario ************
    Me.Height = 5670
    Me.Width = 11640

    Me.Left = 770
    Me.Top = 1435

    cmdCerrar.Left = Me.Width - 400
    cmdMaximizar.Left = Me.Width - 690
    cmdRestaurarVentana.Left = Me.Width - 690
    cmdMinimizar.Left = Me.Width - 950
    cmdExcel.Left = Me.Width - 685
    PicExclamacion.Left = cmdExcel.Left - 145
    PicNuevas.Left = PicExclamacion.Left - 345
End Sub

Private Sub Form_Activate()
    If Not m_bActivado Then
        m_bActivado = True
        If sdbgAtrBuscar.Rows > 0 Then
            sdbgAtrBuscar.MoveFirst
        End If
    End If
    If MDI.mnuProceEst.Checked = False Then
        Me.Visible = False
    End If
    
    'Es posible que la zona horaria haya cambiado en otra pantalla, actualizarla
    If oUsuarioSummit.TimeZone <> m_vTZHora Then
        m_vTZHora = oUsuarioSummit.TimeZone
    End If
End Sub

''' <summary>Evento que se lanza cuando se carga el formulario</summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde el evento; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 27/09/2011</revision>

Private Sub Form_Load()
    Dim vNumProce As Variant
    Dim i As Integer
    Dim oAtrBuscar As CAtributosBuscar
    m_bActivado = False
    PonerFieldSeparator Me
    
    SetFormTZ

    If m_adoRecordset.State <> adStateOpen Then
        m_adoRecordset.CursorLocation = adUseClient
    End If
    
    m_iDesde = TipoEstadoProceso.sinitems
    m_iHasta = TipoEstadoProceso.ParcialmenteCerrado
    
    'Inicializar var. para mat. seleccionados
    Set m_oGMN1Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
    Set m_oGMN2Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
    Set m_oGMN3Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
    Set m_oGMN4Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
        
    Me.Height = 5670
    Me.Width = 11640
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    If (MDI.ScaleHeight / 2 - Me.Height / 2) > 200 Then
        Me.Top = (MDI.ScaleHeight / 2 - Me.Height / 2) - 500
    End If
    Me.WindowState = vbMaximized
    
    m_bModomovimiento = False
    
    Screen.MousePointer = vbHourglass
    
    g_bSoloInvitado = False
    If oUsuarioSummit.EsInvitado Then
        If oUsuarioSummit.Acciones Is Nothing Then
            g_bSoloInvitado = True
        Else
            If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceConsulta)) Is Nothing Then
                g_bSoloInvitado = True
            End If
        End If
    End If
    
    'Multilenguaje
    CargarRecursos
    sdbgAtrBuscar.Columns("AMBITO").DropDownHwnd = sdbddAmbito.hWnd
    ConfigurarSeguridad
    
    Screen.MousePointer = vbNormal
    PicExclamacion.Visible = False
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
        PosibleNotificacionDeDespublicacion
    End If
    
    PosibleAlertaContrato
    
        
    Select Case oUsuarioSummit.Tipo
        Case TIpoDeUsuario.Administrador
            lblSaludo = m_sSaludo & ", " & m_sUsuario
        Case TIpoDeUsuario.comprador
            lblSaludo = m_sSaludo & ", " & oUsuarioSummit.Persona.nombre & " " & oUsuarioSummit.Persona.Apellidos
            ComprobarNuevasOfertas
            Timer1.Enabled = True
            m_iIntervalo = 0
        Case TIpoDeUsuario.Persona
            lblSaludo = m_sSaludo & ", " & oUsuarioSummit.Persona.nombre & " " & oUsuarioSummit.Persona.Apellidos
    End Select
            
    m_bMicrostrategy = gParametrosGenerales.gbListadosMicro And oGestorLisPerExternos.HayLisPerExternos(Me.Name)
    cmdMicro.Visible = m_bMicrostrategy
    
    If m_bMicrostrategy Then
        Dim Ador As ADODB.Recordset
        
        Set Ador = oGestorLisPerExternos.DevolverLisPerExterno(Me.Name, basPublic.gParametrosInstalacion.gIdioma)
        
        If Not Ador Is Nothing Then
            cmdMicro.Tag = Ador(0)
            cmdMicro.ToolTipText = Ador(1)
            Ador.Close
            Set Ador = Nothing
        End If
    End If
        
    m_dblTotal = 0
    m_dblTotalAbiertos = 0
    m_dblTotalCerrados = 0
    
    picMenu.Backcolor = RGB(128, 0, 0)
    sdbgMenu.Backcolor = RGB(128, 0, 0)
    picSeparator1.Backcolor = RGB(128, 0, 0)
    picSeparator2.Backcolor = RGB(128, 0, 0)
    picTotPend.Left = picTotPend.Left + 20
    picTotPend.Backcolor = RGB(128, 0, 0)
    picTotCerr.Backcolor = RGB(128, 0, 0)
    picTotCerr.Left = picTotPend.Left + 20
    picTot.Backcolor = RGB(128, 0, 0)
    picTot.Left = picTotPend.Left + 20
    picTotPend.Width = picTotPend.Width + 30
    
    CargarAnyosYMeses
    
    Set m_oEquipos = Nothing
    Set m_oEquipos = oFSGSRaiz.Generar_CEquipos
    
    'Si tiene activada la restricci�n de equipos no se visualizar� la combo de equipos
    If m_bREqp = True Then
        lblEquipo.Visible = False
        sdbcEquipo.Visible = False
        txtEquipo.Visible = True
        txtEquipo.Text = oUsuarioSummit.Persona.codEqp & " - " & oUsuarioSummit.Persona.DenEqp
        
        If m_bRAsig = False Then
            m_oEquipos.CargarTodosLosEquipos oUsuarioSummit.Persona.codEqp, , True
            Set m_oEqpSeleccionado = m_oEquipos.Item(CStr(oUsuarioSummit.Persona.codEqp))
        End If
    End If
    
    'si tiene activada la restricci�n de "Restringir a procesos asignados al comprador" no se visualizar� la combo de compradores ni la de equipos
    If m_bRAsig = True Then
        lblEquipo.Visible = False
        sdbcEquipo.Visible = False
        txtEquipo.Visible = True
        txtEquipo.Text = oUsuarioSummit.Persona.codEqp & " - " & oUsuarioSummit.Persona.DenEqp
        
        lblComprador.Visible = False
        sdbcComprador.Visible = False
        txtComprador.Visible = True
        txtComprador.Text = oUsuarioSummit.Persona.nombre & " " & oUsuarioSummit.Persona.Apellidos
     End If
     
    'Si el usuario tiene la restricci�n de procesos en los que es responsable solo mostrar� el check de responsable cuando tenga tb
    'la restricci�n de procesos abiertos por el usuario (ser�n adicionales)
    If m_bRCompResp = True Then
        If m_bRUsuAper = True Then
            chkResponsable.Visible = True
        Else
            chkResponsable.Visible = False
        End If
    End If
    
    
    'Si solo tiene la restricci�n de Procesos abiertos por el usuario no aparecer� la combo de UO
    'Si tiene la restricci�n de UO tampoco:
    If (m_bRUsuAper = True And m_bRAsig = False And m_bREqp = False And m_bRCompResp = False And m_bRMat = False And m_bRUsuUON = False And m_bRUsuDep = False) _
      Or (m_bRUsuUON = True And m_bRUsuDep = True) Then
        txtUO.Visible = False
        lblUO.Visible = False
        cmdLimpiarUO.Visible = False
        cmdBuscarUO.Visible = False
        
        lblMaterial.Top = 360
        txtMaterial.Top = 300
        lstMultiMaterial.Top = 300
        cmdBuscarMat.Top = 300
        cmdLimpiarMat.Top = 300
        
        lblPresup.Top = 750
        txtPresup.Top = 690
        cmdLimpiarPres.Top = 690
        cmdBuscarPres.Top = 690
    End If
    
    'Si el sistema no est� preparado para trabajar con presupuestos no aparecer� la combo de presupuestos:
    If gParametrosGenerales.gbUsarPres1 = False And gParametrosGenerales.gbUsarPres2 = False And gParametrosGenerales.gbUsarPres3 = False And gParametrosGenerales.gbUsarPres4 = False Then
        lblPresup.Visible = False
        txtPresup.Visible = False
        cmdLimpiarPres.Visible = False
        cmdBuscarPres.Visible = False
        
        lblMaterial.Top = 360
        txtMaterial.Top = 300
        lstMultiMaterial.Top = 300
        cmdBuscarMat.Top = 300
        cmdLimpiarMat.Top = 300
        
        lblUO.Top = 750
        txtUO.Top = 690
        cmdLimpiarUO.Top = 690
        cmdBuscarUO.Top = 690
    End If
    
    'abre un formulario invisible para contener el men� de configuraci�n.Lo hacemos as�,porque si ponemos el
    'men� en frmEST no se ver�a el men� del MDI.
    LockWindowUpdate Me.hWnd
    MDI.MostrarFormulario frmMenuEst, True
    frmMenuEst.Visible = False
    LockWindowUpdate 0&
    
    Screen.MousePointer = vbHourglass
    ConfigurarMenu
        
    CommonDialog1.FLAGS = cdlCCRGBInit
    
    CargarComboOperandos arOper, sdbddOper
    
    'Carga de BD la configuraci�n de los campos, el orden y el filtro
    CargarConfiguracionVisor
    
    'comprueba que los filtros sean correctos y los aplica
    ComprobarYAplicarFiltros
    'Coleccion que recoge los atributos por los que se quiere filtrar y el ambito en el que se quiere que se cumplan
    Set oAtrBuscar = MoverGridAtrBuscarAColleccion(sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0))
    Dim udtDevolVerProcesosVisor As TipoDevolVerProcesosVisor
    udtDevolVerProcesosVisor = FiltroProcesosVisor(False)
    vNumProce = oGestorInformes.DevolverNumeroDeProcesosEnCadaEstado(udtDevolVerProcesosVisor, oAtrBuscar)
    
    With sdbgMenu
        .MoveFirst
        For i = 0 To .Rows - 1
            If .Columns("ID").Value <> CTOTABIERTOS And .Columns("ID").Value <> CSEP1 And .Columns("ID").Value <> CSEP2 And .Columns("ID").Value <> CTOT And .Columns("ID").Value <> CTOTCERRADOS Then
                .Columns("NUMPROCE").Value = vNumProce(.Columns("ID").Value - 1)
                
                m_dblTotal = m_dblTotal + .Columns("NUMPROCE").Value
                
                If .Columns("ID").Value < 8 Then
                    m_dblTotalAbiertos = m_dblTotalAbiertos + .Columns("NUMPROCE").Value
                Else
                    m_dblTotalCerrados = m_dblTotalCerrados + .Columns("NUMPROCE").Value
                End If
                
            End If
            .MoveNext
        Next i
            
        'Mostrarvalor de total
        .Columns("NUMPROCE").Value = m_dblTotal
        
        'Mostrar valor para pendientes
        Dim bEncontrado As Boolean
        .MoveFirst
        i = 0
        While Not bEncontrado
            If .Columns("ID").Value = CTOTABIERTOS Then
                bEncontrado = True
                .Columns("NUMPROCE").Value = m_dblTotalAbiertos
                .Columns("MENU").CellStyleSet "Bold", i
                If m_bTotPend Then
                    picTotPend.Visible = True
                    picTotPend.Top = .RowHeight * i + .Top + 25 + Picture1.Height + frameFiltro.Height + picMostrarFiltro.Height
                End If
            End If
            
            i = i + 1
            .MoveNext
        Wend
        
        picSeparator1.Top = .RowHeight * i + .Top + Picture1.Height + frameFiltro.Height + picMostrarFiltro.Height + 15
        
        bEncontrado = False
        While Not bEncontrado
            If .Columns("ID").Value = CTOTCERRADOS Then
                bEncontrado = True
                .Columns("NUMPROCE").Value = m_dblTotalCerrados
                .Columns("MENU").CellStyleSet "Bold", i
                If m_bTotCerr Then
                    picTotCerr.Visible = True
                    picTotCerr.Top = .RowHeight * i + .Top + 25 + Picture1.Height + frameFiltro.Height + picMostrarFiltro.Height
                End If
                
            End If
            
            i = i + 1
            .MoveNext
        Wend
        
        picSeparator2.Top = .RowHeight * i + .Top + Picture1.Height + frameFiltro.Height + picMostrarFiltro.Height + 15
        
        bEncontrado = False
        While Not bEncontrado
            If .Columns("ID").Value = CTOT Then
                bEncontrado = True
                .Columns("NUMPROCE").Value = m_dblTotal
                .Columns("MENU").CellStyleSet "Bold", i + 1
                If m_bTotCerr Or m_bTotPend Then
                    picTot.Visible = True
                    picTot.Top = .RowHeight * i + .Top + 25 + Picture1.Height + frameFiltro.Height + picMostrarFiltro.Height
                End If
            End If
            
            i = i + 1
            .MoveNext
        Wend
        
        .MoveFirst
    End With
    
    If Not m_bTotPend Then
        udtDevolVerProcesosVisor.DesdeEstado = sinitems
        udtDevolVerProcesosVisor.HastaEstado = ParcialmenteCerrado
        
        Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
        If m_adoRecordset Is Nothing Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        Set sdbgProcesos.DataSource = m_adoRecordset
        
        RedimensionarGrid
        
        m_iSeleccion = CTOTABIERTOS
    
        Dim bNoEncontrado As Boolean
    
        While bNoEncontrado
            If sdbgMenu.Columns("ID").Value = CTOTABIERTOS Then
                bEncontrado = True
            End If
        
            sdbgMenu.MoveNext
        Wend
    Else
        sdbgMenu.MoveFirst
        sdbgMenu.col = 1
        m_iSeleccion = sdbgMenu.Columns("ID").Value
        
        Select Case sdbgMenu.Columns("ID").Value
        
            Case 1 'Pendiente de validar
                m_iDesde = 1
                m_iHasta = 2
                
            Case 2 'Pendiente de asignar proveedores
                m_iDesde = 3
                m_iHasta = 4
                
            Case 3 'Pendiente de enviar peticiones
                m_iDesde = 5
                m_iHasta = 5
        
            Case 4 'En recepci�n de ofertas
                m_iDesde = 6
                m_iHasta = 7
         
            Case 5 'Pendiente de comunicar objetivos
                m_iDesde = 8
                m_iHasta = 9
         
            Case 6 'Pendiente de adjudicaciones
                m_iDesde = 10
                m_iHasta = 10
         
            Case 7 'Parcialmente cerrado
                m_iDesde = 11
                m_iHasta = 11
         
            Case 8 'Adjudicado y sin notificar
                m_iDesde = 12
                m_iHasta = 12
         
            Case 9 'Adjudicado y notificado
                m_iDesde = 13
                m_iHasta = 13
         
            Case 10 'Anulado
                m_iDesde = 20
                m_iHasta = 20
         
            Case 12 'Todos los abiertos
                sdbgMenu.MoveNext
                sdbgMenu.MoveNext
                If sdbgMenu.Columns("ID").Value <> CTOTCERRADOS Then
                    'Si estamos en Todos los abiertos cargamos procesos de Todos los cerrados
                    'porque significa que no hay proceso abiertos para el usuario.
                    m_iSeleccion = 8
                    m_iDesde = 12
                    m_iHasta = 12
                Else
                    'No cargamos ning�n proceso porque no hay ni abiertos ni cerrados para el usuario
                    m_iSeleccion = 0
                    m_iDesde = 0
                    m_iHasta = 0
                End If
                sdbgMenu.MoveFirst
         End Select
         
        udtDevolVerProcesosVisor.DesdeEstado = m_iDesde
        udtDevolVerProcesosVisor.HastaEstado = m_iHasta

        Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
        If m_adoRecordset Is Nothing Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        Set sdbgProcesos.DataSource = m_adoRecordset
        RedimensionarGrid
        
    End If
    
    Set m_oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
        
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Asigna la zona horaria por defecto que se usar� para las horas de subasta</summary>

Private Sub SetFormTZ()
    If Not IsNull(oUsuarioSummit.TimeZone) And Not IsEmpty(oUsuarioSummit.TimeZone) Then
        m_vTZHora = oUsuarioSummit.TimeZone
    Else
        'Asignar la zona horaria del equipo
        m_vTZHora = GetTimeZone.key
    End If
End Sub

''' <summary>Carga las variables de seguridad seg�n el usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 14/01/2013</revision>

Private Sub ConfigurarSeguridad()
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceRestUsuAper)) Is Nothing Then
        m_bRUsuAper = True
    End If
    
    If oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceRestComp)) Is Nothing Then
        m_bRAsig = True
    End If
    
    If oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceRestCompResp)) Is Nothing Then
        m_bRCompResp = True
    End If
    
    If oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceRestEqp)) Is Nothing Then
        m_bREqp = True
    End If
    
    If oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceRestMatComp)) Is Nothing Then
        m_bRMat = True
    End If
    
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceSoloDeAdjDir)) Is Nothing Then
        m_bSoloDeAdjDir = True
    End If
    
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceSoloDeAdjEnReu)) Is Nothing Then
        m_bSoloDeAdjEnReunion = True
    End If
    
    m_bRUsuUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceRestUsuUON)) Is Nothing)
    m_bRUsuDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceRestUsuDep)) Is Nothing)
    m_bRPerfUON = (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceRestPerfUON)) Is Nothing))
        
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestMatComprador)) Is Nothing Then
        m_bRMatBuzOfe = True
    End If
       
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestComprador)) Is Nothing Then
        m_bRAsigBuzOfe = True
    End If
    
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestResponsable)) Is Nothing Then
        m_bRCompRespBuzOfe = True
    End If

    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestEquipo)) Is Nothing Then
        m_bREqpAsigBuzOfe = True
    End If
    
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFEVerOfertasEquipo)) Is Nothing Then
        m_bROfeEqpBuzOfe = True
    End If
    
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFEVerOfertasTodas)) Is Nothing Then
        m_bROfeTodasBuzOfe = True
    End If
    
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestUsuUON)) Is Nothing Then
        m_bRUsuUonBuzOfe = True
    End If
    
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestUsuDep)) Is Nothing Then
        m_bRUsuDepBuzOfe = True
    End If
    
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestUsuAper)) Is Nothing Then
        m_bRUsuAperBuzOfe = True
    End If
End Sub

Private Sub ConfigurarMenu()

    sdbgMenu.RemoveAll
    
    If oUsuarioSummit.Tipo = Administrador Or g_bSoloInvitado Then
        
        sdbgMenu.AddItem 1 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(1)   '"Pendientes de validar apertura"
        sdbgMenu.AddItem 2 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(2)   '"Pendientes de asig. prove."
        sdbgMenu.AddItem 3 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(3)   '"Pendientes de enviar pet."
        sdbgMenu.AddItem 4 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(4)   '"En recepci�n de ofertas"
        sdbgMenu.AddItem 5 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(5)   '"Pend. de comunicar obj."
        sdbgMenu.AddItem 6 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(6)   '"Pendientes de adjudicar"
        sdbgMenu.AddItem 7 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(13)  '"Parcialmente cerrados"
        sdbgMenu.AddItem CTOTABIERTOS & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(7)  '"Todos los abiertos"
        sdbgMenu.AddItem CSEP1 & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        sdbgMenu.AddItem 8 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(8) '"Pend. de notificar las adj."
        sdbgMenu.AddItem 9 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(9) '"Adjudicados y notificados"
        sdbgMenu.AddItem 10 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(10) '"Anulados"
        sdbgMenu.AddItem CTOTCERRADOS & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(11)  '"Todos los cerrados"
        sdbgMenu.AddItem CSEP2 & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        sdbgMenu.AddItem CTOT & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(12) '"Todos"
        
    Else
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProcePendVal)) Is Nothing Then
            sdbgMenu.AddItem 1 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(1)   '"Pendientes de validar apertura"
        Else
            m_bTotPend = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProcePendAsig)) Is Nothing Then
            sdbgMenu.AddItem 2 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(2)   '"Pendientes de asig. prove."
        Else
            m_bTotPend = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProcePendPet)) Is Nothing Then
            sdbgMenu.AddItem 3 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(3)    '"Pendientes de enviar pet."
        Else
            m_bTotPend = True
        End If
            
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceRecOfe)) Is Nothing Then
            sdbgMenu.AddItem 4 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(4)     '"En recepci�n de ofertas"
        Else
            m_bTotPend = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceComObj)) Is Nothing Then
            sdbgMenu.AddItem 5 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(5)     '"Pend. de comunicar obj."
        Else
            m_bTotPend = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProcePendAdj)) Is Nothing Then
            sdbgMenu.AddItem 6 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(6)     '"Pendientes de adjudicar"
        Else
            m_bTotPend = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceParCer)) Is Nothing Then
            sdbgMenu.AddItem 7 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(13)     '"Parcialmente cerrados"
        End If
       
        sdbgMenu.AddItem CTOTABIERTOS & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(7)   '"Todos los abiertos"
        sdbgMenu.AddItem CSEP1 & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProcePendAdjNotif)) Is Nothing Then
            sdbgMenu.AddItem 8 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(8)    '"Pend. de notificar las adj."
        Else
            m_bTotCerr = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceAdjNotif)) Is Nothing Then
            sdbgMenu.AddItem 9 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(9)      '"Adjudicados y notificados"
        Else
            m_bTotCerr = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceAnul)) Is Nothing Then
            sdbgMenu.AddItem 10 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(10)     '"Anulados"
        Else
            m_bTotCerr = True
        End If
        
        sdbgMenu.AddItem CTOTCERRADOS & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(11)    '"Todos los cerrados"
        sdbgMenu.AddItem CSEP2 & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        sdbgMenu.AddItem CTOT & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_sEstados(12)            '"Todos"
                
    End If
    
End Sub

''' <summary>
''' Evento que se lanza cuando se cambia de tama�o al formulario
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde el evento; Tiempo m�ximo: 0</remarks>
Private Sub Form_Resize()
'Para que no casque al minimizarlo mucho
On Error Resume Next

    cmdMinimizar.Visible = True
    Picture1.Visible = True
    Picture2.Visible = True
    pictureMinimizar.Visible = False
    
    lblSaludo.Width = cmdMinimizar.Left - 600 - 75
    
    cmdCerrar.Top = 30
    cmdRestaurarVentana.Top = 30
    cmdMaximizar.Top = 30
            
    m_bModoTamanyo = False
    
    If Me.Height < 2500 Then Exit Sub
    If Me.Width < 3500 Then Exit Sub
    
    frameFiltro.Width = Me.Width - 195
    frDatos(0).Width = frameFiltro.Width - 100
    Line3(1).X2 = frDatos(0).Width
    If Not g_oConfVisor Is Nothing Then
        If g_oConfVisor.OcultarFiltro = True Then
            picMenu.Height = Me.Height - frameFiltro.Top - 645
        Else
            picMenu.Height = Me.Height - frameFiltro.Height - frameFiltro.Top - 645
        End If
        If g_oConfVisor.OcultarMenu = True Then
            sdbgProcesos.Width = Me.Width - 150
        Else
            sdbgProcesos.Width = Me.Width - picMenu.Width - 150
        End If
    Else
        picMenu.Height = Me.Height - frameFiltro.Height - frameFiltro.Top - 645
        sdbgProcesos.Width = Me.Width - picMenu.Width - 150
    End If
    
    sdbgProcesos.Height = picMenu.Height
    sdbgMenu.Height = picMenu.Height - 100
    
    lblFS.Left = Me.Left + Me.Width - 400
    
    If (Me.Left + Me.Width) > 500 Then
        cmdCerrar.Left = Me.Width - 400
        cmdMaximizar.Left = Me.Width - 690
        cmdRestaurarVentana.Left = Me.Width - 690
        cmdMinimizar.Left = Me.Width - 950
        cmdExcel.Left = Me.Width - 685
        
        If m_bMicrostrategy Then
            cmdMicro.Left = cmdExcel.Left - 285
            PicExclamacion.Left = cmdMicro.Left - 200
            PicNuevas.Left = PicExclamacion.Left - 345
        Else
            PicExclamacion.Left = cmdExcel.Left - 200
            PicNuevas.Left = PicExclamacion.Left - 345
        End If
        
        lblSaludo.Width = cmdMinimizar.Left - 600 - 75
    End If
    
    picMostrarFiltro.Left = sdbgProcesos.Left + sdbgProcesos.Width - picMostrarFiltro.Width
    
    m_bModoTamanyo = False
    
    Refresh
    
    Me.caption = ""
    
    If Me.WindowState = vbNormal Then
        cmdRestaurarVentana.Visible = False
        cmdMaximizar.Visible = True
    Else
        cmdRestaurarVentana.Visible = True
        cmdMaximizar.Visible = False
    End If
End Sub

''' <summary>
''' Antes de cerrar almacena en BD la configuraci�n de campos y filtro del visor
''' </summary>
''' <param name="Cancel">Cancelar el cierre de la pantalla</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
    Screen.MousePointer = vbHourglass
   
    'Almacena en BD la configuraci�n de campos y filtro del visor:
    If g_oConfVisor.HayCambios Then GuardarConfiguracionVisor
    
    With sdbgProcesos
        Select Case gParametrosInstalacion.giOrdenVisor
            Case TipoOrdenacionProcesos.OrdPorDen
                If Not .Columns("DESCR").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorMaterial
                If Not .Columns("GMN1").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorfechaapertura
                If Not .Columns("FECAPE").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorfechanecesidad
                If Not .Columns("FECNEC").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorFechaPresentacion
                If Not .Columns("FECPRES").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorEstado
                If Not .Columns("EST").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorResponsable
                If Not .Columns("RESP").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorAdjDir
                If Not .Columns("ADJDIR").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorFechaIniSuministro
                If Not .Columns("FECINI").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorFechaFinSuministro
                If Not .Columns("FECFIN").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorMoneda
                If Not .Columns("MON").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorDestino
                If Not .Columns("DEST").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorDestinoDen
                If Not .Columns("DESTDEN").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorFPago
                If Not .Columns("PAGO").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorProveAct
                If Not .Columns("PROVE").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorProveActDen
                If Not .Columns("PROVEDEN").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorFechaCierre
                If Not .Columns("FECCIERRE").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorConsumido
                If Not .Columns("CONS").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorAdjudicado
                If Not .Columns("IMP").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorPresTotal
                If Not .Columns("PRES").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorP
                If Not .Columns("P").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorO
                If Not .Columns("O").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorW
                If Not .Columns("W").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorR
                If Not .Columns("R").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorAhorro
                If Not .Columns("AHORRO").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorFechaValidezOfertas
                If Not .Columns("FECLIMOFE").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorFecValApertura
                If Not .Columns("FECVALAPERTURA").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorPorcentajeAhorro
                If Not .Columns("PORCENTAJEAHORRO").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorCambio
                If Not .Columns("CAMBIO").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorGsToErp
                If Not .Columns("GSTOERP").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
            Case TipoOrdenacionProcesos.OrdPorSolicVinculada
                If Not .Columns("VINCULADA").Visible Then gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
        End Select
    End With
    oGestorParametros.GuardarOrdenVisorInstalacion gParametrosInstalacion.giOrdenVisor, oUsuarioSummit.Cod, gParametrosInstalacion.gbOrdenVisorAscendente
    
    Set g_oConfVisor = Nothing
    Set m_oGestorReuniones = Nothing
    Set m_oEqpSeleccionado = Nothing
    Set m_oEquipos = Nothing
    Set m_oCompSeleccionado = Nothing
    Set m_oComps = Nothing

    Set m_oGMN1Seleccionados = Nothing
    Set m_oGMN2Seleccionados = Nothing
    Set m_oGMN3Seleccionados = Nothing
    Set m_oGMN4Seleccionados = Nothing
    
    MDI.mnuProceEst.Checked = False
    
    'Descarga el formulario invisible que tiene el men� de configuraci�n
    Unload frmMenuEst
    
    Screen.MousePointer = vbNormal
    
    Me.Visible = False
End Sub

Private Sub frameFiltro_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picToolTip.Visible = False
End Sub

Private Sub lblMinimizar_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then
        m_bModomovimiento = True
        m_bposx = X
        m_bposy = Y
    End If
End Sub

Private Sub lblMinimizar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Me.WindowState = vbMaximized Then Exit Sub
    
    If m_bModomovimiento = True Then
        If m_bposx <> X Or m_bposy <> Y Then
            frmEST.Move frmEST.Left + (X - m_bposx), frmEST.Top + (Y - m_bposy)
        End If
    End If
End Sub

Private Sub lblMinimizar_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    m_bModomovimiento = False
End Sub


Private Sub lblMostraFiltro_Click()
    If picAbrirFiltro.Visible = True Then
        'Hace visible el frame de filtro
        MostrarFiltro
        
    Else
        MostrarAtrBuscar False
        'Oculta el frame del filtro
        OcultarFiltro
        
        g_oConfVisor.HayCambios = True
        g_oConfVisor.OcultarFiltro = True

    End If
End Sub

Private Sub lblMostraFiltro_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picMostrarFiltro.BorderStyle = 1
    picMostrarFiltro.BorderStyle = 0
End Sub


Private Sub lblMostrarMenu_Click()
    'Muestra el menu
    MostrarMenu
End Sub

Private Sub lblMostrarMenu_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picMostrarMenu.BorderStyle = 1
    picMostrarMenu.BorderStyle = 0
End Sub


Private Sub lblOcultarMenu_Click()
    'Ocultar men�
    OcultarMenu
    
    g_oConfVisor.HayCambios = True
    g_oConfVisor.OcultarMenu = True
End Sub

Private Sub lblOcultarMenu_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picOcultarMenu.BorderStyle = 1
    picOcultarMenu.BorderStyle = 0
End Sub


Private Sub lblSaludo_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then
        m_bModomovimiento = True
        m_bposx = X
        m_bposy = Y
    End If

End Sub

Private Sub lblSaludo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If Me.WindowState = vbMaximized Then Exit Sub
    
    If m_bModomovimiento = True Then
        If m_bposx <> X Or m_bposy <> Y Then
            frmEST.Move frmEST.Left + (X - m_bposx), frmEST.Top + (Y - m_bposy)
        End If
    End If
End Sub

Private Sub lblSaludo_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    m_bModomovimiento = False
End Sub


Private Sub optDesde_Click()
    If optDesde.Value = True Then
        sdbcAnyo.Enabled = True
        sdbcMes.Enabled = True
        sMesAnterior = sdbcMes.Text
        sAnyoAnterior = sdbcAnyo.Text
        
        If Not m_bCargandoFiltro Then g_oConfVisor.HayCambios = True
    End If
End Sub

Private Sub optTodos_Click()
    If optTodos.Value = True Then
        sdbcAnyo.Enabled = False
        sdbcMes.Enabled = False
        sMesAnterior = ""
        sAnyoAnterior = ""
        If Not m_bCargandoFiltro Then g_oConfVisor.HayCambios = True
    End If
End Sub

Private Sub picAbrirFiltro_Click()
    'Hace visible el frame de filtro
    MostrarFiltro
End Sub

Private Sub picAbrirFiltro_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picMostrarFiltro.BorderStyle = 1
    picMostrarFiltro.BorderStyle = 0
End Sub


Private Sub PicAbrirMenu_Click()
    'Muestra el men�
    MostrarMenu
End Sub

Private Sub PicAbrirMenu_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picMostrarMenu.BorderStyle = 1
    picMostrarMenu.BorderStyle = 0
End Sub


Private Sub picCerrarFiltro_Click()
    'Oculta el frame del filtro
    OcultarFiltro
    
    g_oConfVisor.HayCambios = True
    g_oConfVisor.OcultarFiltro = True
End Sub

Private Sub picCerrarFiltro_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picMostrarFiltro.BorderStyle = 1
    picMostrarFiltro.BorderStyle = 0
End Sub


Private Sub PicCerrarMenu_Click()
    'Ocultar men�
    OcultarMenu
    
    g_oConfVisor.HayCambios = True
    g_oConfVisor.OcultarMenu = True
End Sub

Private Sub PicCerrarMenu_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picOcultarMenu.BorderStyle = 1
    picOcultarMenu.BorderStyle = 0
End Sub

'Descripcion:Comprueba si hay alguna notificacion de despublicacion o si hay algun contrato proximo a expirar
'Parametros entrada:
'   Button: boton del mouse pulsado
'   Shift:  Si esta pulsado el boton Shift
'   X,Y: Posicion del puntero
'Llamada desde:Evento que salta al pinchar con el raton en el icono de la exclamacion
'Tiempo ejecucion:1seg.
Private Sub PicExclamacion_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If m_bMostrado Then Exit Sub
    Screen.MousePointer = vbNormal
    If m_bAvisoNotificacion Then
        PosibleNotificacionDeDespublicacion
        
        frmAvisoDespublicacion.g_FechaDesDes = m_dtFechaDesDes
        frmAvisoDespublicacion.g_FechaHasDes = m_dtFechaHasDes
        frmAvisoDespublicacion.g_FechaDesAdj = m_dtFechaDesAdj
        frmAvisoDespublicacion.g_FechaHasAdj = m_dtFechaHasAdj
    End If
    If m_bAvisoContrato Then
        PosibleAlertaContrato
    End If
    If PicExclamacion.Visible Then
        m_bMostrado = True
        frmAvisoDespublicacion.g_bAvisoNotificacion = m_bAvisoNotificacion
        frmAvisoDespublicacion.g_bAvisoContrato = m_bAvisoContrato
        frmAvisoDespublicacion.Show 1
        m_bMostrado = False
    End If
End Sub

Private Sub picMostrarFiltro_Click()
    
    If picAbrirFiltro.Visible = True Then
        'Hace visible el frame de filtro
        MostrarFiltro

    Else
        'Oculta el frame del filtro
        OcultarFiltro
        
        g_oConfVisor.HayCambios = True
        g_oConfVisor.OcultarFiltro = True

    End If
    
End Sub

Private Sub picMostrarFiltro_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picMostrarFiltro.BorderStyle = 1
    picMostrarFiltro.BorderStyle = 0
End Sub


Private Sub picMostrarMenu_Click()
    'Hace visible el men� de la izquieda
    MostrarMenu
    
End Sub

Private Sub picMostrarMenu_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picMostrarMenu.BorderStyle = 1
    picMostrarMenu.BorderStyle = 0
End Sub


Private Sub PicNuevas_DblClick()

    If oUsuarioSummit.Tipo <> TIpoDeUsuario.comprador Or (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFEConsultar)) Is Nothing) Then Exit Sub

    Screen.MousePointer = vbHourglass
    frmOFEBuzon.sOrigen = "Visor"
    frmOFEBuzon.opNoLeidas = True
    frmOFEBuzon.sdbcAnyo = ""
    frmOFEBuzon.ProceSelector1.Seleccion = PSTodos
    frmOFEBuzon.cmdActualizar_Click
    MDI.MostrarFormulario frmOFEBuzon
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub PicNuevas_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If oUsuarioSummit.Tipo <> TIpoDeUsuario.comprador Or (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFEConsultar)) Is Nothing) Then Exit Sub

    ConfigurarMenuVisor
    PopupMenu MDI.mnuPopUpBuzOfe
    
End Sub

Private Sub picOcultarMenu_Click()
    'Oculta el men� de la izquierda
    OcultarMenu
    
    g_oConfVisor.HayCambios = True
    g_oConfVisor.OcultarMenu = True
End Sub

Private Sub picOcultarMenu_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picOcultarMenu.BorderStyle = 1
    picOcultarMenu.BorderStyle = 0
End Sub

Private Sub Picture1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

    If Button = vbLeftButton Then
        m_bModomovimiento = True
        m_bposx = X
        m_bposy = Y
    End If
    
End Sub

Private Sub Picture1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If Me.WindowState = vbMaximized Then Exit Sub
        
    If m_bModomovimiento = True Then
        If m_bposx <> X Or m_bposy <> Y Then
            frmEST.Move frmEST.Left + (X - m_bposx), frmEST.Top + (Y - m_bposy)
        End If
    End If
End Sub

Private Sub Picture1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

    m_bModomovimiento = False
   
End Sub


Private Sub pictureMinimizar_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then
        m_bModomovimiento = True
        m_bposx = X
        m_bposy = Y
    End If
End Sub

Private Sub pictureMinimizar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Me.WindowState = vbMaximized Then Exit Sub
        
    If m_bModomovimiento = True Then
        If m_bposx <> X Or m_bposy <> Y Then
            frmEST.Move frmEST.Left + (X - m_bposx), frmEST.Top + (Y - m_bposy)
        End If
    End If
    
End Sub

Private Sub pictureMinimizar_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    m_bModomovimiento = False
End Sub

Private Sub sdbcAnyo_CloseUp()
    g_oConfVisor.HayCambios = True
    
    If sdbcAnyo.Text = sAnyoAnterior Then
        Exit Sub
    Else
        sAnyoAnterior = sdbcAnyo.Text
    End If
End Sub

Private Sub sdbcComprador_Change()
    If m_bCargandoFiltro = True Then Exit Sub
     
    If Not m_bRespetarCombo Then
        Set m_oCompSeleccionado = Nothing
        m_sComCod = ""
    
        g_oConfVisor.HayCambios = True
    End If
End Sub

Private Sub sdbcComprador_Click()
    If Not sdbcComprador.DroppedDown Then
        sdbcComprador = ""
    End If
End Sub


Private Sub sdbcComprador_CloseUp()
    Dim sCod As String
    
    If sdbcComprador.Value = "..." Then
        sdbcComprador.Text = ""
        m_sComCod = ""
        
    ElseIf sdbcComprador.Value = "" Then
        If m_sComCod = "" Then Exit Sub
        m_sComCod = ""
        
    Else
        sCod = m_oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(m_oEqpSeleccionado.Cod))
        Set m_oCompSeleccionado = m_oComps.Item(sCod & sdbcComprador.Columns(0).Text)
        If m_sComCod = m_oCompSeleccionado.Cod Then Exit Sub
        m_sComCod = m_oCompSeleccionado.Cod
    End If
    
    g_oConfVisor.HayCambios = True
End Sub


Private Sub sdbcComprador_DropDown()
    Dim oComp As CComprador
    
    sdbcComprador.RemoveAll
    
    If m_oEqpSeleccionado Is Nothing Then Exit Sub
    
    Set m_oComps = Nothing
    
    Screen.MousePointer = vbHourglass

    Set m_oComps = oFSGSRaiz.generar_CCompradores
    

    m_oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, False, False
    
    Set m_oComps = m_oEqpSeleccionado.Compradores
    
    If m_oComps.Count > 0 Then
        sdbcComprador.AddItem ""
    End If
    
    For Each oComp In m_oComps
        sdbcComprador.AddItem oComp.Cod & Chr(m_lSeparador) & oComp.Cod & " - " & NullToStr(oComp.nombre) & " " & NullToStr(oComp.Apel)
    Next
    
    sdbcComprador.SelStart = 0
    sdbcComprador.SelLength = Len(sdbcComprador.Text)
    sdbcComprador.Refresh

    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcComprador_InitColumnProps()
    sdbcComprador.DataFieldList = "Column 1"
    sdbcComprador.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcComprador_KeyDown(KeyCode As Integer, Shift As Integer)
    If sdbcComprador.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
        sdbcComprador.Text = ""
        m_sComCod = ""
        g_oConfVisor.HayCambios = True
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcComprador_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcComprador.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcComprador.Rows - 1
            bm = sdbcComprador.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcComprador.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcComprador.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcEquipo_Change()
    If m_bCargandoFiltro = True Then Exit Sub
    
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        sdbcComprador = ""
        sdbcComprador.RemoveAll
        m_bRespetarCombo = False
        Set m_oEqpSeleccionado = Nothing
        m_sEqpCod = ""
    
        g_oConfVisor.HayCambios = True
    End If
End Sub

Private Sub sdbcEquipo_Click()
    If Not sdbcEquipo.DroppedDown Then
        sdbcEquipo = ""
    End If
End Sub

Private Sub sdbcEquipo_CloseUp()
    Screen.MousePointer = vbHourglass
    
    If sdbcEquipo.Value = "..." Then
        sdbcEquipo.Columns(0).Value = ""
        sdbcEquipo.Columns(1).Value = ""
        m_sEqpCod = ""
        Set m_oEqpSeleccionado = Nothing
        
    ElseIf sdbcEquipo.Value = "" Then
        If m_sEqpCod = "" Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        m_sEqpCod = ""
        Set m_oEqpSeleccionado = Nothing
        
    Else
        Set m_oEqpSeleccionado = m_oEquipos.Item(sdbcEquipo.Columns(0).Text)
        If m_sEqpCod = m_oEqpSeleccionado.Cod Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        m_sEqpCod = m_oEqpSeleccionado.Cod
    End If
    
    sdbcComprador.Text = ""
    sdbcComprador.RemoveAll
        
    g_oConfVisor.HayCambios = True
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEquipo_DropDown()
    Dim oeqp As CEquipo
    
    Set m_oEquipos = Nothing
    Set m_oEquipos = oFSGSRaiz.Generar_CEquipos
     
    Screen.MousePointer = vbHourglass
    
    sdbcEquipo.RemoveAll
    
    m_oEquipos.CargarTodosLosEquipos , , False, True, False
    
    If m_oEquipos.Count > 0 Then
        sdbcEquipo.AddItem ""
    End If
    For Each oeqp In m_oEquipos
        sdbcEquipo.AddItem oeqp.Cod & Chr(m_lSeparador) & oeqp.Cod & " - " & oeqp.Den
    Next
    
    sdbcEquipo.SelStart = 0
    sdbcEquipo.SelLength = Len(sdbcEquipo.Columns(0).Value)
    sdbcEquipo.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEquipo_InitColumnProps()
    sdbcEquipo.DataFieldList = "Column 1"
    sdbcEquipo.DataFieldToDisplay = "Column 1"
End Sub


Private Sub sdbcEquipo_KeyDown(KeyCode As Integer, Shift As Integer)
    If sdbcEquipo.Text = "" Then Exit Sub
    
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
        sdbcEquipo.Text = ""
        m_sEqpCod = ""
        m_sComCod = ""
        chkResponsable.Value = vbUnchecked
        g_oConfVisor.HayCambios = True
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEquipo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEquipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEquipo.Rows - 1
            bm = sdbcEquipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEquipo.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcEquipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcMes_CloseUp()
    g_oConfVisor.HayCambios = True
    
    If sdbcMes.Text = sMesAnterior Then
        Exit Sub
    Else
        sMesAnterior = sdbcMes.Text
    End If
End Sub

Private Sub sdbgMenu_BtnClick()
    'BLoquea la pantalla hasta que se cargue la grid y los men�s
    LockWindowUpdate Me.hWnd

    With sdbgMenu
        If .Columns(.col).Name = "MENU" Then
            Restaurar
                
        ElseIf .Columns(.col).Name = "COLOR" Then
            CommonDialog1.Color = 0
            
            CommonDialog1.ShowColor
            If CommonDialog1.Color = 0 Then
                LockWindowUpdate 0&
                Exit Sub
            End If
                    
            Select Case .Columns("ID").Value
                Case 1
                    gParametrosInstalacion.TCPendValidar = CommonDialog1.Color
                    .StyleSets.Item("PendValidar").Backcolor = CLng(gParametrosInstalacion.TCPendValidar)
                    .Refresh
                    sdbgProcesos.StyleSets.Item("PendValidar").Backcolor = CLng(gParametrosInstalacion.TCPendValidar)
                Case 2
                    gParametrosInstalacion.TCPendAsigProve = CommonDialog1.Color
                    .StyleSets.Item("PendAsigProve").Backcolor = CLng(gParametrosInstalacion.TCPendAsigProve)
                    .Refresh
                    sdbgProcesos.StyleSets.Item("PendAsigProve").Backcolor = CLng(gParametrosInstalacion.TCPendAsigProve)
                Case 3
                    gParametrosInstalacion.TCPendEnvPet = CommonDialog1.Color
                    .StyleSets.Item("PendEnvPet").Backcolor = CLng(gParametrosInstalacion.TCPendEnvPet)
                    .Refresh
                    sdbgProcesos.StyleSets.Item("PendEnvPet").Backcolor = CLng(gParametrosInstalacion.TCPendEnvPet)
                Case 4
                    gParametrosInstalacion.TCEnRecOFe = CommonDialog1.Color
                    .StyleSets.Item("EnRecOFe").Backcolor = CLng(gParametrosInstalacion.TCEnRecOFe)
                    .Refresh
                    sdbgProcesos.StyleSets.Item("EnRecOFe").Backcolor = CLng(gParametrosInstalacion.TCEnRecOFe)
                Case 5
                    gParametrosInstalacion.TCPendComObj = CommonDialog1.Color
                    .StyleSets.Item("PendComObj").Backcolor = CLng(gParametrosInstalacion.TCPendComObj)
                    .Refresh
                    sdbgProcesos.StyleSets.Item("PendComObj").Backcolor = CLng(gParametrosInstalacion.TCPendComObj)
                Case 6
                    gParametrosInstalacion.TCPendAdj = CommonDialog1.Color
                    .StyleSets.Item("PendAdjudicar").Backcolor = CLng(gParametrosInstalacion.TCPendAdj)
                    .Refresh
                    sdbgProcesos.StyleSets.Item("PendAdjudicar").Backcolor = CLng(gParametrosInstalacion.TCPendAdj)
                Case 7
                    gParametrosInstalacion.TCParcCerr = CommonDialog1.Color
                    sdbgMenu.StyleSets.Item("ParcCerrado").Backcolor = CLng(gParametrosInstalacion.TCParcCerr)
                    sdbgMenu.Refresh
                    sdbgProcesos.StyleSets.Item("ParcCerrado").Backcolor = CLng(gParametrosInstalacion.TCParcCerr)
                Case 8
                    gParametrosInstalacion.TCAdjSinNotificar = CommonDialog1.Color
                    .StyleSets.Item("AdjSinNotificar").Backcolor = CLng(gParametrosInstalacion.TCAdjSinNotificar)
                    .Refresh
                    sdbgProcesos.StyleSets.Item("AdjSinNotificar").Backcolor = CLng(gParametrosInstalacion.TCAdjSinNotificar)
                Case 9
                    gParametrosInstalacion.TCAdjyNotificado = CommonDialog1.Color
                    .StyleSets.Item("AdjyNotificado").Backcolor = CLng(gParametrosInstalacion.TCAdjyNotificado)
                    .Refresh
                    sdbgProcesos.StyleSets.Item("AdjyNotificado").Backcolor = CLng(gParametrosInstalacion.TCAdjyNotificado)
                Case 10
                    gParametrosInstalacion.TCAnulado = CommonDialog1.Color
                    .StyleSets.Item("Anulado").Backcolor = CLng(gParametrosInstalacion.TCAnulado)
                    .Refresh
                    sdbgProcesos.StyleSets.Item("Anulado").Backcolor = CLng(gParametrosInstalacion.TCAnulado)
            End Select
                        
            Restaurar
                        
            oGestorParametros.GuardarColoresInstalacion gParametrosInstalacion, oUsuarioSummit.Cod
        End If
    End With
                      
    'Desbloquea la pantalla
    LockWindowUpdate 0&
End Sub



Private Sub sdbgMenu_InitColumnProps()
    
    sdbgMenu.Columns("NUMPROCE").Locked = True
    sdbgMenu.AllowUpdate = True
    
    If gParametrosInstalacion.TCPendValidar <> "" Then
        sdbgMenu.StyleSets.Item("PendValidar").Backcolor = CLng(gParametrosInstalacion.TCPendValidar)
    End If
    
    If gParametrosInstalacion.TCPendAsigProve <> "" Then
        sdbgMenu.StyleSets.Item("PendAsigProve").Backcolor = CLng(gParametrosInstalacion.TCPendAsigProve)
    End If
    
    If gParametrosInstalacion.TCPendEnvPet <> "" Then
        sdbgMenu.StyleSets.Item("PendEnvPet").Backcolor = CLng(gParametrosInstalacion.TCPendEnvPet)
    End If
    
    If gParametrosInstalacion.TCEnRecOFe <> "" Then
        sdbgMenu.StyleSets.Item("EnRecOfe").Backcolor = CLng(gParametrosInstalacion.TCEnRecOFe)
    End If
    
    If gParametrosInstalacion.TCPendComObj <> "" Then
        sdbgMenu.StyleSets.Item("PendComObj").Backcolor = CLng(gParametrosInstalacion.TCPendComObj)
    End If
    
    If gParametrosInstalacion.TCPendAdj <> "" Then
        sdbgMenu.StyleSets.Item("PendAdjudicar").Backcolor = CLng(gParametrosInstalacion.TCPendAdj)
    End If
    
    If gParametrosInstalacion.TCAdjSinNotificar <> "" Then
        sdbgMenu.StyleSets.Item("AdjSinNotificar").Backcolor = CLng(gParametrosInstalacion.TCAdjSinNotificar)
    End If
    
    If gParametrosInstalacion.TCAdjyNotificado <> "" Then
        sdbgMenu.StyleSets.Item("AdjYNotificado").Backcolor = CLng(gParametrosInstalacion.TCAdjyNotificado)
    End If
    
    If gParametrosInstalacion.TCAnulado <> "" Then
        sdbgMenu.StyleSets.Item("Anulado").Backcolor = CLng(gParametrosInstalacion.TCAnulado)
    End If

    If gParametrosInstalacion.TCParcCerr <> "" Then
        sdbgMenu.StyleSets.Item("ParcCerrado").Backcolor = CLng(gParametrosInstalacion.TCParcCerr)
    End If
    
End Sub

Private Sub sdbgMenu_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim oAtrBuscar As CAtributosBuscar
    If sdbgMenu.col = -1 Then Exit Sub
    
    If m_bRespetarRowColChange Then Exit Sub
    
    If sdbgMenu.Columns(sdbgMenu.col).Name <> "MENU" Then Exit Sub
    
    'BLoquea la pantalla hasta que se cargue la grid y los men�s
    LockWindowUpdate Me.hWnd
    sdbgMenu.Enabled = False
    Select Case sdbgMenu.Columns("ID").Value
        Case 1 'Pendientes de validar apertura
                Screen.MousePointer = vbHourglass
                m_iSeleccion = 1
                m_iDesde = 1
                m_iHasta = 2
                MostrarSeleccion sdbgMenu.Row
        Case 2 'Pendientes de asignar proveedores
                Screen.MousePointer = vbHourglass
                m_iSeleccion = 2
                m_iDesde = 3
                m_iHasta = 4
                MostrarSeleccion sdbgMenu.Row
        Case 3 'Pendientes de enviar peticiones
                Screen.MousePointer = vbHourglass
                m_iSeleccion = 3
                m_iDesde = 5
                m_iHasta = 5
                MostrarSeleccion sdbgMenu.Row
        Case 4 'En recepci�n de ofertas
                Screen.MousePointer = vbHourglass
                m_iSeleccion = 4
                m_iDesde = 6
                m_iHasta = 7
                MostrarSeleccion sdbgMenu.Row
        Case 5 'Pendiente de comunicar objetivos
                Screen.MousePointer = vbHourglass
                m_iSeleccion = 5
                m_iDesde = 8
                m_iHasta = 9
                MostrarSeleccion sdbgMenu.Row
        Case 6 'Pendiente de adjudicar
                Screen.MousePointer = vbHourglass
                m_iSeleccion = 6
                m_iDesde = 10
                m_iHasta = 10
                MostrarSeleccion sdbgMenu.Row
        Case 7 'Parcialmente cerrado
                Screen.MousePointer = vbHourglass
                m_iSeleccion = 7
                m_iDesde = 11
                m_iHasta = 11
                MostrarSeleccion sdbgMenu.Row
        Case 8 'Pendiente de notificar adj.
                Screen.MousePointer = vbHourglass
                m_iSeleccion = 8
                m_iDesde = 12
                m_iHasta = 12
                MostrarSeleccion sdbgMenu.Row
        Case 9 'Adjudicados y notificados
                Screen.MousePointer = vbHourglass
                m_iSeleccion = 9
                m_iDesde = 13
                m_iHasta = 13
                MostrarSeleccion sdbgMenu.Row
        Case 10 'Anulados
                Screen.MousePointer = vbHourglass
                m_iSeleccion = 10
                m_iDesde = 20
                m_iHasta = 20
                MostrarSeleccion sdbgMenu.Row
        Case CTOT 'Todos
                If m_bTotPend Or m_bTotCerr Then Exit Sub
                Screen.MousePointer = vbHourglass
                m_iSeleccion = CTOT
                m_iDesde = 1
                m_iHasta = 20
                MostrarSeleccion sdbgMenu.Row
        Case CTOTABIERTOS 'Todos los abiertos
                If m_bTotPend Then Exit Sub
                Screen.MousePointer = vbHourglass
                m_iSeleccion = CTOTABIERTOS
                m_iDesde = 1
                m_iHasta = 11
                MostrarSeleccion sdbgMenu.Row
        Case CTOTCERRADOS 'Todos los cerrados
                If m_bTotCerr Then Exit Sub
                Screen.MousePointer = vbHourglass
                m_iSeleccion = CTOTCERRADOS
                m_iDesde = 12
                m_iHasta = 20
                MostrarSeleccion sdbgMenu.Row
    End Select
    'Coleccion que recoge los atributos por los que se quiere filtrar y el ambito en el que se quiere que se cumplan
    Set oAtrBuscar = MoverGridAtrBuscarAColleccion(sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0))
    Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(FiltroProcesosVisor(True), m_sEstadoIntegracion(), oAtrBuscar)
     
    If m_adoRecordset Is Nothing Then
        'Desbloquea la pantalla
        LockWindowUpdate 0&
        Screen.MousePointer = vbNormal
        sdbgMenu.Columns("NUMPROCE").Value = 0
        Exit Sub
    End If
    
    Set sdbgProcesos.DataSource = m_adoRecordset
    sdbgMenu.Columns("NUMPROCE").Value = m_adoRecordset.RecordCount
    
    'Desbloquea la pantalla
    LockWindowUpdate 0&
    sdbgMenu.Enabled = True
    Screen.MousePointer = vbNormal
End Sub

''' <summary>General la estructura con los filtros para obtener los procesos</summary>
''' <param name="bFiltrarEstado">Si hay que incluir los filtros por estado</param>
''' <remarks>Llamada desde: Form_Load, cmdExcel_Click, sdbgMenu_RowColChange, sdbgProcesos_HeadClick, OrdenarGridDesdeMenu, Restaurar</remarks>

Private Function FiltroProcesosVisor(ByVal bFiltrarEstado As Boolean) As TipoDevolVerProcesosVisor
    Dim udtDevolVerProcesosVisor As TipoDevolVerProcesosVisor
    Dim lIdPerfil As Long
    Dim vAnyo As Variant
    Dim vMes As Variant
    
    If optDesde.Value Then
        vAnyo = sdbcAnyo.Text
        vMes = sdbcMes.AddItemRowIndex(sdbcMes.Bookmark) + 1
    Else
        vAnyo = Null
        vMes = Null
    End If
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    With udtDevolVerProcesosVisor
        .iNumMaximo = 32000
        If bFiltrarEstado Then
            .DesdeEstado = m_iDesde
            .HastaEstado = m_iHasta
        End If
        .CriterioOrdenacion = gParametrosInstalacion.giOrdenVisor
        .Eqp = basOptimizacion.gCodEqpUsuario
        .Comp = basOptimizacion.gCodPersonaUsuario
        .bRMat = m_bRMat
        .bRAsig = m_bRAsig
        .bREqpAsig = m_bREqp
        .bRCompResponsable = m_bRCompResp
        .SoloDeAdjDirecta = m_bSoloDeAdjDir
        .SoloSinAdjDirecta = m_bSoloDeAdjEnReunion
        .bRUsuAper = m_bRUsuAper
        .CodUsu = basOptimizacion.gvarCodUsuario
        .bRUsuUON = m_bRUsuUON
        .bRUsuDep = m_bRUsuDep
        .UON1 = basOptimizacion.gUON1Usuario
        .UON2 = basOptimizacion.gUON2Usuario
        .UON3 = basOptimizacion.gUON3Usuario
        .Dep = basOptimizacion.gCodDepUsuario
        .bOrdenAsc = gParametrosInstalacion.gbOrdenVisorAscendente
        .vAnyoDesde = vAnyo
        .vMesDesde = vMes
        Set .oGMN1Cod = m_oGMN1Seleccionados
        Set .oGMN2Cod = m_oGMN2Seleccionados
        Set .oGMN3Cod = m_oGMN3Seleccionados
        Set .oGMN4Cod = m_oGMN4Seleccionados
        .sCodEqp = m_sEqpCod
        .sCodCom = m_sComCod
        .bResponsable = SQLBinaryToBoolean(chkResponsable.Value)
        .sUON1Filtro = sUON1
        .sUON2Filtro = sUON2
        .sUON3Filtro = sUON3
        .vPresup1_1 = m_vPresup1_1
        .vPresup1_2 = m_vPresup1_2
        .vPresup1_3 = m_vPresup1_3
        .vPresup1_4 = m_vPresup1_4
        .vPresup2_1 = m_vPresup2_1
        .vPresup2_2 = m_vPresup2_2
        .vPresup2_3 = m_vPresup2_3
        .vPresup2_4 = m_vPresup2_4
        .vPresup3_1 = m_vPresup3_1
        .vPresup3_2 = m_vPresup3_2
        .vPresup3_3 = m_vPresup3_3
        .vPresup3_4 = m_vPresup3_4
        .vPresup4_1 = m_vPresup4_1
        .vPresup4_2 = m_vPresup4_2
        .vPresup4_3 = m_vPresup4_3
        .vPresup4_4 = m_vPresup4_4
        .sUON1Pres = m_sUON1Pres
        .sUON2Pres = m_sUON2Pres
        .sUON3Pres = m_sUON3Pres
        .sEstados = m_sEstados
        .bInvitado = IIf(oUsuarioSummit.Tipo = Administrador, False, True)
        .bSoloInvitado = g_bSoloInvitado
        .bRPerfUON = m_bRPerfUON
        .lIdPerfil = lIdPerfil
        .Denominacion = m_sDenominacion
        .AtrBuscarVisible = m_bAtrBuscarVisible
    End With
    
    FiltroProcesosVisor = udtDevolVerProcesosVisor
End Function

Private Sub sdbgMenu_RowLoaded(ByVal Bookmark As Variant)
    
    Select Case sdbgMenu.Columns("ID").CellValue(Bookmark)
    
        Case CTOTABIERTOS, CTOT, CTOTCERRADOS
                
                If sdbgMenu.Columns("ID").CellValue(Bookmark) = CTOT Then
                    If m_iSeleccion <> CTOT Then
                        sdbgMenu.Columns("MENU").CellStyleSet "Bold", sdbgMenu.Row
                    Else
                        sdbgMenu.Columns("MENU").CellStyleSet "SeleccionadoBold", sdbgMenu.Row
                    End If
                End If
                
                If sdbgMenu.Columns("ID").CellValue(Bookmark) = CTOTABIERTOS Then
                    If m_iSeleccion <> CTOTABIERTOS Then
                        sdbgMenu.Columns("MENU").CellStyleSet "Bold", sdbgMenu.Row
                    Else
                        sdbgMenu.Columns("MENU").CellStyleSet "SeleccionadoBold", sdbgMenu.Row
                    End If
                End If
                
                If sdbgMenu.Columns("ID").CellValue(Bookmark) = CTOTCERRADOS Then
                    If m_iSeleccion <> CTOTCERRADOS Then
                        sdbgMenu.Columns("MENU").CellStyleSet "Bold", sdbgMenu.Row
                    Else
                        sdbgMenu.Columns("MENU").CellStyleSet "SeleccionadoBold", sdbgMenu.Row
                    End If
                End If
        Case 1
                If m_iSeleccion = 1 Then
                    sdbgMenu.Columns("MENU").CellStyleSet "Seleccionado", sdbgMenu.Row
                Else
                    sdbgMenu.Columns("MENU").CellStyleSet "Normal", sdbgMenu.Row
                End If
                
                sdbgMenu.Columns("COLOR").CellStyleSet "PendValidar", sdbgMenu.Row
                
        Case 2
                If m_iSeleccion = 2 Then
                    sdbgMenu.Columns("MENU").CellStyleSet "Seleccionado", sdbgMenu.Row
                Else
                    sdbgMenu.Columns("MENU").CellStyleSet "Normal", sdbgMenu.Row
                End If
                
                sdbgMenu.Columns("COLOR").CellStyleSet "PendAsigProve", sdbgMenu.Row
                
        Case 3
                If m_iSeleccion = 3 Then
                    sdbgMenu.Columns("MENU").CellStyleSet "Seleccionado", sdbgMenu.Row
                Else
                    sdbgMenu.Columns("MENU").CellStyleSet "Normal", sdbgMenu.Row
                End If
                
                sdbgMenu.Columns("COLOR").CellStyleSet "PendEnvPet", sdbgMenu.Row
                
        Case 4
                If m_iSeleccion = 4 Then
                    sdbgMenu.Columns("MENU").CellStyleSet "Seleccionado", sdbgMenu.Row
                Else
                    sdbgMenu.Columns("MENU").CellStyleSet "Normal", sdbgMenu.Row
                End If
                
                sdbgMenu.Columns("COLOR").CellStyleSet "EnRecOFe", sdbgMenu.Row
                
        Case 5
                If m_iSeleccion = 5 Then
                    sdbgMenu.Columns("MENU").CellStyleSet "Seleccionado", sdbgMenu.Row
                Else
                    sdbgMenu.Columns("MENU").CellStyleSet "Normal", sdbgMenu.Row
                End If
                
                sdbgMenu.Columns("COLOR").CellStyleSet "PendComObj", sdbgMenu.Row
                
        Case 6
                If m_iSeleccion = 6 Then
                    sdbgMenu.Columns("MENU").CellStyleSet "Seleccionado", sdbgMenu.Row
                Else
                    sdbgMenu.Columns("MENU").CellStyleSet "Normal", sdbgMenu.Row
                End If
                sdbgMenu.Columns("COLOR").CellStyleSet "PendAdjudicar", sdbgMenu.Row
                
        Case 7
                If m_iSeleccion = 7 Then
                    sdbgMenu.Columns("MENU").CellStyleSet "Seleccionado", sdbgMenu.Row
                Else
                    sdbgMenu.Columns("MENU").CellStyleSet "Normal", sdbgMenu.Row
                End If
                sdbgMenu.Columns("COLOR").CellStyleSet "ParcCerrado", sdbgMenu.Row
        
        Case 8
                If m_iSeleccion = 8 Then
                    sdbgMenu.Columns("MENU").CellStyleSet "Seleccionado", sdbgMenu.Row
                Else
                    sdbgMenu.Columns("MENU").CellStyleSet "Normal", sdbgMenu.Row
                End If
                sdbgMenu.Columns("COLOR").CellStyleSet "AdjSinNotificar", sdbgMenu.Row
                
        Case 9
                If m_iSeleccion = 9 Then
                    sdbgMenu.Columns("MENU").CellStyleSet "Seleccionado", sdbgMenu.Row
                Else
                    sdbgMenu.Columns("MENU").CellStyleSet "Normal", sdbgMenu.Row
                End If
                sdbgMenu.Columns("COLOR").CellStyleSet "AdjyNotificado", sdbgMenu.Row
                
        Case 10
                If m_iSeleccion = 10 Then
                    sdbgMenu.Columns("MENU").CellStyleSet "Seleccionado", sdbgMenu.Row
                Else
                    sdbgMenu.Columns("MENU").CellStyleSet "Normal", sdbgMenu.Row
                End If
                sdbgMenu.Columns("COLOR").CellStyleSet "Anulado", sdbgMenu.Row
                
    End Select
    
End Sub

Private Function DevolverEstado(ByVal i As Integer) As String
Dim sestado As String

    Select Case i
    
        Case 1, 2
                sestado = m_sEstados(1)
        Case 3, 4
                sestado = m_sEstados(2)
        Case 5
                sestado = m_sEstados(3)
        Case 6, 7
                sestado = m_sEstados(4)
        Case 8, 9
                sestado = m_sEstados(5)
        Case 10
                sestado = m_sEstados(6)
        Case 12
                sestado = m_sEstados(8)
        Case 13
                sestado = m_sEstados(9)
        Case 20
                sestado = m_sEstados(10)
        Case 11
                sestado = m_sEstados(13)
                
    End Select
        
    DevolverEstado = sestado
        
End Function

''' <summary>
''' Tras mover una columna guarda las nuevas posiciones del grid
''' </summary>
''' <param name="WhatChanged">Que cambia</param>
''' <param name="NewIndex">A donde cambia</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgProcesos_AfterPosChanged(ByVal WhatChanged As Integer, ByVal NewIndex As Integer)
    sdbgProcesos.Refresh
    g_oConfVisor.AnyoPos = sdbgProcesos.Columns("ANYO").Position
    g_oConfVisor.GMN1pos = sdbgProcesos.Columns("GMN1").Position
    g_oConfVisor.Procepos = sdbgProcesos.Columns("COD").Position
    g_oConfVisor.Descrpos = sdbgProcesos.Columns("DESCR").Position
    g_oConfVisor.EstadoPos = sdbgProcesos.Columns("EST").Position
    g_oConfVisor.Responsablepos = sdbgProcesos.Columns("RESP").Position
    g_oConfVisor.AdjDirpos = sdbgProcesos.Columns("ADJDIR").Position
    g_oConfVisor.Ppos = sdbgProcesos.Columns("P").Position
    g_oConfVisor.Opos = sdbgProcesos.Columns("O").Position
    g_oConfVisor.Wpos = sdbgProcesos.Columns("W").Position
    g_oConfVisor.Rpos = sdbgProcesos.Columns("R").Position
    g_oConfVisor.FecApepos = sdbgProcesos.Columns("FECAPE").Position
    g_oConfVisor.FecLimOfepos = sdbgProcesos.Columns("FECLIMOFE").Position
    g_oConfVisor.FecPresPos = sdbgProcesos.Columns("FECPRES").Position
    g_oConfVisor.FecCierrePos = sdbgProcesos.Columns("FECCIERRE").Position
    g_oConfVisor.FecNecPos = sdbgProcesos.Columns("FECNEC").Position
    g_oConfVisor.FecIniPos = sdbgProcesos.Columns("FECINI").Position
    g_oConfVisor.FecFinPos = sdbgProcesos.Columns("FECFIN").Position
    g_oConfVisor.PresPos = sdbgProcesos.Columns("PRES").Position
    g_oConfVisor.ConsumidoPos = sdbgProcesos.Columns("CONS").Position
    g_oConfVisor.ImportePos = sdbgProcesos.Columns("IMP").Position
    g_oConfVisor.AhorroPos = sdbgProcesos.Columns("AHORRO").Position
    g_oConfVisor.Monedapos = sdbgProcesos.Columns("MON").Position
    g_oConfVisor.Destinopos = sdbgProcesos.Columns("DEST").Position
    g_oConfVisor.DestinoDenpos = sdbgProcesos.Columns("DESTDEN").Position
    g_oConfVisor.Pagopos = sdbgProcesos.Columns("PAGO").Position
    g_oConfVisor.ProveedorPos = sdbgProcesos.Columns("PROVE").Position
    g_oConfVisor.ProveedorDenpos = sdbgProcesos.Columns("PROVEDEN").Position
    g_oConfVisor.FecValAperturapos = sdbgProcesos.Columns("FECVALAPERTURA").Position
    g_oConfVisor.PorcentajeAhorropos = sdbgProcesos.Columns("PORCENTAJEAHORRO").Position
    g_oConfVisor.Cambiopos = sdbgProcesos.Columns("CAMBIO").Position
    g_oConfVisor.gsToErpPos = sdbgProcesos.Columns("GSTOERP").Position
    g_oConfVisor.SolicVinculadaPos = sdbgProcesos.Columns("VINCULADA").Position
End Sub

Private Sub sdbgProcesos_ColMove(ByVal ColIndex As Integer, ByVal NewPos As Integer, Cancel As Integer)
    g_oConfVisor.HayCambios = True
    sdbgProcesos.Refresh
End Sub

''' <summary>
''' Tras redimensionar una columna guarda la nueva dimension de la columna
''' </summary>
''' <param name="ColIndex">columna que cambia</param>
''' <param name="Cancel">cancaler el redimensionar</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgProcesos_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
    g_oConfVisor.HayCambios = True
    sdbgProcesos.Refresh
    'Actualizar el tama�o de la columna en g_oconfvisor
    Select Case sdbgProcesos.Columns(ColIndex).Name
    Case "ADJDIR"
        g_oConfVisor.AdjDirWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "AHORRO"
        g_oConfVisor.AhorroWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "ANYO"
        g_oConfVisor.AnyoWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "CONS"
        g_oConfVisor.ConsumidoWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "DESCR"
        g_oConfVisor.DescrWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "DESTDEN"
        g_oConfVisor.DestinoDenWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "DEST"
        g_oConfVisor.DestinoWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "EST"
        g_oConfVisor.EstadoWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "FECAPE"
        g_oConfVisor.FecApeWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "FECCIERRE"
        g_oConfVisor.FecCierreWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "FECFIN"
        g_oConfVisor.FecFinWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "FECINI"
        g_oConfVisor.FecIniWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "FECLIMOFE"
        g_oConfVisor.FecLimOfeWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "FECNEC"
        g_oConfVisor.FecNecWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "FECPRES"
        g_oConfVisor.FecPresWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "GMN1"
        g_oConfVisor.GMN1Width = sdbgProcesos.Columns(ColIndex).Width
    Case "IMP"
        g_oConfVisor.ImporteWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "MON"
        g_oConfVisor.MonedaWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "O"
        g_oConfVisor.OWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "PAGO"
        g_oConfVisor.PagoWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "PRES"
        g_oConfVisor.PresWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "COD"
        g_oConfVisor.ProceWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "PROVEDEN"
        g_oConfVisor.ProveedorDenWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "PROVE"
        g_oConfVisor.ProveedorWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "P"
        g_oConfVisor.PWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "RESP"
        g_oConfVisor.ResponsableWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "W"
        g_oConfVisor.WWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "R"
        g_oConfVisor.RWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "GSTOERP"
        g_oConfVisor.gsToErpWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "FECVALAPERTURA"
        g_oConfVisor.FecValAperturaWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "PORCENTAJEAHORRO"
        g_oConfVisor.PorcentajeAhorroWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "CAMBIO"
        g_oConfVisor.CambioWidth = sdbgProcesos.Columns(ColIndex).Width
    Case "VINCULADA"
        g_oConfVisor.SolicVinculadaWidth = sdbgProcesos.Columns(ColIndex).Width
    End Select
End Sub

Private Sub sdbgProcesos_DblClick()
    Dim vFechaReu As Variant
    Dim sOpcion As String
    Dim oProcesos As CProcesos
    
    If sdbgProcesos.Rows = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Dim oProceso As cProceso
    Dim teserror As TipoErrorSummit
    Dim iEstado As Variant
    
    Set oProceso = oFSGSRaiz.Generar_CProceso()
    
    oProceso.Anyo = sdbgProcesos.Columns("ANYO").Value
    oProceso.GMN1Cod = sdbgProcesos.Columns("GMN1").Value
    oProceso.Cod = sdbgProcesos.Columns("COD").Value
    
    teserror = oProceso.CargarEstado
    
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Screen.MousePointer = vbDefault
        Set oProceso = Nothing
        Exit Sub
    End If
    
    
    iEstado = oProceso.Estado
    
    Set oProceso = Nothing
    
    Select Case sdbgProcesos.Columns("ESTID").Value
    
        Case 1, 2
        
        
            If oUsuarioSummit.Tipo <> Administrador And oUsuarioSummit.EsInvitado = False Then
             If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEConsultar)) Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
             End If
            End If
            
            frmPROCE.g_bVisor = True
            If frmPROCE.picProceAnya.Enabled = False Then
                frmPROCE.g_bVisor = False
                Screen.MousePointer = vbNormal
                oMensajes.MensajeOKOnly 671
                Exit Sub
            End If
        
            frmPROCE.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
            frmPROCE.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
            frmPROCE.GMN1Seleccionado
            Select Case iEstado
                Case 1, 2
                    frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
                Case 12, 13, 20
                    frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
                Case 11
                    frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
                Case Else
                    frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
            End Select
            frmPROCE.sdbcProceCod = sdbgProcesos.Columns("COD").Value
            frmPROCE.sdbcProceCod_Validate False
            DoEvents
            frmPROCE.SetFocus
        
        Case 3, 4
            
            If oUsuarioSummit.Tipo <> Administrador And oUsuarioSummit.EsInvitado = False Then
             If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVEConsultar)) Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
             End If
            End If
            
            frmSELPROVE.g_bVisor = True
            If frmSELPROVE.picProceAnya.Enabled = False Then
                frmSELPROVE.g_bVisor = False
                Screen.MousePointer = vbNormal
                oMensajes.MensajeOKOnly 671
                Exit Sub
            End If
            
            frmSELPROVE.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
            frmSELPROVE.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
            frmSELPROVE.GMN1Seleccionado
            
            Select Case iEstado
                Case 3, 4
                    frmSELPROVE.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
                Case 12, 13, 20
                    frmSELPROVE.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
                Case 11
                    frmSELPROVE.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
                Case Else
                    frmSELPROVE.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
            End Select
            
            frmSELPROVE.sdbcProceCod = sdbgProcesos.Columns("COD").Value
            frmSELPROVE.sdbcProceCod_Validate False
            DoEvents
            frmSELPROVE.SetFocus
        
        Case 5
            
            If oUsuarioSummit.Tipo <> Administrador And oUsuarioSummit.EsInvitado = False Then
             If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEConsulta)) Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
             End If
            End If
            
            frmOFEPet.g_bVisor = True
            frmOFEPet.ProceSelector1.Seleccion = PSPendEnviarPet
            frmOFEPet.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
            frmOFEPet.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
            frmOFEPet.GMN1Seleccionado
            Select Case iEstado
                Case 5
                    frmOFEPet.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
                Case 12, 13, 20
                    frmOFEPet.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
                Case 11
                    frmOFEPet.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
                Case Else
                    frmOFEPet.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
            End Select
            frmOFEPet.sdbcProceCod = sdbgProcesos.Columns("COD").Value
            frmOFEPet.sdbcProceCod_Validate False
            DoEvents
            frmOFEPet.SetFocus
            
        Case 8, 9
            
            If oUsuarioSummit.Tipo <> Administrador And oUsuarioSummit.EsInvitado = False Then
             If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEConsulta)) Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
             End If
            End If
            
            frmOFEPet.g_bVisor = True
            frmOFEPet.ProceSelector1.Seleccion = PSPendComObj
            frmOFEPet.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
            frmOFEPet.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
            frmOFEPet.GMN1Seleccionado
            Select Case iEstado
                Case 8, 9
                    frmOFEPet.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
                Case 12, 13, 20
                    frmOFEPet.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
                Case 11
                    frmOFEPet.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
                Case Else
                    frmOFEPet.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
            End Select
            frmOFEPet.sdbcProceCod = sdbgProcesos.Columns("COD").Value
            frmOFEPet.sdbcProceCod_Validate False
            DoEvents
            frmOFEPet.SetFocus
        
        Case 12
            
            If oUsuarioSummit.Tipo <> Administrador And oUsuarioSummit.EsInvitado = False Then
             If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEConsulta)) Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
             End If
            End If
            
            frmOFEPet.g_bVisor = True
            frmOFEPet.ProceSelector1.Seleccion = PSPendNotAdj
            frmOFEPet.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
            frmOFEPet.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
            frmOFEPet.GMN1Seleccionado
            Select Case iEstado
                Case 12
                    frmOFEPet.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
                Case 13, 20
                    frmOFEPet.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
                Case Else
                    frmOFEPet.ProceSelector1.Seleccion = PSSeleccion.PSTodos
            End Select
            frmOFEPet.sdbcProceCod = sdbgProcesos.Columns("COD").Value
            frmOFEPet.sdbcProceCod_Validate False
            DoEvents
            frmOFEPet.SetFocus
            
        Case 6, 7
            'si tiene permisos de consulta en la comparativa,muestra la comparativa (o el panel dependiendo si es de AdjDir),
            'si no tiene estos permisos muestra recepci�n de ofertas (si tiene el permiso de consulta)
            If sdbgProcesos.Columns("ADJDIR").Value = 1 Then
                sOpcion = "ADJ"
            Else
                sOpcion = "RESREU"
            End If
            If oUsuarioSummit.Tipo <> Administrador And oUsuarioSummit.EsInvitado = False Then
                If sdbgProcesos.Columns("ADJDIR").Value = 1 And oUsuarioSummit.EsInvitado = False Then
                    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjConsulta)) Is Nothing Then sOpcion = ""
                Else
                    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREUConsultar)) Is Nothing Then sOpcion = ""
                End If
                If sOpcion = "" Then
                    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEConsultar)) Is Nothing Then
                       Screen.MousePointer = vbNormal
                       Exit Sub
                    Else
                        sOpcion = "OFEREC"
                    End If
                End If
            End If
            If sOpcion = "RESREU" Then
                vFechaReu = m_oGestorReuniones.FechaUltimaReunionProceso(sdbgProcesos.Columns("ANYO").Value, sdbgProcesos.Columns("GMN1").Value, sdbgProcesos.Columns("COD").Value)
                If IsNull(vFechaReu) Then   'Si todav�a no se ha metido el proceso en una reuni�n saca un mensaje y abre la comparativa
                    Set oProcesos = oFSGSRaiz.generar_CProcesos
                    oProcesos.CargarTodosLosProcesosDesde 1, conofertas, PreadjYConObjNotificados, TipoOrdenacionProcesos.OrdPorCod, sdbgProcesos.Columns("ANYO").Value, sdbgProcesos.Columns("GMN1").Value, , , , val(sdbgProcesos.Columns("COD").Value), , True, False
                    If oProcesos.Count = 0 Then
                        sOpcion = "OFEREC"
                    Else
                        oMensajes.ProcesoTodaviaSinReunion
                        sOpcion = "ADJ"
                    End If
                    Set oProcesos = Nothing
                End If
            ElseIf sOpcion = "ADJ" Then  'Comprueba que el proceso tenga ofertas
                Set oProcesos = oFSGSRaiz.generar_CProcesos
                oProcesos.CargarTodosLosProcesosDesde 1, conofertas, PreadjYConObjNotificados, TipoOrdenacionProcesos.OrdPorCod, sdbgProcesos.Columns("ANYO").Value, sdbgProcesos.Columns("GMN1").Value, , , , val(sdbgProcesos.Columns("COD").Value), , True, False
                If oProcesos.Count = 0 Then
                    sOpcion = "OFEREC"
                End If
                Set oProcesos = Nothing
            End If
            
            Select Case sOpcion
                Case "ADJ"
                    frmADJ.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
                    frmADJ.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
                    DoEvents
                    frmADJ.GMN1Seleccionado
                    Select Case iEstado
                        Case 10
                            frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
                        Case 12, 13, 20
                            frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
                        Case 11
                            frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
                        Case Else
                            frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
                    End Select
                    DoEvents
                    frmADJ.sdbcProceCod = sdbgProcesos.Columns("COD").Value
                    frmADJ.sdbcProceCod_Validate False
                    DoEvents
                    frmADJ.SetFocus
                
                Case "RESREU"
                    
                    frmRESREU.m_bRespetarCombo = True
                    frmRESREU.sdbcFecReu.Value = vFechaReu
                    frmRESREU.sdbcFecReu.Columns(0).Value = vFechaReu
                    frmRESREU.sdbcFecReu = Format(vFechaReu, "short date") & " " & Format(vFechaReu, "short time")
                    frmRESREU.m_bRespetarCombo = False
                    frmRESREU.sdbcFecReu_Validate False
                    DoEvents
                    frmRESREU.sdbcProceCod.Value = ""
                    frmRESREU.sdbcProceCod.DroppedDown = True
                    frmRESREU.sdbcProceCod.Columns("ANYO").Value = sdbgProcesos.Columns("ANYO").Value
                    frmRESREU.sdbcProceCod.Columns("GMN1").Value = sdbgProcesos.Columns("GMN1").Value
                    frmRESREU.sdbcProceCod.Columns("COD").Value = sdbgProcesos.Columns("COD").Value
                    frmRESREU.sdbcProceCod.DroppedDown = False
                    frmRESREU.sdbcProceCod = CStr(sdbgProcesos.Columns("ANYO").Value) & "/" & CStr(sdbgProcesos.Columns("GMN1").Value) & "/" & CStr(sdbgProcesos.Columns("COD").Value) & " " & sdbgProcesos.Columns("DESCR").Value
                    DoEvents
                    frmRESREU.ProcesoSeleccionado
                    DoEvents
                    frmRESREU.CargarComboProcesos
                    frmRESREU.SetFocus

                Case "OFEREC"
                    frmOFERec.g_bVisor = True
                    If frmOFERec.fraProce.Enabled = False Then
                        frmOFERec.g_bVisor = False
                        Screen.MousePointer = vbNormal
                        oMensajes.MensajeOKOnly 671
                        Exit Sub
                    End If
                    
                    frmOFERec.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
                    frmOFERec.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
                    frmOFERec.GMN1Seleccionado
                    Select Case iEstado
                        Case 6, 7
                            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
                        Case 12, 13, 20
                            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
                        Case 11
                            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
                        Case Else
                            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
                    End Select
                    frmOFERec.sdbcProceCod = sdbgProcesos.Columns("COD").Value
                    frmOFERec.sdbcProceCod_Validate False
                    DoEvents
                    frmOFERec.SetFocus
            End Select
            
        Case 10, 11
            
            If oUsuarioSummit.Tipo <> Administrador And oUsuarioSummit.EsInvitado = False Then
             If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjConsulta)) Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
             End If
            End If
            
            If sdbgProcesos.Columns("ADJDIR").Value = 1 Then
                If sdbgProcesos.Columns("ESTID").Value = 11 Then
                    If oUsuarioSummit.Tipo <> Administrador Then
                        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjConsulta)) Is Nothing Then
                            Screen.MousePointer = vbNormal
                            oMensajes.SeleccioneOpci�nDeMenu
                            Exit Sub
                        End If
                    End If
                    frmADJ.ProceSelector1.Seleccion = PSParcialCerrados
                End If
                
                frmADJ.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
                frmADJ.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
                DoEvents
                frmADJ.GMN1Seleccionado
                Select Case iEstado
                    Case 10
                        frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
                    Case 12, 13, 20
                        frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
                    Case 11
                        frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
                    Case Else
                        frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
                End Select
                DoEvents
                frmADJ.sdbcProceCod = sdbgProcesos.Columns("COD").Value
                frmADJ.sdbcProceCod_Validate False
                DoEvents
                frmADJ.SetFocus
                
            Else
                If oUsuarioSummit.Tipo <> Administrador And oUsuarioSummit.EsInvitado = False Then
                    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREUConsultar)) Is Nothing Then
                        Screen.MousePointer = vbNormal
                        If sdbgProcesos.Columns("ESTID").Value = 11 Then
                            oMensajes.SeleccioneOpci�nDeMenu
                        End If
                        Exit Sub
                    End If
                End If
                    
                vFechaReu = m_oGestorReuniones.FechaUltimaReunionProceso(sdbgProcesos.Columns("ANYO").Value, sdbgProcesos.Columns("GMN1").Value, sdbgProcesos.Columns("COD").Value)
            
                If Not IsNull(vFechaReu) Then
                
                    frmRESREU.m_bRespetarCombo = True
                    frmRESREU.sdbcFecReu.Value = vFechaReu
                    frmRESREU.sdbcFecReu.Columns(0).Value = vFechaReu
                    frmRESREU.sdbcFecReu = Format(vFechaReu, "short date") & " " & Format(vFechaReu, "short time")
                    frmRESREU.m_bRespetarCombo = False
                    frmRESREU.sdbcFecReu_Validate False
                    DoEvents
                    frmRESREU.sdbcProceCod.Value = ""
                    frmRESREU.sdbcProceCod.DroppedDown = True
                    frmRESREU.sdbcProceCod.Columns("ANYO").Value = sdbgProcesos.Columns("ANYO").Value
                    frmRESREU.sdbcProceCod.Columns("GMN1").Value = sdbgProcesos.Columns("GMN1").Value
                    frmRESREU.sdbcProceCod.Columns("COD").Value = sdbgProcesos.Columns("COD").Value
                    frmRESREU.sdbcProceCod.DroppedDown = False
                    frmRESREU.sdbcProceCod = CStr(sdbgProcesos.Columns("ANYO").Value) & "/" & CStr(sdbgProcesos.Columns("GMN1").Value) & "/" & CStr(sdbgProcesos.Columns("COD").Value) & " " & sdbgProcesos.Columns("DESCR").Value
                    DoEvents
                    frmRESREU.ProcesoSeleccionado
                    DoEvents
                    frmRESREU.CargarComboProcesos
                    frmRESREU.SetFocus
                    
                Else
                    oMensajes.ProcesoTodaviaSinReunion
                End If
                
            End If
            
        Case 13, 20
            
            If oUsuarioSummit.Tipo <> Administrador And oUsuarioSummit.EsInvitado = False Then
             If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEConsultar)) Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
             End If
            End If
            
            frmPROCE.g_bVisor = True
            If frmPROCE.picProceAnya.Enabled = False Then
                frmPROCE.g_bVisor = False
                Screen.MousePointer = vbNormal
                oMensajes.MensajeOKOnly 671
                Exit Sub
            End If
            
            frmPROCE.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
            frmPROCE.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
            frmPROCE.GMN1Seleccionado
            Select Case iEstado
                Case 13, 20
                    frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
                Case Else
                    frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSTodos
            End Select
            frmPROCE.sdbcProceCod = sdbgProcesos.Columns("COD").Value
            frmPROCE.sdbcProceCod_Validate False
            DoEvents
            frmPROCE.SetFocus
        
    End Select
    
    Screen.MousePointer = vbNormal
        
End Sub
Public Sub mnuVisorApertura()


    frmPROCE.g_bVisor = True
    If frmPROCE.picProceAnya.Enabled = False Then
        frmPROCE.g_bVisor = False
        oMensajes.MensajeOKOnly 671
        Exit Sub
    End If
        
    Screen.MousePointer = vbHourglass

    Select Case miEstado
    
        Case 1, 2
            frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
        
        Case 12, 13, 20
            frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
        
        Case 11
            frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
        
        Case Else
            frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
    End Select
    
    frmPROCE.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
    frmPROCE.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
    frmPROCE.GMN1Seleccionado
    frmPROCE.sdbcProceCod = sdbgProcesos.Columns("COD").Value
    frmPROCE.sdbcProceCod_Validate False
    DoEvents
    frmPROCE.SetFocus
    Screen.MousePointer = vbNormal
End Sub

Public Sub mnuVisorSelProve()
    frmSELPROVE.g_bVisor = True
    If frmSELPROVE.picProceAnya.Enabled = False Then
        frmSELPROVE.g_bVisor = False
        oMensajes.MensajeOKOnly 671
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    
    Select Case miEstado
    
        Case 3, 4
            frmSELPROVE.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
        
        Case 12, 13, 20
            frmSELPROVE.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
        
        Case 11
            frmSELPROVE.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
        
        Case Else
            frmSELPROVE.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
    End Select

    'Debido a que es necesario refrescar el formulario y que est�n todos los componentes de la grid
    DoEvents
    DoEvents
    
    frmSELPROVE.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
    frmSELPROVE.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
    frmSELPROVE.GMN1Seleccionado
    frmSELPROVE.sdbcProceCod = sdbgProcesos.Columns("COD").Value
    frmSELPROVE.sdbcProceCod_Validate False
    DoEvents
    frmSELPROVE.SetFocus
    
    Screen.MousePointer = vbNormal
End Sub

Public Sub mnuVisorComuniProve()
    Screen.MousePointer = vbHourglass
    frmOFEPet.g_bVisor = True

    Select Case miEstado
        
        Case 5
            frmOFEPet.ProceSelector1.Seleccion = PSPendEnviarPet
            
        Case 8, 9
            frmOFEPet.ProceSelector1.Seleccion = PSPendComObj
        
        Case 12
            frmOFEPet.ProceSelector1.Seleccion = PSPendNotAdj
            
        Case 6, 7, 10
            frmOFEPet.ProceSelector1.Seleccion = PSAbiertos
            
        Case 13, 20
            frmOFEPet.ProceSelector1.Seleccion = PSCerrados
    
        Case 11
            frmOFEPet.ProceSelector1.Seleccion = PSParcialCerrados
        
    End Select
    
    frmOFEPet.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
    frmOFEPet.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
    frmOFEPet.GMN1Seleccionado
    frmOFEPet.sdbcProceCod = sdbgProcesos.Columns("COD").Value
    frmOFEPet.sdbcProceCod_Validate False
    frmOFEPet.SetFocus
            
    Screen.MousePointer = vbNormal
            
End Sub

Public Sub mnuVisorPanel()
    Dim vFechaReu As Variant
            
    Screen.MousePointer = vbHourglass
    
    vFechaReu = m_oGestorReuniones.FechaUltimaReunionProceso(sdbgProcesos.Columns("ANYO").Value, sdbgProcesos.Columns("GMN1").Value, sdbgProcesos.Columns("COD").Value)

    If Not IsNull(vFechaReu) Then
        frmRESREU.m_bRespetarCombo = True
        frmRESREU.sdbcFecReu.Value = vFechaReu
        frmRESREU.sdbcFecReu.Columns(0).Value = vFechaReu
        frmRESREU.sdbcFecReu = Format(vFechaReu, "short date") & " " & Format(vFechaReu, "short time")
        frmRESREU.m_bRespetarCombo = False
        frmRESREU.sdbcFecReu_Validate False
        DoEvents
        frmRESREU.sdbcProceCod.Value = ""
        frmRESREU.sdbcProceCod.DroppedDown = True
        frmRESREU.sdbcProceCod.Columns("ANYO").Value = sdbgProcesos.Columns("ANYO").Value
        frmRESREU.sdbcProceCod.Columns("GMN1").Value = sdbgProcesos.Columns("GMN1").Value
        frmRESREU.sdbcProceCod.Columns("COD").Value = sdbgProcesos.Columns("COD").Value
        frmRESREU.sdbcProceCod.DroppedDown = False
        frmRESREU.sdbcProceCod = CStr(sdbgProcesos.Columns("ANYO").Value) & "/" & CStr(sdbgProcesos.Columns("GMN1").Value) & "/" & CStr(sdbgProcesos.Columns("COD").Value) & " " & sdbgProcesos.Columns("DESCR").Value
        DoEvents
        frmRESREU.ProcesoSeleccionado
        DoEvents
        frmRESREU.CargarComboProcesos
        frmRESREU.SetFocus
    Else
        oMensajes.ProcesoTodaviaSinReunion
    End If
            
    Screen.MousePointer = vbNormal
End Sub

Public Sub mnuVisorComparativa()
    Screen.MousePointer = vbHourglass
    
    Select Case miEstado
        Case 10
            If sdbgProcesos.Columns("ADJDIR").Value = 1 Then
                'Si permite adjudicaci�n directa estar� pendiente
                frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
            Else
                'Si es un proceso de reuni�n tiene que estar abierto
                frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
            End If
        
        Case 12, 13, 20
            frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
        
        Case 11
            frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
        
        Case Else
            frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
    End Select
    
    frmADJ.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
    frmADJ.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
    frmADJ.GMN1Seleccionado
    frmADJ.sdbcProceCod = sdbgProcesos.Columns("COD").Value
    DoEvents
    frmADJ.sdbcProceCod_Validate False
    DoEvents
    frmADJ.SetFocus
    
    Screen.MousePointer = vbNormal
End Sub

Public Sub mnuVisorRecOfe()
    frmOFERec.g_bVisor = True
    If frmOFERec.fraProce.Enabled = False Then
        frmOFERec.g_bVisor = False
        oMensajes.MensajeOKOnly 671
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    Select Case miEstado
    
        Case 6
            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
        
        Case 12, 13, 20
            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
        
        Case 11
            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
        
        Case Else
            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
    End Select
            
    frmOFERec.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
    frmOFERec.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
    frmOFERec.GMN1Seleccionado
    frmOFERec.sdbcProceCod = sdbgProcesos.Columns("COD").Value
    frmOFERec.sdbcProceCod_Validate False
    DoEvents
    frmOFERec.SetFocus
            
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgProcesos_HeadClick(ByVal ColIndex As Integer)
    Dim iOrdenAnt As Integer
    Dim oAtrBuscar As CAtributosBuscar
    'Ordena la grid seg�n la cabecera en la que hayamos pulsado
    Screen.MousePointer = vbHourglass
    
    'BLoquea la pantalla hasta que se cargue la grid y los men�s
    LockWindowUpdate Me.hWnd
    
    iOrdenAnt = gParametrosInstalacion.giOrdenVisor
    
    OrdenSeleccionado sdbgProcesos.Columns(ColIndex).Name
    
    'Se cambia el sentido de ordenaci�n del orden y por lo tanto tambi�n la flecha en la cabecera de la columna
    If iOrdenAnt = gParametrosInstalacion.giOrdenVisor Then
        gParametrosInstalacion.gbOrdenVisorAscendente = Not gParametrosInstalacion.gbOrdenVisorAscendente
    Else
        gParametrosInstalacion.gbOrdenVisorAscendente = True
    End If
    'Coleccion que recoge los atributos por los que se quiere filtrar y el ambito en el que se quiere que se cumplan
    Set oAtrBuscar = MoverGridAtrBuscarAColleccion(sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0))
    Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(FiltroProcesosVisor(True), m_sEstadoIntegracion(), oAtrBuscar)
                          
    If m_adoRecordset Is Nothing Then
        'Desbloquea la pantalla
        LockWindowUpdate 0&
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    Set sdbgProcesos.DataSource = m_adoRecordset
    
      
    'Desbloquea la pantalla
    LockWindowUpdate 0&
                    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgProcesos_InitColumnProps()
    
    If gParametrosInstalacion.TCPendValidar <> "" Then
        sdbgProcesos.StyleSets.Item("PendValidar").Backcolor = CLng(gParametrosInstalacion.TCPendValidar)
    End If
    
    If gParametrosInstalacion.TCPendAsigProve <> "" Then
        sdbgProcesos.StyleSets.Item("PendAsigProve").Backcolor = CLng(gParametrosInstalacion.TCPendAsigProve)
    End If
    
    If gParametrosInstalacion.TCPendEnvPet <> "" Then
        sdbgProcesos.StyleSets.Item("PendEnvPet").Backcolor = CLng(gParametrosInstalacion.TCPendEnvPet)
    End If
    
    If gParametrosInstalacion.TCEnRecOFe <> "" Then
        sdbgProcesos.StyleSets.Item("EnRecOfe").Backcolor = CLng(gParametrosInstalacion.TCEnRecOFe)
    End If
    
    If gParametrosInstalacion.TCPendComObj <> "" Then
        sdbgProcesos.StyleSets.Item("PendComObj").Backcolor = CLng(gParametrosInstalacion.TCPendComObj)
    End If
    
    If gParametrosInstalacion.TCPendAdj <> "" Then
        sdbgProcesos.StyleSets.Item("PendAdjudicar").Backcolor = CLng(gParametrosInstalacion.TCPendAdj)
    End If
    
    If gParametrosInstalacion.TCAdjSinNotificar <> "" Then
        sdbgProcesos.StyleSets.Item("AdjSinNotificar").Backcolor = CLng(gParametrosInstalacion.TCAdjSinNotificar)
    End If
    
    If gParametrosInstalacion.TCAdjyNotificado <> "" Then
        sdbgProcesos.StyleSets.Item("AdjYNotificado").Backcolor = CLng(gParametrosInstalacion.TCAdjyNotificado)
    End If
    
    If gParametrosInstalacion.TCAnulado <> "" Then
        sdbgProcesos.StyleSets.Item("Anulado").Backcolor = CLng(gParametrosInstalacion.TCAnulado)
    End If
  
    If gParametrosInstalacion.TCParcCerr <> "" Then
        sdbgProcesos.StyleSets.Item("ParcCerrado").Backcolor = CLng(gParametrosInstalacion.TCParcCerr)
    End If
 End Sub

Private Sub sdbgProcesos_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'Se visualizar� la picture que muestra el tooltiptext de las columnas P,O y W:
    Dim iIndex As Integer
    
    If sdbgProcesos.WhereIs(X, Y) = ssWhereIsColumnHeading Then
        iIndex = sdbgProcesos.ColContaining(X, Y)
        
        If sdbgProcesos.Columns(iIndex).Name = "P" Then
            lblToolTip.caption = m_stxtToolTipP
            picToolTip.Left = X + 600
            picToolTip.Height = lblToolTip.Height
            picToolTip.Width = lblToolTip.Width
            picToolTip.Top = sdbgProcesos.Top - picToolTip.Height + 10
            picToolTip.Visible = True
            
        ElseIf sdbgProcesos.Columns(iIndex).Name = "O" Then
            lblToolTip.caption = m_stxtToolTipO
            picToolTip.Left = X + 600
            picToolTip.Height = lblToolTip.Height
            picToolTip.Width = lblToolTip.Width
            picToolTip.Top = sdbgProcesos.Top - picToolTip.Height + 10
            picToolTip.Visible = True
            
        ElseIf sdbgProcesos.Columns(iIndex).Name = "W" Then
            lblToolTip.caption = m_stxtToolTipW
            picToolTip.Left = X + 600
            picToolTip.Height = lblToolTip.Height
            picToolTip.Width = lblToolTip.Width
            picToolTip.Top = sdbgProcesos.Top - picToolTip.Height + 10
            picToolTip.Visible = True
        Else
            picToolTip.Visible = False
        End If
    Else
        picToolTip.Visible = False
    End If

End Sub

Private Sub sdbgProcesos_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim i As Integer
Dim oProceso As cProceso
Dim teserror As TipoErrorSummit
Dim sestado As String
Dim iIndex As Integer
    
    If (Button = vbRightButton) And (sdbgProcesos.WhereIs(X, Y) = ssWhereIsColumnHeading) Then
        'Si se ha pulsado sobre la cabecera de una columna muestra los men�s de configuraci�n de los campos y orden del visor
        iIndex = sdbgProcesos.ColContaining(X, Y)
        frmMenuEst.g_sNomCampo = sdbgProcesos.Columns(iIndex).Name
        PopupMenu frmMenuEst.mnuPOPUPConfig
        
    Else
        'Ha pulsado en una fila de la grid o con el bot�n izquierdo del rat�n
        If sdbgProcesos.Rows = 0 Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        Set oProceso = oFSGSRaiz.Generar_CProceso()
        
        oProceso.Anyo = sdbgProcesos.Columns("ANYO").Value
        oProceso.GMN1Cod = sdbgProcesos.Columns("GMN1").Value
        oProceso.Cod = sdbgProcesos.Columns("COD").Value
        teserror = oProceso.CargarEstado
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            Screen.MousePointer = vbDefault
            Set oProceso = Nothing
            Exit Sub
        End If
        
        Screen.MousePointer = vbDefault
        
        miEstado = oProceso.Estado
        mbSubasta = oProceso.ModoSubasta
        
        Set oProceso = Nothing
    
        sestado = DevolverEstado(miEstado)
    
        sdbgProcesos.Columns("EST").Value = sestado
        sdbgProcesos.Columns("ESTID").Value = miEstado
        sdbgProcesos.Update
        
        Select Case miEstado
        
            Case 1, 2
                    
                    For i = 0 To sdbgProcesos.Columns.Count - 1
                        sdbgProcesos.Columns(i).CellStyleSet "PendValidar", sdbgProcesos.Row
                    Next
                    
            Case 3, 4
                    For i = 0 To sdbgProcesos.Columns.Count - 1
                        sdbgProcesos.Columns(i).CellStyleSet "PendAsigProve", sdbgProcesos.Row
                    Next
                    
            Case 5
                    For i = 0 To sdbgProcesos.Columns.Count - 1
                        sdbgProcesos.Columns(i).CellStyleSet "PendEnvPet", sdbgProcesos.Row
                    Next
                    
                    
            Case 6, 7
                    For i = 0 To sdbgProcesos.Columns.Count - 1
                        sdbgProcesos.Columns(i).CellStyleSet "EnRecOFe", sdbgProcesos.Row
                    Next
                    
                    
            Case 8, 9
                    For i = 0 To sdbgProcesos.Columns.Count - 1
                        sdbgProcesos.Columns(i).CellStyleSet "PendComObj", sdbgProcesos.Row
                    Next
                    
            Case 10
                    For i = 0 To sdbgProcesos.Columns.Count - 1
                        sdbgProcesos.Columns(i).CellStyleSet "PendAdjudicar", sdbgProcesos.Row
                    Next
                    
            Case 11
                    For i = 0 To sdbgProcesos.Columns.Count - 1
                        sdbgProcesos.Columns(i).CellStyleSet "ParcCerrado", sdbgProcesos.Row
                    Next
                    
            Case 12
                    For i = 0 To sdbgProcesos.Columns.Count - 1
                        sdbgProcesos.Columns(i).CellStyleSet "AdjSinNotificar", sdbgProcesos.Row
                    Next
                    
            Case 13
                    For i = 0 To sdbgProcesos.Columns.Count - 1
                        sdbgProcesos.Columns(i).CellStyleSet "AdjYNotificado", sdbgProcesos.Row
                    Next
                                    
            Case 20
                    For i = 0 To sdbgProcesos.Columns.Count - 1
                        sdbgProcesos.Columns(i).CellStyleSet "Anulado", sdbgProcesos.Row
                    Next
                    
        End Select
        
        If Button = 2 Then
            mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
            mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
            DoEvents
    
            ConfigurarMenuVisor
            PopupMenu MDI.mnuVisor
        End If
    End If
    
End Sub

''' <summary>Configura el men� contextual</summary>
''' <remarks>Llamada desde: PicNuevas_MouseUp; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 22/06/2011</revision>

Private Sub ConfigurarMenuVisor()
    MDI.mnuVisorComparativa.Enabled = True
    MDI.mnuVisorComuniProve.Enabled = True
    MDI.mnuVisorPanel.Enabled = True
    MDI.mnuVisorRecOfe.Enabled = True
    MDI.mnuVisorSelProve.Enabled = True
            
    If sdbgProcesos.Columns("ADJDIR").Value = 1 Then
        MDI.mnuVisorPanel.Enabled = False
    End If
    
    
    Select Case miEstado
        
        Case 1, 2 'Pendiente de validar apertura
            
            MDI.mnuVisorComparativa.Enabled = False
            MDI.mnuVisorComuniProve.Enabled = False
            MDI.mnuVisorPanel.Enabled = False
            MDI.mnuVisorRecOfe.Enabled = False
            MDI.mnuVisorSelProve.Enabled = False
             
            
        Case 3, 4 'Pendiente de validar asignacion
            
            MDI.mnuVisorComparativa.Enabled = False
            MDI.mnuVisorComuniProve.Enabled = False
            MDI.mnuVisorPanel.Enabled = False
            MDI.mnuVisorRecOfe.Enabled = False
                    
        Case 5, 6
            If Not (mbSubasta And miEstado > conasignacionvalida) Then
                MDI.mnuVisorComparativa.Enabled = False
                MDI.mnuVisorPanel.Enabled = False
            End If
    End Select

End Sub

''' <summary>
''' Estilo que se le va a dar a la fila cuando se cargue.
''' </summary>
''' <param name="Bookmark">Bookmark de la fila</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgProcesos_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    Dim sOrden As String
    Dim dtFecLimOfe As Date
    Dim dtHoraLimOfe As Date

    With sdbgProcesos
        'Mostrar la fecha l�mite de ofertas en la zona horaria del usuario
        If .Columns("FECLIMOFE").Value <> "" Then
            ConvertirUTCaTZ DateValue(.Columns("FECLIMOFE").Value), TimeValue(.Columns("FECLIMOFE").Value), m_vTZHora, dtFecLimOfe, dtHoraLimOfe
            .Columns("FECLIMOFE").Value = CStr(dtFecLimOfe) & " " & CStr(dtHoraLimOfe)
        End If
        
        Select Case .Columns("ESTID").CellValue(Bookmark)
            Case 1, 2
                    For i = 0 To .Columns.Count - 1
                        .Columns(i).CellStyleSet "PendValidar", sdbgProcesos.Row
                    Next
                    DoEvents
            Case 3, 4
                    For i = 0 To .Columns.Count - 1
                        .Columns(i).CellStyleSet "PendAsigProve", sdbgProcesos.Row
                    Next
                    
            Case 5
                    For i = 0 To .Columns.Count - 1
                        .Columns(i).CellStyleSet "PendEnvPet", sdbgProcesos.Row
                    Next
                    
                    
            Case 6, 7
                    For i = 0 To .Columns.Count - 1
                        .Columns(i).CellStyleSet "EnRecOFe", sdbgProcesos.Row
                    Next
                    
                    
            Case 8, 9
                    For i = 0 To .Columns.Count - 1
                        .Columns(i).CellStyleSet "PendComObj", sdbgProcesos.Row
                    Next
                    
            Case 10
                    For i = 0 To .Columns.Count - 1
                        .Columns(i).CellStyleSet "PendAdjudicar", sdbgProcesos.Row
                    Next
                    
            Case 11
                    For i = 0 To .Columns.Count - 1
                        .Columns(i).CellStyleSet "ParcCerrado", sdbgProcesos.Row
                    Next
                    
            Case 12
                    For i = 0 To .Columns.Count - 1
                        .Columns(i).CellStyleSet "AdjSinNotificar", sdbgProcesos.Row
                    Next
                    
            Case 13
                    For i = 0 To .Columns.Count - 1
                        .Columns(i).CellStyleSet "AdjYNotificado", sdbgProcesos.Row
                    Next
                                    
            Case 20
                    For i = 0 To .Columns.Count - 1
                        .Columns(i).CellStyleSet "Anulado", sdbgProcesos.Row
                    Next
                    
        End Select
                
        'Orden del visor.Se marcar� con un tri�ngulo ascendente o descendente.
        For i = 0 To .Columns.Count - 1
            .Columns(i).HeadStyleSet = ""
        Next i
        
        If gParametrosInstalacion.gbOrdenVisorAscendente = True Then
            sOrden = "Asc"
        Else
            sOrden = "Desc"
        End If
        Select Case gParametrosInstalacion.giOrdenVisor
            Case TipoOrdenacionProcesos.OrdPorMaterial
                .Columns("GMN1").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorCod
                .Columns("COD").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorDen
                .Columns("DESCR").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorEstado
                .Columns("EST").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorResponsable
                .Columns("RESP").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorAdjDir
                .Columns("ADJDIR").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorfechaapertura
                .Columns("FECAPE").HeadStyleSet = "CabeceraOrden" & sOrden
                
            Case TipoOrdenacionProcesos.OrdPorFechaValidezOfertas
                .Columns("FECLIMOFE").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorFechaPresentacion
                .Columns("FECPRES").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorFechaCierre
                .Columns("FECCIERRE").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorfechanecesidad
                .Columns("FECNEC").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorFechaIniSuministro
                .Columns("FECINI").HeadStyleSet = "CabeceraOrden" & sOrden
                
            Case TipoOrdenacionProcesos.OrdPorFechaFinSuministro
                .Columns("FECFIN").HeadStyleSet = "CabeceraOrden" & sOrden
                
            Case TipoOrdenacionProcesos.OrdPorMoneda
                .Columns("MON").HeadStyleSet = "CabeceraOrden" & sOrden
                
            Case TipoOrdenacionProcesos.OrdPorDestino
                .Columns("DEST").HeadStyleSet = "CabeceraOrden" & sOrden
                
            Case TipoOrdenacionProcesos.OrdPorDestinoDen
                .Columns("DESTDEN").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorFPago
                .Columns("PAGO").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorProveAct
                .Columns("PROVE").HeadStyleSet = "CabeceraOrden" & sOrden
                
            Case TipoOrdenacionProcesos.OrdPorProveActDen
                .Columns("PROVEDEN").HeadStyleSet = "CabeceraOrden" & sOrden
                
            Case TipoOrdenacionProcesos.OrdPorP
                .Columns("P").HeadStyleSet = "CabeceraOrden" & sOrden
                
            Case TipoOrdenacionProcesos.OrdPorO
                .Columns("O").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorW
                .Columns("W").HeadStyleSet = "CabeceraOrden" & sOrden
                
            Case TipoOrdenacionProcesos.OrdPorR
                .Columns("R").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorPresTotal
                .Columns("PRES").HeadStyleSet = "CabeceraOrden" & sOrden
                
            Case TipoOrdenacionProcesos.OrdPorConsumido
                .Columns("CONS").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorAdjudicado
                .Columns("IMP").HeadStyleSet = "CabeceraOrden" & sOrden
            
            Case TipoOrdenacionProcesos.OrdPorAhorro
                .Columns("AHORRO").HeadStyleSet = "CabeceraOrden" & sOrden
            Case TipoOrdenacionProcesos.OrdPorGsToErp
                .Columns("GSTOERP").HeadStyleSet = "CabeceraOrden" & sOrden
                    
            Case TipoOrdenacionProcesos.OrdPorFecValApertura
                .Columns("FECVALAPERTURA").HeadStyleSet = "CabeceraOrden" & sOrden
                    
            Case TipoOrdenacionProcesos.OrdPorPorcentajeAhorro
                .Columns("PORCENTAJEAHORRO").HeadStyleSet = "CabeceraOrden" & sOrden
        
            Case TipoOrdenacionProcesos.OrdPorCambio
                .Columns("CAMBIO").HeadStyleSet = "CabeceraOrden" & sOrden
                
            Case TipoOrdenacionProcesos.OrdPorSolicVinculada
                .Columns("VINCULADA").HeadStyleSet = "CabeceraOrden" & sOrden
        End Select
    End With
End Sub

Private Sub MostrarSeleccion(ByVal i As Integer)
Dim ij As Integer

    m_bRespetarRowColChange = True
    
    sdbgMenu.MoveFirst
    
    For ij = 0 To sdbgMenu.Rows - 1
            
        If ij = i Then
            If sdbgMenu.Columns("ID").Value = CTOT Or sdbgMenu.Columns("ID").Value = CTOTCERRADOS Or sdbgMenu.Columns("ID").Value = CTOTABIERTOS Then
                sdbgMenu.Columns("MENU").CellStyleSet "SeleccionadoBold", sdbgMenu.Row
            Else
                sdbgMenu.Columns("MENU").CellStyleSet "Seleccionado", sdbgMenu.Row
            End If
        Else
            If sdbgMenu.Columns("ID").Value = CTOT Or sdbgMenu.Columns("ID").Value = CTOTCERRADOS Or sdbgMenu.Columns("ID").Value = CTOTABIERTOS Then
                sdbgMenu.Columns("MENU").CellStyleSet "Bold", sdbgMenu.Row
            Else
                sdbgMenu.Columns("MENU").CellStyleSet "Normal", sdbgMenu.Row
            End If
        End If
        
        sdbgMenu.MoveNext
    Next
    
    m_bRespetarRowColChange = False
    
    sdbgMenu.Row = i
End Sub

Private Sub Timer1_Timer()
    
    m_iIntervalo = m_iIntervalo + 1
    
    If m_iIntervalo = gParametrosInstalacion.giBuzonTimer Then
        m_iIntervalo = 0
        ComprobarNuevasOfertas
    End If
    
End Sub

'Descripcion: Comprueba si hay contratos pr�ximos a expirar, o expirados, y que todav�a no han sido renegociado
'Llamada desde: Form_load // PicExclamacion_MouseUp
'Tiempo ejecucion:0,4seg.
Private Sub PosibleAlertaContrato()
Dim inum As Integer
Dim sPer As String
sPer = ""

If oUsuarioSummit.Tipo <> Administrador Then
    sPer = oUsuarioSummit.Persona.Cod
End If

inum = oUsuarioSummit.DevolverContratosProximosExpirar(sPer)
m_bAvisoContrato = False
If inum > 0 Then
    PicExclamacion.Visible = True
    m_bAvisoContrato = True
End If


End Sub

''' <summary>Comprueba si hay Notificaciones pr�ximos a expirar</summary>
''' <remarks>Llamada desde: Form_load // PicExclamacion_MouseUp; Tiempo ejecucion:0,4seg.</remarks>
''' <revision>LTG 06/06/2012</revision>

Private Sub PosibleNotificacionDeDespublicacion()
    Dim Ador As Ador.Recordset
    Dim sRes As String
    Dim sPub As String
    Dim inum As Integer
    Dim bCargar As Boolean
    Dim dtFecLimOfe As Date
    Dim dtHoraFinOfe As Date
    Dim sFecLimOfe As String
    
    If gParametrosInstalacion.giAvisoDespublica = 0 And gParametrosInstalacion.giAvisoAdj = 0 Then Exit Sub
    
    Set Ador = oUsuarioSummit.DevolverProcesosADespublicar(-gParametrosInstalacion.giAvisoDespublica, -gParametrosInstalacion.giAvisoAdj, BooleanToSQLBinary(gParametrosInstalacion.gbAvisoAdj), BooleanToSQLBinary(gParametrosInstalacion.gbAvisoDespublica))
    m_bAvisoNotificacion = False
    If Not Ador Is Nothing Then
        If Not Ador.EOF Then
            PicExclamacion.Visible = True
            m_bAvisoNotificacion = True
            inum = 1
            Set frmAvisoDespublicacion.g_oProcesos = oFSGSRaiz.generar_CProcesos
            frmAvisoDespublicacion.g_bAvisoNotificacion = True
            frmAvisoDespublicacion.sdbgProcesos.RemoveAll
            With frmAvisoDespublicacion.sdbgProcesos
            
                While Not Ador.EOF
                    bCargar = False
                    If Not IsNull(Ador("FECLIMOFE").Value) Then
                        If CDate(Format(Ador("FECLIMOFE").Value, "short date")) >= Date Then
                            bCargar = True
                        End If
                    End If
                    If Not IsNull(Ador("FECPRES").Value) Then
                        If CDate(Ador("FECPRES").Value) >= Date Then
                            bCargar = True
                        End If
                    End If
                    If bCargar Then
                        If basOptimizacion.gCodEqpUsuario = NullToStr(Ador("EQP").Value) And basOptimizacion.gCodCompradorUsuario = NullToStr(Ador("COM").Value) Then
                            sRes = 1
                        Else
                            sRes = 0
                        End If
                        If Ador("PUB").Value > 0 Then
                            sPub = 1
                        Else
                            sPub = 0
                        End If
                        
                        sFecLimOfe = ""
                        If Not IsNull(Ador("FECLIMOFE").Value) Then
                            ConvertirUTCaTZ DateValue(Ador("FECLIMOFE").Value), TimeValue(Ador("FECLIMOFE").Value), m_vTZHora, dtFecLimOfe, dtHoraFinOfe
                            sFecLimOfe = DateValue(dtFecLimOfe) & " " & TimeValue(dtHoraFinOfe)
                        End If
                        
                        .AddItem Ador("ANYO").Value & Chr(m_lSeparador) & Ador("GMN1").Value & Chr(m_lSeparador) & Ador("COD").Value & Chr(m_lSeparador) & _
                            Ador("DEN").Value & Chr(m_lSeparador) & sFecLimOfe & Chr(m_lSeparador) & NullToStr(Ador("FECPRES").Value) & _
                            Chr(m_lSeparador) & sRes & Chr(m_lSeparador) & sPub & Chr(m_lSeparador) & Ador("ADJDIR").Value & Chr(m_lSeparador) & inum & _
                            Chr(m_lSeparador) & Ador("EST").Value & Chr(m_lSeparador) & Ador("SUBASTA").Value
                        
                        frmAvisoDespublicacion.g_oProcesos.Add Ador("ANYO").Value, Ador("COD").Value, Ador("GMN1").Value, Ador("DEN").Value, Ador("ADJDIR").Value, Ador("FECAPE").Value, , , , Ador("EST").Value, , Ador("FECNEC").Value, NullToStr(Ador("FECPRES").Value), , NullToStr(Ador("FECLIMOFE").Value), , , inum
                        frmAvisoDespublicacion.g_oProcesos.Item(CStr(inum)).ModoSubasta = SQLBinaryToBoolean(Ador("SUBASTA").Value)
                        inum = inum + 1
                    End If
                   Ador.MoveNext
                Wend
            End With
        Else
            PicExclamacion.Visible = False
        End If
        Ador.Close
        Set Ador = Nothing
    Else
        PicExclamacion.Visible = False
    End If
End Sub

''' <summary>
''' Guardar la configuracion de columnas del Visor
''' </summary>
''' <remarks>Llamada desde: Form_unload ; Tiempo m�ximo: 0,2</remarks>
Private Sub GuardarConfiguracionVisor()
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    
    With g_oConfVisor
        'almacena las posiciones
        .AnyoPos = sdbgProcesos.Columns("ANYO").Position
        .GMN1pos = sdbgProcesos.Columns("GMN1").Position
        .Procepos = sdbgProcesos.Columns("COD").Position
        .Descrpos = sdbgProcesos.Columns("DESCR").Position
        .EstadoPos = sdbgProcesos.Columns("EST").Position
        .Responsablepos = sdbgProcesos.Columns("RESP").Position
        .AdjDirpos = sdbgProcesos.Columns("ADJDIR").Position
        .Ppos = sdbgProcesos.Columns("P").Position
        .Opos = sdbgProcesos.Columns("O").Position
        .Wpos = sdbgProcesos.Columns("W").Position
        .Rpos = sdbgProcesos.Columns("R").Position
        .FecApepos = sdbgProcesos.Columns("FECAPE").Position
        .FecLimOfepos = sdbgProcesos.Columns("FECLIMOFE").Position
        .FecPresPos = sdbgProcesos.Columns("FECPRES").Position
        .FecCierrePos = sdbgProcesos.Columns("FECCIERRE").Position
        .FecNecPos = sdbgProcesos.Columns("FECNEC").Position
        .FecIniPos = sdbgProcesos.Columns("FECINI").Position
        .FecFinPos = sdbgProcesos.Columns("FECFIN").Position
        .PresPos = sdbgProcesos.Columns("PRES").Position
        .ConsumidoPos = sdbgProcesos.Columns("CONS").Position
        .ImportePos = sdbgProcesos.Columns("IMP").Position
        .AhorroPos = sdbgProcesos.Columns("AHORRO").Position
        .Monedapos = sdbgProcesos.Columns("MON").Position
        .Destinopos = sdbgProcesos.Columns("DEST").Position
        .DestinoDenpos = sdbgProcesos.Columns("DESTDEN").Position
        .Pagopos = sdbgProcesos.Columns("PAGO").Position
        .ProveedorPos = sdbgProcesos.Columns("PROVE").Position
        .ProveedorDenpos = sdbgProcesos.Columns("PROVEDEN").Position
        
        If Col_GsToERP_Visible() Then .gsToErpPos = sdbgProcesos.Columns("GSTOERP").Position
        .FecValAperturapos = sdbgProcesos.Columns("FECVALAPERTURA").Position
        .PorcentajeAhorropos = sdbgProcesos.Columns("PORCENTAJEAHORRO").Position
        .Cambiopos = sdbgProcesos.Columns("CAMBIO").Position
        .SolicVinculadaPos = sdbgProcesos.Columns("VINCULADA").Position
            
        'almacena los widths
        .AnyoWidth = sdbgProcesos.Columns("ANYO").Width
        .GMN1Width = sdbgProcesos.Columns("GMN1").Width
        .ProceWidth = sdbgProcesos.Columns("COD").Width
        .DescrWidth = sdbgProcesos.Columns("DESCR").Width
        .EstadoWidth = sdbgProcesos.Columns("EST").Width
        .ResponsableWidth = sdbgProcesos.Columns("RESP").Width
        .AdjDirWidth = sdbgProcesos.Columns("ADJDIR").Width
        .PWidth = sdbgProcesos.Columns("P").Width
        .OWidth = sdbgProcesos.Columns("O").Width
        .WWidth = sdbgProcesos.Columns("W").Width
        .RWidth = sdbgProcesos.Columns("R").Width
        .FecApeWidth = sdbgProcesos.Columns("FECAPE").Width
        .FecLimOfeWidth = sdbgProcesos.Columns("FECLIMOFE").Width
        .FecPresWidth = sdbgProcesos.Columns("FECPRES").Width
        .FecCierreWidth = sdbgProcesos.Columns("FECCIERRE").Width
        .FecNecWidth = sdbgProcesos.Columns("FECNEC").Width
        .FecIniWidth = sdbgProcesos.Columns("FECINI").Width
        .FecFinWidth = sdbgProcesos.Columns("FECFIN").Width
        .PresWidth = sdbgProcesos.Columns("PRES").Width
        .ConsumidoWidth = sdbgProcesos.Columns("CONS").Width
        .ImporteWidth = sdbgProcesos.Columns("IMP").Width
        .AhorroWidth = sdbgProcesos.Columns("AHORRO").Width
        .MonedaWidth = sdbgProcesos.Columns("MON").Width
        .DestinoWidth = sdbgProcesos.Columns("DEST").Width
        .DestinoDenWidth = sdbgProcesos.Columns("DESTDEN").Width
        .PagoWidth = sdbgProcesos.Columns("PAGO").Width
        .ProveedorWidth = sdbgProcesos.Columns("PROVE").Width
        .ProveedorDenWidth = sdbgProcesos.Columns("PROVEDEN").Width
        If Col_GsToERP_Visible() Then .gsToErpWidth = sdbgProcesos.Columns("GSTOERP").Width
        .FecValAperturaWidth = sdbgProcesos.Columns("FECVALAPERTURA").Width
        .PorcentajeAhorroWidth = sdbgProcesos.Columns("PORCENTAJEAHORRO").Width
        .CambioWidth = sdbgProcesos.Columns("CAMBIO").Width
        .SolicVinculadaWidth = sdbgProcesos.Columns("VINCULADA").Width
        
        'almacena el filtro de mes/a�o:
        If optTodos.Value Then
            .AnyoDesde = Null
            .MesDesde = Null
        Else
            .AnyoDesde = Trim(sdbcAnyo.Text)
            .MesDesde = sdbcMes.AddItemRowIndex(sdbcMes.Bookmark) + 1
        End If
        
        'almacena el filtro de material:
        Set .GMN1Cod = m_oGMN1Seleccionados
        Set .GMN2Cod = m_oGMN2Seleccionados
        Set .GMN3Cod = m_oGMN3Seleccionados
        Set .GMN4Cod = m_oGMN4Seleccionados
        
        'almacena el filtro de equipo
        .codEqp = StrToNull(m_sEqpCod)
        
        'almacena el filtro del comprador
        .CodComprador = StrToNull(m_sComCod)
        
        'almacena el filtro del Responsable
        .responsable = SQLBinaryToBoolean(chkResponsable.Value)
        
        'almacena el filtro de la unidad organizativa:
        .UON1 = StrToNull(sUON1)
        .UON2 = StrToNull(sUON2)
        .UON3 = StrToNull(sUON3)
        
        'almacena el filtro de presupuestos:
        .Pres1Niv1 = m_vPresup1_1
        .Pres1Niv2 = m_vPresup1_2
        .Pres1Niv3 = m_vPresup1_3
        .Pres1Niv4 = m_vPresup1_4
    
        .Pres2Niv1 = m_vPresup2_1
        .Pres2Niv2 = m_vPresup2_2
        .Pres2Niv3 = m_vPresup2_3
        .Pres2Niv4 = m_vPresup2_4
    
        .Pres3Niv1 = m_vPresup3_1
        .Pres3Niv2 = m_vPresup3_2
        .Pres3Niv3 = m_vPresup3_3
        .Pres3Niv4 = m_vPresup3_4
    
        .Pres4Niv1 = m_vPresup4_1
        .Pres4Niv2 = m_vPresup4_2
        .Pres4Niv3 = m_vPresup4_3
        .Pres4Niv4 = m_vPresup4_4
        
        .Denominacion = m_sDenominacion
        .AtrBuscarVisible = frDatos(0).Visible
        Set .AtrBuscar = MoverGridAtrBuscarAColleccion(sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0))
    End With
    
    Set oIBaseDatos = g_oConfVisor
    If Not oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
        'Inserta la configuraci�n del visor
        teserror = oIBaseDatos.AnyadirABaseDatos
    Else
        'Modifica la configuraci�n del visor
        teserror = oIBaseDatos.FinalizarEdicionModificando
    End If
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        oIBaseDatos.CancelarEdicion
        Exit Sub
    End If
    Set oIBaseDatos = Nothing
End Sub

''' <summary>
''' Carga la configuraci�n por defecto
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarConfiguracionVisor()
    Dim oConfVisores As CConfVistasVisor
    Dim ConfigVisor As TipoConfigVisor
    
    'Carga la configuraci�n de los campos,orden y filtro para el usuario de la aplicaci�n
    Set oConfVisores = oFSGSRaiz.Generar_CConfVistasVisor
    
    oConfVisores.CargarConfVisor oUsuarioSummit.Cod
    With ConfigVisor
        If oConfVisores.Item(1) Is Nothing Then
            .bAnyoVisible = True: .iAnyoPos = 0: .dblAnyoWidth = 400                                    'A�o
            .bGMN1Visible = True: .iGMN1Pos = 1: .dblGMN1Width = 450                                    'GMN1
            .bProceVisible = True: .iProcePos = 2: .dblProceWidth = 650                                 'Proce
            .bDescrVisible = True: .iDescrPos = 3: .dblDescrWidth = 2700                                'Descr
            .bEstadoVisible = True: .iEstadoPos = 4: .dblEstadoWidth = 1700                             'Estado
            .bRespVisible = True: .iRespPos = 5: .dblRespWidth = 980                                    'Responsable
            .bAdjDirVisible = True: .iAdjDirPos = 6: .dblAdjDirWidth = 300                              'AdjDir
            .bPVisible = False: .iPPos = 7: .dblPWidth = 400                                            'P
            .bOVisible = False: .iOPos = 8: .dblOWidth = 400                                            'O
            .bWVisible = False: .iWPos = 9: .dblWWidth = 400                                            'W
            .bRVisible = False: .iRPos = 10: .dblRWidth = 400                                           'R
            .bFecApeVisible = False: .iFecApePos = 11: .dblFecApeWidth = 800                            'FecApe
            .bFecValAperturaVisible = False: .iFecValAperturaPos = 12: .dblFecValAperturaWidth = 900    'FecValApertura
            .bFecLimOfeVisible = False: .iFecLimOfePos = 13: .dblFecLimOfeWidth = 800                   'FecLimOfe
            .bFecPresVisible = True: .iFecPresPos = 14: .dblFecPresWidth = 800                          'FecPres
            .bFecCierreVisible = False: .iFecCierrePos = 15: .dblFecCierreWidth = 800                   'FecCierre
            .bFecNecVisible = False: .iFecNecPos = 16: .dblFecNecWidth = 800                            'FecNec
            .bFecIniVisible = False: .iFecIniPos = 17: .dblFecIniWidth = 800                            'FecIni
            .bFecFinVisible = False: .iFecFinPos = 18: .dblFecFinWidth = 800                            'FecFin
            .bPresVisible = False: .iPresPos = 19: .dblPresWidth = 900                                  'Pres
            .bConsumidoVisible = False: .iConsumidoPos = 20: .dblConsumidoWidth = 900                   'Consumido
            .bImporteVisible = False: .iImportePos = 21: .dblImporteWidth = 900                         'importe
            .bAhorroVisible = False: .iAhorroPos = 22: .dblAhorroWidth = 900                            'Ahorro
            .bPorcentajeAhorroVisible = False: .iPorcentajeAhorroPos = 23: .dblPorcentajeAhorroWidth = 900  'PorcentajeAhorro
            'El campo 24 separa el resto
            If Col_GsToERP_Visible() Then
                .bgsToErpVisible = False: .igsToErpPos = 24: .dblgsToErpWidth = 900             'gstoerp
                .bMonedaVisible = False: .iMonedaPos = 25: .dblMonedaWidth = 900                'moneda
                .bCambioVisible = False: .iCambioPos = 26: .dblCambioWidth = 900                'Cambio
                .bDestVisible = False: .iDestPos = 27: .dblDestWidth = 900                      'Dest
                .bDestDenVisible = False: .iDestDenPos = 28: .dblDestDenWidth = 900             'destden
                .bPagoVisible = False: .iPagoPos = 29: .dblPagoWidth = 900                      'pago
                .bProvVisible = False: .iProvPos = 30: .dblProvWidth = 900                      'Prove
                .bProvDenVisible = False: .iProvDenPos = 31: .dblProvDenWidth = 900             'proveDen
                .bSolicVinculadaVisible = False: .iSolicVinculadaPos = 32: .dblSolicVinculadaWidth = 900      'Vinculada
            Else
                .bMonedaVisible = False: .iMonedaPos = 24: .dblMonedaWidth = 900                'moneda
                .bCambioVisible = False: .iCambioPos = 25: .dblCambioWidth = 900                'Cambio
                .bDestVisible = False: .iDestPos = 26: .dblDestWidth = 900                      'Dest
                .bDestDenVisible = False: .iDestDenPos = 27: .dblDestDenWidth = 900             'destden
                .bPagoVisible = False: .iPagoPos = 28: .dblPagoWidth = 900                      'pago
                .bProvVisible = False: .iProvPos = 29: .dblProvWidth = 900                      'Prove
                .bProvDenVisible = False: .iProvDenPos = 30: .dblProvDenWidth = 900             'proveDen
                .bSolicVinculadaVisible = False: .iSolicVinculadaPos = 31: .dblSolicVinculadaWidth = 900      'Vinculada
            End If
            .bOcultarFiltro = False
            .bOcultarMenu = False
            .vAnyoDesde = Null
            .vMesDesde = Null
            Set .oGMN1 = Nothing
            Set .oGMN2 = Nothing
            Set .oGMN3 = Nothing
            Set .oGMN4 = Nothing
            .vCodEqp = Null
            .vCodCom = Null
            .bResponsable = False
            .vUON1 = Null: .vUON2 = Null: .vUON3 = Null
            .vPres1Niv1 = Null: .vPres1Niv2 = Null: .vPres1Niv3 = Null: .vPres1Niv4 = Null
            .vPres2Niv1 = Null: .vPres2Niv2 = Null: .vPres2Niv3 = Null: .vPres2Niv4 = Null
            .vPres3Niv1 = Null: .vPres3Niv2 = Null: .vPres3Niv3 = Null: .vPres3Niv4 = Null
            .vPres4Niv1 = Null: .vPres4Niv2 = Null: .vPres4Niv3 = Null: .vPres4Niv4 = Null
            .vDenominacion = Null
            .bAtrBuscarVisible = False
            oConfVisores.Add oUsuarioSummit.Cod, ConfigVisor
        Else
            'Cargamos los atributos
            Set oConfVisores.Item(1).AtrBuscar = oFSGSRaiz.Generar_CAtributosBuscar
            oConfVisores.Item(1).CargarAtrBuscar
        End If
    End With
    Set g_oConfVisor = oConfVisores.Item(1)

    'la configuracion ha podido cambiar en el arranque con nuevos parametros pregunto por gstoerp
    If Col_GsToERP_Visible() And g_oConfVisor.gsToErpVisible = False Then
        'antes no habia gsToErp y ahora debe haberlo --> actualizamos el configurador
        g_oConfVisor.gsToErpVisible = False 'ya lo pondra el usuario si quiere
        g_oConfVisor.gsToErpPos = 24
        g_oConfVisor.gsToErpWidth = 900
    End If
    If Not Col_GsToERP_Visible() And g_oConfVisor.gsToErpVisible = True Then
        'antes habia gsToErp y ahora  no debe haberlo--> actualizamos el configurador
        g_oConfVisor.gsToErpVisible = False
        g_oConfVisor.gsToErpPos = 0
        g_oConfVisor.gsToErpWidth = 0
    End If
    Set oConfVisores = Nothing
End Sub

''' <summary>
''' Pone las columnas con el orden,tama�o y visibilidad con que est�n guardadas en BD para el usuario
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub RedimensionarGrid()
    Dim j As Integer
    
    'ordena las columnas seg�n la posici�n
    OrdenarColumnasConf
    
    For j = 0 To sdbgProcesos.Columns.Count - 1
        sdbgProcesos.Columns(j).HasHeadForeColor = -1   'True
        sdbgProcesos.Columns(j).HeadForeColor = 16777215
    Next j
                
    'Posici�n de los campos (Hay 33)
    For j = 0 To 32
        Select Case m_arrOrden(j)
            Case "ANYO"
                sdbgProcesos.Columns("ANYO").Position = g_oConfVisor.AnyoPos
            Case "GMN1"
                sdbgProcesos.Columns("GMN1").Position = g_oConfVisor.GMN1pos
            Case "PROCE"
                sdbgProcesos.Columns("COD").Position = g_oConfVisor.Procepos
            Case "DESCR"
                sdbgProcesos.Columns("DESCR").Position = g_oConfVisor.Descrpos
            Case "EST"
                sdbgProcesos.Columns("EST").Position = g_oConfVisor.EstadoPos
            Case "RESP"
                sdbgProcesos.Columns("RESP").Position = g_oConfVisor.Responsablepos
            Case "ADJDIR"
                sdbgProcesos.Columns("ADJDIR").Position = g_oConfVisor.AdjDirpos
            Case "P"
                sdbgProcesos.Columns("P").Position = g_oConfVisor.Ppos
            Case "O"
                sdbgProcesos.Columns("O").Position = g_oConfVisor.Opos
            Case "W"
                sdbgProcesos.Columns("W").Position = g_oConfVisor.Wpos
            Case "R"
                sdbgProcesos.Columns("R").Position = g_oConfVisor.Rpos
            Case "FECAPE"
                sdbgProcesos.Columns("FECAPE").Position = g_oConfVisor.FecApepos
            Case "FECLIMOFE"
                sdbgProcesos.Columns("FECLIMOFE").Position = g_oConfVisor.FecLimOfepos
            Case "FECPRES"
                sdbgProcesos.Columns("FECPRES").Position = g_oConfVisor.FecPresPos
            Case "FECCIERRE"
                sdbgProcesos.Columns("FECCIERRE").Position = g_oConfVisor.FecCierrePos
            Case "FECNEC"
                sdbgProcesos.Columns("FECNEC").Position = g_oConfVisor.FecNecPos
            Case "FECINI"
                sdbgProcesos.Columns("FECINI").Position = g_oConfVisor.FecIniPos
            Case "FECFIN"
                sdbgProcesos.Columns("FECFIN").Position = g_oConfVisor.FecFinPos
            Case "PRES"
                sdbgProcesos.Columns("PRES").Position = g_oConfVisor.PresPos
                sdbgProcesos.Columns("PRES").NumberFormat = "Standard"
            Case "CONS"
                sdbgProcesos.Columns("CONS").Position = g_oConfVisor.ConsumidoPos
                sdbgProcesos.Columns("CONS").NumberFormat = "Standard"
            Case "IMP"
                sdbgProcesos.Columns("IMP").Position = g_oConfVisor.ImportePos
                sdbgProcesos.Columns("IMP").NumberFormat = "Standard"
            Case "AHORRO"
                sdbgProcesos.Columns("AHORRO").Position = g_oConfVisor.AhorroPos
                sdbgProcesos.Columns("AHORRO").NumberFormat = "Standard"
            Case "MON"
                sdbgProcesos.Columns("MON").Position = g_oConfVisor.Monedapos
            Case "DEST"
                sdbgProcesos.Columns("DEST").Position = g_oConfVisor.Destinopos
            Case "DESTDEN"
                sdbgProcesos.Columns("DESTDEN").Position = g_oConfVisor.DestinoDenpos
            Case "PAGO"
                sdbgProcesos.Columns("PAGO").Position = g_oConfVisor.Pagopos
            Case "PROVE"
                sdbgProcesos.Columns("PROVE").Position = g_oConfVisor.ProveedorPos
            Case "PROVEDEN"
                sdbgProcesos.Columns("PROVEDEN").Position = g_oConfVisor.ProveedorDenpos
            Case "FECVALAPERTURA"
                sdbgProcesos.Columns("FECVALAPERTURA").Position = g_oConfVisor.FecValAperturapos
            Case "PORCENTAJEAHORRO"
                sdbgProcesos.Columns("PORCENTAJEAHORRO").Position = g_oConfVisor.PorcentajeAhorropos
            Case "CAMBIO"
                sdbgProcesos.Columns("CAMBIO").Position = g_oConfVisor.Cambiopos
            Case "GSTOERP"
                sdbgProcesos.Columns("GSTOERP").Position = g_oConfVisor.gsToErpPos
            Case "VINCULADA"
                sdbgProcesos.Columns("VINCULADA").Position = g_oConfVisor.SolicVinculadaPos
        End Select
    Next j
    
    sdbgProcesos.Columns("ESTID").Visible = False
    sdbgProcesos.Columns("ADJDIR").Style = ssStyleCheckBox
    sdbgProcesos.Columns("ADJDIR").Width = 404.7874
    sdbgProcesos.Columns("VINCULADA").Style = ssStyleCheckBox
    sdbgProcesos.Columns("VINCULADA").Width = 480.7874
    sdbgProcesos.Columns("ANYO").caption = m_sCaptionsGrid(0)
    sdbgProcesos.Columns("GMN1").caption = m_sCaptionsGrid(1)
    sdbgProcesos.Columns("COD").caption = m_sCaptionsGrid(2)
    sdbgProcesos.Columns("DESCR").caption = m_sCaptionsGrid(3)
    sdbgProcesos.Columns("EST").caption = m_sCaptionsGrid(4)
    sdbgProcesos.Columns("RESP").caption = m_sCaptionsGrid(5)
    sdbgProcesos.Columns("ADJDIR").caption = m_sCaptionsGrid(6)
    sdbgProcesos.Columns("FECPRES").caption = m_sCaptionsGrid(7)
    sdbgProcesos.Columns("FECAPE").caption = m_sCaptionsGrid(8)
    sdbgProcesos.Columns("FECLIMOFE").caption = m_sCaptionsGrid(9)
    sdbgProcesos.Columns("FECCIERRE").caption = m_sCaptionsGrid(10)
    sdbgProcesos.Columns("FECINI").caption = m_sCaptionsGrid(11)
    sdbgProcesos.Columns("FECFIN").caption = m_sCaptionsGrid(12)
    sdbgProcesos.Columns("PRES").caption = m_sCaptionsGrid(13)
    sdbgProcesos.Columns("CONS").caption = m_sCaptionsGrid(14)
    sdbgProcesos.Columns("IMP").caption = m_sCaptionsGrid(15)
    sdbgProcesos.Columns("MON").caption = m_sCaptionsGrid(16)
    sdbgProcesos.Columns("DEST").caption = m_sCaptionsGrid(17)
    sdbgProcesos.Columns("DESTDEN").caption = m_sCaptionsGrid(18)
    sdbgProcesos.Columns("PAGO").caption = m_sCaptionsGrid(19)
    sdbgProcesos.Columns("PROVE").caption = m_sCaptionsGrid(20)
    sdbgProcesos.Columns("PROVEDEN").caption = m_sCaptionsGrid(21)
    sdbgProcesos.Columns("AHORRO").caption = m_sCaptionsGrid(22)
    sdbgProcesos.Columns("FECNEC").caption = m_sCaptionsGrid(23)
    sdbgProcesos.Columns("P").caption = m_sCaptionsGrid(24)
    sdbgProcesos.Columns("FECVALAPERTURA").caption = m_sCaptionsGrid(27)
    sdbgProcesos.Columns("PORCENTAJEAHORRO").caption = m_sCaptionsGrid(28)
    sdbgProcesos.Columns("CAMBIO").caption = m_sCaptionsGrid(29)
    sdbgProcesos.Columns("GSTOERP").caption = m_sCaptionsGrid(30)
    sdbgProcesos.Columns("R").caption = m_sCaptionsGrid(31)
    sdbgProcesos.Columns("VINCULADA").caption = m_sCaptionsGrid(32)
    sdbgProcesos.Columns("COM").Visible = False
    sdbgProcesos.Columns("EQP").Visible = False
        
    'Visibilidad de las columnas:
    sdbgProcesos.Columns("ANYO").Visible = g_oConfVisor.AnyoVisible
    sdbgProcesos.Columns("GMN1").Visible = g_oConfVisor.GMN1Visible
    sdbgProcesos.Columns("COD").Visible = g_oConfVisor.ProceVisible
    sdbgProcesos.Columns("DESCR").Visible = g_oConfVisor.DescrVisible
    sdbgProcesos.Columns("EST").Visible = g_oConfVisor.EstadoVisible
    sdbgProcesos.Columns("RESP").Visible = g_oConfVisor.ResponsableVisible
    sdbgProcesos.Columns("ADJDIR").Visible = g_oConfVisor.AdjDirVisible
    sdbgProcesos.Columns("P").Visible = g_oConfVisor.PVisible
    sdbgProcesos.Columns("O").Visible = g_oConfVisor.OVisible
    sdbgProcesos.Columns("W").Visible = g_oConfVisor.WVisible
    sdbgProcesos.Columns("R").Visible = g_oConfVisor.RVisible
    sdbgProcesos.Columns("FECAPE").Visible = g_oConfVisor.FecApeVisible
    sdbgProcesos.Columns("FECLIMOFE").Visible = g_oConfVisor.FecLimOfeVisible
    sdbgProcesos.Columns("FECPRES").Visible = g_oConfVisor.FecPresVisible
    sdbgProcesos.Columns("FECCIERRE").Visible = g_oConfVisor.FecCierreVisible
    sdbgProcesos.Columns("FECNEC").Visible = g_oConfVisor.FecNecVisible
    sdbgProcesos.Columns("FECINI").Visible = g_oConfVisor.FecIniVisible
    sdbgProcesos.Columns("FECFIN").Visible = g_oConfVisor.FecFinVisible
    sdbgProcesos.Columns("PRES").Visible = g_oConfVisor.PresVisible
    sdbgProcesos.Columns("CONS").Visible = g_oConfVisor.ConsumidoVisible
    sdbgProcesos.Columns("IMP").Visible = g_oConfVisor.ImporteVisible
    sdbgProcesos.Columns("AHORRO").Visible = g_oConfVisor.AhorroVisible
    sdbgProcesos.Columns("MON").Visible = g_oConfVisor.MonedaVisible
    sdbgProcesos.Columns("DEST").Visible = g_oConfVisor.DestinoVisible
    sdbgProcesos.Columns("DESTDEN").Visible = g_oConfVisor.DestinoDenVisible
    sdbgProcesos.Columns("PAGO").Visible = g_oConfVisor.PagoVisible
    sdbgProcesos.Columns("PROVE").Visible = g_oConfVisor.ProveedorVisible
    sdbgProcesos.Columns("PROVEDEN").Visible = g_oConfVisor.ProveedorDenVisible
    sdbgProcesos.Columns("GSTOERP").Visible = g_oConfVisor.gsToErpVisible
    sdbgProcesos.Columns("FECVALAPERTURA").Visible = g_oConfVisor.FecValAperturaVisible
    sdbgProcesos.Columns("PORCENTAJEAHORRO").Visible = g_oConfVisor.PorcentajeAhorroVisible
    sdbgProcesos.Columns("CAMBIO").Visible = g_oConfVisor.CambioVisible
    sdbgProcesos.Columns("VINCULADA").Visible = g_oConfVisor.SolicVinculadaVisible
    
    'Tama�o de los campos del grupo 0
    'Se pone el width en el mismo orden en el que est�n los positions,porque sino algunas veces no funciona
    'bien
    For j = 0 To 32
        Select Case m_arrOrden(j)
            Case "ANYO"
                sdbgProcesos.Columns("ANYO").Width = g_oConfVisor.AnyoWidth
            Case "GMN1"
                sdbgProcesos.Columns("GMN1").Width = g_oConfVisor.GMN1Width
            Case "PROCE"
                sdbgProcesos.Columns("COD").Width = g_oConfVisor.ProceWidth
            Case "DESCR"
                sdbgProcesos.Columns("DESCR").Width = g_oConfVisor.DescrWidth
            Case "EST"
                sdbgProcesos.Columns("EST").Width = g_oConfVisor.EstadoWidth
            Case "RESP"
                sdbgProcesos.Columns("RESP").Width = g_oConfVisor.ResponsableWidth
            Case "ANYO"
                sdbgProcesos.Columns("ADJDIR").Width = g_oConfVisor.AdjDirWidth
            Case "P"
                sdbgProcesos.Columns("P").Width = g_oConfVisor.PWidth
            Case "O"
                sdbgProcesos.Columns("O").Width = g_oConfVisor.OWidth
            Case "W"
                sdbgProcesos.Columns("W").Width = g_oConfVisor.WWidth
            Case "R"
                sdbgProcesos.Columns("R").Width = g_oConfVisor.RWidth
            Case "FECAPE"
                sdbgProcesos.Columns("FECAPE").Width = g_oConfVisor.FecApeWidth
            Case "FECLIMOFE"
                sdbgProcesos.Columns("FECLIMOFE").Width = g_oConfVisor.FecLimOfeWidth
            Case "FECPRES"
                sdbgProcesos.Columns("FECPRES").Width = g_oConfVisor.FecPresWidth
            Case "FECCIERRE"
                sdbgProcesos.Columns("FECCIERRE").Width = g_oConfVisor.FecCierreWidth
            Case "FECNEC"
                sdbgProcesos.Columns("FECNEC").Width = g_oConfVisor.FecNecWidth
            Case "FECINI"
                sdbgProcesos.Columns("FECINI").Width = g_oConfVisor.FecIniWidth
            Case "FECFIN"
                sdbgProcesos.Columns("FECFIN").Width = g_oConfVisor.FecFinWidth
            Case "PRES"
                sdbgProcesos.Columns("PRES").Width = g_oConfVisor.PresWidth
            Case "CONS"
                sdbgProcesos.Columns("CONS").Width = g_oConfVisor.ConsumidoWidth
            Case "IMP"
                sdbgProcesos.Columns("IMP").Width = g_oConfVisor.ImporteWidth
            Case "AHORRO"
                sdbgProcesos.Columns("AHORRO").Width = g_oConfVisor.AhorroWidth
            Case "MON"
                sdbgProcesos.Columns("MON").Width = g_oConfVisor.MonedaWidth
            Case "DEST"
                sdbgProcesos.Columns("DEST").Width = g_oConfVisor.DestinoWidth
            Case "DESTDEN"
                sdbgProcesos.Columns("DESTDEN").Width = g_oConfVisor.DestinoDenWidth
            Case "PAGO"
                sdbgProcesos.Columns("PAGO").Width = g_oConfVisor.PagoWidth
            Case "PROVE"
                sdbgProcesos.Columns("PROVE").Width = g_oConfVisor.ProveedorWidth
            Case "PROVEDEN"
                sdbgProcesos.Columns("PROVEDEN").Width = g_oConfVisor.ProveedorDenWidth
            Case "GSTOERP"
                sdbgProcesos.Columns("GSTOERP").Width = g_oConfVisor.gsToErpWidth
            Case "FECVALAPERTURA"
                sdbgProcesos.Columns("FECVALAPERTURA").Width = g_oConfVisor.FecValAperturaWidth
            Case "PORCENTAJEAHORRO"
                sdbgProcesos.Columns("PORCENTAJEAHORRO").Width = g_oConfVisor.PorcentajeAhorroWidth
            Case "CAMBIO"
                sdbgProcesos.Columns("CAMBIO").Width = g_oConfVisor.CambioWidth
            Case "VINCULADA"
                sdbgProcesos.Columns("VINCULADA").Width = g_oConfVisor.SolicVinculadaWidth
        End Select
    Next j
    
    'Oculta el frame del filtro o no,dependiendo de la configuraci�n guardada
    If g_oConfVisor.OcultarFiltro = True Then
        OcultarFiltro
    End If
    'Ocultar el picture del men�,o no dependiendo de la configuraci�n guardada
    If g_oConfVisor.OcultarMenu = True Then
        OcultarMenu
    End If

End Sub

''' <summary>
''' Ordena las columnas seg�n la posici�n
''' </summary>
''' <remarks>Llamada desde: RedimensionarGrid ; Tiempo m�ximo: 0,2</remarks>
Private Sub OrdenarColumnasConf()
    Dim iOrden As Integer
    Dim iOrdenados As Integer
    Dim iOrdenAnt As Integer
    Dim sCol As String
    Dim bEncontrado As Boolean
    Dim i As Integer
    
    '(les pone las posiciones en orden,porque sino a veces no funciona bien)
    iOrdenAnt = 0
    iOrdenados = 0
    Erase m_arrOrden
    
    While iOrdenados < 33
        iOrden = 0
        sCol = ""
        If iOrdenAnt <= g_oConfVisor.AnyoPos Then
            bEncontrado = False
            For i = 0 To iOrdenados
                If m_arrOrden(i) = "ANYO" Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
            If bEncontrado = False Then
                iOrden = g_oConfVisor.AnyoPos
                sCol = "ANYO"
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.GMN1pos) Then
            If g_oConfVisor.GMN1pos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "GMN1" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.GMN1pos
                    sCol = "GMN1"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.Procepos) Then
            If g_oConfVisor.Procepos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "PROCE" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.Procepos
                    sCol = "PROCE"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.Descrpos) Then
            If g_oConfVisor.Descrpos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "DESCR" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.Descrpos
                    sCol = "DESCR"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.EstadoPos) Then
            If g_oConfVisor.EstadoPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "EST" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.EstadoPos
                    sCol = "EST"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.Responsablepos) Then
            If g_oConfVisor.Responsablepos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "RESP" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.Responsablepos
                    sCol = "RESP"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.AdjDirpos) Then
            If g_oConfVisor.AdjDirpos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "ADJDIR" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.AdjDirpos
                    sCol = "ADJDIR"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.Ppos) Then
           If g_oConfVisor.Ppos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "P" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.Ppos
                    sCol = "P"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.Opos) Then
            If g_oConfVisor.Opos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "O" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.Opos
                    sCol = "O"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.Wpos) Then
            If g_oConfVisor.Wpos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "W" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.Wpos
                    sCol = "W"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.Rpos) Then
            If g_oConfVisor.Rpos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "R" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.Rpos
                    sCol = "R"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.FecApepos) Then
            If g_oConfVisor.FecApepos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "FECAPE" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.FecApepos
                    sCol = "FECAPE"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.FecLimOfepos) Then
            If g_oConfVisor.FecLimOfepos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "FECLIMOFE" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.FecLimOfepos
                    sCol = "FECLIMOFE"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.FecPresPos) Then
            If g_oConfVisor.FecPresPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "FECPRES" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.FecPresPos
                    sCol = "FECPRES"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.FecCierrePos) Then
            If g_oConfVisor.FecCierrePos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "FECCIERRE" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.FecCierrePos
                    sCol = "FECCIERRE"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.FecNecPos) Then
            If g_oConfVisor.FecNecPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "FECNEC" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.FecNecPos
                    sCol = "FECNEC"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.FecIniPos) Then
            If g_oConfVisor.FecIniPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "FECINI" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.FecIniPos
                    sCol = "FECINI"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.FecFinPos) Then
            If g_oConfVisor.FecFinPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "FECFIN" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.FecFinPos
                    sCol = "FECFIN"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.PresPos) Then
            If g_oConfVisor.PresPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "PRES" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.PresPos
                    sCol = "PRES"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.ConsumidoPos) Then
            If g_oConfVisor.ConsumidoPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "CONS" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.ConsumidoPos
                    sCol = "CONS"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.ImportePos) Then
            If g_oConfVisor.ImportePos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "IMP" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.ImportePos
                    sCol = "IMP"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.AhorroPos) Then
            If g_oConfVisor.AhorroPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "AHORRO" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.AhorroPos
                    sCol = "AHORRO"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.Monedapos) Then
            If g_oConfVisor.Monedapos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "MON" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.Monedapos
                    sCol = "MON"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.Destinopos) Then
            If g_oConfVisor.Destinopos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "DEST" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.Destinopos
                    sCol = "DEST"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.DestinoDenpos) Then
            If g_oConfVisor.DestinoDenpos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "DESTDEN" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.DestinoDenpos
                    sCol = "DESTDEN"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.Pagopos) Then
            If g_oConfVisor.Pagopos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "PAGO" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.Pagopos
                    sCol = "PAGO"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.ProveedorPos) Then
            If g_oConfVisor.ProveedorPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "PROVE" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.ProveedorPos
                    sCol = "PROVE"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.ProveedorDenpos) Then
            If g_oConfVisor.ProveedorDenpos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "PROVEDEN" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.ProveedorDenpos
                    sCol = "PROVEDEN"
                End If
            End If
        End If
        
        'Nota: en el If de arriba he cambiado 27 por 31
        If (iOrdenAnt <= g_oConfVisor.gsToErpPos) Then
            If g_oConfVisor.gsToErpPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "GSTOERP" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.gsToErpPos
                    sCol = "GSTOERP"
                End If
            End If
        End If
        
        
        If (iOrdenAnt <= g_oConfVisor.FecValAperturapos) Then
            If g_oConfVisor.FecValAperturapos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "FECVALAPERTURA" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.FecValAperturapos
                    sCol = "FECVALAPERTURA"
                End If
            End If
        End If
        
            
        If (iOrdenAnt <= g_oConfVisor.PorcentajeAhorropos) Then
            If g_oConfVisor.PorcentajeAhorropos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "PORCENTAJEAHORRO" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.PorcentajeAhorropos
                    sCol = "PORCENTAJEAHORRO"
                End If
            End If
        End If
            
        If (iOrdenAnt <= g_oConfVisor.Cambiopos) Then
            If g_oConfVisor.Cambiopos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "CAMBIO" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.Cambiopos
                    sCol = "CAMBIO"
                End If
            End If
        End If
        
        If (iOrdenAnt <= g_oConfVisor.SolicVinculadaPos) Then
            If g_oConfVisor.SolicVinculadaPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "VINCULADA" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = g_oConfVisor.SolicVinculadaPos
                    sCol = "VINCULADA"
                End If
            End If
        End If
        
        If sCol <> "" Then
            m_arrOrden(iOrdenados) = sCol
            iOrdenados = iOrdenados + 1
            iOrdenAnt = iOrden
        Else
            iOrdenAnt = iOrdenAnt + 1
        End If
    Wend
        
    
End Sub

''' <summary>Ordena la grid desde el men�</summary>
''' <param name="bAsc">orden ascendente</param>
''' <param name="sCol">columna por la que se ordena</param>
''' <remarks>Llamada desde:</remarks>
''' <revision>LTG 15/01/2013</revision>

Public Sub OrdenarGridDesdeMenu(ByVal bAsc As Boolean, ByVal sCol As String)
Dim oAtrBuscar As CAtributosBuscar
    'Ordena la grid seg�n la cabecera en la que hayamos pulsado
    Screen.MousePointer = vbHourglass
    
    'BLoquea la pantalla hasta que se cargue la grid y los men�s
    LockWindowUpdate Me.hWnd
        
    If bAsc Then
        'Ordena ascendentemente
        gParametrosInstalacion.gbOrdenVisorAscendente = True
    Else
        'Ordena descendentemente
        gParametrosInstalacion.gbOrdenVisorAscendente = False
    End If
    
    OrdenSeleccionado sCol
    'Coleccion que recoge los atributos por los que se quiere filtrar y el ambito en el que se quiere que se cumplan
    Set oAtrBuscar = MoverGridAtrBuscarAColleccion(sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0))
    Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(FiltroProcesosVisor(True), m_sEstadoIntegracion(), oAtrBuscar)
                      
    If m_adoRecordset Is Nothing Then
        'Desbloquea la pantalla
        LockWindowUpdate 0&
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set sdbgProcesos.DataSource = m_adoRecordset
    
    'Desbloquea la pantalla
    LockWindowUpdate 0&

    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Ordena la grid seg�n la columna la que hayamos seleccionado
''' </summary>
''' <param name="sCol">por que columna se ordena</param>
''' <remarks>Llamada desde: sdbgProcesos_HeadClick  OrdenarGridDesdeMenu; Tiempo m�ximo: 0,2</remarks>
Private Sub OrdenSeleccionado(ByVal sCol As String)
    Select Case sCol
        Case "GMN1"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorMaterial
        Case "ANYO", "COD"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCod
        Case "DESCR"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorDen
        Case "EST"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorEstado
        Case "RESP"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorResponsable
        Case "ADJDIR"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorAdjDir
        Case "FECAPE"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorfechaapertura
        Case "FECLIMOFE"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorFechaValidezOfertas
        Case "FECPRES"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorFechaPresentacion
        Case "FECCIERRE"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorFechaCierre
        Case "FECNEC"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorfechanecesidad
        Case "FECINI"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorFechaIniSuministro
        Case "FECFIN"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorFechaFinSuministro
        Case "MON"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorMoneda
        Case "DEST"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorDestino
        Case "DESTDEN"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorDestinoDen
        Case "PAGO"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorFPago
        Case "PROVE"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorProveAct
        Case "PROVEDEN"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorProveActDen
        Case "P"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorP
        Case "O"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorO
        Case "W"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorW
        Case "R"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorR
        Case "PRES"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorPresTotal
        Case "CONS"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorConsumido
        Case "IMP"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorAdjudicado
        Case "AHORRO"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorAhorro
        Case "FECVALAPERTURA"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorFecValApertura
        Case "PORCENTAJEAHORRO"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorPorcentajeAhorro
        Case "CAMBIO"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorCambio
        Case "GSTOERP"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorGsToErp
        Case "VINCULADA"
            gParametrosInstalacion.giOrdenVisor = TipoOrdenacionProcesos.OrdPorSolicVinculada
    End Select
End Sub

Private Sub OcultarMenu()
    'Oculta el men� de la izquierda del formulario
    picMenu.Visible = False
    
    sdbgProcesos.Width = sdbgProcesos.Width + picMenu.Width
    sdbgProcesos.Left = 0
    
    picSeparator1.Visible = False
    picSeparator2.Visible = False
    picTotPend.Visible = False
    picTotCerr.Visible = False
    picTot.Visible = False
    
    'Trata los iconos
    picMostrarMenu.Visible = True
    picOcultarMenu.Visible = False
End Sub

Private Sub OcultarFiltro()
    Dim i As Integer
    Dim bEncontrado As Boolean
    
    'BLoquea la pantalla hasta que oculte el filtro
    LockWindowUpdate Me.hWnd
    
    'Hace invisible el frame de filtro
    frameFiltro.Visible = False
    
    'Top de los pictures de ocultar/mostrar
    picMostrarFiltro.Top = 560
    picMostrarMenu.Top = 560
    picOcultarMenu.Top = 560
    
    'Hace mas grande el men� y la grid de procesos
    sdbgProcesos.Top = picMostrarFiltro.Top + 260
    picMenu.Top = sdbgProcesos.Top
    
    sdbgProcesos.Height = sdbgProcesos.Height + frameFiltro.Height
    picMenu.Height = sdbgProcesos.Height
    

    'dimensiona el men�:
    sdbgMenu.MoveFirst
    i = 0
    
    While Not bEncontrado
        If sdbgMenu.Columns("ID").Value = CTOTABIERTOS Then
            bEncontrado = True
            If m_bTotPend Then
                picTotPend.Top = sdbgMenu.RowHeight * i + sdbgMenu.Top + 25 + Picture1.Height + picMostrarFiltro.Height + 75
            End If
        End If
        i = i + 1
        sdbgMenu.MoveNext
    Wend
    
    picSeparator1.Top = sdbgMenu.RowHeight * i + sdbgMenu.Top + Picture1.Height + picMostrarFiltro.Height + 85
    
    bEncontrado = False
    
    While Not bEncontrado
        If sdbgMenu.Columns("ID").Value = CTOTCERRADOS Then
            bEncontrado = True
            If m_bTotCerr Then
                picTotCerr.Top = sdbgMenu.RowHeight * i + sdbgMenu.Top + 25 + Picture1.Height + picMostrarFiltro.Height + 75
            End If
        End If
        i = i + 1
        sdbgMenu.MoveNext
    Wend
    
    picSeparator2.Top = sdbgMenu.RowHeight * i + sdbgMenu.Top + Picture1.Height + picMostrarFiltro.Height + 85
    
    bEncontrado = False
    
    While Not bEncontrado
        If sdbgMenu.Columns("ID").Value = CTOT Then
            bEncontrado = True
            If m_bTotCerr Or m_bTotPend Then
                picTot.Top = sdbgMenu.RowHeight * i + sdbgMenu.Top + 25 + Picture1.Height + picMostrarFiltro.Height + 75
            End If
        End If
        i = i + 1
        sdbgMenu.MoveNext
    Wend
    
    sdbgMenu.MoveFirst
    
    'Trata los iconos
    picCerrarFiltro.Visible = False
    picAbrirFiltro.Visible = True
    lblMostraFiltro.caption = m_stxtMostrar
    
    
    'Desbloquea la pantalla
    LockWindowUpdate 0&
    
End Sub

Private Sub CargarAnyosYMeses()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    'Carga los a�os
    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        sdbcAnyo.AddItem iInd
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
    
    'Carga los meses
    sdbcMes.AddItem m_sIdiMeses(1)
    sdbcMes.AddItem m_sIdiMeses(2)
    sdbcMes.AddItem m_sIdiMeses(3)
    sdbcMes.AddItem m_sIdiMeses(4)
    sdbcMes.AddItem m_sIdiMeses(5)
    sdbcMes.AddItem m_sIdiMeses(6)
    sdbcMes.AddItem m_sIdiMeses(7)
    sdbcMes.AddItem m_sIdiMeses(8)
    sdbcMes.AddItem m_sIdiMeses(9)
    sdbcMes.AddItem m_sIdiMeses(10)
    sdbcMes.AddItem m_sIdiMeses(11)
    sdbcMes.AddItem m_sIdiMeses(12)
    
    sdbcMes.ListAutoPosition = True
    sdbcMes.MoveFirst
    sdbcMes.Text = sdbcMes.Columns(0).Value
    
    
End Sub

Private Sub MostrarMatSeleccionado()
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    Dim iNumSel As Integer

    lstMultiMaterial.clear

    If Not m_oGMN1Seleccionados Is Nothing Then
        iNumSel = m_oGMN1Seleccionados.Count
        
        For Each oGMN1 In m_oGMN1Seleccionados
            lstMultiMaterial.AddItem oGMN1.Cod & " - " & oGMN1.Den
        Next
    End If
    
    If Not m_oGMN2Seleccionados Is Nothing Then
        iNumSel = iNumSel + m_oGMN2Seleccionados.Count
        
        For Each oGMN2 In m_oGMN2Seleccionados
            lstMultiMaterial.AddItem oGMN2.GMN1Cod & " - " & oGMN2.Cod & " - " & oGMN2.Den
        Next
    End If
    
    If Not m_oGMN3Seleccionados Is Nothing Then
        iNumSel = iNumSel + m_oGMN3Seleccionados.Count
        
        For Each oGMN3 In m_oGMN3Seleccionados
            lstMultiMaterial.AddItem oGMN3.GMN1Cod & " - " & oGMN3.GMN2Cod & " - " & oGMN3.Cod & " - " & oGMN3.Den
        Next
    End If
    
    If Not m_oGMN4Seleccionados Is Nothing Then
        iNumSel = iNumSel + m_oGMN4Seleccionados.Count
        
        For Each oGMN4 In m_oGMN4Seleccionados
            lstMultiMaterial.AddItem oGMN4.GMN1Cod & " - " & oGMN4.GMN2Cod & " - " & oGMN4.GMN3Cod & " - " & oGMN4.Cod & " - " & oGMN4.Den
        Next
    End If
    
    txtMaterial.Text = lstMultiMaterial.List(0)
    If iNumSel > 1 Then
        txtMaterial.Text = txtMaterial.Text & " ..."
        If Me.TextWidth(txtMaterial.Text) > txtMaterial.Width Then
            'Si el texto es demasiado laro acortarlo hasta que se vean los 3 ptos.
            'se utiliza un pic porque el form (el otro objeto que tiene el m�todo TextWidth) tiene font distinto
                                
            Set picFont.Font = txtMaterial.Font
            While picFont.TextWidth(txtMaterial.Text) > (txtMaterial.Width - 200)
                txtMaterial.Text = Left(txtMaterial.Text, Len(txtMaterial.Text) - 5) & " ..."
            Wend
        End If
    End If

    g_oConfVisor.HayCambios = True
End Sub

Public Sub MostrarUOSeleccionada()
    If sUON1 = frmSELUO.sUON1 And sUON2 = frmSELUO.sUON2 And sUON3 = frmSELUO.sUON3 Then Exit Sub
        
    If frmSELUO.sUON3 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sUON3 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = frmSELUO.sUON3
        
    ElseIf frmSELUO.sUON2 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = ""
        
    ElseIf frmSELUO.sUON1 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = ""
        sUON3 = ""
        
    Else
        txtUO.Text = ""
        txtUO.Refresh
        sUON1 = ""
        sUON2 = ""
        sUON3 = ""
    End If
    
    g_oConfVisor.HayCambios = True
End Sub

Private Sub Restaurar()
    Dim vNumProce As Variant
    Dim i As Integer
    Dim oAtrBuscar As CAtributosBuscar
    DoEvents
    
    m_dblTotal = 0
    m_dblTotalAbiertos = 0
    m_dblTotalCerrados = 0
    
    Screen.MousePointer = vbHourglass
    
    'BLoquea la pantalla hasta que se cargue la grid y los men�s
    LockWindowUpdate Me.hWnd
    'Coleccion que recoge los atributos por los que se quiere filtrar y el ambito en el que se quiere que se cumplan
    Set oAtrBuscar = MoverGridAtrBuscarAColleccion(sdbgAtrBuscar, m_sAdjDir(1), m_sAdjDir(0))
    Dim udtDevolVerProcesosVisor As TipoDevolVerProcesosVisor
    udtDevolVerProcesosVisor = FiltroProcesosVisor(False)
    vNumProce = oGestorInformes.DevolverNumeroDeProcesosEnCadaEstado(FiltroProcesosVisor(False), oAtrBuscar)
    
    
    With sdbgMenu
        .MoveFirst
        For i = 0 To .Rows - 1
            If .Columns("ID").Value <> CTOTABIERTOS And .Columns("ID").Value <> CSEP1 And .Columns("ID").Value <> CSEP2 And .Columns("ID").Value <> CTOT And .Columns("ID").Value <> CTOTCERRADOS Then
                .Columns("NUMPROCE").Value = vNumProce(.Columns("ID").Value - 1)
                
                m_dblTotal = m_dblTotal + .Columns("NUMPROCE").Value
                
                If .Columns("ID").Value < 8 Then
                    m_dblTotalAbiertos = m_dblTotalAbiertos + .Columns("NUMPROCE").Value
                Else
                    m_dblTotalCerrados = m_dblTotalCerrados + .Columns("NUMPROCE").Value
                End If
                
            End If
            
            .MoveNext
        Next i
        
        'Mostrarvalor de total
        .Columns("NUMPROCE").Value = m_dblTotal
        
        'Mostrar valor para pendientes
        Dim bEncontrado As Boolean
        .MoveFirst
        i = 0
        While Not bEncontrado
            If .Columns("ID").Value = CTOTABIERTOS Then
                bEncontrado = True
                .Columns("NUMPROCE").Value = m_dblTotalAbiertos
            End If
            
            i = i + 1
            .MoveNext
        Wend
        bEncontrado = False
        
        While Not bEncontrado
            If .Columns("ID").Value = CTOTCERRADOS Then
                bEncontrado = True
                .Columns("NUMPROCE").Value = m_dblTotalCerrados
            End If
            
            i = i + 1
            .MoveNext
        Wend
        bEncontrado = False
        
        While Not bEncontrado
            If .Columns("ID").Value = CTOT Then
                bEncontrado = True
                .Columns("NUMPROCE").Value = m_dblTotal
            End If
            
            i = i + 1
            .MoveNext
        Wend
        
        'Si el usuario es comprador comprueba que tenga ofertas no leidas para
        'habilitar el bot�n del sobre para ir al buz�n de ofertas
        If oUsuarioSummit.Tipo = comprador Then ComprobarNuevasOfertas
        
        .MoveFirst
    End With
        
    Select Case m_iSeleccion
        Case 1 'Pendientes de validar apertura
                With udtDevolVerProcesosVisor
                    .DesdeEstado = sinitems
                    .HastaEstado = ConItemsSinValidar
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case 2 'Pendientes de asignar proveedores
                With udtDevolVerProcesosVisor
                    .DesdeEstado = validado
                    .HastaEstado = Conproveasignados
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case 3 'Pendientes de enviar peticiones
                With udtDevolVerProcesosVisor
                    .DesdeEstado = conasignacionvalida
                    .HastaEstado = conasignacionvalida
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case 4 'En recepci�n de ofertas
                With udtDevolVerProcesosVisor
                    .DesdeEstado = conpeticiones
                    .HastaEstado = conofertas
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case 5 'Pendiente de comunicar objetivos
                With udtDevolVerProcesosVisor
                    .DesdeEstado = ConObjetivosSinNotificar
                    .HastaEstado = ConObjetivosSinNotificarYPreadjudicado
                End With
                
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case 6 'Pendiente de adjudicar
                With udtDevolVerProcesosVisor
                    .DesdeEstado = PreadjYConObjNotificados
                    .HastaEstado = PreadjYConObjNotificados
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case 7 'Parcialmente cerrado
                With udtDevolVerProcesosVisor
                    .DesdeEstado = ParcialmenteCerrado
                    .HastaEstado = ParcialmenteCerrado
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case 8 'Pendiente de notificar adj.
                With udtDevolVerProcesosVisor
                    .DesdeEstado = conadjudicaciones
                    .HastaEstado = conadjudicaciones
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case 9 'Adjudicados y notificados
                With udtDevolVerProcesosVisor
                    .DesdeEstado = ConAdjudicacionesNotificadas
                    .HastaEstado = ConAdjudicacionesNotificadas
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case 10 'Anulados
                With udtDevolVerProcesosVisor
                    .DesdeEstado = Cerrado
                    .HastaEstado = Cerrado
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case CTOT 'Todos
                With udtDevolVerProcesosVisor
                    .DesdeEstado = sinitems
                    .HastaEstado = Cerrado
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case CTOTCERRADOS 'Todos los Cerrados
                With udtDevolVerProcesosVisor
                    .DesdeEstado = conadjudicaciones
                    .HastaEstado = Cerrado
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
                
        Case CTOTABIERTOS 'Todos los pendientes
                With udtDevolVerProcesosVisor
                    .DesdeEstado = sinitems
                    .HastaEstado = ParcialmenteCerrado
                End With
    
                Set m_adoRecordset = oGestorInformes.DevolVerProcesosVisor(udtDevolVerProcesosVisor, m_sEstadoIntegracion(), oAtrBuscar)
    End Select
    
    Set sdbgProcesos.DataSource = m_adoRecordset
    
    'Desbloquea la pantalla
    LockWindowUpdate 0&
    
    Screen.MousePointer = vbNormal
    
    DoEvents
End Sub

Public Sub MostrarPresSeleccionado12(ByVal iTipo As Integer)
    Dim sLblUO As String
    Dim sPresup1 As String
    Dim sPresup2 As String
    Dim sPresup3 As String
    Dim sPresup4 As String
    
    m_vPresup1_1 = Null
    m_vPresup1_2 = Null
    m_vPresup1_3 = Null
    m_vPresup1_4 = Null
    m_vPresup2_1 = Null
    m_vPresup2_2 = Null
    m_vPresup2_3 = Null
    m_vPresup2_4 = Null
    m_vPresup3_1 = Null
    m_vPresup3_2 = Null
    m_vPresup3_3 = Null
    m_vPresup3_4 = Null
    m_vPresup4_1 = Null
    m_vPresup4_2 = Null
    m_vPresup4_3 = Null
    m_vPresup4_4 = Null
    m_sUON1Pres = ""
    m_sUON2Pres = ""
    m_sUON3Pres = ""
    
    m_sUON1Pres = frmSELPresAnuUON.g_sUON1
    m_sUON2Pres = frmSELPresAnuUON.g_sUON2
    m_sUON3Pres = frmSELPresAnuUON.g_sUON3
    
    sLblUO = ""
    If m_sUON1Pres <> "" Then
        sLblUO = "(" & m_sUON1Pres
        If m_sUON2Pres <> "" Then
            sLblUO = sLblUO & " - " & m_sUON2Pres
            If m_sUON3Pres <> "" Then
                sLblUO = sLblUO & " - " & m_sUON3Pres & ") "
            Else
                sLblUO = sLblUO & ") "
            End If
        Else
            sLblUO = sLblUO & ") "
        End If
    End If
    
    sPresup1 = frmSELPresAnuUON.g_sPRES1
    sPresup2 = frmSELPresAnuUON.g_sPRES2
    sPresup3 = frmSELPresAnuUON.g_sPRES3
    sPresup4 = frmSELPresAnuUON.g_sPRES4

    If sPresup4 <> "" Then
        txtPresup.Text = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sPresup3 <> "" Then
        txtPresup.Text = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sPresup2 <> "" Then
        txtPresup.Text = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sPresup1 <> "" Then
        txtPresup.Text = sLblUO & sPresup1 & " " & frmSELPresAnuUON.g_sDenPres
    Else
        txtPresup.Text = sLblUO
    End If
    
    If iTipo = 1 Then
        m_vPresup1_1 = frmSELPresAnuUON.g_vIdPRES1
        m_vPresup1_2 = frmSELPresAnuUON.g_vIdPRES2
        m_vPresup1_3 = frmSELPresAnuUON.g_vIdPRES3
        m_vPresup1_4 = frmSELPresAnuUON.g_vIdPRES4
    Else
        m_vPresup2_1 = frmSELPresAnuUON.g_vIdPRES1
        m_vPresup2_2 = frmSELPresAnuUON.g_vIdPRES2
        m_vPresup2_3 = frmSELPresAnuUON.g_vIdPRES3
        m_vPresup2_4 = frmSELPresAnuUON.g_vIdPRES4
    End If
    
    g_oConfVisor.HayCambios = True
End Sub

Public Sub MostrarPresSeleccionado34(ByVal iTipo As Integer)
    Dim sLblUO As String
    Dim sPresup1 As String
    Dim sPresup2 As String
    Dim sPresup3 As String
    Dim sPresup4 As String
    
    m_vPresup1_1 = Null
    m_vPresup1_2 = Null
    m_vPresup1_3 = Null
    m_vPresup1_4 = Null
    m_vPresup2_1 = Null
    m_vPresup2_2 = Null
    m_vPresup2_3 = Null
    m_vPresup2_4 = Null
    m_vPresup3_1 = Null
    m_vPresup3_2 = Null
    m_vPresup3_3 = Null
    m_vPresup3_4 = Null
    m_vPresup4_1 = Null
    m_vPresup4_2 = Null
    m_vPresup4_3 = Null
    m_vPresup4_4 = Null
    m_sUON1Pres = ""
    m_sUON2Pres = ""
    m_sUON3Pres = ""
    
    m_sUON1Pres = frmSELPresUO.g_sUON1
    m_sUON2Pres = frmSELPresUO.g_sUON2
    m_sUON3Pres = frmSELPresUO.g_sUON3
    
    sLblUO = ""
    If m_sUON1Pres <> "" Then
        sLblUO = "(" & m_sUON1Pres
        If m_sUON2Pres <> "" Then
            sLblUO = sLblUO & " - " & m_sUON2Pres
            If m_sUON3Pres <> "" Then
                sLblUO = sLblUO & " - " & m_sUON3Pres & ") "
            Else
                sLblUO = sLblUO & ") "
            End If
        Else
            sLblUO = sLblUO & ") "
        End If
    End If
    
    sPresup1 = frmSELPresUO.g_sPRES1
    sPresup2 = frmSELPresUO.g_sPRES2
    sPresup3 = frmSELPresUO.g_sPRES3
    sPresup4 = frmSELPresUO.g_sPRES4

    If sPresup4 <> "" Then
        txtPresup.Text = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresUO.g_sDenPres
    ElseIf sPresup3 <> "" Then
        txtPresup.Text = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresUO.g_sDenPres
    ElseIf sPresup2 <> "" Then
        txtPresup.Text = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresUO.g_sDenPres
    ElseIf sPresup1 <> "" Then
        txtPresup.Text = sLblUO & sPresup1 & " " & frmSELPresUO.g_sDenPres
    Else
        txtPresup.Text = sLblUO
    End If

    If iTipo = 3 Then
        m_vPresup3_1 = frmSELPresUO.g_vIdPRES1
        m_vPresup3_2 = frmSELPresUO.g_vIdPRES2
        m_vPresup3_3 = frmSELPresUO.g_vIdPRES3
        m_vPresup3_4 = frmSELPresUO.g_vIdPRES4
    Else
        m_vPresup4_1 = frmSELPresUO.g_vIdPRES1
        m_vPresup4_2 = frmSELPresUO.g_vIdPRES2
        m_vPresup4_3 = frmSELPresUO.g_vIdPRES3
        m_vPresup4_4 = frmSELPresUO.g_vIdPRES4
    End If
    
    g_oConfVisor.HayCambios = True
End Sub

''' <summary>Comprueba y aplica los filtros</summary>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub ComprobarYAplicarFiltros()
    Dim oEquipos As CEquipos
    Dim oComps As CCompradores
    Dim sLblUO As String
    Dim Ador As Ador.Recordset
    Dim oPresupuestos1 As CPresProyectosNivel1
    Dim oPresupuestos2 As CPresContablesNivel1
    Dim oPresupuestos3 As CPresConceptos3Nivel1
    Dim oPresupuestos4 As CPresConceptos4Nivel1
    Dim oUnidades1 As CUnidadesOrgNivel1
    Dim oUnidades2 As CUnidadesOrgNivel2
    Dim oUnidades3 As CUnidadesOrgNivel3
    Dim bEliminarFitro As Boolean
    
    m_bCargandoFiltro = True
    
    '******************** Filtro de mes/a�o ***************************:
    If Not IsNull(g_oConfVisor.AnyoDesde) Then
        optDesde.Value = True
        sdbcAnyo.Text = g_oConfVisor.AnyoDesde
        sdbcMes.MoveRecords g_oConfVisor.MesDesde - 1
        sdbcMes.Text = sdbcMes.Columns(0).Value
        sAnyoAnterior = sdbcAnyo.Text
        sMesAnterior = sdbcMes.Text
    Else
        sAnyoAnterior = ""
        sMesAnterior = ""
    End If
    
    '*********************** FILTRO DE MATERIAL ************************:
    bEliminarFitro = False
    Set m_oGMN1Seleccionados = g_oConfVisor.GMN1Cod
    Set m_oGMN2Seleccionados = g_oConfVisor.GMN2Cod
    Set m_oGMN3Seleccionados = g_oConfVisor.GMN3Cod
    Set m_oGMN4Seleccionados = g_oConfVisor.GMN4Cod
    
    If m_oGMN1Seleccionados Is Nothing Then Set m_oGMN1Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
    If m_oGMN2Seleccionados Is Nothing Then Set m_oGMN2Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
    If m_oGMN3Seleccionados Is Nothing Then Set m_oGMN3Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
    If m_oGMN4Seleccionados Is Nothing Then Set m_oGMN4Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
        
    If m_oGMN4Seleccionados.Count > 0 Then m_oGMN4Seleccionados.CargarDenGMN4s oUsuarioSummit.idioma
    If m_oGMN3Seleccionados.Count > 0 Then m_oGMN3Seleccionados.CargarDenGMN3s oUsuarioSummit.idioma
    If m_oGMN2Seleccionados.Count > 0 Then m_oGMN2Seleccionados.CargarDenGMN2s oUsuarioSummit.idioma
    If m_oGMN1Seleccionados.Count > 0 Then m_oGMN1Seleccionados.CargarDenGMN1s oUsuarioSummit.idioma
            
    If m_oGMN4Seleccionados.Count + m_oGMN3Seleccionados.Count + m_oGMN2Seleccionados.Count + m_oGMN1Seleccionados.Count = 0 Then
        txtMaterial.Text = ""
        lstMultiMaterial.clear
        g_oConfVisor.HayCambios = True
    Else
        MostrarMatSeleccionado
    End If
    
    '************************* FILTRO DE EQUIPO ***************************************:
    If Not IsNull(g_oConfVisor.codEqp) Then
        If m_bREqp = True And (g_oConfVisor.codEqp <> basOptimizacion.gCodEqpUsuario) Then
            'si tiene restricci�n de equipo y el equipo seleccionado en el filtro es distinto del eqp del usuario, borramos el filtro:
            m_sEqpCod = ""
            g_oConfVisor.HayCambios = True
        Else
            Set oEquipos = oFSGSRaiz.Generar_CEquipos
            oEquipos.CargarTodosLosEquipos g_oConfVisor.codEqp, , True
            If oEquipos.Count = 0 Then  'Si el equipo se ha eliminado de BD borramos el filtro
                m_sEqpCod = ""
                g_oConfVisor.HayCambios = True
            Else
                sdbcEquipo.Text = g_oConfVisor.codEqp & " - " & oEquipos.Item(1).Den
                sdbcEquipo.Columns(0).Value = g_oConfVisor.codEqp
                m_sEqpCod = g_oConfVisor.codEqp
                m_oEquipos.CargarTodosLosEquipos g_oConfVisor.codEqp, , True
                Set m_oEqpSeleccionado = m_oEquipos.Item(CStr(g_oConfVisor.codEqp))
            
            End If
            Set oEquipos = Nothing
        End If
    End If
    
    
    '********************* FILTRO DE COMPRADOR ****************************************:
    If Not IsNull(g_oConfVisor.CodComprador) Then
        If m_bRAsig = True And (g_oConfVisor.CodComprador <> basOptimizacion.gCodCompradorUsuario) Then
            'Si hay restricci�n de comprador y el comprador seleccionado en el filtro es distinto del usuario , borramos el filtro:
            m_sComCod = ""
            g_oConfVisor.HayCambios = True
        ElseIf m_bRAsig = False And m_bREqp = False And IsNull(g_oConfVisor.codEqp) And Not IsNull(g_oConfVisor.CodComprador) Then
            'si hab�a restricc. de equipo y se borra, habiendo filtro por comprador se elimina el comprador
            m_sComCod = ""
            g_oConfVisor.HayCambios = True
        ElseIf m_bRAsig = False And m_bREqp = True And Not IsNull(g_oConfVisor.codEqp) And Not IsNull(g_oConfVisor.CodComprador) And (g_oConfVisor.codEqp <> basOptimizacion.gCodEqpUsuario) Then
            'si hay restricci�n de equipo y hab�a filtro por comprador: si los equipos son distintos se elimina el filtro de comprador
            m_sComCod = ""
            g_oConfVisor.HayCambios = True
        Else
            Set oComps = oFSGSRaiz.generar_CCompradores
            oComps.CargarTodosLosCompradores g_oConfVisor.CodComprador, , , True
            If oComps.Count = 0 Then  'Si el comprador se ha eliminado de BD borramos el filtro
                m_sComCod = ""
                g_oConfVisor.HayCambios = True
            Else
                sdbcComprador.Text = g_oConfVisor.CodComprador & " - " & oComps.Item(1).nombre & " " & oComps.Item(1).Apel
                sdbcComprador.Columns(0).Value = g_oConfVisor.CodComprador
                m_sComCod = g_oConfVisor.CodComprador
            End If
            Set oComps = Nothing
        End If
    End If
    
    '*************************** RESPONSABLE *********************************:
    If g_oConfVisor.responsable = True Then
        If m_sComCod = "" And m_sEqpCod = "" And m_bRAsig = False And m_bREqp = False Then
            'Si no hay restricciones ni equipo ni comprador seleccionado no se puede marcar el check de responsable
            g_oConfVisor.responsable = False
            chkResponsable.Value = 0
            g_oConfVisor.HayCambios = True
        Else
            chkResponsable.Value = 1
        End If
    Else
        chkResponsable.Value = 0
    End If
    
    
    
    '************************ FILTRO DE UNIDAD ORGANIZATIVA ****************:
    sUON1 = NullToStr(g_oConfVisor.UON1)
    sUON2 = NullToStr(g_oConfVisor.UON2)
    sUON3 = NullToStr(g_oConfVisor.UON3)
    bEliminarFitro = False
    'comprueba si hay filtro de UO y restricci�n de UO,que el filtro seleccionado est� dentro de la restricci�n:
    If sUON1 <> "" Or sUON2 <> "" Or sUON1 <> "" Then
        If m_bRUsuUON = True Then
            If basOptimizacion.gUON3Usuario <> "" Then
                If Not (sUON1 = basOptimizacion.gUON1Usuario And sUON2 = basOptimizacion.gUON2Usuario And sUON3 = basOptimizacion.gUON3Usuario) Then
                    bEliminarFitro = True
                End If
            ElseIf basOptimizacion.gUON2Usuario <> "" Then
                If Not (sUON1 = basOptimizacion.gUON1Usuario And sUON2 = basOptimizacion.gUON2Usuario) Then bEliminarFitro = True
            ElseIf basOptimizacion.gUON1Usuario <> "" Then
                If sUON1 <> basOptimizacion.gUON1Usuario Then bEliminarFitro = True
            End If
        End If
    End If
    If bEliminarFitro = True Then
        sUON1 = ""
        sUON2 = ""
        sUON3 = ""
        txtUO.Text = ""
        g_oConfVisor.HayCambios = True
    Else
        If sUON3 <> "" Then
            Set oUnidades3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
            If oUnidades3.DevolverDenominacion(sUON1, sUON2, sUON3) = "" Then  'Si se ha eliminado la UO lo borramos del filtro
                sUON1 = ""
                sUON2 = ""
                sUON3 = ""
                txtUO.Text = ""
                g_oConfVisor.HayCambios = True
            Else
                txtUO.Text = sUON1 & " - " & sUON2 & " - " & sUON3
            End If
            Set oUnidades3 = Nothing
            
        ElseIf sUON2 <> "" Then
            Set oUnidades2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
            If oUnidades2.DevolverDenominacion(sUON1, sUON2) = "" Then
                sUON1 = ""
                sUON2 = ""
                txtUO.Text = ""
                g_oConfVisor.HayCambios = True
            Else
                txtUO.Text = sUON1 & " - " & sUON2
            End If
            Set oUnidades2 = Nothing
            
        ElseIf sUON1 <> "" Then
            Set oUnidades1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
            If oUnidades1.DevolverDenominacion(sUON1) = "" Then
                sUON1 = ""
                txtUO.Text = ""
                g_oConfVisor.HayCambios = True
            Else
                txtUO.Text = sUON1
            End If
            Set oUnidades1 = Nothing
        End If
    End If
    

    '*********** FILTRO DE PRESUPUESTOS ************* :
    m_vPresup1_1 = g_oConfVisor.Pres1Niv1
    m_vPresup1_2 = g_oConfVisor.Pres1Niv2
    m_vPresup1_3 = g_oConfVisor.Pres1Niv3
    m_vPresup1_4 = g_oConfVisor.Pres1Niv4
    m_vPresup2_1 = g_oConfVisor.Pres2Niv1
    m_vPresup2_2 = g_oConfVisor.Pres2Niv2
    m_vPresup2_3 = g_oConfVisor.Pres2Niv3
    m_vPresup2_4 = g_oConfVisor.Pres2Niv4
    m_vPresup3_1 = g_oConfVisor.Pres3Niv1
    m_vPresup3_2 = g_oConfVisor.Pres3Niv2
    m_vPresup3_3 = g_oConfVisor.Pres3Niv3
    m_vPresup3_4 = g_oConfVisor.Pres3Niv4
    m_vPresup4_1 = g_oConfVisor.Pres4Niv1
    m_vPresup4_2 = g_oConfVisor.Pres4Niv2
    m_vPresup4_3 = g_oConfVisor.Pres4Niv3
    m_vPresup4_4 = g_oConfVisor.Pres4Niv4
    
    bEliminarFitro = False
    If Not IsNull(m_vPresup1_1) Or Not IsNull(m_vPresup1_2) Or Not IsNull(m_vPresup1_3) Or Not IsNull(m_vPresup1_4) Then
        Set oPresupuestos1 = oFSGSRaiz.generar_CPresProyectosNivel1
        Set Ador = oPresupuestos1.DevolverPresupuestosDesde(m_vPresup1_1, m_vPresup1_2, m_vPresup1_3, m_vPresup1_4)
        Set oPresupuestos1 = Nothing
        bEliminarFitro = True
        
    ElseIf Not IsNull(m_vPresup2_1) Or Not IsNull(m_vPresup2_2) Or Not IsNull(m_vPresup2_3) Or Not IsNull(m_vPresup2_4) Then
        Set oPresupuestos2 = oFSGSRaiz.Generar_CPresContablesNivel1
        Set Ador = oPresupuestos2.DevolverPresupuestosDesde(m_vPresup2_1, m_vPresup2_2, m_vPresup2_3, m_vPresup2_4)
        Set oPresupuestos2 = Nothing
        bEliminarFitro = True
        
    ElseIf Not IsNull(m_vPresup3_1) Or Not IsNull(m_vPresup3_2) Or Not IsNull(m_vPresup3_3) Or Not IsNull(m_vPresup3_4) Then
        Set oPresupuestos3 = oFSGSRaiz.Generar_CPresConceptos3Nivel1
        Set Ador = oPresupuestos3.DevolverPresupuestosDesde(m_vPresup3_1, m_vPresup3_2, m_vPresup3_3, m_vPresup3_4)
        Set oPresupuestos3 = Nothing
        bEliminarFitro = True
        
    ElseIf Not IsNull(m_vPresup4_1) Or Not IsNull(m_vPresup4_2) Or Not IsNull(m_vPresup4_3) Or Not IsNull(m_vPresup4_4) Then
        Set oPresupuestos4 = oFSGSRaiz.Generar_CPresConceptos4Nivel1
        Set Ador = oPresupuestos4.DevolverPresupuestosDesde(m_vPresup4_1, m_vPresup4_2, m_vPresup4_3, m_vPresup4_4)
        Set oPresupuestos4 = Nothing
        bEliminarFitro = True
    End If
    
    If Not Ador Is Nothing Then
        m_sUON1Pres = NullToStr(Ador("UON1").Value)
        m_sUON2Pres = NullToStr(Ador("UON2").Value)
        m_sUON3Pres = NullToStr(Ador("UON3").Value)
    
        sLblUO = ""
        If Ador("UON1").Value <> "" Then
            sLblUO = "(" & Ador("UON1").Value
            If Ador("UON2").Value <> "" Then
                sLblUO = sLblUO & " - " & Ador("UON2").Value
                If Ador("UON3").Value <> "" Then
                    sLblUO = sLblUO & " - " & Ador("UON3").Value & ") "
                Else
                    sLblUO = sLblUO & ") "
                End If
            Else
                sLblUO = sLblUO & ") "
            End If
        End If
        If Not IsNull(Ador("PRES4").Value) Then
            txtPresup.Text = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("PRES3").Value & " - " & Ador("PRES4").Value & " - " & Ador("DEN").Value
        ElseIf Not IsNull(Ador("PRES3").Value) Then
            txtPresup.Text = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("PRES3").Value & " - " & Ador("DEN").Value
        ElseIf Not IsNull(Ador("PRES2").Value) Then
            txtPresup.Text = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("DEN").Value
        ElseIf Not IsNull(Ador("PRES1").Value) Then
            txtPresup.Text = sLblUO & Ador("PRES1").Value & " - " & Ador("DEN").Value
        Else
            txtPresup.Text = sLblUO
        End If
        
        Ador.Close
        Set Ador = Nothing
        
    Else 'Se ha eliminado el presupuesto
        If bEliminarFitro = True Then
            m_vPresup1_1 = Null
            m_vPresup1_2 = Null
            m_vPresup1_3 = Null
            m_vPresup1_4 = Null
            m_vPresup2_1 = Null
            m_vPresup2_2 = Null
            m_vPresup2_3 = Null
            m_vPresup2_4 = Null
            m_vPresup3_1 = Null
            m_vPresup3_2 = Null
            m_vPresup3_3 = Null
            m_vPresup3_4 = Null
            m_vPresup4_1 = Null
            m_vPresup4_2 = Null
            m_vPresup4_3 = Null
            m_vPresup4_4 = Null
            txtPresup.Text = ""
            m_sUON1Pres = ""
            m_sUON2Pres = ""
            m_sUON3Pres = ""
            g_oConfVisor.HayCambios = True
        End If
    End If
    
    '******************** FILTRO DE DENOMINACI�N *******************
    If Not IsNull(g_oConfVisor.Denominacion) Then
        txtDenominacion = g_oConfVisor.Denominacion
        m_sDenominacion = g_oConfVisor.Denominacion
    Else
        m_sDenominacion = ""
    End If
    '******************** FILTRO DE ATRIBUTOS *******************
    m_bAtrBuscarVisible = g_oConfVisor.AtrBuscarVisible
    CargarAtrBuscar sdbgAtrBuscar, g_oConfVisor, m_sAdjDir(1), m_sAdjDir(0), sdbddAmbito, TAtributoAmbito.ProcesoAtr
    MostrarAtrBuscar m_bAtrBuscarVisible
    m_bCargandoFiltro = False
End Sub

Private Sub MostrarMenu()
    picMenu.Visible = True
    
    sdbgProcesos.Width = sdbgProcesos.Width - picMenu.Width
    sdbgProcesos.Left = 3120
    
    picSeparator1.Visible = True
    picSeparator2.Visible = True
    If m_bTotPend Then
        picTotPend.Visible = True
    End If
    If m_bTotCerr Then
        picTotCerr.Visible = True
    End If
    If m_bTotCerr Or m_bTotPend Then
        picTot.Visible = True
    End If
            
    'Trata los iconos
    picMostrarMenu.Visible = False
    picOcultarMenu.Visible = True
    
    g_oConfVisor.HayCambios = True
    g_oConfVisor.OcultarMenu = False
End Sub

Private Sub MostrarFiltro()
    'Hace visible el frame de filtro
    Dim bEncontrado As Boolean
    Dim i As Integer

    'BLoquea la pantalla hasta que se muestre el filtro
    LockWindowUpdate Me.hWnd
    
    frameFiltro.Visible = True
    
    'Pictures de mostrar y ocultar
    picMostrarFiltro.Top = frameFiltro.Height + Picture1.Height
    picMostrarMenu.Top = picMostrarFiltro.Top
    picOcultarMenu.Top = picMostrarFiltro.Top
    
    'Hace m�s peque�os la grid de procesos y el men�
    sdbgProcesos.Top = picMostrarFiltro.Top + 260
    picMenu.Top = sdbgProcesos.Top
    
    sdbgProcesos.Height = Me.Height - picMostrarFiltro.Top
    picMenu.Height = sdbgProcesos.Height
    
    'dimensiona el men�:
    sdbgMenu.MoveFirst
    i = 0
    
    While Not bEncontrado
        If sdbgMenu.Columns("ID").Value = CTOTABIERTOS Then
            bEncontrado = True
            If m_bTotPend Then
                picTotPend.Top = sdbgMenu.RowHeight * i + sdbgMenu.Top + 25 + Picture1.Height + frameFiltro.Height + picMostrarFiltro.Height
            End If
        End If
        i = i + 1
        sdbgMenu.MoveNext
    Wend
    
    picSeparator1.Top = sdbgMenu.RowHeight * i + sdbgMenu.Top + Picture1.Height + frameFiltro.Height + picMostrarFiltro.Height + 15
    
    bEncontrado = False
    
    While Not bEncontrado
        If sdbgMenu.Columns("ID").Value = CTOTCERRADOS Then
            bEncontrado = True
            If m_bTotCerr Then
                picTotCerr.Top = sdbgMenu.RowHeight * i + sdbgMenu.Top + 25 + Picture1.Height + frameFiltro.Height + picMostrarFiltro.Height
            End If
        End If
        i = i + 1
        sdbgMenu.MoveNext
    Wend
    
    picSeparator2.Top = sdbgMenu.RowHeight * i + sdbgMenu.Top + Picture1.Height + frameFiltro.Height + picMostrarFiltro.Height + 15
    
    bEncontrado = False
    
    While Not bEncontrado
        If sdbgMenu.Columns("ID").Value = CTOT Then
            bEncontrado = True
            If m_bTotCerr Or m_bTotPend Then
                picTot.Top = sdbgMenu.RowHeight * i + sdbgMenu.Top + 25 + Picture1.Height + frameFiltro.Height + picMostrarFiltro.Height
            End If
        End If
        i = i + 1
        sdbgMenu.MoveNext
    Wend
    
    sdbgMenu.MoveFirst
    
    'Trata los iconos
    picCerrarFiltro.Visible = True
    picAbrirFiltro.Visible = False
    lblMostraFiltro.caption = m_stxtOcultar
    
    g_oConfVisor.HayCambios = True
    g_oConfVisor.OcultarFiltro = False
    
    
    'Desbloquea la pantalla
    LockWindowUpdate 0&
End Sub

Private Sub txtDenominacion_Change()
    If m_bCargandoFiltro Then Exit Sub
        
    m_sDenominacion = txtDenominacion.Text
    g_oConfVisor.HayCambios = True
End Sub

Private Sub txtMaterial_KeyDown(KeyCode As Integer, Shift As Integer)
    If txtMaterial.Text = "" Then Exit Sub
    
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         cmdLimpiarMat_Click
    End If
End Sub

Private Sub txtMaterial_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim iNumMat As Integer
    
    iNumMat = 0
    If Not m_oGMN1Seleccionados Is Nothing Then
        iNumMat = iNumMat + m_oGMN1Seleccionados.Count
    End If
    If Not m_oGMN2Seleccionados Is Nothing Then
        iNumMat = iNumMat + m_oGMN2Seleccionados.Count
    End If
    If Not m_oGMN3Seleccionados Is Nothing Then
        iNumMat = iNumMat + m_oGMN3Seleccionados.Count
    End If
    If Not m_oGMN4Seleccionados Is Nothing Then
        iNumMat = iNumMat + m_oGMN4Seleccionados.Count
    End If
    
    If iNumMat > 1 Then
        'Mostrar la lista de materiales
        lstMultiMaterial.Height = iNumMat * (txtMaterial.Height - 30)
        If lstMultiMaterial.Height > cnMultiMatMaxHeight Then lstMultiMaterial.Height = cnMultiMatMaxHeight
        lstMultiMaterial.Visible = True
        lstMultiMaterial.ZOrder 0
    End If
End Sub

Private Sub lstMultiMaterial_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lWnd As Long
    
    'El c�digo siguiente imita el MouseLeave
    
    lWnd = GetCapture

    If lWnd <> lstMultiMaterial.hWnd Then
        'Dentro de lstMultiMaterial
        SetCapture lstMultiMaterial.hWnd
    Else
        If (X < 0) Or (X > lstMultiMaterial.Width) Or _
            (Y < 0) Or (Y > lstMultiMaterial.Height) Then
            'Fuera de lstMultiMaterial
            ReleaseCapture
            lstMultiMaterial.Visible = False
            lstMultiMaterial.ZOrder 1
        End If
    End If
End Sub

Private Sub txtPresup_KeyDown(KeyCode As Integer, Shift As Integer)
    If txtPresup.Text = "" Then Exit Sub
    
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         cmdLimpiarPres_Click
    End If
End Sub

Private Sub txtUO_KeyDown(KeyCode As Integer, Shift As Integer)
    If txtUO.Text = "" Then Exit Sub
    
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         cmdLimpiarUO_Click
    End If
End Sub

'ESTADO DEL PROCESO RESPECTO AL TRASPASO DE LAS ADJUDICACIONES
Private Function Col_GsToERP_Visible() As Boolean

    If gParametrosGenerales.gbIntegracion Then
        If Not (gParametrosGenerales.gbTraspasoAdjERP And gParametrosIntegracion.gaExportar(EntidadIntegracion.Adj)) Then
            'si esto no se cumple el usuario no lo ver� en la lista de campos a mostrar en el visor)
            Col_GsToERP_Visible = False   'no vera la columna
        Else
            Col_GsToERP_Visible = True   'vera la columna
        End If
    Else
        Col_GsToERP_Visible = False
    End If
    
End Function

