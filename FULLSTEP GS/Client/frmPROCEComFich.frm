VERSION 5.00
Begin VB.Form frmPROCEComFich 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Comentario"
   ClientHeight    =   2970
   ClientLeft      =   5025
   ClientTop       =   3495
   ClientWidth     =   5085
   Icon            =   "frmPROCEComFich.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2970
   ScaleWidth      =   5085
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkProcFich 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   550
      Width           =   4395
   End
   Begin VB.CommandButton cmdAceptar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "&Aceptar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1245
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2600
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2400
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   2600
      Width           =   1005
   End
   Begin VB.TextBox txtCom 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1665
      Left            =   120
      MaxLength       =   500
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   820
      Width           =   4815
   End
   Begin VB.Label lblFich 
      BackColor       =   &H00808000&
      Caption         =   "Fichero adjunto:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   1560
      TabIndex        =   4
      Top             =   120
      Width           =   3300
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Fichero adjunto:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "frmPROCEComFich"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public sOrigen As String
Public m_bDescargarFrm As Boolean
Dim m_bActivado As Boolean

Private m_sMsgError As String
Private Sub Arrange(valor As Boolean)
Dim iAbajo As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not valor Then
        If lblFich.Visible Then
            txtCom.Top = lblFich.Top + lblFich.Height + 75
        Else
            txtCom.Top = lblFich.Top
        End If
        iAbajo = txtCom.Top + txtCom.Height - cmdAceptar.Height - 50
        txtCom.Height = iAbajo
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPROCEComFich", "Arrange", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub cmdAceptar_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case sOrigen
        
        Case "frmPROCE"
                
                frmPROCE.g_sComentario = txtCom.Text
                frmPROCE.g_bCancelarEsp = False
        
        Case "frmPROCEItemEsp"
                
                frmPROCEItemEsp.g_sComentario = txtCom.Text
                frmPROCEItemEsp.g_bCancelarEsp = False
        Case "frmOFERecAdj"
                
                frmOFERecAdj.g_sComentario = txtCom.Text
                frmOFERecAdj.g_bCancelarEsp = False
        
        Case "frmArticuloEsp"
                
                frmArticuloEsp.g_sComentario = txtCom.Text
                
                If chkProcFich.Value = vbChecked Then
                    frmArticuloEsp.g_bSoloRuta = True
                Else
                    frmArticuloEsp.g_bSoloRuta = False
                End If
                frmArticuloEsp.g_bCancelarEsp = False
        
        Case "frmOFERec"
                
                frmOFERec.bCancelarEsp = False
                frmOFERec.sComentario = txtCom.Text
                
        Case "frmPROCEItemArticuloEspA", "frmPROCEItemArticuloEspI"
                
                frmPROCEItemArticuloEsp.g_sComentario = txtCom.Text
                If sOrigen = "frmPROCEItemArticuloEspA" Then
                    If chkProcFich.Value = vbChecked Then
                        frmPROCEItemArticuloEsp.g_bSoloRuta = True
                    Else
                        frmPROCEItemArticuloEsp.g_bSoloRuta = False
                    End If
                End If
                frmPROCEItemArticuloEsp.g_bCancelarEsp = False
                
        Case "frmPROVE"
                
                frmPROVE.g_sComentario = txtCom.Text
                frmPROVE.g_bCancelarEsp = False
        
        Case "frmCatalogo"
                
                frmCatalogo.bCancelarEsp = False
                frmCatalogo.sComentario = txtCom.Text
        
        Case "frmCatalogoAdjunArt"
                
                frmCatalogoAdjun.g_sComentario = txtCom.Text
                
                If chkProcFich.Value = vbChecked Then
                    frmCatalogoAdjun.g_bSoloRuta = True
                Else
                    frmCatalogoAdjun.g_bSoloRuta = False
                End If
                frmCatalogoAdjun.g_bCancelarEsp = False
                
        Case "frmCatalogoAtribEspYAdj"
                
                frmCatalogoAtribEspYAdj.g_sComentario = txtCom.Text
                
                If chkProcFich.Value = vbChecked Then
                    frmCatalogoAtribEspYAdj.g_bSoloRuta = True
                Else
                    frmCatalogoAtribEspYAdj.g_bSoloRuta = False
                End If
                frmCatalogoAtribEspYAdj.g_bCancelarEsp = False
                
        Case "frmCatalogoAdjun"
            
            frmCatalogoAdjun.g_bCancelarEsp = False
            frmCatalogoAdjun.g_sComentario = txtCom.Text
            
        Case "frmCATDatExtAdjun"
            frmCATDatExtAdjun.g_bCancelarEsp = False
            If chkProcFich.Value = vbChecked Then
                frmCATDatExtAdjun.g_bSoloRuta = True
            Else
                frmCATDatExtAdjun.g_bSoloRuta = False
            End If
            frmCATDatExtAdjun.g_sComentario = txtCom.Text
            
        Case "frmSeguimientoEsp", "frmPEDIDOSLin"
            frmSeguimientoEsp.ctlAdjunPedidos.g_bCancelarEsp = False
            frmSeguimientoEsp.ctlAdjunPedidos.g_sComentario = txtCom.Text
        Case "frmPEDIDOS"
            frmPEDIDOS.ctlAdjunPedidos.g_bCancelarEsp = False
            frmPEDIDOS.ctlAdjunPedidos.g_sComentario = txtCom.Text
        
        Case "frmPlantillasProce"
            frmPlantillasProce.g_bCancelarEsp = False
            If chkProcFich.Value = vbChecked Then
                frmPlantillasProce.g_bSoloRuta = True
            Else
                frmPlantillasProce.g_bSoloRuta = False
            End If
            
            frmPlantillasProce.g_sComentario = txtCom.Text
            
        Case "frmADJComent"
                
                frmAdjComent.g_sComentario = txtCom.Text
                
                If chkProcFich.Value = vbChecked Then
                    frmAdjComent.g_bSoloRuta = True
                Else
                    frmAdjComent.g_bSoloRuta = False
                End If
                frmAdjComent.g_bCancelarEsp = False
            
        Case "frmFormAdjuntos"
            frmFormAdjuntos.g_bCancelarEsp = False
            frmFormAdjuntos.g_bSoloRuta = False
            frmFormAdjuntos.g_sComentario = txtCom.Text
        
        Case "frmCatalogoAtribEspYAdj"
            frmCatalogoAtribEspYAdj.g_bCancelarEsp = False
            frmCatalogoAtribEspYAdj.g_sComentario = txtCom.Text
    End Select
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPROCEComFich", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cmdCancelar_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case sOrigen
        
        Case "frmPROCE"
                
                frmPROCE.g_bCancelarEsp = True
        
        Case "frmOFERec"
                
                frmOFERec.bCancelarEsp = True
        
        Case "frmOFERecAdj"
                frmOFERecAdj.g_bCancelarEsp = True
            
        Case "frmPROCEItemEsp"
                
                frmPROCEItemEsp.g_bCancelarEsp = True
            
        Case "frmArticuloEsp"
                
                frmArticuloEsp.g_bCancelarEsp = True
        
        Case "frmPROCEItemArticuloEspA", "frmPROCEItemArticuloEspI"
                
                frmPROCEItemArticuloEsp.g_bCancelarEsp = True
                
        Case "frmPROVE"
                
                frmPROVE.g_bCancelarEsp = True
    
        Case "frmCatalogo"
                
                frmCatalogo.bCancelarEsp = True
        
        Case "frmCatalogoAdjunArt"
                frmCatalogoAdjun.g_bCancelarEsp = True
        
        Case "frmCatalogoAtribEspYAdj"
                frmCatalogoAtribEspYAdj.g_bCancelarEsp = True
        
        Case "frmCatalogoAdjun"
            
            frmCatalogoAdjun.g_bCancelarEsp = True
                
        Case "frmCATDatExtAdjun"
            frmCATDatExtAdjun.g_bCancelarEsp = True
            
        Case "frmSeguimientoEsp", "frmPEDIDOSLin"
            frmSeguimientoEsp.ctlAdjunPedidos.g_bCancelarEsp = True
            
        Case "frmPlantillasProce"
            frmPlantillasProce.g_bCancelarEsp = True
        
        Case "frmADJComent"
            frmAdjComent.g_bCancelarEsp = True
            
        Case "frmFormAdjuntos"
            frmFormAdjuntos.g_bCancelarEsp = True
        Case "frmCatalogoAtribEspYAdj"
            frmCatalogoAtribEspYAdj.g_bCancelarEsp = True
        Case "frmPEDIDOS"
            frmPEDIDOS.ctlAdjunPedidos.g_bCancelarEsp = True
    End Select
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPROCEComFich", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCE_COMFICH, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmPROCEComFich.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        lblFich.caption = Ador(0).Value
        Ador.MoveNext
        chkProcFich.caption = Ador(0).Value
        
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPROCEComFich", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bActivado Then
        m_bActivado = True
    End If
    If Me.Visible Then txtCom.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPROCEComFich", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Load()
    Dim valor As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    If sOrigen = "frmArticuloEsp" Or sOrigen = "frmPROCEItemArticuloEspA" Or sOrigen = "frmCatalogoAdjunArt" Or sOrigen = "frmCatalogoAtribEspYAdj" Or _
       sOrigen = "frmCATDatExtAdjun" Or sOrigen = "frmPlantillasProce" Or sOrigen = "frmADJComent" Then

       valor = True
    Else
        With chkProcFich
            valor = False
            .Visible = False
            .Value = vbUnchecked
        End With
    End If
    Arrange valor
    CargarRecursos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPROCEComFich", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEComFich", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


