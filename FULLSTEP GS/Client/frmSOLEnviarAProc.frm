VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSOLEnviarAProc 
   BackColor       =   &H00808000&
   Caption         =   "DSolicitud de compra: "
   ClientHeight    =   7935
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10575
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSOLEnviarAProc.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7935
   ScaleWidth      =   10575
   StartUpPosition =   1  'CenterOwner
   Begin SSDataWidgets_B.SSDBGrid sdbgLineas 
      Height          =   4800
      Left            =   240
      TabIndex        =   8
      Top             =   2440
      Width           =   10100
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   23
      stylesets.count =   8
      stylesets(0).Name=   "apedido"
      stylesets(0).BackColor=   -2147483633
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSOLEnviarAProc.frx":0CB2
      stylesets(0).AlignmentPicture=   3
      stylesets(1).Name=   "RiesgoBajoamarillo"
      stylesets(1).ForeColor=   32768
      stylesets(1).BackColor=   14680063
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSOLEnviarAProc.frx":0E3E
      stylesets(2).Name=   "RiesgoAltoamarillo"
      stylesets(2).ForeColor=   202
      stylesets(2).BackColor=   14680063
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSOLEnviarAProc.frx":0E5A
      stylesets(3).Name=   "amarillo"
      stylesets(3).BackColor=   14680063
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmSOLEnviarAProc.frx":0E76
      stylesets(4).Name=   "RiesgoBajo"
      stylesets(4).ForeColor=   32768
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmSOLEnviarAProc.frx":0E92
      stylesets(5).Name=   "RiesgoAlto"
      stylesets(5).ForeColor=   202
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmSOLEnviarAProc.frx":0EAE
      stylesets(6).Name=   "RiesgoMedio"
      stylesets(6).ForeColor=   4227327
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmSOLEnviarAProc.frx":0ECA
      stylesets(7).Name=   "RiesgoMedioamarillo"
      stylesets(7).ForeColor=   4227327
      stylesets(7).BackColor=   14680063
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmSOLEnviarAProc.frx":0EE6
      MultiLine       =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      MaxSelectedRows =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   23
      Columns(0).Width=   926
      Columns(0).Caption=   "SEL"
      Columns(0).Name =   "SEL"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(0).ButtonsAlways=   -1  'True
      Columns(0).StyleSet=   "amarillo"
      Columns(1).Width=   1138
      Columns(1).Caption=   "NUM"
      Columns(1).Name =   "NUM"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).StyleSet=   "amarillo"
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "LINEA"
      Columns(2).Name =   "LINEA"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "GR_DESTINO_PROC"
      Columns(3).Name =   "GR_DESTINO_PROC"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   1
      Columns(3).StyleSet=   "amarillo"
      Columns(4).Width=   3201
      Columns(4).Caption=   "FAM_MAT"
      Columns(4).Name =   "FAM_MAT"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).StyleSet=   "amarillo"
      Columns(5).Width=   3200
      Columns(5).Caption=   "COD_ART"
      Columns(5).Name =   "COD_ART"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(5).StyleSet=   "amarillo"
      Columns(6).Width=   3201
      Columns(6).Caption=   "DESCR_ART"
      Columns(6).Name =   "DESCR_ART"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).StyleSet=   "amarillo"
      Columns(7).Width=   3200
      Columns(7).Caption=   "FEC_INI"
      Columns(7).Name =   "FEC_INI"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(7).StyleSet=   "amarillo"
      Columns(8).Width=   3200
      Columns(8).Caption=   "FEC_FIN"
      Columns(8).Name =   "FEC_FIN"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(8).StyleSet=   "amarillo"
      Columns(9).Width=   3201
      Columns(9).Caption=   "CANT"
      Columns(9).Name =   "CANT"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).NumberFormat=   "Standard"
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(9).StyleSet=   "amarillo"
      Columns(10).Width=   3201
      Columns(10).Caption=   "PRECUNI"
      Columns(10).Name=   "PRECUNI"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).NumberFormat=   "Standard"
      Columns(10).FieldLen=   256
      Columns(10).Locked=   -1  'True
      Columns(10).StyleSet=   "amarillo"
      Columns(11).Width=   3200
      Columns(11).Caption=   "UNIDAD"
      Columns(11).Name=   "UNIDAD"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(11).Locked=   -1  'True
      Columns(11).StyleSet=   "amarillo"
      Columns(12).Width=   3200
      Columns(12).Caption=   "IMPORTE"
      Columns(12).Name=   "IMPORTE"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).NumberFormat=   "Standard"
      Columns(12).FieldLen=   256
      Columns(12).Locked=   -1  'True
      Columns(12).StyleSet=   "amarillo"
      Columns(13).Width=   3200
      Columns(13).Caption=   "DEST"
      Columns(13).Name=   "DEST"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(13).Locked=   -1  'True
      Columns(13).StyleSet=   "amarillo"
      Columns(14).Width=   3200
      Columns(14).Caption=   "PAGO"
      Columns(14).Name=   "PAGO"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(14).Locked=   -1  'True
      Columns(14).StyleSet=   "amarillo"
      Columns(15).Width=   3200
      Columns(15).Caption=   "PROV"
      Columns(15).Name=   "PROV"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      Columns(15).Locked=   -1  'True
      Columns(15).Style=   1
      Columns(15).StyleSet=   "amarillo"
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "ESP"
      Columns(16).Name=   "ESP"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      Columns(16).Style=   4
      Columns(16).ButtonsAlways=   -1  'True
      Columns(17).Width=   3200
      Columns(17).Visible=   0   'False
      Columns(17).Caption=   "COD_PROVE"
      Columns(17).Name=   "COD_PROVE"
      Columns(17).DataField=   "Column 17"
      Columns(17).DataType=   8
      Columns(17).FieldLen=   256
      Columns(18).Width=   3200
      Columns(18).Visible=   0   'False
      Columns(18).Caption=   "GMN1"
      Columns(18).Name=   "GMN1"
      Columns(18).DataField=   "Column 18"
      Columns(18).DataType=   8
      Columns(18).FieldLen=   256
      Columns(19).Width=   3200
      Columns(19).Visible=   0   'False
      Columns(19).Caption=   "GMN2"
      Columns(19).Name=   "GMN2"
      Columns(19).DataField=   "Column 19"
      Columns(19).DataType=   8
      Columns(19).FieldLen=   256
      Columns(20).Width=   3200
      Columns(20).Visible=   0   'False
      Columns(20).Caption=   "GMN3"
      Columns(20).Name=   "GMN3"
      Columns(20).DataField=   "Column 20"
      Columns(20).DataType=   8
      Columns(20).FieldLen=   256
      Columns(21).Width=   3200
      Columns(21).Visible=   0   'False
      Columns(21).Caption=   "GMN4"
      Columns(21).Name=   "GMN4"
      Columns(21).DataField=   "Column 21"
      Columns(21).DataType=   8
      Columns(21).FieldLen=   256
      Columns(22).Width=   3200
      Columns(22).Visible=   0   'False
      Columns(22).Caption=   "RIESGO"
      Columns(22).Name=   "RIESGO"
      Columns(22).DataField=   "Column 22"
      Columns(22).DataType=   8
      Columns(22).FieldLen=   256
      _ExtentX        =   17815
      _ExtentY        =   8467
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   3600
      ScaleHeight     =   375
      ScaleWidth      =   3255
      TabIndex        =   11
      Top             =   7550
      Width           =   3255
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "DAceptar"
         Height          =   315
         Left            =   0
         TabIndex        =   9
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "DCancelar"
         Height          =   315
         Left            =   1500
         TabIndex        =   10
         Top             =   0
         Width           =   1215
      End
   End
   Begin VB.Frame fraCabecera 
      BackColor       =   &H00808000&
      Caption         =   "DSeleccione el proceso"
      ForeColor       =   &H00FFFFFF&
      Height          =   1695
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   10455
      Begin VB.CheckBox chkAdjuntos 
         BackColor       =   &H00808000&
         Caption         =   "DTraspasar los archivos adjuntos al proceso"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   135
         TabIndex        =   6
         Top             =   1080
         Value           =   1  'Checked
         Width           =   3700
      End
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   10000
         Picture         =   "frmSOLEnviarAProc.frx":0F02
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   360
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   2240
         TabIndex        =   2
         Top             =   360
         Width           =   825
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1455
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   660
         TabIndex        =   1
         Top             =   360
         Width           =   825
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1455
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
         Height          =   285
         Left            =   3980
         TabIndex        =   3
         Top             =   360
         Width           =   1065
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         Row.Count       =   1
         Col.Count       =   2
         Row(0).Col(0)   =   "1"
         Row(0).Col(1)   =   "Maquinaria para planta nueva"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6959
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
         Height          =   285
         Left            =   5060
         TabIndex        =   4
         Top             =   360
         Width           =   4930
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         Row.Count       =   1
         Col.Count       =   2
         Row(0).Col(0)   =   "Maquinaria para planta nueva"
         Row(0).Col(1)   =   "1"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5292
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1376
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   8696
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblGMN1_4 
         BackColor       =   &H00808000&
         Caption         =   "DComm:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   1715
         TabIndex        =   14
         Top             =   390
         Width           =   450
      End
      Begin VB.Label lblAnyo 
         BackColor       =   &H00808000&
         Caption         =   "DA�o:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   120
         TabIndex        =   13
         Top             =   390
         Width           =   525
      End
      Begin VB.Label lblProceso 
         BackColor       =   &H00808000&
         Caption         =   "DProceso:"
         ForeColor       =   &H00FFFFFF&
         Height          =   180
         Left            =   3200
         TabIndex        =   12
         Top             =   390
         Width           =   700
      End
   End
   Begin MSComctlLib.TabStrip ssTabGrupos 
      Height          =   5535
      Left            =   60
      TabIndex        =   7
      Top             =   1920
      Width           =   10455
      _ExtentX        =   18441
      _ExtentY        =   9763
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSOLEnviarAProc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 Option Explicit

'Variables p�blicas:
Public g_lIdSOL As Long
Public g_lNumVersion As Long
Public b_Bloqueo As Boolean

'Clases:
Private m_oProcesoPlant As cProceso
Private m_oProcesoNuevo As cProceso
Private m_oProcesoSeleccionado As cProceso
Private m_oSolicitudSeleccionada As CInstancia
Private m_oGMN1Seleccionado As CGrupoMatNivel1
Private m_oGruposGMN1 As CGruposMatNivel1
Private m_oProcesos As CProcesos

'Idiomas:
Private m_sIdiCaption As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_sIdiProve As String
Private m_sIdioma(2) As String

'Variables para func. combos
Private m_bRespetarCombo As Boolean
Private m_bCargarComboDesde As Boolean

'Seguridad
Private m_bRMat As Boolean
Private m_bRAsig As Boolean
Private m_bRCompResp As Boolean
Private m_bREqpAsig As Boolean
Private m_bRUsuAper As Boolean
Private m_bRUsuUON As Boolean
Private m_bRUsuDep As Boolean
Private m_bRDest As Boolean
Private m_bModifProcPlantilla As Boolean
Private m_bModifProcDesdeSolic As Boolean

'C�digo y equipo del comprador
Private m_sCodComp As String
Private m_sCodEqp As String
Private m_arrMatComp() As Variant

'Array con las familias de material
Private m_arrFamMat() As Variant '0 GMN1, 1 GMN2, 2 GMN3, 3 GMN4, 4 DEN

Private oPlantillas As CPlantillas
Public oPlantillaSeleccionada As CPlantilla
Private restriccion As Boolean

Private m_bModificarEstructuraProceso As Boolean
Private m_bPermProcMultimaterial As Boolean  'Permitir abrir procesos multimaterial
Private m_sErrDistribucion As String

Public Property Get ProcNuevo() As cProceso
    Set ProcNuevo = m_oProcesoNuevo
End Property
Public Property Set ProcNuevo(ByVal valor As cProceso)
    Set m_oProcesoNuevo = valor
End Property
Public Property Get ProcSeleccionado() As cProceso
    Set ProcSeleccionado = m_oProcesoSeleccionado
End Property

Public Property Set ProcSeleccionado(ByVal valor As cProceso)
    Set m_oProcesoSeleccionado = valor
End Property

Private Sub MostrarDetalleProveedor(ByVal CodProve As String)
Dim oProves As CProveedores
Dim oProve As CProveedor
Dim oCon As CContacto
        
    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.CargarTodosLosProveedoresDesde3 1, CodProve, , True
    
    If oProves.Count = 0 Then
        oMensajes.DatoEliminado m_sIdiProve
        Set oProves = Nothing
        Exit Sub
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor CodProve
    
    Set frmProveDetalle.g_oProveSeleccionado = oProve
    frmProveDetalle.caption = CodProve & "  " & oProve.Den
    frmProveDetalle.lblCodPortProve = NullToStr(oProve.CodPortal)
    frmProveDetalle.g_bPremium = oProve.EsPremium
    frmProveDetalle.g_bActivo = oProve.EsPremiumActivo
    frmProveDetalle.lblDir = NullToStr(oProve.Direccion)
    frmProveDetalle.lblCp = NullToStr(oProve.cP)
    frmProveDetalle.lblPob = NullToStr(oProve.Poblacion)
    frmProveDetalle.lblPaiCod = NullToStr(oProve.CodPais)
    frmProveDetalle.lblPaiDen = NullToStr(oProve.DenPais)
    frmProveDetalle.lblMonCod = NullToStr(oProve.CodMon)
    frmProveDetalle.lblMonDen = NullToStr(oProve.DenMon)
    frmProveDetalle.lblProviCod = NullToStr(oProve.CodProvi)
    frmProveDetalle.lblProviDen = NullToStr(oProve.DenProvi)
    frmProveDetalle.lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
    frmProveDetalle.lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
    frmProveDetalle.lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
    
    If Trim(oProve.Calif1) <> "" Then frmProveDetalle.lblCal1Den = oProve.Calif1
    If Trim(oProve.Calif2) <> "" Then frmProveDetalle.lblcal2den = oProve.Calif2
    If Trim(oProve.Calif3) <> "" Then frmProveDetalle.lblcal3den = oProve.Calif3
        
    frmProveDetalle.lblCal1Val = DblToStr(oProve.Val1)
    frmProveDetalle.lblCal2Val = DblToStr(oProve.Val2)
    frmProveDetalle.lblCal3Val = DblToStr(oProve.Val3)

    frmProveDetalle.lblURL = NullToStr(oProve.URLPROVE)

    oProve.CargarTodosLosContactos , , , , True, , , , , , , True
    
    For Each oCon In oProve.Contactos
        frmProveDetalle.sdbgContactos.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.Departamento & Chr(m_lSeparador) & oCon.Cargo _
        & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Port & Chr(m_lSeparador)
    Next
        
    Set oProves = Nothing
    Set oProve = Nothing
        
    frmProveDetalle.Show vbModal
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOL_ENVIAR_PROC, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        m_sIdiCaption = Ador(0).Value   '1 Solicitud de compra
        Ador.MoveNext
        fraCabecera.caption = Ador(0).Value '2 Seleccione el proceso
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value '3 A�o:
        Ador.MoveNext
        lblProceso.caption = Ador(0).Value '4 Proce:
        Ador.MoveNext
        Ador.MoveNext
        chkAdjuntos.caption = Ador(0).Value  '6  Traspasar los archivos adjuntos al proceso
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value '7 &Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '8 &Cancelar
        Ador.MoveNext
        sdbgLineas.Columns("SEL").caption = Ador(0).Value  '9 Sel.
        Ador.MoveNext
        sdbgLineas.Columns("COD_ART").caption = Ador(0).Value  '10 Art�culo
        Ador.MoveNext
        sdbgLineas.Columns("DESCR_ART").caption = Ador(0).Value  '11 Descripci�n
        Ador.MoveNext
        sdbgLineas.Columns("FEC_INI").caption = Ador(0).Value  '12 Inicio suministro
        Ador.MoveNext
        sdbgLineas.Columns("FEC_FIN").caption = Ador(0).Value  '13 Fin suministro
        Ador.MoveNext
        sdbgLineas.Columns("CANT").caption = Ador(0).Value  '14 Cantidad
        Ador.MoveNext
        sdbgLineas.Columns("PRECUNI").caption = Ador(0).Value  '15 Precio unitario
        Ador.MoveNext
        sdbgLineas.Columns("UNIDAD").caption = Ador(0).Value  '16 Unidad
        Ador.MoveNext
        sdbgLineas.Columns("IMPORTE").caption = Ador(0).Value  '17 Importe
        Ador.MoveNext
        sdbgLineas.Columns("DEST").caption = Ador(0).Value  '18 Destino
        Ador.MoveNext
        sdbgLineas.Columns("PAGO").caption = Ador(0).Value  '19 Forma de pago
        Ador.MoveNext
        sdbgLineas.Columns("PROV").caption = Ador(0).Value  '20 Proveedor
        m_sIdiProve = Ador(0).Value
        Ador.MoveNext
        sdbgLineas.Columns("ESP").caption = Ador(0).Value  '21 Especificaciones
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value '22 S�
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value '23 No
        Ador.MoveNext
        m_sIdioma(1) = Ador(0).Value  '24 C�digo
        Ador.MoveNext
        m_sIdioma(2) = Ador(0).Value  '25 Material
        Ador.MoveNext
        Me.sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value    '26 C�digo
        Me.sdbcProceCod.Columns(0).caption = Ador(0).Value
        Me.sdbcProceDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcProceCod.Columns(1).caption = Ador(0).Value '27 Denominaci�n
        Me.sdbcProceDen.Columns(0).caption = Ador(0).Value
        Me.sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgLineas.Columns("FAM_MAT").caption = Ador(0).Value '29 Familia de material
        Ador.MoveNext
        sdbgLineas.Columns("NUM").caption = Ador(0).Value               'N� Lin.
        Ador.MoveNext
        sdbgLineas.Columns("GR_DESTINO_PROC").caption = Ador(0).Value   'Grupo destino proceso
        Ador.MoveNext
        m_sErrDistribucion = Ador(0).Value  '"La distribuc��n del proceso no coincide con la del art�culo"
        Ador.Close
    End If

    Set Ador = Nothing
    lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
End Sub

Private Sub CargarDatosInstancia()
Dim oInstancias As CInstancias
Dim oGrupo As CGrupo
Dim oComprador As CCompradores
Dim Ador As Ador.Recordset
Dim i As Integer

    'Carga los datos de la solicitud:
    Set oInstancias = oFSGSRaiz.Generar_CInstancias
    oInstancias.BuscarSolicitudes OrdSolicPorId, , g_lIdSOL
    Set m_oSolicitudSeleccionada = oInstancias.Item(1)
    Set oInstancias = Nothing
    m_oSolicitudSeleccionada.NumVersion = g_lNumVersion
    
    'Carga los grupos:
    m_oSolicitudSeleccionada.CargarGrupos
    
    'Si se trata de un comprador con restricci�n de material guardamos sus materiales en un array.
    Set oComprador = Nothing
    Set oComprador = oFSGSRaiz.generar_CCompradores
    If m_bRMat Then
        m_arrMatComp = oComprador.DevolverMaterialesDeUnComprador(m_sCodEqp, m_sCodComp)
    End If
        
    'Carga los materiales, a nivel de campo:
    i = 1
    Set Ador = m_oSolicitudSeleccionada.DevolverMaterialesInstancia_Campo
    While Not Ador.EOF
        ReDim Preserve m_arrFamMat(4, i - 1)
        m_arrFamMat(0, i - 1) = Ador.Fields("VAL_GMN1")
        m_arrFamMat(1, i - 1) = Ador.Fields("VAL_GMN2")
        m_arrFamMat(2, i - 1) = Ador.Fields("VAL_GMN3")
        m_arrFamMat(3, i - 1) = Ador.Fields("VAL_GMN4")
        m_arrFamMat(4, i - 1) = Ador.Fields("DEN")
        i = i + 1
        Ador.MoveNext
    Wend
    Ador.Close
    Set Ador = Nothing
    
    'Carga los materiales, a nivel de desglose:
    'i = 1
    Set Ador = m_oSolicitudSeleccionada.DevolverMaterialesInstancia_Desglose
    While Not Ador.EOF
        ReDim Preserve m_arrFamMat(4, i - 1)
        m_arrFamMat(0, i - 1) = Ador.Fields("VAL_GMN1")
        m_arrFamMat(1, i - 1) = Ador.Fields("VAL_GMN2")
        m_arrFamMat(2, i - 1) = Ador.Fields("VAL_GMN3")
        m_arrFamMat(3, i - 1) = Ador.Fields("VAL_GMN4")
        m_arrFamMat(4, i - 1) = Ador.Fields("DEN")
        i = i + 1
        Ador.MoveNext
    Wend
    Ador.Close
    Set Ador = Nothing
    
    'carga las l�neas de desglose y genera la estructura del proceso:
    GenerarNuevosGrupos
    
    'A�ade los grupos al tab y se posiciona en el primero:
    ssTabGrupos.Tabs.clear
    If Not m_oProcesoNuevo.Grupos Is Nothing Then
        For Each oGrupo In m_oProcesoNuevo.Grupos
            ssTabGrupos.Tabs.Add , "G" & oGrupo.Codigo, oGrupo.Codigo & "-" & oGrupo.Den
        Next
    End If
    If ssTabGrupos.Tabs.Count > 0 Then
        ssTabGrupos.Tabs(1).Selected = True
    End If
End Sub

''' <summary>
''' Click del boton Aceptar para enviar solicitud a proceso. En funcion de los datos generara enviar los datos al proceso o pedira que se rellenen los que faltan
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Tiempo m�ximo: Puede superar los 2 segundos </remarks>
Private Sub cmdAceptar_Click()
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim i As Integer
    Dim adoResMat As New ADODB.Recordset
    Dim Ador As ADODB.Recordset
    Dim iResp As Integer
    Dim k As Long
    Dim m As Long
    Dim b_CambiarGrupo As Boolean
    Dim X As Long
    Dim bYaExisteGrupo As Boolean
    Dim Index As Long
    Dim Item As CItem
    Dim oUON As IUon
    Dim oUons As CUnidadesOrganizativas
    Dim oGrupoForm As CFormGrupo
    Dim oCampo As CFormItem
    Dim oDesglose As CFormItem
    Dim sVariables() As String
    Dim sCentro As String
    Dim sOrgCompras As String
    Dim iNumGru As Integer
    If sdbcProceCod.Text = "" Then
        oMensajes.SeleccioneProceso
        Exit Sub
    End If
       
    Index = 1
    
    Screen.MousePointer = vbHourglass
       
    'Traslado de los items de un grupo a otro seg�n lo seleccionado
    'Llega aqui a trasladar cuando se ha seleccionado definitivamente un proceso
    iNumGru = m_oProcesoNuevo.Grupos.Count
InicioPorCambio:

    For k = 1 To iNumGru
        b_CambiarGrupo = False
        If Not m_oProcesoNuevo.Grupos.Item(k).Items Is Nothing Then
            For m = 1 To m_oProcesoNuevo.Grupos.Item(k).Items.Count
                If Not m_oProcesoNuevo.Grupos.Item(k).Items.Item(m).eliminado Then
                    If m_oProcesoNuevo.Grupos.Item(k).Items.Item(m).grupoCod <> m_oProcesoNuevo.Grupos.Item(k).Codigo Then
                        b_CambiarGrupo = True
    
                        If b_CambiarGrupo Then
                            
                            bYaExisteGrupo = False
                            For X = 1 To iNumGru
                                If m_oProcesoNuevo.Grupos.Item(X).Codigo = m_oProcesoNuevo.Grupos.Item(k).Items.Item(m).grupoCod Then
                                    Set oGrupo = m_oProcesoNuevo.Grupos.Item(X)
                                    bYaExisteGrupo = True
                                    Exit For
                                End If
                            Next
                            If Not bYaExisteGrupo Then
                                For X = 1 To m_oProcesoSeleccionado.Grupos.Count
                                    'Miramos si el grupo destino ya existe para a�adirlo o no
                                    If m_oProcesoSeleccionado.Grupos.Item(X).Codigo = m_oProcesoNuevo.Grupos.Item(k).Items.Item(m).grupoCod Then
                                        For Each oGrupo In m_oProcesoNuevo.Grupos
                                            If m_oProcesoSeleccionado.Grupos.Item(X).Codigo = oGrupo.Codigo Then
                                                bYaExisteGrupo = True
                                                Exit For
                                            End If
                                        Next
                                        If Not bYaExisteGrupo Then
                                            Set oGrupo = m_oProcesoNuevo.Grupos.Add(m_oProcesoNuevo, m_oProcesoSeleccionado.Grupos.Item(X).Codigo, m_oProcesoSeleccionado.Grupos.Item(X).Den, m_oProcesoSeleccionado.Grupos.Item(X).Codigo)
                                            oGrupo.IdGrupoSolicit = m_oProcesoNuevo.Grupos.Item(k).IdGrupoSolicit
                                            bYaExisteGrupo = True
                                        End If
                                        Exit For
                                    End If
                                Next
                            End If
    
                            If bYaExisteGrupo Then
                                'El grupo destino ya existe. Ahora se mira si Solo hay que trasladar el item
                                
                                If oGrupo.Items Is Nothing Then
                                    Set oGrupo.Items = oFSGSRaiz.Generar_CItems
                                End If
                                Set Item = m_oProcesoNuevo.Grupos.Item(k).Items.Item(m)
                                oGrupo.Items.Add m_oProcesoNuevo, Item.Id, Item.DestCod, Item.UniCod, Item.PagCod, Item.Cantidad, Item.FechaInicioSuministro, Item.FechaFinSuministro, Item.ArticuloCod, Item.Descr, Item.Precio, Item.Presupuesto, , , , , , , , , , Item.grupoCod, , Item.SolicitudId, , , , Item.GMN1Cod, Item.GMN2Cod, Item.GMN3Cod, Item.GMN4Cod, Item.AtributosEspecificacion, , Item.CopiaCampoArt, , , Item.IdLineaSolicit, Item.IdCampoSolicit
                                m_oProcesoNuevo.Grupos.Item(k).Items.Remove (CStr(m_oProcesoNuevo.Grupos.Item(k).Items.Item(m).Id))
                                GoTo InicioPorCambio
                            End If
                        End If
                    End If
                End If
            Next m
        End If
    Next k
    
    'Si se ha realizado ya el traslado de items, no se deja volver a modificar, porque..
    'Internamente se ha generado la estructura en m_oProcesoNuevo, pero visualmente no se muestra el cambio
    b_Bloqueo = True
    
    Dim restriccion As Boolean
    Dim iItems As Integer
    restriccion = CargarPlantilla(m_oProcesoSeleccionado)
    
    'Moneda y cambio ser�n los del proceso destino
    m_oProcesoNuevo.MonCod = m_oProcesoSeleccionado.MonCod
    m_oProcesoNuevo.Cambio = m_oProcesoSeleccionado.Cambio
    
    If (Not IsNull(m_oProcesoSeleccionado.Plantilla) And m_bModifProcPlantilla = False) Or _
    (m_oProcesoSeleccionado.DesdeSolicitud = 1 And m_bModifProcDesdeSolic = False) Then
        
        m_bModificarEstructuraProceso = False
        
        i = oMensajes.AvisoModificarEstructuraProceso()
        If i = vbYes Then
        Else
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        Screen.MousePointer = vbHourglass
    Else
        m_bModificarEstructuraProceso = True
    End If

    m_oProcesoNuevo.DefDistribUON = m_oProcesoSeleccionado.DefDistribUON
    If m_oProcesoNuevo.DefDistribUON = EnProceso Then
        If m_oProcesoSeleccionado.HayDistribucionUON Then
            m_oProcesoNuevo.HayDistribucionUON = True
    
            For Each oUON In m_oProcesoSeleccionado.getUonsDistribucion
                If oUON.Nivel = 3 Then
                    Set m_oProcesoNuevo.DistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                    m_oProcesoNuevo.DistsNivel3.Add Nothing, oUON.CodUnidadOrgNivel1, oUON.CodUnidadOrgNivel2, oUON.CodUnidadOrgNivel3, 100, , 0
                ElseIf oUON.Nivel = 2 Then
                    Set m_oProcesoNuevo.DistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                    m_oProcesoNuevo.DistsNivel2.Add Nothing, oUON.CodUnidadOrgNivel1, oUON.CodUnidadOrgNivel2, 100, , 0
                ElseIf oUON.Nivel = 1 Then
                    Set m_oProcesoNuevo.DistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
                    m_oProcesoNuevo.DistsNivel1.Add Nothing, oUON.CodUnidadOrgNivel1, 100, , 0
                End If
            Next
        End If
    ElseIf m_oProcesoNuevo.DefDistribUON = EnGrupo Then
        If Not m_oProcesoNuevo.Grupos Is Nothing Then
            For Each oGrupo In m_oProcesoNuevo.Grupos
                If Not m_oProcesoSeleccionado.Grupos.Item(oGrupo.Codigo) Is Nothing Then
                    'Se le asigna la distribuci�n existente en el grupo destino para ese grupo
                    For Each oUON In m_oProcesoSeleccionado.getUonsDistribucion(, m_oProcesoSeleccionado.Grupos.Item(oGrupo.Codigo).Id)
                        If oUON.Nivel = 3 Then
                            Set oGrupo.DistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                            oGrupo.DistsNivel3.Add Nothing, oUON.CodUnidadOrgNivel1, oUON.CodUnidadOrgNivel2, oUON.CodUnidadOrgNivel3, 100, , 0
                        ElseIf oUON.Nivel = 2 Then
                            Set oGrupo.DistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                            oGrupo.DistsNivel2.Add Nothing, oUON.CodUnidadOrgNivel1, oUON.CodUnidadOrgNivel2, 100, , 0
                        ElseIf oUON.Nivel = 1 Then
                            Set oGrupo.DistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
                            oGrupo.DistsNivel1.Add Nothing, oUON.CodUnidadOrgNivel1, 100, , 0
                        End If
                    Next
                End If
            Next
        End If
    
        'FALTA Metido en Pruebas 31900.9/2015/67 para HACERLO mas tarde
        'cproceso.EnviarSolicitudAProceso si tienes permiso para crear grupos y el grupo q va a ser insertado por esta solicitud es nuevo ...
        '   Mirar si tiene permiso
        '   Mirar si va a ser nuevgo grupo
        '   Mirar si dos grupos, 1 coincide otro no, q pida distrib del nuevo.
        '   Mirar si dos grupos, 1 coincide otro no, q solo grabe la distrib del nuevo.
    ElseIf m_oProcesoNuevo.DefDistribUON = EnItem Then
        'El item sera nuevo por lo q seguro q en bbdd no esta la distribuci�n.
        'Si hay UON/Org compras/Centro SM en la solicitud a nivel de desglose o formulario lleva esa.
        'Si no, y la solicitud tiene UON/Org compras/Centro SM a nivel de formulario se coge esa
        If Not m_oProcesoNuevo.Grupos Is Nothing Then
            'Obtener la distribuci�n
            For Each oGrupoForm In m_oSolicitudSeleccionada.Grupos
                If oGrupoForm.Orden = 1 Then
                    Dim bCentro As Boolean
                    Dim bOrgCompras As Boolean
                    Dim bUON As Boolean
                    Dim Centro As Variant
                    Dim OrgCompras As Variant
                    Dim UniOrganizativa As Variant
    
                    For Each oCampo In oGrupoForm.CAMPOS
                        If oCampo.CampoGS = TipoCampoGS.OrganizacionCompras Or oCampo.CampoGS = TipoCampoGS.UnidadOrganizativa Or oCampo.CampoGS = TipoCampoGS.Centro Or oCampo.CampoGS = TipoCampoGS.CentroCoste Then
                            If oCampo.CampoGS = TipoCampoGS.Centro Then
                                bCentro = True
                                Centro = oCampo.valorText
                                If Not IsNull(Centro) Then m_oSolicitudSeleccionada.RecogerUON bCentro, bOrgCompras, bUON, Centro, OrgCompras, UniOrganizativa, NullToStr(oCampo.valorText), m_oProcesoNuevo
                            ElseIf oCampo.CampoGS = TipoCampoGS.OrganizacionCompras Then
                                If Not bCentro Then
                                    bOrgCompras = True
                                    OrgCompras = oCampo.valorText
                                    If Not IsNull(OrgCompras) Then m_oSolicitudSeleccionada.RecogerUON bCentro, bOrgCompras, bUON, Centro, OrgCompras, UniOrganizativa, NullToStr(oCampo.valorText), m_oProcesoNuevo
                                End If
                            ElseIf oCampo.CampoGS = TipoCampoGS.UnidadOrganizativa Then
                                If Not bCentro And Not bOrgCompras Then
                                    bUON = True
                                    UniOrganizativa = oCampo.valorText
                                    If Not IsNull(UniOrganizativa) Then m_oSolicitudSeleccionada.RecogerUON bCentro, bOrgCompras, bUON, Centro, OrgCompras, UniOrganizativa, NullToStr(oCampo.valorText), m_oProcesoNuevo
                                End If
                            ElseIf oCampo.CampoGS = TipoCampoGS.CentroCoste Then
                                If Not bCentro And Not bOrgCompras And Not bUON Then
                                    If Not IsNull(oCampo.valorText) Then m_oSolicitudSeleccionada.RecogerUON bCentro, bOrgCompras, bUON, Centro, OrgCompras, UniOrganizativa, NullToStr(oCampo.valorText), m_oProcesoNuevo
                                End If
                            End If
                        End If
                    Next
                End If
            Next
            
            'Asignar la distribuci�n a los items que no la tienen
            For Each oGrupo In m_oProcesoNuevo.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If NullToStr(oItem.UON1Cod) = "" And NullToStr(oItem.UON2Cod) = "" And NullToStr(oItem.UON3Cod) = "" Then
                            If Not m_oProcesoNuevo.DistsNivel3 Is Nothing Then
                                oItem.UON1Cod = m_oProcesoNuevo.DistsNivel3.Item(1).CodUON1
                                oItem.UON2Cod = m_oProcesoNuevo.DistsNivel3.Item(1).CodUON2
                                oItem.UON3Cod = m_oProcesoNuevo.DistsNivel3.Item(1).CodUON3
                            ElseIf Not m_oProcesoNuevo.DistsNivel2 Is Nothing Then
                                oItem.UON1Cod = m_oProcesoNuevo.DistsNivel2.Item(1).CodUON1
                                oItem.UON2Cod = m_oProcesoNuevo.DistsNivel2.Item(1).CodUON2
                            ElseIf Not m_oProcesoNuevo.DistsNivel1 Is Nothing Then
                                oItem.UON1Cod = m_oProcesoNuevo.DistsNivel1.Item(1).CodUON1
                            End If
                        End If
                    Next
                End If
            Next
        End If
        Set oItem = Nothing
    End If
        
    'Comprobar q todos los items a insertar cumplen con la distribuci�n (ART4_UON).
    If Not m_oProcesoNuevo.Grupos Is Nothing Then
        Set oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
        For Each oGrupo In m_oProcesoNuevo.Grupos
            If Not oGrupo.Items Is Nothing Then
                For Each oItem In oGrupo.Items
                    Select Case m_oProcesoSeleccionado.DefDistribUON
                        Case EnProceso, EnGrupo
                            Set oItem.proceso = m_oProcesoSeleccionado
                            If ("" & oItem.ArticuloCod) <> "" Then 'Artic NO codificados-> todos uons. Cumple.
                                If Not (oItem.SePuedeAsignar(True)) Then
                                    Screen.MousePointer = vbNormal
                                    oMensajes.mensajeGenericoOkOnly m_sErrDistribucion & " " & oItem.Articulo.uons.toString
                                    b_Bloqueo = False
                                    Exit Sub
                                End If
                            End If
                        Case EnItem
                            For Each oGrupoForm In m_oSolicitudSeleccionada.Grupos
                                oGrupoForm.CargarTodosLosCampos
                                'SI CORRESPONDEN AL PRIMER GRUPO
                                For Each oCampo In oGrupoForm.CAMPOS
                                    If ((oCampo.CampoGS = TipoCampoGS.Desglose) Or (IsNull(oCampo.CampoGS) And oCampo.Tipo = TipoDesglose)) Then
                                        'Carga los campos de desglose:
                                        oCampo.CargarDesglose
                                        Set Ador = oCampo.DevolverLineasDesglose(m_sIdiTrue, m_sIdiFalse)
                                        Do While Not Ador.EOF
                                            If Ador.Fields("LINEA").Value = oItem.IdLineaSolicit Then
                                                For Each oDesglose In oCampo.Desglose
                                                    Select Case oDesglose.CampoGS
                                                        Case TipoCampoGS.Centro
                                                            If Ador.Fields.Item("C_" & oDesglose.Id).Value & "" <> "" Then
                                                                sVariables = Split(Ador.Fields.Item("C_" & oDesglose.Id).Value & "", "-")
                                                                sCentro = Trim(sVariables(0))
                                                            End If
                                                        Case TipoCampoGS.OrganizacionCompras
                                                            If Ador.Fields.Item("C_" & oDesglose.Id).Value & "" <> "" Then
                                                                sVariables = Split(Ador.Fields.Item("C_" & oDesglose.Id).Value & "", "-")
                                                                sOrgCompras = Trim(sVariables(0))
                                                            End If
                                                    End Select
                                                Next
                                            End If
                                            Ador.MoveNext
                                        Loop
                                        Ador.Close
                                        Set Ador = Nothing
                                    End If
                                Next
                            Next
                            If sCentro <> "" And sOrgCompras <> "" Then
                                Set oUON = oUons.ObtenerUONDeCentroOrgCompras(sCentro, sOrgCompras)
                                oItem.UON1Cod = oUON.CodUnidadOrgNivel1
                                oItem.UON2Cod = oUON.CodUnidadOrgNivel2
                                oItem.UON3Cod = oUON.CodUnidadOrgNivel3
                            End If
                            sCentro = ""
                            sOrgCompras = ""
                    End Select
                Next
            End If
        Next
        Set oUons = Nothing
    End If
    
    iItems = 0
    If Not m_oProcesoNuevo.Grupos Is Nothing Then
        For Each oGrupo In m_oProcesoNuevo.Grupos
            If Not oGrupo.Items Is Nothing Then
                For Each oItem In oGrupo.Items
                    iItems = iItems + 1
                    If oItem.eliminado = False Then
                        'Si tiene y no puede abrir procesos con items no codificados le mostramos el error
                        If Not basParametros.gParametrosGenerales.gbPermAbrirItemsSinArtCod Then
                            If oItem.ArticuloCod = "" Then
                                Screen.MousePointer = vbNormal
                                oMensajes.ImposibleAbrirProcesoConItemsNoCodificados
                                Exit Sub
                            End If
                        End If
                    End If
                    'edu T98 asegurarse que no hay materiales restringidos en la plantilla del proceso destino

                    If restriccion Then
                        If oPlantillaSeleccionada.AdmiteMaterial(oItem, oPlantillaSeleccionada.Id) = False Then
                            oItem.eliminado = True
                        End If
                    End If
                    
                    If oItem.eliminado = True Then
                        iItems = iItems - 1
                    End If
                    
                Next
            End If
        Next
    End If
    
    If iItems = 0 Then
        Screen.MousePointer = vbNormal
        oMensajes.SeleccioneMaterial
        Exit Sub
    End If
    
    If m_bPermProcMultimaterial Then
        Set adoResMat = m_oProcesoSeleccionado.MaterialesEnProceso
        If adoResMat.RecordCount = 1 Then
            If Not m_oProcesoNuevo.Grupos Is Nothing Then
                iResp = -1
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If oItem.eliminado = False Then
                                If Not m_oProcesoSeleccionado.ExisteMaterialEnProceso(oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN3Cod, oItem.GMN4Cod) Then
                                    iResp = oMensajes.MensajeYesNo(948)
                                    If iResp = vbNo Then
                                        Screen.MousePointer = vbNormal
                                        adoResMat.Close
                                        Set adoResMat = Nothing
                                        Exit Sub
                                    Else
                                        Exit For
                                    End If
                                End If
                            End If
                        Next
                        If iResp = vbYes Then
                            Exit For
                        End If
                    End If
                Next
            End If
        ElseIf adoResMat.RecordCount = 0 And iItems > 1 Then
            'Comprobar si se van a enviar items de distinto material
            Dim sGMN1Cod As String
            Dim sGMN2Cod As String
            Dim sGMN3Cod As String
            Dim sGMN4Cod As String
            Dim bMultiMat As Boolean
            
            bMultiMat = False
            For Each oGrupo In m_oProcesoNuevo.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If oItem.eliminado = False Then
                            If sGMN1Cod <> "" And sGMN2Cod <> "" And sGMN3Cod <> "" And sGMN4Cod <> "" Then
                                If sGMN1Cod <> oItem.GMN1Cod Or sGMN2Cod <> oItem.GMN2Cod Or sGMN3Cod <> oItem.GMN3Cod Or sGMN4Cod <> oItem.GMN4Cod Then
                                    bMultiMat = True
                                    Exit For
                                End If
                            Else
                                sGMN1Cod = oItem.GMN1Cod
                                sGMN2Cod = oItem.GMN2Cod
                                sGMN3Cod = oItem.GMN3Cod
                                sGMN4Cod = oItem.GMN4Cod
                            End If
                        End If
                    Next
                    
                    If bMultiMat Then
                        iResp = oMensajes.MensajeYesNo(948)
                        If iResp = vbNo Then
                            Screen.MousePointer = vbNormal
                            adoResMat.Close
                            Set adoResMat = Nothing
                            b_Bloqueo = False
                            Exit Sub
                        End If
                        Screen.MousePointer = vbHourglass
                    End If
                End If
            Next
        End If
        adoResMat.Close
        Set adoResMat = Nothing
    End If
    
    'Elimina de la colecci�n los art�culos que se hayan deseleccionado:
    If Not m_oProcesoNuevo.Grupos Is Nothing Then
        For Each oGrupo In m_oProcesoNuevo.Grupos
            If Not oGrupo.Items Is Nothing Then
                For Each oItem In oGrupo.Items
                    If oItem.eliminado = True Then
                        oGrupo.Items.Remove (CStr(oItem.Id))
                    End If
                Next
            End If
        Next
    End If
    
    If Not gParametrosGenerales.gbPMCargarTodosGrupos Then
        'Elimina de la colecci�n los grupos sin art�culos seleccionados
        If Not m_oProcesoNuevo.Grupos Is Nothing Then
            For i = m_oProcesoNuevo.Grupos.Count To 1 Step -1
                Set oGrupo = m_oProcesoNuevo.Grupos.Item(i)
                
                If Not oGrupo.Items Is Nothing Then
                    If oGrupo.Items.Count = 0 Then
                        m_oProcesoNuevo.Grupos.Remove (CStr(oGrupo.Codigo))
                    End If
                Else
                    m_oProcesoNuevo.Grupos.Remove (CStr(oGrupo.Codigo))
                End If
            Next
        End If
    End If
    
    
    Select Case m_oProcesoNuevo.DefDestino
        Case EnProceso
            If m_oProcesoSeleccionado.DefDestino = EnGrupo Then
                m_oProcesoNuevo.DefDestino = EnGrupo
                For Each oGrupo In m_oProcesoNuevo.Grupos
                   oGrupo.DefDestino = True
                   oGrupo.DestCod = m_oProcesoNuevo.DestCod
                Next
            ElseIf m_oProcesoSeleccionado.DefDestino = EnItem Then
                m_oProcesoNuevo.DefDestino = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefDestino = False
                Next
            End If
            For Each oGrupo In m_oProcesoNuevo.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        oItem.DestCod = m_oProcesoNuevo.DestCod
                    Next
                End If
            Next
        Case EnGrupo
            If m_oProcesoSeleccionado.DefDestino = EnItem Then
                m_oProcesoNuevo.DefDestino = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefDestino = False
                Next
            End If
            For Each oGrupo In m_oProcesoNuevo.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        oItem.DestCod = oGrupo.DestCod
                    Next
                End If
            Next
        Case NoDefinido
            If m_oProcesoSeleccionado.DefDestino = EnProceso Then
                m_oProcesoNuevo.DefDestino = EnProceso
                m_oProcesoNuevo.DestCod = m_oProcesoSeleccionado.DestCod
            ElseIf m_oProcesoSeleccionado.DefDestino = EnGrupo Then
                m_oProcesoNuevo.DefDestino = EnGrupo
            ElseIf m_oProcesoSeleccionado.DefDestino = EnItem Then
                m_oProcesoNuevo.DefDestino = EnItem
            End If
    End Select
    
    Select Case m_oProcesoNuevo.DefFormaPago
        Case EnProceso
            If m_oProcesoSeleccionado.DefFormaPago = EnGrupo Then
                m_oProcesoNuevo.DefFormaPago = EnGrupo
                For Each oGrupo In m_oProcesoNuevo.Grupos
                   oGrupo.DefFormaPago = True
                   oGrupo.PagCod = m_oProcesoNuevo.PagCod
                Next
            ElseIf m_oProcesoSeleccionado.DefFormaPago = EnItem Then
                m_oProcesoNuevo.DefFormaPago = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefFormaPago = False
                Next
            End If
            For Each oGrupo In m_oProcesoNuevo.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        oItem.PagCod = m_oProcesoNuevo.PagCod
                    Next
                End If
            Next
        Case EnGrupo
            If m_oProcesoSeleccionado.DefFormaPago = EnItem Then
                m_oProcesoNuevo.DefFormaPago = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefFormaPago = False
                Next
            End If
            For Each oGrupo In m_oProcesoNuevo.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        oItem.PagCod = oGrupo.PagCod
                    Next
                End If
            Next
        Case NoDefinido
            If m_oProcesoSeleccionado.DefFormaPago = EnProceso Then
                m_oProcesoNuevo.DefFormaPago = EnProceso
                m_oProcesoNuevo.PagCod = m_oProcesoSeleccionado.PagCod
            ElseIf m_oProcesoSeleccionado.DefFormaPago = EnGrupo Then
                m_oProcesoNuevo.DefFormaPago = EnGrupo
            ElseIf m_oProcesoSeleccionado.DefFormaPago = EnItem Then
                m_oProcesoNuevo.DefFormaPago = EnItem
            End If
    End Select
    
    Select Case m_oProcesoNuevo.DefFechasSum
        Case EnProceso
            If m_oProcesoSeleccionado.DefFechasSum = EnGrupo Then
                m_oProcesoNuevo.DefFechasSum = EnGrupo
                For Each oGrupo In m_oProcesoNuevo.Grupos
                   oGrupo.DefFechasSum = True
                   oGrupo.FechaInicioSuministro = m_oProcesoNuevo.FechaInicioSuministro
                   oGrupo.FechaFinSuministro = m_oProcesoNuevo.FechaFinSuministro
                Next
            ElseIf m_oProcesoSeleccionado.DefFechasSum = EnItem Then
                m_oProcesoNuevo.DefFechasSum = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefFechasSum = False
                Next
            End If
            For Each oGrupo In m_oProcesoNuevo.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If Not IsNull(m_oProcesoNuevo.FechaInicioSuministro) Then
                            oItem.FechaInicioSuministro = m_oProcesoNuevo.FechaInicioSuministro
                        End If
                        If Not IsNull(m_oProcesoNuevo.FechaFinSuministro) Then
                            oItem.FechaFinSuministro = m_oProcesoNuevo.FechaFinSuministro
                        End If
                    Next
                End If
            Next
        Case EnGrupo
            If m_oProcesoSeleccionado.DefFechasSum = EnItem Then
                m_oProcesoNuevo.DefFechasSum = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefFechasSum = False
                Next
            End If
            For Each oGrupo In m_oProcesoNuevo.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If Not IsNull(oGrupo.FechaInicioSuministro) Then
                            oItem.FechaInicioSuministro = oGrupo.FechaInicioSuministro
                        End If
                        If Not IsNull(oGrupo.FechaFinSuministro) Then
                            oItem.FechaFinSuministro = oGrupo.FechaFinSuministro
                        End If
                    Next
                End If
            Next
        Case NoDefinido
            If m_oProcesoSeleccionado.DefFechasSum = EnProceso Then
                m_oProcesoNuevo.DefFechasSum = EnProceso
                m_oProcesoNuevo.FechaInicioSuministro = m_oProcesoSeleccionado.FechaInicioSuministro
                m_oProcesoNuevo.FechaFinSuministro = m_oProcesoSeleccionado.FechaFinSuministro
            ElseIf m_oProcesoSeleccionado.DefFechasSum = EnGrupo Then
                m_oProcesoNuevo.DefFechasSum = EnGrupo
            ElseIf m_oProcesoSeleccionado.DefFechasSum = EnItem Then
                m_oProcesoNuevo.DefFechasSum = EnItem
            End If
    End Select
    
    Select Case m_oProcesoNuevo.DefPresAnualTipo1
        Case EnProceso
            If m_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo Then
                m_oProcesoNuevo.DefPresAnualTipo1 = EnGrupo
                For Each oGrupo In m_oProcesoNuevo.Grupos
                   oGrupo.DefPresAnualTipo1 = True
                Next
            ElseIf m_oProcesoSeleccionado.DefPresAnualTipo1 = EnItem Then
                m_oProcesoNuevo.DefPresAnualTipo1 = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefPresAnualTipo1 = False
                Next
            End If
        Case EnGrupo
            If m_oProcesoSeleccionado.DefPresAnualTipo1 = EnItem Then
                m_oProcesoNuevo.DefPresAnualTipo1 = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefPresAnualTipo1 = False
                Next
            End If
        Case NoDefinido
            If m_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso Then
                m_oProcesoNuevo.DefPresAnualTipo1 = EnProceso
            ElseIf m_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo Then
                m_oProcesoNuevo.DefPresAnualTipo1 = EnGrupo
            ElseIf m_oProcesoSeleccionado.DefPresAnualTipo1 = EnItem Then
                m_oProcesoNuevo.DefPresAnualTipo1 = EnItem
            End If
    End Select
    
    Select Case m_oProcesoNuevo.DefPresAnualTipo2
        Case EnProceso
            If m_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo Then
                m_oProcesoNuevo.DefPresAnualTipo2 = EnGrupo
                For Each oGrupo In m_oProcesoNuevo.Grupos
                   oGrupo.DefPresAnualTipo2 = True
                Next
            ElseIf m_oProcesoSeleccionado.DefPresAnualTipo2 = EnItem Then
                m_oProcesoNuevo.DefPresAnualTipo2 = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefPresAnualTipo2 = False
                Next
            End If
        Case EnGrupo
            If m_oProcesoSeleccionado.DefPresAnualTipo2 = EnItem Then
                m_oProcesoNuevo.DefPresAnualTipo2 = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefPresAnualTipo2 = False
                Next
            End If
        Case NoDefinido
            If m_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso Then
                m_oProcesoNuevo.DefPresAnualTipo2 = EnProceso
            ElseIf m_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo Then
                m_oProcesoNuevo.DefPresAnualTipo2 = EnGrupo
            ElseIf m_oProcesoSeleccionado.DefPresAnualTipo2 = EnItem Then
                m_oProcesoNuevo.DefPresAnualTipo2 = EnItem
            End If
    End Select
    
    Select Case m_oProcesoNuevo.DefPresTipo1
        Case EnProceso
            If m_oProcesoSeleccionado.DefPresTipo1 = EnGrupo Then
                m_oProcesoNuevo.DefPresTipo1 = EnGrupo
                For Each oGrupo In m_oProcesoNuevo.Grupos
                   oGrupo.DefPresTipo1 = True
                Next
            ElseIf m_oProcesoSeleccionado.DefPresTipo1 = EnItem Then
                m_oProcesoNuevo.DefPresTipo1 = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefPresTipo1 = False
                Next
            End If
        Case EnGrupo
            If m_oProcesoSeleccionado.DefPresTipo1 = EnItem Then
                m_oProcesoNuevo.DefPresTipo1 = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefPresTipo1 = False
                Next
            End If
        Case NoDefinido
            If m_oProcesoSeleccionado.DefPresTipo1 = EnProceso Then
                m_oProcesoNuevo.DefPresTipo1 = EnProceso
            ElseIf m_oProcesoSeleccionado.DefPresTipo1 = EnGrupo Then
                m_oProcesoNuevo.DefPresTipo1 = EnGrupo
            ElseIf m_oProcesoSeleccionado.DefPresTipo1 = EnItem Then
                m_oProcesoNuevo.DefPresTipo1 = EnItem
            End If
    End Select
    
    Select Case m_oProcesoNuevo.DefPresTipo2
        Case EnProceso
            If m_oProcesoSeleccionado.DefPresTipo2 = EnGrupo Then
                m_oProcesoNuevo.DefPresTipo2 = EnGrupo
                For Each oGrupo In m_oProcesoNuevo.Grupos
                   oGrupo.DefPresTipo2 = True
                Next
            ElseIf m_oProcesoSeleccionado.DefPresTipo2 = EnItem Then
                m_oProcesoNuevo.DefPresTipo2 = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefPresTipo2 = False
                Next
            End If
        Case EnGrupo
            If m_oProcesoSeleccionado.DefPresTipo2 = EnItem Then
                m_oProcesoNuevo.DefPresTipo2 = EnItem
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    oGrupo.DefPresTipo2 = False
                Next
            End If
        Case NoDefinido
            If m_oProcesoSeleccionado.DefPresTipo2 = EnProceso Then
                m_oProcesoNuevo.DefPresTipo2 = EnProceso
            ElseIf m_oProcesoSeleccionado.DefPresTipo2 = EnGrupo Then
                m_oProcesoNuevo.DefPresTipo2 = EnGrupo
            ElseIf m_oProcesoSeleccionado.DefPresTipo2 = EnItem Then
                m_oProcesoNuevo.DefPresTipo2 = EnItem
            End If
    End Select
    
    ComprobacionEnviarAProceso
    
    Screen.MousePointer = vbNormal
    
    If m_oProcesoSeleccionado Is Nothing Then
        Unload Me
    Else
        'Se comprueba si falta alg�n dato necesario y se muestra la pantalla que los pide:
        Set frmSOLAbrirFaltan.g_oSolicitudSeleccionada = m_oSolicitudSeleccionada
        Set frmSOLAbrirFaltan.g_oProceso = m_oProcesoNuevo
        Set frmSOLAbrirFaltan.g_oProcesoAEnviar = m_oProcesoSeleccionado
        frmSOLAbrirFaltan.g_bModificarEstructuraProceso = m_bModificarEstructuraProceso
        
        If chkAdjuntos.Value = 1 Then
            frmSOLAbrirFaltan.g_bTraspasarAdjuntos = True
        Else
            frmSOLAbrirFaltan.g_bTraspasarAdjuntos = False
        End If
        frmSOLAbrirFaltan.g_bTraspasarAtributos = True
        Unload Me
        frmSOLAbrirFaltan.Show vbModal
    End If
    
    Set Item = Nothing
End Sub

Private Sub cmdBuscar_Click()
    frmPROCEBuscar.bRDest = m_bRDest
    frmPROCEBuscar.m_bProveAsigComp = False
    frmPROCEBuscar.m_bProveAsigEqp = False
    frmPROCEBuscar.bRAsig = m_bRAsig
    frmPROCEBuscar.bREqpAsig = m_bREqpAsig
    frmPROCEBuscar.bRCompResponsable = m_bRCompResp
    frmPROCEBuscar.bRMat = m_bRMat
    frmPROCEBuscar.bRUsuAper = m_bRUsuAper
    frmPROCEBuscar.bRUsuDep = m_bRUsuDep
    frmPROCEBuscar.bRUsuUON = m_bRUsuUON
    frmPROCEBuscar.sOrigen = "frmSOLEnviarAProc"
    frmPROCEBuscar.sdbcAnyo = sdbcAnyo
    frmPROCEBuscar.sdbcGMN1Proce_Cod = sdbcGMN1_4Cod
    frmPROCEBuscar.sdbcGMN1Proce_Cod_Validate False
    frmPROCEBuscar.Show vbModal
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Dim i As Long

    Me.Height = 8445
    Me.Width = 10695
    
    b_Bloqueo = False
    
    frmADJCargar.Show
    frmADJCargar.Refresh
    
    ConfigurarSeguridad
    CargarRecursos
    PonerFieldSeparator Me
    CargarAnyos
    
    Set m_oProcesos = oFSGSRaiz.generar_CProcesos
    If basPublic.gParametrosInstalacion.giLockTimeOut <= 0 Then
        basPublic.gParametrosInstalacion.giLockTimeOut = basParametros.gParametrosGenerales.giLockTimeOut
    End If
    m_oProcesos.EstablecerTiempoDeBloqueo basPublic.gParametrosInstalacion.giLockTimeOut
        
    'Genera los datos para el proceso
    CargarDatosInstancia
    
    If Not m_bPermProcMultimaterial Then
        sdbgLineas.Columns("FAM_MAT").Visible = False
    End If
    
    Unload frmADJCargar
    
    Me.caption = m_sIdiCaption & " " & m_oSolicitudSeleccionada.Id & " " & m_oSolicitudSeleccionada.DescrBreve

    'Con permisos, se visualiza la columna 'Grupo destino proceso'
    If m_bModifProcPlantilla Or m_bModifProcDesdeSolic Then
        sdbgLineas.Columns("GR_DESTINO_PROC").Visible = True
    Else
        sdbgLineas.Columns("GR_DESTINO_PROC").Visible = False
    End If
    
    'Mantener el valor de las pesta�as del proceso
    '(Asi m_oProcesoPlant ya definido por si el usuario da a continuar sin haber elegido proceso)
    Set m_oProcesoPlant = Nothing
    Set m_oProcesoPlant = oFSGSRaiz.Generar_CProceso
    Set m_oProcesoPlant.Grupos = oFSGSRaiz.Generar_CGrupos
        
    If Not m_oProcesoNuevo.Grupos Is Nothing Then
        For i = 1 To m_oProcesoNuevo.Grupos.Count
            m_oProcesoPlant.Grupos.Add m_oProcesoNuevo, m_oProcesoNuevo.Grupos.Item(i).Codigo, m_oProcesoNuevo.Grupos.Item(i).Den
        Next i
    End If
End Sub

Private Sub Form_Resize()
Dim i As Integer

    If Me.Width < 1500 Then Exit Sub
    If Me.Height < 3000 Then Exit Sub
    
    fraCabecera.Width = Me.Width - 330
    ssTabGrupos.Width = fraCabecera.Width
    
    ssTabGrupos.Height = Me.Height - (fraCabecera.Height + picNavigate.Height + 900)
    sdbgLineas.Height = ssTabGrupos.Height - 720
    sdbgLineas.Width = ssTabGrupos.Width - 355
    sdbgLineas.Top = ssTabGrupos.Top + 625
    
    picNavigate.Top = ssTabGrupos.Top + ssTabGrupos.Height + 120
    picNavigate.Left = Me.Width / 3
    
    sdbgLineas.Columns("COD_ART").Width = Me.sdbgLineas.Width * 0.1
    sdbgLineas.Columns("DESCR_ART").Width = Me.sdbgLineas.Width * 0.25
    sdbgLineas.Columns("FEC_INI").Width = Me.sdbgLineas.Width * 0.1
    sdbgLineas.Columns("FEC_FIN").Width = Me.sdbgLineas.Width * 0.1
    sdbgLineas.Columns("CANT").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("PRECUNI").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("IMPORTE").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("UNIDAD").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("DEST").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("PAGO").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("PROV").Width = Me.sdbgLineas.Width * 0.12
    
    For i = 22 To sdbgLineas.Columns.Count - 1
        sdbgLineas.Columns.Item(i).Width = Me.sdbgLineas.Width * 0.12
    Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oSolicitudSeleccionada = Nothing
    Set m_oProcesoSeleccionado = Nothing
    Set m_oGMN1Seleccionado = Nothing
    Set m_oGruposGMN1 = Nothing
    Set m_oProcesos = Nothing
    Set m_oProcesoNuevo = Nothing
    Set oPlantillaSeleccionada = Nothing
    
    frmSELGrupo.txtLabelCod = ""
    frmSELGrupo.txtLabelDen = ""
End Sub

Private Sub sdbcAnyo_Click()
    sdbcProceCod = ""
End Sub


Private Sub sdbcGMN1_4Cod_Change()
    If Not m_bRespetarCombo Then
        m_bCargarComboDesde = True
        Set m_oGMN1Seleccionado = Nothing
        sdbcProceCod = ""
    End If
End Sub

Private Sub sdbcGMN1_4Cod_Click()
    If Not sdbcGMN1_4Cod.DroppedDown Then sdbcGMN1_4Cod = ""
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    m_bRespetarCombo = False
    
    GMN1Seleccionado
    m_bCargarComboDesde = False
     
    sdbcProceCod = ""
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
          
    sdbcGMN1_4Cod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set m_oGruposGMN1 = Nothing
    Set m_oGruposGMN1 = oFSGSRaiz.Generar_CGruposMatNivel1

    If m_bCargarComboDesde Then
    
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
      
            Set m_oGruposGMN1 = Nothing
            Set m_oGruposGMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario)
        Else
            Set m_oGruposGMN1 = Nothing
            Set m_oGruposGMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        
            m_oGruposGMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            
            Set m_oGruposGMN1 = Nothing
            Set m_oGruposGMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario)
        Else
            Set m_oGruposGMN1 = Nothing
            Set m_oGruposGMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         
            m_oGruposGMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = m_oGruposGMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oGruposGMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Cod.Rows - 1
            bm = sdbcGMN1_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim m_oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        Exit Sub
    End If
      
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposGMN1 = Nothing
        Set m_oGruposGMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If m_oGruposGMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            oMensajes.NoValido m_sIdioma(2)
        Else
            Set m_oGMN1Seleccionado = Nothing
            Set m_oGMN1Seleccionado = m_oGruposGMN1.Item(scod1)
            
            m_bCargarComboDesde = False
        End If
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
       
        oGMN1.Cod = sdbcGMN1_4Cod
        Set m_oIBaseDatos = oGMN1
        
        bExiste = m_oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            oMensajes.NoValido m_sIdioma(2)
        Else
            Set m_oGMN1Seleccionado = Nothing
            Set m_oGMN1Seleccionado = oGMN1
           
            m_bCargarComboDesde = False
            
        End If
    
    End If
        
    Set oGMN1 = Nothing
    Set m_oIBaseDatos = Nothing
    Set m_oGruposGMN1 = Nothing
    Set oIMAsig = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceCod_Change()
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        sdbcProceDen.Text = ""
        
        Set m_oProcesoSeleccionado = Nothing
        
        'Limpia la combo de denominaci�n
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        
        m_bRespetarCombo = False
        
        If sdbcProceCod <> "" Then
            m_bCargarComboDesde = True
        End If
    End If
End Sub

Private Sub sdbcProceCod_Click()
    If Not sdbcProceCod.DroppedDown Then
        sdbcProceDen = ""
        sdbcProceCod = ""
    End If
End Sub

Private Sub sdbcProceCod_CloseUp()
    Dim i As Long

    If sdbcProceCod.Value = "..." Or sdbcProceCod = "" Then
        sdbcProceCod.Text = ""
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    m_bRespetarCombo = True
    sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
    sdbcProceCod.Text = sdbcProceCod.Columns(0).Text
    m_bRespetarCombo = False
    
    ProcesoSeleccionado
    m_bCargarComboDesde = False

    'Visualizaci�n de la columna 'Grupo destino proceso'
    If Not m_oProcesoSeleccionado Is Nothing Then
        With sdbgLineas
            Dim bEditButton As Boolean
            bEditButton = False
            
            'Proceso creado desde solicitud con plantilla
            If Not IsNull(m_oProcesoSeleccionado.Plantilla) And (m_oProcesoSeleccionado.DesdeSolicitud = 1) Then
                'En ese caso, tendr�an que estar activos los 2 tipos de permisos (o bien tener mas de un grupo)
                bEditButton = (m_oProcesoSeleccionado.Grupos.Count > 1 And (m_bModifProcPlantilla And m_bModifProcDesdeSolic))
            Else
                bEditButton = (m_oProcesoSeleccionado.Grupos.Count > 1 And (IsNull(m_oProcesoSeleccionado.Plantilla) Or (Not IsNull(m_oProcesoSeleccionado.Plantilla) And m_bModifProcPlantilla) Or (m_oProcesoSeleccionado.DesdeSolicitud = 1 And m_bModifProcDesdeSolic)))
            End If
            
            If bEditButton Then
                .Columns("GR_DESTINO_PROC").Visible = True
                .Columns("GR_DESTINO_PROC").Style = ssStyleEditButton
                .Columns("GR_DESTINO_PROC").Locked = False
            Else
                'Se mantiene visible la columna pero no se deja modificar
                .Columns("GR_DESTINO_PROC").Visible = True
                .Columns("GR_DESTINO_PROC").Style = ssStyleEdit
                .Columns("GR_DESTINO_PROC").Locked = True
            End If
        End With
    End If
    
    EstablecerGrupoItemsProcesoSolicitud
    SSTabGrupos_Click
        
    'Mantener el valor de las pesta�as del proceso
    Set m_oProcesoPlant = Nothing
    Set m_oProcesoPlant = oFSGSRaiz.Generar_CProceso
    Set m_oProcesoPlant.Grupos = oFSGSRaiz.Generar_CGrupos
        
    For i = 1 To m_oProcesoNuevo.Grupos.Count
        If m_oProcesoPlant.Grupos.Item(m_oProcesoNuevo.Grupos.Item(i).Codigo) Is Nothing Then m_oProcesoPlant.Grupos.Add m_oProcesoNuevo, m_oProcesoNuevo.Grupos.Item(i).Codigo, m_oProcesoNuevo.Grupos.Item(i).Den
    Next i
    
    frmSELGrupo.txtLabelCod = ""
    frmSELGrupo.txtLabelDen = ""
    
    Screen.MousePointer = vbNormal
End Sub
'Al cargar el proceso de compra pasa los �tems a los grupos del proceso.
Private Sub EstablecerGrupoItemsProcesoSolicitud()
Dim oItem As CItem
    
    If Not m_oProcesoNuevo Is Nothing Then
        If Not m_oProcesoNuevo.Grupos Is Nothing Then
            If m_oProcesoNuevo.Grupos.Count > 0 Then
                Dim oGrupo As CGrupo
                Dim i As Integer
                Dim sGrupoKey As String
                
                For i = 1 To m_oProcesoNuevo.Grupos.Count
                    Set oGrupo = m_oProcesoNuevo.Grupos.Item(i)
                    sGrupoKey = Mid(ssTabGrupos.Tabs(i).key, 2) 'Las keys originales est�n en la col. de tabs
                    If Not oGrupo.Items Is Nothing Then
                        If oGrupo.Items.Count > 0 Then
                            For Each oItem In oGrupo.Items
                                EstablecerGrupoItemSolicitud sGrupoKey, oItem
                            Next
                        End If
                    End If
                Next
                Set oItem = Nothing
                Set oGrupo = Nothing
            End If
        End If
    End If
End Sub

Private Sub EstablecerGrupoItemSolicitud(ByVal sGrupoKey As String, ByRef oItem As CItem)
    Dim oGrupo As CGrupo
    Dim bEncontrado As Boolean
        
    oItem.grupoCod = ""

    If Not m_oProcesoSeleccionado Is Nothing Then 'Proceso seleccionado pero sin grupos: siguen por defecto los de la solicitud
        If m_oProcesoSeleccionado.Grupos.Count = 0 Then 'Proceso seleccionado pero sin grupos: siguen por defecto los de la solicitud
            oItem.GrupoDen = m_oProcesoNuevo.Grupos.Item(sGrupoKey).Den
            oItem.grupoCod = m_oProcesoNuevo.Grupos.Item(sGrupoKey).Codigo
            oItem.GrupoID = m_oProcesoNuevo.Grupos.Item(sGrupoKey).Id
        Else
            If m_oProcesoNuevo.Grupos.Count > 0 Then
                bEncontrado = False
                For Each oGrupo In m_oProcesoSeleccionado.Grupos 'Se busca si algun grupo del proceso tiene el mismo cod o den que el de la solicitud
                    If m_oProcesoNuevo.Grupos.Item(sGrupoKey).Codigo = oGrupo.Codigo Or UCase(m_oProcesoNuevo.Grupos.Item(sGrupoKey).Den) = UCase(oGrupo.Den) Then
                        oItem.GrupoDen = oGrupo.Den
                        oItem.grupoCod = oGrupo.Codigo
                        oItem.GrupoID = oGrupo.Id
                        bEncontrado = True
                        Exit For
                    End If
                Next
                If Not bEncontrado Then 'Sino, se asigna el de la misma posici�n
                    For Each oGrupo In m_oProcesoSeleccionado.Grupos
                        If m_oProcesoNuevo.Grupos.Item(sGrupoKey).Id = oGrupo.Id Then
                                oItem.GrupoDen = oGrupo.Den
                                oItem.grupoCod = oGrupo.Codigo
                                oItem.GrupoID = oGrupo.Id
                                Exit For
                        End If
                    Next
                End If
                'Caso: Solicitud tiene mas grupos que el proceso: se asigna el �ltimo grupo del proceso
                If oItem.grupoCod = "" Then
                    oItem.GrupoDen = m_oProcesoSeleccionado.Grupos.Item(m_oProcesoSeleccionado.Grupos.Count).Den
                    oItem.grupoCod = m_oProcesoSeleccionado.Grupos.Item(m_oProcesoSeleccionado.Grupos.Count).Codigo
                    oItem.GrupoID = m_oProcesoSeleccionado.Grupos.Item(m_oProcesoSeleccionado.Grupos.Count).Id
                End If
            End If
        End If
    Else
        'Si no se ha elegido aun proceso, sale la que estaba de antes por defecto: la de la solicitud
        oItem.GrupoDen = m_oProcesoNuevo.Grupos.Item(sGrupoKey).Den
        oItem.grupoCod = m_oProcesoNuevo.Grupos.Item(sGrupoKey).Codigo
        oItem.GrupoID = m_oProcesoNuevo.Grupos.Item(sGrupoKey).Id
    End If
End Sub

Private Sub sdbcProceCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    sdbcProceCod.RemoveAll
    
    If m_oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Set m_oProcesoSeleccionado = Nothing
    
    Screen.MousePointer = vbHourglass
    
    If m_bCargarComboDesde Then
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.sinitems, TipoEstadoProceso.ParcialmenteCerrado, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo, sdbcGMN1_4Cod, , , , val(sdbcProceCod), , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
    Else
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.sinitems, TipoEstadoProceso.ParcialmenteCerrado, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo, sdbcGMN1_4Cod, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
    End If
    
    Codigos = m_oProcesos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oProcesos.EOF Then
        sdbcProceCod.AddItem "..."
    End If

    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Text)
    sdbcProceCod.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceCod_InitColumnProps()
    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProceCod_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProceCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceCod.Rows - 1
            bm = sdbcProceCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcProceCod_Validate(Cancel As Boolean)
    Dim i As Long

    If sdbcProceCod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = "" Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        Set m_oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcProceCod.Value > giMaxProceCod Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sIdioma(1)
        Set m_oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If Not IsNumeric(sdbcProceCod) Then
        Screen.MousePointer = vbNormal
        sdbcProceCod.Text = ""
        oMensajes.NoValido m_sIdioma(1)
        Set m_oProcesoSeleccionado = Nothing
        If m_oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Exit Sub
    End If
    
    If sdbcProceDen.Columns(1).Value = sdbcProceCod.Text Then Exit Sub
    
    If sdbcProceCod.Text = sdbcProceCod.Columns(0).Text Then
        m_bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
        m_bRespetarCombo = False
        If m_oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Exit Sub
    End If
          
    Screen.MousePointer = vbHourglass
           
    m_oProcesos.CargarTodosLosProcesosDesde 1, sinitems, ParcialmenteCerrado, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo, sdbcGMN1_4Cod, , , , val(sdbcProceCod), , True, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
        
    If m_oProcesos.Count = 0 Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sIdioma(1)

        Set m_oProcesoSeleccionado = Nothing
    Else
        m_bRespetarCombo = True
        sdbcProceDen.Text = m_oProcesos.Item(1).Den
        
        sdbcProceCod.Columns(0).Text = sdbcProceCod.Text
        sdbcProceCod.Columns(1).Text = sdbcProceDen.Text
        
        sdbcProceDen.Columns(1).Value = sdbcProceCod.Columns(0).Value
        sdbcProceDen.Columns(0).Value = sdbcProceCod.Columns(1).Value
       
        m_bRespetarCombo = False
        Set m_oProcesoSeleccionado = Nothing
        Set m_oProcesoSeleccionado = m_oProcesos.Item(1)
     
        m_bCargarComboDesde = False
        ProcesoSeleccionado
        
        'Visualizaci�n de la columna 'Grupo destino proceso'
        If Not m_oProcesoSeleccionado Is Nothing Then
            With sdbgLineas
                Dim bEditButton As Boolean
                'Editamos grupo si: permisos de modificar estructura plant/solic o el profeso tiene mas de un grupo o si es deplantilla tenemos permiso o si es de solicitud y tenemos permiso
                bEditButton = (m_bModifProcPlantilla And m_bModifProcDesdeSolic) Or (m_oProcesoSeleccionado.Grupos.Count > 1) Or (IsNull(m_oProcesoSeleccionado.Plantilla) And m_bModifProcPlantilla) Or (m_oProcesoSeleccionado.DesdeSolicitud = 1 And m_bModifProcDesdeSolic)
                
                If bEditButton Then
                    .Columns("GR_DESTINO_PROC").Visible = True
                    .Columns("GR_DESTINO_PROC").Style = ssStyleEditButton
                    .Columns("GR_DESTINO_PROC").Locked = False
                Else
                    'Se mantiene visible la columna pero no se deja modificar
                    .Columns("GR_DESTINO_PROC").Visible = True
                    .Columns("GR_DESTINO_PROC").Style = ssStyleEdit
                    .Columns("GR_DESTINO_PROC").Locked = True
                End If
            End With
        End If
        
        EstablecerGrupoItemsProcesoSolicitud
        SSTabGrupos_Click
                    
        'Mantener el valor de las pesta�as del proceso
        Set m_oProcesoPlant = Nothing
        Set m_oProcesoPlant = oFSGSRaiz.Generar_CProceso
        Set m_oProcesoPlant.Grupos = oFSGSRaiz.Generar_CGrupos
            
        For i = 1 To m_oProcesoNuevo.Grupos.Count
            If m_oProcesoPlant.Grupos.Item(m_oProcesoNuevo.Grupos.Item(i).Codigo) Is Nothing Then m_oProcesoPlant.Grupos.Add m_oProcesoNuevo, m_oProcesoNuevo.Grupos.Item(i).Codigo, m_oProcesoNuevo.Grupos.Item(i).Den
        Next i
        
        frmSELGrupo.txtLabelCod = ""
        frmSELGrupo.txtLabelDen = ""
    End If
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceDen_Change()
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        sdbcProceCod.Text = ""
    
        Set m_oProcesoSeleccionado = Nothing
        
         'Limpia la combo de denominaci�n
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        
        m_bRespetarCombo = False
        m_bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcProceDen_Click()
    If Not sdbcProceDen.DroppedDown Then
        sdbcProceDen = ""
        sdbcProceCod = ""
    End If
End Sub

Private Sub sdbcProceDen_CloseUp()

    Dim i As Long

    If sdbcProceDen.Value = "....." Or sdbcProceDen = "" Then
        sdbcProceDen.Text = ""
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    m_bRespetarCombo = True
    sdbcProceCod.Text = sdbcProceDen.Columns(1).Text
    sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
    m_bRespetarCombo = False

    ProcesoSeleccionado
    m_bCargarComboDesde = False
    
    
    'Visualizaci�n de la columna 'Grupo destino proceso'
    If Not m_oProcesoSeleccionado Is Nothing Then
        With sdbgLineas
            Dim bEditButton As Boolean
            bEditButton = False
            
            'Proceso creado desde solicitud con plantilla
            If Not IsNull(m_oProcesoSeleccionado.Plantilla) And (m_oProcesoSeleccionado.DesdeSolicitud = 1) Then
                'En ese caso, tendr�an que estar activos los 2 tipos de permisos (o bien tener mas de un grupo)
                bEditButton = (m_oProcesoSeleccionado.Grupos.Count > 1 And (m_bModifProcPlantilla And m_bModifProcDesdeSolic))
            Else
                bEditButton = (m_oProcesoSeleccionado.Grupos.Count > 1 And (IsNull(m_oProcesoSeleccionado.Plantilla) Or (Not IsNull(m_oProcesoSeleccionado.Plantilla) And m_bModifProcPlantilla) Or (m_oProcesoSeleccionado.DesdeSolicitud = 1 And m_bModifProcDesdeSolic)))
            End If
            
            If bEditButton Then
                .Columns("GR_DESTINO_PROC").Visible = True
                .Columns("GR_DESTINO_PROC").Style = ssStyleEditButton
                .Columns("GR_DESTINO_PROC").Locked = False
            Else
                'Se mantiene visible la columna pero no se deja modificar
                .Columns("GR_DESTINO_PROC").Visible = True
                .Columns("GR_DESTINO_PROC").Style = ssStyleEdit
                .Columns("GR_DESTINO_PROC").Locked = True
            End If
        End With
    End If
    
    EstablecerGrupoItemsProcesoSolicitud
    SSTabGrupos_Click

    'Mantener el valor de las pesta�as del proceso
    Set m_oProcesoPlant = Nothing
    Set m_oProcesoPlant = oFSGSRaiz.Generar_CProceso
    Set m_oProcesoPlant.Grupos = oFSGSRaiz.Generar_CGrupos
        
    For i = 1 To m_oProcesoNuevo.Grupos.Count
        If m_oProcesoPlant.Grupos.Item(m_oProcesoNuevo.Grupos.Item(i).Codigo) Is Nothing Then m_oProcesoPlant.Grupos.Add m_oProcesoNuevo, m_oProcesoNuevo.Grupos.Item(i).Codigo, m_oProcesoNuevo.Grupos.Item(i).Den
    Next i
    
    frmSELGrupo.txtLabelCod = ""
    frmSELGrupo.txtLabelDen = ""
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcProceDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
    sdbcProceDen.RemoveAll
    If m_oGMN1Seleccionado Is Nothing Then Exit Sub
    Set m_oProcesoSeleccionado = Nothing
    Screen.MousePointer = vbHourglass

    If m_bCargarComboDesde Then
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.sinitems, TipoEstadoProceso.ParcialmenteCerrado, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo, sdbcGMN1_4Cod, , , , , sdbcProceDen, False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
    Else
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.sinitems, TipoEstadoProceso.ParcialmenteCerrado, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo, sdbcGMN1_4Cod, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
    End If
    
    Codigos = m_oProcesos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not m_oProcesos.EOF Then sdbcProceDen.AddItem "....."
    
    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Text)
    sdbcProceDen.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceDen_InitColumnProps()
    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProceDen_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProceDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceDen.Rows - 1
            bm = sdbcProceDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgLineas_BtnClick()
 Dim sGrupo As String
 Dim oGrpProceso As CGrupo
 Dim k As Long
 Dim i As Long
     
    Select Case sdbgLineas.Columns(sdbgLineas.col).Name
        Case "PROV"
            MostrarDetalleProveedor (sdbgLineas.Columns("COD_PROVE").Value)
            
            'Bot�n para elegir el grupo destino:
        Case "GR_DESTINO_PROC"
            
                sGrupo = Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2)
                
                If Not m_oProcesoSeleccionado Is Nothing Then
                     
                    'Proceso creado desde solicitud con plantilla
                    If Not IsNull(m_oProcesoSeleccionado.Plantilla) And (m_oProcesoSeleccionado.DesdeSolicitud = 1) Then
                        'En ese caso, tendr�an que estar activos los 2 tipos de permisos
                        If (m_bModifProcPlantilla And m_bModifProcDesdeSolic) Then
                            frmSELGrupo.PermisoModificar = True
                        Else
                            frmSELGrupo.PermisoModificar = False
                        End If
                    Else
                        If (Not IsNull(m_oProcesoSeleccionado.Plantilla) And m_bModifProcPlantilla) Or (m_oProcesoSeleccionado.DesdeSolicitud = 1 And m_bModifProcDesdeSolic) _
                            Or (IsNull(m_oProcesoSeleccionado.Plantilla) And m_oProcesoSeleccionado.DesdeSolicitud = 0) Then
                            frmSELGrupo.PermisoModificar = True
                        Else
                            frmSELGrupo.PermisoModificar = False
                        End If
                    End If
            
                End If
                    
                frmSELGrupo.FormOrigen = "frmSOLEnviarAProc"
            
                'El desplegable sdbcPlantillas de frmSELGrupo tiene los grupos del proceso seleccionado
                If Not m_oProcesoSeleccionado Is Nothing Then
                    frmSELGrupo.sdbcGruposDestino.RemoveAll
                    'Proceso sin grupos: Se cargue el grupo de la solicitud
                    If m_oProcesoSeleccionado.Grupos.Count = 0 Then
                        frmSELGrupo.sdbcGruposDestino.AddItem "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Codigo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Den & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Codigo & " - " & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Den
                    Else
                        For Each oGrpProceso In m_oProcesoSeleccionado.Grupos
                        'Id Plantilla, Cod Grupo, Id Grupo, Den Grupo, Cod-Den
                            If m_oProcesoSeleccionado.Estado < ParcialmenteCerrado Or (m_oProcesoSeleccionado.Estado = ParcialmenteCerrado And NullToDbl0(oGrpProceso.Cerrado) <> 1) Then
                                frmSELGrupo.sdbcGruposDestino.AddItem oGrpProceso.IDPlantilla & Chr(m_lSeparador) & oGrpProceso.Codigo & Chr(m_lSeparador) & oGrpProceso.Id & Chr(m_lSeparador) & oGrpProceso.Den & Chr(m_lSeparador) & oGrpProceso.Codigo & " - " & oGrpProceso.Den
                            End If
                        Next
                    End If
                End If
                
                'Aun sin seleccionar proceso: Se cargue el grupo de la solicitud
                If m_oProcesoSeleccionado Is Nothing Then
                    frmSELGrupo.sdbcGruposDestino.AddItem "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Codigo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Den & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Codigo & " - " & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Den
                End If
                                
                'Grupo Nuevo
                If Not m_oProcesoSeleccionado Is Nothing Then
                    
                    'Proceso creado desde solicitud con plantilla
                    If Not IsNull(m_oProcesoSeleccionado.Plantilla) And (m_oProcesoSeleccionado.DesdeSolicitud = 1) Then
                        'En ese caso, tendr�an que estar activos los 2 tipos de permisos
                        If (m_bModifProcPlantilla And m_bModifProcDesdeSolic) Then
                            'En el desplegable que salgan los grupos nuevos creados
                            If m_oProcesoNuevo.Grupos.Count > 1 Then
                                For i = 2 To m_oProcesoNuevo.Grupos.Count
                                    frmSELGrupo.sdbcGruposDestino.AddItem "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(i).Codigo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(i).Den & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(i).Codigo & " - " & m_oProcesoNuevo.Grupos.Item(i).Den
                                Next
                            End If
                            
                            'Mas la opci�n de 'Grupo Nuevo'
                            frmSELGrupo.sdbcGruposDestino.AddItem "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Codigo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Den & Chr(m_lSeparador) & frmSELGrupo.txtGrupoNuevo
                        End If
                    Else
                        If (Not IsNull(m_oProcesoSeleccionado.Plantilla) And m_bModifProcPlantilla) Or (m_oProcesoSeleccionado.DesdeSolicitud = 1 And m_bModifProcDesdeSolic) _
                            Or (IsNull(m_oProcesoSeleccionado.Plantilla) And m_oProcesoSeleccionado.DesdeSolicitud = 0) Then
                            'En el desplegable que salgan los grupos nuevos creados
                            If m_oProcesoNuevo.Grupos.Count > 1 Then
                                For i = 2 To m_oProcesoNuevo.Grupos.Count
                                    frmSELGrupo.sdbcGruposDestino.AddItem "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(i).Codigo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(i).Den & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(i).Codigo & " - " & m_oProcesoNuevo.Grupos.Item(i).Den
                                Next
                            End If
                            
                            'Mas la opci�n de 'Grupo Nuevo'
                            frmSELGrupo.sdbcGruposDestino.AddItem "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Codigo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Den & Chr(m_lSeparador) & frmSELGrupo.txtGrupoNuevo
                        End If
                    End If
                
                End If
                
                'Si la solicitud tiene mas de un grupo, se muestra la tercera opci�n
                'El primer grupo de 'Datos Generales' se obvia, se tienen en cuenta unicamente los de desglose
                If ssTabGrupos.Tabs.Count > 1 Then
                    frmSELGrupo.VisibleOpt3 = True
                Else
                    frmSELGrupo.VisibleOpt3 = False
                End If
                
                'Pesta�a (grupo de la solicitud) se est� tratando
                frmSELGrupo.GrupoSeleccionado = ssTabGrupos.selectedItem
                'Linea de donde se abrir� 'frmSELGrupo'
                frmSELGrupo.Linea = sdbgLineas.Columns("LINEA").Value
                
                If Not m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)) Is Nothing Then
                    If frmSELGrupo.txtLabelCod = "" Then
                        frmSELGrupo.txtLabelCod = m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Codigo
                    End If
                    
                    If frmSELGrupo.txtLabelDen = "" Then
                        frmSELGrupo.txtLabelDen = m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Den
                    End If
                End If
                
                If Not m_oProcesoSeleccionado Is Nothing Then
                    'Caso que el proceso seleccionado no tenga grupos
                    If m_oProcesoSeleccionado.Grupos.Count = 0 Then
                        frmSELGrupo.ComboValue = m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Codigo & " - " & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Den
                        
                        'Mantener el valor si se ha cambiado antes
                        If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> frmSELGrupo.ComboValue Then
                            frmSELGrupo.ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                        End If
                    Else
                        If ssTabGrupos.Tabs.Count = 1 Then
                            'Si la solicitud solo tiene 1 grupo, por defecto el 1� grupo de la plantilla
                            frmSELGrupo.ComboValue = m_oProcesoSeleccionado.Grupos.Item(1).Codigo & " - " & m_oProcesoSeleccionado.Grupos.Item(1).Den
                            
                            'Mantener el valor si se ha cambiado antes
                            If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> frmSELGrupo.ComboValue Then
                                frmSELGrupo.ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                            End If
                        Else
                            For k = 1 To m_oProcesoSeleccionado.Grupos.Count
                                'Se busca si algun grupo del proceso tiene el mismo cod o den que el de la solicitud
                                If m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Codigo = m_oProcesoSeleccionado.Grupos.Item(k).Codigo Or m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Den = m_oProcesoSeleccionado.Grupos.Item(k).Den Then
                                    frmSELGrupo.ComboValue = m_oProcesoSeleccionado.Grupos.Item(k).Codigo & " - " & m_oProcesoSeleccionado.Grupos.Item(k).Den
                                    
                                    'Mantener el valor si se ha cambiado antes
                                    If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> frmSELGrupo.ComboValue Then
                                        frmSELGrupo.ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                                    End If
                                    Exit For
                                Else
                                'Sino, se asigna el de la misma posici�n
                                    If m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Id = m_oProcesoSeleccionado.Grupos.Item(k).Id Then
                                        frmSELGrupo.ComboValue = m_oProcesoSeleccionado.Grupos.Item(k).Codigo & " - " & m_oProcesoSeleccionado.Grupos.Item(k).Den
                                        
                                        'Mantener el valor si se ha cambiado antes
                                        If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> frmSELGrupo.ComboValue Then
                                            frmSELGrupo.ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                                        End If
                                        
                                        Exit For
                                    End If
                                End If
                            Next k
                            'Caso: Solicitud tiene mas grupos que el proceso: se asigna el �ltimo grupo del proceso
                            If frmSELGrupo.ComboValue = "" Then
                                frmSELGrupo.ComboValue = m_oProcesoSeleccionado.Grupos.Item(m_oProcesoSeleccionado.Grupos.Count).Codigo & " - " & m_oProcesoSeleccionado.Grupos.Item(m_oProcesoSeleccionado.Grupos.Count).Den
                                
                                'Mantener el valor si se ha cambiado antes
                                If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> frmSELGrupo.ComboValue Then
                                    frmSELGrupo.ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                                End If
                            End If
                        End If
                    End If
                Else
                    'Si no se ha elegido aun proceso
                    frmSELGrupo.ComboValue = m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Codigo & " - " & m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Den
                    
                    'Mantener el valor si se ha cambiado antes
                    If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> frmSELGrupo.ComboValue Then
                        frmSELGrupo.ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                    End If
                End If
                
                frmSELGrupo.optTodasLasLineasSolicitud(2).Visible = frmSELGrupo.VisibleOpt3
                frmSELGrupo.sdbcGruposDestino.Value = frmSELGrupo.ComboValue
                frmSELGrupo.optTrasladarUnaLinea(1).caption = Replace(frmSELGrupo.optTrasladarUnaLinea(1).caption, "xxx", sdbgLineas.Columns("NUM").Value)
                frmSELGrupo.optTodasLasLineasPesta�a(0).caption = Replace(frmSELGrupo.optTodasLasLineasPesta�a(0).caption, "xxx", frmSELGrupo.GrupoSeleccionado)
                
                'No permitir volver a cambiar la estructura, una vez trasladados los items
                If b_Bloqueo = False Then
                    frmSELGrupo.Show vbModal
                End If
    End Select
End Sub

Private Sub sdbgLineas_Change()
    Dim sGrupo As String
    Dim oGrupoProce As CGrupo
    Dim bCoincide As Boolean
    Dim vGrupo As Variant

    If sdbgLineas.Columns(sdbgLineas.col).Name <> "SEL" Then Exit Sub
    
    sGrupo = Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2)
    
    If GridCheckToBoolean(sdbgLineas.Columns("SEL").Value) = True Then
    
        If sdbgLineas.Columns("CANT").Value = "" Or sdbgLineas.Columns("DESCR_ART").Value = "" Then
            sdbgLineas.Columns("SEL").Value = 0
            Exit Sub
        End If

        'Verificar que el grupo no est� cerrado en el proceso destino
        If Not m_oProcesoSeleccionado Is Nothing Then
            vGrupo = Split(sdbgLineas.Columns("GR_DESTINO_PROC").Value, " - ")
            For Each oGrupoProce In m_oProcesoSeleccionado.Grupos
                'Se compara el grupo en el grid
                'Si coincide la denominaci�n o el c�digo, los items ir�n a ese grupo
                bCoincide = False
                If vGrupo(1) = oGrupoProce.Den Then
                    bCoincide = True
                Else
                    If vGrupo(0) = oGrupoProce.Codigo Then
                        bCoincide = True
                    End If
                End If
                If bCoincide Then
                    If SQLBinaryToBoolean(oGrupoProce.Cerrado) Then
                        sdbgLineas.Columns("SEL").Value = "0"
                        m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(CStr(sdbgLineas.Columns("LINEA").Value)).eliminado = True
                        
                        oMensajes.ImposibleEnviarArticuloProceso
                        Exit Sub
                    End If
                End If
            Next
        End If
        
        m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(CStr(sdbgLineas.Columns("LINEA").Value)).eliminado = False
        If Not oPlantillaSeleccionada Is Nothing Then
            If oPlantillaSeleccionada.AdmiteMaterial(m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(CStr(sdbgLineas.Columns("LINEA").Value)), oPlantillaSeleccionada.Id) = False Then
                sdbgLineas.Columns("SEL").Value = "0"
                oMensajes.ImposibleEnviarMaterialAProcesoRestriccionPorPlantilla " : " & oPlantillaSeleccionada.nombre
            End If
        End If
        
         If Not m_bPermProcMultimaterial Then
            If Not m_oProcesoSeleccionado.ExisteMaterialEnProceso(sdbgLineas.Columns("GMN1").Value, sdbgLineas.Columns("GMN2").Value, sdbgLineas.Columns("GMN3").Value, sdbgLineas.Columns("GMN4").Value) Then
                sdbgLineas.Columns("SEL").Value = "0"
            End If
        End If
    Else
        m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(CStr(sdbgLineas.Columns("LINEA").Value)).eliminado = True
    End If
End Sub

Private Sub sdbgLineas_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
Dim s As String
Select Case sdbgLineas.Columns("RIESGO").Value
    Case 1
        s = "RiesgoAlto"
    Case 2
        s = "RiesgoMedio"
    Case 3
        s = "RiesgoBajo"
End Select
For i = 0 To 22
    If Right(sdbgLineas.Columns(i).StyleSet, 8) = "amarillo" Then
        sdbgLineas.Columns(i).CellStyleSet s & "amarillo"
    Else
        sdbgLineas.Columns(i).CellStyleSet s
    End If
Next i
End Sub

Private Sub SSTabGrupos_Click()
    Dim sGrupo As String
    Dim oGrupo As CGrupo
    Dim sCadena As String
    Dim sproveedor As String
    Dim oItem As CItem
    Dim i As Integer
    Dim oAtributoEspecificacion As CAtributo
    Dim bExiste As Boolean
    Dim j As Integer
    Dim vbleCod As String
    Dim vbleDen As String
    Dim vbleId As String
    Dim vbleProcCod As String
    Dim vbleProcDen As String
    Dim iRiesgo As Integer
    Dim oGMN4 As CGrupoMatNivel4
    sdbgLineas.Visible = False
    
    sdbgLineas.RemoveAll
    
    'Eliminamos las columnas de los atributos de especificacion
    For i = sdbgLineas.Columns.Count - 1 To 23 Step -1
        sdbgLineas.Columns.Remove (i)
    Next
    
    If ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key = "" Then Exit Sub

    sGrupo = Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2)
    Set oGrupo = m_oProcesoNuevo.Grupos.Item(CStr(sGrupo))
    vbleCod = oGrupo.Codigo
    vbleDen = oGrupo.Den
    vbleId = oGrupo.Id
    If Not m_oProcesoSeleccionado Is Nothing Then
        vbleProcCod = m_oProcesoSeleccionado.Cod
        vbleProcDen = m_oProcesoSeleccionado.Den
    End If

    If Not oGrupo.Items Is Nothing Then
    
        For Each oItem In oGrupo.Items
            'Se insertan las columnas necesarias para los atributos de especificaci�n de los items de este grupo.
            If Not oItem.AtributosEspecificacion Is Nothing Then
                i = sdbgLineas.Columns.Count
                
                For Each oAtributoEspecificacion In oItem.AtributosEspecificacion
                    bExiste = False
                    For j = 23 To sdbgLineas.Columns.Count - 1
                        If sdbgLineas.Columns.Item(j).Name = oAtributoEspecificacion.Atrib Then
                            bExiste = True
                            Exit For
                        End If
                    Next
                    If Not bExiste Then
                        sdbgLineas.Columns.Add i
                        sdbgLineas.Columns(i).Name = oAtributoEspecificacion.Atrib
                        sdbgLineas.Columns(i).caption = oAtributoEspecificacion.Cod
                        sdbgLineas.Columns(i).DataType = 8
                        sdbgLineas.Columns(i).FieldLen = 256
                        sdbgLineas.Columns(i).Locked = True
                        sdbgLineas.Columns(i).Style = ssStyleEdit
                        sdbgLineas.Columns(i).StyleSet = "amarillo"
                        sdbgLineas.Columns(i).Width = 1814
                        
                        If oAtributoEspecificacion.pedido Then
                            sdbgLineas.Columns(i).HeadStyleSet = "apedido"
                        End If
                                                
                        i = i + 1
                    End If
                Next
            End If
        Next
        
        For Each oItem In oGrupo.Items
            iRiesgo = 0
            If IsNull(oItem.ProveAct) Then
                sproveedor = ""
            Else
                sproveedor = oItem.ProveAct & " - " & oItem.ProveActDen
            End If
            
            sCadena = IIf(oItem.eliminado = True, 0, 1)
            
            'N�mero de Linea
            sCadena = sCadena & Chr(m_lSeparador) & oItem.IdLineaSolicit
            sCadena = sCadena & Chr(m_lSeparador) & oItem.Id
                      
            'EstablecerGrupoItemSolicitud sGrupo, vbleId, vbleCod, vbleDen, oItem
            sCadena = sCadena & Chr(m_lSeparador) & oItem.grupoCod & " - " & oItem.GrupoDen
            
            'Extraemos la famila del material.
On Error GoTo errArrayVacio
            For i = 0 To UBound(m_arrFamMat, 2)
                If m_arrFamMat(0, i) = oItem.GMN1Cod And m_arrFamMat(1, i) = oItem.GMN2Cod And m_arrFamMat(2, i) = oItem.GMN3Cod And m_arrFamMat(3, i) = oItem.GMN4Cod Then
                    sCadena = sCadena & Chr(m_lSeparador) & oItem.GMN1Cod & "-" & oItem.GMN2Cod & "-" & oItem.GMN3Cod & "-" & oItem.GMN4Cod & "-" & m_arrFamMat(4, i)
                    Exit For
                End If
            Next
SinMaterial:
            'sCadena = sCadena & chr(m_lSeparador) & oItem.Id & chr(m_lSeparador) & oItem.ArticuloCod & chr(m_lSeparador) & oItem.Descr
            sCadena = sCadena & Chr(m_lSeparador) & oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr
            sCadena = sCadena & Chr(m_lSeparador) & IIf(oItem.FechaInicioSuministro = TimeValue("00:00"), "", Format(oItem.FechaInicioSuministro, "Short Date"))
            sCadena = sCadena & Chr(m_lSeparador) & IIf(oItem.FechaFinSuministro = TimeValue("00:00"), "", Format(oItem.FechaFinSuministro, "Short Date"))
            sCadena = sCadena & Chr(m_lSeparador) & oItem.Cantidad & Chr(m_lSeparador) & oItem.Precio & Chr(m_lSeparador) & oItem.UniCod
            sCadena = sCadena & Chr(m_lSeparador) & oItem.Presupuesto & Chr(m_lSeparador) & oItem.DestCod & Chr(m_lSeparador) & oItem.PagCod
            sCadena = sCadena & Chr(m_lSeparador) & sproveedor & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oItem.ProveAct

            'almacena el material en la grid al que pertenece el art�culo:
            If IsNull(oItem.ArticuloCod) Then
                sCadena = sCadena & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
            Else
                sCadena = sCadena & Chr(m_lSeparador) & oItem.GMN1Cod & Chr(m_lSeparador) & oItem.GMN2Cod & Chr(m_lSeparador) & oItem.GMN3Cod & Chr(m_lSeparador) & oItem.GMN4Cod
                 'Sacamos el riesgo
                Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
                oGMN4.GMN1Cod = oItem.GMN1Cod
                oGMN4.GMN2Cod = oItem.GMN2Cod
                oGMN4.GMN3Cod = oItem.GMN3Cod
                oGMN4.Cod = oItem.GMN4Cod
                iRiesgo = oGMN4.DevolverRiesgo
                Set oGMN4 = Nothing
            End If
            sCadena = sCadena & Chr(m_lSeparador) & iRiesgo
            If Not oItem.AtributosEspecificacion Is Nothing Then
                For i = 23 To sdbgLineas.Columns.Count - 1
                    sCadena = sCadena & Chr(m_lSeparador) & ""
                    
                    For Each oAtributoEspecificacion In oItem.AtributosEspecificacion
                        If sdbgLineas.Columns.Item(i).Name = oAtributoEspecificacion.Atrib Then
                            Select Case oAtributoEspecificacion.Tipo
                            Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                sCadena = sCadena & oAtributoEspecificacion.valorText
                            Case TiposDeAtributos.TipoNumerico
                                sCadena = sCadena & CStr(NullToStr(oAtributoEspecificacion.valorNum))
                            Case TiposDeAtributos.TipoFecha
                                sCadena = sCadena & IIf(oAtributoEspecificacion.valorFec = TimeValue("00:00"), "", Format(oAtributoEspecificacion.valorFec, "Short Date"))
                            Case TiposDeAtributos.TipoBoolean
                                sCadena = sCadena & IIf(oAtributoEspecificacion.valorBool = 1, "S�", "No")
                            End Select
                            
                            Exit For
                        End If
                    Next
                Next
            End If
            sdbgLineas.AddItem sCadena
        Next
               
        Dim restriccion As Boolean ' si hay plantilla desmarcar los items que sean del material de la plantilla
        restriccion = CargarPlantilla(m_oProcesoSeleccionado)
        If restriccion Then
            If CStr(sdbgLineas.Columns("LINEA").Value) <> "" Then
                If oPlantillaSeleccionada.AdmiteMaterial(m_oProcesoNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(CStr(sdbgLineas.Columns("LINEA").Value)), oPlantillaSeleccionada.Id) = False Then
                    sdbgLineas.Columns("SEL").Value = "0"
                End If
            End If
        End If
    End If
    
    sdbgLineas.Visible = True
    
    Exit Sub

errArrayVacio:
    Select Case err.Number
        Case 9
            sCadena = sCadena & Chr(m_lSeparador) & ""
            GoTo SinMaterial
    End Select
End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        sdbcAnyo.AddItem iInd
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
End Sub

Public Sub CargarProcesoConBusqueda()
    Set m_oProcesos = Nothing
    Set m_oProcesos = frmPROCEBuscar.oProceEncontrados
   
    Set frmPROCEBuscar.oProceEncontrados = Nothing
    sdbcAnyo = m_oProcesos.Item(1).Anyo
    m_bRespetarCombo = True
    sdbcGMN1_4Cod = m_oProcesos.Item(1).GMN1Cod
    sdbcProceCod = m_oProcesos.Item(1).Cod
    sdbcProceDen = m_oProcesos.Item(1).Den
    m_bRespetarCombo = False
    
    ProcesoSeleccionado
End Sub

Public Sub GMN1Seleccionado()
    Set m_oGMN1Seleccionado = Nothing
    Set m_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    
    m_oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
End Sub

Private Sub ConfigurarSeguridad()
    
        'Restricci�n de material:
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrMat)) Is Nothing) Then
            m_bRMat = True
            m_sCodComp = oUsuarioSummit.comprador.Cod
            m_sCodEqp = oUsuarioSummit.comprador.codEqp
        End If

        m_bRAsig = (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestAsignado)) Is Nothing))
        m_bRCompResp = (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestResponsable)) Is Nothing))
        m_bREqpAsig = (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestEqpAsignado)) Is Nothing))
        m_bRUsuAper = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUsuAper)) Is Nothing)
        m_bRUsuUON = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUsuUON)) Is Nothing)
        m_bRUsuDep = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUsuDep)) Is Nothing)
        m_bRDest = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDest)) Is Nothing)
        m_bModifProcPlantilla = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModifProcPlantilla)) Is Nothing)
        m_bModifProcDesdeSolic = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModifProcDesdeSolic)) Is Nothing)
                
        'Abrir procesos multimaterial
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEPermProcMultimaterial)) Is Nothing) Then
            If gParametrosGenerales.gbMultiMaterial Then
                m_bPermProcMultimaterial = True
            Else
                m_bPermProcMultimaterial = False
            End If
        Else
            m_bPermProcMultimaterial = False
        End If
End Sub

''' <summary>
''' Selecciona el proceso al que se envia
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sdbcProceCod_CloseUp;sdbcProceCod_Validate;sdbcProceDen_CloseUp;sdbcProceDen_Validate;CargarProcesoConBusqueda; Tiempo m�ximo:0,6</remarks>

Private Sub ProcesoSeleccionado()
Dim scod As String
Dim oGrupo As CGrupo
Dim oGrupoProce As CGrupo
Dim oItem As CItem
Dim i As Integer
Dim sGrupo As String
Dim bCoincide As Boolean

    'Ha seleccionado un proceso
    
    scod = sdbcGMN1_4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod))
    scod = CStr(sdbcAnyo) & scod & Trim(sdbcProceCod)
           
    Set m_oProcesoSeleccionado = Nothing
    Set m_oProcesoSeleccionado = m_oProcesos.Item(scod)
    Set oPlantillaSeleccionada = Nothing
    
    m_oProcesoSeleccionado.CargarDatosGeneralesProceso

    '''Comprueba si los items de la solicitud corresponden con el material del proceso,si no es as� se eliminan:
    If m_oProcesoNuevo.Grupos Is Nothing Then Exit Sub
    
    Dim restriccion As Boolean
    restriccion = CargarPlantilla(m_oProcesoSeleccionado)
    
    'Ahora un proceso puede tener m�s de un material.
    For Each oGrupo In m_oProcesoNuevo.Grupos
        If Not oGrupo.Items Is Nothing Then
            If oGrupo.Items.Count > 0 Then
                sdbgLineas.MoveFirst
                For i = 0 To sdbgLineas.Rows - 1
                    If CStr(sdbgLineas.Columns("LINEA").Value) <> "" Then
                        If Not oGrupo.Items.Item(CStr(sdbgLineas.Columns("LINEA").Value)) Is Nothing Then
                            oGrupo.Items.Item(CStr(sdbgLineas.Columns("LINEA").Value)).eliminado = False
                        End If
                    End If
                    sdbgLineas.MoveNext
                Next
                
                'si el proceso es de plantilla se comprueba si los art�culos son del material de la plantilla
                'Elimina los �tems que no son
                If restriccion Then
                    For Each oItem In oGrupo.Items
                        If oItem.eliminado = False Then
                            If oPlantillaSeleccionada.AdmiteMaterial(oItem, oPlantillaSeleccionada.Id) = False Then oItem.eliminado = True
                        End If
                    Next
                End If
                'Si el usuario no puede crear procesos multimaterial comprueba si los art�culos son del material de proceso
                'Elimina los �tems que no son
                If Not m_bPermProcMultimaterial Then
                    For Each oItem In oGrupo.Items
                        If oItem.eliminado = False Then
                            If Not m_oProcesoSeleccionado.ExisteMaterialEnProceso(oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN3Cod, oItem.GMN4Cod) Then
                                oItem.eliminado = True
                            End If
                        End If
                    Next
                End If
                
                'Comprobar si los grupos a los que ir�n cada item est�n cerrados
                For Each oGrupoProce In m_oProcesoSeleccionado.Grupos
                    'Si coincide la denominaci�n o el c�digo, los items ir�n a ese grupo
                    bCoincide = False
                    If UCase(oGrupo.Den) = UCase(oGrupoProce.Den) Then
                        bCoincide = True
                    Else
                        If oGrupo.Codigo = oGrupoProce.Codigo Then bCoincide = True
                    End If
                
                    If bCoincide Then
                        If SQLBinaryToBoolean(oGrupoProce.Cerrado) Then
                            For Each oItem In oGrupo.Items
                                oItem.eliminado = True
                            Next
                        End If
                    End If
                Next
                
                'Desmarcar los �tems eliminados arriba
                sGrupo = Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2)
                If oGrupo.Codigo = sGrupo Then
                    sdbgLineas.MoveFirst
                    For i = 0 To sdbgLineas.Rows - 1
    
                        sdbgLineas.Columns("SEL").Value = "1"
                        
                        If GridCheckToBoolean(sdbgLineas.Columns("SEL").Value) = True Then
                            If oGrupo.Items.Item(CStr(sdbgLineas.Columns("LINEA").Value)).eliminado = True Then
                                sdbgLineas.Columns("SEL").Value = "0"
                            End If
                            
                        End If
                        sdbgLineas.MoveNext
                    Next i
                    sdbgLineas.MoveFirst
                End If
            End If
        End If
    Next
    
End Sub

''' <summary>
''' Genera los nuevos datos de la solicitud en el proceso
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: frmSolEnviarAProcCargarDatosInstancia; Tiempo m�ximo:0,6</remarks>
Private Sub GenerarNuevosGrupos()
    'carga las l�neas de desglose y genera la estructura del proceso:
    Set m_oProcesoNuevo = Nothing
    Set m_oProcesoNuevo = oFSGSRaiz.Generar_CProceso
    
    m_oSolicitudSeleccionada.GenerarEstructuraProceso m_oProcesoNuevo, g_oParametrosSM, Nothing, m_bRMat, m_sIdiTrue, m_sIdiFalse, m_arrMatComp
    
    'Si el destino,forma de pago y fechas de suministro no est�n a nivel de �tem no se muestran en la grid:
    If m_oProcesoNuevo.DefDestino <> EnItem Then sdbgLineas.Columns("DEST").Visible = False
    If m_oProcesoNuevo.DefFormaPago <> EnItem Then sdbgLineas.Columns("PAGO").Visible = False

    If m_oProcesoNuevo.DefFechasSum <> EnItem Then
        sdbgLineas.Columns("FEC_INI").Visible = False
        sdbgLineas.Columns("FEC_FIN").Visible = False
    End If

    If m_oProcesoNuevo.DefProveActual <> EnItem Then sdbgLineas.Columns("PROV").Visible = False
End Sub


Private Sub ComprobacionEnviarAProceso()
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim sArticulosaBorrar As String
Dim sArticulosaModificar As String
Dim irespuesta As Integer
Dim oArticulos As CArticulos
        
    irespuesta = vbYes
    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
        
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEAltaArticulos)) Is Nothing) Or _
            (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.art4)) Then
                    
            If Not m_oProcesoNuevo.Grupos Is Nothing Then
           
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            ''Buscar el Articulo en la BBDD
                            If ("" & oItem.ArticuloCod) <> "" Then
                                Set oArticulos = oFSGSRaiz.Generar_CArticulos
                                oArticulos.CargarTodosLosArticulos oItem.ArticuloCod, , True, , , , oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN3Cod, oItem.GMN4Cod
                                                        
                                If oArticulos.Count = 0 Then
                                    sArticulosaBorrar = sArticulosaBorrar & oItem.ArticuloCod & " - " & oItem.Descr & Chr(10)
                                    oGrupo.Items.Remove (CStr(oItem.Id))
                                ElseIf oArticulos.Count = 1 Then
                                    'Si la den es distinta y el articulo no generico
                                    If oItem.Descr <> oArticulos.Item(1).Den And Not oArticulos.Item(1).Generico Then
                                        sArticulosaModificar = sArticulosaModificar & oItem.ArticuloCod & " - " & oItem.Descr & Chr(10)
                                        oItem.Descr = oArticulos.Item(1).Den
                                    End If
                                End If
                            
                                Set oArticulos = Nothing
                            End If
                        Next
                    End If
                Next
            End If
        End If
        
        If sArticulosaBorrar <> "" And Not (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.art4)) Then
            irespuesta = oMensajes.PreguntaAbrirProcesoEliminandoArticulos(sArticulosaBorrar, True)
            If irespuesta = vbYes Then
                If sArticulosaModificar <> "" And Not (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.art4)) Then
                    irespuesta = oMensajes.PreguntaAbrirProcesoSinModificarDescripcion(sArticulosaModificar, True)
                End If
            End If
        ElseIf sArticulosaModificar <> "" And Not (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.art4)) Then
            irespuesta = oMensajes.PreguntaAbrirProcesoSinModificarDescripcion(sArticulosaModificar, True)
        End If
    Else
        'En caso de que sea Administrador mirar si el sentido de la integracion es de SAP -> a FULLSTEP
        If (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.art4)) Then
            If Not m_oProcesoNuevo.Grupos Is Nothing Then
                For Each oGrupo In m_oProcesoNuevo.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            ''Buscar el Articulo en la BBDD
                            If ("" & oItem.ArticuloCod) <> "" Then
                                Set oArticulos = oFSGSRaiz.Generar_CArticulos
                                oArticulos.CargarTodosLosArticulos oItem.ArticuloCod, , True, , , , oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN3Cod, oItem.GMN4Cod
                            
                                If oArticulos.Count = 0 Then
                                    oGrupo.Items.Remove (CStr(oItem.Id))
                                ElseIf oArticulos.Count = 1 Then
                                    If Not oArticulos.Item(1).Generico Then oItem.Descr = oArticulos.Item(1).Den
                                End If
                            
                                Set oArticulos = Nothing
                            End If
                        Next
                    End If
                Next
            End If
        End If
    End If
 
    If irespuesta <> vbYes Then Unload Me
End Sub

'devuelve true si la plantilla tuiene restriccion
Private Function CargarPlantilla(oProceso As Object) As Boolean
    Static Plantilla As Integer
    
    If IsNull(Plantilla) Then Plantilla = 0
        
    CargarPlantilla = False

    If oProceso Is Nothing Then Exit Function
    If IsNull(oProceso.Plantilla) Then Exit Function
    If oProceso.Plantilla = 0 Then Exit Function
        
    'cambio de plantilla
    If Plantilla <> oProceso.Plantilla Then Set oPlantillaSeleccionada = Nothing
        
    If oPlantillaSeleccionada Is Nothing Then
        Set oPlantillas = oFSGSRaiz.Generar_CPlantillas
        oPlantillas.CargarTodasLasPlantillas oProceso.Plantilla, False
        Set oPlantillaSeleccionada = oPlantillas.Item(CStr(oProceso.Plantilla))
        CargarPlantilla = oPlantillaSeleccionada.ExisteRestriccionMaterial(oProceso.Plantilla)
    Else
        CargarPlantilla = oPlantillaSeleccionada.RestriccionMaterial
    End If
End Function

Function ContarLineasSeleccionadas() As Integer
    Dim i As Integer
    Dim n As Integer
    
    i = 0
    n = 0
    
    For i = 0 To sdbgLineas.Rows - 1
        If GridCheckToBoolean(sdbgLineas.Columns("SEL").Value) = True Then
            n = n + 1
        End If
        sdbgLineas.MoveNext
    Next
    
    ContarLineasSeleccionadas = n
End Function
