VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCATDatExtModificar 
   BackColor       =   &H00808000&
   Caption         =   "ART001- Art�culo 0001; Prove1 - Proveedor1"
   ClientHeight    =   5070
   ClientLeft      =   2310
   ClientTop       =   3270
   ClientWidth     =   7620
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATDatExtModificar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5070
   ScaleWidth      =   7620
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2580
      TabIndex        =   9
      Top             =   600
      Width           =   1155
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3840
      TabIndex        =   8
      Top             =   600
      Width           =   1155
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
      Height          =   915
      Left            =   5190
      TabIndex        =   7
      Top             =   1860
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCATDatExtModificar.frx":0CB2
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   53
      Columns(0).Width=   3201
      Columns(0).Name =   "NOMBRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   495
      Left            =   2250
      ScaleHeight     =   495
      ScaleWidth      =   5235
      TabIndex        =   6
      Top             =   90
      Width           =   5235
      Begin VB.TextBox txtCodExt 
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   0
         MaxLength       =   100
         TabIndex        =   2
         Top             =   60
         Width           =   5085
      End
   End
   Begin VB.CommandButton cmdDeshacer 
      Caption         =   "&Deshacer"
      Enabled         =   0   'False
      Height          =   315
      Left            =   90
      TabIndex        =   5
      Top             =   4740
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.CommandButton cmdModoEdicion 
      Caption         =   "&Edici�n"
      Height          =   315
      Left            =   6330
      TabIndex        =   0
      Top             =   4740
      Width           =   1155
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
      Height          =   3720
      Left            =   60
      TabIndex        =   3
      Top             =   990
      Width           =   7425
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   8
      stylesets.count =   3
      stylesets(0).Name=   "Checked"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCATDatExtModificar.frx":0CCE
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCATDatExtModificar.frx":0F2A
      stylesets(2).Name=   "UnChecked"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmCATDatExtModificar.frx":0F46
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   8
      Columns(0).Width=   2434
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasHeadForeColor=   -1  'True
      Columns(0).HasHeadBackColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).HeadForeColor=   16777215
      Columns(0).HeadBackColor=   32896
      Columns(0).BackColor=   52428
      Columns(1).Width=   6085
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).Style=   1
      Columns(1).ButtonsAlways=   -1  'True
      Columns(1).HasHeadForeColor=   -1  'True
      Columns(1).HasHeadBackColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).HeadForeColor=   16777215
      Columns(1).HeadBackColor=   32896
      Columns(1).BackColor=   52428
      Columns(2).Width=   3572
      Columns(2).Caption=   "Valor"
      Columns(2).Name =   "VALOR"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).Locked=   -1  'True
      Columns(2).HasHeadForeColor=   -1  'True
      Columns(2).HasHeadBackColor=   -1  'True
      Columns(2).HeadForeColor=   16777215
      Columns(2).HeadBackColor=   32896
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "TIPO"
      Columns(3).Name =   "TIPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "INTRO"
      Columns(4).Name =   "INTRO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ID_A"
      Columns(5).Name =   "ID"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ID_P"
      Columns(6).Name =   "INDICE"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "OBL"
      Columns(7).Name =   "OBL"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   11
      Columns(7).FieldLen=   256
      _ExtentX        =   13097
      _ExtentY        =   6562
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Atributos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   60
      TabIndex        =   4
      Top             =   660
      Width           =   7395
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "C�digo externo del art�culo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   180
      TabIndex        =   1
      Top             =   180
      Width           =   1995
   End
End
Attribute VB_Name = "frmCATDatExtModificar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_sIdiEdicion As String
Private m_sIdiConsulta As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private sIdiMayor As String
Private sIdiMenor As String
Private sIdiEntre As String

Public bRespetarCombo As Boolean
Private bModoEdicion As Boolean
Private bModError As Boolean
Private Accion As accionessummit
Private bAceptar As Boolean

Private m_oAtribEnEdicion As CAtributo
Public g_oArticulo As CArticulo
Public g_oProveedor As CProveedor
Public g_bSoloCodExt As Boolean

Private Sub cmdAceptar_Click()
If Accion = ACCArticDatExtModificar Then
    bAceptar = True
    txtCodExt_Validate False
End If
Unload Me
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub cmdDeshacer_Click()
    bModError = False

    sdbgAtributos.CancelUpdate
    sdbgAtributos.DataChanged = False

    If Not m_oAtribEnEdicion Is Nothing Then
        Set m_oAtribEnEdicion = Nothing
    End If

    cmdDeshacer.Enabled = False

    Accion = ACCArticDatExtConsultar

    If Me.Visible Then sdbgAtributos.SetFocus
End Sub

Private Sub Form_Load()
Me.Height = 5535
Me.Width = 7740

Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

CargarRecursos
    PonerFieldSeparator Me

If g_bSoloCodExt Then
    cmdAceptar.Visible = True
    cmdCancelar.Visible = True
    Label2.Visible = False
    Picture1.Enabled = True
    Me.Height = 1350
    bAceptar = False
Else
    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
    Label2.Visible = True
    Picture1.Enabled = False
    MostrarAtributos
    'Se carga en modo editar
    sdbgAtributos.Columns(2).Locked = False
    cmdDeshacer.Visible = True
    Picture1.Enabled = True
    bModoEdicion = True

End If
Accion = ACCArticDatExtConsultar


End Sub

Private Sub cmdModoEdicion_Click()

    If bModoEdicion Then  'De edici�n a consulta

        If sdbgAtributos.DataChanged Then
            bModError = False
            If sdbgAtributos.Rows = 1 Then
                sdbgAtributos.Update
            Else
                sdbgAtributos.MoveNext
                If Not bModError Then
                    sdbgAtributos.MovePrevious
                End If
            End If
        End If
        If Not bModError Then
            cmdModoEdicion.caption = m_sIdiEdicion
            cmdDeshacer.Visible = False
            bModoEdicion = False
            Picture1.Enabled = False
            sdbgAtributos.Columns(2).Locked = True
            sdbgAtributos.Columns("VALOR").Style = ssStyleEdit
            Accion = ACCArticDatExtConsultar
            ActualizarGridArticulos
        End If
    Else 'De consulta a edicion
        
        Screen.MousePointer = vbHourglass
        
        sdbgAtributos.Columns(2).Locked = False
        DoEvents
        sdbgAtributos.Col = 2
        Screen.MousePointer = vbNormal
        cmdDeshacer.Visible = True
        Picture1.Enabled = True
        cmdModoEdicion.caption = m_sIdiConsulta
        bModoEdicion = True
    End If

End Sub

                

Private Sub Form_Resize()
If Me.Height < 2000 Then Exit Sub
If Me.Width < 2000 Then Exit Sub

Label2.Width = Me.Width - 345
sdbgAtributos.Width = Me.Width - 315
sdbgAtributos.Height = Me.Height - 1815
cmdDeshacer.Top = Me.Height - 795
cmdModoEdicion.Top = Me.Height - 795
cmdModoEdicion.Left = sdbgAtributos.Left + sdbgAtributos.Width - cmdModoEdicion.Width

sdbgAtributos.Columns(0).Width = sdbgAtributos.Width * 0.2
sdbgAtributos.Columns(1).Width = sdbgAtributos.Width * 0.5
sdbgAtributos.Columns(2).Width = sdbgAtributos.Width * 0.3
If sdbgAtributos.Columns(2).Width > 600 Then
    sdbgAtributos.Columns(2).Width = sdbgAtributos.Columns(2).Width - 550
End If
sdbddValor.Width = sdbgAtributos.Columns(2).Width
sdbddValor.Columns(0).Width = sdbgAtributos.Columns(2).Width

End Sub

Private Sub Form_Unload(Cancel As Integer)
If bModoEdicion And Not g_bSoloCodExt Then
    If Accion = ACCArticDatExtModificar Then
        txtCodExt_Validate False
        If sdbgAtributos.DataChanged Then
            bModError = False
            sdbgAtributos.Update
        End If
    End If
    ActualizarGridArticulos
End If
Set m_oAtribEnEdicion = Nothing
Set g_oArticulo = Nothing
Set g_oProveedor = Nothing
bModError = False
bModoEdicion = False
bRespetarCombo = False
End Sub

Private Sub sdbgatributos_AfterUpdate(RtnDispErrMsg As Integer)
    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila

    Dim teserror As TipoErrorSummit

    RtnDispErrMsg = 0

    If m_oAtribEnEdicion Is Nothing Then
        DoEvents
        sdbgatributos_Change
        DoEvents
    End If

    bModError = False
    
    If sdbgAtributos.Columns("VALOR").Value = "" Then
            m_oAtribEnEdicion.valor = Null
    Else
        Select Case m_oAtribEnEdicion.Tipo
        Case TiposDeAtributos.TipoString
            m_oAtribEnEdicion.valor = CStr(sdbgAtributos.Columns("VALOR").Value)
        Case TiposDeAtributos.TipoNumerico
            m_oAtribEnEdicion.valor = CDbl(sdbgAtributos.Columns("VALOR").Value)
        Case TiposDeAtributos.TipoFecha
            m_oAtribEnEdicion.valor = CDate(sdbgAtributos.Columns("VALOR").Value)
        Case TiposDeAtributos.TipoBoolean
            If UCase(sdbgAtributos.Columns("VALOR").Value) = UCase(m_sIdiTrue) Then
                m_oAtribEnEdicion.valor = True
            Else
                m_oAtribEnEdicion.valor = False
            End If
        End Select
    End If
    
    teserror = m_oAtribEnEdicion.GuardarAtributoDeProveedor(g_oProveedor.Cod)
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        If Me.Visible Then sdbgAtributos.SetFocus
        bModError = True

    Else
        ''' Registro de acciones
       basSeguridad.RegistrarAccion Accion, "Prove:" & g_oProveedor.Cod & "GMN1:" & g_oArticulo.GMN1Cod & "GMN2:" & g_oArticulo.GMN2Cod & "GMN3:" & g_oArticulo.GMN3Cod & "GMN4:" & g_oArticulo.GMN4Cod & "Art:" & g_oArticulo.Cod & "Atrib:" & m_oAtribEnEdicion.Id
            
        Accion = ACCArticDatExtConsultar
        cmdDeshacer.Enabled = False
        
        Set m_oAtribEnEdicion = Nothing
    End If
End Sub



Private Sub sdbgatributos_BeforeUpdate(Cancel As Integer)
Dim oElem As CValorPond
Dim bEncontrado As Boolean
Dim bSalir As Boolean
Dim sError As String

    If sdbgAtributos.Columns("VALOR").Text <> "" Then
        bSalir = False
        Select Case sdbgAtributos.Columns("TIPO").Value
        Case TiposDeAtributos.TipoNumerico
            sError = "TIPO2"
            If Not IsNumeric(sdbgAtributos.Columns("VALOR").Text) Then
                bSalir = True
            End If
        Case TiposDeAtributos.TipoFecha
            sError = "TIPO3"
            If Not IsDate(sdbgAtributos.Columns("VALOR").Text) Then
                bSalir = True
            End If
        Case TiposDeAtributos.TipoBoolean
            sError = "TIPO4"
            If UCase(sdbgAtributos.Columns("VALOR").Text) <> UCase(m_sIdiTrue) And UCase(sdbgAtributos.Columns("VALOR").Text) <> UCase(m_sIdiFalse) Then
                bSalir = True
            End If
        End Select
        If bSalir Then
            oMensajes.AtributoValorNoValido sError
            Cancel = True
            bModError = True
            If Me.Visible Then sdbgAtributos.SetFocus
            sdbgAtributos.Col = 2
            Exit Sub
        End If
        
        bEncontrado = False
        If m_oAtribEnEdicion.TipoIntroduccion = Introselec Then
            For Each oElem In m_oAtribEnEdicion.ListaPonderacion
                Select Case sdbgAtributos.Columns("TIPO").Value
                Case TiposDeAtributos.TipoString
                    If oElem.ValorLista = sdbgAtributos.Columns("VALOR").Text Then
                        bEncontrado = True
                        Exit For
                    End If
                Case TiposDeAtributos.TipoNumerico
                    If CDbl(oElem.ValorLista) = CDbl(sdbgAtributos.Columns("VALOR").Text) Then
                        bEncontrado = True
                        Exit For
                    End If
                Case TiposDeAtributos.TipoFecha
                    If CDate(oElem.ValorLista) = CDate(sdbgAtributos.Columns("VALOR").Text) Then
                        bEncontrado = True
                        Exit For
                    End If
                End Select
            Next
            If Not bEncontrado Then
                oMensajes.AtributoValorNoValido "NO_LISTA"
                bModError = True
                If Me.Visible Then sdbgAtributos.SetFocus
                sdbgAtributos.Col = 2
                Cancel = True
                Exit Sub
            End If
        End If
        bSalir = False
        sError = ""
        If m_oAtribEnEdicion.TipoIntroduccion = IntroLibre Then
            Select Case sdbgAtributos.Columns("TIPO").Value
                Case TiposDeAtributos.TipoNumerico
                    If IsNumeric(m_oAtribEnEdicion.Maximo) Then
                        sError = FormateoNumerico(m_oAtribEnEdicion.Maximo)
                        If CDbl(m_oAtribEnEdicion.Maximo) < CDbl(sdbgAtributos.Columns("VALOR").Text) Then
                            bSalir = True
                        End If
                    End If
                    If IsNumeric(m_oAtribEnEdicion.Minimo) Then
                        If sError = "" Then
                            sError = sIdiMayor & " " & FormateoNumerico(m_oAtribEnEdicion.Minimo)
                        Else
                            sError = sIdiEntre & "  " & FormateoNumerico(m_oAtribEnEdicion.Minimo) & " - " & sError
                        End If
                        If CDbl(m_oAtribEnEdicion.Minimo) > CDbl(sdbgAtributos.Columns("VALOR").Text) Then
                            bSalir = True
                        End If
                    Else
                        If sError <> "" Then
                            sError = sIdiMenor & " " & sError
                        End If
                    End If
                Case TiposDeAtributos.TipoFecha
                    If IsDate(m_oAtribEnEdicion.Maximo) Then
                        sError = m_oAtribEnEdicion.Maximo
                        If CDate(m_oAtribEnEdicion.Maximo) < CDate(sdbgAtributos.Columns("VALOR").Text) Then
                            bSalir = True
                        End If
                    End If
                    If IsDate(m_oAtribEnEdicion.Minimo) Then
                        If sError = "" Then
                            sError = sIdiMayor & " " & m_oAtribEnEdicion.Minimo
                        Else
                            sError = sIdiEntre & "  " & m_oAtribEnEdicion.Minimo & " - " & sError
                        End If
                        If CDate(m_oAtribEnEdicion.Minimo) > CDate(sdbgAtributos.Columns("VALOR").Text) Then
                            bSalir = True
                        End If
                    Else
                        If sError <> "" Then
                            sError = sIdiMenor & " " & sError
                        End If
                    End If
            End Select
            If bSalir Then
                oMensajes.AtributoValorNoValido sError
                bModError = True
                If Me.Visible Then sdbgAtributos.SetFocus
                sdbgAtributos.Col = 2
                Cancel = True
            End If
        End If
        
        
    End If 'Si hab�a valor
    


End Sub

Private Sub sdbgatributos_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    If bModoEdicion Then

        DoEvents

        Set m_oAtribEnEdicion = Nothing

        Set m_oAtribEnEdicion = g_oArticulo.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("INDICE").Value))
        Accion = ACCArticDatExtModificar
        cmdDeshacer.Enabled = True
    End If

End Sub



Private Sub sdbgAtributos_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyEscape Then

        If sdbgAtributos.DataChanged = False Then

            sdbgAtributos.CancelUpdate
            sdbgAtributos.DataChanged = False

            If Not m_oAtribEnEdicion Is Nothing Then
                Set m_oAtribEnEdicion = Nothing
            End If

            cmdDeshacer.Enabled = False
            Accion = ACCRecOfeCon

        End If

    End If

End Sub

Private Sub sdbgAtributos_BtnClick()
Dim oatrib As CAtributo

    Set oatrib = g_oArticulo.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("INDICE").Value))
    Set frmDetAtribProce.g_oAtributo = oatrib
    frmDetAtribProce.g_sOrigen = "frmDatExtModif"
    frmDetAtribProce.Show 1
 

End Sub

Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

If bModoEdicion Then
    Select Case sdbgAtributos.Columns("INTRO").Value
        Case 1:  'Seleccion
            sdbddValor.RemoveAll
            sdbddValor.AddItem ""
            If sdbgAtributos.Columns("TIPO").Value = 2 Then
                sdbddValor.Columns(0).Alignment = ssCaptionAlignmentRight
            Else
                sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
            End If
            sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
            sdbddValor.Enabled = True
        Case 0: 'Libre
            If sdbgAtributos.Columns("TIPO").Value = 4 Then
                sdbddValor.RemoveAll
                sdbddValor.AddItem ""
                sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                sdbddValor.Enabled = True
            Else
                sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
                sdbddValor.Enabled = False
            End If
    End Select
            
End If
End Sub


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAT_DATOS_EXT_Modificar, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        Label2.caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiEdicion = Ador(0).Value
        Ador.MoveNext
        cmdModoEdicion.caption = Ador(0).Value
        m_sIdiConsulta = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value
        Ador.MoveNext
        sIdiMayor = Ador(0).Value
        Ador.MoveNext
        sIdiMenor = Ador(0).Value
        Ador.MoveNext
        sIdiEntre = Ador(0).Value
        Ador.Close
            
    End If
    
    Set Ador = Nothing
End Sub

Private Sub MostrarAtributos()
Dim oAtr As CAtributo

sdbgAtributos.RemoveAll


For Each oAtr In g_oArticulo.ATRIBUTOS
    sdbgAtributos.AddItem oAtr.Cod & Chr(m_lSeparador) & oAtr.Den & Chr(m_lSeparador) & MostrarAtrib(oAtr.valor, oAtr.Tipo) & Chr(m_lSeparador) & oAtr.Tipo & Chr(m_lSeparador) & oAtr.TipoIntroduccion & Chr(m_lSeparador) & oAtr.Id & Chr(m_lSeparador) & oAtr.indice
    If oAtr.TipoIntroduccion = Introselec Then
        oAtr.CargarListaDeValores
    End If
Next

End Sub


Private Function MostrarAtrib(ByVal vValorAtrib As Variant, ByVal udtTipo As TiposDeAtributos) As Variant

Select Case udtTipo
    Case TipoBoolean
        Select Case vValorAtrib
        Case 0, False
            MostrarAtrib = m_sIdiFalse
        Case 1, True
            MostrarAtrib = m_sIdiTrue
        Case Else
            MostrarAtrib = ""
        End Select
    Case Else
        If IsNull(vValorAtrib) Or IsEmpty(vValorAtrib) Or vValorAtrib = "" Then
            MostrarAtrib = ""
        Else
            MostrarAtrib = vValorAtrib
        End If
End Select
        
End Function


Private Sub sdbddValor_DropDown()
Dim oLista As CValoresPond
Dim oElem As CValorPond

If sdbgAtributos.Columns("VALOR").Locked Then
    sdbddValor.Enabled = False
    Exit Sub
End If

sdbddValor.RemoveAll
If sdbgAtributos.Columns("INTRO").Value = "1" Then
    Set oLista = g_oArticulo.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("INDICE").Value)).ListaPonderacion
    For Each oElem In oLista
        sdbddValor.AddItem oElem.ValorLista
    Next
Else
    If sdbgAtributos.Columns("TIPO").Value = 4 Then
        sdbddValor.AddItem m_sIdiTrue
        sdbddValor.AddItem m_sIdiFalse
    End If
End If
End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns("VALOR").Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
Dim bExiste As Boolean
Dim oLista As CValoresPond
Dim oElem As CValorPond

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            Set oLista = g_oArticulo.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("INDICE").Value)).ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If sdbgAtributos.Columns("TIPO").Value = 4 Then
                If UCase(m_sIdiTrue) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                    bExiste = True
                End If
                If UCase(m_sIdiFalse) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                    bExiste = True
                End If
                
            End If
        End If
        If Not bExiste Then
            sdbgAtributos.Columns("VALOR").Text = ""
        End If
        RtnPassed = True
    End If
End Sub

Private Sub txtCodExt_Change()
    If Not bRespetarCombo Then
        If Accion <> ACCArticDatExtModificar Then
            Accion = ACCArticDatExtModificar
        End If
    End If
    
End Sub

Private Sub txtCodExt_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit
Dim vCod As Variant

    If g_bSoloCodExt And Not bAceptar Then Exit Sub
    
    If Accion = ACCArticDatExtModificar Then
    
        Screen.MousePointer = vbHourglass
        
        vCod = g_oArticulo.CodigoExterno
        
        If StrComp(NullToStr(vCod), txtCodExt.Text, vbTextCompare) <> 0 Then
            
            teserror = g_oProveedor.GuardarCodigoExterno(g_oArticulo.Cod, StrToNull(txtCodExt.Text))
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Cancel = True
                Accion = ACCArticDatExtConsultar
                Exit Sub
            End If
            
            'Registrar accion
            basSeguridad.RegistrarAccion Accion, "Prove:" & g_oProveedor.Cod & "GMN1:" & g_oArticulo.GMN1Cod & "GMN2:" & g_oArticulo.GMN2Cod & "GMN3:" & g_oArticulo.GMN3Cod & "GMN4:" & g_oArticulo.GMN4Cod & "Art:" & g_oArticulo.Cod & "CodExt:" & txtCodExt.Text
            g_oArticulo.CodigoExterno = txtCodExt.Text
            frmCatalogoDatosExt.sdbgArt.Columns("Codigo").Value = txtCodExt.Text
            frmCatalogoDatosExt.sdbgArt.Update
        End If
        Screen.MousePointer = vbNormal
        Accion = ACCArticDatExtConsultar
    End If


End Sub

Private Sub ActualizarGridArticulos()
Dim i As Integer
Dim vbook As Variant

    For i = 0 To sdbgAtributos.Rows - 1
        vbook = sdbgAtributos.AddItemBookmark(i)
        If i + 1 < 10 Then
            If frmCatalogoDatosExt.sdbgArt.Columns("ID" & CStr(i + 1)).Value = sdbgAtributos.Columns("ID").CellValue(vbook) Then
                frmCatalogoDatosExt.sdbgArt.Columns("Valor" & CStr(i + 1)).Value = sdbgAtributos.Columns("VALOR").CellValue(vbook)
            End If
        End If
    Next
    frmCatalogoDatosExt.sdbgArt.Update

End Sub
