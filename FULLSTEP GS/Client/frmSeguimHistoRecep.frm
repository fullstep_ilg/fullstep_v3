VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSeguimHistoRecep 
   BackColor       =   &H00808000&
   Caption         =   "DHist�rico de recepciones de l�nea de pedido"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8880
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSeguimHistoRecep.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   8880
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdRecepcionar 
      Caption         =   "DIr a recepciones"
      Height          =   315
      Left            =   60
      TabIndex        =   0
      Top             =   2610
      Width           =   1695
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRecepcion 
      Height          =   2445
      Left            =   60
      TabIndex        =   1
      Top             =   0
      Width           =   8730
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   4
      stylesets.count =   5
      stylesets(0).Name=   "IntOK"
      stylesets(0).BackColor=   -2147483633
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSeguimHistoRecep.frx":0CB2
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSeguimHistoRecep.frx":0CCE
      stylesets(2).Name=   "ActiveRow"
      stylesets(2).ForeColor=   16777215
      stylesets(2).BackColor=   -2147483647
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSeguimHistoRecep.frx":0CEA
      stylesets(2).AlignmentText=   0
      stylesets(3).Name=   "IntHeader"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmSeguimHistoRecep.frx":0D06
      stylesets(3).AlignmentPicture=   0
      stylesets(4).Name=   "IntKO"
      stylesets(4).BackColor=   255
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmSeguimHistoRecep.frx":0E5E
      AllowUpdate     =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      MaxSelectedRows =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Caption=   "FECHA"
      Columns(0).Name =   "FECHA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "CANTIDAD"
      Columns(1).Name =   "CANTIDAD"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "ALBARAN"
      Columns(2).Name =   "ALBARAN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "RECEPTOR"
      Columns(3).Name =   "RECEPTOR"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   15399
      _ExtentY        =   4313
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSeguimHistoRecep"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmSeguimHistorico
''' *** Creacion: 27/07/2001


Option Explicit

Private sIdioma(2) As String

''' Pantalla con las recepciones por los que pasa una linea de pedido
Public oLineaPedido As CLineaPedido
Public Adores As ador.Recordset

Public IraRecep As Boolean

''' <summary>
''' Cierra la pantalla y le indica a frmSeguimiento q lance frmRecepciones
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdRecepcionar_Click()
    IraRecep = True
    Unload Me
End Sub

''' <summary>
''' Cargar la pantalla
''' </summary>
''' <remarks>Llamada desde: frmSeguimiento ; Tiempo m�ximo: 0</remarks>
Private Sub Form_Load()

    CargarRecursos
    
    Me.Width = 7365
    Me.Height = 4620
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    DoEvents
        
    Me.caption = sIdioma(0)
    
    Set Adores = oLineaPedido.DevolverTodasLasRecepciones
    CargarGrid Adores
    
    IraRecep = False
    
End Sub

''' <summary>
''' Redimensionar los controles de panatalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Form_Resize()
    
    ''' * Objetivo: Adecuar los controles
    
    If Height >= 400 Then sdbgRecepcion.Height = Height - Me.cmdRecepcionar.Height - 700
    If Width >= 120 Then sdbgRecepcion.Width = Width - 200
    
    sdbgRecepcion.Columns(0).Width = sdbgRecepcion.Width * 22 / 100
    sdbgRecepcion.Columns(1).Width = sdbgRecepcion.Width * 22 / 100
    sdbgRecepcion.Columns(2).Width = sdbgRecepcion.Width * 36 / 100
    sdbgRecepcion.Columns(3).Width = sdbgRecepcion.Width * 20 / 100 - 510
    
    Me.cmdRecepcionar.Top = sdbgRecepcion.Height + sdbgRecepcion.Top + 50
    
End Sub

''' <summary>
''' Carga los idiomas del formulario.
''' </summary>
''' <remarks>Llamada desde=Form_load; Tiempo m�ximo=0,2</remarks>
Private Sub CargarRecursos()
Dim ador As ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SEGUIMHISTORECEP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        sIdioma(0) = ador(0).Value
        ador.MoveNext
        
        For i = 0 To 2
            sdbgRecepcion.Columns(i).caption = ador(0).Value
            ador.MoveNext
        Next

        ador.MoveNext
        ador.MoveNext
        sdbgRecepcion.Columns(3).caption = ador(0).Value

        ador.MoveNext
        cmdRecepcionar.caption = ador(0).Value

        ador.Close
        
    End If
    
    Set ador = Nothing
    
End Sub


''' <summary>
''' Carga la grid
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0</remarks>
Private Sub CargarGrid(ByVal Adores As ador.Recordset)
        
    sdbgRecepcion.RemoveAll
        
    While Not Adores.EOF
        sdbgRecepcion.AddItem Adores("FECHA") & Chr(9) & Adores("CANT") & Chr(9) & Adores("ALBARAN") & Chr(9) & NullToStr(Adores("RECEPTOR"))
        Adores.MoveNext
    Wend

    'Cierra el recordset
    Adores.Close
    Set Adores = Nothing
    
End Sub
