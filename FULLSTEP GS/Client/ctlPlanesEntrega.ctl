VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{22ACD161-99EB-11D2-9BB3-00400561D975}#1.0#0"; "PVCalendar9.ocx"
Begin VB.UserControl ctlPlanesEntrega 
   ClientHeight    =   3945
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10245
   ScaleHeight     =   3945
   ScaleWidth      =   10245
   Begin VB.Frame fraCalendar 
      Appearance      =   0  'Flat
      Height          =   2385
      Left            =   210
      TabIndex        =   11
      Top             =   0
      Visible         =   0   'False
      Width           =   2535
      Begin VB.CommandButton cmdAceptarFecha 
         Caption         =   "Aceptar"
         Height          =   285
         Left            =   450
         TabIndex        =   13
         Top             =   2040
         Width           =   825
      End
      Begin VB.CommandButton cmdCancelarFecha 
         Caption         =   "Cancelar"
         Height          =   285
         Left            =   1290
         TabIndex        =   12
         Top             =   2040
         Width           =   825
      End
      Begin PVATLCALENDARLib.PVCalendar PVCalendar1 
         Height          =   1935
         Left            =   30
         TabIndex        =   14
         Top             =   90
         Width           =   2475
         _Version        =   524288
         BorderStyle     =   0
         Appearance      =   1
         FirstDay        =   1
         Frame           =   0
         SelectMode      =   2
         DisplayFormat   =   0
         DateOrientation =   8
         CustomTextOrientation=   2
         ImageOrientation=   5
         DOWText0        =   "Dom"
         DOWText1        =   "Lun"
         DOWText2        =   "Mar"
         DOWText3        =   "Mier"
         DOWText4        =   "Jue"
         DOWText5        =   "Vie"
         DOWText6        =   "Sab"
         MonthText0      =   "January"
         MonthText1      =   "February"
         MonthText2      =   "March"
         MonthText3      =   "April"
         MonthText4      =   "May"
         MonthText5      =   "June"
         MonthText6      =   "July"
         MonthText7      =   "August"
         MonthText8      =   "September"
         MonthText9      =   "October"
         MonthText10     =   "November"
         MonthText11     =   "December"
         HeaderBackColor =   13160660
         HeaderForeColor =   0
         DisplayBackColor=   16777215
         DisplayForeColor=   0
         DayBackColor    =   16777215
         DayForeColor    =   0
         SelectedDayForeColor=   0
         SelectedDayBackColor=   12632256
         BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DOWFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DaysFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MultiLineText   =   -1  'True
         EditMode        =   1
         BeginProperty TextFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picAutom 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   150
      ScaleHeight     =   375
      ScaleWidth      =   2475
      TabIndex        =   9
      Top             =   0
      Width           =   2475
      Begin VB.CheckBox chkAutom 
         Caption         =   "dRecepcion automatica"
         Height          =   255
         Left            =   0
         TabIndex        =   10
         Top             =   60
         Width           =   2475
      End
   End
   Begin VB.PictureBox picPubPortal 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   2700
      ScaleHeight     =   375
      ScaleWidth      =   2265
      TabIndex        =   7
      Top             =   0
      Width           =   2265
      Begin VB.CheckBox chkPubPortal 
         Caption         =   "dPublicar en PORTAL"
         Height          =   255
         Left            =   0
         TabIndex        =   8
         Top             =   60
         Width           =   2235
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   10245
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3570
      Width           =   10245
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2340
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   30
         Width           =   1005
      End
   End
   Begin VB.PictureBox picAplicar 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   4980
      ScaleHeight     =   375
      ScaleWidth      =   5145
      TabIndex        =   0
      Top             =   0
      Width           =   5145
      Begin VB.CheckBox chkAplicarTodos 
         Caption         =   "dAplicar el plan de entregas en todas las lineas del pedido"
         Height          =   255
         Left            =   60
         TabIndex        =   1
         Top             =   60
         Width           =   5055
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPlanes 
      Height          =   3150
      Left            =   120
      TabIndex        =   6
      Top             =   390
      Width           =   10005
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   9
      stylesets.count =   2
      stylesets(0).Name=   "Eliminado"
      stylesets(0).BackColor=   12632256
      stylesets(0).Picture=   "ctlPlanesEntrega.ctx":0000
      stylesets(1).Name=   "FondoGris"
      stylesets(1).BackColor=   12632256
      stylesets(1).Picture=   "ctlPlanesEntrega.ctx":001C
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      BalloonHelp     =   0   'False
      MaxSelectedRows =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   9
      Columns(0).Width=   2778
      Columns(0).Caption=   "DFecha de entrega"
      Columns(0).Name =   "FECHA"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   1
      Columns(0).ButtonsAlways=   -1  'True
      Columns(1).Width=   1931
      Columns(1).Caption=   "DCantidad"
      Columns(1).Name =   "CANT"
      Columns(1).Alignment=   1
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1085
      Columns(2).Caption=   "DU.P."
      Columns(2).Name =   "UP"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "dALBARAN"
      Columns(3).Name =   "ALBARAN"
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "ID"
      Columns(4).Name =   "ID"
      Columns(4).CaptionAlignment=   0
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   2037
      Columns(5).Caption=   "PRECIO"
      Columns(5).Name =   "PRECIO"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   0
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   2381
      Columns(6).Caption=   "COSTES"
      Columns(6).Name =   "COSTES"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   0
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   2381
      Columns(7).Caption=   "DESCUENTOS"
      Columns(7).Name =   "DESCUENTOS"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   0
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   2381
      Columns(8).Caption=   "IMPORTE"
      Columns(8).Name =   "IMPORTE"
      Columns(8).Alignment=   1
      Columns(8).CaptionAlignment=   0
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      _ExtentX        =   17648
      _ExtentY        =   5556
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "ctlPlanesEntrega"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public oLineaPedidoPantallaLlamada As CLineaPedido
Public g_oOrdenEntrega As COrdenEntrega
Public PermitirAplicarTodos As Boolean
Public m_bAplicarTodos As Boolean
''Si no tienes los permisos solo puedes ver
Public SoloConsulta As Boolean
''Si VienesDeSeguimiento entonces ya hay linea de pedio en bdd luego en el beforeUpdate puedes grabar.
''Si vienes de emisi�n la grabaci�n es una vez q se emita, luego hay q hacerlo con colecciones
Public DesdeSeguimiento As Boolean
Public SolicitarAceptProve As Boolean

Private oPlanesConfigurado As CPlanesEntrega
Private oIBaseDatos As IBaseDatos

''' Control de errores
Private bValError As Boolean
Private bValErrorFechaDesdeCantidad As Boolean

''' Selector fechas
Private inum As Integer
Private dDateCur As Date
Private aDatesAux() As Variant

Private aDates_Ins() As Variant
Private aCantidades_Ins() As Variant
Private aAlbaran_Ins() As Variant
Private aId_Ins() As Variant
Private aImporte_Ins() As Variant

Private m_bRespetarInsert As Boolean
Private m_bRespetarInsertBF As Boolean

Private m_dCantLineas As Double
Private m_dImpLineas As Double

Private m_dtFechaDeEmisionLinea As Date 'la fecha de emisi�n de la linea. En casos de estar en la emisi�n la fecha es la del sistema
Private m_dtFechaAModificar As Variant
Private dblEquiv As Double
Private m_bUnload As Boolean

Public g_bMostrandoLOG As Boolean

'9620
Public m_sSinFEntrega As String
Public m_sSinAnyoImput As String

Private m_bCancelPorAnnoIncorrecto As Boolean

Private Sub Arrange()
Dim lWidth As Double
lWidth = UserControl.Width - sdbgPlanes.Left - 100
sdbgPlanes.Width = lWidth
If DesdeSeguimiento Then '7080
    sdbgPlanes.Columns("FECHA").Width = lWidth * 0.23
    sdbgPlanes.Columns("CANT").Width = lWidth * 0.18
    sdbgPlanes.Columns("UP").Width = lWidth * 0.15
    sdbgPlanes.Columns("ALBARAN").Width = lWidth * 0.35
Else
    sdbgPlanes.Columns("FECHA").Width = lWidth * 0.16
    sdbgPlanes.Columns("CANT").Width = lWidth * 0.12
    sdbgPlanes.Columns("UP").Width = lWidth * 0.06
    sdbgPlanes.Columns("PRECIO").Width = lWidth * 0.14
    sdbgPlanes.Columns("COSTES").Width = lWidth * 0.15
    sdbgPlanes.Columns("DESCUENTOS").Width = lWidth * 0.15
    sdbgPlanes.Columns("IMPORTE").Width = lWidth * 0.15
End If
picAutom.Width = lWidth * 0.25
chkAutom.Width = picAutom.Width - 30
picPubPortal.Left = picAutom.Left + picAutom.Width
picPubPortal.Width = lWidth * 0.25
chkPubPortal.Width = picPubPortal.Width - 30
picAplicar.Width = (lWidth * 0.45) - 30
chkAplicarTodos.Width = picAplicar.Width - 30
picAplicar.Left = picPubPortal.Left + picPubPortal.Width
If DesdeSeguimiento Then 'aplicar en todas las lineas del pedido
    If (gParametrosGenerales.giPedPubPortal <> ComunicacionPedidoPORTAL.NoActivo) Then
        picAplicar.Left = sdbgPlanes.Left + sdbgPlanes.Width - picAplicar.Width
    Else
        picAplicar.Left = sdbgPlanes.Left + (sdbgPlanes.Width / 2)
    End If
    chkAplicarTodos.Left = 300
Else 'aplicar el plan de entregas en todas las lineas del pedido
    If (gParametrosGenerales.giPedPubPortal <> ComunicacionPedidoPORTAL.NoActivo) Then
        picAplicar.Left = sdbgPlanes.Left + sdbgPlanes.Width - 30 - UserControl.picAplicar.Width
    Else
        picAplicar.Left = sdbgPlanes.Left + (sdbgPlanes.Width / 2)
    End If
End If
If Not SoloConsulta Then
    If picNavigate.Top - (picAplicar.Height + picAplicar.Top) - 50 > 200 Then
        sdbgPlanes.Height = picNavigate.Top - (picAplicar.Height + picAplicar.Top) - 50
    End If
Else
    If picNavigate.Top + picNavigate.Height - (picAplicar.Height + picAplicar.Top) - 50 > 200 Then
        sdbgPlanes.Height = picNavigate.Top + picNavigate.Height - (picAplicar.Height + picAplicar.Top) - 50
    End If
End If
sdbgPlanes.ZOrder
End Sub

Public Sub ConfigurarSeguridad()
If SoloConsulta Then
    cmdA�adir.Visible = False
    cmdEliminar.Visible = False
    cmdDeshacer.Visible = False
    sdbgPlanes.AllowAddNew = False
    sdbgPlanes.AllowDelete = False
    sdbgPlanes.AllowUpdate = False
    sdbgPlanes.Columns("FECHA").Locked = True
    sdbgPlanes.Columns("FECHA").Style = ssStyleEdit
    sdbgPlanes.Columns("CANT").Locked = True
    picPubPortal.Enabled = False
    picAutom.Enabled = False
    picAplicar.Enabled = False
Else
    cmdA�adir.Visible = True
    cmdEliminar.Visible = True
    cmdDeshacer.Visible = True
    If (sdbgPlanes.Rows = 0) Then
        cmdEliminar.Enabled = False
    ElseIf sdbgPlanes.Columns("ALBARAN").Value <> "" Then
        cmdEliminar.Enabled = False
    Else
        cmdEliminar.Enabled = True
    End If
    sdbgPlanes.AllowAddNew = True
    sdbgPlanes.AllowDelete = True
    sdbgPlanes.AllowUpdate = True
    sdbgPlanes.Columns("FECHA").Locked = False
    sdbgPlanes.Columns("CANT").Locked = False
    sdbgPlanes.Columns("FECHA").Style = ssStyleEditButton
    picPubPortal.Enabled = True
    picAutom.Enabled = True
    picAplicar.Enabled = True
End If
Arrange
End Sub

''' <summary>
''' Se acaba de chequear "Aplicar a todas las lineas" -> aplica los planes a todas las lineas del pedido.
''' Se acaba de deschequear "Aplicar a todas las lineas" -> actualiza la orden con este cambio
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub chkAplicarTodos_Click()
Dim irespuesta As Integer
If m_bRespetarInsert Then Exit Sub
If UserControl.chkAplicarTodos.Value = vbChecked Then
    If DesdeSeguimiento Then
        If oLineaPedidoPantallaLlamada.AlgunaRecepcionEnPedido Then
            oMensajes.VasAAplicarATodosNoPuedes 0
            m_bRespetarInsert = True
            UserControl.chkAplicarTodos.Value = vbUnchecked
            m_bRespetarInsert = False
            UserControl.picAplicar.Enabled = False
            Exit Sub
        End If
    End If
    irespuesta = oMensajes.VasAAplicarATodos
    If irespuesta = vbNo Then
        m_bRespetarInsert = True
        UserControl.chkAplicarTodos.Value = vbUnchecked
        m_bRespetarInsert = False
        Exit Sub
    End If
    If DesdeSeguimiento Then
        If oLineaPedidoPantallaLlamada.tipoRecepcion = 0 Then
            frmSeguimiento.AplicarPlanConfigurado True, oPlanesConfigurado, oLineaPedidoPantallaLlamada.CantidadPedida, (chkAutom.Value = vbChecked), , (chkPubPortal.Value = vbChecked)
        Else
            frmSeguimiento.AplicarPlanConfigurado True, oPlanesConfigurado, , (chkAutom.Value = vbChecked), , (chkPubPortal.Value = vbChecked), oLineaPedidoPantallaLlamada.importePedido, True
        End If
    End If
Else
    If DesdeSeguimiento Then
        If oLineaPedidoPantallaLlamada.AlgunaRecepcionEnPedido Then
            irespuesta = oMensajes.VasAAplicarATodosNoPuedes(1)
            If irespuesta = vbNo Then
                m_bRespetarInsert = True
                UserControl.chkAplicarTodos.Value = vbChecked
                m_bRespetarInsert = False
                Exit Sub
            Else
                UserControl.picAplicar.Enabled = False
            End If
        End If
        If oLineaPedidoPantallaLlamada.tipoRecepcion = 0 Then
            frmSeguimiento.AplicarPlanConfigurado False, oPlanesConfigurado, oLineaPedidoPantallaLlamada.CantidadPedida
        Else
            frmSeguimiento.AplicarPlanConfigurado False, oPlanesConfigurado, , , , , oLineaPedidoPantallaLlamada.importePedido, True
        End If
    End If
End If
End Sub
''' <summary>
''' Actualiza la "recepci�n automatica" de la linea de pedido q esta siendo configurada.
''' Si esta activo "Aplicar a todas las lineas" tambien se actualiza en el resto de lineas del pedido
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub chkAutom_Click()
Dim teserror As TipoErrorSummit
Dim RecepAutom As Boolean
If m_bRespetarInsert Then Exit Sub
RecepAutom = (chkAutom.Value = vbChecked) And (oPlanesConfigurado.Count > 0)
If UserControl.chkAplicarTodos.Value = vbChecked Then
    If DesdeSeguimiento Then
        frmSeguimiento.AplicarPlanConfiguradoAutom RecepAutom
    Else
        'Se hara al cerrar la aplicacion del plan para todas las lineas
        oLineaPedidoPantallaLlamada.RecepAutom = RecepAutom
    End If
Else
    oLineaPedidoPantallaLlamada.RecepAutom = RecepAutom
    If DesdeSeguimiento Then
        oLineaPedidoPantallaLlamada.ActualizarPlanEntrega
        If (teserror.NumError <> TESnoerror) Then
            TratarError teserror
            Exit Sub
        End If
        frmSeguimiento.oLineaEnEdicion.RecepAutom = RecepAutom
    End If
End If
End Sub

Private Sub chkPubPortal_Click()
Dim teserror As TipoErrorSummit
Dim bCheck As Boolean
If m_bRespetarInsert Then Exit Sub
bCheck = (UserControl.chkPubPortal.Value = vbChecked)
If chkAplicarTodos.Value = vbChecked Then
    If DesdeSeguimiento Then
        frmSeguimiento.AplicarPlanPubPortal bCheck
    Else
        'Se hara al cerrar la aplicacion del plan para todas las lineas
        oLineaPedidoPantallaLlamada.PlanesPubPortal = bCheck
    End If
Else
    oLineaPedidoPantallaLlamada.PlanesPubPortal = bCheck
    If DesdeSeguimiento Then
        oLineaPedidoPantallaLlamada.ActualizarPlanEntrega
        If (teserror.NumError <> TESnoerror) Then
            TratarError teserror
            Exit Sub
        End If
        frmSeguimiento.oLineaEnEdicion.PlanesPubPortal = bCheck
    End If
End If
End Sub

''' <summary>
''' Cierra la pantalla de Calendario y trae las fechas seleccionads q no estuvieran ya entre los planes
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdAceptarFecha_Click()
Dim sCadena As String
Dim i As Integer
Dim k As Integer
Dim bEncontrado As Boolean
Dim Coste As Double, SumCoste As Double
Dim Dto As Double, SumDto As Double
Dim importe As Double
Dim NumLineasConCant As Integer
Dim j As Integer
Dim dImporte As Double
Dim sCoste As String, sDto As String, sImporte As String
Dim oPlanConfigurado As CPlanEntrega
Dim l As Integer
Dim laux As Integer
Dim bEsInsert As Boolean
Dim teserror As TipoErrorSummit
Dim aIdentificadores(1) As Variant
bEncontrado = False
inum = UBound(aDatesAux)
k = 1
While k <= UBound(aDatesAux)
    PVCalendar1.DATESelected(aDatesAux(k)) = False
    If (m_dtFechaDeEmisionLinea > CDate(aDatesAux(k))) Then
        If bEncontrado = False Then
            oMensajes.ErrorPlanesEntrega (0)
        End If
        If Not (IsNull(m_dtFechaAModificar)) Then
            m_dtFechaAModificar = Null
        End If
        For i = k To UBound(aDatesAux) - 1
            aDatesAux(i) = aDatesAux(i + 1)
        Next
        inum = inum - 1
        ReDim Preserve aDatesAux(inum)
        bEncontrado = True
        k = k - 1
    End If
    k = k + 1
Wend
ReDim aDates_Ins(0)
ReDim aCantidades_Ins(0)
ReDim aImporte_Ins(0)
ReDim aAlbaran_Ins(0)
ReDim aId_Ins(0)
k = -1
For Each oPlanConfigurado In oPlanesConfigurado
    ReDim Preserve aDates_Ins(UBound(aDates_Ins) + 1)
    aDates_Ins(UBound(aDates_Ins)) = CStr(oPlanConfigurado.Fecha)
    ReDim Preserve aCantidades_Ins(UBound(aCantidades_Ins) + 1)
    aCantidades_Ins(UBound(aCantidades_Ins)) = oPlanConfigurado.Cantidad
    ReDim Preserve aImporte_Ins(UBound(aImporte_Ins) + 1)
    aImporte_Ins(UBound(aImporte_Ins)) = oPlanConfigurado.importe
    ReDim Preserve aAlbaran_Ins(UBound(aAlbaran_Ins) + 1)
    aAlbaran_Ins(UBound(aAlbaran_Ins)) = oPlanConfigurado.albaran
    ReDim Preserve aId_Ins(UBound(aId_Ins) + 1)
    aId_Ins(UBound(aId_Ins)) = oPlanConfigurado.Id
    If oPlanConfigurado.Id <> "" Then
        If oPlanConfigurado.Id <= k Then
            k = oPlanConfigurado.Id - 1
        End If
    End If
Next
If Not (IsNull(m_dtFechaAModificar)) Then
    For j = 0 To UBound(aDates_Ins)
        If Not (aDates_Ins(j)) = "" Then
            If CDate(aDatesAux(1)) = CDate(aDates_Ins(j)) Then
                oMensajes.ErrorPlanesEntrega (2)
                Exit Sub
            End If
        End If
    Next
    For j = 0 To UBound(aDates_Ins)
        If Not (aDates_Ins(j)) = "" Then
            If CDate(aDates_Ins(j)) = m_dtFechaAModificar Then
                aDates_Ins(j) = CStr(aDatesAux(1))
            End If
        End If
    Next
End If
Set oPlanesConfigurado = oFSGSRaiz.Generar_CPlanesEntrega
Orden
If Not (IsNull(m_dtFechaAModificar)) Then
    laux = -1
    For i = 0 To UBound(aDates_Ins)
        If IsDate(aDates_Ins(i)) Then
            If CDate(aDates_Ins(i)) = CDate(aDatesAux(1)) Then
                laux = i - 1
            End If
        End If
    Next
End If
Screen.MousePointer = vbHourglass
LockWindowUpdate UserControl.hWnd
sdbgPlanes.RemoveAll
SumCoste = 0
SumDto = 0
j = 1
If (Not DesdeSeguimiento) Then
    NumLineasConCant = UBound(aDates_Ins)
Else
    NumLineasConCant = 0
    For i = 0 To UBound(aDates_Ins)
        If IsDate(aDates_Ins(i)) Then
            NumLineasConCant = NumLineasConCant + 1
        End If
    Next
    If NumLineasConCant = 0 Then NumLineasConCant = 1
End If
m_bRespetarInsert = True
l = -1
For i = 0 To UBound(aDates_Ins)
    If IsDate(aDates_Ins(i)) Or ((Not DesdeSeguimiento) And (i > 0)) Then
        If Not DesdeSeguimiento Then
            If j = UBound(aDates_Ins) Then
                Coste = oLineaPedidoPantallaLlamada.ImporteCostes - SumCoste
                Coste = Round(Coste, 2)
                Dto = oLineaPedidoPantallaLlamada.ImporteDescuentos - SumDto
                Dto = Round(Dto, 2)
            Else
                Coste = oLineaPedidoPantallaLlamada.ImporteCostes / NumLineasConCant
                Coste = Round(Coste, 2)
                SumCoste = SumCoste + Coste
                Dto = oLineaPedidoPantallaLlamada.ImporteDescuentos / NumLineasConCant
                Dto = Round(Dto, 2)
                SumDto = SumDto + Dto
            End If
            If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then
                importe = StrToDbl0(aImporte_Ins(i))
            Else
                importe = (StrToDbl0(aCantidades_Ins(i)) * oLineaPedidoPantallaLlamada.PrecioUP) + Coste - Dto
            End If
            j = j + 1
            sCoste = CStr(Coste)
            sDto = CStr(Dto)
            sImporte = CStr(importe)
            aImporte_Ins(i) = sImporte
        End If
        If (l = -1) And (aCantidades_Ins(i) = "") Then
            l = i - 1
        End If
        sCadena = aDates_Ins(i) & Chr(m_lSeparador) & aCantidades_Ins(i)
        sCadena = sCadena & Chr(m_lSeparador) & oLineaPedidoPantallaLlamada.UnidadPedido & Chr(m_lSeparador) & aAlbaran_Ins(i)
        sCadena = sCadena & Chr(m_lSeparador) & IIf(aId_Ins(i) <> "", aId_Ins(i), k)
        sCadena = sCadena & Chr(m_lSeparador) & oLineaPedidoPantallaLlamada.PrecioUP
        sCadena = sCadena & Chr(m_lSeparador) & sCoste & Chr(m_lSeparador) & sDto & Chr(m_lSeparador) & aImporte_Ins(i)
        UserControl.sdbgPlanes.AddItem sCadena
        If (aImporte_Ins(i) <> "") Then
            dImporte = CLng(aImporte_Ins(i)) / dblEquiv
        End If
        If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then
            Set oPlanConfigurado = oPlanesConfigurado.Add(IIf(aId_Ins(i) <> "", aId_Ins(i), k), oLineaPedidoPantallaLlamada.Id, oLineaPedidoPantallaLlamada.Orden, aDates_Ins(i), aCantidades_Ins(i), aAlbaran_Ins(i), , CStr(dImporte))
        Else
            Set oPlanConfigurado = oPlanesConfigurado.Add(IIf(aId_Ins(i) <> "", aId_Ins(i), k), oLineaPedidoPantallaLlamada.Id, oLineaPedidoPantallaLlamada.Orden, aDates_Ins(i), aCantidades_Ins(i), aAlbaran_Ins(i))
        End If
        If Not (IsNull(m_dtFechaAModificar)) Then
            If DesdeSeguimiento Then
                If CDate(oPlanConfigurado.Fecha) = CDate(aDatesAux(1)) Then
                    Set oPlanConfigurado.OrdenEntrega = g_oOrdenEntrega
                    Set oIBaseDatos = oPlanConfigurado
                    Set oPlanConfigurado.LineaPedido = oLineaPedidoPantallaLlamada
                    If (oLineaPedidoPantallaLlamada.tipoRecepcion = 0 And oPlanConfigurado.Cantidad <> "") _
                    Or (oLineaPedidoPantallaLlamada.tipoRecepcion = 1 And oPlanConfigurado.importe <> "") Then
                        If (oPlanConfigurado.Id = "") Then
                            bEsInsert = True
                        Else
                            bEsInsert = (CInt(oPlanConfigurado.Id) <= 0)
                        End If
                        If bEsInsert Then
                            teserror = oIBaseDatos.AnyadirABaseDatos
                        Else
                            teserror = oIBaseDatos.FinalizarEdicionModificando
                        End If
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Exit Sub
                        End If
                        If (UserControl.chkAplicarTodos.Value = vbChecked) Then
                            aIdentificadores(1) = m_dtFechaAModificar
                            frmSeguimiento.EliminarPlanesDeLineaPedido aIdentificadores
                            If oLineaPedidoPantallaLlamada.tipoRecepcion = 0 Then
                                frmSeguimiento.AplicarUnPlanConfigurado oPlanConfigurado, oLineaPedidoPantallaLlamada.CantidadPedida, True, False
                            Else
                                frmSeguimiento.AplicarUnPlanConfigurado oPlanConfigurado, , True, False, oLineaPedidoPantallaLlamada.importePedido, True
                            End If
                        End If
                    End If
                End If
            End If
        End If
        k = k - 1
    End If
Next
UserControl.sdbgPlanes.MoveFirst
i = 0
If laux > -1 Then l = laux
While (i < l)
    i = i + 1
    UserControl.sdbgPlanes.MoveNext
Wend
LockWindowUpdate 0&
Screen.MousePointer = vbNormal
m_bRespetarInsert = False
UserControl.sdbgPlanes.col = 1
fraCalendar.Visible = False
End Sub

''' <summary>
''' Se posiciona en la linea de inserci�n del grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdA�adir_Click()
sdbgPlanes.Scroll 0, sdbgPlanes.Rows - sdbgPlanes.Row
If sdbgPlanes.VisibleRows > 0 Then
    If sdbgPlanes.VisibleRows > sdbgPlanes.Rows Then
        sdbgPlanes.Row = sdbgPlanes.Rows
    Else
        sdbgPlanes.Row = sdbgPlanes.Rows - (sdbgPlanes.Rows - sdbgPlanes.VisibleRows) - 1
    End If
End If
sdbgPlanes.col = 0
If sdbgPlanes.Visible Then sdbgPlanes.SetFocus
End Sub

''' <summary>
''' Cierra la pantalla de Calendario
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdCancelarFecha_Click()
fraCalendar.Visible = False
Dim i As Integer
For i = 1 To UBound(aDatesAux)
    PVCalendar1.DATESelected(aDatesAux(i)) = False
Next
End Sub

''' <summary>
''' Deshacer el ultimo cambio en el grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdDeshacer_Click()
    sdbgPlanes.CancelUpdate
    sdbgPlanes.DataChanged = False
    UserControl.cmdDeshacer.Enabled = False
    CancelarActualizar True, 0, 0
End Sub

''' <summary>
''' Eliminar los Planes de Entrega seleccionados
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdEliminar_Click()
    Dim aIdentificadores As Variant
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim oPlanConfigurado As CPlanEntrega
    Dim bEncontrado As Boolean
    Dim ATodos As Boolean
    Dim RecepAutom As Boolean
    Dim Coste As Double, SumCoste As Double
    Dim Dto As Double, SumDto As Double
    Dim importe As Double
    Dim NumLineasConCant As Integer
    If UserControl.sdbgPlanes.Rows = 0 Then Exit Sub
    Screen.MousePointer = vbHourglass
    ATodos = (UserControl.chkAplicarTodos.Value = vbChecked)
    RecepAutom = (UserControl.chkAutom.Value = vbChecked)
    ReDim aIdentificadores(sdbgPlanes.SelBookmarks.Count)
    i = 0
    j = 0
    
    If (sdbgPlanes.SelBookmarks.Count > 0) And DesdeSeguimiento Then
        oLineaPedidoPantallaLlamada.Planes_Comprometido_Operar True, False, True
    End If
    
    While i < sdbgPlanes.SelBookmarks.Count
        If Not (sdbgPlanes.Columns("ALBARAN").CellValue(sdbgPlanes.SelBookmarks(i)) = "") Then
            sdbgPlanes.SelBookmarks.Remove i
        Else
            If Not (sdbgPlanes.Columns("ID").CellValue(sdbgPlanes.SelBookmarks(i)) = "") Then 'Filas en bd o no
                aIdentificadores(j + 1) = sdbgPlanes.Columns("FECHA").CellValue(sdbgPlanes.SelBookmarks(i))
                sdbgPlanes.Bookmark = sdbgPlanes.SelBookmarks(i)

                If DesdeSeguimiento Then
                    Set oLineaPedidoPantallaLlamada.ObjetoOrden = g_oOrdenEntrega
                    oLineaPedidoPantallaLlamada.LimpiarPlanesEntrega sdbgPlanes.Columns("ID").CellValue(sdbgPlanes.SelBookmarks(i))
                End If
                k = 1
                bEncontrado = False
                For Each oPlanConfigurado In oPlanesConfigurado
                    If oPlanConfigurado.Id = CLng(sdbgPlanes.Columns("ID").CellValue(sdbgPlanes.SelBookmarks(i))) Then
                        bEncontrado = True
                        Exit For
                    End If
                    k = k + 1
                Next
            Else
                aIdentificadores(j + 1) = sdbgPlanes.Columns("FECHA").CellValue(sdbgPlanes.SelBookmarks(i))
                k = 1
                bEncontrado = False
                For Each oPlanConfigurado In oPlanesConfigurado
                    If Not IsDate(sdbgPlanes.Columns("FECHA").CellValue(sdbgPlanes.SelBookmarks(i))) Then
                        If oPlanConfigurado.Fecha = sdbgPlanes.Columns("FECHA").CellValue(sdbgPlanes.SelBookmarks(i)) Then
                            bEncontrado = True
                            Exit For
                        End If
                    Else
                        If CDate(oPlanConfigurado.Fecha) = CDate(sdbgPlanes.Columns("FECHA").CellValue(sdbgPlanes.SelBookmarks(i))) Then
                            bEncontrado = True
                            Exit For
                        End If
                    End If
                    k = k + 1
                Next
            End If
            If bEncontrado Then oPlanesConfigurado.Remove k
            i = i + 1
        End If
        j = j + 1
    Wend
    
    If (sdbgPlanes.SelBookmarks.Count > 0) And DesdeSeguimiento Then
        oLineaPedidoPantallaLlamada.Planes_Comprometido_Operar False, False, True
    End If
    
    sdbgPlanes.DeleteSelected
    DoEvents
    sdbgPlanes.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal
    CancelarActualizar True, 0, 0
    oLineaPedidoPantallaLlamada.HayPlanes = (oPlanesConfigurado.Count > 0)
    If (ATodos) Then
        If DesdeSeguimiento Then
            frmSeguimiento.EliminarPlanesDeLineaPedido aIdentificadores
        'Else Se hara al cerrar la aplicacion del plan para todas las lineas
        End If
    End If
    If Not DesdeSeguimiento Then
        NumLineasConCant = oPlanesConfigurado.Count
        SumCoste = 0
        SumDto = 0
        Screen.MousePointer = vbHourglass
        LockWindowUpdate UserControl.hWnd
        UserControl.sdbgPlanes.MoveFirst
        For i = 0 To sdbgPlanes.Rows - 1
            If i = oPlanesConfigurado.Count - 1 Then
                Coste = oLineaPedidoPantallaLlamada.ImporteCostes - SumCoste
                Coste = Round(Coste, 2)
                Dto = oLineaPedidoPantallaLlamada.ImporteDescuentos - SumDto
                Dto = Round(Dto, 2)
            Else
                Coste = oLineaPedidoPantallaLlamada.ImporteCostes / NumLineasConCant
                Coste = Round(Coste, 2)
                SumCoste = SumCoste + Coste
                Dto = oLineaPedidoPantallaLlamada.ImporteDescuentos / NumLineasConCant
                Dto = Round(Dto, 2)
                SumDto = SumDto + Dto
            End If
            If oLineaPedidoPantallaLlamada.tipoRecepcion <> 1 Then
                importe = (StrToDbl0(UserControl.sdbgPlanes.Columns("CANTIDAD").Value) * oLineaPedidoPantallaLlamada.PrecioUP) + Coste - Dto
            Else
                importe = UserControl.sdbgPlanes.Columns("IMPORTE").Value
            End If
            UserControl.sdbgPlanes.Columns("COSTES").Value = CStr(Coste)
            UserControl.sdbgPlanes.Columns("DESCUENTOS").Value = CStr(Dto)
            UserControl.sdbgPlanes.Columns("IMPORTE").Value = CStr(importe)
            UserControl.sdbgPlanes.MoveNext
        Next
        sdbgPlanes.col = 0
        sdbgPlanes.Refresh
        LockWindowUpdate 0&
        Screen.MousePointer = vbNormal
        DoEvents
    End If
End Sub


''' <summary>Configura el formulario</summary>
''' <remarks>Llamada desde=Form_load</remarks>
''' <revision>LTG 15/02/2013</revision>

Private Sub ConfigurarFormulario()
    m_bRespetarInsert = True
    chkAutom.Value = IIf(oLineaPedidoPantallaLlamada.RecepAutom, vbChecked, vbUnchecked)
    chkPubPortal.Value = IIf(oLineaPedidoPantallaLlamada.PlanesPubPortal, vbChecked, vbUnchecked)
    If PermitirAplicarTodos Then
        picAplicar.Visible = True
        chkAplicarTodos.Value = IIf(m_bAplicarTodos, vbChecked, vbUnchecked)
        picAplicar.Enabled = True
    Else
        picAplicar.Visible = False
    End If
    m_bRespetarInsert = False
    If gParametrosGenerales.giPlanRecepAutom = SiempreAutom Then
        picAutom.Enabled = False
        chkAutom.Value = vbChecked
    ElseIf gParametrosGenerales.giPlanRecepAutom = NuncaAutom Then
        picAutom.Enabled = False
        chkAutom.Value = vbUnchecked
    End If
    picPubPortal.Visible = (gParametrosGenerales.giPedPubPortal <> ComunicacionPedidoPORTAL.NoActivo)
    If gParametrosGenerales.giPedPubPortal = ComunicacionPedidoPORTAL.NoActivo Then chkPubPortal.Value = vbUnchecked
    ConfigurarSeguridad
    cmdDeshacer.Enabled = False
End Sub

''' <summary>
''' Carga los idiomas del formulario.
''' </summary>
''' <remarks>Llamada desde=Form_load; Tiempo m�ximo=0,2</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    On Error Resume Next
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PlanesEntrega, basPublic.gParametrosInstalacion.gIdioma)
    If Not Ador Is Nothing Then
        'El primero es para el caption del formulario
        Ador.MoveNext
        UserControl.chkAutom.caption = Ador(0).Value
        Ador.MoveNext
        UserControl.chkAplicarTodos.caption = Ador(0).Value
        Ador.MoveNext
        UserControl.sdbgPlanes.Columns("FECHA").caption = Ador(0).Value
        Ador.MoveNext
        UserControl.sdbgPlanes.Columns("CANT").caption = Ador(0).Value
        Ador.MoveNext
        UserControl.sdbgPlanes.Columns("UP").caption = Ador(0).Value
        Ador.MoveNext
        UserControl.sdbgPlanes.Columns("ALBARAN").caption = Ador(0).Value
        Ador.MoveNext
        UserControl.sdbgPlanes.Columns("PRECIO").caption = Ador(0).Value
        Ador.MoveNext
        UserControl.sdbgPlanes.Columns("COSTES").caption = Ador(0).Value
        Ador.MoveNext
        UserControl.sdbgPlanes.Columns("DESCUENTOS").caption = Ador(0).Value
        Ador.MoveNext
        UserControl.sdbgPlanes.Columns("IMPORTE").caption = Ador(0).Value
        Ador.MoveNext
        UserControl.cmdA�adir.caption = Ador(0).Value
        Ador.MoveNext
        UserControl.cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        UserControl.cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        If DesdeSeguimiento Then UserControl.chkAplicarTodos.caption = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(0) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(1) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(2) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(3) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(4) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(5) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(6) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(7) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(8) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(9) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(10) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.MonthText(11) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.DOWText(0) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.DOWText(1) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.DOWText(2) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.DOWText(3) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.DOWText(4) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.DOWText(5) = Ador(0).Value
        Ador.MoveNext
        UserControl.PVCalendar1.DOWText(6) = Ador(0).Value
        Ador.MoveNext
        chkPubPortal.caption = Ador(0).Value
        Ador.Close
    End If
    Set Ador = Nothing
End Sub

''' <summary>
''' Cargar el Grid
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0</remarks>
Private Sub CargarGrid()
    Dim sCadena As String
    Dim oPlan As CPlanEntrega
    Dim i As Integer
    Dim Coste As Double, SumCoste As Double
    Dim Dto As Double, SumDto As Double
    Dim importe As Double
    sdbgPlanes.Columns("ALBARAN").Visible = (DesdeSeguimiento = True)
    If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then
        sdbgPlanes.Columns("CANT").Visible = False
        sdbgPlanes.Columns("PRECIO").Visible = False
        sdbgPlanes.Columns("COSTES").Visible = False
        sdbgPlanes.Columns("DESCUENTOS").Visible = False
        sdbgPlanes.Columns("IMPORTE").Visible = True
    Else
        sdbgPlanes.Columns("PRECIO").Visible = (DesdeSeguimiento = False)
        sdbgPlanes.Columns("COSTES").Visible = (DesdeSeguimiento = False)
        sdbgPlanes.Columns("DESCUENTOS").Visible = (DesdeSeguimiento = False)
        sdbgPlanes.Columns("IMPORTE").Visible = (DesdeSeguimiento = False)
    End If
    If DesdeSeguimiento Then 'BD
        oLineaPedidoPantallaLlamada.CargarPlanesEntrega g_bMostrandoLOG
        If oLineaPedidoPantallaLlamada.Planes.Count = 0 Then Exit Sub
        m_bRespetarInsert = True
        i = 0
        m_dCantLineas = 0
        m_dImpLineas = 0
        SumCoste = 0
        SumDto = 0
        For Each oPlan In oLineaPedidoPantallaLlamada.Planes
            sCadena = oPlan.Fecha & Chr(m_lSeparador) & oPlan.Cantidad
            sCadena = sCadena & Chr(m_lSeparador) & oLineaPedidoPantallaLlamada.UnidadPedido & Chr(m_lSeparador) & oPlan.albaran
            sCadena = sCadena & Chr(m_lSeparador) & oPlan.Id & Chr(m_lSeparador) & oLineaPedidoPantallaLlamada.PrecioUP
            sCadena = sCadena & Chr(m_lSeparador) & CStr(Coste) & Chr(m_lSeparador) & CStr(Dto) & Chr(m_lSeparador) & oPlan.importe * dblEquiv
            UserControl.sdbgPlanes.AddItem sCadena
            If oPlan.Cantidad <> "" Then
                m_dCantLineas = m_dCantLineas + oPlan.Cantidad
            End If
            If oPlan.importe <> "" Then
                m_dImpLineas = m_dImpLineas + oPlan.importe
            End If
            If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then
                oPlanesConfigurado.Add oPlan.Id, oLineaPedidoPantallaLlamada.Id, oLineaPedidoPantallaLlamada.Orden, CDate(oPlan.Fecha), oPlan.Cantidad, oPlan.albaran, , oPlan.importe
            Else
                oPlanesConfigurado.Add oPlan.Id, oLineaPedidoPantallaLlamada.Id, oLineaPedidoPantallaLlamada.Orden, CDate(oPlan.Fecha), oPlan.Cantidad, oPlan.albaran
            End If
            i = i + 1
        Next
    Else 'Coleccion planes de clineapedido
        If oLineaPedidoPantallaLlamada.Planes Is Nothing Then
            Set oLineaPedidoPantallaLlamada.Planes = oFSGSRaiz.Generar_CPlanesEntrega
        Else
            i = 0
            For Each oPlan In oLineaPedidoPantallaLlamada.Planes
                ReDim Preserve aDatesAux(i + 1)
                aDatesAux(i + 1) = CDate(oPlan.Fecha)
                ReDim Preserve aDates_Ins(i)
                aDates_Ins(i) = CDate(oPlan.Fecha)
                ReDim Preserve aCantidades_Ins(i)
                aCantidades_Ins(i) = oPlan.Cantidad
                ReDim Preserve aImporte_Ins(i)
                aImporte_Ins(i) = oPlan.importe
                If oPlan.Cantidad <> "" Then
                    m_dCantLineas = m_dCantLineas + oPlan.Cantidad
                End If
                If oPlan.importe <> "" Then
                    m_dImpLineas = m_dImpLineas + oPlan.importe
                End If
                ReDim Preserve aAlbaran_Ins(i)
                aAlbaran_Ins(i) = oPlan.albaran
                ReDim Preserve aId_Ins(i)
                aId_Ins(i) = oPlan.Id
                i = i + 1
            Next
            Orden
            m_bRespetarInsert = True
            For i = 0 To UBound(aDates_Ins)
                If IsDate(aDates_Ins(i)) Then
                    If i = UBound(aDates_Ins) Then
                        Coste = oLineaPedidoPantallaLlamada.ImporteCostes - SumCoste
                        Coste = Round(Coste, 2)
                        Dto = oLineaPedidoPantallaLlamada.ImporteDescuentos - SumDto
                        Dto = Round(Dto, 2)
                    Else
                        Coste = oLineaPedidoPantallaLlamada.ImporteCostes / (UBound(aDates_Ins) + 1)
                        Coste = Round(Coste, 2)
                        SumCoste = SumCoste + Coste
                        Dto = oLineaPedidoPantallaLlamada.ImporteDescuentos / (UBound(aDates_Ins) + 1)
                        Dto = Round(Dto, 2)
                        SumDto = SumDto + Dto
                    End If
                    If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then
                        importe = StrToDbl0(aImporte_Ins(i))
                    Else
                        importe = (StrToDbl0(aCantidades_Ins(i)) * oLineaPedidoPantallaLlamada.PrecioUP) + Coste - Dto
                    End If
                    sCadena = aDates_Ins(i) & Chr(m_lSeparador) & aCantidades_Ins(i)
                    sCadena = sCadena & Chr(m_lSeparador) & oLineaPedidoPantallaLlamada.UnidadPedido & Chr(m_lSeparador) & aAlbaran_Ins(i)
                    sCadena = sCadena & Chr(m_lSeparador) & aId_Ins(i) & Chr(m_lSeparador) & oLineaPedidoPantallaLlamada.PrecioUP
                    sCadena = sCadena & Chr(m_lSeparador) & CStr(Coste) & Chr(m_lSeparador) & CStr(Dto) & Chr(m_lSeparador) & CStr(importe)
                    UserControl.sdbgPlanes.AddItem sCadena
                    If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then
                        oPlanesConfigurado.Add aId_Ins(i), oLineaPedidoPantallaLlamada.Id, oLineaPedidoPantallaLlamada.Orden, aDates_Ins(i), aCantidades_Ins(i), aAlbaran_Ins(i), , importe
                    Else
                        oPlanesConfigurado.Add aId_Ins(i), oLineaPedidoPantallaLlamada.Id, oLineaPedidoPantallaLlamada.Orden, aDates_Ins(i), aCantidades_Ins(i), aAlbaran_Ins(i)
                    End If
                End If
            Next
            m_bRespetarInsert = False
        End If
    End If
    m_bRespetarInsert = False
End Sub


Public Sub Terminate(Optional ByRef Cancel As Integer = 0)
    GuardarCambios Cancel
    Set oIBaseDatos = Nothing
    Set oPlanesConfigurado = Nothing
    Set oLineaPedidoPantallaLlamada = Nothing
    m_bCancelPorAnnoIncorrecto = False
End Sub

Public Sub GuardarCambios(Optional ByRef Cancel As Integer = 0)
Dim ATodos As Boolean
Dim RecepAutom As Boolean
Dim bPubPortal As Boolean
Dim Fallo As Boolean

If oLineaPedidoPantallaLlamada.Bloq = 1 Then Exit Sub
If oLineaPedidoPantallaLlamada.Baja_LOG = 1 Then Exit Sub
ATodos = (chkAplicarTodos.Value = vbChecked)
RecepAutom = (chkAutom.Value = vbChecked) And (oPlanesConfigurado.Count > 0)
bPubPortal = (chkPubPortal.Value = vbChecked)

If sdbgPlanes.DataChanged = True Then
    bValError = False
    bValErrorFechaDesdeCantidad = False
                
    sdbgPlanes.Update

    If bValError = True Or bValErrorFechaDesdeCantidad = True Then
        Cancel = True
        Exit Sub
    End If
End If

If SolicitarAceptProve And oLineaPedidoPantallaLlamada.HayPlanes And Not oLineaPedidoPantallaLlamada.PlanesPubPortal Then
    oMensajes.AceptProvePubPlanes
    Cancel = True
    Exit Sub
End If
If (actualizarYSalir = True) Then
    Cancel = True
    Exit Sub
End If
If Not DesdeSeguimiento Then
    If oLineaPedidoPantallaLlamada.tipoRecepcion <> 1 Then
        AplicarPlanConfigurado g_oOrdenEntrega, oLineaPedidoPantallaLlamada, ATodos, oPlanesConfigurado, oLineaPedidoPantallaLlamada.CantidadPedida, RecepAutom, , bPubPortal
    Else
        'Le paso el importe total de la linea y le digo que es de tipo recepci�n por importe
        AplicarPlanConfigurado g_oOrdenEntrega, oLineaPedidoPantallaLlamada, ATodos, oPlanesConfigurado, oLineaPedidoPantallaLlamada.CantidadPedida, RecepAutom, , bPubPortal, oLineaPedidoPantallaLlamada.ImporteNeto, True
    End If
ElseIf (oPlanesConfigurado.Count = 0) Then
    'Asegurarse de quitar recep autom si no hay lineas. Previamente se han quitado las lineas sin cant de la coleccion
    If chkAutom.Value = vbChecked Then
        chkAutom_Click
    End If
ElseIf (gParametrosGenerales.giPlanRecepAutom = SiempreAutom) Then
    'Asegurarse de poner recep autom si hay lineas al salir. Al entrar pod�a no haber-> no recep autom en bbdd.
    If chkAutom.Value = vbUnchecked Then
        chkAutom_Click
    End If
End If
End Sub

''' <summary>
''' Comprueba q todo este correcto con las cantidades antes de cerrar.
''' Tambi�n comprueba los importes para los de recepcion por importe.
''' </summary>
''' <returns>Si todo este correcto antes de cerrar o no</returns>
''' <remarks>Llamada desde: Form_Unload ; Tiempo m�ximo: 0</remarks>
Private Function actualizarYSalir() As Boolean
    Dim bm As Variant
    Dim i As Integer
    Dim k As Integer
    Dim oPlanConfigurado As CPlanEntrega
    Dim HayCantLineaVacia As Boolean
    HayCantLineaVacia = False
    Dim HayImporteLineaVacia As Boolean
    HayImporteLineaVacia = False
    For i = 0 To UserControl.sdbgPlanes.Rows - 1
        bm = sdbgPlanes.AddItemBookmark(i)
        If sdbgPlanes.Columns("FECHA").CellValue(bm) <> "" Then
            'sdbgPlanes_BeforeColUpdate se encarga de esto
        Else
            oMensajes.NoValido UserControl.sdbgPlanes.Columns("FECHA").caption

            actualizarYSalir = True
            Exit Function
        End If
        If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then
            If sdbgPlanes.Columns("IMPORTE").CellValue(bm) <> "" Then
                'sdbgPlanes_BeforeColUpdate se encarga de esto
            Else
                HayImporteLineaVacia = True
            End If
        Else
            If sdbgPlanes.Columns("CANT").CellValue(bm) <> "" Then
                'sdbgPlanes_BeforeColUpdate se encarga de esto
            Else
                HayCantLineaVacia = True
            End If
        End If
    Next
    If HayCantLineaVacia Then
        If oMensajes.ErrorPlanesEntregaCantidadVacia = vbNo Then
            actualizarYSalir = True
            Exit Function
        End If
    End If
    If HayImporteLineaVacia Then
        If oMensajes.ErrorPlanesEntregaImporteVacio = vbNo Then
            actualizarYSalir = True
            Exit Function
        End If
    End If
    For i = 0 To UserControl.sdbgPlanes.Rows - 1
        bm = sdbgPlanes.AddItemBookmark(i)
        
        If (((oLineaPedidoPantallaLlamada.tipoRecepcion <> 1) And (sdbgPlanes.Columns("CANT").CellValue(bm) = "")) _
            Or ((oLineaPedidoPantallaLlamada.tipoRecepcion = 1) And (sdbgPlanes.Columns("IMPORTE").CellValue(bm) = ""))) Then
            k = 1
            For Each oPlanConfigurado In oPlanesConfigurado
                If CDate(oPlanConfigurado.Fecha) = CDate(sdbgPlanes.Columns("FECHA").CellValue(bm)) Then
                    oPlanesConfigurado.Remove k
                    Exit For
                End If
                k = k + 1
            Next
        End If
    Next
    If Not DesdeSeguimiento Then
        If oPlanesConfigurado.Count > 0 Then
            oLineaPedidoPantallaLlamada.HayPlanes = True
            Set oLineaPedidoPantallaLlamada.Planes = oPlanesConfigurado
        Else
            oLineaPedidoPantallaLlamada.HayPlanes = False
            Set oLineaPedidoPantallaLlamada.Planes = oFSGSRaiz.Generar_CPlanesEntrega
        End If
    End If
    actualizarYSalir = False
End Function

''' <summary>
''' Mantener en el array aDatesAux lo se selecciona/deselecciona en la pantalla de calendario en cada momento.
''' Bucle para todas las fechas del mes pq existe la posibilidad de dar una fecha,pulsar ctrl y pulsar otra fecha,tanto para selccionar como para deseleccionar.
''' </summary>
''' <param name="NewDate">Fecha para la cual se ha cambiado la selecci�n</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub PVCalendar1_Change(ByVal NewDate As Date)
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim inum As Integer
    Dim bEncontrado As Boolean
    Dim Ultimo As Integer
    Dim Dia As Date
    If Month(NewDate) <> Month(dDateCur) Then
        PVCalendar1.DATESelected(NewDate) = False
        For i = 0 To UBound(aDatesAux)
            If Month(NewDate) = Month(aDatesAux(i)) And Not IsEmpty(aDatesAux(i)) Then
                PVCalendar1.DATESelected(aDatesAux(i)) = True
            End If
        Next
    Else
        inum = UBound(aDatesAux)
        Ultimo = Day(DateSerial(Year(NewDate), Month(NewDate) + 1, 0))
        For k = 1 To Ultimo
            bEncontrado = False
            Dia = DateSerial(Year(NewDate), Month(NewDate), k)
            For i = 0 To UBound(aDatesAux)
                If aDatesAux(i) = Dia Then
                    bEncontrado = True
                    j = i
                    Exit For
                End If
            Next
            If PVCalendar1.DATESelected(Dia) Then
                'Insertar
                If Not bEncontrado Then
                    ReDim Preserve aDatesAux(UBound(aDatesAux) + 1)
                    aDatesAux(UBound(aDatesAux)) = Dia
                    inum = UBound(aDatesAux)
                End If
            Else
                'Quitar
                If bEncontrado Then
                    For i = j To UBound(aDatesAux) - 1
                        aDatesAux(i) = aDatesAux(i + 1)
                    Next
                    inum = inum - 1
                    ReDim Preserve aDatesAux(inum)
                End If
            End If
        Next
    End If
    dDateCur = NewDate
End Sub

''' <summary>
''' Mantener en el array aDatesAux lo se selecciona/deselecciona en la pantalla de calendario en cada momento.
''' </summary>
''' <param name="Button">Button izq/central/der pulsado</param>
''' <param name="Shift">Tecla Shift pulsada o no</param>
''' <param name="X">Coordenada X donde se cliqueo</param>
''' <param name="Y">Coordenada X donde se cliqueo</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub PVCalendar1_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    PVCalendar1_Change dDateCur
End Sub

''' <summary>
''' Posicionar el bookmark en la fila actual despues de eliminar
''' </summary>
''' <param name="RtnDispErrMsg">Si hay error o no</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgPlanes_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgPlanes.Rows = 0) Then
        Exit Sub
    End If
    If sdbgPlanes.Visible Then sdbgPlanes.SetFocus
    sdbgPlanes.Bookmark = sdbgPlanes.RowBookmark(sdbgPlanes.Row)
End Sub

''' <summary>
''' Si no hay error, volver a la situacion normal
''' </summary>
''' <param name="RtnDispErrMsg">Si hay error o no</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgPlanes_AfterInsert(RtnDispErrMsg As Integer)
    Dim Coste As Double, SumCoste As Double
    Dim Dto As Double, SumDto As Double
    Dim importe As Double
    Dim NumLineasConCant As Integer
    Dim i As Integer, k As Integer
    Dim sCoste As String, sDto As String, sImporte As String, sCadena As String
    Dim bm As Variant
    Dim FechaTocada As Variant
    Dim oPlanConfigurado As CPlanEntrega
    RtnDispErrMsg = 0
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    If m_bRespetarInsertBF Then
        m_bRespetarInsertBF = False
        If Not DesdeSeguimiento Then
            NumLineasConCant = oPlanesConfigurado.Count
            SumCoste = 0
            SumDto = 0
            Screen.MousePointer = vbHourglass
            LockWindowUpdate UserControl.hWnd
            bm = sdbgPlanes.RowBookmark(sdbgPlanes.Row)
            UserControl.sdbgPlanes.MoveFirst
            For i = 0 To sdbgPlanes.Rows - 1
                If i = oPlanesConfigurado.Count - 1 Then
                    Coste = oLineaPedidoPantallaLlamada.ImporteCostes - SumCoste
                    Coste = Round(Coste, 2)
                    Dto = oLineaPedidoPantallaLlamada.ImporteDescuentos - SumDto
                    Dto = Round(Dto, 2)
                Else
                    Coste = oLineaPedidoPantallaLlamada.ImporteCostes / NumLineasConCant
                    Coste = Round(Coste, 2)
                    SumCoste = SumCoste + Coste
                    Dto = oLineaPedidoPantallaLlamada.ImporteDescuentos / NumLineasConCant
                    Dto = Round(Dto, 2)
                    SumDto = SumDto + Dto
                End If
                If oLineaPedidoPantallaLlamada.tipoRecepcion <> 1 Then
                    importe = (StrToDbl0(UserControl.sdbgPlanes.Columns("CANTIDAD").Value) * oLineaPedidoPantallaLlamada.PrecioUP) + Coste - Dto
                Else
                    importe = UserControl.sdbgPlanes.Columns("IMPORTE").Value
                End If
                UserControl.sdbgPlanes.Columns("COSTES").Value = CStr(Coste)
                UserControl.sdbgPlanes.Columns("DESCUENTOS").Value = CStr(Dto)
                UserControl.sdbgPlanes.Columns("IMPORTE").Value = CStr(importe)
                UserControl.sdbgPlanes.MoveNext
            Next
            sdbgPlanes.Bookmark = bm
            sdbgPlanes.col = 0
            sdbgPlanes.Refresh
            LockWindowUpdate 0&
            Screen.MousePointer = vbNormal
            DoEvents
        End If
        Exit Sub
    End If
    If m_bRespetarInsert Then Exit Sub
    bm = UserControl.sdbgPlanes.GetBookmark(0)
    If IsDate(sdbgPlanes.Columns("FECHA").Value) Then
        FechaTocada = CDate(sdbgPlanes.Columns("FECHA").CellText(bm))
    End If
    If Not DesdeSeguimiento Then
        Screen.MousePointer = vbHourglass
        LockWindowUpdate UserControl.hWnd
        k = 0
        For Each oPlanConfigurado In oPlanesConfigurado
            If oPlanConfigurado.Id < k Then
                k = oPlanConfigurado.Id
            End If
        Next
        k = k - 1
        SumCoste = 0
        SumDto = 0
        NumLineasConCant = oPlanesConfigurado.Count
        sdbgPlanes.RemoveAll
        m_bRespetarInsert = True
        For Each oPlanConfigurado In oPlanesConfigurado
            If Not DesdeSeguimiento Then
                If i = oPlanesConfigurado.Count Then
                    Coste = oLineaPedidoPantallaLlamada.ImporteCostes - SumCoste
                    Coste = Round(Coste, 2)
                    Dto = oLineaPedidoPantallaLlamada.ImporteDescuentos - SumDto
                    Dto = Round(Dto, 2)
                Else
                    Coste = oLineaPedidoPantallaLlamada.ImporteCostes / NumLineasConCant
                    Coste = Round(Coste, 2)
                    SumCoste = SumCoste + Coste
                    Dto = oLineaPedidoPantallaLlamada.ImporteDescuentos / NumLineasConCant
                    Dto = Round(Dto, 2)
                    SumDto = SumDto + Dto
                End If
                If oLineaPedidoPantallaLlamada.tipoRecepcion <> 1 Then
                    importe = (StrToDbl0(oPlanConfigurado.Cantidad) * oLineaPedidoPantallaLlamada.PrecioUP) + Coste - Dto
                Else
                    importe = StrToDbl0(oPlanConfigurado.importe)
                End If
                sCoste = CStr(Coste)
                sDto = CStr(Dto)
                sImporte = CStr(importe)
            End If
            sCadena = oPlanConfigurado.Fecha & Chr(m_lSeparador) & oPlanConfigurado.Cantidad
            sCadena = sCadena & Chr(m_lSeparador) & oLineaPedidoPantallaLlamada.UnidadPedido & Chr(m_lSeparador) & oPlanConfigurado.albaran
            sCadena = sCadena & Chr(m_lSeparador) & oPlanConfigurado.Id & Chr(m_lSeparador) & oLineaPedidoPantallaLlamada.PrecioUP
            sCadena = sCadena & Chr(m_lSeparador) & sCoste & Chr(m_lSeparador) & sDto & Chr(m_lSeparador) & sImporte
            UserControl.sdbgPlanes.AddItem sCadena
            k = k - 1
        Next
        UserControl.sdbgPlanes.MoveFirst
        For i = 0 To sdbgPlanes.Rows - 1
            bm = sdbgPlanes.GetBookmark(i)
            If Not IsDate(sdbgPlanes.Columns("FECHA").CellText(bm)) Then
                sdbgPlanes.Bookmark = bm
                Exit For
            Else
                If FechaTocada = CDate(sdbgPlanes.Columns("FECHA").CellText(bm)) Then
                    sdbgPlanes.Bookmark = bm
                    Exit For
                End If
            End If
        Next
        LockWindowUpdate 0&
        Screen.MousePointer = vbNormal
        If sdbgPlanes.Visible Then sdbgPlanes.SetFocus
        sdbgPlanes.col = 0
        sdbgPlanes.Refresh
        DoEvents
        m_bRespetarInsert = False
    End If
End Sub

''' <summary>
''' Si no hay error, volver a la situacion normal
''' </summary>
''' <param name="RtnDispErrMsg">Si hay error o no</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgPlanes_AfterUpdate(RtnDispErrMsg As Integer)
    Dim bmUpd As Variant
    RtnDispErrMsg = 0
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    bmUpd = UserControl.sdbgPlanes.GetBookmark(0)
    If ((Not DesdeSeguimiento) And (oLineaPedidoPantallaLlamada.tipoRecepcion <> 1)) Then
        UserControl.sdbgPlanes.Columns("IMPORTE").Value = (StrToDbl0(sdbgPlanes.Columns("CANT").CellValue(bmUpd)) * oLineaPedidoPantallaLlamada.PrecioUP)
        UserControl.sdbgPlanes.Columns("IMPORTE").Value = UserControl.sdbgPlanes.Columns("IMPORTE").Value + StrToDbl0(sdbgPlanes.Columns("COSTES").CellValue(bmUpd))
        UserControl.sdbgPlanes.Columns("IMPORTE").Value = UserControl.sdbgPlanes.Columns("IMPORTE").Value - StrToDbl0(sdbgPlanes.Columns("DESCUENTOS").CellValue(bmUpd))
    End If
End Sub

''' <summary>
''' Controlar q no se puedan meter fechas anteriores a la de emisi�n,
''' ni mas cantidad q la pedida en la linea de pedido ni m�s importe para los de recepci�n por importe
''' </summary>
''' <param name="ColIndex">Indice de la columna cambiada</param>
''' <param name="OldValue">Valor anterior</param>
''' <param name="Cancel">Para deshacer el cambio</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub sdbgPlanes_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim oPlanConfigurado As CPlanEntrega
    Dim iNumDecimales As Integer
    bValErrorFechaDesdeCantidad = False
    Cancel = False
    If sdbgPlanes.Columns(ColIndex).Name = "FECHA" Then
        If Not IsDate(sdbgPlanes.Columns("FECHA").Value) Then
            If Not (sdbgPlanes.Columns("FECHA").Value = "") Then
                oMensajes.NoValido UserControl.sdbgPlanes.Columns("FECHA").caption
                Cancel = True
            End If
            Exit Sub
        End If
        Cancel = (m_dtFechaDeEmisionLinea > CDate(UserControl.sdbgPlanes.Columns(ColIndex).Value))
        If Cancel Then
            oMensajes.ErrorPlanesEntrega (0)
            If IsDate(OldValue) Then
                For Each oPlanConfigurado In oPlanesConfigurado
                    If IsDate(oPlanConfigurado.Fecha) Then
                        If CDate(oPlanConfigurado.Fecha) = CDate(OldValue) Then
                            oPlanConfigurado.Fecha = ""
                            Exit For
                        End If
                    End If
                Next
            End If
            Exit Sub
        End If
        'Si ya esta la nueva fecha ....
         For Each oPlanConfigurado In oPlanesConfigurado
            If oPlanConfigurado.Fecha = CDate(sdbgPlanes.Columns("FECHA").Value) Then
                oMensajes.ErrorPlanesEntrega (2)
                Cancel = True
                
                m_bCancelPorAnnoIncorrecto = True
                
                Exit Sub
            End If
        Next
        If DesdeSeguimiento Then
            Dim sMensaje As String
            Dim oImput As Cimputacion
            Dim sDenPartida As String
            Dim sCodPadre As String
            If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
                For Each oImput In oLineaPedidoPantallaLlamada.Imputaciones
                    If Not g_oParametrosSM Is Nothing Then
                        If g_oParametrosSM.Item(oImput.PRES5).PluriAnual Then
                            If Not oImput.ExistePartidaPlurianual(Year(sdbgPlanes.Columns("FECHA").Value)) Then
                                Select Case g_oParametrosSM.Item(oImput.PRES5).impnivel
                                Case 2
                                    sCodPadre = oImput.Pres1
                                Case 3
                                    sCodPadre = oImput.Pres1 & "-" & oImput.Pres2
                                Case 4
                                    sCodPadre = oImput.Pres1 & "-" & oImput.Pres2 & "-" & oImput.Pres3
                                End Select
                                If oImput.CodContrato <> "" Then
                                    sDenPartida = oImput.CodContrato & " - " & oImput.PresDen & IIf(sCodPadre = "", "", " (" & sCodPadre & ")")
                                Else
                                    sDenPartida = oImput.PresDen & IIf(sCodPadre = "", "", " (" & sCodPadre & ")")
                                End If
                                sMensaje = Replace(Replace(Replace(m_sSinAnyoImput, "@@@", oLineaPedidoPantallaLlamada.Num), "@ANYO@", Year(sdbgPlanes.Columns("FECHA").Value)), "@PARTIDA@", sDenPartida)
                                oMensajes.SeguimientoPedido_PartidaPlurianualIncorrecta sMensaje
                                                                                                
                                Cancel = True
                                
                                m_bCancelPorAnnoIncorrecto = True
                                
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
        End If
        m_dtFechaAModificar = CDate(sdbgPlanes.Columns("FECHA").Value)
        If OldValue = "" Then Exit Sub
        For Each oPlanConfigurado In oPlanesConfigurado
            If IsDate(oPlanConfigurado.Fecha) Then
                If CDate(oPlanConfigurado.Fecha) = CDate(OldValue) Then
                    m_dtFechaAModificar = CDate(oPlanConfigurado.Fecha)
                    oPlanConfigurado.Fecha = sdbgPlanes.Columns("FECHA").Value
                    Exit For
                End If
            End If
        Next
    ElseIf UserControl.sdbgPlanes.Columns(ColIndex).Name = "CANT" Then
        If Not (UserControl.sdbgPlanes.Columns("CANT").Value = "") Then
            If Not IsNumeric(UserControl.sdbgPlanes.Columns("CANT").Value) Then
                oMensajes.NoValido UserControl.sdbgPlanes.Columns("CANT").caption
                Cancel = True
            Else
                iNumDecimales = ValidarNumeroMaximoDecimales(sdbgPlanes.Columns("UP").Value, sdbgPlanes.Columns("CANT").Value)
                If iNumDecimales <> -1 Then
                    oMensajes.MensajeMaximoDecimales 1125, iNumDecimales, sdbgPlanes.Columns("UP").Value
                    If sdbgPlanes.Visible Then UserControl.sdbgPlanes.SetFocus
                    UserControl.sdbgPlanes.col = 1
                    Cancel = True
                    Exit Sub
                Else
                    Cancel = CancelarActualizar(False, OldValue, UserControl.sdbgPlanes.Columns(ColIndex).Value)
                    If Cancel = True Then
                        If OldValue <> "" Then m_dCantLineas = m_dCantLineas + OldValue
                        m_dCantLineas = m_dCantLineas - UserControl.sdbgPlanes.Columns(ColIndex).Value
                    End If
                End If
            End If
        Else
            If OldValue <> "" Then m_dCantLineas = m_dCantLineas - OldValue
        End If
        If Not IsDate(sdbgPlanes.Columns("FECHA").Value) Then
            oMensajes.NoValido sdbgPlanes.Columns("FECHA").caption
            UserControl.sdbgPlanes.col = 0
            bValErrorFechaDesdeCantidad = True
            If UserControl.sdbgPlanes.IsAddRow Then
                m_bRespetarInsertBF = True
            End If
        ElseIf Cancel = False Then
            For Each oPlanConfigurado In oPlanesConfigurado
                If IsDate(oPlanConfigurado.Fecha) Then
                    If CDate(oPlanConfigurado.Fecha) = CDate(sdbgPlanes.Columns("FECHA").Value) Then
                        oPlanConfigurado.Cantidad = sdbgPlanes.Columns("CANT").Value
                        Exit For
                    End If
                End If
            Next
        End If
    ElseIf UserControl.sdbgPlanes.Columns(ColIndex).Name = "IMPORTE" Then
        If OldValue <> "" Then
            OldValue = OldValue / dblEquiv
        End If
        Dim dImporte As Double
        If sdbgPlanes.Columns("IMPORTE").Value <> "" Then
            dImporte = CInt(sdbgPlanes.Columns("IMPORTE").Value) / dblEquiv
        End If
        If Not (UserControl.sdbgPlanes.Columns("IMPORTE").Value = "") Then
            If Not IsNumeric(UserControl.sdbgPlanes.Columns("IMPORTE").Value) Then
                oMensajes.NoValido UserControl.sdbgPlanes.Columns("IMPORTE").caption
                Cancel = True
            Else
                iNumDecimales = ValidarNumeroMaximoDecimales(sdbgPlanes.Columns("UP").Value, sdbgPlanes.Columns("IMPORTE").Value)
                If iNumDecimales <> -1 Then
                    oMensajes.MensajeMaximoDecimales 1125, iNumDecimales, sdbgPlanes.Columns("UP").Value
                    If sdbgPlanes.Visible Then sdbgPlanes.SetFocus
                    UserControl.sdbgPlanes.col = 1
                    Cancel = True
                    Exit Sub
                Else
                    Cancel = CancelarActualizar(False, OldValue, dImporte)
                    If Cancel = True Then
                        If OldValue <> "" Then m_dImpLineas = m_dImpLineas + OldValue
                        
                        m_dImpLineas = m_dImpLineas - dImporte
                    End If
                End If
            End If
        Else
            If OldValue <> "" Then m_dImpLineas = m_dImpLineas - OldValue
        End If
        If Not IsDate(sdbgPlanes.Columns("FECHA").Value) Then
            oMensajes.NoValido sdbgPlanes.Columns("FECHA").caption
            sdbgPlanes.col = 0
            bValErrorFechaDesdeCantidad = True
            If UserControl.sdbgPlanes.IsAddRow Then
                m_bRespetarInsertBF = True
            End If
        ElseIf Cancel = False Then
            For Each oPlanConfigurado In oPlanesConfigurado
                If IsDate(oPlanConfigurado.Fecha) Then
                    If CDate(oPlanConfigurado.Fecha) = CDate(sdbgPlanes.Columns("FECHA").Value) Then
                        oPlanConfigurado.importe = dImporte
                        Exit For
                    End If
                End If
            Next
        End If
    End If
End Sub

''' <summary>
''' No Confirmacion antes de eliminar
''' </summary>
''' <param name="Cancel">Si se cancela o no el borrado</param>
''' <param name="DispPromptMsg">si se`pregunat antes de eliminar</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgPlanes_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

''' <summary>
''' Guarda los cambios producidos el grid
''' </summary>
''' <param name="Cancel">Si se cancela o no el update</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgPlanes_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim oPlanConfigurado As CPlanEntrega
    Dim oPlanConfigurado2 As CPlanEntrega
    Dim oPlanConfigurado3 As CPlanEntrega
    Dim bEncontrado As Boolean
    Dim bEsInsert As Boolean
    Dim bRefrescarPantalla As Boolean
    Dim i As Integer
    Dim k As Integer
    Dim aIdentificadores(1) As Variant
    Dim bEsInsertTodos As Boolean
    bValError = False
    Cancel = False
    If UserControl.sdbgPlanes.IsAddRow Then
        Set oPlanConfigurado = oFSGSRaiz.Generar_CPlanEntrega
    ElseIf (UserControl.sdbgPlanes.Columns("ID").Value = "") Then
        bEncontrado = False
        If IsDate(sdbgPlanes.Columns("FECHA").Value) Then
            For Each oPlanConfigurado In oPlanesConfigurado
                If IsDate(oPlanConfigurado.Fecha) Then
                    If CDate(oPlanConfigurado.Fecha) = CDate(sdbgPlanes.Columns("FECHA").Value) Then
                        bEncontrado = True
                        Exit For
                    End If
                End If
            Next
        End If
        If bEncontrado = False Then
            Set oPlanConfigurado = oFSGSRaiz.Generar_CPlanEntrega
        End If
    Else
        Set oPlanConfigurado = oPlanesConfigurado.Item(UserControl.sdbgPlanes.Columns("ID").Value)
        If oPlanConfigurado Is Nothing Then
            For Each oPlanConfigurado In oPlanesConfigurado
                If oPlanConfigurado.Id = CLng(UserControl.sdbgPlanes.Columns("ID").Value) Then
                    Exit For
                End If
            Next
        End If
        bEsInsertTodos = False
        If IsDate(sdbgPlanes.Columns("FECHA").Value) Then
            m_dtFechaAModificar = CDate(sdbgPlanes.Columns("FECHA").Value)
        Else
            m_dtFechaAModificar = ""
        End If
        If DesdeSeguimiento And (chkAplicarTodos.Value = vbChecked) Then
            If IsDate(oPlanConfigurado.Fecha) Then
                If Not (CDate(oPlanConfigurado.Fecha) = m_dtFechaAModificar) Then
                    bEsInsertTodos = True
                
                    aIdentificadores(1) = m_dtFechaAModificar
                    frmSeguimiento.EliminarPlanesDeLineaPedido aIdentificadores
                End If
            End If
        End If
        bEncontrado = True
    End If
    Set oPlanConfigurado.OrdenEntrega = g_oOrdenEntrega
    oPlanConfigurado.Fecha = sdbgPlanes.Columns("FECHA").Value
    oPlanConfigurado.Cantidad = sdbgPlanes.Columns("CANT").Value
    oPlanConfigurado.albaran = sdbgPlanes.Columns("ALBARAN").Value
    oPlanConfigurado.idLineaPedido = oLineaPedidoPantallaLlamada.Id
    oPlanConfigurado.IDOrdenEntrega = oLineaPedidoPantallaLlamada.Orden
    If (UserControl.sdbgPlanes.Columns("IMPORTE").Value <> "") Then
        oPlanConfigurado.importe = CInt(UserControl.sdbgPlanes.Columns("IMPORTE").Value) / dblEquiv
    Else
        oPlanConfigurado.importe = UserControl.sdbgPlanes.Columns("IMPORTE").Value
    End If
    Set oPlanConfigurado.LineaPedido = oLineaPedidoPantallaLlamada
    Set oIBaseDatos = oPlanConfigurado
    i = 0
    k = 0
    For Each oPlanConfigurado2 In oPlanesConfigurado
        If oLineaPedidoPantallaLlamada.tipoRecepcion <> 1 Then
            If oPlanConfigurado2.Cantidad <> "" Then
                i = i + 1
            End If
        Else
            If oPlanConfigurado2.importe <> "" Then
                i = i + 1
            End If
        End If
        If oPlanConfigurado2.Id < k Then k = oPlanConfigurado2.Id
    Next
    k = k - 1
    If (((oLineaPedidoPantallaLlamada.tipoRecepcion = 0) And (oPlanConfigurado.Cantidad <> "")) _
    Or ((oLineaPedidoPantallaLlamada.tipoRecepcion = 1) And (oPlanConfigurado.importe <> ""))) Then
        bEsInsert = UserControl.sdbgPlanes.IsAddRow
        If (UserControl.sdbgPlanes.Columns("ID").Value = "") Then
            bEsInsert = True
        Else
            bEsInsert = bEsInsert Or (CInt(UserControl.sdbgPlanes.Columns("ID").Value) <= 0)
        End If
        If bEsInsert Then
            If DesdeSeguimiento Then
                If IsDate(oPlanConfigurado.Fecha) Then
                    If IsDate(oLineaPedidoPantallaLlamada.FechaEntrega) Then
                        'Quita Fecha Entrega. Metes Plan
                        oLineaPedidoPantallaLlamada.Planes_Comprometido_Operar True, True, False
                    Else
                        oLineaPedidoPantallaLlamada.Planes_Comprometido_Operar True, False, True
                    End If
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    oLineaPedidoPantallaLlamada.Planes_Comprometido_Operar False, False, True
                End If
            End If
            bRefrescarPantalla = (i = 1)
         Else
            If DesdeSeguimiento Then
                If IsDate(oPlanConfigurado.Fecha) Then
                    oLineaPedidoPantallaLlamada.Planes_Comprometido_Operar True, False, True
                    teserror = oIBaseDatos.FinalizarEdicionModificando
                    oLineaPedidoPantallaLlamada.Planes_Comprometido_Operar False, False, True
                End If
             End If
         End If
         If DesdeSeguimiento Then
             If teserror.NumError <> TESnoerror Then
                 TratarError teserror
                 Cancel = True
                 GoTo Salir
             End If
         End If
        If UserControl.sdbgPlanes.IsAddRow Or (bEncontrado = False) Then
            If Not DesdeSeguimiento Then
                oPlanConfigurado.Id = k
            End If
            If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then
                oPlanesConfigurado.Add oPlanConfigurado.Id, oLineaPedidoPantallaLlamada.Id, oLineaPedidoPantallaLlamada.Orden _
                , oPlanConfigurado.Fecha, oPlanConfigurado.Cantidad, oPlanConfigurado.albaran, , oPlanConfigurado.importe
            Else
                oPlanesConfigurado.Add oPlanConfigurado.Id, oLineaPedidoPantallaLlamada.Id, oLineaPedidoPantallaLlamada.Orden _
                , oPlanConfigurado.Fecha, oPlanConfigurado.Cantidad, oPlanConfigurado.albaran
            End If
        End If
        UserControl.sdbgPlanes.Columns("ID").Value = CStr(oPlanConfigurado.Id)
        oLineaPedidoPantallaLlamada.HayPlanes = True
        oLineaPedidoPantallaLlamada.FechaEntrega = Null
        oLineaPedidoPantallaLlamada.Obligat = False
    ElseIf (UserControl.sdbgPlanes.Columns("ID").Value <> "") Then
        If DesdeSeguimiento Then
            Set oLineaPedidoPantallaLlamada.ObjetoOrden = g_oOrdenEntrega
            oLineaPedidoPantallaLlamada.LimpiarPlanesEntrega UserControl.sdbgPlanes.Columns("ID").Value
        End If
        UserControl.sdbgPlanes.Columns("ID").Value = ""
    ElseIf Not DesdeSeguimiento Then
        If UserControl.sdbgPlanes.IsAddRow Or (bEncontrado = False) Then
            oPlanConfigurado.Id = k
            If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then
                oPlanesConfigurado.Add oPlanConfigurado.Id, oLineaPedidoPantallaLlamada.Id, oLineaPedidoPantallaLlamada.Orden, oPlanConfigurado.Fecha, oPlanConfigurado.Cantidad, oPlanConfigurado.albaran, , oPlanConfigurado.importe
            Else
                oPlanesConfigurado.Add oPlanConfigurado.Id, oLineaPedidoPantallaLlamada.Id, oLineaPedidoPantallaLlamada.Orden, oPlanConfigurado.Fecha, oPlanConfigurado.Cantidad, oPlanConfigurado.albaran
            End If
            UserControl.sdbgPlanes.Columns("ID").Value = CStr(oPlanConfigurado.Id)
        End If
    End If
    If (UserControl.chkAplicarTodos.Value = vbChecked) Then
        If DesdeSeguimiento Then
            Dim aFechas() As Variant
            ReDim aFechas(0)
            If oLineaPedidoPantallaLlamada.Planes.Count > 0 Then
                For i = 1 To oLineaPedidoPantallaLlamada.Planes.Count
                    ReDim Preserve aFechas(UBound(aFechas) + 1)
                    aFechas(i) = oLineaPedidoPantallaLlamada.Planes.Item(i).Fecha
                Next
                frmSeguimiento.EliminarPlanesDeLineaPedido (aFechas)
            End If
            Set oPlanConfigurado3 = oFSGSRaiz.Generar_CPlanEntrega
            For Each oPlanConfigurado3 In oPlanesConfigurado
                If oLineaPedidoPantallaLlamada.tipoRecepcion = 0 Then
                    frmSeguimiento.AplicarUnPlanConfigurado oPlanConfigurado3, oLineaPedidoPantallaLlamada.CantidadPedida, bEsInsert Or bEsInsertTodos, bRefrescarPantalla
                Else
                    frmSeguimiento.AplicarUnPlanConfigurado oPlanConfigurado3, , bEsInsert Or bEsInsertTodos, bRefrescarPantalla, oLineaPedidoPantallaLlamada.importePedido, True
                End If
            Next
        End If
    End If
Salir:
    bValError = Cancel
    If sdbgPlanes.Visible Then sdbgPlanes.SetFocus
End Sub

''' <summary>
''' Abre el calendario para q se puedan selccionar fechas
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgPlanes_BtnClick()
    Dim Ultimo As Integer
    Dim k As Integer
    Dim Dia As Date
    bValError = False
    bValErrorFechaDesdeCantidad = False
    If UserControl.sdbgPlanes.DataChanged = True Then sdbgPlanes.Update
    If bValError = True Or bValErrorFechaDesdeCantidad = True Then Exit Sub
    If UserControl.sdbgPlanes.Columns("ALBARAN").Value <> "" Then Exit Sub
    m_dtFechaAModificar = Null
    If UserControl.sdbgPlanes.IsAddRow Or (UserControl.sdbgPlanes.Columns("FECHA").Value = "") Then
        PVCalendar1.Value = Date
        PVCalendar1.DATESelected(Date) = False
        dDateCur = Date
        PVCalendar1.SelectMode = 2
    Else
        m_dtFechaAModificar = CDate(UserControl.sdbgPlanes.Columns("FECHA").Value)
        PVCalendar1.Value = m_dtFechaAModificar
        dDateCur = m_dtFechaAModificar
        PVCalendar1.SelectMode = 1
    End If
    Ultimo = Day(DateSerial(Year(dDateCur), Month(dDateCur) + 1, 0))
    For k = 1 To Ultimo
        Dia = DateSerial(Year(dDateCur), Month(dDateCur), k)
        PVCalendar1.DATESelected(Dia) = False
    Next
    ReDim aDatesAux(0)
    If UserControl.sdbgPlanes.IsAddRow Or (UserControl.sdbgPlanes.Columns("FECHA").Value = "") Then
    Else
        PVCalendar1.DATESelected(m_dtFechaAModificar) = True
    End If
    fraCalendar.Visible = True
    fraCalendar.ZOrder
End Sub

''' <summary>
''' Evento que salta al producirse un cambio en el grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgPlanes_Change()
    Dim teserror As TipoErrorSummit
    Dim oPlanConfigurado As CPlanEntrega
    If cmdDeshacer.Enabled = False Then
        cmdDeshacer.Enabled = True
    End If
    If DesdeSeguimiento Then
        If Not (UserControl.sdbgPlanes.Columns("ID").Value = "") Then
            If (CInt(UserControl.sdbgPlanes.Columns("ID").Value) > -1) Then
                Set oPlanConfigurado = oPlanesConfigurado.Item(UserControl.sdbgPlanes.Columns("ID").Value)
                If oPlanConfigurado Is Nothing Then
                    For Each oPlanConfigurado In oPlanesConfigurado
                        If oPlanConfigurado.Id = CLng(UserControl.sdbgPlanes.Columns("ID").Value) Then Exit For
                    Next
                End If
                If oPlanConfigurado Is Nothing Then Exit Sub
                
                If m_bCancelPorAnnoIncorrecto Then
                    m_bCancelPorAnnoIncorrecto = False
                    sdbgPlanes.DataChanged = False
                    cmdDeshacer.Enabled = False
                    Exit Sub
                End If
                
                ''mirar en bd q no lo hayan tocado
                Set oIBaseDatos = oPlanConfigurado
                teserror = oIBaseDatos.IniciarEdicion
                If (teserror.NumError = TESDatoEliminado) Or (teserror.NumError = TESInfModificada) Then
                    TratarError teserror
                    sdbgPlanes.DataChanged = False
                    Exit Sub
                End If
            End If
        End If
    End If
    If UserControl.sdbgPlanes.Columns("UP").Value = "" Then
        UserControl.sdbgPlanes.Columns("UP").Value = oLineaPedidoPantallaLlamada.UnidadPedido
        UserControl.sdbgPlanes.Columns("PRECIO").Value = oLineaPedidoPantallaLlamada.PrecioUP
    End If
End Sub

''' <summary>
''' Captura la tecla Supr (Delete) y cuando �sta es presionada se lanza el evento Click asociado al bot�n eliminar para borrar de la BD y de la grid las lineas seleccionadas.
''' </summary>
''' <param name="KeyCode">Contiene el c�digo interno de la tecla pulsada.</param>
''' <param name="Shift">Es la m�scara, sin uso en esta subrutina.</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgPlanes_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        If sdbgPlanes.SelBookmarks.Count > 0 Then
            cmdEliminar_Click
        End If
    End If
End Sub


''' <summary>
''' Evento al moverse de columna o de fila en la grid
''' </summary>
''' <param name="lastRow">Fila desde la que se mueve</param>
''' <param name="LastCol">Columna desde la que se mueve</param>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub sdbgPlanes_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If Not SoloConsulta Then
        If DesdeSeguimiento Then
            If UserControl.sdbgPlanes.Columns("ALBARAN").Value <> "" Then
                UserControl.sdbgPlanes.Columns("FECHA").Locked = True
                UserControl.sdbgPlanes.Columns("CANT").Locked = True
                UserControl.sdbgPlanes.Columns("IMPORTE").Locked = True
                UserControl.cmdEliminar.Enabled = False
            Else
                UserControl.sdbgPlanes.Columns("FECHA").Locked = False
                UserControl.sdbgPlanes.Columns("CANT").Locked = False
                If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then UserControl.sdbgPlanes.Columns("IMPORTE").Locked = False
                UserControl.cmdEliminar.Enabled = True
            End If
        Else
            UserControl.sdbgPlanes.Columns("FECHA").Locked = False
            UserControl.sdbgPlanes.Columns("CANT").Locked = False
            If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then UserControl.sdbgPlanes.Columns("IMPORTE").Locked = False
            UserControl.cmdEliminar.Enabled = True
        End If
    Else
        UserControl.sdbgPlanes.Columns("FECHA").Locked = True
        UserControl.sdbgPlanes.Columns("FECHA").Style = ssStyleEdit
        UserControl.sdbgPlanes.Columns("IMPORTE").Locked = True
        UserControl.sdbgPlanes.Columns("CANT").Locked = True
    End If
End Sub

''' <summary>
''' ORDENACION POR BURBUJA ("buble sort") MEJORADO de un array unidimensional, de menor a mayor.
''' </summary>
''' <remarks>Llamada desde: cmdAceptarFecha_Click ; Tiempo m�ximo: 0</remarks>
Private Sub Orden()
    Dim iMin    As Long
    Dim iMax    As Long
    Dim pos     As Long
    Dim i       As Long
    Dim j       As Long
    Dim bEncontrado As Boolean
    Dim aux As Variant
    For i = 1 To UBound(aDatesAux)
        bEncontrado = False
        For j = 0 To UBound(aDates_Ins)
            If Not (aDates_Ins(j)) = "" Then
                If CDate(aDatesAux(i)) = CDate(aDates_Ins(j)) Then
                    bEncontrado = True
                    Exit For
                End If
            End If
        Next
        If Not bEncontrado Then
            ReDim Preserve aDates_Ins(UBound(aDates_Ins) + 1)
            aDates_Ins(UBound(aDates_Ins)) = CStr(aDatesAux(i))
            ReDim Preserve aCantidades_Ins(UBound(aCantidades_Ins) + 1)
            aCantidades_Ins(UBound(aCantidades_Ins)) = ""
            ReDim Preserve aImporte_Ins(UBound(aImporte_Ins) + 1)
            aImporte_Ins(UBound(aImporte_Ins)) = ""
            ReDim Preserve aAlbaran_Ins(UBound(aAlbaran_Ins) + 1)
            aAlbaran_Ins(UBound(aAlbaran_Ins)) = ""
            ReDim Preserve aId_Ins(UBound(aId_Ins) + 1)
            aId_Ins(UBound(aId_Ins)) = ""
        End If
    Next
    iMin = LBound(aDates_Ins)
    iMax = UBound(aDates_Ins)
    While iMax > iMin
        pos = iMin
        For i = iMin To iMax - 1
            If Not (aDates_Ins(i)) = "" And Not (aDates_Ins(i + 1)) = "" Then
                If CDate(aDates_Ins(i)) > CDate(aDates_Ins(i + 1)) Then
                    aux = aDates_Ins(i + 1)
                    aDates_Ins(i + 1) = aDates_Ins(i)
                    aDates_Ins(i) = aux
                    aux = aCantidades_Ins(i + 1)
                    aCantidades_Ins(i + 1) = aCantidades_Ins(i)
                    aCantidades_Ins(i) = aux
                    aux = aImporte_Ins(i + 1)
                    aImporte_Ins(i + 1) = aImporte_Ins(i)
                    aImporte_Ins(i) = aux
                    aux = aAlbaran_Ins(i + 1)
                    aAlbaran_Ins(i + 1) = aAlbaran_Ins(i)
                    aAlbaran_Ins(i) = aux
                    aux = aId_Ins(i + 1)
                    aId_Ins(i + 1) = aId_Ins(i)
                    aId_Ins(i) = aux
                    pos = i
                End If
            ElseIf (aDates_Ins(i)) = "" And Not IsEmpty(aDates_Ins(i)) And Not (aDates_Ins(i + 1)) = "" Then
                aux = aDates_Ins(i + 1)
                aDates_Ins(i + 1) = aDates_Ins(i)
                aDates_Ins(i) = aux
                aux = aCantidades_Ins(i + 1)
                aCantidades_Ins(i + 1) = aCantidades_Ins(i)
                aCantidades_Ins(i) = aux
                aux = aImporte_Ins(i + 1)
                aImporte_Ins(i + 1) = aImporte_Ins(i)
                aImporte_Ins(i) = aux
                aux = aAlbaran_Ins(i + 1)
                aAlbaran_Ins(i + 1) = aAlbaran_Ins(i)
                aAlbaran_Ins(i) = aux
                aux = aId_Ins(i + 1)
                aId_Ins(i + 1) = aId_Ins(i)
                aId_Ins(i) = aux
                pos = i
            End If
        Next i
        iMax = pos
    Wend
    If UBound(aDates_Ins) > 0 Then
        If aDates_Ins(UBound(aDates_Ins)) = "" Then
            iMin = LBound(aId_Ins) + 1
            iMax = UBound(aId_Ins)
            For i = iMin To iMax - 1
                If aId_Ins(i) = "" Then
                    aCantidades_Ins(i) = aCantidades_Ins(UBound(aCantidades_Ins))
                    aImporte_Ins(i) = aImporte_Ins(UBound(aImporte_Ins))
                    Exit For
                End If
            Next i
            ReDim Preserve aDates_Ins(UBound(aDates_Ins) - 1)
            ReDim Preserve aCantidades_Ins(UBound(aCantidades_Ins) - 1)
            ReDim Preserve aImporte_Ins(UBound(aImporte_Ins) - 1)
            ReDim Preserve aAlbaran_Ins(UBound(aAlbaran_Ins) - 1)
            ReDim Preserve aId_Ins(UBound(aId_Ins) - 1)
        End If
    End If
End Sub

''' <summary>
''' Mantener la variable global m_dCantLineas (cantidad total en planes de entrega) para controlar q no se sobrepase la cantidad pedida de la linea de pedido
''' Mantener la variable global m_dImpLineas (importe total en planes de entrega) para controlar q no se sobrepase la cantidad pedida de la linea de pedido
''' </summary>
''' <param name="BucleCarga">En los casos de cmdDeshacer_Click y cmdEliminar_Click la edici�n puede ser de multiples lineas luego hay q usar el grid no es suficiente con OldValue y NewValue</param>
''' <param name="OldValue">Valor antes de la edici�n</param>
''' <param name="NewValue">Valor tras la edici�n</param>
''' <returns>Si pasa el control o no</returns>
''' <remarks>Llamada desde: cmdDeshacer_Click   cmdEliminar_Click     sdbgPlanes_BeforeColUpdate; Tiempo m�ximo: 0</remarks>
Private Function CancelarActualizar(ByVal BucleCarga As Boolean, ByVal OldValue As Variant, ByVal NewValue As Variant) As Boolean
Dim bm As Variant
Dim i As Long
If oLineaPedidoPantallaLlamada.tipoRecepcion <> 1 Then
    If BucleCarga Then
        m_dCantLineas = 0
        For i = 0 To UserControl.sdbgPlanes.Rows - 1
            bm = sdbgPlanes.AddItemBookmark(i)
            If sdbgPlanes.Columns("CANT").CellValue(bm) > "" Then
                m_dCantLineas = m_dCantLineas + CDbl(sdbgPlanes.Columns("CANT").CellValue(bm))
            End If
        Next
        CancelarActualizar = False
    Else
        If OldValue <> "" Then m_dCantLineas = m_dCantLineas - OldValue
        m_dCantLineas = m_dCantLineas + NewValue
        If (m_dCantLineas > oLineaPedidoPantallaLlamada.CantidadPedida) Then
            oMensajes.ErrorPlanesEntrega (1)
            CancelarActualizar = True
        Else
            CancelarActualizar = False
        End If
    End If
Else
    If BucleCarga Then
        m_dImpLineas = 0
        For i = 0 To UserControl.sdbgPlanes.Rows - 1
            bm = sdbgPlanes.AddItemBookmark(i)
            If sdbgPlanes.Columns("IMPORTE").CellValue(bm) > "" Then
                m_dImpLineas = m_dImpLineas + CDbl(sdbgPlanes.Columns("IMPORTE").CellValue(bm)) / dblEquiv
            End If
        Next
        CancelarActualizar = False
    Else
        If OldValue <> "" Then m_dImpLineas = m_dImpLineas - OldValue
        m_dImpLineas = m_dImpLineas + NewValue
        If (m_dImpLineas > oLineaPedidoPantallaLlamada.ImporteNeto) Then
            oMensajes.ErrorPlanesEntrega (3)
            CancelarActualizar = True
        Else
            CancelarActualizar = False
        End If
    End If
End If
End Function


Public Sub Initialize()
m_bUnload = False
sdbgPlanes.RemoveAll
CargarRecursos
PonerFieldSeparator
If Not oLineaPedidoPantallaLlamada Is Nothing Then
    dblEquiv = IIf(IsNull(oLineaPedidoPantallaLlamada.CambioMonOferta), 1, oLineaPedidoPantallaLlamada.CambioMonOferta)
Else
    dblEquiv = 1
End If
Set oPlanesConfigurado = oFSGSRaiz.Generar_CPlanesEntrega
ConfigurarFormulario
m_dCantLineas = 0
m_dImpLineas = 0
Screen.MousePointer = vbHourglass
ReDim aDatesAux(0)
ReDim aDates_Ins(0)
ReDim aCantidades_Ins(0)
ReDim aImporte_Ins(0)
ReDim aAlbaran_Ins(0)
ReDim aId_Ins(0)
CargarGrid
If gParametrosGenerales.giPlanRecepAutom = SiempreAutom Then
    If DesdeSeguimiento Then
        If (oPlanesConfigurado.Count > 0) And oLineaPedidoPantallaLlamada.RecepAutom Then
            chkAutom_Click
        Else
            UserControl.picAutom.Enabled = True
            m_bRespetarInsert = True
            chkAutom.Value = vbUnchecked
            m_bRespetarInsert = False
        End If
    End If
End If
If oLineaPedidoPantallaLlamada.tipoRecepcion = 1 Then
    sdbgPlanes.Columns("IMPORTE").Locked = False
End If
If Not DesdeSeguimiento Then
    m_dtFechaDeEmisionLinea = DateSerial(Year(Now), Month(Now), Day(Now))
Else
    m_dtFechaDeEmisionLinea = CDate(oLineaPedidoPantallaLlamada.FechaEmision)
End If
Screen.MousePointer = vbNormal
End Sub
Public Sub PonerFieldSeparator()
Dim sdg As Control
Dim Name4 As String
Dim Name5 As String
For Each sdg In UserControl.Controls
    Name4 = LCase(Left(sdg.Name, 4))
    Name5 = LCase(Left(sdg.Name, 5))
    If Name4 = "sdbg" Or Name4 = "sdbc" Or Name5 = "sdbdd" Or Name4 = "ssdb" Then
        If sdg.DataMode = ssDataModeAddItem Then
            sdg.FieldSeparator = Chr(m_lSeparador)
        End If
    End If
Next
End Sub

Private Sub sdbgPlanes_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
If Not oLineaPedidoPantallaLlamada Is Nothing Then
    If oLineaPedidoPantallaLlamada.Baja_LOG = 1 And Not g_bMostrandoLOG Then
        sdbgPlanes.Columns("FECHA").CellStyleSet "FondoGris"
        sdbgPlanes.Columns("CANT").CellStyleSet "FondoGris"
        sdbgPlanes.Columns("UP").CellStyleSet "FondoGris"
        sdbgPlanes.Columns("ALBARAN").CellStyleSet "FondoGris"
        Exit Sub
    End If
End If
If g_oOrdenEntrega.TipoLogPedido = Eliminar_PlanEntrega_Linea Then
    For i = 0 To sdbgPlanes.Cols
        sdbgPlanes.Columns("FECHA").CellStyleSet "Eliminado"
        sdbgPlanes.Columns("CANT").CellStyleSet "Eliminado"
        sdbgPlanes.Columns("UP").CellStyleSet "Eliminado"
        sdbgPlanes.Columns("ALBARAN").CellStyleSet "Eliminado"
    Next i
End If
End Sub

Private Sub UserControl_Resize()
    Arrange
End Sub

Public Function HayAlMenosUnplan() As Boolean
    HayAlMenosUnplan = (oPlanesConfigurado.Count > 0)
End Function

Public Function AlgoCambiasteEnPlan() As Boolean
    AlgoCambiasteEnPlan = (sdbgPlanes.DataChanged = True)
End Function
