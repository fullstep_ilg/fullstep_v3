VERSION 5.00
Begin VB.Form frmSeguimAceptacion 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DConfirmaci�n de notificaci�n a receptor del pedido"
   ClientHeight    =   2175
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7815
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSeguimAceptacion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2175
   ScaleWidth      =   7815
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   3860
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1800
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Height          =   315
      Left            =   2720
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1800
      Width           =   1005
   End
   Begin VB.TextBox txtEmail 
      Height          =   285
      Left            =   1800
      TabIndex        =   3
      Top             =   1260
      Width           =   4655
   End
   Begin VB.Label lblUsuRecep 
      BackColor       =   &H00808000&
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1800
      TabIndex        =   2
      Top             =   940
      Width           =   2775
   End
   Begin VB.Label lblReceptor 
      BackColor       =   &H00808000&
      Caption         =   "Usuario receptor:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   150
      TabIndex        =   1
      Top             =   940
      Width           =   1575
   End
   Begin VB.Label lblAceptar 
      BackColor       =   &H00808000&
      Caption         =   $"frmSeguimAceptacion.frx":0CB2
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   150
      TabIndex        =   0
      Top             =   120
      Width           =   7575
   End
End
Attribute VB_Name = "frmSeguimAceptacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sCodReceptor As String
Public g_oOrdenEntrega As COrdenEntrega

Private m_oPersona As CPersona
Private m_sIdiEmail As String

Private Sub cmdAceptar_Click()
    Dim errormail As TipoErrorSummit
    Dim sCuerpo As String
    Dim docword As Object
    Dim appword  As Object
    Dim bHTML As Boolean
    Dim sIdioma As String
    
    'Al aceptar se nos mostrar� una pantalla con el email que se va a enviar.Ser� HTML o TXT dependiendo
    'de si el usuario lo es del EP y tiene definida la opci�n HTML en opciones.
    If Trim(txtEmail.Text) = "" Then
        oMensajes.FaltanDatos (m_sIdiEmail)
        Exit Sub
    End If
     
    Screen.MousePointer = vbHourglass
    
    bHTML = SQLBinaryToBoolean(m_oPersona.TipoMail)
    
    'si el usuario receptor es un usuario del EP,coger� la plantilla,pero con el idioma del usuario en el EP
    If m_oPersona.AccesoFSEP = True And m_oPersona.idioma <> "" Then
        sIdioma = m_oPersona.idioma
    Else
        sIdioma = oGestorParametros.DevolverIdiomaUsuario(m_oPersona.Usuario)
        If sIdioma = "" Then
            sIdioma = gParametrosGenerales.gIdioma
        End If
    End If
    
    Dim oCEMailPedido As CEmailPedido
    Set oCEMailPedido = GenerarCEmailPedido(oFSGSRaiz, oMensajes, gParametrosInstalacion, gParametrosGenerales)
    If bHTML Then
        'Genera el cuerpo del mensaje en formato HTML
        sCuerpo = oCEMailPedido.GenerarMensajeAceptacionHTML(sIdioma, g_oOrdenEntrega)
    Else
        'Genera el cuerpo del mensaje en formato TXT
        sCuerpo = oCEMailPedido.GenerarMensajeAceptacionTXT(sIdioma, g_oOrdenEntrega)
    End If
    Set oCEMailPedido = Nothing
    
    Screen.MousePointer = vbNormal
    
    If oIdsMail Is Nothing Then Set oIdsMail = IniciarSesionMail

    errormail = ComponerMensaje(Trim(txtEmail.Text), gParametrosInstalacion.gsPedNotifMailSubject, sCuerpo, , "frmSeguimAceptacion", bHTML, _
                                entidadNotificacion:=entidadNotificacion.PedDirecto, _
                                tipoNotificacion:=TipoNotificacionEmail.PedAceptacion, _
                                lIdInstancia:=g_oOrdenEntrega.Id, _
                                sToName:=m_oPersona.nombre & " " & m_oPersona.Apellidos)
    If errormail.NumError <> TESnoerror Then
        basErrores.TratarError errormail
    End If
    
    FinalizarSesionMail
    
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    CargarRecursos
    
    'Carga los datos del receptor:
    Set m_oPersona = oFSGSRaiz.Generar_CPersona
    m_oPersona.Cod = g_sCodReceptor
    m_oPersona.CargarTodosLosDatos True
    
    lblUsuRecep.caption = NullToStr(m_oPersona.nombre) & " " & NullToStr(m_oPersona.Apellidos)
    txtEmail.Text = NullToStr(m_oPersona.mail)
End Sub


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
      
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SEGUIM_ACEP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.MoveNext
        lblReceptor.caption = Ador(0).Value
        Ador.MoveNext
        lblAceptar.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiEmail = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    g_sCodReceptor = ""
    Set g_oOrdenEntrega = Nothing
    Set m_oPersona = Nothing
End Sub



