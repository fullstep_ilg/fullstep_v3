VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroNegProceDetalle 
   BackColor       =   &H00808000&
   Caption         =   "Form1"
   ClientHeight    =   4050
   ClientLeft      =   2475
   ClientTop       =   1365
   ClientWidth     =   8715
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInfAhorroNegProceDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4050
   ScaleWidth      =   8715
   Begin SSDataWidgets_B.SSDBGrid sdbgRes 
      Height          =   3255
      Left            =   480
      TabIndex        =   0
      Top             =   600
      Width           =   8055
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   9
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegProceDetalle.frx":0CB2
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4744445
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegProceDetalle.frx":0CCE
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroNegProceDetalle.frx":0CEA
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   9
      Columns(0).Width=   1535
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   5221296
      Columns(1).Width=   3307
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   5221296
      Columns(2).Width=   2514
      Columns(2).Caption=   "Presupuesto"
      Columns(2).Name =   "PRES"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "Standard"
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   15400959
      Columns(3).Width=   2328
      Columns(3).Caption=   "Adjudicado"
      Columns(3).Name =   "ADJ"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   2
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   15400959
      Columns(4).Width=   2143
      Columns(4).Caption=   "Ahorro"
      Columns(4).Name =   "AHO"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(5).Width=   1376
      Columns(5).Caption=   "%"
      Columns(5).Name =   "PORCEN"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   2
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "0.0#\%"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ANYO"
      Columns(6).Name =   "ANYO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "PROCE"
      Columns(7).Name =   "PROCE"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "CODPROCE"
      Columns(8).Name =   "GMN1"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      _ExtentX        =   14208
      _ExtentY        =   5741
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdGrid 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   90
      Picture         =   "frmInfAhorroNegProceDetalle.frx":0D06
      Style           =   1  'Graphical
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   480
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.PictureBox picTipoGrafico 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   480
      ScaleHeight     =   315
      ScaleWidth      =   1665
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   480
      Visible         =   0   'False
      Width           =   1665
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
         Height          =   285
         Left            =   60
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   0
         Width           =   1515
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Row.Count       =   5
         Row(0)          =   "Barras 2D"
         Row(1)          =   "Barras 3D"
         Row(2)          =   "Lineas 2D"
         Row(3)          =   "Lineas 3D"
         Row(4)          =   "Tarta"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "TIPO"
         Columns(0).Name =   "TIPO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2672
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   3075
      Left            =   480
      OleObjectBlob   =   "frmInfAhorroNegProceDetalle.frx":0D8B
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   840
      Width           =   8055
   End
   Begin VB.CommandButton cmdActualizar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   90
      Picture         =   "frmInfAhorroNegProceDetalle.frx":27C6
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   810
      Width           =   315
   End
   Begin VB.CommandButton cmdGrafico 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   90
      Picture         =   "frmInfAhorroNegProceDetalle.frx":2851
      Style           =   1  'Graphical
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   480
      Width           =   315
   End
   Begin MSComctlLib.TabStrip sstabGeneral 
      Height          =   3885
      Left            =   60
      TabIndex        =   3
      Top             =   120
      Width           =   8640
      _ExtentX        =   15240
      _ExtentY        =   6853
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Key             =   "General"
            Object.Tag             =   "General$"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmInfAhorroNegProceDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_sOrigen  As String
Public g_dEquivalencia As Double
'Proceso
Public g_iAnyo As Integer
Public g_sGmn1 As String
Public g_iCod As Long
'Multi lenguaje
Private m_sTodos As String
Private sIdiTipoGrafico(5) As String

Public sGmn1 As String
Public sGmn2 As String
Public sGmn3 As String
Public sGmn4 As String
Public sDetalle As String
Public sFecha As String


Private Sub cmdActualizar_Click()
    If cmdGrid.Visible Then
        MostrarGrafico sdbcTipoGrafico.Value
    End If
End Sub

Private Sub Form_Resize()
    If Me.Height < 1100 Then Exit Sub
    If Me.Width < 1100 Then Exit Sub
    
    sstabGeneral.Width = Me.Width - 200
    sstabGeneral.Height = Me.Height - 675
    
    sdbgRes.Width = sstabGeneral.Width - 480
    sdbgRes.Height = sstabGeneral.Height - 825
    
'    If sdbgRes.Columns(2).Visible = False Then
'        sdbgRes.Columns(0).Width = sdbgRes.Width * 0.15
'        sdbgRes.Columns(1).Width = sdbgRes.Width * 0.25
'        sdbgRes.Columns(3).Width = sdbgRes.Width * 0.15
'        sdbgRes.Columns(4).Width = sdbgRes.Width * 0.15
'        sdbgRes.Columns(5).Width = sdbgRes.Width * 0.15
''        sdbgRes.Columns(6).Width = sdbgRes.Width * 0.15 - 470
'    Else
        sdbgRes.Columns(0).Width = sdbgRes.Width * 0.115
        sdbgRes.Columns(1).Width = sdbgRes.Width * 0.265
        sdbgRes.Columns(2).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(3).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(4).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(5).Width = sdbgRes.Width * 0.155 - 470
'        sdbgRes.Columns(6).Width = sdbgRes.Width * 0.15 - 470
    
'    End If
    
    MSChart1.Width = sdbgRes.Width
    MSChart1.Height = sdbgRes.Height - 300
    
End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub cmdGrafico_Click()
    If sdbgRes.Rows = 0 Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    picTipoGrafico.Visible = True
'        sdbcTipoGrafico.Value = "Barras 3D"
    sdbcTipoGrafico.Value = sIdiTipoGrafico(2)
    MostrarGrafico sdbcTipoGrafico.Value
    cmdGrafico.Visible = False
    cmdGrid.Visible = True
    sdbgRes.Visible = False
    MSChart1.Visible = True
    Screen.MousePointer = vbNormal
End Sub

Private Sub Form_Load()

CargarRecursos

MSChart1.Visible = False

sdbgRes.RemoveAll

    PonerFieldSeparator Me

CargarGridGrupos

End Sub

Private Sub CargarGridGrupos()
Dim ADORs As Ador.Recordset
Dim i As Integer

    If g_sOrigen = "Proceso" Then
    
        If sDetalle = "" Then
            Set ADORs = oGestorInformes.AhorroNegociadoGrupo(g_iAnyo, g_sGmn1, g_iCod, , , True, , CDate(sFecha))
        Else
            Select Case sDetalle
                Case "GMN1"
                    Set ADORs = oGestorInformes.AhorroNegociadoGrupo(g_iAnyo, g_sGmn1, g_iCod, , , True, , CDate(sFecha), sGmn1, sGmn2, sGmn3, sGmn4)
                Case "GMN2"
                    Set ADORs = oGestorInformes.AhorroNegociadoGrupo(g_iAnyo, g_sGmn1, g_iCod, , , True, , CDate(sFecha), sGmn1, sGmn2)
                Case "GMN3"
                    Set ADORs = oGestorInformes.AhorroNegociadoGrupo(g_iAnyo, g_sGmn1, g_iCod, , , True, , CDate(sFecha), sGmn1, sGmn2, sGmn3)
                Case "GMN4"
                    Set ADORs = oGestorInformes.AhorroNegociadoGrupo(g_iAnyo, g_sGmn1, g_iCod, , , True, , CDate(sFecha), sGmn1, sGmn2, sGmn3, sGmn4)
            End Select
        End If
       
    ElseIf g_sOrigen = "Concep3Desde" Then
        Set ADORs = oGestorInformes.AhorroNegociadoGrupoConcep3(g_iAnyo, g_sGmn1, g_iCod, frmInfAhorroNegConcep3Desde.g_sPRES1, frmInfAhorroNegConcep3Desde.g_sPRES2, frmInfAhorroNegConcep3Desde.g_sPRES3, frmInfAhorroNegConcep3Desde.g_sPRES4, , , , , frmInfAhorroNegConcep3Desde.g_sUON1, frmInfAhorroNegConcep3Desde.g_sUON2, frmInfAhorroNegConcep3Desde.g_sUON3, CDate(sFecha))
    ElseIf g_sOrigen = "Concep3Reu" Then
        Set ADORs = oGestorInformes.AhorroNegociadoGrupoConcep3(g_iAnyo, g_sGmn1, g_iCod, frmInfAhorroNegConcep3Reu.g_sPRES1, frmInfAhorroNegConcep3Reu.g_sPRES2, frmInfAhorroNegConcep3Reu.g_sPRES3, frmInfAhorroNegConcep3Reu.g_sPRES4, , , , , frmInfAhorroNegConcep3Reu.g_sUON1, frmInfAhorroNegConcep3Reu.g_sUON2, frmInfAhorroNegConcep3Reu.g_sUON3, CDate(sFecha))
    ElseIf g_sOrigen = "Concep4Desde" Then
        Set ADORs = oGestorInformes.AhorroNegociadoGrupoConcep4(g_iAnyo, g_sGmn1, g_iCod, frmInfAhorroNegConcep4Desde.g_sPRES1, frmInfAhorroNegConcep4Desde.g_sPRES2, frmInfAhorroNegConcep4Desde.g_sPRES3, frmInfAhorroNegConcep4Desde.g_sPRES4, , , , , frmInfAhorroNegConcep4Desde.g_sUON1, frmInfAhorroNegConcep4Desde.g_sUON2, frmInfAhorroNegConcep4Desde.g_sUON3, CDate(sFecha))
    ElseIf g_sOrigen = "Concep4Reu" Then
        Set ADORs = oGestorInformes.AhorroNegociadoGrupoConcep4(g_iAnyo, g_sGmn1, g_iCod, frmInfAhorroNegConcep4Reu.g_sPRES1, frmInfAhorroNegConcep4Reu.g_sPRES2, frmInfAhorroNegConcep4Reu.g_sPRES3, frmInfAhorroNegConcep4Reu.g_sPRES4, , , , , frmInfAhorroNegConcep4Reu.g_sUON1, frmInfAhorroNegConcep4Reu.g_sUON2, frmInfAhorroNegConcep4Reu.g_sUON3, CDate(sFecha))
    End If

    If ADORs Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
     
    i = 2
    
    While Not ADORs.EOF

        sdbgRes.AddItem ADORs("GRUPO").Value & Chr(m_lSeparador) _
        & ADORs("DEN").Value _
        & Chr(m_lSeparador) & g_dEquivalencia * ADORs("PRES").Value _
        & Chr(m_lSeparador) & g_dEquivalencia * ADORs("PREC").Value _
        & Chr(m_lSeparador) & g_dEquivalencia * ADORs("AHORRO").Value _
        & Chr(m_lSeparador) & ADORs("AHORROPORC").Value
        
        sstabGeneral.Tabs.Add i, "A" & ADORs("GRUPO"), ADORs("GRUPO") & " - " & ADORs("DEN")
        sstabGeneral.Tabs(i).Tag = ADORs("GRUPO")

        ADORs.MoveNext
        
        i = i + 1
        
    Wend
    
    If ADORs.RecordCount > 1 Then
        sstabGeneral.Tabs.Add i, "ALL", m_sTodos
        sstabGeneral.Tabs(i).Tag = "ALL"
    End If
    ADORs.Close
    Set ADORs = Nothing
      
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRO_NEGPROCEDETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
       ' Caption = Ador(0).Value
        sdbgRes.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sstabGeneral.Tabs(1).caption = Ador(0).Value
        Ador.MoveNext
        m_sTodos = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.RemoveAll
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        Ador.Close
    End If

    Set Ador = Nothing

End Sub




Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub sstabGeneral_Click()
Dim sCodigo As String
Dim sNombre As String
Dim sDen As String
Dim ADORs As Ador.Recordset

    If sstabGeneral.selectedItem.Tag = sstabGeneral.Tag Then Exit Sub
    
    sstabGeneral.Enabled = False
    Me.Enabled = False
    
    sdbgRes.RemoveAll

    sCodigo = sstabGeneral.selectedItem.Tag
    
    If UCase(sCodigo) = "ALL" Then
        If g_sOrigen = "Proceso" Then
            If sDetalle = "" Then
                Set ADORs = oGestorInformes.AhorroNegociadoProceso(g_iAnyo, g_sGmn1, g_iCod, , , , , , , , , , , CDate(sFecha))
            Else
                Select Case sDetalle
                    Case "GMN1"
                        Set ADORs = oGestorInformes.AhorroNegociadoProceso(g_iAnyo, g_sGmn1, g_iCod, , , , , , , Me.sGmn1, , , , CDate(sFecha))
                    Case "GMN2"
                        Set ADORs = oGestorInformes.AhorroNegociadoProceso(g_iAnyo, g_sGmn1, g_iCod, , , , , , , Me.sGmn1, Me.sGmn2, , , CDate(sFecha))
                    Case "GMN3"
                        Set ADORs = oGestorInformes.AhorroNegociadoProceso(g_iAnyo, g_sGmn1, g_iCod, , , , , , , Me.sGmn1, Me.sGmn2, Me.sGmn3, , CDate(sFecha))
                    Case "GMN4"
                        Set ADORs = oGestorInformes.AhorroNegociadoProceso(g_iAnyo, g_sGmn1, g_iCod, , , , , , , Me.sGmn1, Me.sGmn2, Me.sGmn3, Me.sGmn4, CDate(sFecha))
                End Select
            End If
        ElseIf g_sOrigen = "Concep3Desde" Then
            Set ADORs = oGestorInformes.AhorroNegociadoProcesoPres3(g_iAnyo, g_sGmn1, g_iCod, , , , , 3, , frmInfAhorroNegConcep3Desde.g_sPRES1, frmInfAhorroNegConcep3Desde.g_sPRES2, frmInfAhorroNegConcep3Desde.g_sPRES3, frmInfAhorroNegConcep3Desde.g_sPRES4, frmInfAhorroNegConcep3Desde.g_sUON1, frmInfAhorroNegConcep3Desde.g_sUON2, frmInfAhorroNegConcep3Desde.g_sUON3, CDate(sFecha))
        ElseIf g_sOrigen = "Concep3Reu" Then
            Set ADORs = oGestorInformes.AhorroNegociadoProcesoPres3(g_iAnyo, g_sGmn1, g_iCod, , , , , 3, , frmInfAhorroNegConcep3Reu.g_sPRES1, frmInfAhorroNegConcep3Reu.g_sPRES2, frmInfAhorroNegConcep3Reu.g_sPRES3, frmInfAhorroNegConcep3Reu.g_sPRES4, frmInfAhorroNegConcep3Reu.g_sUON1, frmInfAhorroNegConcep3Reu.g_sUON2, frmInfAhorroNegConcep3Reu.g_sUON3, CDate(sFecha))
        ElseIf g_sOrigen = "Concep4Desde" Then
            Set ADORs = oGestorInformes.AhorroNegociadoProcesoPres4(g_iAnyo, g_sGmn1, g_iCod, , , , , 4, , frmInfAhorroNegConcep4Desde.g_sPRES1, frmInfAhorroNegConcep4Desde.g_sPRES2, frmInfAhorroNegConcep4Desde.g_sPRES3, frmInfAhorroNegConcep4Desde.g_sPRES4, frmInfAhorroNegConcep4Desde.g_sUON1, frmInfAhorroNegConcep4Desde.g_sUON2, frmInfAhorroNegConcep4Desde.g_sUON3, CDate(sFecha))
        ElseIf g_sOrigen = "Concep4Reu" Then
            Set ADORs = oGestorInformes.AhorroNegociadoProcesoPres4(g_iAnyo, g_sGmn1, g_iCod, , , , , 4, , frmInfAhorroNegConcep4Reu.g_sPRES1, frmInfAhorroNegConcep4Reu.g_sPRES2, frmInfAhorroNegConcep4Reu.g_sPRES3, frmInfAhorroNegConcep4Reu.g_sPRES4, frmInfAhorroNegConcep4Reu.g_sUON1, frmInfAhorroNegConcep4Reu.g_sUON2, frmInfAhorroNegConcep4Reu.g_sUON3, CDate(sFecha))
        End If
    ElseIf sCodigo = "General$" Then
        If g_sOrigen = "Proceso" Then
            If sDetalle = "" Then
                Set ADORs = oGestorInformes.AhorroNegociadoGrupo(g_iAnyo, g_sGmn1, g_iCod, , , True, , CDate(sFecha), sGmn1, sGmn2, sGmn3, sGmn4)
            Else
                Select Case sDetalle
                    Case "GMN1"
                        Set ADORs = oGestorInformes.AhorroNegociadoGrupo(g_iAnyo, g_sGmn1, g_iCod, , , True, , CDate(sFecha), sGmn1)
                    Case "GMN2"
                        Set ADORs = oGestorInformes.AhorroNegociadoGrupo(g_iAnyo, g_sGmn1, g_iCod, , , True, , CDate(sFecha), sGmn1, sGmn2)
                    Case "GMN3"
                        Set ADORs = oGestorInformes.AhorroNegociadoGrupo(g_iAnyo, g_sGmn1, g_iCod, , , True, , CDate(sFecha), sGmn1, sGmn2, sGmn3)
                    Case "GMN4"
                        Set ADORs = oGestorInformes.AhorroNegociadoGrupo(g_iAnyo, g_sGmn1, g_iCod, , , True, , CDate(sFecha), sGmn1, sGmn2, sGmn3, sGmn4)
                End Select
            End If
           
           
        ElseIf g_sOrigen = "Concep3Desde" Then
           Set ADORs = oGestorInformes.AhorroNegociadoGrupoConcep3(g_iAnyo, g_sGmn1, g_iCod, frmInfAhorroNegConcep3Desde.g_sPRES1, frmInfAhorroNegConcep3Desde.g_sPRES2, frmInfAhorroNegConcep3Desde.g_sPRES3, frmInfAhorroNegConcep3Desde.g_sPRES4, , , , , frmInfAhorroNegConcep3Desde.g_sUON1, frmInfAhorroNegConcep3Desde.g_sUON2, frmInfAhorroNegConcep3Desde.g_sUON3, CDate(sFecha))
        ElseIf g_sOrigen = "Concep3Reu" Then
           Set ADORs = oGestorInformes.AhorroNegociadoGrupoConcep3(g_iAnyo, g_sGmn1, g_iCod, frmInfAhorroNegConcep3Reu.g_sPRES1, frmInfAhorroNegConcep3Reu.g_sPRES2, frmInfAhorroNegConcep3Reu.g_sPRES3, frmInfAhorroNegConcep3Reu.g_sPRES4, , , , , frmInfAhorroNegConcep3Reu.g_sUON1, frmInfAhorroNegConcep3Reu.g_sUON2, frmInfAhorroNegConcep3Reu.g_sUON3, CDate(sFecha))
        ElseIf g_sOrigen = "Concep4Desde" Then
            Set ADORs = oGestorInformes.AhorroNegociadoGrupoConcep4(g_iAnyo, g_sGmn1, g_iCod, frmInfAhorroNegConcep4Desde.g_sPRES1, frmInfAhorroNegConcep4Desde.g_sPRES2, frmInfAhorroNegConcep4Desde.g_sPRES3, frmInfAhorroNegConcep4Desde.g_sPRES4, , , , , frmInfAhorroNegConcep4Desde.g_sUON1, frmInfAhorroNegConcep4Desde.g_sUON2, frmInfAhorroNegConcep4Desde.g_sUON3, CDate(sFecha))
        ElseIf g_sOrigen = "Concep4Reu" Then
            Set ADORs = oGestorInformes.AhorroNegociadoGrupoConcep4(g_iAnyo, g_sGmn1, g_iCod, frmInfAhorroNegConcep4Reu.g_sPRES1, frmInfAhorroNegConcep4Reu.g_sPRES2, frmInfAhorroNegConcep4Reu.g_sPRES3, frmInfAhorroNegConcep4Reu.g_sPRES4, , , , , frmInfAhorroNegConcep4Reu.g_sUON1, frmInfAhorroNegConcep4Reu.g_sUON2, frmInfAhorroNegConcep4Reu.g_sUON3, CDate(sFecha))
        End If
    Else
        If g_sOrigen = "Proceso" Then
            If sDetalle = "" Then
                Set ADORs = oGestorInformes.AhorroNegociadoProceso(g_iAnyo, g_sGmn1, g_iCod, , , , , , sCodigo, , , , , CDate(sFecha))
            Else
                Select Case sDetalle
                    Case "GMN1"
                        Set ADORs = oGestorInformes.AhorroNegociadoProceso(g_iAnyo, g_sGmn1, g_iCod, , , , , , sCodigo, Me.sGmn1, , , , CDate(sFecha))
                    Case "GMN2"
                        Set ADORs = oGestorInformes.AhorroNegociadoProceso(g_iAnyo, g_sGmn1, g_iCod, , , , , , sCodigo, Me.sGmn1, Me.sGmn2, CDate(sFecha))
                    Case "GMN3"
                        Set ADORs = oGestorInformes.AhorroNegociadoProceso(g_iAnyo, g_sGmn1, g_iCod, , , , , , sCodigo, Me.sGmn1, Me.sGmn2, Me.sGmn3, , CDate(sFecha))
                    Case "GMN4"
                        Set ADORs = oGestorInformes.AhorroNegociadoProceso(g_iAnyo, g_sGmn1, g_iCod, , , , , , sCodigo, Me.sGmn1, Me.sGmn2, Me.sGmn3, Me.sGmn4, CDate(sFecha))
                End Select
            End If
        ElseIf g_sOrigen = "Concep3Desde" Then
            Set ADORs = oGestorInformes.AhorroNegociadoProcesoPres3(g_iAnyo, g_sGmn1, g_iCod, , , , , 3, sCodigo, frmInfAhorroNegConcep3Desde.g_sPRES1, frmInfAhorroNegConcep3Desde.g_sPRES2, frmInfAhorroNegConcep3Desde.g_sPRES3, frmInfAhorroNegConcep3Desde.g_sPRES4, frmInfAhorroNegConcep3Desde.g_sUON1, frmInfAhorroNegConcep3Desde.g_sUON2, frmInfAhorroNegConcep3Desde.g_sUON3, CDate(sFecha))
        ElseIf g_sOrigen = "Concep3Reu" Then
            Set ADORs = oGestorInformes.AhorroNegociadoProcesoPres3(g_iAnyo, g_sGmn1, g_iCod, , , , , 3, sCodigo, frmInfAhorroNegConcep3Reu.g_sPRES1, frmInfAhorroNegConcep3Reu.g_sPRES2, frmInfAhorroNegConcep3Reu.g_sPRES3, frmInfAhorroNegConcep3Reu.g_sPRES4, frmInfAhorroNegConcep3Reu.g_sUON1, frmInfAhorroNegConcep3Reu.g_sUON2, frmInfAhorroNegConcep3Reu.g_sUON3, CDate(sFecha))
        ElseIf g_sOrigen = "Concep4Desde" Then
            Set ADORs = oGestorInformes.AhorroNegociadoProcesoPres4(g_iAnyo, g_sGmn1, g_iCod, , , , , 4, sCodigo, frmInfAhorroNegConcep4Desde.g_sPRES1, frmInfAhorroNegConcep4Desde.g_sPRES2, frmInfAhorroNegConcep4Desde.g_sPRES3, frmInfAhorroNegConcep4Desde.g_sPRES4, frmInfAhorroNegConcep4Desde.g_sUON1, frmInfAhorroNegConcep4Desde.g_sUON2, frmInfAhorroNegConcep4Desde.g_sUON3, CDate(sFecha))
        ElseIf g_sOrigen = "Concep4Reu" Then
            Set ADORs = oGestorInformes.AhorroNegociadoProcesoPres4(g_iAnyo, g_sGmn1, g_iCod, , , , , 4, sCodigo, frmInfAhorroNegConcep4Reu.g_sPRES1, frmInfAhorroNegConcep4Reu.g_sPRES2, frmInfAhorroNegConcep4Reu.g_sPRES3, frmInfAhorroNegConcep4Reu.g_sPRES4, frmInfAhorroNegConcep4Reu.g_sUON1, frmInfAhorroNegConcep4Reu.g_sUON2, frmInfAhorroNegConcep4Reu.g_sUON3, CDate(sFecha))
        End If
        
    End If
    
    While Not ADORs.EOF
        If sCodigo = "General$" Then
            sNombre = ADORs("GRUPO").Value
            sDen = ADORs("DEN").Value
        Else
            sNombre = NullToStr(ADORs("ART").Value)
            sDen = NullToStr(ADORs("DESCR").Value)
        End If
            
            sdbgRes.AddItem sNombre & Chr(m_lSeparador) _
            & sDen _
            & Chr(m_lSeparador) & g_dEquivalencia * ADORs("PRES").Value _
            & Chr(m_lSeparador) & g_dEquivalencia * ADORs("PREC").Value _
            & Chr(m_lSeparador) & g_dEquivalencia * ADORs("AHORRO").Value _
            & Chr(m_lSeparador) & ADORs("AHORROPORC").Value
    
            ADORs.MoveNext
           
    Wend
    
    sCodigo = ""
    
    ADORs.Close
    Set ADORs = Nothing
    sstabGeneral.Enabled = True
    Me.Enabled = True
    
    If sdbgRes.Visible = False Then
        cmdGrafico_Click
    End If
End Sub

Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    MSChart1.Visible = True
    Select Case Tipo
    
'        Case "Barras 2D", "Barras 3D"
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
'                If Tipo = "Barras 3D" Then
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                
'                If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
       
End Sub

Private Sub cmdGrid_Click()

    picTipoGrafico.Visible = False
    cmdGrafico.Visible = True
    cmdGrid.Visible = False
    sdbgRes.Visible = True
    MSChart1.Visible = False
    
End Sub
