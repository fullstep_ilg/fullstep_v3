VERSION 5.00
Begin VB.Form frmPROCEEspMod 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Modificar especificación"
   ClientHeight    =   2505
   ClientLeft      =   1185
   ClientTop       =   4305
   ClientWidth     =   4680
   Icon            =   "frmPROCEEspMod.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2505
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1838
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   2100
      Width           =   1005
   End
   Begin VB.TextBox txtNombre 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1140
      TabIndex        =   4
      Top             =   120
      Width           =   3435
   End
   Begin VB.TextBox txtCom 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      MaxLength       =   500
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   780
      Width           =   4455
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2385
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2100
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1275
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   2100
      Width           =   1005
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Nombre:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   180
      TabIndex        =   5
      Top             =   180
      Width           =   975
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Comentario:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   180
      TabIndex        =   3
      Top             =   540
      Width           =   975
   End
End
Attribute VB_Name = "frmPROCEEspMod"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String
Public g_oIBaseDatos As IBaseDatos
Public g_bSoloLectura As Boolean
Private teserror As TipoErrorSummit
Private m_sNombre As String
Private m_sExtension As String
Private m_sModif As String
Private m_sComentario As String
Private oFos As FileSystemObject

Private Function DevolverExtension(ByVal sNomArchivo As String) As String
Dim iPosicion As Integer

    iPosicion = InStr(1, sNomArchivo, ".")
    DevolverExtension = Mid(sNomArchivo, iPosicion + 1)

End Function

Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit
Dim oItems As CItems
    
    If InStr(1, txtNombre, ".", vbTextCompare) <> 0 Then
        oMensajes.NoValido "Nombre"
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    Select Case g_sOrigen
    
        Case "frmPROCE"
        
            frmPROCE.g_oProcesoSeleccionado.especificaciones.Item(CStr(frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Tag)).nombre = Trim(txtNombre) & "." & m_sExtension
            frmPROCE.g_oProcesoSeleccionado.especificaciones.Item(CStr(frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.ListSubItems.Item(2).Text = Trim(txtCom.Text)
             frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Text = Trim(txtNombre.Text) & "." & m_sExtension
    
        Case "frmPROCEG"
        
            frmPROCE.g_oGrupoSeleccionado.especificaciones.Item(CStr(frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
            frmPROCE.g_oGrupoSeleccionado.especificaciones.Item(CStr(frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.ListSubItems.Item(2).Text = Trim(txtCom.Text)
             frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Text = Trim(txtNombre.Text) & "." & m_sExtension
        
        Case "frmOFERecO"
        
            frmOFERec.g_oOfertaSeleccionada.Adjuntos.Item(CStr(frmOFERec.sdbgAdjun.Columns("ID").Value)).nombre = Trim(txtNombre) & "." & m_sExtension
            frmOFERec.g_oOfertaSeleccionada.Adjuntos.Item(CStr(frmOFERec.sdbgAdjun.Columns("ID").Value)).Comentario = StrToNull(Trim(txtCom))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             frmOFERec.sdbgAdjun.Columns("COMENTARIO").Value = Trim(txtCom)
             frmOFERec.sdbgAdjun.Columns("FICHERO").Value = Trim(txtNombre) & "." & m_sExtension
        
        Case "frmOFERecG"
        
            frmOFERec.g_oGrupoOferSeleccionado.Adjuntos.Item(CStr(frmOFERec.sdbgAdjun.Columns("ID").Value)).nombre = Trim(txtNombre) & "." & m_sExtension
            frmOFERec.g_oGrupoOferSeleccionado.Adjuntos.Item(CStr(frmOFERec.sdbgAdjun.Columns("ID").Value)).Comentario = StrToNull(Trim(txtCom))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             frmOFERec.sdbgAdjun.Columns("COMENTARIO").Value = Trim(txtCom)
             frmOFERec.sdbgAdjun.Columns("FICHERO").Value = Trim(txtNombre) & "." & m_sExtension
        
        Case "frmOFERecAdj"
        
            frmOFERecAdj.g_oPrecio.Adjuntos.Item(CStr(frmOFERecAdj.lstvwAdjun.selectedItem.Tag)).nombre = Trim(txtNombre) & "." & m_sExtension
            frmOFERecAdj.g_oPrecio.Adjuntos.Item(CStr(frmOFERecAdj.lstvwAdjun.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             frmOFERecAdj.lstvwAdjun.selectedItem.ListSubItems.Item(2).Text = Trim(txtCom)
             frmOFERecAdj.lstvwAdjun.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
    
        Case "frmArticuloEsp"
            
            If IsNull(frmArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmArticuloEsp.lstvwEsp.selectedItem.Tag)).Ruta) Then  'El archivo es adjunto
            
                frmArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmArticuloEsp.lstvwEsp.selectedItem.Tag)).nombre = Trim(txtNombre) & "." & m_sExtension
                frmArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmArticuloEsp.lstvwEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom))
                teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                frmArticuloEsp.lstvwEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom)
                frmArticuloEsp.lstvwEsp.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
                
            Else 'El archivo es vinculado
                Set oFos = New FileSystemObject
                If oFos.FolderExists(frmArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmArticuloEsp.lstvwEsp.selectedItem.Tag)).Ruta) And oFos.FileExists(frmArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmArticuloEsp.lstvwEsp.selectedItem.Tag)).Ruta & frmArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmArticuloEsp.lstvwEsp.selectedItem.Tag)).nombre) Then
                    frmArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmArticuloEsp.lstvwEsp.selectedItem.Tag)).nombre = Trim(txtNombre) & "." & m_sExtension
                    frmArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmArticuloEsp.lstvwEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom))
                    teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    frmArticuloEsp.lstvwEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom)
                    frmArticuloEsp.lstvwEsp.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension

                Else
                    oMensajes.ImposibleModificarEsp (frmArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmArticuloEsp.lstvwEsp.selectedItem.Tag)).nombre & " - " & frmArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmArticuloEsp.lstvwEsp.selectedItem.Tag)).Ruta)
                End If
            
            End If
    
        Case "frmPROCEItemArticuloEspA"
        
            Screen.MousePointer = vbHourglass
            
            'Archivo adjunto
            If IsNull(frmPROCEItemArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Tag)).Ruta) Then
                
                frmPROCEItemArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                frmPROCEItemArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                teserror = g_oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Exit Sub
                 End If
                 frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
                 frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
            
            Else 'Archivo vinculado
                
                Set oFos = New FileSystemObject
                If oFos.FolderExists(frmPROCEItemArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Tag)).Ruta) And oFos.FileExists(frmPROCEItemArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Tag)).Ruta & frmPROCEItemArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Tag)).nombre) Then
                    frmPROCEItemArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                    frmPROCEItemArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                    teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
                    frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Text = Trim(txtNombre.Text) & "." & m_sExtension
                Else
                    oMensajes.ImposibleModificarEsp (frmPROCEItemArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Tag)).nombre & " - " & frmPROCEItemArticuloEsp.g_oArticulo.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Tag)).Ruta)
                End If
            End If
            Screen.MousePointer = vbNormal
        
        
        Case "frmPROCEItemArticuloEspI"
        
            Screen.MousePointer = vbHourglass
            frmPROCEItemArticuloEsp.g_oItem.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwItemEsp.selectedItem.Tag)).nombre = Trim(txtNombre) & "." & m_sExtension
            frmPROCEItemArticuloEsp.g_oItem.especificaciones.Item(CStr(frmPROCEItemArticuloEsp.lstvwItemEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
            frmPROCEItemArticuloEsp.lstvwItemEsp.selectedItem.ListSubItems.Item(2).Text = Trim(txtCom)
            frmPROCEItemArticuloEsp.lstvwItemEsp.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
            Screen.MousePointer = vbNormal
            
        Case "frmPROVE"
        
            frmPROVE.g_oProveSeleccionado.especificaciones.Item(CStr(frmPROVE.lstvwEsp.selectedItem.Tag)).nombre = Trim(txtNombre) & "." & m_sExtension
            frmPROVE.g_oProveSeleccionado.especificaciones.Item(CStr(frmPROVE.lstvwEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             frmPROVE.lstvwEsp.selectedItem.ListSubItems.Item(2).Text = Trim(txtCom)
             frmPROVE.lstvwEsp.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
    
        Case "frmPROCEItemEsp"
            
            frmPROCEItemEsp.g_oItem.especificaciones.Item(CStr(frmPROCEItemEsp.lstvwEsp.selectedItem.Tag)).nombre = Trim(txtNombre) & "." & m_sExtension
            frmPROCEItemEsp.g_oItem.especificaciones.Item(CStr(frmPROCEItemEsp.lstvwEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             frmPROCEItemEsp.lstvwEsp.selectedItem.ListSubItems.Item(2).Text = Trim(txtCom)
             frmPROCEItemEsp.lstvwEsp.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
    
        Case "PROCE_MODIFITEMS"
        
            Set oItems = oFSGSRaiz.Generar_CItems
            teserror = oItems.ModificarEspecMultiplesItems(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_arItems, frmPROCEItemEsp.lstvwEsp.selectedItem.Tag, Trim(txtNombre.Text), Trim(txtCom.Text))
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set oItems = Nothing
                Exit Sub
            End If
            Set oItems = Nothing
            
            frmPROCEItemEsp.lstvwEsp.selectedItem.ListSubItems.Item(2).Text = Trim(txtCom.Text)
            frmPROCEItemEsp.lstvwEsp.selectedItem.Text = Trim(txtNombre.Text) & "." & m_sExtension
        
        
        Case "frmCatalogo"
        
            frmCatalogo.g_oAdjun.nombre = Trim(txtNombre) & "." & m_sExtension
            frmCatalogo.g_oAdjun.Comentario = StrToNull(Trim(txtCom))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             frmCatalogo.lstvwAdjun.selectedItem.ListSubItems.Item(2).Text = Trim(txtCom)
             frmCatalogo.lstvwAdjun.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
        
        Case "frmCatalogoAdjunArt"
        
            Screen.MousePointer = vbHourglass
            
            'Archivo adjunto
            If IsNull(frmCatalogoAdjun.g_oArticulo.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Tag)).Ruta) Then
                
                frmCatalogoAdjun.g_oArticulo.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                frmCatalogoAdjun.g_oArticulo.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                teserror = g_oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Exit Sub
                 End If
                 frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
                 frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
            
            Else 'Archivo vinculado
                
                Set oFos = New FileSystemObject
                If oFos.FolderExists(frmCatalogoAdjun.g_oArticulo.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Tag)).Ruta) And oFos.FileExists(frmCatalogoAdjun.g_oArticulo.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Tag)).Ruta & frmCatalogoAdjun.g_oArticulo.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Tag)).nombre) Then
                    frmCatalogoAdjun.g_oArticulo.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                    frmCatalogoAdjun.g_oArticulo.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                    teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
                    frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Text = Trim(txtNombre.Text) & "." & m_sExtension
                Else
                    oMensajes.ImposibleModificarEsp (frmCatalogoAdjun.g_oArticulo.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Tag)).Ruta & frmCatalogoAdjun.g_oArticulo.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Tag)).nombre)
                End If
            End If
            Screen.MousePointer = vbNormal

        Case "frmCatalogoAdjunArtProve"
        
            Screen.MousePointer = vbHourglass
            
            'Archivo adjunto
            If IsNull(frmCatalogoAdjun.g_oArticuloProve.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Tag)).Ruta) Then
                
                frmCatalogoAdjun.g_oArticuloProve.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                frmCatalogoAdjun.g_oArticuloProve.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                teserror = g_oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Exit Sub
                 End If
                 frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
                 frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
            
            Else 'Archivo vinculado
                
                Set oFos = New FileSystemObject
                If oFos.FolderExists(frmCatalogoAdjun.g_oArticuloProve.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Tag)).Ruta) And oFos.FileExists(frmCatalogoAdjun.g_oArticuloProve.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Tag)).Ruta & frmCatalogoAdjun.g_oArticuloProve.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Tag)).nombre) Then
                    frmCatalogoAdjun.g_oArticuloProve.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                    frmCatalogoAdjun.g_oArticuloProve.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                    teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
                    frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Text = Trim(txtNombre.Text) & "." & m_sExtension
                Else
                    oMensajes.ImposibleModificarEsp (frmCatalogoAdjun.g_oArticuloProve.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Tag)).Ruta & frmCatalogoAdjun.g_oArticuloProve.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Tag)).nombre)
                End If
            End If
            Screen.MousePointer = vbNormal
        
        Case "frmCatalogoAdjunLinea"
        
            Screen.MousePointer = vbHourglass
            
            frmCatalogoAdjun.g_oLinea.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(2).selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
            frmCatalogoAdjun.g_oLinea.especificaciones.Item(CStr(frmCatalogoAdjun.lstvwArticuloEsp(2).selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             frmCatalogoAdjun.lstvwArticuloEsp(2).selectedItem.ListSubItems.Item(2).Text = Trim(txtCom.Text)
             frmCatalogoAdjun.lstvwArticuloEsp(2).selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
            Screen.MousePointer = vbNormal
                           
        Case "frmCatalogoAtribEspYAdj"
        
            Screen.MousePointer = vbHourglass
            
            frmCatalogoAtribEspYAdj.g_oLinea.especificaciones.Item(CStr(frmCatalogoAtribEspYAdj.lstvwArticuloEsp.selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
            frmCatalogoAtribEspYAdj.g_oLinea.especificaciones.Item(CStr(frmCatalogoAtribEspYAdj.lstvwArticuloEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             frmCatalogoAtribEspYAdj.lstvwArticuloEsp.selectedItem.ListSubItems.Item(2).Text = Trim(txtCom.Text)
             frmCatalogoAtribEspYAdj.lstvwArticuloEsp.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
            Screen.MousePointer = vbNormal
                           
        Case "frmCATDatExtAdjunArt"
            Screen.MousePointer = vbHourglass
            
            'Archivo adjunto
            If IsNull(frmCATDatExtAdjun.g_oArticulo.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Tag)).Ruta) Then
                
                frmCATDatExtAdjun.g_oArticulo.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                frmCATDatExtAdjun.g_oArticulo.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                teserror = g_oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Exit Sub
                 End If
                 frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
                 frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
            
            Else 'Archivo vinculado
                
                Set oFos = New FileSystemObject
                If oFos.FolderExists(frmCATDatExtAdjun.g_oArticulo.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Tag)).Ruta) And oFos.FileExists(frmCATDatExtAdjun.g_oArticulo.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Tag)).Ruta & frmCATDatExtAdjun.g_oArticulo.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Tag)).nombre) Then
                    frmCATDatExtAdjun.g_oArticulo.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                    frmCATDatExtAdjun.g_oArticulo.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                    teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
                    frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Text = Trim(txtNombre.Text) & "." & m_sExtension
                Else
                    oMensajes.ImposibleModificarEsp (frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Tag)).Ruta & frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Tag)).nombre)
                End If
            End If
            Screen.MousePointer = vbNormal
            
        Case "frmCATDatExtAdjunProve"
            Screen.MousePointer = vbHourglass
            
            'Archivo adjunto
            If IsNull(frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Tag)).Ruta) Then
                
                frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                teserror = g_oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Exit Sub
                 End If
                 frmCATDatExtAdjun.lstvwProveEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
                 frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
            
            Else 'Archivo vinculado
                
                Set oFos = New FileSystemObject
                If oFos.FolderExists(frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Tag)).Ruta) And oFos.FileExists(frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Tag)).Ruta & frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Tag)).nombre) Then
                    frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                    frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                    teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    frmCATDatExtAdjun.lstvwProveEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
                    frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Text = Trim(txtNombre.Text) & "." & m_sExtension
                Else
                    oMensajes.ImposibleModificarEsp (frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Tag)).Ruta & frmCATDatExtAdjun.g_oArtProve.especificaciones.Item(CStr(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Tag)).nombre)
                End If
            End If
            Screen.MousePointer = vbNormal
        Case "frmSeguimientoEsp"    'Seguimiento de pedidos
            If frmSeguimientoEsp.ctlAdjunPedidos.g_oLinea Is Nothing Then
                frmSeguimientoEsp.ctlAdjunPedidos.g_oOrden.Adjuntos.Item(CStr(frmSeguimientoEsp.ctlAdjunPedidos.g_sSelectedItem)).nombre = Trim(txtNombre) & "." & m_sExtension
                frmSeguimientoEsp.ctlAdjunPedidos.g_oOrden.Adjuntos.Item(CStr(frmSeguimientoEsp.ctlAdjunPedidos.g_sSelectedItem)).Comentario = StrToNull(Trim(txtCom))
            Else
                frmSeguimientoEsp.ctlAdjunPedidos.g_oLinea.Adjuntos.Item(CStr(frmSeguimientoEsp.ctlAdjunPedidos.g_sSelectedItem)).nombre = Trim(txtNombre) & "." & m_sExtension
                frmSeguimientoEsp.ctlAdjunPedidos.g_oLinea.Adjuntos.Item(CStr(frmSeguimientoEsp.ctlAdjunPedidos.g_sSelectedItem)).Comentario = StrToNull(Trim(txtCom))
            End If
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
               Screen.MousePointer = vbNormal
               basErrores.TratarError teserror
               Exit Sub
            End If
            frmSeguimientoEsp.ctlAdjunPedidos.g_sComentario = Trim(txtCom.Text)
            frmSeguimientoEsp.ctlAdjunPedidos.g_sNombre = Trim(txtNombre.Text) & "." & m_sExtension
        Case "frmPEDIDOS"   'EMISION cabecera
            frmPEDIDOS.ctlAdjunPedidos.g_oOrden.Adjuntos.Item(CStr(frmPEDIDOS.ctlAdjunPedidos.g_sSelectedItem)).nombre = Trim(txtNombre) & "." & m_sExtension
            frmPEDIDOS.ctlAdjunPedidos.g_oOrden.Adjuntos.Item(CStr(frmPEDIDOS.ctlAdjunPedidos.g_sSelectedItem)).Comentario = StrToNull(Trim(txtCom))
            frmPEDIDOS.ctlAdjunPedidos.g_sComentario = Trim(txtCom.Text)
            frmPEDIDOS.ctlAdjunPedidos.g_sNombre = Trim(txtNombre.Text) & "." & m_sExtension
        Case "frmPEDIDOSLin"    'EMISION línea de pedido
            frmSeguimientoEsp.ctlAdjunPedidos.g_oLinea.Adjuntos.Item(CStr(frmSeguimientoEsp.ctlAdjunPedidos.g_sSelectedItem)).nombre = Trim(txtNombre) & "." & m_sExtension
            frmSeguimientoEsp.ctlAdjunPedidos.g_oLinea.Adjuntos.Item(CStr(frmSeguimientoEsp.ctlAdjunPedidos.g_sSelectedItem)).Comentario = StrToNull(Trim(txtCom))
            frmSeguimientoEsp.ctlAdjunPedidos.g_sComentario = Trim(txtCom.Text)
            frmSeguimientoEsp.ctlAdjunPedidos.g_sNombre = Trim(txtNombre.Text) & "." & m_sExtension
        Case "frmPlantillasProce"
            frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).nombre = Trim(txtNombre) & "." & m_sExtension
            frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             
             RegistrarAccion ACCPlantProceEspMod, "Modificación archivo de especificación: Id:" & frmPlantillasProce.lstvwEsp.selectedItem.Tag & " Plantilla: " & frmPlantillasProce.g_oPlantillaSeleccionada.Id
             
             frmPlantillasProce.lstvwEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
             frmPlantillasProce.lstvwEsp.selectedItem.Text = Trim(txtNombre.Text) & "." & m_sExtension
            'Archivo adjunto
            If IsNull(frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).Ruta) Then
                
                frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                teserror = g_oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Exit Sub
                 End If
             frmPlantillasProce.lstvwEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
             frmPlantillasProce.lstvwEsp.selectedItem.Text = Trim(txtNombre.Text) & "." & m_sExtension
            
            Else 'Archivo vinculado
                
                Set oFos = New FileSystemObject
                If oFos.FolderExists(frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).Ruta) And oFos.FileExists(frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).Ruta & frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).nombre) Then
                    frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                    frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom.Text))
                    teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    frmPlantillasProce.lstvwEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom.Text)
                    frmPlantillasProce.lstvwEsp.selectedItem.Text = Trim(txtNombre.Text) & "." & m_sExtension
                Else
                    oMensajes.ImposibleModificarEsp (frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).Ruta & frmPlantillasProce.g_oPlantillaSeleccionada.especificaciones.Item(CStr(frmPlantillasProce.lstvwEsp.selectedItem.Tag)).nombre)
                End If
            End If
        
        Case "frmADJComent"
            
            If IsNull(frmAdjComent.g_oProceso.ADJAdjuntos.Item(CStr(frmAdjComent.lstvwEsp.selectedItem.Tag)).Ruta) Then  'El archivo es adjunto
                frmAdjComent.g_oProceso.ADJAdjuntos.Item(CStr(frmAdjComent.lstvwEsp.selectedItem.Tag)).nombre = Trim(txtNombre) & "." & m_sExtension
                frmAdjComent.g_oProceso.ADJAdjuntos.Item(CStr(frmAdjComent.lstvwEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom))
                teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                frmAdjComent.lstvwEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom)
                frmAdjComent.lstvwEsp.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
                
            Else 'El archivo es vinculado
                Set oFos = New FileSystemObject
                If oFos.FolderExists(frmAdjComent.g_oProceso.ADJAdjuntos.Item(CStr(frmAdjComent.lstvwEsp.selectedItem.Tag)).Ruta) And oFos.FileExists(frmAdjComent.g_oProceso.ADJAdjuntos.Item(CStr(frmAdjComent.lstvwEsp.selectedItem.Tag)).Ruta & frmAdjComent.g_oProceso.ADJAdjuntos.Item(CStr(frmAdjComent.lstvwEsp.selectedItem.Tag)).nombre) Then
                    frmAdjComent.g_oProceso.ADJAdjuntos.Item(CStr(frmAdjComent.lstvwEsp.selectedItem.Tag)).nombre = Trim(txtNombre) & "." & m_sExtension
                    frmAdjComent.g_oProceso.ADJAdjuntos.Item(CStr(frmAdjComent.lstvwEsp.selectedItem.Tag)).Comentario = StrToNull(Trim(txtCom))
                    teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    frmAdjComent.lstvwEsp.selectedItem.ListSubItems.Item(3).Text = Trim(txtCom)
                    frmAdjComent.lstvwEsp.selectedItem.Text = Trim(txtNombre) & "." & m_sExtension
                    basSeguridad.RegistrarAccion accionessummit.ACCOfeCompAdjunMod, "Proceso: " & CStr(frmAdjComent.g_oProceso.Anyo) & " / " & frmAdjComent.g_oProceso.GMN1Cod & " / " & CStr(frmAdjComent.g_oProceso.Cod) & " Archivo: " & frmAdjComent.g_oProceso.ADJAdjuntos.Item(frmAdjComent.lstvwEsp.selectedItem.Tag).nombre

                Else
                    oMensajes.ImposibleModificarEsp (frmAdjComent.g_oProceso.ADJAdjuntos.Item(CStr(frmAdjComent.lstvwEsp.selectedItem.Tag)).nombre & " - " & frmAdjComent.g_oProceso.ADJAdjuntos.Item(CStr(frmAdjComent.lstvwEsp.selectedItem.Tag)).Ruta)
                End If
            
            End If
        
        Case "frmFormAdjuntos"
            If frmFormAdjuntos.g_sOrigen = "frmSOLConfiguracion" Then
                frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre = Trim(txtNombre) & "." & m_sExtension
                frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario = StrToNull(Trim(txtCom))
                teserror = g_oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Exit Sub
                 End If
                 frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value = Trim(txtNombre.Text) & "." & m_sExtension
                 
                'Archivo adjunto
                If IsNull(frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta) Then
                    
                    frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                    frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario = StrToNull(Trim(txtCom.Text))
                    teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                     End If
                    frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value = Trim(txtNombre.Text) & "." & m_sExtension
                    
                Else 'Archivo vinculado
                    
                    Set oFos = New FileSystemObject
                    If oFos.FolderExists(frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta) And oFos.FileExists(frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta & frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre) Then
                        frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                        frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario = StrToNull(Trim(txtCom.Text))
                        teserror = g_oIBaseDatos.FinalizarEdicionModificando
                        If teserror.NumError <> TESnoerror Then
                            Screen.MousePointer = vbNormal
                            basErrores.TratarError teserror
                            Exit Sub
                        End If
                        frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value = Trim(txtNombre.Text) & "." & m_sExtension
                    Else
                        oMensajes.ImposibleModificarEsp (frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta & frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre)
                    End If
                End If
            Else
                frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre = Trim(txtNombre) & "." & m_sExtension
                frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario = StrToNull(Trim(txtCom))
                teserror = g_oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Exit Sub
                 End If
                 frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value = Trim(txtNombre.Text) & "." & m_sExtension
                 
                'Archivo adjunto
                If IsNull(frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta) Then
                    
                    frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                    frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario = StrToNull(Trim(txtCom.Text))
                    teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                     End If
                    frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value = Trim(txtNombre.Text) & "." & m_sExtension
                    
                Else 'Archivo vinculado
                    
                    Set oFos = New FileSystemObject
                    If oFos.FolderExists(frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta) And oFos.FileExists(frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta & frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre) Then
                        frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                        frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario = StrToNull(Trim(txtCom.Text))
                        teserror = g_oIBaseDatos.FinalizarEdicionModificando
                        If teserror.NumError <> TESnoerror Then
                            Screen.MousePointer = vbNormal
                            basErrores.TratarError teserror
                            Exit Sub
                        End If
                        frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value = Trim(txtNombre.Text) & "." & m_sExtension
                    Else
                        oMensajes.ImposibleModificarEsp (frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta & frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre)
                    End If
                End If
            End If
            frmFormAdjuntos.sdbgAdjuntos.Update
            
        Case "frmDesgloseAdjuntos"
            frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre = Trim(txtNombre) & "." & m_sExtension
            frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario = StrToNull(Trim(txtCom))
            teserror = g_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
             End If
             frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value = Trim(txtNombre.Text) & "." & m_sExtension
             
            'Archivo adjunto
            If IsNull(frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta) Then
                
                frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario = StrToNull(Trim(txtCom.Text))
                teserror = g_oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Exit Sub
                 End If
                frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value = Trim(txtNombre.Text) & "." & m_sExtension
                
            Else 'Archivo vinculado
                
                Set oFos = New FileSystemObject
                If oFos.FolderExists(frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta) And oFos.FileExists(frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta & frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre) Then
                    frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre = Trim(txtNombre.Text) & "." & m_sExtension
                    frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario = StrToNull(Trim(txtCom.Text))
                    teserror = g_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value = Trim(txtNombre.Text) & "." & m_sExtension
                Else
                    oMensajes.ImposibleModificarEsp (frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Ruta & frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).nombre)
                End If
            End If
            frmFormAdjuntos.sdbgAdjuntos.Update
    End Select
        
    Set g_oIBaseDatos = Nothing
    
    Screen.MousePointer = vbNormal
    
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()
    
   Unload Me
    
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    
    If txtNombre.Visible Then txtNombre.SetFocus
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCE_ESP_MOD, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmPROCEEspMod.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        Label2.caption = Ador(0).Value
        Ador.MoveNext
        m_sModif = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        cmdCerrar.caption = Ador(0).Value
        Ador.MoveNext
        m_sComentario = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarRecursos

    Select Case g_sOrigen
    
        Case "frmPROCE"
            
            m_sNombre = Left(frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Text, InStr(1, frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.ListSubItems.Item(2).Text
          
        Case "frmPROCEG"
            
            m_sNombre = Left(frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Text, InStr(1, frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmPROCE.ctlEspecificacionesProceso1.ListaEsp.selectedItem.ListSubItems.Item(2).Text
             
        Case "frmOFERecO", "frmOFERecG"
        
            m_sNombre = Left(frmOFERec.sdbgAdjun.Columns("FICHERO").Value, InStr(1, frmOFERec.sdbgAdjun.Columns("FICHERO").Value, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmOFERec.sdbgAdjun.Columns("FICHERO").Value)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmOFERec.sdbgAdjun.Columns("COMENTARIO").Value
            Me.caption = m_sComentario
        
        Case "frmOFERecAdj"
        
            m_sNombre = Left(frmOFERecAdj.lstvwAdjun.selectedItem.Text, InStr(1, frmOFERecAdj.lstvwAdjun.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmOFERecAdj.lstvwAdjun.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmOFERecAdj.lstvwAdjun.selectedItem.ListSubItems.Item(2).Text
            Me.caption = m_sModif
            
        Case "frmArticuloEsp"
        
            m_sNombre = Left(frmArticuloEsp.lstvwEsp.selectedItem.Text, InStr(1, frmArticuloEsp.lstvwEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmArticuloEsp.lstvwEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmArticuloEsp.lstvwEsp.selectedItem.ListSubItems.Item(3).Text
    
        Case "frmPROCEItemArticuloEspA"
        
            m_sNombre = Left(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Text, InStr(1, frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmPROCEItemArticuloEsp.lstvwArticuloEsp.selectedItem.ListSubItems.Item(3).Text
    
        Case "frmPROCEItemArticuloEspI"
        
            m_sNombre = Left(frmPROCEItemArticuloEsp.lstvwItemEsp.selectedItem.Text, InStr(1, frmPROCEItemArticuloEsp.lstvwItemEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmPROCEItemArticuloEsp.lstvwItemEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmPROCEItemArticuloEsp.lstvwItemEsp.selectedItem.ListSubItems.Item(2).Text
    
        Case "frmPROVE"
        
            m_sNombre = Left(frmPROVE.lstvwEsp.selectedItem.Text, InStr(1, frmPROVE.lstvwEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmPROVE.lstvwEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmPROVE.lstvwEsp.selectedItem.ListSubItems.Item(2).Text
            Me.caption = m_sModif
            
        Case "frmPROCEItemEsp"
        
            m_sNombre = Left(frmPROCEItemEsp.lstvwEsp.selectedItem.Text, InStr(1, frmPROCEItemEsp.lstvwEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmPROCEItemEsp.lstvwEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmPROCEItemEsp.lstvwEsp.selectedItem.ListSubItems.Item(2).Text
    
        Case "PROCE_MODIFITEMS"
        
            m_sNombre = Left(frmPROCEItemEsp.lstvwEsp.selectedItem.Text, InStr(1, frmPROCEItemEsp.lstvwEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmPROCEItemEsp.lstvwEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmPROCEItemEsp.lstvwEsp.selectedItem.ListSubItems.Item(2).Text
            
        Case "frmCatalogo"
        
            m_sNombre = Left(frmCatalogo.lstvwAdjun.selectedItem.Text, InStr(1, frmCatalogo.lstvwAdjun.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmCatalogo.lstvwAdjun.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmCatalogo.lstvwAdjun.selectedItem.ListSubItems.Item(2).Text
            Me.caption = m_sModif
            
        Case "frmCatalogoAdjunArt"
            m_sNombre = Left(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Text, InStr(1, frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmCatalogoAdjun.lstvwArticuloEsp(0).selectedItem.ListSubItems.Item(3).Text
            
        Case "frmCatalogoAdjunArtProve"
            m_sNombre = Left(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Text, InStr(1, frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmCatalogoAdjun.lstvwArticuloEsp(1).selectedItem.ListSubItems.Item(3).Text

        Case "frmCatalogoAtribEspYAdj"
            m_sNombre = Left(frmCatalogoAtribEspYAdj.lstvwArticuloEsp.selectedItem.Text, InStr(1, frmCatalogoAtribEspYAdj.lstvwArticuloEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmCatalogoAtribEspYAdj.lstvwArticuloEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmCatalogoAtribEspYAdj.lstvwArticuloEsp.selectedItem.ListSubItems.Item(2).Text

        Case "frmCatalogoAdjunLinea"
            m_sNombre = Left(frmCatalogoAdjun.lstvwArticuloEsp(2).selectedItem.Text, InStr(1, frmCatalogoAdjun.lstvwArticuloEsp(2).selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmCatalogoAdjun.lstvwArticuloEsp(2).selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmCatalogoAdjun.lstvwArticuloEsp(2).selectedItem.ListSubItems.Item(2).Text

        Case "frmCATDatExtAdjunProve"
            m_sNombre = Left(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Text, InStr(1, frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmCATDatExtAdjun.lstvwProveEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmCATDatExtAdjun.lstvwProveEsp.selectedItem.ListSubItems.Item(3).Text
            
        Case "frmCATDatExtAdjunArt"
            m_sNombre = Left(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Text, InStr(1, frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmCATDatExtAdjun.lstvwArticuloEsp.selectedItem.ListSubItems.Item(3).Text
        Case "frmSeguimientoEsp", "frmPEDIDOSLin"
            m_sNombre = Left(frmSeguimientoEsp.ctlAdjunPedidos.g_sNombre, InStr(1, frmSeguimientoEsp.ctlAdjunPedidos.g_sNombre, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmSeguimientoEsp.ctlAdjunPedidos.g_sNombre)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmSeguimientoEsp.ctlAdjunPedidos.g_sComentario
        Case "frmPEDIDOS"
            m_sNombre = Left(frmPEDIDOS.ctlAdjunPedidos.g_sNombre, InStr(1, frmPEDIDOS.ctlAdjunPedidos.g_sNombre, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmPEDIDOS.ctlAdjunPedidos.g_sNombre)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmPEDIDOS.ctlAdjunPedidos.g_sComentario
        Case "frmPlantillasProce"
            m_sNombre = Left(frmPlantillasProce.lstvwEsp.selectedItem.Text, InStr(1, frmPlantillasProce.lstvwEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmPlantillasProce.lstvwEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmPlantillasProce.lstvwEsp.selectedItem.ListSubItems.Item(3).Text
            
        Case "frmADJComent"
            m_sNombre = Left(frmAdjComent.lstvwEsp.selectedItem.Text, InStr(1, frmAdjComent.lstvwEsp.selectedItem.Text, ".", vbTextCompare) - 1)
            m_sExtension = DevolverExtension(frmAdjComent.lstvwEsp.selectedItem.Text)
            txtNombre.Text = m_sNombre
            txtCom.Text = frmAdjComent.lstvwEsp.selectedItem.ListSubItems.Item(3).Text
            
        Case "frmFormAdjuntos"
            If InStr(1, frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value, ".", vbTextCompare) > 0 Then
                m_sNombre = Left(frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value, InStr(1, frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value, ".", vbTextCompare) - 1)
                m_sExtension = DevolverExtension(frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value)
            Else
                m_sNombre = frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value
                m_sExtension = ""
            End If
            txtNombre.Text = m_sNombre
            If frmFormAdjuntos.g_sOrigen = "frmSOLConfiguracion" Then
                txtCom.Text = NullToStr(frmFormAdjuntos.g_oSolicitudSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario)
            Else
                txtCom.Text = NullToStr(frmFormAdjuntos.g_oCampoSeleccionado.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario)
            End If
            Me.caption = m_sModif
            
        Case "frmDesgloseAdjuntos"
            If InStr(1, frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value, ".", vbTextCompare) > 0 Then
                m_sNombre = Left(frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value, InStr(1, frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value, ".", vbTextCompare) - 1)
                m_sExtension = DevolverExtension(frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value)
            Else
                m_sNombre = frmFormAdjuntos.sdbgAdjuntos.Columns("ARCHIVO").Value
                m_sExtension = ""
            End If
            txtNombre.Text = m_sNombre
            txtCom.Text = NullToStr(frmFormAdjuntos.g_oLineaSeleccionada.Adjuntos.Item(CStr(frmFormAdjuntos.sdbgAdjuntos.Columns("ID").Value)).Comentario)
            Me.caption = m_sModif
    End Select
        
    cmdAceptar.Visible = Not g_bSoloLectura
    cmdCancelar.Visible = Not g_bSoloLectura
    cmdCerrar.Visible = g_bSoloLectura
    txtNombre.Locked = g_bSoloLectura
    txtCom.Locked = g_bSoloLectura

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If Not g_oIBaseDatos Is Nothing Then
        Screen.MousePointer = vbHourglass
        g_oIBaseDatos.CancelarEdicion
        Screen.MousePointer = vbNormal
    End If
    
    Set g_oIBaseDatos = Nothing
    g_bSoloLectura = False
        
End Sub

