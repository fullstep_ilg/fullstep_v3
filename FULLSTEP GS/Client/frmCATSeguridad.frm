VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCATSeguridad 
   Caption         =   "Configuraci�n de seguridad:"
   ClientHeight    =   6105
   ClientLeft      =   360
   ClientTop       =   2805
   ClientWidth     =   11685
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATSeguridad.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6105
   ScaleWidth      =   11685
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   13
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":0CB2
            Key             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":0D51
            Key             =   "Notificado2"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":0DBB
            Key             =   "Persona"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":0E26
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":0ED6
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":0F86
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":1036
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":13C9
            Key             =   "Aprobador"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":143A
            Key             =   "Notificado"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":14AB
            Key             =   "PerUsu"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":1546
            Key             =   "PersonaBaja"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":15D1
            Key             =   "Notificado2Baja"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATSeguridad.frx":165E
            Key             =   "PerUsuBaja"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab stabSeguridad 
      Height          =   5355
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10755
      _ExtentX        =   18971
      _ExtentY        =   9446
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DLimite Adjudicacion"
      TabPicture(0)   =   "frmCATSeguridad.frx":1704
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "tvwLimAdj"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "picIconAdj"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "L�mite de pedido"
      TabPicture(1)   =   "frmCATSeguridad.frx":1720
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdBuscarUsuario"
      Tab(1).Control(1)=   "tbBuscarUsu"
      Tab(1).Control(2)=   "lstListAprovisionadores"
      Tab(1).Control(3)=   "lblBuscarUsu"
      Tab(1).Control(4)=   "lblMon"
      Tab(1).Control(5)=   "lblMonCen"
      Tab(1).ControlCount=   6
      TabCaption(2)   =   "DFlujos de Aprobaci�n"
      TabPicture(2)   =   "frmCATSeguridad.frx":173C
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "sdbgFlujosAprobacion"
      Tab(2).Control(1)=   "sdbcTipoPedido"
      Tab(2).Control(2)=   "sdbcEmp"
      Tab(2).Control(3)=   "sdbcFlujos"
      Tab(2).Control(4)=   "lblFlujosAprobacion"
      Tab(2).Control(5)=   "lblEmpresas"
      Tab(2).Control(6)=   "lblTipoPedido"
      Tab(2).ControlCount=   7
      Begin VB.PictureBox picIconAdj 
         BackColor       =   &H00808080&
         Height          =   435
         Left            =   7080
         ScaleHeight     =   375
         ScaleWidth      =   3435
         TabIndex        =   10
         Top             =   780
         Width           =   3495
         Begin VB.PictureBox Picture2 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   220
            Left            =   60
            Picture         =   "frmCATSeguridad.frx":1758
            ScaleHeight     =   225
            ScaleWidth      =   255
            TabIndex        =   12
            Top             =   60
            Width           =   255
         End
         Begin VB.PictureBox Picture4 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   220
            Left            =   2190
            Picture         =   "frmCATSeguridad.frx":17B9
            ScaleHeight     =   225
            ScaleWidth      =   255
            TabIndex        =   11
            Top             =   60
            Width           =   255
         End
         Begin VB.Label lblAprobador 
            BackColor       =   &H00808080&
            Caption         =   "Aprobador"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   360
            TabIndex        =   14
            ToolTipText     =   "Encargado de aprobar los pedidos que superen la cantidad adjudicada en el proceso"
            Top             =   60
            Width           =   1035
         End
         Begin VB.Label lblNotif1 
            BackColor       =   &H00808080&
            Caption         =   "Notificado"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   2490
            TabIndex        =   13
            ToolTipText     =   "Recibe las notificaciones de pedidos que superen la limitaci�n de tipo1"
            Top             =   45
            Width           =   2055
         End
      End
      Begin VB.CommandButton cmdBuscarUsuario 
         Height          =   295
         Left            =   -69220
         Picture         =   "frmCATSeguridad.frx":181A
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   720
         Width           =   335
      End
      Begin VB.TextBox tbBuscarUsu 
         Height          =   285
         Left            =   -72170
         TabIndex        =   6
         Top             =   720
         Width           =   2865
      End
      Begin MSComctlLib.TreeView tvwLimAdj 
         Height          =   4695
         Left            =   120
         TabIndex        =   9
         Top             =   540
         Width           =   10515
         _ExtentX        =   18547
         _ExtentY        =   8281
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgFlujosAprobacion 
         Height          =   4050
         Left            =   -74760
         TabIndex        =   15
         Top             =   1080
         Width           =   10365
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   9
         stylesets.count =   3
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCATSeguridad.frx":186B
         stylesets(1).Name=   "StringTachado"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   -1  'True
         EndProperty
         stylesets(1).Picture=   "frmCATSeguridad.frx":1887
         stylesets(2).Name=   "Bloqueado"
         stylesets(2).BackColor=   12632256
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmCATSeguridad.frx":18A3
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   9
         Columns(0).Width=   10054
         Columns(0).Caption=   "TIPO_PEDIDO"
         Columns(0).Name =   "TIPO_PEDIDO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "TIPO_PEDIDO_HIDDEN"
         Columns(1).Name =   "TIPO_PEDIDO_HIDDEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   8546
         Columns(2).Caption=   "EMP"
         Columns(2).Name =   "EMP"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "EMP_HIDDEN"
         Columns(3).Name =   "EMP_HIDDEN"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   12541
         Columns(4).Caption=   "SOLICITUD"
         Columns(4).Name =   "SOLICITUD"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "SOLICITUD_HIDDEN"
         Columns(5).Name =   "SOLICITUD_HIDDEN"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "TIPOSOLICITUD"
         Columns(6).Name =   "TIPOSOLICITUD"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "FORMULARIO"
         Columns(7).Name =   "FORMULARIO"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "WORKFLOW"
         Columns(8).Name =   "WORKFLOW"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         _ExtentX        =   18283
         _ExtentY        =   7144
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ListView lstListAprovisionadores 
         Height          =   4035
         Left            =   -74880
         TabIndex        =   17
         Top             =   1080
         Width           =   10500
         _ExtentX        =   18521
         _ExtentY        =   7117
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "DEN"
            Object.Width           =   8820
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoPedido 
         Height          =   285
         Left            =   -74760
         TabIndex        =   19
         Top             =   720
         Width           =   2805
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4948
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEmp 
         Height          =   285
         Left            =   -71760
         TabIndex        =   20
         Top             =   720
         Width           =   2805
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4948
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcFlujos 
         Height          =   285
         Left            =   -68760
         TabIndex        =   21
         Top             =   720
         Width           =   2805
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4948
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblFlujosAprobacion 
         Caption         =   "DFlujo de Aprobacion"
         Height          =   255
         Left            =   -68760
         TabIndex        =   24
         Top             =   480
         Width           =   2175
      End
      Begin VB.Label lblEmpresas 
         Caption         =   "DEmpresas"
         Height          =   255
         Left            =   -71760
         TabIndex        =   23
         Top             =   480
         Width           =   2175
      End
      Begin VB.Label lblTipoPedido 
         Caption         =   "DTipos de Pedido"
         Height          =   255
         Left            =   -74760
         TabIndex        =   22
         Top             =   480
         Width           =   2175
      End
      Begin VB.Label lblBuscarUsu 
         Caption         =   "B�squeda por c�digo de usuario :"
         Height          =   285
         Left            =   -74610
         TabIndex        =   5
         Top             =   750
         Width           =   2535
      End
      Begin VB.Label lblMon 
         Caption         =   "Moneda"
         Height          =   180
         Left            =   -70425
         TabIndex        =   4
         Top             =   360
         Width           =   825
      End
      Begin VB.Label lblMonCen 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   -69600
         TabIndex        =   3
         Top             =   315
         Width           =   4275
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   660
      Left            =   0
      ScaleHeight     =   660
      ScaleWidth      =   11685
      TabIndex        =   2
      Top             =   5445
      Width           =   11685
      Begin VB.CommandButton cmdEliminarFlujos 
         Caption         =   "dEliminarFlujos"
         Height          =   345
         Left            =   5040
         TabIndex        =   18
         Top             =   45
         Width           =   1695
      End
      Begin VB.CommandButton cmdA�adirFlujos 
         Caption         =   "dA�adirFlujos"
         Height          =   345
         Left            =   3240
         TabIndex        =   16
         Top             =   45
         Width           =   1695
      End
      Begin VB.CommandButton cmdA�adirAprovisionador 
         Caption         =   "dA�adir Aprovisionador"
         Height          =   345
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   45
         Width           =   1845
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Height          =   345
         Left            =   2100
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   45
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmCATSeguridad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Variables de restricciones
Public bConsulSegur As Boolean
Public bModifSegur As Boolean
Public bModifAcceso As Boolean
Public bRUO As Boolean
Public bRPerfUO As Boolean
Public bRDep As Boolean

Private Accion As accionessummit

Public oSeguridad As CSeguridad
Public oAproviSeleccionado As cAprovisionador

'Variables de idiomas
Private sIdiMarcaAproba As String
Private sIdiMarcaNotif As String
Private sIdiElimAproba As String
Private sIdiElimNotif As String
Private sIdiEliminar As String
Private sIdiSustituir As String
Private sIdiDetalle As String
Private sIdiNotifVentana As String
Public NombreRama As String
Private oMonedas As CMonedas
Private iPosUsuBuscado As Integer
Private m_sNoHayFlujo As String
Private m_sEliminarFlujos As String
Private m_sEliminarAprovisionador As String

Public Property Let PosUsuBuscado(ByVal Data As Integer)
    Let iPosUsuBuscado = Data
End Property
Public Property Get PosUsuBuscado() As Integer
    PosUsuBuscado = iPosUsuBuscado
End Property


''' <summary>Mostrara el formulario para a�adir un aprovisionador a la categoria</summary>
Private Sub cmdA�adirAprovisionador_Click()
    Accion = ACCCatSeguridadAnyaAprovi
    A�adirAprovisionador
End Sub
''' <summary>Mostrara el formulario para a�adir flujos de aprobacion(solicitudes) a las categorias segun el tipo de pedido y empresa</summary>
Private Sub cmdA�adirFlujos_Click()
    frmFlujosAprobacionPedidos.lCategoria = oSeguridad.Categoria
    frmFlujosAprobacionPedidos.lNivelCategoria = oSeguridad.NivelCat
    frmFlujosAprobacionPedidos.Show vbModal
    
    Me.sdbcEmp.Value = ""
    Me.sdbcEmp.Text = ""
    Me.sdbcFlujos.Value = ""
    Me.sdbcFlujos.Text = ""
    Me.sdbcTipoPedido.Value = ""
    Me.sdbcTipoPedido.Value = ""
    'Actualizamos el grid con los flujos que se hayan insertado
    CargarGridFlujosAprobacionCategoria
    'Actualizamos el combo de empresas
    CargarEmpresas
    'Actualizamos el combo de tipos de pedido
    CargarTipoPedidos
End Sub
''' <summary>Eliminara los flujos de aprobacion seleccionados(solicitudes)</summary>
Private Sub cmdEliminarFlujos_Click()
    Screen.MousePointer = vbHourglass
    Dim inum As Integer
    Dim oFlujosAprobacion As cFlujosAprobacion
    Dim oFlujoAprobacion As cFlujoAprobacion
    Dim tsError As TipoErrorSummit
    Dim i As Integer
    
    With sdbgFlujosAprobacion
        If .SelBookmarks.Count > 0 Then
            i = oMensajes.PreguntaEliminar(m_sEliminarFlujos)
            If i = vbYes Then
                inum = 0
                Set oFlujosAprobacion = oFSGSRaiz.Generar_CFlujosAprobacion
                'A�adimos a la coleccion los flujos que se van a eliminar
                While inum < .SelBookmarks.Count
                    .Bookmark = .SelBookmarks(inum)
                    oFlujosAprobacion.Add oSeguridad.Categoria, oSeguridad.NivelCat, .Columns("TIPO_PEDIDO_HIDDEN").Text, .Columns("TIPO_PEDIDO").Text, .Columns("EMP_HIDDEN").Text, .Columns("EMP").Text, .Columns("SOLICITUD_HIDDEN").Text, .Columns("SOLICITUD").Text, .Columns("TIPOSOLICITUD").Text, .Columns("FORMULARIO").Text, .Columns("WORKFLOW").Text
                inum = inum + 1
                Wend
                'Recorremos esa coleccion y los eliminamos de BDD
                For Each oFlujoAprobacion In oFlujosAprobacion
                    tsError = oFlujoAprobacion.EliminarDeBaseDeDatos
                    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
                    If tsError.NumError <> TESnoerror Then
                        TratarError tsError
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                Next
                'Eliminamos las filas seleccionadas del grid
                .DeleteSelected
                .Update
                .Refresh
            Else
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            Screen.MousePointer = vbNormal
            'No hay ningun Flujo de aprobaci�n seleccionado
            MsgBox m_sNoHayFlujo, vbInformation
            Exit Sub
        End If
    End With
    'Actualizo el combo de empresas
    CargarEmpresas
    'Actualizo el combo de pedidos
    CargarTipoPedidos
    
    Screen.MousePointer = vbNormal
    sdbgFlujosAprobacion.MoveLast
End Sub

Private Sub cmdBuscarUsuario_Click()
Dim blnEncontrado As Boolean

'Si hemos llegaod al ultimo nodo ponemos el PosUsuBuscado a 1 para que empiece desde arriba otra vez
If PosUsuBuscado >= lstListAprovisionadores.ListItems.Count Then
   PosUsuBuscado = 1
Else
   PosUsuBuscado = PosUsuBuscado + 1
End If

blnEncontrado = False

'Nueva version para seguir buscanod cada vez que se oprima el boton buscar
Do While Not blnEncontrado And PosUsuBuscado <= lstListAprovisionadores.ListItems.Count

'Version en el que se mira si contiene la cadena en el Tag
If InStr(UCase(lstListAprovisionadores.ListItems(PosUsuBuscado).Text), UCase(tbBuscarUsu.Text)) > 0 Then
    blnEncontrado = True
Else
    PosUsuBuscado = PosUsuBuscado + 1
End If
Loop

If blnEncontrado Then
    Set lstListAprovisionadores.selectedItem = lstListAprovisionadores.ListItems(PosUsuBuscado)
    lstListAprovisionadores.selectedItem.EnsureVisible
Else
    If lstListAprovisionadores.ListItems.Count > 0 Then
        Set lstListAprovisionadores.selectedItem = lstListAprovisionadores.ListItems(1)
    End If
End If
End Sub



Private Sub Form_Activate()

    Screen.MousePointer = vbHourglass
    
    oSeguridad.CargarDatosSeguridad  'Cargar los datos de la seguridad
    

    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    oMonedas.CargarTodasLasMonedasDesde 1, gParametrosGenerales.gsMONCEN, , , True, True, True
    
    lblMonCen.caption = oMonedas.Item(1).Cod & " - " & oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    
    If FSEPConf Then
        'Funcionamos SIN GS
        stabSeguridad.TabEnabled(0) = False
        stabSeguridad.TabVisible(0) = False

        'Cargamos en el listview los aprovisionadores de la categoria
        CargarListaAprovisionadores
    Else
      If stabSeguridad.Tab = 0 Then
        'Funcionamos CON GS
        GenerarEstructuraOrg (False) ' Generar la estructura de la organizacion en el treeview
        cmdA�adirAprovisionador.Visible = False
        cmdA�adirFlujos.Visible = False
        cmdEliminarFlujos.Visible = False
        cmdListado.Left = cmdA�adirAprovisionador.Left
      ElseIf stabSeguridad.Tab = 1 Then
        'Cargamos en el listview los aprovisionadores de la categoria
        CargarListaAprovisionadores
        cmdA�adirAprovisionador.Visible = True
        cmdA�adirFlujos.Visible = False
        cmdEliminarFlujos.Visible = False
        cmdListado.Left = cmdA�adirAprovisionador.Left + cmdA�adirAprovisionador.Width + 100
      End If
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub Form_Deactivate()
    If frmCatalogo.tvwCategorias.selectedItem.Image <> "SEGURO" And frmCatalogo.tvwCategorias.selectedItem.Image <> "BAJASEG" And oSeguridad.Id <> 0 Then
        If oSeguridad.Categoria = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem) Then
            frmCatalogo.tvwCategorias.selectedItem.Image = "SEGURO"
            frmCatalogo.tvwCategorias.selectedItem.Tag = frmCatalogo.tvwCategorias.selectedItem.Tag & "-%$SEG" & oSeguridad.Id
            frmCatalogo.ActualizarSeguridadEnCategoria oSeguridad.Id, frmCatalogo.tvwCategorias.selectedItem
        End If
    End If
End Sub
Private Sub Form_Load()

Accion = ACCCatSeguridadCon

Me.Width = 9930
Me.Height = 6105
If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
    Me.Top = 0
    Me.Left = 0
End If

PonerFieldSeparator Me

CargarRecursos  ' Cargar los captions dependiendo del idioma

PosUsuBuscado = 1

End Sub


Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    ' Departamentos asociados
    Dim oDepsAsocN0 As CDepAsociados
    Dim oDepsAsocN1 As CDepAsociados
    Dim oDepsAsocN2 As CDepAsociados
    Dim oDepsAsocN3 As CDepAsociados
    Dim oDepAsoc As CDepAsociado
    ' Unidades organizativas
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3
    ' Personas
    Dim oPersN0 As CPersonas
    Dim oPersN1 As CPersonas
    Dim oPersN2 As CPersonas
    Dim oPersN3 As CPersonas
    Dim oPer As CPersona
    ' Otras
    Dim nodx As node
    Dim varCodPersona As Variant
    Dim oNotif As CPersona
    Dim lIdPerfil As Long
    
    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    Set oPersN0 = oFSGSRaiz.Generar_CPersonas
    Set oPersN1 = oFSGSRaiz.Generar_CPersonas
    Set oPersN2 = oFSGSRaiz.Generar_CPersonas
    Set oPersN3 = oFSGSRaiz.Generar_CPersonas
         
    If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or oUsuarioSummit.Tipo = TIpoDeUsuario.comprador) And (bRUO Or bRDep Or bRPerfUO) Then
        If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
        varCodPersona = oUsuarioSummit.Persona.Cod
        oDepsAsocN0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 0, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
        
        ' Cargamos las personas  de esos departamentos.
        For Each oDepAsoc In oDepsAsocN0
             oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True, , True
        Next
            
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , False, , bOrdenadoPorDen, False, , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, , , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, , , , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
                oDepsAsocN3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 3, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
                ' Cargamos las personas  de esos departamentos.
        
                'Personas
                
                For Each oDepAsoc In oDepsAsocN1
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN2
                   oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN3
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True, True
                Next
        End Select
        
        
    Else
        
        oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False
        oPersN0.CargarTodasLasPersonas , , , , 0, , True, , , False, , True, , True
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True, , False
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True, , False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True, , False
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True, , False
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True, , False
                oPersN3.CargarTodasLasPersonas , , , , 3, , True, , , False, , True, , False
                
        End Select
        
        
    End If
    
   '************************************************************
    'Generamos la estructura arborea
    tvwLimAdj.Nodes.clear

    ' Unidades organizativas
    Set nodx = tvwLimAdj.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwLimAdj.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
            
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwLimAdj.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
            
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwLimAdj.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
            
    Next
    
    ' Departamentos
    
    If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) _
        And (bRUO Or bRDep Or bRPerfUO) Then
    
        For Each oDepAsoc In oDepsAsocN0
            
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            Set nodx = tvwLimAdj.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not oSeguridad.AprobadorLimAdj Is Nothing Then
                    If oSeguridad.AprobadorLimAdj.Cod = oPer.Cod Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Aprobador")
                    ElseIf Not oSeguridad.NotificadosLimAdj.Item(oPer.Cod) Is Nothing Then
                            Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Notificado")
                        ElseIf Not IsNull(oPer.Usuario) Then
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                            Else
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                Else
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                    Else
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
                If IsNull(oPer.Usuario) Then
                    nodx.Tag = "P" & nodx.Tag
                Else
                    nodx.Tag = "U" & nodx.Tag
                End If
            Next
        
        Next
                   
                
        For Each oDepAsoc In oDepsAsocN1
            
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwLimAdj.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not oSeguridad.AprobadorLimAdj Is Nothing Then
                    If oSeguridad.AprobadorLimAdj.Cod = oPer.Cod Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Aprobador")
                    ElseIf Not oSeguridad.NotificadosLimAdj(oPer.Cod) Is Nothing Then
                            Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Notificado")
                        ElseIf Not IsNull(oPer.Usuario) Then
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                            Else
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                Else
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                    Else
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                End If
                nodx.Tag = "PER1" & CStr(oPer.Cod)
                If IsNull(oPer.Usuario) Then
                    nodx.Tag = "P" & nodx.Tag

                Else
                    nodx.Tag = "U" & nodx.Tag

                End If
            
            Next
        
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwLimAdj.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not oSeguridad.AprobadorLimAdj Is Nothing Then
                    If oSeguridad.AprobadorLimAdj.Cod = oPer.Cod Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Aprobador")
                    ElseIf Not oSeguridad.NotificadosLimAdj(oPer.Cod) Is Nothing Then
                            Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Notificado")
                        ElseIf Not IsNull(oPer.Usuario) Then
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                            Else
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                Else
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                    Else
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                End If

                nodx.Tag = "PER2" & CStr(oPer.Cod)
                If IsNull(oPer.Usuario) Then
                    nodx.Tag = "P" & nodx.Tag
                Else
                    nodx.Tag = "U" & nodx.Tag
                End If
            
            Next
       
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwLimAdj.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not oSeguridad.AprobadorLimAdj Is Nothing Then
                    If oSeguridad.AprobadorLimAdj.Cod = oPer.Cod Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Aprobador")
                    ElseIf Not oSeguridad.NotificadosLimAdj(oPer.Cod) Is Nothing Then
                            Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Notificado")
                            ElseIf Not IsNull(oPer.Usuario) Then
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                            Else
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                Else
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                    Else
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                End If
                    
                nodx.Tag = "PER3" & CStr(oPer.Cod)
                If IsNull(oPer.Usuario) Then
                    nodx.Tag = "P" & nodx.Tag
                Else
                    nodx.Tag = "U" & nodx.Tag
                End If
            
            Next
        Next
    
    Else
    
        'Departamentos
            
            For Each oDepAsoc In oDepsAsocN0
            
                scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                Set nodx = tvwLimAdj.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            
            Next
                
        For Each oDepAsoc In oDepsAsocN1
            
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwLimAdj.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwLimAdj.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwLimAdj.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            
        Next
                
            'Personas
            
            For Each oPer In oPersN0
                scod1 = oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not oSeguridad.AprobadorLimAdj Is Nothing Then
                    If oSeguridad.AprobadorLimAdj.Cod = oPer.Cod Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Aprobador")
                    ElseIf Not oSeguridad.NotificadosLimAdj(oPer.Cod) Is Nothing Then
                            Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Notificado")
                        ElseIf Not IsNull(oPer.Usuario) Then
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                            Else
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                Else
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                    Else
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
                If IsNull(oPer.Usuario) Then
                    nodx.Tag = "P" & nodx.Tag
                Else
                    nodx.Tag = "U" & nodx.Tag
                End If
            
            Next
            
            For Each oPer In oPersN1
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not oSeguridad.AprobadorLimAdj Is Nothing Then
                    If oSeguridad.AprobadorLimAdj.Cod = oPer.Cod Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Aprobador")
                    ElseIf Not oSeguridad.NotificadosLimAdj(oPer.Cod) Is Nothing Then
                            Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Notificado")
                        ElseIf Not IsNull(oPer.Usuario) Then
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                            Else
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                Else
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                    Else
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                End If
                    
                nodx.Tag = "PER1" & CStr(oPer.Cod)
                If IsNull(oPer.Usuario) Then
                    nodx.Tag = "P" & nodx.Tag
                Else
                    nodx.Tag = "U" & nodx.Tag
                End If
                
            Next
            
            For Each oPer In oPersN2
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not oSeguridad.AprobadorLimAdj Is Nothing Then
                    If oSeguridad.AprobadorLimAdj.Cod = oPer.Cod Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Aprobador")
                    ElseIf Not oSeguridad.NotificadosLimAdj(oPer.Cod) Is Nothing Then
                            Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Notificado")
                        ElseIf Not IsNull(oPer.Usuario) Then
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                            Else
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                Else
                    If Not IsNull(oPer.Usuario) Then
                          Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                      Else
                          Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                End If
                nodx.Tag = "PER2" & CStr(oPer.Cod)
                If IsNull(oPer.Usuario) Then
                    nodx.Tag = "P" & nodx.Tag
                Else
                    nodx.Tag = "U" & nodx.Tag
                End If
            Next
            
            For Each oPer In oPersN3
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
                scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not oSeguridad.AprobadorLimAdj Is Nothing Then
                    If oSeguridad.AprobadorLimAdj.Cod = oPer.Cod Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Aprobador")
                    ElseIf Not oSeguridad.NotificadosLimAdj(oPer.Cod) Is Nothing Then
                            Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Notificado")
                        ElseIf Not IsNull(oPer.Usuario) Then
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                            Else
                                Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                            End If
                Else
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PerUsuBaja", "PerUsu"))
                    Else
                        Set nodx = tvwLimAdj.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, IIf(oPer.BajaLog = 1, "PersonaBaja", "Persona"))
                    End If
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
                If IsNull(oPer.Usuario) Then
                    nodx.Tag = "P" & nodx.Tag
                Else
                    nodx.Tag = "U" & nodx.Tag
                End If
            Next
    End If
    
    If Not oSeguridad.AprobadorLimAdj Is Nothing Then
        If Not IsNull(oSeguridad.AprobadorLimAdj.Cod) And oSeguridad.AprobadorLimAdj.Cod <> "" And oSeguridad.AprobadorLimAdj.BajaLog = 0 Then
            Set nodx = tvwLimAdj.Nodes.Item("PERS" & CStr(oSeguridad.AprobadorLimAdj.Cod))
            nodx.Selected = True
            nodx.EnsureVisible
            ConfigurarInterfazSeguridadAdj nodx
        End If
    End If
    
    If Not oSeguridad.NotificadosLimAdj Is Nothing Then
        If Not IsNull(oSeguridad.AprobadorLimAdj.Cod) And oSeguridad.AprobadorLimAdj.Cod <> "" And oSeguridad.AprobadorLimAdj.BajaLog = 0 Then
            For Each oNotif In oSeguridad.NotificadosLimAdj
                Set nodx = tvwLimAdj.Nodes.Item("PERS" & CStr(oNotif.Cod))
                nodx.EnsureVisible
            Next
        End If
    End If
    
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    Set oDepAsoc = Nothing
    
    Set oPersN0 = Nothing
    Set oPersN1 = Nothing
    Set oPersN2 = Nothing
    Set oPersN3 = Nothing
    Set oPer = Nothing
    
End Sub

Private Function MarcarComoAprobadorAdj(CodPersona As String) As Boolean
Dim oPer As CPersona
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim iRes As Integer
Dim bAccesoFSEP As Boolean
Dim nodx As node
Dim bRegistrar As Boolean

    
    Set oIBaseDatos = oSeguridad
    
    Accion = ACCCatSeguridadAnyaAprob1
    
    If Not oSeguridad.AprobadorLimAdj Is Nothing Then
        If Not IsNull(oSeguridad.AprobadorLimAdj.Cod) And oSeguridad.AprobadorLimAdj.Cod <> "" And _
           oSeguridad.AprobadorLimAdj.Cod <> CodPersona Then
            Accion = ACCCatSeguridadModAprob1
            iRes = oMensajes.PreguntaCambiarAprobadorAdjudicacion(oSeguridad.AprobadorLimAdj.Cod & " - " & oSeguridad.AprobadorLimAdj.nombre & " " & oSeguridad.AprobadorLimAdj.Apellidos)
            If iRes = vbNo Then
                MarcarComoAprobadorAdj = False
                Exit Function
            End If
            If Not tvwLimAdj.Nodes("PERS" & CStr(oSeguridad.AprobadorLimAdj.Cod)) Is Nothing Then
                Set nodx = tvwLimAdj.Nodes("PERS" & CStr(oSeguridad.AprobadorLimAdj.Cod))
            End If
        End If
    End If
    
    Set oPer = oFSGSRaiz.Generar_CPersona
    oPer.Cod = CodPersona
    teserror = oPer.CargarTodosLosDatos(True)
    If teserror.NumError <> TESnoerror Then
        If teserror.NumError = TESUsuarioPersonaEliminado Then
            oMensajes.ImposibleMarcarAprobador 1, 226
        Else
            TratarError teserror
        End If
        Set oPer = Nothing
        Set nodx = Nothing
        MarcarComoAprobadorAdj = False
        Exit Function
    End If
    If Not oPer.AccesoFSEP Then
        If Not bModifAcceso Then
            oMensajes.UsuarioSinAccesoFSEP 1
        Else
            iRes = oMensajes.PreguntaDarAccesoFSEP(1)
            If iRes = vbYes Then
                bAccesoFSEP = True
            End If
        End If
    End If
    
    teserror = oSeguridad.MarcarComoAprobador(oPer.Cod, bAccesoFSEP)
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        MarcarComoAprobadorAdj = False
        Set nodx = Nothing
        Exit Function
    End If
    If bRegistrar Then
        bRegistrar = False
        RegistrarAccion accionessummit.ACCCatSeguridadAnya, "Cat: " & CStr(oSeguridad.Categoria) & " Nivel: " & CStr(oSeguridad.NivelCat) & " Seguridad: " & CStr(oSeguridad.Id)
    End If
            
    cmdListado.Enabled = True
    If Not nodx Is Nothing Then
        RegistrarAccion accionessummit.ACCCatSeguridadModAprob1, "Per:" & Trim(CodPersona)
        If Left(nodx.Tag, 1) = "U" Then
            nodx.Image = "PerUsu"
        Else
            nodx.Image = "Persona"
        End If
        Set nodx = Nothing
    End If
    
    'Registrar accion
    RegistrarAccion Accion, "Per:" & Trim(CodPersona)
    
    oSeguridad.AprobadorLimAdj.CargarTodosLosDatos
    MarcarComoAprobadorAdj = True
End Function
Private Function EliminarAprobadorAdj() As Boolean
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim iRes As Integer
Dim bAccesoFSEP As Boolean

    Set oIBaseDatos = oSeguridad
    If Not oSeguridad.AprobadorLimAdj Is Nothing Then
        If oSeguridad.AprobadorLimAdj.Cod <> "" Then
            If bModifAcceso Then
                oSeguridad.AprobadorLimAdj.CargarTodosLosDatos True
                If oSeguridad.AprobadorLimAdj.AccesoFSEP Then
                    If Not oSeguridad.ExisteAprobadorAdjEnOtraSeguridad Then
                        iRes = oMensajes.PreguntaQuitarAccesoFSEP
                        If iRes = vbYes Then
                            bAccesoFSEP = True
                        End If
                    End If
                End If
            End If
            teserror = oSeguridad.EliminarAprobadorAdjudicacion(bAccesoFSEP)
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                EliminarAprobadorAdj = False
                Exit Function
            End If
        End If
    End If
    
    EliminarAprobadorAdj = True
End Function



Private Sub tvwLimAdj_Collapse(ByVal node As MSComctlLib.node)
ConfigurarInterfazSeguridadAdj node
End Sub

Private Sub tvwLimAdj_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nodx As node

If Button = 2 Then

    Set nodx = tvwLimAdj.selectedItem
    
    If Not nodx Is Nothing Then
        
        If Left(nodx.Tag, 4) = "PPER" Or Left(nodx.Tag, 4) = "UPER" Then
            
           PopupMenu MDI.mnuPopUpCATSeguridad(0)

        End If
        
    End If

End If
   
End Sub

Private Sub tvwLimAdj_NodeClick(ByVal node As MSComctlLib.node)
ConfigurarInterfazSeguridadAdj node
End Sub

Public Sub mnuPopupLimiteAdjudicacion(Index As Integer)
Dim nodx As node
Dim CodPer As String
Dim iRes As Integer
Dim teserror As TipoErrorSummit
Dim bRegistrar As Boolean
Dim bBaja As Boolean

    Set nodx = tvwLimAdj.selectedItem
    CodPer = Right(nodx.Tag, Len(nodx.Tag) - 5)
    
    Select Case Index
    Case 0 'Marcar/Eliminar aprobador
        If MDI.mnuPopUpCATSeguridadAdj(0).caption = sIdiMarcaAproba Then 'marcar
            If nodx.Image = "Notificado" Then
                iRes = oMensajes.PreguntaCambiarNotificado1PorAprob1
                If iRes = vbNo Then
                    Exit Sub
                End If
            End If
            If MarcarComoAprobadorAdj(CodPer) Then
                nodx.Image = "Aprobador"
            End If
        Else 'eliminar
            iRes = oMensajes.PreguntaEliminar(lblAprobador.caption)
            If iRes = vbYes Then
                If oSeguridad.AprobadorLimAdj.BajaLog = 1 Then bBaja = True
                If EliminarAprobadorAdj Then
                    If Left(nodx.Tag, 1) = "U" Then
                        If bBaja = True Then
                            nodx.Image = "PerUsuBaja"
                        Else
                            nodx.Image = "PerUsu"
                        End If
                    Else
                        If bBaja = True Then
                            nodx.Image = "PersonaBaja"
                        Else
                            nodx.Image = "Persona"
                        End If
                    End If
                    RegistrarAccion ACCCatSeguridadEliAprob1, "Persona: " & CodPer
                End If
            End If
        End If
    Case 1 'marcar/eliminar notificado
        If MDI.mnuPopUpCATSeguridadAdj(1).caption = sIdiMarcaNotif Then 'marcar
            teserror = oSeguridad.MarcarComoNotificadoAdjudicacion(CodPer)
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
            Else
                cmdListado.Enabled = True
                nodx.Image = "Notificado"
                If bRegistrar Then
                    bRegistrar = False
                    RegistrarAccion accionessummit.ACCCatSeguridadAnya, "Cat: " & CStr(oSeguridad.Categoria) & " Nivel: " & CStr(oSeguridad.NivelCat) & " Seguridad: " & CStr(oSeguridad.Id)
                End If
                RegistrarAccion ACCCatSeguridadAnyaNotif1, "Persona: " & CodPer
            End If

        Else 'eliminar
            iRes = oMensajes.PreguntaEliminar(lblNotif1.caption)
            If iRes = vbYes Then
                If oSeguridad.NotificadosLimAdj.Item(CodPer).BajaLog = 1 Then bBaja = True
                teserror = oSeguridad.EliminarNotificadoAdjudicacion(CodPer)
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                Else
                    If Left(nodx.Tag, 1) = "U" Then
                        If bBaja = True Then
                            nodx.Image = "PerUsuBaja"
                        Else
                            nodx.Image = "PerUsu"
                        End If
                    Else
                        If bBaja = True Then
                            nodx.Image = "PersonaBaja"
                        Else
                            nodx.Image = "Persona"
                        End If
                    End If
                    RegistrarAccion ACCCatSeguridadEliNotif1, "Persona: " & CodPer
                End If
            End If
        End If
    Case 2
        DetallePersona CodPer
    End Select
    
    Accion = ACCCatSeguridadCon
    
End Sub



Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATSEGURIDAD, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        stabSeguridad.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        stabSeguridad.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        lblAprobador.caption = Ador(0).Value
        Ador.MoveNext
        lblNotif1.caption = Ador(0).Value
        Ador.MoveNext
        sIdiMarcaAproba = Ador(0).Value
        MDI.mnuPopUpCATSeguridadAdj(0).caption = Ador(0).Value
        Ador.MoveNext
        sIdiMarcaNotif = Ador(0).Value
        MDI.mnuPopUpCATSeguridadAdj(0).caption = Ador(0).Value
        Ador.MoveNext
        sIdiElimAproba = Ador(0).Value
        Ador.MoveNext
        sIdiElimNotif = Ador(0).Value
        Ador.MoveNext
        MDI.mnuPopUpCATSeguridadPed(0).caption = Ador(0).Value 'A�adir aprovisionador
        Ador.MoveNext
        MDI.mnuPopUpCATSeguridadPed(1).caption = Ador(0).Value 'A�adir notificado
        Ador.MoveNext
        Ador.MoveNext
        MDI.mnuPopUpCATSeguridadAdj(2).caption = Ador(0).Value
        MDI.mnuPopUpCATSeguridadPed(4).caption = Ador(0).Value 'Detalle
        sIdiDetalle = Ador(0).Value
        Ador.MoveNext
        MDI.mnuPopUpCATSeguridadPed(3).caption = Ador(0).Value 'Eliminar
        sIdiEliminar = Ador(0).Value
        Ador.MoveNext
        sIdiSustituir = Ador(0).Value '15
        Ador.MoveNext
        MDI.mnuPopUpCATSeguridadPed(2).caption = sIdiSustituir
        MDI.mnuPopUpCATSeguridadPed(5).caption = Ador(0).Value 'Modificar aprovisionador
        Ador.MoveNext
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        sIdiNotifVentana = Ador(0).Value
        Ador.MoveNext
        lblMon.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        cmdA�adirAprovisionador.caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdA�adirFlujos.caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdEliminarFlujos.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblTipoPedido.caption = Ador(0).Value
        Me.sdbgFlujosAprobacion.Columns("TIPO_PEDIDO").caption = Ador(0).Value
        Ador.MoveNext
        Me.lblEmpresas.caption = Ador(0).Value
        Me.sdbgFlujosAprobacion.Columns("EMP").caption = Ador(0).Value 'Empresa
        Ador.MoveNext
        Me.lblFlujosAprobacion.caption = Ador(0).Value 'Flujo de aprobaci�n
        Me.sdbgFlujosAprobacion.Columns("SOLICITUD").caption = Ador(0).Value
        Ador.MoveNext
        stabSeguridad.TabCaption(2) = Ador(0).Value 'Flujos de aprobaci�n
        Ador.MoveNext
        m_sNoHayFlujo = Ador(0).Value
        Ador.MoveNext
        m_sEliminarFlujos = Ador(0).Value
        Ador.MoveNext
        m_sEliminarAprovisionador = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing

End Sub
''' <summary>Carga en el ListView la lista de aprovisionadores de la categoria</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub CargarListaAprovisionadores()
    Dim cAprovisionador As cAprovisionador
    Dim listItem As MSComctlLib.listItem
    Dim sCodAprovisionador As String 'Codigo del aprovisionador
    Dim sDenAprovisionador As String 'Denominacion de aprobador(den del departamento o de la UON, o nombre y apellidos de la persona)
    Dim sTextoAprovisionador As String 'Texto que aparecera en el item de la lista
    Dim Index As Integer
    Dim key As String
     lstListAprovisionadores.ListItems.clear
     
     If Not oSeguridad.Aprovisionadores Is Nothing Then
        For Each cAprovisionador In oSeguridad.Aprovisionadores
           If Not cAprovisionador.Persona Is Nothing Then
               'El aprovisionador es una persona
               If cAprovisionador.UON3 <> "" Then
                    key = "PER3" & cAprovisionador.Persona.Cod & "#L" & CStr(cAprovisionador.Id)
               ElseIf cAprovisionador.UON2 <> "" Then
                    key = "PER2" & cAprovisionador.Persona.Cod & "#L" & CStr(cAprovisionador.Id)
               ElseIf cAprovisionador.UON1 <> "" Then
                    key = "PER1" & cAprovisionador.Persona.Cod & "#L" & CStr(cAprovisionador.Id)
               Else
                    key = "PER0" & cAprovisionador.Persona.Cod & "#L" & CStr(cAprovisionador.Id)
               End If
               sTextoAprovisionador = cAprovisionador.Persona.Cod & " - " & cAprovisionador.Persona.Apellidos & ", " & cAprovisionador.Persona.nombre & " - " & Format(cAprovisionador.importe, "#,##0.00") & " " & gParametrosGenerales.gsMONCEN
               Set listItem = lstListAprovisionadores.ListItems.Add(, key, sTextoAprovisionador, , 3)
               listItem.Tag = CStr(cAprovisionador.importe)
           ElseIf Not cAprovisionador.Departamento Is Nothing Then
               'El aprovisionador es una departamento
               
               'Si el departamento cuelga de una unidad organizativa se le enlaza su codigo a la denominacion que aparecera en la lista
                If cAprovisionador.UON3 <> "" Then
                    sTextoAprovisionador = cAprovisionador.UON1 & Space(2) & cAprovisionador.UON2 & Space(2) & cAprovisionador.UON3
                    key = "DEP3" & cAprovisionador.Departamento.Cod & "#L" & CStr(cAprovisionador.Id)
                ElseIf cAprovisionador.UON2 <> "" Then
                    sTextoAprovisionador = cAprovisionador.UON1 & Space(2) & cAprovisionador.UON2
                    key = "DEP2" & cAprovisionador.Departamento.Cod & "#L" & CStr(cAprovisionador.Id)
                ElseIf cAprovisionador.UON1 <> "" Then
                    sTextoAprovisionador = cAprovisionador.UON1
                    key = "DEP1" & cAprovisionador.Departamento.Cod & "#L" & CStr(cAprovisionador.Id)
                Else
                    sTextoAprovisionador = ""
                    key = "DEP0" & cAprovisionador.Departamento.Cod & "#L" & CStr(cAprovisionador.Id)
                End If
               
               
               sTextoAprovisionador = IIf(sTextoAprovisionador <> "", sTextoAprovisionador & " - ", "") & cAprovisionador.Departamento.Cod & " - " & cAprovisionador.Departamento.Den & " - " & Format(cAprovisionador.importe, "#,##0.00") & " " & gParametrosGenerales.gsMONCEN
               Set listItem = lstListAprovisionadores.ListItems.Add(, key, sTextoAprovisionador, , 6)
               listItem.Tag = CStr(cAprovisionador.importe)
           Else
               'El aprovisionador es una Unidad organizativa
                If cAprovisionador.UON3 <> "" Then
                    sDenAprovisionador = cAprovisionador.DenUON3
                    sCodAprovisionador = cAprovisionador.UON1 & Space(2) & cAprovisionador.UON2 & Space(2) & cAprovisionador.UON3
                    key = "UON3" & cAprovisionador.UON3 & "#L" & CStr(cAprovisionador.Id)
                ElseIf cAprovisionador.UON2 <> "" Then
                    sDenAprovisionador = cAprovisionador.DenUON2
                    sCodAprovisionador = cAprovisionador.UON1 & Space(2) & cAprovisionador.UON2
                    key = "UON2" & cAprovisionador.UON2 & "#L" & CStr(cAprovisionador.Id)
                Else
                    sDenAprovisionador = cAprovisionador.DenUON1
                    sCodAprovisionador = cAprovisionador.UON1
                    key = "UON1" & cAprovisionador.UON1 & "#L" & CStr(cAprovisionador.Id)
                End If
                sTextoAprovisionador = sCodAprovisionador & " - " & sDenAprovisionador & " - " & Format(cAprovisionador.importe, "#,##0.00") & " " & gParametrosGenerales.gsMONCEN
               
               
               'Comprobamos que nivel de Uon es, para sacar el icono corresponiente en el listView
               If cAprovisionador.UON3 <> "" Then
                    Set listItem = lstListAprovisionadores.ListItems.Add(, key, sTextoAprovisionador, , 7)
                    listItem.Tag = CStr(cAprovisionador.importe)
               ElseIf cAprovisionador.UON2 <> "" Then
                    Set listItem = lstListAprovisionadores.ListItems.Add(, key, sTextoAprovisionador, , 5)
                    listItem.Tag = CStr(cAprovisionador.importe)
               Else
                    Set listItem = lstListAprovisionadores.ListItems.Add(, key, sTextoAprovisionador, , 4)
                    listItem.Tag = CStr(cAprovisionador.importe)
               End If
           End If
           Index = Index + 1
        Next
     End If
End Sub

Private Sub DetallePersona(ByVal Codigo As String)
Dim oPer As CPersona
Dim teserror As TipoErrorSummit

            Accion = ACCPerDet
    
            Set oPer = oFSGSRaiz.Generar_CPersona
            oPer.Cod = Codigo
            teserror = oPer.CargarTodosLosDatos
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Accion = ACCCatSeguridadCon
                Exit Sub
            End If
            
            Set frmESTRORGPersona.frmOrigen = Me
            frmESTRORGPersona.caption = sIdiDetalle
            frmESTRORGPersona.txtCOD = oPer.Cod
            frmESTRORGPersona.txtNom = NullToStr(oPer.nombre)
            frmESTRORGPersona.txtApel = oPer.Apellidos
            frmESTRORGPersona.txtCargo = NullToStr(oPer.Cargo)
            frmESTRORGPersona.txtFax = NullToStr(oPer.Fax)
            frmESTRORGPersona.txtMail = NullToStr(oPer.mail)
            frmESTRORGPersona.txtTfno = NullToStr(oPer.Tfno)
            frmESTRORGPersona.txtTfno2 = NullToStr(oPer.Tfno2)
            frmESTRORGPersona.sdbcEquipo.Text = NullToStr(oPer.codEqp)
            If NullToStr(oPer.codEqp) <> "" Then
                frmESTRORGPersona.chkFunCompra.Value = vbChecked
            End If
            Screen.MousePointer = vbNormal
            frmESTRORGPersona.Show 1
            

    Accion = ACCCatSeguridadCon
End Sub



Public Sub mnuPopupLimitePedido(Index As Integer)
    Dim Item As MSComctlLib.listItem 'item de la lista de aprovisionadores
    Dim iRes As Integer
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    
    Set Item = lstListAprovisionadores.selectedItem
    
    
    Select Case Index
    Case 2 'Sustituir (aprovisionador)
        Accion = ACCCatSeguridadSusAprovi
        GestionarAprovisionador Item
    Case 3 'Eliminar/Sustituir
        If MDI.mnuPopUpCATSeguridadPed(3).caption = sIdiEliminar Then
            'Eliminar aprovisionador
            Accion = accionessummit.ACCCatSeguridadEliAprovi
            iRes = oMensajes.PreguntaEliminar(m_sEliminarAprovisionador)
            If iRes = vbYes Then
                oAproviSeleccionado.CargarTodosLosDatos
                If bModifAcceso Then
                    'Al eliminar un aprovisionador, si es una persona se pregunta si se le quita el acceso a EP
                    If Not oAproviSeleccionado.Persona Is Nothing Then
                         If oAproviSeleccionado.Persona.AccesoFSEP Then
                            iRes = oMensajes.PreguntaQuitarAccesoFSEP
                            If iRes = vbYes Then
                                oAproviSeleccionado.DarAcceso = True
                            End If
                        End If
                    End If
                End If
                Set oIBaseDatos = oAproviSeleccionado
                teserror = oIBaseDatos.EliminarDeBaseDatos
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                Else
                    RegistrarAccion ACCCatSeguridadEliAprovi, "ID Aprov: " & oAproviSeleccionado.Id
                    Set oAproviSeleccionado = Nothing
                    'Si se ha eliminado bien en Bd, lo eliminamos del listView
                    EliminarAprovisionadorEnEstructura
                End If

            End If
        Else 'Sustituir aprovisionador
            Accion = ACCCatSeguridadSusAprovi
            GestionarAprovisionador Item
        
        End If
    
    Case 4 'Detalle
        Accion = ACCCatSeguridadCon
        'Solo se sacara la opcion de menu "detalle" cuando el aprovisionador sea una persona
        If Not oAproviSeleccionado.Persona Is Nothing Then
            DetallePersona oAproviSeleccionado.Persona.Cod
        End If
    Case 5 'Modificar
        Accion = ACCCatSeguridadMODAprovi
        ModificarAprovisionador Item
    End Select
    
    Set oIBaseDatos = Nothing
    Accion = ACCCatSeguridadCon
End Sub

Public Sub ConfigurarInterfazSeguridadPed(ByVal nodx As MSComctlLib.node)
Dim iNivel As Integer
Dim sString As String

If Not nodx Is Nothing Then
    If nodx.Tag = "WFW0" Then
        If bModifSegur Then
            MDI.mnuPopUpCATSeguridadPed.Item(0).Visible = True
            MDI.mnuPopUpCATSeguridadPed.Item(1).Visible = False
            MDI.mnuPopUpCATSeguridadPed.Item(2).Visible = False
            MDI.mnuPopUpCATSeguridadPed.Item(3).Visible = False
            MDI.mnuPopUpCATSeguridadPed.Item(4).Visible = False
            MDI.mnuPopUpCATSeguridadPed.Item(5).Visible = False
        End If
        Exit Sub
    End If
    sString = Mid(nodx.Tag, InStr(1, nodx.Tag, "$-") + 5)
    iNivel = Mid(sString, 1, InStr(1, sString, "-$") - 1)
    
    If nodx.Image = "Persona" Or nodx.Image = "PersonaBaja" Then
            
        If bModifSegur Then
            
             MDI.mnuPopUpCATSeguridadPed.Item(0).Visible = True
             MDI.mnuPopUpCATSeguridadPed.Item(1).Visible = True
             MDI.mnuPopUpCATSeguridadPed.Item(2).Visible = True
             MDI.mnuPopUpCATSeguridadPed.Item(3).Visible = True
             MDI.mnuPopUpCATSeguridadPed.Item(4).Visible = True
             MDI.mnuPopUpCATSeguridadPed.Item(5).Visible = True
             If nodx.Children > 0 Then
                 MDI.mnuPopUpCATSeguridadPed.Item(3).caption = sIdiSustituir
             Else
                 MDI.mnuPopUpCATSeguridadPed.Item(3).caption = sIdiEliminar
             End If
         Else
             MDI.mnuPopUpCATSeguridadPed.Item(5).Visible = False
             MDI.mnuPopUpCATSeguridadPed.Item(4).Visible = True
             MDI.mnuPopUpCATSeguridadPed.Item(0).Visible = False
             MDI.mnuPopUpCATSeguridadPed.Item(1).Visible = False
             MDI.mnuPopUpCATSeguridadPed.Item(2).Visible = False
             MDI.mnuPopUpCATSeguridadPed.Item(3).Visible = False
         End If
    Else
        If bModifSegur Then
             MDI.mnuPopUpCATSeguridadPed.Item(3).Visible = True
             MDI.mnuPopUpCATSeguridadPed.Item(4).Visible = True
             MDI.mnuPopUpCATSeguridadPed.Item(5).Visible = True
             MDI.mnuPopUpCATSeguridadPed.Item(0).Visible = False
             MDI.mnuPopUpCATSeguridadPed.Item(1).Visible = False
             MDI.mnuPopUpCATSeguridadPed.Item(2).Visible = False
             MDI.mnuPopUpCATSeguridadPed.Item(3).caption = sIdiEliminar
         Else
             MDI.mnuPopUpCATSeguridadPed.Item(4).Visible = True
             MDI.mnuPopUpCATSeguridadPed.Item(5).Visible = False
             MDI.mnuPopUpCATSeguridadPed.Item(0).Visible = False
             MDI.mnuPopUpCATSeguridadPed.Item(1).Visible = False
             MDI.mnuPopUpCATSeguridadPed.Item(2).Visible = False
             MDI.mnuPopUpCATSeguridadPed.Item(3).Visible = False
         End If

    End If

End If
End Sub

Public Sub ConfigurarInterfazSeguridadAdj(ByVal nodx As node)
Dim sTipoNod As String

If Not nodx Is Nothing Then

    sTipoNod = Right(nodx.Tag, Len(nodx.Tag) - 1)
    If Left(sTipoNod, 4) = "PER0" Or Left(sTipoNod, 4) = "PER1" Or Left(sTipoNod, 4) = "PER2" Or Left(sTipoNod, 4) = "PER3" Then
            
        If bModifSegur Then
             MDI.mnuPopUpCATSeguridadAdj.Item(0).Visible = True
             MDI.mnuPopUpCATSeguridadAdj.Item(1).Visible = True
             MDI.mnuPopUpCATSeguridadAdj.Item(2).Visible = True
             If nodx.Image = "Aprobador" Then
                 MDI.mnuPopUpCATSeguridadAdj.Item(0).caption = sIdiElimAproba
                 MDI.mnuPopUpCATSeguridadAdj.Item(1).caption = sIdiMarcaNotif
                 MDI.mnuPopUpCATSeguridadAdj.Item(1).Enabled = False
             Else
                 MDI.mnuPopUpCATSeguridadAdj.Item(1).Enabled = True
                 If nodx.Image = "Notificado" Then
                     MDI.mnuPopUpCATSeguridadAdj.Item(0).caption = sIdiMarcaAproba
                     MDI.mnuPopUpCATSeguridadAdj.Item(1).caption = sIdiElimNotif
                 Else
                     MDI.mnuPopUpCATSeguridadAdj.Item(0).caption = sIdiMarcaAproba
                     MDI.mnuPopUpCATSeguridadAdj.Item(1).caption = sIdiMarcaNotif
                 End If
                If Left(nodx.Tag, 1) = "P" Then 'No es usuaria, no puede darle aprobador
                    MDI.mnuPopUpCATSeguridadAdj.Item(0).Enabled = False
                Else
                    MDI.mnuPopUpCATSeguridadAdj.Item(0).Enabled = True
                End If
                 
            End If
            If nodx.Image = "PersonaBaja" Or nodx.Image = "Notificado2Baja" Or nodx.Image = "PerUsuBaja" Then
                MDI.mnuPopUpCATSeguridadAdj.Item(0).Enabled = False
                MDI.mnuPopUpCATSeguridadAdj.Item(1).Enabled = False
            End If
         Else
             MDI.mnuPopUpCATSeguridadAdj.Item(2).Visible = True
             MDI.mnuPopUpCATSeguridadAdj.Item(0).Visible = False
             MDI.mnuPopUpCATSeguridadAdj.Item(1).Visible = False
         End If
        
    End If

End If
End Sub


Private Sub Form_Resize()

If Me.Height < 1550 Then Exit Sub
If Me.Width < 800 Then Exit Sub
        
    stabSeguridad.Height = Me.Height - 1250
    stabSeguridad.Width = Me.Width - 135
    lstListAprovisionadores.Width = Me.Width - 495
    lstListAprovisionadores.Height = Me.Height - 2450
    sdbgFlujosAprobacion.Height = Me.Height - 2450
    sdbgFlujosAprobacion.Width = Me.Width - 495
    tvwLimAdj.Width = Me.Width - 495
    tvwLimAdj.Height = Me.Height - 2450
    If stabSeguridad.Tab = 0 Then
        picIconAdj.Left = tvwLimAdj.Width - picIconAdj.Width - 90
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)

If frmCatalogo.tvwCategorias.selectedItem.Image <> "SEGURO" And frmCatalogo.tvwCategorias.selectedItem.Image <> "CAT_INT" And frmCatalogo.tvwCategorias.selectedItem.Image <> "BAJASEG" And oSeguridad.Id <> 0 Then
    If oSeguridad.Categoria = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem) Then
        frmCatalogo.tvwCategorias.selectedItem.Image = "SEGURO"
        frmCatalogo.tvwCategorias.selectedItem.Tag = frmCatalogo.tvwCategorias.selectedItem.Tag & "-%$SEG" & oSeguridad.Id
        frmCatalogo.ActualizarSeguridadEnCategoria oSeguridad.Id, frmCatalogo.tvwCategorias.selectedItem
    End If
End If
Set oMonedas = Nothing
Me.Visible = False
End Sub


''' <summary>Asigna el aprovisionador seleccionado</summary>
''' <remarks></remarks>
Private Sub lstListAprovisionadores_ItemClick(ByVal Item As MSComctlLib.listItem)
    Set oAproviSeleccionado = Nothing
    
    Set oAproviSeleccionado = oFSGSRaiz.Generar_CAprovisionador
    Dim key As String
    key = Item.key
    key = Mid(key, InStr(1, key, "#", vbTextCompare) + 1, Len(key))
    oAproviSeleccionado.Id = Replace(key, "L", "")
    Set oAproviSeleccionado = ObtenerAprovisionador(oAproviSeleccionado.Id)
End Sub
''' <summary>Muestra el menu popup con las opciones de sustitur, eliminar....el aprovisionador</summary>
''' <remarks></remarks>
Private Sub lstListAprovisionadores_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        If Not lstListAprovisionadores.selectedItem Is Nothing Then
            'La opci�n de "Detalle" del popup solo se mostrara cuando sea un aprovisionador persona
            If Not oAproviSeleccionado Is Nothing Then
                If Not oAproviSeleccionado.Persona Is Nothing Then
                    MDI.mnuPopUpCATSeguridadPed(4).Visible = True
                Else
                    MDI.mnuPopUpCATSeguridadPed(4).Visible = False
                End If
            End If
            
            
            PopupMenu MDI.mnuPopUpCATSeguridad(1)
        End If
    End If
End Sub
''' <summary>Devolvera el objeto cAprovisionador para el ID que se pasa por parametro</summary>
''' <param:name="idAprovisionador">Id del aprovisionador</param>
''' <return>objeto cAprovisionador</return>
Private Function ObtenerAprovisionador(ByVal idAprovisionador As Integer)
    Dim oAprovisionador As cAprovisionador
    If Not oSeguridad.Aprovisionadores Is Nothing Then
        For Each oAprovisionador In oSeguridad.Aprovisionadores
            If oAprovisionador.Id = idAprovisionador Then
                Set ObtenerAprovisionador = oAprovisionador
                Exit Function
            End If
        Next
    End If
    'No se ha encontrado el aprovisionador para el ID pasado
    Set ObtenerAprovisionador = Nothing
End Function

Private Sub stabSeguridad_Click(PreviousTab As Integer)
    oSeguridad.CargarDatosSeguridad
    
    If stabSeguridad.Tab = 0 Then 'Si es el tab de aprobador
      CargarListaAprovisionadores
      cmdA�adirAprovisionador.Visible = False
      cmdA�adirFlujos.Visible = False
      cmdEliminarFlujos.Visible = False
      cmdListado.Left = cmdA�adirAprovisionador.Left
      cmdListado.Visible = True
    ElseIf stabSeguridad.Tab = 1 Then 'Si es el tab de aprovisionadores
      CargarListaAprovisionadores
      cmdA�adirAprovisionador.Visible = True
      cmdA�adirFlujos.Visible = False
      cmdEliminarFlujos.Visible = False
      cmdListado.Left = cmdA�adirAprovisionador.Left + cmdA�adirAprovisionador.Width + 100
      cmdListado.Visible = True
    Else 'Resto de tabs
      'Cargamos los flujos de aprobacion de la categoria de esta seguridad
      CargarGridFlujosAprobacionCategoria
      CargarSolicitudes
      CargarEmpresas
      CargarTipoPedidos
      cmdListado.Visible = False
      cmdA�adirAprovisionador.Visible = False
      cmdA�adirFlujos.Visible = True
      cmdEliminarFlujos.Visible = True
      cmdA�adirFlujos.Left = cmdA�adirAprovisionador.Left
      cmdEliminarFlujos.Left = cmdA�adirFlujos.Width + 200
    End If
End Sub

Private Sub tbBuscarUsu_Validate(Cancel As Boolean)
      PosUsuBuscado = 1
End Sub

''' <summary>Mostrara el formulario para a�adir un aprovisionador a la categoria</summary>
Private Sub A�adirAprovisionador()
    frmSELPerWorkflow.Accion = Accion
    frmSELPerWorkflow.bRUO = bRUO
    frmSELPerWorkflow.bRPerfUO = bRPerfUO
    frmSELPerWorkflow.bRDep = bRDep
    frmSELPerWorkflow.sOrigen = "frmCATSeguridad"
    frmSELPerWorkflow.Categoria = oSeguridad.Categoria
    frmSELPerWorkflow.NivelCategoria = oSeguridad.NivelCat
    
    frmSELPerWorkflow.Show 1
End Sub
Private Sub ModificarAprovisionador(ByVal oItem As MSComctlLib.listItem)
    frmSELPerWorkflow.Accion = Accion
    frmSELPerWorkflow.bRUO = bRUO
    frmSELPerWorkflow.bRPerfUO = bRPerfUO
    frmSELPerWorkflow.bRDep = bRDep
    frmSELPerWorkflow.sOrigen = "frmCATSeguridad"
    frmSELPerWorkflow.Categoria = oSeguridad.Categoria
    frmSELPerWorkflow.NivelCategoria = oSeguridad.NivelCat
    
    Set frmSELPerWorkflow.oAprovSeleccionado = oAproviSeleccionado
    frmSELPerWorkflow.keyAprovisionadorSeleccionado = Mid(oItem.key, 1, InStr(1, oItem.key, "#") - 1)
    
    frmSELPerWorkflow.Show 1
End Sub

Private Sub GestionarAprovisionador(Optional ByVal oItem As MSComctlLib.listItem)
    frmSELPerWorkflow.Accion = Accion
    frmSELPerWorkflow.bRUO = bRUO
    frmSELPerWorkflow.bRPerfUO = bRPerfUO
    frmSELPerWorkflow.bRDep = bRDep
    frmSELPerWorkflow.sOrigen = "frmCATSeguridad"
    frmSELPerWorkflow.Categoria = oSeguridad.Categoria
    frmSELPerWorkflow.NivelCategoria = oSeguridad.NivelCat
    
    Select Case Accion
    Case ACCCatSeguridadSusAprovi
        
        Set frmSELPerWorkflow.oAprovSeleccionado = oAproviSeleccionado
    
    End Select
    
    frmSELPerWorkflow.Show 1
End Sub


''' <summary>Comprobara si existe el aprovisionador en la lista de aprovisionadores</summary>
''' <param:name="oAprovisionadorNew">Aprovisionador a comprobar</param>
''' <return>True si existe el aprovisionador, false si no existe</return>
Public Function ExisteAprovisionadorEnLista(oAprovisionadorNew As cAprovisionador)
    Dim oAprovisionadorLista As cAprovisionador 'Aprovisionador que recogemos de la lista de aprovisionadores
            
    If Not oSeguridad.Aprovisionadores Is Nothing Then
        For Each oAprovisionadorLista In oSeguridad.Aprovisionadores
            If oAprovisionadorLista.UON1 = oAprovisionadorNew.UON1 And oAprovisionadorLista.UON2 = oAprovisionadorNew.UON2 And oAprovisionadorLista.UON3 = oAprovisionadorNew.UON3 Then
                'Coinciden las unidades organizativas
                If Not oAprovisionadorNew.Departamento Is Nothing And Not oAprovisionadorLista.Departamento Is Nothing Then
                    If oAprovisionadorLista.Departamento.Cod = oAprovisionadorNew.Departamento.Cod Then
                        If Not oAprovisionadorNew.Persona Is Nothing And Not oAprovisionadorLista.Persona Is Nothing Then
                            'El aprovisionador es una persona
                            If oAprovisionadorLista.Persona.Cod = oAprovisionadorNew.Persona.Cod Then
                                'El aprovisionador es una persona y ya existe en la lista
                                ExisteAprovisionadorEnLista = True
                                Exit Function
                            End If
                        Else
                            If oAprovisionadorNew.Persona Is Nothing And oAprovisionadorLista.Persona Is Nothing Then
                                'El aprovisionador es un departamento y ya existe en la lista
                                ExisteAprovisionadorEnLista = True
                                Exit Function
                            End If
                        End If
                    End If
                Else
                    If oAprovisionadorNew.Departamento Is Nothing And oAprovisionadorLista.Departamento Is Nothing Then
                        'El aprovisionador es una unidad organizativa y ya existe en la lista
                        ExisteAprovisionadorEnLista = True
                        Exit Function
                    End If
                End If
            End If
        Next
    End If
    ExisteAprovisionadorEnLista = False
End Function

''' <summary>A�adira el aprovisionador a la lista</summary>
''' <param:name="oAprovisionadorNew">Aprovisionador a a�adir</param>
''' <return>True si a�ade el aprovisionador a la lista, false si ya existe y no lo a�ade</return>
Public Sub A�adirAprovisionadorEnEstructura(oAprovisionadorNew As cAprovisionador)
    Dim listItem As MSComctlLib.listItem
    Dim sTextoAprovisionador As String
    Dim sCodAprovisionador As String
    Dim sDenAprovisionador As String
    
    
        
    'a�adiremos al listview el aprovisionador
    If Not oAprovisionadorNew.Persona Is Nothing Then
        'El aprovisionador es una persona
        sTextoAprovisionador = oAprovisionadorNew.Persona.Cod & " - " & oAprovisionadorNew.Persona.Apellidos & ", " & oAprovisionadorNew.Persona.nombre & " - " & Format(oAprovisionadorNew.importe, "#,##0.00") & " " & gParametrosGenerales.gsMONCEN
        Set listItem = lstListAprovisionadores.ListItems.Add(, "L" & CStr(oAprovisionadorNew.Id), sTextoAprovisionador, , 3)
        listItem.Tag = CStr(oAprovisionadorNew.importe)
    ElseIf Not oAprovisionadorNew.Departamento Is Nothing Then
        'El aprovisionador es una departamento
        
        'Si el departamento cuelga de una unidad organizativa se le enlaza su codigo a la denominacion que aparecera en la lista
         If oAprovisionadorNew.UON3 <> "" Then
             sTextoAprovisionador = oAprovisionadorNew.UON1 & Space(2) & oAprovisionadorNew.UON2 & Space(2) & oAprovisionadorNew.UON3
         ElseIf oAprovisionadorNew.UON2 <> "" Then
             sTextoAprovisionador = oAprovisionadorNew.UON1 & Space(2) & oAprovisionadorNew.UON2
         Else
             sTextoAprovisionador = oAprovisionadorNew.UON1
         End If
        
        sTextoAprovisionador = sTextoAprovisionador & " - " & oAprovisionadorNew.Departamento.Cod & " - " & oAprovisionadorNew.Departamento.Den & " - " & Format(oAprovisionadorNew.importe, "#,##0.00") & " " & gParametrosGenerales.gsMONCEN
        Set listItem = lstListAprovisionadores.ListItems.Add(, "L" & CStr(oAprovisionadorNew.Id), sTextoAprovisionador, , 6)
        listItem.Tag = CStr(oAprovisionadorNew.importe)
    Else
        'El aprovisionador es una Unidad organizativa
         If oAprovisionadorNew.UON3 <> "" Then
             sDenAprovisionador = oAprovisionadorNew.DenUON3
             sCodAprovisionador = oAprovisionadorNew.UON1 & Space(2) & oAprovisionadorNew.UON2 & Space(2) & oAprovisionadorNew.UON3
         ElseIf oAprovisionadorNew.UON2 <> "" Then
             sDenAprovisionador = oAprovisionadorNew.DenUON2
             sCodAprovisionador = oAprovisionadorNew.UON1 & Space(2) & oAprovisionadorNew.UON2
         Else
             sDenAprovisionador = oAprovisionadorNew.DenUON1
             sCodAprovisionador = oAprovisionadorNew.UON1
         End If
         sTextoAprovisionador = sCodAprovisionador & " - " & sDenAprovisionador & " - " & Format(oAprovisionadorNew.importe, "#,##0.00") & " " & gParametrosGenerales.gsMONCEN
        
        'Comprobamos que nivel de Uon es para sacar el icono corresponiente
        If oAprovisionadorNew.UON3 <> "" Then
             Set listItem = lstListAprovisionadores.ListItems.Add(, "L" & CStr(oAprovisionadorNew.Id), sTextoAprovisionador, , 7)
             listItem.Tag = CStr(oAprovisionadorNew.importe)
        ElseIf oAprovisionadorNew.UON2 <> "" Then
             Set listItem = lstListAprovisionadores.ListItems.Add(, "L" & CStr(oAprovisionadorNew.Id), sTextoAprovisionador, , 5)
             listItem.Tag = CStr(oAprovisionadorNew.importe)
        Else
             Set listItem = lstListAprovisionadores.ListItems.Add(, "L" & CStr(oAprovisionadorNew.Id), sTextoAprovisionador, , 4)
             listItem.Tag = CStr(oAprovisionadorNew.importe)
        End If
    End If
    
    If oSeguridad.SeguridadConfigurada = False Then
        'Si no tienen la seguridad configurada y se esta configurando al a�adir el aprobador
        'se actualizaran las lineas de catalogo de esa categoria indicandole que tienen seguridad
        oSeguridad.ActualizarSeguridadLineasCatalogo
    End If
    
    'Recargamos la propiedad aprovisionadores del objeto seguridad para actualizar los aprovisionadores
    oSeguridad.CargarDatosSeguridad
End Sub
''' <summary>Eliminara el item de la lista de aprovisionadores con el aprovisionador por el que se ha sustituido</summary>
Public Sub EliminarAprovisionadorEnEstructura()
    Dim oItem As MSComctlLib.listItem
    
    Set oItem = lstListAprovisionadores.selectedItem
    
    If Not oItem Is Nothing Then
        'Eliminamos el aprovisionador del listView
        lstListAprovisionadores.ListItems.Remove oItem.Index
        Set oAproviSeleccionado = Nothing
    End If
    'Recargamos la propiedad aprovisionadores del objeto seguridad para actualizar los aprovisionadores
    oSeguridad.CargarDatosSeguridad
End Sub
''' <summary>Modificara el item de la lista de aprovisionadores con el aprovisionador por el que se ha sustituido</summary>
Public Sub ModificarAprovisionadorEnEstructura()
    Dim Item As MSComctlLib.listItem
    Dim sCodAprovisionador As String
    Dim sDenAprovisionador As String
    Dim sTextoAprovisionador As String 'Texto que aparecera en el item de la lista
    Dim key As String
    Set Item = lstListAprovisionadores.selectedItem
    
    If Not oAproviSeleccionado.Persona Is Nothing Then
        'El aprovisionador es una persona
        sTextoAprovisionador = oAproviSeleccionado.Persona.Cod & " - " & oAproviSeleccionado.Persona.nombre & " - " & Format(oAproviSeleccionado.importe, "#,##0.00") & " " & gParametrosGenerales.gsMONCEN
        'El aprovisionador es una persona
        If oAproviSeleccionado.UON3 <> "" Then
             key = "PER3" & oAproviSeleccionado.Persona.Cod & "#L" & CStr(oAproviSeleccionado.Id)
        ElseIf oAproviSeleccionado.UON2 <> "" Then
             key = "PER2" & oAproviSeleccionado.Persona.Cod & "#L" & CStr(oAproviSeleccionado.Id)
        ElseIf oAproviSeleccionado.UON1 <> "" Then
             key = "PER1" & oAproviSeleccionado.Persona.Cod & "#L" & CStr(oAproviSeleccionado.Id)
        Else
             key = "PER0" & oAproviSeleccionado.Persona.Cod & "#L" & CStr(oAproviSeleccionado.Id)
        End If
        Item.key = key
        Item.Tag = CStr(oAproviSeleccionado.importe)
        Item.Text = sTextoAprovisionador
        Item.SmallIcon = 3
    ElseIf Not oAproviSeleccionado.Departamento Is Nothing Then
        'El aprovisionador es una departamento
        If oAproviSeleccionado.UON3 <> "" Then
            sTextoAprovisionador = oAproviSeleccionado.UON1 & Space(2) & oAproviSeleccionado.UON2 & Space(2) & oAproviSeleccionado.UON3
            key = "DEP3" & oAproviSeleccionado.Departamento.Cod & "#L" & CStr(oAproviSeleccionado.Id)
        ElseIf oAproviSeleccionado.UON2 <> "" Then
            sTextoAprovisionador = oAproviSeleccionado.UON1 & Space(2) & oAproviSeleccionado.UON2
            key = "DEP2" & oAproviSeleccionado.Departamento.Cod & "#L" & CStr(oAproviSeleccionado.Id)
        ElseIf oAproviSeleccionado.UON1 <> "" Then
            sTextoAprovisionador = oAproviSeleccionado.UON1
            key = "DEP1" & oAproviSeleccionado.Departamento.Cod & "#L" & CStr(oAproviSeleccionado.Id)
        Else
            key = "DEP0" & oAproviSeleccionado.Departamento.Cod & "#L" & CStr(oAproviSeleccionado.Id)
        End If
        sTextoAprovisionador = IIf(sTextoAprovisionador <> "", sTextoAprovisionador & " - ", "") & oAproviSeleccionado.Departamento.Cod & " - " & oAproviSeleccionado.Departamento.Den & " - " & Format(oAproviSeleccionado.importe, "#,##0.00") & " " & gParametrosGenerales.gsMONCEN
        Item.key = key
        Item.Tag = CStr(oAproviSeleccionado.importe)
        Item.Text = sTextoAprovisionador
        Item.SmallIcon = 6
    Else
        'El aprovisionador es una Unidad organizativa
        If oAproviSeleccionado.UON3 <> "" Then
            sDenAprovisionador = oAproviSeleccionado.DenUON3
            sCodAprovisionador = oAproviSeleccionado.UON1 & Space(2) & oAproviSeleccionado.UON2 & Space(2) & oAproviSeleccionado.UON3
            key = "UON3" & oAproviSeleccionado.UON3 & "#L" & CStr(oAproviSeleccionado.Id)
        ElseIf oAproviSeleccionado.UON2 <> "" Then
            sDenAprovisionador = oAproviSeleccionado.DenUON2
            sCodAprovisionador = oAproviSeleccionado.UON1 & Space(2) & oAproviSeleccionado.UON2
            key = "UON2" & oAproviSeleccionado.UON1 & "#L" & CStr(oAproviSeleccionado.Id)
        Else
            sDenAprovisionador = oAproviSeleccionado.DenUON1
            sCodAprovisionador = oAproviSeleccionado.UON1
            key = "UON1" & oAproviSeleccionado.UON1 & "#L" & CStr(oAproviSeleccionado.Id)
        End If
        sTextoAprovisionador = sCodAprovisionador & " - " & sDenAprovisionador & " - " & Format(oAproviSeleccionado.importe, "#,##0.00") & " " & gParametrosGenerales.gsMONCEN
        'Comprobamos que nivel de Uon es, para sacar el icono corresponiente
        If oAproviSeleccionado.UON3 <> "" Then
             Item.key = key
             Item.Tag = CStr(oAproviSeleccionado.importe)
             Item.Text = sTextoAprovisionador
             Item.SmallIcon = 7
        ElseIf oAproviSeleccionado.UON2 <> "" Then
             Item.key = key
             Item.Tag = CStr(oAproviSeleccionado.importe)
             Item.Text = sTextoAprovisionador
             Item.SmallIcon = 5
        Else
             Item.key = key
             Item.Tag = CStr(oAproviSeleccionado.importe)
             Item.Text = sTextoAprovisionador
             Item.SmallIcon = 4
        End If
    End If
    
    'Recargamos la propiedad aprovisionadores del objeto seguridad para actualizar los aprovisionadores
    oSeguridad.CargarDatosSeguridad
End Sub

Private Sub cmdlistado_Click()

    '' * Objetivo: Obtener un listado del seguridad
    frmLstCATSeguridad.ACN1Seleccionada = 0
    frmLstCATSeguridad.ACN2Seleccionada = 0
    frmLstCATSeguridad.ACN3Seleccionada = 0
    frmLstCATSeguridad.ACN4Seleccionada = 0
    frmLstCATSeguridad.ACN5Seleccionada = 0
    frmLstCATSeguridad.lSeguridad = oSeguridad.Id
    frmLstCATSeguridad.sOrigen = "frmCATSeguridad"
    
    Select Case oSeguridad.NivelCat
    Case 1
        frmLstCATSeguridad.ACN1Seleccionada = oSeguridad.Categoria
        frmLstCATSeguridad.lCategoria = oSeguridad.Categoria
        frmLstCATSeguridad.lNivelCat = 1
    Case 2
        frmLstCATSeguridad.ACN2Seleccionada = oSeguridad.Categoria
        frmLstCATSeguridad.lCategoria = oSeguridad.Categoria
        frmLstCATSeguridad.lNivelCat = 2
    Case 3
        frmLstCATSeguridad.ACN3Seleccionada = oSeguridad.Categoria
        frmLstCATSeguridad.lCategoria = oSeguridad.Categoria
        frmLstCATSeguridad.lNivelCat = 3
    Case 4
        frmLstCATSeguridad.ACN4Seleccionada = oSeguridad.Categoria
        frmLstCATSeguridad.lCategoria = oSeguridad.Categoria
        frmLstCATSeguridad.lNivelCat = 4
    Case 5
        frmLstCATSeguridad.ACN5Seleccionada = oSeguridad.Categoria
        frmLstCATSeguridad.lCategoria = oSeguridad.Categoria
        frmLstCATSeguridad.lNivelCat = 5
    End Select

    frmLstCATSeguridad.lblCategoria = NombreRama
    frmLstCATSeguridad.Show 1

End Sub

''' <summary>Carga en la grid los flujos de aprobacion de los pedidos web de una categoria</summary>
Private Sub CargarGridFlujosAprobacionCategoria()
    Dim oFlujoAprobacion As cFlujoAprobacion
    'Cargamos los flujos de aprobacion para la seguridad de esta categoria
    oSeguridad.cargarflujosaprobacion Me.sdbcTipoPedido.Value, sdbcEmp.Value, Me.sdbcFlujos.Value
    
    'Cargamos la grid con los flujos
    sdbgFlujosAprobacion.RemoveAll
    For Each oFlujoAprobacion In oSeguridad.FlujosAprobacion
        sdbgFlujosAprobacion.AddItem oFlujoAprobacion.TipoPedidoDen & Chr(m_lSeparador) & oFlujoAprobacion.TipoPedido & Chr(m_lSeparador) & oFlujoAprobacion.Empden & Chr(m_lSeparador) & oFlujoAprobacion.Emp & Chr(m_lSeparador) & oFlujoAprobacion.SolicitudDen & Chr(m_lSeparador) & oFlujoAprobacion.Solicitud & Chr(m_lSeparador) & oFlujoAprobacion.tiposolicitud & Chr(m_lSeparador) & oFlujoAprobacion.Formulario & Chr(m_lSeparador) & oFlujoAprobacion.Workflow
    Next
    
End Sub


''' <summary>
''' Carga las solicitudes del tipo Solicitud de pedido para a�adir al combo de flujos de aprobacion
''' </summary>
Private Sub CargarSolicitudes()
    Dim oRes As ADODB.Recordset
    Dim oSolicitudes As CSolicitudes
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    Set oRes = oSolicitudes.DevolverSolicitudesDeTipo(tiposolicitud.SolicitudDePedidoCatalogo)
    sdbcFlujos.RemoveAll
    Me.sdbcFlujos.AddItem "" & Chr(m_lSeparador) & ""
    While Not oRes.EOF
        Me.sdbcFlujos.AddItem oRes("ID").Value & Chr(m_lSeparador) & oRes("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador)
        oRes.MoveNext
    Wend
End Sub

''' <summary>
''' Carga todos los tipos de pedido que esten asociados a la categoria
''' </summary>
''' <remarks>Llamada desde:Form_load; Tiempo m�ximo:0,3seg.</remarks>
Private Sub CargarTipoPedidos()
    Dim oTiposPedido As CTiposPedido
    Dim oTipoPedido As CTipoPedido
    Set oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
    
    'Cargo todos los tipos de pedidos
    oTiposPedido.CargarTiposPedidosPorFlujoDeAprobacion gParametrosInstalacion.gIdioma, oSeguridad.Categoria, oSeguridad.NivelCat
    sdbcTipoPedido.RemoveAll
    Me.sdbcTipoPedido.AddItem "" & Chr(m_lSeparador) & ""
    For Each oTipoPedido In oTiposPedido
       Me.sdbcTipoPedido.AddItem oTipoPedido.Id & Chr(m_lSeparador) & oTipoPedido.Den
    Next
    
    Set oTiposPedido = Nothing
End Sub

''' <summary>
''' Carga las empresas para el combo
''' </summary>
Private Sub CargarEmpresas()
    Dim oRes As ADODB.Recordset
    Dim oEmpresas As CEmpresas
    Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
    Set oRes = oEmpresas.DevolverEmpresasPorCategoria(oSeguridad.Categoria, oSeguridad.NivelCat)
    sdbcEmp.RemoveAll
    Me.sdbcEmp.AddItem "" & Chr(m_lSeparador) & ""
    While Not oRes.EOF
        Me.sdbcEmp.AddItem oRes("EMP_ID").Value & Chr(m_lSeparador) & oRes("DEN").Value
        oRes.MoveNext
    Wend
    Set oEmpresas = Nothing
    Set oRes = Nothing
End Sub


''' <summary>
''' Inicializa el combo de Tipos de Pedido
''' </summary>
Private Sub sdbcTipoPedido_InitColumnProps()
    sdbcTipoPedido.DataFieldList = "Column 0"
    sdbcTipoPedido.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Inicializa el combo de Flujos
''' </summary>
Private Sub sdbcFlujos_InitColumnProps()
    sdbcFlujos.DataFieldList = "Column 0"
    sdbcFlujos.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Inicializa el combo de Empresas
''' </summary>
Private Sub sdbcEmp_InitColumnProps()
    sdbcEmp.DataFieldList = "Column 0"
    sdbcEmp.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcFlujos_CloseUp()
    CargarGridFlujosAprobacionCategoria
End Sub

Private Sub sdbcFlujos_LostFocus()
    CargarGridFlujosAprobacionCategoria
End Sub
Private Sub sdbcEmp_CloseUp()
    CargarGridFlujosAprobacionCategoria
End Sub
Private Sub sdbcEmp_LostFocus()
    CargarGridFlujosAprobacionCategoria
End Sub
Private Sub sdbcTipoPedido_CloseUp()
    CargarGridFlujosAprobacionCategoria
End Sub

Private Sub sdbcTipoPedido_LostFocus()
    CargarGridFlujosAprobacionCategoria
End Sub

''''''''''''''''''''''''''''''''''''
'''EVENTOS GRID FLUJOS APROBACION'''
''''''''''''''''''''''''''''''''''''

''' <summary>Evento que se produce al cambiar de fila o de celda en el grid</summary>
Private Sub sdbgFlujosAprobacion_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If Me.sdbgFlujosAprobacion.Col <> -1 Then
        If sdbgFlujosAprobacion.Columns(Me.sdbgFlujosAprobacion.Col).Name = "SOLICITUD" Then
            sdbgFlujosAprobacion.Columns("SOLICITUD").Style = ssStyleEditButton
            sdbgFlujosAprobacion.Columns("SOLICITUD").Locked = True
        End If
    End If
End Sub
''' <summary>Evento que se produce al pulsar el boton de una celda</summary>
Private Sub sdbgFlujosAprobacion_BtnClick()
    If sdbgFlujosAprobacion.Columns(Me.sdbgFlujosAprobacion.Col).Name = "SOLICITUD" Then
        frmFlujos.m_bModifFlujo = True
        frmFlujos.m_lIdFlujo = sdbgFlujosAprobacion.Columns("WORKFLOW").Value
        frmFlujos.m_lIdFormulario = sdbgFlujosAprobacion.Columns("FORMULARIO").Value
        frmFlujos.m_sSolicitud = sdbgFlujosAprobacion.Columns("SOLICITUD").Value
        frmFlujos.g_lSolicitud = sdbgFlujosAprobacion.Columns("SOLICITUD_HIDDEN").Value
        frmFlujos.g_lSolicitudTipo = sdbgFlujosAprobacion.Columns("TIPOSOLICITUD").Value
        frmFlujos.lblDenFlujo.caption = ""
        frmFlujos.g_iSolicitudTipoTipo = sdbgFlujosAprobacion.Columns("SOLICITUD_HIDDEN").Value
        frmFlujos.Show vbModal
    End If
End Sub
''' <summary>Evento que se produce antes de eliminar una fila del grid y evita que salte el mensaje de eliminar propio del grid</summary>
Private Sub sdbgFlujosAprobacion_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub
