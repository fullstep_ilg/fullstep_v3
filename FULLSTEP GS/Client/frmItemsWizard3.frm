VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmItemsWizard3 
   Caption         =   "Distribuci�n de la compra"
   ClientHeight    =   5190
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6930
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmItemsWizard3.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5190
   ScaleWidth      =   6930
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   1175
      ScaleHeight     =   495
      ScaleWidth      =   4695
      TabIndex        =   14
      Top             =   4700
      Width           =   4700
      Begin VB.CommandButton cmdVolver 
         Caption         =   "<<             "
         Height          =   315
         Left            =   1200
         TabIndex        =   18
         Top             =   100
         Width           =   1095
      End
      Begin VB.CommandButton cmdFinalizar 
         Caption         =   "&Finalizar"
         Height          =   315
         Left            =   3600
         TabIndex        =   17
         Top             =   100
         Width           =   1095
      End
      Begin VB.CommandButton cmdContinuar 
         Caption         =   "           >>"
         Default         =   -1  'True
         Height          =   315
         Left            =   2400
         TabIndex        =   16
         Top             =   100
         Width           =   1095
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   0
         TabIndex        =   15
         Top             =   100
         Width           =   1095
      End
   End
   Begin VB.PictureBox picPorcen 
      Height          =   495
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   6720
      TabIndex        =   13
      Top             =   4170
      Width           =   6780
      Begin VB.TextBox txtPorcentaje 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1170
         TabIndex        =   9
         Top             =   90
         Width           =   615
      End
      Begin VB.TextBox txtCantidad 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2775
         TabIndex        =   11
         Top             =   90
         Width           =   1155
      End
      Begin MSComctlLib.Slider Slider1 
         Height          =   315
         Left            =   4665
         TabIndex        =   12
         Top             =   60
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         Max             =   100
      End
      Begin VB.Label lblCantidad 
         Caption         =   "Cantidad:"
         Height          =   195
         Left            =   1890
         TabIndex        =   10
         Top             =   120
         Width           =   855
      End
      Begin VB.Label lblPorcentaje 
         Caption         =   "Porcentaje:"
         Height          =   195
         Left            =   60
         TabIndex        =   8
         Top             =   120
         Width           =   1035
      End
   End
   Begin MSComctlLib.TreeView tvwestrorg 
      Height          =   3375
      Left            =   60
      TabIndex        =   7
      Top             =   750
      Width           =   6780
      _ExtentX        =   11959
      _ExtentY        =   5953
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   500
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard3.frx":0CB2
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard3.frx":0D50
            Key             =   "UON3A"
            Object.Tag             =   "UON3A"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard3.frx":0E10
            Key             =   "UON2A"
            Object.Tag             =   "UON2A"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard3.frx":0ED2
            Key             =   "UON1A"
            Object.Tag             =   "UON1A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard3.frx":0F92
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard3.frx":1024
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard3.frx":10D4
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblCantSinDist 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   5340
      TabIndex        =   6
      Top             =   420
      Width           =   1380
   End
   Begin VB.Label lblCantDist 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1860
      TabIndex        =   3
      Top             =   420
      Width           =   1380
   End
   Begin VB.Label lblDist 
      Caption         =   "Distribu�do:"
      Height          =   195
      Left            =   0
      TabIndex        =   1
      Top             =   450
      Width           =   1065
   End
   Begin VB.Label lblPorcenDist 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1065
      TabIndex        =   2
      Top             =   420
      Width           =   780
   End
   Begin VB.Label lblPorcenSinDist 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   4545
      TabIndex        =   5
      Top             =   420
      Width           =   780
   End
   Begin VB.Label lblSinDist 
      Caption         =   "Sin distribuir:"
      Height          =   195
      Left            =   3285
      TabIndex        =   4
      Top             =   450
      Width           =   1185
   End
   Begin VB.Label lblSel 
      Caption         =   "Seleccione las unidades oganizativas a las que imputar la compra"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   6600
   End
End
Attribute VB_Name = "frmItemsWizard3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Unidades organizativas
Private m_oUnidadesOrgN1 As CUnidadesOrgNivel1
Private m_oUnidadesOrgN2 As CUnidadesOrgNivel2
Private m_oUnidadesOrgN3 As CUnidadesOrgNivel3

Private dDistPorcen As Double
Private dSinCantidad As Double
Private dDistCantidad As Double
Private dCantidad As Double
Private bNoCantidad As Boolean

Private oDistsNivel1 As CDistItemsNivel1
Private oDistsNivel2 As CDistItemsNivel2
Private oDistsNivel3 As CDistItemsNivel3

'Restricciones
Private m_bRuo As Boolean

Private m_stexto As String

Private m_bRespetarPorcen As Boolean
Private m_bCancelar As Boolean
Private m_bTodosValidos As Boolean
Private m_sUONMostrar As String
Private m_sCodsItemsNoDistribuibles As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean

Private m_sMsgError As String
''' <summary>Comprueba que una distribuci�n en UONs es valida para los �tems</summary>
''' <remarks>Llamada desde: cmdContinuar_Click, cmdFinalizar_Click, cmdVolver_Click; Tiempo m�ximo: 0</remarks>
''' <revision>EPB 13/09/2012<revision>
Private Sub AsignarDistrib()
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nodx As MSComctlLib.node
Dim dPorcen As Double
Dim vCad1 As Variant
Dim vCad2 As Variant
Dim vCad3 As Variant
Dim oItem As CItem
Dim sDist As String
   
Dim i As Integer
Dim bEsValido As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
bEsValido = False
    
    Set nodx = tvwestrorg.Nodes(1)
   
    If nodx.Children = 0 Then Exit Sub
   
    dPorcen = 0
    
    Set nod1 = nodx.Child
    
    While Not nod1 Is Nothing
        
        ' No distribu�do
        If nod1.Image = "UON1" Then
            
            Set nod2 = nod1.Child
            While Not nod2 Is Nothing
                ' No distribu�do
                If nod2.Image = "UON2" Then
                    Set nod3 = nod2.Child
                    While Not nod3 Is Nothing
                        If nod3.Image = "UON3A" Then
                            If DevolverPorcentaje(nod3) > dPorcen Then
                                vCad1 = Right(nod3.Parent.Parent.Tag, Len(nod3.Parent.Parent.Tag) - 4)
                                vCad2 = Right(nod3.Parent.Tag, Len(nod3.Parent.Tag) - 4)
                                vCad3 = Right(nod3.Tag, Len(nod3.Tag) - 4)
                                dPorcen = DevolverPorcentaje(nod3)
                            End If
                        End If
                        Set nod3 = nod3.Next
                    Wend
                Else
                    ' Si est� distribu�do UON2
                    If DevolverPorcentaje(nod2) > dPorcen Then
                        vCad1 = Right(nod2.Parent.Tag, Len(nod2.Parent.Tag) - 4)
                        vCad2 = Right(nod2.Tag, Len(nod2.Tag) - 4)
                        vCad3 = Null
                        dPorcen = DevolverPorcentaje(nod2)
                    End If
                End If
                Set nod2 = nod2.Next
            Wend
        Else
        ' Si est� distribu�do UON1
            If DevolverPorcentaje(nod1) > dPorcen Then
                vCad1 = Right(nod1.Tag, Len(nod1.Tag) - 4)
                vCad2 = Null
                vCad3 = Null
                dPorcen = DevolverPorcentaje(nod1)
            End If
        End If
        Set nod1 = nod1.Next
    Wend
     
        
    'Comprobamos que se puede distribuir en la UON que lo estamos haciendo.
   Set oItem = oFSGSRaiz.Generar_CItem
   Dim iValidoN1 As Integer
   iValidoN1 = 0
   Dim iValidoN2 As Integer
   iValidoN2 = 0
   Dim iNoValidoN2 As Integer
   iNoValidoN2 = 0
   
   Dim iValidoN3 As Integer
   iValidoN3 = 0
  
    m_bTodosValidos = True
    m_sCodsItemsNoDistribuibles = ""
    If frmPROCE.g_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    For i = 1 To frmItemsWizard.g_oItems.Count
    
        Dim oUon As IUOn
        Dim oArticulo As CArticulo
        Dim o As IUOn
        Dim dist1 As CDistItemNivel1
        Dim dist2 As CDistItemNivel2
        Dim dist3 As CDistItemNivel3
        Dim bItemvalido As Boolean
        bItemvalido = True
        Dim oUons As CUnidadesOrganizativas
        Set oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
        Set oArticulo = oFSGSRaiz.Generar_CArticulo
        oArticulo.Cod = frmItemsWizard.g_oItems.Item(i).ArticuloCod
                
        If Not oDistsNivel1 Is Nothing Then
            For Each dist1 In oDistsNivel1
                Set oUon = oUons.createUon(dist1.CodUON1, "", "", "")
                bItemvalido = False
                For Each o In oArticulo.uons
                    If o.incluye(oUon) Or oUon.incluye(o) Then
                        bItemvalido = True
                        Exit For
                    End If
                Next
            Next
        End If
         
        If bItemvalido Then
            If Not oDistsNivel2 Is Nothing Then
                For Each dist2 In oDistsNivel2
                    Set oUon = oUons.createUon(dist2.CodUON1, dist2.CodUON2, "", "")
                                    
                    bItemvalido = False
                    For Each o In oArticulo.uons
                        If o.incluye(oUon) Or oUon.incluye(o) Then
                            bItemvalido = True
                            Exit For
                        End If
                    Next
                Next
            End If
        End If
        
        If bItemvalido Then
            If Not oDistsNivel3 Is Nothing Then
                For Each dist3 In oDistsNivel3
                    Set oUon = oUons.createUon(dist3.CodUON1, dist3.CodUON2, dist3.CodUON3, "")
                    
                    bItemvalido = False
                    For Each o In oArticulo.uons
                        If o.incluye(oUon) Or oUon.incluye(o) Then
                            bItemvalido = True
                            Exit For
                        End If
                    Next
                Next
            End If
        End If
        
        If Not bItemvalido Then
            m_bTodosValidos = False
            oMensajes.ImposibleAnyadirItemIndividualADistribucion oArticulo.uons.toString, 1
            Exit Sub
        End If
            
        
    Next
                
    Set oItem = Nothing
    Screen.MousePointer = vbNormal
    'If m_bTodosValidos Then
        If NullToStr(vCad3) <> "" Then
            sDist = vCad1 & "-" & vCad2 & "-" & vCad3
        ElseIf NullToStr(vCad2) <> "" Then
            sDist = vCad1 & "-" & vCad2
        Else
            sDist = NullToStr(vCad1)
        End If
        
        frmItemsWizard.g_sDistrib = sDist
        
        Set frmItemsWizard.g_oDistsNivel1 = oDistsNivel1
        Set frmItemsWizard.g_oDistsNivel2 = oDistsNivel2
        Set frmItemsWizard.g_oDistsNivel3 = oDistsNivel3
    
    'End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "AsignarDistrib", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCancelar = True
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdContinuar_Click()

    'Controlar la selecci�n
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If dSinCantidad = dCantidad And Not bNoCantidad Then
        oMensajes.ProceDistObligatoria
        Exit Sub
    End If
        
    m_sUONMostrar = ""
    AsignarDistrib
    
    If Not m_bTodosValidos Then
        oMensajes.ImposibleAnyadirItemADistribucion m_sUONMostrar, m_sCodsItemsNoDistribuibles, TipoDefinicionDatoProceso.EnItem
        Screen.MousePointer = vbNormal
                       
        Exit Sub
    End If
        
    '**PRESANU1 (en ITEM o en grupo pero no en el grupo del �tem)
    If (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo1)) Then
        If gParametrosGenerales.gbUsarPres1 Then
            m_bCancelar = False
            Unload Me
            frmItemsWizard4.g_iTipoPres = 1
            frmItemsWizard4.Show 1
            Exit Sub
        End If
    Else
        If Not gParametrosGenerales.gbPresupuestosAut Then
            If gParametrosGenerales.gbUsarPres1 Then
                If Not frmItemsWizard.g_bPresAnu1OK Then
                    'Si no est� en el �tem hay que mirar si hace falta redistribuirlo antes de pasar al siguiente presupuesto
                    If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso Then 'PRESANU2 en PROCE
                        If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 Then
                            frmItemsWizard.ReasignarPresup 2, True
                        End If
                    Else
                        If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo Then 'PRESANU2 en GRUPO
                            If frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 Then
                                frmItemsWizard.ReasignarPresup 2, False
                            End If
                        End If
                    End If
                End If
            End If
        End If
        '**PRESANU2 (en ITEM o en grupo pero no en el grupo del �tem)
        If (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo2)) Then
            If gParametrosGenerales.gbUsarPres2 Then
                m_bCancelar = False
                Unload Me
                frmItemsWizard4.g_iTipoPres = 2
                frmItemsWizard4.Show 1
                Exit Sub
            End If
        Else
            If Not gParametrosGenerales.gbPresupuestosAut Then
                If gParametrosGenerales.gbUsarPres2 Then
                    If Not frmItemsWizard.g_bPresAnu2OK Then
                        'Si no est� en el �tem hay que mirar si hace falta redistribuirlo antes de pasar al siguiente presupuesto
                        If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso Then 'PRESANU2 en PROCE
                            If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 Then
                                frmItemsWizard.ReasignarPresup 2, True
                            End If
                        Else
                            If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo Then 'PRESANU2 en GRUPO
                                If frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 Then
                                    frmItemsWizard.ReasignarPresup 2, False
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            '**PRES1 (en ITEM o en grupo pero no en el grupo del �tem)
            If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo1)) Then
                If gParametrosGenerales.gbUsarPres3 Then
                    m_bCancelar = False
                    Unload Me
                    frmItemsWizard4.g_iTipoPres = 3
                    frmItemsWizard4.Show 1
                    Exit Sub
                End If
            Else
                If Not gParametrosGenerales.gbPresupuestosAut Then
                    If Not frmItemsWizard.g_bPres1OK Then
                        If gParametrosGenerales.gbUsarPres3 Then
                            'Si no est� en el �tem hay que mirar si hace falta redistribuirlo antes de pasar al siguiente presupuesto
                            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso Then 'PRES1 en PROCE
                                If frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                                    frmItemsWizard.ReasignarPresup 3, True
                                End If
                            Else
                                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo Then 'PRES1 en GRUPO
                                    If frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 Then
                                        frmItemsWizard.ReasignarPresup 3, False
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
                '**PRES2 (en ITEM o en grupo pero no en el grupo del �tem)
                If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo2)) Then
                    If gParametrosGenerales.gbUsarPres4 Then
                        m_bCancelar = False
                        Unload Me
                        frmItemsWizard4.g_iTipoPres = 4
                        frmItemsWizard4.Show 1
                        Exit Sub
                    End If
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "cmdContinuar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>
''' Finaliza la insercion de datos del item
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdFinalizr_click sistema; Tiempo m�ximo:0</remarks>

Private Sub cmdFinalizar_Click()
    Dim teserror As TipoErrorSummit
    Dim bItems As Boolean

    'Controlar la selecci�n
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If CDbl(lblCantSinDist.caption) = dCantidad And Not bNoCantidad Then
        oMensajes.ProceDistObligatoria
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    m_sUONMostrar = ""
    AsignarDistrib

    If Not m_bTodosValidos Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    If Not gParametrosGenerales.gbPresupuestosAut Then
        'Si alguno de los presupuestos est� en proce o grupo reasignar presupuestos
        '**PRESANU1
        If gParametrosGenerales.gbUsarPres1 Then
            If Not frmItemsWizard.g_bPresAnu1OK Then
                If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso Then 'PRESANU2 en PROCE
                    If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 Then
                        frmItemsWizard.ReasignarPresup 2, True
                    End If
                Else
                    If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo Then 'PRESANU2 en GRUPO
                        If frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 Then
                            frmItemsWizard.ReasignarPresup 2, False
                        End If
                    End If
                End If
             End If
        End If
        '**PRESANU2
        If gParametrosGenerales.gbUsarPres2 Then
            If Not frmItemsWizard.g_bPresAnu2OK Then
                If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso Then 'PRESANU2 en PROCE
                    If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 Then
                        frmItemsWizard.ReasignarPresup 2, True
                    End If
                Else
                    If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo Then 'PRESANU2 en GRUPO
                        If frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 Then
                            frmItemsWizard.ReasignarPresup 2, False
                        End If
                    End If
                End If
            End If
        End If
        '***PRES1
        If gParametrosGenerales.gbUsarPres3 Then
            If Not frmItemsWizard.g_bPres1OK Then
                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso Then 'PRES1 en PROCE
                    If frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                        frmItemsWizard.ReasignarPresup 3, True
                    End If
                Else
                    If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo Then 'PRES1 en GRUPO
                        If frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 Then
                            frmItemsWizard.ReasignarPresup 3, False
                        End If
                    End If
                End If
            End If
        End If
        '***PRES2
        If gParametrosGenerales.gbUsarPres4 Then
            If Not frmItemsWizard.g_bPres2OK Then
                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Then 'PRES1 en PROCE
                    If frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 Then
                        frmItemsWizard.ReasignarPresup 4, True
                    End If
                Else
                    If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo Then 'PRES1 en GRUPO
                        If frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 Then
                            frmItemsWizard.ReasignarPresup 4, False
                        End If
                    End If
                End If
            End If
        End If
    End If
    
    If Not IsEmpty(frmPROCE.g_oProcesoSeleccionado.Plantilla) And Not IsNull(frmPROCE.g_oProcesoSeleccionado.Plantilla) Then
        If frmPROCE.g_oProcesoSeleccionado.DefEspItems Then
            frmPROCE.m_bAtribItemPlant = True
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    teserror = frmItemsWizard.AnyadirMultiplesItems(frmItemsWizard.g_vValores(4), frmItemsWizard.g_vValores(6), frmItemsWizard.HayUON, frmItemsWizard.HayPresup, frmItemsWizard.g_vValores(15), frmItemsWizard.g_vValores(17))
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        
        If teserror.NumError = TESVolumenAdjDirSuperado Then
            If frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then basErrores.TratarError teserror
            If Not frmPROCE.g_oProcesoSeleccionado.PermSaltarseVolMaxAdjDir Then
                m_bCancelar = True
                Unload Me
                Exit Sub
            End If
        Else
            basErrores.TratarError teserror
            m_bCancelar = True
            Unload Me
            Exit Sub
        End If
    End If
    
    frmItemsWizard.AnyadirItemsAGridApertura
    
    If frmItemsWizard.g_oItems.Count > 1 Then bItems = True
    
    m_bCancelar = True
    Unload Me
    
    oMensajes.ItemsAnyadidosAProceOK bItems

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "cmdFinalizar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub cmdVolver_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    AsignarDistrib
    Screen.MousePointer = vbNormal
    
    m_bCancelar = False
    Unload Me
    frmItemsWizard2.MostrarValoresSeleccionados
    frmItemsWizard2.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "cmdVolver_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
    If Me.Visible Then tvwestrorg.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
m_bDescargarFrm = False
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarRecursos
    
    ConfigurarSeguridad
        
    Set m_oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set m_oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set m_oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3

    If NoHayParametro(frmItemsWizard.g_oItems.Item(1).Cantidad) Then bNoCantidad = True
    dCantidad = StrToDbl0(frmItemsWizard.g_oItems.Item(1).Cantidad)
    If bNoCantidad Then txtCantidad.Locked = True

    GenerarEstructuraOrg False
    Set oDistsNivel1 = frmItemsWizard.g_oDistsNivel1
    Set oDistsNivel2 = frmItemsWizard.g_oDistsNivel2
    Set oDistsNivel3 = frmItemsWizard.g_oDistsNivel3
    
    If oDistsNivel1 Is Nothing Then
        Set oDistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
    End If
    If oDistsNivel2 Is Nothing Then
        Set oDistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
    End If
    If oDistsNivel3 Is Nothing Then
        Set oDistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
    End If

    tvwestrorg_NodeClick tvwestrorg.Nodes(1)
    
    m_bCancelar = True
        
    If Not frmPROCE.sdbgItems.Columns("PRESUP").Visible Then
        cmdContinuar.Visible = False
        cmdFinalizar.Left = cmdContinuar.Left
        cmdFinalizar.Default = True
    Else
'        HabilitarControlesConfigurables
    
        If (gParametrosGenerales.gbUsarPres1 And (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo1)) And (gParametrosGenerales.gbOBLPP)) Then
            cmdFinalizar.Visible = False
        Else
            If (gParametrosGenerales.gbUsarPres2 And (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo2)) And (gParametrosGenerales.gbOBLPC)) Then
                cmdFinalizar.Visible = False
            Else
                If (gParametrosGenerales.gbUsarPres3 And (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo1)) And (gParametrosGenerales.gbOBLPres3)) Then
                    cmdFinalizar.Visible = False
                Else
                    If (gParametrosGenerales.gbUsarPres4 And (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo2)) And (gParametrosGenerales.gbOBLPres4)) Then
                        cmdFinalizar.Visible = False
                    End If
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>
''' Genera el arbol de UONs
''' </summary>
''' <param name=bOrdenadoPorDen>Para ordenar por denominaci�n</param>
''' <returns></returns>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo:0</remarks>
''' <revision>EPB 13/09/2012<revision>
Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3
Dim nodx As node

Dim adoRecordset As ADODB.Recordset
Dim UON1tmp As Variant
Dim UON2tmp As Variant
Dim UON3tmp As Variant

Dim oItem As CItem

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next

    tvwestrorg.Nodes.clear
    
    Screen.MousePointer = vbHourglass

    Set oItem = oFSGSRaiz.Generar_CItem
    
    If oDistsNivel1 Is Nothing Then
        Set oDistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
    End If
    If oDistsNivel2 Is Nothing Then
        Set oDistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
    End If
    If oDistsNivel3 Is Nothing Then
        Set oDistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
    End If

    Select Case gParametrosGenerales.giNEO

        Case 1
                m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, , , basOptimizacion.gCodDepUsuario, m_bRuo, , , , , bOrdenadoPorDen
                    
                If frmItemsWizard.g_oDistsNivel1 Is Nothing And frmItemsWizard.g_oDistsNivel2 Is Nothing And frmItemsWizard.g_oDistsNivel3 Is Nothing Then
                ' Aqui solo entra si se esta a�adiendo el item
                    Set oItem.proceso = frmPROCE.g_oProcesoSeleccionado
                    oItem.GrupoID = frmPROCE.g_oGrupoSeleccionado.Id
                    Set adoRecordset = oItem.DevolverUONsNivelItems(frmItemsWizard.g_oItems.Item(1).ArticuloCod, 1)
                    If Not adoRecordset Is Nothing Then
                        UON1tmp = Null
                        UON1tmp = NullToStr(adoRecordset("UON1").Value)
                        oDistsNivel1.Add oItem, UON1tmp, 1
                        adoRecordset.Close
                        Set adoRecordset = Nothing
                    End If
                    
                    
                End If
                    
        Case 2
               m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , basOptimizacion.gCodDepUsuario, m_bRuo, , , , , bOrdenadoPorDen
               m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , basOptimizacion.gCodDepUsuario, m_bRuo, , , , , bOrdenadoPorDen
               
                If frmItemsWizard.g_oDistsNivel1 Is Nothing And frmItemsWizard.g_oDistsNivel2 Is Nothing And frmItemsWizard.g_oDistsNivel3 Is Nothing Then
                    Set oItem.proceso = frmPROCE.g_oProcesoSeleccionado
                    oItem.GrupoID = frmPROCE.g_oGrupoSeleccionado.Id
                    Set adoRecordset = oItem.DevolverUONsNivelItems(frmItemsWizard.g_oItems.Item(1).ArticuloCod, 2)
                    If Not adoRecordset Is Nothing Then
                        UON1tmp = Null
                        UON2tmp = Null
                        UON1tmp = NullToStr(adoRecordset("UON1").Value)
                        If Not IsNull(adoRecordset("UON2").Value) Then
                            UON2tmp = NullToStr(adoRecordset("UON2").Value)
                                oDistsNivel2.Add oItem, UON1tmp, UON2tmp, 1
                        Else
                            oDistsNivel1.Add oItem, UON1tmp, 1
                            
                        End If
                        
                        adoRecordset.Close
                        Set adoRecordset = Nothing
                    End If
                End If
                    
                
               
        Case 3
            m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRuo, , , , , bOrdenadoPorDen
            m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRuo, , , , , bOrdenadoPorDen
            m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRuo, , , , , bOrdenadoPorDen

            If frmItemsWizard.g_oDistsNivel1 Is Nothing And frmItemsWizard.g_oDistsNivel2 Is Nothing And frmItemsWizard.g_oDistsNivel3 Is Nothing Then
                Set oItem.proceso = frmPROCE.g_oProcesoSeleccionado
                oItem.GrupoID = frmPROCE.g_oGrupoSeleccionado.Id
            
                Set adoRecordset = oItem.DevolverUONsNivelItems(frmItemsWizard.g_oItems.Item(1).ArticuloCod, 3)
                If Not adoRecordset Is Nothing Then
                    UON1tmp = Null
                    UON2tmp = Null
                    UON3tmp = Null
                    UON1tmp = NullToStr(adoRecordset("UON1").Value)
                    If Not IsNull(adoRecordset("UON2").Value) Then
                        UON2tmp = NullToStr(adoRecordset("UON2").Value)
                        If Not IsNull(adoRecordset("UON3").Value) Then
                                UON3tmp = NullToStr(adoRecordset("UON3").Value)
                                oDistsNivel3.Add oItem, UON1tmp, UON2tmp, UON3tmp, 1
                        Else
                            oDistsNivel2.Add oItem, UON1tmp, UON2tmp, 1
                        End If
                    Else
                        oDistsNivel1.Add oItem, UON1tmp, 1
                    End If
                    
                    
                    adoRecordset.Close
                    Set adoRecordset = Nothing
                    
                End If
            End If
                    
    End Select
    
    Set oItem = Nothing
    
    dDistPorcen = 0

    lblPorcenDist.caption = CStr(dDistPorcen * 100) & " %"
    If Not bNoCantidad Then lblCantDist.caption = Format(CStr(dCantidad * dDistPorcen), "Standard")
    lblPorcenSinDist.caption = CStr((1 - dDistPorcen) * 100) & " %"
    If Not bNoCantidad Then lblCantSinDist.caption = Format(CStr(dCantidad * (1 - dDistPorcen)), "Standard")

   '************************************************************
    'Generamos la estructura arborea

    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True

    For Each oUON1 In m_oUnidadesOrgN1

        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        nodx.Tag = "UON1" & CStr(oUON1.Cod)

    Next

    For Each oUON2 In m_oUnidadesOrgN2

        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON1" & scod1 & "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        nodx.Tag = "UON2" & CStr(oUON2.Cod)

    Next

    For Each oUON3 In m_oUnidadesOrgN3

        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1 & "UON2" & scod2, tvwChild, "UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        nodx.Tag = "UON3" & CStr(oUON3.Cod)

    Next

    Set nodx = Nothing
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing

    Screen.MousePointer = vbNormal
    
    If frmItemsWizard.g_oDistsNivel1 Is Nothing And frmItemsWizard.g_oDistsNivel2 Is Nothing And frmItemsWizard.g_oDistsNivel3 Is Nothing Then
        MostrarDistribSeleccionadaPorDefecto
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "GenerarEstructuraOrg", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub ConfigurarSeguridad()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUOPers)) Is Nothing) Then
            m_bRuo = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "ConfigurarSeguridad", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub CargarRecursos()
    Dim Adores As Ador.Recordset
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_ITEMS_WIZARD3, basPublic.gParametrosInstalacion.gIdioma)

    If Not Adores Is Nothing Then
        Me.caption = Adores(0).Value
        Adores.MoveNext
        lblSel.caption = Adores(0).Value
        Adores.MoveNext
        lblDist.caption = Adores(0).Value
        Adores.MoveNext
        lblSinDist.caption = Adores(0).Value
        Adores.MoveNext
        lblPorcentaje.caption = Adores(0).Value
        Adores.MoveNext
        lblCantidad.caption = Adores(0).Value
        Adores.MoveNext
        cmdFinalizar.caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value

        Adores.Close

    End If
    
    Set Adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "CargarRecursos", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

''' <summary>
''' Resize de la pantalla
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Form_Load; Cualquier cambio de tama�o de la pantalla; Tiempo m�ximo:0</remarks>
Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If Me.Height < 2000 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    tvwestrorg.Height = Me.Height - 2250
    picPorcen.Top = Me.Height - 1500
    tvwestrorg.Width = Me.Width - 200
    picPorcen.Width = tvwestrorg.Width
    picNavigate.Top = picPorcen.Top + 530
    picNavigate.Left = Me.Width / 2 - picNavigate.Width / 2
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oUnidadesOrgN1 = Nothing
    Set m_oUnidadesOrgN2 = Nothing
    Set m_oUnidadesOrgN3 = Nothing

    dDistPorcen = 0
    dSinCantidad = 0
    dDistCantidad = 0
    dCantidad = 0

    Set oDistsNivel1 = Nothing
    Set oDistsNivel2 = Nothing
    Set oDistsNivel3 = Nothing
    
    If m_bCancelar Then
        frmItemsWizard.g_bCancelar = m_bCancelar
        Unload frmItemsWizard
    End If
    bNoCantidad = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub Slider1_Change()
Dim dPorcen As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If tvwestrorg.selectedItem Is Nothing Then 'ninguno
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If tvwestrorg.selectedItem.Parent Is Nothing Then 'Raiz
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        Exit Sub
    End If

    dPorcen = CDec(DevolverPorcentaje(tvwestrorg.selectedItem) * 100)
     
    If Slider1.Value <> dPorcen Then
        If Slider1.Value >= Slider1.Max Then
            dPorcen = dPorcen + 100 - CDec(dDistPorcen * 100)
        Else
            dPorcen = Slider1.Value
        End If
        txtPorcentaje.Text = dPorcen
    
    '    m_bRespetarPorcen = True
        If dPorcen = 0 Then
            m_bRespetarPorcen = True
            txtCantidad.Text = ""
        Else
            m_bRespetarPorcen = True
            If Not bNoCantidad Then txtCantidad.Text = CDec(dPorcen / 100) * dCantidad
        End If
        
    End If

    m_bRespetarPorcen = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "Slider1_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub tvwestrorg_NodeClick(ByVal node As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarInterfaz node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "tvwestrorg_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Establece el interfaz de un nodo del arbol
''' </summary>
''' <param name=node>Nodo</param>
''' <returns></returns>
''' <remarks>Llamada desde: tvwestrorg_NodeClick; Tiempo m�ximo:0</remarks>
Private Sub ConfigurarInterfaz(ByVal node As MSComctlLib.node)
Dim dPorcen As Double
Dim nodoHermano As MSComctlLib.node
Dim dPorcentajeHermano As Double
Dim iDistrib As Integer
Dim dDistrib As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    If node.Parent Is Nothing Then
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    m_stexto = node.Text
    
    'Configuramos seg�n nivel de de distribuci�n m�nimo
    Select Case gParametrosGenerales.giNIVDIST
        Case 2
            If Mid(node.Image, 4, 1) = "1" Then
                picPorcen.Enabled = False
                Exit Sub
            Else
                picPorcen.Enabled = True
            End If
        Case 3
            If Mid(node.Image, 4, 1) = "1" Or Mid(node.Image, 4, 1) = "2" Then
                picPorcen.Enabled = False
                Exit Sub
            Else
                picPorcen.Enabled = True
            End If
        Case Else
            picPorcen.Enabled = True
    End Select
        
    'Confirguramos dependiendo de si est� restringido a su unidad organizativa
    If m_bRuo Then
        If node.Parent.Parent Is Nothing Then
            'Ha seleccionado la de nivel1
            'Tenemos que ver si la del usuario esta por encima de la seleccionada
            If Trim(basOptimizacion.gUON1Usuario) = "" Then
                ' El usuario tiene permisos sobre la UON1 seleccionada
                picPorcen.Enabled = True
            Else
                picPorcen.Enabled = False
                Exit Sub
            End If
        Else
            If node.Parent.Parent.Parent Is Nothing Then
                'Ha seleccionado la de nivel2
                'Tenemos que ver si la del usuario esta por encima de la seleccionada
                If Trim(basOptimizacion.gUON3Usuario) = "" Then
                    ' El usuario tiene permisos sobre la UON2 seleccionada
                    picPorcen.Enabled = True
                Else
                    picPorcen.Enabled = False
                    Exit Sub
                End If
            End If
        End If
    End If
    
    Slider1.Max = 100
    If node.Image = "UON1A" Or node.Image = "UON2A" Or node.Image = "UON3A" Then
        m_bRespetarPorcen = True
        dPorcen = DevolverPorcentaje(node)
        m_bRespetarPorcen = True
        txtPorcentaje.Text = CDec(dPorcen * 100)
        m_bRespetarPorcen = True
        If txtPorcentaje.Text < 32767 Then
            Slider1.Value = Int(txtPorcentaje.Text)
        End If
        m_bRespetarPorcen = True
        If Not bNoCantidad Then txtCantidad.Text = dPorcen * dCantidad
        m_bRespetarPorcen = False
        If Slider1.Value = 0 Then
            If Int(100 - CDec(dDistPorcen * 100)) > 1 Then
                Slider1.Max = Int(100 - CDec(dDistPorcen * 100))
            Else
                Slider1.Max = 1
            End If
        Else
            Slider1.Max = Slider1.Value + Int(100 - CDec(dDistPorcen * 100))
        End If
    Else
       m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        
        If node.Selected = True Then
            dDistrib = DevolverPorcentajePadreHijos(node)
            dDistPorcen = dDistPorcen - dDistrib
            
            If dDistrib <> 0 Then
                LimpiarRama node, True
                'Y pone ese porcentaje en el nuevo nodo
                txtPorcentaje.Text = CDec(dDistrib * 100)
                If txtPorcentaje.Text < 32767 Then
                '    Slider1.Value = txtPorcentaje
                     Slider1.Max = IIf(Int(txtPorcentaje.Text) = 0, 1, Int(txtPorcentaje.Text))
                End If
                
            Else
            
                If CDbl((100 - CDec(dDistPorcen * 100))) > 0 Then
                    'Si no llega al 100% asigna lo que falta
                   txtPorcentaje.Text = CDbl(100 - CDec(dDistPorcen * 100))
                    If txtPorcentaje.Text < 32767 Then
                        Slider1.Max = txtPorcentaje.Text
                        Slider1.Value = Slider1.Max
                    End If
                    
                Else
                    'Si hay m�s de un nodo asignado no hace nada.Si no lo asigna el 100% a este nodo en el que nos posicionamos.
                    iDistrib = 0
                    If Not oDistsNivel1 Is Nothing Then iDistrib = iDistrib + oDistsNivel1.Count
                    If Not oDistsNivel2 Is Nothing Then iDistrib = iDistrib + oDistsNivel2.Count
                    If Not oDistsNivel3 Is Nothing Then iDistrib = iDistrib + oDistsNivel3.Count
                    If iDistrib >= 2 Then   'Hay mas de un nodo asignado:no hace nada
                        'Si solo hay un nodo hermano asignado cogeremos su porcentaje y lo pondremos en el seleccionado
                        Set nodoHermano = Nothing
                        Set nodoHermano = SoloUnHermanoAsignado(node)
                        If Not nodoHermano Is Nothing Then
                            dPorcentajeHermano = CDec(DevolverPorcentaje(nodoHermano))
                            Slider1.Max = IIf(Int(CDec(dPorcentajeHermano * 100)) = 0, 1, Int(CDec(dPorcentajeHermano * 100)))
                            nodoHermano.Image = Mid(nodoHermano.Image, 1, 4)
                            nodoHermano.Text = QuitarPorcentaje(nodoHermano.Text)
                            EliminarColeccion nodoHermano
                            dDistPorcen = dDistPorcen - dPorcentajeHermano
                            Slider1.Value = Slider1.Max
                        Else
                            Slider1.Max = 1
                        End If
                        
                    Else  'Hay un nodo asignado al 100% o todav�a no hay nada distribuido: asigna el 100%
                        dDistPorcen = 0
                        LimpiarTodasLasRamas
                        Slider1.Max = 100
                        Slider1.Value = 100
                    End If
                End If
            End If
        Else
            If Int(100 - CDec(dDistPorcen * 100)) > 1 Then
                Slider1.Max = IIf(Int(100 - CDec(dDistPorcen * 100)) = 0, 1, Int(100 - CDec(dDistPorcen * 100)))
            Else
                Slider1.Max = 1
            End If
        End If
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "ConfigurarInterfaz", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub
Private Function SoloUnHermanoAsignado(ByVal node As MSComctlLib.node) As Object
Dim lCount As Long
Dim nod As MSComctlLib.node
Dim nodoHermano As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
lCount = 0


Set nod = node.FirstSibling
While Not nod Is Nothing
    If Mid(nod.Image, 5, 1) = "A" And nod.key <> node.key Then
        Set nodoHermano = Nothing
        Set nodoHermano = nod
        lCount = lCount + 1
    End If
    Set nod = nod.Next
Wend

    If lCount = 1 Then
        Set SoloUnHermanoAsignado = nodoHermano
    Else
        Set SoloUnHermanoAsignado = Nothing
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "SoloUnHermanoAsignado", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function
Private Function DevolverPorcentaje(ByVal nod As MSComctlLib.node) As Double
Dim dPorcen As Double
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nod Is Nothing Then Exit Function

    sCod = DevolverCodigoColeccion(nod)
    Select Case Left(nod.Tag, 4)
    Case "UON1"
        If oDistsNivel1.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = oDistsNivel1.Item(sCod).Porcentaje
        End If
    Case "UON2"
        If oDistsNivel2.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = oDistsNivel2.Item(sCod).Porcentaje
        End If
    Case "UON3"
        If oDistsNivel3.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = oDistsNivel3.Item(sCod).Porcentaje
        End If
    End Select
    
    DevolverPorcentaje = dPorcen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "DevolverPorcentaje", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Sub txtCantidad_Change()
Dim nodx As MSComctlLib.node
Dim dCant As Double
       
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    Set nodx = tvwestrorg.selectedItem
    
    If nodx Is Nothing Then 'ninguno
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If nodx.Parent Is Nothing Then 'Raiz
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    dCant = DevolverPorcentaje(nodx)
    dCant = dCantidad * dCant
         
    If Trim(txtCantidad.Text) = "" Then
        If dCant <> 0 Then
            m_bRespetarPorcen = True
            txtPorcentaje.Text = ""
            m_bRespetarPorcen = True
            Slider1.Value = 0
            m_stexto = QuitarPorcentaje(m_stexto)
            tvwestrorg.selectedItem.Text = m_stexto
            Select Case Left(tvwestrorg.selectedItem.Tag, 4)
                Case "UON1"
                    tvwestrorg.selectedItem.Image = "UON1"
                Case "UON2"
                    tvwestrorg.selectedItem.Image = "UON2"
                Case "UON3"
                    tvwestrorg.selectedItem.Image = "UON3"
            End Select
            m_bRespetarPorcen = False
            
            DistribuirCant nodx
            
            lblCantSinDist.caption = dSinCantidad
            lblPorcenSinDist.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
            lblCantDist.caption = Format(dDistCantidad, "Standard")
            lblPorcenDist.caption = Format(CDec(dDistPorcen * 100), "Standard") & "%"
        End If
        Exit Sub
    End If
                
    If Not IsNumeric(txtCantidad.Text) Then
        m_bRespetarPorcen = True
        txtCantidad.Text = Left(txtCantidad.Text, Len(txtCantidad.Text) - 1)
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    
    m_stexto = nodx.Text
    If Mid(nodx.Image, 5, 1) = "A" Then
        m_stexto = QuitarPorcentaje(m_stexto)
    End If
    
    If CDbl(txtCantidad.Text) <= 0 Then
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        tvwestrorg.selectedItem.Text = m_stexto
        Select Case Left(tvwestrorg.selectedItem.Tag, 4)
            Case "UON1"
                tvwestrorg.selectedItem.Image = "UON1"
            Case "UON2"
                tvwestrorg.selectedItem.Image = "UON2"
            Case "UON3"
                tvwestrorg.selectedItem.Image = "UON3"
        End Select
        
        DistribuirCant nodx
        
        lblCantSinDist.caption = dSinCantidad
        lblPorcenSinDist.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
        lblCantDist.caption = Format(dDistCantidad, "Standard")
        lblPorcenDist.caption = Format(CDec(dDistPorcen * 100), "Standard") & "%"
           
    Else
        If txtCantidad.Text <> dCant Then
            If CDec((txtCantidad.Text / dCantidad) * 100) < 32767 Then
                m_bRespetarPorcen = True
                Slider1.Value = Int(CDec((txtCantidad.Text / dCantidad) * 100))
            End If
            m_bRespetarPorcen = True
            txtPorcentaje.Text = CDec((txtCantidad.Text / dCantidad) * 100)
            m_bRespetarPorcen = False
            tvwestrorg.selectedItem.Text = m_stexto & " (" & txtPorcentaje.Text & "%" & ")"
            Select Case Left(tvwestrorg.selectedItem.Tag, 4)
                Case "UON1"
                    tvwestrorg.selectedItem.Image = "UON1A"
                Case "UON2"
                    tvwestrorg.selectedItem.Image = "UON2A"
                Case "UON3"
                    tvwestrorg.selectedItem.Image = "UON3A"
            End Select
            
            LimpiarRama tvwestrorg.selectedItem
            
            DistribuirCant nodx
            
            lblCantSinDist.caption = dSinCantidad
            lblCantDist.caption = Format(dDistCantidad, "Standard")
            lblPorcenSinDist.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
            lblPorcenDist.caption = Format(CDec(dDistPorcen * 100), "Standard") & "%"
            
            If dDistCantidad > dCantidad Then
                txtPorcentaje.Text = Left(txtPorcentaje.Text, Len(txtPorcentaje.Text) - 1)
            End If
        End If
                
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "txtCantidad_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub txtPorcentaje_Change()
Dim nodx As MSComctlLib.node
Dim dPorcen As Double

        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    Set nodx = tvwestrorg.selectedItem
    
    If nodx Is Nothing Then 'ninguno
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If nodx.Parent Is Nothing Then 'Raiz
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    dPorcen = DevolverPorcentaje(nodx)

    If Trim(txtPorcentaje.Text) = "" Then
        If dPorcen <> 0 Then
            m_bRespetarPorcen = True
            txtCantidad.Text = ""
            m_bRespetarPorcen = True
            Slider1.Value = 0
            m_stexto = QuitarPorcentaje(m_stexto)
            tvwestrorg.selectedItem.Text = m_stexto
            Select Case Left(tvwestrorg.selectedItem.Tag, 4)
                Case "UON1"
                    tvwestrorg.selectedItem.Image = "UON1"
                Case "UON2"
                    tvwestrorg.selectedItem.Image = "UON2"
                Case "UON3"
                    tvwestrorg.selectedItem.Image = "UON3"
            End Select
            m_bRespetarPorcen = False
            
            Distribuir nodx
            
            If Not bNoCantidad Then lblCantSinDist.caption = dSinCantidad
            lblPorcenSinDist.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
            If Not bNoCantidad Then lblCantDist.caption = Format(dDistCantidad, "Standard")
            lblPorcenDist.caption = Format(CDec(dDistPorcen * 100), "Standard") & "%"
            
        End If
        Exit Sub
    End If
        
    If Not IsNumeric(txtPorcentaje.Text) Then
        m_bRespetarPorcen = True
        txtPorcentaje.Text = Left(txtPorcentaje.Text, Len(txtPorcentaje.Text) - 1)
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    m_stexto = nodx.Text
    If Mid(nodx.Image, 5, 1) = "A" Then
        m_stexto = QuitarPorcentaje(m_stexto)
    End If
    
    If CDbl(txtPorcentaje.Text) <= 0 Then
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        tvwestrorg.selectedItem.Text = m_stexto
        Select Case Left(tvwestrorg.selectedItem.Tag, 4)
            Case "UON1"
                tvwestrorg.selectedItem.Image = "UON1"
            Case "UON2"
                tvwestrorg.selectedItem.Image = "UON2"
            Case "UON3"
                tvwestrorg.selectedItem.Image = "UON3"
        End Select
        
        Distribuir nodx
        
        If Not bNoCantidad Then lblCantSinDist.caption = dSinCantidad
        lblPorcenSinDist.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
        If Not bNoCantidad Then lblCantDist.caption = Format(dDistCantidad, "Standard")
        lblPorcenDist.caption = DblToStr(CDec(dDistPorcen * 100)) & "%"

    Else
        If txtPorcentaje.Text <> CDec(dPorcen * 100) Then
            m_bRespetarPorcen = True
            If txtPorcentaje.Text < 32767 Then
                Slider1.Value = Int(txtPorcentaje.Text)
            End If
            m_bRespetarPorcen = True
            txtCantidad.Text = CDec(txtPorcentaje.Text / 100) * dCantidad
            m_bRespetarPorcen = False
            
            tvwestrorg.selectedItem.Text = m_stexto & " (" & txtPorcentaje.Text & "%" & ")"
            Select Case Left(tvwestrorg.selectedItem.Tag, 4)
                Case "UON1"
                    tvwestrorg.selectedItem.Image = "UON1A"
                Case "UON2"
                    tvwestrorg.selectedItem.Image = "UON2A"
                Case "UON3"
                    tvwestrorg.selectedItem.Image = "UON3A"
            End Select
            
            LimpiarRama tvwestrorg.selectedItem
            
            Distribuir nodx
            
            If Not bNoCantidad Then lblCantSinDist.caption = dSinCantidad
            lblPorcenSinDist.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
            If Not bNoCantidad Then lblCantDist.caption = Format(dDistCantidad, "Standard")
            lblPorcenDist.caption = DblToStr(CDec(dDistPorcen * 100)) & "%"
            If dDistCantidad > dCantidad Then
                txtPorcentaje.Text = Left(txtPorcentaje.Text, Len(txtPorcentaje.Text) - 1)
            End If
            
        End If
    End If

    Set nodx = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "txtPorcentaje_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Function QuitarPorcentaje(ByVal sTexto As String) As String
Dim i As Long
Dim sAux As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sAux = sTexto
    
    If sTexto = "" Then Exit Function
    
    i = Len(sTexto)
    
    If i = 1 Then Exit Function
    
    sTexto = Left(sTexto, Len(sTexto) - 1)
    
    While i > 1
        
        If Mid(sTexto, Len(sTexto), 1) <> "(" Then
            sTexto = Left(sTexto, Len(sTexto) - 1)
            i = i - 1
        Else
            sTexto = Left(sTexto, Len(sTexto) - 2)
            i = -1
        End If
    
    Wend
    
    If i = 0 Or i = 1 Then
        sTexto = sAux
    End If
    
    QuitarPorcentaje = sTexto
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "QuitarPorcentaje", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Sub LimpiarRama(ByVal node As MSComctlLib.node, Optional ByVal bEliminar As Boolean)
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node

   'Primero hacia arriba
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nod1 = node.Parent
    
    While Not nod1 Is Nothing
        
        If Mid(nod1.Image, 5, 1) = "A" Then
            nod1.Image = Mid(nod1.Image, 1, 4)
            nod1.Text = QuitarPorcentaje(nod1.Text)
            
            If bEliminar = True Then
                EliminarColeccion nod1
            End If
            
            Exit Sub
        Else
            Set nod1 = nod1.Parent
        End If
    
    Wend
    
    'Ahora hacia abajo
    
    Set nod1 = node.Child
    
    While Not nod1 Is Nothing
        
        If Mid(nod1.Image, 5, 1) = "A" Then
            nod1.Image = Mid(nod1.Image, 1, 4)
            nod1.Text = QuitarPorcentaje(nod1.Text)
            
            If bEliminar = True Then
                EliminarColeccion nod1
            End If
            
        Else
            Set nod2 = nod1.Child
                
            While Not nod2 Is Nothing
                
                If Mid(nod2.Image, 5, 1) = "A" Then
                    nod2.Image = Mid(nod2.Image, 1, 4)
                    nod2.Text = QuitarPorcentaje(nod2.Text)
                    
                    If bEliminar = True Then
                        EliminarColeccion nod2
                    End If
                    
                Else
                    Set nod3 = nod2.Child
                        
                    While Not nod3 Is Nothing
                        If Mid(nod3.Image, 5, 1) = "A" Then
                            nod3.Image = Mid(nod3.Image, 1, 4)
                            nod3.Text = QuitarPorcentaje(nod3.Text)
                            
                            If bEliminar = True Then
                                EliminarColeccion nod3
                            End If
                        End If
                        Set nod3 = nod3.Next
                    Wend
                End If
                Set nod2 = nod2.Next
            Wend
        
        End If
        
        Set nod1 = nod1.Next
        
        Wend
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "LimpiarRama", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub


Public Sub MostrarDistribSeleccionada()
Dim oDistNivel1 As CDistItemNivel1
Dim oDistNivel2 As CDistItemNivel2
Dim oDistNivel3 As CDistItemNivel3
Dim nodx As MSComctlLib.node

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dDistPorcen = 0
    If frmItemsWizard.g_oDistsNivel1 Is Nothing And frmItemsWizard.g_oDistsNivel2 Is Nothing And frmItemsWizard.g_oDistsNivel3 Is Nothing Then Exit Sub

    If Not frmItemsWizard.g_oDistsNivel1 Is Nothing Then
        For Each oDistNivel1 In frmItemsWizard.g_oDistsNivel1
            Set nodx = Nothing
            scod1 = oDistNivel1.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel1.CodUON1))
            Set nodx = tvwestrorg.Nodes("UON1" & scod1)
            nodx.Text = nodx.Text & " (" & oDistNivel1.Porcentaje * 100 & "%)"
            nodx.Image = "UON1A"
            dDistPorcen = dDistPorcen + oDistNivel1.Porcentaje
        Next
    End If

    If Not frmItemsWizard.g_oDistsNivel2 Is Nothing Then
        For Each oDistNivel2 In frmItemsWizard.g_oDistsNivel2
            Set nodx = Nothing
            scod1 = oDistNivel2.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel2.CodUON1))
            scod2 = oDistNivel2.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel2.CodUON2))
            Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2)
            nodx.Text = nodx.Text & " (" & oDistNivel2.Porcentaje * 100 & "%)"
            nodx.Image = "UON2A"
            nodx.Parent.Expanded = True
            dDistPorcen = dDistPorcen + oDistNivel2.Porcentaje
        Next
    End If

    If Not frmItemsWizard.g_oDistsNivel3 Is Nothing Then
        For Each oDistNivel3 In frmItemsWizard.g_oDistsNivel3
            Set nodx = Nothing
            scod1 = oDistNivel3.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel3.CodUON1))
            scod2 = oDistNivel3.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel3.CodUON2))
            scod3 = oDistNivel3.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDistNivel3.CodUON3))
            Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3)
            nodx.Text = nodx.Text & " (" & oDistNivel3.Porcentaje * 100 & "%)"
            nodx.Image = "UON3A"
            nodx.Parent.Parent.Expanded = True
            nodx.Parent.Expanded = True
            dDistPorcen = dDistPorcen + oDistNivel3.Porcentaje
        Next
    End If
    dDistCantidad = CDec(dDistPorcen * dCantidad)
    dSinCantidad = dCantidad - dDistCantidad

    lblCantSinDist.caption = dSinCantidad
    lblPorcenSinDist.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
    lblCantDist.caption = Format(dDistCantidad, "Standard")
    lblPorcenDist.caption = Format(CDec(dDistPorcen * 100), "Standard") & "%"

    tvwestrorg.selectedItem = tvwestrorg.Nodes(1)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "MostrarDistribSeleccionada", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Function DevolverCodigoColeccion(ByVal nod As MSComctlLib.node) As String
Dim sCod As String
Dim scod1 As String
Dim scod2 As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sCod = Right(nod.Tag, Len(nod.Tag) - 4)
    Select Case Left(nod.Tag, 4)
    Case "UON1"
        sCod = sCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sCod))
    Case "UON2"
        scod1 = Right(nod.Parent.Tag, Len(nod.Parent.Tag) - 4)
        scod2 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(scod1))
        sCod = scod2 & sCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sCod))
    Case "UON3"
        scod1 = Right(nod.Parent.Parent.Tag, Len(nod.Parent.Parent.Tag) - 4)
        scod2 = Right(nod.Parent.Tag, Len(nod.Parent.Tag) - 4)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(scod1))
        scod2 = scod1 & scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(scod2))
        sCod = scod2 & sCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sCod))
    End Select
    DevolverCodigoColeccion = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "DevolverCodigoColeccion", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function
Private Sub Distribuir(nodo As MSComctlLib.node)
Dim sCod As String
Dim dPorcen As Double
Dim oItem As CItem
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nodo Is Nothing Then Exit Sub
            
    sCod = DevolverCodigoColeccion(nodo)
    
    If Trim(txtPorcentaje.Text) = "" Then
        dPorcen = 0
    Else
        dPorcen = txtPorcentaje.Text
    End If
    
    Set oItem = oFSGSRaiz.Generar_CItem
        
    Select Case Left(nodo.Tag, 4)
    Case "UON1"
        ' Si est� distribu�do UON1
        If oDistsNivel1.Item(sCod) Is Nothing Then
            oDistsNivel1.Add oItem, Right(nodo.Tag, Len(nodo.Tag) - 4), CDec(dPorcen / 100)
            dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistPorcen = dDistPorcen - oDistsNivel1.Item(sCod).Porcentaje
            If dPorcen = 0 Then
                oDistsNivel1.Remove sCod
            Else
                oDistsNivel1.Item(sCod).Porcentaje = CDec(dPorcen / 100)
                dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            End If
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
        End If
        
    Case "UON2"
                        
        If oDistsNivel2.Item(sCod) Is Nothing Then
            oDistsNivel2.Add oItem, Right(nodo.Parent.Tag, Len(nodo.Parent.Tag) - 4), Right(nodo.Tag, Len(nodo.Tag) - 4), CDec(dPorcen / 100)
            dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistPorcen = dDistPorcen - oDistsNivel2.Item(sCod).Porcentaje
            If dPorcen = 0 Then
                oDistsNivel2.Remove sCod
            Else
                oDistsNivel2.Item(sCod).Porcentaje = CDec(dPorcen / 100)
                dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            End If
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
            
        End If
    
    Case "UON3"
        If oDistsNivel3.Item(sCod) Is Nothing Then
            oDistsNivel3.Add oItem, Right(nodo.Parent.Parent.Tag, Len(nodo.Parent.Parent.Tag) - 4), Right(nodo.Parent.Tag, Len(nodo.Parent.Tag) - 4), Right(nodo.Tag, Len(nodo.Tag) - 4), CDec(dPorcen / 100)
            dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistPorcen = dDistPorcen - oDistsNivel3.Item(sCod).Porcentaje
            If dPorcen = 0 Then
                oDistsNivel3.Remove sCod
            Else
                oDistsNivel3.Item(sCod).Porcentaje = CDec(dPorcen / 100)
                dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            End If
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
        End If
    
    End Select
    Set oItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "Distribuir", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub
Private Sub DistribuirCant(nodo As MSComctlLib.node)
Dim sCod As String
Dim dCant As Double
Dim dPorcen As Double
Dim oItem As CItem

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nodo Is Nothing Then Exit Sub
            
    sCod = DevolverCodigoColeccion(nodo)
    
    If Trim(txtCantidad.Text) = "" Then
        dCant = 0
    Else
        dCant = CDbl(txtCantidad.Text)
    End If
    dPorcen = dCant / dCantidad
    
    Set oItem = oFSGSRaiz.Generar_CItem
    
    Select Case Left(nodo.Tag, 4)
    Case "UON1"
        ' Si est� distribu�do UON1
        If oDistsNivel1.Item(sCod) Is Nothing Then
            oDistsNivel1.Add oItem, Right(nodo.Tag, Len(nodo.Tag) - 4), dPorcen
            dDistCantidad = dDistCantidad + dCant
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistCantidad = dDistCantidad - CDec(oDistsNivel1.Item(sCod).Porcentaje * dCantidad)
            If dCant = 0 Then
                oDistsNivel1.Remove sCod
            Else
                oDistsNivel1.Item(sCod).Porcentaje = dPorcen
                dDistCantidad = dDistCantidad + dCant
            End If
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        End If
        
    Case "UON2"
                        
        If oDistsNivel2.Item(sCod) Is Nothing Then
            oDistsNivel2.Add oItem, Right(nodo.Parent.Tag, Len(nodo.Parent.Tag) - 4), Right(nodo.Tag, Len(nodo.Tag) - 4), dPorcen
            dDistCantidad = dDistCantidad + dCant
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistCantidad = dDistCantidad - CDec(oDistsNivel2.Item(sCod).Porcentaje * dCantidad)
            If dCant = 0 Then
                oDistsNivel2.Remove sCod
            Else
                oDistsNivel2.Item(sCod).Porcentaje = dPorcen
                dDistCantidad = dDistCantidad + dCant
            End If
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        End If
    
    Case "UON3"
        If oDistsNivel3.Item(sCod) Is Nothing Then
            oDistsNivel3.Add oItem, Right(nodo.Parent.Parent.Tag, Len(nodo.Parent.Parent.Tag) - 4), Right(nodo.Parent.Tag, Len(nodo.Parent.Tag) - 4), Right(nodo.Tag, Len(nodo.Tag) - 4), dPorcen
            dDistCantidad = dDistCantidad + dCant
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistCantidad = dDistCantidad - CDec(oDistsNivel3.Item(sCod).Porcentaje * dCantidad)
            If dCant = 0 Then
                oDistsNivel3.Remove sCod
            Else
                oDistsNivel3.Item(sCod).Porcentaje = dPorcen
                dDistCantidad = dDistCantidad + dCant
            End If
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        End If
    
    End Select
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "DistribuirCant", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Function DevolverPorcentajePadreHijos(ByVal nod As MSComctlLib.node) As Double
Dim dPorcen As Double
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nod Is Nothing Then Exit Function
    
    dPorcen = 0
    
    'Primero hacia arriba
    Set nod1 = nod.Parent
    
    While Not nod1 Is Nothing
        If Mid(nod1.Image, 5, 1) = "A" Then
            dPorcen = dPorcen + DevolverPorcentaje(nod1)
            DevolverPorcentajePadreHijos = dPorcen
            Exit Function
        Else
            Set nod1 = nod1.Parent
        End If
    Wend
    
    'Ahora hacia abajo
    
    Set nod1 = nod.Child
    
    While Not nod1 Is Nothing
        
        If Mid(nod1.Image, 5, 1) = "A" Then
            dPorcen = dPorcen + DevolverPorcentaje(nod1)
        Else
            Set nod2 = nod1.Child
                
            While Not nod2 Is Nothing
                
                If Mid(nod2.Image, 5, 1) = "A" Then
                    dPorcen = dPorcen + DevolverPorcentaje(nod2)
                Else
                    Set nod3 = nod2.Child
                        
                    While Not nod3 Is Nothing
                        If Mid(nod3.Image, 5, 1) = "A" Then
                            dPorcen = dPorcen + DevolverPorcentaje(nod3)
                        End If
                        Set nod3 = nod3.Next
                    Wend
                End If
                Set nod2 = nod2.Next
            Wend
        
        End If
        
        Set nod1 = nod1.Next
        
    Wend
    
    Set nod1 = Nothing
    Set nod2 = Nothing
    Set nod3 = Nothing
    
    DevolverPorcentajePadreHijos = dPorcen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "DevolverPorcentajePadreHijos", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Private Sub LimpiarTodasLasRamas()
    Dim nodx As node
    Dim oDistNivel1 As CDistItemNivel1
    Dim oDistNivel2 As CDistItemNivel2
    Dim oDistNivel3 As CDistItemNivel3
    Dim sCod As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    
    'Limpia todas las ramas con distribuciones:
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For Each oDistNivel1 In oDistsNivel1
        Set nodx = Nothing
        scod1 = oDistNivel1.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel1.CodUON1))
        Set nodx = tvwestrorg.Nodes("UON1" & scod1)
        LimpiarRama nodx
        nodx.Image = Mid(nodx.Image, 1, 4)
        nodx.Text = QuitarPorcentaje(nodx.Text)
        sCod = DevolverCodigoColeccion(nodx)
        oDistsNivel1.Remove sCod
    Next
    
    For Each oDistNivel2 In oDistsNivel2
        Set nodx = Nothing
        scod1 = oDistNivel2.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel2.CodUON1))
        scod2 = oDistNivel2.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel2.CodUON2))
        Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2)
        LimpiarRama nodx
        nodx.Image = Mid(nodx.Image, 1, 4)
        nodx.Text = QuitarPorcentaje(nodx.Text)
        sCod = DevolverCodigoColeccion(nodx)
        oDistsNivel2.Remove sCod
    Next
    
    For Each oDistNivel3 In oDistsNivel3
        Set nodx = Nothing
        scod1 = oDistNivel3.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel3.CodUON1))
        scod2 = oDistNivel3.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel3.CodUON2))
        scod3 = oDistNivel3.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDistNivel3.CodUON3))
        Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3)
        LimpiarRama nodx
        nodx.Image = Mid(nodx.Image, 1, 4)
        nodx.Text = QuitarPorcentaje(nodx.Text)
        sCod = DevolverCodigoColeccion(nodx)
        oDistsNivel3.Remove sCod
        
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "LimpiarTodasLasRamas", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub
Private Sub EliminarColeccion(ByVal nod As MSComctlLib.node)
    Dim sCod As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sCod = DevolverCodigoColeccion(nod)
    
    If Not oDistsNivel1.Item(sCod) Is Nothing Then
        oDistsNivel1.Remove sCod
    ElseIf Not oDistsNivel2.Item(sCod) Is Nothing Then
        oDistsNivel2.Remove sCod
    ElseIf Not oDistsNivel3.Item(sCod) Is Nothing Then
        oDistsNivel3.Remove sCod
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "EliminarColeccion", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub


Public Sub MostrarDistribSeleccionadaPorDefecto()
'Funcion que se encarga de cargar por defecto al 100% una UON de uno de los items a a�adir
Dim oDistNivel1 As CDistItemNivel1
Dim oDistNivel2 As CDistItemNivel2
Dim oDistNivel3 As CDistItemNivel3
Dim nodx As MSComctlLib.node

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dDistPorcen = 0
    If frmItemsWizard.g_oDistsNivel1 Is Nothing And frmItemsWizard.g_oDistsNivel2 Is Nothing And frmItemsWizard.g_oDistsNivel3 Is Nothing Then

        If Not oDistsNivel1 Is Nothing Then
            For Each oDistNivel1 In oDistsNivel1
                Set nodx = Nothing
                scod1 = oDistNivel1.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel1.CodUON1))
                Set nodx = tvwestrorg.Nodes("UON1" & scod1)
                nodx.Text = nodx.Text & " (" & oDistNivel1.Porcentaje * 100 & "%)"
                nodx.Image = "UON1A"
                dDistPorcen = dDistPorcen + oDistNivel1.Porcentaje
            Next
        End If
    
        If Not oDistsNivel2 Is Nothing Then
            For Each oDistNivel2 In oDistsNivel2
                Set nodx = Nothing
                scod1 = oDistNivel2.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel2.CodUON1))
                scod2 = oDistNivel2.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel2.CodUON2))
                Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2)
                nodx.Text = nodx.Text & " (" & oDistNivel2.Porcentaje * 100 & "%)"
                nodx.Image = "UON2A"
                nodx.Parent.Expanded = True
                dDistPorcen = dDistPorcen + oDistNivel2.Porcentaje
            Next
        End If
    
        If Not oDistsNivel3 Is Nothing Then
            For Each oDistNivel3 In oDistsNivel3
                Set nodx = Nothing
                scod1 = oDistNivel3.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel3.CodUON1))
                scod2 = oDistNivel3.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel3.CodUON2))
                scod3 = oDistNivel3.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDistNivel3.CodUON3))
                Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3)
                nodx.Text = nodx.Text & " (" & oDistNivel3.Porcentaje * 100 & "%)"
                nodx.Image = "UON3A"
                nodx.Parent.Parent.Expanded = True
                nodx.Parent.Expanded = True
                dDistPorcen = dDistPorcen + oDistNivel3.Porcentaje
            Next
        End If
        
        dDistCantidad = CDec(dDistPorcen * dCantidad)
        dSinCantidad = dCantidad - dDistCantidad
    
        lblCantSinDist.caption = dSinCantidad
        lblPorcenSinDist.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
        lblCantDist.caption = Format(dDistCantidad, "Standard")
        lblPorcenDist.caption = Format(CDec(dDistPorcen * 100), "Standard") & "%"
    
        tvwestrorg.selectedItem = tvwestrorg.Nodes(1)
        
        Set frmItemsWizard.g_oDistsNivel1 = oDistsNivel1
        Set frmItemsWizard.g_oDistsNivel2 = oDistsNivel2
        Set frmItemsWizard.g_oDistsNivel3 = oDistsNivel3

        Set oDistsNivel1 = Nothing
        Set oDistsNivel2 = Nothing
        Set oDistsNivel3 = Nothing

    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard3", "MostrarDistribSeleccionadaPorDefecto", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

