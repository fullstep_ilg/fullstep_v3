VERSION 5.00
Begin VB.Form frmAtribMsg 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FULLSTEP"
   ClientHeight    =   2760
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6690
   ControlBox      =   0   'False
   Icon            =   "frmATRIBMsg.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2760
   ScaleWidth      =   6690
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   300
      Left            =   3240
      TabIndex        =   3
      Top             =   2400
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   300
      Left            =   2040
      TabIndex        =   2
      Top             =   2400
      Width           =   1005
   End
   Begin VB.TextBox txtMsg 
      BackColor       =   &H8000000B&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Text            =   "frmATRIBMsg.frx":000C
      Top             =   600
      Width           =   6495
   End
   Begin VB.Image Image1 
      Height          =   555
      Left            =   120
      Picture         =   "frmATRIBMsg.frx":0045
      Top             =   0
      Width           =   585
   End
   Begin VB.Label lblContinuar 
      Caption         =   "� Desea continuar ?"
      Height          =   195
      Left            =   120
      TabIndex        =   4
      Top             =   2160
      Width           =   3255
   End
   Begin VB.Label lblCabecera 
      Caption         =   $"frmATRIBMsg.frx":0142
      Height          =   375
      Left            =   720
      TabIndex        =   1
      Top             =   120
      Width           =   5655
   End
End
Attribute VB_Name = "frmAtribMsg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private m_sProcesosyPedidos As String
Private m_sFormularios As String
Public m_itipoMensaje As Integer
Public g_sOrigen As String

''' <summary>
''' Devuelve true a la llamada al mensaje sobre los atributos
''' </summary>
''' <returns>True</returns>
''' <remarks>Llamada desde: frmAtrib,frmproce,frmPlantillasProce,frmformularios,frmPARTipoSolicit,frmLista,frmESTRMATAtrib  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub cmdAceptar_Click()
    Select Case g_sOrigen
        Case "MDIfrmATRIB", "MDI"
            frmAtrib.m_bRespuesta = True
        Case "PLANTILLAS"
        Case "frmPROCE"
             frmPROCE.g_ofrmATRIB.m_bRespuesta = True
        Case "PLANTILLASfrmATRIB", "PLANTILLASESPfrmATRIB"
            frmPlantillasProce.g_ofrmATRIB.m_bRespuesta = True
        Case "frmPROCEfrmATRIB"
            frmPROCE.g_ofrmATRIB.m_bRespuesta = True
        Case "frmFormulariosfrmATRIB"
            frmFormularios.g_ofrmATRIB.m_bRespuesta = True
        Case "frmDesglosefrmATRIB"
            frmFormularios.g_ofrmDesglose.g_ofrmATRIB.m_bRespuesta = True
        Case "frmPARTipoSolicitfrmATRIB"
            frmPARTipoSolicit.g_ofrmATRIB.m_bRespuesta = True
        Case "frmPARTipoSolicitDesglosefrmATRIB"
            frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.m_bRespuesta = True
        Case "FRMLISTA"
            frmLista.m_bRespuesta = True
        Case "ITEM_ATRIBESPfrmATRIB"
            frmPROCE.g_ofrmATRIB.m_bRespuesta = True
        Case "ATRIB_ESP_GRUPOfrmATRIB"
            frmPROCE.g_ofrmATRIB.m_bRespuesta = True
        Case "ATRIB_ESPfrmATRIB"
            frmPROCE.g_ofrmATRIB.m_bRespuesta = True
        Case "CONFINTEGRACIONfrmATRIB"
            frmCONFIntegracion.g_ofrmATRIB.m_bRespuesta = True
        Case "frmFormularios"
            frmFormularios.g_ofrmATRIB.m_bRespuesta = True
        Case Else
            frmESTRMATAtrib.g_ofrmATRIB.m_bRespuesta = False
    End Select
    Unload Me
End Sub

''' <summary>
''' Devuelve false a la llamada al mensaje sobre los atributos
''' </summary>
''' <returns>False</returns>
''' <remarks>Llamada desde: frmAtrib,frmproce,frmPlantillasProce,frmformularios,frmPARTipoSolicit,frmLista,frmESTRMATAtrib  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub cmdCancelar_Click()
    Select Case g_sOrigen
        Case "MDIfrmATRIB", "MDI"
            frmAtrib.m_bRespuesta = False
        Case "PLANTILLAS"
        Case "frmPROCE"
        Case "PLANTILLASfrmATRIB", "PLANTILLASESPfrmATRIB"
            frmPlantillasProce.g_ofrmATRIB.m_bRespuesta = False
        Case "frmPROCEfrmATRIB"
            frmPROCE.g_ofrmATRIB.m_bRespuesta = False
        Case "frmFormulariosfrmATRIB"
            frmFormularios.g_ofrmATRIB.m_bRespuesta = False
        Case "frmDesglosefrmATRIB"
            frmFormularios.g_ofrmDesglose.g_ofrmATRIB.m_bRespuesta = False
        Case "frmPARTipoSolicitfrmATRIB"
            frmPARTipoSolicit.g_ofrmATRIB.m_bRespuesta = False
        Case "frmPARTipoSolicitDesglosefrmATRIB"
            frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.m_bRespuesta = False
        Case "FRMLISTA"
            frmLista.m_bRespuesta = False
        Case "ITEM_ATRIBESPfrmATRIB"
            frmPROCE.g_ofrmATRIB.m_bRespuesta = False
        Case "ATRIB_ESP_GRUPOfrmATRIB"
            frmPROCE.g_ofrmATRIB.m_bRespuesta = False
        Case "ATRIB_ESPfrmATRIB"
            frmPROCE.g_ofrmATRIB.m_bRespuesta = False
        Case "frmFormularios"
            frmFormularios.g_ofrmATRIB.m_bRespuesta = False
        Case "CONFINTEGRACIONfrmATRIB"
            frmCONFIntegracion.g_ofrmATRIB.m_bRespuesta = False
        Case Else
            frmESTRMATAtrib.g_ofrmATRIB.m_bRespuesta = False
    End Select
    Unload Me
End Sub

Private Sub Form_Load()
    CargarRecursos
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ATRIBMSG, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        m_sFormularios = Ador(0).Value
        Ador.MoveNext
        m_sProcesosyPedidos = Ador(0).Value
        Ador.MoveNext
        lblContinuar.caption = Ador(0).Value
        
        If m_itipoMensaje = 1 Then
            lblCabecera.caption = m_sFormularios
        Else
            lblCabecera.caption = m_sProcesosyPedidos
        End If
        
        Ador.MoveNext
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub
