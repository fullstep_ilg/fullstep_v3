VERSION 5.00
Begin VB.Form frmDESTFiltrar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Destinos (Filtro)+"
   ClientHeight    =   2580
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5400
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDESTFiltrar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2580
   ScaleWidth      =   5400
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   60
      TabIndex        =   8
      Top             =   1140
      Width           =   5235
      Begin VB.CheckBox chkIgualDen 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total+"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   3420
         TabIndex        =   3
         Top             =   420
         Width           =   1680
      End
      Begin VB.TextBox txtDEN 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   240
         MaxLength       =   100
         TabIndex        =   2
         Top             =   360
         Width           =   3075
      End
      Begin VB.OptionButton optDEN 
         BackColor       =   &H00808000&
         Caption         =   "Denominaci�n+"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   120
         TabIndex        =   9
         Top             =   -60
         Width           =   1695
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   60
      TabIndex        =   6
      Top             =   120
      Width           =   5235
      Begin VB.CheckBox chkIgualCod 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total+"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   1140
         TabIndex        =   1
         Top             =   420
         Width           =   3570
      End
      Begin VB.TextBox txtCOD 
         Height          =   285
         Left            =   240
         MaxLength       =   3
         TabIndex        =   0
         Top             =   360
         Width           =   795
      End
      Begin VB.OptionButton optCOD 
         BackColor       =   &H00808000&
         Caption         =   "C�digo+"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   150
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   15
         Value           =   -1  'True
         Width           =   1215
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar+"
      Height          =   345
      Left            =   2655
      TabIndex        =   5
      Top             =   2160
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar+"
      Default         =   -1  'True
      Height          =   345
      Left            =   1440
      TabIndex        =   4
      Top             =   2160
      Width           =   1005
   End
End
Attribute VB_Name = "frmDESTFiltrar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmDESTFiltrar
''' *** Creacion: 28/12/1998 (Javier Arana)
''' *** Ultima revision: 14/01/1999 (Alfredo Magallon)

Option Explicit

Private m_oIdiomas As CIdiomas


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DESTFILTRAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        chkIgualCod.caption = Ador(0).Value
        chkIgualDen.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmDESTFiltrar.caption = Ador(0).Value
        Ador.MoveNext
        optCOD.caption = Ador(0).Value
        Ador.MoveNext
        optDEN.caption = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
            
End Sub

Private Sub Form_Load()

    ''' * Objetivo: Situar el formulario respecto al
    ''' * Objetivo: principal de Destinos
    
    On Error Resume Next
    
    Me.Left = frmESTRORG.Left + 500
    Me.Top = frmESTRORG.Top + 1000
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    CargarRecursos
    
    txtCOD.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodDEST
    
End Sub
Private Sub optCOD_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    If optCOD.Value = True Then
        optDEN.Value = False
    End If
        
    If Me.Visible Then txtCOD.SetFocus
   
End Sub
Private Sub chkIgualDen_Click()
    
    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    If Me.Visible Then txtDEN.SetFocus

End Sub
Private Sub chkIgualCod_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    If Me.Visible Then txtCOD.SetFocus
    
End Sub
Private Sub optDEN_Click()

    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    If optDEN.Value = True Then
        optCOD.Value = False
    End If
    
    If Me.Visible Then txtDEN.SetFocus
    
End Sub

Private Sub cmdAceptar_Click()
    Dim Dest As CDestino
    Dim sDest As String
    Dim oIdi As CIdioma
    ''' * Objetivo: Aplicar el filtro y descargar
    ''' * Objetivo: el formulario
    
    Screen.MousePointer = vbHourglass
    
    Set frmESTRORG.oDestinos = Nothing
    Set frmESTRORG.oDestinos = oFSGSRaiz.generar_CDestinos
 
    frmESTRORG.ponerCaption "", 1, False
        
    If optCOD.Value = True And txtCOD <> "" Then
        
        If Not chkIgualCod.Value = vbChecked Then
            frmESTRORG.oDestinos.CargarTodosLosDestinosUON frmESTRORG.g_vUON1, frmESTRORG.g_vUON2, frmESTRORG.g_vUON3, , , Trim(txtCOD.Text), , False, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , frmESTRORG.g_vUON4
            frmESTRORG.ponerCaption txtCOD, 3, True
        Else
            frmESTRORG.oDestinos.CargarTodosLosDestinosUON frmESTRORG.g_vUON1, frmESTRORG.g_vUON2, frmESTRORG.g_vUON3, , , Trim(txtCOD.Text), , True, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , frmESTRORG.g_vUON4
            frmESTRORG.ponerCaption txtCOD, 3, False
        End If
        
    Else
    
        If optDEN.Value = True And txtDEN <> "" Then
        
            If Not chkIgualDen.Value = vbChecked Then
                frmESTRORG.oDestinos.CargarTodosLosDestinosUON frmESTRORG.g_vUON1, frmESTRORG.g_vUON2, frmESTRORG.g_vUON3, , , , Trim(txtDEN.Text), False, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , frmESTRORG.g_vUON4
                frmESTRORG.ponerCaption txtDEN, 4, True
            Else
                frmESTRORG.oDestinos.CargarTodosLosDestinosUON frmESTRORG.g_vUON1, frmESTRORG.g_vUON2, frmESTRORG.g_vUON3, , , , Trim(txtDEN.Text), True, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , frmESTRORG.g_vUON4
                frmESTRORG.ponerCaption txtDEN, 4, False
            End If
        
        Else
        
            frmESTRORG.oDestinos.CargarTodosLosDestinosUON frmESTRORG.g_vUON1, frmESTRORG.g_vUON2, frmESTRORG.g_vUON3, , , , , , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , frmESTRORG.g_vUON4
            
        End If
        
    End If
            
    frmESTRORG.sdbgDestinos.RemoveAll
        
    For Each Dest In frmESTRORG.oDestinos
        sDest = Dest.EstadoIntegracion & Chr(m_lSeparador) & Dest.Cod & Chr(m_lSeparador) & Dest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        For Each oIdi In m_oIdiomas
            If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
                sDest = sDest & Chr(m_lSeparador) & Dest.Denominaciones.Item(oIdi.Cod).Den
            End If
        Next
        sDest = sDest & Chr(m_lSeparador) & Dest.dir & Chr(m_lSeparador) & Dest.POB & Chr(m_lSeparador) & Dest.cP & Chr(m_lSeparador) & Dest.Pais & Chr(m_lSeparador) & Dest.Provi & Chr(m_lSeparador) & Chr(m_lSeparador) & Dest.UONString & Chr(m_lSeparador) & Dest.UON1 & Chr(m_lSeparador) & Dest.UON2 & Chr(m_lSeparador) & Dest.UON3 & Chr(m_lSeparador) & Dest.UON4
        frmESTRORG.sdbgDestinos.AddItem sDest
    Next
    
    MDI.MostrarFormulario frmESTRORG, True
    frmESTRORG.sdbgDestinos.MoveFirst
    
    Screen.MousePointer = vbNormal
    
    Unload Me

    frmESTRORG.SetFocus
    
End Sub
Private Sub cmdCancelar_Click()

    ''' * Objetivo: Cerrar el formulario
    
    MDI.MostrarFormulario frmESTRORG, True
    
    Unload Me
    
    frmESTRORG.SetFocus
    
    
    
End Sub
Private Sub Form_Activate()

    ''' * Objetivo: Iniciar el formulario
    
    If Me.Visible Then txtCOD.SetFocus
    
End Sub
Private Sub txtCOD_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por codigo

    optCOD.Value = True
    optDEN.Value = False
    
End Sub
Private Sub txtDEN_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por denominacion

    optCOD.Value = False
    optDEN.Value = True
    
End Sub

