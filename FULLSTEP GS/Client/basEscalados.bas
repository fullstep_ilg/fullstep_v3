Attribute VB_Name = "basEscalados"
Option Explicit

''' <summary>Calcula la cantidad adjudicada a un proveedor para el c�lculo de importes y ahorros</summary>
''' <param name="oGrupo">Grupo para el que se est� haciendo el c�lculo</param>
''' <param name="oProvesAsig">Proveedores asignados</param>
''' <param name="oAdjsProve">Adjudicaciones calculadas a nivel de proveedor</param>
''' <param name="sCodProve">Cod. proveedor</param>
''' <param name="lIdItem">Id del item</param>
''' <param name="dblPorcentaje">Porcentaje adjudicado al proveedor</param>
''' <returns>La cantidad asignada</returns>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>LTG 20/02/2012</revision>

Public Function CantidadProveedor(ByVal oGrupo As CGrupo, ByVal oProvesAsig As CProveedores, ByVal oAdjsProve As CAdjudicaciones, ByVal scodProve As String, _
        ByVal lIdItem As Long, ByVal dblPorcentaje As Double) As Double
    Dim oAdj As CAdjudicacion
    Dim oAdjProve As CAdjudicacion
    Dim oProve As CProveedor
    Dim vCantItem As Variant
    Dim dblSuma As Double
    Dim iNumAdj As Integer
    Dim sProve As String
    
    'Si el item tiene cantidad el dato lo da el porcentaje, si no, la suma de las cantidades adjudicadas a cada escalado
    vCantItem = oGrupo.Items.Item(CStr(lIdItem)).Cantidad
    If IsNull(vCantItem) Or IsEmpty(vCantItem) Then
        For Each oAdj In oGrupo.Adjudicaciones
            If oAdj.ProveCod = scodProve And oAdj.Id = lIdItem Then
                If Not IsNull(oAdj.Escalado) Then
                    dblSuma = dblSuma + NullToDbl0(oAdj.Adjudicado)
                Else
                    dblSuma = dblSuma + NullToDbl0(oAdj.Adjudicado)
                End If
            End If
        Next
        
        CantidadProveedor = dblSuma
    Else
        If dblPorcentaje = 0 Then
            'Si el porcentaje es 0 se toma la cantidad del item que queda por adjudicar para dar una propuesta
            For Each oAdj In oGrupo.Adjudicaciones
                If oAdj.Id = lIdItem Then
                    If Not IsNull(oAdj.Escalado) Then
                        dblSuma = dblSuma + NullToDbl0(oAdj.Adjudicado)
                    Else
                        dblSuma = dblSuma + NullToDbl0(vCantItem * oAdj.Porcentaje / 100)
                    End If
                End If
            Next
            
            If dblSuma = 0 Then
                'Si no se ha adjudicado nada se reparte la cantidad del item entre los proveedores adjudicados
                iNumAdj = 0
                For Each oProve In oProvesAsig
                    sProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    Set oAdjProve = oAdjsProve.Item(CStr(lIdItem) & sProve)
                    
                    If Not oAdjProve Is Nothing Then iNumAdj = iNumAdj + 1
                                    
                    Set oAdjProve = Nothing
                Next
                Set oProve = Nothing
                
                If iNumAdj > 0 Then
                    CantidadProveedor = vCantItem / iNumAdj
                Else
                    CantidadProveedor = 0
                End If
            Else
                CantidadProveedor = vCantItem - dblSuma
            End If
        Else
            CantidadProveedor = vCantItem * dblPorcentaje / 100
        End If
    End If
End Function

''' <summary>Calcula el importe adjudicado a un proveedor en moneda de oferta</summary>
''' <param name="oProceso">Proceso</param>
''' <param name="oAdjudicaciones">Adjudicaciones</param>
''' <param name="oAtribsFormulas">Atributos</param>
''' <param name="oGrupo">Grupo para el que se est� haciendo el c�lculo</param>
''' <param name="oOferta">ultima oferta para el proceso</param>
''' <param name="sCodProve">C�digo del proveedor</param>
''' <param name="lIdItem">Id del item</param>
''' <param name="dblCantidadProveedor">Cantidad adjudicada al proveedor</param>
''' <returns>Importe adjudicado al proveedor</returns>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>LTG 20/02/2012</revision>

Public Function ImporteAdjudicadoProveedor(ByVal oProceso As cProceso, ByVal oAdjudicaciones As CAdjudicaciones, ByVal oAtribsFormulas As CAtributos, _
        ByVal oGrupo As CGrupo, ByVal oOferta As COferta, ByVal scodProve As String, ByVal lIdItem As Long, Optional ByVal dblCantidadProveedor As Double) As Double
    Dim dblImporte As Double
    Dim iNumAdj As Integer
    Dim iNumRangoAdj As Integer
    Dim dblSumaPrecios As Double
    Dim dblSumaImportes As Double
    Dim dblSumaImportesTot As Double
    Dim dblImporteTot As Double
    Dim dblSumaCantAdj As Double
    Dim sCod As String
    Dim oAdj As CAdjudicacion
    Dim oEscalado As CEscalado
    Dim dblSumAdjItem As Double
    Dim dblImporteAux As Double
        
    For Each oEscalado In oGrupo.Escalados
        sCod = KeyEscalado(scodProve, lIdItem, oEscalado.Id)
        Set oAdj = oAdjudicaciones.Item(sCod)
        
        If Not oAdj Is Nothing Then
            iNumAdj = iNumAdj + 1
            dblSumaCantAdj = dblSumaCantAdj + NullToDbl0(oAdj.Adjudicado)
                        
            If NullToDbl0(oAdj.Adjudicado) = 0 Then
                If Not oOferta.Lineas.Item(CStr(lIdItem)).Escalados.Item(CStr(oAdj.Escalado)) Is Nothing Then
                    'el importe se calcula con la media de los importes de los escalados adjudicados
                    dblImporteTot = dblCantidadProveedor * NullToDbl0(oOferta.Lineas.Item(CStr(lIdItem)).Escalados.Item(CStr(oAdj.Escalado)).PrecioOferta)
                    
                    dblSumaImportesTot = dblSumaImportesTot + AplicarAtributos(oProceso, oAtribsFormulas, dblImporteTot, TotalItem, scodProve, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, lIdItem, oEscalado.Id)
                End If
            Else
                dblSumaImportes = dblSumaImportes + AplicarAtributos(oProceso, oAtribsFormulas, oAdj.ImporteAdj, TotalItem, scodProve, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, lIdItem, oEscalado.Id)
            End If
            
            iNumRangoAdj = iNumRangoAdj + 1
        End If
        
        Set oAdj = Nothing
    Next
    Set oEscalado = Nothing
    
    dblImporte = 0
    If iNumAdj = 0 Then     'Or (iNumAdj > 0 And dblSumaCantAdj = 0) Then
        'Si no hay adjudicaciones o hay adjudicaciones pero no hay cantidades adjudicadas
        dblImporte = NullToDbl0(PrecioProveItemEsc(oGrupo, oOferta, oGrupo.Items.Item(CStr(lIdItem)))) * dblCantidadProveedor
    ElseIf iNumAdj > 0 And dblSumaCantAdj = 0 Then
        If iNumRangoAdj > 0 Then dblImporte = dblSumaImportesTot / iNumRangoAdj
    Else
        If iNumRangoAdj > 0 Then
            If dblSumaImportes > 0 Then
                dblImporte = dblSumaImportes
            Else
                If dblCantidadProveedor > 0 Then
                    dblImporte = (dblCantidadProveedor * dblSumaPrecios) / iNumRangoAdj
                Else
                    'Si el proveedor no tiene cantidad adjudicada se utiliza la cantidad proporcional que quede sin adjudicar del item
                    dblSumAdjItem = 0
                    For Each oAdj In oGrupo.Adjudicaciones
                        If oAdj.Id = lIdItem Then
                            dblSumAdjItem = dblSumAdjItem + NullToDbl0(oAdj.Adjudicado)
                        End If
                    Next
                    Set oAdj = Nothing
                    
                    dblImporte = ((NullToDbl0(oGrupo.Items.Item(CStr(lIdItem)).Cantidad) - dblSumAdjItem) * dblSumaPrecios) / iNumRangoAdj
                End If
            End If
        End If
    End If
    
    ImporteAdjudicadoProveedor = dblImporte
End Function

''' <summary>Calcula el importe adjudicado a un proveedor con atributos</summary>
''' <param name="oGrupo">Grupo para el que se est� haciendo el c�lculo</param>
''' <param name="sCodProve">C�digo del proveedor</param>
''' <param name="lIdItem">Id del item</param>
''' <returns>Importe adjudicado al proveedor con atributos</returns>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>LTG 20/02/2012</revision>

Public Function ImporteAdjudicadoProveedorTot(ByVal oGrupo As CGrupo, ByVal scodProve As String, ByVal lIdItem As Long) As Double
    Dim sCod As String
    Dim oAdj As CAdjudicacion
    Dim oEscalado As CEscalado
        
    For Each oEscalado In oGrupo.Escalados
        sCod = KeyEscalado(scodProve, lIdItem, oEscalado.Id)
        Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
        
        If Not oAdj Is Nothing Then
            ImporteAdjudicadoProveedorTot = ImporteAdjudicadoProveedorTot + oAdj.ImporteAdjtot
        End If
        
        Set oAdj = Nothing
    Next
    Set oEscalado = Nothing
End Function

''' <summary>Calcula el importe consumido para un proveedor en moneda de proceso</summary>
''' <param name="oGrupo">Grupo para el que se est� haciendo el c�lculo</param>
''' <param name="sProve">C�digo del proveedor</param>
''' <param name="lIdItem">Id del item</param>
''' <param name="dblCantidadProveedor">Cantidad asignada al proveedor</param>
''' <returns>Importe consumido</returns>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>LTG 22/02/2012</revision>

Public Function ConsumidoProveedor(ByVal oGrupo As CGrupo, ByVal sProve As String, ByVal lIdItem As Long, ByVal dblCantidadProveedor As Double) As Double
    Dim dblSumaPrecios As Double
    Dim dblSumaImportes As Double
    Dim dblSumaCantAdj As Double
    Dim iNumRangoAdj As Integer
    Dim sCod As String
    Dim oEscalado As CEscalado
    Dim vPres As Variant
    Dim oAdj As CAdjudicacion
    Dim dblSumAdjItem As Double
    Dim iNumAdj As Integer
        
    'importe item=presupuesto medio de los escalados adjudicados*cantidad prove
    For Each oEscalado In oGrupo.Escalados
        sCod = KeyEscalado(sProve, lIdItem, oEscalado.Id)
        
        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
            iNumAdj = iNumAdj + 1
            dblSumaCantAdj = dblSumaCantAdj + NullToDbl0(oGrupo.Adjudicaciones.Item(sCod).Adjudicado)
            
            vPres = oGrupo.Items.Item(CStr(lIdItem)).PresupuestoEscalado(oEscalado.Id)
            If Not IsNull(vPres) Then
                dblSumaImportes = dblSumaImportes + (vPres * NullToDbl0(oGrupo.Adjudicaciones.Item(sCod).Adjudicado))
                dblSumaPrecios = dblSumaPrecios + vPres
                
                iNumRangoAdj = iNumRangoAdj + 1
            End If
        End If
    Next
                
    If iNumAdj = 0 Then     'Or (iNumAdj > 0 And dblSumaCantAdj = 0) Then
        'Si no hay adjudicaciones o hay adjudicaciones pero no hay cantidades adjudicadas
        ConsumidoProveedor = dblCantidadProveedor * NullToDbl0(oGrupo.Items.Item(CStr(lIdItem)).PresupuestoProveItemEsc(oGrupo.Escalados))
    ElseIf iNumAdj > 0 And dblSumaCantAdj = 0 Then
        If iNumRangoAdj > 0 Then ConsumidoProveedor = dblCantidadProveedor * (dblSumaPrecios / iNumRangoAdj)
    Else
        If iNumRangoAdj > 0 Then
            If dblCantidadProveedor <> 0 Then
                ConsumidoProveedor = dblSumaImportes
            Else
                'Si el proveedor no tiene cantidad adjudicada se utiliza la cantidad proporcional que quede sin adjudicar del item
                dblSumAdjItem = 0
                For Each oAdj In oGrupo.Adjudicaciones
                    If oAdj.Id = lIdItem Then
                        dblSumAdjItem = dblSumAdjItem + NullToDbl0(oAdj.Adjudicado)
                    End If
                Next
                Set oAdj = Nothing
                
                ConsumidoProveedor = (dblSumaPrecios / iNumRangoAdj) * (NullToDbl0(oGrupo.Items.Item(CStr(lIdItem)).Cantidad - dblSumAdjItem))
            End If
        End If
    End If
    'Ponemos el valor del consumido del item para el proveedor en todas las adjudicaciones de los escalados
    For Each oEscalado In oGrupo.Escalados
        sCod = KeyEscalado(sProve, lIdItem, oEscalado.Id)
        
        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
            oGrupo.Adjudicaciones.Item(sCod).ConsumidoItemProve = ConsumidoProveedor 'mon proceso
        End If
    Next
    Set oEscalado = Nothing
End Function

''' <summary>Devuelve el objetivo del item para un escalado</summary>
''' <param name="oItem">Objeto item</param>
''' <param name="lIdEscalado">Id del escalado</param>
''' <returns>El objetivo</returns>
''' <remarks>Llamada desde: ImporteProveedor</remarks>
''' <revision>21/03/2012</revision>

Public Function ObjetivoEscalado(ByVal oItem As CItem, ByVal lIdEscalado As Long) As Variant
    Dim vObjetivo As Variant
    
    vObjetivo = Null
    
    If Not oItem.Escalados Is Nothing Then
        If Not oItem.Escalados.Item(CStr(lIdEscalado)) Is Nothing Then
            vObjetivo = oItem.Escalados.Item(CStr(lIdEscalado)).Objetivo
        End If
    End If
    
    'Si no hay presupuesto se utiliza para el c�lculo el presupuesto unitario del �tem
    If IsNull(vObjetivo) Then vObjetivo = oItem.Objetivo
    
    ObjetivoEscalado = vObjetivo
End Function

''' <summary>Construye la clave para las colecciones de adjudicaciones de escalados</summary>
''' <param name="sCodProve">C�digo del proveedor</param>
''' <param name="lIdItem">Id del item</param>
''' <param name="lIdEscalado">Id del escalado</param>
''' <returns>String con la clave</returns>
''' <remarks>Llamada desde: sdbgEscalado_RowColChange</remarks>
''' <revision>21/03/2012</revision>

Public Function KeyEscalado(ByVal scodProve As String, ByVal lIdItem As Long, ByVal lIdEscalado As Long) As String
    Dim sCod As String
    
    sCod = scodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(scodProve))
    KeyEscalado = CStr(lIdItem) & sCod & CStr(lIdEscalado)
End Function

''' <summary>Calcula el presupuesto unitario de un item para un proveedor en funci�n de los escalados adjudicados.
'''          Se calcula como la media de los presupuestos de los escalados adjudicados</summary>
''' <param name="oGrupo">grupo</param>
''' <param name="oItem">item</param>
''' <param name="sProve">Cod. proveedor</param>
''' <param name="dblCantAdjProve">Cantidad adjudicada al proveedor</param>
''' <remarks>Llamada desde: Recalcular</remarks>
''' <revision>LTG 22/02/2012</revision>

Public Function PresupuestoProveItemEscAdj(ByVal oGrupo As CGrupo, ByVal oItem As CItem, ByVal sProve As String, _
        ByVal dblCantAdjProve As Double) As Variant
    Dim oEscalado As CEscalado
    Dim oAdj As CAdjudicacion
    Dim dblPres As Double
    Dim dblSuma As Double
    Dim dblSumaCantAdjEsc As Double
    Dim sCod As String
    Dim iAdj As Integer
    Dim vPres As Variant
    
    PresupuestoProveItemEscAdj = Null
    
    dblPres = 0
    iAdj = 0
    For Each oEscalado In oGrupo.Escalados
        sCod = KeyEscalado(sProve, oItem.Id, oEscalado.Id)
        Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
        
        If Not oAdj Is Nothing Then
            vPres = oItem.PresupuestoEscalado(oEscalado.Id)
            If Not IsNull(vPres) Then
                If dblCantAdjProve > 0 Then
                    dblSuma = dblSuma + (NullToDbl0(oAdj.Adjudicado) * vPres)
                    dblSumaCantAdjEsc = dblSumaCantAdjEsc + NullToDbl0(oAdj.Adjudicado)
                End If
                
                dblPres = dblPres + vPres
                iAdj = iAdj + 1
            End If
        End If
    Next
    
    If dblSumaCantAdjEsc > 0 Then
        PresupuestoProveItemEscAdj = dblSuma / dblSumaCantAdjEsc
    Else
        If iAdj > 0 Then
            PresupuestoProveItemEscAdj = dblPres / iAdj
        End If
    End If
End Function

''' <summary>Calcula el objetivo unitario de un item para un proveedor en funci�n de los escalados.
'''          Se calcula como la media de los objetivos de los escalados</summary>
''' <param name="oGrupo">grupo</param>
''' <param name="oItem">item</param>
''' <remarks>Llamada desde: Recalcular</remarks>
''' <revision>LTG 22/02/2012</revision>

Public Function ObjetivoProveItemEsc(ByVal oGrupo As CGrupo, ByVal oItem As CItem) As Variant
    Dim oEscalado As CEscalado
    Dim dblObj As Double
    Dim sCod As String
    Dim iEsc As Integer
    Dim vObj As Variant
    
    ObjetivoProveItemEsc = Null
    
    dblObj = 0
    iEsc = 0
    For Each oEscalado In oGrupo.Escalados
        vObj = ObjetivoEscalado(oItem, oEscalado.Id)
        If Not IsNull(vObj) Then
            dblObj = dblObj + vObj
            iEsc = iEsc + 1
        End If
    Next
    Set oEscalado = Nothing
        
    If iEsc > 0 Then
        ObjetivoProveItemEsc = dblObj / iEsc
    End If
End Function

''' <summary>Calcula el precio unitario de un item para un proveedor en funci�n de los escalados adjudicados.
'''          Se calcula como la media de los precios de los escalados adjudicados</summary>
''' <param name="oGrupo">grupo</param>
''' <param name="oItem">item</param>
''' <param name="sProve">Cod. proveedor</param>
''' <param name="dblCantAdjProve">Cantidad adjudicada al proveedor</param>
''' <remarks>Llamada desde: Recalcular</remarks>
''' <revision>LTG 22/02/2012</revision>

Public Function PrecioProveItemEscAdj(ByVal oGrupo As CGrupo, ByVal oOferta As COferta, ByVal oItem As CItem, ByVal sProve As String, _
        ByVal dblCantAdjProve As Double) As Variant
    Dim oEscalado As CEscalado
    Dim oAdj As CAdjudicacion
    Dim dblPrec As Double
    Dim dblSuma As Double
    Dim dblSumaCantAdjEsc As Double
    Dim sCod As String
    Dim iAdj As Integer
    
    PrecioProveItemEscAdj = Null
    
    dblPrec = 0
    iAdj = 0
    For Each oEscalado In oGrupo.Escalados
        sCod = KeyEscalado(sProve, oItem.Id, oEscalado.Id)
        Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
            
        If Not oAdj Is Nothing Then
            If Not IsNull(oItem.PresupuestoEscalado(oEscalado.Id)) Then
                If Not oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)) Is Nothing Then
                    If dblCantAdjProve > 0 Then
                        dblSuma = dblSuma + (NullToDbl0(oAdj.Adjudicado) * oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)).PrecioOferta)
                        dblSumaCantAdjEsc = dblSumaCantAdjEsc + NullToDbl0(oAdj.Adjudicado)
                    End If
                    
                    dblPrec = dblPrec + oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)).PrecioOferta
                    iAdj = iAdj + 1
                End If
            End If
        End If
    Next
    Set oAdj = Nothing
        
    If dblSumaCantAdjEsc > 0 Then
        PrecioProveItemEscAdj = dblSuma / dblSumaCantAdjEsc
    Else
        If iAdj > 0 Then
            PrecioProveItemEscAdj = dblPrec / iAdj
        End If
    End If
End Function

''' <summary>Calcula el precio unitario de un item para un proveedor en funci�n de los escalados. En moneda oferta
'''          Se calcula como la media de los precios de los escalados</summary>
''' <param name="oGrupo">grupo</param>
''' <param name="oItem">item</param>
''' <remarks>Llamada desde: Recalcular</remarks>
''' <revision>LTG 22/02/2012</revision>

Public Function PrecioProveItemEsc(ByVal oGrupo As CGrupo, ByVal oOferta As COferta, ByVal oItem As CItem) As Variant
    Dim oEscalado As CEscalado
    Dim dblPrec As Double
    Dim iEsc As Integer
    
    PrecioProveItemEsc = Null
    
    dblPrec = 0
    iEsc = 0
    For Each oEscalado In oGrupo.Escalados
        If Not IsNull(oItem.PresupuestoEscalado(oEscalado.Id)) Then
            If Not oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)) Is Nothing Then
                If Not IsNull(oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)).PrecioOferta) Then
                    dblPrec = dblPrec + oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)).PrecioOferta
                    iEsc = iEsc + 1
                End If
            End If
        End If
    Next
            
    If iEsc > 0 Then
        PrecioProveItemEsc = dblPrec / iEsc
    End If
End Function

''' <summary>Comprueba que las cantidades adjudicadas a los escalados no superen la cantidad del �tem</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="lIdItem">Id. del item</param>
''' <returns>Booleano indicando si las cantidades adjudicadas son correctas</returns>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>21/03/2012</revision>

Public Function ComprobarSumaCantAdj(ByVal oGrupo As CGrupo, ByVal lIdItem As Long) As Boolean
    Dim oAdj As CAdjudicacion
    Dim dblSumaCant As Double
    
    ComprobarSumaCantAdj = True
    
    If NullToDbl0(oGrupo.Items.Item(CStr(lIdItem)).Cantidad) > 0 Then
        For Each oAdj In oGrupo.Adjudicaciones
            If oAdj.Id = lIdItem Then
                dblSumaCant = dblSumaCant + NullToDbl0(oAdj.Adjudicado)
            End If
        Next
            
        If dblSumaCant > oGrupo.Items.Item(CStr(lIdItem)).Cantidad Then
            ComprobarSumaCantAdj = False
        End If
    End If
End Function

''' <summary>Calcula el presupuesto de un item en funci�n de los escalados adjudicados.
'''          Se calcula como la media de los precios de los escalados adjudicados</summary>
''' <param name="oGrupo">grupo</param>
''' <param name="oItem">item</param>
''' <param name="oProvesAsig">Proveedores asignados</param>
''' <param name="vPrecio">Presupuesto unitario</param>
''' <param name="vObjetivo">Objetivo</param>
''' <param name="lIdEscalado">Id. del escalado</param>
''' <remarks>Llamada desde: Recalcular</remarks>
''' <revision>LTG 21/02/2012</revision>

Public Sub PresupuestoYObjetivoItemConEscalados(ByVal oGrupo As CGrupo, ByVal oItem As CItem, ByVal oProvesAsig As CProveedores, ByRef vPrecio As Variant, _
        ByRef vObjetivo As Variant, Optional ByVal lIdEscalado As Long)
    Dim oEscalado As CEscalado
    Dim oProve As CProveedor
    Dim sCod As String
    Dim iNumAdj As Integer
    Dim dblSumaPresupTotal As Double
    Dim dblSumaImportes As Double
    Dim dblSumaCantidades As Double
    Dim vSumaObjetivos As Variant
    Dim bEncontrado As Boolean
    Dim dblCant As Double
    Dim vPresItem As Variant
    Dim dblPres As Double
    
    vPrecio = 0
    vObjetivo = 0
                        
    If Not IsNull(oItem.Cantidad) And Not IsEmpty(oItem.Cantidad) Then
        'El presupuesto es el del escalado adjudicado (el que se corresponde con la cantidad)
        bEncontrado = False
        For Each oProve In oProvesAsig
            iNumAdj = 0
            dblSumaPresupTotal = 0
            dblSumaCantidades = 0
            vSumaObjetivos = Null
            dblCant = 0
            dblPres = 0
                
            If lIdEscalado <> 0 Then
                sCod = KeyEscalado(oProve.Cod, oItem.Id, lIdEscalado)
                If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                    If Not oItem.Escalados.Item(CStr(lIdEscalado)) Is Nothing Then
                        vObjetivo = oItem.Escalados.Item(CStr(lIdEscalado)).Objetivo 'En moneda proceso
                    End If
                    vPrecio = oItem.PresupuestoEscalado(lIdEscalado)  'En moneda proceso
                    
                    Exit For
                End If
            Else
                'Puede haber m�s de un escalado adjudicado
                'oGrupo.Adjudicaciones.Item(sCod).Adjudicado es la cantidad adjudicada al escalado
                For Each oEscalado In oGrupo.Escalados
                    sCod = KeyEscalado(oProve.Cod, oItem.Id, oEscalado.Id)
                    If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        vPresItem = oItem.PresupuestoEscalado(oEscalado.Id)  'Presup unitario del escalado
                        If Not IsNull(vPresItem) Then
                            iNumAdj = iNumAdj + 1
                            If Not oItem.Escalados.Item(CStr(lIdEscalado)) Is Nothing Then
                                vSumaObjetivos = vSumaObjetivos + oItem.Escalados.Item(CStr(lIdEscalado)).Objetivo
                            End If
                            dblSumaPresupTotal = dblSumaPresupTotal + (vPresItem * NullToDbl0(oGrupo.Adjudicaciones.Item(sCod).Adjudicado))
                            dblPres = dblPres + vPresItem 'Suma de presup unitarios
                    
                            dblCant = dblCant + NullToDbl0(oGrupo.Adjudicaciones.Item(sCod).Adjudicado) 'Suma de cantidades
                        End If
                    End If
                Next
                
                If dblCant > 0 Then 'Presupuesto unitario=presup total/cantidad
                    vPrecio = vPrecio + (dblSumaPresupTotal / dblCant)
                Else
                    If iNumAdj > 0 Then vPrecio = dblPres / iNumAdj 'Presupuesto unitario=suma de presup unitarios/numero de escalados adjudicados
                End If
                If iNumAdj > 0 Then vObjetivo = vObjetivo + (vSumaObjetivos / iNumAdj)
            End If
        Next
    Else
        If Not oItem Is Nothing Then
            For Each oProve In oProvesAsig
                iNumAdj = 0
                dblSumaPresupTotal = 0
                vSumaObjetivos = Null
                dblCant = 0
                
                If lIdEscalado <> 0 Then
                    sCod = KeyEscalado(oProve.Cod, oItem.Id, lIdEscalado)
                    If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        iNumAdj = 1
                        If Not oItem.Escalados.Item(CStr(lIdEscalado)) Is Nothing Then
                            vSumaObjetivos = oItem.Escalados.Item(CStr(lIdEscalado)).Objetivo
                        End If
                        dblSumaPresupTotal = NullToDbl0(oItem.PresupuestoEscalado(lIdEscalado))
                    End If
                Else
                    For Each oEscalado In oGrupo.Escalados
                        sCod = KeyEscalado(oProve.Cod, oItem.Id, oEscalado.Id)
                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                            vPresItem = oItem.PresupuestoEscalado(oEscalado.Id)
                            If Not IsNull(vPresItem) Then
                                'El escalado est� adjudicado por lo menos en un proveedor y hay que tenerlo en cuenta
                                'en el c�lculo del precio
                                iNumAdj = iNumAdj + 1
                                If Not oItem.Escalados.Item(CStr(lIdEscalado)) Is Nothing Then
                                    vSumaObjetivos = vSumaObjetivos + oItem.Escalados.Item(CStr(lIdEscalado)).Objetivo
                                End If
                                dblSumaPresupTotal = dblSumaPresupTotal + vPresItem
                                
                                'If oGrupo.adjudicaciones.Item(sCod).Adjudicado > dblCant Then dblCant = oGrupo.adjudicaciones.Item(sCod).Adjudicado
                                dblCant = dblCant + NullToDbl0(oGrupo.Adjudicaciones.Item(sCod).Adjudicado)
                            End If
                        End If
                    Next
                End If
                
                If iNumAdj > 0 Then
                    vPrecio = vPrecio + ((dblSumaPresupTotal / iNumAdj) * (dblCant / iNumAdj) / oItem.Cantidad)
                    vObjetivo = vObjetivo + (vSumaObjetivos / iNumAdj)
                End If
            Next
        End If
    End If
        
    Set oEscalado = Nothing
    Set oProve = Nothing
End Sub

''' <summary>Calcula los datos de las adjudicaciones para los grupos con escalados</summary>
''' <param name="oProcesoSeleccionado">proceso</param>
''' <param name="oGrupos">Grupos del proceso</param>
''' <param name="oProvesAsig">Asignaciones del proceso</param>
''' <param name="oAtribsFormulas">Atributos</param>
''' <returns>Objeto CAdjudicaciones con las adjudicaciones calculadas para los grupos</returns>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>LTG 20/02/2012</revision>

Public Function CalcularAdjudicacionesGruposEscalados(ByVal oProcesoSeleccionado As cProceso, ByVal oGrupos As CGrupos, ByVal oProvesAsig As CProveedores, _
        ByVal oAtribsFormulas As CAtributos) As CAdjudicaciones
    Dim oGrupo As CGrupo
    Dim oAdj As CAdjudicacion
    Dim oEscalado As CEscalado
    Dim oAdjProve As CAdjudicacion
    Dim oOferta As COferta
    Dim oItem As CItem
    Dim dblCantidadEscalado As Double
    Dim dblImporteEscalado As Double
    Dim dblCantidadProveedor As Double
    Dim dblImporteAdjudicadoProveedor As Double
    Dim dblImporteAdjudicadoProveedorTot As Double
    Dim dblConsumidoProve As Double 'En moneda proceso
    Dim dblImporteAhorroProve As Double 'En moneda oferta
    Dim dblPorcenAhorroProve As Double
    Dim scodProve As String
    Dim sCod As String
    Dim vImporteAdjudicadoItem As Variant
    Dim vImporteConsumidoItem As Variant
    Dim oProve As CProveedor
    Dim oAdjsProveEsc As CAdjudicaciones
    Dim dblImporteAux As Double
    Dim oEsc As CEscalado
    Dim iNumEscAdj As Integer
    Dim dblPorcen As Double
    
    Set oAdjsProveEsc = oFSGSRaiz.Generar_CAdjudicaciones
    
    For Each oGrupo In oGrupos
        If oGrupo.UsarEscalados Then
            For Each oAdj In oGrupo.Adjudicaciones
                'primero crear los items de la colecci�n de adjudicaciones de proveedor
                scodProve = oAdj.ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAdj.ProveCod))
                Set oAdjProve = oAdjsProveEsc.Item(CStr(oAdj.Id) & scodProve)
                If oAdjProve Is Nothing Then
                    Set oAdjProve = oAdjsProveEsc.Add(oAdj.ProveCod, oAdj.NumOfe, oAdj.Id, , NullToDbl0(oAdj.Porcentaje), , , , , oGrupo.Codigo, vCambio:=oAdj.Cambio)
                End If
            Next
            
            For Each oAdj In oGrupo.Adjudicaciones
                'primero crear los items de la colecci�n de adjudicaciones de proveedor
                Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oAdj.ProveCod))
                                                                
                'C�lculos escalado
                If Not IsNull(oAdj.Escalado) Then
                    Set oEscalado = oGrupo.Escalados.Item(CStr(oAdj.Escalado))
                    dblCantidadEscalado = NullToDbl0(oAdj.Adjudicado)
                    dblImporteEscalado = 0
                    If Not IsNull(oGrupo.Items.Item(CStr(oAdj.Id)).PresupuestoEscalado(oAdj.Escalado)) Then
                        If Not oOferta.Lineas.Item(CStr(oAdj.Id)).Escalados.Item(CStr(oAdj.Escalado)) Is Nothing Then
                            dblImporteEscalado = dblCantidadEscalado * NullToDbl0(oOferta.Lineas.Item(CStr(oAdj.Id)).Escalados.Item(CStr(oAdj.Escalado)).PrecioOferta)
                        End If
                    End If
                    
                    Set oItem = oGrupo.Items.Item(CStr(oAdj.Id))
                    If Not IsEmpty(oItem.Cantidad) And Not IsNull(oItem.Cantidad) And NullToDbl0(oAdj.Adjudicado) > 0 Then
                        If oItem.Cantidad > 0 Then oAdj.Porcentaje = (NullToDbl0(oAdj.Adjudicado) / oItem.Cantidad) * 100
                    ElseIf Not IsNull(oAdj.Porcentaje) Then
                        'Si no hay cantidades adjudicadas el porcentaje (que es el porcentaje de ITEM_ADJ) se reparte entre todos los escalados adjudicados
                        '(Si ninguno de ellos tiene una cantidad adjudicada)
                        oAdj.Porcentaje = 100
                        iNumEscAdj = 0
                        For Each oEsc In oGrupo.Escalados
                            sCod = KeyEscalado(oAdj.ProveCod, oAdj.Id, oEsc.Id)
                            If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                                If IsNull(oGrupo.Adjudicaciones.Item(sCod).Adjudicado) Then
                                    iNumEscAdj = iNumEscAdj + 1
                                Else
                                    iNumEscAdj = 0
                                    Exit For
                                End If
                            End If
                        Next
                        If iNumEscAdj > 0 Then
                            oAdj.Porcentaje = oAdj.Porcentaje / iNumEscAdj
                        Else
                            oAdj.Porcentaje = 0
                        End If
                    End If
                    oAdj.ImporteAdj = dblImporteEscalado 'mon oferta
                    oAdj.importe = CDec(dblImporteEscalado / oAdj.Cambio) 'mon proceso sin atribs
                    If ComprobarAplicarAtribsItem(oAtribsFormulas, CStr(oAdj.Id), oGrupo.Codigo) Then   'Si esta todo el item a este prove
                        oAdj.ImporteAdjtot = AplicarAtributos(oProcesoSeleccionado, oAtribsFormulas, oAdj.ImporteAdj, TotalItem, oAdj.ProveCod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, oAdj.Id, oAdj.Escalado)
                    Else
                        oAdj.ImporteAdjtot = oAdj.ImporteAdj 'mon oferta
                    End If
                End If
            Next
            
            'C�lculos proveedor
            For Each oAdjProve In oAdjsProveEsc
                If oAdjProve.Grupo = oGrupo.Codigo Then
                    Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oAdjProve.ProveCod))
                    
                    dblCantidadProveedor = CantidadProveedor(oGrupo, oProvesAsig, oAdjsProveEsc, oAdjProve.ProveCod, oAdjProve.Id, oAdjProve.Porcentaje)
                    dblImporteAdjudicadoProveedor = ImporteAdjudicadoProveedor(oProcesoSeleccionado, oGrupo.Adjudicaciones, oAtribsFormulas, oGrupo, oOferta, oAdjProve.ProveCod, oAdjProve.Id, dblCantidadProveedor)
                    dblImporteAdjudicadoProveedorTot = ImporteAdjudicadoProveedorTot(oGrupo, oAdjProve.ProveCod, oAdjProve.Id)
                    dblConsumidoProve = CDec(ConsumidoProveedor(oGrupo, oAdjProve.ProveCod, oAdjProve.Id, dblCantidadProveedor) * oAdjProve.Cambio) 'en mondeda oferta
                    dblImporteAhorroProve = dblConsumidoProve - dblImporteAdjudicadoProveedor  'En moneada oferta
                    If dblConsumidoProve > 0 Then dblPorcenAhorroProve = CDec(dblImporteAhorroProve / dblConsumidoProve) * 100
                    
                    'Colecci�n de adjudicaciones (datos reales)
                    oAdjProve.ConsumidoItemProve = dblConsumidoProve 'mon oferta
                    oAdjProve.Adjudicado = dblCantidadProveedor
                    oAdjProve.ImporteAdj = dblImporteAdjudicadoProveedor 'en mon oferta
                    oAdjProve.importe = CDec(dblImporteAdjudicadoProveedor / oAdjProve.Cambio) 'en mondeda proceso
                    oAdjProve.ImporteAdjtot = dblImporteAdjudicadoProveedorTot
                    oAdjProve.pedido = oAdjProve.pedido
                    oAdjProve.Catalogo = oAdjProve.Catalogo
                    'C�lculo de los porcentajes adjudicados
                    Set oItem = oGrupo.Items.Item(CStr(oAdjProve.Id))
                    If Not IsEmpty(oItem.Cantidad) And Not IsNull(oItem.Cantidad) Then
                        If oItem.Cantidad > 0 Then oAdjProve.Porcentaje = (oAdjProve.Adjudicado / oItem.Cantidad) * 100
                    End If
                End If
            Next
            
            'C�culos items
            For Each oItem In oGrupo.Items
                'Sumar las cantidades adjudicadas a los proveedores
                vImporteAdjudicadoItem = Empty
                vImporteConsumidoItem = Empty
                For Each oProve In oProvesAsig
                    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    sCod = CStr(oItem.Id) & scodProve
                    Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                    If Not oAdjsProveEsc Is Nothing Then
                        If Not oAdjsProveEsc.Item(sCod) Is Nothing Then
                            vImporteAdjudicadoItem = vImporteAdjudicadoItem + CDec(oAdjsProveEsc.Item(sCod).ImporteAdj / oAdjsProveEsc.Item(sCod).Cambio)
                            vImporteConsumidoItem = vImporteConsumidoItem + CDec(oAdjsProveEsc.Item(sCod).ConsumidoItemProve / oAdjsProveEsc.Item(sCod).Cambio)
                        End If
                    End If
                    Set oOferta = Nothing
                Next
                oItem.ImporteAdj = vImporteAdjudicadoItem
                oItem.Consumido = vImporteConsumidoItem
            Next
        End If
    Next
    
    Set CalcularAdjudicacionesGruposEscalados = oAdjsProveEsc
    
    Set oAdj = Nothing
    Set oEscalado = Nothing
    Set oAdjProve = Nothing
    Set oOferta = Nothing
    Set oItem = Nothing
    Set oProve = Nothing
    Set oGrupo = Nothing
    Set oAdjsProveEsc = Nothing
End Function

''' <summary>Calcula la adjudicaci�n del total a un proveedor dado cuando el grupo tiene escalados</summary>
''' <param name="sCodProve">C�digo del proveedor</param>
''' <param name="lItem">ID del item</param>
''' <param name="oProcesoSeleccionado">Proceso seleccionado</param>
''' <param name="oGrupo">Grupo seleccionado</param>
''' <param name="oOferta">Ultima oferta</param>
''' <param name="oProvesAsig">Proveedores asignados</param>
''' <param name="oAtribsFormulas">Atributos de f�rmula</param>
''' <returns>Adjudicacion para el 100% al proveedor</returns>
''' <remarks>Llamada desde: ObtenerGraficoItem</remarks>
''' <revision>LTG 28/02/2012</revision>

Public Function CalcularAdjTotalProveedorEsc(ByVal scodProve As String, ByVal lItem As Long, ByVal oProcesoSeleccionado As cProceso, ByVal oGrupo As CGrupo, _
        ByVal oOferta As COferta, ByVal oProvesAsig As CProveedores, ByVal oAtribsFormulas As CAtributos) As CAdjudicacion
    Dim oAdjsTot As CAdjudicaciones
    Dim oAdjsTotEsc As CAdjudicaciones
    Dim oAdjTot As CAdjudicacion
    Dim oAdjTotEsc As CAdjudicacion
    Dim oAdj As CAdjudicacion
    Dim dblCantidadEscalado As Double
    Dim dblImporteEscalado As Double
    Dim sCod As String
    Dim dblCantidadProveedor As Double
    Dim dblImporteAdjudicadoProveedor As Double
    Dim dblImporteProve As Double
    Dim dblImporteAux As Double
    Dim sProve As String
    
    If Not oGrupo.Adjudicaciones Is Nothing Then
        If oGrupo.Adjudicaciones.Count > 0 Then
            Set oAdjsTot = oFSGSRaiz.Generar_CAdjudicaciones
            Set oAdjsTotEsc = oFSGSRaiz.Generar_CAdjudicaciones
            
            For Each oAdj In oGrupo.Adjudicaciones
                sProve = oAdj.ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAdj.ProveCod))
                Set oAdjTot = oAdjsTot.Item(CStr(oAdj.Id) & sProve)
                If oAdjTot Is Nothing Then
                    Set oAdjTot = oAdjsTot.Add(oAdj.ProveCod, oAdj.NumOfe, oAdj.Id, , 100, , , , , oAdj.Grupo)
                End If
            Next
            
            For Each oAdj In oGrupo.Adjudicaciones
                If oAdj.ProveCod = scodProve And oAdj.Id = lItem Then
                    If Not IsNull(oAdj.Escalado) Then
                        Set oAdjTotEsc = oAdjsTotEsc.Add(scodProve, oOferta.Num, lItem, oOferta.Lineas.Item(CStr(lItem)).Escalados.Item(CStr(oAdj.Escalado)).PrecioOferta, _
                                            100, , oOferta.Lineas.Item(CStr(lItem)).Escalados.Item(CStr(oAdj.Escalado)).PrecioOferta, _
                                             oOferta.Lineas.Item(CStr(lItem)).Escalados.Item(CStr(oAdj.Escalado)).Objetivo, , oAdj.Grupo, , , , oAdj.Escalado)
                        
                        'C�lculos escalado
                        dblCantidadEscalado = NullToDbl0(oAdj.Adjudicado)
                        dblImporteEscalado = dblCantidadEscalado * oOferta.Lineas.Item(CStr(lItem)).Escalados.Item(CStr(oAdj.Escalado)).PrecioOferta
                        
                        'Datos de la adjudicaci�n
                        oAdjTotEsc.Adjudicado = dblCantidadEscalado
                        oAdjTotEsc.Porcentaje = 100
                        oAdjTotEsc.ImporteAdj = dblImporteEscalado
                        oAdjTotEsc.importe = CDec(dblImporteEscalado / oOferta.Cambio) 'en moneda de proceso
                        If ComprobarAplicarAtribsItem(oAtribsFormulas, CStr(lItem), oGrupo.Codigo) Then    'Si esta todo el item a este prove
                            oAdjTotEsc.ImporteAdjtot = AplicarAtributos(oProcesoSeleccionado, oAtribsFormulas, oAdjTotEsc.ImporteAdj, TotalItem, scodProve, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, lItem, oAdj.Escalado)
                        Else
                            oAdjTotEsc.ImporteAdjtot = oAdjTotEsc.ImporteAdj
                        End If
                    Else
                        'Caso de adjudicaci�n de cantidad a proveedor sin adjudicaciones en los escalados o con adjudicaciones pero sin cantidad
                        Set oAdjTotEsc = oAdjsTotEsc.Add(scodProve, oOferta.Num, lItem, , oAdj.Porcentaje, , , , , oAdj.Grupo)
                                                
                        'Datos de la adjudicaci�n
                        oAdjTotEsc.Adjudicado = CantidadProveedor(oGrupo, oProvesAsig, oAdjsTot, scodProve, lItem, oAdj.Porcentaje)
                        oAdjTotEsc.ImporteAdj = oAdjTotEsc.Adjudicado * PrecioProveItemEsc(oGrupo, oOferta, oGrupo.Items.Item(CStr(lItem)))
                        oAdjTotEsc.importe = CDec(NullToDbl0(oAdjTotEsc.ImporteAdj) / oOferta.Cambio)
                        If ComprobarAplicarAtribsItem(oAtribsFormulas, CStr(lItem), oGrupo.Codigo) Then    'Si esta todo el item a este prove
                            oAdjTotEsc.ImporteAdjtot = AplicarAtributos(oProcesoSeleccionado, oAtribsFormulas, oAdjTotEsc.ImporteAdj, TotalItem, scodProve, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, lItem)
                        Else
                            oAdjTotEsc.ImporteAdjtot = oAdjTotEsc.ImporteAdj
                        End If
                    End If
                End If
            Next
                                                            
            'C�lculos proveedor
            dblCantidadProveedor = CantidadProveedor(oGrupo, oProvesAsig, oAdjsTot, scodProve, lItem, oAdjTot.Porcentaje)
            dblImporteAdjudicadoProveedor = ImporteAdjudicadoProveedor(oProcesoSeleccionado, oAdjsTotEsc, oAtribsFormulas, oGrupo, oOferta, scodProve, lItem, dblCantidadProveedor)
            dblImporteProve = ConsumidoProveedor(oGrupo, scodProve, lItem, NullToDbl0(oGrupo.Items.Item(CStr(lItem)).Cantidad))
                                                                                                       
            'Actualizar la adjudicacion del proveedor (real)
            oAdjTot.Adjudicado = dblCantidadProveedor
            oAdjTot.Porcentaje = 100
            oAdjTot.ImporteAdj = dblImporteAdjudicadoProveedor
            'Guardo en .Importe el importe con los datos del item (para calcular el ahorro)
            oAdjTot.importe = AplicarAtributos(oProcesoSeleccionado, oAtribsFormulas, dblImporteProve, TotalItem, scodProve, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, lItem) / oOferta.Cambio
        End If
    End If
    
    Set CalcularAdjTotalProveedorEsc = oAdjTot
    
    Set oAdjsTot = Nothing
    Set oAdjsTotEsc = Nothing
    Set oAdjTot = Nothing
    Set oAdjTotEsc = Nothing
    Set oAdj = Nothing
End Function

''' <summary>Devuelve el n� de adjudicaciones para un proveedor y un item dado</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="sCodProve">Cod. proveedor</param>
''' <param name="lItem">Id. item</param>
''' <returns>N� de adjudicaciones existentes</returns>
''' <remarks>Llamada desde: Recalcular</remarks>
''' <revision>21/03/2012</revision>

Public Function NumAdjudicacionesProve(ByVal oGrupo As CGrupo, ByVal scodProve As String, ByVal lItem As Long) As Integer
    Dim oEsc As CEscalado
    Dim sCod As String
    
    NumAdjudicacionesProve = 0
    
    If Not oGrupo.Adjudicaciones Is Nothing Then
        For Each oEsc In oGrupo.Escalados
            sCod = KeyEscalado(scodProve, lItem, oEsc.Id)
            If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                NumAdjudicacionesProve = NumAdjudicacionesProve + 1
            End If
        Next
    End If
End Function

''' <summary>Indica si existen adjudicaciones para grupos de escalados sin cantidad o presupuesto unitario</summary>
''' <param name="bCierreTotal">Indica si es un cierre total</param>
''' <param name="oProce">Proceso cuyas adjudicaciones hay que cerrar</param>
''' <param name="oAdjs">Adjudicaciones que hay que comprobar en caso de que sea un cierre parcial</param>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: Adjudicar; Tiempo m�ximo: 0,1</remarks>
''' <revision>21/03/2012</revision>

Public Function ExistenAdjSinCantPresup(ByVal bCierreTotal As Boolean, ByVal oProce As cProceso, Optional ByVal oAdjs As CAdjudicaciones) As Boolean
    Dim oGrupo As CGrupo
    Dim oAdj As CAdjudicacion
    
    ExistenAdjSinCantPresup = False
    
    If bCierreTotal Then
        For Each oGrupo In oProce.Grupos
            If oGrupo.UsarEscalados Then
                For Each oAdj In oGrupo.Adjudicaciones
                    If IsNull(oGrupo.Items.Item(CStr(oAdj.Id)).Cantidad) Or IsNull(oGrupo.Items.Item(CStr(oAdj.Id)).PresupuestoEscalado(oAdj.Escalado)) Then
                        ExistenAdjSinCantPresup = True
                        Exit For
                    End If
                Next
                Set oAdj = Nothing
            End If
        Next
        Set oGrupo = Nothing
    Else
        If Not oAdjs Is Nothing Then
            For Each oAdj In oAdjs
                If oProce.Grupos.Item(oAdj.Grupo).UsarEscalados Then
                    If IsNull(oProce.Grupos.Item(oAdj.Grupo).Items.Item(CStr(oAdj.Id)).Cantidad) Or IsNull(oProce.Grupos.Item(oAdj.Grupo).Items.Item(CStr(oAdj.Id)).PresupuestoEscalado(oAdj.Escalado)) Then
                        ExistenAdjSinCantPresup = True
                        Exit For
                    End If
                End If
            Next
            Set oAdj = Nothing
        End If
    End If
End Function

''' <summary>
''' Comprueba que la adjudicaci�n de cantidades a escalados sea correcta. Hay que poner cantidad adjudicada en los escalados adjudicados
''' que van a entrar en el calculo del ahorro, esto es, los que tienen presupuesto en el escalado o si no lo tienen si hay presupuesto de �tem
''' </summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="sProveCod">Cod. proveedor</param>
''' <param name="lIdItem">Id. item</param>
''' <param name="oEscalados">Colecci�n de escalados que faltan de adjudicar la cantidad</param>
''' <returns>Booleano indicando si la adjudicaci�n es correcta</returns>
''' <remarks>Llamada desde: sdbgEscalado_BeforeUpdate</remarks>
''' <revision>21/03/2012</revision>

Public Function ComprobarCantAdjEscaladosProve(ByVal oGrupo As CGrupo, ByVal sProveCod As String, ByVal lIdItem As Long, ByRef oEscalados As CEscalados) As Boolean
    Dim oAdj As CAdjudicacion
    Dim oEsc As CEscalado
    Dim dblSuma As Double
    Dim sCod As String
    Dim bHayCant As Boolean
    Dim vPresEsc As Variant

    ComprobarCantAdjEscaladosProve = False
    
    bHayCant = True
    For Each oAdj In oGrupo.Adjudicaciones
        If oAdj.ProveCod = sProveCod And oAdj.Id = lIdItem Then
            If Not IsNull(oAdj.Escalado) Then
                vPresEsc = oGrupo.Items.Item(CStr(lIdItem)).PresupuestoEscalado(oAdj.Escalado)
                If Not IsNull(vPresEsc) And Not IsEmpty(vPresEsc) Then
                    If NullToDbl0(oAdj.Adjudicado) = 0 Then bHayCant = False
                End If
                
                dblSuma = dblSuma + NullToDbl0(oAdj.Adjudicado)
            End If
        End If
    Next
    Set oAdj = Nothing
    
    If Not bHayCant And dblSuma > 0 Then
        Set oEscalados = oFSGSRaiz.Generar_CEscalados
        
        For Each oEsc In oGrupo.Escalados
            sCod = KeyEscalado(sProveCod, lIdItem, oEsc.Id)
            Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
            If Not oAdj Is Nothing Then
                If Not NullToDbl0(oAdj.Adjudicado) > 0 Then
                    oEscalados.Add oEsc.Id, oEsc.Inicial, oEsc.final
                End If
            End If
            Set oAdj = Nothing
        Next
    Else
        ComprobarCantAdjEscaladosProve = True
    End If
End Function

''' <summary>Calcula los datos de las adjudicaciones a nivel de proveedor para mostrar en el grid (c�lculos internos)</summary>
''' <param name="oProceso">Proceso</param>
''' <param name="oGrupo">Grupo</param>
''' <param name="oProvesAsig">Proveedores asignados</param>
''' <param name="oAtributos">Atributos</param>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>LTG 20/02/2012</revision>

Public Function CalcularAdjudicacionesInterno(ByVal oProceso As cProceso, ByRef oGrupo As CGrupo, ByVal oProvesAsig As CProveedores, ByVal oAtributos As CAtributos) As CAdjudicaciones
    Dim oAdj As CAdjudicacion
    Dim oAdjProve As CAdjudicacion
    Dim oOferta As COferta
    Dim oProve As CProveedor
    Dim dblCantidadProveedor As Double
    Dim dblImporteAdjudicadoProveedor As Double
    Dim dblConsumidoProve As Double
    Dim dblImporteAhorroProve As Double
    Dim dblPorcenAhorroProve As Double
    Dim scodProve As String
    Dim oAdjsProve As CAdjudicaciones
    Dim oItem As CItem
    Dim colNumAdjsProve As Dictionary
    Dim colCantAdjsProve As Dictionary

        If oGrupo.UsarEscalados Then
        Set oAdjsProve = oFSGSRaiz.Generar_CAdjudicaciones
        Set colNumAdjsProve = New Dictionary
        Set colCantAdjsProve = New Dictionary
        
        'Inicializaci�n colecciones
        For Each oProve In oProvesAsig
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            
            For Each oItem In oGrupo.Items
                colNumAdjsProve.Add CStr(oItem.Id) & scodProve, 0
                colCantAdjsProve.Add CStr(oItem.Id) & scodProve, 0
            Next
        Next
        Set oProve = Nothing
        Set oItem = Nothing
        
        For Each oAdj In oGrupo.Adjudicaciones
            Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oAdj.ProveCod))
            
            'primero crear los items de la colecci�n de adjudicaciones de proveedor
            scodProve = oAdj.ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAdj.ProveCod))
            Set oAdjProve = oAdjsProve.Item(CStr(oAdj.Id) & scodProve)
            If oAdjProve Is Nothing Then
                Set oAdjProve = oAdjsProve.Add(oAdj.ProveCod, oOferta.Num, oAdj.Id, , NullToDbl0(oAdj.Porcentaje), , , , , oGrupo.Codigo, , , , , , , , oAdj.Cambio)
            Else
                oAdjProve.Porcentaje = oAdjProve.Porcentaje + oAdj.Porcentaje
            End If
            
            'Acumulaci�n de s�lo las adjudicaciones a escalados (no las posibles de proveedor)
            If Not IsNull(oAdj.Escalado) Then
                colNumAdjsProve.Item(CStr(oAdj.Id) & scodProve) = colNumAdjsProve.Item(oAdj.ProveCod) + 1
                colCantAdjsProve.Item(CStr(oAdj.Id) & scodProve) = colCantAdjsProve.Item(oAdj.ProveCod) + NullToDbl0(oAdj.Adjudicado)
            End If
        Next
        
        'C�lculos proveedor
        For Each oAdjProve In oAdjsProve
            If oAdjProve.Grupo = oGrupo.Codigo Then
                scodProve = oAdjProve.ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAdjProve.ProveCod))
                
                Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oAdjProve.ProveCod))
                
                dblCantidadProveedor = CantidadProveedor(oGrupo, oProvesAsig, oAdjsProve, oAdjProve.ProveCod, oAdjProve.Id, oAdjProve.Porcentaje)
                dblImporteAdjudicadoProveedor = ImporteAdjudicadoProveedor(oProceso, oGrupo.Adjudicaciones, oAtributos, oGrupo, oOferta, oAdjProve.ProveCod, oAdjProve.Id, dblCantidadProveedor) / oAdjProve.Cambio
                dblConsumidoProve = ConsumidoProveedor(oGrupo, oAdjProve.ProveCod, oAdjProve.Id, dblCantidadProveedor)
                dblImporteAhorroProve = dblConsumidoProve - dblImporteAdjudicadoProveedor
                If dblConsumidoProve > 0 Then dblPorcenAhorroProve = (dblImporteAhorroProve / dblConsumidoProve) * 100
                         
                oAdjProve.Adjudicado = dblCantidadProveedor 'Se utiliza la prop. Adjudicado para guardar la cantidad
                Set oItem = oGrupo.Items.Item(CStr(oAdjProve.Id))
                If Not IsNull(oItem.Cantidad) And Not IsEmpty(oItem.Cantidad) Then
                    oAdjProve.Porcentaje = (dblCantidadProveedor / oItem.Cantidad) * 100
                Else
                    oAdjProve.Porcentaje = 0
                End If
                If dblImporteAdjudicadoProveedor > 0 Then
                    'Si no hay adjudicaciones o hay adjudicaciones pero no hay cantidades adjudicadas
                    If colNumAdjsProve.Item(CStr(oAdjProve.Id) & scodProve) = 0 Then    'Or (colNumAdjsProve.Item(CStr(oAdjProve.Id) & scodProve) > 0 And colCantAdjsProve.Item(CStr(oAdjProve.Id) & scodProve) = 0) Then
                        oAdjProve.PresUnitario = oGrupo.Items.Item(CStr(oAdjProve.Id)).PresupuestoProveItemEsc(oGrupo.Escalados)
                        oAdjProve.Precio = PrecioProveItemEsc(oGrupo, oOferta, oGrupo.Items.Item(CStr(oAdjProve.Id)))
                    Else
                        oAdjProve.PresUnitario = PresupuestoProveItemEscAdj(oGrupo, oGrupo.Items.Item(CStr(oAdjProve.Id)), oAdjProve.ProveCod, dblCantidadProveedor)
                        oAdjProve.Precio = PrecioProveItemEscAdj(oGrupo, oOferta, oGrupo.Items.Item(CStr(oAdjProve.Id)), oAdjProve.ProveCod, dblCantidadProveedor)
                    End If
                    oAdjProve.ImporteAdj = dblImporteAdjudicadoProveedor
                    oAdjProve.importe = dblImporteAhorroProve   'Se utiliza la prop. Importe para guardar el ahorro
                    oAdjProve.ImporteAdjtot = dblConsumidoProve  'Se utiliza la prop. ImporteAdjTot para guardar el consumido
                End If
            End If
        Next
         
        Set CalcularAdjudicacionesInterno = oAdjsProve
    End If
    
    Set oAdj = Nothing
    Set oAdjProve = Nothing
    Set oOferta = Nothing
    Set oAdjsProve = Nothing
    Set oItem = Nothing
    Set colNumAdjsProve = Nothing
    Set colCantAdjsProve = Nothing
End Function

''' <summary>Calcula la cantidad adjudicada a un item/proveedor por escalados</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="lIdItem">Id del item que se est� modificando</param>
''' <param name="sCodProv">C�digo del proveedor</param>
''' <returns>Cantidad adjudicada al proveedor/item</returns>
''' <remarks>Llamada desde: RecalculaPorEscalado</remarks>
''' <revision>21/03/2012</revision>

Public Function CantidadAdjItemProvePorEscalado(ByVal oGrupo As CGrupo, ByVal lIdItem As Long, ByVal scodProve As String) As Double
    Dim oEsc As CEscalado
    Dim sCod As String
    
    CantidadAdjItemProvePorEscalado = 0
    
    For Each oEsc In oGrupo.Escalados
        sCod = KeyEscalado(scodProve, lIdItem, oEsc.Id)
        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
            CantidadAdjItemProvePorEscalado = CantidadAdjItemProvePorEscalado + NullToDbl0(oGrupo.Adjudicaciones.Item(sCod).Adjudicado)
        End If
    Next
    Set oEsc = Nothing
End Function

''' <summary>Calcula la cantidad adjudicada a un item para un escalado</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="lIdItem">Id. �tem</param>
''' <param name="lIdEsc">Id. escalado</param>
''' <param name="oProvesAsig">Proveedores asignados</param>
''' <returns>Cantidad adjudicada al item para el escalado indicado</returns>
''' <remarks>Llamada desde: CtlADJEscalado.AddItem</remarks>
''' <revision>16/10/2012</revision>

Public Function CantidadAdjItemEscalado(ByVal oGrupo As CGrupo, ByVal lIdItem As Long, ByVal lIdEsc As Long, ByVal oProvesAsig As CProveedores) As Variant
    Dim oProve As CProveedor
    Dim sCod As String
    
    For Each oProve In oProvesAsig
        sCod = KeyEscalado(oProve.Cod, lIdItem, lIdEsc)
        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
            If NullToDbl0(oGrupo.Adjudicaciones.Item(sCod).Adjudicado) > 0 Then
                CantidadAdjItemEscalado = CantidadAdjItemEscalado + NullToDbl0(oGrupo.Adjudicaciones.Item(sCod).Adjudicado)
            End If
        End If
    Next
    Set oProve = Nothing
End Function

''' <summary>Indica si la cantidad de un proveedor es editable</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="lIdItem">Id. del item</param>
''' <param name="sCodProve">Cod. proveedor</param>
''' <returns>Booleano indicando si la cantidad del proveedor es editable</returns>
''' <remarks>Llamada desde: sdbgEscalado_RowColChange</remarks>
''' <revision>LTG 23/02/2012</revision>

Public Function CantidadProveedorEditable(ByVal oGrupo As CGrupo, ByVal lIdItem As Long, ByVal scodProve As String) As Boolean
    Dim oAdj As CAdjudicacion
    Dim dblSuma As Double
    Dim iNumAdj As Integer
              
    CantidadProveedorEditable = False
    
    'La cantidad del proveedor estar� editable si hay cantidad en el �tem y hay alguna adjudicaci�n hecha pero sin cantidad
    If Not IsNull(oGrupo.Items.Item(CStr(lIdItem)).Cantidad) And Not IsEmpty(oGrupo.Items.Item(CStr(lIdItem)).Cantidad) Then
        If Not oGrupo.Adjudicaciones Is Nothing Then
            If oGrupo.Adjudicaciones.Count > 0 Then
                iNumAdj = 0
                
                For Each oAdj In oGrupo.Adjudicaciones
                    If oAdj.ProveCod = scodProve And oAdj.Id = lIdItem And Not IsNull(oAdj.Escalado) Then
                        dblSuma = dblSuma + NullToDbl0(oAdj.Adjudicado)
                        iNumAdj = iNumAdj + 1
                    End If
                Next
                Set oAdj = Nothing
                    
                If dblSuma = 0 And iNumAdj > 0 Then CantidadProveedorEditable = True
                
                Set oGrupo = Nothing
            End If
        End If
    End If
End Function

''' <summary>Calcula el importe �ptimo de un item con escalados en moneda de oferta</summary>
''' <param name="sCodProve">C�digo del proveedor</param>
''' <param name="oItem">Objeto item</param>
''' <param name="oOferta">Objeto escalado de la oferta para el que se calcula el importe</param>
''' <param name="lIdEscalado">Id del escalado para el que se ha calculado el rpecio �ptimo</param>
''' <param name="dblCant">Cantidad adjudicada</param>
''' <remarks>Llamada desde: CalcularOptimaItemEsc; Tiempo m�ximo: 0,1</remarks>
''' <revision>LTG 22/02/2012</revision>

Public Function ImporteOptimoItemEsc(ByVal scodProve As String, ByVal oItem As CItem, ByVal oOferta As COferta, ByRef lIdEscalado As Long, ByRef oProcesoSeleccionado As cProceso, ByRef oAtribsFormulas As CAtributos, _
        ByRef dblImporteAux As Double, Optional ByRef dblImporteTotalItem As Double, Optional ByRef dblCant As Double) As Double
    Dim oEscalado As CEscalado
    Dim dblImporteEsc As Double
    Dim dblImpOpt As Double
    Dim dblCantidad As Double
    Dim vPresEsc As Variant
    Dim lIdEscOfeMin As Long
    Dim dblPrecOfeMin As Double
    Dim lEscVal As Long
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lIdEscalado = 0
    lEscVal = 0
        
    If Not IsNull(oItem.Cantidad) And Not IsEmpty(oItem.Cantidad) Then
        dblCantidad = oItem.Cantidad
        lEscVal = oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.EscaladoValido(oItem.Cantidad)
    Else
        dblCantidad = 1
    End If
        
    For Each oEscalado In oOferta.Lineas.Item(CStr(oItem.Id)).Escalados
        If Not IsNull(oEscalado.PrecioOferta) Then
            If lEscVal = 0 Or (lEscVal <> 0 And oEscalado.Id = lEscVal) Then
                vPresEsc = oItem.PresupuestoEscalado(oEscalado.Id)
                If Not IsNull(vPresEsc) Then
                    dblImporteEsc = CDec(dblCantidad * CDec(oEscalado.PrecioOferta))
                    If ComprobarAplicarAtribsItem(oAtribsFormulas, CStr(oItem.Id), oItem.grupoCod) Then     'Si esta todo el item a este prove
                        dblImporteEsc = AplicarAtributos(oProcesoSeleccionado, oAtribsFormulas, dblImporteEsc, TotalItem, scodProve, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oItem.grupoCod, oItem.Id, oEscalado.Id)
                    End If
                    dblImporteEsc = CDec(dblImporteEsc / dblCantidad)
                    
                    If dblImpOpt = 0 Or (dblImpOpt > 0 And dblImporteEsc > 0 And dblImporteEsc < dblImpOpt) Then
                        dblImporteTotalItem = dblImporteEsc * dblCantidad
                        dblCant = dblCantidad
                        dblImpOpt = dblImporteEsc
                        lIdEscalado = oEscalado.Id
                    End If
                Else
                    'Se va guardando la mejor oferta para el caso de que no haya presupuesto para ning�n escalado y por tanto los importes sean todos 0
                    If dblPrecOfeMin = 0 Or (dblPrecOfeMin > 0 And oEscalado.PrecioOferta < dblPrecOfeMin) Then
                        lIdEscOfeMin = oEscalado.Id
                        dblPrecOfeMin = oEscalado.PrecioOferta
                    End If
                End If
            End If
        End If
    Next
    
    'Caso de todos los escalados sin presupuesto
    If lIdEscalado = 0 Then
        dblImporteTotalItem = dblPrecOfeMin * dblCantidad
        dblCant = dblCantidad
        dblImpOpt = dblPrecOfeMin
        lIdEscalado = lIdEscOfeMin
    End If
    
    ImporteOptimoItemEsc = dblImpOpt
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
   If err.Number <> 0 Then
      oFSGSRaiz.TratarError "Formulario", "frmADJ", "ImporteOptimoItemEsc", err, Erl
      Exit Function
   End If
End Function


