VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmESTRCOMPDetalleCom 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Detalle"
   ClientHeight    =   5985
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4125
   Icon            =   "frmESTRCOMComDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5985
   ScaleWidth      =   4125
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   3420
      Left            =   915
      ScaleHeight     =   3420
      ScaleWidth      =   3585
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   60
      Width           =   3585
      Begin VB.TextBox txtCargo 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   17
         Top             =   1320
         Width           =   2800
      End
      Begin VB.TextBox txtTfno2 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         MaxLength       =   20
         TabIndex        =   16
         Top             =   2160
         Width           =   2800
      End
      Begin VB.TextBox txtApel 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         MaxLength       =   100
         TabIndex        =   1
         Top             =   900
         Width           =   2800
      End
      Begin VB.TextBox txtCod 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         MaxLength       =   3
         TabIndex        =   0
         Top             =   60
         Width           =   1155
      End
      Begin VB.TextBox txtMail 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         MaxLength       =   100
         TabIndex        =   5
         Top             =   3000
         Width           =   2800
      End
      Begin VB.TextBox txtFax 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         MaxLength       =   20
         TabIndex        =   4
         Top             =   2580
         Width           =   2800
      End
      Begin VB.TextBox txtTfno 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         MaxLength       =   20
         TabIndex        =   3
         Top             =   1740
         Width           =   2800
      End
      Begin VB.TextBox txtNom 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         MaxLength       =   50
         TabIndex        =   2
         Top             =   480
         Width           =   2800
      End
   End
   Begin VB.Timer timDet 
      Interval        =   60000
      Left            =   3840
      Top             =   5280
   End
   Begin MSComctlLib.TreeView tvwestrorg 
      Height          =   2400
      Left            =   60
      TabIndex        =   13
      Top             =   3480
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   4233
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3840
      Top             =   5880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMComDetalle.frx":0CB2
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMComDetalle.frx":0D51
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMComDetalle.frx":0E01
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMComDetalle.frx":0EB1
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMComDetalle.frx":0F61
            Key             =   "Persona"
            Object.Tag             =   "Persona"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMComDetalle.frx":0FCC
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMComDetalle.frx":135F
            Key             =   "PerCesta"
            Object.Tag             =   "PerCesta"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblCargo 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "DCargo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   195
      Left            =   45
      TabIndex        =   15
      Top             =   1440
      Width           =   600
   End
   Begin VB.Label lblTfno2 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "DTfno2:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   195
      Left            =   45
      TabIndex        =   14
      Top             =   2280
      Width           =   585
   End
   Begin VB.Label lblNom 
      BackColor       =   &H00808000&
      Caption         =   "Nombre:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   45
      TabIndex        =   12
      Top             =   600
      Width           =   945
   End
   Begin VB.Label lblCod 
      BackColor       =   &H00808000&
      Caption         =   "C�digo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   45
      TabIndex        =   11
      Top             =   180
      Width           =   855
   End
   Begin VB.Label lblApel 
      BackColor       =   &H00808000&
      Caption         =   "Apellidos:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   45
      TabIndex        =   10
      Top             =   1020
      Width           =   945
   End
   Begin VB.Label lblTfno 
      BackColor       =   &H00808000&
      Caption         =   "Tfno.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   45
      TabIndex        =   9
      Top             =   1860
      Width           =   825
   End
   Begin VB.Label lblFax 
      BackColor       =   &H00808000&
      Caption         =   "Fax:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   45
      TabIndex        =   8
      Top             =   2700
      Width           =   825
   End
   Begin VB.Label lblMail 
      BackColor       =   &H00808000&
      Caption         =   "E-mail:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   45
      TabIndex        =   7
      Top             =   3120
      Width           =   825
   End
End
Attribute VB_Name = "frmESTRCOMPDetalleCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'HBA
Private sIdiCod
Private sIdiApel

Public UON1 As String
Public UON2 As String
Public UON3 As String
Public Dep As String

Public sOrigen As String

Private Sub Form_Activate()

    'A�ade el comprador y su correspondiente estructura
    A�adirComprador
End Sub

Private Sub Form_Load()

    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCOM
    
    picDatos.Enabled = False

    'Carga los textos seg�n el idioma
    CargarRecursos

End Sub

Private Sub Form_Unload(Cancel As Integer)

If sOrigen = "frmESTRCOMP" Then
    Set frmESTRCOMP.oCompradorSeleccionado = Nothing
    Set frmESTRCOMP.oIBaseDatos = Nothing
End If

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTR_COMPDETALLECOM, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        lblCod.Caption = Ador(0).Value
        Ador.MoveNext
        lblApel.Caption = Ador(0).Value
        Ador.MoveNext
        lblNom.Caption = Ador(0).Value
        Ador.MoveNext
        lblTfno.Caption = Ador(0).Value
        Ador.MoveNext
        lblFax.Caption = Ador(0).Value
        Ador.MoveNext
        lblMail.Caption = Ador(0).Value
        Ador.MoveNext
        'cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        'cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiCod = Ador(0).Value
        Ador.MoveNext
        sIdiApel = Ador(0).Value
        Ador.MoveNext
        lblTfno2.Caption = Ador(0).Value
        Ador.MoveNext
        lblCargo.Caption = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing

End Sub

Private Sub A�adirComprador()
'A�ade el comprador y se posiciona en �l
Dim nodx As Node
Dim scod1 As String
Dim sCod2 As String
Dim sCod3 As String
Dim sCod4 As String
Dim iNivel As Integer

Dim oUON1 As CUnidadesOrgNivel1
Dim oUON2 As CUnidadesOrgNivel2
Dim oUON3 As CUnidadesOrgNivel3
Dim oDep As CDepartamentos
    
    ' Unidades organizativas
    iNivel = 0
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
      
    If UON1 <> "" Then
        Set oUON1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
        oUON1.CargarTodasLasUnidadesOrgNivel1 UON1, UON2, UON3, Dep, True, , UON1, , True
        
        iNivel = 1
        scod1 = UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(UON1))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, UON1 & " - " & oUON1.Item(1).Den, "UON1")
        nodx.Tag = "UON1" & CStr(UON1)
    End If
    
    If UON2 <> "" Then
        Set oUON2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
        oUON2.CargarTodasLasUnidadesOrgNivel2 UON1, UON2, UON3, Dep, True, , UON2, , True
        
        iNivel = 2
        scod1 = UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(UON1))
        sCod2 = scod1 & UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(UON2))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & sCod2, UON2 & " - " & oUON2.Item(1).Den, "UON2")
        nodx.Tag = "UON2" & CStr(UON2)
    End If
    
    If UON3 <> "" Then
        Set oUON3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
        oUON3.CargarTodasLasUnidadesOrgNivel3 UON1, UON2, UON3, Dep, True, , UON3, , True
        
        iNivel = 3
        scod1 = UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(UON1))
        sCod2 = scod1 & UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(UON2))
        sCod3 = sCod2 & UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(UON3))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & sCod2, tvwChild, "UON3" & sCod3, UON3 & " - " & oUON3.Item(1).Den, "UON3")
        nodx.Tag = "UON3" & CStr(UON3)
    End If
    
    'Departamento
    If Dep <> "" Then
        Set oDep = oFSGSRaiz.Generar_CDepartamentos
        oDep.CargarTodosLosDepartamentos Dep, , True
        
        If sCod3 <> "" Then
            sCod4 = sCod3 & Dep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(Dep))
            Set nodx = tvwestrorg.Nodes.Add("UON" & iNivel & sCod3, tvwChild, "DEP" & sCod4, Dep & " - " & oDep.Item(1).Den, "Departamento")
        ElseIf sCod2 <> "" Then
            sCod4 = sCod2 & Dep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(Dep))
            Set nodx = tvwestrorg.Nodes.Add("UON" & iNivel & sCod2, tvwChild, "DEP" & sCod4, Dep & " - " & oDep.Item(1).Den, "Departamento")
        ElseIf scod1 <> "" Then
            sCod4 = scod1 & Dep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(Dep))
            Set nodx = tvwestrorg.Nodes.Add("UON" & iNivel & scod1, tvwChild, "DEP" & sCod4, Dep & " - " & oDep.Item(1).Den, "Departamento")
            Else
            sCod4 = Dep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(Dep))
            Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEP" & sCod4, Dep & " - " & oDep.Item(1).Den, "Departamento")
            
        End If
        nodx.Tag = "DEP" & CStr(Dep)
    
        'a�ade el comprador al �rbol
        If sOrigen = "frmESTRCOMP" Then
            Set nodx = tvwestrorg.Nodes.Add("DEP" & sCod4, tvwChild, "PERS" & CStr(frmESTRCOMP.oCompradorSeleccionado.Cod), CStr(frmESTRCOMP.oCompradorSeleccionado.Cod) & " - " & frmESTRCOMP.oCompradorSeleccionado.Apel & " " & frmESTRCOMP.oCompradorSeleccionado.nombre, "Persona")
            nodx.Tag = "PER" & CStr(frmESTRCOMP.oCompradorSeleccionado.Cod)
        Else
            Set nodx = tvwestrorg.Nodes.Add("DEP" & sCod4, tvwChild, "PERS" & CStr(txtCod), CStr(txtCod) & " - " & Me.txtApel & " " & Me.txtNom, "Persona")
            nodx.Tag = "PER" & CStr(txtCod)
        End If
        nodx.Selected = True
        
    End If
    
    'Cierra los recordsets
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    Set oDep = Nothing
End Sub


