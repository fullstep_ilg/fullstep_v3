VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmAdmProvePortal 
   Caption         =   "DAdministraci�n de proveedores del portal"
   ClientHeight    =   5820
   ClientLeft      =   1695
   ClientTop       =   4920
   ClientWidth     =   8445
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAdmProvePortal.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5820
   ScaleWidth      =   8445
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraActividad 
      Caption         =   "DBusqueda por actividades"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1755
      Left            =   150
      TabIndex        =   14
      Top             =   180
      Width           =   8205
      Begin VB.CommandButton cmdBuscar1 
         Caption         =   "&Buscar"
         Height          =   315
         Left            =   6150
         TabIndex        =   1
         Top             =   720
         Width           =   1365
      End
      Begin VB.TextBox txtActividad 
         Height          =   285
         Left            =   1050
         TabIndex        =   0
         Top             =   720
         Width           =   4785
      End
      Begin VB.Label lblComentario 
         Caption         =   "D(Introduzca los primeros caracteres de la actividad a buscar)"
         Height          =   225
         Left            =   1050
         TabIndex        =   20
         Top             =   1140
         Width           =   4875
      End
   End
   Begin VB.Frame fraDirecta 
      Caption         =   "DBusqueda directa"
      Height          =   3620
      Left            =   150
      TabIndex        =   15
      Top             =   1980
      Width           =   8205
      Begin VB.CheckBox chkNoFS 
         Caption         =   "DMostrar s�lo proveedores sin correspondencia en FullStep GS"
         Height          =   270
         Left            =   600
         TabIndex        =   21
         Top             =   1950
         Width           =   7060
      End
      Begin VB.CommandButton cmdBuscar2 
         Caption         =   "&Buscar"
         Height          =   315
         Left            =   6150
         TabIndex        =   13
         Top             =   2970
         Width           =   1365
      End
      Begin VB.CommandButton cmdSELACT 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6270
         Picture         =   "frmAdmProvePortal.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   360
         Width           =   285
      End
      Begin VB.TextBox txtNombre 
         Height          =   285
         Left            =   1830
         TabIndex        =   9
         Top             =   1470
         Width           =   4785
      End
      Begin VB.CommandButton cmdBorrar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5970
         Picture         =   "frmAdmProvePortal.frx":01B6
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   360
         Width           =   285
      End
      Begin VB.CheckBox chkSoloAut 
         Caption         =   "DMostrar solo proveedores autorizados"
         Height          =   255
         Left            =   900
         TabIndex        =   11
         Top             =   2700
         Width           =   5340
      End
      Begin VB.CheckBox chkSoloFS 
         Caption         =   "DMostrar solo proveedores con correspondencia en FullStep GS"
         Height          =   270
         Left            =   600
         TabIndex        =   10
         Top             =   2360
         Width           =   7060
      End
      Begin VB.CheckBox chkPremium 
         Caption         =   "DMostrar solo proveedores Premium"
         Height          =   270
         Left            =   600
         TabIndex        =   12
         Top             =   3100
         Width           =   6135
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
         Height          =   285
         Left            =   1830
         TabIndex        =   5
         Top             =   720
         Width           =   945
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2540
         Columns(0).Caption=   "DCod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "DDenominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
         Height          =   285
         Left            =   2790
         TabIndex        =   6
         Top             =   720
         Width           =   3795
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   6165
         Columns(0).Caption=   "DDenominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "DCod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6694
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProviCod 
         Height          =   285
         Left            =   1830
         TabIndex        =   7
         Top             =   1080
         Width           =   945
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2540
         Columns(0).Caption=   "DCod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "DDenominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProviDen 
         Height          =   285
         Left            =   2790
         TabIndex        =   8
         Top             =   1080
         Width           =   3795
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   6165
         Columns(0).Caption=   "DDenominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "DCod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6694
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblNombre 
         Caption         =   "DNombre:"
         Height          =   255
         Left            =   675
         TabIndex        =   19
         Top             =   1500
         Width           =   1155
      End
      Begin VB.Label lblActi 
         Caption         =   "DActividad:"
         Height          =   225
         Left            =   675
         TabIndex        =   18
         Top             =   360
         Width           =   1155
      End
      Begin VB.Label lblPais 
         Caption         =   "DPa�s:"
         Height          =   225
         Left            =   675
         TabIndex        =   17
         Top             =   720
         Width           =   1155
      End
      Begin VB.Label lblProvincia 
         Caption         =   "DProvincia:"
         Height          =   225
         Left            =   675
         TabIndex        =   16
         Top             =   1110
         Width           =   1155
      End
      Begin VB.Label lblActividad 
         BackColor       =   &H0099CCFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1830
         TabIndex        =   2
         Top             =   360
         Width           =   4125
      End
   End
End
Attribute VB_Name = "frmAdmProvePortal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'          frmAdmprovePortal
'   muestra la pantalla en la que se pueden especificar las condiones para
'   realizar la busqueda de proveedores del portal

Option Explicit

'variables publicas
Public g_vACN1Seleccionada As Variant
Public g_vACN2Seleccionada As Variant
Public g_vACN3Seleccionada As Variant
Public g_vACN4Seleccionada As Variant
Public g_vACN5Seleccionada As Variant
Public g_oProveedores As CProveedores
Public g_sOrigen As String

Private m_bPaiRespetarCombo As Boolean
Private m_bPaiCargarComboDesde As Boolean
Private m_bProviRespetarCombo As Boolean
Private m_bProviCargarComboDesde As Boolean

Private m_oPaises As CPaises
Private m_oPaisSeleccionado As CPais

'Textos de la traducci�n
Private m_sTitulo As String
Private m_sCap(1 To 3) As String

Private Sub chkSoloAut_Click()
    
    If chkSoloAut.Value = vbUnchecked And chkSoloFS.Value = vbUnchecked Then Exit Sub
        
    chkSoloFS.Value = vbChecked
    
End Sub

Private Sub chkSoloFS_Click()
    
    If chkSoloFS.Value = vbUnchecked Then
        chkSoloAut.Value = vbUnchecked
    End If
    
    
End Sub

Private Sub cmdBuscar1_Click()
Dim oActsN1 As CActividadesNivel1
Dim scod1 As String
Dim oGrupoMatNivel4 As CGrupoMatNivel4

    Set oActsN1 = Nothing
    Set oActsN1 = oFSGSRaiz.generar_CActividadesNivel1
    
    If Mid(txtActividad, 2, 1) = "" Then
        oMensajes.NoValidaFaltanCaracteres
        Exit Sub
    End If
    
    frmEsperaPortal.Show
    DoEvents
    Screen.MousePointer = vbHourglass
    If g_sOrigen = "frmSELPROVEAnya" Then
        frmAdmPROVEPortalBuscar.g_sOrigen = g_sOrigen
        
        Set oGrupoMatNivel4 = oFSGSRaiz.Generar_CGrupoMatNivel4
        oGrupoMatNivel4.GMN1Cod = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN1Cod
        oGrupoMatNivel4.GMN1Den = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN1Den
        oGrupoMatNivel4.GMN2Cod = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN2Cod
        oGrupoMatNivel4.GMN2Den = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN2Den
        oGrupoMatNivel4.GMN3Cod = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN3Cod
        oGrupoMatNivel4.GMN3Den = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN3Den
        oGrupoMatNivel4.Cod = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).Cod
        oGrupoMatNivel4.Den = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).Den
             
        Set frmAdmPROVEPortalBuscar.g_oGrupoMatNivel4 = oGrupoMatNivel4
    Else
        frmAdmPROVEPortalBuscar.g_sOrigen = ""
        g_sOrigen = "frmAdmProvePortalresult"
    End If
    
    
    Set frmAdmPROVEPortalBuscar.g_ArItem = oActsN1.BusquedaActividades(True, basPublic.gParametrosInstalacion.gIdiomaPortal, txtActividad)
    
    Unload frmEsperaPortal
     
    frmAdmPROVEPortalBuscar.sdbcActividades.RemoveAll
    frmAdmPROVEPortalBuscar.lblCaracteres = txtActividad
    
    
    If Not frmAdmPROVEPortalBuscar.g_ArItem Is Nothing Then
        With frmAdmPROVEPortalBuscar
        If Not .g_ArItem.EOF Then
            While Not .g_ArItem.EOF
                scod1 = .g_ArItem("CODACT1").Value
                If Not IsNull(.g_ArItem("ACT2").Value) Then
                    scod1 = scod1 & "-" & .g_ArItem("CODACT2").Value
                End If
                If Not IsNull(.g_ArItem("ACT3").Value) Then
                    scod1 = scod1 & "-" & .g_ArItem("CODACT3").Value
                End If
                If Not IsNull(.g_ArItem("ACT4").Value) Then
                    scod1 = scod1 & "-" & .g_ArItem("CODACT4").Value
                End If
                If Not IsNull(.g_ArItem("ACT5").Value) Then
                    scod1 = scod1 & "-" & .g_ArItem("CODACT5").Value
                End If
        
                frmAdmPROVEPortalBuscar.sdbcActividades.AddItem .g_ArItem("DEN").Value & " (" & scod1 & ")" & Chr(m_lSeparador) & NullToStr(.g_ArItem("PROVE").Value) & Chr(m_lSeparador) & NullToStr(.g_ArItem("ACT1").Value) & Chr(m_lSeparador) & NullToStr(.g_ArItem("ACT2").Value) & Chr(m_lSeparador) & NullToStr(.g_ArItem("ACT3").Value) & Chr(m_lSeparador) & NullToStr(.g_ArItem("ACT4").Value) & Chr(m_lSeparador) & NullToStr(.g_ArItem("ACT5").Value) & Chr(m_lSeparador) & scod1
                .g_ArItem.MoveNext
            Wend
        End If
        End With
    End If
    Screen.MousePointer = vbNormal
    frmAdmPROVEPortalBuscar.Show

End Sub

Private Sub cmdBuscar2_Click()
Dim oGrupoMatNivel4 As CGrupoMatNivel4

    Screen.MousePointer = vbHourglass
    CargarProveedores
    frmAdmPROVEPortalResult.g_vACN1Seleccionada = g_vACN1Seleccionada
    frmAdmPROVEPortalResult.g_vACN2Seleccionada = g_vACN2Seleccionada
    frmAdmPROVEPortalResult.g_vACN3Seleccionada = g_vACN3Seleccionada
    frmAdmPROVEPortalResult.g_vACN4Seleccionada = g_vACN4Seleccionada
    frmAdmPROVEPortalResult.g_vACN5Seleccionada = g_vACN5Seleccionada
    frmAdmPROVEPortalResult.g_sNombre = txtNombre
    frmAdmPROVEPortalResult.g_sPais = sdbcPaiCod.Columns("ID").Value
    frmAdmPROVEPortalResult.g_sProvi = sdbcProviCod.Columns("ID").Value
    frmAdmPROVEPortalResult.g_bSoloFS = chkSoloFS.Value
    frmAdmPROVEPortalResult.g_bSoloAut = chkSoloAut.Value
    frmAdmPROVEPortalResult.g_bPremium = chkPremium.Value
    frmAdmPROVEPortalResult.g_bNoFS = chkNoFS.Value
    frmAdmPROVEPortalResult.cmdAceptar.Visible = False
    frmAdmPROVEPortalResult.cmdCancelar.Visible = False
    frmAdmPROVEPortalResult.g_sOrigenCarga = "Buscar"
    
    If g_sOrigen = "frmSELPROVEAnya" Then
        frmAdmPROVEPortalResult.g_sOrigen = g_sOrigen
        
        Set oGrupoMatNivel4 = oFSGSRaiz.Generar_CGrupoMatNivel4
        oGrupoMatNivel4.GMN1Cod = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN1Cod
        oGrupoMatNivel4.GMN1Den = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN1Den
        oGrupoMatNivel4.GMN2Cod = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN2Cod
        oGrupoMatNivel4.GMN2Den = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN2Den
        oGrupoMatNivel4.GMN3Cod = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN3Cod
        oGrupoMatNivel4.GMN3Den = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).GMN3Den
        oGrupoMatNivel4.Cod = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).Cod
        oGrupoMatNivel4.Den = frmSELPROVE.oProcesoSeleccionado.MaterialProce.Item(1).Den
             
        Set frmAdmPROVEPortalResult.g_oGrupoMatNivel4 = oGrupoMatNivel4
        
    Else
        frmAdmPROVEPortalResult.g_sOrigen = "frmAdmProvePortal"
    End If
    Screen.MousePointer = vbNormal
    If frmAdmPROVEPortalResult.sdbgProve.Rows < 1 Then
        frmAdmPROVEPortalResult.cmdModoEdicion.Enabled = False
    End If
    
    frmAdmPROVEPortalResult.SetFocus
    
    Set oGrupoMatNivel4 = Nothing
    
End Sub


Private Sub cmdBorrar_Click()

    lblActividad.caption = ""
    g_vACN1Seleccionada = Null
    g_vACN2Seleccionada = Null
    g_vACN3Seleccionada = Null
    g_vACN4Seleccionada = Null
    g_vACN5Seleccionada = Null

End Sub

Private Sub cmdSELACT_Click()
    frmSELMAT.sOrigen = "frmAdmProvePortalresult"
    frmSELMAT.Hide
    frmSELMAT.Show 1
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub Form_Load()
    Me.Width = 8595
        
    If Not gParametrosGenerales.gbPremium Or FSEPConf Then
        Me.Height = 5825
        chkPremium.Visible = False
        fraDirecta.Height = 3220
        cmdBuscar2.Top = 2610
    Else
        Me.Height = 6225
        chkPremium.Visible = True
        fraDirecta.Height = 3620
        cmdBuscar2.Top = 2970
    End If

    CargarRecursos
    
    Set m_oPaises = oFSGSRaiz.Generar_CPaises(True)
    Set g_oProveedores = oFSGSRaiz.generar_CProveedores

    m_sTitulo = Me.caption
    
    PonerFieldSeparator Me
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADM_PROVEPORTAL, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    

        m_sCap(3) = Ador(0).Value     '1 - [ comunicando con el portal ] -
        Ador.MoveNext
        chkSoloAut.caption = Ador(0).Value  '2
        Ador.MoveNext
        chkSoloFS.caption = Ador(0).Value  '3
        Ador.MoveNext
        Me.caption = Ador(0).Value  '4
        Ador.MoveNext
        lblActi.caption = Ador(0).Value  '5  'Actividad
        Ador.MoveNext
        lblPais.caption = Ador(0).Value  '6
        Ador.MoveNext
        lblProvincia.caption = Ador(0).Value  '7
        Ador.MoveNext
        lblNombre.caption = Ador(0).Value  '8
        Ador.MoveNext
        chkPremium.caption = Ador(0).Value '9
        Ador.MoveNext
        fraActividad.caption = Ador(0).Value '10  Busqueda por actividad
        Ador.MoveNext
        fraDirecta.caption = Ador(0).Value '11
        Ador.MoveNext
        cmdBuscar1.caption = Ador(0).Value '12
        cmdBuscar2.caption = Ador(0).Value
        Ador.MoveNext
        lblComentario.caption = Ador(0).Value '13  (Introduzca los primeros caracteres de la actividad a buscar)
        Ador.MoveNext
        sdbcPaiCod.Columns(0).caption = Ador(0).Value '14 Cod
        sdbcPaiDen.Columns(1).caption = Ador(0).Value
        sdbcProviCod.Columns(0).caption = Ador(0).Value
        sdbcProviDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcPaiCod.Columns(1).caption = Ador(0).Value '15 Denominaci�n
        sdbcPaiDen.Columns(0).caption = Ador(0).Value
        sdbcProviCod.Columns(1).caption = Ador(0).Value
        sdbcProviDen.Columns(0).caption = Ador(0).Value

        Ador.MoveNext
        chkNoFS.caption = Ador(0).Value  '16 Mostrar s�lo proveedores sin correspondencia en Fullstep GS
        
        Ador.MoveNext
        If gParametrosGenerales.gbSincronizacionMat = True Then
            lblActi.caption = Ador(0).Value  '17  Material
            Ador.MoveNext
            fraActividad.caption = Ador(0).Value '18  B�squeda por material
            Ador.MoveNext
            lblComentario.caption = Ador(0).Value '19  (Introduzca los primeros caracteres del material a buscar)
        Else
            Ador.MoveNext
        End If
        
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


Private Sub Form_Unload(Cancel As Integer)
    g_vACN1Seleccionada = Null
    g_vACN2Seleccionada = Null
    g_vACN3Seleccionada = Null
    g_vACN4Seleccionada = Null
    g_vACN5Seleccionada = Null
    
    Set g_oProveedores = Nothing
    Set m_oPaisSeleccionado = Nothing
    Set m_oPaises = Nothing
    
    If g_sOrigen = "frmSELPROVEAnya" Then
        frmSELPROVEAnya.bNoDescargar = True
    End If
    g_sOrigen = ""
    Me.Visible = False
End Sub

Private Sub sdbcPaiCod_Change()
    
    If Not m_bPaiRespetarCombo Then
    
        m_bPaiRespetarCombo = True
        sdbcPaiDen.Text = ""
        m_bPaiRespetarCombo = False
        
        m_bPaiCargarComboDesde = True
        Set m_oPaisSeleccionado = Nothing
        
    End If
    
    sdbcProviCod = ""
    sdbcProviCod.Columns(0).Value = ""
    sdbcProviCod.Columns(1).Value = ""
    sdbcProviCod.Columns(2).Value = ""
    sdbcProviDen = ""
    sdbcProviDen.Columns(0).Value = ""
    sdbcProviDen.Columns(1).Value = ""
    sdbcProviDen.Columns(2).Value = ""
    
    
End Sub

Private Sub sdbcPaiCod_CloseUp()
    
    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    m_bPaiRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiCod.Columns(1).Text
    sdbcPaiCod.Text = sdbcPaiCod.Columns(0).Text
    m_bPaiRespetarCombo = False
    
    Set m_oPaisSeleccionado = m_oPaises.Item(sdbcPaiCod.Columns(0).Text)
        
    m_bPaiCargarComboDesde = False
        
End Sub

Private Sub sdbcPaiCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcPaiCod.RemoveAll
       
    Me.caption = m_sTitulo & m_sCap(3)
    
    If m_bPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcPaiCod.Text), , , False
    Else
        m_oPaises.CargarTodosLosPaises , , , , , False
    End If
    Me.caption = m_sTitulo
    
    Codigos = m_oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Id(i)
    Next
    
    If m_bPaiCargarComboDesde And Not m_oPaises.EOF Then
        sdbcPaiCod.AddItem "..."
    End If
    
    sdbcPaiCod.SelStart = 0
    sdbcPaiCod.SelLength = Len(sdbcPaiCod.Text)
    sdbcPaiCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPaiCod_InitColumnProps()

    sdbcPaiCod.DataFieldList = "Column 0"
    sdbcPaiCod.DataFieldToDisplay = "Column 0"
    
End Sub



Private Sub sdbcPaiCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiCod.Rows - 1
            bm = sdbcPaiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPaiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcPaiCod_Validate(Cancel As Boolean)

 
    Dim bExiste As Boolean
    
    sdbcPaiCod.RemoveAll
    sdbcPaiDen.RemoveAll
    
    If sdbcPaiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
   
    Me.caption = m_sTitulo & m_sCap(3)
    
    Screen.MousePointer = vbHourglass
    m_oPaises.CargarTodosLosPaises sdbcPaiCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    Me.caption = m_sTitulo
    
    bExiste = Not (m_oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiCod.Text = ""
    Else
        sdbcPaiCod.AddItem m_oPaises.Item(1).Cod & Chr(m_lSeparador) & m_oPaises.Item(1).Den & Chr(m_lSeparador) & m_oPaises.Item(1).Id
        m_bPaiRespetarCombo = True
        sdbcPaiDen.Text = m_oPaises.Item(1).Den
        
        sdbcPaiCod.Columns(0).Value = sdbcPaiCod.Text
        sdbcPaiCod.Columns(1).Value = sdbcPaiDen.Text
        
        m_bPaiRespetarCombo = False
        Set m_oPaisSeleccionado = m_oPaises.Item(1)
        m_bPaiCargarComboDesde = False

    End If
       
End Sub
Private Sub sdbcPaiDen_Change()
    
    If Not m_bPaiRespetarCombo Then
    
        m_bPaiRespetarCombo = True
        sdbcPaiCod.Text = ""
        m_bPaiRespetarCombo = False
        m_bPaiCargarComboDesde = True
        Set m_oPaisSeleccionado = Nothing

    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
        
End Sub
Private Sub sdbcPaiDen_CloseUp()
    
    If sdbcPaiDen.Value = "..." Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    
    m_bPaiRespetarCombo = True
    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcPaiDen.Text = sdbcPaiDen.Columns(0).Text
    m_bPaiRespetarCombo = False
    
    Set m_oPaisSeleccionado = m_oPaises.Item(sdbcPaiDen.Columns(1).Text)
    
    m_bPaiCargarComboDesde = False
        
End Sub

Private Sub sdbcPaiDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcPaiDen.RemoveAll
    
    Me.caption = m_sTitulo & m_sCap(3)
    
    'DoEvents
    If m_bPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcPaiDen.Text), True, False
    Else
        m_oPaises.CargarTodosLosPaises , , , True, , False
    End If
    
    Me.caption = m_sTitulo
    
    Codigos = m_oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Id(i)
    Next
    
    If m_bPaiCargarComboDesde And Not m_oPaises.EOF Then
        sdbcPaiDen.AddItem "..."
    End If

    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
    sdbcPaiCod.Refresh

    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcPaiDen_InitColumnProps()

    sdbcPaiDen.DataFieldList = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcPaiDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcPaiCod.DroppedDown = False
        sdbcPaiCod.Text = ""
        sdbcPaiCod.RemoveAll
        sdbcPaiDen.DroppedDown = False
        sdbcPaiDen.Text = ""
        sdbcPaiDen.RemoveAll
        Set m_oPaisSeleccionado = Nothing
    End If
End Sub
Private Sub sdbcPaiDen_PositionList(ByVal Text As String)

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiDen.Rows - 1
            bm = sdbcPaiDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPaiDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcPaiDen_Validate(Cancel As Boolean)

   
    Dim bExiste As Boolean
    
    sdbcPaiCod.RemoveAll
    sdbcPaiDen.RemoveAll

    If sdbcPaiDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    
    Screen.MousePointer = vbHourglass
    Me.caption = m_sTitulo & m_sCap(3)
    m_oPaises.CargarTodosLosPaises , sdbcPaiDen.Text, True, , False
    Me.caption = m_sTitulo
    Screen.MousePointer = vbNormal
    
    bExiste = Not (m_oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiDen.Text = ""
    Else
        sdbcPaiCod.AddItem m_oPaises.Item(1).Cod & Chr(m_lSeparador) & m_oPaises.Item(1).Den & Chr(m_lSeparador) & m_oPaises.Item(1).Id
        sdbcPaiDen.AddItem m_oPaises.Item(1).Den & Chr(m_lSeparador) & m_oPaises.Item(1).Cod & Chr(m_lSeparador) & m_oPaises.Item(1).Id
        m_bPaiRespetarCombo = True
        sdbcPaiCod.Text = m_oPaises.Item(1).Cod
        sdbcPaiDen.Columns(0).Value = sdbcPaiDen.Text
        sdbcPaiDen.Columns(1).Value = sdbcPaiCod.Text
        m_bPaiRespetarCombo = False
        Set m_oPaisSeleccionado = m_oPaises.Item(1)
        m_bPaiCargarComboDesde = False
        
    End If
    
   
End Sub

Private Sub sdbcProviCod_Change()
    
    If Not m_bProviRespetarCombo Then
    
        m_bProviRespetarCombo = True
        sdbcProviDen.Text = ""
        sdbcProviCod.RemoveAll
        sdbcProviDen.RemoveAll
        sdbcProviCod.Columns(0).Value = ""
        sdbcProviCod.Columns(1).Value = ""
        sdbcProviCod.Columns(2).Value = ""
        sdbcProviDen.Columns(0).Value = ""
        sdbcProviDen.Columns(1).Value = ""
        sdbcProviDen.Columns(2).Value = ""
        
        m_bProviRespetarCombo = False
        
        m_bProviCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcProviCod_CloseUp()
    
    If sdbcProviCod.Value = "..." Then
        sdbcProviCod.Text = ""
        Exit Sub
    End If
    
    m_bProviRespetarCombo = True
    sdbcProviDen.Text = sdbcProviCod.Columns(1).Text
    sdbcProviCod.Text = sdbcProviCod.Columns(0).Text
    m_bProviRespetarCombo = False
    
    m_bProviCargarComboDesde = False
        
End Sub

Private Sub sdbcProviCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcProviCod.RemoveAll
        
    If Not m_oPaisSeleccionado Is Nothing Then
       
        Me.caption = m_sTitulo & m_sCap(3)
        
        If m_bProviCargarComboDesde Then
            m_oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProviCod.Text), , , False
        Else
            m_oPaisSeleccionado.CargarTodasLasProvincias , , , , False
        End If
        
        Me.caption = m_sTitulo
        
        Codigos = m_oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Id(i)
        Next
        
        If m_bProviCargarComboDesde And Not m_oPaisSeleccionado.Provincias.EOF Then
            sdbcProviCod.AddItem "..."
        End If

    End If
    
    sdbcProviCod.SelStart = 0
    sdbcProviCod.SelLength = Len(sdbcProviCod.Text)
    sdbcProviCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcProviCod_InitColumnProps()

    sdbcProviCod.DataFieldList = "Column 0"
    sdbcProviCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProviCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProviCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProviCod.Rows - 1
            bm = sdbcProviCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProviCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProviCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcProviCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProviCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe la provincia
    
    If Not m_oPaisSeleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        Me.caption = m_sTitulo & m_sCap(3)
        m_oPaisSeleccionado.CargarTodasLasProvincias sdbcProviCod.Text, , True, , False
        Me.caption = m_sTitulo
        bExiste = Not (m_oPaisSeleccionado.Provincias.Count = 0)
        Screen.MousePointer = vbNormal
    Else
    
        bExiste = False
        
    End If
    
        
    If Not bExiste Then
        sdbcProviCod.Text = ""
    Else
        m_bProviRespetarCombo = True
        sdbcProviDen.Text = m_oPaisSeleccionado.Provincias.Item(1).Den
        sdbcProviCod.Columns(0).Value = sdbcProviCod.Text
        sdbcProviCod.Columns(1).Value = sdbcProviDen.Text
        sdbcProviCod.Columns(2).Value = m_oPaisSeleccionado.Provincias.Item(1).Id
        
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = False
    End If
    
End Sub
Private Sub sdbcProviDen_Change()
    
    If Not m_bProviRespetarCombo Then
    
        m_bProviRespetarCombo = True
        sdbcProviCod.Text = ""
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = True
        sdbcProviCod.RemoveAll
        sdbcProviDen.RemoveAll
        sdbcProviCod.Columns(0).Value = ""
        sdbcProviCod.Columns(1).Value = ""
        sdbcProviCod.Columns(2).Value = ""
        sdbcProviDen.Columns(0).Value = ""
        sdbcProviDen.Columns(1).Value = ""
        sdbcProviDen.Columns(2).Value = ""
    End If
    
End Sub
Private Sub sdbcProviDen_CloseUp()
    
    If sdbcProviDen.Value = "..." Then
        sdbcProviDen.Text = ""
        Exit Sub
    End If
    
    m_bProviRespetarCombo = True
    sdbcProviCod.Text = sdbcProviDen.Columns(1).Text
    sdbcProviDen.Text = sdbcProviDen.Columns(0).Text
    m_bProviRespetarCombo = False
    
    m_bProviCargarComboDesde = False
        
End Sub
Private Sub sdbcProviDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    
    sdbcProviDen.RemoveAll
    
    If Not m_oPaisSeleccionado Is Nothing Then
        
        Me.caption = m_sTitulo & m_sCap(3)
        If m_bProviCargarComboDesde Then
            m_oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProviDen.Text), True, False
        Else
            m_oPaisSeleccionado.CargarTodasLasProvincias , , , True, False
        End If
        Me.caption = m_sTitulo
        
        Codigos = m_oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Id(i)
        Next
        
        If m_bProviCargarComboDesde And Not m_oPaisSeleccionado.Provincias.EOF Then
            sdbcProviDen.AddItem "..."
        End If

    End If
    
    sdbcProviDen.SelStart = 0
    sdbcProviDen.SelLength = Len(sdbcProviDen.Text)
    sdbcProviCod.Refresh

    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcProviDen_InitColumnProps()

    sdbcProviDen.DataFieldList = "Column 0"
    sdbcProviDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcProviDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcProviCod.DroppedDown = False
        sdbcProviCod.Text = ""
        sdbcProviCod.RemoveAll
        sdbcProviDen.DroppedDown = False
        sdbcProviDen.Text = ""
        sdbcProviDen.RemoveAll
    End If
End Sub


Private Sub sdbcProviDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProviDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProviDen.Rows - 1
            bm = sdbcProviDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProviDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProviDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcProviDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProviDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe la provincia
    
    If Not m_oPaisSeleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        Me.caption = m_sTitulo & m_sCap(3)
         
        m_oPaisSeleccionado.CargarTodasLasProvincias , sdbcProviDen.Text, True, , False
        
        Me.caption = m_sTitulo
        
        bExiste = Not (m_oPaisSeleccionado.Provincias.Count = 0)
        Screen.MousePointer = vbNormal
    Else
    
        bExiste = False
        
    End If
    
        
    If Not bExiste Then
        sdbcProviDen.Text = ""
    Else
        m_bProviRespetarCombo = True
        sdbcProviCod.Text = m_oPaisSeleccionado.Provincias.Item(1).Cod
        
        sdbcProviDen.Columns(0).Value = sdbcProviDen.Text
        sdbcProviDen.Columns(1).Value = sdbcProviCod.Text
        sdbcProviCod.Columns(2).Value = m_oPaisSeleccionado.Provincias.Item(1).Id
        
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = False
    End If

End Sub

Private Sub CargarProveedores()
Dim oProve As CProveedor
Dim aut As Integer

    frmEsperaPortal.Show
    DoEvents
    sdbcPaiCod_Validate False
    g_oProveedores.CargarTodosLosProveedoresDelPortal g_vACN1Seleccionada, g_vACN2Seleccionada, g_vACN3Seleccionada, g_vACN4Seleccionada, g_vACN5Seleccionada, txtNombre, sdbcPaiCod.Columns("ID").Value, sdbcProviCod.Columns("ID").Value, (chkSoloFS = vbChecked), (chkSoloAut = vbChecked), , (chkPremium = vbChecked), , (chkNoFS = vbChecked)
    
    Unload frmEsperaPortal

    For Each oProve In g_oProveedores
        With oProve

            If .EstadoEnPortal = fsgsserver.TESPCActivo Then
                aut = 1
            Else
                aut = 0
            End If
            frmAdmPROVEPortalResult.sdbgProve.AddItem .CodPortal & Chr(m_lSeparador) & .Den & Chr(m_lSeparador) & Chr(m_lSeparador) & .CodPais & " " & .DenProvi & Chr(m_lSeparador) & Chr(m_lSeparador) & .Cod & Chr(m_lSeparador) & aut & Chr(m_lSeparador) & .IDPortal & Chr(m_lSeparador) & .EsPremium & Chr(m_lSeparador) & .Cod & Chr(m_lSeparador) & Chr(m_lSeparador) & .NIF

        End With
    Next

End Sub
