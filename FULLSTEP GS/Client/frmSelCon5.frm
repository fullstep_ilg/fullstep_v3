VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSelCon5 
   BackColor       =   &H00808000&
   Caption         =   "Form1"
   ClientHeight    =   5505
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7410
   Icon            =   "frmSelCon5.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5505
   ScaleWidth      =   7410
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   3435
      TabIndex        =   1
      Top             =   5115
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2175
      TabIndex        =   0
      Top             =   5115
      Width           =   1005
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   960
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCon5.frx":0CB2
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCon5.frx":177C
            Key             =   "PRES1"
            Object.Tag             =   "PRES1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCon5.frx":1BCE
            Key             =   "PRES2"
            Object.Tag             =   "PRES2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCon5.frx":2020
            Key             =   "PRES3"
            Object.Tag             =   "PRES3"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCon5.frx":2472
            Key             =   "PRES4"
            Object.Tag             =   "PRES4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCon5.frx":28C4
            Key             =   "PRESBAJALOGICA"
            Object.Tag             =   "PRESBAJALOGICA"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwEstrPres 
      Height          =   4665
      Left            =   120
      TabIndex        =   2
      Top             =   240
      Width           =   6870
      _ExtentX        =   12118
      _ExtentY        =   8229
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSelCon5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private iNivelMaximo As Integer

Public m_oPresupuestos As CPresConceptos5Nivel1
' Variables para interactuar con otros forms
Public m_sPres0 As String
Public opres1 As CPresConcep5Nivel1
Public oPRES2 As CPresConcep5Nivel2
Public oPRES3 As CPresConcep5Nivel3
Public oPRES4 As CPresConcep5Nivel4
Public m_bVerBajaLog As Boolean

' Variable de control de flujo
'Public Accion As accionessummit

Private oPres0 As cPresConcep5Nivel0

Private Sub Form_Activate()
    'tvwEstrPres_NodeClick tvwEstrPres.SelectedItem
    If Me.Visible Then tvwEstrPres.SetFocus
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset


' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELCON5, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.caption = Ador(0).Value 'Seleccion:
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value 'Seleccionar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value 'Cerrar

        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
   ' Me.caption = sIdiSel & " " & gParametrosGenerales.gsSingPres4
        
    tvwEstrPres.Top = 315
    tvwEstrPres.Height = 4425
    
    DoEvents
    
    PartidaSeleccionada m_sPres0
    If Not oPres0 Is Nothing Then
        GenerarEstructuraPresupuestos
        Me.caption = Me.caption & " " & oPres0.Den
    End If
End Sub

Private Sub PartidaSeleccionada(ByVal sPres0 As String)
Dim oIBaseDatos As IBaseDatos
    
    Set oPres0 = oFSGSRaiz.Generar_CPresConcep5Nivel0
    oPres0.Cod = sPres0
    Set oIBaseDatos = oPres0
    oIBaseDatos.IniciarEdicion
    Set oIBaseDatos = Nothing

End Sub

Private Sub GenerarEstructuraPresupuestos()
    Set m_oPresupuestos = oFSGSRaiz.Generar_CPresConceptos5Nivel1
    m_oPresupuestos.GenerarEstructuraPresupuestos , oPres0.Cod
    
    'Generamos el arbol de presupuestos que se ve en el formulario
    GenerarArbolPresupuestos
End Sub


Private Sub GenerarArbolPresupuestos()
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim opres1 As CPresConcep5Nivel1
Dim oPRES2 As CPresConcep5Nivel2
Dim oPRES3 As CPresConcep5Nivel3
Dim oPRES4 As CPresConcep5Nivel4

Dim nodx As MSComctlLib.node

iNivelMaximo = 4


    tvwEstrPres.Nodes.clear

    Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", oPres0.Den, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True

    For Each opres1 In m_oPresupuestos
        If Not ((Not m_bVerBajaLog) And opres1.BajaLog) Then
            scod1 = opres1.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(opres1.Cod))
            Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, opres1.Cod & " - " & opres1.Den, "PRES1")
            nodx.Tag = "PRES1" & opres1.Cod
            If opres1.BajaLog Then
                nodx.Image = "PRESBAJALOGICA"
                nodx.Backcolor = &H8000000F 'color gris
            End If
        End If

        For Each oPRES2 In opres1.PresConceptos5Nivel2
            If Not ((Not m_bVerBajaLog) And oPRES2.BajaLog) Then
                scod2 = oPRES2.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(oPRES2.Cod))
                Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                nodx.Tag = "PRES2" & oPRES2.Cod
                If oPRES2.BajaLog Then
                    nodx.Image = "PRESBAJALOGICA"
                    nodx.Backcolor = &H8000000F 'color gris
                End If
            End If

                For Each oPRES3 In oPRES2.PresConceptos5Nivel3
                    If Not ((Not m_bVerBajaLog) And oPRES3.BajaLog) Then
                        scod3 = oPRES3.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(oPRES3.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                        nodx.Tag = "PRES3" & oPRES3.Cod
                        If oPRES3.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.Backcolor = &H8000000F 'color gris
                        End If
                    End If

                    For Each oPRES4 In oPRES3.PresConceptos5Nivel4
                        If Not ((Not m_bVerBajaLog) And oPRES4.BajaLog) Then
                            scod4 = oPRES4.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(oPRES4.Cod))
                            Set nodx = tvwEstrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                            nodx.Tag = "PRES4" & oPRES4.Cod
                            If oPRES4.BajaLog Then
                                nodx.Image = "PRESBAJALOGICA"
                                nodx.Backcolor = &H8000000F 'color gris
                            End If
                        End If
                    Next
                    Set oPRES4 = Nothing
                Next
                Set oPRES3 = Nothing
        Next
        Set oPRES2 = Nothing
    Next


    Set opres1 = Nothing
        
    Exit Sub
    
ERROR:
    Set nodx = Nothing
    Resume Next

End Sub

    
Private Sub Form_Resize()
On Error Resume Next
    If Me.Width > 250 Then tvwEstrPres.Width = Me.Width - 250
    If Me.Height >= 1600 Then tvwEstrPres.Height = Me.Height - 1600
    '''picSepar.Top = tvwEstrPres.Top + tvwEstrPres.Height
    tvwEstrPres.Left = 75
    '''picSepar.Left = tvwEstrPres.Left
    '''picSepar.Width = tvwEstrPres.Width

    cmdAceptar.Top = tvwEstrPres.Top + tvwEstrPres.Height + 50
    cmdCancelar.Top = cmdAceptar.Top

    cmdAceptar.Left = tvwEstrPres.Left + tvwEstrPres.Width / 2 - 1100
    cmdCancelar.Left = tvwEstrPres.Left + tvwEstrPres.Width / 2
End Sub


Private Sub Form_Unload(Cancel As Integer)
        
    Set m_oPresupuestos = Nothing
    Set opres1 = Nothing
    Set oPRES2 = Nothing
    Set oPRES3 = Nothing
    Set oPRES4 = Nothing
    
End Sub

Private Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

If node Is Nothing Then Exit Function

DevolverCod = Right(node.Tag, Len(node.Tag) - 5)
 
End Function

Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

    Set nodx = tvwEstrPres.selectedItem
    
    If Not nodx Is Nothing Then
        
        Screen.MousePointer = vbHourglass
        Select Case Left(nodx.Tag, 5)
        
            Case "PRES1"
                
                scod1 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx)))
                Set opres1 = m_oPresupuestos.Item(scod1)
                
            Case "PRES2"
                
                scod1 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent)))
                scod2 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx)))
                Set oPRES2 = m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2)
                                
            Case "PRES3"
                
                scod1 = DevolverCod(nodx.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx.Parent)))
                scod3 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(DevolverCod(nodx)))
                Set oPRES3 = m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3)
                
            Case "PRES4"
                
                scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx.Parent.Parent)))
                scod3 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(DevolverCod(nodx.Parent)))
                scod4 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(DevolverCod(nodx)))
                Set oPRES4 = m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).PresConceptos5Nivel4.Item(scod1 & scod2 & scod3 & scod4)
                
        End Select
        
        Screen.MousePointer = vbNormal
        
        frmLstPRESPorCon5.MostrarParConSeleccionada
        

    End If
    
    Unload Me

End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub




