VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPROVEPortalDetalle 
   Caption         =   " DDetalle del proveedor del portal"
   ClientHeight    =   4950
   ClientLeft      =   660
   ClientTop       =   3795
   ClientWidth     =   9600
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROVEPortalDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4950
   ScaleWidth      =   9600
   Begin TabDlg.SSTab stabPROVE 
      Height          =   4920
      Left            =   30
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   30
      Width           =   9435
      _ExtentX        =   16642
      _ExtentY        =   8678
      _Version        =   393216
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   520
      ForeColor       =   -2147483640
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Datos generales"
      TabPicture(0)   =   "frmPROVEPortalDetalle.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblVol"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblCod"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblDen"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblPro"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblMon"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblNif"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblPai"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblDir"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblPob"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblCP"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lblIdi"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "lblUrl"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Timer1"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "cmdlistado"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "txtVol"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "txtCod"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "txtProv"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "txtMon"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "txtIdi"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "txtPai"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "frmHom"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "frmRef"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "txtDen"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "txtPob"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "txtCP"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "txtNif"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "txtDir"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "txtURL"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).ControlCount=   28
      TabCaption(1)   =   "Actividades"
      TabPicture(1)   =   "frmPROVEPortalDetalle.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "tvwEstrMat"
      Tab(1).Control(1)=   "ImageList2"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Observaciones"
      TabPicture(2)   =   "frmPROVEPortalDetalle.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "txtObs"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Contactos"
      TabPicture(3)   =   "frmPROVEPortalDetalle.frx":0D06
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "sdbgContactos"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Archivos adjuntos"
      TabPicture(4)   =   "frmPROVEPortalDetalle.frx":0D22
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "picOpenSave"
      Tab(4).Control(1)=   "lstvwEspProvePortal"
      Tab(4).Control(2)=   "dlgEsp"
      Tab(4).Control(3)=   "lblEspProvePortal"
      Tab(4).ControlCount=   4
      Begin VB.TextBox txtObs 
         BackColor       =   &H00FFFFFF&
         Height          =   4110
         Left            =   -74775
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   42
         Top             =   540
         Width           =   8910
      End
      Begin VB.TextBox txtURL 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1305
         Locked          =   -1  'True
         TabIndex        =   31
         Top             =   4305
         Width           =   7605
      End
      Begin VB.TextBox txtDir 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1305
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   1770
         Width           =   2895
      End
      Begin VB.TextBox txtNif 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1305
         Locked          =   -1  'True
         TabIndex        =   29
         Top             =   1410
         Width           =   2895
      End
      Begin VB.TextBox txtCP 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1305
         Locked          =   -1  'True
         TabIndex        =   28
         Top             =   2145
         Width           =   2895
      End
      Begin VB.TextBox txtPob 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1305
         Locked          =   -1  'True
         TabIndex        =   27
         Top             =   2505
         Width           =   2895
      End
      Begin VB.TextBox txtDen 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1305
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   1050
         Width           =   2895
      End
      Begin VB.Frame frmRef 
         Caption         =   "Clientes de referencia"
         Height          =   1755
         Left            =   4500
         TabIndex        =   21
         Top             =   1020
         Width           =   4695
         Begin VB.TextBox txtCliRef4 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   225
            Locked          =   -1  'True
            TabIndex        =   25
            Top             =   1380
            Width           =   4170
         End
         Begin VB.TextBox txtCliRef3 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   225
            Locked          =   -1  'True
            TabIndex        =   24
            Top             =   1015
            Width           =   4170
         End
         Begin VB.TextBox txtCliRef2 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   225
            Locked          =   -1  'True
            TabIndex        =   23
            Top             =   650
            Width           =   4170
         End
         Begin VB.TextBox txtCliRef1 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   225
            Locked          =   -1  'True
            TabIndex        =   22
            Top             =   285
            Width           =   4170
         End
      End
      Begin VB.Frame frmHom 
         Caption         =   "Homologaciones"
         Height          =   1455
         Left            =   4500
         TabIndex        =   17
         Top             =   2790
         Width           =   4695
         Begin VB.TextBox txtHom3 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   20
            Top             =   1005
            Width           =   4170
         End
         Begin VB.TextBox txtHom2 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   19
            Top             =   645
            Width           =   4170
         End
         Begin VB.TextBox txtHom1 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   18
            Top             =   285
            Width           =   4170
         End
      End
      Begin VB.TextBox txtPai 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1305
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   2850
         Width           =   2895
      End
      Begin VB.TextBox txtIdi 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1305
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   3900
         Width           =   2895
      End
      Begin VB.TextBox txtMon 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1305
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   3555
         Width           =   2895
      End
      Begin VB.TextBox txtProv 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1305
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   3195
         Width           =   2895
      End
      Begin VB.TextBox txtCod 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1305
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   690
         Width           =   2895
      End
      Begin VB.TextBox txtVol 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   6210
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   690
         Width           =   1335
      End
      Begin VB.CommandButton cmdlistado 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8820
         Picture         =   "frmPROVEPortalDetalle.frx":0D3E
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   660
         Width           =   315
      End
      Begin VB.PictureBox picOpenSave 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -67170
         ScaleHeight     =   375
         ScaleWidth      =   1005
         TabIndex        =   5
         Top             =   4275
         Width           =   1005
         Begin VB.CommandButton cmdSalvarAA 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   0
            Picture         =   "frmPROVEPortalDetalle.frx":1080
            Style           =   1  'Graphical
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   60
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAbrirAA 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   480
            Picture         =   "frmPROVEPortalDetalle.frx":13C2
            Style           =   1  'Graphical
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   60
            UseMaskColor    =   -1  'True
            Width           =   420
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   0
         Top             =   0
      End
      Begin MSComctlLib.ListView lstvwEspProvePortal 
         Height          =   3585
         Left            =   -74700
         TabIndex        =   1
         Top             =   660
         Width           =   8760
         _ExtentX        =   15452
         _ExtentY        =   6324
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList2"
         SmallIcons      =   "ImageList2"
         ForeColor       =   -2147483640
         BackColor       =   10079487
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Comentario"
            Object.Width           =   6588
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha"
            Object.Width           =   2867
         EndProperty
      End
      Begin MSComDlg.CommonDialog dlgEsp 
         Left            =   -68205
         Top             =   360
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         CancelError     =   -1  'True
         DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgContactos 
         Height          =   4005
         Left            =   -74760
         TabIndex        =   2
         Top             =   600
         Width           =   8940
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   14
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmPROVEPortalDetalle.frx":1704
         stylesets(1).Name=   "Tan"
         stylesets(1).BackColor=   10079487
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmPROVEPortalDetalle.frx":1720
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         BalloonHelp     =   0   'False
         MaxSelectedRows =   1
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns.Count   =   14
         Columns(0).Width=   3200
         Columns(0).Caption=   "Apellidos"
         Columns(0).Name =   "APE"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   100
         Columns(1).Width=   4022
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   20
         Columns(2).Width=   2328
         Columns(2).Caption=   "Departamento"
         Columns(2).Name =   "DEP"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   20
         Columns(3).Width=   3200
         Columns(3).Caption=   "Cargo"
         Columns(3).Name =   "CAR"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   100
         Columns(4).Width=   2223
         Columns(4).Caption=   "Tel�fono"
         Columns(4).Name =   "TFNO"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Caption=   "Tel�fono2"
         Columns(5).Name =   "TFNO2"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   20
         Columns(6).Width=   3201
         Columns(6).Caption=   "Tel�fono m�vil"
         Columns(6).Name =   "TFNO_MOVIL"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Caption=   "Fax"
         Columns(7).Name =   "FAX"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   100
         Columns(8).Width=   3200
         Columns(8).Caption=   "Mail"
         Columns(8).Name =   "MAIL"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   200
         Columns(9).Width=   3200
         Columns(9).Caption=   "NIF"
         Columns(9).Name =   "NIF"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   2540
         Columns(10).Caption=   "Recibe peticiones"
         Columns(10).Name=   "DEF"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(10).Style=   2
         Columns(11).Width=   3200
         Columns(11).Visible=   0   'False
         Columns(11).Caption=   "PORTAL"
         Columns(11).Name=   "PORTAL"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   11
         Columns(11).FieldLen=   256
         Columns(12).Width=   3200
         Columns(12).Caption=   "Aprovisionamiento"
         Columns(12).Name=   "APROV"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   11
         Columns(12).FieldLen=   256
         Columns(12).Style=   2
         Columns(13).Width=   3200
         Columns(13).Caption=   "Subasta"
         Columns(13).Name=   "SUB"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(13).Style=   2
         _ExtentX        =   15769
         _ExtentY        =   7064
         _StockProps     =   79
         BackColor       =   13226710
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.TreeView tvwEstrMat 
         Height          =   4095
         Left            =   -74775
         TabIndex        =   3
         Top             =   540
         Width           =   8895
         _ExtentX        =   15690
         _ExtentY        =   7223
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList2"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ImageList ImageList2 
         Left            =   -74850
         Top             =   360
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   13
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":173C
               Key             =   "Raiz"
               Object.Tag             =   "Raiz"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":1B90
               Key             =   "ACT"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":1CA2
               Key             =   "ACTASIG"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":1DB4
               Key             =   "Raiz2"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":2206
               Key             =   "GMN4"
               Object.Tag             =   "GMN4"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":255A
               Key             =   "GMN4A"
               Object.Tag             =   "GMN4A"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":28AE
               Key             =   "GMN1"
               Object.Tag             =   "GMN1"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":2C02
               Key             =   "GMN1A"
               Object.Tag             =   "GMN1A"
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":2F56
               Key             =   "GMN2"
               Object.Tag             =   "GMN2"
            EndProperty
            BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":32AA
               Key             =   "GMN2A"
               Object.Tag             =   "GMN2A"
            EndProperty
            BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":35FE
               Key             =   "GMN3A"
               Object.Tag             =   "GMN3A"
            EndProperty
            BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":3952
               Key             =   "GMN3"
               Object.Tag             =   "GMN3"
            EndProperty
            BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPROVEPortalDetalle.frx":3CA6
               Key             =   "ESP"
               Object.Tag             =   "ESP"
            EndProperty
         EndProperty
      End
      Begin VB.Label lblUrl 
         AutoSize        =   -1  'True
         Caption         =   "URL:"
         Height          =   195
         Left            =   150
         TabIndex        =   41
         Top             =   4305
         Width           =   1000
      End
      Begin VB.Label lblIdi 
         Caption         =   "Idioma:"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   150
         TabIndex        =   40
         Top             =   3930
         Width           =   1000
      End
      Begin VB.Label lblCP 
         AutoSize        =   -1  'True
         Caption         =   "CP:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   150
         TabIndex        =   39
         Top             =   2160
         Width           =   1000
      End
      Begin VB.Label lblPob 
         AutoSize        =   -1  'True
         Caption         =   "Poblaci�n:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   150
         TabIndex        =   38
         Top             =   2520
         Width           =   1000
      End
      Begin VB.Label lblDir 
         AutoSize        =   -1  'True
         Caption         =   "Direcci�n:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   150
         TabIndex        =   37
         Top             =   1800
         Width           =   1000
      End
      Begin VB.Label lblPai 
         AutoSize        =   -1  'True
         Caption         =   "Pa�s:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   150
         TabIndex        =   36
         Top             =   2865
         Width           =   1000
      End
      Begin VB.Label lblNif 
         AutoSize        =   -1  'True
         Caption         =   "NIF:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   150
         TabIndex        =   35
         Top             =   1425
         Width           =   1000
      End
      Begin VB.Label lblMon 
         AutoSize        =   -1  'True
         Caption         =   "Moneda:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   150
         TabIndex        =   34
         Top             =   3585
         Width           =   1000
      End
      Begin VB.Label lblPro 
         AutoSize        =   -1  'True
         Caption         =   "Provincia:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   150
         TabIndex        =   33
         Top             =   3225
         Width           =   1000
      End
      Begin VB.Label lblDen 
         AutoSize        =   -1  'True
         Caption         =   "Denominaci�n:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   150
         TabIndex        =   32
         Top             =   1065
         Width           =   1050
      End
      Begin VB.Label lblCod 
         AutoSize        =   -1  'True
         Caption         =   "C�digo:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   150
         TabIndex        =   12
         Top             =   720
         Width           =   1000
      End
      Begin VB.Label lblVol 
         AutoSize        =   -1  'True
         Caption         =   "Volumen facturaci�n:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   4560
         TabIndex        =   11
         Top             =   720
         Width           =   1515
      End
      Begin VB.Label lblEspProvePortal 
         Caption         =   "DArchivos disponibles en el portal:"
         Height          =   255
         Left            =   -74820
         TabIndex        =   4
         Top             =   420
         Width           =   4005
      End
   End
End
Attribute VB_Name = "frmPROVEPortalDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmPROVEPortalDetalle
''' *** Creacion: 15/01/2002

Option Explicit

''' publicas
Public g_oProveSeleccionado As CProveedor
Public g_sProveCod As String
Public g_sProveDen As String
Public g_sComentario As String
Public g_bCancelarEsp As Boolean

' Variables de idioma
Private m_sIdioma As String

' variables para las selecciones en datos
Private m_oContacto As CContacto
Private m_oIBAseDatosEnEdicion As IBaseDatos

'variable para la coleccion de proveedores
Private m_oProves As CProveedores

'Guardaremos los nombres de los temporales aqui, para borrarlos al salir
Private m_sArFileNames() As String

'Variables para el listado
Private m_stxtTitulo As String
Private m_stxtPag As String
Private m_stxtDe As String
Private m_stxtProve As String
Private m_stxtTipo As String
Private m_ArItem As Ador.Recordset
Private m_sIdiTipoOrig As String
Private m_sIdiGuardar As String
Private m_sIdiArchivo As String
Private m_skb As String

Private Sub MostrarDatosProveedor()

With g_oProveSeleccionado
    
        txtCod = g_sProveCod
        txtDen = .Den
        txtDir = NullToStr(.Direccion)
        txtCP = NullToStr(.cP)
        txtPob = NullToStr(.Poblacion)
        txtPai = NullToStr(.DenPais)
        txtMon = NullToStr(.DenMon)
        txtProv = NullToStr(.CodProvi) & "" & NullToStr(.DenProvi)
        txtNif = NullToStr(.NIF)
        txtURL = NullToStr(.URLPROVE)
        txtIdi = NullToStr(.IdiPortal)
        txtVol = NullToStr(.VolFact)
        txtCliRef1 = NullToStr(.Cliref1)
        txtCliRef2 = NullToStr(.Cliref2)
        txtCliRef3 = NullToStr(.Cliref3)
        txtCliRef4 = NullToStr(.Cliref4)
        txtHom1 = NullToStr(.Hom1)
        txtHom2 = NullToStr(.Hom2)
        txtHom3 = NullToStr(.Hom3)
     
End With
End Sub


Public Sub ProveedorSeleccionado()
Dim teserror As TipoErrorSummit
Dim oActsN1 As CActividadesNivel1
Dim oActsN2 As CActividadesNivel2
Dim oActsN3 As CActividadesNivel3
Dim oActsN4 As CActividadesNivel4
Dim oActsN5 As CActividadesNivel5

Dim oACN1 As CActividadNivel1
Dim oACN2 As CActividadNivel2
Dim oACN3 As CActividadNivel3
Dim oACN4 As CActividadNivel4
Dim oACN5 As CActividadNivel5

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

Dim nodx As MSComctlLib.node

    Screen.MousePointer = vbHourglass
    
    Set m_oProves = oFSGSRaiz.generar_CProveedores
    m_oProves.CargarTodosLosProveedoresDelPortal Null, Null, Null, Null, Null, g_sProveDen, "", "", False, False, 1, False
    
    Set g_oProveSeleccionado = Nothing
    Set g_oProveSeleccionado = m_oProves.Item(g_sProveCod)
    
    If g_oProveSeleccionado.Cod <> "" And Not IsNull(g_oProveSeleccionado.Cod) Then

        frmEsperaPortal.Show
        DoEvents
        
        teserror = g_oProveSeleccionado.CopiarDatosProveedorPortal
        
        Unload frmEsperaPortal
        Screen.MousePointer = vbNormal
    
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
    End If
    
    If Not g_oProveSeleccionado Is Nothing Then
                    
        With g_oProveSeleccionado
        
            stabPROVE.Enabled = True
            MostrarDatosProveedor
              
            'Cargamos Actividades del Proveedor
            
            
            tvwEstrMat.Nodes.clear
            
            Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", m_sIdioma, "Raiz2")
            
            nodx.Expanded = True
            
            On Error GoTo Error
            
            
            
            '***********Cargamos Las Actividades de Nivel1******************
            
            Set oActsN1 = Nothing
            Set oActsN1 = oFSGSRaiz.generar_CActividadesNivel1
            oActsN1.CargarActividadesN1Proveedor .IDPortal, gParametrosInstalacion.gIdiomaPortal
            
            If Not oActsN1 Is Nothing Then
            
                For Each oACN1 In oActsN1
                
                    scod1 = CStr(oACN1.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN1.Id)))
                    
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN1.Cod & " - " & oACN1.Den, "ACTASIG")
                           
                Next
                 
            End If
            
            Set oActsN1 = Nothing
            Set oACN1 = Nothing
            
            
            '***********Cargamos Las Actividades de Nivel2******************
            
            Set oActsN2 = Nothing
            Set oActsN2 = oFSGSRaiz.Generar_CActividadesNivel2
            oActsN2.CargarActividadesN2Proveedor .IDPortal, gParametrosInstalacion.gIdiomaPortal
            
            If Not oActsN2 Is Nothing Then
            
                For Each oACN2 In oActsN2
                
                    scod1 = CStr(oACN2.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN2.ACN1)))
                    scod2 = CStr(oACN2.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN2.Id)))
                    
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN2.CodACT1 & " - " & oACN2.DenACT1, "ACT")
                    
                    Set nodx = tvwEstrMat.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN2.Cod & " - " & oACN2.Den, "ACTASIG")
                   
                    If nodx.Parent.Image = "ACTASIG" Or nodx.Image = "ACTASIG" Then
                        nodx.Parent.Expanded = True
                    End If
                    
                            
                Next
                
            End If
            
            Set oActsN2 = Nothing
            Set oACN2 = Nothing
            
            
            '***********Cargamos Las Actividades de Nivel3******************
            
            Set oActsN3 = Nothing
            Set oActsN3 = oFSGSRaiz.Generar_CActividadesNivel3
            oActsN3.CargarActividadesN3Proveedor .IDPortal, gParametrosInstalacion.gIdiomaPortal
                    
            
            If Not oActsN3 Is Nothing Then
            
                For Each oACN3 In oActsN3
                
                    scod1 = CStr(oACN3.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.ACN1)))
                    scod2 = CStr(oACN3.acn2) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.acn2)))
                    scod3 = CStr(oACN3.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.Id)))
                    
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN3.CodACT1 & " - " & oACN3.DenACT1, "ACT")
                    
                    Set nodx = tvwEstrMat.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN3.CodACT2 & " - " & oACN3.DenACT2, "ACT")
                    
                    Set nodx = tvwEstrMat.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN3.Cod & " - " & oACN3.Den, "ACTASIG")
            
                    If nodx.Parent.Image = "ACTASIG" Or nodx.Image = "ACTASIG" Then
                        nodx.Parent.Parent.Expanded = True
                        nodx.Parent.Expanded = True
                    End If
                    
              
                Next
            
            End If
            
            Set oActsN3 = Nothing
            Set oACN3 = Nothing
            
            
            '***********Cargamos Las Actividades de Nivel4******************
            
            Set oActsN4 = Nothing
            Set oActsN4 = oFSGSRaiz.Generar_CActividadesNivel4
            oActsN4.CargarActividadesN4Proveedor .IDPortal, gParametrosInstalacion.gIdiomaPortal
            
            If Not oActsN4 Is Nothing Then
            
                For Each oACN4 In oActsN4
                
                    scod1 = CStr(oACN4.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.ACN1)))
                    scod2 = CStr(oACN4.acn2) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.acn2)))
                    scod3 = CStr(oACN4.acn3) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.acn3)))
                    scod4 = CStr(oACN4.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.Id)))
                    
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN4.CodACT1 & " - " & oACN4.DenACT1, "ACT")
                    
                    Set nodx = tvwEstrMat.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN4.CodACT2 & " - " & oACN4.DenACT2, "ACT")
                    
                    Set nodx = tvwEstrMat.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN4.CodACT3 & " - " & oACN4.DenACT3, "ACT")
                    
                    Set nodx = tvwEstrMat.Nodes.Add("ACN3" & scod1 & scod2 & scod3, tvwChild, "ACN4" & scod1 & scod2 & scod3 & scod4, oACN4.Cod & " - " & oACN4.Den, "ACTASIG")
            
                    If nodx.Parent.Image = "ACTASIG" Or nodx.Image = "ACTASIG" Then
                        nodx.Parent.Parent.Parent.Expanded = True
                        nodx.Parent.Parent.Expanded = True
                        nodx.Parent.Expanded = True
                    End If
                    
                    
                Next
            
            End If
            
            Set oActsN4 = Nothing
            Set oACN4 = Nothing
                    
            
            '***********Cargamos Las Actividades de Nivel5******************
            
            Set oActsN5 = Nothing
            Set oActsN5 = oFSGSRaiz.Generar_CActividadesNivel5
            oActsN5.CargarActividadesN5Proveedor .IDPortal, gParametrosInstalacion.gIdiomaPortal
            
            
            If Not oActsN5 Is Nothing Then
            
                For Each oACN5 In oActsN5
                
                    scod1 = CStr(oACN5.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ACN1)))
                    scod2 = CStr(oACN5.acn2) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.acn2)))
                    scod3 = CStr(oACN5.acn3) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.acn3)))
                    scod4 = CStr(oACN5.acn4) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.acn4)))
                    scod5 = CStr(oACN5.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.Id)))
                    
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN5.CodACT1 & " - " & oACN5.DenACT1, "ACT")
                    
                    Set nodx = tvwEstrMat.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN5.CodACT2 & " - " & oACN5.DenACT2, "ACT")
                    
                    Set nodx = tvwEstrMat.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN5.CodACT3 & " - " & oACN5.DenACT3, "ACT")
                    
                    Set nodx = tvwEstrMat.Nodes.Add("ACN3" & scod1 & scod2 & scod3, tvwChild, "ACN4" & scod1 & scod2 & scod3 & scod4, oACN5.CodACT4 & " - " & oACN5.DenACT4, "ACT")
                    
                    Set nodx = tvwEstrMat.Nodes.Add("ACN4" & scod1 & scod2 & scod3 & scod4, tvwChild, "ACN5" & scod1 & scod2 & scod3 & scod4 & scod5, oACN5.Cod & " - " & oACN5.Den, "ACTASIG")
                    
                    If nodx.Parent.Image = "ACTASIG" Or nodx.Image = "ACTASIG" Then
                        nodx.Parent.Parent.Parent.Parent.Expanded = True
                        nodx.Parent.Parent.Parent.Expanded = True
                        nodx.Parent.Parent.Expanded = True
                        nodx.Parent.Expanded = True
                    End If
                    
                Next
            
            End If
            
            Set oActsN5 = Nothing
            Set oACN5 = Nothing
              
            tvwEstrMat.Nodes("Raiz").Selected = True
            
            'Cargamos las Observaciones
            txtObs = NullToStr(.Comen)
   
            'Cargamos contactos
            If .Cod <> "" And Not IsNull(.Cod) Then
                .CargarTodosLosContactos , , , , True, , , , , , , , True, True
            Else
                .CargarContactosDesdePortal True
            End If
            For Each m_oContacto In .Contactos
                sdbgContactos.AddItem m_oContacto.Apellidos & Chr(m_lSeparador) & m_oContacto.nombre & Chr(m_lSeparador) & m_oContacto.Departamento & Chr(m_lSeparador) & m_oContacto.Cargo & Chr(m_lSeparador) & m_oContacto.Tfno & Chr(m_lSeparador) & m_oContacto.Tfno2 & Chr(m_lSeparador) & m_oContacto.tfnomovil & Chr(m_lSeparador) & m_oContacto.Fax & Chr(m_lSeparador) & m_oContacto.mail & Chr(m_lSeparador) & m_oContacto.NIF & Chr(m_lSeparador) & m_oContacto.Def & Chr(m_lSeparador) & m_oContacto.Port & Chr(m_lSeparador) & m_oContacto.Aprovisionador & Chr(m_lSeparador) & m_oContacto.NotifSubasta
            Next
                        
            'Cargamos las Especificaciones (Ficheros Adjuntos)
            .CargarTodasLasEspecificaciones True, False
            AnyadirEspsProvePortalALista
            lstvwEspProvePortal.Visible = True
            lstvwEspProvePortal.Height = stabPROVE.Height - 1400
            lstvwEspProvePortal.Width = stabPROVE.Width - 600
            lstvwEspProvePortal.ColumnHeaders(1).Width = lstvwEspProvePortal.Width * 0.2
            lstvwEspProvePortal.ColumnHeaders(2).Width = lstvwEspProvePortal.Width * 0.15
            lstvwEspProvePortal.ColumnHeaders(3).Width = lstvwEspProvePortal.Width * 0.5
            lstvwEspProvePortal.ColumnHeaders(4).Width = lstvwEspProvePortal.Width * 0.19
            
        End With

    End If
    
    Screen.MousePointer = vbNormal
    
Error:
    Set nodx = Nothing
    Resume Next
End Sub

Private Sub AnyadirEspsProvePortalALista()
Dim oEspProvePortal As CEspecificacion

    lstvwEspProvePortal.ListItems.clear
    
    For Each oEspProvePortal In g_oProveSeleccionado.EspecificacionesProvePortal
        lstvwEspProvePortal.ListItems.Add , "ESP" & CStr(oEspProvePortal.Id), oEspProvePortal.nombre, , "ESP"
        lstvwEspProvePortal.ListItems.Item("ESP" & CStr(oEspProvePortal.Id)).ToolTipText = NullToStr(oEspProvePortal.Comentario)
        lstvwEspProvePortal.ListItems.Item("ESP" & CStr(oEspProvePortal.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEspProvePortal.DataSize / 1024) & " " & m_skb
        lstvwEspProvePortal.ListItems.Item("ESP" & CStr(oEspProvePortal.Id)).ListSubItems.Add , "Com", NullToStr(oEspProvePortal.Comentario)
        lstvwEspProvePortal.ListItems.Item("ESP" & CStr(oEspProvePortal.Id)).ListSubItems.Add , "Fec", oEspProvePortal.Fecha
        lstvwEspProvePortal.ListItems.Item("ESP" & CStr(oEspProvePortal.Id)).Tag = oEspProvePortal.Id
    Next
    
    Set oEspProvePortal = Nothing

End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROVPORDET, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    

        Me.caption = Ador(0).Value     '1
        m_stxtTitulo = Ador(0).Value
        Ador.MoveNext
        stabPROVE.TabCaption(0) = Ador(0).Value  '2
        Ador.MoveNext
        stabPROVE.TabCaption(1) = Ador(0).Value  '3  Actividades
        m_sIdioma = Ador(0).Value
        Ador.MoveNext
        stabPROVE.TabCaption(2) = Ador(0).Value '4
        Ador.MoveNext
        stabPROVE.TabCaption(3) = Ador(0).Value '5
        Ador.MoveNext
        stabPROVE.TabCaption(4) = Ador(0).Value  '6
        Ador.MoveNext
        lblCod.caption = Ador(0).Value  '7
        Ador.MoveNext
        lblDen.caption = Ador(0).Value  '8
        Ador.MoveNext
        lblNif.caption = Ador(0).Value '9
        Ador.MoveNext
        lblDir.caption = Ador(0).Value '10
        Ador.MoveNext
        lblCP.caption = Ador(0).Value '11
        Ador.MoveNext
        lblPob.caption = Ador(0).Value '12
        Ador.MoveNext
        lblPai.caption = Ador(0).Value '13
        Ador.MoveNext
        lblPro.caption = Ador(0).Value '14
        Ador.MoveNext
        lblMon.caption = Ador(0).Value '15
        Ador.MoveNext
        lblIdi.caption = Ador(0).Value & ":" '16
        Ador.MoveNext
        lblUrl.caption = Ador(0).Value '17
        Ador.MoveNext
        lblVol.caption = Ador(0).Value '18
        Ador.MoveNext
        frmRef.caption = Ador(0).Value '19
        Ador.MoveNext
        frmHom.caption = Ador(0).Value '20
        Ador.MoveNext
        sdbgContactos.Columns(0).caption = Ador(0).Value '21
        Ador.MoveNext
        sdbgContactos.Columns(1).caption = Ador(0).Value '22
        Ador.MoveNext
        sdbgContactos.Columns(2).caption = Ador(0).Value '23
        Ador.MoveNext
        sdbgContactos.Columns(3).caption = Ador(0).Value '24
        Ador.MoveNext
        sdbgContactos.Columns(4).caption = Ador(0).Value '25
        Ador.MoveNext
        sdbgContactos.Columns(5).caption = Ador(0).Value '26
        Ador.MoveNext
        sdbgContactos.Columns(6).caption = Ador(0).Value '27
        Ador.MoveNext
        sdbgContactos.Columns(7).caption = Ador(0).Value '28
        Ador.MoveNext
        sdbgContactos.Columns(8).caption = Ador(0).Value '29
        Ador.MoveNext
        sdbgContactos.Columns(10).caption = Ador(0).Value '30
        Ador.MoveNext
        sdbgContactos.Columns(12).caption = Ador(0).Value '31
        Ador.MoveNext
        lblEspProvePortal.caption = Ador(0).Value '32
        Ador.MoveNext
        lstvwEspProvePortal.ColumnHeaders(1).Text = Ador(0).Value '33
        Ador.MoveNext
        lstvwEspProvePortal.ColumnHeaders(3).Text = Ador(0).Value '34
        Ador.MoveNext
        lstvwEspProvePortal.ColumnHeaders(4).Text = Ador(0).Value '35
        Ador.MoveNext
        m_stxtDe = Ador(0).Value '36
        Ador.MoveNext
        m_stxtPag = Ador(0).Value '37
        Ador.MoveNext
        m_stxtProve = Ador(0).Value '38
        Ador.MoveNext
        m_stxtTipo = Ador(0).Value '39
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        m_sIdiGuardar = Ador(0).Value '42
        Ador.MoveNext
        m_sIdiTipoOrig = Ador(0).Value '43
        Ador.MoveNext
        m_sIdiArchivo = Ador(0).Value '44
        
        Ador.MoveNext
        lstvwEspProvePortal.ColumnHeaders(2).Text = Ador(0).Value
        Ador.MoveNext
        m_skb = Ador(0).Value
        
        Ador.MoveNext
        If gParametrosGenerales.gbSincronizacionMat = True Then
            stabPROVE.TabCaption(1) = Ador(0).Value  '47  Materiales
            m_sIdioma = Ador(0).Value
        End If
        
        Ador.MoveNext
        sdbgContactos.Columns(13).caption = Ador(0).Value '48
        'Ador.MoveNext
        'sdbgContactos.Columns("NIF").caption = Ador(0).Value '49 Nif
        sdbgContactos.Columns("NIF").caption = "Nif"
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

Private Sub RestoDeCampos()

        m_ArItem.AddNew
        m_ArItem("DEN").Value = txtDen
        m_ArItem("COD").Value = txtCod
        m_ArItem("NIF").Value = txtNif
        m_ArItem("DIR").Value = txtDir
        m_ArItem("VOLFACT").Value = txtVol
        m_ArItem("CP").Value = txtCP
        m_ArItem("POB").Value = txtPob
        m_ArItem("PROVDEN").Value = txtProv
        m_ArItem("PAIDEN").Value = txtPai
        m_ArItem("URLCIA").Value = txtURL
        m_ArItem("CLIREF1").Value = txtCliRef1
        m_ArItem("CLIREF2").Value = txtCliRef2
        m_ArItem("CLIREF3").Value = txtCliRef3
        m_ArItem("CLIREF4").Value = txtCliRef4
        m_ArItem("HOM1").Value = txtHom1
        m_ArItem("HOM2").Value = txtHom2
        m_ArItem("HOM3").Value = txtHom3
        m_ArItem("COMENT").Value = txtObs
        m_ArItem("USUNOM").Value = ""
        m_ArItem("USUAPE").Value = ""
        m_ArItem("USUFCEST").Value = 0
        m_ArItem("USUFPEST") = 0
        m_ArItem("USUDEP").Value = ""
        m_ArItem("USUCAR").Value = ""
        m_ArItem("USUTFNO").Value = ""
        m_ArItem("USUTFNO2").Value = ""
        m_ArItem("USUFAX").Value = ""
        m_ArItem("USUMAIL").Value = ""
        m_ArItem("USUTFNOMOV").Value = ""
        m_ArItem("CIAESPNOM").Value = ""
        m_ArItem("CIAESPCOM").Value = ""
End Sub


Private Sub cmdAbrirAA_Click()
    Dim Item As MSComctlLib.listItem
    Dim DataFile As Integer
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
    Dim lInit As Long
    Dim lSize As Long
    Dim bytChunk() As Byte


On Error GoTo Cancelar:
    Set Item = lstvwEspProvePortal.selectedItem
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
        Exit Sub
    End If

    If g_oProveSeleccionado Is Nothing Then Exit Sub

    sFileName = FSGSLibrary.DevolverPathFichTemp
    sFileName = sFileName & Item.Text
    sFileTitle = Item.Text

    'Comprueba si existe el fichero y si existe se borra:
    Dim FOSFile As Scripting.FileSystemObject
    Set FOSFile = New Scripting.FileSystemObject
    If FOSFile.FileExists(sFileName) Then
        FOSFile.DeleteFile sFileName, True
    End If
    Set FOSFile = Nothing
    
    ' Cargamos el contenido en la esp.
    Set oEsp = g_oProveSeleccionado.EspecificacionesProvePortal.Item(CStr(lstvwEspProvePortal.selectedItem.Tag))
    teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspProveedor, True)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oEsp = Nothing

        Exit Sub
    End If
    oEsp.LeerAdjuntoCia oEsp.Id, g_oProveSeleccionado.IDPortal, oEsp.DataSize, sFileName
    
    m_sArFileNames(UBound(m_sArFileNames)) = sFileName
    ReDim Preserve m_sArFileNames(UBound(m_sArFileNames) + 1)

    'Lanzamos la aplicacion
    ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If

    Set oEsp = Nothing
End Sub

''' <summary>
''' Obtener un listado de compradores
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdlistado_Click()

Dim oReport As CRAXDRT.Report
Dim pv As Preview
Dim oFos As FileSystemObject
Dim RepPath As String
Dim oEspProvePortal As CEspecificacion
Dim i As Integer
Dim node As MSComctlLib.node
Dim ActDen As String

       
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptCiasDetalle.rpt"
        
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
   
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    Screen.MousePointer = vbHourglass
    
    ' Formula Fields
   
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & m_stxtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_stxtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProv")).Text = """" & m_stxtProve & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodigo")).Text = """" & lblCod.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNif")).Text = """" & lblNif.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtVolFact")).Text = """" & lblVol.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDir")).Text = """" & lblDir.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProvi")).Text = """" & lblPro.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPai")).Text = """" & lblPai.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtURL")).Text = """" & lblUrl.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCli")).Text = """" & frmRef.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNom")).Text = """" & sdbgContactos.Columns(1).caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtHom")).Text = """" & frmHom.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtObs")).Text = """" & stabPROVE.TabCaption(2) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCom")).Text = """" & lstvwEspProvePortal.ColumnHeaders(3).Text & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDep")).Text = """" & sdbgContactos.Columns(2).caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCar")).Text = """" & sdbgContactos.Columns(3).caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTfno")).Text = """" & sdbgContactos.Columns(4).caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTipo")).Text = """" & m_stxtTipo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFax")).Text = """" & sdbgContactos.Columns(7).caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMail")).Text = """" & sdbgContactos.Columns(8).caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTfnoMov")).Text = """" & sdbgContactos.Columns(6).caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFich")).Text = """" & stabPROVE.TabCaption(4) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAct")).Text = """" & stabPROVE.TabCaption(1) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCont")).Text = """" & stabPROVE.TabCaption(3) & """"
    
    Set m_ArItem = New Ador.Recordset
    
    m_ArItem.Fields.Append "DEN", adVarChar, 100
    m_ArItem.Fields.Append "COD", adVarChar, 20
    m_ArItem.Fields.Append "NIF", adVarChar, 20
    m_ArItem.Fields.Append "DIR", adVarChar, 200
    m_ArItem.Fields.Append "VOLFACT", adDouble
    m_ArItem.Fields.Append "CP", adVarChar, 20
    m_ArItem.Fields.Append "POB", adVarChar, 100
    m_ArItem.Fields.Append "PROVDEN", adVarChar, 100
    m_ArItem.Fields.Append "PAIDEN", adVarChar, 100
    m_ArItem.Fields.Append "URLCIA", adVarChar, 255
    m_ArItem.Fields.Append "CLIREF1", adVarChar, 50
    m_ArItem.Fields.Append "CLIREF2", adVarChar, 50
    m_ArItem.Fields.Append "CLIREF3", adVarChar, 50
    m_ArItem.Fields.Append "CLIREF4", adVarChar, 50
    m_ArItem.Fields.Append "HOM1", adVarChar, 50
    m_ArItem.Fields.Append "HOM2", adVarChar, 50
    m_ArItem.Fields.Append "HOM3", adVarChar, 50
    m_ArItem.Fields.Append "COMENT", adVarChar, 1000
    m_ArItem.Fields.Append "USUNOM", adVarChar, 20
    m_ArItem.Fields.Append "USUAPE", adVarChar, 100
    m_ArItem.Fields.Append "USUFCEST", adInteger
    m_ArItem.Fields.Append "USUFPEST", adInteger
    m_ArItem.Fields.Append "USUDEP", adVarChar, 100
    m_ArItem.Fields.Append "USUCAR", adVarChar, 100
    m_ArItem.Fields.Append "USUTFNO", adVarChar, 20
    m_ArItem.Fields.Append "USUTFNO2", adVarChar, 20
    m_ArItem.Fields.Append "USUFAX", adVarChar, 20
    m_ArItem.Fields.Append "USUMAIL", adVarChar, 100
    m_ArItem.Fields.Append "USUTFNOMOV", adVarChar, 20
    m_ArItem.Fields.Append "CIAESPNOM", adVarChar, 300
    m_ArItem.Fields.Append "CIAESPCOM", adVarChar, 500
    m_ArItem.Fields.Append "ACT1", adVarChar, 20
    m_ArItem.Fields.Append "ACT2", adVarChar, 20
    m_ArItem.Fields.Append "ACT3", adVarChar, 20
    m_ArItem.Fields.Append "ACT4", adVarChar, 20
    m_ArItem.Fields.Append "ACT5", adVarChar, 20
    m_ArItem.Fields.Append "ACTDEN", adVarChar, 300
    
    m_ArItem.Open
    sdbgContactos.MoveFirst
    For i = 0 To sdbgContactos.Rows
        m_ArItem.AddNew
        m_ArItem("DEN").Value = txtDen
        m_ArItem("COD").Value = txtCod
        m_ArItem("NIF").Value = txtNif
        m_ArItem("DIR").Value = txtDir
        m_ArItem("VOLFACT").Value = txtVol
        m_ArItem("CP").Value = txtCP
        m_ArItem("POB").Value = txtPob
        m_ArItem("PROVDEN").Value = txtProv
        m_ArItem("PAIDEN").Value = txtPai
        m_ArItem("URLCIA").Value = txtURL
        m_ArItem("CLIREF1").Value = txtCliRef1
        m_ArItem("CLIREF2").Value = txtCliRef2
        m_ArItem("CLIREF3").Value = txtCliRef3
        m_ArItem("CLIREF4").Value = txtCliRef4
        m_ArItem("HOM1").Value = txtHom1
        m_ArItem("HOM2").Value = txtHom2
        m_ArItem("HOM3").Value = txtHom3
        m_ArItem("COMENT").Value = txtObs
        m_ArItem("USUNOM").Value = sdbgContactos.Columns("DEN").Value
        m_ArItem("USUAPE").Value = sdbgContactos.Columns("APE").Value
        If sdbgContactos.Columns("DEF").Value = "" Then
            m_ArItem("USUFCEST").Value = 0
        Else
            m_ArItem("USUFCEST").Value = BooleanToSQLBinary(sdbgContactos.Columns("DEF").Value)
        End If
        If sdbgContactos.Columns("APROV").Value = "" Then
            m_ArItem("USUFPEST").Value = 0
        Else
            m_ArItem("USUFPEST") = BooleanToSQLBinary(sdbgContactos.Columns("APROV").Value)
        End If
        m_ArItem("USUDEP").Value = sdbgContactos.Columns("DEP").Value
        m_ArItem("USUCAR").Value = sdbgContactos.Columns("CAR").Value
        m_ArItem("USUTFNO").Value = sdbgContactos.Columns("TFNO").Value
        m_ArItem("USUTFNO2").Value = sdbgContactos.Columns("TFNO2").Value
        m_ArItem("USUFAX").Value = sdbgContactos.Columns("FAX").Value
        m_ArItem("USUMAIL").Value = sdbgContactos.Columns("MAIL").Value
        m_ArItem("USUTFNOMOV").Value = sdbgContactos.Columns("TFNO_MOVIL").Value
        m_ArItem("CIAESPNOM").Value = ""
        m_ArItem("CIAESPCOM").Value = ""
        m_ArItem("ACT1").Value = "0"
        m_ArItem("ACT2").Value = "0"
        m_ArItem("ACT3").Value = "0"
        m_ArItem("ACT4").Value = "0"
        m_ArItem("ACT5").Value = "0"
        m_ArItem("ACTDEN").Value = ""
        m_ArItem.Update
        sdbgContactos.MoveNext
        i = i + 1
    Next i
    
    For Each oEspProvePortal In g_oProveSeleccionado.EspecificacionesProvePortal
        m_ArItem.AddNew
        m_ArItem("DEN").Value = txtDen
        m_ArItem("COD").Value = txtCod
        m_ArItem("NIF").Value = txtNif
        m_ArItem("DIR").Value = txtDir
        m_ArItem("VOLFACT").Value = txtVol
        m_ArItem("CP").Value = txtCP
        m_ArItem("POB").Value = txtPob
        m_ArItem("PROVDEN").Value = txtProv
        m_ArItem("PAIDEN").Value = txtPai
        m_ArItem("URLCIA").Value = txtURL
        m_ArItem("CLIREF1").Value = txtCliRef1
        m_ArItem("CLIREF2").Value = txtCliRef2
        m_ArItem("CLIREF3").Value = txtCliRef3
        m_ArItem("CLIREF4").Value = txtCliRef4
        m_ArItem("HOM1").Value = txtHom1
        m_ArItem("HOM2").Value = txtHom2
        m_ArItem("HOM3").Value = txtHom3
        m_ArItem("COMENT").Value = txtObs
        m_ArItem("USUNOM").Value = ""
        m_ArItem("USUAPE").Value = ""
        m_ArItem("USUFCEST").Value = 0
        m_ArItem("USUFPEST") = 0
        m_ArItem("USUDEP").Value = ""
        m_ArItem("USUCAR").Value = ""
        m_ArItem("USUTFNO").Value = ""
        m_ArItem("USUTFNO2").Value = ""
        m_ArItem("USUFAX").Value = ""
        m_ArItem("USUMAIL").Value = ""
        m_ArItem("USUTFNOMOV").Value = ""
        m_ArItem("CIAESPNOM").Value = NullToStr(oEspProvePortal.nombre)
        m_ArItem("CIAESPCOM").Value = NullToStr(oEspProvePortal.Comentario)
        m_ArItem("ACT1").Value = "0"
        m_ArItem("ACT2").Value = "0"
        m_ArItem("ACT3").Value = "0"
        m_ArItem("ACT4").Value = "0"
        m_ArItem("ACT5").Value = "0"
        m_ArItem("ACTDEN").Value = 0
        m_ArItem.Update
    Next
    
    'Recorremos todo el �rbol buscar las actividades escogidas
    For i = 1 To tvwEstrMat.Nodes.Count
        
        Set node = tvwEstrMat.Nodes(i)
        If Not node Is Nothing And node <> "Actividades" Then
            Screen.MousePointer = vbHourglass
            
            ActDen = Mid(node, 6)
            If node.Image = "ACTASIG" Then
                Select Case Left(node.key, 4)
                Case "ACN1"
                    
                        RestoDeCampos
                        m_ArItem("ACT1").Value = DevolverId(node)
                        m_ArItem("ACT2").Value = "0"
                        m_ArItem("ACT3").Value = "0"
                        m_ArItem("ACT4").Value = "0"
                        m_ArItem("ACT5").Value = "0"
                        m_ArItem("ACTDEN").Value = ActDen
                        m_ArItem.Update
                              
                Case "ACN2"
                    
                        RestoDeCampos
                        m_ArItem("ACT1").Value = DevolverId(node.Parent)
                        m_ArItem("ACT2").Value = DevolverId(node)
                        m_ArItem("ACT3").Value = "0"
                        m_ArItem("ACT4").Value = "0"
                        m_ArItem("ACT5").Value = "0"
                        m_ArItem("ACTDEN").Value = ActDen
                        m_ArItem.Update
                   
                Case "ACN3"
                    
                        RestoDeCampos
                        m_ArItem("ACT1").Value = DevolverId(node.Parent.Parent)
                        m_ArItem("ACT2").Value = DevolverId(node.Parent)
                        m_ArItem("ACT3").Value = DevolverId(node)
                        m_ArItem("ACT4").Value = "0"
                        m_ArItem("ACT5").Value = "0"
                        m_ArItem("ACTDEN").Value = ActDen
                        m_ArItem.Update
                    
                Case "ACN4"
                    
                        RestoDeCampos
                        m_ArItem("ACT1").Value = DevolverId(node.Parent.Parent.Parent)
                        m_ArItem("ACT2").Value = DevolverId(node.Parent.Parent)
                        m_ArItem("ACT3").Value = DevolverId(node.Parent)
                        m_ArItem("ACT4").Value = DevolverId(node)
                        m_ArItem("ACT5").Value = "0"
                        m_ArItem("ACTDEN").Value = ActDen
                        m_ArItem.Update
 
                Case "ACN5"
                    
                        RestoDeCampos
                        m_ArItem("ACT1").Value = DevolverId(node.Parent.Parent.Parent.Parent)
                        m_ArItem("ACT2").Value = DevolverId(node.Parent.Parent.Parent)
                        m_ArItem("ACT3").Value = DevolverId(node.Parent.Parent)
                        m_ArItem("ACT4").Value = DevolverId(node.Parent)
                        m_ArItem("ACT5").Value = DevolverId(node)
                        m_ArItem("ACTDEN").Value = ActDen
                        m_ArItem.Update
                    
                End Select
            End If
        End If
    Next
    
    oReport.Database.SetDataSource m_ArItem
    
    Set m_ArItem = Nothing
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    DoEvents
    frmESPERA.Show
    frmESPERA.lblGeneral.caption = Me.caption
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""

    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = m_stxtTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    Timer1.Enabled = True
    
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Set oReport = Nothing
    Unload Me
    DoEvents
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdSalvarAA_Click()
    Dim Item As MSComctlLib.listItem
    Dim DataFile As Integer
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
    Dim lInit As Long
    Dim lSize As Long
    Dim bytChunk() As Byte

On Error GoTo Cancelar:

    Set Item = lstvwEspProvePortal.selectedItem
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
        Exit Sub
    End If

    If g_oProveSeleccionado Is Nothing Then Exit Sub
    dlgEsp.DialogTitle = m_sIdiGuardar

    dlgEsp.Filter = m_sIdiTipoOrig & "|*.*"
    dlgEsp.filename = Item.Text
    dlgEsp.ShowSave

    sFileName = dlgEsp.filename
    sFileTitle = dlgEsp.FileTitle
    If sFileTitle = "" Then
        oMensajes.NoValido m_sIdiArchivo
        Set m_oIBAseDatosEnEdicion = Nothing
        Exit Sub
    End If
    ' Cargamos el contenido en la esp.
    Set oEsp = g_oProveSeleccionado.EspecificacionesProvePortal.Item(CStr(lstvwEspProvePortal.selectedItem.Tag))
    teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspProveedor, True)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oEsp = Nothing

        Exit Sub
    End If
    oEsp.LeerAdjuntoCia oEsp.Id, g_oProveSeleccionado.IDPortal, oEsp.DataSize, sFileName
Cancelar:

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        

        Set oEsp = Nothing
    End If
End Sub
Private Sub Form_Load()
    Me.Height = 5340
    Me.Width = 9540
    CargarRecursos
    PonerFieldSeparator Me
    'Inicializar el array de ficheros
    ReDim m_sArFileNames(0)
    If gParametrosGenerales.gbSubasta = False Or gParametrosGenerales.giINSTWEB = SinWeb Then
        sdbgContactos.Columns(13).Visible = False
    Else
        sdbgContactos.Columns(13).Visible = True
    End If
End Sub

Public Function DevolverId(ByVal node As MSComctlLib.node) As Variant

If node Is Nothing Then Exit Function
 
DevolverId = Right(node, Len(node) - 3)
DevolverId = Left(node, Len(node) - Len(DevolverId))

End Function

Private Sub Arrange()
    If Me.Width <= 1200 Then Exit Sub
    
    If Me.Height <= 1700 Then Exit Sub
    
    stabPROVE.Width = Me.Width - 135
    stabPROVE.Height = Me.Height - 420
    Select Case stabPROVE.Tab



        Case 1
            tvwEstrMat.Height = stabPROVE.Height - 795
            tvwEstrMat.Width = stabPROVE.Width - 480

        Case 2
            txtObs.Height = stabPROVE.Height - 1095
            txtObs.Width = stabPROVE.Width - 625


        Case 3
            sdbgContactos.Height = stabPROVE.Height - 795
            sdbgContactos.Width = stabPROVE.Width - 465
            sdbgContactos.Columns(0).Width = sdbgContactos.Width * 20 / 100
            sdbgContactos.Columns(1).Width = sdbgContactos.Width * 20 / 100
            sdbgContactos.Columns(2).Width = sdbgContactos.Width * 20 / 100
            sdbgContactos.Columns(3).Width = sdbgContactos.Width * 16 / 100
            sdbgContactos.Columns(4).Width = sdbgContactos.Width * 17 / 100
            sdbgContactos.Columns(5).Width = sdbgContactos.Width * 15 / 100
            sdbgContactos.Columns(6).Width = sdbgContactos.Width * 20 / 100
            sdbgContactos.Columns("NIF").Width = sdbgContactos.Width * 15 / 100
        Case 4
            lstvwEspProvePortal.Height = stabPROVE.Height - 1395
            lstvwEspProvePortal.Width = stabPROVE.Width - 525
            lstvwEspProvePortal.ColumnHeaders(1).Width = lstvwEspProvePortal.Width * 0.3
            lstvwEspProvePortal.ColumnHeaders(2).Width = lstvwEspProvePortal.Width * 0.15
            lstvwEspProvePortal.ColumnHeaders(3).Width = lstvwEspProvePortal.Width * 0.34
            lstvwEspProvePortal.ColumnHeaders(4).Width = lstvwEspProvePortal.Width * 0.2
            picOpenSave.Top = lstvwEspProvePortal.Top + lstvwEspProvePortal.Height + 15
            picOpenSave.Left = lstvwEspProvePortal.Width - 600

    End Select
End Sub
Private Sub Form_Unload(Cancel As Integer)
Dim FOSFile As Scripting.FileSystemObject
Dim i As Integer
Dim bBorrando As Boolean

On Error GoTo Error
    'Borramos los archivos temporales que hayamos creado
    Set FOSFile = New Scripting.FileSystemObject
    
    i = 0
    While i < UBound(m_sArFileNames)
        bBorrando = True
        If FOSFile.FileExists(m_sArFileNames(i)) Then
            FOSFile.DeleteFile m_sArFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
       
    Set FOSFile = Nothing
    Me.Visible = False
    
    Exit Sub

Error:
    
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = m_sArFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If

End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub stabPROVE_Click(PreviousTab As Integer)
    Arrange
End Sub

Private Sub Timer1_Timer()
    If frmESPERA.ProgressBar2.Value < 90 Then
        frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
    End If
    If frmESPERA.ProgressBar1.Value < 10 Then
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    End If
End Sub
