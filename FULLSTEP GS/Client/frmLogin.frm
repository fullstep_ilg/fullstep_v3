VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmLogin 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Inicio de sesi�n"
   ClientHeight    =   2880
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4545
   ClipControls    =   0   'False
   ForeColor       =   &H00000000&
   Icon            =   "frmLogin.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   2880
   ScaleWidth      =   4545
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      Height          =   1995
      Left            =   180
      ScaleHeight     =   1935
      ScaleWidth      =   4095
      TabIndex        =   2
      Top             =   720
      Width           =   4155
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   870
         TabIndex        =   6
         Top             =   1455
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1980
         TabIndex        =   5
         Top             =   1455
         Width           =   1005
      End
      Begin VB.TextBox txtPWD 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1320
         MaxLength       =   100
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   600
         Width           =   2535
      End
      Begin VB.TextBox txtCOD 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   50
         TabIndex        =   0
         Top             =   180
         Width           =   2535
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcBD 
         Height          =   285
         Left            =   1320
         TabIndex        =   8
         Top             =   1020
         Width           =   2535
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   8
         Columns(0).Width=   4048
         Columns(0).Caption=   "DENOM"
         Columns(0).Name =   "DENOM"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "SERVIDOR"
         Columns(1).Name =   "SERVIDOR"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "NOMBRE"
         Columns(2).Name =   "NOMBRE"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "USUARIO"
         Columns(3).Name =   "USUARIO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "PASSWOD"
         Columns(4).Name =   "PASSWORD"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "PRINCIPAL"
         Columns(5).Name =   "PRINCIPAL"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "LOGIN"
         Columns(6).Name =   "LOGIN"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "ID"
         Columns(7).Name =   "ID"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   4471
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   -2147483630
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblBD 
         BackColor       =   &H00808000&
         Caption         =   "Base de datos:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   240
         TabIndex        =   9
         Top             =   1050
         Width           =   1065
      End
      Begin VB.Label lblPWD 
         BackColor       =   &H00808000&
         Caption         =   "Contrase�a:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   240
         TabIndex        =   4
         Top             =   660
         Width           =   1080
      End
      Begin VB.Label lblCOD 
         BackColor       =   &H00808000&
         Caption         =   "Usuario:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   1080
      End
   End
   Begin VB.Label lblTitulo 
      Alignment       =   2  'Center
      Caption         =   "Inicio de sesi�n FullStep GS"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00400040&
      Height          =   375
      Left            =   15
      TabIndex        =   7
      Top             =   180
      Width           =   4500
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bLogin As Boolean
Private m_iIntentos As Integer

Private m_bPrincipal_Es_Login As Boolean
Private m_sServidor_Login As String
Private m_sNombre_Login As String
Private m_sUsuario_Login As String
Private m_sPassword_Login As String
Private m_lIDServidor_Login As Long

''' <summary>
''' Logearte en la aplicaci�n.
''' Si has proporcionado un usuario/pwd valido pero te ha caducado la pwd,no te logea sino q te pide q cambies la pasword.
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,3</remarks>
Private Sub cmdAceptar_Click()
    Dim iCont As Integer
    Dim sPassEncript As String
    Dim oUsuario As CUsuario
    Dim validadoLDAP As Integer
    Dim liclogin As String, liccontra As String
    Dim res As Integer
    Dim sBDIdiomas As String
    Dim Ador As Ador.Recordset
    Dim sMensaje As String
    
    If txtCOD.Text = "" Then
        sMensaje = oMensajes.CargarTextoMensaje(5)
        sMensaje = sMensaje & " " & oMensajes.CargarTexto(OTROS, 63)
        MsgBox sMensaje, vbCritical, "FULLSTEP GS"
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    If gParametrosGenerales.giWinSecurity = TiposDeAutenticacion.LDAP Then
        validadoLDAP = oGestorSeguridad.ValidarUsuarioLDAP(txtCOD.Text, txtPWD.Text)
    End If
    
    If gParametrosGenerales.giWinSecurity <> TiposDeAutenticacion.LDAP Or validadoLDAP = 1 Then
        Set oUsuario = oFSGSRaiz.generar_cusuario
        
        If Me.sdbcBD.Visible = True Then
            If m_bPrincipal_Es_Login Then
                'Lic
                'Es decir, no se toca gServidor, gBaseDatos
            Else
                gServidor = m_sServidor_Login
                gBaseDatos = m_sNombre_Login
                
                liclogin = m_sUsuario_Login
                liccontra = m_sPassword_Login
                
                'Nos conectamos a la BD de LOGIN
                res = oFSGSRaiz.Conectar(gInstancia, gServidor, gBaseDatos, liclogin, liccontra, True)

                Select Case res
                     Case 1
                         oMensajes.ImposibleConectarBD
                         Unload Me
                         Exit Sub
                End Select
                
                Set oUsuario = oFSGSRaiz.generar_cusuario
                                
                Set oGestorSeguridad = oFSGSRaiz.generar_cgestorseguridad
                
            End If
        Else
            Set oUsuario = oGestorSeguridad.DevolverBaseUsuario(frmLogin.txtCOD)
                
            If oUsuario.Tipo = 0 Or IsNull(oUsuario.fecpwd) Then
                If m_iIntentos < oUsuario.Bloq Or oUsuario.Bloq = 0 Then
                    For iCont = 1 To 5
                        iCont = DesactivarConexion(iCont)
                    Next iCont
                    oMensajes.UsuarioNoAutorizado
                    Set oUsuarioSummit = Nothing
                    m_iIntentos = m_iIntentos + 1
                Else
                    m_bLogin = True
                    Unload Me
                End If
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        
        End If
                
        ''' Validaci�n
        If gParametrosGenerales.giWinSecurity = TiposDeAutenticacion.LDAP And validadoLDAP = 1 Then
            Set oUsuarioSummit = oGestorSeguridad.ValidarUsuario(txtCOD.Text, txtPWD.Text, True)
        Else
            Set oUsuarioSummit = oGestorSeguridad.ValidarUsuario(txtCOD.Text, txtPWD.Text) 'la fecha la lee de bbdd
        End If
        
        
        If oUsuarioSummit.Bloq <> 100 Then
            If oUsuarioSummit Is Nothing Or oUsuarioSummit.Cod = "" Then
                oMensajes.UsuarioNoAutorizado
                Screen.MousePointer = vbNormal
                Exit Sub
            Else
                m_bLogin = True
            End If
        Else
            m_bLogin = True
        End If
        
        '''CARGA DE TEXTOS PARA CONTROL DE ERRORES
        CargarRecursos_Errores
        If m_bLogin = True Then
            If Me.sdbcBD.Visible = False Then
                
                Unload Me
                
                If Contrase�aExpirada(gParametrosGenerales.giEDAD_MAX_PWD, oUsuarioSummit.fecpwd) Then
                    Screen.MousePointer = vbNormal
                    If Not MostrarFormUSUCamb(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, True, oUsuarioSummit, oMensajes, gParametrosGenerales) Then basSubMain.FinDeSesion
                End If
                
                
            Else
                If Contrase�aExpirada(gParametrosGenerales.giEDAD_MAX_PWD, oUsuarioSummit.fecpwd) Then
                    Screen.MousePointer = vbNormal
                    If Not MostrarFormUSUCamb(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, True, oUsuarioSummit, oMensajes, gParametrosGenerales) Then basSubMain.FinDeSesion
                End If
                                
                'Ya te has logeado en la indicado como LOGIN=1. Y ahora a hacer todo lo q hacia antes basSubMian y este mismo cmdAceptar
                If Me.sdbcBD.Columns("PRINCIPAL").Value = 1 Then
                    If m_bPrincipal_Es_Login Then
                        'No has tocado nada. Todo debe ser como hace bassubmain si no hay bbdd consulta
                    Else
                        'Has tocado gServidor y gBaseDatos. Todo debe ser como hace bassubmain si no hay bbdd consulta
                        CargarInstancia
                    
                        oFSGSRaiz.Conectar gInstancia, gServidor, gBaseDatos, "", "", True
                        
                        Set oGestorSeguridad = oFSGSRaiz.generar_cgestorseguridad
                    End If
                                                        
                    'Esto de DevolverBaseUsuario no se ha hecho sobre la de LOGIN. Solo segun prototipo, ValidarUsuario.
                    'Hago DevolverBaseUsuario sobre la q me CONECTO
                    'El 1er validarUsuario es sobre misma bbdd y usuario q lo ser�a el 2do. Luego no 2do ValidarUsuario.
                    If Not ValidarSobreBdElegida(True) Then
                        Screen.MousePointer = vbNormal
                        oMensajes.UsuarioNoAutorizado
                        Exit Sub
                    End If
                    
                    oUsuarioSummit.IdBDLogin = m_lIDServidor_Login
                    oUsuarioSummit.IdBDConsulta = Me.sdbcBD.Columns("ID").Value
                                                            
                    Screen.MousePointer = vbNormal
                    
                    Unload Me
                                        
                Else
                    gServidor = EncriptarAES("", Me.sdbcBD.Columns("SERVIDOR").Value, False, DateSerial(1974, 3, 5), 1, Administrador)
                    gBaseDatos = EncriptarAES("", Me.sdbcBD.Columns("NOMBRE").Value, False, DateSerial(1974, 3, 5), 1, Administrador)
                    
                    liclogin = EncriptarAES("", Me.sdbcBD.Columns("USUARIO").Value, False, DateSerial(1974, 3, 5), 1, Administrador)
                    liccontra = EncriptarAES("", Me.sdbcBD.Columns("PASSWORD").Value, False, DateSerial(1974, 3, 5), 1, Administrador)
                                    
                    'Nos conectamos a la BD del COMBO
                    'Hacemos lo mismo q basSubMain. Se ha quitado de basSubMain lo posible, lo repetido si
                    'no te conectas a bbdd q no sea la del ini.
                    sBDIdiomas = oFSGSRaiz.RecuperarBDIdiomas(gInstancia, gServidor, gBaseDatos)
                                                      
                    res = oFSGSRaiz.Autentificar(liclogin, liccontra, gInstancia, gServidor, gBaseDatos)
                    
                    Select Case res
                    Case 1
                        m_bLogin = False
                        oMensajes.ImposibleConectarBD
                        Unload Me
                        Exit Sub
                    Case 2
                        m_bLogin = False
                        oMensajes.ImposibleAutentificar
                        Unload Me
                        Exit Sub
                    End Select
                                     
                    res = oFSGSRaiz.Conectar(gInstancia, gServidor, gBaseDatos, liclogin, liccontra)
    
                    Select Case res
                         Case 1
                            m_bLogin = False
                             oMensajes.ImposibleConectarBD
                             Unload Me
                             Exit Sub
                    End Select
                    
                    Set oGestorSeguridad = oFSGSRaiz.generar_cgestorseguridad
                    
                    'Esto de DevolverBaseUsuario no se ha hecho sobre la de LOGIN. Solo segun prototipo, ValidarUsuario.
                    'Hago DevolverBaseUsuario sobre la q me CONECTO
                    'Aprovecho para 2do validarUsuario.
                    If Not ValidarSobreBdElegida(False) Then
                        oMensajes.UsuarioNoAutorizado
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                    
                    oUsuarioSummit.IdBDLogin = m_lIDServidor_Login
                    oUsuarioSummit.IdBDConsulta = Me.sdbcBD.Columns("ID").Value
                    
                    Unload Me
                                                    
                    Set oGestorIdiomas = New FSgsidiomas.CGestorIdiomas
                    
                    oGestorIdiomas.Inicializar (sBDIdiomas)
                    
                    Set oMensajes = New FSGSLibrary.CMensajes
                    Set oMensajes.oGestorIdiomas = oGestorIdiomas
                    Set oMensajes.frmMessageBox = frmMessageBox
                    oMensajes.IdiomaInstalacion = basPublic.gParametrosInstalacion.gIdioma
                                    
                    ' Instancio el gestor de seguridad
                    'O el de BasSubMain o instanciado antes. Set oGestorSeguridad = oFSGSRaiz.generar_cgestorseguridad
                    
                    ''' CARGA DE PARAMETROS GENERALES
                    Set oGestorParametros = oFSGSRaiz.generar_CGestorParametros
                    gParametrosGenerales = oGestorParametros.DevolverParametrosGenerales(gParametrosInstalacion.gIdioma)
                    If gParametrosGenerales.giINSTWEB = ConPortal Then
                        If basParametros.gParametrosGenerales.gsFSP_SRV = "" Or basParametros.gParametrosGenerales.gsFSP_BD = "" Or basParametros.gParametrosGenerales.gsFSP_CIA = "" Then
                            oMensajes.InstalacionPortalIncompleta
                            Unload frmEsperaAccion
                            FinDeSesion
                        End If
                    End If
                    
                End If
            End If
        End If
    End If
    
    If gParametrosGenerales.giWinSecurity = TiposDeAutenticacion.LDAP And validadoLDAP = 0 Then
        oMensajes.UsuarioNoAutorizado
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
End Sub

Private Sub cmdCancelar_Click()
basSubMain.FinDeSesion
End Sub


''' <summary>
''' Carga la pantalla.
''' Tarea 3412: Preparar el login para que permita seleccionar el a�o de la BD al que conectarse
'''     El combo de Base de datos solo aparace de haber registros.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
    CargarRecursos
    
    PonerFieldSeparator Me
    
    m_iIntentos = 1
    
    If Not CargaCombo(True) Then
        Me.lblBD.Visible = False
        Me.sdbcBD.Visible = False
        
        Me.cmdAceptar.Top = Me.sdbcBD.Top
        Me.cmdCancelar.Top = Me.cmdAceptar.Top
        
        Me.Picture1.Height = Me.Picture1.Height - 400
        Me.Height = Me.Height - 400
    End If

End Sub

Private Sub Form_Unload(Cancel As Integer)

If Not m_bLogin Then basSubMain.FinDeSesion

End Sub

''' <summary>
''' Carga los textos multiidioma
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LOGIN, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        lblTitulo.caption = Ador(0).Value
        Ador.MoveNext
        lblCOD.caption = Ador(0).Value
        Ador.MoveNext
        lblPWD.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblBD.caption = Ador(0).Value
        Ador.MoveNext
        sdbcBD.Columns("DENOM").caption = Ador(0).Value
        
        Ador.Close
    
    End If


   Set Ador = Nothing

 

End Sub



''' <summary>
''' Carga el combo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcBD_DropDown()

    sdbcBD.RemoveAll
        
    If CargaCombo(False) Then
    
        sdbcBD.SelStart = 0
        sdbcBD.SelLength = Len(sdbcBD.Text)
        sdbcBD.Refresh
    
    End If
            
End Sub

''' <summary>
''' Inicializa el combo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcBD_InitColumnProps()
    sdbcBD.DataFieldList = "Column 0"
    sdbcBD.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">Text seleccion</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcBD_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcBD.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcBD.Rows - 1
            bm = sdbcBD.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcBD.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcBD.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

''' <summary>
''' Carga el combo y cargo la informaci�n necesaria para conectar con la bd de login
''' </summary>
''' <param name="SeleccionarPrimero">En el form_load, cargo la informaci�n necesaria para conectar con la bd de login (siempre q no sea la db del ini)</param>
''' <returns>Si hay o no registros en la tabla</returns>
''' <remarks>Llamada desde: sdbcBD_DropDown Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Function CargaCombo(ByVal SeleccionarPrimero As Boolean) As Boolean
    Dim oBD As cBaseDeDatos
    Dim ADORs As Recordset
    
    Screen.MousePointer = vbHourglass
    
    Set oBD = oFSGSRaiz.Generar_CBaseDeDatos
    Set ADORs = oBD.CargarBasesDeDatos
    
    If ADORs Is Nothing Then
        CargaCombo = False
    
        Screen.MousePointer = vbNormal
        Exit Function
    End If
   
    While Not ADORs.EOF
        
        If ADORs("LOGIN") = 1 Then
            If ADORs("PRINCIPAL") = 1 Then
                sdbcBD.AddItem ADORs("DENOMINACION") & Chr(m_lSeparador) & ADORs("SERVIDOR") & Chr(m_lSeparador) & ADORs("NOMBRE") & Chr(m_lSeparador) _
                & ADORs("USUARIO") & Chr(m_lSeparador) & ADORs("PASSWORD") & Chr(m_lSeparador) & ADORs("PRINCIPAL") & Chr(m_lSeparador) & ADORs("LOGIN") _
                & Chr(m_lSeparador) & ADORs("ID")
                
                If SeleccionarPrimero Then
                    m_bPrincipal_Es_Login = True
                    m_lIDServidor_Login = ADORs("ID")
                End If
            ElseIf SeleccionarPrimero Then
                m_bPrincipal_Es_Login = False
                m_lIDServidor_Login = ADORs("ID")
                
                m_sServidor_Login = EncriptarAES("", ADORs("SERVIDOR"), False, DateSerial(1974, 3, 5), 1, Administrador)
                m_sNombre_Login = EncriptarAES("", ADORs("NOMBRE"), False, DateSerial(1974, 3, 5), 1, Administrador)
                m_sUsuario_Login = EncriptarAES("", ADORs("USUARIO"), False, DateSerial(1974, 3, 5), 1, Administrador)
                m_sPassword_Login = EncriptarAES("", ADORs("PASSWORD"), False, DateSerial(1974, 3, 5), 1, Administrador)
            End If
        Else
            sdbcBD.AddItem ADORs("DENOMINACION") & Chr(m_lSeparador) & ADORs("SERVIDOR") & Chr(m_lSeparador) & ADORs("NOMBRE") & Chr(m_lSeparador) _
            & ADORs("USUARIO") & Chr(m_lSeparador) & ADORs("PASSWORD") & Chr(m_lSeparador) & ADORs("PRINCIPAL") & Chr(m_lSeparador) & ADORs("LOGIN") _
            & Chr(m_lSeparador) & ADORs("ID")
        End If
        
        ADORs.MoveNext
    Wend
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal
    Set oBD = Nothing
    
    If SeleccionarPrimero Then
        sdbcBD.MoveFirst
        sdbcBD.Text = sdbcBD.Columns("DENOM").Value
        
        sdbcBD.SelStart = 0
        sdbcBD.SelLength = Len(sdbcBD.Columns("DENOM").Value)
        sdbcBD.Refresh
    End If
    
    CargaCombo = True
End Function

''' <summary>
''' Determina si valida el usuario o no.
''' </summary>
''' <param name="ElegidaPrincipal">True. Validar usuario ya hecho.</param>
''' <returns>valida el usuario o no</returns>
''' <remarks>Llamada desde: cmdAceptar_click ; Tiempo m�ximo: 0,2</remarks>
Private Function ValidarSobreBdElegida(ByVal ElegidaPrincipal As Boolean) As Boolean
    Dim oUsuario As CUsuario
    Dim iCont As Integer
    
    Set oUsuario = oGestorSeguridad.DevolverBaseUsuario(frmLogin.txtCOD)
                    
    If oUsuario.Tipo = 0 Or IsNull(oUsuario.fecpwd) Then
        If m_iIntentos < oUsuario.Bloq Or oUsuario.Bloq = 0 Then
            For iCont = 1 To 5
                iCont = DesactivarConexion(iCont)
            Next iCont
            
            m_bLogin = False
            
            Set oUsuarioSummit = Nothing
            m_iIntentos = m_iIntentos + 1
        Else
            m_bLogin = True
            Unload Me
        End If
        
        ValidarSobreBdElegida = False
        
        Exit Function
    End If
    
    If m_bPrincipal_Es_Login And ElegidaPrincipal Then
        m_bLogin = True
        
        ValidarSobreBdElegida = True
        
        Exit Function
    End If
    
    Set oUsuarioSummit = oGestorSeguridad.ValidarUsuario(txtCOD.Text, "", True) 'la fecha la lee de bbdd

    If oUsuarioSummit.Bloq <> 100 Then
        If oUsuarioSummit Is Nothing Or oUsuarioSummit.Cod = "" Then
            oMensajes.UsuarioNoAutorizado
            
            ValidarSobreBdElegida = False
            m_bLogin = False
            
            Exit Function
        Else
            m_bLogin = True
        End If
    Else
        m_bLogin = True
    End If
    
    ValidarSobreBdElegida = True
End Function
