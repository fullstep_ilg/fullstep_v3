VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmItemsWizard 
   Caption         =   "A�adir art�culos a proceso"
   ClientHeight    =   7185
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8730
   Icon            =   "frmItemsWizard.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7185
   ScaleWidth      =   8730
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picCriterios 
      BorderStyle     =   0  'None
      Height          =   5415
      Left            =   120
      ScaleHeight     =   5415
      ScaleWidth      =   8415
      TabIndex        =   7
      Top             =   720
      Width           =   8415
      Begin VB.PictureBox picCriteriosBusqueda 
         BorderStyle     =   0  'None
         Height          =   1455
         Left            =   120
         ScaleHeight     =   1455
         ScaleWidth      =   8415
         TabIndex        =   24
         Top             =   120
         Width           =   8415
         Begin VB.ListBox lstMultiUon 
            BackColor       =   &H80000018&
            Height          =   255
            ItemData        =   "frmItemsWizard.frx":0CB2
            Left            =   1080
            List            =   "frmItemsWizard.frx":0CB9
            TabIndex        =   33
            Top             =   600
            Visible         =   0   'False
            Width           =   5160
         End
         Begin VB.CommandButton cmdBuscaAtrib 
            Height          =   285
            Left            =   7200
            Picture         =   "frmItemsWizard.frx":0CCA
            Style           =   1  'Graphical
            TabIndex        =   31
            TabStop         =   0   'False
            Top             =   1080
            Width           =   315
         End
         Begin VB.CommandButton cmdSelUon 
            Height          =   315
            Left            =   6840
            Picture         =   "frmItemsWizard.frx":0D57
            Style           =   1  'Graphical
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   600
            Width           =   345
         End
         Begin VB.CommandButton cmdBorrarUon 
            Height          =   315
            Left            =   6435
            Picture         =   "frmItemsWizard.frx":0DC3
            Style           =   1  'Graphical
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   600
            Width           =   345
         End
         Begin VB.CheckBox chkBuscarCentrales 
            Caption         =   "Buscar s�lo art�culos centrales"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   4080
            TabIndex        =   26
            Top             =   240
            Width           =   4170
         End
         Begin VB.CheckBox chkBuscarGenericos 
            Caption         =   "Buscar s�lo art�culos genericos"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   120
            TabIndex        =   25
            Top             =   240
            Width           =   3570
         End
         Begin VB.Label lblBusqAtrib 
            Caption         =   "B�squeda por atributos"
            Height          =   255
            Left            =   120
            TabIndex        =   32
            Top             =   1080
            Width           =   1935
         End
         Begin VB.Label lblUon 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   1080
            TabIndex        =   30
            Top             =   600
            Width           =   5160
         End
         Begin VB.Label lblUniOrg 
            BackStyle       =   0  'Transparent
            Caption         =   "DUni. Org:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   120
            TabIndex        =   29
            Top             =   645
            Width           =   1050
         End
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddOper 
         Height          =   915
         Left            =   1320
         TabIndex        =   23
         Top             =   2160
         Width           =   2025
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmItemsWizard.frx":0E68
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Name =   "VALOR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DESC"
         Columns(1).Name =   "DESC"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3572
         _ExtentY        =   1614
         _StockProps     =   77
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame fraChecks 
         BorderStyle     =   0  'None
         Height          =   1455
         Left            =   240
         TabIndex        =   10
         Top             =   4080
         Width           =   7695
         Begin VB.CheckBox chkRecep 
            Caption         =   "DOpcional"
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   2
            Left            =   5400
            TabIndex        =   21
            Top             =   1110
            Width           =   1935
         End
         Begin VB.CheckBox chkRecep 
            Caption         =   "DNo recepcionar"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   5400
            TabIndex        =   20
            Top             =   810
            Width           =   1935
         End
         Begin VB.CheckBox chkRecep 
            Caption         =   "DObligatoria"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   1
            Left            =   5400
            TabIndex        =   19
            Top             =   510
            Width           =   1935
         End
         Begin VB.CheckBox chkAlmacen 
            Caption         =   "DOpcional"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   2
            Left            =   2760
            TabIndex        =   17
            Top             =   1110
            Width           =   1875
         End
         Begin VB.CheckBox chkAlmacen 
            Caption         =   "DNo almacenar"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   0
            Left            =   2760
            TabIndex        =   16
            Top             =   810
            Width           =   1935
         End
         Begin VB.CheckBox chkAlmacen 
            Caption         =   "DObligatorio"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   1
            Left            =   2760
            TabIndex        =   15
            Top             =   510
            Width           =   1905
         End
         Begin VB.CheckBox chkConcepto 
            Caption         =   "DGasto/Inversi�n"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   13
            Top             =   1110
            Width           =   1935
         End
         Begin VB.CheckBox chkConcepto 
            Caption         =   "DInversi�n"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   12
            Top             =   810
            Width           =   1935
         End
         Begin VB.CheckBox chkConcepto 
            Caption         =   "DGasto"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   11
            Top             =   510
            Width           =   1935
         End
         Begin VB.Label lblRecepcion 
            Caption         =   "DRecepci�n"
            Height          =   255
            Left            =   5400
            TabIndex        =   22
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label lblAlmacenamiento 
            Caption         =   "DAlmacenamiento"
            Height          =   375
            Left            =   2760
            TabIndex        =   18
            Top             =   120
            Width           =   1575
         End
         Begin VB.Label lblConcepto 
            Caption         =   "DConcepto"
            Height          =   255
            Left            =   240
            TabIndex        =   14
            Top             =   120
            Width           =   975
         End
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
         Height          =   915
         Left            =   3840
         TabIndex        =   8
         Top             =   2160
         Width           =   2025
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmItemsWizard.frx":0E84
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Name =   "VALOR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DESC"
         Columns(1).Name =   "DESC"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3572
         _ExtentY        =   1614
         _StockProps     =   77
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
         Height          =   2415
         Left            =   240
         TabIndex        =   9
         Top             =   1560
         Width           =   7665
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   12
         stylesets.count =   3
         stylesets(0).Name=   "Yellow"
         stylesets(0).BackColor=   11862015
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmItemsWizard.frx":0EA0
         stylesets(1).Name=   "Normal"
         stylesets(1).ForeColor=   0
         stylesets(1).BackColor=   16777215
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmItemsWizard.frx":0EBC
         stylesets(2).Name=   "Header"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmItemsWizard.frx":0ED8
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         HeadStyleSet    =   "Header"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   12
         Columns(0).Width=   979
         Columns(0).Name =   "USAR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   2831
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   4233
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1058
         Columns(3).Caption=   "OPER"
         Columns(3).Name =   "OPER"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3889
         Columns(4).Caption=   "VALOR"
         Columns(4).Name =   "VALOR"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "GRUPO"
         Columns(5).Name =   "GRUPO"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "INTRO"
         Columns(6).Name =   "INTRO"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "IDTIPO"
         Columns(7).Name =   "IDTIPO"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ID_ATRIB"
         Columns(8).Name =   "ID_ATRIB"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "MAXIMO"
         Columns(9).Name =   "MAXIMO"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "MINIMO"
         Columns(10).Name=   "MINIMO"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   3200
         Columns(11).Visible=   0   'False
         Columns(11).Caption=   "VALOR_ATRIB"
         Columns(11).Name=   "VALOR_ATRIB"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         _ExtentX        =   13520
         _ExtentY        =   4260
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "           >>"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4140
      TabIndex        =   2
      Top             =   6720
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2895
      TabIndex        =   1
      Top             =   6720
      Width           =   1095
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   15
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard.frx":0EF4
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard.frx":1348
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard.frx":13F8
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard.frx":14A8
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard.frx":1558
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard.frx":160F
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard.frx":16BF
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard.frx":176F
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItemsWizard.frx":1826
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgArticulos 
      Height          =   4620
      Left            =   120
      TabIndex        =   5
      Top             =   720
      Width           =   8250
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   13
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmItemsWizard.frx":18D6
      stylesets(1).Name=   "Adjudica"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmItemsWizard.frx":18F2
      stylesets(2).Name=   "styEspAdjSi"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmItemsWizard.frx":1C67
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      PictureButton   =   "frmItemsWizard.frx":1CD7
      BalloonHelp     =   0   'False
      CellNavigation  =   1
      MaxSelectedRows =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   13
      Columns(0).Width=   2064
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777152
      Columns(1).Width=   4154
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1085
      Columns(2).Caption=   "Unidad def."
      Columns(2).Name =   "UNI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   1984
      Columns(3).Caption=   "Consumo anual"
      Columns(3).Name =   "CANT"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   3200
      Columns(4).Caption=   "UONS"
      Columns(4).Name =   "UON_DEN"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   1
      Columns(5).Width=   1535
      Columns(5).Caption=   "CENTRAL"
      Columns(5).Name =   "ART"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "ARTICULOAGREGADO"
      Columns(6).Name =   "AGREGADO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Style=   4
      Columns(6).ButtonsAlways=   -1  'True
      Columns(7).Width=   1138
      Columns(7).Caption=   "Adj"
      Columns(7).Name =   "ADJ"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Style=   4
      Columns(7).ButtonsAlways=   -1  'True
      Columns(8).Width=   1164
      Columns(8).Caption=   "Espec."
      Columns(8).Name =   "ESPEC"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Style=   4
      Columns(8).ButtonsAlways=   -1  'True
      Columns(9).Width=   1640
      Columns(9).Caption=   "Incluir Esp."
      Columns(9).Name =   "INCESPEC"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "ESPECADJ"
      Columns(10).Name=   "ESPECADJ"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "NUMADJ"
      Columns(11).Name=   "NUMADJ"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "GENERICO"
      Columns(12).Name=   "GENERICO"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      _ExtentX        =   14552
      _ExtentY        =   8149
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab ssTabEst 
      Height          =   6300
      Left            =   60
      TabIndex        =   3
      Top             =   345
      Width           =   8625
      _ExtentX        =   15214
      _ExtentY        =   11113
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      TabCaption(0)   =   "Estructura"
      TabPicture(0)   =   "frmItemsWizard.frx":1DE9
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "tvwEstMat"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "tvwEstructura"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Criterio de filtrado"
      TabPicture(1)   =   "frmItemsWizard.frx":1E05
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Art�culos"
      TabPicture(2)   =   "frmItemsWizard.frx":1E21
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      Begin MSComctlLib.TreeView tvwEstructura 
         Height          =   975
         Left            =   6480
         TabIndex        =   6
         Top             =   3840
         Visible         =   0   'False
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   1720
         _Version        =   393217
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin MSComctlLib.TreeView tvwEstMat 
         Height          =   4560
         Left            =   150
         TabIndex        =   4
         Top             =   435
         Width           =   7770
         _ExtentX        =   13705
         _ExtentY        =   8043
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Label lblSel 
      Caption         =   "Seleccione los art�culos que desea a�adir al proceso"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   150
      TabIndex        =   0
      Top             =   60
      Width           =   5535
   End
End
Attribute VB_Name = "frmItemsWizard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnSeparacion As Long = 120

Public g_oGMN4Seleccionado As CGrupoMatNivel4
Public g_oItemSeleccionado As CItem

Public g_bRDest As Boolean
'Datos de presupuestos
Public g_oPresupuestos1Nivel4 As CPresProyectosNivel4
Public g_oPresupuestos2Nivel4 As CPresContablesNivel4
Public g_oPresupuestos3Nivel4 As CPresConceptos3Nivel4
Public g_oPresupuestos4Nivel4 As CPresConceptos4Nivel4
'Datos de distribuci�n
Public g_oDistsNivel1 As CDistItemsNivel1
Public g_oDistsNivel2 As CDistItemsNivel2
Public g_oDistsNivel3 As CDistItemsNivel3
'Datos de items
Public g_oItems As CItems
Public g_vValores As Variant 'Array con valores de unidades,destino, etc.
Public g_vValoresEsc As Variant 'Array con valores de escalados


Public g_bCancelar As Boolean
Public g_iReasignarPresup As Integer '(0->NO reasignar,1->SI reasignar,2->no preguntado)
'Variables para controlar los presupuestos
Public g_bPresAnu1OK As Boolean
Public g_bPresAnu2OK As Boolean
Public g_bPres1OK As Boolean
Public g_bPres2OK As Boolean

Public g_sDistrib As String
Private m_sGMN As String

Private m_aIdentificadores As Collection

Public bRestProvMatComp As Boolean
Public bRMatComprador As Boolean
Public bPermProcMultiMaterial As Boolean
'edu T98

Public oPlantilla As CPlantilla

Public bRComprador As Boolean 'Restringir apertura items al material del comprador
Private sEstructura As String
Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4
Public g_oGrupoSeleccionado As CGrupo
Public g_oProcesoSeleccionado As CProceso
Private m_oUonsSeleccionadas As CUnidadesOrganizativas

Public g_oItemsNoValidos As Dictionary 'Item-Unidad que no se pueden agregar al grid (para el recuento)
Public g_oUniNoValidos As Dictionary ' Unidad-Items que no se pueden agregar al grid (para el mensaje de error

Private m_sIdiEspec As String
Private msTrue As String
Private msFalse As String
Private msMsgMinMax As String
Private arOper As Variant
Private m_sVariasUnidades As String

'Variable para el orden den listado
Private m_sOrdenListado As String
Private m_sOrderAscDesc As String

'Seguridad
Private m_bRestrDistrUsu As Boolean
Private m_bRestrDistrPerf As Boolean

Private oAtribs As CAtributos

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
'''''''''''
''' <summary>
''' A�ade a sdbgItems de frmProce los items
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdFinalizar_Click (frmItemsWizard2,frmItemsWizard3,frmItemsWizard4) </remarks>
''' <remarks>Tiempo m�ximo: dependiendo del numero de elementos puede superar los 2 segundos </remarks>

Public Sub AnyadirItemsAGridApertura()
Dim oItem As CItem
Dim iPresAnu1 As Integer
Dim iPresAnu2 As Integer
Dim iPres1 As Integer
Dim iPres2 As Integer
Dim sItems As String

Dim sGrupo As String
Dim lGrupoID As Long
Dim oPlantillas As CPlantillas



If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set g_oItemsNoValidos = New Dictionary
Set g_oUniNoValidos = New Dictionary

Dim i As Integer
Dim bDePlantilla As Boolean  'Si true es que este proceso se ha creado a partir de una plantilla.

    bDePlantilla = False


    If Not g_oPresupuestos1Nivel4 Is Nothing Then
        If g_oPresupuestos1Nivel4.Count > 0 Then
            iPresAnu1 = 1
        End If
    End If
    
    If Not g_oPresupuestos2Nivel4 Is Nothing Then
        If g_oPresupuestos2Nivel4.Count > 0 Then
            iPresAnu2 = 1
        End If
    End If
    
    If Not g_oPresupuestos3Nivel4 Is Nothing Then
        If g_oPresupuestos3Nivel4.Count > 0 Then
            iPres1 = 1
        End If
    End If
    
    If Not g_oPresupuestos4Nivel4 Is Nothing Then
        If g_oPresupuestos4Nivel4.Count > 0 Then
            iPres2 = 1
        End If
    End If
    
    frmPROCE.sdbgItems.SelBookmarks.RemoveAll
    
    sItems = ""
    
Dim sCadAtribs As String
Dim bPrimero As Boolean
bPrimero = True
Dim bA�adirCadenaAtribs As Boolean
Dim sValor As String
    
sCadAtribs = ""
Dim procesarItem As Boolean

For Each oItem In g_oItems
    procesarItem = True
    If gParametrosGenerales.gbOBLUnidadMedida And oItem.proceso.Grupos.Item(oItem.grupoCod).UsarEscalados Then
        If oItem.proceso.Grupos.Item(oItem.grupoCod).UnidadEscalado <> oItem.UniCod Then
            procesarItem = False
        End If
    End If
    
    
    If procesarItem Then
    
        If bPrimero Then
            If Not IsEmpty(frmPROCE.g_oProcesoSeleccionado.Plantilla) Then
                If Not IsNull(frmPROCE.g_oProcesoSeleccionado.Plantilla) Then
                     bDePlantilla = True
                                
                    If frmPROCE.g_oGrupoSeleccionado.Codigo <> "" Then
                        sGrupo = frmPROCE.g_oGrupoSeleccionado.Codigo
                        lGrupoID = frmPROCE.g_oGrupoSeleccionado.Id
                    End If
                                                        
                    Set oPlantillas = oFSGSRaiz.Generar_CPlantillas
                    oPlantillas.CargarTodasLasPlantillas frmPROCE.g_oProcesoSeleccionado.Plantilla, False
                    Set frmPROCE.m_oPlantillaSeleccionada = oPlantillas.Item(CStr(frmPROCE.g_oProcesoSeleccionado.Plantilla))
                    frmPROCE.m_oPlantillaSeleccionada.CargarTodosLosAtributosEspecificacion sGrupo, True
                    
                    If frmPROCE.m_oPlantillaSeleccionada.AtributosEspecificacion.Count = 0 Then
                        bDePlantilla = False
                    End If
                End If
            End If
            
            bPrimero = False
        End If
                
        If Not g_oGMN4Seleccionado.ARTICULOS Is Nothing Then
            Dim j As Integer
            For j = 1 To g_oGMN4Seleccionado.ARTICULOS.Count
                If g_oGMN4Seleccionado.ARTICULOS.Item(j).Cod = oItem.ArticuloCod Then
                    Set frmPROCE.ArticuloSeleccionado = g_oGMN4Seleccionado.ARTICULOS.Item(j)
                    frmPROCE.A�adirAtributosGridItems

                    For i = 1 To frmPROCE.sdbgItems.Columns.Count - 1
                        sValor = ""
                        If Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 1, 3) = "AT_" Then
                            If Not frmPROCE.ArticuloSeleccionado.ATRIBUTOS.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))) Is Nothing Then
    
                                Select Case frmPROCE.ArticuloSeleccionado.ATRIBUTOS.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).Tipo
                                Case TipoBoolean
                                    Select Case frmPROCE.ArticuloSeleccionado.ATRIBUTOS.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorBool
                                        Case 0, False
                                            sValor = Chr(m_lSeparador) & frmPROCE.m_sIdiFalse
                                        Case 1, True
                                            sValor = Chr(m_lSeparador) & frmPROCE.m_sIdiTrue
                                        Case Else
                                            sValor = Chr(m_lSeparador) & ""
                                    End Select
                                Case TipoFecha
                                    If Not IsNull(frmPROCE.ArticuloSeleccionado.ATRIBUTOS.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorFec) Then
                                        sValor = Chr(m_lSeparador) & Format(frmPROCE.ArticuloSeleccionado.ATRIBUTOS.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorFec, "Short date")
                                    End If
                                Case TipoNumerico
                                    If Not IsNull(frmPROCE.ArticuloSeleccionado.ATRIBUTOS.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorNum) Then
                                        sValor = Chr(m_lSeparador) & frmPROCE.ArticuloSeleccionado.ATRIBUTOS.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorNum
                                    Else
                                        sValor = Chr(m_lSeparador)
                                    End If
                                Case TipoTextoCorto, TipoTextoMedio, TipoTextoLargo, TipoString
                                    sValor = Chr(m_lSeparador) & EliminarTab(NullToStr(frmPROCE.ArticuloSeleccionado.ATRIBUTOS.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorText))
                                End Select
    
                            End If
                            If sValor = "" Then
                                If bDePlantilla Then
                                    If Not frmPROCE.m_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))) Is Nothing Then
                                        Select Case frmPROCE.m_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).Tipo
                                        Case TipoBoolean
                                            Select Case frmPROCE.m_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorBool
                                                Case 0, False
                                                    sValor = Chr(m_lSeparador) & frmPROCE.m_sIdiFalse
                                                Case 1, True
                                                    sValor = Chr(m_lSeparador) & frmPROCE.m_sIdiTrue
                                                Case Else
                                                    sValor = Chr(m_lSeparador) & ""
                                            End Select
                                        Case TipoFecha
                                            If Not IsNull(frmPROCE.m_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorFec) Then
                                                sValor = Chr(m_lSeparador) & Format(frmPROCE.m_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorFec, "Short date")
                                            Else
                                                sValor = Chr(m_lSeparador) & ""
                                            End If
                                        Case TipoNumerico
                                            If Not IsNull(frmPROCE.m_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorNum) Then
                                                sValor = Chr(m_lSeparador) & frmPROCE.m_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorNum
                                            Else
                                                sValor = Chr(m_lSeparador) & ""
                                            End If
                                        Case TipoTextoCorto, TipoTextoMedio, TipoTextoLargo, TipoString
                                            sValor = Chr(m_lSeparador) & EliminarTab(NullToStr(frmPROCE.m_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(Mid(frmPROCE.sdbgItems.Columns.Item(i).Name, 4))).valorText))
                                        End Select
                                    
                                        bA�adirCadenaAtribs = True
                                        sCadAtribs = sCadAtribs & sValor
                                    Else
                                        sCadAtribs = sCadAtribs & Chr(m_lSeparador)
                                    End If
                                
                                Else
                                    sCadAtribs = sCadAtribs & Chr(m_lSeparador)
                                End If
                            Else
                                bA�adirCadenaAtribs = True
                                sCadAtribs = sCadAtribs & sValor
                            End If
    
                        End If
                    Next
                End If
            Next
        End If
        
        'A�o de imputaci�n
        Dim iAnyoImputacion As Integer
        iAnyoImputacion = Obtener_AnyoImputacion_Item(g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, NullToDbl0(oItem.Id), NullToDbl0(oItem.SolicitudId), NullToDbl0(oItem.IdLineaSolicit))
    
        If bA�adirCadenaAtribs Then
            frmPROCE.sdbgItems.AddItem oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador) & IIf(frmPROCE.sdbgItems.Columns("DEST").Visible, oItem.DestCod, "") & Chr(m_lSeparador) & oItem.UniCod & Chr(m_lSeparador) & IIf(frmPROCE.sdbgItems.Columns("PROVE").Visible, oItem.ProveAct, "") _
            & Chr(m_lSeparador) & oItem.Cantidad & Chr(m_lSeparador) & oItem.Precio & Chr(m_lSeparador) & oItem.Presupuesto & Chr(m_lSeparador) & IIf(frmPROCE.sdbgItems.Columns("INI").Visible, oItem.FechaInicioSuministro, "") & Chr(m_lSeparador) & IIf(frmPROCE.sdbgItems.Columns("FIN").Visible, oItem.FechaFinSuministro, "") _
            & Chr(m_lSeparador) & IIf(frmPROCE.sdbgItems.Columns("PAG").Visible, oItem.PagCod, "") & Chr(m_lSeparador) & NullToStr(oItem.SolicitudId) _
            & Chr(m_lSeparador) & IIf(oItem.IdLineaSolicit = 0, "", oItem.IdLineaSolicit) & Chr(m_lSeparador) & IIf(iAnyoImputacion > 0, iAnyoImputacion, "") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & g_sDistrib & Chr(m_lSeparador) & NullToStr(oItem.esp) & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & oItem.Id & Chr(m_lSeparador) & frmPROCE.g_oGrupoSeleccionado.Codigo & Chr(m_lSeparador) & oItem.FECACT & Chr(m_lSeparador) & oItem.EspAdj _
            & Chr(m_lSeparador) & oItem.esp & Chr(m_lSeparador) & oItem.Presupuesto & Chr(m_lSeparador) & oItem.Cantidad & Chr(m_lSeparador) & oItem.Precio & Chr(m_lSeparador) & iPresAnu1 & Chr(m_lSeparador) & iPresAnu2 & Chr(m_lSeparador) & iPres1 & Chr(m_lSeparador) & iPres2 & Chr(m_lSeparador) & BooleanToSQLBinary(oItem.Generico) & Chr(m_lSeparador) & oItem.Descr _
            & Chr(m_lSeparador) & oItem.GMN1Cod & Chr(m_lSeparador) & oItem.GMN2Cod & Chr(m_lSeparador) & oItem.GMN3Cod & Chr(m_lSeparador) & oItem.GMN4Cod & Chr(m_lSeparador) & g_oGMN4Seleccionado.ObtenerDenGMN4(oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN3Cod, oItem.GMN4Cod, basPublic.gParametrosInstalacion.gIdioma) & Chr(m_lSeparador) & frmPROCE.g_oGrupoSeleccionado.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "0" & sCadAtribs
        Else
            frmPROCE.sdbgItems.AddItem oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador) & IIf(frmPROCE.sdbgItems.Columns("DEST").Visible, oItem.DestCod, "") & Chr(m_lSeparador) & oItem.UniCod & Chr(m_lSeparador) & IIf(frmPROCE.sdbgItems.Columns("PROVE").Visible, oItem.ProveAct, "") _
            & Chr(m_lSeparador) & oItem.Cantidad & Chr(m_lSeparador) & oItem.Precio & Chr(m_lSeparador) & oItem.Presupuesto & Chr(m_lSeparador) & IIf(frmPROCE.sdbgItems.Columns("INI").Visible, oItem.FechaInicioSuministro, "") & Chr(m_lSeparador) & IIf(frmPROCE.sdbgItems.Columns("FIN").Visible, oItem.FechaFinSuministro, "") _
            & Chr(m_lSeparador) & IIf(frmPROCE.sdbgItems.Columns("PAG").Visible, oItem.PagCod, "") & Chr(m_lSeparador) & NullToStr(oItem.SolicitudId) _
            & Chr(m_lSeparador) & IIf(oItem.IdLineaSolicit = 0, "", oItem.IdLineaSolicit) & Chr(m_lSeparador) & IIf(iAnyoImputacion > 0, iAnyoImputacion, "") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & g_sDistrib & Chr(m_lSeparador) & NullToStr(oItem.esp) & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & oItem.Id & Chr(m_lSeparador) & frmPROCE.g_oGrupoSeleccionado.Codigo & Chr(m_lSeparador) & oItem.FECACT & Chr(m_lSeparador) & oItem.EspAdj _
            & Chr(m_lSeparador) & oItem.esp & Chr(m_lSeparador) & oItem.Presupuesto & Chr(m_lSeparador) & oItem.Cantidad & Chr(m_lSeparador) & oItem.Precio & Chr(m_lSeparador) & iPresAnu1 & Chr(m_lSeparador) & iPresAnu2 & Chr(m_lSeparador) & iPres1 & Chr(m_lSeparador) & iPres2 & Chr(m_lSeparador) & BooleanToSQLBinary(oItem.Generico) & Chr(m_lSeparador) & oItem.Descr _
            & Chr(m_lSeparador) & oItem.GMN1Cod & Chr(m_lSeparador) & oItem.GMN2Cod & Chr(m_lSeparador) & oItem.GMN3Cod & Chr(m_lSeparador) & oItem.GMN4Cod & Chr(m_lSeparador) & g_oGMN4Seleccionado.ObtenerDenGMN4(oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN3Cod, oItem.GMN4Cod, basPublic.gParametrosInstalacion.gIdioma) & Chr(m_lSeparador) & frmPROCE.g_oGrupoSeleccionado.Id & Chr(m_lSeparador) & ""
        End If
        
        sCadAtribs = ""
        
        'Recalcular el presupuesto
        frmPROCE.g_dPresGrupo = frmPROCE.g_dPresGrupo + NullToDbl0(oItem.Presupuesto)
        frmPROCE.g_oProcesoSeleccionado.PresAbierto = NullToDbl0(frmPROCE.g_oProcesoSeleccionado.PresAbierto) + NullToDbl0(oItem.Presupuesto)
                       
    Else
        ' El item no se a�ade a la lista y hay que a�adirlo a una variable para despu�s mostrar que �tems no se han podido a�adir
    
        
        If g_oUniNoValidos.Exists(CStr(oItem.UniCod)) Then
            g_oUniNoValidos(CStr(oItem.UniCod)) = g_oUniNoValidos(oItem.UniCod) & ", " & oItem.ArticuloCod
        Else
            g_oUniNoValidos(CStr(oItem.UniCod)) = oItem.ArticuloCod
        End If
        g_oItemsNoValidos(CStr(oItem.ArticuloCod)) = oItem.UniCod
    
    End If
    
    
Next
    
frmPROCE.sdbgItems.MoveLast
    
frmPROCE.lblPresGrupo = Format(frmPROCE.g_dPresGrupo, "#,##0.00####################")
frmPROCE.lblPresTotalProce = Format(frmPROCE.g_oProcesoSeleccionado.PresAbierto, "#,##0.00####################")

frmPROCE.g_oGrupoSeleccionado.NumItems = frmPROCE.g_oGrupoSeleccionado.NumItems + g_oItems.Count - g_oItemsNoValidos.Count

If g_oItems.Count > 1 Then
    sItems = " Id:" & g_oItems.Item(1).Id & " a Id:" & g_oItems.Item(g_oItems.Count).Id
Else
    sItems = " Id:" & g_oItems.Item(1).Id
End If

basSeguridad.RegistrarAccion accionessummit.ACCProceAnyaMultItems, "Anyo:" & Trim(frmPROCE.g_oProcesoSeleccionado.Anyo) & " Gmn1:" & Trim(frmPROCE.g_oProcesoSeleccionado.GMN1Cod) & " Proce:" & frmPROCE.g_oProcesoSeleccionado.Cod & sItems

Set oItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "AnyadirItemsAGridApertura", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Public Function HayUON() As Boolean
Dim bUON As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        If g_oDistsNivel1 Is Nothing Then
            bUON = False
        Else
            If g_oDistsNivel1.Count > 0 Then
                bUON = True
            Else
                bUON = False
            End If
        End If
        If Not bUON Then
            If g_oDistsNivel2 Is Nothing Then
                bUON = False
            Else
                If g_oDistsNivel2.Count > 0 Then
                    bUON = True
                Else
                    bUON = False
                End If
            End If
        End If
        If Not bUON Then
            If g_oDistsNivel3 Is Nothing Then
                bUON = False
            Else
                If g_oDistsNivel3.Count > 0 Then
                    bUON = True
                Else
                    bUON = False
                End If
            End If
        End If
        
        HayUON = bUON
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "HayUON", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Public Function HayPresup() As Boolean
Dim bPresup As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        If g_oPresupuestos1Nivel4 Is Nothing Then
            bPresup = False
        Else
            If g_oPresupuestos1Nivel4.Count > 0 Then
                bPresup = True
            Else
                bPresup = False
            End If
        End If
        If Not bPresup Then
            If g_oPresupuestos2Nivel4 Is Nothing Then
                bPresup = False
            Else
                If g_oPresupuestos2Nivel4.Count > 0 Then
                    bPresup = True
                Else
                    bPresup = False
                End If
            End If
        End If
        If Not bPresup Then
            If g_oPresupuestos3Nivel4 Is Nothing Then
                bPresup = False
            Else
                If g_oPresupuestos3Nivel4.Count > 0 Then
                    bPresup = True
                Else
                    bPresup = False
                End If
            End If
        End If
        
        If Not bPresup Then
            If g_oPresupuestos4Nivel4 Is Nothing Then
                bPresup = False
            Else
                If g_oPresupuestos4Nivel4.Count > 0 Then
                    bPresup = True
                Else
                    bPresup = False
                End If
            End If
        End If
        
        HayPresup = bPresup
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "HayPresup", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function


Public Sub SeleccionarArticulos()
Dim i As Long
Dim j As Long
Dim ltotalItems As Long

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgArticulos.MoveFirst
    j = 0
    ltotalItems = g_oItems.Count
    
    For i = 0 To sdbgArticulos.Rows - 1
        If j = ltotalItems Then Exit For
        If g_oItems.Item(CStr(j)).ArticuloCod = sdbgArticulos.Columns("COD").Value Then
            sdbgArticulos.SelBookmarks.Add sdbgArticulos.Bookmark
            j = j + 1
        End If
        sdbgArticulos.MoveNext
    Next
    
    sdbgArticulos.MoveFirst
    sdbgArticulos.Bookmark = sdbgArticulos.SelBookmarks.Item(0)
    
    If ltotalItems > 0 Then
    'Elimino para volver a insertar en caso de que pulse al boton volver de frmitemswizard2
    For i = ltotalItems To 1 Step -1
        g_oItems.Remove (i)
    Next
    
    End If
    
    g_bCancelar = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "SeleccionarArticulos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub



Private Sub Arrange()
    On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    
    If Me.Height < 6900 Then Me.Height = 6900
    If Me.Width < 9090 Then Me.Width = 9090
    
    ssTabEst.Height = Me.Height - 1290
    ssTabEst.Width = Me.Width - 270
    ssTabEst.Visible = True
            
    If Not bPermProcMultiMaterial And frmPROCE.g_oProcesoSeleccionado.MaterialProce.Count = 1 Then
        ssTabEst.TabVisible(0) = False
    End If
    
    Select Case ssTabEst.Tab
        Case 0
            picCriterios.Visible = False
            
            tvwEstMat.Height = ssTabEst.Height - 500
            tvwEstMat.Width = ssTabEst.Width - 250
            
            sdbgArticulos.Visible = False
            sdbgArticulos.Height = Me.Height - 1290
            sdbgArticulos.Width = Me.Width - 270
        Case 1
            sdbgArticulos.Visible = False
            
            picCriterios.Height = ssTabEst.Height - 500
            picCriterios.Width = ssTabEst.Width - 100
            picCriterios.Visible = True
        Case 2
            picCriterios.Visible = False
            
            sdbgArticulos.Height = ssTabEst.Height - 500
            sdbgArticulos.Width = ssTabEst.Width - 100
            sdbgArticulos.Visible = True
    End Select
    
    cmdContinuar.Visible = True
    cmdCancelar.Visible = True
    cmdCancelar.Left = ssTabEst.Width / 2 - cmdCancelar.Width - 100
    cmdContinuar.Left = cmdCancelar.Left + cmdCancelar.Width + 200
    cmdCancelar.Top = ssTabEst.Top + ssTabEst.Height + 100
    cmdContinuar.Top = cmdCancelar.Top

    If sdbgArticulos.Columns("INCESPEC").Visible Then
        sdbgArticulos.Columns("COD").Width = sdbgArticulos.Width * 13 / 100
        sdbgArticulos.Columns("DEN").Width = sdbgArticulos.Width * 25 / 100
        sdbgArticulos.Columns("UNI").Width = sdbgArticulos.Width * 10 / 100
        sdbgArticulos.Columns("CANT").Width = sdbgArticulos.Width * 15 / 100
        sdbgArticulos.Columns("INCESPEC").Width = sdbgArticulos.Width * 15 / 100
    Else
        sdbgArticulos.Columns("COD").Width = sdbgArticulos.Width * 18 / 100
        sdbgArticulos.Columns("DEN").Width = sdbgArticulos.Width * 29 / 100
        sdbgArticulos.Columns("UNI").Width = sdbgArticulos.Width * 13 / 100
        sdbgArticulos.Columns("CANT").Width = sdbgArticulos.Width * 15 / 100
    End If
    If Me.Width > 9000 Then
        sdbgArticulos.Columns("ADJ").Width = sdbgArticulos.Width * 10 / 100
        sdbgArticulos.Columns("ESPEC").Width = sdbgArticulos.Width * 11 / 100
    Else
        sdbgArticulos.Columns("ADJ").Width = sdbgArticulos.Width * 9 / 100
        sdbgArticulos.Columns("ESPEC").Width = sdbgArticulos.Width * 9 / 100
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub

Private Sub CargarRecursos()
Dim Adores As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_ITEMS_WIZARD, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Adores Is Nothing Then
        Me.caption = Adores(0).Value
        Adores.MoveNext
        lblSel.caption = Adores(0).Value
        Adores.MoveNext
        sdbgArticulos.Columns("COD").caption = Adores(0).Value
        Adores.MoveNext
        sdbgArticulos.Columns("DEN").caption = Adores(0).Value
        Adores.MoveNext
        sdbgArticulos.Columns("UNI").caption = Adores(0).Value
        Adores.MoveNext
        sdbgArticulos.Columns("CANT").caption = Adores(0).Value
        Adores.MoveNext
        sdbgArticulos.Columns("ADJ").caption = Adores(0).Value
        Adores.MoveNext
        sdbgArticulos.Columns("ESPEC").caption = Adores(0).Value
        Adores.MoveNext
        sdbgArticulos.Columns("INCESPEC").caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value
        Adores.MoveNext
        ssTabEst.TabCaption(0) = Adores(0).Value
        Adores.MoveNext
        ssTabEst.TabCaption(2) = Adores(0).Value
        Adores.MoveNext
        sEstructura = Adores(0).Value 'Materiales
        Adores.MoveNext
        ssTabEst.TabCaption(1) = Adores(0).Value
        Adores.MoveNext
        chkBuscarGenericos.caption = Adores(0).Value
        Adores.MoveNext
        lblBusqAtrib.caption = Adores(0).Value
        Adores.MoveNext
        sdbgAtributos.Columns("COD").caption = Adores(0).Value
        Adores.MoveNext
        sdbgAtributos.Columns("DEN").caption = Adores(0).Value
        Adores.MoveNext
        sdbgAtributos.Columns("VALOR").caption = Adores(0).Value
        Adores.MoveNext
        lblConcepto.caption = Adores(0).Value
        Adores.MoveNext
        lblAlmacenamiento.caption = Adores(0).Value
        Adores.MoveNext
        lblRecepcion.caption = Adores(0).Value
        Adores.MoveNext
        chkConcepto(0).caption = Adores(0).Value
        Adores.MoveNext
        chkConcepto(1).caption = Adores(0).Value
        Adores.MoveNext
        chkConcepto(2).caption = Adores(0).Value
        Adores.MoveNext
        chkAlmacen(1).caption = Adores(0).Value
        chkRecep(1).caption = Adores(0).Value
        Adores.MoveNext
        chkAlmacen(0).caption = Adores(0).Value
        Adores.MoveNext
        chkAlmacen(2).caption = Adores(0).Value
        chkRecep(2).caption = Adores(0).Value
        Adores.MoveNext
        chkRecep(0).caption = Adores(0).Value
        Adores.MoveNext
        msTrue = Adores(0).Value
        Adores.MoveNext
        msFalse = Adores(0).Value
        Adores.MoveNext
        msMsgMinMax = Adores(0).Value
        Adores.MoveNext
        sdbgAtributos.Columns("OPER").caption = Adores(0).Value
        Adores.MoveNext
        sdbgArticulos.Columns("UON_DEN").caption = Adores(0).Value
        Me.lblUniOrg.caption = Adores(0).Value
        Adores.MoveNext
        m_sVariasUnidades = Adores(0).Value
        Adores.Close
        
    End If
    
        
    'pargen lit
    Dim oLiterales As CLiterales
    Set oLiterales = oGestorParametros.DevolverLiterales(48, 50, basPublic.gParametrosInstalacion.gIdioma)
    sdbgArticulos.Columns("AGREGADO").caption = oLiterales.Item(1).Den 'Art de planta o agregado
    sdbgArticulos.Columns("ART").caption = oLiterales.Item(3).Den 'central
    Set oLiterales = Nothing
    Set Adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Public Sub ConfigurarSeguridad()
    'Restriccion a la distribuci�n de la compra
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUOPers)) Is Nothing) Then
        m_bRestrDistrUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDisCompraUONPerf)) Is Nothing) Then
        m_bRestrDistrPerf = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub cmdBorrarUon_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.lblUon.caption = ""
    m_oUonsSeleccionadas.clear
    CargarAtributos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "cmdBorrarUon_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBuscaAtrib_Click()
    Dim ofrmATRIB As frmAtribMod
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set ofrmATRIB = New frmAtribMod
    
    ofrmATRIB.g_sOrigen = "frmItemsWizard"
    
    ofrmATRIB.sstabGeneral.Tab = 0
    ofrmATRIB.g_GMN1RespetarCombo = True
     
    If Not g_oGMN4Seleccionado Is Nothing Then
        ofrmATRIB.g_sGmn1 = g_oGMN4Seleccionado.GMN1Cod
        ofrmATRIB.g_sGmn2 = g_oGMN4Seleccionado.GMN2Cod
        ofrmATRIB.g_sGmn3 = g_oGMN4Seleccionado.GMN3Cod
        ofrmATRIB.g_sGmn4 = g_oGMN4Seleccionado.Cod
        
        ofrmATRIB.sdbcGMN1_4Cod.Text = g_oGMN4Seleccionado.GMN1Cod
        
        If g_oGMN4Seleccionado.GMN1Den = "" Then
            ofrmATRIB.sdbcGMN1_4Cod_Validate False
        Else
            ofrmATRIB.sdbcGMN1_4Den.Text = g_oGMN4Seleccionado.GMN1Den
        End If
        ofrmATRIB.g_GMN1RespetarCombo = False
        ofrmATRIB.g_GMN2RespetarCombo = True
        ofrmATRIB.sdbcGMN2_4Cod.Text = g_oGMN4Seleccionado.GMN2Cod
        If g_oGMN4Seleccionado.GMN2Den = "" Then
            ofrmATRIB.sdbcGMN2_4Cod_Validate False
        Else
            ofrmATRIB.sdbcGMN2_4Den.Text = g_oGMN4Seleccionado.GMN2Den
        End If
        ofrmATRIB.g_GMN2RespetarCombo = False
        ofrmATRIB.g_GMN3RespetarCombo = True
        ofrmATRIB.sdbcGMN3_4Cod.Text = g_oGMN4Seleccionado.GMN3Cod
        If g_oGMN4Seleccionado.GMN3Den = "" Then
            ofrmATRIB.sdbcGMN3_4Cod_Validate False
        Else
            ofrmATRIB.sdbcGMN3_4Den.Text = g_oGMN4Seleccionado.GMN3Den
        End If
        ofrmATRIB.g_GMN3RespetarCombo = False
        ofrmATRIB.g_GMN4RespetarCombo = True
        ofrmATRIB.sdbcGMN4_4Cod.Text = g_oGMN4Seleccionado.Cod
        If g_oGMN4Seleccionado.Den = "" Then
            ofrmATRIB.sdbcGMN4_4Cod_Validate False
        Else
            ofrmATRIB.sdbcGMN4_4Den.Text = g_oGMN4Seleccionado.Den
        End If
        ofrmATRIB.g_GMN4RespetarCombo = False
        ofrmATRIB.cmdSeleccionar.Visible = True
    End If
    
    ofrmATRIB.sdbgAtributosGrupo.SelectTypeRow = ssSelectionTypeMultiSelectRange
    ofrmATRIB.g_bSoloSeleccion = True
    
    ofrmATRIB.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "cmdBuscaAtrib_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Public Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_bCancelar = True
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdContinuar_Click()
Dim oItem As CItem
Dim i As Long
Dim bEsAsignable As Boolean
Dim bPararInsercion As Boolean
Dim sUON As String
Dim sCodItems As String
Dim numArticulos As Integer
Dim sArticulo As String
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bPermProcMultiMaterial Then
        If sdbgArticulos.SelBookmarks.Count = 0 Then
            Exit Sub
        Else
            numArticulos = sdbgArticulos.SelBookmarks.Count
        End If
        
    Else
        If ssTabEst.Tab = 0 Then
            'Si esta en la pesta�a Estructura y ha seleccionado una rama de material se a�adir�n todos
            'los materiales de esa rama de material
            Dim nodx As MSComctlLib.node
            
            Set nodx = tvwEstMat.selectedItem
            
            If Left(nodx.Tag, 4) <> "GMN4" Then
                oMensajes.SeleccioneMatNivel4
                Exit Sub
            End If
            If Not g_oGMN4Seleccionado Is Nothing Then
                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , , , False, True, , , True
                If g_oGMN4Seleccionado.ARTICULOS.Count = 0 Then
                    oMensajes.NoExistenArticulos
                    Exit Sub
                Else
                    numArticulos = g_oGMN4Seleccionado.ARTICULOS.Count
                    For i = 1 To numArticulos
                        g_oGMN4Seleccionado.ARTICULOS.Item(i).DevolverTodosLosAtributosDelArticulo , True
                    Next
                    CargarGrid
                End If
            Else
                Exit Sub
            End If
        Else
            If sdbgArticulos.SelBookmarks.Count = 0 Then Exit Sub
            numArticulos = sdbgArticulos.SelBookmarks.Count
        End If
    End If
     
    Set oItem = oFSGSRaiz.Generar_CItem
    bPararInsercion = False
    sCodItems = ""
    For i = 0 To numArticulos - 1
    
            If bPermProcMultiMaterial Then
                sdbgArticulos.Bookmark = i
            End If
            bEsAsignable = True
            
            If Not g_oItems.Item(CStr(i)) Is Nothing Then 'Ya est� en la colecci�n
                If GridCheckToBoolean(sdbgArticulos.Columns("INCESPEC").CellValue(sdbgArticulos.SelBookmarks.Item(i))) = True Then
                    g_oItems.Item(CStr(i)).EspAdj = 1
                Else
                    g_oItems.Item(CStr(i)).EspAdj = 0
                End If
                
            Else 'No est� en la colecci�n, se a�ade
                sArticulo = sdbgArticulos.Columns("COD").CellValue(sdbgArticulos.SelBookmarks.Item(i))
                
                sCod = g_oGMN4Seleccionado.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(g_oGMN4Seleccionado.GMN1Cod))
                sCod = sCod & g_oGMN4Seleccionado.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(g_oGMN4Seleccionado.GMN2Cod))
                sCod = sCod & g_oGMN4Seleccionado.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(g_oGMN4Seleccionado.GMN3Cod))
                sCod = sCod & g_oGMN4Seleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(g_oGMN4Seleccionado.Cod))
                sCod = sCod & sArticulo
                Set oItem.AtributosEspecificacion = g_oGMN4Seleccionado.ARTICULOS.Item(sArticulo).DevolverTodosLosAtributosDelArticulo(, True)
                Set g_oGMN4Seleccionado.ARTICULOS.Item(sArticulo).ATRIBUTOS = oItem.AtributosEspecificacion
                
                If frmPROCE.g_oProcesoSeleccionado Is Nothing Then Exit Sub
                bEsAsignable = False
                'Si esta activado el tema de comprobar la asignacion de las UON de los articulos
                    Select Case frmPROCE.g_oProcesoSeleccionado.DefDistribUON
                        Case TipoDefinicionDatoProceso.EnProceso, TipoDefinicionDatoProceso.EnGrupo

                            oItem.ArticuloCod = sdbgArticulos.Columns("COD").CellValue(sdbgArticulos.SelBookmarks.Item(i))
                            Set oItem.proceso = frmPROCE.g_oProcesoSeleccionado
                            oItem.GrupoID = frmPROCE.g_oGrupoSeleccionado.Id
                            oItem.grupoCod = frmPROCE.g_oGrupoSeleccionado.Codigo
                            If oItem.SePuedeAsignar(True) Then
                                bEsAsignable = True
                            End If
                        Case TipoDefinicionDatoProceso.EnItem
                            'Aqui se pone como asignable, se comprueba al seleccionar las UONS
                            bEsAsignable = True
                        End Select
                
                
                If bEsAsignable Then
                    Select Case frmPROCE.g_oProcesoSeleccionado.DefDestino
                        Case EnItem
                                oItem.DestCod = frmPROCE.sdbgItems.Columns("DEST").Value
                        Case EnGrupo
                                If frmPROCE.g_oGrupoSeleccionado.DefDestino Then
                                    oItem.DestCod = frmPROCE.g_oGrupoSeleccionado.DestCod
                                Else
                                    oItem.DestCod = frmPROCE.sdbgItems.Columns("DEST").Value
                                End If
                        Case EnProceso
                                oItem.DestCod = frmPROCE.g_oProcesoSeleccionado.DestCod
                    End Select
                    'PAG
                    Select Case frmPROCE.g_oProcesoSeleccionado.DefFormaPago
                        Case EnItem
                                oItem.PagCod = frmPROCE.sdbgItems.Columns("PAG").Value
                        Case EnGrupo
                                If frmPROCE.g_oGrupoSeleccionado.DefFormaPago Then
                                    oItem.PagCod = frmPROCE.g_oGrupoSeleccionado.PagCod
                                Else
                                    oItem.PagCod = frmPROCE.sdbgItems.Columns("PAG").Value
                                End If
                        Case EnProceso
                                oItem.PagCod = frmPROCE.g_oProcesoSeleccionado.PagCod
                    End Select
                    'FECHAS
                    Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                        Case EnItem
                                If frmPROCE.sdbgItems.Columns("INI").Value <> "" Then
                                    oItem.FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").Value
                                End If
                                If frmPROCE.sdbgItems.Columns("FIN").Value <> "" Then
                                    oItem.FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").Value
                                End If
                        Case EnGrupo
                                If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                                    oItem.FechaInicioSuministro = frmPROCE.g_oGrupoSeleccionado.FechaInicioSuministro
                                    oItem.FechaFinSuministro = frmPROCE.g_oGrupoSeleccionado.FechaFinSuministro
                                Else
                                    If frmPROCE.sdbgItems.Columns("INI").Value <> "" Then
                                        oItem.FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").Value
                                    End If
                                    If frmPROCE.sdbgItems.Columns("FIN").Value <> "" Then
                                        oItem.FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").Value
                                    End If
                                End If
                        Case EnProceso
                                oItem.FechaInicioSuministro = frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro
                                oItem.FechaFinSuministro = frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro
                    End Select
                    'PROVE
                    Select Case frmPROCE.g_oProcesoSeleccionado.DefProveActual
                        Case EnItem
                                oItem.ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").Value)
                        Case EnGrupo
                                If frmPROCE.g_oGrupoSeleccionado.DefProveActual Then
                                    oItem.ProveAct = frmPROCE.g_oGrupoSeleccionado.ProveActual
                                Else
                                    oItem.ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").Value)
                                End If
                        Case EnProceso
                                oItem.ProveAct = NullToStr(frmPROCE.g_oProcesoSeleccionado.ProveActual)
                    End Select
                    'SOLICIT
                    Select Case frmPROCE.g_oProcesoSeleccionado.DefSolicitud
                        Case EnItem
                                If frmPROCE.sdbgItems.Columns("SOLICITUD").Value <> "" And Not frmPROCE.sdbgItems.Columns("SOLICITUD").Locked Then
                                    oItem.SolicitudId = CLng(ObtenerIdSolicitud(frmPROCE.sdbgItems.Columns("SOLICITUD").Value))
                                Else
                                    oItem.SolicitudId = Null
                                End If
                        Case EnGrupo
                                If frmPROCE.g_oGrupoSeleccionado.DefSolicitud Then
                                    oItem.SolicitudId = frmPROCE.g_oGrupoSeleccionado.SolicitudId
                                Else
                                    If frmPROCE.sdbgItems.Columns("SOLICITUD").Value = "" Then
                                        oItem.SolicitudId = Null
                                    Else
                                        oItem.SolicitudId = CLng(frmPROCE.sdbgItems.Columns("SOLICITUD").Value)
                                    End If
                                End If
                        Case EnProceso
                                oItem.SolicitudId = frmPROCE.g_oProcesoSeleccionado.SolicitudId
                    End Select
                    'ART
                    oItem.GMN1Cod = g_oGMN4Seleccionado.GMN1Cod
                    oItem.GMN2Cod = g_oGMN4Seleccionado.GMN2Cod
                    oItem.GMN3Cod = g_oGMN4Seleccionado.GMN3Cod
                    oItem.GMN4Cod = g_oGMN4Seleccionado.Cod
                    
                    oItem.ArticuloCod = sdbgArticulos.Columns("COD").CellValue(sdbgArticulos.SelBookmarks.Item(i))
                    oItem.Descr = sdbgArticulos.Columns("DEN").CellValue(sdbgArticulos.SelBookmarks.Item(i))
                    'UNI
                    oItem.UniCod = sdbgArticulos.Columns("UNI").CellValue(sdbgArticulos.SelBookmarks.Item(i))
                    'CANT
                    If sdbgArticulos.Columns("CANT").CellValue(sdbgArticulos.SelBookmarks.Item(i)) = "" Then
                        oItem.Cantidad = 0
                    Else
                        oItem.Cantidad = sdbgArticulos.Columns("CANT").CellValue(sdbgArticulos.SelBookmarks.Item(i))
                    End If
                    oItem.GrupoID = frmPROCE.g_oGrupoSeleccionado.Id
                    oItem.grupoCod = frmPROCE.g_oGrupoSeleccionado.Codigo
                    oItem.Generico = GridCheckToBoolean(sdbgArticulos.Columns("GENERICO").CellValue(sdbgArticulos.SelBookmarks.Item(i)))
                    
                    g_oItems.Add frmPROCE.g_oProcesoSeleccionado, i, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, , , , , , , IIf(sdbgArticulos.Columns("INCESPEC").CellValue(sdbgArticulos.SelBookmarks.Item(i)) = "-1", 1, 0), , , oItem.ProveAct, , oItem.grupoCod, , oItem.SolicitudId, , , oItem.Generico, oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN3Cod, oItem.GMN4Cod, oItem.AtributosEspecificacion, oItem.GrupoID
                    
                Else
                    'Como oItem.SePuedeAsignar() traga cualquiera que no tenga art4_uon (solo si gbOBLAsigUonArt=true) por aqui no entra
                    ' Se controla la asignacion y no es asignable
                    bPararInsercion = True
                    'Controlar la asignacion de unidades organizativas de los articulos
                    Dim oUon As IUOn
                
                    For Each oUon In g_oProcesoSeleccionado.getUonsDistribucion(oItem.Id, g_oGrupoSeleccionado.Id)
                        sUON = sUON & oUon.titulo & vbCrLf
                    Next
                    sUON = sUON & ";"
                    sCodItems = sCodItems & sdbgArticulos.Columns("COD").CellValue(sdbgArticulos.SelBookmarks.Item(i)) & ";"
                    Set oUon = Nothing
                        
                End If
            End If
        Next
        
'        If gParametrosGenerales.gbOblAsigPresUonArtAper Then
'            If Not g_oItems Is Nothing Then
'                If g_oItems.Count > 0 Then
'                    If g_oItems.MultiplesOrgCompras Then
'                        oMensajes.ImposibleSeleccionarMultiplesItems
'
'                        For i = g_oItems.Count To 1 Step -1
'                            g_oItems.Remove (i)
'                        Next
'
'                        Exit Sub
'                    End If
'                End If
'
'            End If
'        End If
            
        
        If bPararInsercion Then 'And gParametrosGenerales.gbOBLAsigUonArt Then
            oMensajes.ImposibleAnyadirItemADistribucion sUON, sCodItems, TipoDefinicionDatoProceso.EnProceso
            'El tercer parametro es para diferenciar las distribuciones a nivel de item de las demas
            'Eliminamos los g_oItems para que los vuelva a insertar
            'Esto se introduce pq g_oItems es nothing si dejamos 2 minutos de inactividad
            If Not g_oItems Is Nothing Then
                If g_oItems.Count > 0 Then
                    For i = g_oItems.Count To 1 Step -1
                        g_oItems.Remove (i)
                    Next
                End If
                            
            End If
            
            Exit Sub
            
        End If
    
    Set oItem = Nothing
    
    If Not IsEmpty(g_vValores(0)) Then 'hide wizard1
        frmItemsWizard2.MostrarValoresSeleccionados
    Else
        ReDim g_vValores(17)
    End If
    frmItemsWizard2.g_bRDest = g_bRDest
    frmItemsWizard2.bRestProvMatComp = bRestProvMatComp
    frmItemsWizard2.bPermProcMultiMaterial = bPermProcMultiMaterial
    g_bCancelar = False
    Me.Hide
    frmItemsWizard2.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "cmdContinuar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdSelUon_Click()
    Dim uons1 As CUnidadesOrgNivel1
    Dim uons2 As CUnidadesOrgNivel2
    Dim uons3 As CUnidadesOrgNivel3
    Dim oUon As IUOn
    
    Dim frmSELUO As frmSELUO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set frmSELUO = New frmSELUO
    frmSELUO.multiselect = True
    If m_oUonsSeleccionadas.Count > 0 Then
        Set frmSELUO.UonsSeleccionadas = m_oUonsSeleccionadas
    End If
    Set uons1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set uons2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set uons3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
    
    '''cargamos las unidades organizativas que se pueden usar
    Select Case g_oProcesoSeleccionado.DefDistribUON
        Case TipoDefinicionDatoProceso.EnProceso
            uons1.cargarTodasLasUnidadesDeDistribucionProceso g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod
            uons2.cargarTodasLasUnidadesDeDistribucionProceso g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod
            uons3.cargarTodasLasUnidadesDeDistribucionProceso g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod
        Case TipoDefinicionDatoProceso.EnGrupo
            'g_oGrupoSeleccionado.HayDistribucion no est� bien mantenida, s�lo es correcta cuando cargamos un proceso existente
            'as� que cargamos las unidades del grupo y si no hay ninguna es que no hab�a distribuci�n y tomamos s�lo las uon restringidas
            uons1.cargarTodasLasUnidadesDeDistribucionGrupo g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, g_oGrupoSeleccionado.Id
            uons2.cargarTodasLasUnidadesDeDistribucionGrupo g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, g_oGrupoSeleccionado.Id
            uons3.cargarTodasLasUnidadesDeDistribucionGrupo g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, g_oGrupoSeleccionado.Id
            If uons1.Count = 0 And uons2.Count = 0 And uons3.Count = 0 Then
                cargarUonsRestringidas uons1, uons2, uons3
            End If
        Case TipoDefinicionDatoProceso.EnItem
            'si no hay distribuci�n seleccionamos las uon con restricci�n de usuario
            cargarUonsRestringidas uons1, uons2, uons3
    End Select
    
    frmSELUO.cargarArbol uons1, uons2, uons3
    frmSELUO.Show vbModal
    If frmSELUO.Aceptar And frmSELUO.UonsSeleccionadas.Count <> 0 Then
        Set m_oUonsSeleccionadas = frmSELUO.UonsSeleccionadas
        Me.lstMultiUon.clear
        For Each oUon In m_oUonsSeleccionadas
            Me.lstMultiUon.AddItem oUon.titulo
        Next
        Me.lblUon.caption = frmSELUO.UonsSeleccionadas.titulo
    End If
    CargarAtributos
    Set uons1 = Nothing
    Set uons2 = Nothing
    Set uons3 = Nothing
    Set frmSELUO = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "cmdSelUon_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Initialize()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oUonsSeleccionadas = oFSGSRaiz.Generar_CUnidadesOrganizativas
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "Form_Initialize", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, True
m_bActivado = False
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
           
    Set oAtribs = oFSGSRaiz.Generar_CAtributos
    
    CargarRecursos
    ConfigurarSeguridad
    
    PonerFieldSeparator Me
    
    If Not frmPROCE.sdbgItems.Columns("ESP").Visible Then
        sdbgArticulos.Columns("INCESPEC").Visible = False
    Else
        If gParametrosInstalacion.giAnyadirEspecArticulo = PreguntarAdj Then
            sdbgArticulos.Columns("INCESPEC").Visible = True
        Else
            sdbgArticulos.Columns("INCESPEC").Visible = False
        End If
    End If

    If Not bPermProcMultiMaterial Then
        If frmPROCE.g_oProcesoSeleccionado.MaterialProce.Count = 1 Then
            
            Arrange
            CargarGrid
               
            m_sGMN = g_oGMN4Seleccionado.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(g_oGMN4Seleccionado.GMN1Cod))
            m_sGMN = m_sGMN & g_oGMN4Seleccionado.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(g_oGMN4Seleccionado.GMN2Cod))
            m_sGMN = m_sGMN & g_oGMN4Seleccionado.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(g_oGMN4Seleccionado.GMN3Cod))
            m_sGMN = m_sGMN & g_oGMN4Seleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(g_oGMN4Seleccionado.Cod))
        Else
            'Si el usuario esta viendo un proceso que se abrio multimaterial, podra a�adir items de las ramas de material que tenga el proceso.
            ssTabEst.Visible = True
        
            CargarEstructura True
            
            Set g_oItems = oFSGSRaiz.Generar_CItems
            ReDim g_vValores(0)
        End If
    Else
        'Si se permite abrir procesos multimaterial
        ssTabEst.Visible = True
        
        CargarEstructura False
        
    End If
    
    CargarComboOperandos
    sdbgAtributos.Columns("OPER").DropDownHwnd = sdbddOper.hWnd
    
    Set g_oItems = oFSGSRaiz.Generar_CItems
    ReDim g_vValores(0)
    
    g_bCancelar = True
    g_iReasignarPresup = 2
    g_bPresAnu1OK = False
    g_bPresAnu2OK = False
    g_bPres1OK = False
    g_bPres2OK = False
    
    CargarAtributos
    
    'mostrar/ocultar columnas referidas a articulos centrales en funci�n del par�metro general
    Me.chkBuscarCentrales.Visible = gParametrosGenerales.gbArticulosCentrales
    Me.sdbgArticulos.Columns("ART").Visible = gParametrosGenerales.gbArticulosCentrales
    Me.sdbgArticulos.Columns("AGREGADO").Visible = gParametrosGenerales.gbArticulosCentrales
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Public Sub ReasignarPresup(ByVal iTipo As Integer, ByVal bProce As Boolean)
Dim irespuesta As Integer
Dim dblPresNuevo As Double
Dim dPresAbierto As Double
Dim dPresAbiertoAnya As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmItemsWizard.g_iReasignarPresup = 2 Then
        irespuesta = oMensajes.PreguntaReasignarPresupuestos
        If irespuesta = vbYes Then
            frmItemsWizard.g_iReasignarPresup = 1
        Else
            frmItemsWizard.g_iReasignarPresup = 0
        End If
    End If
    
    If frmItemsWizard.g_iReasignarPresup = 1 Then

            'pantallas de presupuestos
            dblPresNuevo = frmItemsWizard.g_oItems.DevolverPresupuestoAAnyadir(frmItemsWizard.g_vValores(4), frmItemsWizard.g_vValores(6))
            If bProce Then
                dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                frmPRESAsig.g_iAmbitoPresup = 1
            Else
                dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                frmPRESAsig.g_iAmbitoPresup = 2
            End If
            dPresAbiertoAnya = dPresAbierto + dblPresNuevo
            frmPRESAsig.g_dblAbierto = dPresAbiertoAnya
            frmPRESAsig.g_dblAsignado = dPresAbierto
            frmPRESAsig.g_bModif = True
            'Si estamos a�adiendo un �tem a un grupo que no ten�a ning�n �tem hasta ahora
            'pero que s� ten�a distribuido alg�n presupuesto el importe asignado es 0 pero lo igualamos al abierto
            'para que no aparezca 100% pendiente con ramas asignadas marcadas en rojo.
            Select Case iTipo
                Case 1
                        If bProce Then
                            If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 Then
                                frmPRESAsig.g_bHayPres = True
                                If dPresAbierto = 0 Then
                                    frmPRESAsig.g_dblAsignado = frmPRESAsig.g_dblAbierto
                                End If
                            Else
                                frmPRESAsig.g_bHayPres = False
                            End If
                        Else
                            If frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 Then
                                frmPRESAsig.g_bHayPres = True
                                If dPresAbierto = 0 Then
                                    frmPRESAsig.g_dblAsignado = frmPRESAsig.g_dblAbierto
                                End If
                            Else
                                frmPRESAsig.g_bHayPres = False
                            End If
                        End If
                        frmPRESAsig.g_iTipoPres = 1
                        frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo1
                Case 2
                        If bProce Then
                            If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 Then
                                frmPRESAsig.g_bHayPres = True
                                If dPresAbierto = 0 Then
                                    frmPRESAsig.g_dblAsignado = frmPRESAsig.g_dblAbierto
                                End If
                            Else
                                frmPRESAsig.g_bHayPres = False
                            End If
                        Else
                            If frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 Then
                                frmPRESAsig.g_bHayPres = True
                                If dPresAbierto = 0 Then
                                    frmPRESAsig.g_dblAsignado = frmPRESAsig.g_dblAbierto
                                End If
                            Else
                                frmPRESAsig.g_bHayPres = False
                            End If
                        End If
                        frmPRESAsig.g_iTipoPres = 2
                        frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo2
                Case 3
                        If bProce Then
                            If frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                                frmPRESAsig.g_bHayPres = True
                                If dPresAbierto = 0 Then
                                    frmPRESAsig.g_dblAsignado = frmPRESAsig.g_dblAbierto
                                End If
                            Else
                                frmPRESAsig.g_bHayPres = False
                            End If
                        Else
                            If frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 Then
                                frmPRESAsig.g_bHayPres = True
                                If dPresAbierto = 0 Then
                                    frmPRESAsig.g_dblAsignado = frmPRESAsig.g_dblAbierto
                                End If
                             Else
                                frmPRESAsig.g_bHayPres = False
                             End If
                        End If
                        frmPRESAsig.g_iTipoPres = 3
                        frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo1
                Case 4
                        If bProce Then
                            If frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 Then
                                frmPRESAsig.g_bHayPres = True
                                If dPresAbierto = 0 Then
                                    frmPRESAsig.g_dblAsignado = frmPRESAsig.g_dblAbierto
                                End If
                             Else
                                frmPRESAsig.g_bHayPres = False
                             End If
                        Else
                            If frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 Then
                                frmPRESAsig.g_bHayPres = True
                                If dPresAbierto = 0 Then
                                    frmPRESAsig.g_dblAsignado = frmPRESAsig.g_dblAbierto
                                End If
                            Else
                                frmPRESAsig.g_bHayPres = False
                            End If
                        End If
                        frmPRESAsig.g_iTipoPres = 4
                        frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo2
            End Select
            
            frmPRESAsig.g_bHayPresBajaLog = False
            frmPRESAsig.g_sOrigen = "ANYA_ITEMS"
            frmPRESAsig.Show 1
            
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "ReasignarPresup", err, Erl, , m_bActivado)
      Exit Sub
   End If
        
End Sub

Private Sub CargarGrid()
Dim oArticulo As CArticulo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgArticulos.RemoveAll
    
    For Each oArticulo In g_oGMN4Seleccionado.ARTICULOS
        Dim strLinea As String
        strLinea = oArticulo.Cod & Chr(m_lSeparador)
        strLinea = strLinea & oArticulo.Den & Chr(m_lSeparador)
        strLinea = strLinea & oArticulo.CodigoUnidad & Chr(m_lSeparador)
        strLinea = strLinea & oArticulo.Cantidad & Chr(m_lSeparador)
        strLinea = strLinea & oArticulo.NombreUON & Chr(m_lSeparador)
        strLinea = strLinea & oArticulo.CodArticuloCentral & Chr(m_lSeparador)
        strLinea = strLinea & BooleanToSQLBinary(oArticulo.isCentral) & Chr(m_lSeparador)
        strLinea = strLinea & "" & Chr(m_lSeparador)
        strLinea = strLinea & "" & Chr(m_lSeparador)
        strLinea = strLinea & IIf(gParametrosInstalacion.giAnyadirEspecArticulo = Adjuntar And oArticulo.EspAdj = 1, "-1", 0) & Chr(m_lSeparador)
        strLinea = strLinea & oArticulo.EspAdj & Chr(m_lSeparador)
        strLinea = strLinea & IIf(oArticulo.ConAdjsAnteriores, 1, 0) & Chr(m_lSeparador)
        strLinea = strLinea & BooleanToSQLBinary(oArticulo.Generico)
        
        sdbgArticulos.AddItem strLinea
    Next
    
    Set oArticulo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "CargarGrid", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Resize()

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub

Private Sub Form_Terminate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oUonsSeleccionadas = Nothing
    Set oAtribs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "Form_Terminate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If g_bCancelar Then
        Set g_oGMN4Seleccionado = Nothing
        Set g_oItems = Nothing
        g_vValores = Null
        Set g_oPresupuestos1Nivel4 = Nothing
        Set g_oPresupuestos2Nivel4 = Nothing
        Set g_oPresupuestos3Nivel4 = Nothing
        Set g_oPresupuestos4Nivel4 = Nothing
        Set g_oDistsNivel1 = Nothing
        Set g_oDistsNivel2 = Nothing
        Set g_oDistsNivel3 = Nothing
        g_iReasignarPresup = 2
        g_sDistrib = ""
        m_sGMN = ""
        m_sIdiEspec = ""
        g_bRDest = False
        g_bPresAnu1OK = False
        g_bPresAnu2OK = False
        g_bPres1OK = False
        g_bPres2OK = False

    End If
    m_oUonsSeleccionadas.clear
    Set oAtribs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub lblUon_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oUonsSeleccionadas.Count > 1 Then
        lstMultiUon.Height = m_oUonsSeleccionadas.Count * (lblUon.Height - 30)
        lstMultiUon.Visible = True
        lstMultiUon.ZOrder 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "lblUon_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub lstMultiUon_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lWnd As Long
    
    'El c�digo siguiente imita el MouseLeave
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lWnd = GetCapture

    If lWnd <> lstMultiUon.hWnd Then
        'Dentro de lstMultiMaterial
        SetCapture lstMultiUon.hWnd
    Else
        If (X < 0) Or (X > lstMultiUon.Width) Or _
            (Y < 0) Or (Y > lstMultiUon.Height) Then
            'Fuera de lstMultiMaterial
            ReleaseCapture
            lstMultiUon.Visible = False
            lstMultiUon.ZOrder 1
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "lstMultiUon_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub picCriterios_Resize()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAtributos.Width = picCriterios.Width - (2 * cnSeparacion)
    sdbgAtributos.Height = picCriterios.Height - sdbgAtributos.Top - fraChecks.Height - 3 * cnSeparacion
    cmdBuscaAtrib.Left = sdbgAtributos.Width - cmdBuscaAtrib.Width - 50
    fraChecks.Top = picCriterios.Height - fraChecks.Height - (2 * cnSeparacion)
    
    sdbgAtributos.Columns("USAR").Width = 600
    sdbgAtributos.Columns("COD").Width = (sdbgAtributos.Width - 1200) * 0.22
    sdbgAtributos.Columns("DEN").Width = (sdbgAtributos.Width - 1200) * 0.42
    sdbgAtributos.Columns("OPER").Width = 600
    sdbgAtributos.Columns("VALOR").Width = (sdbgAtributos.Width - 1200) * 0.33
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "picCriterios_Resize", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub CargarComboOperandos()
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    arOper = Array("=", ">", ">=", "<", "<=")
    
    sdbddOper.RemoveAll
    
    sdbddOper.AddItem ""
    For i = 0 To UBound(arOper)
        sdbddOper.AddItem arOper(i) & Chr(m_lSeparador) & arOper(i)
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "CargarComboOperandos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbddOper_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddOper.DataFieldList = "Column 0"
    sdbddOper.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbddOper_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddOper_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
   
    sdbddOper.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOper.Rows - 1
            bm = sdbddOper.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.col).Value = Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))
                sdbddOper.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbddOper_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddOper_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        
        ''' Comprobar la existencia en la lista
        For i = 0 To UBound(arOper)
            If arOper(i) = sdbgAtributos.Columns(sdbgAtributos.col).Text Then
                bExiste = True
                Exit For
            End If
        Next
        
        If Not bExiste Then
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.col).Text
            sdbgAtributos.Columns(sdbgAtributos.col).Text = ""
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbddOper_ValidateList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddValor_DropDown()
    Dim oatrib As Catributo
    Dim iIdAtrib As Integer
    Dim oLista As CValoresPond
    Dim oElem As CValorPond

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddValor.RemoveAll
    
    sdbddValor.AddItem ""

    iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

    Set oatrib = oFSGSRaiz.Generar_CAtributo
    Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)

    If Not oatrib Is Nothing Then
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            oatrib.CargarListaDeValores
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                sdbddValor.AddItem oElem.ValorLista & Chr(m_lSeparador) & oElem.ValorLista
            Next
            Set oLista = Nothing
        Else
            'Campo de tipo boolean:
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                sdbddValor.AddItem msTrue & Chr(m_lSeparador) & msTrue
                sdbddValor.AddItem msFalse & Chr(m_lSeparador) & msFalse
            End If
        End If
    
        Set oatrib = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbddValor_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddValor_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbddValor_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.col).Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbddValor_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As Catributo
    Dim iIdAtrib As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

            Set oatrib = oFSGSRaiz.Generar_CAtributo
            Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)
            
            If oatrib Is Nothing Then Exit Sub
            
            oatrib.CargarListaDeValores
            
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                If UCase(msTrue) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "1"
                    bExiste = True
                End If
                If UCase(msFalse) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "0"
                    bExiste = True
                End If
                
            End If
        End If
        If Not bExiste Then
            sdbgAtributos.Columns(sdbgAtributos.col).Text = ""
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.col).caption
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbddValor_ValidateList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbgArticulos_BtnClick()
Dim teserror As TipoErrorSummit
Dim sCod As String
Dim oArticulo As CArticulo
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgArticulos.col = -1 Then Exit Sub

'    sdbgArticulos.SelBookmarks.Add sdbgArticulos.Bookmark
    If Not g_oGMN4Seleccionado Is Nothing Then
    
        m_sGMN = g_oGMN4Seleccionado.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(g_oGMN4Seleccionado.GMN1Cod))
        m_sGMN = m_sGMN & g_oGMN4Seleccionado.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(g_oGMN4Seleccionado.GMN2Cod))
        m_sGMN = m_sGMN & g_oGMN4Seleccionado.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(g_oGMN4Seleccionado.GMN3Cod))
        m_sGMN = m_sGMN & g_oGMN4Seleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(g_oGMN4Seleccionado.Cod))

    End If

    sCod = m_sGMN & sdbgArticulos.Columns("COD").Value
    
    Select Case sdbgArticulos.Columns(sdbgArticulos.col).Name

        Case "ADJ"

                'Comprobamos si existen adjudicaciones de summit

                Set frmESTRMATProve.oAdjudicaciones = oFSGSRaiz.Generar_CAdjsDeArt

                Set frmESTRMATProve.oArticulo = Nothing
                Set frmESTRMATProve.oArticulo = g_oGMN4Seleccionado.ARTICULOS.Item(sdbgArticulos.Columns("COD").Value)

                If frmESTRMATProve.oArticulo Is Nothing Then Exit Sub

                frmESTRMATProve.oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorFecFin, frmESTRMATProve.oArticulo, False, True

                frmESTRMATProve.Show 1

        Case "ESPEC" 'Especificaciones

                frmArticuloEsp.g_sOrigen = "frmItemsWizard"
                frmArticuloEsp.txtArticuloEsp.Locked = True
                frmArticuloEsp.cmdA�adirEsp.Enabled = False
                frmArticuloEsp.cmdEliminarEsp.Enabled = False
                frmArticuloEsp.cmdModificarEsp.Enabled = False
                frmArticuloEsp.cmdSalvarEsp.Enabled = True
                frmArticuloEsp.cmdAbrirEsp.Enabled = True
                frmArticuloEsp.cmdRestaurar.Visible = False
                
                Set frmArticuloEsp.g_oArticulo = g_oGMN4Seleccionado.ARTICULOS.Item(sdbgArticulos.Columns("COD").Value)

                If frmArticuloEsp.g_oArticulo Is Nothing Then Exit Sub

                frmArticuloEsp.g_oArticulo.CargarTodasLasEspecificaciones

                If Not sdbgArticulos.IsAddRow Then

                    'Configurar parametros del commondialog
                    frmArticuloEsp.cmmdEsp.FLAGS = cdlOFNHideReadOnly
                    frmArticuloEsp.caption = m_sIdiEspec & " " & sdbgArticulos.Columns("COD").Value & " " & sdbgArticulos.Columns("DEN").Value

                    Set frmArticuloEsp.g_oIBaseDatos = frmArticuloEsp.g_oArticulo
                    teserror = frmArticuloEsp.g_oIBaseDatos.IniciarEdicion

                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set frmArticuloEsp.g_oIBaseDatos = Nothing
                        Set frmArticuloEsp.g_oArticulo = Nothing
                        Exit Sub
                    End If

                    frmArticuloEsp.g_oIBaseDatos.CancelarEdicion
                    frmArticuloEsp.g_bRespetarCombo = True
                    frmArticuloEsp.txtArticuloEsp.Text = NullToStr(frmArticuloEsp.g_oArticulo.esp)
                    frmArticuloEsp.g_bRespetarCombo = False
                    frmArticuloEsp.AnyadirEspsALista
                    frmArticuloEsp.Show 1
                    If Not g_oGMN4Seleccionado Is Nothing Then
                        If Not g_oGMN4Seleccionado.ARTICULOS.Item(sCod) Is Nothing Then
                            If g_oGMN4Seleccionado.ARTICULOS.Item(sCod).EspAdj = 1 Then
                                sdbgArticulos.Columns("ESPEC").CellStyleSet "styEspAdjSi"
                            Else
                                sdbgArticulos.Columns("ESPEC").CellStyleSet ""
                            End If
                            sdbgArticulos.Refresh
                        End If
                    End If
                End If
            Case "UON_DEN"
                    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
                    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
                    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
                    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
                    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
                    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
                    
                    oUnidadesOrgN1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.perfil.Id
                    oUnidadesOrgN2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.perfil.Id
                    oUnidadesOrgN3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.perfil.Id
                    
                    Dim frm As frmSELUO
                    Set frm = New frmSELUO
                    frm.multiselect = True
                    frm.EnableCheck = False
                    frm.cargarArbol oUnidadesOrgN1, oUnidadesOrgN2, oUnidadesOrgN3
                    
                    
                    Set oArticulo = g_oGMN4Seleccionado.ARTICULOS.Item(sdbgArticulos.Columns("COD").Value)
                    
                    frm.subtitulo = oArticulo.CodDen
                                        
                    
                    Set frm.UonsSeleccionadas = oArticulo.uons
                    
                    frm.Show vbModal
                
                    Set frm = Nothing
            Case "AGREGADO"
                    
                    
                    Set oArticulo = g_oGMN4Seleccionado.ARTICULOS.Item(sdbgArticulos.Columns("COD").Value)
                    If oArticulo.isCentral Then
                        frmArtCentralUnifConfirm.ModoEdicion = False
                        frmArtCentralUnifConfirm.enableCancel = False
                        Set frmArtCentralUnifConfirm.ArticuloCentral = oArticulo
                        frmArtCentralUnifConfirm.Show vbModal
                    End If

    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbgArticulos_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbgArticulos_Change()
Dim bk As Variant
Dim bAdjuntar As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_aIdentificadores Is Nothing Then
        If GridCheckToBoolean(sdbgArticulos.Columns("ESPECADJ").Value) = False Then
            sdbgArticulos.Columns("INCESPEC").Value = "0"
        End If
        sdbgArticulos.Update
    
    Else
        If GridCheckToBoolean(sdbgArticulos.Columns("INCESPEC").Value) = False Then
            bAdjuntar = False
        Else
            bAdjuntar = True
        End If
        
        For Each bk In m_aIdentificadores
            sdbgArticulos.Bookmark = bk
            
            If bAdjuntar = True Then
                If GridCheckToBoolean(sdbgArticulos.Columns("ESPECADJ").Value) = False Then
                    sdbgArticulos.Columns("INCESPEC").Value = "0"
                Else
                    sdbgArticulos.Columns("INCESPEC").Value = "-1"
                End If
            Else
                sdbgArticulos.Columns("INCESPEC").Value = "0"
            End If
        Next
        sdbgArticulos.Update
        Set m_aIdentificadores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbgArticulos_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbgArticulos_Click()
''******************************************************************
''*   Guardamos los items que se han seleccionado en una coleccion
''*     para tratar las modificaciones sobre ellos en conjunto
''*
''*******************************************************************
Dim bk As Variant
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If gParametrosInstalacion.giAnyadirEspecArticulo <> PreguntarAdj Then Exit Sub
    
    If sdbgArticulos.col = -1 Then
        If sdbgArticulos.SelBookmarks.Count > 0 Then
            Set m_aIdentificadores = New Collection
            
            For Each bk In sdbgArticulos.SelBookmarks
                m_aIdentificadores.Add bk
            Next
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbgArticulos_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgArticulos_HeadClick(ByVal ColIndex As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    Select Case Me.sdbgArticulos.Columns(ColIndex).Name
    Case "COD"
        m_sOrdenListado = ""
    Case "DEN"
        m_sOrdenListado = "ART_DEN"
    Case "UNI"
        m_sOrdenListado = "ART_UNI"
    Case "CANT"
        m_sOrdenListado = "ART_CANT"
    Case "UON_DEN"
        m_sOrdenListado = "UON_DEN"
    Case "ART"
        m_sOrdenListado = "ART"
    Case Else
        m_sOrdenListado = ""
    End Select
    m_sOrderAscDesc = IIf(m_sOrderAscDesc = "", "DESC", "")
    
    CargarArticulos
    
    CargarGrid
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbgArticulos_HeadClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgArticulos_RowLoaded(ByVal Bookmark As Variant)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsNull(Bookmark) And Not IsEmpty(Bookmark) Then
        If sdbgArticulos.Columns("NUMADJ").Value = "1" Then
            sdbgArticulos.Columns("ADJ").CellStyleSet "Adjudica"
        Else
            sdbgArticulos.Columns("ADJ").CellStyleSet ""
        End If
        If sdbgArticulos.Columns("AGREGADO").Value = "1" Then
            sdbgArticulos.Columns("AGREGADO").CellStyleSet "Adjudica"
            sdbgArticulos.Columns("AGREGADO").Text = ""
        Else
            sdbgArticulos.Columns("AGREGADO").CellStyleSet ""
            sdbgArticulos.Columns("AGREGADO").Text = ""
        End If
        If sdbgArticulos.Columns("ESPECADJ").Value = "1" Then
            sdbgArticulos.Columns("ESPEC").CellStyleSet "styEspAdjSi"
        Else
            sdbgArticulos.Columns("ESPEC").CellStyleSet ""
        End If
        If sdbgArticulos.Columns("UON_DEN").Value = "Multiples*" Then
            sdbgArticulos.Columns("UON_DEN").Value = m_sVariasUnidades
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbgArticulos_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' A�ade multiples items a un grupo del proceso
''' </summary>
''' <param name="bCAntDef">Cantidad por defecto </param>
''' <param name="bPresAdj">PresupuestoAdjudicado</param>
''' <param name="bUON">Unidad organizativa</param>
''' <param name="bPresup">presupuesto</param>
''' <param name="bProveAdj">Proveedor Adjudicado</param>
''' <param name="bPresPlanif">Presup planificado</param>
''' <returns>Si hay Error</returns>
''' <remarks>Llamada desde: frmitemswizard2.cmdFinalizar,frmitemswizard3.cmdFinalizar,frmitemswizard4.cmdFinalizar Tiempo m�ximo:0 </remarks>

Public Function AnyadirMultiplesItems(Optional ByVal bCantDef As Boolean, Optional ByVal bPresAdj As Boolean, Optional ByVal bUON As Boolean, Optional ByVal bPresup As Boolean, Optional ByVal bProveAdj As Boolean, Optional ByVal bPresPlanif As Boolean) As TipoErrorSummit
Dim teserror As TipoErrorSummit
Dim oItem As CItem

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    If frmPROCE.m_bAtribItemPlant Then
        teserror = g_oItems.AnyadirMultiplesItems(bCantDef, bPresAdj, bUON, g_oDistsNivel1, g_oDistsNivel2, g_oDistsNivel3, bPresup, g_oPresupuestos1Nivel4, g_oPresupuestos2Nivel4, g_oPresupuestos3Nivel4, g_oPresupuestos4Nivel4, bProveAdj, bPresPlanif, basOptimizacion.gvarCodUsuario, bPermProcMultiMaterial, , True, frmPROCE.m_oPlantillaSeleccionada.AtributosEspecificacion)
        frmPROCE.m_bAtribItemPlant = False
    Else
        teserror = g_oItems.AnyadirMultiplesItems(bCantDef, bPresAdj, bUON, g_oDistsNivel1, g_oDistsNivel2, g_oDistsNivel3, bPresup, g_oPresupuestos1Nivel4, g_oPresupuestos2Nivel4, g_oPresupuestos3Nivel4, g_oPresupuestos4Nivel4, bProveAdj, bPresPlanif, basOptimizacion.gvarCodUsuario, bPermProcMultiMaterial, , False)
    End If
    
    If teserror.NumError = TESnoerror Or (teserror.NumError = TESVolumenAdjDirSuperado And frmPROCE.g_oProcesoSeleccionado.PermSaltarseVolMaxAdjDir) Then
    Dim procesarItem As Boolean
        For Each oItem In g_oItems
            procesarItem = True
            If gParametrosGenerales.gbOBLUnidadMedida And oItem.proceso.Grupos.Item(oItem.grupoCod).UsarEscalados Then
                If oItem.proceso.Grupos.Item(oItem.grupoCod).UnidadEscalado <> oItem.UniCod Then
                    procesarItem = False
                End If
            End If
            If procesarItem Then
                frmPROCE.g_oGrupoSeleccionado.Items.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , oItem.EspAdj, , , oItem.ProveAct, , oItem.grupoCod, , oItem.SolicitudId, oItem.MinPujGanador, , oItem.Generico, oAtributosEspecificacion:=oItem.AtributosEspecificacion, lGrupoID:=oItem.GrupoID
            End If
        Next
    End If
    AnyadirMultiplesItems = teserror
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "AnyadirMultiplesItems", err, Erl, , m_bActivado)
      Exit Function
   End If
    
End Function


Private Sub CargarEstructuraMateriales()

    Dim oGrupsMN1 As CGruposMatNivel1
    Dim oGrupsMN4 As CGruposMatNivel4

    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    Dim oIMaterialComprador As IMaterialAsignado

    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String

    Dim nodx As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    tvwEstMat.Nodes.clear

    Set nodx = tvwEstMat.Nodes.Add(, , "Raiz", sEstructura, "Raiz")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    nodx.Selected = True

    If bRMatComprador Then
        'Restringir la apertura de items al material del comprador
        Set oIMaterialComprador = oUsuarioSummit.comprador
        If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm
        With oIMaterialComprador

            If oPlantilla Is Nothing Then
                Set oGrupsMN4 = oIMaterialComprador.DevolverGruposMN4Asignados(, , , , False, , , , basOptimizacion.gCodPersonaUsuario)
            Else
                Set oGrupsMN4 = oIMaterialComprador.DevolverGruposMN4Asignados(, , , , False, , , , basOptimizacion.gCodPersonaUsuario, oPlantilla.Id)
            End If

            If Not oGrupsMN4 Is Nothing Then
                
                For Each oGMN4 In oGrupsMN4
                    scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
                    scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
                    scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
                    scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                    
                    If Not ExisteNodo("GMN1" & scod1) Then
                        Set nodx = tvwEstMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN4.GMN1Cod & " - " & oGMN4.GMN1Den, "GMN1")
                        nodx.Tag = "GMN1" & oGMN4.GMN1Cod ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                    End If
                    
                    If Not ExisteNodo("GMN2" & scod1 & scod2) Then
                        Set nodx = tvwEstMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN4.GMN2Cod & " - " & oGMN4.GMN2Den, "GMN2")
                        nodx.Tag = "GMN2" & oGMN4.GMN2Cod ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                    End If
                    
                    If Not ExisteNodo("GMN3" & scod1 & scod2 & scod3) Then
                        Set nodx = tvwEstMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN4.GMN3Cod & " - " & oGMN4.GMN3Den, "GMN3")
                        nodx.Tag = "GMN3" & oGMN4.GMN3Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    End If
                        
                    Set nodx = tvwEstMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                    nodx.Tag = "GMN4" & oGMN4.Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    If Not g_oGMN4Seleccionado Is Nothing Then
                        If g_oGMN4Seleccionado.GMN1Cod = oGMN4.GMN1Cod And g_oGMN4Seleccionado.GMN2Cod = oGMN4.GMN2Cod And g_oGMN4Seleccionado.GMN3Cod = oGMN4.GMN3Cod And g_oGMN4Seleccionado.Cod = oGMN4.Cod Then
                            nodx.Parent.Parent.Parent.Expanded = True
                            nodx.Parent.Parent.Expanded = True
                            nodx.Parent.Expanded = True
                            nodx.Selected = True
                        End If
                    End If
                Next
                 'Si solo tiene un material  lo mostramos expandido
                If tvwEstMat.Nodes.Count = 5 Then
                    tvwEstMat.Nodes(1).Expanded = True
                    tvwEstMat.Nodes(1).Child.Expanded = True
                    tvwEstMat.Nodes(1).Child.Child.Expanded = True
                    tvwEstMat.Nodes(1).Child.Child.Child.Expanded = True
                    Set tvwEstMat.selectedItem = tvwEstMat.Nodes(1).Child.Child.Child.Child
                    tvwEstMat_NodeClick tvwEstMat.selectedItem
               End If
            End If

         End With

    Else 'No hay restriccion de comprador

        Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1

        If oPlantilla Is Nothing Then
            oGrupsMN1.GenerarEstructuraMateriales False
        Else
            oGrupsMN1.GenerarEstructuraMateriales False, oPlantilla.Id
        End If
        
        For Each oGMN1 In oGrupsMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
            nodx.Tag = "GMN1" & oGMN1.Cod

            For Each oGMN2 In oGMN1.GruposMatNivel2
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                nodx.Tag = "GMN2" & oGMN2.Cod

                For Each oGMN3 In oGMN2.GruposMatNivel3
                    scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                    Set nodx = tvwEstMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                    nodx.Tag = "GMN3" & oGMN3.Cod

                    For Each oGMN4 In oGMN3.GruposMatNivel4
                        scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                        Set nodx = tvwEstMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                        nodx.Tag = "GMN4" & oGMN4.Cod
                        If Not g_oGMN4Seleccionado Is Nothing Then
                            If g_oGMN4Seleccionado.GMN1Cod = oGMN1.Cod And g_oGMN4Seleccionado.GMN2Cod = oGMN2.Cod And g_oGMN4Seleccionado.GMN3Cod = oGMN3.Cod And g_oGMN4Seleccionado.Cod = oGMN4.Cod Then
                                nodx.Parent.Parent.Parent.Expanded = True
                                nodx.Parent.Parent.Expanded = True
                                nodx.Parent.Expanded = True
                                nodx.Expanded = True
                                nodx.Selected = True
                            End If
                        End If
                    Next
                Next
            Next
        Next
    End If
    Set oGrupsMN1 = Nothing
    Exit Sub

ERROR_Frm:
        Set nodx = Nothing
        Resume Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "CargarEstructuraMateriales", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Sub
   End If

End Sub

Private Sub sdbgatributos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgAtributos
        Select Case .Columns(ColIndex).Name
            Case "VALOR"
                'Comprobar que el valor introducido se corresponde con el tipo de atributo
        If .Columns("VALOR").Text <> "" Then
            Select Case UCase(.Columns("IDTIPO").Text)
                Case TiposDeAtributos.TipoString
                Case TiposDeAtributos.TipoNumerico
                    If (Not IsNumeric(.Columns("VALOR").Value)) Then
                        MsgBox "El valor del campo Valor debe ser num�rico", vbInformation, "FULLSTEP"
                        Cancel = True
                    Else
                        If .Columns("OPER").Text <> "" And .Columns("OPER").Text = "=" Then
                            If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                            End If
                        End If
                    End If
                    
                    If .Columns("OPER").Text = "" Then
                        .Columns("OPER").Text = "="
                        If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                            Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                        End If
                    End If
                Case TiposDeAtributos.TipoFecha
                    If (Not IsDate(.Columns("VALOR").Text) And .Columns("VALOR").Value <> "") Then
                        MsgBox "El valor del campo Valor debe ser una fecha.", vbInformation, "FULLSTEP"
                        Cancel = True
                    Else
                        If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                            If CDate(.Columns("MINIMO").Value) > CDate(.Columns("VALOR").Value) Or CDate(.Columns("MAXIMO").Value) < CDate(.Columns("VALOR").Value) Then
                                MsgBox Replace(Replace(msMsgMinMax, "@Valor1", .Columns("MINIMO").Value), "@Valor2", .Columns("MAXIMO").Value), vbInformation, "FULLSTEP"
                                Cancel = True
                            End If
                        End If
                    End If
                Case TiposDeAtributos.TipoBoolean
            End Select
        End If
            Case "OPER"
                'Comprobar si se est� intentando dejar en blanco la col. del operando teniendo un valor introducido
                If .Columns("OPER").Text = vbNullString And .Columns("VALOR").Text <> vbNullString Then
                    Cancel = True
                ElseIf .Columns("OPER").Text = "=" And .Columns("VALOR").Text <> vbNullString Then
                    Select Case UCase(.Columns("IDTIPO").Text)
                        Case TiposDeAtributos.TipoNumerico
                            If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                            End If
                    End Select
                End If
        End Select
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbgatributos_BeforeColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Comprueba que un valor est� entre un m�nimo y un m�ximo</summary>
''' <remarks>Llamada desde: sdbgatributos_BeforeColUpdate</remarks>
''' <param name="dblValor">Valor</param>
''' <param name="dblMin">M�nimo</param>
''' <param name="dblMax">M�ximo</param>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Public Function ComprobarValoresMaxMin(ByVal dblValor As Double, ByVal dblMin As Double, ByVal dblMax As Double) As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ComprobarValoresMaxMin = True
    
    If dblMin > dblValor Or dblMax < dblValor Then
        MsgBox Replace(Replace(msMsgMinMax, "@Valor1", dblMin), "@Valor2", dblMax), vbInformation, "FULLSTEP"
        ComprobarValoresMaxMin = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "ComprobarValoresMaxMin", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bCargarCombo As Boolean
    
    'Combo de operandos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgAtributos.Columns("IDTIPO").Value = TipoNumerico Then
        sdbgAtributos.Columns("OPER").DropDownHwnd = sdbddOper.hWnd
        sdbgAtributos.Columns("OPER").Locked = False
    Else
        sdbgAtributos.Columns("OPER").DropDownHwnd = 0
        sdbgAtributos.Columns("OPER").Locked = True
    End If
    
    'Combo de valores
    bCargarCombo = False
    If sdbgAtributos.col = sdbgAtributos.Columns("VALOR").Position Then
        If sdbgAtributos.Columns("INTRO").Value Then
            bCargarCombo = True
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TipoBoolean Then
                bCargarCombo = True
            End If
        End If
    End If
    
    If bCargarCombo Then
        sdbddValor.RemoveAll
        sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
        sdbddValor.Enabled = True
        sdbddValor_DropDown
    Else
        sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "sdbgAtributos_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ssTabEst_Click(PreviousTab As Integer)
    Dim node As MSComctlLib.node
    
    Dim bCargarArt As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    Select Case ssTabEst.Tab
        Case 0
            'edu T98
            Arrange
    
                If bPermProcMultiMaterial Then
                    CargarEstructuraMateriales
                Else
                    If frmPROCE.g_oProcesoSeleccionado.MaterialProce.Count > 1 Then
                        If bRMatComprador Then
                            CargarEstructuraMateriales
                        Else
                            CargarEstructuraMaterialesProceso
                        End If
                    End If
                End If
        Case 1
            Arrange
        Case 2
            If Not bPermProcMultiMaterial And frmPROCE.g_oProcesoSeleccionado.MaterialProce.Count = 1 Then
                Arrange
                CargarArticulos
                CargarGrid
            Else
                bCargarArt = False
                
                Set node = tvwEstMat.selectedItem
                If node Is Nothing Then
                    If Not g_oGMN4Seleccionado Is Nothing Then bCargarArt = True
                Else
                    If Left(node.Tag, 4) = "GMN4" Then
                        bCargarArt = True
                    Else
                        oMensajes.SeleccioneMatNivel4
                        ssTabEst.Tab = 0
                    End If
                End If
                
                If bCargarArt Then
                    Arrange
                    CargarArticulos
                    CargarGrid
                End If
            End If
    End Select
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "ssTabEst_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Carga los art�culos en g_oGMN4Seleccionado seg�n los criterios establecidos</summary>
''' <remarks>Llamada desde: ssTabEst_Click</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub CargarArticulos(Optional ByVal orderColumn As String)
    Dim arConcepto(2) As Boolean
    Dim arAlmacen(2) As Boolean
    Dim arRecep(2) As Boolean
    Dim i As Integer
    Dim iNumAtrib As Integer
    Dim oAtributos As CAtributos
    Dim o As Catributo
    
    'Preparar los checks de los conceptos en arrays
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For i = 0 To 2
        If chkConcepto(i).Value = vbChecked Then arConcepto(i) = True
        If chkAlmacen(i).Value = vbChecked Then arAlmacen(i) = True
        If chkRecep(i).Value = vbChecked Then arRecep(i) = True
    Next
    
    'Atributos
    If sdbgAtributos.Rows > 0 Then
        Set oAtributos = oFSGSRaiz.Generar_CAtributos
        
        With sdbgAtributos
            iNumAtrib = 0
            Do While iNumAtrib < .Rows
                .Bookmark = iNumAtrib
                If .Columns("USAR").Value Then
                    If .Columns("VALOR").Value <> "" Then
                        Select Case .Columns("IDTIPO").Value
                            Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_text:=Replace(.Columns("VALOR").Value, "*", "%"))
                            Case TiposDeAtributos.TipoNumerico
                                Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_num:=.Columns("VALOR").Value, Formula:=.Columns("OPER").Value)
                            Case TiposDeAtributos.TipoFecha
                                Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_fec:=.Columns("VALOR").Value)
                            Case TiposDeAtributos.TipoBoolean
                                Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_bool:=.Columns("VALOR_ATRIB").Value)
                        End Select
                    Else
                        Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value)
                    End If
                    o.AmbitoAtributo = oAtribs.Item(.Columns("ID_ATRIB").Value).AmbitoAtributo
                End If
                iNumAtrib = iNumAtrib + 1
            Loop
        End With
    End If
    
    Dim ouons1 As CUnidadesOrgNivel1
    Dim ouons2 As CUnidadesOrgNivel2
    Dim ouons3 As CUnidadesOrgNivel3
    
    Set ouons1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set ouons2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set ouons3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
    
    '''obtenemos las unidades sobre las que hay que filtrar
    Dim oUonsDistribuidas As CUnidadesOrganizativas
    '''comprobar si la distribuci�n ya est� lista
    If m_oUonsSeleccionadas.Count > 0 Then
        Set oUonsDistribuidas = m_oUonsSeleccionadas
    Else
        Set oUonsDistribuidas = oFSGSRaiz.Generar_CUnidadesOrganizativas
        '''comprobar si la distribuci�n ya est� lista
        '''En caso de no estarlo restringimos a las unidades organizativas disponibles en la apertura de proceso
        ''' si no est� distribuido
        Select Case g_oProcesoSeleccionado.DefDistribUON
            Case TipoDefinicionDatoProceso.EnProceso
                oUonsDistribuidas.cargarTodasLasUnidadesDistribuidasNivelProceso g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod
            Case TipoDefinicionDatoProceso.EnGrupo
                If g_oGrupoSeleccionado.HayDistribucionUON Then
                    oUonsDistribuidas.cargarTodasLasUnidadesDistribuidasNivelGrupo g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, g_oGrupoSeleccionado.Id
                Else
                    oUonsDistribuidas.cargarTodasLasUnidadesUsuPerf oUsuarioSummit, m_bRestrDistrUsu, m_bRestrDistrPerf, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
                End If
            Case TipoDefinicionDatoProceso.EnItem
                If Not g_oItemSeleccionado Is Nothing Then
                    oUonsDistribuidas.cargarTodasLasUnidadesDistribuidasNivelItem g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, g_oItemSeleccionado.Id
                Else
                    oUonsDistribuidas.cargarTodasLasUnidadesUsuPerf oUsuarioSummit, m_bRestrDistrUsu, m_bRestrDistrPerf, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
                End If
        End Select

    End If
       
    '''si lo est�, restringimos listado art�culos a aquellas uons de la distribuci�n o la selecci�n
    g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , , , False, _
                    True, , , True, , , , , , , , , , chkBuscarGenericos.Value, arConcepto, arAlmacen, arRecep, oAtributos, , , , , , , , True, True, , , Me.chkBuscarCentrales.Value, m_sOrdenListado, m_sOrderAscDesc, oUonsDistribuidas, True, , True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "CargarArticulos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Carga el grid de atributos</summary>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub CargarAtributos()
    Dim oatrib As Catributo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAtributos.RemoveAll
    
    'Cargar atributos de grupo
    If Not g_oGrupoSeleccionado.AtributosDefinicionEspecItem Is Nothing Then
        For Each oatrib In g_oGrupoSeleccionado.AtributosDefinicionEspecItem
            sdbgAtributos.AddItem "0" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo
        Next
        
        Set oatrib = Nothing
    End If
    
    'Cargar atributos de material
    CargarAtributosMaterial
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "CargarAtributos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Carga el grid de atributos con los atributos de material</summary>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub CargarAtributosMaterial()
    Dim oatrib As Catributo
    Dim oAtribGrupo As Catributo
    Dim oGMN4 As CGrupoMatNivel4
    Dim bAddAtrib As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oGMN4Seleccionado Is Nothing Then
        Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
        oGMN4.GMN1Cod = g_oGMN4Seleccionado.GMN1Cod
        oGMN4.GMN2Cod = g_oGMN4Seleccionado.GMN2Cod
        oGMN4.GMN3Cod = g_oGMN4Seleccionado.GMN3Cod
        oGMN4.Cod = g_oGMN4Seleccionado.Cod
        
        If m_oUonsSeleccionadas.Count > 0 Then
            Set oAtribs = oGMN4.DevolverAtribMatYArtMat(oUons:=m_oUonsSeleccionadas)
        Else
            Set oAtribs = oGMN4.DevolverAtribMatYArtMat
        End If
        If Not oAtribs Is Nothing Then
            If oAtribs.Count > 0 Then
                With sdbgAtributos
                    For Each oatrib In oAtribs
                        'Comprobar que no existe ya
                        bAddAtrib = True
                
                        If Not g_oGrupoSeleccionado.AtributosDefinicionEspecItem Is Nothing Then
                            For Each oAtribGrupo In g_oGrupoSeleccionado.AtributosDefinicionEspecItem
                                If oatrib.Id = CLng(oAtribGrupo.Atrib) Then
                                    bAddAtrib = False
                                    Exit For
                                End If
                            Next
                        End If
                        
                        If bAddAtrib Then
                            .AddItem "0" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo
                        End If
                    Next
                End With
            End If
        End If
    End If
    
    Set oGMN4 = Nothing
    Set oatrib = Nothing
    Set oAtribGrupo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "CargarAtributosMaterial", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Elimina del grid de atributos los atributos de material</summary>
''' <remarks>Llamada desde: cmdSelMat_Click, cmdBorrar_Click </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub VaciarAtributosMaterial()
'Jim : Borro todos los atributos , para evitar posibles conflictos con los a�adidos por uon, por material o individualmente.
'Si se puede esto ya se arreglar� m�s adelante con tiempo.
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAtributos.RemoveAll
    Set oAtribs = Nothing
    Set oAtribs = oFSGSRaiz.Generar_CAtributos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "VaciarAtributosMaterial", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Function
    DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "DevolverCod", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub tvwEstMat_NodeClick(ByVal node As MSComctlLib.node)
    Dim nodx As MSComctlLib.node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwEstMat.selectedItem
    Set g_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    If Left(nodx.Tag, 4) = "GMN4" Then
        g_oGMN4Seleccionado.GMN1Cod = DevolverCod(nodx.Parent.Parent.Parent)
        g_oGMN4Seleccionado.GMN2Cod = DevolverCod(nodx.Parent.Parent)
        g_oGMN4Seleccionado.GMN3Cod = DevolverCod(nodx.Parent)
        g_oGMN4Seleccionado.Cod = DevolverCod(nodx)
        
        VaciarAtributosMaterial
        CargarAtributosMaterial
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "tvwEstMat_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub CargarEstructuraMaterialesProceso()
    Dim nodx As MSComctlLib.node
    Dim oGrupsMN1 As CGruposMatNivel1
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim j As Integer
    
    'Recorrer el arbol y marcar cada rama de material seleccionada en el proceso y dejarla expandida
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm
    
    frmPROCE.g_oProcesoSeleccionado.MaterialProce.DevolverEstructuraDeMateriales
    
    Set nodx = tvwEstMat.Nodes.Add(, , "Raiz", sEstructura, "Raiz")
    nodx.Selected = True
    
    Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    
    oGrupsMN1.GenerarEstructuraMateriales False
    
    For Each oGMN1 In oGrupsMN1
        For j = 1 To frmPROCE.g_oProcesoSeleccionado.MaterialProce.Count
            If frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(j).GMN1Cod = oGMN1.Cod Then
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                For Each oGMN2 In oGMN1.GruposMatNivel2
                    If frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(j).GMN2Cod = oGMN2.Cod Then
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        For Each oGMN3 In oGMN2.GruposMatNivel3
                            If frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(j).GMN3Cod = oGMN3.Cod Then
                                scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                                For Each oGMN4 In oGMN3.GruposMatNivel4
                                    If frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(j).Cod = oGMN4.Cod Then
                                        scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                        
                                        Set nodx = tvwEstMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                                        nodx.Tag = "GMN1" & oGMN1.Cod
                                        Set nodx = tvwEstMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                                        nodx.Tag = "GMN2" & oGMN2.Cod
                                        Set nodx = tvwEstMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                                        nodx.Tag = "GMN3" & oGMN3.Cod
                                        Set nodx = tvwEstMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                                        nodx.Tag = "GMN4" & oGMN4.Cod
                                        
                                        nodx.Parent.Parent.Parent.Parent.Expanded = True
                                        
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next
    Next
        
ERROR_Frm:
    Set nodx = Nothing
    Resume Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "CargarEstructuraMaterialesProceso", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Sub
   End If
End Sub



'edu T98
'si hay plantilla con restriccion de material seleccionar exclusivamente materiales de la plantilla

Sub CargarEstructura(bProceso As Boolean)

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        If Not g_oGMN4Seleccionado Is Nothing Then
            If g_oGMN4Seleccionado.ARTICULOS.Count = 0 Then
                If bProceso = False Then
                    CargarEstructuraMateriales
                Else
                    CargarEstructuraMaterialesProceso
                End If

                Arrange
            Else
                'Si ya hay items cargados se nos situara en la pesta�a de articulos del mismo material que el ultimo item cargado
                CargarGrid
                ssTabEst.Tab = 2
            End If
        Else
            CargarEstructuraMateriales
            Arrange
        End If
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "CargarEstructura", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Function ObtenerIdSolicitud(IdSolicitud As String) As Variant
'Funcion que devuelve el Id de las solicitud sin las reemisiones si las tuviera
'Parametro IdSolicitud= 1234 (2) -->Devuelve 1234
Dim posIni As Byte

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
posIni = InStr(1, IdSolicitud, "(")
If posIni > 0 Then
    ObtenerIdSolicitud = Trim(Mid(IdSolicitud, 1, Len(IdSolicitud) - (Len(IdSolicitud) - posIni) - 1))
Else
    ObtenerIdSolicitud = IdSolicitud
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "ObtenerIdSolicitud", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function


Public Function cargarUonsRestringidas(ByRef uons1 As CUnidadesOrgNivel1, ByRef uons2 As CUnidadesOrgNivel2, ByRef uons3 As CUnidadesOrgNivel3)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    uons1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.perfil.Id, , , False
    uons2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.perfil.Id, , , False
    uons3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.perfil.Id, , False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "cargarUonsRestringidas", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Function addAtributo(ByRef oAtributo As Catributo) As Catributo
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not oAtribs.existe(oAtributo.Id) Then
        Set addAtributo = oAtribs.addAtributo(oAtributo)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard", "addAtributo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>Comprueba si existe un nodo en el �rbol. La colecci�n de nodos del �rbol genera un error si se intenta acceder a un nodo que no existe</summary>
''' <param name="sKey">Key del nodo<param>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Function ExisteNodo(ByVal sKey As String) As Boolean
    Dim nodx As MSComctlLib.node
    
    ''********* ������ NO MODIFICAR EL CONTROL DE ERRORES !!!!!! ***************
    On Error GoTo NoSeEncuentra:

    Set nodx = tvwEstMat.Nodes.Item(sKey)
    ExisteNodo = True
    
    Exit Function
NoSeEncuentra:
    ExisteNodo = False
    Set nodx = Nothing
End Function
