VERSION 5.00
Begin VB.Form frmCIERREFecReape 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DFecha de reapertura"
   ClientHeight    =   1185
   ClientLeft      =   1515
   ClientTop       =   1380
   ClientWidth     =   2850
   Icon            =   "frmCIERREFecReape.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1185
   ScaleWidth      =   2850
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   330
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   750
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1530
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   750
      Width           =   1005
   End
   Begin VB.CommandButton cmdCalendar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2160
      Picture         =   "frmCIERREFecReape.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   240
      Width           =   315
   End
   Begin VB.TextBox txtFecReape 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1020
      TabIndex        =   0
      Top             =   240
      Width           =   1110
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "DFecha:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   480
      TabIndex        =   2
      Top             =   285
      Width           =   555
   End
End
Attribute VB_Name = "frmCIERREFecReape"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private sCap1 As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String

Private Sub cmdAceptar_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    If txtFecReape <> "" Then
        If Not IsDate(txtFecReape) Then
            oMensajes.NoValido sCap1
            Exit Sub
        End If
        frmPROCE.g_vFecReape = CDate(txtFecReape)
    Else
        frmPROCE.g_vFecReape = Null
    End If
    
    frmPROCE.g_bCancelar = False
    
    Unload Me

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCIERREFecReape", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalendar_Click()
    AbrirFormCalendar Me, txtFecReape
End Sub

Private Sub cmdCancelar_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    frmPROCE.g_bCancelar = True
    Unload Me

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCIERREFecReape", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If

    If Not m_bActivado Then m_bActivado = True
    
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCIERREFecReape", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    m_bActivado = False
    CargarRecursos

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCIERREFecReape", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CIERREFECREAPE, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
        sCap1 = Ador(0).Value      '1
        Label8.caption = sCap1 & ":"
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value      '2
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value     '3
        Ador.MoveNext
        Ador.MoveNext
        frmCIERREFecReape.caption = Ador(0).Value     '5
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If

    oFSGSRaiz.pg_sFrmCargado Me.Name, False

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCIERREFecReape", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


