Attribute VB_Name = "CRbasPublic"
Option Explicit

Public Function GenerarCRArticulos() As CRArticulos
    Dim oCRArticulos As CRArticulos
    
    Set oCRArticulos = New CRArticulos
    With oCRArticulos
        .SincronizacionMat = gParametrosGenerales.gbSincronizacionMat
        .IdiomaPortal = gParametrosInstalacion.gIdiomaPortal
    End With
    
    Set GenerarCRArticulos = oCRArticulos
End Function

Public Function GenerarCRDestinos() As CRDestinos
    Dim oCRDestinos As CRDestinos
    
    Set oCRDestinos = New CRDestinos
    With oCRDestinos
        .IdiomaInstalacion = gParametrosInstalacion.gIdioma
    End With
    
    Set GenerarCRDestinos = oCRDestinos
End Function

Public Function GenerarCREqpPorProve() As CREqpPorProve
    Dim oCREqpPorProve As CREqpPorProve
    
    Set oCREqpPorProve = New CREqpPorProve
    With oCREqpPorProve
        .IdiomaInstalacion = basPublic.gParametrosInstalacion.gIdioma
    End With
    
    Set GenerarCREqpPorProve = oCREqpPorProve
End Function

Public Function GenerarCRInformes() As CRInformes
    Dim oCRInformes As CRInformes
    
    Set oCRInformes = New CRInformes
    With oCRInformes
        .CodEqpUsuario = basOptimizacion.gCodEqpUsuario
    End With
    
    Set GenerarCRInformes = oCRInformes
End Function

Public Function GenerarCRMatPorProve() As CRMatPorProve
    Dim oCRMatPorProve As CRMatPorProve
    
    Set oCRMatPorProve = New CRMatPorProve
    With oCRMatPorProve
        .Pymes = gParametrosGenerales.gbPymes
        .UsuarioTipo = oUsuarioSummit.Tipo
        Set .UsuarioComprador = oUsuarioSummit.comprador
        .IdiomaInstalacion = basPublic.gParametrosInstalacion.gIdioma
    End With
    
    Set GenerarCRMatPorProve = oCRMatPorProve
End Function

Public Function GenerarCROrganigrama(ByVal SelecUON As Variant) As CROrganigrama
    Dim oCROrganigrama As CROrganigrama
    
    Set oCROrganigrama = New CROrganigrama
    With oCROrganigrama
        .AccesoFSSM = gParametrosGenerales.gsAccesoFSSM
    End With
    
    Set GenerarCROrganigrama = oCROrganigrama
End Function

Public Function GenerarCRParametros() As CRParametros
    Dim oCRParametros As CRParametros
    
    Set oCRParametros = New CRParametros
    With oCRParametros
        .IdiomaInstalacion = basPublic.gParametrosInstalacion.gIdioma
    End With
    
    Set GenerarCRParametros = oCRParametros
End Function

Public Function GenerarCRPresParCon() As CRPresParCon
    Dim oCRPresParCon As CRPresParCon
    
    Set oCRPresParCon = New CRPresParCon
    With oCRPresParCon
        .DEN_UON0 = gParametrosGenerales.gsDEN_UON0
        .DEN_UON1 = gParametrosGenerales.gsDEN_UON1
        .DEN_UON2 = gParametrosGenerales.gsDEN_UON2
        .DEN_UON3 = gParametrosGenerales.gsDEN_UON3
    End With
    
    Set GenerarCRPresParCon = oCRPresParCon
End Function

Public Function GenerarCRPresPorMat() As CRPresPorMat
    Dim oCRPresPorMat As CRPresPorMat
    
    Set oCRPresPorMat = New CRPresPorMat
    With oCRPresPorMat
        .SincronizacionMat = gParametrosGenerales.gbSincronizacionMat
        .NEM = basParametros.gParametrosGenerales.giNEM
        .IdiomaPortal = gParametrosInstalacion.gIdiomaPortal
    End With
    
    Set GenerarCRPresPorMat = oCRPresPorMat
End Function

Public Function GenerarCRPresPorProy() As CRPresPorProy
    Dim oCRPresPorProy As CRPresPorProy
    
    Set oCRPresPorProy = New CRPresPorProy
    With oCRPresPorProy
        .DEN_UON0 = gParametrosGenerales.gsDEN_UON0
        .DEN_UON1 = gParametrosGenerales.gsDEN_UON1
        .DEN_UON2 = gParametrosGenerales.gsDEN_UON2
        .DEN_UON3 = gParametrosGenerales.gsDEN_UON3
    End With
    
    Set GenerarCRPresPorProy = oCRPresPorProy
End Function

Public Function GenerarCRProceso() As CRProceso
    Dim oCRProceso As CRProceso
    
    Set oCRProceso = New CRProceso
    With oCRProceso
        .ProveGrupos = gParametrosGenerales.gbProveGrupos
        .arSimbolos = garSimbolos
        .CodEqpUsuario = basOptimizacion.gCodEqpUsuario
        .CodCompradorUsuario = basOptimizacion.gCodCompradorUsuario
        .UON1Usuario = basOptimizacion.gUON1Usuario
        .UON2Usuario = basOptimizacion.gUON2Usuario
        .UON3Usuario = basOptimizacion.gUON3Usuario
        .CodDepUsuario = basOptimizacion.gCodDepUsuario
        .CodPersonaUsuario = basOptimizacion.gCodPersonaUsuario
        .AdminPublica = basParametros.gParametrosGenerales.gbAdminPublica
    End With
    
    Set GenerarCRProceso = oCRProceso
End Function

Public Function GenerarCRProveedores() As CRProveedores
    Dim oCRProveedores As CRProveedores
    
    Set oCRProveedores = New CRProveedores
    With oCRProveedores
        .IdiomaInstalacion = basPublic.gParametrosInstalacion.gIdioma
        .AccesoFSIM = gParametrosGenerales.gsAccesoFSIM
        .LOGPREBLOQ = gParametrosGenerales.giLOGPREBLOQ
    End With
    
    Set GenerarCRProveedores = oCRProveedores
End Function

Public Function GenerarCRProvePorEqp() As CRProvePorEqp
    Dim oCRProvePorEqp As CRProvePorEqp
    
    Set oCRProvePorEqp = New CRProvePorEqp
    With oCRProvePorEqp
        .IdiomaInstalacion = basPublic.gParametrosInstalacion.gIdioma
    End With
    
    Set GenerarCRProvePorEqp = oCRProvePorEqp
End Function

Public Function GenerarCRProvePorMat() As CRProvePorMat
    Dim oCRProvePorMat As CRProvePorMat
    
    Set oCRProvePorMat = New CRProvePorMat
    With oCRProvePorMat
        .CodUsuario = basOptimizacion.gvarCodUsuario
    End With
    
    Set GenerarCRProvePorMat = oCRProvePorMat
End Function

Public Function GenerarCRReunion() As CRReunion
    Dim oCRReunion As CRReunion
    
    Set oCRReunion = New CRReunion
    With oCRReunion
        .UON1Usuario = basOptimizacion.gUON1Usuario
        .UON2Usuario = basOptimizacion.gUON2Usuario
        .UON3Usuario = basOptimizacion.gUON3Usuario
        .CodEqpUsuario = basOptimizacion.gCodEqpUsuario
        .CodCompradorUsuario = basOptimizacion.gCodCompradorUsuario
        .CodDepUsuario = basOptimizacion.gCodDepUsuario
        .CodPersonaUsuario = basOptimizacion.gCodPersonaUsuario
    End With
    
    Set GenerarCRReunion = oCRReunion
End Function

Public Function GenerarCRSeguridad() As CRSeguridad
    Dim oCRSeguridad As CRSeguridad
    
    Set oCRSeguridad = New CRSeguridad
    With oCRSeguridad
        .LOGPREBLOQ = gParametrosGenerales.giLOGPREBLOQ
    End With
    
    Set GenerarCRSeguridad = oCRSeguridad
End Function

Public Function GenerarCRPedidos(ByVal RepPath As String) As CRPedidos
    Dim oCRPedidos As CRPedidos
    
    Set oCRPedidos = New CRPedidos
    With oCRPedidos
        .RepPath = RepPath
        .NomCodPersonalizPedido = gParametrosGenerales.gsNomCodPersonalizPedido
        .UsarOrgCompras = gParametrosGenerales.gbUsarOrgCompras
        .AccFSSM = gParametrosGenerales.gsAccesoFSSM
        .ActivarCodProveErp = gParametrosGenerales.gbActivarCodProveErp
        .CodPersonalizPedido = gParametrosGenerales.gbCodPersonalizPedido
        .CodPersonalizDirecto = gParametrosGenerales.gbCodPersonalizDirecto
        .SolicitudesCompras = gParametrosGenerales.gbSolicitudesCompras
        .FSEPConf = FSEPConf
        .PedidosDirFac = gParametrosGenerales.gbPedidosDirFac
        .AccFSIM = gParametrosGenerales.gsAccesoFSIM
        Set .ParametrosSM = g_oParametrosSM
        .UsarPres1 = gParametrosGenerales.gbUsarPres1
        .UsarPedPres1 = gParametrosGenerales.gbUsarPedPres1
        .SingPres1 = gParametrosGenerales.gsSingPres1
        .UsarPres2 = gParametrosGenerales.gbUsarPres2
        .UsarPedPres2 = gParametrosGenerales.gbUsarPedPres2
        .SingPres2 = gParametrosGenerales.gsSingPres2
        .UsarPres3 = gParametrosGenerales.gbUsarPres3
        .UsarPedPres3 = gParametrosGenerales.gbUsarPedPres3
        .SingPres3 = gParametrosGenerales.gsSingPres3
        .UsarPres4 = gParametrosGenerales.gbUsarPres4
        .UsarPedPres4 = gParametrosGenerales.gbUsarPedPres4
        .SingPres4 = gParametrosGenerales.gsSingPres4
    End With
    
    Set GenerarCRPedidos = oCRPedidos
End Function

Public Function GenerarCEmailAdj(ByRef oFSGSRaiz As CRaiz, ByRef oParametrosGenerales As ParametrosGenerales, ByRef oParametrosInstalacion As ParametrosInstalacion, ByRef oMensajes As CMensajes, _
                                 ByRef oUsuarioSummit As CUsuario, ByRef oLongitudesDeCodigos As LongitudesDeCodigos, ByRef oTipoDeUsuario As TipoDeUsuario) As CEmailAdj
    Dim oCEmailAdj As CEmailAdj
    
    Set oCEmailAdj = New CEmailAdj
    Set oCEmailAdj.oFSGSRaiz = oFSGSRaiz
    Set oCEmailAdj.Mensajes = oMensajes
    Set oCEmailAdj.UsuarioSummit = oUsuarioSummit
    oCEmailAdj.ParametrosGenerales = oParametrosGenerales
    oCEmailAdj.ParametrosInstalacion = oParametrosInstalacion
    oCEmailAdj.LongitudesDeCodigos = oLongitudesDeCodigos
    oCEmailAdj.oTipoDeUsuario = oTipoDeUsuario
    
    Set GenerarCEmailAdj = oCEmailAdj
End Function

Public Function GenerarCEmailProce(ByRef oFSGSRaiz As CRaiz, ByRef oParametrosGenerales As ParametrosGenerales, ByRef oParametrosInstalacion As ParametrosInstalacion, ByRef oMensajes As CMensajes, _
                                   ByRef oTipoDeUsuario As TipoDeUsuario, ByRef oUsuarioSummit As CUsuario, ByRef oLongitudesDeCodigos As LongitudesDeCodigos) As CEmailProce
    Dim oCEmailProce As CEmailProce
    
    Set oCEmailProce = New CEmailProce
    Set oCEmailProce.oFSGSRaiz = oFSGSRaiz
    Set oCEmailProce.Mensajes = oMensajes
    Set oCEmailProce.UsuarioSummit = oUsuarioSummit
    oCEmailProce.ParametrosGenerales = oParametrosGenerales
    oCEmailProce.ParametrosInstalacion = oParametrosInstalacion
    oCEmailProce.oTipoDeUsuario = oTipoDeUsuario
    oCEmailProce.LongitudesDeCodigos = oLongitudesDeCodigos
    
    Set GenerarCEmailProce = oCEmailProce
End Function

Public Function GenerarCEmailReuConvo(ByRef oMensajes As CMensajes, ByRef oGestorReuniones As CGestorReuniones) As CEmailReuConvo
    Dim oCEmailReuConvo As CEmailReuConvo
    
    Set oCEmailReuConvo = New CEmailReuConvo
    Set oCEmailReuConvo.Mensajes = oMensajes
    Set oCEmailReuConvo.GestorReuniones = oGestorReuniones
    
    Set GenerarCEmailReuConvo = oCEmailReuConvo
End Function

Public Function GenerarCEmailPedido(ByRef oFSGSRaiz As CRaiz, ByRef oMensajes As CMensajes, ByRef oParametrosInstalacion As ParametrosInstalacion, ByRef oParametrosGenerales As ParametrosGenerales) As CEmailPedido
    Dim oCEMailPedido As CEmailPedido
    
    Set oCEMailPedido = New CEmailPedido
    Set oCEMailPedido.oFSGSRaiz = oFSGSRaiz
    Set oCEMailPedido.Mensajes = oMensajes
    oCEMailPedido.ParametrosInstalacion = oParametrosInstalacion
    oCEMailPedido.ParametrosGenerales = oParametrosGenerales
    Set GenerarCEmailPedido = oCEMailPedido
End Function

Public Function GenerarCEmailQA(ByRef Mensajes As CMensajes, ByRef oParametrosInstalacion As ParametrosInstalacion, ByRef oLongitudesDeCodigos As LongitudesDeCodigos) As CEmailQA
    Dim oCEMailQA As CEmailQA
    
    Set oCEMailQA = New CEmailQA
    Set oCEMailQA.Mensajes = oMensajes
    oCEMailQA.ParametrosInstalacion = oParametrosInstalacion
    oCEMailQA.LongitudesDeCodigos = oLongitudesDeCodigos
    
    Set GenerarCEmailQA = oCEMailQA
End Function

Private Sub MostrarInforme(ByRef oReport As CRAXDRT.Report, ByVal sTitulo As String)
    Dim pv As Preview
    
    If Not oReport Is Nothing Then
        Set pv = New Preview
        pv.Hide
        pv.caption = sTitulo 'Listado de monedas
        Set pv.g_oReport = oReport
        pv.crViewer.ReportSource = oReport
        pv.crViewer.ViewReport
        pv.crViewer.Visible = True
        pv.Show
        
        Set oReport = Nothing
    End If
End Sub

Public Sub AbrirLstUNI()
    Dim oReport As CRAXDRT.Report
    Dim sTitulo As String
    
    Set oReport = MostrarFormLstUNI(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oMensajes, oGestorParametros, gParametrosInstalacion.gsRPTPATH, gParametrosGenerales.gsRPTPATH, g_GuardarParametrosIns, sTitulo)
    MostrarInforme oReport, sTitulo
    Set oReport = Nothing
End Sub

Public Sub AbrirLstMON()
    Dim oReport As CRAXDRT.Report
    Dim sTitulo As String
    
    Set oReport = MostrarFormLstMON(oGestorIdiomas, oGestorInformes, basPublic.gParametrosInstalacion.gIdioma, oMensajes, oGestorParametros, gParametrosInstalacion.gsRPTPATH, gParametrosGenerales.gsRPTPATH, g_GuardarParametrosIns, sTitulo)
    MostrarInforme oReport, sTitulo
    Set oReport = Nothing
End Sub

Public Sub AbrirLstPERFIL()
    Dim oReport As CRAXDRT.Report
    Dim sTitulo As String
    
    Set oReport = MostrarFormLstPERFIL(oGestorIdiomas, oGestorInformes, basPublic.gParametrosInstalacion.gIdioma, oMensajes, oGestorParametros, _
                                        gParametrosInstalacion.gsRPTPATH, gParametrosGenerales.gsRPTPATH, g_GuardarParametrosIns, sTitulo, _
                                        gParametrosInstalacion.giCargaMaximaCombos, oGestorSeguridad, oFSGSRaiz, gParametrosGenerales.gsPlurPres1, _
                                        gParametrosGenerales.gsPlurPres2, gParametrosGenerales.gsPlurPres3, gParametrosGenerales.gsPlurPres4, gParametrosGenerales.gbOblProveEqp)
    MostrarInforme oReport, sTitulo
    Set oReport = Nothing
End Sub

Public Sub AbrirLstPAI()
    Dim oReport As CRAXDRT.Report
    Dim sTitulo As String
    
    Set oReport = MostrarFormLstPAI(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oMensajes, gParametrosInstalacion.gsRPTPATH, gParametrosGenerales.gsRPTPATH, g_GuardarParametrosIns, sTitulo)
    MostrarInforme oReport, sTitulo
    Set oReport = Nothing
End Sub

Public Sub AbrirLstPROVI()
    Dim oReport As CRAXDRT.Report
    Dim sTitulo As String
    
    Set oReport = MostrarFormLstPROVI(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oMensajes, gParametrosInstalacion.gsRPTPATH, _
                                        gParametrosGenerales.gsRPTPATH, g_GuardarParametrosIns, sTitulo, oFSGSRaiz, gParametrosInstalacion.giCargaMaximaCombos, _
                                        gParametrosInstalacion.gsPais, gParametrosGenerales.gsPAIDEF)
    MostrarInforme oReport, sTitulo
    Set oReport = Nothing
End Sub


Public Sub AbrirLstESTRCOMP(Optional ByVal bOrdenarPorDen As Boolean = False, Optional ByRef oEquipoSeleccionado As CEquipo, Optional ByVal sOrigen As String = "")
    Dim oReport As CRAXDRT.Report
    Dim sTitulo As String
    
    Set oReport = MostrarFormLstESTRCOMP(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oMensajes, gParametrosInstalacion.gsRPTPATH, _
                                        gParametrosGenerales.gsRPTPATH, g_GuardarParametrosIns, sTitulo, oFSGSRaiz, gParametrosInstalacion.giCargaMaximaCombos, _
                                        bOrdenarPorDen, oEquipoSeleccionado, sOrigen, basOptimizacion.gCodEqpUsuario, oUsuarioSummit)
    MostrarInforme oReport, sTitulo
    Set oReport = Nothing
End Sub

Public Sub AbrirLstWORKFLOW()
    Dim oReport As CRAXDRT.Report
    Dim sTitulo As String
    
    Set oReport = MostrarFormLstWORKFLOW(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oMensajes, gParametrosInstalacion.gsRPTPATH, _
                                            gParametrosGenerales.gsRPTPATH, g_GuardarParametrosIns, sTitulo, oGestorInformes, oFSGSRaiz)
    MostrarInforme oReport, sTitulo
    Set oReport = Nothing
End Sub
Public Sub AbrirLstParametros(ByVal sOrigen As String, Optional ByVal bOrdDen As Boolean = False)
    Dim oReport As CRAXDRT.Report
    Dim sTitulo As String
    
    Set oReport = MostrarFormLstParametros(sOrigen, oFSGSRaiz, oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oGestorParametros, oMensajes, gParametrosInstalacion.gsRPTPATH, gParametrosGenerales.gsRPTPATH, _
        g_GuardarParametrosIns, gParametrosGenerales.gbSENT_ORD_CAL1, gParametrosGenerales.gbSENT_ORD_CAL2, gParametrosGenerales.gbSENT_ORD_CAL3, sTitulo, bOrdDen)
    MostrarInforme oReport, sTitulo
    Set oReport = Nothing
End Sub
