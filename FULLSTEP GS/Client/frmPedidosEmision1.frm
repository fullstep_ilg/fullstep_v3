VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPedidosEmision1 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n de contactos para notificaci�n de pedido"
   ClientHeight    =   5130
   ClientLeft      =   1050
   ClientTop       =   2040
   ClientWidth     =   13155
   Icon            =   "frmPedidosEmision1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5130
   ScaleWidth      =   13155
   Begin SSDataWidgets_B.SSDBGrid sdbgPet 
      Height          =   3330
      Left            =   90
      TabIndex        =   13
      Top             =   1275
      Width           =   14790
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   20
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPedidosEmision1.frx":0CB2
      stylesets(1).Name=   "Tan"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmPedidosEmision1.frx":0CCE
      UseGroups       =   -1  'True
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   4
      Groups(0).Width =   6985
      Groups(0).Caption=   "Proveedores"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadForeColor=   16777215
      Groups(0).HeadBackColor=   8421504
      Groups(0).Columns.Count=   2
      Groups(0).Columns(0).Width=   2566
      Groups(0).Columns(0).Caption=   "C�digo"
      Groups(0).Columns(0).Name=   "CODPROVE"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   4419
      Groups(0).Columns(1).Caption=   "Denominaci�n"
      Groups(0).Columns(1).Name=   "DENPROVE"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(1).Width =   7250
      Groups(1).Caption=   "Contactos"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadForeColor=   16777215
      Groups(1).HeadBackColor=   8421504
      Groups(1).Columns.Count=   2
      Groups(1).Columns(0).Width=   3810
      Groups(1).Columns(0).Caption=   "Apellidos"
      Groups(1).Columns(0).Name=   "APE"
      Groups(1).Columns(0).DataField=   "Column 2"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(1).Width=   3440
      Groups(1).Columns(1).Caption=   "Nombre"
      Groups(1).Columns(1).Name=   "NOM"
      Groups(1).Columns(1).DataField=   "Column 3"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(2).Width =   4763
      Groups(2).Caption=   "Pedido"
      Groups(2).HasHeadForeColor=   -1  'True
      Groups(2).HasHeadBackColor=   -1  'True
      Groups(2).HeadForeColor=   16777215
      Groups(2).HeadBackColor=   8421504
      Groups(2).Columns(0).Width=   4763
      Groups(2).Columns(0).Caption=   "dFormato"
      Groups(2).Columns(0).Name=   "FORMATO"
      Groups(2).Columns(0).DataField=   "Column 4"
      Groups(2).Columns(0).DataType=   8
      Groups(2).Columns(0).FieldLen=   256
      Groups(3).Width =   3254
      Groups(3).Caption=   "Via de notificaci�n"
      Groups(3).HasHeadForeColor=   -1  'True
      Groups(3).HasHeadBackColor=   -1  'True
      Groups(3).HeadForeColor=   16777215
      Groups(3).HeadBackColor=   8421504
      Groups(3).Columns.Count=   15
      Groups(3).Columns(0).Width=   1561
      Groups(3).Columns(0).Caption=   "Mail"
      Groups(3).Columns(0).Name=   "MAIL"
      Groups(3).Columns(0).Alignment=   2
      Groups(3).Columns(0).DataField=   "Column 5"
      Groups(3).Columns(0).DataType=   11
      Groups(3).Columns(0).FieldLen=   256
      Groups(3).Columns(0).Style=   2
      Groups(3).Columns(1).Width=   1693
      Groups(3).Columns(1).Caption=   "Carta"
      Groups(3).Columns(1).Name=   "IMP"
      Groups(3).Columns(1).Alignment=   2
      Groups(3).Columns(1).DataField=   "Column 6"
      Groups(3).Columns(1).DataType=   11
      Groups(3).Columns(1).FieldLen=   256
      Groups(3).Columns(1).Style=   2
      Groups(3).Columns(2).Width=   1085
      Groups(3).Columns(2).Visible=   0   'False
      Groups(3).Columns(2).Caption=   "NOBORRAR"
      Groups(3).Columns(2).Name=   "NOBORRAR"
      Groups(3).Columns(2).DataField=   "Column 7"
      Groups(3).Columns(2).DataType=   11
      Groups(3).Columns(2).FieldLen=   256
      Groups(3).Columns(3).Width=   847
      Groups(3).Columns(3).Visible=   0   'False
      Groups(3).Columns(3).Caption=   "EMAIL"
      Groups(3).Columns(3).Name=   "EMAIL"
      Groups(3).Columns(3).DataField=   "Column 8"
      Groups(3).Columns(3).DataType=   8
      Groups(3).Columns(3).FieldLen=   256
      Groups(3).Columns(4).Width=   953
      Groups(3).Columns(4).Visible=   0   'False
      Groups(3).Columns(4).Caption=   "IND"
      Groups(3).Columns(4).Name=   "IND"
      Groups(3).Columns(4).DataField=   "Column 9"
      Groups(3).Columns(4).DataType=   8
      Groups(3).Columns(4).FieldLen=   256
      Groups(3).Columns(5).Width=   741
      Groups(3).Columns(5).Visible=   0   'False
      Groups(3).Columns(5).Caption=   "CODPROV"
      Groups(3).Columns(5).Name=   "CODPROV"
      Groups(3).Columns(5).DataField=   "Column 10"
      Groups(3).Columns(5).DataType=   8
      Groups(3).Columns(5).FieldLen=   256
      Groups(3).Columns(6).Width=   503
      Groups(3).Columns(6).Visible=   0   'False
      Groups(3).Columns(6).Caption=   "TFNO_MOVIL"
      Groups(3).Columns(6).Name=   "TFNO_MOVIL"
      Groups(3).Columns(6).DataField=   "Column 11"
      Groups(3).Columns(6).DataType=   8
      Groups(3).Columns(6).FieldLen=   256
      Groups(3).Columns(7).Width=   423
      Groups(3).Columns(7).Visible=   0   'False
      Groups(3).Columns(7).Caption=   "FAX"
      Groups(3).Columns(7).Name=   "FAX"
      Groups(3).Columns(7).DataField=   "Column 12"
      Groups(3).Columns(7).DataType=   8
      Groups(3).Columns(7).FieldLen=   256
      Groups(3).Columns(8).Width=   953
      Groups(3).Columns(8).Visible=   0   'False
      Groups(3).Columns(8).Caption=   "TFNO"
      Groups(3).Columns(8).Name=   "TFNO"
      Groups(3).Columns(8).DataField=   "Column 13"
      Groups(3).Columns(8).DataType=   8
      Groups(3).Columns(8).FieldLen=   256
      Groups(3).Columns(9).Width=   1402
      Groups(3).Columns(9).Visible=   0   'False
      Groups(3).Columns(9).Caption=   "FSP_COD"
      Groups(3).Columns(9).Name=   "FSP_COD"
      Groups(3).Columns(9).DataField=   "Column 14"
      Groups(3).Columns(9).DataType=   8
      Groups(3).Columns(9).FieldLen=   256
      Groups(3).Columns(10).Width=   1402
      Groups(3).Columns(10).Visible=   0   'False
      Groups(3).Columns(10).Caption=   "PREMIUM"
      Groups(3).Columns(10).Name=   "PREMIUM"
      Groups(3).Columns(10).DataField=   "Column 15"
      Groups(3).Columns(10).DataType=   11
      Groups(3).Columns(10).FieldLen=   256
      Groups(3).Columns(11).Width=   1402
      Groups(3).Columns(11).Visible=   0   'False
      Groups(3).Columns(11).Caption=   "ACTIVO"
      Groups(3).Columns(11).Name=   "ACTIVO"
      Groups(3).Columns(11).DataField=   "Column 16"
      Groups(3).Columns(11).DataType=   11
      Groups(3).Columns(11).FieldLen=   256
      Groups(3).Columns(12).Width=   1402
      Groups(3).Columns(12).Visible=   0   'False
      Groups(3).Columns(12).Caption=   "OFERTADOWEB"
      Groups(3).Columns(12).Name=   "OFERTADOWEB"
      Groups(3).Columns(12).DataField=   "Column 17"
      Groups(3).Columns(12).DataType=   11
      Groups(3).Columns(12).FieldLen=   256
      Groups(3).Columns(13).Width=   688
      Groups(3).Columns(13).Visible=   0   'False
      Groups(3).Columns(13).Caption=   "CODCON"
      Groups(3).Columns(13).Name=   "CODCON"
      Groups(3).Columns(13).DataField=   "Column 18"
      Groups(3).Columns(13).DataType=   8
      Groups(3).Columns(13).FieldLen=   256
      Groups(3).Columns(14).Width=   1402
      Groups(3).Columns(14).Visible=   0   'False
      Groups(3).Columns(14).Caption=   "PORT"
      Groups(3).Columns(14).Name=   "PORT"
      Groups(3).Columns(14).DataField=   "Column 19"
      Groups(3).Columns(14).DataType=   11
      Groups(3).Columns(14).FieldLen=   256
      _ExtentX        =   26088
      _ExtentY        =   5874
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdDOT 
      Height          =   315
      Left            =   10515
      Picture         =   "frmPedidosEmision1.frx":0CEA
      Style           =   1  'Graphical
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   240
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.TextBox txtDOT 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2640
      MaxLength       =   255
      TabIndex        =   5
      Top             =   240
      Width           =   7800
   End
   Begin VB.TextBox txtMailDot 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2640
      MaxLength       =   255
      TabIndex        =   4
      Top             =   660
      Width           =   7800
   End
   Begin VB.CommandButton cmdMailDot 
      Height          =   315
      Left            =   10515
      Picture         =   "frmPedidosEmision1.frx":102C
      Style           =   1  'Graphical
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   660
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   13155
      TabIndex        =   0
      Top             =   4635
      Width           =   13155
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6660
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   90
         Width           =   1065
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5460
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   90
         Width           =   1065
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddConApe 
      Height          =   1515
      Left            =   630
      TabIndex        =   9
      Top             =   2760
      Width           =   5715
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   1
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPedidosEmision1.frx":136E
      stylesets(0).AlignmentText=   0
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   4022
      Columns(0).Caption=   "DApellidos"
      Columns(0).Name =   "APE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1852
      Columns(1).Caption=   "DNombre"
      Columns(1).Name =   "NOM"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3201
      Columns(2).Caption=   "DMail"
      Columns(2).Name =   "EMAIL"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "DTipoPet"
      Columns(4).Name =   "TIPOPET"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   11
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "TFNO_MOVIL"
      Columns(5).Name =   "TFNO_MOVIL"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "FAX"
      Columns(6).Name =   "FAX"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "TFNO"
      Columns(7).Name =   "TFNO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      _ExtentX        =   10081
      _ExtentY        =   2672
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddConNom 
      Height          =   1515
      Left            =   1800
      TabIndex        =   10
      Top             =   2520
      Width           =   5775
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   1
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPedidosEmision1.frx":138A
      stylesets(0).AlignmentText=   0
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   1852
      Columns(0).Caption=   "DNombre"
      Columns(0).Name =   "NOM"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4022
      Columns(1).Caption=   "DApellidos"
      Columns(1).Name =   "APE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3942
      Columns(2).Caption=   "DMail"
      Columns(2).Name =   "EMAIL"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "DTipoPet"
      Columns(4).Name =   "TIPOPET"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   11
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "TFNO_MOVIL"
      Columns(5).Name =   "TFNO_MOVIL"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "FAX"
      Columns(6).Name =   "FAX"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "TFNO"
      Columns(7).Name =   "TFNO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   2672
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveCod 
      Height          =   1305
      Left            =   1080
      TabIndex        =   11
      Top             =   2400
      Width           =   5175
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   1
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPedidosEmision1.frx":13A6
      stylesets(0).AlignmentText=   0
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3201
      Columns(0).Caption=   "DCod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6006
      Columns(1).Caption=   "DDenominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9128
      _ExtentY        =   2302
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveDen 
      Height          =   1575
      Left            =   1440
      TabIndex        =   12
      Top             =   2400
      Width           =   5055
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   1
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPedidosEmision1.frx":13C2
      stylesets(0).AlignmentText=   0
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5477
      Columns(0).Caption=   "DDenominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2990
      Columns(1).Caption=   "DC�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8916
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddFormato 
      Height          =   1020
      Left            =   2835
      TabIndex        =   14
      Top             =   1350
      Width           =   1800
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      Row.Count       =   2
      Row(0)          =   "Word"
      Row(1)          =   "Excel"
      stylesets.count =   1
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPedidosEmision1.frx":13DE
      stylesets(0).AlignmentText=   0
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   4022
      Columns(0).Caption=   "Formato"
      Columns(0).Name =   "FORMATO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3175
      _ExtentY        =   1799
      _StockProps     =   77
   End
   Begin MSComDlg.CommonDialog cmmdDot 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label7 
      BackColor       =   &H00808000&
      Caption         =   "Plantilla para carta:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   285
      TabIndex        =   8
      Top             =   300
      Width           =   2190
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Plantilla para e-mail:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   285
      TabIndex        =   7
      Top             =   720
      Width           =   2190
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00FFFFFF&
      Height          =   1140
      Left            =   90
      Top             =   60
      Width           =   12930
   End
End
Attribute VB_Name = "frmPedidosEmision1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private oProves As CProveedores
Private oProve As CProveedor
Private oContactos As CContactos
Private oNotificacion As CNotificacionPedido
Private oFos As Scripting.FileSystemObject
Private oEmpresas As CEmpresas

Private bCargarComboDesdeDD As Boolean

Private oOrdenEntrega As COrdenEntrega

Private sCap(1 To 15) As String
Private sXML As String
Private sExcel As String
Private sayFileNameF As String
Private NombreF As String
Private TipoF As String

Private scodProve As String
Private sDenProve As String
Private sApe As String
Private sNom As String
Private sFormat As String
Private bNotifMail As Boolean
Private bNotifCarta As Boolean

Private iNotificaciones() As Integer
Private sAdministrador As String

Private bNoMofif As Boolean
Private appword  As Object
Private appexcel  As Object
Private sPedido As String
Private sFormatoNumber As String

Private m_sIdiFalse As String
Private m_sIdiTrue As String

Public g_oOrigen As Form
Public g_bCancelarMail As Boolean


''' <summary>
''' Carga las cadenas de idiomas
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0,1</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PEDIDOS_EMISION, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        frmPedidosEmision1.caption = Ador(0).Value
        Ador.MoveNext
        Label7.caption = Ador(0).Value
        Ador.MoveNext
        Label2.caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups.Item(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups.Item(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups.Item(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups.Item(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns("MAIL").caption = Ador(0).Value
        sdbddConApe.Columns(2).caption = Ador(0).Value
        sdbddConNom.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns("IMP").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(2).caption = Ador(0).Value
        sdbddConNom.Columns(1).caption = Ador(0).Value
        sdbddConApe.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(3).caption = Ador(0).Value
        sdbddConNom.Columns(0).caption = Ador(0).Value
        sdbddConApe.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns("FORMATO").caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        'FORMATO
        Ador.MoveNext
        sdbddFormato.Columns(0).Value = Ador(0).Value
        sExcel = Ador(0).Value
        sdbddFormato.MoveNext
        Ador.MoveNext
        sdbddFormato.Columns(0).Value = Ador(0).Value
        sXML = Ador(0).Value
    
    '***********************
        Ador.MoveNext
        sCap(1) = Ador(0).Value      '1 Plantilla
        Ador.MoveNext
        sCap(2) = Ador(0).Value      '2 Generando comunicaci�n de cierre...
        Ador.MoveNext
        sCap(3) = Ador(0).Value     '3 Creando comunicaciones...
        Ador.MoveNext
        sCap(4) = Ador(0).Value     '4 Contacto:
        Ador.MoveNext
        sCap(5) = Ador(0).Value     '5 Iniciando sesi�n de correo ...
        Ador.MoveNext
        sCap(6) = Ador(0).Value     '6 Iniciando Microsoft Word...
        Ador.MoveNext
        sCap(7) = Ador(0).Value     '7 A�adiendo plantilla
        Ador.MoveNext
        sCap(8) = Ador(0).Value     '8 Enviando mensaje ...
        Ador.MoveNext
        sCap(9) = Ador(0).Value     '9 Visualizando impreso...
        Ador.MoveNext
        sCap(10) = Ador(0).Value    '10 Cerrando sesi�n de correo...
        Ador.MoveNext
        sCap(11) = Ador(0).Value    '11 Direcci�n de correo electr�nico
        Ador.MoveNext
        sCap(12) = Ador(0).Value
        Ador.MoveNext
        sAdministrador = Ador(0).Value
        Ador.MoveNext
        sCap(13) = Ador(0).Value
        Ador.MoveNext
        sCap(14) = Ador(0).Value
        Ador.MoveNext
        sdbddConNom.Columns("TIPOPET").caption = Ador(0).Value
        Ador.MoveNext
        sCap(15) = Ador(0).Value
        Ador.MoveNext
        sdbddProveCod.Columns(1).caption = Ador(0).Value
        sdbddProveDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbddProveCod.Columns(0).caption = Ador(0).Value
        sdbddProveDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sPedido = Ador(0).Value
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value
        
        Ador.Close
    End If
End Sub

''' <summary>
''' Lanzar la notificaci�n del pedido directo
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim i As Integer
    Dim iNumPets As Integer
    Dim iNumPet As Integer
    Dim iFormato As Integer
    Dim sUsuario As Variant
    Dim bActivarWord As Boolean
    Dim bActivarExcel As Boolean
    Dim oPedido As CPedido
    Dim oOrden As COrdenEntrega
    Dim oNotificacion As CNotificacionPedido
    Dim oNotifs As CNotificacionesPedido
    Dim docword As Object
    Dim docPetWord As Object
    Dim docPetExcel As Object
    Dim rangeword As Object
    Dim bSesionIniciada As Boolean   'Indica si se ha iniciado una sesion de correo
    Dim splantilla As String
    Dim sPlantillaMail As String
    Dim sTemp As String
    Dim bSalvar As Boolean
    Dim bDisplayAlerts As Boolean
    Dim errormail As TipoErrorSummit
    Dim sCuerpo As String
    Dim arAtach() As String
    Dim sMails As String
    Dim oFSO As Scripting.FileSystemObject
    Dim oRPedido As CRPedido
    
    ' Se utiliza un �nico documento
    On Error GoTo Error:
    sdbgPet.Update
    
    sdbgPet.MoveFirst
    For i = 0 To sdbgPet.Rows - 1
        If Trim(sdbgPet.Columns(2).Value) = "" And sdbgPet.Columns(0).Text <> "" Then
            oMensajes.ContactoNoValido
            Exit Sub
        End If
        sdbgPet.MoveNext
    Next
        
    'Comprobaci�n de las plantillas
    If Trim(txtDOT) = "" Then
        oMensajes.NoValido sCap(1)
        If Me.Visible Then txtDOT.SetFocus
        Exit Sub
    Else
        If Right(txtDOT, 3) <> "dot" Then
            oMensajes.NoValido sCap(1)
            If Me.Visible Then txtDOT.SetFocus
            Exit Sub
        End If
    End If
    If Not oFos.FileExists(txtDOT) Then
        oMensajes.PlantillaNoEncontrada txtDOT
        If Me.Visible Then txtDOT.SetFocus
        Exit Sub
    End If

    splantilla = txtDOT

  ' Plantillas para notificaciones via mail
    If gParametrosGenerales.giMail <> 0 Then
        If Trim(txtMailDot) = "" Then
            oMensajes.NoValido sCap(1)
            If Me.Visible Then txtMailDot.SetFocus
            Exit Sub
        Else
            If Right(txtMailDot, 3) <> "dot" Then
                oMensajes.NoValido sCap(1)
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
            If Not oFos.FileExists(txtMailDot) Then
                oMensajes.PlantillaNoEncontrada txtMailDot
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
        End If
    End If
    sPlantillaMail = txtMailDot
    
    Dim oEmpresas As CEmpresas
    Dim sCarpeta As String
    Dim sMarcaPlantillas As String
    Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
    oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , , , , g_oOrigen.g_oOrdenesTemporales.Item(1).Empresa
    If InStr(gParametrosInstalacion.gsPedidoDot, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
       InStr(gParametrosInstalacion.gsPedidoXLT, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
        'Buscamos la empresa CARPETA_PLANTILLAS
        If oEmpresas.Count > 0 Then sCarpeta = oEmpresas.Item(1).Carpeta_Plantillas
    End If
    If sCarpeta = "" And gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    
    If gParametrosInstalacion.gsPedidoXLT = "" Then
        
        If gParametrosGenerales.gsPedidoXLT = "" Then
            oMensajes.NoValido sCap(1)
            'Set NotificacionPedidoExcel = Nothing
            Exit Sub
        Else
            gParametrosInstalacion.gsPedidoXLT = gParametrosGenerales.gsPedidoXLT
            g_GuardarParametrosIns = True
        End If
    
    End If
    
    If Right(gParametrosInstalacion.gsPedidoXLT, 3) <> "xlt" Then
        oMensajes.NoValido sCap(1) & ":" & Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)
        Exit Sub
    End If
        
    If Not oFos.FileExists(Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)) Then
        oMensajes.PlantillaNoEncontrada Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)
        Exit Sub
    End If

    If gParametrosInstalacion.gsPedidoDot = "" Then
        
        If gParametrosGenerales.gsPedidoDot = "" Then
            oMensajes.NoValido sCap(1)
            Exit Sub
        Else
            gParametrosInstalacion.gsPedidoDot = gParametrosGenerales.gsPedidoDot
            g_GuardarParametrosIns = True
        End If
    
    End If
    
    If Right(gParametrosInstalacion.gsPedidoDot, 3) <> "dot" Then
        oMensajes.NoValido sCap(1) & ":" & Replace(gParametrosInstalacion.gsPedidoDot, sMarcaPlantillas, sCarpeta)
        Exit Sub
    End If
        
    If Not oFos.FileExists(Replace(gParametrosInstalacion.gsPedidoDot, sMarcaPlantillas, sCarpeta)) Then
        oMensajes.PlantillaNoEncontrada Replace(gParametrosInstalacion.gsPedidoDot, sMarcaPlantillas, sCarpeta)
        Exit Sub
    End If

'Cargar las notificaciones a�adididas en la coleccion
    
    Me.Hide
    frmESPERA.lblGeneral.caption = sCap(2)
    frmESPERA.Top = Me.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = Me.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Show

    DoEvents
    frmESPERA.ProgressBar1.Value = 1

        
    sdbgPet.MoveFirst
    iNumPets = 0

    
    'Emitir pedidos
    sUsuario = g_oOrigen.DevolverAprovisionador
    
    g_oOrigen.g_oOrdenesTemporales.CargarCentrosDistribucion
    If g_oOrigen.g_TipoEmision = TipoEmisionPedido.PedidoEstandar Then
        teserror = g_oOrigen.g_oOrdenesTemporales.EmitirPedidoConLineasTemporales(sUsuario, g_oOrigen.m_oTipoPedido.Id, g_oOrigen.g_bSolicitudAbono)
    Else
        teserror = g_oOrigen.g_oOrdenesTemporales.EmitirPedidoConLineasTemporales(sUsuario)
    End If
    If teserror.NumError <> TESnoerror Then
        Unload frmESPERA
        TratarError teserror
        'Si el error es de adjuntos aunque avisamos de que no se han adjuntado todos los ficheros, seguimos ya que la orden ya se ha grabado
        If teserror.NumError <> erroressummit.TESErrorAdjuntarFichero Then Exit Sub
    End If
    For Each oOrden In g_oOrigen.g_oOrdenesTemporales
        RegistrarAccion ACCEmitirPedidosDirEmitir, "Pedido: " & oOrden.Anyo & "/" & oOrden.NumPedido & "/" & oOrden.Numero & "; ID Orden: " & oOrden.Id
    Next
    
    Set oNotifs = oFSGSRaiz.Generar_CNotificacionesPedido
    sdbgPet.MoveFirst
    For i = 0 To sdbgPet.Rows - 1
        If sdbgPet.Columns("CODPROVE").Value <> "" Then
            iNumPets = iNumPets + 1
            iFormato = 0
            If sdbgPet.Columns("FORMATO").Value = sXML Then iFormato = 1
            If sdbgPet.Columns("FORMATO").Value = sExcel Then iFormato = 2
            Set oOrden = g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value)
            If oOrden.Notificaciones.Item(sdbgPet.Columns("IND").Value) Is Nothing Then
                sdbgPet.Columns("IND").Value = iNumPets
                oOrden.Notificaciones.Add , oOrden.idPedido, oOrden.Id, sdbgPet.Columns(0).Value, sdbgPet.Columns(1).Value, , sdbgPet.Columns(2).Value, sdbgPet.Columns(3).Value, sdbgPet.Columns("EMAIL").Value, , , , sdbgPet.Columns("MAIL").Value, sdbgPet.Columns("IMP").Value, iFormato, iNumPets
            End If
            oNotifs.Add , oOrden.Notificaciones.Item(sdbgPet.Columns("IND").Value).pedido, oOrden.Notificaciones.Item(sdbgPet.Columns("IND").Value).Orden, sdbgPet.Columns(0).Value, sdbgPet.Columns(1).Value, , sdbgPet.Columns(2).Value, sdbgPet.Columns(3).Value, sdbgPet.Columns("EMAIL").Value, , , , sdbgPet.Columns("MAIL").Value, sdbgPet.Columns("IMP").Value, iFormato, iNumPets
            Set oOrden = Nothing
        End If
        sdbgPet.MoveNext
    Next
    
    'Paso las ordenes al form de resumen
    Set frmPedidosEmision2.oPedido = oFSGSRaiz.Generar_CPedido
    frmPedidosEmision2.oPedido.Anyo = Year(Date)
    frmPedidosEmision2.oPedido.Estado = 1
    frmPedidosEmision2.oPedido.Fecha = g_oOrigen.g_oOrdenesTemporales.Item(1).Fecha
    frmPedidosEmision2.oPedido.Id = g_oOrigen.g_oOrdenesTemporales.Item(1).idPedido
    frmPedidosEmision2.oPedido.Numero = g_oOrigen.g_oOrdenesTemporales.Item(1).NumPedido
    frmPedidosEmision2.oPedido.Persona = sUsuario
    frmPedidosEmision2.oPedido.Tipo = Directo
    Set frmPedidosEmision2.oPedido.OrdenesEntrega = g_oOrigen.g_oOrdenesTemporales
    sMails = RellenaPedNotif_CCO(sMails, g_oOrigen.g_oOrdenesTemporales)
      
    ' Recorremos las notificaciones para enviar un mail u obtener un impreso

    iNumPet = 0
    i = 0

    For Each oNotificacion In oNotifs

        iNumPet = iNumPet + 1
        
        frmESPERA.lblContacto = sCap(4) & " " & oNotificacion.nombre & " " & oNotificacion.Apellidos
        frmESPERA.lblContacto.Refresh
        DoEvents
        frmESPERA.ProgressBar2.Value = val((iNumPet / iNumPets) * 100)
        frmESPERA.ProgressBar1.Value = 2
        frmESPERA.lblDetalle = sCap(5)
        frmESPERA.lblDetalle.Refresh
 
        DoEvents
        'Cojo los datos que van a ser comunes
        Set oOrden = g_oOrigen.g_oOrdenesTemporales.Item(oNotificacion.CodProve)
        Set oProves = oFSGSRaiz.generar_CProveedores
        oProves.Add oNotificacion.CodProve, oNotificacion.DenProve
        oProves.CargarDatosProveedor oNotificacion.CodProve
        oOrden.LeerPersonaProceso
        oOrden.LeerReceptorProceso

        If oNotificacion.mail Then
           ' Env�o de mensaje al proveedor VIA MAIL

           If Not bSesionIniciada Or oIdsMail Is Nothing Then
                   frmESPERA.ProgressBar1.Value = 4
                   frmESPERA.lblDetalle = sCap(5)
                   frmESPERA.lblDetalle.Refresh
                   DoEvents
                   Set oIdsMail = IniciarSesionMail
                   bSesionIniciada = True
           End If
'
           If appword Is Nothing Then
               frmESPERA.ProgressBar1.Value = 2
               If oNotificacion.FormatoPedido = 1 Then
                   frmESPERA.lblDetalle = sCap(6)
               Else
                   frmESPERA.lblDetalle = sCap(15)
               End If
               frmESPERA.lblDetalle.Refresh
               DoEvents
               Set appword = CreateObject("Word.Application")
               bSalvar = appword.Options.SavePropertiesPrompt
               appword.Options.SavePropertiesPrompt = False
           End If

           frmESPERA.ProgressBar1.Value = 3
           DoEvents

               frmESPERA.lblDetalle = sCap(7) & " " & sPlantillaMail & " ..."
               frmESPERA.lblDetalle.Refresh
               
               Set oRPedido = New CRPedido
               Set docword = oRPedido.CartaNotificacionPedidoDirecto(appword, oNotificacion, sPlantillaMail, g_oOrigen.g_oOrdenesTemporales.Item(oProves.Item(1).Cod), oProves.Item(1), oEmpresas.Item(1), gParametrosGenerales)
               Set oRPedido = Nothing
               sCuerpo = docword.Range.Text
               docword.Close False

               ' Primero adjunto el documento con el pedido
               
               frmESPERA.ProgressBar1.Value = 4
               
               If oNotificacion.FormatoPedido = 1 Then
                   frmESPERA.lblDetalle = sCap(6)
               Else
                   frmESPERA.lblDetalle = sCap(15)
               End If
               frmESPERA.lblDetalle.Refresh
               DoEvents
               If oNotificacion.FormatoPedido = 1 Then
                    Set oFSO = New Scripting.FileSystemObject
                    If Not oFSO.FileExists(Replace(gParametrosInstalacion.gsPedidoDot, sMarcaPlantillas, sCarpeta)) Then
                        oMensajes.PlantillaNoEncontrada Replace(gParametrosInstalacion.gsPedidoDot, sMarcaPlantillas, sCarpeta)
                    Else
                        Set oRPedido = New CRPedido
                        Set docPetWord = oRPedido.PedidoWord(oNotificacion, appword, g_oOrigen.g_oOrdenesTemporales.Item(oProves.Item(1).Cod), oProves.Item(1), oEmpresas.Item(1), "Standard", oFSGSRaiz, oGestorIdiomas, _
                            gParametrosGenerales, gParametrosInstalacion, oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 64), oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 63), oUsuarioSummit.Perfil)
                        Set oRPedido = Nothing
                    End If
                    Set oFSO = Nothing
                   
                    If Not docPetWord Is Nothing Then
                        sTemp = DevolverPathFichTemp
                        docPetWord.SaveAs sTemp & sPedido & CStr(oNotificacion.indice) & ".doc"
                        docPetWord.Close
                        sayFileNameF = sTemp & sPedido & CStr(oNotificacion.indice) & ".doc"
                        NombreF = sPedido
                        TipoF = ".doc"
                    End If
               ElseIf oNotificacion.FormatoPedido = 2 Then
                   Set appexcel = CreateObject("Excel.Application")
                   bDisplayAlerts = appexcel.DisplayAlerts
                   appexcel.DisplayAlerts = False
                   
                   Set oRPedido = New CRPedido
                   Set docPetExcel = oRPedido.PedidoExcel(appexcel, oOrden, oEmpresas.Item(1), oProves.Item(1), "#,##0.00", oFSGSRaiz, gParametrosGenerales, gParametrosInstalacion, _
                                                          oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 64), oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 63), g_oParametrosSM)
                   Set oRPedido = Nothing
                   
                   If Not docPetExcel Is Nothing Then
                      sTemp = DevolverPathFichTemp
                      docPetExcel.SaveAs sTemp & sPedido & CStr(oNotificacion.indice) & ".xls", docPetExcel.fileformat
                      docPetExcel.Close
                      sayFileNameF = sTemp & sPedido & CStr(oNotificacion.indice) & ".xls"
                      NombreF = sPedido
                      TipoF = ".xls"
                   End If
                    'Cuando la notificacion es via mail, pueden abrir el adjunto excel y cerrarlo
                    'el objeto appexcel sigue manteniendo una referencia al EXCEL correcta,
                    ' todo el codigo se ejecuta OK, pero al abrir un segundo adjunto, se queda colgado
                    ' el EXCEL. Asi que se soluciona, creando una nueva referencia a EXCEL cada vez
                   appexcel.Quit
                   Set appexcel = Nothing
               End If
               
                If oNotificacion.FormatoPedido = 1 Or oNotificacion.FormatoPedido = 2 Then
                    ReDim arAtach(0)
                    arAtach(0) = NombreF & CStr(oNotificacion.indice) & TipoF
                    ReDim Preserve arAtach(UBound(arAtach) + 1)
                Else
                    ReDim arAtach(0)
                End If
                
                g_bCancelarMail = False
                errormail = ComponerMensaje(oNotificacion.Email, gParametrosInstalacion.gsPedEmiMailSubject, sCuerpo, arAtach, "frmPedidosEmision1", _
                                Prove:=oNotificacion.CodProve, _
                                lIdInstancia:=oOrden.Id, _
                                entidadNotificacion:=PedDirecto, _
                                tipoNotificacion:=PedEmision, _
                                sToName:="", _
                                sCC:=gParametrosInstalacion.gsRecipientCC, sCCO:=sMails, sRemitenteEmpresa:=oEmpresas.Item(1).Remitente)
                
               'Metemos en la notificaci�n
                If errormail.NumError <> TESnoerror Then
                    TratarError errormail
                    oNotificacion.Fallido = True
                    oNotificacion.MailCancel = True
                ElseIf g_bCancelarMail = True Then
                    oNotificacion.MailCancel = True
                End If
                frmESPERA.ProgressBar1.Value = 8
                frmESPERA.lblDetalle = sCap(8)
                frmESPERA.lblDetalle.Refresh
                DoEvents

                'Almacenamos las notificaciones en la BD
                If oNotificacion.MailCancel = False Then
                    oNotificacion.IDRegistroMail = g_lIDRegistroEmail
                    teserror = oNotificacion.RealizarComunicacion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                    End If
                End If

       Else
               'Generamos un documento de notificaci�n impresa para cada contacto
               ' *************** PETICIONES VIA IMPRESO **********************
           If oNotificacion.Carta Then

               bActivarWord = True
               If appword Is Nothing Then
                   frmESPERA.ProgressBar1.Value = 2
                   
               If oNotificacion.FormatoPedido = 1 Then
                   frmESPERA.lblDetalle = sCap(6)
               Else
                   frmESPERA.lblDetalle = sCap(15)
               End If
                   frmESPERA.lblDetalle.Refresh
                   DoEvents
                   Set appword = CreateObject("Word.Application")
                   bSalvar = appword.Options.SavePropertiesPrompt
                   appword.Options.SavePropertiesPrompt = False
               End If

               frmESPERA.ProgressBar1.Value = 3
               frmESPERA.lblDetalle = sCap(7) & " " & splantilla & " ..."
               frmESPERA.lblDetalle.Refresh
               
               Set oRPedido = New CRPedido
               Set docword = oRPedido.CartaNotificacionPedidoDirecto(appword, oNotificacion, splantilla, g_oOrigen.g_oOrdenesTemporales.Item(oProves.Item(1).Cod), oProves.Item(1), oEmpresas.Item(1), gParametrosGenerales)
               Set oRPedido = Nothing
                
               frmESPERA.ProgressBar1.Value = 4
               If oNotificacion.FormatoPedido = 1 Then
                   frmESPERA.lblDetalle = sCap(6)
               Else
                   frmESPERA.lblDetalle = sCap(15)
               End If
               frmESPERA.lblDetalle.Refresh
               DoEvents
               If oNotificacion.FormatoPedido = 1 Then
                    Set oFSO = New Scripting.FileSystemObject
                    If Not oFSO.FileExists(Replace(gParametrosInstalacion.gsPedidoDot, sMarcaPlantillas, sCarpeta)) Then
                        oMensajes.PlantillaNoEncontrada Replace(gParametrosInstalacion.gsPedidoDot, sMarcaPlantillas, sCarpeta)
                    Else
                        Set oRPedido = New CRPedido
                        Set docPetWord = oRPedido.PedidoWord(oNotificacion, appword, g_oOrigen.g_oOrdenesTemporales.Item(oProves.Item(1).Cod), oProves.Item(1), oEmpresas.Item(1), "Standard", oFSGSRaiz, oGestorIdiomas, _
                            gParametrosGenerales, gParametrosInstalacion, oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 64), oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 63), oUsuarioSummit.Perfil)
                        Set oRPedido = Nothing
                    End If
                    Set oFSO = Nothing
                    
                    If Not docPetWord Is Nothing Then
                        ' Tengo que pegar este �ltimo documento en las peticiones anteriores
                        docPetWord.Range.Select
                        docPetWord.Range.Copy
                        docPetWord.Close False
                        Set rangeword = docword.Range
                        rangeword.Collapse Direction:=0 'wdCollapseEnd
                        rangeword.InsertBreak Type:=7 'wdPageBreak
                        rangeword.Paste
                    End If
                ElseIf oNotificacion.FormatoPedido = 2 Then
                    bActivarExcel = True
                    If appexcel Is Nothing Then
                    Set appexcel = CreateObject("Excel.Application")
                    bDisplayAlerts = appexcel.DisplayAlerts
                    appexcel.DisplayAlerts = False
                    End If
                    
                    Set oRPedido = New CRPedido
                    Set docPetExcel = oRPedido.PedidoExcel(appexcel, oOrden, oEmpresas.Item(1), oProves.Item(1), "#,##0.00", oFSGSRaiz, gParametrosGenerales, gParametrosInstalacion, _
                                                           oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 64), oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 63), g_oParametrosSM)
                    Set oRPedido = Nothing
                    
                    If Not docPetExcel Is Nothing Then
                    '                  appexcel.Visible = True
                    End If
                End If
           End If
       End If
    Next

    appword.Options.SavePropertiesPrompt = bSalvar
    appexcel.DisplayAlerts = bDisplayAlerts
    If bActivarWord Then
        frmESPERA.ProgressBar1.Value = 7
        frmESPERA.lblDetalle = sCap(9)
        frmESPERA.lblDetalle.Refresh
        DoEvents
        appword.Visible = True
    End If

    If bActivarExcel Then
        frmESPERA.ProgressBar1.Value = 7
        frmESPERA.lblDetalle = sCap(9)
        frmESPERA.lblDetalle.Refresh
        DoEvents
        appexcel.Visible = True
    End If

    If bSesionIniciada Then
        frmESPERA.ProgressBar1.Value = 5
        frmESPERA.lblDetalle = sCap(10)
        FinalizarSesionMail
    End If

    Unload frmESPERA
    Set oProves = Nothing
    If appword.Visible = False Then
        ' cuando se realizan solo peticiones via web, queda una tarea
        ' de word abierta que no se destruye con appword = nothing
        appword.Quit
    End If
    If appexcel.Visible = False Then
        ' cuando se realizan solo peticiones via web, queda una tarea
        ' de word abierta que no se destruye con appword = nothing
        appexcel.Quit
    End If
    
    Set appword = Nothing
    Set docword = Nothing
    Set appexcel = Nothing
    Set docPetWord = Nothing
    Set docPetExcel = Nothing

    Set oNotificacion = Nothing
    Set oNotifs = Nothing
    Set oPedido = Nothing
    Screen.MousePointer = vbNormal

    Unload Me
    
    g_oOrigen.sdbgMiPedido.RemoveAll
    g_oOrigen.sstabPedidos.Tab = 1
    g_oOrigen.sstabPedidos.Tab = 0
    g_oOrigen.sdbgPedidos.RemoveAll
    g_oOrigen.sdbcGMN1_4Cod = ""
    g_oOrigen.sdbcProceCod = ""
    g_oOrigen.sdbcProceDen = ""
    g_oOrigen.txtCenCoste = ""
    g_oOrigen.txtContrato = ""
    g_oOrigen.LimpiarMiPedido
    g_oOrigen.sstabPedidos.TabEnabled(0) = True
       
    frmPedidosEmision2.Show 1
    
    'Notificar las conversiones de proveedor de QA a real
    For Each oOrden In g_oOrigen.g_oOrdenesTemporales
        NotificarPasoProveedorQAaReal True, sProve:=oOrden.ProveCod, iOrdenAnyo:=oOrden.Anyo, lNumPedido:=oOrden.NumPedido, lOrdenNumero:=oOrden.Numero
    Next
    
    g_oOrigen.VaciarOrdenesTemporales
    
    Exit Sub
Error:
    Select Case err.Number
        Case 5151
            MsgBox err.Description, vbCritical, "Fullstep"
        Case Else
            Resume Next
    End Select
End Sub
Private Sub cmdCancelar_Click()

Unload Me

End Sub

Private Sub cmdDot_Click()
On Error GoTo ErrPlantilla

    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sCap(12) & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtDOT = cmmdDot.filename

    Exit Sub

ErrPlantilla:

    Exit Sub
End Sub
Private Sub cmdMailDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sCap(12) & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtMailDot = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oEmpresas = Nothing
End Sub

Private Sub sdbddConApe_DropDown()
    Screen.MousePointer = vbHourglass
    
    For Each oProve In oProves
        If oProve.Cod = sdbgPet.Columns(0).Value Then
            If bCargarComboDesdeDD Then
             Set oContactos = oProve.CargarTodosLosContactos(sdbgPet.ActiveCell.Value, , , , True)
            Else
                Set oContactos = oProve.CargarTodosLosContactos(, , , , True)
            End If
            CargarGridConContactosApe
        End If
    Next
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbddConNom_DropDown()
    Screen.MousePointer = vbHourglass
    
    For Each oProve In oProves
        If oProve.Cod = sdbgPet.Columns(0).Value Then
            If bCargarComboDesdeDD Then
                Set oContactos = oProve.CargarTodosLosContactos(, sdbgPet.ActiveCell.Value, , True)
                Set oContactos = oProve.CargarTodosLosContactos(, , , True)
            Else
                Set oContactos = oProve.CargarTodosLosContactos(, , , True)
            End If
        
            CargarGridConContactosNom
        End If
    Next
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddConNom_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddConNom.DataFieldList = "Column 0"
    sdbddConNom.DataFieldToDisplay = "Column 0"
    
End Sub

''' <summary>
''' CLose up del combo del nombre de contacto
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbddConNom_CloseUp()

If sdbgPet.Columns(0).Text = "" Then
    sdbgPet.Columns(2).Value = ""
    sdbgPet.Columns(3).Value = ""
    Exit Sub
End If

    If sdbddConNom.Columns(1).Value <> "" Then
        sdbgPet.Columns(3).Value = sdbddConNom.Columns(0).Value
        sdbgPet.Columns(2).Value = sdbddConNom.Columns(1).Value
        sdbgPet.Columns("EMAIL").Value = sdbddConNom.Columns(2).Value
        sdbgPet.Columns("CODPROV").Value = sdbddConNom.Columns(3).Value
        sdbgPet.Columns("TFNO").Value = sdbddConNom.Columns("TFNO").Value
        sdbgPet.Columns("FAX").Value = sdbddConNom.Columns("FAX").Value
        sdbgPet.Columns("TFNO_MOVIL").Value = sdbddConNom.Columns("TFNO_MOVIL").Value

        If sdbddConNom.Columns(2).Value <> "" And gParametrosGenerales.giMail <> 0 Then
            sdbgPet.Columns("MAIL").Value = 1
            sdbgPet.Columns("IMP").Value = 0
        Else
            sdbgPet.Columns("MAIL").Value = 0
            sdbgPet.Columns("IMP").Value = 1
        End If
        
        For Each oNotificacion In g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones
            If Not g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value) Is Nothing Then
                g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).nombre = sdbddConNom.Columns(0).Value
                g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).Apellidos = sdbddConNom.Columns(1).Value
                g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).Email = sdbddConNom.Columns(2).Value
                g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).mail = Int(sdbgPet.Columns("MAIL").Value)
                g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).Carta = Int(sdbgPet.Columns("IMP").Value)
                Exit For
            End If
        Next
    End If

End Sub
''' <summary>
''' CLose up del combo del apellido de contacto
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbddConApe_CloseUp()

If sdbgPet.Columns(0).Text = "" Then
    sdbgPet.Columns(2).Value = ""
    sdbgPet.Columns(3).Value = ""
    Exit Sub
End If
    
    If sdbddConApe.Columns(0).Value <> "" Then
        
        sdbgPet.Columns(2).Value = sdbddConApe.Columns(0).Value
        sdbgPet.Columns(3).Value = sdbddConApe.Columns(1).Value
        sdbgPet.Columns("EMAIL").Value = sdbddConApe.Columns(2).Value
        sdbgPet.Columns("CODPROV").Value = sdbddConApe.Columns(3).Value
        sdbgPet.Columns("TFNO").Value = sdbddConApe.Columns("TFNO").Value
        sdbgPet.Columns("FAX").Value = sdbddConApe.Columns("FAX").Value
        sdbgPet.Columns("TFNO_MOVIL").Value = sdbddConApe.Columns("TFNO_MOVIL").Value

        If sdbddConApe.Columns(2).Value <> "" And gParametrosGenerales.giMail <> 0 Then
        ' contacto con e-mail
            sdbgPet.Columns("MAIL").Value = 1
            sdbgPet.Columns("IMP").Value = 0
        Else
            sdbgPet.Columns("MAIL").Value = 0
            sdbgPet.Columns("IMP").Value = 1
        End If
        
        'Modificar la coleccion
       
        For Each oNotificacion In g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones
            If Not g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value) Is Nothing Then
                g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).Apellidos = sdbddConApe.Columns(0).Value
                g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).nombre = sdbddConApe.Columns(1).Value
                g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).Email = sdbddConApe.Columns(2).Value
                g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).mail = Int(sdbgPet.Columns("MAIL").Value)
                g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).Carta = Int(sdbgPet.Columns("IMP").Value)
                Exit For
            End If
        Next
    End If
    
End Sub
Private Sub sdbddConApe_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddConApe.DataFieldList = "Column 0"
    sdbddConApe.DataFieldToDisplay = "Column 0"
    
End Sub

''' <summary>
''' CLose up del combo del formato
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbddFormato_CloseUp()

If sdbgPet.Columns(0).Value = "" Then Exit Sub

sdbgPet.Columns("FORMATO").Value = sdbddFormato.Columns(0).Value

For Each oNotificacion In g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones
    If Not g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value) Is Nothing Then
        If sdbddFormato.Columns(0).Value = sXML Then g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).FormatoPedido = 1
        If sdbddFormato.Columns(0).Value = sExcel Then g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).FormatoPedido = 2
        Exit For
    End If
Next

End Sub

Private Sub sdbddFormato_DropDown()

sdbgPet.ActiveCell.SelStart = 0
sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
End Sub

Private Sub sdbddFormato_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddFormato.DataFieldList = "Column 0"
    sdbddFormato.DataFieldToDisplay = "Column 0"
    
End Sub

''' <summary>
''' Drop Down del combo del codigo del proveedor
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbddProveCod_DropDown()
    Dim i As Integer
    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada

    Screen.MousePointer = vbHourglass
    
    scodProve = sdbgPet.Columns(0).Value
    sDenProve = sdbgPet.Columns(1).Value
    sApe = sdbgPet.Columns(2).Value
    sNom = sdbgPet.Columns(3).Value
    sFormat = sdbgPet.Columns("FORMATO").Text
    If sdbgPet.Columns("MAIL").Value = True Then bNotifMail = True
    If sdbgPet.Columns("IMP").Value = True Then bNotifCarta = True
    
    If sdbgPet.Columns(0).Value <> "" Then
        For Each oNotificacion In g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones
            For i = 0 To g_oOrigen.g_oOrdenesTemporales.Count
                If iNotificaciones(i) = sdbgPet.Columns("IND").Value Then
                    bNoMofif = True
                End If
            Next
        Next
    End If
    
    sdbddProveCod.RemoveAll
    
    For Each oProve In oProves
        sdbddProveCod.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den
    Next

    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbddProveCod_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddProveCod.DataFieldList = "Column 0"
    sdbddProveCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddProveCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveCod.Rows - 1
            bm = sdbddProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddProveCod_ValidateList(Text As String, RtnPassed As Integer)
    Screen.MousePointer = vbNormal
End Sub
''' <summary>
''' Evento que salta cuando se selecciona un codigo de proveedor
''' </summary>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub sdbddProveCod_CloseUp()

If bNoMofif Then
    bNoMofif = False
    sdbgPet.Columns(0).Value = scodProve
    sdbgPet.Columns(1).Value = sDenProve
    sdbgPet.Columns(2).Value = sApe
    sdbgPet.Columns(3).Value = sNom
    sdbgPet.Columns("FORMATO").Value = sFormat
    If bNotifMail = True Then sdbgPet.Columns("MAIL").Value = True
    If bNotifCarta = True Then sdbgPet.Columns("IMP").Value = True
    bNotifCarta = False
    bNotifMail = False
    Exit Sub
End If

    If sdbddProveCod.Columns(0).Value <> "" Then
        sdbgPet.Columns(1).Value = sdbddProveCod.Columns(1).Value
        sdbgPet.Columns(0).Value = sdbddProveCod.Columns(0).Value
    End If
    
End Sub
''' <summary>
''' Evento que salta cuando se selecciona una denominacion de proveedor
''' </summary>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub sdbddProveDen_CloseUp()

If bNoMofif Then
    bNoMofif = False
    sdbgPet.Columns(0).Value = scodProve
    sdbgPet.Columns(1).Value = sDenProve
    sdbgPet.Columns(2).Value = sApe
    sdbgPet.Columns(3).Value = sNom
    sdbgPet.Columns("FORMATO").Value = sFormat
    If bNotifMail = True Then sdbgPet.Columns("MAIL").Value = True
    If bNotifCarta = True Then sdbgPet.Columns("IMP").Value = True
    bNotifCarta = False
    bNotifMail = False
    Exit Sub
End If

If sdbddProveDen.Columns(0).Value <> "" Then
    sdbgPet.Columns(0).Value = sdbddProveDen.Columns(1).Value
    sdbgPet.Columns(1).Value = sdbddProveDen.Columns(0).Value
End If

End Sub

''' <summary>
''' Drop Down del combo de la denominacion del proveedor
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>

Private Sub sdbddProveDen_DropDown()
    Dim i As Integer
    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    Screen.MousePointer = vbHourglass
    
    sdbddProveDen.RemoveAll
    
    scodProve = sdbgPet.Columns(0).Value
    sDenProve = sdbgPet.Columns(1).Value
    sApe = sdbgPet.Columns(2).Value
    sNom = sdbgPet.Columns(3).Value
    sFormat = sdbgPet.Columns("FORMATO").Value
    If sdbgPet.Columns("MAIL").Value = True Then bNotifMail = True
    If sdbgPet.Columns("IMP").Value = True Then bNotifCarta = True
    
    If sdbgPet.Columns(0).Value <> "" Then
        For Each oNotificacion In g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones
            For i = 0 To g_oOrigen.g_oOrdenesTemporales.Count
                If iNotificaciones(i) = sdbgPet.Columns("IND").Value Then
                    bNoMofif = True
                End If
            Next
        Next
    End If
    
    For Each oProve In oProves
        sdbddProveDen.AddItem oProve.Den & Chr(m_lSeparador) & oProve.Cod
    Next

    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddProveDen_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddProveDen.DataFieldList = "Column 0"
    sdbddProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddProveDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveDen.Rows - 1
            bm = sdbddProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

''' <summary>
''' Cargar del formulario
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
''' <remarks>Revisado por ngo</remarks>
Private Sub Form_Load()

    Dim i As Integer
    Dim j As Integer
    Dim scodProve As String
    Dim sDenProve  As String
    Dim oCon As CContacto
    Dim sFax As String
    Dim sTfno As String
    Dim sTfnoMovil As String
    Dim sFormato As String
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
   '  On Error GoTo error:
    
    Set oFos = New Scripting.FileSystemObject
    
    Set oProve = oFSGSRaiz.generar_CProveedor
    Set oProves = oFSGSRaiz.generar_CProveedores
    Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
    
     
    If Not g_oParametrosSM Is Nothing And frmPEDIDOS.m_bImputaPedido = True Then
        oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , , , , g_oOrigen.sdbcCenEmpresa.Columns("ID").Value
    Else
        oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , , , , g_oOrigen.sdbcCenEmpresa.Columns("ID").Value
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    bNotifCarta = False
    bNotifMail = False
        
    If gParametrosGenerales.giMail = 0 Then
        
        txtMailDot.Enabled = False
        cmdMailDot.Enabled = False
        sdbgPet.Columns("IMP").Locked = True
        'Todas las comunicaciones via carta
        For Each oOrdenEntrega In g_oOrigen.g_oOrdenesTemporales
            i = i + 1
            For Each oNotificacion In g_oOrigen.g_oOrdenesTemporales.Item(i).Notificaciones
                oNotificacion.Carta = True
                oNotificacion.mail = False
                j = j + 1
            Next
        Next
    End If
    
    i = 0
    j = 0
    sdbgPet.Columns(0).DropDownHwnd = sdbddProveCod.hWnd
    sdbgPet.Columns(1).DropDownHwnd = sdbddProveDen.hWnd
    sdbgPet.Columns(2).DropDownHwnd = sdbddConApe.hWnd
    sdbgPet.Columns(3).DropDownHwnd = sdbddConNom.hWnd
    sdbgPet.Columns("FORMATO").DropDownHwnd = sdbddFormato.hWnd
    
    sdbddConApe.AddItem ""
    sdbddConNom.AddItem ""
    sdbddProveCod.AddItem ""
    sdbddProveDen.AddItem ""
    sdbddFormato.AddItem ""
    ReDim Preserve iNotificaciones(g_oOrigen.g_oOrdenesTemporales.Count)
    
    For Each oOrdenEntrega In g_oOrigen.g_oOrdenesTemporales
        j = j + 1
         
        For Each oNotificacion In g_oOrigen.g_oOrdenesTemporales.Item(j).Notificaciones
            oProve.Cod = oNotificacion.CodProve
            Set oContactos = oProve.CargarTodosLosContactos(, , , True)
            'a�adir las column de fax,tfno,movil,
            For Each oCon In oContactos
                If oCon.Apellidos = oNotificacion.Apellidos Then
                    sTfnoMovil = oCon.tfnomovil
                    sTfno = oCon.Tfno
                    sFax = oCon.Fax
                End If
            
            Next
            
            If oNotificacion.FormatoPedido = 0 Then sFormato = ""
            If oNotificacion.FormatoPedido = 1 Then sFormato = sXML
            If oNotificacion.FormatoPedido = 2 Then sFormato = sExcel
            
            sdbgPet.AddItem oNotificacion.CodProve & Chr(m_lSeparador) & oNotificacion.DenProve & Chr(m_lSeparador) & oNotificacion.Apellidos & Chr(m_lSeparador) & oNotificacion.nombre & Chr(m_lSeparador) & sFormato & Chr(m_lSeparador) & oNotificacion.mail & Chr(m_lSeparador) & oNotificacion.Carta & Chr(m_lSeparador) & True & Chr(m_lSeparador) & oNotificacion.Email & Chr(m_lSeparador) & oNotificacion.indice & Chr(m_lSeparador) & oNotificacion.CodProve & Chr(m_lSeparador) & sTfnoMovil & Chr(m_lSeparador) & sFax & Chr(m_lSeparador) & sTfno
            
            scodProve = oNotificacion.CodProve
            sDenProve = oNotificacion.DenProve
            iNotificaciones(i) = oNotificacion.indice
            i = i + 1
        Next
        oProves.Add scodProve, sDenProve
    Next
    
    j = 0

'     PLANTILLAS PARA CARTAS

    If gParametrosInstalacion.gsPedidoDirEmisionCarta = "" Then
        If gParametrosGenerales.gsPEDDEMICARTADOT = "" Then
            oMensajes.NoValido sCap(13)
        Else
            gParametrosInstalacion.gsPedidoDirEmisionCarta = gParametrosGenerales.gsPEDDEMICARTADOT
            g_GuardarParametrosIns = True
        End If
    End If
    
'     PLANTILLAS PARA MAIL

    If gParametrosInstalacion.gsPedidoDirEmisionMail = "" Then
        If gParametrosGenerales.gsPEDDEMIMAILDOT = "" Then
            oMensajes.NoValido sCap(14)
        Else
            gParametrosInstalacion.gsPedidoDirEmisionMail = gParametrosGenerales.gsPEDDEMIMAILDOT
            g_GuardarParametrosIns = True
        End If
    End If
    Dim sCarpeta As String
    Dim sMarcaPlantillas As String
    If InStr(gParametrosInstalacion.gsPedidoDirEmisionCarta, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsPedidoDirEmisionMail, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
        'Buscamos la empresa CARPETA_PLANTILLAS
        sCarpeta = oEmpresas.Item(1).Carpeta_Plantillas
    End If
    If sCarpeta = "" And gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    txtDOT = Replace(gParametrosInstalacion.gsPedidoDirEmisionCarta, sMarcaPlantillas, sCarpeta)
    txtMailDot = Replace(gParametrosInstalacion.gsPedidoDirEmisionMail, sMarcaPlantillas, sCarpeta)

    'Obtiene el n� de decimales a mostrar en esta pantalla
    sFormatoNumber = "#,##0."
    For i = 1 To gParametrosInstalacion.giNumDecimalesComp
        sFormatoNumber = sFormatoNumber & "0"
    Next i
    
    Exit Sub

Error:

    MsgBox err.Description

    
End Sub
Private Sub CargarGridConContactosApe()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oCon As CContacto
    
    sdbddConApe.RemoveAll
    
    For Each oCon In oContactos
        sdbddConApe.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.Tfno
    Next
    
    If sdbddConApe.Rows = 0 Then
        sdbddConApe.AddItem ""
    End If
    
    sdbddConApe.MoveFirst
    
End Sub
Private Sub CargarGridConContactosNom()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oCon As CContacto
    
    sdbddConNom.RemoveAll
    
    For Each oCon In oContactos
        sdbddConNom.AddItem oCon.nombre & Chr(m_lSeparador) & oCon.Apellidos & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.Tfno
    Next
    
    If sdbddConNom.Rows = 0 Then
        sdbddConNom.AddItem ""
    End If
    
    sdbddConNom.MoveFirst
    
End Sub

Private Sub sdbgPet_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

DispPromptMsg = 0

If sdbgPet.Columns("NOBORRAR").Value = True Then sdbgPet.SelBookmarks.RemoveAll: Exit Sub
If sdbgPet.Rows = 0 Then Exit Sub
If sdbgPet.SelBookmarks.Count > 1 Then sdbgPet.SelBookmarks.RemoveAll: Exit Sub
If sdbgPet.SelBookmarks.Count = 0 Then Exit Sub




End Sub

''' <summary>
''' Guarda los cambios producidos el grid
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>

Private Sub sdbgPet_BeforeUpdate(Cancel As Integer)

If g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROV").Value) Is Nothing Then Exit Sub

For Each oNotificacion In g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROV").Value).Notificaciones
    If Not g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROV").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value) Is Nothing Then
        sdbgPet.Columns("CODPROVE").Value = g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROV").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).CodProve
        sdbgPet.Columns("DENPROVE").Value = g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROV").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).DenProve
    End If
Next


End Sub

''' <summary>
''' Evento que salta al producirse un cambio en el grid
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbgPet_Change()

If sdbgPet.Columns("CODPROVE").Text = "" Then
    sdbgPet.Columns("MAIL").Value = False
    sdbgPet.Columns("IMP").Value = False
    Exit Sub
End If

Select Case sdbgPet.col

    Case 0, 1
        
        sdbgPet.Columns(2).Value = ""
        sdbgPet.Columns(3).Value = ""
        sdbgPet.Columns("FORMATO").Value = ""
        sdbgPet.Columns("MAIL").Value = 0
        sdbgPet.Columns("IMP").Value = 0
        sdbgPet.Columns("EMAIL").Value = ""
        
        Exit Sub
End Select

If sdbgPet.Columns("CODPROVE").Value <> "" Then

    If sdbgPet.Columns(sdbgPet.col).Name = "MAIL" Then
        If sdbgPet.Columns("MAIL").Value = -1 Or sdbgPet.Columns("MAIL").Value = 1 Then
            For Each oNotificacion In g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones
                If Not g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value) Is Nothing Then
                    g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).Carta = False
                    g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).mail = True
    
                    Exit For
                End If
            Next
            sdbgPet.Columns("IMP").Value = False
        Else
            sdbgPet.Columns("MAIL").Value = True
        End If
    End If
    
    
    If sdbgPet.Columns(sdbgPet.col).Name = "IMP" Then
        If sdbgPet.Columns("IMP").Value = -1 Or sdbgPet.Columns("IMP").Value = 1 Then
            For Each oNotificacion In g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones
                If Not g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value) Is Nothing Then
                    g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).Carta = True
                    g_oOrigen.g_oOrdenesTemporales.Item(sdbgPet.Columns("CODPROVE").Value).Notificaciones.Item(sdbgPet.Columns("IND").Value).mail = False
    
                    Exit For
                End If
            Next
            sdbgPet.Columns("MAIL").Value = False
        Else
            sdbgPet.Columns("IMP").Value = True
        End If
    End If

Else

'a�adir a la coleccion
End If
End Sub


Private Sub sdbgPet_InitColumnProps()
    sdbgPet.Columns(0).Locked = True
    sdbgPet.Columns(1).Locked = True
    sdbgPet.Columns(2).Locked = True
    sdbgPet.Columns(3).Locked = True
End Sub




