VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Begin VB.Form frmPRESArt 
   Caption         =   "DPresupuestos por material"
   ClientHeight    =   8040
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11280
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPRESArt.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   8040
   ScaleWidth      =   11280
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7560
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   22
      ImageHeight     =   22
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPRESArt.frx":014A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      Height          =   400
      Left            =   120
      ScaleHeight     =   405
      ScaleWidth      =   9615
      TabIndex        =   3
      Top             =   7600
      Width           =   9615
      Begin VB.CommandButton cmdListado 
         Caption         =   "DListado"
         Height          =   345
         Left            =   6355
         TabIndex        =   8
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "DRestaurar"
         Height          =   345
         Left            =   5275
         TabIndex        =   7
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdCargarObj 
         Caption         =   "DCargar objetivos"
         Enabled         =   0   'False
         Height          =   345
         Left            =   3600
         TabIndex        =   6
         Top             =   0
         Width           =   1600
      End
      Begin VB.CommandButton cmdCargarConsumo 
         Caption         =   "DCargar consumo"
         Enabled         =   0   'False
         Height          =   345
         Left            =   1925
         TabIndex        =   5
         Top             =   0
         Width           =   1600
      End
      Begin VB.CommandButton cmdCargarPres 
         Caption         =   "DCargar presupuestos"
         Enabled         =   0   'False
         Height          =   345
         Left            =   0
         TabIndex        =   4
         Top             =   0
         Width           =   1850
      End
   End
   Begin VB.CommandButton cmdModoEdicion 
      Caption         =   "&Edici�n"
      Height          =   345
      Left            =   10150
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   7600
      Width           =   1005
   End
   Begin UltraGrid.SSUltraGrid sdbgUltraPres 
      Height          =   7075
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   11055
      _ExtentX        =   19500
      _ExtentY        =   12488
      _Version        =   131072
      GridFlags       =   17040384
      LayoutFlags     =   68419584
      Bands           =   "frmPRESArt.frx":02A2
      Override        =   "frmPRESArt.frx":2C78
      Caption         =   "sdbgUltraPres"
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
      Height          =   285
      Left            =   1000
      TabIndex        =   9
      Top             =   120
      Width           =   960
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   1693
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1693
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16777215
   End
   Begin VB.Label lblAnyo 
      Caption         =   "DA�o:"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   160
      Width           =   735
   End
End
Attribute VB_Name = "frmPRESArt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

''' Coleccion de Presupuestos
Public m_oPresupuestos As CPresPorMateriales

' Variables de seguridad
Public bRMat As Boolean
Private bModif As Boolean

'Control de flujo
Private Accion As AccionesSummit
Private m_bModoEdicion As Boolean

Private m_iAnyoSeleccionado As Integer
Private m_bCalculando As Boolean

'Variables para los idiomas
Private m_sIdiConsulta As String
Private m_sIdiEdicion As String
Private m_sTitConsulta As String
Private m_sTitEdicion As String
Private m_sTitulo As String

Private Sub cmdCargarConsumo_Click()
    
    CargarConsumo
    
End Sub

Private Sub cmdCargarObj_Click()
    Dim irespuesta As Integer
    
    'Si no hay filas seleccionadas,selecciona la activa
    If sdbgUltraPres.Selected.Rows.Count = 0 Then
        sdbgUltraPres.Selected.Rows.Add sdbgUltraPres.ActiveRow
    End If

    If sdbgUltraPres.ActiveRow.GetParent.Cells("PRESDIRECTAV").value = 1 Then
        irespuesta = oMensajes.ExistePresupuestoDirectos()
        If irespuesta = vbNo Then
            Exit Sub
        End If
    End If
    
    frmPRESArtCargaObj.Show vbModal
    
End Sub

Private Sub cmdCargarPres_Click()
    
    PopupMenu MDI.mnuPopUpPresArtPres, , picNavigate.Left + cmdCargarPres.Width, picNavigate.Top + 150
End Sub


Private Sub cmdlistado_Click()
    If Not IsEmpty(m_iAnyoSeleccionado) Then
        frmLstPRESPorMat.iAnyoSeleccionado = m_iAnyoSeleccionado
        frmLstPRESPorMat.sdbcAnyo = m_iAnyoSeleccionado
    End If
        
    frmLstPRESPorMat.PonerMatSeleccionadoEnPres
    
    frmLstPRESPorMat.Show vbModal
End Sub

Private Sub cmdModoEdicion_Click()
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not m_bModoEdicion Then
        sdbgUltraPres.Bands.Item(3).Override.AllowUpdate = ssAllowUpdateYes
        If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.PresArt) Then
            sdbgUltraPres.Bands.Item(4).Override.AllowUpdate = ssAllowUpdateNo
        Else
            sdbgUltraPres.Bands.Item(4).Override.AllowUpdate = ssAllowUpdateYes
        End If
        
        Me.caption = m_sTitulo & " " & m_sTitEdicion
        
        cmdModoEdicion.caption = m_sIdiConsulta
        
        cmdRestaurar.Visible = False
        cmdListado.Visible = False
            
        m_bModoEdicion = True
        
        Accion = ACCPresMatCon
    
    Else

        sdbgUltraPres.Bands.Item(3).Override.AllowUpdate = ssAllowUpdateNo
        sdbgUltraPres.Bands.Item(4).Override.AllowUpdate = ssAllowUpdateNo
        
        cmdRestaurar.Visible = True
        cmdListado.Visible = True
    
        Me.caption = m_sTitulo & " " & m_sTitConsulta
        
        cmdModoEdicion.caption = m_sIdiEdicion
        
        m_bModoEdicion = False
        
    End If
    
    If Me.Visible Then sdbgUltraPres.SetFocus
End Sub

Private Sub cmdRestaurar_Click()
    Set m_oPresupuestos = Nothing
    Set m_oPresupuestos = oFSGSRaiz.Generar_CPresPorMateriales
    
    CargarGridPresupuestos
    sdbgUltraPres.CollapseAll
    
End Sub

Private Sub Form_Load()
    Me.Width = 11400
    Me.Height = 8550
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarAnyos
    m_iAnyoSeleccionado = Year(Date)
        
    ConfigurarSeguridad
    
    ' Incializar la coleccion de presupuestos
    Set m_oPresupuestos = oFSGSRaiz.Generar_CPresPorMateriales

    CargarGridPresupuestos
    sdbgUltraPres.CollapseAll
    
    'Carga el layout de la ultragrid
    sdbgUltraPres.Layout.Load App.Path & "\materiales.ugd", ssPersistenceTypeFile, ssPropCatBands + ssPropCatGroups + ssPropCatGeneral + ssPropCatUnboundColumns + ssPropCatAppearanceCollection + ssPropCatGeneral
    
    CargarRecursos

End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRES_ART, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then

        m_sTitulo = Ador(0).value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).value  '2 A�o:
        
        Ador.MoveNext
        sdbgUltraPres.Bands(0).Columns("PRESV").Header.caption = Ador(0).value   '3 presupuesto
        sdbgUltraPres.Bands(1).Columns("PRESV").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(2).Columns("PRESV").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(3).Columns("PRESV").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(4).Columns("PRESV").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(4).Columns("PRESTOT").Header.caption = Ador(0).value
        
        Ador.MoveNext
        sdbgUltraPres.Bands(0).Columns("OBJV").Header.caption = Ador(0).value   '4 objetivo
        sdbgUltraPres.Bands(1).Columns("OBJV").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(2).Columns("OBJV").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(3).Columns("OBJV").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(4).Columns("OBJV").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(4).Columns("OBJTOT").Header.caption = Ador(0).value
        
        Ador.MoveNext
        sdbgUltraPres.Bands(0).Columns("AHORRO").Header.caption = Ador(0).value  '5 ahorro
        sdbgUltraPres.Bands(1).Columns("AHORRO").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(2).Columns("AHORRO").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(3).Columns("AHORRO").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(4).Columns("AHORRO").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(4).Columns("AHORROTOT").Header.caption = Ador(0).value
        
        Ador.MoveNext
        sdbgUltraPres.Bands(0).Columns("PORCEN").Header.caption = Ador(0).value  '6 %Ahorro
        sdbgUltraPres.Bands(1).Columns("PORCEN").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(2).Columns("PORCEN").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(3).Columns("PORCEN").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(4).Columns("PORCEN").Header.caption = Ador(0).value
        sdbgUltraPres.Bands(4).Columns("PORCENTOT").Header.caption = Ador(0).value
        
        Ador.MoveNext
        sdbgUltraPres.Bands(4).Columns("COD").Header.caption = Ador(0).value  '7 c�digo
        
        Ador.MoveNext
        sdbgUltraPres.Bands(4).Columns("DEN").Header.caption = Ador(0).value '8 denominaci�n
        
        Ador.MoveNext
        sdbgUltraPres.Bands(4).Columns("UNI").Header.caption = Ador(0).value   '9 Unidad
        sdbgUltraPres.Bands(5).Columns("UNI").Header.caption = Ador(0).value
        
        Ador.MoveNext
        sdbgUltraPres.Bands(4).Groups(0).Header.caption = Ador(0).value  '10 art�culos
        
        Ador.MoveNext
        sdbgUltraPres.Bands(4).Groups(1).Header.caption = Ador(0).value  '11 Presupuestos unitarios
        
        Ador.MoveNext
        sdbgUltraPres.Bands(4).Groups(2).Header.caption = Ador(0).value   '12 Presupuestos totales
        
        Ador.MoveNext
        cmdCargarPres.caption = Ador(0).value
        Ador.MoveNext
        cmdCargarConsumo.caption = Ador(0).value
        Ador.MoveNext
        cmdCargarObj.caption = Ador(0).value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).value
        Ador.MoveNext
        cmdListado.caption = Ador(0).value
        Ador.MoveNext
        cmdModoEdicion.caption = Ador(0).value
        m_sIdiEdicion = Ador(0).value
        
        Ador.MoveNext
        sdbgUltraPres.Bands(4).Columns("CANTV").Header.caption = Ador(0).value   '19 Cantidad
        sdbgUltraPres.Bands(5).Columns("CANT").Header.caption = Ador(0).value
        
        Ador.MoveNext
        m_sTitConsulta = Ador(0).value  '20 (Consulta)
        Ador.MoveNext
        m_sTitEdicion = Ador(0).value '21 (Edici�n)
        Ador.MoveNext
        m_sIdiConsulta = Ador(0).value '22 &Consulta
        
        Ador.MoveNext
        sdbgUltraPres.Bands(5).Columns("DEST").Header.caption = Ador(0).value  '23 Dest.
        Ador.MoveNext
        sdbgUltraPres.Bands(5).Columns("CODPROVE").Header.caption = Ador(0).value  '24 Proveedor
        Ador.MoveNext
        sdbgUltraPres.Bands(5).Columns("DENPROVE").Header.caption = Ador(0).value  '25 Nombre proveedor
        Ador.MoveNext
        sdbgUltraPres.Bands(5).Columns("PREC").Header.caption = Ador(0).value  '26 Precio
        Ador.MoveNext
        sdbgUltraPres.Bands(5).Columns("PORCEN").Header.caption = Ador(0).value  '27 %
        Ador.MoveNext
        sdbgUltraPres.Bands(5).Columns("FECINI").Header.caption = Ador(0).value  '28 F.Inicio
        Ador.MoveNext
        sdbgUltraPres.Bands(5).Columns("FECFIN").Header.caption = Ador(0).value  '29 F.fin
        Ador.MoveNext
        sdbgUltraPres.Bands(5).Groups(0).Header.caption = Ador(0).value  '30 �ltimas adjudicaciones
        
        Ador.Close

    End If

    Set Ador = Nothing

    
    sdbgUltraPres.Bands(0).Columns("DEN").Header.caption = basParametros.gParametrosGenerales.gsDEN_GMN1
    sdbgUltraPres.Bands(1).Columns("DEN").Header.caption = basParametros.gParametrosGenerales.gsDEN_GMN2
    sdbgUltraPres.Bands(2).Columns("DEN").Header.caption = basParametros.gParametrosGenerales.gsDEN_GMN3
    sdbgUltraPres.Bands(3).Columns("DEN").Header.caption = basParametros.gParametrosGenerales.gsDEN_GMN4
    
    Me.caption = m_sTitulo & " " & m_sTitConsulta
End Sub

Private Sub ConfigurarSeguridad()

    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
    
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorMATModificar)) Is Nothing) Then
            bModif = True
        End If
        
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorMATRestMatComp)) Is Nothing) Then
            bRMat = True
        End If
        
    Else
        bModif = True
    End If
    
    If Not bModif Then
        cmdModoEdicion.Visible = False
    
        cmdCargarConsumo.Visible = False
        cmdCargarObj.Visible = False
        cmdCargarPres.Visible = False
        
        cmdRestaurar.Left = cmdCargarPres.Left
        cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 75
        
    Else
        If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.PresArt) Then
            cmdCargarConsumo.Visible = False
            cmdCargarObj.Visible = False
            cmdCargarPres.Visible = False
            cmdRestaurar.Left = cmdCargarPres.Left
            cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 75
        End If
    End If

    
End Sub

Private Sub Form_Resize()
    If Me.Width < 500 Then Exit Sub
    If Me.Height < 2000 Then Exit Sub
    
    sdbgUltraPres.Width = Me.Width - 405
    sdbgUltraPres.Height = Me.Height - 1475
    
    picNavigate.Top = sdbgUltraPres.Top + sdbgUltraPres.Height + 70
    cmdModoEdicion.Top = picNavigate.Top
    cmdModoEdicion.Left = sdbgUltraPres.Left + sdbgUltraPres.Width - cmdModoEdicion.Width
    
    If sdbgUltraPres.DataSource Is Nothing Then Exit Sub
    
    sdbgUltraPres.Bands(0).Columns("COD").Width = sdbgUltraPres.Width / 6
    sdbgUltraPres.Bands(0).Columns("DEN").Width = sdbgUltraPres.Width / 3.5
    sdbgUltraPres.Bands(0).Columns("PRESV").Width = sdbgUltraPres.Width / 9
    sdbgUltraPres.Bands(0).Columns("OBJV").Width = sdbgUltraPres.Width / 9
    sdbgUltraPres.Bands(0).Columns("AHORRO").Width = sdbgUltraPres.Width / 9
    sdbgUltraPres.Bands(0).Columns("PORCEN").Width = sdbgUltraPres.Width / 9
    
'''    sdbgUltraPres.Bands(4).Columns("COD").Width = sdbgUltraPres.Width / 12
'''    sdbgUltraPres.Bands(4).Columns("DEN").Width = sdbgUltraPres.Width / 5
'''    sdbgUltraPres.Bands(4).Columns("UNI").Width = sdbgUltraPres.Width / 15
'''    sdbgUltraPres.Bands(4).Columns("CANTV").Width = sdbgUltraPres.Width / 12
'''    sdbgUltraPres.Bands(4).Columns("PRESV").Width = sdbgUltraPres.Width / 12
'''    sdbgUltraPres.Bands(4).Columns("PRESTOT").Width = sdbgUltraPres.Width / 12
'''    sdbgUltraPres.Bands(4).Columns("OBJV").Width = sdbgUltraPres.Width / 12
'''    sdbgUltraPres.Bands(4).Columns("OBJTOT").Width = sdbgUltraPres.Width / 12
'''    sdbgUltraPres.Bands(4).Columns("AHORRO").Width = sdbgUltraPres.Width / 12
'''    sdbgUltraPres.Bands(4).Columns("AHORROTOT").Width = sdbgUltraPres.Width / 12
'''    sdbgUltraPres.Bands(4).Columns("PORCEN").Width = sdbgUltraPres.Width / 14
'''    sdbgUltraPres.Bands(4).Columns("PORCENTOT").Width = sdbgUltraPres.Width / 14
End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer
    
    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 10
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    m_bModoEdicion = False
    Set m_oPresupuestos = Nothing
    Me.Visible = False
End Sub

Private Sub sdbcAnyo_Click()
        
    If sdbcAnyo.value <> m_iAnyoSeleccionado Then
        m_iAnyoSeleccionado = Int(sdbcAnyo.value)
        
        CargarGridPresupuestos
        sdbgUltraPres.CollapseAll
    End If
    
End Sub

Private Sub sdbcAnyo_CloseUp()

    If sdbcAnyo.value <> m_iAnyoSeleccionado Then
        m_iAnyoSeleccionado = Int(sdbcAnyo.value)
        
        CargarGridPresupuestos
        sdbgUltraPres.CollapseAll
    End If
    
End Sub

Private Sub CargarGridPresupuestos()
    
    'Cargamos los presupuestos para el a�o seleccionado
    
    Select Case gParametrosGenerales.giNEM

        Case 1

        Case 2

        Case 3

        Case 4

            Screen.MousePointer = vbHourglass
            If bRMat Then
                Set sdbgUltraPres.DataSource = m_oPresupuestos.DevolverEstructuraDeMateriales(m_iAnyoSeleccionado, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.PresArt))
            Else
                Set sdbgUltraPres.DataSource = m_oPresupuestos.DevolverEstructuraDeMateriales(m_iAnyoSeleccionado, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.PresArt))
            End If
            Screen.MousePointer = vbNormal

    End Select
    
End Sub


Private Sub sdbgUltraPres_AfterCellUpdate(ByVal Cell As UltraGrid.SSCell)
    Dim teserror As TipoErrorSummit
    Dim oPresupuesto As CPresPorMaterial
    Dim oIBaseDatos As IBaseDatos
    Dim v As Variant
    Dim bInsertar As Boolean
    Dim sRegistro As String
    Dim bEstilo As Boolean
    Dim iColumna As Integer
    
    If Cell.DataChanged = False Then Exit Sub
    
    If m_bCalculando = True Then Exit Sub
    
    If Cell.Row.Cells("CARGADO").value <> 1 Then Exit Sub
    
    'Si no es una columna de pres,obj,cant o totales no hace nada
    If Cell.Column.key <> "PRESV" And Cell.Column.key <> "OBJV" And Cell.Column.key <> "PRESTOT" _
        And Cell.Column.key <> "OBJTOT" And Cell.Column.key <> "CANTV" And Cell.Column.key <> "AHORRO" _
        And Cell.Column.key <> "PORCEN" And Cell.Column.key <> "AHORROTOT" And Cell.Column.key <> "PORCENTOT" Then
        Exit Sub
    End If
    
    m_bCalculando = True
    
    'Calcula el ahorro y los totales y los valores de las filas padres
    iColumna = 0
    Select Case Cell.Column.key
        Case "PRESTOT"
            iColumna = 1
        Case "OBJTOT"
            iColumna = 2
        Case "AHORRO"
            iColumna = 3
        Case "PORCEN"
            iColumna = 4
        Case "AHORROTOT"
            iColumna = 5
        Case "PORCENTOT"
            iColumna = 6
        Case "CANTV"
            iColumna = 7
    End Select
    ActualizarTotales Cell.Row, iColumna
    
    
    'Almacena en BD:
    teserror.NumError = TESnoerror
    
    Set oPresupuesto = oFSGSRaiz.Generar_CPresPorMaterial
    oPresupuesto.Usuario = basOptimizacion.gvarCodUsuario
    
    oPresupuesto.Anyo = m_iAnyoSeleccionado
    oPresupuesto.CodGMN1 = Cell.Row.Cells("GMN1").value
    oPresupuesto.CodGMN2 = Cell.Row.Cells("GMN2").value
    oPresupuesto.CodGMN3 = Cell.Row.Cells("GMN3").value

    If Cell.Row.Band.key = "gmn4" Then
        oPresupuesto.codGMN4 = Cell.Row.Cells("COD").value
        oPresupuesto.Articulo = Null
    Else
        oPresupuesto.codGMN4 = Cell.Row.Cells("GMN4").value
        oPresupuesto.Articulo = Cell.Row.Cells("COD").value
        oPresupuesto.Cantidad = StrToNull(Cell.Row.Cells("CANTV").value)
    End If

    oPresupuesto.Presupuesto = StrToNull(Cell.Row.Cells("PRESV").value)
    oPresupuesto.Objetivo = StrToNull(Cell.Row.Cells("OBJV").value)
    
    Set oIBaseDatos = oPresupuesto
    
'''    If Cell.Row.Cells("ANYOV").Value = "" Then
'''        bInsertar = True
'''    Else
'''        bInsertar = False
'''    End If
    If oIBaseDatos.ComprobarExistenciaEnBaseDatos = True Then
        bInsertar = False
    Else
        bInsertar = True
    End If
    
    'Almacena en BD:
    If bInsertar = True Then
        'INSERTA EN BD
        teserror = oIBaseDatos.AnyadirABaseDatos
    Else
        'MODIFICA EN BD
        teserror = oIBaseDatos.FinalizarEdicionModificando
    End If
    
    If teserror.NumError <> TESnoerror Then
        v = sdbgUltraPres.ActiveCell.value
        TratarError teserror
        If Me.Visible Then sdbgUltraPres.SetFocus
        sdbgUltraPres.ActiveCell.value = v
        Set oIBaseDatos = Nothing
        Set oPresupuesto = Nothing
        Accion = ACCPresMatCon
        m_bCalculando = False
        Exit Sub
        
    Else
        'Registro de acciones
        If IsNull(oPresupuesto.Articulo) Then
            sRegistro = "Anyo:" & CStr(m_iAnyoSeleccionado) & " CodGMN1:" & oPresupuesto.CodGMN1 & " CodGMN2:" & oPresupuesto.CodGMN2 & " CodGMN3:" & oPresupuesto.CodGMN3 & " CodGMN4:" & oPresupuesto.codGMN4
            If Cell.Row.Cells("PRESDIRECTAV").value = 0 Then bEstilo = True
            Cell.Row.Cells("PRESDIRECTAV").value = 1
        Else
            sRegistro = "Anyo:" & CStr(m_iAnyoSeleccionado) & " CodGMN1:" & oPresupuesto.CodGMN1 & " CodGMN2:" & oPresupuesto.CodGMN2 & " CodGMN3:" & oPresupuesto.CodGMN3 & " CodGMN4:" & oPresupuesto.codGMN4 & " Art:" & oPresupuesto.Articulo
            If Cell.Row.GetParent.Cells("PRESDIRECTAV").value = 1 And Cell.Column.key <> "CANTV" Then
                If Cell.Row.Cells("PRESV").value <> "" Or Cell.Row.Cells("OBJV").value <> "" Then
                    bEstilo = True
                    Cell.Row.GetParent.Cells("PRESDIRECTAV").value = 0
                End If
            End If
        End If
        
        If bInsertar = True Then
            basSeguridad.RegistrarAccion AccionesSummit.ACCPresMatAnya, sRegistro
            Cell.Row.Cells("ANYOV").value = m_iAnyoSeleccionado
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCPresMatMod, sRegistro
        End If
        If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.PresArt) Then
            Cell.Row.Cells("INT").Appearance = "Rojo"
        End If
            
    End If
    
    If bEstilo = True Then
        ActualizarEstilo Cell.Row
    End If
        
    Set oIBaseDatos = Nothing
    Set oPresupuesto = Nothing
    Accion = ACCPresMatCon
    
    m_bCalculando = False
    
End Sub

Private Sub sdbgUltraPres_BeforeCellUpdate(ByVal Cell As UltraGrid.SSCell, NewValue As Variant, ByVal Cancel As UltraGrid.SSReturnBoolean)
    'aqu� se har�an las comprobaciones antes de guardar a BD:
    Dim irespuesta As Integer
    Dim oPresupuesto As CPresPorMaterial
    
    If Cell.DataChanged = False Then Exit Sub
    
    If m_bCalculando = True Then Exit Sub
    
    If Cell.Row.Cells("CARGADO").value <> 1 Then Exit Sub
    
    'Si no es una columna de pres,obj,cant o totales no hace nada
    If Cell.Column.key <> "PRESV" And Cell.Column.key <> "OBJV" And Cell.Column.key <> "PRESTOT" _
        And Cell.Column.key <> "OBJTOT" And Cell.Column.key <> "CANTV" And Cell.Column.key <> "PRESDIRECTAV" _
        And Cell.Column.key <> "AHORRO" And Cell.Column.key <> "PORCEN" And Cell.Column.key <> "AHORROTOT" And Cell.Column.key <> "PORCENTOT" Then
        Exit Sub
    End If
    
    If IsNull(NewValue) Then
        NewValue = ""
    Else
        If Not IsNumeric(NewValue) And NewValue <> "" Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    Select Case Cell.Row.Band.Index
        Case 3
            'nivel gmn4
            If Cell.Row.Cells("PRESDIRECTAV").value <> 1 Then
                'comprueba si tiene art�culos con presupuestos asignados:
                Set oPresupuesto = oFSGSRaiz.Generar_CPresPorMaterial

                oPresupuesto.Anyo = m_iAnyoSeleccionado
                oPresupuesto.CodGMN1 = Cell.Row.Cells("GMN1").value
                oPresupuesto.CodGMN2 = Cell.Row.Cells("GMN2").value
                oPresupuesto.CodGMN3 = Cell.Row.Cells("GMN3").value
                oPresupuesto.codGMN4 = Cell.Row.Cells("COD").value
                oPresupuesto.Articulo = Null
                
                If oPresupuesto.ExistenPresupuestosArticulos = True Then
                    irespuesta = oMensajes.ExistePresupuestoArticulos()
                    If irespuesta = vbNo Then
                        Cancel = True
                        Exit Sub
                    End If
                End If
                Set oPresupuesto = Nothing
            End If
        
        Case 4
            'nivel art�culo
            If Cell.Row.GetParent.Cells("PRESDIRECTAV").value = 1 And Cell.Column.key <> "CANTV" Then
                If Not Cell.Row.GetParent.Cells("PRESV").value = "" And Not Cell.Row.GetParent.Cells("OBJV").value = "" Then
                    irespuesta = oMensajes.ExistePresupuestoDirectos()
                    If irespuesta = vbNo Then
                        Cancel = True
                        Exit Sub
                    End If
                End If
            End If
        
    End Select
    
    
End Sub

Private Sub sdbgUltraPres_BeforeRowActivate(ByVal Row As UltraGrid.SSRow)
    If Row.Band.Index = 4 Then
        cmdCargarConsumo.Enabled = True
        cmdCargarObj.Enabled = True
        cmdCargarPres.Enabled = True
    Else
        cmdCargarConsumo.Enabled = False
        cmdCargarObj.Enabled = False
        cmdCargarPres.Enabled = False
    End If
End Sub

Private Sub sdbgUltraPres_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
    Dim i As Integer
    
    'Inicializa el layout de la grid
    
    For i = 0 To 3
        sdbgUltraPres.Bands.Item(i).Override.AllowUpdate = ssAllowUpdateNo
        
        sdbgUltraPres.Bands(i).Columns("PRESV").Format = "#,##0.00"
        sdbgUltraPres.Bands(i).Columns("OBJV").Format = "#,##0.00"
        sdbgUltraPres.Bands(i).Columns("AHORRO").Format = "#,##0.00"
        sdbgUltraPres.Bands(i).Columns("PORCEN").Format = "#,##0.00\%"
    Next i
    
    sdbgUltraPres.Bands.Item(4).Override.AllowUpdate = ssAllowUpdateNo
    
    sdbgUltraPres.Bands(4).Columns("CANTV").Format = "#,##0.00"
    sdbgUltraPres.Bands(4).Columns("PRESV").Format = "#,##0.00"
    sdbgUltraPres.Bands(4).Columns("PRESTOT").Format = "#,##0.00"
    sdbgUltraPres.Bands(4).Columns("OBJV").Format = "#,##0.00"
    sdbgUltraPres.Bands(4).Columns("OBJTOT").Format = "#,##0.00"
    sdbgUltraPres.Bands(4).Columns("AHORRO").Format = "#,##0.00"
    sdbgUltraPres.Bands(4).Columns("AHORROTOT").Format = "#,##0.00"
    sdbgUltraPres.Bands(4).Columns("PORCEN").Format = "#,##0.00\%"
    sdbgUltraPres.Bands(4).Columns("PORCENTOT").Format = "#,##0.00\%"
    sdbgUltraPres.Bands(5).Columns("PREC").Format = "#,##0.00"
    sdbgUltraPres.Bands(5).Columns("CANT").Format = "#,##0.00"
    sdbgUltraPres.Bands(5).Columns("PORCEN").Format = "#,##0.00\%"
    
    With Layout.Appearances.Add("Blanco")
        .BackColor = RGB(255, 255, 255)
    End With
    With Layout.Appearances.Add("Amarillo")
        .BackColor = RGB(255, 255, 223)
    End With
    With Layout.Appearances.Add("Gris")
        .BackColor = RGB(223, 223, 223)
    End With
    With Layout.Appearances.Add("Rojo")
        .BackColor = RGB(255, 0, 0)
    End With
    With Layout.Appearances.Add("GrisFila")
        .BackColor = &HC0C0C0
    End With
    
    sdbgUltraPres.Bands.Item(0).Override.ActiveRowAppearance.BackColor = -1
    
    sdbgUltraPres.Bands(5).Columns("DEST").Activation = ssActivationAllowEdit
    sdbgUltraPres.Bands(5).Columns("UNI").Activation = ssActivationAllowEdit
    sdbgUltraPres.Bands(5).Columns("CODPROVE").Activation = ssActivationAllowEdit
    sdbgUltraPres.Bands(5).Columns("DENPROVE").Activation = ssActivationAllowEdit
    sdbgUltraPres.Bands(5).Columns("PREC").Activation = ssActivationAllowEdit
    sdbgUltraPres.Bands(5).Columns("CANT").Activation = ssActivationAllowEdit
    sdbgUltraPres.Bands(5).Columns("PORCEN").Activation = ssActivationAllowEdit
    sdbgUltraPres.Bands(5).Columns("FECINI").Activation = ssActivationAllowEdit
    sdbgUltraPres.Bands(5).Columns("FECFIN").Activation = ssActivationAllowEdit
    
    'a�ade una columna a la banda de los art�culos para la integraci�n:
    If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.PresArt) Then
        sdbgUltraPres.Bands(4).Columns.Add "INT"
        sdbgUltraPres.Bands(4).Columns("INT").Header.Appearance.Picture = ImageList1.ListImages.Item(1).Picture
        sdbgUltraPres.Bands(4).Columns("INT").Header.Appearance.PictureAlign = UltraGrid.ssAlignRight
        sdbgUltraPres.Bands(4).Columns("INT").Header.caption = " "
        sdbgUltraPres.Bands(4).Columns("INT").Width = 400
        sdbgUltraPres.Bands(4).Groups("ART").Columns.Add sdbgUltraPres.Bands(4).Columns("INT"), 1
        
        'Oculta la columna con el estado de la integraci�n
        sdbgUltraPres.Bands(4).Columns("ESTADO").Hidden = True
    End If
    
End Sub

Private Sub sdbgUltraPres_InitializeRow(ByVal Context As UltraGrid.Constants_Context, ByVal Row As UltraGrid.SSRow, ByVal ReInitialize As Boolean)
    'El evento se lanza cuando se carga la fila
    
    Select Case Row.Band.key
        Case "gmn1", "gmn2", "gmn3"
            If Row.Cells("CARGADO").value <> 1 Then
                Row.Cells("PRESV").value = NullToStr(Row.Cells("PRES").value)
                Row.Cells("OBJV").value = NullToStr(Row.Cells("OBJ").value)
                Row.Cells("CARGADO").value = 1
                
                If Row.Cells("OBJV").value <> "" And Row.Cells("PRESV").value <> "" Then
                    Row.Cells("AHORRO").value = Row.Cells("PRESV").value - Row.Cells("OBJV").value
                    If Row.Cells("PRESV").value = 0 Then
                        Row.Cells("PORCEN").value = 0
                    Else
                        Row.Cells("PORCEN").value = (Row.Cells("AHORRO").value / Row.Cells("PRESV").value) * 100
                    End If
                End If
            End If
            
        Case "gmn4"
            If Row.Cells("CARGADO").value <> 1 Then
                Row.Cells("PRESV").value = NullToStr(Row.Cells("PRES").value)
                Row.Cells("OBJV").value = NullToStr(Row.Cells("OBJ").value)
                Row.Cells("ANYOV").value = NullToStr(Row.Cells("ANYO").value)
                Row.Cells("PRESDIRECTAV").value = NullToDbl0(Row.Cells("PRESDIRECTA").value)
                Row.Cells("CARGADO").value = 1
                
                If Not Row.Cells("OBJV").value = "" And Not Row.Cells("PRESV").value = "" Then
                    Row.Cells("AHORRO").value = Row.Cells("PRESV").value - Row.Cells("OBJV").value
                    If Row.Cells("PRESV").value = 0 Then
                        Row.Cells("PORCEN").value = 0
                    Else
                        Row.Cells("PORCEN").value = (Row.Cells("AHORRO").value / Row.Cells("PRESV").value) * 100
                    End If
                End If
            End If
            
            If Row.Cells("PRESDIRECTAV").value = 1 Then
                Row.Cells("PRESV").Appearance = "Blanco"
                Row.Cells("OBJV").Appearance = "Blanco"
                Row.Cells("AHORRO").Appearance = "Blanco"
                Row.Cells("PORCEN").Appearance = "Blanco"
            Else
                Row.Cells("PRESV").Appearance = "Amarillo"
                Row.Cells("OBJV").Appearance = "Amarillo"
                Row.Cells("AHORRO").Appearance = "Amarillo"
                Row.Cells("PORCEN").Appearance = "Amarillo"
            End If
            
        Case "Articulos"
            If Row.Cells("CARGADO").value <> 1 Then
                Row.Cells("CANTV").value = NullToStr(Row.Cells("CANT").value)
                Row.Cells("PRESV").value = NullToStr(Row.Cells("PRES").value)
                Row.Cells("OBJV").value = NullToStr(Row.Cells("OBJ").value)
                Row.Cells("ANYOV").value = NullToStr(Row.Cells("ANYO").value)
                Row.Cells("CARGADO").value = 1
                
                If Not Row.Cells("OBJV").value = "" And Not Row.Cells("PRESV").value = "" Then
                    Row.Cells("AHORRO").value = Row.Cells("PRESV").value - Row.Cells("OBJV").value
                    If Row.Cells("PRESV").value = 0 Then
                        Row.Cells("PORCEN").value = 0
                    Else
                        Row.Cells("PORCEN").value = (Row.Cells("AHORRO").value / Row.Cells("PRESV").value) * 100
                    End If
                End If
                
                'Calcula los presupuestos totales
                If Not Row.Cells("CANTV").value = "" And Not Row.Cells("PRESV").value = "" Then
                    Row.Cells("PRESTOT").value = Row.Cells("CANTV").value * Row.Cells("PRESV").value
                End If
                
                If Not Row.Cells("CANTV").value = "" And Not Row.Cells("OBJV").value = "" Then
                    Row.Cells("OBJTOT").value = Row.Cells("CANTV").value * Row.Cells("OBJV").value
                End If
                
                If Not Row.Cells("OBJTOT").value = "" And Not Row.Cells("PRESTOT").value = "" Then
                    Row.Cells("AHORROTOT").value = Row.Cells("PRESTOT").value - StrToDbl0(Row.Cells("OBJTOT").value)
                End If
                
                If Row.Cells("PRESTOT").value = 0 Then
                    Row.Cells("PORCENTOT").value = 0
                Else
                    If Not Row.Cells("AHORROTOT").value = "" Then
                        Row.Cells("PORCENTOT").value = (Row.Cells("AHORROTOT").value / Row.Cells("PRESTOT").value) * 100
                    End If
                End If
                
                'Dependiendo del estado de la integraci�n pone en gris o rojo la columna que lo indica
                If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.PresArt) Then
                    If Row.Cells("ESTADO").value = 4 Then
                        Row.Cells("INT").Appearance = "GrisFila"
                    Else
                        Row.Cells("INT").Appearance = "Rojo"
                    End If
                End If
            End If
            
            If Row.GetParent.Cells("PRESDIRECTAV").value = 1 Then
                Row.Cells("CANTV").Appearance = "Gris"
                Row.Cells("PRESV").Appearance = "Gris"
                Row.Cells("OBJV").Appearance = "Gris"
                Row.Cells("AHORRO").Appearance = "Gris"
                Row.Cells("PORCEN").Appearance = "Gris"
                Row.Cells("PRESTOT").Appearance = "Gris"
                Row.Cells("OBJTOT").Appearance = "Gris"
                Row.Cells("AHORROTOT").Appearance = "Gris"
                Row.Cells("PORCENTOT").Appearance = "Gris"
            Else
                Row.Cells("CANTV").Appearance = "Blanco"
                Row.Cells("PRESV").Appearance = "Blanco"
                Row.Cells("OBJV").Appearance = "Blanco"
                Row.Cells("AHORRO").Appearance = "Blanco"
                Row.Cells("PORCEN").Appearance = "Blanco"
                Row.Cells("PRESTOT").Appearance = "Blanco"
                Row.Cells("OBJTOT").Appearance = "Blanco"
                Row.Cells("AHORROTOT").Appearance = "Blanco"
                Row.Cells("PORCENTOT").Appearance = "Blanco"
            End If
            
    End Select
End Sub

Private Sub sdbgUltraPres_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If sdbgUltraPres.ActiveRow Is Nothing Then Exit Sub
    
    If sdbgUltraPres.ActiveRow.Band.Index <> 4 Then Exit Sub
    
    If m_bModoEdicion = False Then Exit Sub
    
    If Button = 2 Then
        If sdbgUltraPres.Selected.Rows.Count = 0 Then
            sdbgUltraPres.Selected.Rows.Add sdbgUltraPres.ActiveRow
        End If
    
        PopupMenu MDI.mnuPopUpPresArtMod
    End If
End Sub

Private Sub ActualizarTotales(ByVal Row As UltraGrid.SSRow, iColumnaMod As Integer)
    Dim oRow1 As SSRow
    Dim oRow2 As SSRow
    Dim oRow3 As SSRow
    Dim oRow4 As SSRow
    Dim vSumPres As Variant
    Dim vSumObj As Variant
    Dim oRow As SSRow

    'Actualiza los totales y los stylesets
        
    Select Case Row.Band.Index
        Case 4
            'ART�CULOS
            
            'Ha modificado los totales:
            Select Case iColumnaMod
                Case 1:
                    'Se ha modificado el presupuesto total
                    If Row.Cells("PRESTOT").value = "" Then
                        Row.Cells("PRESV").value = ""
                    Else
                        If Row.Cells("CANTV").value = "" Or Row.Cells("CANTV").value = 0 Then
                            Row.Cells("PRESV").value = ""
                        Else
                            Row.Cells("PRESV").value = Row.Cells("PRESTOT").value / Row.Cells("CANTV").value
                        End If
                    End If
                
                Case 2:
                    'Se ha modificado el objetivo total
                    If Row.Cells("OBJTOT").value = "" Then
                        Row.Cells("OBJV").value = ""
                    Else
                        If Row.Cells("CANTV").value = "" Or Row.Cells("CANTV").value = 0 Then
                            Row.Cells("OBJV").value = ""
                        Else
                            Row.Cells("OBJV").value = Row.Cells("OBJTOT").value / Row.Cells("CANTV").value
                        End If
                    End If
                            
                Case 3:
                    'Se ha modificado el ahorro unitario
                    If Row.Cells("AHORRO").value = "" Then
                        Row.Cells("OBJV").value = ""
                    ElseIf Row.Cells("PRESV").value = "" Then
                        Row.Cells("OBJV").value = ""
                        oMensajes.FaltanDatos 80 'Presupuesto
                    Else
                        Row.Cells("OBJV").value = Row.Cells("PRESV").value - Row.Cells("AHORRO").value
                    End If
                    
                Case 4:
                    'Se ha modificado el porcentaje de ahorro unitario
                    If Row.Cells("PORCEN").value = "" Then
                        Row.Cells("OBJV").value = ""
                    ElseIf Row.Cells("PRESV").value = "" Then
                        Row.Cells("OBJV").value = ""
                        oMensajes.FaltanDatos 80
                    Else
                        Row.Cells("OBJV").value = ((Row.Cells("PRESV").value * 100) - (Row.Cells("PRESV").value * Row.Cells("PORCEN").value)) / 100
                    End If
                    
                Case 5:
                    'Se ha modificado el ahorro total
                    If Row.Cells("AHORROTOT").value = "" Then
                        Row.Cells("OBJV").value = ""
                    ElseIf Row.Cells("CANTV").value = "" Then
                        Row.Cells("OBJV").value = ""
                    ElseIf Row.Cells("PRESV").value = "" Then
                        Row.Cells("OBJV").value = ""
                    Else
                        Row.Cells("OBJV").value = ((Row.Cells("PRESV").value * Row.Cells("CANTV").value) - Row.Cells("AHORROTOT").value) / Row.Cells("CANTV").value
                    End If
                
                Case 6:
                    'Se ha modificado el porcentaje de ahorro TOTAL
                    If Row.Cells("PORCENTOT").value = "" Then
                        Row.Cells("OBJV").value = ""
                    ElseIf Row.Cells("CANTV").value = "" Then
                        Row.Cells("OBJV").value = ""
                    ElseIf Row.Cells("PRESV").value = "" Then
                        Row.Cells("OBJV").value = ""
                    Else
                        Row.Cells("OBJV").value = (((Row.Cells("PRESV").value * 100) - (Row.Cells("PRESV").value * Row.Cells("PORCENTOT").value)) / 100)
                    End If
            End Select
            
            'Calcula los presupuestos unitarios del art�culo:
            If Row.Cells("OBJV").value <> "" And Row.Cells("PRESV").value <> "" Then
                Row.Cells("AHORRO").value = Row.Cells("PRESV").value - Row.Cells("OBJV").value
                If Row.Cells("PRESV").value = 0 Then
                    Row.Cells("PORCEN").value = 0
                Else
                    Row.Cells("PORCEN").value = (Row.Cells("AHORRO").value / Row.Cells("PRESV").value) * 100
                End If
            Else
                Row.Cells("AHORRO").value = ""
                Row.Cells("PORCEN").value = ""
            End If
    
            'Calcula los presupuestos totales del art�culo
            If Row.Cells("CANTV").value <> "" Then
                If iColumnaMod <> 1 And iColumnaMod <> 2 Then
                    If Row.Cells("PRESV").value = "" Then
                        Row.Cells("PRESTOT").value = ""
                    Else
                        Row.Cells("PRESTOT").value = Row.Cells("CANTV").value * Row.Cells("PRESV").value
                    End If
                    If Row.Cells("OBJV").value = "" Then
                        Row.Cells("OBJTOT").value = ""
                    Else
                        Row.Cells("OBJTOT").value = Row.Cells("CANTV").value * StrToDbl0(Row.Cells("OBJV").value)
                    End If
                End If
                
                If Row.Cells("PRESTOT").value = "" Or Row.Cells("OBJTOT").value = "" Then
                    Row.Cells("AHORROTOT").value = ""
                    Row.Cells("PORCENTOT").value = ""
                Else
                    If iColumnaMod <> 5 Then
                        Row.Cells("AHORROTOT").value = Row.Cells("PRESTOT").value - StrToDbl0(Row.Cells("OBJTOT").value)
                    End If
                End If
                
                If Row.Cells("AHORROTOT").value = "" Then
                    Row.Cells("PORCENTOT").value = ""
                ElseIf Row.Cells("PRESTOT").value = 0 Or Row.Cells("PRESTOT").value = "" Then
                    Row.Cells("PORCENTOT").value = 0
                Else
                    If iColumnaMod <> 6 Then
                        Row.Cells("PORCENTOT").value = (Row.Cells("AHORROTOT").value / Row.Cells("PRESTOT").value) * 100
                    End If
                End If
                
            Else
                Row.Cells("PRESTOT").value = ""
                Row.Cells("OBJTOT").value = ""
                Row.Cells("AHORROTOT").value = ""
                Row.Cells("PORCENTOT").value = ""
            End If
            
            Set oRow4 = Row.GetParent
            If Not (iColumnaMod = 7 And oRow4.Cells("PRESDIRECTAV").value = 1) Then
                vSumPres = ""
                vSumObj = ""
                If oRow4.HasChild = True Then
                    Set oRow = oRow4.GetChild(ssChildRowFirst)
                    If oRow.Cells("PRESTOT").value <> "" Then
                        vSumPres = StrToDbl0(oRow.Cells("PRESTOT").value)
                    End If
                    If oRow.Cells("OBJTOT").value <> "" Then
                        vSumObj = StrToDbl0(oRow.Cells("OBJTOT").value)
                    End If
                    
                    While oRow.HasNextSibling
                        Set oRow = oRow.GetSibling(ssSiblingRowNext)
                        If oRow.Cells("PRESTOT").value <> "" Then
                            vSumPres = StrToDbl0(vSumPres) + StrToDbl0(oRow.Cells("PRESTOT").value)
                        End If
                        If oRow.Cells("OBJTOT").value <> "" Then
                            vSumObj = StrToDbl0(vSumObj) + StrToDbl0(oRow.Cells("OBJTOT").value)
                        End If
                    Wend
            
                    oRow4.Cells("PRESV").value = DblToStr(vSumPres)
                    oRow4.Cells("OBJV").value = DblToStr(vSumObj)
                    If Not oRow4.Cells("OBJV").value = "" And Not oRow4.Cells("PRESV").value = "" Then
                        oRow4.Cells("AHORRO").value = oRow4.Cells("PRESV").value - oRow4.Cells("OBJV").value
                        If oRow4.Cells("PRESV").value = 0 Then
                            oRow4.Cells("PORCEN").value = 0
                        Else
                            oRow4.Cells("PORCEN").value = (oRow4.Cells("AHORRO").value / oRow4.Cells("PRESV").value) * 100
                        End If
                    End If
                    
                End If
            End If
            
        Case 3
            'GMN4
            Select Case iColumnaMod
             Case 3:
                    'Se ha modificado el ahorro unitario
                    If Row.Cells("AHORRO").value = "" Then
                        Row.Cells("OBJV").value = ""
                    ElseIf Row.Cells("PRESV").value = "" Then
                        oMensajes.FaltanDatos 80 'Presupuesto
                        Row.Cells("OBJV").value = ""
                    Else
                        Row.Cells("OBJV").value = Row.Cells("PRESV").value - Row.Cells("AHORRO").value
                    End If
                    
                Case 4:
                    'Se ha modificado el porcentaje de ahorro unitario
                    If Row.Cells("PORCEN").value = "" Then
                        Row.Cells("OBJV").value = ""
                    ElseIf Row.Cells("PRESV").value = "" Then
                        oMensajes.FaltanDatos 80 'Presupuesto
                        Row.Cells("OBJV").value = ""
                    Else
                        Row.Cells("OBJV").value = ((Row.Cells("PRESV").value * 100) - (Row.Cells("PRESV").value * Row.Cells("PORCEN").value)) / 100
                    End If
            End Select
            
            If Not Row.Cells("OBJV").value = "" And Not Row.Cells("PRESV").value = "" Then
                If iColumnaMod <> 3 Then
                    Row.Cells("AHORRO").value = Row.Cells("PRESV").value - Row.Cells("OBJV").value
                End If
                If iColumnaMod <> 4 Then
                    If Row.Cells("PRESV").value = 0 Then
                        Row.Cells("PORCEN").value = 0
                    Else
                        Row.Cells("PORCEN").value = (Row.Cells("AHORRO").value / Row.Cells("PRESV").value) * 100
                    End If
                End If
            Else
                Row.Cells("AHORRO").value = ""
                Row.Cells("PORCEN").value = ""
            End If
    
            Set oRow4 = Row
    End Select
    
    'GMN3
    vSumPres = ""
    vSumObj = ""
    Set oRow3 = oRow4.GetParent
    If oRow3.HasChild = True Then
        Set oRow = oRow3.GetChild(ssChildRowFirst)
        If oRow.Cells("PRESV").value <> "" Then
            vSumPres = StrToDbl0(oRow.Cells("PRESV").value)
        End If
        If oRow.Cells("OBJV").value <> "" Then
            vSumObj = StrToDbl0(oRow.Cells("OBJV").value)
        End If
        
        While oRow.HasNextSibling
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
            If oRow.Cells("PRESV").value <> "" Then
                vSumPres = StrToDbl0(vSumPres) + StrToDbl0(oRow.Cells("PRESV").value)
            End If
            If oRow.Cells("OBJV").value <> "" Then
                vSumObj = StrToDbl0(vSumObj) + StrToDbl0(oRow.Cells("OBJV").value)
            End If
        Wend

        oRow3.Cells("PRESV").value = DblToStr(vSumPres)
        oRow3.Cells("OBJV").value = DblToStr(vSumObj)
        If Not oRow3.Cells("OBJV").value = "" And Not oRow3.Cells("PRESV").value = "" Then
            oRow3.Cells("AHORRO").value = oRow3.Cells("PRESV").value - oRow3.Cells("OBJV").value
            If oRow3.Cells("PRESV").value = 0 Then
                oRow3.Cells("PORCEN").value = 0
            Else
                oRow3.Cells("PORCEN").value = (oRow3.Cells("AHORRO").value / oRow3.Cells("PRESV").value) * 100
            End If
        Else
            oRow3.Cells("AHORRO").value = ""
            oRow3.Cells("PORCEN").value = ""
        End If
    End If
        
    
    'GMN2
    vSumPres = ""
    vSumObj = ""
    Set oRow2 = oRow3.GetParent
    If oRow2.HasChild = True Then
        Set oRow = oRow2.GetChild(ssChildRowFirst)
        If oRow.Cells("PRESV").value <> "" Then
            vSumPres = StrToDbl0(oRow.Cells("PRESV").value)
        End If
        If oRow.Cells("OBJV").value <> "" Then
            vSumObj = StrToDbl0(oRow.Cells("OBJV").value)
        End If
        
        While oRow.HasNextSibling
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
            If oRow.Cells("PRESV").value <> "" Then
                vSumPres = StrToDbl0(vSumPres) + StrToDbl0(oRow.Cells("PRESV").value)
            End If
            If oRow.Cells("OBJV").value <> "" Then
                vSumObj = StrToDbl0(vSumObj) + StrToDbl0(oRow.Cells("OBJV").value)
            End If
        Wend

        oRow2.Cells("PRESV").value = DblToStr(vSumPres)
        oRow2.Cells("OBJV").value = DblToStr(vSumObj)
        If Not oRow2.Cells("OBJV").value = "" And Not oRow2.Cells("PRESV").value = "" Then
            oRow2.Cells("AHORRO").value = oRow2.Cells("PRESV").value - oRow2.Cells("OBJV").value
            If oRow2.Cells("PRESV").value = 0 Then
                oRow2.Cells("PORCEN").value = 0
            Else
                oRow2.Cells("PORCEN").value = (oRow2.Cells("AHORRO").value / oRow2.Cells("PRESV").value) * 100
            End If
        Else
            oRow2.Cells("AHORRO").value = ""
            oRow2.Cells("PORCEN").value = ""
        End If
    End If
    
    'GMN1
    vSumPres = ""
    vSumObj = ""
    Set oRow1 = oRow2.GetParent
    If oRow1.HasChild = True Then
        Set oRow = oRow1.GetChild(ssChildRowFirst)
        If oRow.Cells("PRESV").value <> "" Then
            vSumPres = StrToDbl0(oRow.Cells("PRESV").value)
        End If
        If oRow.Cells("OBJV").value <> "" Then
            vSumObj = StrToDbl0(oRow.Cells("OBJV").value)
        End If
        
        While oRow.HasNextSibling
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
            If Row.Cells("PRESV").value <> "" Then
                vSumPres = StrToDbl0(vSumPres) + StrToDbl0(oRow.Cells("PRESV").value)
            End If
            If oRow.Cells("OBJV").value <> "" Then
                vSumObj = StrToDbl0(vSumObj) + StrToDbl0(oRow.Cells("OBJV").value)
            End If
        Wend

        oRow1.Cells("PRESV").value = DblToStr(vSumPres)
        oRow1.Cells("OBJV").value = DblToStr(vSumObj)
        If Not oRow1.Cells("OBJV").value = "" And Not oRow1.Cells("PRESV").value = "" Then
            oRow1.Cells("AHORRO").value = oRow1.Cells("PRESV").value - oRow1.Cells("OBJV").value
            If oRow1.Cells("PRESV").value = 0 Then
                oRow1.Cells("PORCEN").value = 0
            Else
                oRow1.Cells("PORCEN").value = (oRow1.Cells("AHORRO").value / oRow1.Cells("PRESV").value) * 100
            End If
        Else
            oRow1.Cells("AHORRO").value = ""
            oRow1.Cells("PORCEN").value = ""
        End If
    End If
    
    
    'Libera los objetos
    Set oRow = Nothing
    Set oRow1 = Nothing
    Set oRow2 = Nothing
    Set oRow3 = Nothing
    Set oRow4 = Nothing
    
End Sub

Public Sub ModificarPresupuestos(ByVal iModif As Integer)
    Dim irespuesta As Integer
    
    If sdbgUltraPres.ActiveRow.GetParent.Cells("PRESDIRECTAV").value = 1 Then
        irespuesta = oMensajes.ExistePresupuestoDirectos()
        If irespuesta = vbNo Then
            Exit Sub
        End If
    End If
    
    frmPRESArtMod.g_iModif = iModif
        
    frmPRESArtMod.Show vbModal
    
End Sub

Private Sub CargarConsumo()
    Dim oRow As SSRow
    Dim vSumCant As Variant
    Dim oArticulo As CPresPorMaterial
    Dim sNoExisten As String
    Dim irespuesta As Integer
    
    'Suma las cantidades adjudicadas para el art�culo/unidad en el a�o anterior al seleccionado
    'y lo muestra en la grid,almacen�ndolo en BD
    
    irespuesta = oMensajes.PreguntaCargarConsumo
    If irespuesta = vbNo Then Exit Sub
    
    'Si no hay filas seleccionadas,selecciona la activa
    If sdbgUltraPres.Selected.Rows.Count = 0 Then
        sdbgUltraPres.Selected.Rows.Add sdbgUltraPres.ActiveRow
    End If
        
    If sdbgUltraPres.ActiveRow.GetParent.Cells("PRESDIRECTAV").value = 1 Then
        irespuesta = oMensajes.ExistePresupuestoDirectos()
        If irespuesta = vbNo Then
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    sNoExisten = ""
    
    'va calculando la suma de las cantidades para cada art�culo:
    For Each oRow In sdbgUltraPres.Selected.Rows
        Set oArticulo = oFSGSRaiz.Generar_CPresPorMaterial
        
        oArticulo.Articulo = oRow.Cells("COD").value
        oArticulo.CodGMN1 = oRow.Cells("GMN1").value
        oArticulo.CodGMN2 = oRow.Cells("GMN2").value
        oArticulo.CodGMN3 = oRow.Cells("GMN3").value
        oArticulo.codGMN4 = oRow.Cells("GMN4").value
        oArticulo.Anyo = m_iAnyoSeleccionado - 1
        
        vSumCant = oArticulo.DevolverConsumoAnyoAnterior()
        
        If IsNull(vSumCant) Then
            'oRow.Cells("CANTV").Value = ""
            If sdbgUltraPres.Selected.Rows.Count = 1 Then
                oMensajes.NoExisteConsumo
            Else
                sNoExisten = sNoExisten & vbCrLf & oRow.Cells("COD").value & " - " & oRow.Cells("DEN").value
            End If
        Else
            oRow.Cells("CANTV").value = vSumCant
        End If
        
        Set oArticulo = Nothing
    Next
    
    If sNoExisten <> "" Then
        oMensajes.NoExisteConsumo sNoExisten
    End If
    
    sdbgUltraPres.Update
    
    Screen.MousePointer = vbNormal
    
    sdbgUltraPres.Selected.ClearAll
    
End Sub

Public Sub CargarPresupuestos(ByVal iOpcion As Integer)
    Dim oRow As SSRow
    Dim vPres As Variant
    Dim oArticulo As CPresPorMaterial
    Dim sNoExisten As String
    Dim irespuesta As Integer
    
    'Si no hay filas seleccionadas,selecciona la activa
    If sdbgUltraPres.Selected.Rows.Count = 0 Then
        sdbgUltraPres.Selected.Rows.Add sdbgUltraPres.ActiveRow
    End If
        
    If sdbgUltraPres.ActiveRow.GetParent.Cells("PRESDIRECTAV").value = 1 Then
        irespuesta = oMensajes.ExistePresupuestoDirectos()
        If irespuesta = vbNo Then
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    sNoExisten = ""
    
    For Each oRow In sdbgUltraPres.Selected.Rows
        Set oArticulo = oFSGSRaiz.Generar_CPresPorMaterial
        
        oArticulo.Articulo = oRow.Cells("COD").value
        oArticulo.CodGMN1 = oRow.Cells("GMN1").value
        oArticulo.CodGMN2 = oRow.Cells("GMN2").value
        oArticulo.CodGMN3 = oRow.Cells("GMN3").value
        oArticulo.codGMN4 = oRow.Cells("GMN4").value
        oArticulo.Anyo = m_iAnyoSeleccionado - 1
                
        Select Case iOpcion
            
            Case 0
                'Media del a�o anterior
                vPres = oArticulo.DevolverMediaAdjAnyoAnterior(oRow.Cells("UNI").value)
                
            Case 1
                '�ltima adjudicaci�n del a�o anterior
                vPres = oArticulo.DevolverUltimaAdjAnyoAnterior(oRow.Cells("UNI").value)
                
        End Select
            
        If IsNull(vPres) Then
            'oRow.Cells("PRESV").Value = ""
            If sdbgUltraPres.Selected.Rows.Count = 1 Then
                oMensajes.NoExisteAdjudicacion
            Else
                sNoExisten = sNoExisten & vbCrLf & oRow.Cells("COD").value & " - " & oRow.Cells("DEN").value
            End If
        Else
            oRow.Cells("PRESV").value = vPres
        End If
                
        Set oArticulo = Nothing
    Next

    If sNoExisten <> "" Then
        oMensajes.NoExisteAdjudicacion sNoExisten
    End If
    
    sdbgUltraPres.Update
    
    Screen.MousePointer = vbNormal
    
    sdbgUltraPres.Selected.ClearAll
End Sub


Private Sub ActualizarEstilo(ByVal Row As UltraGrid.SSRow)
Dim oRow4 As SSRow
Dim oRow As SSRow

    'Actualiza el estilo de las columnas:
    Select Case Row.Band.Index
        Case 4
            'Art�culos:
            Set oRow4 = Row.GetParent
        Case 3
            'GMN4
            Set oRow4 = Row
    End Select
        
        
    If oRow4.Cells("PRESDIRECTAV").value = 1 Then
        oRow4.Cells("PRESV").Appearance = "Blanco"
        oRow4.Cells("OBJV").Appearance = "Blanco"
        oRow4.Cells("AHORRO").Appearance = "Blanco"
        oRow4.Cells("PORCEN").Appearance = "Blanco"
        
        If oRow4.HasChild = True Then
            Set oRow = oRow4.GetChild(ssSiblingRowFirst)
            oRow.Cells("CANTV").Appearance = "Gris"
            oRow.Cells("PRESV").Appearance = "Gris"
            oRow.Cells("OBJV").Appearance = "Gris"
            oRow.Cells("AHORRO").Appearance = "Gris"
            oRow.Cells("PORCEN").Appearance = "Gris"
            oRow.Cells("PRESTOT").Appearance = "Gris"
            oRow.Cells("OBJTOT").Appearance = "Gris"
            oRow.Cells("AHORROTOT").Appearance = "Gris"
            oRow.Cells("PORCENTOT").Appearance = "Gris"
                
            Do While oRow.HasNextSibling
                Set oRow = oRow.GetSibling(ssSiblingRowNext)
                
                oRow.Cells("CANTV").Appearance = "Gris"
                oRow.Cells("PRESV").Appearance = "Gris"
                oRow.Cells("OBJV").Appearance = "Gris"
                oRow.Cells("AHORRO").Appearance = "Gris"
                oRow.Cells("PORCEN").Appearance = "Gris"
                oRow.Cells("PRESTOT").Appearance = "Gris"
                oRow.Cells("OBJTOT").Appearance = "Gris"
                oRow.Cells("AHORROTOT").Appearance = "Gris"
                oRow.Cells("PORCENTOT").Appearance = "Gris"
            Loop
        End If
                    
    Else
        oRow4.Cells("PRESV").Appearance = "Amarillo"
        oRow4.Cells("OBJV").Appearance = "Amarillo"
        oRow4.Cells("AHORRO").Appearance = "Amarillo"
        oRow4.Cells("PORCEN").Appearance = "Amarillo"
        
        If oRow4.HasChild = True Then
            Set oRow = oRow4.GetChild(ssSiblingRowFirst)
            oRow.Cells("CANTV").Appearance = "Blanco"
            oRow.Cells("PRESV").Appearance = "Blanco"
            oRow.Cells("OBJV").Appearance = "Blanco"
            oRow.Cells("AHORRO").Appearance = "Blanco"
            oRow.Cells("PORCEN").Appearance = "Blanco"
            oRow.Cells("PRESTOT").Appearance = "Blanco"
            oRow.Cells("OBJTOT").Appearance = "Blanco"
            oRow.Cells("AHORROTOT").Appearance = "Blanco"
            oRow.Cells("PORCENTOT").Appearance = "Blanco"
                
            Do While oRow.HasNextSibling
                Set oRow = oRow.GetSibling(ssSiblingRowNext)
                
                oRow.Cells("CANTV").Appearance = "Blanco"
                oRow.Cells("PRESV").Appearance = "Blanco"
                oRow.Cells("OBJV").Appearance = "Blanco"
                oRow.Cells("AHORRO").Appearance = "Blanco"
                oRow.Cells("PORCEN").Appearance = "Blanco"
                oRow.Cells("PRESTOT").Appearance = "Blanco"
                oRow.Cells("OBJTOT").Appearance = "Blanco"
                oRow.Cells("AHORROTOT").Appearance = "Blanco"
                oRow.Cells("PORCENTOT").Appearance = "Blanco"
            Loop
        End If
    End If
                
    'Libera los objetos
    Set oRow = Nothing
    Set oRow4 = Nothing
    
End Sub

    



