VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmSolicitudes 
   Caption         =   "DSolicitudes"
   ClientHeight    =   9495
   ClientLeft      =   195
   ClientTop       =   4065
   ClientWidth     =   15690
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSolicitudes.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9495
   ScaleWidth      =   15690
   Begin VB.PictureBox picTop 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   4035
      Left            =   30
      ScaleHeight     =   4005
      ScaleWidth      =   15405
      TabIndex        =   36
      Top             =   30
      Width           =   15435
      Begin VB.PictureBox picBusquedaAvanzada 
         BorderStyle     =   0  'None
         Height          =   2115
         Left            =   90
         ScaleHeight     =   2115
         ScaleWidth      =   14205
         TabIndex        =   68
         Top             =   1440
         Visible         =   0   'False
         Width           =   14205
         Begin VB.PictureBox picNumSolicitErp 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   7380
            ScaleHeight     =   285
            ScaleWidth      =   6075
            TabIndex        =   84
            Top             =   1530
            Width           =   6075
            Begin VB.CommandButton cmdAyuda 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   7.5
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   5610
               Picture         =   "frmSolicitudes.frx":014A
               Style           =   1  'Graphical
               TabIndex        =   86
               Top             =   0
               Width           =   225
            End
            Begin VB.TextBox txtNumSolicErp 
               Height          =   285
               Left            =   1490
               TabIndex        =   85
               Top             =   0
               Width           =   4095
            End
            Begin VB.Label lblNumSolicitErp 
               AutoSize        =   -1  'True
               Caption         =   "DN� solicit. ERP"
               Height          =   195
               Left            =   0
               TabIndex        =   87
               Top             =   30
               Width           =   1110
            End
         End
         Begin VB.CommandButton cmdBuscarProveedor 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6840
            Picture         =   "frmSolicitudes.frx":037B
            Style           =   1  'Graphical
            TabIndex        =   83
            Top             =   840
            Width           =   315
         End
         Begin VB.ListBox lstContrato 
            Height          =   270
            IntegralHeight  =   0   'False
            Left            =   2040
            TabIndex        =   82
            Top             =   1575
            Visible         =   0   'False
            Width           =   4950
         End
         Begin VB.ListBox lstCentroCoste 
            Height          =   300
            IntegralHeight  =   0   'False
            Left            =   1650
            TabIndex        =   81
            Top             =   1200
            Visible         =   0   'False
            Width           =   4935
         End
         Begin VB.TextBox txtCentroCoste 
            Height          =   285
            Left            =   1530
            TabIndex        =   80
            Top             =   1200
            Width           =   4950
         End
         Begin VB.TextBox txtContrato 
            Height          =   285
            Index           =   0
            Left            =   1530
            TabIndex        =   79
            Top             =   1560
            Width           =   4950
         End
         Begin VB.TextBox txtImporteHasta 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   11715
            TabIndex        =   78
            Top             =   1200
            Width           =   1575
         End
         Begin VB.TextBox txtImporteDesde 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   8865
            TabIndex        =   77
            Top             =   1200
            Width           =   1515
         End
         Begin VB.TextBox txtMateriales 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   1530
            Locked          =   -1  'True
            TabIndex        =   76
            Top             =   120
            Width           =   4950
         End
         Begin VB.TextBox txtDenArticulo 
            Height          =   285
            Left            =   3600
            TabIndex        =   75
            Top             =   480
            Width           =   3615
         End
         Begin VB.CommandButton cmdLimpiarMateriales 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6510
            Picture         =   "frmSolicitudes.frx":0408
            Style           =   1  'Graphical
            TabIndex        =   74
            Top             =   120
            Width           =   315
         End
         Begin VB.CommandButton cmdLimpiarCentroCoste 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6510
            Picture         =   "frmSolicitudes.frx":04AD
            Style           =   1  'Graphical
            TabIndex        =   73
            Top             =   1200
            Width           =   315
         End
         Begin VB.CommandButton cmdLimpiarContrato 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   6510
            Picture         =   "frmSolicitudes.frx":0552
            Style           =   1  'Graphical
            TabIndex        =   72
            Top             =   1560
            Width           =   315
         End
         Begin VB.CommandButton cmdBuscarMateriales 
            Height          =   285
            Left            =   6870
            Picture         =   "frmSolicitudes.frx":05F7
            Style           =   1  'Graphical
            TabIndex        =   71
            Top             =   120
            Width           =   315
         End
         Begin VB.CommandButton cmdBuscarCentroCoste 
            Height          =   285
            Left            =   6870
            Picture         =   "frmSolicitudes.frx":0979
            Style           =   1  'Graphical
            TabIndex        =   70
            Top             =   1200
            Width           =   315
         End
         Begin VB.CommandButton cmdBuscarContrato 
            Height          =   285
            Index           =   0
            Left            =   6870
            Picture         =   "frmSolicitudes.frx":0CFB
            Style           =   1  'Graphical
            TabIndex        =   69
            Top             =   1560
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgPartidas 
            Height          =   1500
            Left            =   1755
            TabIndex        =   88
            Top             =   1560
            Visible         =   0   'False
            Width           =   6390
            _Version        =   196617
            DataMode        =   2
            BorderStyle     =   0
            RecordSelectors =   0   'False
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            Col.Count       =   11
            stylesets.count =   2
            stylesets(0).Name=   "Rojo"
            stylesets(0).BackColor=   8421631
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmSolicitudes.frx":107D
            stylesets(0).AlignmentText=   0
            stylesets(1).Name=   "Verde"
            stylesets(1).BackColor=   8454016
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmSolicitudes.frx":1099
            stylesets(1).AlignmentText=   0
            DividerType     =   2
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ExtraHeight     =   79
            Columns.Count   =   11
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   -2147483643
            Columns(1).Width=   2778
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   -2147483633
            Columns(2).Width=   8096
            Columns(2).Caption=   "VALOR"
            Columns(2).Name =   "VALOR"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Style=   1
            Columns(2).ButtonsAlways=   -1  'True
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "NIVEL"
            Columns(3).Name =   "NIVEL"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "PRES5"
            Columns(4).Name =   "PRES5NIV1"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Locked=   -1  'True
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "PRES5UON"
            Columns(5).Name =   "PRES5UON"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Locked=   -1  'True
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "VALIDADO"
            Columns(6).Name =   "VALIDADO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).Locked=   -1  'True
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "PRES5NIV2"
            Columns(7).Name =   "PRES5NIV2"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "PRES5NIV3"
            Columns(8).Name =   "PRES5NIV3"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "PRES5NIV4"
            Columns(9).Name =   "PRES5NIV4"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "TAG"
            Columns(10).Name=   "TAG"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            _ExtentX        =   11271
            _ExtentY        =   2646
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcComprador 
            Height          =   285
            Left            =   8865
            TabIndex        =   89
            Top             =   840
            Width           =   5145
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   11192
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   9075
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcDepartamento 
            Height          =   285
            Left            =   8865
            TabIndex        =   90
            Top             =   480
            Width           =   5145
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2619
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7197
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   9075
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcArticulo 
            Height          =   285
            Left            =   1530
            TabIndex        =   91
            Top             =   480
            Width           =   2055
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4630
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3625
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEmpresa 
            Height          =   285
            Left            =   8865
            TabIndex        =   92
            Top             =   120
            Width           =   5145
            DataFieldList   =   "Column 1"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   4
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2593
            Columns(1).Caption=   "NIF"
            Columns(1).Name =   "NIF"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   9022
            Columns(2).Caption=   "DEN"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "NIF_DEN"
            Columns(3).Name =   "NIF_DEN"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            _ExtentX        =   9075
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 3"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveedor 
            Height          =   285
            Left            =   1530
            TabIndex        =   93
            Top             =   840
            Width           =   2055
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4630
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3625
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcDenProveedor 
            Height          =   285
            Left            =   3600
            TabIndex        =   94
            Top             =   840
            Width           =   3225
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4630
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5689
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 0"
         End
         Begin VB.Label lblProveedor 
            AutoSize        =   -1  'True
            Caption         =   "DProveedor:"
            Height          =   195
            Left            =   60
            TabIndex        =   105
            Top             =   885
            Width           =   1335
         End
         Begin VB.Label lblMon 
            AutoSize        =   -1  'True
            Caption         =   "MONCEN"
            Height          =   195
            Left            =   13350
            TabIndex        =   104
            Top             =   1245
            Width           =   645
         End
         Begin VB.Label lblMateriales 
            AutoSize        =   -1  'True
            Caption         =   "DMateriales:"
            Height          =   195
            Left            =   75
            TabIndex        =   103
            Top             =   165
            Width           =   900
         End
         Begin VB.Label lblArticulo 
            AutoSize        =   -1  'True
            Caption         =   "DArticulo:"
            Height          =   195
            Left            =   75
            TabIndex        =   102
            Top             =   525
            Width           =   705
         End
         Begin VB.Label lblImporteDesde 
            AutoSize        =   -1  'True
            Caption         =   "DImporte desde:"
            Height          =   195
            Left            =   7365
            TabIndex        =   101
            Top             =   1245
            Width           =   1215
         End
         Begin VB.Label lblCentroCoste 
            AutoSize        =   -1  'True
            Caption         =   "DCentro coste:"
            Height          =   195
            Left            =   75
            TabIndex        =   100
            Top             =   1245
            Width           =   1095
         End
         Begin VB.Label lblImporteHasta 
            AutoSize        =   -1  'True
            Caption         =   "DImporte hasta:"
            Height          =   195
            Left            =   10485
            TabIndex        =   99
            Top             =   1245
            Width           =   1185
         End
         Begin VB.Label lblEmpresa 
            AutoSize        =   -1  'True
            Caption         =   "DEmpresa:"
            Height          =   195
            Left            =   7365
            TabIndex        =   98
            Top             =   165
            Width           =   1335
         End
         Begin VB.Label lblDepartamento 
            AutoSize        =   -1  'True
            Caption         =   "DDepartamento:"
            Height          =   195
            Left            =   7365
            TabIndex        =   97
            Top             =   525
            Width           =   1200
         End
         Begin VB.Label lblComprador 
            AutoSize        =   -1  'True
            Caption         =   "DComprador:"
            Height          =   195
            Left            =   7365
            TabIndex        =   96
            Top             =   885
            Width           =   960
         End
         Begin VB.Label lblContrato 
            Caption         =   "DContrato:"
            Height          =   195
            Index           =   0
            Left            =   75
            TabIndex        =   95
            Top             =   1605
            Width           =   1185
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   65535
         Left            =   14490
         Top             =   2550
      End
      Begin VB.CommandButton cmdExcel 
         Height          =   315
         Left            =   14490
         Picture         =   "frmSolicitudes.frx":10B5
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   90
         Width           =   330
      End
      Begin VB.TextBox txtUO 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4590
         Locked          =   -1  'True
         TabIndex        =   56
         Top             =   885
         Width           =   1995
      End
      Begin VB.CommandButton cmdLimpiarUO 
         Height          =   285
         Left            =   6615
         Picture         =   "frmSolicitudes.frx":15C7
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   885
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscarUO 
         Height          =   285
         Left            =   6975
         Picture         =   "frmSolicitudes.frx":1B09
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   885
         Width           =   315
      End
      Begin VB.CommandButton cmdPeticBorrar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3735
         Picture         =   "frmSolicitudes.frx":1E8B
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   885
         Width           =   315
      End
      Begin VB.PictureBox picEstado 
         BorderStyle     =   0  'None
         Height          =   990
         Left            =   9720
         ScaleHeight     =   990
         ScaleWidth      =   4275
         TabIndex        =   46
         Top             =   120
         Width           =   4275
         Begin VB.CheckBox chkEstado 
            Caption         =   "DEn curso de aprobaci�n"
            Height          =   300
            Index           =   5
            Left            =   0
            TabIndex        =   52
            Top             =   0
            Width           =   2400
         End
         Begin VB.CheckBox chkEstado 
            Caption         =   "DCerradas"
            Height          =   255
            Index           =   4
            Left            =   2700
            TabIndex        =   51
            Top             =   510
            Width           =   1215
         End
         Begin VB.CheckBox chkEstado 
            Caption         =   "DRechazadas"
            Height          =   255
            Index           =   2
            Left            =   2700
            TabIndex        =   50
            Top             =   30
            Width           =   1455
         End
         Begin VB.CheckBox chkEstado 
            Caption         =   "DAnuladas"
            Height          =   255
            Index           =   3
            Left            =   2700
            TabIndex        =   49
            Top             =   270
            Width           =   1335
         End
         Begin VB.CheckBox chkEstado 
            Caption         =   "DAprobadas"
            Height          =   255
            Index           =   1
            Left            =   0
            TabIndex        =   48
            Top             =   510
            Width           =   1335
         End
         Begin VB.CheckBox chkEstado 
            Caption         =   "DPendientes"
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   47
            Top             =   270
            Value           =   1  'Checked
            Width           =   1455
         End
      End
      Begin VB.TextBox txtFecDesde 
         Height          =   285
         Left            =   7950
         TabIndex        =   45
         Top             =   525
         Width           =   990
      End
      Begin VB.TextBox txtFecHasta 
         Height          =   285
         Left            =   7950
         TabIndex        =   44
         Top             =   885
         Width           =   990
      End
      Begin VB.CommandButton cmdCalFecDesde 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9000
         Picture         =   "frmSolicitudes.frx":1F30
         Style           =   1  'Graphical
         TabIndex        =   43
         TabStop         =   0   'False
         Top             =   525
         Width           =   315
      End
      Begin VB.CommandButton cmdCalFecHasta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9000
         Picture         =   "frmSolicitudes.frx":24BA
         Style           =   1  'Graphical
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   885
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "DBuscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   12720
         Style           =   1  'Graphical
         TabIndex        =   41
         TabStop         =   0   'False
         ToolTipText     =   "Mantenimiento"
         Top             =   3570
         Width           =   990
      End
      Begin VB.CommandButton cmdImprimir 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   14100
         Picture         =   "frmSolicitudes.frx":2A44
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   90
         UseMaskColor    =   -1  'True
         Width           =   330
      End
      Begin VB.TextBox txtId 
         Height          =   285
         Left            =   1605
         TabIndex        =   39
         Top             =   510
         Width           =   1300
      End
      Begin VB.TextBox txtDescr 
         Height          =   285
         Left            =   4215
         TabIndex        =   38
         Top             =   510
         Width           =   3030
      End
      Begin VB.CommandButton cmdLimpiarTodo 
         Caption         =   "DLimpiar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   13830
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   3570
         Width           =   990
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoSolicit 
         Height          =   285
         Left            =   1605
         TabIndex        =   58
         Top             =   120
         Width           =   7560
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2249
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   9419
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   13335
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPeticionario 
         Height          =   285
         Left            =   1605
         TabIndex        =   59
         Top             =   885
         Width           =   2115
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         HeadLines       =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4895
         Columns(1).Caption=   "PET"
         Columns(1).Name =   "PET"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3731
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin MSComDlg.CommonDialog cmmdGenerarExcel 
         Left            =   120
         Top             =   120
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.PictureBox picAbrirCerrar 
         BorderStyle     =   0  'None
         Height          =   135
         Index           =   1
         Left            =   210
         Picture         =   "frmSolicitudes.frx":2B46
         ScaleHeight     =   135
         ScaleWidth      =   135
         TabIndex        =   106
         Top             =   1290
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.PictureBox picAbrirCerrar 
         BorderStyle     =   0  'None
         Height          =   135
         Index           =   0
         Left            =   210
         Picture         =   "frmSolicitudes.frx":2E90
         ScaleHeight     =   135
         ScaleWidth      =   135
         TabIndex        =   107
         Top             =   1290
         Width           =   135
      End
      Begin VB.Label lblMostrarBusquedaAvanzada 
         AutoSize        =   -1  'True
         Caption         =   "DBusqueda avanzada"
         Height          =   195
         Left            =   420
         MouseIcon       =   "frmSolicitudes.frx":31D8
         MousePointer    =   99  'Custom
         TabIndex        =   67
         Top             =   1260
         Width           =   1635
      End
      Begin VB.Label lblUO 
         Caption         =   "UON: "
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   4110
         TabIndex        =   66
         Top             =   960
         Width           =   480
      End
      Begin VB.Label lblTipoSolicit 
         Caption         =   "Dtipo de solicitud:"
         Height          =   195
         Left            =   165
         TabIndex        =   65
         Top             =   150
         Width           =   1350
      End
      Begin VB.Label lblFecDesde 
         Caption         =   "DDesde:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   7380
         TabIndex        =   64
         Top             =   555
         Width           =   660
      End
      Begin VB.Label lblFecHasta 
         Caption         =   "Hasta:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   7380
         TabIndex        =   63
         Top             =   915
         Width           =   660
      End
      Begin VB.Label lblIdentificador 
         AutoSize        =   -1  'True
         Caption         =   "DIdentificador:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   165
         TabIndex        =   62
         Top             =   540
         Width           =   1080
      End
      Begin VB.Label lblPeticionario 
         AutoSize        =   -1  'True
         Caption         =   "DPeticionario : "
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   165
         TabIndex        =   61
         Top             =   915
         Width           =   1080
      End
      Begin VB.Label lblDescripcion 
         Caption         =   "DDescripci�n: "
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   3075
         TabIndex        =   60
         Top             =   540
         Width           =   1080
      End
      Begin VB.Shape ShapeSelecc 
         Height          =   3870
         Left            =   30
         Top             =   60
         Width           =   14835
      End
   End
   Begin VB.PictureBox picBottom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   4860
      Left            =   30
      ScaleHeight     =   4830
      ScaleWidth      =   15405
      TabIndex        =   13
      Top             =   4050
      Width           =   15435
      Begin VB.PictureBox picGrid 
         BorderStyle     =   0  'None
         Height          =   3975
         Left            =   30
         ScaleHeight     =   3975
         ScaleWidth      =   12465
         TabIndex        =   27
         Top             =   420
         Width           =   12465
         Begin SSDataWidgets_B.SSDBGrid sdbgSolicitudes 
            Height          =   3255
            Left            =   0
            TabIndex        =   28
            Top             =   0
            Width           =   9435
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   26
            stylesets.count =   32
            stylesets(0).Name=   "alertaProcesoAnulada"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmSolicitudes.frx":332A
            stylesets(1).Name=   "alertaProceso"
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmSolicitudes.frx":370E
            stylesets(1).AlignmentPicture=   0
            stylesets(2).Name=   "alertaIntegracionAprobada"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmSolicitudes.frx":3AF2
            stylesets(3).Name=   "alertaProcesoCurso"
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmSolicitudes.frx":3E93
            stylesets(4).Name=   "alertaProcesoCerrada"
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "frmSolicitudes.frx":4277
            stylesets(5).Name=   "alertaProcesoAprobada"
            stylesets(5).HasFont=   -1  'True
            BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(5).Picture=   "frmSolicitudes.frx":465B
            stylesets(6).Name=   "alertaPedidoPendiente"
            stylesets(6).HasFont=   -1  'True
            BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(6).Picture=   "frmSolicitudes.frx":4A3F
            stylesets(7).Name=   "Normal"
            stylesets(7).BackColor=   16777215
            stylesets(7).HasFont=   -1  'True
            BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(7).Picture=   "frmSolicitudes.frx":4DE1
            stylesets(8).Name=   "Seleccion"
            stylesets(8).ForeColor=   16777215
            stylesets(8).BackColor=   8388608
            stylesets(8).HasFont=   -1  'True
            BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(8).Picture=   "frmSolicitudes.frx":4DFD
            stylesets(9).Name=   "alertaPedidoRechazada"
            stylesets(9).HasFont=   -1  'True
            BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(9).Picture=   "frmSolicitudes.frx":4E19
            stylesets(10).Name=   "Curso"
            stylesets(10).ForeColor=   0
            stylesets(10).BackColor=   12648447
            stylesets(10).HasFont=   -1  'True
            BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(10).Picture=   "frmSolicitudes.frx":51BB
            stylesets(11).Name=   "alertaMonitorizacion"
            stylesets(11).BackColor=   12648447
            stylesets(11).HasFont=   -1  'True
            BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(11).Picture=   "frmSolicitudes.frx":51D7
            stylesets(12).Name=   "alertaPedido"
            stylesets(12).HasFont=   -1  'True
            BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(12).Picture=   "frmSolicitudes.frx":557D
            stylesets(12).AlignmentPicture=   0
            stylesets(13).Name=   "Anulada"
            stylesets(13).ForeColor=   0
            stylesets(13).BackColor=   4744445
            stylesets(13).HasFont=   -1  'True
            BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(13).Picture=   "frmSolicitudes.frx":591F
            stylesets(14).Name=   "alertaIntegracion"
            stylesets(14).HasFont=   -1  'True
            BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(14).Picture=   "frmSolicitudes.frx":593B
            stylesets(14).AlignmentPicture=   0
            stylesets(15).Name=   "alertaIntegracionAnulada"
            stylesets(15).HasFont=   -1  'True
            BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(15).Picture=   "frmSolicitudes.frx":5CDC
            stylesets(16).Name=   "alertaProcesoPendiente"
            stylesets(16).HasFont=   -1  'True
            BeginProperty stylesets(16).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(16).Picture=   "frmSolicitudes.frx":607D
            stylesets(17).Name=   "Cerrada"
            stylesets(17).ForeColor=   0
            stylesets(17).BackColor=   10079487
            stylesets(17).HasFont=   -1  'True
            BeginProperty stylesets(17).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(17).Picture=   "frmSolicitudes.frx":6461
            stylesets(18).Name=   "alertaPedidoAnulada"
            stylesets(18).HasFont=   -1  'True
            BeginProperty stylesets(18).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(18).Picture=   "frmSolicitudes.frx":647D
            stylesets(19).Name=   "alertaIntegracionCerrada"
            stylesets(19).HasFont=   -1  'True
            BeginProperty stylesets(19).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(19).Picture=   "frmSolicitudes.frx":681F
            stylesets(20).Name=   "alertaPedidoAprobada"
            stylesets(20).HasFont=   -1  'True
            BeginProperty stylesets(20).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(20).Picture=   "frmSolicitudes.frx":6BC0
            stylesets(21).Name=   "ASC"
            stylesets(21).HasFont=   -1  'True
            BeginProperty stylesets(21).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(21).Picture=   "frmSolicitudes.frx":6F62
            stylesets(21).AlignmentPicture=   0
            stylesets(22).Name=   "Pendiente"
            stylesets(22).ForeColor=   0
            stylesets(22).BackColor=   16777215
            stylesets(22).HasFont=   -1  'True
            BeginProperty stylesets(22).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(22).Picture=   "frmSolicitudes.frx":6F7E
            stylesets(23).Name=   "Header"
            stylesets(23).ForeColor=   0
            stylesets(23).BackColor=   -2147483633
            stylesets(23).HasFont=   -1  'True
            BeginProperty stylesets(23).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(23).Picture=   "frmSolicitudes.frx":6F9A
            stylesets(24).Name=   "alertaPedidoCerrada"
            stylesets(24).HasFont=   -1  'True
            BeginProperty stylesets(24).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(24).Picture=   "frmSolicitudes.frx":6FB6
            stylesets(25).Name=   "alertaIntegracionPendiente"
            stylesets(25).HasFont=   -1  'True
            BeginProperty stylesets(25).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(25).Picture=   "frmSolicitudes.frx":7358
            stylesets(26).Name=   "alertaProcesoRechazada"
            stylesets(26).HasFont=   -1  'True
            BeginProperty stylesets(26).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(26).Picture=   "frmSolicitudes.frx":76F9
            stylesets(27).Name=   "Aprobada"
            stylesets(27).ForeColor=   0
            stylesets(27).BackColor=   10409635
            stylesets(27).HasFont=   -1  'True
            BeginProperty stylesets(27).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(27).Picture=   "frmSolicitudes.frx":7ADD
            stylesets(28).Name=   "alertaPedidoCurso"
            stylesets(28).HasFont=   -1  'True
            BeginProperty stylesets(28).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(28).Picture=   "frmSolicitudes.frx":7AF9
            stylesets(29).Name=   "alertaIntegracionCurso"
            stylesets(29).HasFont=   -1  'True
            BeginProperty stylesets(29).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(29).Picture=   "frmSolicitudes.frx":7E9B
            stylesets(30).Name=   "Rechazada"
            stylesets(30).ForeColor=   0
            stylesets(30).BackColor=   12632256
            stylesets(30).HasFont=   -1  'True
            BeginProperty stylesets(30).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(30).Picture=   "frmSolicitudes.frx":823C
            stylesets(31).Name=   "alertaIntegracionRechazada"
            stylesets(31).HasFont=   -1  'True
            BeginProperty stylesets(31).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(31).Picture=   "frmSolicitudes.frx":8258
            BeveColorScheme =   0
            BevelColorFrame =   12632256
            BevelColorHighlight=   8421504
            BevelColorShadow=   128
            BevelColorFace  =   12632256
            CheckBox3D      =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            CellNavigation  =   1
            MaxSelectedRows =   0
            HeadStyleSet    =   "Header"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ExtraHeight     =   26
            Columns.Count   =   26
            Columns(0).Width=   3201
            Columns(0).Name =   "ALERTA_INTEGRACION"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).StyleSet=   "alertaIntegracion"
            Columns(1).Width=   3200
            Columns(1).Name =   "ALERTA_MONITORIZACION"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   3201
            Columns(2).Name =   "ALERTA_PROCESO"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(2).StyleSet=   "alertaProceso"
            Columns(3).Width=   3201
            Columns(3).Name =   "ALERTA_PEDIDO"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(3).StyleSet=   "alertaPedido"
            Columns(4).Width=   2752
            Columns(4).Caption=   "TIPO"
            Columns(4).Name =   "TIPO"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Style=   1
            Columns(4).ButtonsAlways=   -1  'True
            Columns(5).Width=   1667
            Columns(5).Caption=   "Alta"
            Columns(5).Name =   "ALTA"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Locked=   -1  'True
            Columns(6).Width=   1667
            Columns(6).Caption=   "Necesidad"
            Columns(6).Name =   "NECESIDAD"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).Locked=   -1  'True
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "Ident."
            Columns(7).Name =   "ID"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(7).Locked=   -1  'True
            Columns(8).Width=   3200
            Columns(8).Caption=   "ID2"
            Columns(8).Name =   "ID2"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Caption=   "dN� solicit. ERP"
            Columns(9).Name =   "NUM_SOL_ERP"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(9).Locked=   -1  'True
            Columns(10).Width=   2910
            Columns(10).Caption=   "Denominaci�n"
            Columns(10).Name=   "DESCR"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(10).Locked=   -1  'True
            Columns(11).Width=   1773
            Columns(11).Caption=   "Importe"
            Columns(11).Name=   "IMPORTE"
            Columns(11).Alignment=   1
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).NumberFormat=   "standard"
            Columns(11).FieldLen=   256
            Columns(11).Locked=   -1  'True
            Columns(12).Width=   3069
            Columns(12).Caption=   "Peticionario"
            Columns(12).Name=   "PET"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(12).Locked=   -1  'True
            Columns(12).Style=   1
            Columns(12).ButtonsAlways=   -1  'True
            Columns(13).Width=   1931
            Columns(13).Caption=   "Estado"
            Columns(13).Name=   "ESTADO"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(13).Locked=   -1  'True
            Columns(14).Width=   2831
            Columns(14).Caption=   "UON"
            Columns(14).Name=   "UON"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(15).Width=   3200
            Columns(15).Caption=   "DEPT"
            Columns(15).Name=   "DEPT"
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            Columns(16).Width=   2831
            Columns(16).Caption=   "Comprador"
            Columns(16).Name=   "COMP"
            Columns(16).DataField=   "Column 16"
            Columns(16).DataType=   8
            Columns(16).FieldLen=   256
            Columns(16).Locked=   -1  'True
            Columns(16).Style=   1
            Columns(16).ButtonsAlways=   -1  'True
            Columns(17).Width=   3200
            Columns(17).Visible=   0   'False
            Columns(17).Caption=   "ID_ESTADO"
            Columns(17).Name=   "ID_ESTADO"
            Columns(17).DataField=   "Column 17"
            Columns(17).DataType=   8
            Columns(17).FieldLen=   256
            Columns(18).Width=   3200
            Columns(18).Visible=   0   'False
            Columns(18).Caption=   "COD_PER"
            Columns(18).Name=   "COD_PER"
            Columns(18).DataField=   "Column 18"
            Columns(18).DataType=   8
            Columns(18).FieldLen=   256
            Columns(19).Width=   3200
            Columns(19).Visible=   0   'False
            Columns(19).Caption=   "COD_COMP"
            Columns(19).Name=   "COD_COMP"
            Columns(19).DataField=   "Column 19"
            Columns(19).DataType=   8
            Columns(19).FieldLen=   256
            Columns(20).Width=   3200
            Columns(20).Visible=   0   'False
            Columns(20).Caption=   "DESTINATARIO_PROV"
            Columns(20).Name=   "DESTINATARIO_PROV"
            Columns(20).DataField=   "Column 20"
            Columns(20).DataType=   8
            Columns(20).FieldLen=   256
            Columns(21).Width=   3200
            Columns(21).Visible=   0   'False
            Columns(21).Caption=   "Monitorizacion"
            Columns(21).Name=   "MONITORIZ"
            Columns(21).DataField=   "Column 21"
            Columns(21).DataType=   11
            Columns(21).FieldLen=   256
            Columns(22).Width=   3200
            Columns(22).Visible=   0   'False
            Columns(22).Caption=   "SIN_PROCESO"
            Columns(22).Name=   "SIN_PROCESO"
            Columns(22).DataField=   "Column 22"
            Columns(22).DataType=   11
            Columns(22).FieldLen=   256
            Columns(23).Width=   3200
            Columns(23).Visible=   0   'False
            Columns(23).Caption=   "SIN_PEDIDO"
            Columns(23).Name=   "SIN_PEDIDO"
            Columns(23).DataField=   "Column 23"
            Columns(23).DataType=   11
            Columns(23).FieldLen=   256
            Columns(24).Width=   3200
            Columns(24).Visible=   0   'False
            Columns(24).Caption=   "INTEGRACION"
            Columns(24).Name=   "INTEGRACION"
            Columns(24).DataField=   "Column 24"
            Columns(24).DataType=   8
            Columns(24).FieldLen=   256
            Columns(25).Width=   3200
            Columns(25).Visible=   0   'False
            Columns(25).Caption=   "TEXTOMONITORIZ"
            Columns(25).Name=   "TEXTOMONITORIZ"
            Columns(25).DataField=   "Column 25"
            Columns(25).DataType=   8
            Columns(25).FieldLen=   256
            _ExtentX        =   16642
            _ExtentY        =   5741
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.PictureBox picGridToolbar 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   0
         ScaleHeight     =   345
         ScaleWidth      =   15390
         TabIndex        =   29
         Top             =   0
         Width           =   15420
         Begin VB.CommandButton cmdFirst 
            Height          =   285
            Left            =   30
            Picture         =   "frmSolicitudes.frx":85F9
            Style           =   1  'Graphical
            TabIndex        =   33
            Top             =   45
            Width           =   315
         End
         Begin VB.CommandButton cmdPrevious 
            Height          =   285
            Left            =   375
            Picture         =   "frmSolicitudes.frx":86A5
            Style           =   1  'Graphical
            TabIndex        =   32
            Top             =   45
            Width           =   315
         End
         Begin VB.CommandButton cmdNext 
            Height          =   285
            Left            =   2475
            Picture         =   "frmSolicitudes.frx":8747
            Style           =   1  'Graphical
            TabIndex        =   31
            Top             =   45
            Width           =   315
         End
         Begin VB.CommandButton cmdLast 
            Height          =   285
            Left            =   2820
            Picture         =   "frmSolicitudes.frx":87E9
            Style           =   1  'Graphical
            TabIndex        =   30
            Top             =   45
            Width           =   315
         End
         Begin VB.PictureBox picAbrirCerrar 
            BorderStyle     =   0  'None
            Height          =   135
            Index           =   3
            Left            =   14460
            Picture         =   "frmSolicitudes.frx":8896
            ScaleHeight     =   135
            ScaleWidth      =   135
            TabIndex        =   110
            Top             =   120
            Visible         =   0   'False
            Width           =   135
         End
         Begin VB.PictureBox picAbrirCerrar 
            BorderStyle     =   0  'None
            Height          =   135
            Index           =   2
            Left            =   14460
            Picture         =   "frmSolicitudes.frx":8BE0
            ScaleHeight     =   135
            ScaleWidth      =   135
            TabIndex        =   109
            Top             =   120
            Width           =   135
         End
         Begin VB.Line Line2 
            X1              =   12690
            X2              =   12690
            Y1              =   0
            Y2              =   360
         End
         Begin VB.Line Line1 
            X1              =   14370
            X2              =   14370
            Y1              =   0
            Y2              =   360
         End
         Begin VB.Label lblOcultar 
            AutoSize        =   -1  'True
            Caption         =   "DOcultar"
            Height          =   195
            Left            =   14670
            MouseIcon       =   "frmSolicitudes.frx":8F28
            MousePointer    =   99  'Custom
            TabIndex        =   108
            Top             =   70
            Width           =   630
         End
         Begin VB.Label lblPageNumber 
            Alignment       =   2  'Center
            Caption         =   "DPagina {n} de {m}"
            Height          =   195
            Left            =   735
            TabIndex        =   35
            Top             =   90
            Width           =   1680
         End
         Begin VB.Label lblConfigurarAlerta 
            AutoSize        =   -1  'True
            Caption         =   "DConfigurar alerta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   12870
            MouseIcon       =   "frmSolicitudes.frx":907A
            MousePointer    =   99  'Custom
            TabIndex        =   34
            Top             =   75
            Width           =   1335
         End
      End
      Begin VB.Frame fraConfAlerta 
         Height          =   2070
         Left            =   10815
         TabIndex        =   14
         Top             =   285
         Visible         =   0   'False
         Width           =   4335
         Begin VB.CommandButton cmdCancelConfAlerta 
            Caption         =   "DCancelar"
            Height          =   345
            Left            =   2355
            TabIndex        =   22
            Top             =   1515
            Width           =   980
         End
         Begin VB.CommandButton cmdOKConfAlerta 
            Caption         =   "DAceptar"
            Height          =   345
            Left            =   1245
            TabIndex        =   21
            Top             =   1515
            Width           =   980
         End
         Begin VB.CheckBox chkConfAlertaErrIntegracion 
            Height          =   195
            Left            =   150
            TabIndex        =   20
            Top             =   1140
            Width           =   180
         End
         Begin VB.CheckBox chkConfAlertaSinPedido 
            Height          =   255
            Left            =   150
            TabIndex        =   19
            Top             =   825
            Width           =   180
         End
         Begin VB.CheckBox chkConfAlertaSinProceso 
            Height          =   240
            Left            =   150
            TabIndex        =   18
            Top             =   525
            Width           =   180
         End
         Begin VB.PictureBox imgConfAlertaSinProceso 
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   390
            Picture         =   "frmSolicitudes.frx":91CC
            ScaleHeight     =   285
            ScaleWidth      =   210
            TabIndex        =   17
            Top             =   510
            Width           =   205
         End
         Begin VB.PictureBox imgConfAlertaSinPedido 
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   390
            Picture         =   "frmSolicitudes.frx":95A0
            ScaleHeight     =   285
            ScaleWidth      =   225
            TabIndex        =   16
            Top             =   840
            Width           =   225
         End
         Begin VB.PictureBox imgConfAlertaErrIntegracion 
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   420
            Picture         =   "frmSolicitudes.frx":9932
            ScaleHeight     =   285
            ScaleWidth      =   195
            TabIndex        =   15
            Top             =   1140
            Width           =   195
         End
         Begin VB.Label lblConfAlertaTitulo 
            AutoSize        =   -1  'True
            Caption         =   "DConfigurar alerta para"
            Height          =   195
            Left            =   150
            TabIndex        =   26
            Top             =   210
            Width           =   1710
         End
         Begin VB.Label lblConfAlertaSinProceso 
            Caption         =   "DSolicitudes sin proceso asociado"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   690
            TabIndex        =   25
            Top             =   540
            Width           =   3480
         End
         Begin VB.Label lblConfAlertaSinPedido 
            Caption         =   "DSolicitudes sin pedido asociado"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   690
            TabIndex        =   24
            Top             =   840
            Width           =   3480
         End
         Begin VB.Label lblConfAlertaErrIntegracion 
            Caption         =   "DSolicitudes con errores de integraci�n"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   690
            TabIndex        =   23
            Top             =   1155
            Width           =   3480
         End
      End
   End
   Begin VB.PictureBox picControl 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   360
      Left            =   0
      ScaleHeight     =   360
      ScaleWidth      =   15690
      TabIndex        =   10
      Top             =   9135
      Width           =   15690
      Begin VB.CommandButton cmdAbrirProc 
         Caption         =   "Abrir &proceso"
         Height          =   345
         Left            =   11580
         TabIndex        =   12
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPedidoDir 
         Caption         =   "Pedido directo"
         Height          =   345
         Left            =   12840
         TabIndex        =   11
         Top             =   0
         Width           =   1200
      End
      Begin VB.CommandButton cmdAsignar 
         Caption         =   "A&signar comprador"
         Height          =   345
         Left            =   2200
         TabIndex        =   2
         Top             =   0
         Width           =   1560
      End
      Begin VB.CommandButton cmdAprobar 
         Caption         =   "&Aprobar"
         Height          =   345
         Left            =   0
         TabIndex        =   0
         Top             =   0
         Width           =   1020
      End
      Begin VB.CommandButton cmdRechazar 
         Caption         =   "&Rechazar"
         Height          =   345
         Left            =   1140
         TabIndex        =   1
         Top             =   0
         Width           =   980
      End
      Begin VB.CommandButton cmdAnular 
         Caption         =   "&Anular"
         Height          =   345
         Left            =   7180
         TabIndex        =   6
         Top             =   0
         Width           =   980
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "&Modificar"
         Height          =   345
         Left            =   3880
         TabIndex        =   3
         Top             =   0
         Width           =   980
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "&Cerrar"
         Height          =   345
         Left            =   4980
         TabIndex        =   4
         Top             =   0
         Width           =   980
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   8280
         TabIndex        =   7
         Top             =   0
         Width           =   980
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Left            =   9380
         TabIndex        =   8
         Top             =   0
         Width           =   980
      End
      Begin VB.CommandButton cmdReabrir 
         Caption         =   "R&eabrir"
         Height          =   345
         Left            =   6080
         TabIndex        =   5
         Top             =   0
         Width           =   980
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Height          =   345
         Left            =   10480
         TabIndex        =   9
         Top             =   0
         Width           =   980
      End
   End
End
Attribute VB_Name = "frmSolicitudes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_sIncrementoAltura As Single

'Variables p�blicas
Public g_sOrigen As String
Public g_ofrmDetalleSolic As frmSolicitudDetalle

'Variables privadas
Private m_oIBaseDatos As IBaseDatos
Private m_oInstancias As CInstancias

'Para el filtro de UO
Public sUON1 As String
Public sUON2 As String
Public sUON3 As String

'filtro de materiales
Private m_sGMN1Cod As String
Private m_sGMN2Cod As String
Private m_sGMN3Cod As String
Private m_sGMN4Cod As String

Public g_oCenCos As CCentroCoste
Public g_oPartida As CContratopres
Public g_oConfVisorSol As CConfVistaVisorSol
Public g_oConfVisorSolPPs As CConfVistasVisorSolPP

Private m_bCargandoPantalla As Boolean
Private m_bCargandoFiltro As Boolean
Private m_sNombreExcel As String
Private m_slblProgreso As String
Private m_sCaptionsGrid(11) As String ' los caption de la grid
Private m_sMensajeExito As String
Private m_sPageNumber As String
Private m_sSolicitud As String

'Seguridad
Private m_bRDep As Boolean
Private m_bRuo As Boolean
Private m_bREquipo As Boolean
Private m_bRAsig As Boolean
Private m_bAprobRechazoCierre As Boolean
Private m_bAnular As Boolean
Private m_bEliminar As Boolean
Private m_bAsignar As Boolean
Private m_bReabrir As Boolean
Private m_bModificar As Boolean
Private m_bAbrirProc As Boolean
Private m_bEnviarProc As Boolean
Private m_bPedidoDirecto As Boolean
Private m_bVerEnCurso As Boolean
Private m_bRestrMatComp As Boolean
Private m_bRPerfUON As Boolean
Private m_bAbrirProcEnCurso As Boolean

'Variables de idiomas:
Private m_sPendiente As String
Private m_sAprobada As String
Private m_sRechazada As String
Private m_sAnulada As String
Private m_sCerrada As String
Private sId As String
Private sFecDesde As String
Private sFecHasta As String
Private sImporteHasta As String
Private sImporteDesde As String
Private sOrdenando As String
Private m_sEnCurso As String
Private m_sGenRechazada As String
Private m_sProveedor As String

Private m_bRespetarCombo As Boolean

Private m_sMensajeEnProceso As String
Private m_sMensajeSinProceso As String
Private m_sMensajeSinPedido As String
Private m_sMensajeErrorIntegracion As String

'Para el filtro por materiales/articulos
Private m_bEditandoArticulo As Boolean
Private m_bEditandoArticuloConDropDown As Boolean
Private m_bEditandoArticuloDen As Boolean
Private m_oGMN4Seleccionado As CGrupoMatNivel4

'Para el filtro de centros de centro coste
Private m_sUON1_CC As String
Private m_sUON2_CC As String
Private m_sUON3_CC As String
Private m_sUON4_CC As String
Public sUON_CC As String 'se pasa al formulario de listado de solicitudes
Private m_bCentroCosteValidado As Boolean
Private m_oCentrosCoste As CCentrosCoste

'Para el filtro de partidas
Private m_asPres5Niv0() As String
Private m_aiNivelImp() As Integer
Private m_asPres5UON() As String
Private m_aPres5() As TipoConfigVisorSolPP
Private m_bPres5Validado As Boolean
Private m_oContratosPres As CContratosPres

'Para los filtros de configuracion de alerta
Private m_SinPedido As Boolean
Private m_SinProceso As Boolean
Private m_ErrorIntegracion As Boolean
Private m_bHayIntegracionSalida As Boolean
Private m_bHayIntegracionSolicitudes As Boolean
'
Private bRespetarComboProve As Boolean
Private bCargarComboDesde As Boolean

'3105
Private g_ofrmDesgloseValores As frmSolicitudDesglose
'3114
Private m_bPermProcMultimaterial As Boolean  'Permitir abrir procesos multimaterial
Private m_sIdiErrorEvaluacion(2) As String

Private m_sIdiBusAvanzadaMostrar As String
Private m_sIdiBusAvanzadaOcultar As String
Private m_sIdiOcultar As String
Private m_sIdiMostrar As String
'*************************
'  GESTION DE ERRORES
'*************************
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_sMsgError As String

'---------------------------------------------------------------------------------------
' Procedure : Arrange
' Author    : gfa
' Date      : 18/02/2016
' Purpose   : Se trara de posicionar los controles en la ventana en funcion del tama�o de la misma
'---------------------------------------------------------------------------------------
'
Private Sub Arrange()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next
If Me.Width < 400 Then Exit Sub
If Me.Height < 2300 Then Exit Sub

cmdBuscar.Top = cmdLimpiarTodo.Top
If lblOcultar.caption = m_sIdiMostrar Then
    picTop.Height = 0
Else
    picTop.Height = cmdBuscar.Top + cmdBuscar.Height + 120
End If
ShapeSelecc.Height = picTop.Height - 140
picBottom.Height = Me.Height - picTop.Top - picTop.Height - picControl.Height - 565
picBottom.Top = Me.Height - picBottom.Height - picControl.Height - 565
picBottom.Width = Me.Width - (picBottom.Left * 2) - 100
picTop.Width = picBottom.Width
'Colocampos el recuadro interior de la parte de arriba
ShapeSelecc.Width = picTop.Width - (ShapeSelecc.Left * 2) - 30
'Colocamos los botones en los extremos para que quede encuadrado
cmdExcel.Left = ShapeSelecc.Width - ShapeSelecc.Left - cmdExcel.Width
cmdImprimir.Left = cmdExcel.Left - cmdExcel.Width - 60
cmdLimpiarTodo.Left = ShapeSelecc.Width - ShapeSelecc.Left - cmdLimpiarTodo.Width
cmdBuscar.Left = cmdLimpiarTodo.Left - cmdLimpiarTodo.Width - 80
picGridToolbar.Width = picBottom.Width
lblOcultar.Left = picGridToolbar.Width - lblOcultar.Width - 250
picAbrirCerrar(2).Left = lblOcultar.Left - picAbrirCerrar(3).Width - 20
picAbrirCerrar(3).Left = picAbrirCerrar(2).Left
Line1.X1 = picAbrirCerrar(3).Left - 80
Line1.X2 = Line1.X1
lblConfigurarAlerta.Left = Line1.X1 - 80 - lblConfigurarAlerta.Width
Line2.X1 = lblConfigurarAlerta.Left - 80
Line2.X2 = Line2.X1
picGrid.Width = picBottom.Width - picGrid.Left - 60
picGrid.Height = picBottom.Height - picGrid.Top - 60
sdbgSolicitudes.Width = picGrid.Width - 120
sdbgSolicitudes.Height = picGrid.Height - 60
If fraConfAlerta.Visible Then
    fraConfAlerta.Left = picBottom.Width - fraConfAlerta.Width - 185
End If
If sdbgSolicitudes.Rows > 0 Then
    Dim vbm As Variant
    vbm = sdbgSolicitudes.Bookmark
    sdbgSolicitudes.Bookmark = 0
End If
If sdbgSolicitudes.Rows > 0 Then
    'Recup�ro el bookmark y si no se ve la fila la pongo la priemra visible
    sdbgSolicitudes.Bookmark = vbm
    If sdbgSolicitudes.AddItemRowIndex(sdbgSolicitudes.Bookmark) < sdbgSolicitudes.AddItemRowIndex(sdbgSolicitudes.FirstRow) Or sdbgSolicitudes.AddItemRowIndex(sdbgSolicitudes.Bookmark) > sdbgSolicitudes.AddItemRowIndex(sdbgSolicitudes.FirstRow) + sdbgSolicitudes.VisibleRows - 1 Then
        sdbgSolicitudes.FirstRow = sdbgSolicitudes.Bookmark
    End If
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudes", "Arrange", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdAyuda_Click()
    MostrarFormPROCEAyuda oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub

''' <summary>
''' Cuando se deschequea se comprueba si existe alg�n check m�s marcado.
''' En caso contrario no permite quitar el check
'''
''' Salvo al entrar pq esto hacia q "pendientes" se quedara activado salvo q "en curso de aprobaci�n" se activara antes
''' </summary>
''' <param name="Index">Identidad del check del q cambia el estado</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub chkEstado_Click(Index As Integer)
    Dim i As Integer
    Dim bCancelar As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bCargandoPantalla Then Exit Sub
    
    If m_bCargandoFiltro = True Then g_oConfVisorSol.HayCambios = True
    
    'cuando se deschequea se comprueba si existe alg�n check m�s marcado.
    'En caso contrario no permite quitar el check
    
    If chkEstado(Index).Value = vbUnchecked Then
        bCancelar = True
        For i = 0 To chkEstado.Count - 1
            If chkEstado(i).Value = vbChecked Then
                bCancelar = False
                Exit For
            End If
        Next i
        If bCancelar = True Then
            chkEstado(Index).Value = vbChecked
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "chkEstado_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
''' <summary>
''' Abrir proceso
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAbrirProc_Click()
    Dim arSolicitudes() As CInstancia
    Dim vbm As Variant
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    With sdbgSolicitudes
        If .Rows = 0 Then Exit Sub
        If .Row = -1 Then Exit Sub
        If .SelBookmarks Is Nothing Then Exit Sub
        If .SelBookmarks.Count = 0 Then .SelBookmarks.Add .Bookmark
            
        Screen.MousePointer = vbHourglass
        
        'Si hay m�s de una solicitud seleccionada
        If Not ComprobarAbrirProceso Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        If m_bPermProcMultimaterial Then
            If Not ComprobarProcesoMultimaterial Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
        
        ReDim arSolicitudes(0 To .SelBookmarks.Count - 1)
        Dim i As Integer
        i = 0
        For Each vbm In .SelBookmarks
            Set arSolicitudes(i) = m_oInstancias.Item(.Columns("ID").CellValue(vbm))
            i = i + 1
        Next
    End With
            
    Screen.MousePointer = vbNormal
    
    frmSOLAbrirProc.g_arSolicitudes = arSolicitudes
    frmSOLAbrirProc.m_iGenerar = 0
    frmSOLAbrirProc.Show vbModal
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdAbrirProc_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Comprueba si se va a abrir un proceso multimaterial</summary>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: cmdAbrirProc_Click</remarks>

Private Function ComprobarProcesoMultimaterial() As Boolean
    Dim bProcMultiMat As Boolean
    Dim vbm As Variant
    Dim oInstancia As CInstancia
    Dim Ador As Ador.Recordset
    Dim iResp As Integer
    
    ComprobarProcesoMultimaterial = True
    
    bProcMultiMat = False
    If Not sdbgSolicitudes.SelBookmarks Is Nothing Then
        If sdbgSolicitudes.SelBookmarks.Count > 0 Then
            For Each vbm In sdbgSolicitudes.SelBookmarks
                Set oInstancia = m_oInstancias.Item(sdbgSolicitudes.Columns("ID").CellValue(vbm))
                Set Ador = oInstancia.DevolverMaterialesInstancia_Campo
                If Ador.RecordCount > 1 Then
                    bProcMultiMat = True
                    Exit For
                Else
                    Set Ador = oInstancia.DevolverMaterialesInstancia_Desglose
                    If Ador.RecordCount > 1 Then
                        bProcMultiMat = True
                        Exit For
                    End If
                End If
            Next
            
            Set Ador = Nothing
            Set oInstancia = Nothing
        End If
    End If
    
    If bProcMultiMat Then
        iResp = oMensajes.MensajeYesNo(948)
        ComprobarProcesoMultimaterial = (iResp = vbYes)
    End If
End Function

''' <summary>Comprueba que se dan las condiciones necesarias para poder abrir un pedido desde varias solicitudes</summary>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: cmdPedidoDir_Click</remarks>

Private Function ComprobarAbrirPedido() As Boolean
    Dim vbm As Variant
    Dim oInstancia As CInstancia
    Dim bAbrir As Boolean
    Dim arSolicitudes As Variant
    Dim bBloqueoEtapa As Boolean
    Dim bBloqueoCondiciones As Boolean
    
    bAbrir = True
    
    If Not sdbgSolicitudes.SelBookmarks Is Nothing Then
        If sdbgSolicitudes.SelBookmarks.Count > 0 Then
            'Comprobar por cada tipo de solitud si el tipo de solicitud permite realizar pedidos directos aunque el flujo no est� finalizado
            For Each vbm In sdbgSolicitudes.SelBookmarks
                Set oInstancia = m_oInstancias.Item(sdbgSolicitudes.Columns("ID").CellValue(vbm))
                If oInstancia.Estado = EstadoSolicitud.GenRechazada Or (oInstancia.Estado < EstadoSolicitud.Pendiente And Not oInstancia.Solicitud.PermitirPedidoDirecto) Then
                    If IsEmpty(arSolicitudes) Then
                        ReDim arSolicitudes(0 To 0)
                    Else
                        ReDim Preserve arSolicitudes(0 To UBound(arSolicitudes) + 1)
                    End If
                    arSolicitudes(UBound(arSolicitudes)) = oInstancia.Id
                    
                    bAbrir = False
                End If
            Next
            Set oInstancia = Nothing
            
            If bAbrir Then
                'Se comprueban las condiciones de bloqueo por etapa para cada solicitud
                If gParametrosGenerales.gbSolicitudesCompras Then
                    For Each vbm In sdbgSolicitudes.SelBookmarks
                        Set oInstancia = m_oInstancias.Item(sdbgSolicitudes.Columns("ID").CellValue(vbm))
                        
                        bBloqueoEtapa = BloqueoEtapa(oInstancia.Id)
                        If bBloqueoEtapa Then
                            If IsEmpty(arSolicitudes) Then
                                ReDim arSolicitudes(0 To 0)
                            Else
                                ReDim Preserve arSolicitudes(0 To UBound(arSolicitudes) + 1)
                            End If
                            arSolicitudes(UBound(arSolicitudes)) = oInstancia.Id
                            
                            bAbrir = False
                        End If
                    Next
                    Set oInstancia = Nothing
                End If
                
                If bAbrir Then
                    'Se comprueban las condiciones de bloqueo por f�rmula
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        Dim bMostrarSolicitudEnMensaje As Boolean
                        bMostrarSolicitudEnMensaje = (sdbgSolicitudes.SelBookmarks.Count > 1)
                        
                        For Each vbm In sdbgSolicitudes.SelBookmarks
                            Set oInstancia = m_oInstancias.Item(sdbgSolicitudes.Columns("ID").CellValue(vbm))
                            
                            'Se comprueban las condiciones de bloqueo por f�rmula
                            bBloqueoCondiciones = BloqueoCondiciones(oInstancia.Id, TipoBloqueo.PedidosDirectos, m_sIdiErrorEvaluacion(1), m_sIdiErrorEvaluacion(2), bMostrarSolicitudEnMensaje, m_sSolicitud)
                            If bBloqueoCondiciones Then
                                bAbrir = False
                                Exit For
                            End If
                        Next
                        Set oInstancia = Nothing
                    End If
                Else
                    oMensajes.ImposibleAperturaPedidoEtapasMultiSolicitud arSolicitudes
                End If
            Else
                oMensajes.NoPermitidoPedidoMultiSolicitud arSolicitudes
            End If
        End If
    End If
    
    ComprobarAbrirPedido = bAbrir
End Function

''' <summary>Comprueba que se dan las condiciones necesarias para poder abrir un proceso desde varias solicitudes</summary>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: cmdAbrirProc_Click</remarks>

Private Function ComprobarAbrirProceso() As Boolean
    Dim vbm As Variant
    Dim oInstancia As CInstancia
    Dim bAbrir As Boolean
    Dim arSolicitudes As Variant
    Dim bBloqueoEtapa As Boolean
    
    bAbrir = True
    
    If Not sdbgSolicitudes.SelBookmarks Is Nothing Then
        If sdbgSolicitudes.SelBookmarks.Count > 0 Then
            For Each vbm In sdbgSolicitudes.SelBookmarks
                Set oInstancia = m_oInstancias.Item(sdbgSolicitudes.Columns("ID").CellValue(vbm))
                                
                'Las solicitudes no pueden estar en estado rechazada, anulada o cerrada
                If oInstancia.Estado = EstadoSolicitud.GenRechazada Or oInstancia.Estado = EstadoSolicitud.Rechazada Or oInstancia.Estado = EstadoSolicitud.Anulada Or oInstancia.Estado = EstadoSolicitud.Cerrada Then
                    If IsEmpty(arSolicitudes) Then
                        ReDim arSolicitudes(0 To 0)
                    Else
                        ReDim Preserve arSolicitudes(0 To UBound(arSolicitudes) + 1)
                    End If
                    arSolicitudes(UBound(arSolicitudes)) = oInstancia.Id
                    
                    bAbrir = False
                End If
                
                'Si no tiene permiso para abrir procesos desde solicitudes en curso de aprobaci�n validar que el estado no sea "en curso de aprobaci�n"
                If Not m_bAbrirProcEnCurso And oInstancia.Estado = EstadoSolicitud.GenPendiente Then
                    If IsEmpty(arSolicitudes) Then
                        ReDim arSolicitudes(0 To 0)
                    Else
                        ReDim Preserve arSolicitudes(0 To UBound(arSolicitudes) + 1)
                    End If
                    arSolicitudes(UBound(arSolicitudes)) = oInstancia.Id
                    
                    bAbrir = False
                End If
            Next
            Set oInstancia = Nothing
            
            If bAbrir Then
                'Comprobar que los formularios de todas las solicitudes seleccionadas son el mismo y que todos tienen la misma moneda
                If sdbgSolicitudes.SelBookmarks.Count > 1 Then
                    Dim lIdForm As Long
                    Dim sMoneda As String
                    For Each vbm In sdbgSolicitudes.SelBookmarks
                        Set oInstancia = m_oInstancias.Item(sdbgSolicitudes.Columns("ID").CellValue(vbm))
                        
                        If lIdForm = 0 Then
                            If Not oInstancia.Solicitud.Formulario Is Nothing Then
                                lIdForm = oInstancia.Solicitud.Formulario.Id
                            Else
                                bAbrir = False
                                Exit For
                            End If
                            sMoneda = oInstancia.Moneda
                        Else
                            If Not oInstancia.Solicitud.Formulario Is Nothing Then
                                If lIdForm <> oInstancia.Solicitud.Formulario.Id Or sMoneda <> oInstancia.Moneda Then
                                    bAbrir = False
                                    Exit For
                                End If
                            Else
                                bAbrir = False
                                Exit For
                            End If
                        End If
                    Next
                End If
                
                If bAbrir Then
                    'Comprobar bloqueos de etapa
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        For Each vbm In sdbgSolicitudes.SelBookmarks
                            bBloqueoEtapa = False
                            Set oInstancia = m_oInstancias.Item(sdbgSolicitudes.Columns("ID").CellValue(vbm))
                            
                            'Si el usuario puede abrir proceso o emitir pedido mientras la solicitud est� en curso hay que comprobar si la etapa en la que se encuetra la solicitud
                            'Se comprueban las condiciones de bloqueo por etapa
                            bBloqueoEtapa = BloqueoEtapaProceso(oInstancia)
                            
                            If bBloqueoEtapa Then
                                If IsEmpty(arSolicitudes) Then
                                    ReDim arSolicitudes(0 To 0)
                                Else
                                    ReDim Preserve arSolicitudes(0 To UBound(arSolicitudes) + 1)
                                End If
                                arSolicitudes(UBound(arSolicitudes)) = oInstancia.Id
                                
                                bAbrir = False
                            End If
                        Next
                        Set oInstancia = Nothing
                        
                        If Not bAbrir Then
                            oMensajes.ImposibleAperturaProcesoEtapasMultiSolicitud arSolicitudes
                        End If
                    End If
                Else
                    If oInstancia.Solicitud.Formulario Is Nothing Then
                        oMensajes.ImposibleAperturaProcesoFormulariosDistintos
                    ElseIf lIdForm <> oInstancia.Solicitud.Formulario.Id Then
                        oMensajes.ImposibleAperturaProcesoFormulariosDistintos
                    ElseIf sMoneda <> oInstancia.Moneda Then
                        oMensajes.ImposibleAperturaProcesoMonedasDistintas
                    End If
                End If
                
                Set oInstancia = Nothing
            Else
                oMensajes.ImposibleAbrirProcEmitirPedDesdeSolicitudes arSolicitudes
            End If
        End If
    End If
    
    ComprobarAbrirProceso = bAbrir
End Function

Private Function BloqueoEtapaProceso(ByRef oInstancia As CInstancia) As Boolean
    Dim Ador As Ador.Recordset
        
    Set Ador = oInstancia.ComprobarBloqueoEtapa()
    
    If Not Ador.EOF Then
        BloqueoEtapaProceso = IIf(Ador.Fields("BLOQUEO_APERTURA").Value = 1, False, True)
    Else
        BloqueoEtapaProceso = False
    End If
    
    Ador.Close
    Set Ador = Nothing
End Function

Private Sub cmdAnular_Click()
    AnularSolicitud
End Sub

Private Sub cmdAprobar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AprobarSolicitud
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdAprobar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

Private Sub cmdAsignar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AsignarComprador
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdAsignar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Carga el grid
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdBuscar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_oInstancias.PaginaAMostrar = 1
    m_oInstancias.PaginaActual = 0
    CargarGridSolicitudes gParametrosInstalacion.giOrdenVisorSol
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdBuscar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBuscarCentroCoste_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSelCenCoste.g_sOrigen = "frmSolicitudes"
    frmSelCenCoste.g_bSaltarComprobacionArbol = True
    frmSelCenCoste.Show vbModal
    If Not g_oCenCos Is Nothing Then
        If Len(g_oCenCos.Cod) > 0 Then
            If Len(g_oCenCos.CodConcat) > 0 Then
                txtCentroCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & " (" & g_oCenCos.CodConcat & ")"
            Else
                txtCentroCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
            End If
            txtCentroCoste.Tag = g_oCenCos.Cod
            m_bCentroCosteValidado = True
            
            m_sUON1_CC = g_oCenCos.UON1
            m_sUON2_CC = g_oCenCos.UON2
            m_sUON3_CC = g_oCenCos.UON3
            m_sUON4_CC = g_oCenCos.UON4
            
            sUON_CC = FormatearUON(g_oCenCos)
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdBuscarCentroCoste_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Function FormatearUON(ByVal CentroCoste As CCentroCoste)
    Dim sUON As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Function
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If CentroCoste.UON1 <> "" Then
        sUON = sUON & CentroCoste.UON1
        If CentroCoste.UON2 <> "" Then
            sUON = sUON & "#" & CentroCoste.UON2
            If CentroCoste.UON3 <> "" Then
                sUON = sUON & "#" & CentroCoste.UON3
                If CentroCoste.UON4 <> "" Then
                    sUON = sUON & "#" & CentroCoste.UON4
                End If
            End If
        End If
    End If
    FormatearUON = sUON

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "FormatearUON", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub cmdBuscarContrato_Click(Index As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSelCContables.g_sOrigen = "frmSolicitudes"
    frmSelCContables.g_sPres0 = m_asPres5Niv0(Index)
    frmSelCContables.g_iNivel = m_aiNivelImp(Index)
    frmSelCContables.Show vbModal
    If Not g_oPartida Is Nothing Then
        If Len(g_oPartida.Cod) > 0 Then
                        
            txtContrato(Index).Text = g_oPartida.Cod & " - " & g_oPartida.Den
            txtContrato(Index).Tag = g_oPartida.Cod
            m_asPres5UON(Index) = FormatearPres5o(m_asPres5Niv0(Index), g_oPartida)
                        
            m_aPres5(Index).sPres5_Nivel0 = m_asPres5Niv0(Index)
            m_aPres5(Index).sPres5_Nivel1 = g_oPartida.Pres1
            m_aPres5(Index).sPres5_Nivel2 = g_oPartida.Pres2
            m_aPres5(Index).sPres5_Nivel3 = g_oPartida.Pres3
            m_aPres5(Index).sPres5_Nivel4 = g_oPartida.Pres4
                        
            m_bPres5Validado = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdBuscarContrato_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Function FormatearPres5o(ByVal sPres5Niv0 As String, ByVal Partida As CContratopres) As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Function
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   FormatearPres5o = FormatearPres5s(sPres5Niv0, Partida.Pres1, Partida.Pres2, Partida.Pres3, Partida.Pres4)

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "FormatearPres5o", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function FormatearPres5s(ByVal sPres5Niv0 As String, ByVal sPres5Niv1 As String, ByVal sPres5Niv2 As String, ByVal sPres5Niv3 As String, ByVal sPres5Niv4 As String) As String
    Dim sPRES5 As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sPRES5 = sPres5Niv0
    If sPres5Niv1 <> "" Then
        sPRES5 = sPRES5 & "@" & sPres5Niv1
        If sPres5Niv2 <> "" Then
            sPRES5 = sPRES5 & "|" & sPres5Niv2
            If sPres5Niv3 <> "" Then
                sPRES5 = sPRES5 & "|" & sPres5Niv3
                If sPres5Niv4 <> "" Then
                    sPRES5 = sPRES5 & "|" & sPres5Niv4
                End If
            End If
        End If
    End If
    FormatearPres5s = sPRES5
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "FormatearPres5s", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub cmdBuscarMateriales_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELMAT.sOrigen = Me.Name
    frmSELMAT.bRComprador = m_bRestrMatComp
    frmSELMAT.m_PYME = oUsuarioSummit.Pyme
    frmSELMAT.Hide
    frmSELMAT.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdBuscarMateriales_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Buscar proveedor
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdBuscarProveedor_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmPROVEBuscar.sOrigen = "frmSolicitudes"
    frmPROVEBuscar.bRMat = m_bRestrMatComp
    frmPROVEBuscar.Hide
    frmPROVEBuscar.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdBuscarProveedor_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBuscarUO_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELUO.sOrigen = "frmSolicitudes"
    frmSELUO.bRUO = m_bRuo
    frmSELUO.bRPerfUON = m_bRPerfUON
    frmSELUO.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdBuscarUO_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub cmdCancelConfAlerta_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    chkConfAlertaErrIntegracion.Value = BooleanToSQLBinary(m_ErrorIntegracion)
    chkConfAlertaSinProceso.Value = BooleanToSQLBinary(m_SinProceso)
    chkConfAlertaSinPedido.Value = BooleanToSQLBinary(m_SinPedido)
    fraConfAlerta.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdCancelConfAlerta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Se genera un excel con el contenido del visor de solicitudes
''' </summary>
''' <remarks>Llamada desde: al hacer click sobre el bot�n; Tiempo m�ximo: depende del contenido del visor</remarks>
Private Sub cmdExcel_Click()
    Dim sTemp, sTemp1 As String
    Dim fso As Scripting.FileSystemObject
    Dim oFile As File
    Dim lInit As Long
    Dim sFile As String
    Dim sFileTitle As String
    Dim arrVisor() As Variant
    Dim i As Integer
    Dim j As Integer
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim oExcelAdoConn As ADODB.Connection
    Dim sConnect As String
    Dim sSQL As String
    Dim bDisplayAlerts As Boolean
    Dim appexcel As Object
    Dim wkb  As Object
    Dim oSheet As Object
    Dim sMateriales As String
    Dim sCodArticulo As String
    Dim lEmpresa As Long
    Dim sDepartamento As String
    Dim sComprador As String
    Dim sPartidas As String
    Dim oInstancias As CInstancias
    Dim lIdPerfil As Long
    Dim sproveedor As String
    Dim bcoincidnumsolerp As Boolean
    Dim strNumsolicitud As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oInstancias = oFSGSRaiz.Generar_CInstancias
    
    sTemp = DevolverPathFichTemp & "Solicitudes.xls"
    Set fso = New Scripting.FileSystemObject
    fso.CopyFile App.Path & "\PlantillaExport.xls", sTemp
    Set oFile = fso.GetFile(sTemp)
    If oFile.Attributes And 1 Then
        oFile.Attributes = oFile.Attributes Xor 1
    End If
    Set oFile = Nothing
    
    If sdbgSolicitudes.Rows = 0 Then
        oMensajes.NoHayDatos
        Timer1.Enabled = False
        Exit Sub
    Else
        Dim oInstancia As CInstancia
        Dim sPet As String
        Dim vEstado() As Variant
        Dim sestado As String
        Dim iOrden As TipoOrdenacionSolicitudes
        Dim vFecNec As Variant
        Dim lTipo As Long
        Dim sAsignadoA As String
        Dim sCodAsignadoA As String
        Dim sTrasladadaProv As String
        
        If sdbcPeticionario.Text <> "" Then sPet = sdbcPeticionario.Columns(0).Value
        If sdbcTipoSolicit.Text <> "" Then lTipo = sdbcTipoSolicit.Columns("ID").Value
                
        'Obtiene los estados
        i = 1
        If Trim(txtId.Text) = "" Then
            'S�lo busca por los estados cuando no se ha introducido el identificador
            If chkEstado(0).Value = vbChecked Then  'Pendiente:cuando se ha aprobado desde el WS (si hab�a workflow) o si no hab�a cuando se ha enviado
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.Pendiente
                i = i + 1
            End If
            If chkEstado(1).Value = vbChecked Then
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.Aprobada
                i = i + 1
            End If
            If chkEstado(2).Value = vbChecked Then
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.Rechazada
                i = i + 1
            End If
            If chkEstado(3).Value = vbChecked Then
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.Anulada
                i = i + 1
            End If
            If chkEstado(4).Value = vbChecked Then
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.Cerrada
                i = i + 1
            End If
            If chkEstado(5).Value = vbChecked Then
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.GenGuardada
                i = i + 1
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.GenPendiente
                i = i + 1
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.GenEnviada
                i = i + 1
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.GenAprobada
                i = i + 1
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.GenRechazada
            End If
        Else
            ReDim Preserve vEstado(i)
            vEstado(i) = ""
        End If
       
        If m_sGMN1Cod <> "" Then
            sMateriales = Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(m_sGMN1Cod)) & m_sGMN1Cod
            If m_sGMN2Cod <> "" Then
                sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(m_sGMN2Cod)) & m_sGMN2Cod
                If m_sGMN3Cod <> "" Then
                    sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(m_sGMN3Cod)) & m_sGMN3Cod
                    If m_sGMN4Cod <> "" Then
                        sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(m_sGMN4Cod)) & m_sGMN4Cod
                    End If
                End If
            End If
        End If
            
        If sdbcArticulo.Text <> "" Then sCodArticulo = sdbcArticulo.Columns(0).Value
        If sdbcProveedor.Text <> "" Then sproveedor = sdbcProveedor.Columns(0).Value
        If sdbcEmpresa.Text <> "" Then lEmpresa = sdbcEmpresa.Columns(0).Value
        If sdbcDepartamento.Text <> "" Then sDepartamento = sdbcDepartamento.Columns(0).Value
        If sdbcComprador.Text <> "" Then sComprador = sdbcComprador.Columns(0).Value
                
        If Not g_oParametrosSM Is Nothing Then
            If g_oParametrosSM.Count > 1 Then
                Dim vbm As Variant
                Dim Row As Integer
                Row = sdbgPartidas.Rows - 1
                While Row >= 0
                    vbm = sdbgPartidas.AddItemBookmark(Row)
                    If Len(sdbgPartidas.Columns("VALOR").CellValue(vbm)) > 0 Then
                        If Len(sPartidas) = 0 Then
                            sPartidas = sdbgPartidas.Columns("PRES5UON").CellValue(vbm)
                        Else
                            sPartidas = sPartidas & ";" & sdbgPartidas.Columns("PRES5UON").CellValue(vbm)
                        End If
                    End If
                    Row = Row - 1
                Wend
            Else
                sPartidas = m_asPres5UON(0)
            End If
        End If
        
        iOrden = 2
        
        If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
        
        'Num solicitud ERP
        
        bcoincidnumsolerp = True
        strNumsolicitud = Me.txtNumSolicErp.Text
        If InStr(1, Me.txtNumSolicErp.Text, "*") > 0 Then
            strNumsolicitud = Replace(Me.txtNumSolicErp.Text, "*", "%")
            bcoincidnumsolerp = False
        End If
    
        If m_bRuo Then
            oInstancias.BuscarSolicitudes iOrden, lTipo, Trim(txtId.Text), txtDescr.Text, sPet, vEstado, txtFecDesde.Text, txtFecHasta.Text, m_bRAsig, _
            m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, _
            basOptimizacion.gUON3Usuario, m_bRDep, basOptimizacion.gCodDepUsuario, basOptimizacion.gCodPersonaUsuario, , sMateriales, sCodArticulo, _
            txtDenArticulo.Text, NullToDbl0(StrToDblOrNull(txtImporteDesde.Text)), NullToDbl0(StrToDblOrNull(txtImporteHasta.Text)), sUON_CC, lEmpresa, _
            sDepartamento, sComprador, sPartidas, , , , , m_bRPerfUON, lIdPerfil, sproveedor, , sUON1, sUON2, sUON3, , strNumsolicitud, bcoincidnumsolerp
        Else
            oInstancias.BuscarSolicitudes iOrden, lTipo, Trim(txtId.Text), txtDescr.Text, sPet, vEstado, txtFecDesde.Text, txtFecHasta.Text, m_bRAsig, _
            m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, , , , m_bRDep, basOptimizacion.gCodDepUsuario, _
            basOptimizacion.gCodPersonaUsuario, , sMateriales, sCodArticulo, txtDenArticulo.Text, NullToDbl0(StrToDblOrNull(txtImporteDesde.Text)), _
            NullToDbl0(StrToDblOrNull(txtImporteHasta.Text)), sUON_CC, lEmpresa, sDepartamento, sComprador, sPartidas, , , , , m_bRPerfUON, _
            lIdPerfil, sproveedor, , sUON1, sUON2, sUON3, , strNumsolicitud, bcoincidnumsolerp

        End If
        
        ReDim arrVisor(oInstancias.Count + 1, 11)
        arrVisor(0, 0) = sdbgSolicitudes.Columns("TIPO").Name
        arrVisor(0, 1) = sdbgSolicitudes.Columns("ALTA").Name
        arrVisor(0, 2) = sdbgSolicitudes.Columns("NECESIDAD").Name
        arrVisor(0, 3) = sdbgSolicitudes.Columns("ID").Name
        arrVisor(0, 4) = sdbgSolicitudes.Columns("NUM_SOL_ERP").Name
        arrVisor(0, 5) = sdbgSolicitudes.Columns("DESCR").Name
        arrVisor(0, 6) = sdbgSolicitudes.Columns("IMPORTE").Name
        arrVisor(0, 7) = sdbgSolicitudes.Columns("PET").Name
        arrVisor(0, 8) = sdbgSolicitudes.Columns("ESTADO").Name
        arrVisor(0, 9) = sdbgSolicitudes.Columns("UON").Name
        arrVisor(0, 10) = sdbgSolicitudes.Columns("DEPT").Name
        arrVisor(0, 11) = sdbgSolicitudes.Columns("COMP").Name
        
        i = 1
        
        For Each oInstancia In oInstancias
            Select Case oInstancia.Estado
                Case EstadoSolicitud.Anulada
                    sestado = m_sAnulada
                Case EstadoSolicitud.Aprobada
                    sestado = m_sAprobada
                Case EstadoSolicitud.Cerrada
                    sestado = m_sCerrada
                Case EstadoSolicitud.Rechazada
                    sestado = m_sRechazada
                Case EstadoSolicitud.Pendiente
                    sestado = m_sPendiente
                Case EstadoSolicitud.GenRechazada
                    sestado = m_sGenRechazada
                Case Else  'En Curso de Aprobaci�n
                    sestado = m_sEnCurso
            End Select
        
            If IsNull(oInstancia.FecNecesidad) Then
                vFecNec = ""
            Else
                vFecNec = Format(oInstancia.FecNecesidad, "Short date")
            End If
            
            sAsignadoA = ""
            sCodAsignadoA = ""
            sTrasladadaProv = ""
            If Not oInstancia.comprador Is Nothing Then
                sAsignadoA = oInstancia.comprador.Cod & " - " & NullToStr(oInstancia.comprador.nombre) & " " & NullToStr(oInstancia.comprador.Apel)
                sCodAsignadoA = oInstancia.comprador.Cod
            ElseIf oInstancia.Estado < EstadoSolicitud.Pendiente Then  'Si est� dentro del workflow
                If oInstancia.Trasladada = True Then  'Si est� trasladada muestra el proveedor o usuario al que se le ha trasladado
                    If Not oInstancia.Destinatario Is Nothing Then
                        sAsignadoA = oInstancia.Destinatario.Cod & " - " & NullToStr(oInstancia.Destinatario.nombre) & " " & NullToStr(oInstancia.Destinatario.Apellidos)
                        sCodAsignadoA = oInstancia.Destinatario.Cod
                    ElseIf Not oInstancia.DestinatarioProv Is Nothing Then
                        sAsignadoA = oInstancia.DestinatarioProv.Cod & " - " & NullToStr(oInstancia.DestinatarioProv.Den)
                        sCodAsignadoA = oInstancia.DestinatarioProv.Cod
                        sTrasladadaProv = oInstancia.DestinatarioProv.Cod
                    End If
                ElseIf Not oInstancia.Aprobador Is Nothing Then  'muestra el aprobador actual
                    sAsignadoA = oInstancia.Aprobador.Cod & " - " & NullToStr(oInstancia.Aprobador.nombre) & " " & NullToStr(oInstancia.Aprobador.Apellidos)
                    sCodAsignadoA = oInstancia.Aprobador.Cod
                End If
            ElseIf Not oInstancia.Solicitud.Gestor Is Nothing Then
                sAsignadoA = oInstancia.Solicitud.Gestor.Cod & " - " & NullToStr(oInstancia.Solicitud.Gestor.nombre) & " " & NullToStr(oInstancia.Solicitud.Gestor.Apellidos)
                sCodAsignadoA = oInstancia.Solicitud.Gestor.Cod
            End If
            
            Dim sInstanciaConReemisiones As String
            
            If oInstancia.Num_Reemisiones > 0 Then
                sInstanciaConReemisiones = oInstancia.Id & " (" & (oInstancia.Num_Reemisiones) & ")"
            Else
                sInstanciaConReemisiones = oInstancia.Id
            End If
            
            arrVisor(i, 0) = NullToStr(oInstancia.Solicitud.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            arrVisor(i, 1) = Format(oInstancia.FecAlta, "Short date")
            arrVisor(i, 2) = vFecNec
            arrVisor(i, 3) = sInstanciaConReemisiones
            arrVisor(i, 4) = oInstancia.NumSolicitudErp
            arrVisor(i, 5) = oInstancia.DescrBreve
            arrVisor(i, 6) = oInstancia.importe / oInstancia.Cambio
            arrVisor(i, 7) = oInstancia.Peticionario.Cod & " - " & NullToStr(oInstancia.Peticionario.nombre) & " " & NullToStr(oInstancia.Peticionario.Apellidos)
            arrVisor(i, 8) = sestado
            arrVisor(i, 9) = oInstancia.UON_DEN
            arrVisor(i, 10) = oInstancia.Departamento
            arrVisor(i, 11) = sAsignadoA
            
            i = i + 1
        Next
        
        'Se carga el array en el excel
        Set oExcelAdoConn = New ADODB.Connection
        sConnect = "Provider=MSDASQL.1;" _
                 & "Extended Properties=""DBQ=" & sTemp & ";" _
                 & "Driver={Microsoft Excel Driver (*.xls)};" _
                 & "FIL=excel 8.0;" _
                 & "ReadOnly=0;" _
                 & "UID=admin;"""

        oExcelAdoConn.Open sConnect

        Set adoComm = New ADODB.Command
        Set adoComm.ActiveConnection = oExcelAdoConn
        sSQL = " CREATE TABLE [" & m_sNombreExcel & "] ("
        
        For j = 0 To 11
            If arrVisor(0, j) <> "" Then
                Select Case j
                Case 3
                    sSQL = sSQL & arrVisor(0, j) & " memo,"
                Case 6
                    sSQL = sSQL & arrVisor(0, j) & " number NULL,"
                Case 1
                    sSQL = sSQL & arrVisor(0, j) & " date NULL,"
                Case Else
                    sSQL = sSQL & arrVisor(0, j) & " memo,"
                End Select
            End If
        Next
        sSQL = Left(sSQL, Len(sSQL) - 1) & ")" 'para quitarle la ,
        oExcelAdoConn.Execute sSQL
        lInit = 0
        
        frmProgreso.ProgressBar.Value = 0
        frmProgreso.ProgressBar.Max = oInstancias.Count
        frmProgreso.lblTransferir = m_slblProgreso
        frmProgreso.Top = 3000
        frmProgreso.Left = 1500
        frmProgreso.Show
        DoEvents
        
        While lInit < oInstancias.Count
           For j = 1 To UBound(arrVisor, 1) - 1
                sSQL = " INSERT INTO [" & m_sNombreExcel & "$] values("
                For i = 0 To 11
                    If arrVisor(0, i) <> "" Then
                        Set adoParam = adoComm.CreateParameter("", adVarChar, adParamInput, 2000, Null)
                        adoComm.Parameters.Append adoParam
                        Select Case arrVisor(0, i)
                            Case "TIPO"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 0))) & "',"
                            Case "ALTA"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 1))) & "',"
                            Case "NECESIDAD"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 2))) & "',"
                            Case "ID"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 3))) & "',"
                            Case "NUM_SOL_ERP"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 4))) & "',"
                            Case "DESCR"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 5))) & "',"
                            Case "IMPORTE"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 6))) & "',"
                            Case "PET"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 7))) & "',"
                            Case "ESTADO"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 8))) & "',"
                            Case "UON"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 9))) & "',"
                            Case "DEPT"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 10))) & "',"
                            Case "COMP"
                                sSQL = sSQL & "'" & DblQuote(NullToStr(arrVisor(j, 11))) & "',"
                            
                        End Select
                    End If
                Next
                sSQL = Left(sSQL, Len(sSQL) - 1) & ")" 'para quitarle la ,
                adoComm.CommandText = sSQL
                adoComm.CommandType = adCmdText
                adoComm.Prepared = True
                adoComm.Execute
                lInit = lInit + 1
                frmProgreso.ProgressBar.Value = frmProgreso.ProgressBar.Value + 1
            Next
        Wend

        Unload frmProgreso

        Set adoComm = Nothing
        
        oExcelAdoConn.Close
        Set oExcelAdoConn = Nothing

        'Dar formato al excel con los anchos de las columnas definidos en g_oConfVisor
        Set appexcel = CreateObject("Excel.Application")
        Set wkb = appexcel.Workbooks.Add(sTemp)
        i = 1
        While wkb.Sheets(m_sNombreExcel).Cells(i).Value <> ""
            wkb.Sheets(m_sNombreExcel).Cells(i).Font.Bold = True
            Select Case wkb.Sheets(m_sNombreExcel).Cells(i).Value
                Case "TIPO"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(0)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.TipoWidth / 100
                Case "ALTA"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(1)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.AltaWidth / 100
                Case "NECESIDAD"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(2)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.NecesidadWidth / 100
                Case "ID"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(3)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.IdentWidth / 100
                Case "NUM_SOL_ERP"
                    If Not m_bHayIntegracionSolicitudes Then
                        wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = 0
                        wkb.Sheets(m_sNombreExcel).Columns(i).Hidden = True
                    Else
                        wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(4)
                        wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.NumSolErpWidth / 100
                    End If
                Case "DESCR"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(5)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.DenominacionWidth / 100
                Case "IMPORTE"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(6)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.ImporteWidth / 100
                Case "PET"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(7)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.PeticionarioWidth / 100
                Case "ESTADO"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(8)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.EstadoWidth / 100
                Case "UON"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(9)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.UONWidth / 100
                Case "DEPT"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(10)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.DepartamentoWidth / 100
                Case "COMP"
                    wkb.Sheets(m_sNombreExcel).Cells(i).Value = m_sCaptionsGrid(11)
                    wkb.Sheets(m_sNombreExcel).Cells(i).ColumnWidth = g_oConfVisorSol.AsignadoAWidth / 100
            End Select
            i = i + 1
        Wend
        
        'Le guardo con otro nombre porque al hacer cambios sale una ventana preguntando si se
        'van a guardar los cambios. Luego, cuando termine todo se borraran los dos excel temporales.
        sTemp1 = sTemp
        sTemp1 = Mid(sTemp, 1, Len(sTemp) - 4) & "1.xls"
        wkb.SaveAs sTemp1
         
        bDisplayAlerts = appexcel.DisplayAlerts
        appexcel.DisplayAlerts = False
                
        For Each oSheet In appexcel.worksheets
            If oSheet.Name <> m_sNombreExcel Then
                oSheet.Delete
                Exit For
            End If
        Next
        fso.DeleteFile sTemp
        wkb.SaveAs sTemp
        appexcel.DisplayAlerts = bDisplayAlerts
        appexcel.Quit
        Set appexcel = Nothing
       
        cmmdGenerarExcel.CancelError = True
        cmmdGenerarExcel.Filter = "Excel |*.xls"
        cmmdGenerarExcel.filename = "Solicitudes.xls"
        cmmdGenerarExcel.ShowSave
        
        sFile = cmmdGenerarExcel.filename
        sFileTitle = cmmdGenerarExcel.FileTitle
        If sFileTitle = "" Then
            fso.DeleteFile sTemp
            fso.DeleteFile sTemp1
            Set fso = Nothing
            Exit Sub
        Else
            'Mostrar mensaje de proceso finalizado
            fso.CopyFile sTemp, sFile
            oMensajes.MensajeOKOnly m_sMensajeExito, TipoIconoMensaje.Information
            fso.DeleteFile sTemp
            fso.DeleteFile sTemp1
            Set fso = Nothing
            Exit Sub
        End If
    End If
  
    Exit Sub
Error:

    If err.Number = cdlCancel Then
        fso.DeleteFile sTemp
        fso.DeleteFile sTemp1
        Set fso = Nothing
        Exit Sub
    Else
        Unload frmProgreso
        If err.Number <> 0 Then
              m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdExcel_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        End If
        If sTemp <> "" Then fso.DeleteFile sTemp
        If sTemp1 <> "" Then fso.DeleteFile sTemp1
        
        Set fso = Nothing
        Set appexcel = Nothing
        Exit Sub
    End If
End Sub

Private Sub cmdCerrar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    CerrarSolicitud
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdCerrar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdEliminar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    EliminarSolicitud
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdEliminar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Carga el grid, su primera pagina.
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdFirst_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_oInstancias.PaginaAMostrar = 1
    cmdFirst.Enabled = False
    cmdPrevious.Enabled = False
    cmdNext.Enabled = True
    cmdLast.Enabled = True
    
    CargarGridSolicitudes gParametrosInstalacion.giOrdenVisorSol
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdFirst_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdImprimir_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ListarProceso
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdImprimir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Carga el grid, su ultima pagina.
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdLast_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_oInstancias.PaginaAMostrar = m_oInstancias.TotalPaginas
    cmdLast.Enabled = False
    cmdNext.Enabled = False
    cmdFirst.Enabled = True
    cmdPrevious.Enabled = True
    
    CargarGridSolicitudes gParametrosInstalacion.giOrdenVisorSol
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdLast_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdLimpiarCentroCoste_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtCentroCoste.Text = ""
    Set g_oCenCos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdLimpiarCentroCoste_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdLimpiarContrato_Click(Index As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtContrato(Index).Text = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdLimpiarContrato_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdLimpiarMateriales_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtMateriales.Text = ""
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
    Set m_oGMN4Seleccionado = Nothing
    sdbcArticulo.Value = ""
    txtDenArticulo.Text = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdLimpiarMateriales_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdLimpiarTodo_Click()
Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sdbcTipoSolicit.Text = ""
txtId.Text = ""
txtDescr.Text = ""
txtFecDesde.Text = ""
txtFecHasta.Text = ""
cmdPeticBorrar_Click
cmdLimpiarUO_Click
For i = 0 To chkEstado.UBound
    chkEstado(i).Value = 0
Next i
cmdLimpiarMateriales_Click
sdbcArticulo.Text = ""
sdbcArticulo_Validate True
sdbcProveedor.Text = ""
sdbcProveedor_Validate True
cmdLimpiarCentroCoste_Click
cmdLimpiarContrato_Click 0
sdbcEmpresa.Text = ""
sdbcDepartamento.Text = ""
sdbcComprador.Text = ""
txtImporteDesde.Text = ""
txtImporteHasta.Text = ""
txtNumSolicErp.Text = ""
sdbgPartidas.RemoveAll
CargarPartidas2
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudes", "cmdLimpiarTodo_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdLimpiarUO_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtUO.Text = "" Then Exit Sub
    
    txtUO.Text = ""
    
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    
    If m_bCargandoFiltro = True Then g_oConfVisorSol.HayCambios = True
    
    DoEvents
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdLimpiarUO_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

Private Sub cmdlistado_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ListarProceso
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdlistado_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdModificar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ModificarSolicitud
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdModificar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Carga el grid, su pagina siguiente (paginaci�n).
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdNext_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_oInstancias.PaginaAMostrar = m_oInstancias.PaginaActual + 1
    If m_oInstancias.TotalPaginas = m_oInstancias.PaginaAMostrar Then
        cmdLast.Enabled = False
        cmdNext.Enabled = False
    End If
    cmdFirst.Enabled = True
    cmdPrevious.Enabled = True
    
    CargarGridSolicitudes gParametrosInstalacion.giOrdenVisorSol
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdNext_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Muestra / ocultas las columnas de aviso segun la configuraci�n
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdOKConfAlerta_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    fraConfAlerta.Visible = False
    CargarGridSolicitudes gParametrosInstalacion.giOrdenVisorSol
    sdbgSolicitudes.Scroll -100, 0
    sdbgSolicitudes.Update
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdOKConfAlerta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
''' <summary>
''' Crear pedido directo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdPedidoDir_Click()
    Dim irespuesta As Integer
    Dim arSolicitudes() As CInstancia
    Dim vbm As Variant
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    With sdbgSolicitudes
        If .Rows = 0 Then Exit Sub
        If .Row = -1 Then Exit Sub
        If .SelBookmarks Is Nothing Then Exit Sub
        If .SelBookmarks.Count = 0 Then .SelBookmarks.Add .Bookmark
        
        'Si el usuario no tiene permisos para emitir pedidos directos le sale un mensaje indic�ndoselo:
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIREmitir)) Is Nothing Then
            irespuesta = oMensajes.PreguntaGenerarPedDirecto
            If irespuesta = vbNo Then Exit Sub
        End If
            
        Screen.MousePointer = vbHourglass
        
        If Not ComprobarAbrirProceso Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        If Not ComprobarAbrirPedido Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        If m_bPermProcMultimaterial Then
            If Not ComprobarProcesoMultimaterial Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
            
        ReDim arSolicitudes(0 To .SelBookmarks.Count - 1)
        Dim i As Integer
        i = 0
        For Each vbm In .SelBookmarks
            Set arSolicitudes(i) = m_oInstancias.Item(.Columns("ID").CellValue(vbm))
            i = i + 1
        Next
    End With
               
    Screen.MousePointer = vbNormal
        
    frmSOLAbrirProc.g_arSolicitudes = arSolicitudes
    frmSOLAbrirProc.g_bGenerarPedido = True
    frmSOLAbrirProc.m_iGenerar = 3
    frmSOLAbrirProc.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdPedidoDir_Click", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdPeticBorrar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPeticionario.Text = ""
    sdbcPeticionario_Validate False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdPeticBorrar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Carga el grid, su pagina anterior (paginaci�n).
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdPrevious_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_oInstancias.PaginaAMostrar = m_oInstancias.PaginaActual - 1
    If m_oInstancias.PaginaAMostrar = 1 Then
        cmdFirst.Enabled = False
        cmdPrevious.Enabled = False
    End If
    cmdNext.Enabled = True
    cmdLast.Enabled = True
    
    CargarGridSolicitudes gParametrosInstalacion.giOrdenVisorSol
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdPrevious_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdReabrir_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ReabrirSolicitud
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdReabrir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdRechazar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    RechazarSolicitud
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdRechazar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Recarga el grid
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdRestaurar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_oInstancias.PaginaAMostrar = 1
    m_oInstancias.PaginaActual = 0
    CargarGridSolicitudes 4
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "cmdRestaurar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
    lblMostrarBusquedaAvanzada_Click
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub



''' <summary>
''' Carga el formulario de solicitudes, con la configuraci�n de los campos, el orden y el filtro
''' </summary>
''' <remarks>Llamada desde; Al cargarse el formulario</remarks>
Private Sub Form_Load()
       
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    m_bActivado = False
    
    Set m_oInstancias = oFSGSRaiz.Generar_CInstancias
    m_oInstancias.BusquedaConPaginacion = True

    Set m_oCentrosCoste = oFSGSRaiz.Generar_CCentrosCoste
    Set m_oContratosPres = oFSGSRaiz.Generar_CContratosPres
    lblMon.caption = gParametrosGenerales.gsMONCEN
    
    sdbgPartidas.RowHeight = 285
    
    Me.Width = 15600
    Me.Height = 10170
        
    CargarRecursos
    PonerFieldSeparator Me
    
    ConfigurarSeguridad
    
    'Cargar la combo sdbcTipoSolicit con todos los usuarios con acceso al EP
'    CargarTiposSolicitud
'    CargarPersonas
'    CargarArticulos
'    CargarEmpresas
'    CargarDepartamentos
'    CargarCompradores
    CargarStilos
    
    picAbrirCerrar(2).Visible = False
    picAbrirCerrar(3).Visible = True
    
    ReDim m_asPres5Niv0(0)
    ReDim m_aiNivelImp(0)
    ReDim m_asPres5UON(0)
    ReDim m_aPres5(0)
    
    'NOTA:
    'Para cargar las partidas hay dos metodos:
    'CargarPartidas1 -> crea un array de controles
    'CargarPartidas2 -> visualiza un grid
    'De momento utilizar el grid, pero mantener el otro codigo
    '(Por esto CargarPartidas2 utiliza los controles de indice 0)
    
    'CargarPartidas1
    CargarPartidas2
    
    lstCentroCoste.Left = txtCentroCoste.Left
    lstCentroCoste.Top = txtCentroCoste.Top + txtCentroCoste.Height
    lstCentroCoste.Height = 570
    
    If g_oParametrosSM Is Nothing Then
        lblCentroCoste.Visible = False
        txtCentroCoste.Visible = False
        cmdLimpiarCentroCoste.Visible = False
        cmdBuscarCentroCoste.Visible = False
        lstCentroCoste.Visible = False
        '''Si no activo en el men� avanzado mostramos el campo Num_solicitud_erp
        picNumSolicitErp.Left = lblProveedor.Left
        picNumSolicitErp.Top = lblProveedor.Top + lblProveedor.Height + 120
    End If
    
    'Cuando el usuario tenga la restricci�n �Restringir las acciones a la unidad organizativa del usuario�
    'y no tiene restricci�n a las UONs del perfil
    'los botones de borrar y seleccionar unidad organizativa estar�n no visibles
    'y la etiqueta vendr� precargada con la unidad organizativa del usuario
    '(mostraremos los c�digos y la denominaci�n de la UO).
    If m_bRuo And Not m_bRPerfUON Then
        sUON1 = basOptimizacion.gUON1Usuario
        sUON2 = basOptimizacion.gUON2Usuario
        sUON3 = basOptimizacion.gUON3Usuario
        cmdLimpiarUO.Visible = False
        cmdBuscarUO.Visible = False
        txtUO.Width = 2670
        PonerDenominaciondeUO sUON1, sUON2, sUON3
    Else
        cmdLimpiarUO.Visible = True
        cmdBuscarUO.Visible = True
        txtUO.Width = 1995
    End If
    
    ComprobarIntegracion
        
    'Carga de BD la configuraci�n de los campos, el orden y el filtro
    gParametrosInstalacion.gbOrdenVisorSolAscendente = True
    CargarConfiguracionVisorSol
    
    If Not g_oParametrosSM Is Nothing Then
        CargarConfiguracionVisorSolPP
    End If
    
    'comprueba que los filtros sean correctos y los aplica
    m_bCargandoPantalla = True
    ComprobarYAplicarFiltros
    m_bCargandoPantalla = False
    
    CargarGridSolicitudes gParametrosInstalacion.giOrdenVisorSol
    
    'Pone las columnas con el orden,tama�o y visibilidad con que est�n guardadas en BD para el usuario
    RedimensionarGrid
    
    MDI.mnuPopUpSolicitudes.Item(1).Visible = True 'detalle de desglose
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub PonerDenominaciondeUO(ByVal pUON1 As String, ByVal pUON2 As String, ByVal pUON3 As String)
    Dim oUnidades1 As CUnidadesOrgNivel1
    Dim oUnidades2 As CUnidadesOrgNivel2
    Dim oUnidades3 As CUnidadesOrgNivel3

    Dim strCadena As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If pUON3 <> "" Then
        Set oUnidades3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
        strCadena = oUnidades3.DevolverDenominacion(pUON1, pUON2, pUON3)
        If strCadena = "" Then
            txtUO.Text = ""
        Else
            txtUO.Text = pUON1 & " - " & pUON2 & " - " & pUON3 & " - " & strCadena
        End If
        Set oUnidades3 = Nothing
        
    ElseIf pUON2 <> "" Then
        Set oUnidades2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
        strCadena = oUnidades2.DevolverDenominacion(pUON1, pUON2)
        If strCadena = "" Then
            txtUO.Text = ""
        Else
            txtUO.Text = pUON1 & " - " & pUON2 & " - " & strCadena
        End If
        Set oUnidades2 = Nothing
        
    ElseIf pUON1 <> "" Then
        Set oUnidades1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
        strCadena = oUnidades1.DevolverDenominacion(pUON1)
        If strCadena = "" Then
            txtUO.Text = ""
        Else
            txtUO.Text = pUON1 & " - " & strCadena
        End If
        Set oUnidades1 = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "PonerDenominaciondeUO", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' 'Redimensiona el formulario
''' </summary>
''' <remarks>Llamada desde; Al cargarse, activarse y cambiar de tama�o el formulario</remarks>
Private Sub Form_Resize()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "Form_Resize", err, Erl)
      Exit Sub
   End If
End Sub

Private Sub MostrarBusquedaAvanzada()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
picBusquedaAvanzada.Visible = True
picAbrirCerrar(0).Visible = False
picAbrirCerrar(1).Visible = True
cmdLimpiarTodo.Top = picBusquedaAvanzada.Top + picBusquedaAvanzada.Height
lblMostrarBusquedaAvanzada.caption = m_sIdiBusAvanzadaOcultar
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "MostrarBusquedaAvanzada", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub OcultarBusquedaAvanzada()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
picBusquedaAvanzada.Visible = False
picAbrirCerrar(1).Visible = False
picAbrirCerrar(0).Visible = True
cmdLimpiarTodo.Top = lblMostrarBusquedaAvanzada.Top + lblMostrarBusquedaAvanzada.Height
cmdBuscar.Top = cmdLimpiarTodo.Top
lblMostrarBusquedaAvanzada.caption = m_sIdiBusAvanzadaMostrar
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "OcultarBusquedaAvanzada", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
''' <summary>
''' Carga los texto de pantalla
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = "" & Ador(0).Value       '1 Solicitudes de compra
        Ador.MoveNext
        sId = "" & Ador(0).Value  'Identificador
        lblIdentificador.caption = "" & Ador(0).Value & ":"
        Ador.MoveNext
        lblDescripcion.caption = "" & Ador(0).Value   'descripci�n
        Ador.MoveNext
        lblPeticionario.caption = "" & Ador(0).Value  'peticionario
        Ador.MoveNext
        lblFecDesde.caption = "" & Ador(0).Value & ":"  'desde
        sFecDesde = "" & Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = "" & Ador(0).Value & ":"  'hasta
        sFecHasta = "" & Ador(0).Value
        Ador.MoveNext
        chkEstado(0).caption = "" & Ador(0).Value   'pendientes
        Ador.MoveNext
        chkEstado(1).caption = "" & Ador(0).Value   'aprobadas
        Ador.MoveNext
        chkEstado(2).caption = "" & Ador(0).Value   'rechazadas
        Ador.MoveNext
        chkEstado(3).caption = "" & Ador(0).Value   'anuladas
        Ador.MoveNext
        chkEstado(4).caption = "" & Ador(0).Value   'cerradas
        Ador.MoveNext
        cmdAprobar.caption = "" & Ador(0).Value   'aprobar
        Ador.MoveNext
        cmdRechazar.caption = "" & Ador(0).Value   'rechazar
        Ador.MoveNext
        cmdAnular.caption = "" & Ador(0).Value   'anular
        Ador.MoveNext
        cmdCerrar.caption = "" & Ador(0).Value  'cerrar
        Ador.MoveNext
        cmdModificar.caption = "" & Ador(0).Value  'modificar
        Ador.MoveNext
        cmdEliminar.caption = "" & Ador(0).Value   'eliminar
        Ador.MoveNext
        cmdAsignar.caption = "" & Ador(0).Value  'asignar
        Ador.MoveNext
        cmdRestaurar.caption = "" & Ador(0).Value   'restaurar
        Ador.MoveNext
        
        sdbgSolicitudes.Columns("ALTA").caption = "" & Ador(0).Value  'alta
        m_sCaptionsGrid(1) = "" & Ador(0).Value
        Ador.MoveNext
        sdbgSolicitudes.Columns("NECESIDAD").caption = "" & Ador(0).Value  'necesidad
        m_sCaptionsGrid(2) = "" & Ador(0).Value
        Ador.MoveNext
        sdbgSolicitudes.Columns("ID2").caption = "" & Ador(0).Value  'identificador
        m_sCaptionsGrid(3) = "" & Ador(0).Value
        Ador.MoveNext
        sdbgSolicitudes.Columns("DESCR").caption = "" & Ador(0).Value  'descripci�n breve
        m_sCaptionsGrid(5) = "" & Ador(0).Value
        Ador.MoveNext
        sdbgSolicitudes.Columns("IMPORTE").caption = "" & Ador(0).Value  'importe
        m_sCaptionsGrid(6) = "" & Ador(0).Value
        Ador.MoveNext
        sdbgSolicitudes.Columns("PET").caption = "" & Ador(0).Value  'peticionario
        m_sCaptionsGrid(7) = "" & Ador(0).Value
        Ador.MoveNext
        sdbgSolicitudes.Columns("ESTADO").caption = "" & Ador(0).Value  'estado
        m_sCaptionsGrid(8) = "" & Ador(0).Value
        Ador.MoveNext
        sdbgSolicitudes.Columns("COMP").caption = "" & Ador(0).Value  'comprador
        m_sCaptionsGrid(11) = "" & Ador(0).Value
        
        Ador.MoveNext
        m_sPendiente = "" & Ador(0).Value   'Pendiente
        Ador.MoveNext
        m_sAprobada = "" & Ador(0).Value    'Aprobada
        Ador.MoveNext
        m_sRechazada = "" & Ador(0).Value   'Rechazada
        Ador.MoveNext
        m_sAnulada = "" & Ador(0).Value     'Anulada
        Ador.MoveNext
        m_sCerrada = "" & Ador(0).Value     'Cerrada
        
        Ador.MoveNext
        cmdReabrir.caption = "" & Ador(0).Value  'Reabrir
        
        Ador.MoveNext
        sOrdenando = "" & Ador(0).Value 'Ordenando...
        
        Ador.MoveNext
        sdbgSolicitudes.Columns("UO").caption = "" & Ador(0).Value  'unidad  organizativa
        m_sCaptionsGrid(9) = "" & Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = "" & Ador(0).Value
        
        Ador.MoveNext
        m_sEnCurso = "" & Ador(0).Value  'En curso de aprobaci�n
        chkEstado(5).caption = "" & Ador(0).Value
        
        Ador.MoveNext
        sdbcTipoSolicit.Columns("COD").caption = "" & Ador(0).Value  'C�digo
        sdbcArticulo.Columns("COD").caption = "" & Ador(0).Value
        sdbcDepartamento.Columns("COD").caption = "" & Ador(0).Value
        
        Ador.MoveNext
        sdbcTipoSolicit.Columns("DEN").caption = "" & Ador(0).Value  'Denominaci�n
        sdbcArticulo.Columns("DEN").caption = "" & Ador(0).Value
        sdbcDepartamento.Columns("DEN").caption = "" & Ador(0).Value
        sdbcEmpresa.Columns("DEN").caption = "" & Ador(0).Value
        
        Ador.MoveNext
        lblTipoSolicit.caption = "" & Ador(0).Value
        Ador.MoveNext
        sdbgSolicitudes.Columns("TIPO").caption = "" & Ador(0).Value  'Tipo
        m_sCaptionsGrid(0) = "" & Ador(0).Value  'Tipo
        Ador.MoveNext
        m_sProveedor = "" & Ador(0).Value 'Proveedor
        Ador.MoveNext
        m_sMensajeEnProceso = "" & Ador(0).Value 'La solicitud est� siendo procesada en estos momentos. En breves instantes estar� disponible para su gesti�n.
        Ador.MoveNext
        m_slblProgreso = "" & Ador(0).Value 'Exportando visor a excel
        Ador.MoveNext
        m_sMensajeExito = "" & Ador(0).Value ' La exportacion a excel ha finalizado con �xito
        Ador.MoveNext
        m_sNombreExcel = "" & Ador(0).Value 'Nombre de la hoja excel
        Ador.MoveNext
        lblUO.caption = "" & Ador(0).Value & ":"  'UON
        Ador.MoveNext
        sdbgSolicitudes.Columns("UON").caption = "" & Ador(0).Value  'Unidad org.
        Ador.MoveNext
        cmdBuscar.ToolTipText = "" & Ador(0).Value   'Actualizar
        Ador.MoveNext
        lblMateriales.caption = "" & Ador(0).Value
        Ador.MoveNext
        lblArticulo.caption = "" & Ador(0).Value
        Ador.MoveNext
        lblImporteDesde.caption = "" & Ador(0).Value
        sImporteDesde = "" & Ador(0).Value
        If Right(sImporteDesde, 1) = ":" Then sImporteDesde = Left(sImporteDesde, Len(sImporteDesde) - 1)
        Ador.MoveNext
        lblImporteHasta.caption = "" & Ador(0).Value
        sImporteHasta = "" & Ador(0).Value
        If Right(sImporteHasta, 1) = ":" Then sImporteHasta = Left(sImporteHasta, Len(sImporteHasta) - 1)
        Ador.MoveNext
        lblCentroCoste.caption = "" & Ador(0).Value
        Ador.MoveNext
        lblEmpresa.caption = "" & Ador(0).Value
        Ador.MoveNext
        lblDepartamento.caption = "" & Ador(0).Value
        Ador.MoveNext
        lblComprador.caption = "" & Ador(0).Value
        Ador.MoveNext
        m_sIdiBusAvanzadaMostrar = "" & Ador(0).Value
        Ador.MoveNext
        m_sIdiBusAvanzadaOcultar = "" & Ador(0).Value
        Ador.MoveNext
        lblConfigurarAlerta.caption = "" & Ador(0).Value
        Ador.MoveNext
        sdbgSolicitudes.Columns("DEPT").caption = "" & Ador(0).Value
        m_sCaptionsGrid(10) = "" & Ador(0).Value
        Ador.MoveNext
        lblConfAlertaTitulo.caption = "" & Ador(0).Value
        Ador.MoveNext
        lblConfAlertaSinProceso.caption = "" & Ador(0).Value
        Ador.MoveNext
        lblConfAlertaSinPedido.caption = "" & Ador(0).Value
        Ador.MoveNext
        lblConfAlertaErrIntegracion.caption = "" & Ador(0).Value
        Ador.MoveNext
        m_sMensajeSinProceso = "" & Ador(0).Value
        Ador.MoveNext
        m_sMensajeSinPedido = "" & Ador(0).Value
        Ador.MoveNext
        m_sMensajeErrorIntegracion = "" & Ador(0).Value '68
        Ador.MoveNext
        cmdOKConfAlerta.caption = "" & Ador(0).Value
        Ador.MoveNext
        cmdCancelConfAlerta.caption = "" & Ador(0).Value
        Ador.MoveNext
        sdbcEmpresa.Columns("NIF").caption = "" & Ador(0).Value
        Ador.MoveNext
        m_sPageNumber = "" & Ador(0).Value  '72
        Ador.MoveNext
        lblProveedor.caption = "" & Ador(0).Value  '73
        Ador.MoveNext
        cmdAbrirProc.caption = Ador(0).Value
        Ador.MoveNext
        cmdPedidoDir.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiErrorEvaluacion(1) = Ador(0).Value  '15 Error al realizar el c�lculo:F�rmula inv�lida.
        Ador.MoveNext
        m_sIdiErrorEvaluacion(2) = Ador(0).Value  '16 Error al realizar el c�lculo:Valores incorrectos.
        Ador.MoveNext
        lblNumSolicitErp.caption = Ador(0).Value & " : "
        sdbgSolicitudes.Columns("NUM_SOL_ERP").caption = Ador(0).Value
        m_sCaptionsGrid(4) = Ador(0).Value
        Ador.MoveNext
        m_sSolicitud = Ador(0).Value   'Solicitud
        Ador.MoveNext
        cmdLimpiarTodo.caption = Ador(0).Value
        Ador.MoveNext
        cmdBuscar.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiOcultar = Ador(0).Value
        lblOcultar.caption = m_sIdiOcultar
        Ador.MoveNext
        m_sIdiMostrar = Ador(0).Value
        Ador.MoveNext
        m_sGenRechazada = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'Almacena en BD la configuraci�n de campos y filtro del visor:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bDescargarFrm = False Then
    If g_oConfVisorSol.HayCambios = True Then
        GuardarConfiguracionVisorSol
    End If
    If Not g_oParametrosSM Is Nothing Then
        If g_oConfVisorSolPPs.HayCambios = True Then
            GuardarConfiguracionVisorSolPP
        End If
    End If
End If
      m_bDescargarFrm = False
    Set g_oConfVisorSolPPs = Nothing
    Set g_oConfVisorSol = Nothing
            
    Set m_oInstancias = Nothing
    Set m_oIBaseDatos = Nothing
    
    If Not g_ofrmDetalleSolic Is Nothing Then
        Unload g_ofrmDetalleSolic
        Set g_ofrmDetalleSolic = Nothing
    End If
    
    Set m_oGMN4Seleccionado = Nothing
    Set m_oCentrosCoste = Nothing
    Set m_oContratosPres = Nothing
    
    Unload frmSolicitudDetalle
    Me.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub lblConfigurarAlerta_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    fraConfAlerta.Left = picBottom.Width - fraConfAlerta.Width - 185
    fraConfAlerta.Visible = True
    fraConfAlerta.ZOrder
    m_SinProceso = SQLBinaryToBoolean(chkConfAlertaSinProceso.Value)
    m_SinPedido = SQLBinaryToBoolean(chkConfAlertaSinPedido.Value)
    m_ErrorIntegracion = SQLBinaryToBoolean(chkConfAlertaErrIntegracion.Value)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "lblConfigurarAlerta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub lblMostrarBusquedaAvanzada_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If lblMostrarBusquedaAvanzada.caption = m_sIdiBusAvanzadaMostrar Then
    MostrarBusquedaAvanzada
Else
    OcultarBusquedaAvanzada
End If

Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "lblMostrarBusquedaAvanzada_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub lblOcultar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If lblOcultar.caption = m_sIdiOcultar Then
    'Ocultar B�squeda
    picTop.Visible = False
    picAbrirCerrar(2).Visible = True
    picAbrirCerrar(3).Visible = False
    lblOcultar.caption = m_sIdiMostrar
Else
    'Mostrar B�squeda
    picTop.Visible = True
    picAbrirCerrar(2).Visible = False
    picAbrirCerrar(3).Visible = True
    lblOcultar.caption = m_sIdiOcultar
End If
Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudes", "lblOcultar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub lstCentroCoste_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtCentroCoste.Text = lstCentroCoste.List(lstCentroCoste.ListIndex)
    lstCentroCoste.Visible = False
    m_bCentroCosteValidado = True
    txtCentroCoste.Backcolor = &HC0FFC0
    
    Dim selectedItem As CCentroCoste
    Set selectedItem = m_oCentrosCoste.Item(lstCentroCoste.ListIndex + 1)
    If Not selectedItem Is Nothing Then
        m_sUON1_CC = selectedItem.UON1
        m_sUON2_CC = selectedItem.UON2
        m_sUON3_CC = selectedItem.UON3
        m_sUON4_CC = selectedItem.UON4
        sUON_CC = FormatearUON(selectedItem)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "lstCentroCoste_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub lstContrato_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtContrato(0).Text = lstContrato.List(lstContrato.ListIndex)
    lstContrato.Visible = False
    m_bPres5Validado = True
    txtContrato(0).Backcolor = &HC0FFC0

    Dim selectedItem As CContratopres
    Set selectedItem = m_oContratosPres.Item(lstContrato.ListIndex + 1)
    If Not selectedItem Is Nothing Then
        txtContrato(0).Tag = selectedItem.Cod
        m_asPres5UON(0) = FormatearPres5o(m_asPres5Niv0(0), selectedItem)
        m_aPres5(0).sPres5_Nivel0 = m_asPres5Niv0(0)
        m_aPres5(0).sPres5_Nivel1 = selectedItem.Pres1
        m_aPres5(0).sPres5_Nivel2 = selectedItem.Pres2
        m_aPres5(0).sPres5_Nivel3 = selectedItem.Pres3
        m_aPres5(0).sPres5_Nivel4 = selectedItem.Pres4
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "lstContrato_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcArticulo_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bEditandoArticulo = True
    m_bEditandoArticuloConDropDown = False
    If Not m_bEditandoArticuloDen Then
        txtDenArticulo.Text = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcArticulo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcArticulo_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Len(sdbcArticulo.Value) <> 0 Then
        txtDenArticulo.Text = sdbcArticulo.Columns(1).Text
    End If
    m_bEditandoArticuloConDropDown = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcArticulo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcArticulo_DropDown()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bEditandoArticulo = False
    m_bEditandoArticuloConDropDown = True
    sdbcArticulo.RemoveAll
    
    CargarArticulos

    sdbcArticulo.SelStart = 0
    sdbcArticulo.SelLength = Len(sdbcArticulo.Text)
    sdbcArticulo.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcArticulo_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>Carga el combo de peticionarios</summary>
''' <remarks>Llamada desde: sdbcPeticionario_DropDown</remarks>
''' <revision>LTG 16/01/2013</revision>

Private Sub CargarPersonas()
    Dim oPersona As CPersona
    Dim oPersonas As CPersonas
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oPersonas = oFSGSRaiz.Generar_CPersonas
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If m_bRuo Then
        oPersonas.CargarPeticionariosSolCompra True, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRPerfUON, lIdPerfil
    Else
        oPersonas.CargarPeticionariosSolCompra True, , , , , m_bRPerfUON, lIdPerfil
    End If
    
    For Each oPersona In oPersonas
        sdbcPeticionario.AddItem oPersona.Cod & Chr(m_lSeparador) & oPersona.Cod & " - " & NullToStr(oPersona.nombre) & " " & oPersona.Apellidos
    Next
    Set oPersonas = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarPersonas", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub CargarArticulos()
    Dim oArticulos As CArticulos
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
        
    If m_bRestrMatComp Then
        If Not m_oGMN4Seleccionado Is Nothing Or sdbcArticulo.Text <> "" Then
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, m_sGMN4Cod, False, Trim(sdbcArticulo), , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
        End If
    Else
        If m_oGMN4Seleccionado Is Nothing Then
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, , False, Trim(sdbcArticulo.Text), , , , , , , , , , , , , oUsuarioSummit.Pyme
        Else
            m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcArticulo.Text), , , , , , False
            Set oArticulos = m_oGMN4Seleccionado.ARTICULOS
        End If
    End If

    Dim oArt As CArticulo
    For Each oArt In oArticulos
        sdbcArticulo.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den
    Next
    
    Set oArticulos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarArticulos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub sdbcArticulo_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bEditandoArticulo And Len(sdbcArticulo.Text) <> 0 Then
        Dim oArticulos As CArticulos
        Set oArticulos = oFSGSRaiz.Generar_CArticulos
        If m_bRestrMatComp Then
            If Not m_oGMN4Seleccionado Is Nothing Or sdbcArticulo.Text <> "" Then
                oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, m_sGMN4Cod, True, Trim(sdbcArticulo), , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
            End If
        Else
            If m_oGMN4Seleccionado Is Nothing Then
                oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, , True, Trim(sdbcArticulo.Text), , True, , , , , , , , , , , oUsuarioSummit.Pyme
            Else
                m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcArticulo.Text), , True, , , , True
                Set oArticulos = m_oGMN4Seleccionado.ARTICULOS
            End If
        End If
        If oArticulos.Count = 0 Then
            oMensajes.NoValido 21
            sdbcArticulo.Text = ""
            txtDenArticulo.Text = ""
        Else
            sdbcArticulo.Columns(0).Value = oArticulos.Item(1).Cod
            sdbcArticulo.Columns(1).Value = oArticulos.Item(1).Den
            txtDenArticulo.Text = oArticulos.Item(1).Den
        End If
        Set oArticulos = Nothing
    End If
    m_bEditandoArticulo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcArticulo_Validate", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcComprador_DropDown()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcComprador.RemoveAll
    
    CargarCompradores

    sdbcComprador.SelStart = 0
    sdbcComprador.SelLength = Len(sdbcComprador.Text)
    sdbcComprador.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcComprador_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>Carga el combo de compradores</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 16/01/2013</revision>

Private Sub CargarCompradores()
    Dim oCompradores As CCompradores
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    Dim lIdPerfil As Long
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id

    If m_bRuo Then
        oCompradores.CargarTodosLosCompradoresPorUON , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, sdbcDepartamento.Value, True, "NOM", , , m_bRPerfUON, lIdPerfil
    Else
        oCompradores.CargarTodosLosCompradoresPorUON , , , , , , sdbcDepartamento.Value, True, "NOM", , oUsuarioSummit.Pyme, m_bRPerfUON, lIdPerfil
    End If

    sdbcComprador.AddItem vbNullString & Chr(m_lSeparador) & vbNullString
    Dim oComprador As CComprador
    For Each oComprador In oCompradores
        sdbcComprador.AddItem oComprador.Cod & Chr(m_lSeparador) & oComprador.Cod & " - " & oComprador.nombre & " " & oComprador.Apel
    Next

    Set oCompradores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarCompradores", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub sdbcComprador_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         sdbcComprador.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcComprador_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Evento que salta al producirse un cambio en el combo. Limpia el c�digo.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboProve Then
    
        bRespetarComboProve = True
        sdbcProveedor.Text = ""
        bRespetarComboProve = False
        
        bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcDenProveedor_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Evento que salta al producirse un click en el combo. Limpia si no esta desplagado
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcDenProveedor.DroppedDown Then
        sdbcProveedor = ""
        sdbcDenProveedor = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcDenProveedor_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Evento que salta al cerrarse el combo. Establece el codigo y denominaci�n elegidos.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDenProveedor.Value = "..." Then
        sdbcDenProveedor.Text = ""
        Exit Sub
    End If
    
    If sdbcDenProveedor.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveedor.Text = sdbcDenProveedor.Columns(1).Text
    sdbcProveedor.Columns(0).Value = sdbcDenProveedor.Columns(1).Text
    sdbcDenProveedor.Text = sdbcDenProveedor.Columns(0).Text
    bRespetarComboProve = False
    
    DoEvents
       
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcDenProveedor_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Evento que salta al desplegarse el combo. Carga las denominaciones.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_DropDown()
    Dim i As Integer
    Dim oProves As CProveedores
    Dim Codigos As TipoDatosCombo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    Screen.MousePointer = vbHourglass
    
    sdbcDenProveedor.RemoveAll
    If bCargarComboDesde Then
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcDenProveedor.Text), , True, , m_bRestrMatComp, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , , , True, , m_bRestrMatComp, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcDenProveedor.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not oProves.EOF Then
        sdbcDenProveedor.AddItem "..."
    End If

    sdbcDenProveedor.SelStart = 0
    sdbcDenProveedor.SelLength = Len(sdbcDenProveedor.Text)
    sdbcProveedor.Refresh

    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
    
    Set oProves = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcDenProveedor_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Evento que salta al seleccionar en el combo. Posicionarse en el combo segun la seleccion
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDenProveedor.DataFieldList = "Column 0"
    sdbcDenProveedor.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcDenProveedor_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Evento que salta al seleccionar en el combo. Posicionarse en el combo segun la seleccion
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_PositionList(ByVal Text As String)
PositionList sdbcDenProveedor, Text
End Sub

Private Sub sdbcDepartamento_DropDown()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDepartamento.RemoveAll

    CargarDepartamentos
    
    sdbcDepartamento.SelStart = 0
    sdbcDepartamento.SelLength = Len(sdbcDepartamento.Text)
    sdbcDepartamento.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcDepartamento_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarDepartamentos()
    Dim oDepartamentos As CDepartamentos
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
       
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If sUON1 <> "" Or sUON2 <> "" Or sUON3 <> "" Then
        oDepartamentos.CargarTodosLosDepartamentosPorUON , , sUON1, sUON2, sUON3, , True, , , m_bRPerfUON, lIdPerfil
    Else
        If m_bRuo Then
            oDepartamentos.CargarTodosLosDepartamentosPorUON , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , True, , , m_bRPerfUON, lIdPerfil
        Else
            oDepartamentos.CargarTodosLosDepartamentos , , , True, , , oUsuarioSummit.Pyme, m_bRPerfUON, lIdPerfil
        End If
    End If
    sdbcDepartamento.AddItem vbNullString & Chr(m_lSeparador) & vbNullString
    Dim oDepartamento As CDepartamento
    For Each oDepartamento In oDepartamentos
        sdbcDepartamento.AddItem oDepartamento.Cod & Chr(m_lSeparador) & oDepartamento.Den
    Next

    Set oDepartamentos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarDepartamentos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub sdbcDepartamento_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         sdbcDepartamento.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcDepartamento_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcEmpresa_DropDown()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass

    sdbcEmpresa.RemoveAll
        
    CargarEmpresas
    
    sdbcEmpresa.SelStart = 0
    sdbcEmpresa.SelLength = Len(sdbcEmpresa.Text)
    sdbcEmpresa.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcEmpresa_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarEmpresas()
    Dim oEmpresas As CEmpresas
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
    If m_bRuo Then
        oEmpresas.CargarTodasLasEmpresasDesde , , , , True, , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , , , m_bRPerfUON, lIdPerfil
    Else
        oEmpresas.CargarTodasLasEmpresasDesde , , , , True, , , , , , , , oUsuarioSummit.Pyme, m_bRPerfUON, lIdPerfil
    End If
    
    sdbcEmpresa.AddItem 0 & Chr(m_lSeparador) & vbNullString & Chr(m_lSeparador) & vbNullString & Chr(m_lSeparador) & vbNullString
    Dim oEmpresa As CEmpresa
    For Each oEmpresa In oEmpresas
        sdbcEmpresa.AddItem oEmpresa.Id & Chr(m_lSeparador) & oEmpresa.NIF & Chr(m_lSeparador) & oEmpresa.Den & Chr(m_lSeparador) & oEmpresa.NIF & " - " & oEmpresa.Den
    Next
    
    Set oEmpresas = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarEmpresas", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbcEmpresa_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         sdbcEmpresa.Value = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcEmpresa_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcPeticionario_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcPeticionario.DroppedDown Then
        sdbcPeticionario = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcPeticionario_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPeticionario_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcPeticionario.Value = "..." Or sdbcPeticionario = "" Then
        sdbcPeticionario.Text = ""
        Exit Sub
    End If
        
    m_bRespetarCombo = True
    sdbcPeticionario.Text = sdbcPeticionario.Columns(1).Text
    m_bRespetarCombo = False
    If m_bCargandoFiltro = True Then g_oConfVisorSol.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcPeticionario_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPeticionario_DropDown()
    'Cargar la combo con todos los usuarios con acceso al EP
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    sdbcPeticionario.RemoveAll
    CargarPersonas
    sdbcPeticionario.SelStart = 0
    sdbcPeticionario.SelLength = Len(sdbcPeticionario.Text)
    sdbcPeticionario.Refresh
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcPeticionario_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPeticionario_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPeticionario.DataFieldList = "Column 1"
    sdbcPeticionario.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcPeticionario_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcPeticionario_PositionList(ByVal Text As String)
PositionList sdbcPeticionario, Text
End Sub

''' <summary>
''' Posicionarse en el combo seg�n la selecci�n
''' </summary>
''' <param name="Id">Valor seleccionado</param>
''' <returns>Posici�n del valor seleccionado</returns>
''' <remarks>Llamada desde: ComprobarYAplicarFiltros; Tiempo m�ximo: 0,1 sg</remarks>
Private Function sdbcArticulo_PositionListId(ByVal Id As String) As String
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcArticulo.MoveFirst
    
    If Id <> "" Then
        For i = 0 To sdbcArticulo.Rows - 1
            bm = sdbcArticulo.GetBookmark(i)
            If Id = sdbcArticulo.Columns(0).CellText(bm) Then
                sdbcArticulo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    sdbcArticulo_PositionListId = sdbcArticulo.Columns(0).CellText(bm)
End Function

''' <summary>
''' Posicionarse en el combo seg�n la selecci�n
''' </summary>
''' <param name="Id">Valor seleccionado</param>
''' <returns>Posici�n del valor seleccionado</returns>
''' <remarks>Llamada desde: ComprobarYAplicarFiltros; Tiempo m�ximo: 0,1 sg</remarks>
Private Function sdbcEmpresa_PositionListId(ByVal Id As String) As String
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcArticulo.MoveFirst
    
    If Id <> "" Then
        For i = 0 To sdbcEmpresa.Rows - 1
            bm = sdbcEmpresa.GetBookmark(i)
            If Id = sdbcEmpresa.Columns(0).CellText(bm) Then
                sdbcEmpresa.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    sdbcEmpresa_PositionListId = sdbcEmpresa.Columns(1).CellText(bm) & " - " & sdbcEmpresa.Columns(2).CellText(bm)
End Function

''' <summary>
''' Posicionarse en el combo seg�n la selecci�n
''' </summary>
''' <param name="Id">Valor seleccionado</param>
''' <returns>Posici�n del valor seleccionado</returns>
''' <remarks>Llamada desde: ComprobarYAplicarFiltros; Tiempo m�ximo: 0,1 sg</remarks>
Private Function sdbcDepartamento_PositionListId(ByVal Id As String) As String
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcDepartamento.MoveFirst
    
    If Id <> "" Then
        For i = 0 To sdbcDepartamento.Rows - 1
            bm = sdbcDepartamento.GetBookmark(i)
            If Id = sdbcDepartamento.Columns(0).CellText(bm) Then
                sdbcDepartamento.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    sdbcDepartamento_PositionListId = sdbcDepartamento.Columns(1).CellText(bm)
End Function

''' <summary>
''' Posicionarse en el combo seg�n la selecci�n
''' </summary>
''' <param name="Id">Valor seleccionado</param>
''' <returns>Posici�n del valor seleccionado</returns>
''' <remarks>Llamada desde: ComprobarYAplicarFiltros; Tiempo m�ximo: 0,1 sg</remarks>
Private Function sdbcComprador_PositionListId(ByVal Id As String) As String
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcComprador.MoveFirst
    
    If Id <> "" Then
        For i = 0 To sdbcComprador.Rows - 1
            bm = sdbcComprador.GetBookmark(i)
            If Id = sdbcComprador.Columns(0).CellText(bm) Then
                sdbcComprador.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    sdbcComprador_PositionListId = sdbcComprador.Columns(1).CellText(bm)
End Function

''' <summary>
''' Evento que salta al producirse un cambio en el combo. Limpia la denominaci�n.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_Change()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboProve Then
    
        bRespetarComboProve = True
        sdbcDenProveedor.Text = ""
        bRespetarComboProve = False
        
        bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcProveedor_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Evento que salta al producirse un click en el combo. Limpia si no esta desplagado
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcProveedor.DroppedDown Then
        sdbcProveedor = ""
        sdbcDenProveedor = ""
     End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcProveedor_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Evento que salta al cerrarse el combo. Establece el codigo y denominaci�n elegidos.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveedor.Value = "..." Then
        sdbcProveedor.Text = ""
        Exit Sub
    End If
    
    If sdbcProveedor.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcDenProveedor.Text = sdbcProveedor.Columns(1).Text
    sdbcDenProveedor.Columns(0).Value = sdbcProveedor.Columns(1).Text
    sdbcProveedor.Text = sdbcProveedor.Columns(0).Text
    bRespetarComboProve = False
    
    DoEvents
        
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcProveedor_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Evento que salta al desplegarse el combo. Carga los codigo.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_DropDown()
    Dim i As Integer
    Dim oProves As CProveedores
    Dim Codigos As TipoDatosCombo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = oFSGSRaiz.generar_CProveedores

    Screen.MousePointer = vbHourglass
    
    sdbcProveedor.RemoveAll
    
    If bCargarComboDesde Then
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProveedor.Text), , , , , m_bRestrMatComp, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , , , , , m_bRestrMatComp, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    End If

    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveedor.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next

    If Not oProves.EOF Then
        sdbcProveedor.AddItem "..."
    End If

    sdbcProveedor.SelStart = 0
    sdbcProveedor.SelLength = Len(sdbcProveedor.Text)
    sdbcProveedor.Refresh
    
    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
    
    Set oProves = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcProveedor_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Evento que salta al inicializarse el combo.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveedor.DataFieldList = "Column 0"
    sdbcProveedor.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcProveedor_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Evento que salta al seleccionar en el combo. Posicionarse en el combo segun la seleccion
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_PositionList(ByVal Text As String)
PositionList sdbcProveedor, Text
End Sub

''' <summary>
''' Evento que salta al escribir y salir del combo. Validar q es un c�digo valido.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oProves As CProveedores
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveedor.Text = "" Then Exit Sub
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    ''' Solo continuamos si existe el proveedor
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveedor.Text), , True, , , m_bRestrMatComp, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario

    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveedor.Text))
    
    If Not bExiste Then
        sdbcProveedor.Text = ""
    Else
        bRespetarComboProve = True
        sdbcDenProveedor.Text = oProves.Item(1).Den
        
        sdbcProveedor.Columns(0).Value = sdbcProveedor.Text
        sdbcProveedor.Columns(1).Value = sdbcDenProveedor.Text
        
        bRespetarComboProve = False
    End If
    bCargarComboDesde = False
    
    Set oProves = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcProveedor_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcTipoSolicit_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcTipoSolicit.DroppedDown Then
        sdbcTipoSolicit = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcTipoSolicit_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcTipoSolicit_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcTipoSolicit.Value = "..." Or sdbcTipoSolicit.Value = "" Then
        sdbcTipoSolicit.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcTipoSolicit.Text = sdbcTipoSolicit.Columns(1).Text
    m_bRespetarCombo = False
    If m_bCargandoFiltro = True Then g_oConfVisorSol.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcTipoSolicit_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcTipoSolicit_DropDown()
    'Cargar la combo con todos los usuarios con acceso al EP
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcTipoSolicit.RemoveAll
    
    CargarTiposSolicitud
   
    sdbcTipoSolicit.SelStart = 0
    sdbcTipoSolicit.SelLength = Len(sdbcTipoSolicit.Text)
    sdbcTipoSolicit.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcTipoSolicit_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarTiposSolicitud()
    Dim oSolic As CSolicitud
    Dim oSolicitudes As CSolicitudes
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    If m_bRuo Then
        oSolicitudes.CargarTodasSolicitudesDeCompras , , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRPerfUON, lIdPerfil
    Else
        oSolicitudes.CargarTodasSolicitudesDeCompras , , , , , , , m_bRPerfUON, lIdPerfil
    End If
        
    For Each oSolic In oSolicitudes
        sdbcTipoSolicit.AddItem oSolic.Codigo & Chr(m_lSeparador) & NullToStr(oSolic.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & Chr(m_lSeparador) & oSolic.Id
    Next
        
    Set oSolicitudes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarTiposSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub sdbcTipoSolicit_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcTipoSolicit.DataFieldList = "Column 1"
    sdbcTipoSolicit.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcTipoSolicit_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcTipoSolicit_PositionList(ByVal Text As String)
PositionList sdbcTipoSolicit, Text
End Sub

Private Sub sdbcTipoSolicit_PositionListId(ByVal Id As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcTipoSolicit.MoveFirst
    
    If Id <> "" Then
        For i = 0 To sdbcTipoSolicit.Rows - 1
            bm = sdbcTipoSolicit.GetBookmark(i)
            If Id = sdbcTipoSolicit.Columns(1).CellText(bm) Then
                sdbcTipoSolicit.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Function sdbcPeticionario_PositionListId(ByVal Id As String) As String
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPeticionario.MoveFirst
    
    If Id <> "" Then
        For i = 0 To sdbcPeticionario.Rows - 1
            bm = sdbcPeticionario.GetBookmark(i)
            If Id = sdbcPeticionario.Columns(0).CellText(bm) Then
                sdbcPeticionario.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    sdbcPeticionario_PositionListId = sdbcPeticionario.Columns(1).CellText(bm)
End Function

Private Sub sdbgPartidas_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgPartidas.Columns(ColIndex).Name = "VALOR" And sdbgPartidas.Columns("VALIDADO").Value = 0 Then
        sdbgPartidas.Columns("PRES5NIV1").Value = ""
        sdbgPartidas.Columns("PRES5NIV2").Value = ""
        sdbgPartidas.Columns("PRES5NIV3").Value = ""
        sdbgPartidas.Columns("PRES5NIV4").Value = ""
        sdbgPartidas.Columns("TAG").Value = ""
        sdbgPartidas.Columns("PRES5UON").Value = ""
        
        If sdbgPartidas.Columns(ColIndex).Value <> "" Then
            Dim pos As Integer
            pos = InStr(1, sdbgPartidas.Columns("VALOR").Value, " - ", vbTextCompare)
            If pos > 0 Then
                sdbgPartidas.Columns("TAG").Value = Trim(Mid(sdbgPartidas.Columns("VALOR").Value, 1, pos))
            Else
                sdbgPartidas.Columns("TAG").Value = ""
            End If
        
            Dim sValue As String
            If Len(sdbgPartidas.Columns("TAG").Value) > 0 Then
                sValue = CStr(sdbgPartidas.Columns("TAG").Value)
            Else
                sValue = sdbgPartidas.Columns(ColIndex).Value
            End If
               
            If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
            
            If sUON1 <> "" Or sUON2 <> "" Or sUON3 <> "" Then
                m_oContratosPres.CargarTodosLosContratos sdbgPartidas.Columns("COD").Value, sdbgPartidas.Columns("NIVEL").Value, IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, sUON1, sUON2, sUON3, , sValue, , , m_bRPerfUON, lIdPerfil
            Else
                If m_bRuo Then
                    m_oContratosPres.CargarTodosLosContratos sdbgPartidas.Columns("COD").Value, sdbgPartidas.Columns("NIVEL").Value, IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , sValue, , , m_bRPerfUON, lIdPerfil
                Else
                    If oUsuarioSummit.Pyme <> 0 Then
                        m_oContratosPres.CargarTodosLosContratos sdbgPartidas.Columns("COD").Value, sdbgPartidas.Columns("NIVEL").Value, IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, , , , sValue, , , m_bRPerfUON, lIdPerfil
                    Else
                        m_oContratosPres.CargarTodosLosContratos sdbgPartidas.Columns("COD").Value, sdbgPartidas.Columns("NIVEL").Value, IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, , , , , sValue, , , m_bRPerfUON, lIdPerfil
                    End If
                End If
            End If

            If m_oContratosPres.Count > 0 Then
                'Se coge la primera partida
                sdbgPartidas.Columns("VALOR").Value = m_oContratosPres.Item(1).Cod & " - " & m_oContratosPres.Item(1).Den
                sdbgPartidas.Columns("TAG").Value = m_oContratosPres.Item(1).Cod
                sdbgPartidas.Columns("PRES5UON").Value = FormatearPres5o(sdbgPartidas.Columns("COD").Value, m_oContratosPres.Item(1))
                sdbgPartidas.Columns("VALIDADO").Value = 1
                sdbgPartidas.Columns("PRES5NIV1").Value = m_oContratosPres.Item(1).Pres1
                sdbgPartidas.Columns("PRES5NIV2").Value = m_oContratosPres.Item(1).Pres2
                sdbgPartidas.Columns("PRES5NIV3").Value = m_oContratosPres.Item(1).Pres3
                sdbgPartidas.Columns("PRES5NIV4").Value = m_oContratosPres.Item(1).Pres4
            Else
                sdbgPartidas.Columns("VALOR").Value = sValue
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgPartidas_BeforeColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbgPartidas_BtnClick()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSelCContables.g_sOrigen = "frmSolicitudes"
    frmSelCContables.g_sPres0 = sdbgPartidas.Columns("COD").Value
    frmSelCContables.g_iNivel = sdbgPartidas.Columns("NIVEL").Value
    frmSelCContables.Show vbModal
    If Not g_oPartida Is Nothing Then
        If Len(g_oPartida.Cod) > 0 Then
                       
            sdbgPartidas.Columns("VALOR").Value = g_oPartida.Cod & " - " & g_oPartida.Den
            sdbgPartidas.Columns("TAG").Value = g_oPartida.Cod
            sdbgPartidas.Columns("PRES5UON").Value = FormatearPres5o(sdbgPartidas.Columns("COD").Value, g_oPartida)
            sdbgPartidas.Columns("VALIDADO").Value = 2
            sdbgPartidas.Columns("PRES5NIV1").Value = g_oPartida.Pres1
            sdbgPartidas.Columns("PRES5NIV2").Value = g_oPartida.Pres2
            sdbgPartidas.Columns("PRES5NIV3").Value = g_oPartida.Pres3
            sdbgPartidas.Columns("PRES5NIV4").Value = g_oPartida.Pres4
            
            sdbgPartidas.Update
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgPartidas_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgPartidas_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgPartidas.Columns("VALIDADO").Value = 0
    sdbgPartidas.Columns("VALOR").CellStyleSet ""
    Set g_oPartida = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgPartidas_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbgPartidas_KeyPress(KeyAscii As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        If sdbgPartidas.Row + 1 = sdbgPartidas.Rows Then
            sdbgPartidas.MoveFirst
        Else
            sdbgPartidas.MoveNext
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgPartidas_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgPartidas_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgPartidas.Columns("VALOR").CellValue(Bookmark) = "" Then
        sdbgPartidas.Columns("VALOR").CellStyleSet ""
    Else
        If sdbgPartidas.Columns("VALIDADO").CellValue(Bookmark) = 1 Then
            sdbgPartidas.Columns("VALOR").CellStyleSet "Verde"
        ElseIf sdbgPartidas.Columns("VALIDADO").CellValue(Bookmark) = 0 Then
            sdbgPartidas.Columns("VALOR").CellStyleSet "Rojo"
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgPartidas_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgSolicitudes_BtnClick()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSolicitudes.col = -1 Then Exit Sub
    
    Select Case sdbgSolicitudes.Columns(sdbgSolicitudes.col).Name
        Case "TIPO"
            If Not m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))).Solicitud Is Nothing Then
                Set frmDetTipoSolicitud.g_oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))).Solicitud
                frmDetTipoSolicitud.Show vbModal
            End If
            
        Case "PET"
            frmDetallePersona.g_sPersona = sdbgSolicitudes.Columns("COD_PER").Value
            frmDetallePersona.Show vbModal
            
        Case "COMP"
            If sdbgSolicitudes.Columns("DESTINATARIO_PROV").Value <> "" Then
                MostrarDetalleProveedor (sdbgSolicitudes.Columns("DESTINATARIO_PROV").Value)
            ElseIf sdbgSolicitudes.Columns("COD_COMP").Value <> "" Then
                frmDetallePersona.g_sPersona = sdbgSolicitudes.Columns("COD_COMP").Value
                frmDetallePersona.Show vbModal
            End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgSolicitudes_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgSolicitudes_DblClick()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSolicitudes.Rows = 0 Then Exit Sub
    
    DetalleSolicitud
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgSolicitudes_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


''' <summary>
''' Ordenar el grid segun la columna
''' </summary>
''' <param name="ColIndex">Columna por la que se va a ordenar</param>
''' <remarks>Llamada desde: Al hacer click sobre la cabecera; Tiempo m�ximo: 0,1 sg</remarks>
Private Sub sdbgSolicitudes_HeadClick(ByVal ColIndex As Integer)
    Dim sHeadCaption As String
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
   
    sHeadCaption = sdbgSolicitudes.Columns(ColIndex).caption
    sdbgSolicitudes.Columns(ColIndex).caption = sOrdenando
    
    m_oInstancias.PaginaAMostrar = 1
    m_oInstancias.PaginaActual = 0
    
    If (gParametrosInstalacion.giOrdenVisorSol = ColIndex) And (ColIndex < 4) Then
        gParametrosInstalacion.gbOrdenVisorSolAscendente = Not gParametrosInstalacion.gbOrdenVisorSolAscendente
    Else
        gParametrosInstalacion.gbOrdenVisorSolAscendente = True
    End If
    
    CargarGridSolicitudes ColIndex
    
    sdbgSolicitudes.Columns(ColIndex).caption = sHeadCaption
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgSolicitudes_HeadClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Muestra el tooltip correspondiente de haberlo
''' </summary>
''' <param name="Button">Boton izquierdo o derecho del raton</param>
''' <param name="Shift">Si esta la tecla shift presionada</param>
''' <param name="X">Posicion x</param>
''' <param name="Y">Posicion y</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgSolicitudes_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim VisibleRow As Integer
    Dim VisibleCol As Integer
    Dim Bookmark As Variant
    Dim ToolTip As String
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSolicitudes.WhereIs(X, Y) = ssWhereIsData Then
        'Get the Visible Row Number
        VisibleRow = sdbgSolicitudes.RowContaining(Y)
        VisibleCol = sdbgSolicitudes.ColContaining(X)
        
        'Get the bookmark of the row
        Bookmark = sdbgSolicitudes.RowBookmark(VisibleRow)
        
        ToolTip = ""
        
        'Set the tooltip of the Grid to the first column of the row the mouse is currently over.
        If sdbgSolicitudes.Columns(VisibleCol).Name = "ALERTA_PROCESO" Then
            If sdbgSolicitudes.Columns("SIN_PROCESO").CellValue(Bookmark) Then
                ToolTip = m_sMensajeSinProceso
            End If
        End If
        
        If sdbgSolicitudes.Columns(VisibleCol).Name = "ALERTA_PEDIDO" Then
            If sdbgSolicitudes.Columns("SIN_PEDIDO").CellValue(Bookmark) Then
                ToolTip = m_sMensajeSinPedido
            End If
        End If
        
        If sdbgSolicitudes.Columns(VisibleCol).Name = "ALERTA_INTEGRACION" Then
            If sdbgSolicitudes.Columns("INTEGRACION").CellValue(Bookmark) <> "" Then
                ToolTip = m_sMensajeErrorIntegracion & " " & sdbgSolicitudes.Columns("INTEGRACION").CellValue(Bookmark)
            End If
        End If
        
        If sdbgSolicitudes.Columns(VisibleCol).Name = "ALERTA_MONITORIZACION" Then
            If sdbgSolicitudes.Columns("MONITORIZ").CellValue(Bookmark) And sdbgSolicitudes.Columns("TEXTOMONITORIZ").CellValue(Bookmark) <> "" Then
                ToolTip = sdbgSolicitudes.Columns("TEXTOMONITORIZ").CellValue(Bookmark)
            End If
        End If
    End If
    
    sdbgSolicitudes.ToolTipText = ToolTip
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgSolicitudes_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' evento que visualiza o no las opciones del men� de solicitudes.
''' </summary>
''' <param name="Button">Boton izquierdo o derecho del raton</param>
''' <param name="Shift">Si esta la tecla shift presionada</param>
''' <param name="X">Posicion x</param>
''' <param name="Y">Posicion y</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgSolicitudes_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSolicitudes.Rows = 0 Then Exit Sub
    
    If Button = 2 Then
        mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
        mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
        DoEvents
          
        'Habilita los men�s seg�n el estado de la solicitud seleccionada
        Select Case sdbgSolicitudes.Columns("ID_ESTADO").CellValue(sdbgSolicitudes.Bookmark)
            Case EstadoSolicitud.Anulada
                MDI.mnuPopUpSolicitudes.Item(8).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(4).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(5).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(6).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(2).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(3).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(7).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(9).Enabled = True
                
            Case EstadoSolicitud.Aprobada
                MDI.mnuPopUpSolicitudes.Item(8).Enabled = True
                MDI.mnuPopUpSolicitudes.Item(4).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(5).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(6).Enabled = True
                MDI.mnuPopUpSolicitudes.Item(2).Enabled = True
                MDI.mnuPopUpSolicitudes.Item(3).Enabled = True
                MDI.mnuPopUpSolicitudes.Item(7).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(9).Enabled = True
                
            Case EstadoSolicitud.Cerrada
                MDI.mnuPopUpSolicitudes.Item(8).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(4).Enabled = True
                MDI.mnuPopUpSolicitudes.Item(5).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(6).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(2).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(3).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(7).Enabled = True
                MDI.mnuPopUpSolicitudes.Item(9).Enabled = True
                
            Case EstadoSolicitud.Pendiente
                MDI.mnuPopUpSolicitudes.Item(8).Enabled = True  'anular
                MDI.mnuPopUpSolicitudes.Item(4).Enabled = True  'aprobar
                MDI.mnuPopUpSolicitudes.Item(5).Enabled = True  'rechazar
                MDI.mnuPopUpSolicitudes.Item(6).Enabled = False 'cerrada
                MDI.mnuPopUpSolicitudes.Item(2).Enabled = True  'modificar
                MDI.mnuPopUpSolicitudes.Item(3).Enabled = True  'asignar comprador
                MDI.mnuPopUpSolicitudes.Item(7).Enabled = False 'reabrir
                MDI.mnuPopUpSolicitudes.Item(9).Enabled = True 'eliminar
                
            Case EstadoSolicitud.Rechazada
                MDI.mnuPopUpSolicitudes.Item(8).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(4).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(5).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(6).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(2).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(3).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(7).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(9).Enabled = True
                
            Case Else   'En Curso de Aprobaci�n
                If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                    MDI.mnuPopUpSolicitudes.Item(8).Enabled = True  'anular
                    MDI.mnuPopUpSolicitudes.Item(9).Enabled = True  'eliminar
                Else
                    MDI.mnuPopUpSolicitudes.Item(8).Enabled = False 'anular
                    MDI.mnuPopUpSolicitudes.Item(9).Enabled = False 'eliminar
                End If
                MDI.mnuPopUpSolicitudes.Item(4).Enabled = False  'aprobar
                MDI.mnuPopUpSolicitudes.Item(5).Enabled = False  'rechazar
                MDI.mnuPopUpSolicitudes.Item(6).Enabled = False 'cerrada
                MDI.mnuPopUpSolicitudes.Item(2).Enabled = False  'modificar
                MDI.mnuPopUpSolicitudes.Item(3).Enabled = False  'asignar comprador
                MDI.mnuPopUpSolicitudes.Item(7).Enabled = False 'reabrir
        End Select
        
        If sdbgSolicitudes.Columns("MONITORIZ").CellValue(sdbgSolicitudes.Bookmark) = False Then
            MDI.mnuPopUpSolicitudes.Item(10).Visible = True 'activar monitorizaci�n
            MDI.mnuPopUpSolicitudes.Item(11).Visible = False 'Deactivar monitorizaci�n
        Else
            MDI.mnuPopUpSolicitudes.Item(10).Visible = False 'activar monitorizaci�n
            MDI.mnuPopUpSolicitudes.Item(11).Visible = True 'Deactivar monitorizaci�n
        End If
        
        PopupMenu MDI.POPUPSolicitudes
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgSolicitudes_MouseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Habilita los men�s seg�n el estado de la solicitud seleccionada
''' Impide q el foco llege a las columnas de estado pq son graficas y la imagen se mueve con foco a pesar de locked a true
''' </summary>
''' <param name="LastRow">Ultima fila</param>
''' <param name="LastCol">Ultima columna</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgSolicitudes_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    HabilitarBotones
    If Me.sdbgSolicitudes.col = -1 Then
    ElseIf Me.sdbgSolicitudes.col < 4 Then
        Me.sdbgSolicitudes.col = 4
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgSolicitudes_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Establece los estilos de las celdas de la fila cargada
''' </summary>
''' <param name="Bookmark">Bookmark de la fila cargada</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgSolicitudes_RowLoaded(ByVal Bookmark As Variant)
    Dim oStyleSetSource As StyleSet
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case sdbgSolicitudes.Columns("ID_ESTADO").CellValue(Bookmark)
        Case EstadoSolicitud.Pendiente
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Pendiente"
            Next i
            Set oStyleSetSource = sdbgSolicitudes.StyleSets("Pendiente")
            
        Case EstadoSolicitud.Aprobada
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Aprobada"
            Next i
            Set oStyleSetSource = sdbgSolicitudes.StyleSets("Aprobada")
            
        Case EstadoSolicitud.Rechazada
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Rechazada"
            Next i
            Set oStyleSetSource = sdbgSolicitudes.StyleSets("Rechazada")
    
        Case EstadoSolicitud.Anulada
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Anulada"
            Next i
            Set oStyleSetSource = sdbgSolicitudes.StyleSets("Anulada")
            
        Case EstadoSolicitud.Cerrada
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Cerrada"
            Next i
            Set oStyleSetSource = sdbgSolicitudes.StyleSets("Cerrada")
            
        Case Else   'En curso
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Curso"
            Next i
            Set oStyleSetSource = sdbgSolicitudes.StyleSets("Curso")
            
    End Select
    
    With oStyleSetSource
        'si para la fila est� activada la monitorizaci�n, o las alertas de proceso/pedido/integraci�n:
        If sdbgSolicitudes.Columns("SIN_PROCESO").CellValue(Bookmark) Then
            sdbgSolicitudes.Columns("ALERTA_PROCESO").CellStyleSet "alertaProceso" & .Name
        End If
        If sdbgSolicitudes.Columns("SIN_PEDIDO").CellValue(Bookmark) Then
            sdbgSolicitudes.Columns("ALERTA_PEDIDO").CellStyleSet "alertaPedido" & .Name
        End If
        If sdbgSolicitudes.Columns("INTEGRACION").CellValue(Bookmark) <> "" Then
            sdbgSolicitudes.Columns("ALERTA_INTEGRACION").CellStyleSet "alertaIntegracion" & .Name
        End If
        If sdbgSolicitudes.Columns("MONITORIZ").CellValue(Bookmark) Then
            sdbgSolicitudes.Columns("ALERTA_MONITORIZACION").CellStyleSet "alertaMonitorizacion"
        End If
    End With
    
    If gParametrosInstalacion.giOrdenVisorSol = 0 Then
        sdbgSolicitudes.Columns("ALERTA_INTEGRACION").HeadStyleSet = IIf(gParametrosInstalacion.gbOrdenVisorSolAscendente, "ASC", "")
        sdbgSolicitudes.Columns("ALERTA_PEDIDO").HeadStyleSet = ""
        sdbgSolicitudes.Columns("ALERTA_PROCESO").HeadStyleSet = ""
    ElseIf gParametrosInstalacion.giOrdenVisorSol = 1 Then
        sdbgSolicitudes.Columns("ALERTA_INTEGRACION").HeadStyleSet = ""
        sdbgSolicitudes.Columns("ALERTA_PEDIDO").HeadStyleSet = ""
        sdbgSolicitudes.Columns("ALERTA_PROCESO").HeadStyleSet = IIf(gParametrosInstalacion.gbOrdenVisorSolAscendente, "ASC", "")
    ElseIf gParametrosInstalacion.giOrdenVisorSol = 2 Then
        sdbgSolicitudes.Columns("ALERTA_INTEGRACION").HeadStyleSet = ""
        sdbgSolicitudes.Columns("ALERTA_PEDIDO").HeadStyleSet = IIf(gParametrosInstalacion.gbOrdenVisorSolAscendente, "ASC", "")
        sdbgSolicitudes.Columns("ALERTA_PROCESO").HeadStyleSet = ""
    Else
        sdbgSolicitudes.Columns("ALERTA_INTEGRACION").HeadStyleSet = ""
        sdbgSolicitudes.Columns("ALERTA_PEDIDO").HeadStyleSet = ""
        sdbgSolicitudes.Columns("ALERTA_PROCESO").HeadStyleSet = ""
    End If

    Set oStyleSetSource = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbgSolicitudes_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga la grid de solicitudes con las solicitudes obtenidas en la b�squeda
''' </summary>
''' <param name="iCol">N� de la columna por la que se ordena</param>
''' <remarks>Llamada desde: cmdBuscar_Click, cmdRestaurar_Click, Form_Load, sdbgSolicitudes_HeadClick; Tiempo m�ximo: 5 sg (depende de la b�squeda);</remarks>
Private Sub CargarGridSolicitudes(iCol As Integer)
    Dim oInstancia As CInstancia
    Dim sPet As String
    Dim vEstado() As Variant
    Dim sestado As String
    Dim i As Integer
    Dim iOrden As TipoOrdenacionSolicitudes
    Dim vFecNec As Variant
    Dim lTipo As Long
    Dim sAsignadoA As String
    Dim sCodAsignadoA As String
    Dim sTrasladadaProv As String
    Dim sReemisiones As String

    Dim sMateriales As String
    Dim sCodArticulo As String

    Dim lEmpresa As Long
    Dim sDepartamento As String
    Dim sproveedor As String
    Dim sComprador As String
    Dim sPartidas As String
    Dim lIdPerfil As Long
    
    Dim bMostrarMensajeSinpedido As Boolean
    Dim bMostrarMensajeSinproceso As Boolean
    Dim bMostrarMensajeSinintegracion As Boolean
    Dim bMostrarMonitorizacionActivada As Boolean
    Dim strNumsolicitud As String
    Dim bcoincidnumsolerp As Boolean
    
    'Hace las comprobaciones necesarias: -----------------------
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    If Trim(txtId.Text) <> "" Then
        If Not IsNumeric(txtId.Text) Then
            oMensajes.NoValido sId
            Exit Sub
        End If
    End If
    
    If Trim(txtFecDesde.Text) <> "" Then
        If Not IsDate(txtFecDesde.Text) Then
            oMensajes.NoValido sFecDesde
            If Me.Visible Then txtFecDesde.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(txtFecHasta.Text) <> "" Then
        If Not IsDate(txtFecHasta.Text) Then
            oMensajes.NoValido sFecHasta
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(txtImporteDesde.Text) <> "" Then
        If Not IsNumeric(txtImporteDesde.Text) Then
            oMensajes.NoValido sImporteDesde
            If Me.Visible Then txtImporteDesde.SetFocus
            Exit Sub
        Else
            FormatearImporteDesde
        End If
    End If
    
    If Trim(txtImporteHasta.Text) <> "" Then
        If Not IsNumeric(txtImporteHasta.Text) Then
            oMensajes.NoValido sImporteHasta
            If Me.Visible Then txtImporteHasta.SetFocus
            Exit Sub
        Else
            FormatearImporteHasta
        End If
    End If
    'fin comprobaciones -----------------------
    
    Screen.MousePointer = vbHourglass
    
    LockWindowUpdate Me.hWnd
    
    sdbgSolicitudes.RemoveAll
    
    sdbgSolicitudes.Columns("ALERTA_PROCESO").Visible = SQLBinaryToBoolean(chkConfAlertaSinProceso.Value)
    sdbgSolicitudes.Columns("ALERTA_PEDIDO").Visible = SQLBinaryToBoolean(chkConfAlertaSinPedido.Value)
    sdbgSolicitudes.Columns("ALERTA_INTEGRACION").Visible = SQLBinaryToBoolean(chkConfAlertaErrIntegracion.Value)
    
    DoEvents
    frmADJCargar.Show
    frmADJCargar.Refresh
          
    If sdbcTipoSolicit.Text <> "" Then
        lTipo = sdbcTipoSolicit.Columns("ID").Value
    End If
    
    If sdbcPeticionario.Text <> "" Then
        sPet = sdbcPeticionario.Columns(0).Value
    End If
    
    'Obtiene los estados
    i = 1
    If Trim(txtId.Text) = "" Then
        'S�lo busca por los estados cuando no se ha introducido el identificador
        If chkEstado(0).Value = vbChecked Then  'Pendiente:cuando se ha aprobado desde el WS (si hab�a workflow) o si no hab�a cuando se ha enviado
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Pendiente
            i = i + 1
        End If
        If chkEstado(1).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Aprobada
            i = i + 1
        End If
        If chkEstado(2).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Rechazada
            i = i + 1
        End If
        If chkEstado(3).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Anulada
            i = i + 1
        End If
        If chkEstado(4).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Cerrada
            i = i + 1
        End If
        If chkEstado(5).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.GenPendiente
            i = i + 1
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.GenRechazada
        End If
        If i = 1 Then 'Esto es para cuando no entra en ningun if para inicializar que sino casca
            ReDim Preserve vEstado(i)
            vEstado(i) = ""
        End If
    Else
        ReDim Preserve vEstado(i)
        vEstado(i) = ""
    End If
    
    If m_sGMN1Cod <> "" Then
        sMateriales = Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(m_sGMN1Cod)) & m_sGMN1Cod
        If m_sGMN2Cod <> "" Then
            sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(m_sGMN2Cod)) & m_sGMN2Cod
            If m_sGMN3Cod <> "" Then
                sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(m_sGMN3Cod)) & m_sGMN3Cod
                If m_sGMN4Cod <> "" Then
                    sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(m_sGMN4Cod)) & m_sGMN4Cod
                End If
            End If
        End If
    End If
            
    If sdbcArticulo.Text <> "" Then sCodArticulo = sdbcArticulo.Columns(0).Value
    If sdbcEmpresa.Text <> "" Then lEmpresa = sdbcEmpresa.Columns(0).Value
    If sdbcDepartamento.Text <> "" Then sDepartamento = sdbcDepartamento.Columns(0).Value
    If sdbcProveedor.Text <> "" Then sproveedor = sdbcProveedor.Columns(0).Value
    If sdbcComprador.Text <> "" Then sComprador = sdbcComprador.Columns(0).Value
        
    Select Case sdbgSolicitudes.Columns(iCol).Name
        Case "TIPO"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorTipo
        Case "ALTA"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorfecalta
        Case "NECESIDAD"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorFecNec
        Case "ID2"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorId
        Case "DESCR"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorDescr
        Case "IMPORTE"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorImporte
        Case "PET"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorPet
        Case "UON"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorUO
        Case "ESTADO"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorEstado
        Case "COMP"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorComp
        Case "DEPT"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorDepartamento
        Case "ALERTA_INTEGRACION"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorAlertaIntegracion
        Case "ALERTA_PROCESO"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorAlertaProceso
        Case "ALERTA_PEDIDO"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorAlertaPedido
        Case "NUM_SOL_ERP"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicNumSolErp
    End Select
    
    gParametrosInstalacion.giOrdenVisorSol = iCol
    
    If Not g_oParametrosSM Is Nothing Then
        If g_oParametrosSM.Count > 1 Then
            Dim vbm As Variant
            Dim Row As Integer
            Row = sdbgPartidas.Rows - 1
            While Row >= 0
                vbm = sdbgPartidas.AddItemBookmark(Row)
                If Len(sdbgPartidas.Columns("VALOR").CellValue(vbm)) > 0 Then
                    If Len(sPartidas) = 0 Then
                        sPartidas = sdbgPartidas.Columns("PRES5UON").CellValue(vbm)
                    Else
                        sPartidas = sPartidas & ";" & sdbgPartidas.Columns("PRES5UON").CellValue(vbm)
                    End If
                End If
                Row = Row - 1
            Wend
        Else
            sPartidas = m_asPres5UON(0)
        End If
    End If
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    'Num solicitud ERP
    bcoincidnumsolerp = True
    strNumsolicitud = Me.txtNumSolicErp.Text
    If InStr(1, Me.txtNumSolicErp.Text, "*") >= 0 Then
        strNumsolicitud = Replace(Me.txtNumSolicErp.Text, "*", "%")
        bcoincidnumsolerp = False
    End If
    
    If m_bRuo Then
        m_oInstancias.BuscarSolicitudes iOrden, lTipo, Trim(txtId.Text), txtDescr.Text, sPet, vEstado, txtFecDesde.Text, txtFecHasta.Text, _
        m_bRAsig, m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, basOptimizacion.gUON1Usuario, _
        basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRDep, basOptimizacion.gCodDepUsuario, basOptimizacion.gCodPersonaUsuario, , _
        sMateriales, sCodArticulo, txtDenArticulo.Text, NullToDbl0(StrToDblOrNull(txtImporteDesde.Text)), NullToDbl0(StrToDblOrNull(txtImporteHasta.Text)), _
        sUON_CC, lEmpresa, sDepartamento, sComprador, sPartidas, SQLBinaryToBoolean(chkConfAlertaSinProceso.Value), _
        SQLBinaryToBoolean(chkConfAlertaSinPedido.Value), SQLBinaryToBoolean(chkConfAlertaErrIntegracion.Value), oUsuarioSummit.Cod, m_bRPerfUON, lIdPerfil, sproveedor, _
        gParametrosInstalacion.gbOrdenVisorSolAscendente, sUON1, sUON2, sUON3, , strNumsolicitud, bcoincidnumsolerp
    Else
        m_oInstancias.BuscarSolicitudes iOrden, lTipo, Trim(txtId.Text), txtDescr.Text, sPet, vEstado, txtFecDesde.Text, txtFecHasta.Text, _
        m_bRAsig, m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, , , , m_bRDep, _
        basOptimizacion.gCodDepUsuario, basOptimizacion.gCodPersonaUsuario, , sMateriales, sCodArticulo, txtDenArticulo.Text, _
        NullToDbl0(StrToDblOrNull(txtImporteDesde.Text)), NullToDbl0(StrToDblOrNull(txtImporteHasta.Text)), sUON_CC, lEmpresa, sDepartamento, sComprador, _
        sPartidas, SQLBinaryToBoolean(chkConfAlertaSinProceso.Value), SQLBinaryToBoolean(chkConfAlertaSinPedido.Value), _
        SQLBinaryToBoolean(chkConfAlertaErrIntegracion.Value), oUsuarioSummit.Cod, m_bRPerfUON, lIdPerfil, sproveedor, _
        gParametrosInstalacion.gbOrdenVisorSolAscendente, sUON1, sUON2, sUON3, , strNumsolicitud, bcoincidnumsolerp
    End If
    
    bMostrarMensajeSinpedido = False
    bMostrarMensajeSinproceso = False
    bMostrarMensajeSinintegracion = False
    bMostrarMonitorizacionActivada = False

    For Each oInstancia In m_oInstancias
        Select Case oInstancia.Estado
            Case EstadoSolicitud.Anulada
                sestado = m_sAnulada
            Case EstadoSolicitud.Aprobada
                sestado = m_sAprobada
            Case EstadoSolicitud.Cerrada
                sestado = m_sCerrada
            Case EstadoSolicitud.Rechazada
                sestado = m_sRechazada
            Case EstadoSolicitud.Pendiente
                sestado = m_sPendiente
            Case EstadoSolicitud.GenRechazada
                sestado = m_sGenRechazada
            Case Else  'En Curso de Aprobaci�n
                sestado = m_sEnCurso
        End Select
    
        If IsNull(oInstancia.FecNecesidad) Then
            vFecNec = ""
        Else
            vFecNec = Format(oInstancia.FecNecesidad, "Short date")
        End If
        
        sAsignadoA = ""
        sCodAsignadoA = ""
        sTrasladadaProv = ""
        If Not oInstancia.comprador Is Nothing Then
            If oInstancia.comprador.Cod <> "" Then
                sAsignadoA = oInstancia.comprador.Cod & " - " & NullToStr(oInstancia.comprador.nombre) & " " & NullToStr(oInstancia.comprador.Apel)
                sCodAsignadoA = oInstancia.comprador.Cod
            End If
        End If
        If sAsignadoA = "" Then
            If oInstancia.Estado < EstadoSolicitud.Pendiente Then  'Si est� dentro del workflow
                If oInstancia.Trasladada = True Then  'Si est� trasladada muestra el proveedor o usuario al que se le ha trasladado
                    If Not oInstancia.Destinatario Is Nothing Then
                        sAsignadoA = oInstancia.Destinatario.Cod & " - " & NullToStr(oInstancia.Destinatario.nombre) & " " & NullToStr(oInstancia.Destinatario.Apellidos)
                        sCodAsignadoA = oInstancia.Destinatario.Cod
                    ElseIf Not oInstancia.DestinatarioProv Is Nothing Then
                        sAsignadoA = oInstancia.DestinatarioProv.Cod & " - " & NullToStr(oInstancia.DestinatarioProv.Den)
                        sCodAsignadoA = oInstancia.DestinatarioProv.Cod
                        sTrasladadaProv = oInstancia.DestinatarioProv.Cod
                    End If
                ElseIf Not oInstancia.Aprobador Is Nothing Then  'muestra el aprobador actual
                    sAsignadoA = oInstancia.Aprobador.Cod & " - " & NullToStr(oInstancia.Aprobador.nombre) & " " & NullToStr(oInstancia.Aprobador.Apellidos)
                    sCodAsignadoA = oInstancia.Aprobador.Cod
                End If
            ElseIf Not oInstancia.Solicitud.Gestor Is Nothing Then
                sAsignadoA = oInstancia.Solicitud.Gestor.Cod & " - " & NullToStr(oInstancia.Solicitud.Gestor.nombre) & " " & NullToStr(oInstancia.Solicitud.Gestor.Apellidos)
                sCodAsignadoA = oInstancia.Solicitud.Gestor.Cod
            End If
        End If
        
        sReemisiones = ""
        If oInstancia.Num_Reemisiones > 0 Then sReemisiones = "  (" & (oInstancia.Num_Reemisiones) & ")"
                
        If bMostrarMensajeSinpedido = False Then
            If oInstancia.MostrarMensajeSinpedido = True Then bMostrarMensajeSinpedido = True
        End If
        If bMostrarMensajeSinproceso = False Then
            If oInstancia.MostrarMensajeSinProceso = True Then bMostrarMensajeSinproceso = True
        End If
        If bMostrarMensajeSinintegracion = False Then
            If oInstancia.MensajeIntegracion <> "" Then bMostrarMensajeSinintegracion = True
        End If
        If bMostrarMonitorizacionActivada = False Then
            If oInstancia.MonitorizacionActivada = True Then bMostrarMonitorizacionActivada = True
        End If
        
        sdbgSolicitudes.AddItem _
        "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & _
        NullToStr(oInstancia.Solicitud.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & Chr(m_lSeparador) & _
        Format(oInstancia.FecAlta, "Short date") & Chr(m_lSeparador) & _
        vFecNec & Chr(m_lSeparador) & _
        oInstancia.Id & Chr(m_lSeparador) & _
        oInstancia.Id & sReemisiones & Chr(m_lSeparador) & _
        oInstancia.NumSolicitudErp & Chr(m_lSeparador) & _
        oInstancia.DescrBreve & Chr(m_lSeparador) & _
        Format(oInstancia.importe, "#,##0.##") & " " & oInstancia.Moneda & Chr(m_lSeparador) & _
        oInstancia.Peticionario.Cod & " - " & NullToStr(oInstancia.Peticionario.nombre) & " " & NullToStr(oInstancia.Peticionario.Apellidos) & Chr(m_lSeparador) & _
        sestado & Chr(m_lSeparador) & _
        oInstancia.UON_DEN & Chr(m_lSeparador) & _
        oInstancia.Departamento & Chr(m_lSeparador) & _
        sAsignadoA & Chr(m_lSeparador) & _
        oInstancia.Estado & Chr(m_lSeparador) & _
        oInstancia.Peticionario.Cod & Chr(m_lSeparador) & _
        sCodAsignadoA & Chr(m_lSeparador) & _
        sTrasladadaProv & Chr(m_lSeparador) & _
        IIf(oInstancia.MonitorizacionActivada, 1, 0) & Chr(m_lSeparador) & _
        IIf(oInstancia.MostrarMensajeSinProceso, 1, 0) & Chr(m_lSeparador) & _
        IIf(oInstancia.MostrarMensajeSinpedido, 1, 0) & Chr(m_lSeparador) & _
        oInstancia.MensajeIntegracion & Chr(m_lSeparador) & _
        oInstancia.TextoMonitorizacion & Chr(m_lSeparador)
    Next
    
    If Not bMostrarMensajeSinpedido Then sdbgSolicitudes.Columns("ALERTA_PEDIDO").Visible = False
    If Not bMostrarMensajeSinproceso Then sdbgSolicitudes.Columns("ALERTA_PROCESO").Visible = False
    If Not bMostrarMensajeSinintegracion Then sdbgSolicitudes.Columns("ALERTA_INTEGRACION").Visible = False
    If Not bMostrarMonitorizacionActivada Then
        sdbgSolicitudes.Columns("ALERTA_MONITORIZACION").Visible = False
    Else
        sdbgSolicitudes.Columns("ALERTA_MONITORIZACION").Visible = True
    End If
        
    'Siempre scrolo hasta el principio, las columnas alerta se ven "dinamicamente". Y de una busqueda q no mostraba ninguna alerta, he pasado a una busqueda q me muestra las 3.
    'En una pantalla 1280x1024 (no entran todas las columnas) no las ve�a ya q el grid se hace automaticamente scroll para q la columna "Tipo" siga siendo la primera.
    sdbgSolicitudes.Scroll -100, 0
    sdbgSolicitudes.Update
        
    sdbgSolicitudes.MoveFirst
       
    If m_oInstancias.TotalPaginas < 2 Then
        OcultarPaginacion
    Else
        MostrarPaginacion
        lblPageNumber.caption = Replace(Replace(m_sPageNumber, "{n}", m_oInstancias.PaginaActual), "{m}", m_oInstancias.TotalPaginas)
        If m_oInstancias.PaginaActual = 1 Then
            cmdLast.Enabled = True
            cmdNext.Enabled = True
            cmdFirst.Enabled = False
            cmdPrevious.Enabled = False
        End If
    End If

    HabilitarBotones
    
    Unload frmADJCargar
    
    LockWindowUpdate 0&
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarGridSolicitudes", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub OcultarPaginacion()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmdLast.Visible = False
    cmdNext.Visible = False
    cmdFirst.Visible = False
    cmdPrevious.Visible = False
    lblPageNumber.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "OcultarPaginacion", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub MostrarPaginacion()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmdLast.Visible = True
    cmdNext.Visible = True
    cmdFirst.Visible = True
    cmdPrevious.Visible = True
    lblPageNumber.Visible = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "MostrarPaginacion", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Public Sub AprobarSolicitud()
    'Llama al formulario para aprobar la solicitud
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))) Is Nothing Then Exit Sub
    
    frmSolicitudAprobar.g_sOrigen = "frmSolicitudes"
    frmSolicitudAprobar.g_sOrigen2 = "frmSolicitudes"
    Set frmSolicitudAprobar.g_oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
    frmSolicitudAprobar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "AprobarSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub RechazarSolicitud()
    'Llama al formulario para anular la solicitud
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))) Is Nothing Then Exit Sub
    
    frmSolicitudGestionar.g_sOrigen = "frmSolicitudes"
    frmSolicitudGestionar.g_sOrigen2 = "frmSolicitudes"
    frmSolicitudGestionar.g_iAccion = 4
    Set frmSolicitudGestionar.g_oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
    frmSolicitudGestionar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "RechazarSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Carga las variables de seguridad seg�n el usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 15/01/2013</revision>
Private Sub ConfigurarSeguridad()
    'Modificar datos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICModificarDatos)) Is Nothing) Then m_bModificar = True
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAprobRechazoCierre)) Is Nothing) Then m_bAprobRechazoCierre = True
    'Asignar comprador
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAsignar)) Is Nothing) Then m_bAsignar = True
    'Reabrir
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICReabrir)) Is Nothing) Then m_bReabrir = True
    'Anular
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAnular)) Is Nothing) Then m_bAnular = True
    'Eliminar
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICEliminar)) Is Nothing) Then m_bEliminar = True
    'Restricci�n de equipo
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestEquipo)) Is Nothing) Then
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
            'S�lo le afectar� la restricci�n si el usuario es un comprador.
            m_bREquipo = True
        End If
    End If
    
    'Restricci�n de asignaci�n
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestAsig)) Is Nothing) Then m_bRAsig = True
    'Restricci�n de unidad organizativa
    m_bRuo = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestUO)) Is Nothing)
    'Restricci�n de departamento
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrDpto)) Is Nothing) Then m_bRDep = True
    'Abrir procesos
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAbrirProc)) Is Nothing) Then m_bAbrirProc = True
    'Enviar a procesos
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICEnviarProc)) Is Nothing) Then m_bEnviarProc = True
    'Pedidos directos
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICPedidoDirecto)) Is Nothing) Then m_bPedidoDirecto = True
    'Ver pedidos en curso
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICEnCurso)) Is Nothing) Then m_bVerEnCurso = True
    'Restriccion material usuario comprador
    If oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrMat)) Is Nothing Then
        m_bRestrMatComp = True
    End If
    
    m_bRPerfUON = (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestPerfUON)) Is Nothing))
    'Abrir proceso proceso desde solicitudes en curso
    m_bAbrirProcEnCurso = (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAbrirProcEnCurso)) Is Nothing))
   
    'Aprobar/rechazar/cierre
    If Not m_bAprobRechazoCierre Then
        MDI.mnuPopUpSolicitudes.Item(4).Visible = False
        MDI.mnuPopUpSolicitudes.Item(5).Visible = False
        MDI.mnuPopUpSolicitudes.Item(6).Visible = False
        cmdAprobar.Visible = False
        cmdRechazar.Visible = False
        cmdCerrar.Visible = False
        cmdAsignar.Left = cmdAprobar.Left
        cmdModificar.Left = cmdAsignar.Left + cmdAsignar.Width + 120
        cmdReabrir.Left = cmdModificar.Left + cmdModificar.Width + 120
        cmdAnular.Left = cmdReabrir.Left + cmdReabrir.Width + 120
        cmdEliminar.Left = cmdAnular.Left + cmdAnular.Width + 120
        cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 120
        cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 120
        cmdAbrirProc.Left = cmdListado.Left + cmdListado.Width + 120
        cmdPedidoDir.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 120
    End If
    
    'Asignar comprador
    If Not m_bAsignar Then
        MDI.mnuPopUpSolicitudes.Item(3).Visible = False
        cmdAsignar.Visible = False
        cmdModificar.Left = cmdAsignar.Left
        If m_bAprobRechazoCierre = True Then
            cmdCerrar.Left = cmdModificar.Left + cmdModificar.Width + 120
            cmdReabrir.Left = cmdCerrar.Left + cmdCerrar.Width + 120
        Else
            cmdReabrir.Left = cmdModificar.Left + cmdModificar.Width + 120
        End If
        cmdAnular.Left = cmdReabrir.Left + cmdReabrir.Width + 120
        cmdEliminar.Left = cmdAnular.Left + cmdAnular.Width + 120
        cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 120
        cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 120
        cmdAbrirProc.Left = cmdListado.Left + cmdListado.Width + 120
        cmdPedidoDir.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 120
    End If
    
    'Modificar
    If Not m_bModificar Then
        MDI.mnuPopUpSolicitudes.Item(2).Visible = False
        cmdModificar.Visible = False
        If m_bAprobRechazoCierre = True Then
            cmdCerrar.Left = cmdModificar.Left
            cmdReabrir.Left = cmdCerrar.Left + cmdCerrar.Width + 120
        Else
            cmdReabrir.Left = cmdModificar.Left
        End If
        cmdAnular.Left = cmdReabrir.Left + cmdReabrir.Width + 120
        cmdEliminar.Left = cmdAnular.Left + cmdAnular.Width + 120
        cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 120
        cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 120
        lblConfigurarAlerta.Visible = False
        cmdAbrirProc.Left = cmdListado.Left + cmdListado.Width + 120
        cmdPedidoDir.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 120
    End If
    
    'Reabrir
    If Not m_bReabrir Then
        MDI.mnuPopUpSolicitudes.Item(7).Visible = False
        cmdReabrir.Visible = False
        cmdAnular.Left = cmdReabrir.Left
        cmdEliminar.Left = cmdAnular.Left + cmdAnular.Width + 120
        cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 120
        cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 120
        cmdAbrirProc.Left = cmdListado.Left + cmdListado.Width + 120
        cmdPedidoDir.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 120
    End If
    
    
    'Anular
    If Not m_bAnular Then
        MDI.mnuPopUpSolicitudes.Item(8).Visible = False
        cmdAnular.Visible = False
        cmdEliminar.Left = cmdAnular.Left
        cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 120
        cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 120
        cmdAbrirProc.Left = cmdListado.Left + cmdListado.Width + 120
        cmdPedidoDir.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 120
    End If
    
    'Eliminar
    If Not m_bEliminar Then
        MDI.mnuPopUpSolicitudes.Item(9).Visible = False
        cmdEliminar.Visible = False
        cmdRestaurar.Left = cmdEliminar.Left
        cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 120
        cmdAbrirProc.Left = cmdListado.Left + cmdListado.Width + 120
        cmdPedidoDir.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 120
    End If
    
    'abrir proceso
    If Not m_bAbrirProc Then
        cmdAbrirProc.Visible = False
        cmdPedidoDir.Left = cmdListado.Left + cmdListado.Width + 70
    End If
    
    'Pedido directo
    If Not m_bPedidoDirecto Then cmdPedidoDir.Visible = False
            
    'Ver pedidos en curso
    If m_bVerEnCurso = False Then
        chkEstado(5).Visible = False
        chkEstado(4).Top = chkEstado(3).Top
        chkEstado(3).Top = chkEstado(2).Top
        chkEstado(2).Top = chkEstado(1).Top
        chkEstado(1).Top = chkEstado(0).Top
        chkEstado(0).Top = chkEstado(5).Top
    End If
    
    'Abrir procesos multimaterial
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEPermProcMultimaterial)) Is Nothing) Then
        If gParametrosGenerales.gbMultiMaterial Then
            m_bPermProcMultimaterial = True
        Else
            m_bPermProcMultimaterial = False
        End If
    Else
        m_bPermProcMultimaterial = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub AnularSolicitud()
    Dim irespuesta As Integer
    
    'Llama al formulario para anular la solicitud
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))) Is Nothing Then Exit Sub
    
    'Comprueba si la solicitud est� incluida ya en alg�n pedido directo o de aprovisionamiento
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))).ComprobarExistenPedidosRelacionados = True Then
        irespuesta = oMensajes.PreguntarAnularCerrarSolicitud
        If irespuesta = vbNo Then Exit Sub
    End If
    
    frmSolicitudGestionar.g_sOrigen = "frmSolicitudes"
    frmSolicitudGestionar.g_sOrigen2 = "frmSolicitudes"
    frmSolicitudGestionar.g_iAccion = 1
    Set frmSolicitudGestionar.g_oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
    frmSolicitudGestionar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "AnularSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub EliminarSolicitud()
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    Dim oInstancia As CInstancia
    Dim Ador As Ador.Recordset
    
    'Elimina la solicitud que est� seleccionada en la grid
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))) Is Nothing Then Exit Sub
    
    irespuesta = oMensajes.PreguntaEliminarSolicitudes(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))

    If irespuesta = vbNo Then Exit Sub
        
    Set oInstancia = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
    oInstancia.Usuario = basOptimizacion.gvarCodUsuario
    
    Set Ador = oInstancia.DevolverProcesosRelacionados
    If Not Ador Is Nothing Then
        If Ador.RecordCount > 0 Then
            oMensajes.MensajeOKOnly 931, Exclamation  'Imposible eliminar la solicitud. Existen procesos asociados
            Exit Sub
        End If
    End If
    Set Ador = oInstancia.DevolverPedidosRelacionados(bSoloExist:=1)
    If Not Ador Is Nothing Then
        If Ador.RecordCount > 0 Then
            oMensajes.MensajeOKOnly 1481, Exclamation  'Imposible eliminar la solicitud. Existen pedidos asociados
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    DoEvents
    frmADJCargar.Show
    frmADJCargar.Refresh
    
    Set m_oIBaseDatos = oInstancia
    teserror = m_oIBaseDatos.EliminarDeBaseDatos

    If teserror.NumError <> TESnoerror Then
        Unload frmADJCargar
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
    Else
        'Elimina la solicitud de la grid y de la colecci�n
        m_oInstancias.Remove (CStr(oInstancia.Id))
        sdbgSolicitudes.RemoveItem (sdbgSolicitudes.AddItemRowIndex(sdbgSolicitudes.Bookmark))
        'Se posiciona en la solicitud siguiente
        If sdbgSolicitudes.Rows > 0 Then
            If IsEmpty(sdbgSolicitudes.RowBookmark(sdbgSolicitudes.Row)) Then
                sdbgSolicitudes.Bookmark = sdbgSolicitudes.RowBookmark(sdbgSolicitudes.Row - 1)
            Else
                sdbgSolicitudes.Bookmark = sdbgSolicitudes.RowBookmark(sdbgSolicitudes.Row)
            End If
        End If

        ''' Registro de acciones
        basSeguridad.RegistrarAccion AccionesSummit.ACCSolicEli, "Id:" & oInstancia.Id
    End If

    HabilitarBotones

    Set oInstancia = Nothing
    Set m_oIBaseDatos = Nothing
    
    Unload frmADJCargar
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "EliminarSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub ReabrirSolicitud()
   'Llama al formulario para anular la solicitud
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))) Is Nothing Then Exit Sub
    
    frmSolicitudGestionar.g_sOrigen = "frmSolicitudes"
    frmSolicitudGestionar.g_sOrigen2 = "frmSolicitudes"
    frmSolicitudGestionar.g_iAccion = 3
    Set frmSolicitudGestionar.g_oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
    frmSolicitudGestionar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "ReabrirSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub CerrarSolicitud()
    Dim irespuesta As Integer
    
    'Llama al formulario para anular la solicitud
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))) Is Nothing Then Exit Sub
    
    'Comprueba si la solicitud est� incluida ya en alg�n pedido directo o de aprovisionamiento
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))).ComprobarExistenPedidosRelacionados = True Then
        irespuesta = oMensajes.PreguntarAnularCerrarSolicitud
        If irespuesta = vbNo Then Exit Sub
    End If
    
    frmSolicitudGestionar.g_sOrigen = "frmSolicitudes"
    frmSolicitudGestionar.g_sOrigen2 = "frmSolicitudes"
    frmSolicitudGestionar.g_iAccion = 2
    Set frmSolicitudGestionar.g_oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
    frmSolicitudGestionar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CerrarSolicitud", err, Erl, Me, m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub ModificarSolicitud()
        'Muestra el detalle de la solicitud para modificarla:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_ofrmDetalleSolic Is Nothing Then
        Unload g_ofrmDetalleSolic
        Set g_ofrmDetalleSolic = Nothing
    End If
    Set g_ofrmDetalleSolic = New frmSolicitudDetalle
    
    g_ofrmDetalleSolic.g_sOrigen = "frmSolicitudes"
    Set g_ofrmDetalleSolic.g_oSolicitudSeleccionada = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))

    Screen.MousePointer = vbHourglass
    DoEvents
    frmADJCargar.Show
    frmADJCargar.Refresh
    
    MDI.MostrarFormulario g_ofrmDetalleSolic
    Unload frmADJCargar
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "ModificarSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
''' <summary>
''' Detalle de desglose
''' </summary>
''' <remarks>Llamada desde: mdi.mnuPopUpSolicitudes_Click ; Tiempo m�ximo: 0,2</remarks>
Public Sub DetalleDesgloseSolicitud()
    Dim oCampo As CFormItem
    Dim oCampoAnt As CFormItem
    Dim oCampoAntAnt As CFormItem
    Dim i As Integer
    Dim oCampoDesglose As CFormItem
    Dim oCampoOrganiza As CFormItem
    Dim oCampoCentro As CFormItem
    
    Dim oInstancia As CInstancia
    Dim oGrupo As CFormGrupo
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSolicitudes.Rows = 0 Then Exit Sub
    If sdbgSolicitudes.Row = -1 Then Exit Sub
    
    'Obtener primer desglose/ centro / org. compras / Instancia
    Set oInstancia = m_oInstancias.Item(Me.sdbgSolicitudes.Columns("ID").Value)
    oInstancia.CargarGrupos
        
    For Each oGrupo In oInstancia.Grupos
        oGrupo.CargarTodosLosCampos
        If Not oGrupo.Campos Is Nothing Then
            i = 0
            
            Set oCampoCentro = Nothing
            Set oCampoOrganiza = Nothing
            Set oCampoDesglose = Nothing
            
            For Each oCampo In oGrupo.Campos
                If ((oCampo.CampoGS = TipoCampoGS.Desglose) Or (IsNull(oCampo.CampoGS) And (oCampo.Tipo = TipoDesglose))) Then
                    Set oCampoDesglose = oCampo
                    
                    If oCampoAnt.CampoGS = TipoCampoGS.Centro Then
                        Set oCampoCentro = oCampoAnt
                        If Not oCampoAntAnt Is Nothing Then
                            If oCampoAntAnt.CampoGS = TipoCampoGS.OrganizacionCompras Then
                                Set oCampoOrganiza = oCampoAntAnt
                            End If
                        End If
                    ElseIf oCampoAnt.CampoGS = TipoCampoGS.OrganizacionCompras Then
                        Set oCampoOrganiza = oCampoAnt
                    End If
                    
                    Exit For
                End If
                
                If i > 0 Then Set oCampoAntAnt = oCampoAnt
                
                i = i + 1
                
                Set oCampoAnt = oCampo
            Next
            
            If Not oCampoDesglose Is Nothing Then Exit For
        End If
    Next
    
    If oCampoDesglose Is Nothing Then
        oMensajes.SolicitudSinDesglose
        Exit Sub
    End If
    
    'Mostrar desglose
    If Not g_ofrmDesgloseValores Is Nothing Then
        Unload g_ofrmDesgloseValores
        Set g_ofrmDesgloseValores = Nothing
    End If
    
    Set g_ofrmDesgloseValores = New frmSolicitudDesglose
    
    g_ofrmDesgloseValores.g_bModif = False

    If oCampoOrganiza Is Nothing Then
        g_ofrmDesgloseValores.g_sCodOrganizacionCompras = "-1"
    Else
        g_ofrmDesgloseValores.g_sCodOrganizacionCompras = oCampoOrganiza.valorText
    End If
    If oCampoCentro Is Nothing Then
        g_ofrmDesgloseValores.g_sCodCentro = "-1"
    Else
        g_ofrmDesgloseValores.g_sCodCentro = oCampoCentro.valorText
    End If

    Set g_ofrmDesgloseValores.g_oCampoDesglose = oCampoDesglose
    Set g_ofrmDesgloseValores.g_oInstanciaSeleccionada = oInstancia
    g_ofrmDesgloseValores.g_sOrigen = "frmVisorSolicitudes"
    
    Screen.MousePointer = vbHourglass
    g_ofrmDesgloseValores.WindowState = vbNormal
    g_ofrmDesgloseValores.Show
        
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "DetalleDesgloseSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub DetalleSolicitud()
    'Muestra el detalle de la solicitud:
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_ofrmDetalleSolic Is Nothing Then
        Unload g_ofrmDetalleSolic
        Set g_ofrmDetalleSolic = Nothing
    End If
    Set g_ofrmDetalleSolic = New frmSolicitudDetalle
    
    g_ofrmDetalleSolic.g_sOrigen = "frmSolicitudes"
    Set g_ofrmDetalleSolic.g_oSolicitudSeleccionada = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))

    Dim oInstancia As CInstancia
    Set oInstancia = oFSGSRaiz.Generar_CInstancia
    oInstancia.CargarDatosInstancia g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Id
    
    DoEvents
    If oInstancia.EnProceso = 1 Then
        frmMessageBox.g_bCancel = False
        frmMessageBox.g_udtBotones = TipoBotonesMensaje.OKOnly
        frmMessageBox.g_udtIcono = TipoIconoMensaje.Information
        frmMessageBox.g_sMensaje = m_sMensajeEnProceso
        Screen.MousePointer = vbNormal
        frmMessageBox.Show 1
    Else
        Screen.MousePointer = vbNormal
        MDI.MostrarFormulario g_ofrmDetalleSolic
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "DetalleSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub AsignarComprador()
    'Llama al formulario para aprobar la solicitud
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))) Is Nothing Then Exit Sub
    
    frmSolicitudAsignarComp.g_sOrigen = "frmSolicitudes"
    frmSolicitudAsignarComp.g_sOrigen2 = "frmSolicitudes"
    frmSolicitudAsignarComp.g_bRestricEquipo = m_bREquipo
    Set frmSolicitudAsignarComp.g_oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
    frmSolicitudAsignarComp.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "AsignarComprador", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub ActualizarEstadoEnGrid(ByVal lID As Long, ByVal iEstado As Integer)
    'Actualiza la grid y la colecci�n con el nuevo estado para la solicitud seleccionada
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case iEstado
        Case EstadoSolicitud.Anulada
            m_oInstancias.Item(CStr(lID)).Estado = EstadoSolicitud.Anulada
            sdbgSolicitudes.Columns("ID_ESTADO").Value = EstadoSolicitud.Anulada
            sdbgSolicitudes.Columns("ESTADO").Value = m_sAnulada
            
        Case EstadoSolicitud.Aprobada
            m_oInstancias.Item(CStr(lID)).Estado = EstadoSolicitud.Aprobada
            sdbgSolicitudes.Columns("ID_ESTADO").Value = EstadoSolicitud.Aprobada
            sdbgSolicitudes.Columns("ESTADO").Value = m_sAprobada
            
        Case EstadoSolicitud.Rechazada
            m_oInstancias.Item(CStr(lID)).Estado = EstadoSolicitud.Rechazada
            sdbgSolicitudes.Columns("ID_ESTADO").Value = EstadoSolicitud.Rechazada
            sdbgSolicitudes.Columns("ESTADO").Value = m_sRechazada
            
        Case EstadoSolicitud.Cerrada
            m_oInstancias.Item(CStr(lID)).Estado = EstadoSolicitud.Cerrada
            sdbgSolicitudes.Columns("ID_ESTADO").Value = EstadoSolicitud.Cerrada
            sdbgSolicitudes.Columns("ESTADO").Value = m_sCerrada
            
    End Select
    
    sdbgSolicitudes.Update
    
    HabilitarBotones

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "ActualizarEstadoEnGrid", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Habilita los men�s seg�n el estado de la solicitud seleccionada
''' </summary>
''' <remarks>Llamada desde: sdbgSolicitudes_RowColChange  CargarGridSolicitudes    EliminarSolicitud        ActualizarEstadoEnGrid; Tiempo m�ximo: 0,2</remarks>
Private Sub HabilitarBotones()
    Dim bMultiSelec As Boolean
    Dim bGenRechazadaSelec As Boolean
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
      
    bMultiSelec = False
    If Not sdbgSolicitudes.SelBookmarks Is Nothing Then bMultiSelec = (sdbgSolicitudes.SelBookmarks.Count > 1)
    'Comprobar si todas las seleccionadas est�n en estado en curso de aprobaci�n (rechazada)
    If bMultiSelec Then
        bGenRechazadaSelec = True
        Dim vbm As Variant
        Dim oInstancia As CInstancia
        For Each vbm In sdbgSolicitudes.SelBookmarks
            Set oInstancia = m_oInstancias.Item(sdbgSolicitudes.Columns("ID").CellValue(vbm))
            If oInstancia.Estado <> EstadoSolicitud.GenRechazada Then
                bGenRechazadaSelec = False
                Exit For
            End If
        Next
        Set oInstancia = Nothing
    End If
    
    If sdbgSolicitudes.Rows = 0 Then
        cmdAprobar.Enabled = False
        cmdRechazar.Enabled = False
        cmdAsignar.Enabled = False
        cmdModificar.Enabled = False
        cmdCerrar.Enabled = False
        cmdReabrir.Enabled = False
        cmdAnular.Enabled = False
        cmdEliminar.Enabled = False
        cmdAbrirProc.Enabled = False
        cmdPedidoDir.Enabled = False
        Exit Sub
    End If
    
    'Por defecto se coge el estado de la solicitud de la primera fila
    'Si la fila actual est� seleccionada se toma el estado de esta para la visibilidad de los botones
    'Si no est� seleccionada se toma cualquier fila (la primera) de la colecci�n de seleccionadas para la visibilidad de los botones
    Dim iEstado As EstadoSolicitud
    iEstado = sdbgSolicitudes.Columns("ID_ESTADO").CellValue(sdbgSolicitudes.GetBookmark(0))
    If Not sdbgSolicitudes.SelBookmarks Is Nothing Then
        If Not IsEmpty(sdbgSolicitudes.SelBookmarks.Item(CStr(sdbgSolicitudes.Bookmark))) Then
            iEstado = sdbgSolicitudes.Columns("ID_ESTADO").CellValue(sdbgSolicitudes.Bookmark)
        Else
            If sdbgSolicitudes.SelBookmarks.Count > 0 Then iEstado = sdbgSolicitudes.Columns("ID_ESTADO").CellValue(sdbgSolicitudes.SelBookmarks(0))
        End If
    End If
    
    Select Case iEstado
        Case EstadoSolicitud.Anulada
            cmdAnular.Enabled = False
            cmdAprobar.Enabled = False
            cmdRechazar.Enabled = False
            cmdCerrar.Enabled = False
            cmdModificar.Enabled = False
            cmdAsignar.Enabled = False
            cmdReabrir.Enabled = False
            cmdEliminar.Enabled = True And Not bMultiSelec
            If bMultiSelec Then
                cmdAbrirProc.Enabled = True
                cmdPedidoDir.Enabled = True
            Else
                cmdAbrirProc.Enabled = False
                cmdPedidoDir.Enabled = False
            End If
            
        Case EstadoSolicitud.Aprobada
            cmdAnular.Enabled = True And Not bMultiSelec
            cmdAprobar.Enabled = False
            cmdRechazar.Enabled = False
            cmdCerrar.Enabled = True And Not bMultiSelec
            cmdModificar.Enabled = True And Not bMultiSelec
            cmdAsignar.Enabled = True And Not bMultiSelec
            cmdReabrir.Enabled = False
            cmdEliminar.Enabled = True And Not bMultiSelec
            cmdAbrirProc.Enabled = (Not bMultiSelec Or (bMultiSelec And Not bGenRechazadaSelec))
            cmdPedidoDir.Enabled = (Not bMultiSelec Or (bMultiSelec And Not bGenRechazadaSelec))
            
        Case EstadoSolicitud.Cerrada
            cmdAnular.Enabled = False
            cmdAprobar.Enabled = True And Not bMultiSelec
            cmdRechazar.Enabled = False
            cmdCerrar.Enabled = False
            cmdModificar.Enabled = False
            cmdAsignar.Enabled = False
            cmdReabrir.Enabled = True And Not bMultiSelec
            cmdEliminar.Enabled = True And Not bMultiSelec
            If bMultiSelec Then
                cmdAbrirProc.Enabled = Not bGenRechazadaSelec
                cmdPedidoDir.Enabled = Not bGenRechazadaSelec
            Else
                cmdAbrirProc.Enabled = False
                cmdPedidoDir.Enabled = False
            End If
            
        Case EstadoSolicitud.Pendiente
            cmdAnular.Enabled = True And Not bMultiSelec
            cmdAprobar.Enabled = True And Not bMultiSelec
            cmdRechazar.Enabled = True And Not bMultiSelec
            cmdCerrar.Enabled = False
            cmdModificar.Enabled = True And Not bMultiSelec
            cmdAsignar.Enabled = True And Not bMultiSelec
            cmdReabrir.Enabled = False
            cmdEliminar.Enabled = True And Not bMultiSelec
            cmdAbrirProc.Enabled = (Not bMultiSelec Or (bMultiSelec And Not bGenRechazadaSelec))
            cmdPedidoDir.Enabled = (Not bMultiSelec Or (bMultiSelec And Not bGenRechazadaSelec))
            
        Case EstadoSolicitud.Rechazada
            cmdAnular.Enabled = False
            cmdAprobar.Enabled = False
            cmdRechazar.Enabled = False
            cmdCerrar.Enabled = False
            cmdModificar.Enabled = False
            cmdAsignar.Enabled = False
            cmdReabrir.Enabled = False
            cmdEliminar.Enabled = True And Not bMultiSelec
            If bMultiSelec Then
                cmdAbrirProc.Enabled = Not bGenRechazadaSelec
                cmdPedidoDir.Enabled = Not bGenRechazadaSelec
            Else
                cmdAbrirProc.Enabled = False
                cmdPedidoDir.Enabled = False
            End If
        
        Case EstadoSolicitud.GenRechazada
            cmdAprobar.Enabled = False
            cmdAsignar.Enabled = True And Not bMultiSelec
            cmdRechazar.Enabled = False
            cmdCerrar.Enabled = False
            cmdModificar.Enabled = False
            cmdReabrir.Enabled = False
            cmdEliminar.Enabled = True And Not bMultiSelec
            cmdAnular.Enabled = True And Not bMultiSelec
            cmdAbrirProc.Enabled = (bMultiSelec And Not bGenRechazadaSelec)
            cmdPedidoDir.Enabled = (bMultiSelec And Not bGenRechazadaSelec)
            
        Case Else  'En Curso de Aprobaci�n
            cmdAprobar.Enabled = False
            cmdAsignar.Enabled = True And Not bMultiSelec
            cmdRechazar.Enabled = False
            cmdCerrar.Enabled = False
            cmdModificar.Enabled = False
            cmdReabrir.Enabled = False
            cmdEliminar.Enabled = True And Not bMultiSelec
            cmdAnular.Enabled = True And Not bMultiSelec
            cmdAbrirProc.Enabled = (Not bMultiSelec Or (bMultiSelec And Not bGenRechazadaSelec))
            cmdPedidoDir.Enabled = (Not bMultiSelec Or (bMultiSelec And Not bGenRechazadaSelec))
    End Select
    
    cmdEliminar.Enabled = Not bMultiSelec
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "HabilitarBotones", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub ActualizarCompradorEnGrid(ByVal lID As Long, ByVal oComp As CComprador)
    'Actualiza la grid y la colecci�n con el nuevo comprador que se le ha asignado
    'a la solicitud seleccionada
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oInstancias.Item(CStr(lID)).comprador = oComp
    
    sdbgSolicitudes.Columns("COMP").Value = oComp.Cod & " - " & NullToStr(oComp.nombre) & " " & NullToStr(oComp.Apel)
    sdbgSolicitudes.Columns("COD_COMP").Value = oComp.Cod
    
    sdbgSolicitudes.Update
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "ActualizarCompradorEnGrid", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub ActualizarGridEliminar()
    'Se le llama desde el formulario de detalle,cuando se elimina la solicitud seleccionada
    Dim oInstancia As CInstancia
    
    'Elimina la solicitud de la grid y de la colecci�n
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))) Is Nothing Then Exit Sub
    
    Set oInstancia = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
    m_oInstancias.Remove (CStr(oInstancia.Id))
    Set oInstancia = Nothing
    
    sdbgSolicitudes.RemoveItem (sdbgSolicitudes.AddItemRowIndex(sdbgSolicitudes.Bookmark))
    
    'Se posiciona en la solicitud siguiente
    If sdbgSolicitudes.Rows > 0 Then
        If IsEmpty(sdbgSolicitudes.RowBookmark(sdbgSolicitudes.Row)) Then
            sdbgSolicitudes.Bookmark = sdbgSolicitudes.RowBookmark(sdbgSolicitudes.Row - 1)
        Else
            sdbgSolicitudes.Bookmark = sdbgSolicitudes.RowBookmark(sdbgSolicitudes.Row)
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "ActualizarGridEliminar", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub ActualizarDatosSolicEnGrid(ByVal oSolic As CInstancia)
    'Actualiza la grid y la colecci�n con el nuevo comprador que se le ha asignado
    'a la solicitud seleccionada
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgSolicitudes.Columns("DESCR").Value = oSolic.DescrBreve
    sdbgSolicitudes.Columns("IMPORTE").Value = oSolic.importe
    
    sdbgSolicitudes.Update
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "ActualizarDatosSolicEnGrid", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub ListarProceso()
    ''' * Objetivo: Obtener un listado de solicitudes
    '''   Muestra el formulario para imprimir el listado
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmLstSolicitud.g_sOrigen = "frmSolicitudes"
    frmLstSolicitud.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "ListarProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbcPeticionario_Validate(Cancel As Boolean)
    Dim oPersonas As CPersonas
    Dim bExiste As Boolean
    Dim sbuscar As String
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
      
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcPeticionario.Text = "" Then
        sdbcPeticionario.Columns(0).Value = ""
        Exit Sub
    End If
    
    If sdbcPeticionario.Text = sdbcPeticionario.Columns(0).Text Then Exit Sub

    Set oPersonas = oFSGSRaiz.Generar_CPersonas
    
    If sdbcPeticionario.Columns(0).Value = "" Then
        sdbcPeticionario.Text = ""
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el peticionario
    
    Screen.MousePointer = vbHourglass
    
    If sdbcPeticionario.Columns(0).Text = "" Then
        sbuscar = "NULL"
    Else
        sbuscar = sdbcPeticionario.Columns(0).Text
    End If
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If m_bRuo Then
        oPersonas.CargarPeticionariosSolCompra True, sbuscar, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRPerfUON, lIdPerfil
    Else
        oPersonas.CargarPeticionariosSolCompra True, sbuscar, , , , m_bRPerfUON, lIdPerfil
    End If
    
    bExiste = Not (oPersonas.Count = 0)
    
    If Not bExiste Then
        sdbcPeticionario.Text = ""
    Else
        m_bRespetarCombo = True
        sdbcPeticionario.Text = oPersonas.Item(1).Cod & " - " & NullToStr(oPersonas.Item(1).nombre) & " " & NullToStr(oPersonas.Item(1).Apellidos)
        sdbcPeticionario.Columns(0).Value = oPersonas.Item(1).Cod
        sdbcPeticionario.Columns(1).Value = oPersonas.Item(1).Cod & "-" & oPersonas.Item(1).nombre & " " & NullToStr(oPersonas.Item(1).Apellidos)
        
        m_bRespetarCombo = False
        
    End If
    
    Set oPersonas = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "sdbcPeticionario_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub MostrarDetalleProveedor(ByVal CodProve As String)
Dim oProves As CProveedores
Dim oProve As CProveedor
Dim oCon As CContacto
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, CodProve, , True
    
    If oProves.Count = 0 Then
        oMensajes.DatoEliminado m_sProveedor
        Set oProves = Nothing
        Exit Sub
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor CodProve
    
    Set frmProveDetalle.g_oProveSeleccionado = oProve
    frmProveDetalle.caption = CodProve & "  " & oProve.Den
    frmProveDetalle.lblCodPortProve = NullToStr(oProve.CodPortal)
    frmProveDetalle.g_bPremium = oProve.EsPremium
    frmProveDetalle.g_bActivo = oProve.EsPremiumActivo
    frmProveDetalle.lblDir = NullToStr(oProve.Direccion)
    frmProveDetalle.lblCp = NullToStr(oProve.cP)
    frmProveDetalle.lblPob = NullToStr(oProve.Poblacion)
    frmProveDetalle.lblPaiCod = NullToStr(oProve.CodPais)
    frmProveDetalle.lblPaiDen = NullToStr(oProve.DenPais)
    frmProveDetalle.lblMonCod = NullToStr(oProve.CodMon)
    frmProveDetalle.lblMonDen = NullToStr(oProve.DenMon)
    frmProveDetalle.lblProviCod = NullToStr(oProve.CodProvi)
    frmProveDetalle.lblProviDen = NullToStr(oProve.DenProvi)
    frmProveDetalle.lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
    frmProveDetalle.lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
    frmProveDetalle.lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
    
    If Trim(oProve.Calif1) <> "" Then frmProveDetalle.lblCal1Den = oProve.Calif1
    If Trim(oProve.Calif2) <> "" Then frmProveDetalle.lblcal2den = oProve.Calif2
    If Trim(oProve.Calif3) <> "" Then frmProveDetalle.lblcal3den = oProve.Calif3
        
    frmProveDetalle.lblCal1Val = DblToStr(oProve.Val1)
    frmProveDetalle.lblCal2Val = DblToStr(oProve.Val2)
    frmProveDetalle.lblCal3Val = DblToStr(oProve.Val3)

    frmProveDetalle.lblURL = NullToStr(oProve.URLPROVE)

    oProve.CargarTodosLosContactos , , , , True, , , , , , , True
    
    For Each oCon In oProve.Contactos
        
        frmProveDetalle.sdbgContactos.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.Departamento & Chr(m_lSeparador) & oCon.Cargo _
        & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Port & Chr(m_lSeparador)
    
    Next
        
    Set oProves = Nothing
    Set oProve = Nothing
    
    frmProveDetalle.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "MostrarDetalleProveedor", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub MostrarUOSeleccionada()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sUON1 = frmSELUO.sUON1 And sUON2 = frmSELUO.sUON2 And sUON3 = frmSELUO.sUON3 Then Exit Sub
        
    If frmSELUO.sUON3 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sUON3 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = frmSELUO.sUON3
        
    ElseIf frmSELUO.sUON2 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = ""
        
    ElseIf frmSELUO.sUON1 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = ""
        sUON3 = ""
        
    Else
        txtUO.Text = ""
        txtUO.Refresh
        sUON1 = ""
        sUON2 = ""
        sUON3 = ""
    End If
    
    If m_bCargandoFiltro = True Then g_oConfVisorSol.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "MostrarUOSeleccionada", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Guardar la configuracion del Visor
''' </summary>
''' <remarks>Llamada desde: Form_unload ; Tiempo m�ximo: 0</remarks>
Private Sub GuardarConfiguracionVisorSol()
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
       
    'almacena los widths
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_oConfVisorSol.TipoWidth = sdbgSolicitudes.Columns("TIPO").Width
    g_oConfVisorSol.AltaWidth = sdbgSolicitudes.Columns("ALTA").Width
    g_oConfVisorSol.NecesidadWidth = sdbgSolicitudes.Columns("NECESIDAD").Width
    g_oConfVisorSol.IdentWidth = sdbgSolicitudes.Columns("ID2").Width
    g_oConfVisorSol.DenominacionWidth = sdbgSolicitudes.Columns("DESCR").Width
    g_oConfVisorSol.ImporteWidth = sdbgSolicitudes.Columns("IMPORTE").Width
    g_oConfVisorSol.PeticionarioWidth = sdbgSolicitudes.Columns("PET").Width
    g_oConfVisorSol.UONWidth = sdbgSolicitudes.Columns("UON").Width
    g_oConfVisorSol.EstadoWidth = sdbgSolicitudes.Columns("ESTADO").Width
    g_oConfVisorSol.AsignadoAWidth = sdbgSolicitudes.Columns("COMP").Width
    g_oConfVisorSol.DepartamentoWidth = sdbgSolicitudes.Columns("DEPT").Width
    g_oConfVisorSol.SinProcesoWidth = sdbgSolicitudes.Columns("ALERTA_PROCESO").Width
    g_oConfVisorSol.SinPedidoWidth = sdbgSolicitudes.Columns("ALERTA_PEDIDO").Width
    g_oConfVisorSol.SinIntegracionWidth = sdbgSolicitudes.Columns("ALERTA_INTEGRACION").Width
    g_oConfVisorSol.NumSolErpWidth = sdbgSolicitudes.Columns("NUM_SOL_ERP").Width
    
    'almacena el filtro Tipo de solicitud:
    If sdbcTipoSolicit.Text <> "" Then
        g_oConfVisorSol.Tipo = sdbcTipoSolicit.Columns("DEN").Value
    Else
        g_oConfVisorSol.Tipo = Null
    End If
       
    'almacena el filtro Identificador
    g_oConfVisorSol.Identificador = StrToNull(txtId.Text)
    
    'almacena el filtro Descripci�n
    g_oConfVisorSol.Descripcion = StrToNull(txtDescr.Text)
    
    'almacena el filtro Desde:
    g_oConfVisorSol.Desde = StrToNull(txtFecDesde.Text)
    
    'almacena el filtro Hasta:
    g_oConfVisorSol.Hasta = StrToNull(txtFecHasta.Text)
    
    'almacena el filtro Peticionario:
    If sdbcPeticionario.Text <> "" Then
        g_oConfVisorSol.Peticionario = sdbcPeticionario.Columns(0).Text
    Else
        g_oConfVisorSol.Peticionario = Null
    End If
     
    'almacena el filtro de la unidad organizativa:
    g_oConfVisorSol.UON1 = StrToNull(sUON1)
    g_oConfVisorSol.UON2 = StrToNull(sUON2)
    g_oConfVisorSol.UON3 = StrToNull(sUON3)
    
    'almacena el filtro En curso de aprobacion
    g_oConfVisorSol.EnCursoAprobacion = SQLBinaryToBoolean(chkEstado(5).Value)
    'almacena el filtro Pendientes
    g_oConfVisorSol.Pendientes = SQLBinaryToBoolean(chkEstado(0).Value)
    'almacena el filtro Aprobadas
    g_oConfVisorSol.Aprobadas = SQLBinaryToBoolean(chkEstado(1).Value)
    'almacena el filtro Rechazadas
    g_oConfVisorSol.Rechazadas = SQLBinaryToBoolean(chkEstado(2).Value)
    'almacena el filtro Anuladas
    g_oConfVisorSol.Anuladas = SQLBinaryToBoolean(chkEstado(3).Value)
    'almacena el filtro Cerradas
    g_oConfVisorSol.Cerradas = SQLBinaryToBoolean(chkEstado(4).Value)
    
    g_oConfVisorSol.GMN1 = m_sGMN1Cod
    g_oConfVisorSol.GMN2 = m_sGMN2Cod
    g_oConfVisorSol.GMN3 = m_sGMN3Cod
    g_oConfVisorSol.GMN4 = m_sGMN4Cod
    
    If sdbcArticulo.Text <> "" Then
        g_oConfVisorSol.CodArticulo = sdbcArticulo.Text
    Else
        g_oConfVisorSol.CodArticulo = ""
    End If
        
    g_oConfVisorSol.DenArticulo = txtDenArticulo.Text
    If Trim(txtImporteDesde.Text) <> "" Then
        If Not IsNumeric(txtImporteDesde.Text) Then txtImporteDesde.Text = ""
    End If
    
    If sdbcProveedor.Text <> "" Then
        g_oConfVisorSol.Proveedor = sdbcProveedor.Text
    Else
        g_oConfVisorSol.Proveedor = ""
    End If
        
    g_oConfVisorSol.ImporteDesde = NullToDbl0(StrToDblOrNull(txtImporteDesde.Text))
    If Trim(txtImporteHasta.Text) <> "" Then
        If Not IsNumeric(txtImporteHasta.Text) Then txtImporteHasta.Text = ""
    End If
    g_oConfVisorSol.ImporteHasta = NullToDbl0(StrToDblOrNull(txtImporteHasta.Text))
    
    If sdbcEmpresa.Text <> "" Then
        g_oConfVisorSol.Empresa = sdbcEmpresa.Columns(0).Text
    Else
        g_oConfVisorSol.Empresa = 0
    End If
    
    If sdbcDepartamento.Text <> "" Then
        g_oConfVisorSol.Departamento = sdbcDepartamento.Columns(0).Text
    Else
        g_oConfVisorSol.Departamento = ""
    End If
    
    If sdbcComprador.Text <> "" Then
        g_oConfVisorSol.comprador = sdbcComprador.Columns(0).Text
    Else
        g_oConfVisorSol.comprador = ""
    End If
    
    g_oConfVisorSol.alertaIntegracion = SQLBinaryToBoolean(chkConfAlertaErrIntegracion.Value)
    g_oConfVisorSol.AlertaSinProceso = SQLBinaryToBoolean(chkConfAlertaSinProceso.Value)
    g_oConfVisorSol.AlertaSinPedido = SQLBinaryToBoolean(chkConfAlertaSinPedido.Value)
    
    g_oConfVisorSol.UON1_CC = m_sUON1_CC
    g_oConfVisorSol.UON2_CC = m_sUON2_CC
    g_oConfVisorSol.UON3_CC = m_sUON3_CC
    g_oConfVisorSol.UON4_CC = m_sUON4_CC
    
    Set oIBaseDatos = g_oConfVisorSol
    
    If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
        'Inserta la configuraci�n del visor
        teserror = oIBaseDatos.AnyadirABaseDatos
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oIBaseDatos.CancelarEdicion
            Exit Sub
        End If
        Set oIBaseDatos = Nothing

    Else
        'Modifica la configuraci�n del visor
        teserror = oIBaseDatos.FinalizarEdicionModificando
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oIBaseDatos.CancelarEdicion
            Exit Sub
        End If
        Set oIBaseDatos = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "GuardarConfiguracionVisorSol", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub GuardarConfiguracionVisorSolPP()
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    Dim i As Integer
    
    Dim udtPres5 As TipoConfigVisorSolPP
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oParametrosSM.Count > 1 Then
        Dim Row As Integer
        For Row = 0 To sdbgPartidas.Rows - 1
            If Len(sdbgPartidas.Columns("VALOR").CellValue(sdbgPartidas.RowBookmark(Row))) > 0 Then
                udtPres5.sPres5_Nivel0 = sdbgPartidas.Columns("COD").CellValue(sdbgPartidas.RowBookmark(Row))
                udtPres5.sPres5_Nivel1 = ""
                udtPres5.sPres5_Nivel2 = ""
                udtPres5.sPres5_Nivel3 = ""
                udtPres5.sPres5_Nivel4 = ""
                If Len(sdbgPartidas.Columns("PRES5NIV1").CellValue(sdbgPartidas.RowBookmark(Row))) > 0 Then
                    udtPres5.sPres5_Nivel1 = sdbgPartidas.Columns("PRES5NIV1").CellValue(sdbgPartidas.RowBookmark(Row))
                    If Len(sdbgPartidas.Columns("PRES5NIV2").CellValue(sdbgPartidas.RowBookmark(Row))) > 0 Then
                        udtPres5.sPres5_Nivel2 = sdbgPartidas.Columns("PRES5NIV2").CellValue(sdbgPartidas.RowBookmark(Row))
                        If Len(sdbgPartidas.Columns("PRES5NIV3").CellValue(sdbgPartidas.RowBookmark(Row))) > 0 Then
                            udtPres5.sPres5_Nivel3 = sdbgPartidas.Columns("PRES5NIV3").CellValue(sdbgPartidas.RowBookmark(Row))
                            If Len(sdbgPartidas.Columns("PRES5NIV4").CellValue(sdbgPartidas.RowBookmark(Row))) > 0 Then
                                udtPres5.sPres5_Nivel4 = sdbgPartidas.Columns("PRES5NIV4").CellValue(sdbgPartidas.RowBookmark(Row))
                            End If
                        End If
                    End If
                    g_oConfVisorSolPPs.Add oUsuarioSummit.Cod, udtPres5
                End If
            End If
        Next
    Else
        If Len(txtContrato(0).Text) > 0 Then
            g_oConfVisorSolPPs.Add oUsuarioSummit.Cod, m_aPres5(0)
        End If
    End If
        
    teserror = g_oConfVisorSolPPs.EliminarConfiguracionDeBaseDatos(oUsuarioSummit.Cod)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    For i = 1 To g_oConfVisorSolPPs.Count
        Set oIBaseDatos = g_oConfVisorSolPPs.Item(i)
        teserror = oIBaseDatos.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oIBaseDatos.CancelarEdicion
            Exit Sub
        End If
    Next
        
    Set oIBaseDatos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "GuardarConfiguracionVisorSolPP", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbgSolicitudes_SelChange(ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
    HabilitarBotones
End Sub

Private Sub txtCentroCoste_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCentroCosteValidado = False
    lstCentroCoste.Visible = False
    txtCentroCoste.Backcolor = &H80000005
    
    m_sUON1_CC = ""
    m_sUON2_CC = ""
    m_sUON3_CC = ""
    m_sUON4_CC = ""
    sUON_CC = ""
    
    Dim pos As Integer
    pos = InStr(1, txtCentroCoste.Text, " - ", vbTextCompare)
    If pos > 0 Then
        txtCentroCoste.Tag = Trim(Mid(txtCentroCoste.Text, 1, pos))
    Else
        txtCentroCoste.Tag = ""
    End If
    
    If txtCentroCoste.Text = "" Then
        Set g_oCenCos = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtCentroCoste_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtCentroCoste_KeyPress(KeyAscii As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        txtCentroCoste_Validate True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtCentroCoste_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtCentroCoste_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bCentroCosteValidado And txtCentroCoste.Text <> "" Then
        Dim sCod As String
        If Len(txtCentroCoste.Tag) > 0 Then
            sCod = CStr(txtCentroCoste.Tag)
        Else
            sCod = txtCentroCoste.Text
        End If
            
        If sUON1 <> "" Or sUON2 <> "" Or sUON3 <> "" Then
            If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
                CargarCentroCostePorUON sCod, sUON1, sUON2, sUON3
            Else
                CargarCentroCostePorUON sCod, sUON1, sUON2, sUON3, oUsuarioSummit.Cod
            End If
        Else
            If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
                CargarCentroCostePorCentroSM sCod
            Else
                If m_bRuo Then
                   CargarCentroCostePorUON sCod, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, oUsuarioSummit.Cod
                Else
                    If oUsuarioSummit.Pyme <> 0 Then
                        CargarCentroCostePorUON sCod, basOptimizacion.gUON1Usuario, , , oUsuarioSummit.Cod
                    Else
                        CargarCentroCostePorCentroSM sCod, oUsuarioSummit.Cod
                    End If
                End If
            End If
        End If
                
        If m_oCentrosCoste.Count > 0 Then
            If m_oCentrosCoste.Count > 1 Then
                lstCentroCoste.clear
                Dim sItemCentro As String
                Dim i As Integer
                For i = 1 To m_oCentrosCoste.Count
                    sItemCentro = m_oCentrosCoste.Item(i).CodConcat
                    If m_oCentrosCoste.Item(1).UON1 <> "" Then
                        sItemCentro = sItemCentro & " (" & m_oCentrosCoste.Item(1).UON1
                        If m_oCentrosCoste.Item(1).UON2 <> "" Then
                            sItemCentro = sItemCentro & "-" & m_oCentrosCoste.Item(1).UON2
                            If m_oCentrosCoste.Item(1).UON3 <> "" Then
                                sItemCentro = sItemCentro & "-" & m_oCentrosCoste.Item(1).UON3
                            End If
                        End If
                        sItemCentro = sItemCentro & ")"
                    End If
                    lstCentroCoste.AddItem sItemCentro
                Next
                lstCentroCoste.Visible = True
                If Me.Visible Then lstCentroCoste.SetFocus
            ElseIf m_oCentrosCoste.Count = 1 Then
                txtCentroCoste.Text = m_oCentrosCoste.Item(1).CodConcat
                If m_oCentrosCoste.Item(1).UON1 <> "" Then
                    txtCentroCoste.Text = txtCentroCoste.Text & " (" & m_oCentrosCoste.Item(1).UON1
                    If m_oCentrosCoste.Item(1).UON2 <> "" Then
                        txtCentroCoste.Text = txtCentroCoste.Text & "-" & m_oCentrosCoste.Item(1).UON2
                        If m_oCentrosCoste.Item(1).UON3 <> "" Then
                            txtCentroCoste.Text = txtCentroCoste.Text & "-" & m_oCentrosCoste.Item(1).UON3
                        End If
                    End If
                    txtCentroCoste.Text = txtCentroCoste.Text & ")"
                End If
                
                txtCentroCoste.Tag = m_oCentrosCoste.Item(1).COD_SM
                txtCentroCoste.Backcolor = &HC0FFC0
    
                m_sUON1_CC = m_oCentrosCoste.Item(1).UON1
                m_sUON2_CC = m_oCentrosCoste.Item(1).UON2
                m_sUON3_CC = m_oCentrosCoste.Item(1).UON3
                m_sUON4_CC = m_oCentrosCoste.Item(1).UON4
                
                sUON_CC = FormatearUON(m_oCentrosCoste.Item(1))
            End If
        Else
            txtCentroCoste.Tag = ""
            txtCentroCoste.Backcolor = &H8080FF
        End If
        m_bCentroCosteValidado = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtCentroCoste_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub CargarCentroCostePorCentroSM(ByVal sCod As String, Optional ByVal sUsu As String)
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
  If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
        
    If Len(sCod) > 0 Then
        '(Ver txtCentro_Validate de frmSelActivo)
        If Len(sUsu) > 0 Then
            m_oCentrosCoste.CargarCentrosDeCoste sCod, , , , , , sUsu, , , , m_bRPerfUON, lIdPerfil
            If m_oCentrosCoste.Count = 0 Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod, , , , , sUsu, , , , m_bRPerfUON, lIdPerfil
                If m_oCentrosCoste.Count = 0 Then
                    m_oCentrosCoste.CargarCentrosDeCoste , , sCod, , , , sUsu, , , , m_bRPerfUON, lIdPerfil
                    If m_oCentrosCoste.Count = 0 Then
                        m_oCentrosCoste.CargarCentrosDeCoste , , , sCod, , , sUsu, , , , m_bRPerfUON, lIdPerfil
                        If m_oCentrosCoste.Count = 0 Then
                            m_oCentrosCoste.CargarCentrosDeCoste , , , , sCod, , sUsu, , , , m_bRPerfUON, lIdPerfil
                            If m_oCentrosCoste.Count = 0 Then
                                m_oCentrosCoste.CargarCentrosDeCoste , , , , , sCod, sUsu, , , , m_bRPerfUON, lIdPerfil
                            End If
                        End If
                    End If
                End If
            End If
        Else
            m_oCentrosCoste.CargarCentrosDeCoste sCod, , , , , , , , , , m_bRPerfUON, lIdPerfil
            If m_oCentrosCoste.Count = 0 Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod, , , , , , , , , m_bRPerfUON, lIdPerfil
                If m_oCentrosCoste.Count = 0 Then
                    m_oCentrosCoste.CargarCentrosDeCoste , , sCod, , , , , , , , m_bRPerfUON, lIdPerfil
                    If m_oCentrosCoste.Count = 0 Then
                        m_oCentrosCoste.CargarCentrosDeCoste , , , sCod, , , , , , , m_bRPerfUON, lIdPerfil
                        If m_oCentrosCoste.Count = 0 Then
                            m_oCentrosCoste.CargarCentrosDeCoste , , , , sCod, , , , , , m_bRPerfUON, lIdPerfil
                            If m_oCentrosCoste.Count = 0 Then
                                m_oCentrosCoste.CargarCentrosDeCoste , , , , , sCod, , , , , m_bRPerfUON, lIdPerfil
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarCentroCostePorCentroSM", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub CargarCentroCostePorUON(ByVal sCod As String, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, _
        Optional ByVal sUsu As String)
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
        
    If Len(sCod) > 0 Then
        If Len(sUsu) > 0 Then
            If sUON1 <> "" And sUON2 <> "" And sUON3 <> "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sUON3, sCod, , sUsu, , , , m_bRPerfUON, lIdPerfil
            ElseIf sUON1 <> "" And sUON2 <> "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sCod, , , sUsu, , , , m_bRPerfUON, lIdPerfil
            ElseIf sUON1 <> "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sCod, , , , sUsu, , , , m_bRPerfUON, lIdPerfil
            ElseIf sUON1 = "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod, , , , , sUsu, , , , m_bRPerfUON, lIdPerfil
            End If
        Else
            If sUON1 <> "" And sUON2 <> "" And sUON3 <> "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sUON3, sCod, , , , , , m_bRPerfUON, lIdPerfil
            ElseIf sUON1 <> "" And sUON2 <> "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sCod, , , , , , , m_bRPerfUON, lIdPerfil
            ElseIf sUON1 <> "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sCod, , , , , , , , m_bRPerfUON, lIdPerfil
            ElseIf sUON1 = "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod, , , , , , , , , m_bRPerfUON, lIdPerfil
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarCentroCostePorUON", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub txtContrato_Change(Index As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bPres5Validado = False
    lstContrato.Visible = False
    txtContrato(Index).Backcolor = &H80000005
    
    m_asPres5UON(Index) = ""
    m_aPres5(Index).sPres5_Nivel0 = ""
    m_aPres5(Index).sPres5_Nivel1 = ""
    m_aPres5(Index).sPres5_Nivel2 = ""
    m_aPres5(Index).sPres5_Nivel3 = ""
    m_aPres5(Index).sPres5_Nivel4 = ""
    
    Dim pos As Integer
    pos = InStr(1, txtContrato(Index).Text, " - ", vbTextCompare)
    If pos > 0 Then
        txtContrato(Index).Tag = Trim(Mid(txtContrato(Index).Text, 1, pos))
    Else
        txtContrato(Index).Tag = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtContrato_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtContrato_KeyPress(Index As Integer, KeyAscii As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        txtContrato_Validate Index, True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtContrato_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub txtContrato_Validate(Index As Integer, Cancel As Boolean)
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bPres5Validado And txtContrato(Index).Text <> "" Then
        Dim sCod As String
        If Len(txtContrato(Index).Tag) > 0 Then
            sCod = CStr(txtContrato(Index).Tag)
        Else
            sCod = txtContrato(Index).Text
        End If
        
        If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
        
        If sUON1 <> "" Or sUON2 <> "" Or sUON3 <> "" Then
            m_oContratosPres.CargarTodosLosContratos m_asPres5Niv0(Index), m_aiNivelImp(Index), IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, sUON1, sUON2, sUON3, , sCod, , , m_bRPerfUON, lIdPerfil
        Else
            If m_bRuo Then
                m_oContratosPres.CargarTodosLosContratos m_asPres5Niv0(Index), m_aiNivelImp(Index), IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , sCod, , , m_bRPerfUON, lIdPerfil
            Else
                If oUsuarioSummit.Pyme <> 0 Then
                    m_oContratosPres.CargarTodosLosContratos m_asPres5Niv0(Index), m_aiNivelImp(Index), IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, , , , sCod, , , m_bRPerfUON, lIdPerfil
                Else
                    m_oContratosPres.CargarTodosLosContratos m_asPres5Niv0(Index), m_aiNivelImp(Index), IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, , , , , sCod, , , m_bRPerfUON, lIdPerfil
                End If
            End If
        End If
        
        If m_oContratosPres.Count > 0 Then
            If m_oContratosPres.Count > 1 Then
                lstContrato.clear
                Dim sItemContrato As String
                Dim i As Integer
                For i = 1 To m_oContratosPres.Count
                    sItemContrato = m_oContratosPres.Item(i).Cod & " - " & m_oContratosPres.Item(i).Den
                    lstContrato.AddItem sItemContrato
                Next
                lstContrato.Left = txtContrato(Index).Left
                lstContrato.Top = txtContrato(Index).Top + txtContrato(Index).Height
                lstContrato.Height = 570
                lstContrato.Visible = True
                If Me.Visible Then lstContrato.SetFocus
            ElseIf m_oContratosPres.Count = 1 Then
                txtContrato(Index).Text = m_oContratosPres.Item(1).Cod & " - " & m_oContratosPres.Item(1).Den
                txtContrato(Index).Tag = m_oContratosPres.Item(1).Cod
                txtContrato(Index).Backcolor = &HC0FFC0
                
                m_asPres5UON(Index) = FormatearPres5o(m_asPres5Niv0(Index), m_oContratosPres.Item(1))
                m_aPres5(Index).sPres5_Nivel0 = m_asPres5Niv0(Index)
                m_aPres5(Index).sPres5_Nivel1 = m_oContratosPres.Item(1).Pres1
                m_aPres5(Index).sPres5_Nivel2 = m_oContratosPres.Item(1).Pres2
                m_aPres5(Index).sPres5_Nivel3 = m_oContratosPres.Item(1).Pres3
                m_aPres5(Index).sPres5_Nivel4 = m_oContratosPres.Item(1).Pres4
            End If
        Else
            txtContrato(Index).Tag = ""
            txtContrato(Index).Backcolor = &H8080FF
        End If
        m_bPres5Validado = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtContrato_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtDenArticulo_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bEditandoArticuloDen = True
    If Not m_bEditandoArticulo And Not m_bEditandoArticuloConDropDown Then
        sdbcArticulo.Value = ""
    End If
    m_bEditandoArticuloDen = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtDenArticulo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtDescr_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bCargandoFiltro = True Then g_oConfVisorSol.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtDescr_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecDesde_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bCargandoFiltro = True Then g_oConfVisorSol.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtFecDesde_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub txtFecHasta_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bCargandoFiltro = True Then g_oConfVisorSol.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtFecHasta_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtId_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
  If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bCargandoFiltro = True Then g_oConfVisorSol.HayCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtId_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga la configuraci�n de los campos,orden y filtro para el usuario de la aplicaci�n
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarConfiguracionVisorSol()
    Dim oConfVisoresSol As CConfVistasVisorSol
    Dim ConfigVisorSol As TipoConfigVisorSol
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oConfVisoresSol = oFSGSRaiz.Generar_CConfVistasVisorSol
    
    oConfVisoresSol.CargarConfVisorSol oUsuarioSummit.Cod
    
    If oConfVisoresSol.Item(1) Is Nothing Then
        'Carga la configuraci�n por defecto
        ConfigVisorSol.dblTipoWidth = 2294.92
        ConfigVisorSol.dblAltaWidth = 945
        ConfigVisorSol.dblNecesidadWidth = 945
        ConfigVisorSol.dblIdentWidth = 600
        ConfigVisorSol.dblDenominacionWidth = 1650
        ConfigVisorSol.dblImporteWidth = 1005
        ConfigVisorSol.dblPeticionarioWidth = 2940
        ConfigVisorSol.dblUONWidth = 1600
        ConfigVisorSol.dblEstadoWidth = 1100
        ConfigVisorSol.dblAsignadoAWidth = 1600
        ConfigVisorSol.dblDepartamentoWidth = 1400
        ConfigVisorSol.dblSinProcesoWidth = 300

        ConfigVisorSol.vTipo = Null
        ConfigVisorSol.vIdentificador = Null
        ConfigVisorSol.vDescripcion = Null
        ConfigVisorSol.vPeticionario = Null
        ConfigVisorSol.vDesde = Null
        ConfigVisorSol.vHasta = Null
        ConfigVisorSol.vUON1 = Null
        ConfigVisorSol.vUON2 = Null
        ConfigVisorSol.vUON3 = Null
        ConfigVisorSol.vEnCursoAprobacion = False
        ConfigVisorSol.vPendientes = False
        ConfigVisorSol.vAprobadas = False
        ConfigVisorSol.vRechazadas = False
        ConfigVisorSol.vAnuladas = False
        ConfigVisorSol.vCerradas = False
        ConfigVisorSol.sGMN1 = ""
        ConfigVisorSol.sGMN2 = ""
        ConfigVisorSol.sGMN3 = ""
        ConfigVisorSol.sGMN4 = ""
        ConfigVisorSol.sCodArticulo = ""
        ConfigVisorSol.sDenArticulo = ""
        ConfigVisorSol.dImporteDesde = 0
        ConfigVisorSol.dImporteHasta = 0
        ConfigVisorSol.lEmpresa = 0
        ConfigVisorSol.sDepartamento = ""
        ConfigVisorSol.sComprador = ""
        ConfigVisorSol.sUON1_CC = ""
        ConfigVisorSol.sUON2_CC = ""
        ConfigVisorSol.sUON3_CC = ""
        ConfigVisorSol.sUON4_CC = ""
        ConfigVisorSol.Proveedor = ""
        ConfigVisorSol.bAlertaSinProceso = False
        ConfigVisorSol.bAlertaSinPedido = False
        ConfigVisorSol.bAlertaIntegracion = False
        
        oConfVisoresSol.Add oUsuarioSummit.Cod, ConfigVisorSol
    End If
    
    Set g_oConfVisorSol = oConfVisoresSol.Item(1)
    
    Set oConfVisoresSol = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarConfiguracionVisorSol", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub CargarConfiguracionVisorSolPP()
    'Carga la configuraci�n de los campos,orden y filtro para el usuario de la aplicaci�n
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set g_oConfVisorSolPPs = oFSGSRaiz.Generar_CConfVistasVisorSolPP
    g_oConfVisorSolPPs.CargarConfVisorSolPP oUsuarioSummit.Cod, True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarConfiguracionVisorSolPP", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Comprueba que los filtros sean correctos y los aplica
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub ComprobarYAplicarFiltros()
    Dim bMostrarBusquedaAvanzada As Boolean
    Dim bHayfiltro As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bMostrarBusquedaAvanzada = False
    m_bCargandoFiltro = True
    bHayfiltro = False
    
    '******************** Filtro Tipo de solicitud ***************************
    If Not IsNull(g_oConfVisorSol.Tipo) Then
        CargarTiposSolicitud
        sdbcTipoSolicit_PositionListId (g_oConfVisorSol.Tipo)
        sdbcTipoSolicit.Text = g_oConfVisorSol.Tipo
        bHayfiltro = True
    Else
        sdbcTipoSolicit.Text = ""
    End If
    
    '******************** Filtro Identificador ***************************
    txtId.Text = NullToStr(g_oConfVisorSol.Identificador)
    If txtId.Text <> "" Then bHayfiltro = True
    
    '******************** Filtro Descripci�n ***************************
    txtDescr.Text = NullToStr(g_oConfVisorSol.Descripcion)
    If txtDescr.Text <> "" Then bHayfiltro = True
    '******************** Filtro Desde  ***************************
    txtFecDesde.Text = NullToStr(g_oConfVisorSol.Desde)
    If txtFecDesde.Text <> "" Then bHayfiltro = True
    '******************** Filtro Hasta ***************************
    txtFecHasta.Text = NullToStr(g_oConfVisorSol.Hasta)
    If txtFecHasta.Text <> "" Then bHayfiltro = True
    '******************** Filtro Peticionario ***************************
    If Not IsNull(g_oConfVisorSol.Peticionario) Then
        CargarPersonas
        sdbcPeticionario.Text = sdbcPeticionario_PositionListId(g_oConfVisorSol.Peticionario)
        bHayfiltro = True
    Else
        sdbcPeticionario.Text = ""
    End If
                
    '******************** Filtro En curso de aprobaci�n ***************************
    chkEstado(5).Value = g_oConfVisorSol.EnCursoAprobacion
    
    '******************** Filtro Pendientes ***************************
    chkEstado(0).Value = g_oConfVisorSol.Pendientes
    
    '******************** Filtro Aprobadas ***************************
    chkEstado(1).Value = g_oConfVisorSol.Aprobadas
        
    '******************** Filtro Rechazadas ***************************
    chkEstado(2).Value = g_oConfVisorSol.Rechazadas
    
    '******************** Filtro Anuladas ***************************
    chkEstado(3).Value = g_oConfVisorSol.Anuladas
    
    '******************** Filtro Cerradas ***************************
    chkEstado(4).Value = g_oConfVisorSol.Cerradas
      
    '************************ FILTRO DE UNIDAD ORGANIZATIVA ****************:
    If m_bRuo = False Then
        sUON1 = NullToStr(g_oConfVisorSol.UON1)
        sUON2 = NullToStr(g_oConfVisorSol.UON2)
        sUON3 = NullToStr(g_oConfVisorSol.UON3)
        PonerDenominaciondeUO sUON1, sUON2, sUON3
        If sUON1 <> "" Then bHayfiltro = True
    End If
    
    '******************** Materiales ***************************
    m_sGMN1Cod = NullToStr(g_oConfVisorSol.GMN1)
    m_sGMN2Cod = NullToStr(g_oConfVisorSol.GMN2)
    m_sGMN3Cod = NullToStr(g_oConfVisorSol.GMN3)
    m_sGMN4Cod = NullToStr(g_oConfVisorSol.GMN4)
    PonerDenominaciondeMaterial
    If txtMateriales.Text <> "" Then bHayfiltro = True
    If m_sGMN1Cod <> "" Then
        bMostrarBusquedaAvanzada = True
    End If
    
    '******************** C�digo Art�culo ***************************
    If g_oConfVisorSol.CodArticulo <> "" Then
        CargarArticulos
        sdbcArticulo.Text = sdbcArticulo_PositionListId(g_oConfVisorSol.CodArticulo)
        bMostrarBusquedaAvanzada = True
        bHayfiltro = True
    Else
        sdbcArticulo.Text = ""
    End If
    
    '******************** Denominaci�n Art�culo ***************************
    If NullToStr(g_oConfVisorSol.DenArticulo) <> "" Then
        bMostrarBusquedaAvanzada = True
        bHayfiltro = True
    End If
    txtDenArticulo.Text = NullToStr(g_oConfVisorSol.DenArticulo)
    
    '******************** C�digo Proveedor ***************************
    If g_oConfVisorSol.Proveedor <> "" Then
        sdbcProveedor.Text = g_oConfVisorSol.Proveedor
        sdbcProveedor_Validate (False)
        bMostrarBusquedaAvanzada = True
        bHayfiltro = True
    Else
        sdbcProveedor.Text = ""
    End If
    
    '******************** Importe Desde  ***************************
    If NullToStr(g_oConfVisorSol.ImporteDesde) <> "0" Then
        txtImporteDesde.Text = NullToStr(g_oConfVisorSol.ImporteDesde)
        bMostrarBusquedaAvanzada = True
        bHayfiltro = True
    Else
        txtImporteDesde.Text = ""
    End If
    
    '******************** Importe Hasta ***************************
    If NullToStr(g_oConfVisorSol.ImporteHasta) <> "0" Then
        txtImporteHasta.Text = NullToStr(g_oConfVisorSol.ImporteHasta)
        bMostrarBusquedaAvanzada = True
        bHayfiltro = True
    Else
        txtImporteHasta.Text = ""
    End If
    
    '******************** Empresa ***************************
    If g_oConfVisorSol.Empresa <> 0 Then
        CargarEmpresas
        sdbcEmpresa.Text = sdbcEmpresa_PositionListId(g_oConfVisorSol.Empresa)
        bMostrarBusquedaAvanzada = True
        bHayfiltro = True
    Else
        sdbcEmpresa.Text = ""
    End If
    
    '******************** Departamento ***************************
    If g_oConfVisorSol.Departamento <> "" Then
        CargarDepartamentos
        sdbcDepartamento.Text = sdbcDepartamento_PositionListId(g_oConfVisorSol.Departamento)
        bMostrarBusquedaAvanzada = True
        bHayfiltro = True
    Else
        sdbcDepartamento.Text = ""
    End If
    
    '******************** Comprador ***************************
    If g_oConfVisorSol.comprador <> "" Then
        CargarCompradores
        sdbcComprador.Text = sdbcComprador_PositionListId(g_oConfVisorSol.comprador)
        bMostrarBusquedaAvanzada = True
        bHayfiltro = True
    Else
        sdbcComprador.Text = ""
    End If
    
    If g_oConfVisorSol.UON1_CC <> "" Then
        m_sUON1_CC = g_oConfVisorSol.UON1_CC
        sUON_CC = m_sUON1_CC
        If g_oConfVisorSol.UON2_CC <> "" Then
            m_sUON2_CC = g_oConfVisorSol.UON2_CC
            sUON_CC = sUON_CC & "#" & m_sUON2_CC
            If g_oConfVisorSol.UON3_CC <> "" Then
                m_sUON3_CC = g_oConfVisorSol.UON3_CC
                sUON_CC = sUON_CC & "#" & m_sUON3_CC
                If g_oConfVisorSol.UON4_CC <> "" Then
                    m_sUON4_CC = g_oConfVisorSol.UON4_CC
                    sUON_CC = sUON_CC & "#" & m_sUON4_CC
                End If
            End If
        End If
        bMostrarBusquedaAvanzada = True
        bHayfiltro = True
    End If
    PonerDenominacionCentroCoste m_sUON1_CC, m_sUON2_CC, m_sUON3_CC, m_sUON4_CC

    If Not g_oConfVisorSolPPs Is Nothing Then
        If g_oConfVisorSolPPs.Count > 0 Then
            bHayfiltro = True
            If g_oParametrosSM.Count > 1 Then
                
                Dim i As Long
                Dim Row As Long
                Dim vbm As Variant
                For i = 1 To g_oConfVisorSolPPs.Count
                    Row = sdbgPartidas.Rows - 1
                    While Row >= 0
                        vbm = sdbgPartidas.AddItemBookmark(Row)
                        sdbgPartidas.Bookmark = vbm
                        If sdbgPartidas.Columns("COD").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel0 Then
                            If Len(g_oConfVisorSolPPs.Item(i).Pres5_Nivel1) > 0 Then
                                sdbgPartidas.Columns("PRES5NIV1").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel1
                                sdbgPartidas.Columns("TAG").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel1
                                If Len(g_oConfVisorSolPPs.Item(i).Pres5_Nivel2) > 0 Then
                                    sdbgPartidas.Columns("PRES5NIV2").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel2
                                    sdbgPartidas.Columns("TAG").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel2
                                    If Len(g_oConfVisorSolPPs.Item(i).Pres5_Nivel3) > 0 Then
                                        sdbgPartidas.Columns("PRES5NIV3").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel3
                                        sdbgPartidas.Columns("TAG").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel3
                                        If Len(g_oConfVisorSolPPs.Item(i).Pres5_Nivel4) > 0 Then
                                            sdbgPartidas.Columns("PRES5NIV4").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel4
                                            sdbgPartidas.Columns("TAG").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel4
                                        End If
                                    End If
                                End If
                            End If
                            If Len(sdbgPartidas.Columns("TAG").Value) > 0 Then
                                sdbgPartidas.Columns("VALOR").Value = sdbgPartidas.Columns("TAG").Value & " - " & m_oContratosPres.DevolverDenominacion(g_oConfVisorSolPPs.Item(i).Pres5_Nivel0, g_oConfVisorSolPPs.Item(i).Pres5_Nivel1, g_oConfVisorSolPPs.Item(i).Pres5_Nivel2, g_oConfVisorSolPPs.Item(i).Pres5_Nivel3, g_oConfVisorSolPPs.Item(i).Pres5_Nivel4)
                                sdbgPartidas.Columns("PRES5UON").Value = FormatearPres5s(g_oConfVisorSolPPs.Item(i).Pres5_Nivel0, g_oConfVisorSolPPs.Item(i).Pres5_Nivel1, g_oConfVisorSolPPs.Item(i).Pres5_Nivel2, g_oConfVisorSolPPs.Item(i).Pres5_Nivel3, g_oConfVisorSolPPs.Item(i).Pres5_Nivel4)
                                sdbgPartidas.Columns("VALIDADO").Value = 2
                            End If
                        End If
                        Row = Row - 1
                    Wend
                Next
                sdbgPartidas.Update
            Else
                Dim pres5Niv0 As String
                Dim pres5Niv1 As String
                Dim pres5Niv2 As String
                Dim pres5Niv3 As String
                Dim pres5Niv4 As String
                
                pres5Niv0 = g_oConfVisorSolPPs.Item(1).Pres5_Nivel0
                If Len(g_oConfVisorSolPPs.Item(1).Pres5_Nivel1) > 0 Then
                    pres5Niv1 = g_oConfVisorSolPPs.Item(1).Pres5_Nivel1
                    txtContrato(0).Tag = g_oConfVisorSolPPs.Item(1).Pres5_Nivel1
                    If Len(g_oConfVisorSolPPs.Item(1).Pres5_Nivel2) > 0 Then
                        pres5Niv2 = g_oConfVisorSolPPs.Item(1).Pres5_Nivel2
                        txtContrato(0).Tag = g_oConfVisorSolPPs.Item(1).Pres5_Nivel2
                        If Len(g_oConfVisorSolPPs.Item(1).Pres5_Nivel3) > 0 Then
                            pres5Niv3 = g_oConfVisorSolPPs.Item(1).Pres5_Nivel3
                            txtContrato(0).Tag = g_oConfVisorSolPPs.Item(1).Pres5_Nivel3
                            If Len(g_oConfVisorSolPPs.Item(1).Pres5_Nivel4) > 0 Then
                                pres5Niv4 = g_oConfVisorSolPPs.Item(1).Pres5_Nivel4
                                txtContrato(0).Tag = g_oConfVisorSolPPs.Item(1).Pres5_Nivel4
                            End If
                        End If
                    End If
                End If
                
                txtContrato(0).Text = txtContrato(0).Tag & " - " & m_oContratosPres.DevolverDenominacion(pres5Niv0, pres5Niv1, pres5Niv2, pres5Niv3, pres5Niv4)
            
                m_aPres5(0).sPres5_Nivel0 = pres5Niv0
                m_aPres5(0).sPres5_Nivel1 = pres5Niv1
                m_aPres5(0).sPres5_Nivel2 = pres5Niv2
                m_aPres5(0).sPres5_Nivel3 = pres5Niv3
                m_aPres5(0).sPres5_Nivel4 = pres5Niv4
                          
                m_asPres5UON(0) = FormatearPres5s(g_oConfVisorSolPPs.Item(1).Pres5_Nivel0, g_oConfVisorSolPPs.Item(1).Pres5_Nivel1, g_oConfVisorSolPPs.Item(1).Pres5_Nivel2, g_oConfVisorSolPPs.Item(1).Pres5_Nivel3, g_oConfVisorSolPPs.Item(1).Pres5_Nivel4)
                m_bPres5Validado = True
            End If
            
            For i = g_oConfVisorSolPPs.Count To 1 Step -1
                g_oConfVisorSolPPs.Remove i
            Next
            
            bMostrarBusquedaAvanzada = True
        End If
    End If
    
    If bMostrarBusquedaAvanzada Then MostrarBusquedaAvanzada

    '******************** Configurar alerta  ***************************
    chkConfAlertaSinProceso.Value = BooleanToSQLBinary(g_oConfVisorSol.AlertaSinProceso)
    chkConfAlertaSinPedido.Value = BooleanToSQLBinary(g_oConfVisorSol.AlertaSinPedido)
    If m_bHayIntegracionSalida Then
        chkConfAlertaErrIntegracion.Value = BooleanToSQLBinary(g_oConfVisorSol.alertaIntegracion)
    Else
        chkConfAlertaErrIntegracion.Value = 0
    End If
    m_bCargandoFiltro = True
    g_oConfVisorSol.HayCambios = True
    If Not g_oConfVisorSolPPs Is Nothing Then
        g_oConfVisorSolPPs.HayCambios = True
    End If
    
    'Si no hay filtro pongo una fecha de una semana antes para que no de tiemout
    If Not bHayfiltro Then
        txtFecDesde.Text = DateAdd("d", -7, Date)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "ComprobarYAplicarFiltros", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Redimensionar las columnas del Grid
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub RedimensionarGrid()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgSolicitudes.Columns("TIPO").Width = g_oConfVisorSol.TipoWidth
    sdbgSolicitudes.Columns("ALTA").Width = g_oConfVisorSol.AltaWidth
    sdbgSolicitudes.Columns("NECESIDAD").Width = g_oConfVisorSol.NecesidadWidth
    sdbgSolicitudes.Columns("ID2").Width = g_oConfVisorSol.IdentWidth
    sdbgSolicitudes.Columns("DESCR").Width = g_oConfVisorSol.DenominacionWidth
    sdbgSolicitudes.Columns("IMPORTE").Width = g_oConfVisorSol.ImporteWidth
    sdbgSolicitudes.Columns("PET").Width = g_oConfVisorSol.PeticionarioWidth
    sdbgSolicitudes.Columns("UON").Width = g_oConfVisorSol.UONWidth
    sdbgSolicitudes.Columns("ESTADO").Width = g_oConfVisorSol.EstadoWidth
    sdbgSolicitudes.Columns("COMP").Width = g_oConfVisorSol.AsignadoAWidth
    sdbgSolicitudes.Columns("DEPT").Width = g_oConfVisorSol.DepartamentoWidth
    sdbgSolicitudes.Columns("ALERTA_PROCESO").Width = g_oConfVisorSol.SinProcesoWidth
    sdbgSolicitudes.Columns("ALERTA_PEDIDO").Width = g_oConfVisorSol.SinPedidoWidth
    sdbgSolicitudes.Columns("ALERTA_INTEGRACION").Width = g_oConfVisorSol.SinIntegracionWidth
    sdbgSolicitudes.Columns("ALERTA_MONITORIZACION").Width = 300
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "RedimensionarGrid", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub ActivarMonitorizacion()
''' <summary>
''' Llama a la pantalla frmSolicitudGestionar, que es la que activa la monitorizaci�n.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Se le llama desde el MDI (mnuPopUpSolicitudes_Click); Tiempo m�ximo:0,1</remarks>

    'Llama al formulario para anular la solicitud
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))) Is Nothing Then Exit Sub
    
    frmSolicitudGestionar.g_sOrigen = "frmSolicitudes"
    frmSolicitudGestionar.g_sOrigen2 = "frmSolicitudes"
    frmSolicitudGestionar.g_iAccion = 5
    Set frmSolicitudGestionar.g_oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
    frmSolicitudGestionar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "ActivarMonitorizacion", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Desactiva el flag de seguimiento de solicitudes del visor de solicitudes de GS.</summary>
''' <returns>Nada</returns>
''' <remarks>Se le llama desde el MDI (mnuPopUpSolicitudes_Click); Tiempo m�ximo:0,1</remarks>
''' <revision>LTG 10/12/2012</revision>
Public Sub DesactivarMonitorizacion()
Dim teserror As TipoErrorSummit
Dim oSolicitud As CInstancia
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
        
    Set oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
    teserror = oSolicitud.Monitorizar(False, basOptimizacion.gCodPersonaUsuario)

    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
    Else
        sdbgSolicitudes.Columns("MONITORIZ").Value = False
        sdbgSolicitudes.Update
        Dim todasSinMonitorizar As Boolean
        Dim i As Integer
        Dim vbm1 As Variant
        todasSinMonitorizar = True
        For i = 0 To sdbgSolicitudes.Rows - 1
            vbm1 = sdbgSolicitudes.AddItemBookmark(i)
            If sdbgSolicitudes.Columns("MONITORIZ").CellValue(vbm1) = True Then
                todasSinMonitorizar = False
                Exit For
            End If
        Next
        If todasSinMonitorizar Then CargarGridSolicitudes gParametrosInstalacion.giOrdenVisorSol
        sdbgSolicitudes.Update
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "DesactivarMonitorizacion", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
'' evento que se lanza cuando se pierde el foco del campo identificador
'' comprueba si el usuario ha introducido un valor num�rico, sino le muestra un aviso.
''' </summary>
Private Sub txtId_Validate(Cancel As Boolean)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtId.Text = "" Then Exit Sub
    If Not IsInteger(txtId.Text) Then
        oMensajes.NoValido sId
        Cancel = True
        Me.txtId.Text = ""
        Exit Sub
    ElseIf InStr(1, txtId.Text, ".") > 0 Or InStr(1, txtId.Text, ",") Then
        oMensajes.NoValido sId
        Cancel = True
        Me.txtId.Text = ""
        Exit Sub
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtId_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
'' evento que se lanza cuando se pierde el foco del campo importe desde.
'' Formatea el campo a formato de puntos y comas.
''' </summary>
Private Sub txtImporteDesde_LostFocus()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If IsNumeric(txtImporteDesde.Text) Then
        FormatearImporteDesde
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtImporteDesde_LostFocus", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
'' Formatea el campo a formato de puntos y comas.
''' </summary>
Private Sub FormatearImporteDesde()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtImporteDesde.Text = Format(txtImporteDesde.Text, "#,##0.##")
    If Not IsNumeric(Right(txtImporteDesde.Text, 1)) Then
        txtImporteDesde.Text = Left(txtImporteDesde.Text, Len(txtImporteDesde.Text) - 1)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "FormatearImporteDesde", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
'' evento que se lanza cuando se pierde el foco del campo importe hasta.
'' Formatea el campo a formato de puntos y comas.
''' </summary>
Private Sub txtImporteHasta_LostFocus()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If IsNumeric(txtImporteHasta.Text) Then
        FormatearImporteHasta
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtImporteHasta_LostFocus", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
'' Formatea el campo a formato de puntos y comas.
''' </summary>
Private Sub FormatearImporteHasta()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtImporteHasta.Text = Format(txtImporteHasta.Text, "#,##0.##")
    If Not IsNumeric(Right(txtImporteHasta.Text, 1)) Then
        txtImporteHasta.Text = Left(txtImporteHasta.Text, Len(txtImporteHasta.Text) - 1)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "FormatearImporteHasta", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub txtMateriales_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         cmdLimpiarMateriales_Click
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "txtMateriales_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Public Sub PonerMatSeleccionado()
    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4
    Dim sNuevoCod1 As String
    Dim sNuevoCod2 As String
    Dim sNuevoCod3 As String
    Dim sNuevoCod4 As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    Set m_oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sNuevoCod1 = oGMN1Seleccionado.Cod
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        sNuevoCod2 = oGMN2Seleccionado.Cod
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        sNuevoCod3 = oGMN3Seleccionado.Cod
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        sNuevoCod4 = oGMN4Seleccionado.Cod
    End If
    If m_sGMN1Cod = sNuevoCod1 And m_sGMN2Cod = sNuevoCod2 And m_sGMN3Cod = sNuevoCod3 And m_sGMN4Cod = sNuevoCod4 Then
        Exit Sub
    End If
    
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
        
    If Not oGMN1Seleccionado Is Nothing Then
        m_sGMN1Cod = oGMN1Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & oGMN1Seleccionado.Den
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        m_sGMN2Cod = oGMN2Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & oGMN2Seleccionado.Den
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        m_sGMN3Cod = oGMN3Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & oGMN3Seleccionado.Den
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        m_sGMN4Cod = oGMN4Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & m_sGMN4Cod & " - " & oGMN4Seleccionado.Den
    End If
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "PonerMatSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub PonerDenominaciondeMaterial()
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4

    Dim strCadena As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_sGMN4Cod <> "" Then
        Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
        strCadena = oGMN4.ObtenerDenGMN4(m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, m_sGMN4Cod, gParametrosInstalacion.gIdioma)
        If strCadena = "" Then
            txtMateriales.Text = ""
        Else
            txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & m_sGMN4Cod & " - " & strCadena
        End If
    ElseIf m_sGMN3Cod <> "" Then
        Set oGMN3 = oFSGSRaiz.generar_CGrupoMatNivel3
        strCadena = oGMN3.ObtenerDenGMN3(m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, gParametrosInstalacion.gIdioma)
        If strCadena = "" Then
            txtMateriales.Text = ""
        Else
            txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & strCadena
        End If
    ElseIf m_sGMN2Cod <> "" Then
        Set oGMN2 = oFSGSRaiz.generar_CGrupoMatNivel2
        strCadena = oGMN2.ObtenerDenGMN2(m_sGMN1Cod, m_sGMN2Cod, gParametrosInstalacion.gIdioma)
        If strCadena = "" Then
            txtMateriales.Text = ""
        Else
            txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & strCadena
        End If
    ElseIf m_sGMN1Cod <> "" Then
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
        strCadena = oGMN1.ObtenerDenGMN1(m_sGMN1Cod, gParametrosInstalacion.gIdioma)
        If strCadena = "" Then
            txtMateriales.Text = ""
        Else
            txtMateriales.Text = m_sGMN1Cod & " - " & strCadena
        End If
    End If
    
    Set oGMN1 = Nothing
    Set oGMN2 = Nothing
    Set oGMN3 = Nothing
    Set oGMN4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "PonerDenominaciondeMaterial", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub PonerDenominacionCentroCoste(ByVal sUON1 As String, ByVal sUON2 As String, ByVal sUON3 As String, ByVal sUON4 As String)
    Dim oCentroCoste As CCentroCoste
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oCentroCoste = oFSGSRaiz.Generar_CCentroCoste
    Dim sDen As String
    sDen = oCentroCoste.DevolverDenominacion(sUON1, sUON2, sUON3, sUON4)
    If sUON4 <> "" Then
        txtCentroCoste.Text = sUON4 & " - " & sDen & " (" & sUON1 & "-" & sUON2 & "-" & sUON3 & ")"
    ElseIf sUON3 <> "" Then
        txtCentroCoste.Text = sUON3 & " - " & sDen & " (" & sUON1 & "-" & sUON2 & ")"
    ElseIf sUON2 <> "" Then
        txtCentroCoste.Text = sUON2 & " - " & sDen & " (" & sUON1 & ")"
    ElseIf sUON1 <> "" Then
        txtCentroCoste.Text = sUON1 & " - " & sDen
    End If
    Set oCentroCoste = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "PonerDenominacionCentroCoste", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


''' <summary>
''' visualiza om la grid de partidas o los texbox en funci�n de si tenemos solo 1 partida o mas de una.
''' </summary>
''' <remarks>Llamada desde: form_load(); Tiempo m�ximo: 0,1</remarks>
Private Sub CargarPartidas2()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oParametrosSM Is Nothing Then
        If g_oParametrosSM.Count > 0 Then
            If g_oParametrosSM.Count > 1 Then
                Dim pres As CParametroSM
                
                For Each pres In g_oParametrosSM
    
                    sdbgPartidas.AddItem pres.PRES5 & Chr(m_lSeparador) & _
                                        pres.NomNivelImputacion & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        pres.impnivel & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        0 & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        ""
                Next
    
                lblContrato(0).Visible = False
                txtContrato(0).Visible = False
                cmdBuscarContrato(0).Visible = False
                cmdLimpiarContrato(0).Visible = False
                sdbgPartidas.Visible = True
    
                sdbgPartidas.Top = lblContrato(0).Top
                sdbgPartidas.Left = lblContrato(0).Left
                sdbgPartidas.Width = (cmdBuscarContrato(0).Left - lblContrato(0).Left) + cmdBuscarContrato(0).Width
    
                Dim cota As Integer
                cota = 5
                If g_oParametrosSM.Count > 2 And g_oParametrosSM.Count < cota Then
                    sdbgPartidas.Height = sdbgPartidas.Rows * (sdbgPartidas.RowHeight + 10)
                    m_sIncrementoAltura = (sdbgPartidas.Rows - 3) * (sdbgPartidas.RowHeight + 10) + 550
                    picBusquedaAvanzada.Height = picBusquedaAvanzada.Height + m_sIncrementoAltura
                    sdbgPartidas.Columns(1).Width = sdbgPartidas.Width * 0.21
                    sdbgPartidas.Columns(2).Width = sdbgPartidas.Width * 0.79
                Else
                    sdbgPartidas.Height = cota * (sdbgPartidas.RowHeight + 10)
                    m_sIncrementoAltura = (cota - 3) * (sdbgPartidas.RowHeight + 10) + 550
                    picBusquedaAvanzada.Height = picBusquedaAvanzada.Height + m_sIncrementoAltura
                    sdbgPartidas.Columns(1).Width = sdbgPartidas.Width * 0.21
                    sdbgPartidas.Columns(2).Width = sdbgPartidas.Width * 0.79
                End If
    
            Else
                lblContrato(0).caption = g_oParametrosSM.Item(1).NomNivelImputacion & ":"
                m_asPres5Niv0(0) = g_oParametrosSM.Item(1).PRES5
                m_aiNivelImp(0) = g_oParametrosSM.Item(1).impnivel
            End If
                    
        End If
    Else
        lblContrato(0).Visible = False
        txtContrato(0).Visible = False
        cmdBuscarContrato(0).Visible = False
        cmdLimpiarContrato(0).Visible = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarPartidas2", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' En funcion de si hay integraci�n o no se muestra/oculta en la configuraci�n de alertas lo relativo a integraci�n
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub ComprobarIntegracion()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bHayIntegracionSalida = m_oInstancias.HayIntegracionSentidoSalida()
    chkConfAlertaErrIntegracion.Visible = m_bHayIntegracionSalida
    imgConfAlertaErrIntegracion.Visible = m_bHayIntegracionSalida
    lblConfAlertaErrIntegracion.Visible = m_bHayIntegracionSalida
    
    If Not m_bHayIntegracionSalida Then
        fraConfAlerta.Height = fraConfAlerta.Height - chkConfAlertaErrIntegracion.Height
        cmdOKConfAlerta.Top = cmdOKConfAlerta.Top - chkConfAlertaErrIntegracion.Height
        cmdCancelConfAlerta.Top = cmdOKConfAlerta.Top
    End If
    
    m_bHayIntegracionSolicitudes = True
    If Not m_oInstancias.HayIntegracionActiva Then
        m_bHayIntegracionSolicitudes = False
        picNumSolicitErp.Visible = False
        sdbgSolicitudes.Columns("NUM_SOL_ERP").Visible = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "ComprobarIntegracion", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Establece el proveedor desde la busqueda de proveedor
''' </summary>
''' <remarks>Llamada desde: frmproveBuscar ; Tiempo m�ximo: 0,2</remarks>
Public Sub CargarProveedorConBusqueda()

    Dim oProves As CProveedores

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    
    bRespetarComboProve = True
    sdbcProveedor = oProves.Item(1).Cod
    sdbcProveedor.Columns(0).Value = oProves.Item(1).Cod
    sdbcDenProveedor.Text = oProves.Item(1).Den
    sdbcDenProveedor.Columns(0).Value = oProves.Item(1).Den
    bRespetarComboProve = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarProveedorConBusqueda", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Pues parece q no hace caso a cambios en el propio grid_rowloaded, as� q meto todos los estilos desde el principio.
''' Tampoco hace caso al cambio de imagen, desde aqui ??. Sencillamente en dise�o pongo la imagen.
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarStilos()
    Dim oStyleSetSource As StyleSet
    Dim oStyleSetSourceImage As StyleSet
    Dim oStyleSet As StyleSet
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oStyleSetSource = sdbgSolicitudes.StyleSets("Pendiente")
    For i = 0 To 2
        Select Case i
        Case 0
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaProceso" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaProceso")
        Case 1
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaPedido" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaPedido")
        Case 2
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaIntegracion" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaIntegracion")
        End Select
        
        oStyleSet.Backcolor = oStyleSetSource.Backcolor
        oStyleSet.AlignmentPicture = oStyleSetSourceImage.AlignmentPicture
        oStyleSet.Forecolor = oStyleSetSourceImage.Forecolor
                
        Set oStyleSetSourceImage = Nothing
        Set oStyleSet = Nothing
    Next
    Set oStyleSetSource = Nothing
    ''
    Set oStyleSetSource = sdbgSolicitudes.StyleSets("Aprobada")
    For i = 0 To 2
        Select Case i
        Case 0
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaProceso" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaProceso")
        Case 1
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaPedido" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaPedido")
        Case 2
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaIntegracion" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaIntegracion")
        End Select
        
        oStyleSet.Backcolor = oStyleSetSource.Backcolor
        oStyleSet.AlignmentPicture = oStyleSetSourceImage.AlignmentPicture
        oStyleSet.Forecolor = oStyleSetSourceImage.Forecolor
                
        Set oStyleSetSourceImage = Nothing
        Set oStyleSet = Nothing
    Next
    Set oStyleSetSource = Nothing
    ''
    Set oStyleSetSource = sdbgSolicitudes.StyleSets("Rechazada")
    For i = 0 To 2
        Select Case i
        Case 0
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaProceso" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaProceso")
        Case 1
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaPedido" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaPedido")
        Case 2
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaIntegracion" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaIntegracion")
        End Select
        
        oStyleSet.Backcolor = oStyleSetSource.Backcolor
        oStyleSet.AlignmentPicture = oStyleSetSourceImage.AlignmentPicture
        oStyleSet.Forecolor = oStyleSetSourceImage.Forecolor
                
        Set oStyleSetSourceImage = Nothing
        Set oStyleSet = Nothing
    Next
    Set oStyleSetSource = Nothing
    ''
    Set oStyleSetSource = sdbgSolicitudes.StyleSets("Anulada")
    For i = 0 To 2
        Select Case i
        Case 0
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaProceso" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaProceso")
        Case 1
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaPedido" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaPedido")
        Case 2
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaIntegracion" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaIntegracion")
        End Select
        
        oStyleSet.Backcolor = oStyleSetSource.Backcolor
        oStyleSet.AlignmentPicture = oStyleSetSourceImage.AlignmentPicture
        oStyleSet.Forecolor = oStyleSetSourceImage.Forecolor
                
        Set oStyleSetSourceImage = Nothing
        Set oStyleSet = Nothing
    Next
    Set oStyleSetSource = Nothing
    ''
    Set oStyleSetSource = sdbgSolicitudes.StyleSets("Cerrada")
    For i = 0 To 2
        Select Case i
        Case 0
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaProceso" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaProceso")
        Case 1
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaPedido" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaPedido")
        Case 2
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaIntegracion" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaIntegracion")
        End Select
        
        oStyleSet.Backcolor = oStyleSetSource.Backcolor
        oStyleSet.AlignmentPicture = oStyleSetSourceImage.AlignmentPicture
        oStyleSet.Forecolor = oStyleSetSourceImage.Forecolor
                
        Set oStyleSetSourceImage = Nothing
        Set oStyleSet = Nothing
    Next
    Set oStyleSetSource = Nothing
    ''
    Set oStyleSetSource = sdbgSolicitudes.StyleSets("Curso")
    For i = 0 To 2
        Select Case i
        Case 0
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaProceso" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaProceso")
        Case 1
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaPedido" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaPedido")
        Case 2
            Set oStyleSet = sdbgSolicitudes.StyleSets("alertaIntegracion" & oStyleSetSource.Name)
            Set oStyleSetSourceImage = sdbgSolicitudes.StyleSets("alertaIntegracion")
        End Select
        
        oStyleSet.Backcolor = oStyleSetSource.Backcolor
        oStyleSet.AlignmentPicture = oStyleSetSourceImage.AlignmentPicture
        oStyleSet.Forecolor = oStyleSetSourceImage.Forecolor
                
        Set oStyleSetSourceImage = Nothing
        Set oStyleSet = Nothing
    Next
    Set oStyleSetSource = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudes", "CargarStilos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
