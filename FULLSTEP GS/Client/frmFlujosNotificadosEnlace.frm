VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFlujosNotificadosEnlace 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DNotificados"
   ClientHeight    =   5070
   ClientLeft      =   45
   ClientTop       =   225
   ClientWidth     =   11370
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFlujosNotificadosEnlace.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5070
   ScaleWidth      =   11370
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin SSDataWidgets_B.SSDBDropDown sdbddNotificados 
      Height          =   555
      Left            =   2880
      TabIndex        =   1
      Top             =   720
      Width           =   7860
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   13864
      Columns(1).Caption=   "NOTIFICADO"
      Columns(1).Name =   "NOTIFICADO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   13864
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTiposNotificado 
      Height          =   555
      Left            =   360
      TabIndex        =   0
      Top             =   720
      Width           =   2500
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   4419
      Columns(1).Caption=   "TIPO"
      Columns(1).Name =   "TIPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4410
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin VB.CommandButton cmdEliminarNotificado 
      Height          =   312
      Left            =   10860
      Picture         =   "frmFlujosNotificadosEnlace.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   480
      Width           =   432
   End
   Begin VB.CommandButton cmdAnyadirNotificado 
      Height          =   312
      Left            =   10860
      Picture         =   "frmFlujosNotificadosEnlace.frx":0D44
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   120
      Width           =   432
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgNotificados 
      Height          =   3720
      Left            =   60
      TabIndex        =   4
      Top             =   60
      Width           =   10695
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   7
      stylesets.count =   6
      stylesets(0).Name=   "No"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFlujosNotificadosEnlace.frx":0DC6
      stylesets(0).AlignmentPicture=   2
      stylesets(1).Name=   "S�"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFlujosNotificadosEnlace.frx":0DE2
      stylesets(1).AlignmentPicture=   2
      stylesets(2).Name=   "Gris"
      stylesets(2).BackColor=   -2147483633
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmFlujosNotificadosEnlace.frx":0DFE
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmFlujosNotificadosEnlace.frx":0E1A
      stylesets(4).Name=   "ActiveRow"
      stylesets(4).ForeColor=   16777215
      stylesets(4).BackColor=   8388608
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmFlujosNotificadosEnlace.frx":0E36
      stylesets(5).Name=   "Amarillo"
      stylesets(5).BackColor=   12648447
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmFlujosNotificadosEnlace.frx":0E52
      DividerType     =   2
      BevelColorHighlight=   16777215
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   106
      Columns.Count   =   7
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "TIPO"
      Columns(1).Name =   "TIPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   2
      Columns(1).FieldLen=   256
      Columns(2).Width=   4419
      Columns(2).Caption=   "DTipo de notificado"
      Columns(2).Name =   "TIPO_DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   3
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "COD_NOTIFICADO"
      Columns(3).Name =   "COD_NOTIFICADO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   10319
      Columns(4).Caption=   "DNotificado"
      Columns(4).Name =   "NOTIFICADO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   3
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "FECACT"
      Columns(5).Name =   "FECACT"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   7
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "DConfiguraci�n campos"
      Columns(6).Name =   "CONFCAMPOS"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).Style=   1
      _ExtentX        =   18865
      _ExtentY        =   6562
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgSubject 
      Height          =   975
      Left            =   1680
      TabIndex        =   5
      Top             =   3960
      Width           =   5535
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   3
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD_IDIOMA"
      Columns(0).Name =   "COD_IDIOMA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "DEN_IDIOMA"
      Columns(1).Name =   "DEN_IDIOMA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasForeColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).ForeColor=   16777215
      Columns(1).BackColor=   8421376
      Columns(2).Width=   6509
      Columns(2).Caption=   "VALOR_IDIOMA"
      Columns(2).Name =   "VALOR_IDIOMA"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   50
      _ExtentX        =   9763
      _ExtentY        =   1720
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblSubject 
      BackColor       =   &H00808000&
      Caption         =   "DAsunto del email:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   120
      TabIndex        =   6
      Top             =   3960
      Width           =   1515
   End
End
Attribute VB_Name = "frmFlujosNotificadosEnlace"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public lIdEnlace As Long
Public lIdFlujo As Long
Public m_bModifFlujo As Boolean
Public lIdFormulario As Long
Public m_oEnlace As CEnlace
Public iTipoSolicitud As Integer

Private m_oNotificadoEnEdicion As CNotificadoAccion
Private m_oNotificadoAnyadir As CNotificadoAccion
Private m_oIBAseDatosEnEdicion As IBaseDatos
Private m_oIBaseDatos As IBaseDatos
Private m_oNotificados As CNotificadosAccion

Private m_oSubject As CMultiidiomas

Private m_bModError As Boolean
Private m_bAnyaError As Boolean
Private m_bError As Boolean

Private m_sTipoNotificado As String
Private m_sNotificado As String
Private m_sConfCampos As String
Private m_sMensajesNotificado(1 To 2) As String
Private m_arTipoNotificado(1 To 3) As String
Private m_arNotificadoGenerico(3 To 8) As String
Private m_GridSubjectUpdated As Boolean

Private m_oFormulario As CFormulario
Public g_sOrigen As String
Public lidexpiracion As Long

Private Sub cmdAnyadirNotificado_Click()
'    cmdAnyadirNotificado.Enabled = False
    
    sdbgNotificados.Scroll 0, sdbgNotificados.Rows - sdbgNotificados.Row
    
    If sdbgNotificados.VisibleRows > 0 Then
        
        If sdbgNotificados.VisibleRows >= sdbgNotificados.Rows Then
            If sdbgNotificados.VisibleRows = sdbgNotificados.Rows Then
                sdbgNotificados.Row = sdbgNotificados.Rows - 1
            Else
                sdbgNotificados.Row = sdbgNotificados.Rows
    
            End If
        Else
            sdbgNotificados.Row = sdbgNotificados.Rows - (sdbgNotificados.Rows - sdbgNotificados.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgNotificados.SetFocus
End Sub

Private Sub cmdEliminarNotificado_Click()
    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit

    If sdbgNotificados.IsAddRow Then
        If sdbgNotificados.DataChanged Then
            sdbgNotificados.CancelUpdate
        End If
    Else
        If sdbgNotificados.Rows = 0 Then Exit Sub
        
        If sdbgNotificados.DataChanged Then
            sdbgNotificados.CancelUpdate
        End If
        
        Screen.MousePointer = vbHourglass
        
        If sdbgNotificados.SelBookmarks.Count = 1 Then
            irespuesta = oMensajes.PreguntaEliminar(sdbgNotificados.Columns("NOTIFICADO").Value)
            
            If irespuesta = vbNo Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set m_oNotificadoEnEdicion = m_oNotificados.Item(CStr(sdbgNotificados.Columns("ID").Value))
            
            Set m_oIBAseDatosEnEdicion = m_oNotificadoEnEdicion
            teserror = m_oIBAseDatosEnEdicion.EliminarDeBaseDatos
            
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                If Me.Visible Then sdbgNotificados.SetFocus
                Exit Sub
            Else
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                If g_sOrigen <> "Expiracion" Then frmFlujos.HayCambios
                m_oNotificados.Remove (CStr(sdbgNotificados.Columns("ID").Value))
                If sdbgNotificados.AddItemRowIndex(sdbgNotificados.Bookmark) > -1 Then
                    sdbgNotificados.RemoveItem (sdbgNotificados.AddItemRowIndex(sdbgNotificados.Bookmark))
                Else
                    sdbgNotificados.RemoveItem (0)
                End If
'                If IsEmpty(sdbgNotificados.GetBookmark(0)) Then
'                    sdbgNotificados.Bookmark = sdbgNotificados.GetBookmark(-1)
'                Else
'                    sdbgNotificados.Bookmark = sdbgNotificados.GetBookmark(0)
'                End If
                basSeguridad.RegistrarAccion AccionesSummit.ACCNotificadoAccionElim, "Enlace:" & lIdEnlace & ",ID:" & m_oNotificadoEnEdicion.Id
            End If
            
            Set m_oIBAseDatosEnEdicion = Nothing
            Set m_oNotificadoEnEdicion = Nothing
            
        End If
            
        sdbgNotificados.SelBookmarks.RemoveAll
    
        Screen.MousePointer = vbNormal
    
    End If
    cmdAnyadirNotificado.Enabled = True
End Sub

Private Sub Form_Load()
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
          
    ConfigurarSeguridad '???????????????????
    
    PonerFieldSeparator Me
    
    
    CargarNotificados
    
    CargarSubjects
    
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSNOTIFICADOSENLACE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value '1
        Ador.MoveNext
        m_sTipoNotificado = Ador(0).Value
        m_sMensajesNotificado(1) = Ador(0).Value
        Ador.MoveNext
        m_sNotificado = Ador(0).Value
        m_sMensajesNotificado(2) = Ador(0).Value
        Ador.MoveNext
        cmdAnyadirNotificado.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdEliminarNotificado.ToolTipText = Ador(0).Value '5
        
        Ador.MoveNext
        m_arTipoNotificado(1) = Ador(0).Value
        Ador.MoveNext
        m_arTipoNotificado(2) = Ador(0).Value
        Ador.MoveNext
        m_arTipoNotificado(3) = Ador(0).Value
        
                                        
        Ador.MoveNext
        m_arNotificadoGenerico(3) = Ador(0).Value
        Ador.MoveNext
        m_arNotificadoGenerico(4) = Ador(0).Value '10
        Ador.MoveNext
        m_arNotificadoGenerico(5) = Ador(0).Value
        Ador.MoveNext
        m_arNotificadoGenerico(6) = Ador(0).Value
        Ador.MoveNext
        m_arNotificadoGenerico(7) = Ador(0).Value
        Ador.MoveNext
        m_sConfCampos = Ador(0).Value
        
        Ador.MoveNext
        lblSubject.caption = Ador(0).Value
        Ador.MoveNext
        ' Dejar uno vac�o para textos por defecto de emails
        Ador.MoveNext
        m_arNotificadoGenerico(8) = Ador(0).Value
        
        
        
        Ador.Close
    End If
End Sub

Private Sub ConfigurarSeguridad()
    If m_bModifFlujo Then
        sdbddTiposNotificado.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgNotificados.Columns("TIPO_DEN").DropDownHwnd = sdbddTiposNotificado.hWnd
        sdbddNotificados.AddItem "0" & Chr(m_lSeparador) & ""
        sdbgNotificados.Columns("NOTIFICADO").DropDownHwnd = sdbddNotificados.hWnd
    Else
        sdbgNotificados.AllowAddNew = False
        sdbgNotificados.AllowUpdate = False
        sdbgNotificados.AllowDelete = False
        
        cmdAnyadirNotificado.Enabled = False
        cmdEliminarNotificado.Enabled = False
    End If
End Sub

Private Sub CargarNotificados()
    Dim oNotificado As CNotificadoAccion
    Dim oPersona As CPersona
    Dim oRol As CPMRol
    
    
    Dim iTipo As TipoNotificadoAccion
    Dim sCodNotificado As String
    Dim sDenNotificado As String
    
    sdbgNotificados.RemoveAll
    
    Set m_oNotificados = oFSGSRaiz.Generar_CNotificadosAccion
    If (g_sOrigen = "Expiracion") Then
        m_oNotificados.CargarNotificados OrigenNotificadoAccion.Expiracion, lidexpiracion
    Else
        m_oNotificados.CargarNotificados OrigenNotificadoAccion.Enlace, lIdEnlace
    End If
    For Each oNotificado In m_oNotificados
        Select Case oNotificado.Tipo
            Case TipoNotificadoAccion.Especifico
                iTipo = TipoNotificadoAccion.Especifico
                
                Set oPersona = oFSGSRaiz.Generar_CPersona
                oPersona.Cod = oNotificado.Per
                oPersona.CargarTodosLosDatos
                
                sCodNotificado = oPersona.Cod
                sDenNotificado = oPersona.nombre & " " & oPersona.Apellidos & " (" & oPersona.mail & ")"
            Case TipoNotificadoAccion.Rol
                iTipo = TipoNotificadoAccion.Rol
                
                Set oRol = oFSGSRaiz.Generar_CPMRol
                oRol.Id = oNotificado.Rol
                Set m_oIBaseDatos = oRol
                m_oIBaseDatos.IniciarEdicion
                
                sCodNotificado = oRol.Id
                sDenNotificado = oRol.Den
            Case Else 'Generico
                iTipo = TipoNotificadoAccion.Peticionario 'Que realmente indica que es GENERICO
                sCodNotificado = CStr(oNotificado.Tipo)
                sDenNotificado = m_arNotificadoGenerico(oNotificado.Tipo)
        End Select
               
        sdbgNotificados.AddItem oNotificado.Id & Chr(m_lSeparador) & iTipo & Chr(m_lSeparador) & m_arTipoNotificado(iTipo) & Chr(m_lSeparador) & sCodNotificado & Chr(m_lSeparador) & sDenNotificado
    Next
    
    Set oNotificado = Nothing
    Set oPersona = Nothing
    Set oRol = Nothing
    Set m_oIBaseDatos = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If sdbgNotificados.DataChanged Then
        m_bError = False
        sdbgNotificados.Update
        If m_bError Then
            Cancel = True
            Exit Sub
        End If
    End If
        
    Dim oEnlace As CEnlace
    Set oEnlace = oFSGSRaiz.Generar_CEnlace

    If m_GridSubjectUpdated Or Me.sdbgSubject.DataChanged Then
        sdbgSubject.Update
        If (g_sOrigen = "Expiracion") Then
            Set oEnlace.Subject = m_oSubject
            oEnlace.ModificarEnlace_GrabarSubject
        Else
            Set m_oEnlace.Subject = m_oSubject
            m_oEnlace.ModificarEnlace_GrabarSubject
        End If
    Else
        If (g_sOrigen = "Expiracion") Then
            Set oEnlace.Subject = m_oSubject 'SubjectExpiracion(lidexpiracion)
            'Set m_oSubject = oEnlace.Subject
        End If
    End If

    If (g_sOrigen = "Expiracion") Then
        
        Dim cId As String
        
        cId = oEnlace.ObtenerExpiracionMail(lidexpiracion)
        If (cId = "0") Then
            oEnlace.InsertarExpiracionMail (lidexpiracion)
        Else
            oEnlace.ModificarExpiracionMail (lidexpiracion)
        End If
    End If
    
    frmFlujosDetalleAccion.m_bBtnClkEt = False
    Set m_oNotificadoEnEdicion = Nothing
    Set m_oNotificadoAnyadir = Nothing
    Set m_oIBAseDatosEnEdicion = Nothing
    Set m_oIBaseDatos = Nothing
    Set m_oNotificados = Nothing
    Set m_oFormulario = Nothing
    g_sOrigen = ""
End Sub

Private Sub sdbddNotificados_CloseUp()
    Dim teserror As TipoErrorSummit
    
    If sdbgNotificados.Columns("NOTIFICADO").Value = "" Then Exit Sub
    
    sdbgNotificados.Columns("COD_NOTIFICADO").Value = sdbddNotificados.Columns(0).Value
    sdbgNotificados.Columns("NOTIFICADO").Value = sdbddNotificados.Columns(1).Value
    
    If Not m_oNotificadoEnEdicion Is Nothing Then
        m_oNotificadoEnEdicion.Configuracion_Rol = False
        m_oNotificadoEnEdicion.CambioDeRoloTipoNotificado sdbgNotificados.Columns("ID").Value, OrigenNotificadoAccion.Enlace
        
        'Solo si es Rol
        If sdbgNotificados.Columns("TIPO").Value = TipoNotificadoAccion.Rol Then
            m_oNotificadoEnEdicion.Rol = sdbddNotificados.Columns(0).Value
        End If
        'TipoNotificadoAccion.Especifico
        'TipoNotificadoAccion.Peticionario
        
        Set m_oIBAseDatosEnEdicion = m_oNotificadoEnEdicion
        teserror = m_oIBAseDatosEnEdicion.FinalizarEdicionModificando
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bModError = True
            If Me.Visible Then sdbgNotificados.SetFocus
            sdbgNotificados.DataChanged = False
        Else
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            If g_sOrigen <> "Expiracion" Then frmFlujos.HayCambios            ''' Registro de acciones
            sdbgNotificados.Columns("FECACT").Value = m_oNotificadoEnEdicion.FECACT
            basSeguridad.RegistrarAccion AccionesSummit.ACCNotificadoAccionModif, "Enlace:" & lIdEnlace & ",ID:" & m_oNotificadoEnEdicion.Id
            m_bModError = False
        End If
    End If
End Sub

Private Sub sdbddNotificados_DropDown()
    Dim oPersonas As CPersonas
    Dim oPersona As CPersona
    Dim oRoles As CPMRoles
    Dim oRol As CPMRol
    Dim i As Integer
    Dim bAnyadido As Boolean

    Screen.MousePointer = vbHourglass
        
    If sdbgNotificados.Columns("TIPO").Value <> "" Then
        sdbddNotificados.RemoveAll
        bAnyadido = False
        Select Case CInt(sdbgNotificados.Columns("TIPO").Value)
            Case TipoNotificadoAccion.Especifico
                Set oPersonas = oFSGSRaiz.Generar_CPersonas
                'basPublic.oGestorSeguridad.BuscarTodosLosUsuariosDeTipoPersona(txtCodUsuPer, txtApePer, txtNombrePer, txtCodPer, , Cuenta, chkSoloComp, chkProcurement.Value, , chkFSWS.Value, chkFSGS.Value, chkFSQA.Value)
                oPersonas.CargarTodasLasPersonas , , , , 4, , True, True, , , , True
                For Each oPersona In oPersonas
                    sdbddNotificados.AddItem oPersona.Cod & Chr(m_lSeparador) & oPersona.nombre & " " & oPersona.Apellidos & " (" & oPersona.mail & ")"
                    bAnyadido = True
                Next
            Case TipoNotificadoAccion.Rol
                Set oRoles = oFSGSRaiz.Generar_CPMRoles
                oRoles.CargarRolesWorkflow lIdFlujo
                For Each oRol In oRoles
                    sdbddNotificados.AddItem oRol.Id & Chr(m_lSeparador) & oRol.Den
                    bAnyadido = True
                Next
            Case TipoNotificadoAccion.Peticionario 'que en realidad se refiere a 'GENERICO' como concepto global
                For i = TipoNotificadoAccion.Peticionario To TipoNotificadoAccion.TNAcomprador
                    If iTipoSolicitud = tipoSolicitud.NoConformidades Then
                        If i >= TipoNotificadoAccion.Peticionario And i <= TipoNotificadoAccion.TodosLosAprobadores Then
                            sdbddNotificados.AddItem i & Chr(m_lSeparador) & m_arNotificadoGenerico(i)
                            bAnyadido = True
                        End If
                    Else
                        If i <> TipoNotificadoAccion.TNAcomprador Or Formulario.ExisteCampoGSFormulario(CodComprador) Then
                            sdbddNotificados.AddItem i & Chr(m_lSeparador) & m_arNotificadoGenerico(i)
                            bAnyadido = True
                        End If
                    End If
                Next
        End Select
    End If
    
    If Not bAnyadido Then
        sdbddNotificados.AddItem "" & Chr(m_lSeparador) & ""
    End If
        
    Set oPersonas = Nothing
    Set oPersona = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddNotificados_InitColumnProps()
    sdbddNotificados.DataFieldList = "Column 0"
    sdbddNotificados.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddNotificados_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddNotificados.MoveFirst

    If sdbgNotificados.Columns("NOTIFICADO").Value <> "" Then
        For i = 0 To sdbddNotificados.Rows - 1
            bm = sdbddNotificados.GetBookmark(i)
            If UCase(sdbgNotificados.Columns("NOTIFICADO").Value) = UCase(Mid(sdbddNotificados.Columns(1).CellText(bm), 1, Len(sdbgNotificados.Columns("NOTIFICADO").Value))) Then
                sdbgNotificados.Columns("NOTIFICADO").Value = Mid(sdbddNotificados.Columns(1).CellText(bm), 1, Len(sdbgNotificados.Columns("NOTIFICADO").Value))
                sdbddNotificados.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
Private Sub sdbddTiposNotificado_CloseUp()
    Dim teserror As TipoErrorSummit
    
    If sdbgNotificados.Columns("TIPO_DEN").Value = "" Then Exit Sub
    
    sdbgNotificados.Columns("TIPO").Value = sdbddTiposNotificado.Columns(0).Value
    sdbgNotificados.Columns("TIPO_DEN").Value = sdbddTiposNotificado.Columns(1).Value
    
    If Not m_oNotificadoEnEdicion Is Nothing Then
        m_oNotificadoEnEdicion.Configuracion_Rol = False
        m_oNotificadoEnEdicion.CambioDeRoloTipoNotificado sdbgNotificados.Columns("ID").Value, OrigenNotificadoAccion.Enlace
        Set m_oIBAseDatosEnEdicion = m_oNotificadoEnEdicion
        teserror = m_oIBAseDatosEnEdicion.FinalizarEdicionModificando
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bModError = True
            If Me.Visible Then sdbgNotificados.SetFocus
            sdbgNotificados.DataChanged = False
        Else
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            If g_sOrigen <> "Expiracion" Then frmFlujos.HayCambios            ''' Registro de acciones
            sdbgNotificados.Columns("FECACT").Value = m_oNotificadoEnEdicion.FECACT
            basSeguridad.RegistrarAccion AccionesSummit.ACCNotificadoAccionModif, "Enlace:" & lIdEnlace & ",ID:" & m_oNotificadoEnEdicion.Id
            m_bModError = False
        End If
    End If
End Sub

Private Sub sdbddTiposNotificado_DropDown()
    
    Screen.MousePointer = vbHourglass
    sdbddTiposNotificado.RemoveAll
    
    sdbddTiposNotificado.AddItem TipoNotificadoAccion.Especifico & Chr(m_lSeparador) & m_arTipoNotificado(TipoNotificadoAccion.Especifico)
    sdbddTiposNotificado.AddItem TipoNotificadoAccion.Rol & Chr(m_lSeparador) & m_arTipoNotificado(TipoNotificadoAccion.Rol)
    sdbddTiposNotificado.AddItem TipoNotificadoAccion.Peticionario & Chr(m_lSeparador) & m_arTipoNotificado(TipoNotificadoAccion.Peticionario) 'En realidad se est� metiendo el texto "Generico"

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddTiposNotificado_InitColumnProps()
    sdbddTiposNotificado.DataFieldList = "Column 0"
    sdbddTiposNotificado.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddTiposNotificado_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddTiposNotificado.MoveFirst

    If sdbgNotificados.Columns("TIPO_DEN").Value <> "" Then
        For i = 0 To sdbddTiposNotificado.Rows - 1
            bm = sdbddTiposNotificado.GetBookmark(i)
            If UCase(sdbgNotificados.Columns("TIPO_DEN").Value) = UCase(Mid(sdbddTiposNotificado.Columns(1).CellText(bm), 1, Len(sdbgNotificados.Columns("TIPO_DEN").Value))) Then
                sdbgNotificados.Columns("TIPO_DEN").Value = Mid(sdbddTiposNotificado.Columns(1).CellText(bm), 1, Len(sdbgNotificados.Columns("TIPO_DEN").Value))
                sdbddTiposNotificado.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgNotificados_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgNotificados.Rows = 0) Then
        Exit Sub
    End If
    If IsEmpty(sdbgNotificados.GetBookmark(0)) Then
        sdbgNotificados.Bookmark = sdbgNotificados.GetBookmark(-1)
    Else
        sdbgNotificados.Bookmark = sdbgNotificados.GetBookmark(0)
    End If
    If Me.Visible Then sdbgNotificados.SetFocus
End Sub

Private Sub sdbgNotificados_AfterInsert(RtnDispErrMsg As Integer)
''' * Objetivo: Si no hay error, volver a la
''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
        
    If m_bAnyaError = False And m_bModifFlujo Then
        cmdAnyadirNotificado.Enabled = True
    End If
    
    If IsEmpty(sdbgNotificados.GetBookmark(0)) Then
        sdbgNotificados.Bookmark = sdbgNotificados.GetBookmark(-1)
    Else
        sdbgNotificados.Bookmark = sdbgNotificados.GetBookmark(0)
    End If
    'sdbgNotificados.Bookmark = sdbgNotificados.RowBookmark(sdbgNotificados.Row)
End Sub

Private Sub sdbgNotificados_AfterUpdate(RtnDispErrMsg As Integer)
    
    RtnDispErrMsg = 0
    If m_bAnyaError = False And m_bModError = False Then
        cmdAnyadirNotificado.Enabled = True
    End If
       
    If Not m_oNotificadoEnEdicion Is Nothing Then
        sdbgNotificados.Columns("FECACT").Value = m_oNotificadoEnEdicion.FECACT
    End If
    
    Set m_oNotificadoEnEdicion = Nothing
End Sub

Private Sub sdbgNotificados_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub


Private Sub sdbgNotificados_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    
    'Comprobar datos Obligatorios
    If sdbgNotificados.Columns("TIPO").Value = "" Or sdbgNotificados.Columns("TIPO").Value = "0" Then
        oMensajes.NoValido m_sMensajesNotificado(1)
        Cancel = True
        GoTo Salir
    End If
    If sdbgNotificados.Columns("COD_NOTIFICADO").Value = "" Then
        oMensajes.NoValido m_sMensajesNotificado(2)
        Cancel = True
        GoTo Salir
    End If
    
    'Guardar Datos
     If sdbgNotificados.IsAddRow Then
         If (g_sOrigen = "Expiracion") Then
            Set m_oNotificadoAnyadir = oFSGSRaiz.Generar_CNotificadoAccion
            m_oNotificadoAnyadir.Origen = OrigenNotificadoAccion.Expiracion
            m_oNotificadoAnyadir.Expiracion = lidexpiracion
         Else
            Set m_oNotificadoAnyadir = oFSGSRaiz.Generar_CNotificadoAccion
            m_oNotificadoAnyadir.Origen = OrigenNotificadoAccion.Enlace
            m_oNotificadoAnyadir.Enlace = lIdEnlace
        End If
        Select Case CInt(sdbgNotificados.Columns("TIPO").Value)
            Case TipoNotificadoAccion.Especifico
                m_oNotificadoAnyadir.Tipo = TipoNotificadoAccion.Especifico
                m_oNotificadoAnyadir.Per = sdbgNotificados.Columns("COD_NOTIFICADO").Value
            Case TipoNotificadoAccion.Rol
                m_oNotificadoAnyadir.Tipo = TipoNotificadoAccion.Rol
                m_oNotificadoAnyadir.Rol = CLng(sdbgNotificados.Columns("COD_NOTIFICADO").Value)
            Case TipoNotificadoAccion.Peticionario 'En Realidad se refiere a 'Generico'
                m_oNotificadoAnyadir.Tipo = CInt(sdbgNotificados.Columns("COD_NOTIFICADO").Value)
        End Select
        
        Set m_oIBAseDatosEnEdicion = m_oNotificadoAnyadir
        teserror = m_oIBAseDatosEnEdicion.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bAnyaError = True
            If Me.Visible Then sdbgNotificados.SetFocus
            GoTo Salir
        Else
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            If g_sOrigen <> "Expiracion" Then frmFlujos.HayCambios
            basSeguridad.RegistrarAccion AccionesSummit.ACCNotificadoAccionAnya, "Enlace:" & lIdEnlace & ",ID:" & m_oNotificadoAnyadir.Id
        End If

        sdbgNotificados.Columns("ID").Value = m_oNotificadoAnyadir.Id
        sdbgNotificados.Columns("FECACT").Value = m_oNotificadoAnyadir.FECACT
        m_oNotificados.AddNotificadoEnlace m_oNotificadoAnyadir
        m_bAnyaError = False

    Else
        If Not m_oNotificadoEnEdicion Is Nothing Then
            Select Case CInt(sdbgNotificados.Columns("TIPO").Value)
                Case TipoNotificadoAccion.Especifico
                    m_oNotificadoEnEdicion.Tipo = TipoNotificadoAccion.Especifico
                    m_oNotificadoEnEdicion.Per = sdbgNotificados.Columns("COD_NOTIFICADO").Value
                    m_oNotificadoEnEdicion.Configuracion_Rol = False
                Case TipoNotificadoAccion.Rol
                    m_oNotificadoEnEdicion.Tipo = TipoNotificadoAccion.Rol
                    m_oNotificadoEnEdicion.Rol = CLng(sdbgNotificados.Columns("COD_NOTIFICADO").Value)
                Case TipoNotificadoAccion.Peticionario 'En Realidad se refiere a 'Generico'
                    m_oNotificadoEnEdicion.Tipo = CInt(sdbgNotificados.Columns("COD_NOTIFICADO").Value)
                Case TipoNotificadoAccion.RespProceCompraAsoc
                    m_oNotificadoEnEdicion.Configuracion_Rol = False
                Case TipoNotificadoAccion.GestorSolicitud
                    m_oNotificadoEnEdicion.Configuracion_Rol = False
            End Select
            
            Set m_oIBAseDatosEnEdicion = m_oNotificadoEnEdicion
            teserror = m_oIBAseDatosEnEdicion.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                m_bModError = True
                If Me.Visible Then sdbgNotificados.SetFocus
                sdbgNotificados.DataChanged = False
                GoTo Salir
    
            Else
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                If g_sOrigen <> "Expiracion" Then frmFlujos.HayCambios
                ''' Registro de acciones
                sdbgNotificados.Columns("FECACT").Value = m_oNotificadoEnEdicion.FECACT
                basSeguridad.RegistrarAccion AccionesSummit.ACCNotificadoAccionModif, "Enlace:" & lIdEnlace & ",ID:" & m_oNotificadoEnEdicion.Id
                m_bModError = False
            End If
    
            Set m_oNotificadoEnEdicion = Nothing
        End If
    End If

    Set m_oNotificadoAnyadir = Nothing
    Exit Sub
    
Salir:
    If Me.Visible Then sdbgNotificados.SetFocus
    m_bError = True
    Exit Sub

End Sub

Private Sub sdbgNotificados_BtnClick()
    With sdbgNotificados
        If .IsAddRow Or .DataChanged Then
            .Update
            If .IsAddRow Or .DataChanged Then Exit Sub
        End If
        
        Set m_oNotificadoEnEdicion = Nothing
        Set m_oNotificadoEnEdicion = m_oNotificados.Item(CStr(sdbgNotificados.Columns("ID").Value))
        
        frmFlujosConfigCampos.lIdFormulario = lIdFormulario
        frmFlujosConfigCampos.lIdNotificado = .Columns("ID").Value
        If (g_sOrigen = "Expiracion") Then
            frmFlujosConfigCampos.iOrigenNotificado = OrigenNotificadoAccion.Expiracion
        Else
            frmFlujosConfigCampos.iOrigenNotificado = OrigenNotificadoAccion.Enlace
        End If
        frmFlujosConfigCampos.bConfiguracionCamposRolVisible = ValidarVisibilidadConfiguracionCamposRol
        Set frmFlujosConfigCampos.oNotificadoEnEdicion = m_oNotificadoEnEdicion
        frmFlujosConfigCampos.bConfiguracionRol = m_oNotificadoEnEdicion.Configuracion_Rol
        frmFlujosConfigCampos.Show vbModal
    End With
End Sub

Private Sub sdbgNotificados_Change()
    Dim teserror As TipoErrorSummit
    
'    cmdAnyadirNotificado.Enabled = False
    
    If Not sdbgNotificados.IsAddRow Then
        Set m_oNotificadoEnEdicion = Nothing
        Set m_oNotificadoEnEdicion = m_oNotificados.Item(CStr(sdbgNotificados.Columns("ID").Value))
        
        Set m_oIBAseDatosEnEdicion = m_oNotificadoEnEdicion
            
        teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
            
        If teserror.NumError = TESInfModificada Then
            TratarError teserror
            sdbgNotificados.DataChanged = False
            
            CargarNotificados
            
            teserror.NumError = TESnoerror
                
        End If
            
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgNotificados.SetFocus
        End If
    Else
         Set m_oNotificadoEnEdicion = Nothing
    End If
    
    If sdbgNotificados.col >= 0 Then
        Select Case sdbgNotificados.Columns(sdbgNotificados.col).Name
            Case "TIPO_DEN"
                sdbgNotificados.Columns("COD_NOTIFICADO").Value = ""
                sdbgNotificados.Columns("NOTIFICADO").Value = ""
        End Select
    End If
    
End Sub

Private Sub sdbgNotificados_InitColumnProps()
    sdbgNotificados.Columns("TIPO_DEN").caption = m_sTipoNotificado
    sdbgNotificados.Columns("NOTIFICADO").caption = m_sNotificado
    sdbgNotificados.Columns("CONFCAMPOS").caption = m_sConfCampos
End Sub

Private Sub sdbgNotificados_KeyDown(KeyCode As Integer, Shift As Integer)
''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: poder eliminar desde el teclado

    If KeyCode = vbKeyDelete Then
            cmdEliminarNotificado_Click
        Exit Sub

    End If
End Sub

Private Sub sdbgNotificados_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeySpace
            If sdbgNotificados.IsAddRow Then
                cmdAnyadirNotificado.Enabled = True
            End If
        Case vbKeyReturn
            If sdbgNotificados.DataChanged Then
                sdbgNotificados.Update
                If m_bError Then
                    Exit Sub
                End If
                cmdAnyadirNotificado.Enabled = True
            End If
        Case vbKeyBack
            If sdbgNotificados.col >= 0 Then
                Select Case sdbgNotificados.Columns(sdbgNotificados.col).Name
                    Case "TIPO_DEN"
                        sdbgNotificados.Columns("TIPO").Value = 0
                        sdbgNotificados.Columns("TIPO_DEN").Value = ""
                        sdbgNotificados.Columns("COD_NOTIFICADO").Value = ""
                        sdbgNotificados.Columns("NOTIFICADO").Value = ""
                    Case "NOTIFICADO"
                        sdbgNotificados.Columns("COD_NOTIFICADO").Value = ""
                        sdbgNotificados.Columns("NOTIFICADO").Value = ""
                        sdbgNotificados.col = sdbgNotificados.Columns("TIPO_DEN").Position
                End Select
            End If
    End Select
End Sub

Private Sub sdbgNotificados_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bMalaColumna As Boolean
    Dim iIndexMensaje As Integer
    Dim sColumnaARellenar As String
    
    If sdbgNotificados.IsAddRow Then
        cmdAnyadirNotificado.Enabled = False
    Else
        cmdAnyadirNotificado.Enabled = True
    End If

    If sdbgNotificados.col < 0 Then
        Exit Sub
    End If
    
    bMalaColumna = False
    Select Case sdbgNotificados.Columns(sdbgNotificados.col).Name
        Case "NOTIFICADO"
            bMalaColumna = True
            iIndexMensaje = 2
            sColumnaARellenar = "TIPO_DEN"
            If sdbgNotificados.Columns("TIPO").Value <> "" Then
                If CInt(sdbgNotificados.Columns("TIPO").Value) > 0 Then
                    bMalaColumna = False
                End If
            End If
    End Select
    If bMalaColumna Then
        oMensajes.NoValido m_sMensajesNotificado(iIndexMensaje)
        sdbgNotificados.col = sdbgNotificados.Columns(sColumnaARellenar).Position
    End If
End Sub

Private Sub CargarSubjects()
    Dim iNumIdiomas As Integer
    Dim oIdiomas As CIdiomas
    
    m_GridSubjectUpdated = False
    Set m_oSubject = oFSGSRaiz.Generar_CMultiidiomas
    Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    
    iNumIdiomas = oIdiomas.Count
    sdbgSubject.ScrollBars = 0
    If iNumIdiomas > 4 Then sdbgSubject.ScrollBars = 2
        
    If Not (g_sOrigen = "Expiracion") Then
        sdbgSubject.RemoveAll
        For Each m_Idi In m_oEnlace.Subject
            m_oSubject.Add m_Idi.Cod, m_Idi.Den
            sdbgSubject.AddItem m_Idi.Cod & Chr(m_lSeparador) & oIdiomas.Item(m_Idi.Cod).Den & Chr(m_lSeparador) & m_Idi.Den
        Next
    Else
        Dim oEnlace As CEnlace
        Set oEnlace = oFSGSRaiz.Generar_CEnlace
        
        Dim cId As String
        
        cId = oEnlace.ObtenerExpiracionMail(lidexpiracion)
        If (cId = "0") Then
            Set oEnlace.Subject = SubjectEnlacePorDefecto()
        Else
            Set oEnlace.Subject = SubjectExpiracion(lidexpiracion)
        End If
        
        'Set oEnlace.Subject = SubjectEnlacePorDefecto()
        Set m_oSubject = oEnlace.Subject
        
        For Each m_Idi In oEnlace.Subject
            sdbgSubject.AddItem m_Idi.Cod & Chr(m_lSeparador) & oIdiomas.Item(m_Idi.Cod).Den & Chr(m_lSeparador) & m_Idi.Den
        Next
    End If
    Set oIdiomas = Nothing
End Sub

Private Sub sdbgSubject_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
End Sub

Private Sub sdbgSubject_BeforeUpdate(Cancel As Integer)
    m_oSubject.Item(sdbgSubject.Columns("COD_IDIOMA").Value).Den = sdbgSubject.Columns("VALOR_IDIOMA").Value
    m_GridSubjectUpdated = True
End Sub

Private Function ValidarVisibilidadConfiguracionCamposRol() As Boolean
    With sdbgNotificados
        Select Case .Columns("TIPO").Value
            Case TipoNotificadoAccion.Peticionario
                Select Case .Columns("COD_NOTIFICADO").Value
                    Case TipoNotificadoAccion.Peticionario, TipoNotificadoAccion.UltimoAprobador
                        ValidarVisibilidadConfiguracionCamposRol = m_oNotificadoEnEdicion.ValidarVisibilidadConfiguracionCamposRol(.Columns("ID").Value, .Columns("COD_NOTIFICADO").Value, OrigenNotificadoAccion.Enlace)
                    Case Else
                        ValidarVisibilidadConfiguracionCamposRol = False
                End Select
            Case TipoNotificadoAccion.Rol
                ValidarVisibilidadConfiguracionCamposRol = True
            Case Else
                ValidarVisibilidadConfiguracionCamposRol = False
        End Select
    End With
End Function

Public Property Get Formulario() As CFormulario
    Set Formulario = m_oFormulario
End Property

Public Property Set Formulario(ByRef f As CFormulario)
    Set m_oFormulario = f
End Property

Private Function SubjectEnlacePorDefecto() As CMultiidiomas
    Dim oIdiomas As CIdiomas
    Dim oIdioma As CIdioma
    Dim oSubject As CMultiidiomas
    Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    Set oSubject = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In oIdiomas
        oSubject.Add oIdioma.Cod, oGestorIdiomas.DevolverTextosDelModulo(FRM_NOTIFENLACESUBJECT, oIdioma.Cod, 17)(0).Value
    Next
    Set SubjectEnlacePorDefecto = oSubject
End Function

Private Function SubjectExpiracion(lidexpiracion As Long) As CMultiidiomas
    Dim oIdiomas As CIdiomas
    Dim oIdioma As CIdioma
    Dim oSubject As CMultiidiomas
    Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    Set oSubject = oFSGSRaiz.Generar_CMultiidiomas
    Dim oEnlace As CEnlace
    Set oEnlace = oFSGSRaiz.Generar_CEnlace
    For Each oIdioma In oIdiomas
        oSubject.Add oIdioma.Cod, oEnlace.DevolverTextosExpiracion(lidexpiracion, oIdioma.Cod)
    Next
    Set SubjectExpiracion = oSubject
End Function


