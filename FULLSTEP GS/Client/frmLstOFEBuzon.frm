VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{1E673DEF-C82F-4FBB-BAB9-77B0634E8F4D}#1.0#0"; "ProceSelector.ocx"
Begin VB.Form frmLstOFEBuzon 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado del buz�n de ofertas (Opciones)"
   ClientHeight    =   5175
   ClientLeft      =   885
   ClientTop       =   3330
   ClientWidth     =   7185
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstOFEBuzon.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5175
   ScaleMode       =   0  'User
   ScaleWidth      =   1403.32
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdObtener 
      Caption         =   "Obtener"
      Height          =   375
      Left            =   5625
      TabIndex        =   27
      Top             =   4785
      Width           =   1410
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4755
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7035
      _ExtentX        =   12409
      _ExtentY        =   8387
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstOFEBuzon.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "FraFiltroFecha"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "FraFiltroProce"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "FraFiltroMat"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "FraFiltroProve"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "FraFiltroEstadoOfe"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstOFEBuzon.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "FraDetalle"
      Tab(1).Control(1)=   "FraOpciones"
      Tab(1).Control(2)=   "FraOrden"
      Tab(1).ControlCount=   3
      Begin VB.Frame FraOrden 
         Caption         =   "Orden"
         Height          =   705
         Left            =   -74805
         TabIndex        =   38
         Top             =   3825
         Width           =   6630
         Begin SSDataWidgets_B.SSDBCombo sdbcOrdenar 
            Height          =   285
            Left            =   1920
            TabIndex        =   39
            Top             =   270
            Width           =   2490
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4392
            Columns(0).Caption=   "ORDENAR"
            Columns(0).Name =   "ORDENAR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "NUM"
            Columns(1).Name =   "NUM"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblOrden 
            Caption         =   "Orden:"
            Height          =   255
            Left            =   900
            TabIndex        =   40
            Top             =   285
            Width           =   915
         End
      End
      Begin VB.Frame FraOpciones 
         Caption         =   "Opciones"
         Height          =   705
         Left            =   -74805
         TabIndex        =   32
         Top             =   3030
         Width           =   6630
         Begin VB.OptionButton opTodas 
            Caption         =   "Todas"
            Height          =   195
            Left            =   5235
            TabIndex        =   35
            Top             =   300
            Width           =   1290
         End
         Begin VB.OptionButton opNoLeidas 
            Caption         =   "S�lo no le�das"
            Height          =   270
            Left            =   2760
            TabIndex        =   34
            Top             =   300
            Width           =   2355
         End
         Begin VB.OptionButton opLeidas 
            Caption         =   "S�lo le�das"
            Height          =   240
            Left            =   525
            TabIndex        =   33
            Top             =   300
            Width           =   2070
         End
      End
      Begin VB.Frame FraDetalle 
         Caption         =   "Detalle"
         Height          =   2385
         Left            =   -74805
         TabIndex        =   28
         Top             =   525
         Width           =   6630
         Begin VB.OptionButton OpAbiertos 
            Caption         =   "S�lo los abiertos"
            Height          =   420
            Left            =   510
            TabIndex        =   44
            Top             =   1020
            Width           =   2115
         End
         Begin VB.OptionButton OpCerrados 
            Caption         =   "S�lo los cerrados"
            Height          =   420
            Left            =   2745
            TabIndex        =   43
            Top             =   1035
            Width           =   2370
         End
         Begin VB.OptionButton OpTodos 
            Caption         =   "Todos"
            Height          =   420
            Left            =   5220
            TabIndex        =   42
            Top             =   1035
            Value           =   -1  'True
            Width           =   1260
         End
         Begin VB.CheckBox chkPrecios 
            Caption         =   "Incluir Precios"
            Height          =   255
            Left            =   300
            TabIndex        =   36
            Top             =   360
            Width           =   3060
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMonDen 
            Height          =   285
            Left            =   1980
            TabIndex        =   31
            Top             =   1875
            Width           =   4080
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1905
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "Equivalencia"
            Columns(2).Name =   "Equivalencia"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   7197
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
            Height          =   285
            Left            =   885
            TabIndex        =   30
            Top             =   1875
            Width           =   1035
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1905
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "Equivalencia"
            Columns(2).Name =   "Equivalencia"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label LblParcialmenteCerrados 
            Caption         =   "Mostrar �tems en procesos parcialmente cerrados:"
            Height          =   270
            Left            =   285
            TabIndex        =   41
            Top             =   735
            Width           =   5685
         End
         Begin VB.Label lblMonedas 
            Caption         =   "Mostrar las cantidades en:"
            Height          =   255
            Left            =   300
            TabIndex        =   29
            Top             =   1515
            Width           =   2130
         End
      End
      Begin VB.Frame FraFiltroEstadoOfe 
         Caption         =   "Filtro por estado de las ofertas"
         Height          =   780
         Left            =   195
         TabIndex        =   24
         Top             =   3780
         Width           =   6660
         Begin SSDataWidgets_B.SSDBCombo sdbcEstadoCod 
            Height          =   285
            Left            =   1050
            TabIndex        =   25
            Top             =   300
            Width           =   2010
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1588
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3519
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3545
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEstadoDen 
            Height          =   285
            Left            =   3300
            TabIndex        =   26
            Top             =   300
            Width           =   2595
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3519
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1588
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4577
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.Frame FraFiltroProve 
         Caption         =   "Filtro por proveedor"
         Height          =   765
         Left            =   210
         TabIndex        =   20
         Top             =   2960
         Width           =   6660
         Begin VB.CommandButton cmdBuscarProve 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5955
            Picture         =   "frmLstOFEBuzon.frx":0CEA
            Style           =   1  'Graphical
            TabIndex        =   23
            Top             =   300
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
            Height          =   285
            Left            =   3300
            TabIndex        =   22
            Top             =   300
            Width           =   2595
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4048
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1931
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4577
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
            Height          =   285
            Left            =   1020
            TabIndex        =   21
            Top             =   300
            Width           =   2010
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1931
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4048
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3545
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.Frame FraFiltroMat 
         Caption         =   "Filtro por material"
         Height          =   735
         Left            =   195
         TabIndex        =   16
         Top             =   2170
         Width           =   6660
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   6135
            Picture         =   "frmLstOFEBuzon.frx":0D77
            Style           =   1  'Graphical
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   300
            Width           =   345
         End
         Begin VB.CommandButton cmdBorrar 
            Height          =   315
            Left            =   5760
            Picture         =   "frmLstOFEBuzon.frx":0DE3
            Style           =   1  'Graphical
            TabIndex        =   18
            Top             =   300
            Width           =   345
         End
         Begin VB.TextBox txtEstMat 
            BackColor       =   &H80000018&
            Enabled         =   0   'False
            Height          =   285
            Left            =   1000
            TabIndex        =   17
            Top             =   300
            Width           =   4710
         End
      End
      Begin VB.Frame FraFiltroProce 
         Caption         =   "Filtro por procesos"
         Height          =   750
         Left            =   195
         TabIndex        =   8
         Top             =   1400
         Width           =   6660
         Begin VB.CommandButton cmdBuscarProce 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6120
            Picture         =   "frmLstOFEBuzon.frx":0E88
            Style           =   1  'Graphical
            TabIndex        =   15
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox txtProceCod 
            Height          =   285
            Left            =   4890
            TabIndex        =   14
            Top             =   300
            Width           =   1170
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
            Height          =   285
            Left            =   1000
            TabIndex        =   11
            Top             =   300
            Width           =   885
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1561
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
            Height          =   285
            Left            =   2505
            TabIndex        =   12
            Top             =   300
            Width           =   1170
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod."
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2064
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SelectorDeProcesos.ProceSelector ProceSelector1 
            Height          =   315
            Left            =   3735
            TabIndex        =   37
            Top             =   300
            Width           =   315
            _ExtentX        =   556
            _ExtentY        =   556
         End
         Begin VB.Label lblCod 
            Caption         =   "C�digo:"
            Height          =   255
            Left            =   4185
            TabIndex        =   13
            Top             =   315
            Width           =   660
         End
         Begin VB.Label lblGMN1_4 
            Caption         =   "Cmd.:"
            Height          =   255
            Left            =   1995
            TabIndex        =   10
            Top             =   315
            Width           =   525
         End
         Begin VB.Label lblAnyo 
            Caption         =   "A�o:"
            Height          =   255
            Left            =   270
            TabIndex        =   9
            Top             =   315
            Width           =   465
         End
      End
      Begin VB.Frame FraFiltroFecha 
         Caption         =   "Filtro por fecha de recepci�n"
         Height          =   885
         Left            =   195
         TabIndex        =   1
         Top             =   480
         Width           =   6660
         Begin VB.Timer Timer1 
            Enabled         =   0   'False
            Interval        =   2000
            Left            =   5835
            Top             =   270
         End
         Begin VB.CommandButton cmdCalFecRecepHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4995
            Picture         =   "frmLstOFEBuzon.frx":0F15
            Style           =   1  'Graphical
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   345
            Width           =   315
         End
         Begin VB.CommandButton cmdCalFecRecepDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2200
            Picture         =   "frmLstOFEBuzon.frx":149F
            Style           =   1  'Graphical
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   345
            Width           =   315
         End
         Begin VB.TextBox txtFecHasta 
            Height          =   285
            Left            =   3810
            TabIndex        =   5
            Top             =   345
            Width           =   1140
         End
         Begin VB.TextBox txtFecDesde 
            Height          =   285
            Left            =   1000
            TabIndex        =   3
            Top             =   345
            Width           =   1140
         End
         Begin VB.Label lblFecHasta 
            Caption         =   "Hasta:"
            Height          =   255
            Left            =   3200
            TabIndex        =   4
            Top             =   345
            Width           =   525
         End
         Begin VB.Label lblFecDesde 
            Caption         =   "Desde:"
            Height          =   255
            Left            =   270
            TabIndex        =   2
            Top             =   345
            Width           =   630
         End
      End
   End
End
Attribute VB_Name = "frmLstOFEBuzon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private oFos As FileSystemObject
Private RepPath As String
Private iProceso As Long
' Variables de idioma
Private sEstados(1 To 11) As String
Private sOrdenar(1 To 4) As String
Private sMON As String
Private sMat As String

' Variables de restricciones
Public bRMat As Boolean             ' material asociado
Public bRAsig As Boolean            ' comprador asignado
Public bRCompResponsable As Boolean        ' comprador responsable
Public bREqpAsig As Boolean
Public bRUsuAper As Boolean
Public bRUsuUON As Boolean
Public bRUsuDep As Boolean
Public bREqp As Boolean
Private bRComp As Boolean
Private bROfeEqp As Boolean
Private bROfeTodas As Boolean

' Origen llamada a formulario
Public sOrigen As String
Private OrdenListado As TipoOrdenacionBuzon
Private iAnio As Integer

'Variables para materiales
Private oGruposMN1 As CGruposMatNivel1
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4
Private sGMN1Cod As String
Private sGMN2Cod As String
Private sGMN3Cod As String
Private sGMN4Cod As String
Private sGMN1Proce As String

' Variables para el manejo de combos
Public bRespetarCombo As Boolean 'combos moneda y estado
Private bCargarComboDesde As Boolean


' Coleccion de procesos a cargar
Private oProcesos As CProcesos
Public oProceEncontrados As CProcesos
'Proceso seleccionado
Public oProcesoSeleccionado As CProceso

'Variables para tratamiento de combo Proveedor
Public RespetarComboProve As Boolean
Private oProves As CProveedores
'Proveedor seleccionado
Public oProveedorSeleccionado As CProveedor

' Interface para obtener proveedores asignados a un proceso
Private oIasig As IAsignaciones

Private oMonedas As CMonedas
Private dequivalencia As Double
Private sMoneda As String

'Colecci�n de Estados de las ofertas
Private oEstados As COfeEstados

'Colecci�n de ofertas
Private oOfertas As COfertas
Private oOferta As COferta
Private oIOfertas As iOfertas
Private oOfertaSeleccionada As COferta

'Multilenguaje
Private sIdiProceso As String
Private sIdiCodigo As String
'Fecha Desde
Private sFecDesde As String
'Fecha Hasta
Private sFecHasta As String
'Moneda
Private sMonedas As String

Private sSeleccion As String

Private stxtTitulo As String
Private stxtMon As String
Private stxtProce As String
Private stxtProve As String
Private stxtFecRecep As String
Private stxtNumOfe As String
Private stxtEstado As String
Private stxtFecValidez As String
Private stxtArticulo As String
Private stxtPrecios As String
Private stxtFechaInicial As String
Private stxtFechaFinal As String
Private stxtAnio As String
Private stxtMat As String
Private stxtLeidas As String
Private stxtNoLeidas As String
Private stxtTodas As String
Private stxtCantidad As String
Private stxtOrden As String
Private stxtFechas As String
Private stxtOfertas As String
Private stxtTimeZone As String
Private sIdiGenerando As String
Private sIdiSeleccionando As String
Private sIdiVisualizando As String
Private FormulaRpt(1, 1 To 7) As String      'formulas textos RPT
Private sMensajeSeleccion As String          'mensaje de restricci�n no adecuada para el informe
Private sEstadoProcesos As String
Private sPendientes As String
Private sAbiertos As String
Private sCerrados As String
Private sTodos As String
Private sParcialmenteCerrados As String

'Recordset desconectados para los ttx
Public g_adoresDG As Ador.Recordset

Private Function ComprobarSeleccion() As Boolean
    'como m�nimo ha de seleccionarse "S�lo no le�das"
    If Not opNoLeidas Then
        If txtFecDesde.Text = "" And txtFecHasta = "" And sdbcAnyo = "" And sdbcGMN1_4Cod = "" And txtProceCod = "" And txtEstMat = "" And sdbcProveCod = "" And sdbcProveDen = "" And sdbcEstadoCod = "" And sdbcEstadoDen = "" And Not chkPrecios Then
            ComprobarSeleccion = False
            Exit Function
        End If
    End If
    ComprobarSeleccion = True
End Function

Private Sub GMN1Seleccionado()
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
 
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
        
    sGMN1Proce = oGMN1Seleccionado.Cod
    sGMN1Cod = "": sGMN2Cod = "": sGMN3Cod = "": sGMN4Cod = ""
    txtEstMat = ""
    
End Sub

''' <summary>Obtiene el listado de ofertas</summary>
''' <remarks>Llamada desde: cmdObtener_Click</remarks>
''' <revision>LTG 29/09/2011</revision>

Private Sub ObtenerListadoOfeBuzon()
    Dim pv As Preview
    Dim oReport As CRAXDRT.Report
    Dim SubListado As CRAXDRT.Report
    Dim oCROferta As CRProceso
    Dim iEstado As Integer
    Dim tipocomprador As Integer
    Dim ListadoPorItems As Boolean
    Dim sProveSin As String
    Dim sMonedaSel As String
    Dim RepPath As String
    Dim Leidas As Integer
    Dim iPC As Integer
    Dim GRUPOFormula As String
    Dim adoresDetalle As Ador.Recordset
    Dim iOffsetTZ As Integer
    
    Set oCROferta = GenerarCRProceso
    
    sSeleccion = GenerarTextoSeleccion
    
    iPC = 2
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
        
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptBuzonOFE.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass

    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
        
   '*********** FORMULA FIELDS REPORT
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & FormulaRpt(1, 1) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & FormulaRpt(1, 2) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & stxtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProce")).Text = """" & stxtProce & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProve")).Text = """" & stxtProve & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecRec")).Text = """" & stxtFecRecep & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNumOfe")).Text = """" & stxtNumOfe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEstado")).Text = """" & stxtEstado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecValidez")).Text = """" & stxtFecValidez & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTimeZone")).Text = """" & stxtTimeZone & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TimeZone")).Text = """" & DescTZUsu & """"
    If sdbcMonCod.Value = "" Then
        oReport.ParameterFields(crs_ParameterIndex(oReport, "EQUIV")).SetCurrentValue 1, 7
    Else
        If Not IsNumeric(dequivalencia) Then
            oReport.ParameterFields(crs_ParameterIndex(oReport, "EQUIV")).SetCurrentValue 1, 7
        Else
            oReport.ParameterFields(crs_ParameterIndex(oReport, "EQUIV")).SetCurrentValue CDbl(dequivalencia), 7
        End If
    End If
    
    If basOptimizacion.gTipoDeUsuario = comprador Then
        If opLeidas Then
            Leidas = 1
        ElseIf opNoLeidas Then
            Leidas = 0
        Else
            Leidas = 2
        End If
    Else
        Leidas = 2
    End If
    
    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    'Calcular el offset entre la hora UTC y la zona horaria del usuario
    iOffsetTZ = ObtenerOffsetTZ
    
    'Report principal
    Set g_adoresDG = oGestorInformes.ListadoBuzonOfertas(bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, bRUsuUON, bRUsuDep, bROfeEqp, bROfeTodas, Leidas, txtFecDesde, txtFecHasta, val(sdbcAnyo), sGMN1Cod, val(txtProceCod), sGMN2Cod, sGMN3Cod, sGMN4Cod, sdbcProveCod, sdbcEstadoCod, sdbcMonCod, OrdenListado, ProceSelector1.Seleccion, basOptimizacion.gTipoDeUsuario, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gvarCodUsuario, basOptimizacion.gCodDepUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, sGMN1Proce, iOffsetTZ)
                                                                                                                                                       
    If Not g_adoresDG Is Nothing Then
        oReport.Database.SetDataSource g_adoresDG
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    
    'SubReport
    If chkPrecios.Value = vbChecked Then
        If OpAbiertos Then
            iPC = 0
        ElseIf OpCerrados Then
            iPC = 1
        Else
            iPC = 2
        End If
    
        oReport.FormulaFields(crs_FormulaIndex(oReport, "SUPRIMIR")).Text = """" & "FALSE" & """"
        
        Set SubListado = oReport.OpenSubreport("rptOFERTAS")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtArticulo")).Text = """" & stxtArticulo & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrecio")).Text = """" & stxtPrecios & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrecio2")).Text = """" & stxtPrecios & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrecio3")).Text = """" & stxtPrecios & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCantidad")).Text = """" & stxtCantidad & """"
    
        Set adoresDetalle = oGestorInformes.ListadoBuzonOfeDetalle(val(sdbcAnyo.Value), sGMN1Cod, val(txtProceCod.Text), sdbcProveCod, iPC)
        If Not adoresDetalle Is Nothing Then
            SubListado.Database.SetDataSource adoresDetalle
        End If

        Set SubListado = Nothing
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "SUPRIMIR")).Text = """" & "TRUE" & """"
    End If
        
        
    '''Orden del grupo
    Select Case OrdenListado
        Case TipoOrdenacionBuzon.OrdPorProce
            GRUPOFormula = "{@PROCESO} & {BuzonOfe_ttx.PROVE} & CStr({BuzonOfe_ttx.OFE},'00000000') "
        Case TipoOrdenacionBuzon.OrdPorProve
            GRUPOFormula = "{BuzonOfe_ttx.PROVE} & {@PROCESO} & CStr({BuzonOfe_ttx.OFE},'00000000') "
        Case TipoOrdenacionBuzon.OrdPorFechaRecep
            GRUPOFormula = "cstr({BuzonOfe_ttx.FECREC},'yyyyMMdd') & {@PROCESO} & {BuzonOfe_ttx.PROVE} & CStr({BuzonOfe_ttx.OFE},'00000000') "
        Case TipoOrdenacionBuzon.OrdPorFechaValidez
            GRUPOFormula = "cstr({BuzonOfe_ttx.FECVAL},'yyyyMMdd') & {@PROCESO} & {BuzonOfe_ttx.PROVE} & CStr({BuzonOfe_ttx.OFE},'00000000') "
        Case Else
            GRUPOFormula = "cstr({BuzonOfe_ttx.FECREC},'yyyyMMdd') & {@PROCESO} & {BuzonOfe_ttx.PROVE} & CStr({BuzonOfe_ttx.OFE},'00000000') "
    End Select

    'Borra las ordenaciones que existan
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
    oReport.FormulaFields(crs_FormulaIndex(oReport, "ORDEN_GRUPO1")).Text = GRUPOFormula
    
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    Me.Hide
    
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    
    Set pv = New Preview
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show

    Timer1.Enabled = True
    pv.caption = stxtTitulo
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    Timer1.Enabled = False

    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    Set oReport = Nothing
    Set oCROferta = Nothing
End Sub

''' <summary>Obtiene el offset entre la hora UTC y la zona horaria del usuario en minutos</summary>
''' <returns>Entero con el offset</returns>
''' <remarks>Llamada desde: ObtenerListadoOfeBuzon</remarks>

Private Function ObtenerOffsetTZ() As Integer
    Dim dtHoraUTC As Date
    Dim dtFechaTZ As Date
    Dim dtHoraTZ As Date
    Dim sFechaTZ As String
    
    dtHoraUTC = ObtenerFechaUTC(Now)
    ConvertirUTCaTZ DateValue(dtHoraUTC), TimeValue(dtHoraUTC), oUsuarioSummit.TimeZone, dtFechaTZ, dtHoraTZ
    sFechaTZ = CStr(dtFechaTZ) & " " & CStr(dtHoraTZ)
    ObtenerOffsetTZ = DateDiff("n", dtHoraUTC, CDate(sFechaTZ))
End Function

''' <summary>Obtiene la descripci�n de la zona horaria del usuario</summary>
''' <returns>String con la descripci�n de la zona horaria del usuario</returns>
''' <remarks>Llamada desde: ObtenerListadoOfeBuzon</remarks>

Private Function DescTZUsu() As String
    Dim dcListaZonasHorarias As Dictionary
    
    Set dcListaZonasHorarias = ObtenerZonasHorarias
    DescTZUsu = dcListaZonasHorarias.Item(oUsuarioSummit.TimeZone)
    
    Set dcListaZonasHorarias = Nothing
End Function

Public Sub PonerMatSeleccionado()

    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
        
    If Not oGMN1Seleccionado Is Nothing Then
        sGMN1Cod = oGMN1Seleccionado.Cod
        txtEstMat = sGMN1Cod
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    Else
        sGMN2Cod = ""
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    Else
        sGMN3Cod = ""
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    Else
        sGMN4Cod = ""
    End If
    
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
End Sub

Private Sub ProcesoSeleccionado()

    Dim sCod As String
    
    Screen.MousePointer = vbHourglass
        
    sCod = sdbcGMN1_4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod))
    sCod = CStr(sdbcAnyo) & sCod & txtProceCod
       
    oProcesos.CargarDatosGeneralesProceso sdbcAnyo, sdbcGMN1_4Cod, val(txtProceCod)
    
    Set oProcesoSeleccionado = Nothing
    
    Set oProcesoSeleccionado = oProcesos.Item(sCod)
    
    If oProcesoSeleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set oIasig = oProcesoSeleccionado
  
    Screen.MousePointer = vbNormal

End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    sdbcAnyo.AddItem ""
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub

Private Sub CargarBuzLec()
    
    If oUsuarioSummit.Tipo <> comprador Then
        opNoLeidas.Enabled = False
        opLeidas.Enabled = False
        opTodas.Value = True
    Else
        If sOrigen = "frmOFEBuzon" Then
            opLeidas.Value = frmOFEBuzon.opLeidas.Value
            opTodas.Value = frmOFEBuzon.opTodas.Value
            opNoLeidas.Value = frmOFEBuzon.opNoLeidas.Value
        Else
            opNoLeidas.Value = True
        End If
    End If

End Sub

Private Sub ConfigurarSeguridad()

    If oUsuarioSummit.Tipo <> Administrador Then
    
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
                              
            If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestMatComprador)) Is Nothing Then
                bRMat = True
            End If
            
            If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestComprador)) Is Nothing Then
                bRAsig = True
            End If
                
            If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestResponsable)) Is Nothing Then
                bRCompResponsable = True
            End If
            
            If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestEquipo)) Is Nothing Then
                bREqpAsig = True
            End If
        
            If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFEVerOfertasEquipo)) Is Nothing Then
                bROfeEqp = True
            End If
        
            If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFEVerOfertasTodas)) Is Nothing Then
                bROfeTodas = True
            End If
          
        End If
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestUsuAper)) Is Nothing) Then
            bRUsuAper = True
        End If
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestUsuUON)) Is Nothing) Then
            bRUsuUON = True
        End If
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestUsuDep)) Is Nothing) Then
            bRUsuDep = True
        End If
    
    End If
End Sub

Private Function GenerarTextoSeleccion() As String

    Dim sUnion As String

    sSeleccion = ""
    sUnion = ""
    
    If txtFecDesde <> "" Or txtFecHasta <> "" Then
        sSeleccion = stxtFechas & " " & CStr(txtFecDesde) & " - " & CStr(txtFecHasta)
    End If
        
    If val(sdbcAnyo) <> 0 Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtAnio & " " & Format(val(sdbcAnyo), "0000")
    End If
    
    If sGMN4Cod <> "" Or sGMN3Cod <> "" Or sGMN2Cod <> "" Or sGMN1Cod <> "" Then
        If txtEstMat <> "" Then
            If sSeleccion <> "" Then sUnion = "; "
            sSeleccion = sSeleccion & sUnion & stxtMat & ": " & txtEstMat
        Else
            If sSeleccion <> "" Then sUnion = "; "
            sSeleccion = sSeleccion & sUnion & stxtMat & ": " & sGMN1Cod
        End If
    End If

    If txtProceCod <> "" Then
            If sSeleccion <> "" Then sUnion = "; "
            sSeleccion = sSeleccion & sUnion & stxtProce & ": " & txtProceCod
                
    Else
            If sSeleccion <> "" Then sUnion = "; "
                Select Case ProceSelector1.Seleccion
                    Case PSSeleccion.PSPendientes
                        sSeleccion = sSeleccion & sUnion & sEstadoProcesos & ": " & sPendientes
                    Case PSSeleccion.PSAbiertos
                        sSeleccion = sSeleccion & sUnion & sEstadoProcesos & ": " & sAbiertos
                    Case PSSeleccion.PSCerrados
                        sSeleccion = sSeleccion & sUnion & sEstadoProcesos & ": " & sCerrados
                    Case PSSeleccion.PSTodos
                        sSeleccion = sSeleccion & sUnion & sEstadoProcesos & ": " & sTodos
                    Case PSSeleccion.PSParcialCerrados
                        sSeleccion = sSeleccion & sUnion & sEstadoProcesos & ": " & sParcialmenteCerrados
                End Select
           
    End If
    
    If sdbcProveCod <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtProve & ": " & Trim(sdbcProveCod)
    End If

    If sdbcEstadoCod <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtEstado & ": " & Trim(sdbcEstadoCod)
    End If
    
    If sdbcMonCod <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtMon & ": " & Trim(sdbcMonCod)
    End If
    
    If opLeidas Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtOfertas & ": " & stxtLeidas
    End If
    
    If opNoLeidas Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtOfertas & ": " & stxtNoLeidas
    End If
    
    If opTodas Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtOfertas & ": " & stxtTodas
    End If
    
    If sdbcOrdenar <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtOrden & " " & sdbcOrdenar
    End If
        
    GenerarTextoSeleccion = sSeleccion

End Function

Private Sub CargarComboOrden()
Dim i As Integer
    OrdenListado = TipoOrdenacionBuzon.OrdPorProve
    For i = 1 To 4
        sdbcOrdenar.AddItem sOrdenar(i) & Chr(m_lSeparador) & i
    Next i
End Sub

Private Sub CargarFechas()
    Dim miFecha As Date
    
    miFecha = DateAdd("d", -gParametrosInstalacion.giBuzonPeriodo, Date)
    txtFecDesde = miFecha
    txtFecHasta = Date
End Sub

Public Sub CargarProcesoConBusqueda()

    Set oProcesos = Nothing
    Set oProcesos = frmPROCEBuscar.oProceEncontrados
    Set frmPROCEBuscar.oProceEncontrados = Nothing
    sdbcAnyo = oProcesos.Item(1).Anyo
    bRespetarCombo = True
    sdbcGMN1_4Cod = oProcesos.Item(1).GMN1Cod
    txtProceCod = oProcesos.Item(1).Cod
    ProcesoSeleccionado
    If Not oProcesoSeleccionado Is Nothing Then
        If oProcesoSeleccionado.Estado >= TipoEstadoProceso.sinitems And oProcesoSeleccionado.Estado <= TipoEstadoProceso.ConItemsSinValidar Then
            ProceSelector1.Seleccion = 0
        Else
            If oProcesoSeleccionado.Estado >= TipoEstadoProceso.validado And oProcesoSeleccionado.Estado <= TipoEstadoProceso.PreadjYConObjNotificados Then
                ProceSelector1.Seleccion = 1
            ElseIf oProcesoSeleccionado.Estado = TipoEstadoProceso.ParcialmenteCerrado Then
                ProceSelector1.Seleccion = 7
            Else
                ProceSelector1.Seleccion = 2
            End If
        End If
    End If
            
End Sub

Public Sub CargarProveedorConBusqueda()
    Dim oProves As CProveedores

    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    RespetarComboProve = True
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveDen.Text = oProves.Item(1).Den
    RespetarComboProve = False

End Sub

Private Sub CargarRecursos()

    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    On Error Resume Next
        
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTOFEBUZON, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        caption = Ador(0).Value
        stxtTitulo = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        sSeleccion = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        FraOpciones.caption = Ador(0).Value
        Ador.MoveNext
        FraFiltroFecha.caption = Ador(0).Value
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value
        Ador.MoveNext
        FraFiltroProce.caption = Ador(0).Value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value
        stxtAnio = Ador(0).Value
        Ador.MoveNext
        lblCod.caption = Ador(0).Value
        sIdiCodigo = Ador(0).Value
        Ador.MoveNext
        FraFiltroMat.caption = Ador(0).Value
        Ador.MoveNext
        FraFiltroProve.caption = Ador(0).Value
        Ador.MoveNext
        FraFiltroEstadoOfe.caption = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        FraDetalle.caption = Ador(0).Value
        Ador.MoveNext
        chkPrecios.caption = Ador(0).Value
        Ador.MoveNext
        lblMonedas.caption = Ador(0).Value
        Ador.MoveNext
        opLeidas.caption = Ador(0).Value
        stxtLeidas = Ador(0).Value
        Ador.MoveNext
        opNoLeidas.caption = Ador(0).Value
        stxtNoLeidas = Ador(0).Value
        Ador.MoveNext
        opTodas.caption = Ador(0).Value
        stxtTodas = Ador(0).Value
        Ador.MoveNext
        lblOrden.caption = Ador(0).Value
        stxtOrden = Ador(0).Value
        FraOrden.caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        sdbcEstadoCod.Columns(0).caption = Ador(0).Value
        sdbcEstadoDen.Columns(1).caption = Ador(0).Value
        sdbcMonCod.Columns(0).caption = Ador(0).Value
        sdbcMonDen.Columns(1).caption = Ador(0).Value
        
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        sdbcProveCod.Columns(1).caption = Ador(0).Value
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        sdbcEstadoCod.Columns(1).caption = Ador(0).Value
        sdbcEstadoDen.Columns(0).caption = Ador(0).Value
        sdbcMonCod.Columns(1).caption = Ador(0).Value
        sdbcMonDen.Columns(0).caption = Ador(0).Value
        
        Ador.MoveNext
        sdbcMonCod.Columns(2).caption = Ador(0).Value
        sdbcMonDen.Columns(2).caption = Ador(0).Value
        
        Ador.MoveNext
        sIdiProceso = Ador(0).Value
        sOrdenar(2) = Ador(0).Value
        stxtProce = Ador(0).Value
        Ador.MoveNext
        sOrdenar(1) = Ador(0).Value
        stxtProve = Ador(0).Value
        Ador.MoveNext
        sOrdenar(3) = Ador(0).Value
        Ador.MoveNext
        sOrdenar(4) = Ador(0).Value
        
        'Idiomas del RPT
        Ador.MoveNext
        stxtMon = Ador(0).Value
        Ador.MoveNext
        stxtFecRecep = Ador(0).Value
        Ador.MoveNext
        stxtNumOfe = Ador(0).Value
        Ador.MoveNext
        stxtEstado = Ador(0).Value
        Ador.MoveNext
        stxtFecValidez = Ador(0).Value
        Ador.MoveNext
        stxtArticulo = Ador(0).Value
        Ador.MoveNext
        stxtPrecios = Ador(0).Value
        Ador.MoveNext
        stxtCantidad = Ador(0).Value
        Ador.MoveNext
        stxtFechas = Ador(0).Value
        Ador.MoveNext
        stxtOfertas = Ador(0).Value
        Ador.MoveNext
        stxtMat = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando = Ador(0).Value
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        sIdiVisualizando = Ador(0).Value
        Ador.MoveNext
        
        ' FORMULAS RPT
        For i = 1 To 2
            FormulaRpt(1, i) = Ador(0).Value
            Ador.MoveNext
        Next
        
        sMensajeSeleccion = Ador(0).Value
        Ador.MoveNext
        sFecDesde = Ador(0).Value
        Ador.MoveNext
        sFecHasta = Ador(0).Value
        Ador.MoveNext
               
        ProceSelector1.AbrevParaPendientes = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaAbiertos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaTodos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaPendientes = Ador(0).Value
        sPendientes = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaAbiertos = Ador(0).Value
        sAbiertos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaCerrados = Ador(0).Value
        sCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaTodos = Ador(0).Value
        sTodos = Ador(0).Value
        OpTodos.caption = Ador(0).Value
        Ador.MoveNext
        sEstadoProcesos = Ador(0).Value
        Ador.MoveNext
        LblParcialmenteCerrados.caption = Ador(0).Value
        Ador.MoveNext
        OpAbiertos.caption = Ador(0).Value
        Ador.MoveNext
        OpCerrados.caption = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaParcialCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaParcialCerrados = Ador(0).Value
        sParcialmenteCerrados = Ador(0).Value
        Ador.MoveNext
        sMonedas = Ador(0).Value
        Ador.MoveNext
        stxtTimeZone = Ador(0).Value & " :"
        
        Ador.Close
    End If
    
    Set Ador = Nothing

End Sub

Private Sub chkPrecios_Click()
    If chkPrecios Then
        sdbcMonCod.Enabled = True
        sdbcMonDen.Enabled = True
        If ProceSelector1.Seleccion = 7 Or ProceSelector1.Seleccion = 3 Then
            OpAbiertos.Enabled = True
            OpCerrados.Enabled = True
            OpTodos.Enabled = True
        End If
    Else
        sdbcMonCod = ""
        sdbcMonCod.Enabled = False
        sdbcMonDen.Enabled = False
        OpAbiertos.Enabled = False
        OpCerrados.Enabled = False
        OpTodos.Value = True
        OpTodos.Enabled = False
    End If
End Sub


Private Sub cmdBorrar_Click()
    txtEstMat.Text = ""
End Sub

Private Sub cmdBuscarProce_Click()
    frmPROCEBuscar.bRDest = False
    frmPROCEBuscar.bRUsuAper = False
    frmPROCEBuscar.bRUsuDep = False
    frmPROCEBuscar.bRUsuUON = False
    frmPROCEBuscar.m_bProveAsigComp = False
    frmPROCEBuscar.m_bProveAsigEqp = False
    frmPROCEBuscar.bRAsig = bRAsig
    frmPROCEBuscar.bRMat = bRMat
    frmPROCEBuscar.bRCompResponsable = bRCompResponsable
    frmPROCEBuscar.bREqpAsig = bREqpAsig
    frmPROCEBuscar.sOrigen = "frmLstOFEBuzon"
    frmPROCEBuscar.sdbcAnyo = sdbcAnyo
    frmPROCEBuscar.sdbcGMN1Proce_Cod = sdbcGMN1_4Cod
    frmPROCEBuscar.txtCod = txtProceCod
    frmPROCEBuscar.Show 1
End Sub

Private Sub cmdBuscarProve_Click()
    frmPROVEBuscar.sOrigen = "frmLstOFEBuzon"
    frmPROVEBuscar.bREqp = bREqp
    frmPROVEBuscar.bRMat = bRMat
    frmPROVEBuscar.CodGMN1 = sdbcGMN1_4Cod.Text
    frmPROVEBuscar.Show 1
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecRecepDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecRecepHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub cmdObtener_Click()
    If Not IsDate(txtFecDesde) Then
        oMensajes.NoValido sFecDesde
        Exit Sub
    End If
    
    If Not IsDate(txtFecHasta) Then
        oMensajes.NoValido sFecHasta
        Exit Sub
    End If
    If chkPrecios = vbChecked Then
        If sdbcMonCod.Text = "" Or sdbcMonDen.Text = "" Then
            oMensajes.NoValido sMonedas
            Exit Sub
        End If
    End If

    If ComprobarSeleccion Then
        Screen.MousePointer = vbHourglass
        ObtenerListadoOfeBuzon
        Screen.MousePointer = vbNormal
    Else
        oMensajes.FaltanDatos (sMensajeSeleccion)
        Exit Sub
    End If
End Sub

Private Sub cmdSelMat_Click()
    frmSELMAT.sOrigen = "frmLstOFEBuzon"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Show 1
End Sub

Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
     sGMN1Cod = ""
     sGMN2Cod = ""
     sGMN3Cod = ""
     sGMN4Cod = ""
    
    sGMN1Proce = ""
    
    ConfigurarSeguridad
      
    CargarRecursos
    
    PonerFieldSeparator Me
        
    CargarAnyos
    
    CargarBuzLec
        
    CargarComboOrden
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    Set oProcesos = oFSGSRaiz.generar_CProcesos
    
    Set oEstados = oFSGSRaiz.generar_COfeEstados

    Set oMonedas = oFSGSRaiz.Generar_CMonedas
     
    CargarFechas
    
    sdbcMonCod.Enabled = False
    sdbcMonDen.Enabled = False
    OpAbiertos.Enabled = False
    OpCerrados.Enabled = False
    OpTodos.Enabled = False
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set oGruposMN1 = Nothing
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    Set oProcesos = Nothing
    Set oProceEncontrados = Nothing
    
    Set oProcesoSeleccionado = Nothing
    Set oProves = Nothing
    
    Set oProveedorSeleccionado = Nothing
    Set oIasig = Nothing
    
    Set oMonedas = Nothing
    Set oEstados = Nothing
    Set oOfertas = Nothing
    Set oOferta = Nothing
    Set oIOfertas = Nothing
    Set oOfertaSeleccionada = Nothing
   
End Sub

Private Sub ProceSelector1_Click(ByVal Opcion As SelectorDeProcesos.PSSeleccion)
    txtProceCod = ""
End Sub

Private Sub sdbcAnyo_Change()
    bRespetarCombo = True
    sdbcGMN1_4Cod = ""
    txtProceCod = ""
    txtEstMat = ""
    sdbcProveCod = ""
    sdbcProveDen = ""
    sdbcEstadoCod = ""
    sdbcEstadoDen = ""
    bRespetarCombo = False
End Sub

Private Sub sdbcAnyo_CloseUp()
    bRespetarCombo = True
    sdbcGMN1_4Cod = ""
    txtProceCod = ""
    txtEstMat = ""
    sdbcProveCod = ""
    sdbcProveDen = ""
    sdbcEstadoCod = ""
    sdbcEstadoDen = ""
    bRespetarCombo = False
End Sub

Private Sub sdbcEstadoCod_Change()
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcEstadoDen = ""
        bRespetarCombo = False
    End If
End Sub

Private Sub sdbcEstadoCod_Click()
    If Not sdbcEstadoCod.DroppedDown Then
        sdbcEstadoCod = ""
    End If
End Sub

Private Sub sdbcEstadoCod_CloseUp()
    If sdbcEstadoCod.Value = "..." Or sdbcEstadoCod = "" Then
        sdbcEstadoCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcEstadoDen.Text = sdbcEstadoCod.Columns(1).Text
    sdbcEstadoCod.Text = sdbcEstadoCod.Columns(0).Text
    bRespetarCombo = False

End Sub


Private Sub sdbcEstadoCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
    sdbcEstadoCod.RemoveAll
        
    Screen.MousePointer = vbHourglass
        
    oEstados.CargarTodosLosOfeEstados , , , , , , , basPublic.gParametrosInstalacion.gIdioma
    
    Codigos = oEstados.DevolverLosCodigos
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcEstadoCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
        Next
    
    sdbcEstadoCod.SelStart = 0
    sdbcEstadoCod.SelLength = Len(sdbcEstadoCod.Text)
    sdbcEstadoCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcEstadoCod_InitColumnProps()
    sdbcEstadoCod.DataFieldList = "Column 0"
    sdbcEstadoCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcEstadoCod_PositionList(ByVal Text As String)
PositionList sdbcEstadoCod, Text
End Sub


Private Sub sdbcEstadoCod_Validate(Cancel As Boolean)
    If sdbcEstadoCod.Text = "" Then Exit Sub
           
    If sdbcEstadoCod.Text = sdbcEstadoCod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcEstadoDen.Text = sdbcEstadoCod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    
    If sdbcEstadoCod.Text = sdbcEstadoDen.Columns(1).Text Then
        bRespetarCombo = True
        sdbcEstadoDen.Text = sdbcEstadoDen.Columns(0).Text
        bRespetarCombo = False
        Exit Sub
    End If

End Sub


Private Sub sdbcEstadoDen_Change()
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcEstadoCod = ""
        bRespetarCombo = False
    End If

End Sub

Private Sub sdbcEstadoDen_Click()
    If Not sdbcEstadoDen.DroppedDown Then
        sdbcEstadoDen = ""
    End If
End Sub


Private Sub sdbcEstadoDen_CloseUp()
    If sdbcEstadoDen.Value = "..." Or sdbcEstadoDen = "" Then
        sdbcEstadoDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcEstadoCod.Text = sdbcEstadoDen.Columns(1).Text
    sdbcEstadoDen.Text = sdbcEstadoDen.Columns(0).Text
    bRespetarCombo = False
End Sub


Private Sub sdbcEstadoDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
    sdbcEstadoDen.RemoveAll
        
    Screen.MousePointer = vbHourglass
        
    oEstados.CargarTodosLosOfeEstados , , , True, , , , basPublic.gParametrosInstalacion.gIdioma
    
    Codigos = oEstados.DevolverLosCodigos
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcEstadoDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
    
    sdbcEstadoDen.SelStart = 0
    sdbcEstadoDen.SelLength = Len(sdbcEstadoCod.Text)
    sdbcEstadoDen.Refresh
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcEstadoDen_InitColumnProps()
    sdbcEstadoDen.DataFieldList = "Column 0"
    sdbcEstadoDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcEstadoDen_PositionList(ByVal Text As String)
PositionList sdbcEstadoDen, Text
End Sub


Private Sub sdbcGMN1_4Cod_Change()
    If Not bRespetarCombo Then
    
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sGMN1Cod = "": sGMN2Cod = "": sGMN3Cod = "": sGMN4Cod = ""
        bRespetarCombo = True
        txtProceCod = ""
        txtEstMat = ""
        sdbcProveCod = ""
        sdbcProveDen = ""
        sdbcEstadoCod = ""
        sdbcEstadoDen = ""
        bRespetarCombo = False

    End If
End Sub


Private Sub sdbcGMN1_4Cod_CloseUp()
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    txtProceCod = ""
    txtEstMat = ""
    sdbcProveCod = ""
    sdbcProveDen = ""
    sdbcEstadoCod = ""
    sdbcEstadoDen = ""
    bRespetarCombo = False
    
    GMN1Seleccionado
    bCargarComboDesde = False
End Sub


Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
    
    sdbcGMN1_4Cod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
 
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
    
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
       
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub

Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    sGMN1Cod = "": sGMN2Cod = "": sGMN3Cod = "": sGMN4Cod = ""
    txtEstMat = ""
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
     
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            oMensajes.NoValido sMat
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)
            sGMN1Cod = oGMN1Seleccionado.Cod
            bCargarComboDesde = False
            
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
   
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            oMensajes.NoValido sMat
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
            sGMN1Cod = oGMN1Seleccionado.Cod
      
            bCargarComboDesde = False
        End If
    End If
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcMonCod_Change()
     If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcMonDen.Text = ""
        bRespetarCombo = False
        
        dequivalencia = 0
        sMoneda = ""
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcMonCod_CloseUp()
    If sdbcMonCod.Value = "..." Or Trim(sdbcMonCod.Value) = "" Then
        sdbcMonCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcMonDen.Text = sdbcMonCod.Columns(1).Text
    sdbcMonCod.Text = sdbcMonCod.Columns(0).Text
    dequivalencia = sdbcMonCod.Columns(2).Value
    sMoneda = sdbcMonCod.Columns(1).Text

    bRespetarCombo = False
    bCargarComboDesde = False
End Sub

Private Sub sdbcMonCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
Dim sDesde As String
Dim oMon As CMoneda

    If bCargarComboDesde Then
        sDesde = sdbcMonCod.Value
    Else
        sDesde = ""
    End If

    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, sDesde, , , True
    
    For Each oMon In oMonedas
        sdbcMonCod.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcMonCod_InitColumnProps()
    sdbcMonCod.DataFieldList = "Column 0"
    sdbcMonCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcMonCod_PositionList(ByVal Text As String)
PositionList sdbcMonCod, Text
End Sub

Private Sub sdbcMonCod_Validate(Cancel As Boolean)
Dim Codigos As TipoDatosCombo

If Trim(sdbcMonCod.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcMonCod.Value)) = UCase(Trim(sdbcMonCod.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcMonDen = sdbcMonCod.Columns(1).Value
        bRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedasDesde 1, sdbcMonCod.Text, , , True, , True
    
    If oMonedas.Item(1) Is Nothing Then
        oMensajes.NoValido sMON
        sdbcMonCod = ""
        dequivalencia = 0
        sMoneda = ""
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        If UCase(oMonedas.Item(1).Cod) <> UCase(sdbcMonCod.Value) Then
            oMensajes.NoValido sMON
            sdbcMonCod = ""
            dequivalencia = 0
            sMoneda = ""
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcMonDen = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            dequivalencia = oMonedas.Item(1).Equiv
            sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            
            sdbcMonCod.Columns(0).Text = sdbcMonCod.Text
            sdbcMonCod.Columns(1).Text = sdbcMonDen.Text
        
            bRespetarCombo = False
        End If
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcMonden_Change()
     If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcMonCod.Text = ""
        bRespetarCombo = False
        dequivalencia = 0
        sMoneda = ""
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcMonDen_CloseUp()
    If sdbcMonDen.Value = "....." Or Trim(sdbcMonDen.Value) = "" Then
        sdbcMonDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcMonDen.Text = sdbcMonDen.Columns(0).Text
    sdbcMonCod.Text = sdbcMonDen.Columns(1).Text
    dequivalencia = sdbcMonDen.Columns(2).Value
    sMoneda = sdbcMonDen.Columns(0).Text

    bRespetarCombo = False
    bCargarComboDesde = False
End Sub

Private Sub sdbcMonDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
Dim sDesde As String
Dim oMon As CMoneda

If bCargarComboDesde Then
    sDesde = sdbcMonDen.Value
Else
    sDesde = ""
End If

    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, , sDesde, True, True
    
    For Each oMon In oMonedas
        sdbcMonDen.AddItem oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Cod & Chr(m_lSeparador) & oMon.Equiv
    Next
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcMonDen_InitColumnProps()
    sdbcMonDen.DataFieldList = "Column 0"
    sdbcMonDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcMonDen_PositionList(ByVal Text As String)
PositionList sdbcMonDen, Text
End Sub

Private Sub sdbcMonDen_Validate(Cancel As Boolean)
Dim Codigos As TipoDatosCombo

If Trim(sdbcMonDen.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcMonDen.Value)) = UCase(Trim(sdbcMonDen.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcMonCod = sdbcMonDen.Columns(1).Value
        bRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedasDesde 1, , sdbcMonDen.Text, True, True, , True
    
    If oMonedas.Item(1) Is Nothing Then
        oMensajes.NoValido sMON
        sdbcMonDen = ""
        dequivalencia = 0
        sMoneda = ""
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        If UCase(oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) <> UCase(sdbcMonDen.Value) Then
            oMensajes.NoValido sMON
            sdbcMonDen = ""
            dequivalencia = 0
            sMoneda = ""
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcMonCod = oMonedas.Item(1).Cod
            dequivalencia = oMonedas.Item(1).Equiv
            sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            
            sdbcMonDen.Columns(1).Text = sdbcMonCod.Text
            sdbcMonDen.Columns(0).Text = sdbcMonDen.Text
            bRespetarCombo = False
        End If
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcOrdenar_click()
 Select Case sdbcOrdenar.Columns("NUM").Value
    Case 1
        OrdenListado = TipoOrdenacionBuzon.OrdPorProve
    Case 2
        OrdenListado = TipoOrdenacionBuzon.OrdPorProce
    Case 3
        OrdenListado = TipoOrdenacionBuzon.OrdPorFechaRecep
    Case 4
        OrdenListado = TipoOrdenacionBuzon.OrdPorFechaValidez
 End Select
End Sub

Private Sub sdbcProveCod_Change()
   If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveDen.Text = ""
        RespetarComboProve = False
    End If
End Sub

Private Sub sdbcProveCod_Click()
     If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

Private Sub sdbcProveCod_CloseUp()
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
End Sub

Private Sub sdbcProveCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer

    sdbcProveCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If txtProceCod.Text = "" Then
        If sdbcProveCod.Text <> "" Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveCod.Text
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos
        End If
    Else
        If sdbcProveCod.Text <> "" Then
            Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveCod.Text, "", False, OrdAsigPorCodProve)
        Else
            Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve)
        End If
    End If
            
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If ((txtProceCod.Text = "") And (Not oProves.EOF)) Then
        sdbcProveCod.AddItem "..."
    End If

    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        RespetarComboProve = True
        sdbcProveCod.Text = oProves.Item(1).Cod
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Text = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Text = sdbcProveDen.Text
            
        RespetarComboProve = False
    End If

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveDen_Change()
    If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveCod.Text = ""
        RespetarComboProve = False
    End If
End Sub

Private Sub sdbcProveDen_Click()
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

Private Sub sdbcProveDen_CloseUp()
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    RespetarComboProve = False
    DoEvents
End Sub

Private Sub sdbcProveDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer

    sdbcProveDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If txtProceCod.Text = "" Then

        If sdbcProveDen.Text <> "" Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , sdbcProveDen.Text, , , , , , , , , , , True
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , , , , , True
        End If
    Else
        If sdbcProveDen.Text <> "" Then
            Set oProves = oIasig.DevolverProveedoresDesde("", sdbcProveDen.Text, False, OrdAsigPorDenProve)
        Else
            Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorDenProve)
        End If
    End If
      
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If ((txtProceCod.Text = "") And (Not oProves.EOF)) Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveCod.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    If chkPrecios And (ProceSelector1.Seleccion = 7 Or ProceSelector1.Seleccion = 3) Then
        OpTodos.Enabled = True
        OpAbiertos.Enabled = True
        OpCerrados.Enabled = True
    Else
        OpTodos.Enabled = False
        OpAbiertos.Enabled = False
        OpCerrados.Enabled = False
    End If
End Sub

Private Sub Timer1_Timer()
   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If
End Sub

Private Sub txtProceCod_Change()
    bRespetarCombo = True
    sdbcProveCod = ""
    sdbcProveDen = ""
    sdbcEstadoCod = ""
    sdbcEstadoDen = ""
    bRespetarCombo = False
    
    If txtProceCod <> "" Then
        cmdBuscarProve.Enabled = False
    Else
        cmdBuscarProve.Enabled = True
    End If
End Sub

Private Sub txtProceCod_Validate(Cancel As Boolean)
    
    If txtProceCod.Text = "" Then Exit Sub
    
    If Not IsNumeric(txtProceCod) Then
        txtProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiCodigo
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If txtProceCod.Text > giMaxProceCod Then
        txtProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProceso
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If oProcesoSeleccionado Is Nothing Then ProcesoSeleccionado
        
    ''' Solo continuamos si existe el proceso
    oProcesos.CargarTodosLosProcesosDesde 1, sinitems, Cerrado, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo, sdbcGMN1_4Cod, , , , val(txtProceCod), , True, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, bRAsig, bREqpAsig, False, , , , bRCompResponsable
        
    If oProcesos.Count = 0 Then
        txtProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProceso
        Set oProcesoSeleccionado = Nothing
    Else
        Set oProcesoSeleccionado = Nothing
        Set oProcesoSeleccionado = oProcesos.Item(1)
        ProcesoSeleccionado
    End If
End Sub
