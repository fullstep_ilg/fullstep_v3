VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmArticuloEsp 
   BackColor       =   &H00808000&
   Caption         =   "Especificaciones"
   ClientHeight    =   3555
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7680
   Icon            =   "frmArticuloEsp.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3555
   ScaleWidth      =   7680
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picNavigate 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   305
      Left            =   5220
      ScaleHeight     =   300
      ScaleWidth      =   2415
      TabIndex        =   6
      Top             =   3060
      Width           =   2415
      Begin VB.CommandButton cmdAbrirEsp 
         Height          =   300
         Left            =   1920
         Picture         =   "frmArticuloEsp.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdSalvarEsp 
         Height          =   300
         Left            =   1440
         Picture         =   "frmArticuloEsp.frx":0D2E
         Style           =   1  'Graphical
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdEliminarEsp 
         Height          =   300
         Left            =   480
         Picture         =   "frmArticuloEsp.frx":0DAF
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdA�adirEsp 
         Height          =   300
         Left            =   0
         Picture         =   "frmArticuloEsp.frx":0E32
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdModificarEsp 
         Height          =   300
         Left            =   960
         Picture         =   "frmArticuloEsp.frx":0EA4
         Style           =   1  'Graphical
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
   End
   Begin VB.CommandButton cmdRestaurar 
      Caption         =   "&Restaurar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   120
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   3120
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.CommandButton cmdListado 
      Caption         =   "&Listado"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1290
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   3120
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.PictureBox picEspGen 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1335
      Left            =   0
      ScaleHeight     =   1335
      ScaleWidth      =   7575
      TabIndex        =   2
      Top             =   120
      Width           =   7575
      Begin VB.TextBox txtArticuloEsp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1275
         Left            =   120
         MaxLength       =   800
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   0
         Top             =   0
         Width           =   7440
      End
   End
   Begin MSComctlLib.ListView lstvwEsp 
      Height          =   1335
      Left            =   120
      TabIndex        =   1
      Top             =   1680
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   2355
      View            =   3
      Arrange         =   1
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      HotTracking     =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Fichero"
         Object.Width           =   3233
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Tamanyo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Path"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Comentario"
         Object.Width           =   6920
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Fecha"
         Object.Width           =   2868
      EndProperty
   End
   Begin MSComDlg.CommonDialog cmmdEsp 
      Left            =   120
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   600
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmArticuloEsp.frx":0FEE
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Archivos adjuntos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   3
      Top             =   1440
      Width           =   1410
   End
End
Attribute VB_Name = "frmArticuloEsp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oArticulo As CArticulo
Public g_oIBaseDatos As IBaseDatos
Public g_bCancelarEsp As Boolean
Public g_sComentario As String
Public g_bRespetarCombo As Boolean
Public g_sOrigen As String
Public g_bSoloRuta As Boolean

Private m_sayFileNames() As String
Private Accion As accionessummit

Private m_sIdioma() As String
Private m_skb As String

Private Sub cmdAbrirEsp_Click()
Dim Articulo As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oFos As FileSystemObject
Dim oFile As Scripting.File

On Error GoTo Cancelar:

Set Articulo = lstvwEsp.selectedItem

If Articulo Is Nothing Then
    oMensajes.SeleccioneFichero
    
Else
    
    sFileName = FSGSLibrary.DevolverPathFichTemp
    sFileName = sFileName & Articulo.Text
    sFileTitle = Articulo.Text
     
    'Comprueba si existe el fichero y si existe se borra:
    Dim FOSFile As Scripting.FileSystemObject
    Set FOSFile = New Scripting.FileSystemObject
    If FOSFile.FileExists(sFileName) Then
        FOSFile.DeleteFile sFileName, True
    End If
    Set FOSFile = Nothing
    
    ' Cargamos el contenido en la esp.
    Set oEsp = g_oArticulo.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
   
    If Not IsNull(oEsp.Ruta) Then 'los archivos son vinculados
        Set oFos = New FileSystemObject
        ''''estas cuatro lineas te devuelven un error debido a que no tiene permiso de acceso pero no devuelve el numero de
        Set oFile = oFos.GetFile(oEsp.Ruta & oEsp.nombre)
        oFile.OpenAsTextStream.ReadAll ''error

        If oFos.FolderExists(oEsp.Ruta) And oFos.FileExists(oEsp.Ruta & oEsp.nombre) Then
            ShellExecute MDI.hWnd, "Open", oEsp.Ruta & oEsp.nombre, 0&, oEsp.Ruta, 1
            
        Else
            
            oMensajes.ImposibleModificarEsp (oEsp.nombre & " - " & oEsp.Ruta)
        End If
    Else ' los archivos son adjuntos, se leen de base de datos
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspArticulo)
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oEsp = Nothing
            Exit Sub
        End If
        
        Dim arrData() As Byte
           
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspArticulo, oEsp.DataSize, sFileName
        
        'Lanzamos la aplicacion
        ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    End If
        
End If

Screen.MousePointer = vbNormal
Set oEsp = Nothing
Set oFos = Nothing

Exit Sub

Cancelar:
    If err.Number = 53 Or err.Number = 70 Then
        oMensajes.ImposibleAccederAFichero
    End If
    
    If err.Number = 62 Then
        'El fichero se encuentra vac�o, pero lo abrimos
        Resume Next
    End If
    
    Screen.MousePointer = vbNormal
            
    Set oEsp = Nothing
    Set oFos = Nothing
End Sub

Private Sub cmdA�adirEsp_Click()
Dim DataFile As Integer
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim sdrive As String
Dim sdrive2 As String
Dim sRuta As String
Dim oFos As FileSystemObject
Dim oDrive As Scripting.Drive
Dim arrFileNames As Variant
Dim iFile As Integer

On Error GoTo Cancelar:
cmmdEsp.filename = ""
cmmdEsp.DialogTitle = m_sIdioma(1)
cmmdEsp.Filter = m_sIdioma(2) & "|*.*"
cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
cmmdEsp.ShowOpen

sFileName = cmmdEsp.filename
sFileTitle = cmmdEsp.FileTitle


arrFileNames = ExtraerFicheros(sFileName)
' Ahora obtenemos el comentario para la especificacion
frmPROCEComFich.sOrigen = "frmArticuloEsp"
frmPROCEComFich.chkProcFich.Visible = True
If UBound(arrFileNames) = 1 Then
    frmPROCEComFich.lblFich = arrFileNames(0) & "\" & arrFileNames(1)
Else
    frmPROCEComFich.lblFich = ""
    frmPROCEComFich.Label1.Visible = False
    frmPROCEComFich.lblFich.Visible = False
    frmPROCEComFich.txtCom.Top = frmPROCEComFich.chkProcFich.Top
    frmPROCEComFich.chkProcFich.Top = frmPROCEComFich.Label1.Top
    frmPROCEComFich.txtCom.Height = 1850
End If
frmPROCEComFich.Show 1
    
If g_bSoloRuta = False Then 'Si es adjunto no pasa por aqui
    If Not g_bCancelarEsp Then
        Set oFos = New Scripting.FileSystemObject
        For iFile = 1 To UBound(arrFileNames)
            sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
            sFileTitle = arrFileNames(iFile)
            Set oEsp = oFSGSRaiz.generar_CEspecificacion
            Set oEsp.Articulo = g_oArticulo
            oEsp.nombre = sFileTitle
            oEsp.Comentario = g_sComentario
            
            teserror = oEsp.ComenzarEscrituraData(TipoEspecificacion.EspArticulo)
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set oEsp = Nothing
                Exit Sub
            End If
               
            Dim sAdjunto As String
            Dim ArrayAdjunto() As String
            Dim oFile As File
            Dim bites As Long
               
            Set oFile = oFos.GetFile(sFileName)
            bites = oFile.Size
            oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                
            sAdjunto = oEsp.GrabarAdjunto(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites, TipoEspecificacion.EspArticulo)
                
            'Creamos un array, cada "substring" se asignar�
            'a un elemento del array
            ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
            oEsp.Id = ArrayAdjunto(0)
            oEsp.DataSize = bites

            g_oArticulo.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, , , oEsp.Comentario, , oEsp.Articulo, , , , , oEsp.DataSize
            
            g_oArticulo.EspAdj = 1
            lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "PATH", oEsp.Ruta
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
            
            basSeguridad.RegistrarAccion accionessummit.ACCArtMod, "Cod:" & g_oArticulo.Cod
        Next iFile
    End If
    lstvwEsp.Refresh
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Exit Sub

Else ' Guardamos la ruta en lugar del archivo
    If Not g_bCancelarEsp Then
        Set oFos = New FileSystemObject
        For iFile = 1 To UBound(arrFileNames)
            sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
            sFileTitle = arrFileNames(iFile)
            cmmdEsp.Filter = m_sIdioma(2) & "|*.*"
            sdrive = oFos.GetDriveName(sFileName)
            sRuta = Left(sFileName, Len(sFileName) - Len(sFileTitle))
            sRuta = Right(sRuta, Len(sRuta) - Len(sdrive))
            Set oDrive = oFos.GetDrive(sdrive)
            sdrive2 = oDrive.ShareName
            
            If sdrive2 = "" Then
                sRuta = sdrive & sRuta
            Else
                sRuta = sdrive2 & sRuta
            End If
            
            If sRuta = "" Then Exit Sub
              
            Set oEsp = oFSGSRaiz.generar_CEspecificacion
            Set oEsp.Articulo = g_oArticulo
            oEsp.nombre = sFileTitle
            oEsp.Comentario = g_sComentario
            oEsp.Ruta = sRuta
            oEsp.DataSize = FileLen(sFileName)
            Set g_oIBaseDatos = oEsp
            
            teserror = g_oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oEsp = Nothing
                Set oDrive = Nothing
                Set oFos = Nothing
                Exit Sub
            End If
        
            g_oArticulo.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, , , oEsp.Comentario, , oEsp.Articulo, , , oEsp.Ruta
        
            g_oArticulo.EspAdj = 1
            lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Path", oEsp.Ruta
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
        
            basSeguridad.RegistrarAccion accionessummit.ACCArtMod, "Cod:" & g_oArticulo.Cod
        Next
    End If
    
    lstvwEsp.Refresh
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Set oFos = Nothing
    Set oDrive = Nothing
    Exit Sub
    
End If

Cancelar:
    
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        
        MsgBox err.Description
        
        Close DataFile
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
        Set oFos = Nothing
        Set oDrive = Nothing
    End If
End Sub

Private Sub cmdEliminarEsp_Click()
Dim Articulo As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
        
On Error GoTo Cancelar:

Set Articulo = lstvwEsp.selectedItem

If Articulo Is Nothing Then
    oMensajes.SeleccioneFichero

Else
    
    irespuesta = oMensajes.PreguntaEliminar(m_sIdioma(3) & ": " & lstvwEsp.selectedItem.Text)
    If irespuesta = vbNo Then Exit Sub
    Set oEsp = g_oArticulo.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
    Set g_oIBaseDatos = oEsp
    
    teserror = g_oIBaseDatos.EliminarDeBaseDatos
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
    Else
        g_oArticulo.especificaciones.Remove (CStr(lstvwEsp.selectedItem.Tag))
        If g_oArticulo.especificaciones.Count = 0 Then
            If txtArticuloEsp = "" Then
                g_oArticulo.EspAdj = 0
            End If
        End If
        basSeguridad.RegistrarAccion accionessummit.ACCArtEli, "Cod:" & g_oArticulo.Cod
        lstvwEsp.ListItems.Remove (CStr(lstvwEsp.selectedItem.key))
    End If
    
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
End If
    
Cancelar:
    
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
        

End Sub


Private Sub cmdModificarEsp_Click()
Dim teserror As TipoErrorSummit

If lstvwEsp.selectedItem Is Nothing Then
    oMensajes.SeleccioneFichero
Else
    
    Set g_oIBaseDatos = g_oArticulo.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
    teserror = g_oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set g_oIBaseDatos = Nothing
        Exit Sub
    End If
    
    basSeguridad.RegistrarAccion accionessummit.ACCArtMod, "Cod:" & g_oArticulo.Cod
    
    Set frmPROCEEspMod.g_oIBaseDatos = g_oIBaseDatos
    frmPROCEEspMod.g_sOrigen = "frmArticuloEsp"
    frmPROCEEspMod.Show 1

End If

End Sub

Private Sub cmdSalvarEsp_Click()
Dim Articulo As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oFos As FileSystemObject
   
On Error GoTo Cancelar:

Set Articulo = lstvwEsp.selectedItem

If Articulo Is Nothing Then
    oMensajes.SeleccioneFichero

Else
    Set oFos = New FileSystemObject
    cmmdEsp.DialogTitle = m_sIdioma(6)
    cmmdEsp.Filter = m_sIdioma(7) & "|*.*"

    cmmdEsp.filename = Articulo.Text
    cmmdEsp.ShowSave
            
    sFileName = cmmdEsp.filename
    sFileTitle = cmmdEsp.FileTitle
    If sFileTitle = "" Then
        oMensajes.NoValido m_sIdioma(8)
        Exit Sub
    End If
    
    Set oEsp = g_oArticulo.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
    
    If IsNull(oEsp.Ruta) Then 'Los archivos son adjuntos
        ' Cargamos el contenido en la esp.
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspArticulo)
    
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oEsp = Nothing
            Exit Sub
        End If
        
        Dim arrData() As Byte
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspArticulo, oEsp.DataSize, sFileName
    
    Else ' los archivos son vinculados
        Set oFos = New FileSystemObject
        If oFos.FolderExists(oEsp.Ruta) And oFos.FileExists(oEsp.Ruta & oEsp.nombre) Then
            oFos.CopyFile oEsp.Ruta & oEsp.nombre, sFileName, False
            Exit Sub
        Else
            oMensajes.ImposibleModificarEsp (oEsp.nombre & " - " & oEsp.Ruta)
            
        End If
        
        Set oFos = Nothing
        Set oEsp = Nothing
    End If
      
End If
    
Cancelar:
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
        Set oFos = Nothing
    End If
End Sub

Public Sub AnyadirEspsALista()
Dim oEsp As CEspecificacion

    lstvwEsp.ListItems.clear
    
    For Each oEsp In g_oArticulo.especificaciones
        lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Path", NullToStr(oEsp.Ruta)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next
    
    If g_oArticulo.especificaciones.Count = 0 And txtArticuloEsp.Locked = True Then
        Me.cmdAbrirEsp.Enabled = False
        Me.cmdSalvarEsp.Enabled = False
    End If
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ARTICULOESP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim m_sIdioma(1 To 13)
        For i = 1 To 13
            m_sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        frmArticuloEsp.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        lstvwEsp.ColumnHeaders(1).Text = Ador(0).Value
        Ador.MoveNext
        
        lstvwEsp.ColumnHeaders(4).Text = Ador(0).Value
        Ador.MoveNext
        lstvwEsp.ColumnHeaders(5).Text = Ador(0).Value
        Ador.MoveNext
        lstvwEsp.ColumnHeaders(3).Text = Ador(0).Value
        Ador.MoveNext
        lstvwEsp.ColumnHeaders(2).Text = Ador(0).Value
        Ador.MoveNext
        m_skb = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub
Private Sub Form_Load()
Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
CargarRecursos
'Array que contendra los ficheros eliminados
ReDim m_sayFileNames(0)
Accion = ACCArtCon

End Sub

Private Sub Form_Resize()
    
    If Me.Height < 2800 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    lstvwEsp.Height = Me.Height * 0.33
    picEspGen.Height = Me.Height - lstvwEsp.Height - 1240
    txtArticuloEsp.Height = picEspGen.Height - 60
    Label1.Top = picEspGen.Top + picEspGen.Height
    lstvwEsp.Top = Label1.Top + 240
    
    picEspGen.Width = Me.Width - 200
    lstvwEsp.Width = Me.Width - 345
    txtArticuloEsp.Width = lstvwEsp.Width
    lstvwEsp.ColumnHeaders(1).Width = lstvwEsp.Width * 0.25
    lstvwEsp.ColumnHeaders(2).Width = lstvwEsp.Width * 0.1
    lstvwEsp.ColumnHeaders(3).Width = lstvwEsp.Width * 0.25
    lstvwEsp.ColumnHeaders(4).Width = lstvwEsp.Width * 0.25
    lstvwEsp.ColumnHeaders(5).Width = lstvwEsp.Width * 0.13
    
    cmdRestaurar.Top = Me.Height - 840
    cmdListado.Top = cmdRestaurar.Top
    picNavigate.Top = cmdRestaurar.Top
    picNavigate.Left = lstvwEsp.Left + lstvwEsp.Width - picNavigate.Width + 60
    cmdA�adirEsp.ToolTipText = m_sIdioma(9)
    cmdEliminarEsp.ToolTipText = m_sIdioma(10)
    cmdModificarEsp.ToolTipText = m_sIdioma(11)
    cmdSalvarEsp.ToolTipText = m_sIdioma(12)
    cmdAbrirEsp.ToolTipText = m_sIdioma(13)
    

End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim i As Integer
Dim FOSFile As Scripting.FileSystemObject
Dim bBorrando As Boolean
Dim irespuesta As Integer
        
    Select Case Accion
    
        Case accionessummit.ACCArtMod
                
                txtArticuloEsp_Validate (irespuesta)
    End Select
    
    Select Case g_sOrigen
        Case "frmItemsWizard"
            frmItemsWizard.sdbgArticulos.SelBookmarks.RemoveAll
        Case "frmESTRMAT"
            frmESTRMAT.sdbgArticulos.SelBookmarks.RemoveAll
    End Select
    
On Error GoTo Error
      'Borramos los archivos temporales que hayamos creado
    Set FOSFile = New Scripting.FileSystemObject
    
    i = 0
    While i < UBound(m_sayFileNames)
        bBorrando = True
        If FOSFile.FileExists(m_sayFileNames(i)) Then
            FOSFile.DeleteFile m_sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
    Set FOSFile = Nothing
    
    Exit Sub

Error:
    
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = m_sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If
        
End Sub


Private Sub txtArticuloEsp_Change()
    If Not g_bRespetarCombo Then
        If Accion <> ACCArtMod Then
            Accion = ACCArtMod
        End If
    End If
    
End Sub

Private Sub txtArticuloEsp_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit
Dim bGuardar As Boolean

If Accion = ACCArtMod Then
        
    If StrComp(NullToStr(g_oArticulo.esp), txtArticuloEsp, vbTextCompare) <> 0 Then
            
        bGuardar = True
        
        g_oArticulo.esp = txtArticuloEsp
        If txtArticuloEsp <> "" Then
            g_oArticulo.EspAdj = 1
        Else
            If g_oArticulo.especificaciones.Count = 0 Then
                g_oArticulo.EspAdj = 0
            End If
        End If
        
        If bGuardar = True Then
            teserror = g_oArticulo.ModificarEspecificacion
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Cancel = True
                Accion = ACCArtCon
                Exit Sub
            End If
        End If
        
        'Registrar accion
        Accion = ACCArtCon
    
    End If
End If

End Sub
