VERSION 5.00
Begin VB.Form frmAtrib 
   Caption         =   "DAtributos"
   ClientHeight    =   7245
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10740
   Icon            =   "frmATRIB.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7245
   ScaleWidth      =   10740
   Begin FSGSClient.ctlATRIB ctlAtributos 
      Height          =   6975
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   10186
   End
End
Attribute VB_Name = "frmAtrib"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String
Private m_sMsgError As String
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Public Property Get g_bCodigoCancelar() As Boolean
    g_bCodigoCancelar = ctlAtributos.g_bCodigoCancelar
End Property

Public Property Let g_bCodigoCancelar(ByVal bNewValue As Boolean)
    ctlAtributos.g_bCodigoCancelar = bNewValue
End Property

Public Property Get g_sCodigoNuevo() As String
    g_sCodigoNuevo = ctlAtributos.g_sCodigoNuevo
End Property

Public Property Let g_sCodigoNuevo(ByVal sNewValue As String)
    ctlAtributos.g_sCodigoNuevo = sNewValue
End Property

Public Property Get g_oOrigen() As Form
    Set g_oOrigen = ctlAtributos.g_oOrigen
End Property

Public Property Set g_oOrigen(ByVal vNewValue As Form)
    Set ctlAtributos.g_oOrigen = vNewValue
End Property

Public Property Get bRComprador() As Boolean
    bRComprador = ctlAtributos.bRComprador
End Property

Public Property Let bRComprador(ByVal vNewValue As Boolean)
    ctlAtributos.bRComprador = vNewValue
End Property

Public Property Get sAmbitoAtribEsp() As String
    sAmbitoAtribEsp = ctlAtributos.sAmbitoAtribEsp
End Property

Public Property Let sAmbitoAtribEsp(ByVal vNewValue As String)
    ctlAtributos.sAmbitoAtribEsp = vNewValue
End Property

Public Property Get sdbcGMN1_4Cod() As SSDBCombo
    Set sdbcGMN1_4Cod = ctlAtributos.sdbcGMN1_4Cod
End Property

Public Property Get sdbcGMN1_4Den() As SSDBCombo
    Set sdbcGMN1_4Den = ctlAtributos.sdbcGMN1_4Den
End Property

Public Property Get sdbcGMN2_4Cod() As SSDBCombo
    Set sdbcGMN2_4Cod = ctlAtributos.sdbcGMN2_4Cod
End Property

Public Property Get sdbcGMN2_4Den() As SSDBCombo
    Set sdbcGMN2_4Den = ctlAtributos.sdbcGMN2_4Den
End Property

Public Property Get sdbcGMN3_4Cod() As SSDBCombo
    Set sdbcGMN3_4Cod = ctlAtributos.sdbcGMN3_4Cod
End Property

Public Property Get sdbcGMN3_4Den() As SSDBCombo
    Set sdbcGMN3_4Den = ctlAtributos.sdbcGMN3_4Den
End Property

Public Property Get sdbcGMN4_4Cod() As SSDBCombo
    Set sdbcGMN4_4Cod = ctlAtributos.sdbcGMN4_4Cod
End Property

Public Property Get sdbcGMN4_4Den() As SSDBCombo
    Set sdbcGMN4_4Den = ctlAtributos.sdbcGMN4_4Den
End Property

Public Property Get sdbgAtributosGrupo() As SSDBGrid
    Set sdbgAtributosGrupo = ctlAtributos.sdbgAtributosGrupo
End Property

Public Property Get sstabGeneral() As SSTab
    Set sstabGeneral = ctlAtributos.sstabGeneral
End Property

Public Property Get g_oAtributoEnEdicion() As CAtributo
    Set g_oAtributoEnEdicion = ctlAtributos.g_oAtributoEnEdicion
End Property

Public Property Set g_oAtributoEnEdicion(ByVal vNewValue As CAtributo)
    Set ctlAtributos.g_oAtributoEnEdicion = vNewValue
End Property

Public Property Get m_bRespuesta() As Boolean
    m_bRespuesta = ctlAtributos.m_bRespuesta
End Property

Public Property Let m_bRespuesta(ByVal vNewValue As Boolean)
    ctlAtributos.m_bRespuesta = vNewValue
End Property

Public Property Get iAmbitoAtribEsp() As Integer
    iAmbitoAtribEsp = ctlAtributos.iAmbitoAtribEsp
End Property

Public Property Let iAmbitoAtribEsp(ByVal vNewValue As Integer)
    ctlAtributos.iAmbitoAtribEsp = vNewValue
End Property

Public Property Get g_sArtCod() As String
    g_sArtCod = ctlAtributos.g_sArtCod
End Property

Public Property Let g_sArtCod(ByVal vNewValue As String)
    ctlAtributos.g_sArtCod = vNewValue
End Property

Public Property Get g_sGmn1() As String
    g_sGmn1 = ctlAtributos.g_sGmn1
End Property

Public Property Let g_sGmn1(ByVal vNewValue As String)
    ctlAtributos.g_sGmn1 = vNewValue
End Property

Public Property Get g_sGmn2() As String
    g_sGmn2 = ctlAtributos.g_sGmn2
End Property

Public Property Let g_sGmn2(ByVal vNewValue As String)
    ctlAtributos.g_sGmn2 = vNewValue
End Property

Public Property Get g_sGmn3() As String
    g_sGmn3 = ctlAtributos.g_sGmn3
End Property

Public Property Let g_sGmn3(ByVal vNewValue As String)
    ctlAtributos.g_sGmn3 = vNewValue
End Property

Public Property Get g_sGmn4() As String
    g_sGmn4 = ctlAtributos.g_sGmn4
End Property

Public Property Let g_sGmn4(ByVal vNewValue As String)
    ctlAtributos.g_sGmn4 = vNewValue
End Property

Public Property Get cmdSeleccionar() As CommandButton
    Set cmdSeleccionar = ctlAtributos.cmdSeleccionar
End Property

Public Property Get cmdModoEdicion() As CommandButton
    Set cmdModoEdicion = ctlAtributos.cmdModoEdicion
End Property

Public Property Get g_GMN1RespetarCombo() As Boolean
    g_GMN1RespetarCombo = ctlAtributos.g_GMN1RespetarCombo
End Property

Public Property Let g_GMN1RespetarCombo(ByVal vNewValue As Boolean)
    ctlAtributos.g_GMN1RespetarCombo = vNewValue
End Property

Public Property Get g_GMN2RespetarCombo() As Boolean
    g_GMN2RespetarCombo = ctlAtributos.g_GMN2RespetarCombo
End Property

Public Property Let g_GMN2RespetarCombo(ByVal vNewValue As Boolean)
    ctlAtributos.g_GMN2RespetarCombo = vNewValue
End Property

Public Property Get g_GMN3RespetarCombo() As Boolean
    g_GMN3RespetarCombo = ctlAtributos.g_GMN3RespetarCombo
End Property

Public Property Let g_GMN3RespetarCombo(ByVal vNewValue As Boolean)
    ctlAtributos.g_GMN3RespetarCombo = vNewValue
End Property

Public Property Get g_GMN4RespetarCombo() As Boolean
    g_GMN4RespetarCombo = ctlAtributos.g_GMN4RespetarCombo
End Property

Public Property Let g_GMN4RespetarCombo(ByVal vNewValue As Boolean)
    ctlAtributos.g_GMN4RespetarCombo = vNewValue
End Property

Public Property Get txtCod() As TextBox
    Set txtCod = ctlAtributos.txtCod
End Property

Public Property Get g_idAtribCod() As Variant
    g_idAtribCod = ctlAtributos.g_idAtribCod
End Property

Public Property Let g_idAtribCod(ByVal vNewValue As Variant)
    ctlAtributos.g_idAtribCod = vNewValue
End Property

Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)
    ctlAtributos.sdbcGMN1_4CodCtrl_Validate Cancel
End Sub

Public Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)
    ctlAtributos.sdbcGMN2_4CodCtrl_Validate Cancel
End Sub

Public Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)
    ctlAtributos.sdbcGMN3_4CodCtrl_Validate Cancel
End Sub

Public Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)
    ctlAtributos.sdbcGMN4_4CodCtrl_Validate Cancel
End Sub

'3328
Public Property Get g_sPedId() As String
    g_sPedId = ctlAtributos.g_sPedId
End Property

Public Property Let g_sPedId(ByVal vNewValue As String)
    ctlAtributos.g_sPedId = vNewValue
End Property

Public Property Get UonsSeleccionadas() As CUnidadesOrganizativas
    Set UonsSeleccionadas = ctlAtributos.UonsSeleccionadas
End Property

Public Property Set UonsSeleccionadas(ByRef oUons As CUnidadesOrganizativas)
    Set ctlAtributos.UonsSeleccionadas = oUons
End Property


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmAtrib", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, True
m_bActivado = False
    ctlAtributos.SetContainer Me
    
    Me.Width = 10600
    Me.Height = 6120
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
    CargarRecursos
    
    ctlAtributos.g_sOrigen = g_sOrigen
    ctlAtributos.Initialize
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmAtrib", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Carga los recursos necesarios del modulo</summary>
''' <returns></returns>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ATRIB, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        For i = 1 To 19
            Ador.MoveNext
        Next
        Me.caption = Ador(0).Value
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmAtrib", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ctlAtributos.Height = Me.Height - 500
    ctlAtributos.Width = Me.Width - 140
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub


Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    Me.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmAtrib", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

'edu T94
'nueva funcion que devuelve el atributo seleccionado
'llamando a la funcion de interfaz DevolverAtributo del objeto origen
'de momento se usa solo se usa desde el control de usuario crlEspecificacionProceso usado en frmPROCE

Public Function Seleccionar(Origen As Object) As CAtributo
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.Show
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmAtrib", "Seleccionar", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
