
The following purchasing condition @PROCESO
has not been transferred to the ERP.

For the following FULLSTEP GS codes, no corresponding ERP code has been found. 
Please, fill in the corresponding ERP codes through the "Update intermediate tables" Access application 
for the indicated trade names.

<EMPRESA>
============================================================	
Trade : @NIF - @DEN
Supplier ERP code: @PROVE
	
Measure Units
=============
@UNI
	
Currencies
==========
@MON

Payment terms
=============
@PAG
============================================================
</EMPRESA>

regards,




