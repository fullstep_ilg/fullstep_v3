VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPedidoActAtribSol 
   Caption         =   "Traspaso de pedido al ERP"
   ClientHeight    =   6960
   ClientLeft      =   43260
   ClientTop       =   450
   ClientWidth     =   13680
   Icon            =   "frmPedidoActAtribSol.frx":0000
   LinkTopic       =   "frmTraspasoPedERP"
   LockControls    =   -1  'True
   ScaleHeight     =   6960
   ScaleWidth      =   13680
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Alerta_peq 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   120
      Picture         =   "frmPedidoActAtribSol.frx":014A
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   13
      Top             =   200
      Width           =   495
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValorItem 
      Height          =   795
      Left            =   8760
      TabIndex        =   12
      Top             =   4710
      Width           =   2805
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ORDEN"
      Columns(0).Name =   "ORDEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "VALOR"
      Columns(1).Name =   "VALOR"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4948
      _ExtentY        =   1402
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValorProce 
      Height          =   795
      Left            =   8580
      TabIndex        =   11
      Top             =   2550
      Width           =   2805
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ORDEN"
      Columns(0).Name =   "ORDEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "VALOR"
      Columns(1).Name =   "VALOR"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4948
      _ExtentY        =   1402
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.PictureBox picProce 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   240
      ScaleHeight     =   375
      ScaleWidth      =   1785
      TabIndex        =   9
      Top             =   1230
      Width           =   1785
      Begin VB.Label lblAtribProce 
         Appearance      =   0  'Flat
         Caption         =   "Atributos de proceso:"
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   30
         TabIndex        =   10
         Top             =   60
         Width           =   1695
      End
   End
   Begin VB.PictureBox picItem 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   270
      ScaleHeight     =   405
      ScaleWidth      =   1575
      TabIndex        =   7
      Top             =   3690
      Width           =   1575
      Begin VB.Label lblAtribItem 
         Caption         =   "Atributos de �tem:"
         Height          =   405
         Left            =   0
         TabIndex        =   8
         Top             =   120
         Width           =   1545
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   510
      Left            =   0
      ScaleHeight     =   510
      ScaleWidth      =   13680
      TabIndex        =   1
      Top             =   6444
      Width           =   13680
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   5460
         TabIndex        =   3
         Top             =   60
         Width           =   1275
      End
      Begin VB.CommandButton cmdSiguiente 
         Caption         =   "Siguiente"
         Height          =   375
         Left            =   4050
         TabIndex        =   2
         Top             =   60
         Width           =   1275
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAtribProce 
      Height          =   2010
      Left            =   300
      TabIndex        =   5
      Top             =   1680
      Width           =   11805
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   8
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPedidoActAtribSol.frx":0588
      stylesets(1).Name=   "NormalInterno"
      stylesets(1).BackColor=   16777152
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmPedidoActAtribSol.frx":05A4
      stylesets(1).AlignmentPicture=   1
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   2487
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasHeadForeColor=   -1  'True
      Columns(0).HasHeadBackColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).HeadBackColor=   -2147483633
      Columns(0).BackColor=   16777152
      Columns(1).Width=   6535
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   200
      Columns(1).Locked=   -1  'True
      Columns(1).HasHeadForeColor=   -1  'True
      Columns(1).HasHeadBackColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).HeadBackColor=   -2147483633
      Columns(1).BackColor=   16777152
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Valor"
      Columns(2).Name =   "VALOR"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   800
      Columns(2).HasHeadForeColor=   -1  'True
      Columns(2).HasHeadBackColor=   -1  'True
      Columns(2).HeadBackColor=   -2147483633
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID_A"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "INTERNO"
      Columns(4).Name =   "INTERNO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   11
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "VALOR_PED"
      Columns(5).Name =   "VALOR_PED"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   0
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "VALOR_SOL"
      Columns(6).Name =   "VALOR_SOL"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   0
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Name =   "CHECK"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      _ExtentX        =   20823
      _ExtentY        =   3545
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAtribItem 
      Height          =   2010
      Left            =   300
      TabIndex        =   6
      Top             =   4110
      Width           =   11805
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   5
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPedidoActAtribSol.frx":07F1
      stylesets(1).Name=   "NormalInterno"
      stylesets(1).BackColor=   16777152
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmPedidoActAtribSol.frx":080D
      stylesets(1).AlignmentPicture=   1
      UseGroups       =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Groups.Count    =   2
      Groups(0).Width =   9208
      Groups(0).Columns.Count=   2
      Groups(0).Columns(0).Width=   979
      Groups(0).Columns(0).Name=   "NUM_LINEA"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   8229
      Groups(0).Columns(1).Caption=   "Art�culo"
      Groups(0).Columns(1).Name=   "ARTICULO"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).BackColor=   16777160
      Groups(1).Width =   5715
      Groups(1).Columns.Count=   3
      Groups(1).Columns(0).Width=   2064
      Groups(1).Columns(0).Caption=   "Precio U.P."
      Groups(1).Columns(0).Name=   "PRECIOUP"
      Groups(1).Columns(0).Alignment=   1
      Groups(1).Columns(0).CaptionAlignment=   2
      Groups(1).Columns(0).DataField=   "Column 2"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Locked=   -1  'True
      Groups(1).Columns(0).HasBackColor=   -1  'True
      Groups(1).Columns(0).BackColor=   11662320
      Groups(1).Columns(1).Width=   1799
      Groups(1).Columns(1).Caption=   "Cantidad"
      Groups(1).Columns(1).Name=   "CANTIDAD"
      Groups(1).Columns(1).Alignment=   1
      Groups(1).Columns(1).CaptionAlignment=   2
      Groups(1).Columns(1).DataField=   "Column 3"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Locked=   -1  'True
      Groups(1).Columns(1).HasBackColor=   -1  'True
      Groups(1).Columns(1).BackColor=   11662320
      Groups(1).Columns(2).Width=   1852
      Groups(1).Columns(2).Caption=   "Unidad"
      Groups(1).Columns(2).Name=   "UNIDAD"
      Groups(1).Columns(2).DataField=   "Column 4"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(2).Locked=   -1  'True
      Groups(1).Columns(2).HasBackColor=   -1  'True
      Groups(1).Columns(2).BackColor=   11662320
      _ExtentX        =   20823
      _ExtentY        =   3545
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TabStrip sstabAtrib 
      Height          =   5535
      Left            =   120
      TabIndex        =   4
      Top             =   810
      Width           =   12195
      _ExtentX        =   21511
      _ExtentY        =   9763
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInfo1 
      Caption         =   $"frmPedidoActAtribSol.frx":0A5A
      Height          =   545
      Left            =   650
      TabIndex        =   0
      Top             =   260
      Width           =   11400
   End
End
Attribute VB_Name = "frmPedidoActAtribSol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables publicas
Public g_sDatoERp As String
Public g_oOrdenEntrega As COrdenEntrega
Public g_oLineaSeleccionada As CLineaPedido

Public g_oOrigen As Form

Public g_oOrdenesAux As COrdenesEntrega
Public g_oOrdenAux As COrdenEntrega

'Bot�n Cancelar
Public g_Cancelar As Boolean

'Variables privadas
Private m_oAtributosProce As CAtributos
Private m_oAtributosItem As CAtributos
Private m_oProves As CProveedores
Private m_oERPsInt As CERPsInt

'variables Textos
Private m_sIdiFalse As String
Private m_sIdiTrue As String

'Variables textos
Private m_sLayOut As String

Private sIdiMayor As String
Private sIdiMenor As String
Private sIdiEntre As String
Private m_sMoneda As String

Private m_bRReceptorUsu As Boolean

Private m_iGrupoFinAtrib As Integer
Private Sub cmdCancelar_Click()
    Unload Me
End Sub

'''<summary>Comprueba si se ha checkeado alg�n atributo para su actualizaci�n</summary>
'''<remarks>Llamada desde:  FSGSClient.frmPedidoActAtribSol.cmdSiguiente_click Tiempo m�ximo: ? </remarks>
Private Sub cmdSiguiente_Click()

Dim i As Integer
Dim j As Integer
Dim inum As Integer
Dim b As Boolean
Dim vbmCab As Variant
Dim vbmLin As Variant
Dim oAtributoCab As CAtributoOfertado
Dim oAtributoLin As CAtributoOfertado
Dim oAtribCab As CAtributo
Dim oAtribLin As CAtributo
Dim oLinea As CLineaPedido
Dim Cod As String

cmdSiguiente.Enabled = False
cmdCancelar.Enabled = False
Screen.MousePointer = vbHourglass
  
sdbgAtribProce.Update
sdbgAtribItem.Update
  
Set g_oOrdenEntrega = g_oOrigen.g_oOrdenesTemporales.Item(CStr(sstabAtrib.selectedItem.caption))

'Se recorre el grid de cabecera para comprobar si se ha checkeado alg�n atributo
With sdbgAtribProce
    For i = 0 To .Rows - 1
        vbmCab = .AddItemBookmark(i)
        If .Columns("CHECK").CellValue(vbmCab) = -1 Or .Columns("CHECK").CellValue(vbmCab) = 1 Then
            'Aqui entra en cada atributo de cabecera que se ha de actualizar
            
            'Buscamos ese atributo en el proceso para actualizarlo
            If Not g_oOrdenEntrega.ATRIBUTOS Is Nothing Then
            For Each oAtributoCab In g_oOrdenEntrega.ATRIBUTOS
                If oAtributoCab.objeto.Id = CInt(.Columns("ID").CellValue(vbmCab)) Then
                    
                    oAtributoCab.Tipo = oAtributoCab.objeto.Tipo
                    
                    'Si el atributo es de tipo Boolean, se asigna true/false dependiendo el valor del grid ('si/no') mediante la funci�n msIdiToBoolean
                    If oAtributoCab.objeto.Tipo = TipoBoolean Then
                        oAtributoCab.objeto.valor = msIdiToBoolean(.Columns("VALOR_SOL").CellValue(vbmCab))
                        oAtributoCab.objeto.setvalor msIdiToBoolean(.Columns("VALOR_SOL").CellValue(vbmCab))
                        oAtributoCab.setvalor msIdiToBoolean(.Columns("VALOR_SOL").CellValue(vbmCab))
                    Else
                    'Si el atributo no es de tipo Boolean, recogemos el valor tal cual del grid
                        oAtributoCab.objeto.valor = .Columns("VALOR_SOL").CellValue(vbmCab)
                        oAtributoCab.objeto.setvalor (.Columns("VALOR_SOL").CellValue(vbmCab))
                        oAtributoCab.setvalor (.Columns("VALOR_SOL").CellValue(vbmCab))
                    End If
                    oAtributoCab.modificado = b
                End If
            Next
            End If
            If Not g_oOrdenEntrega.AtributosModificados Is Nothing Then
            For Each oAtribCab In g_oOrdenEntrega.AtributosModificados
                If oAtribCab.Id = CInt(.Columns("ID").CellValue(vbmCab)) Then
                    oAtribCab.modificado = b
                End If
            Next
            End If
        Else
            'Atributos que no se van a actualizar
            If Not g_oOrdenEntrega.ATRIBUTOS Is Nothing Then
            For Each oAtributoCab In g_oOrdenEntrega.ATRIBUTOS
                If oAtributoCab.objeto.Id = CInt(.Columns("ID").CellValue(vbmCab)) Then
                
                    oAtributoCab.Tipo = oAtributoCab.objeto.Tipo
                
                    'Si el atributo es de tipo Boolean, se asigna true/false dependiendo del valor del grid ('si/no') mediante la funci�n msIdiToBoolean
                    If oAtributoCab.objeto.Tipo = TipoBoolean Then
                        oAtributoCab.objeto.valor = msIdiToBoolean(.Columns("VALOR_PED").CellValue(vbmCab))
                        oAtributoCab.objeto.setvalor msIdiToBoolean((.Columns("VALOR_PED").CellValue(vbmCab)))
                        oAtributoCab.setvalor msIdiToBoolean((.Columns("VALOR_PED").CellValue(vbmCab)))
                    Else
                    'Si el atributo no es de tipo Boolean, recogemos el valor tal cual del grid
                        oAtributoCab.objeto.valor = .Columns("VALOR_PED").CellValue(vbmCab)
                        oAtributoCab.objeto.setvalor (.Columns("VALOR_PED").CellValue(vbmCab))
                        oAtributoCab.setvalor (.Columns("VALOR_PED").CellValue(vbmCab))
                    End If
                    
                    oAtributoCab.modificado = False
                End If
            Next
            End If
            If Not g_oOrdenEntrega.AtributosModificados Is Nothing Then
            For Each oAtribCab In g_oOrdenEntrega.AtributosModificados
                If oAtribCab.Id = CInt(.Columns("ID").CellValue(vbmCab)) Then
                    oAtribCab.modificado = False
                End If
            Next
            End If
        End If
    Next
End With

    

'Se recorre el grid de l�neas para comprobar si se ha checkeado alg�n atributo
With sdbgAtribItem
    For i = 0 To .Rows - 1
        vbmLin = .AddItemBookmark(i)
        'Buscamos la l�nea en el proceso
        For Each oLinea In g_oOrdenEntrega.LineasPedido
            If oLinea.Num = .Columns("NUM_LINEA").CellValue(vbmLin) Then
                'Dentro de cada fila en la grid, vemos cuantos grupos hay. Cada grupo, tiene un check por atributo
                If .Groups.Count > 2 Then
                    For j = 4 To .Groups.Count - 1 Step 3     'Los atributos est�n a partir del tercer grupo (Primer check-5�Grupo-Groups(4))
                        'Recogemos el c�digo del atributo
                        Cod = .Groups(j).Columns(1).CellValue(vbmLin)
                        If .Groups(j).Columns(0).CellValue(vbmLin) = -1 Or .Groups(j).Columns(0).CellValue(vbmLin) = 1 Then
                            inum = 1
                            b = True
                        Else
                            inum = 2
                            b = False
                        End If
                        
                        'Presupuestos en caso de que haya
                        If b Then
                            'Metemos los valores de la instancia
                            If oLinea.Pres1SolCambio And .Groups(j - inum).Columns(0).caption = gParametrosGenerales.gsSingPres1 Then
                                Set oLinea.Presupuestos1 = Nothing
                                Set oLinea.Presupuestos1 = oFSGSRaiz.Generar_CPresProyectosNivel4
                                oLinea.TraspasarPresupuestosDeInstancia oLinea.PRES1Instancia, 1, oLinea.Presupuestos1
                            End If
                            If oLinea.Pres2SolCambio And .Groups(j - inum).Columns(0).caption = gParametrosGenerales.gsSingPres2 Then
                                Set oLinea.Presupuestos2 = Nothing
                                Set oLinea.Presupuestos2 = oFSGSRaiz.Generar_CPresContablesNivel4
                                oLinea.TraspasarPresupuestosDeInstancia oLinea.PRES2Instancia, 2, oLinea.Presupuestos2
                            End If
                            If oLinea.Pres3SolCambio And .Groups(j - inum).Columns(0).caption = gParametrosGenerales.gsSingPres3 Then
                                Set oLinea.Presupuestos3 = Nothing
                                Set oLinea.Presupuestos3 = oFSGSRaiz.Generar_CPresConceptos3Nivel4
                                oLinea.TraspasarPresupuestosDeInstancia oLinea.PRES3Instancia, 3, oLinea.Presupuestos3
                            End If
                            If oLinea.Pres4SolCambio And .Groups(j - inum).Columns(0).caption = gParametrosGenerales.gsSingPres4 Then
                                Set oLinea.Presupuestos4 = Nothing
                                Set oLinea.Presupuestos4 = oFSGSRaiz.Generar_CPresConceptos4Nivel4
                                oLinea.TraspasarPresupuestosDeInstancia oLinea.PRES4Instancia, 4, oLinea.Presupuestos4
                            End If
                        End If
                       
                        If Not .Groups(j - inum).Columns(0).caption = gParametrosGenerales.gsSingPres1 And Not .Groups(j - inum).Columns(0).caption = gParametrosGenerales.gsSingPres2 _
                        And Not .Groups(j - inum).Columns(0).caption = gParametrosGenerales.gsSingPres3 And Not .Groups(j - inum).Columns(0).caption = gParametrosGenerales.gsSingPres4 Then
                            'Aqui entra en cada atributo de l�nea que se ha de actualizar
                            'Buscamos ese atributo en las l�neas del proceso para actualizarlo
                            If Not oLinea.ATRIBUTOS Is Nothing Then
                                For Each oAtributoLin In oLinea.ATRIBUTOS
                                    If oAtributoLin.objeto.idAtribProce = Cod Then
                                        Select Case oAtributoLin.objeto.Tipo
                                            Case TipoNumerico
                                                oAtributoLin.objeto.valorNum = .Groups(j - inum).Columns(0).CellValue(vbmLin)
                                                oAtributoLin.valorNum = .Groups(j - inum).Columns(0).CellValue(vbmLin)
                                                oAtributoLin.modificado = b
                                            Case TipoString
                                                oAtributoLin.objeto.valorText = .Groups(j - inum).Columns(0).CellValue(vbmLin)
                                                oAtributoLin.valorText = .Groups(j - inum).Columns(0).CellValue(vbmLin)
                                                oAtributoLin.modificado = b
                                            Case TipoFecha
                                                oAtributoLin.objeto.valorFec = .Groups(j - inum).Columns(0).CellValue(vbmLin)
                                                oAtributoLin.valorFec = .Groups(j - inum).Columns(0).CellValue(vbmLin)
                                                oAtributoLin.modificado = b
                                            Case TipoBoolean
                                                oAtributoLin.objeto.valorBool = msIdiToBoolean(.Groups(j - inum).Columns(0).CellValue(vbmLin))
                                                oAtributoLin.valorBool = msIdiToBoolean(.Groups(j - inum).Columns(0).CellValue(vbmLin))
                                                oAtributoLin.modificado = b
                                        End Select
                                    End If
                                Next
                            End If
                            If Not oLinea.AtributosModificados Is Nothing Then
                                For Each oAtribLin In oLinea.AtributosModificados
                                    If oAtribLin.Id = Cod Then
                                        oAtribLin.modificado = b
                                    End If
                                Next
                            End If
                            If Not g_oOrdenEntrega.AtributosLineas Is Nothing Then
                                For Each oAtribLin In g_oOrdenEntrega.AtributosLineas
                                    If oAtribLin.Id = Cod Then
                                        Select Case oAtribLin.Tipo
                                            Case TipoNumerico
                                                oAtribLin.valorNum = .Groups(j - inum).Columns(0).CellValue(vbmLin)
                                                oAtribLin.modificado = b
                                            Case TipoString
                                                oAtribLin.valorText = .Groups(j - inum).Columns(0).CellValue(vbmLin)
                                                oAtribLin.modificado = b
                                            Case TipoFecha
                                                oAtribLin.valorFec = .Groups(j - inum).Columns(0).CellValue(vbmLin)
                                                oAtribLin.modificado = b
                                            Case TipoBoolean
                                                oAtribLin.valorBool = msIdiToBoolean(.Groups(j - inum).Columns(0).CellValue(vbmLin))
                                                oAtribLin.modificado = b
                                        End Select
                                    End If
                                Next
                            End If
                        End If
                    Next
                End If
            End If
        Next
    Next
End With
cmdSiguiente.Enabled = True
cmdCancelar.Enabled = True
Screen.MousePointer = vbNormal
g_Cancelar = False
Unload Me

End Sub

Private Sub Form_Activate()
    sdbgAtribItem.SplitterPos = 1
End Sub

''' <summary>
''' Carga la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()

    Dim sTemp As String
    
    Me.Height = 7220
    Me.Width = 12510
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    'Se pone a false, SOLO se pondra a False cuando se le da a siguiente
    g_Cancelar = True
    lblInfo1.Width = sstabAtrib.Width
    
    ' Para que quede centrada en la MDI
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    ' Cargar los captions dependiendo del idioma
    CargarRecursos
    
    ConfigurarSeguridad
    
    PonerFieldSeparator Me
    
     'Tengo que guardar la estructura original de la grid para los atributos
    sTemp = DevolverPathFichTemp
    m_sLayOut = sTemp & "LayMiAtribItem"
    sdbgAtribItem.SaveLayout m_sLayOut, ssSaveLayoutAll
    
    inicializarTabsProveedor
    'Si hay proveedor, 'inicializarTabsProveedor' lleva a 'sstabAtrib_Click', donde se cargan los grids con los datos (CargarGridAtributosCab - CargarGridAtributosLin)
    
    sdbgAtribProce.AllowUpdate = True
    sdbgAtribItem.AllowUpdate = True

End Sub

''' <summary>
''' Redimensiona los campos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Resize()

    If Me.Height < 1500 Then Exit Sub
    If Me.Width < 1500 Then Exit Sub
    If Me.Height < 10875 Then
        Me.Width = 13845
        sstabAtrib.Height = Me.Height - 2000
        sstabAtrib.Width = Me.Width - 350
        sdbgAtribProce.Height = (sstabAtrib.Height / 3)
        sdbgAtribProce.Width = sstabAtrib.Width - 300
        sdbgAtribItem.Height = (sstabAtrib.Height / 3)
        sdbgAtribItem.Width = sstabAtrib.Width - 300
    Else
        sstabAtrib.Height = Me.Height - 2000
        sstabAtrib.Width = Me.Width - 350
        sdbgAtribProce.Height = sstabAtrib.Height / 3 + 800
        sdbgAtribProce.Width = sstabAtrib.Width - 300
        sdbgAtribItem.Height = sstabAtrib.Height / 3 + 800 ''2800
        sdbgAtribItem.Width = sstabAtrib.Width - 300
    End If
    
    'Ancho de las columnas del grid
    sdbgAtribProce.Columns("COD").Width = sdbgAtribProce.Width * 0.2
    sdbgAtribProce.Columns("DEN").Width = sdbgAtribProce.Width * 0.3
    sdbgAtribProce.Columns("VALOR_PED").Width = sdbgAtribProce.Width * 0.2
    sdbgAtribProce.Columns("VALOR_SOL").Width = sdbgAtribProce.Width * 0.2
    sdbgAtribProce.Columns("CHECK").Width = sdbgAtribProce.Width * 0.1
    
    If Me.Height < 10875 Then
        sdbgAtribProce.Top = sstabAtrib.Top + 800
        sdbgAtribItem.Top = sdbgAtribProce.Top + sdbgAtribProce.Height + 500
    Else
        sdbgAtribProce.Top = sstabAtrib.Top + 800
        sdbgAtribItem.Top = sdbgAtribProce.Top + sdbgAtribProce.Height + 1000
    End If

    
    picItem.Top = sdbgAtribItem.Top - 400
    picItem.Left = sdbgAtribItem.Left
    
    
    picProce.Top = sdbgAtribProce.Top - 400
    picProce.Left = sdbgAtribProce.Left
    
    lblInfo1.Width = sstabAtrib.Width

    
    cmdSiguiente.Left = sstabAtrib.Width / 2 - cmdSiguiente.Width - 200
    cmdCancelar.Left = cmdSiguiente.Left + cmdSiguiente.Width + 50
 
End Sub

''' <summary>
''' Cargar las variables de pantalla multiidioma
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo:0 </remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PEDIDO_ACTATRIBSOL, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
       lblInfo1.caption = Ador(0).Value
       Ador.MoveNext
       Ador.MoveNext
       lblAtribProce.caption = Ador(0).Value
       Ador.MoveNext
       lblAtribItem.caption = Ador(0).Value
       Ador.MoveNext
       cmdSiguiente.caption = Ador(0).Value
       Ador.MoveNext
       cmdCancelar.caption = Ador(0).Value
       Ador.MoveNext
       sdbgAtribProce.Columns(0).caption = Ador(0).Value 'Codigo
       Ador.MoveNext
       sdbgAtribProce.Columns(1).caption = Ador(0).Value 'Denominacion
       Ador.MoveNext
       Ador.MoveNext
       sdbgAtribItem.Columns("ARTICULO").caption = Ador(0).Value 'Articulo
       Ador.MoveNext
       sdbgAtribItem.Columns("PRECIOUP").caption = Ador(0).Value 'Precio UP
       Ador.MoveNext
       sdbgAtribItem.Columns("CANTIDAD").caption = Ador(0).Value 'Cantidad
       Ador.MoveNext
       sdbgAtribItem.Columns("UNIDAD").caption = Ador(0).Value 'Unidad
       Ador.MoveNext
       m_sIdiTrue = Ador(0).Value 'Si
       Ador.MoveNext
       m_sIdiFalse = Ador(0).Value 'No
       Ador.MoveNext
       sIdiMayor = Ador(0).Value
       Ador.MoveNext
       sIdiMenor = Ador(0).Value
       Ador.MoveNext
       sIdiEntre = Ador(0).Value
       Ador.MoveNext
       m_sMoneda = Ador(0).Value
       Ador.MoveNext
       Me.caption = Ador(0).Value
       Ador.MoveNext
       lblInfo1 = Ador(0).Value
       Ador.MoveNext
       sdbgAtribProce.Columns("VALOR_PED").caption = Ador(0).Value   'MOD=446, ID=22 Valor Pedido
       Ador.MoveNext
       sdbgAtribProce.Columns("VALOR_SOL").caption = Ador(0).Value   'MOD=446, ID=23 Valor Solicitud
        
       Ador.Close
    End If

    Set Ador = Nothing

End Sub
    
''' <summary>
''' A�ade un tab de proveedor para cada uno
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub inicializarTabsProveedor()

    Dim oProve As CProveedor
    Dim INumProve As Integer
    Dim j As Integer
    Dim oOrden As COrdenEntrega
    Dim oLinea As CLineaPedido
        
    
    Set m_oProves = Nothing
    Set m_oProves = oFSGSRaiz.generar_CProveedores
    
    For Each oOrden In g_oOrigen.g_oOrdenesTemporales
        For Each oLinea In oOrden.LineasPedido
            If m_oProves.Item(CStr(oLinea.ProveCod)) Is Nothing Then
                m_oProves.Add oLinea.ProveCod, oLinea.ProveDen
                m_oProves.CargarDatosProveedor CStr(oLinea.ProveCod)
            End If
        Next
    Next
    
    'a�ade un tab de proveedor para cada uno
    j = sstabAtrib.Tabs.Count
    While j > 0
        sstabAtrib.Tabs.Remove j
        j = sstabAtrib.Tabs.Count
    Wend
    INumProve = 0
    For Each oProve In m_oProves
        INumProve = INumProve + 1
        sstabAtrib.Tabs.Add INumProve, "A" & oProve.Cod, oProve.Cod
        sstabAtrib.Tabs(INumProve).caption = oProve.Cod
    Next
    
    If (INumProve > 0) Then
        sstabAtrib.Tabs(1).Selected = True
    End If
End Sub


Private Sub Form_Unload(Cancel As Integer)

 Dim FOSFile As Scripting.FileSystemObject
        
    Set m_oAtributosProce = Nothing
    Set m_oAtributosItem = Nothing
    Set m_oProves = Nothing
    Set m_oERPsInt = Nothing
 On Error Resume Next
    Set FOSFile = New Scripting.FileSystemObject
    
    FOSFile.DeleteFile m_sLayOut, True 'Borro el layout de la grid
    
    Set FOSFile = Nothing
    
    Me.Visible = False

End Sub

Private Sub sdbgAtribItem_BtnClick()
Select Case sdbgAtribItem.Columns(sdbgAtribItem.col).TagVariant
    Case "VALOR_TEXT"
        frmATRIBDescr.g_bEdicion = False
        frmATRIBDescr.g_sOrigen = "frmPedidoActAtribSol_Item"
        frmATRIBDescr.caption = sdbgAtribItem.Columns(sdbgAtribItem.col).caption
        frmATRIBDescr.txtDescr.Text = NullToStr(sdbgAtribItem.Columns(sdbgAtribItem.col).Value)
        frmATRIBDescr.Show vbModal
End Select
End Sub


Private Sub sdbgAtribItem_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
If sdbgAtribItem.col = -1 Then Exit Sub
Select Case sdbgAtribItem.Columns(sdbgAtribItem.col).TagVariant
    Case "VALOR_TEXT"
        sdbgAtribItem.Columns(sdbgAtribItem.col).Style = ssStyleEditButton
    Case Else
        If sdbgAtribItem.Columns(sdbgAtribItem.col).Style <> ssStyleCheckBox Then
            sdbgAtribItem.Columns(sdbgAtribItem.col).Style = ssStyleEdit
        End If
End Select
If sdbgAtribItem.Columns(sdbgAtribItem.col).Style <> ssStyleCheckBox Then
    sdbgAtribItem.Columns(sdbgAtribItem.col).Locked = True
End If
End Sub

Private Sub sdbgAtribProce_BtnClick()
Select Case sdbgAtribProce.Columns(sdbgAtribProce.col).Name
    Case "VALOR_PED", "VALOR_SOL"
        frmATRIBDescr.g_bEdicion = False
        frmATRIBDescr.g_sOrigen = "frmPedidoActAtribSol_Proce"
        frmATRIBDescr.caption = sdbgAtribProce.Columns(sdbgAtribProce.col).Value
        frmATRIBDescr.txtDescr.Text = NullToStr(sdbgAtribProce.Columns(sdbgAtribProce.col).Value)
        frmATRIBDescr.Show vbModal
End Select
End Sub


Private Sub sdbgAtribProce_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim oAtribCab As CAtributo
If sdbgAtribProce.col = -1 Then Exit Sub
Select Case Left(sdbgAtribProce.Columns(sdbgAtribProce.col).Name, 5)
    Case "VALOR"
        For Each oAtribCab In g_oOrdenEntrega.AtributosModificados
            If oAtribCab.Id = CInt(sdbgAtribProce.Columns("ID").Value) Then
                If oAtribCab.TipoIntroduccion = IntroLibre Then
                    Select Case oAtribCab.Tipo
                        Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
                            sdbgAtribProce.Columns(sdbgAtribProce.col).Style = ssStyleEditButton
                        Case Else
                            sdbgAtribProce.Columns(sdbgAtribProce.col).Style = ssStyleEdit
                    End Select
                Else
                    sdbgAtribProce.Columns(sdbgAtribProce.col).Style = ssStyleEdit
                End If
            End If
        Next
        sdbgAtribProce.Columns(sdbgAtribProce.col).Locked = True
End Select


End Sub

''' <summary>
''' Actualiza los valores de los atributos a modificar de la orden de la grid antes de cambiar de pesta�a de proveedor
''' </summary>
Private Sub sstabAtrib_BeforeClick(Cancel As Integer)

Dim i As Long
Dim j As Long
Dim vbmCab As Variant
Dim vbmLin As Variant
Dim oAtributoCab As CAtributoOfertado
Dim oAtributoLin As CAtributoOfertado
Dim oAtribCab As CAtributo
Dim oAtribLin As CAtributo
Dim oLinea As CLineaPedido
Dim Cod As String

Screen.MousePointer = vbHourglass

sdbgAtribProce.Update
sdbgAtribItem.Update

Set g_oOrdenEntrega = g_oOrigen.g_oOrdenesTemporales.Item(CStr(sstabAtrib.selectedItem.caption))


'Se recorre el grid de cabecera para comprobar si se ha checkeado alg�n atributo
With sdbgAtribProce
    For i = 0 To .Rows - 1
        vbmCab = .AddItemBookmark(i)
        If .Columns("CHECK").CellValue(vbmCab) = -1 Or .Columns("CHECK").CellValue(vbmCab) = 1 Then
            'Aqui entra en cada atributo de cabecera que se ha de actualizar

            'Buscamos ese atributo en el proceso para actualizarlo
            If Not g_oOrdenEntrega.ATRIBUTOS Is Nothing Then
            For Each oAtributoCab In g_oOrdenEntrega.ATRIBUTOS
                If oAtributoCab.objeto.Id = CInt(.Columns("ID").CellValue(vbmCab)) Then
                    
                    oAtributoCab.Tipo = oAtributoCab.objeto.Tipo
                
                    'Si el atributo es de tipo Boolean, se asigna true/false dependiendo del valor del grid ('si/no') mediante la funci�n msIdiToBoolean
                    If oAtributoCab.objeto.Tipo = TipoBoolean Then
                            oAtributoCab.objeto.valor = msIdiToBoolean(.Columns("VALOR_SOL").CellValue(vbmCab))
                            oAtributoCab.objeto.setvalor msIdiToBoolean(.Columns("VALOR_SOL").CellValue(vbmCab))
                            oAtributoCab.setvalor msIdiToBoolean(.Columns("VALOR_SOL").CellValue(vbmCab))
                    Else
                    'Si el atributo no es de tipo Boolean, recogemos el valor tal cual del grid
                        oAtributoCab.objeto.valor = .Columns("VALOR_SOL").CellValue(vbmCab)
                        oAtributoCab.objeto.setvalor (.Columns("VALOR_SOL").CellValue(vbmCab))
                        oAtributoCab.setvalor (.Columns("VALOR_SOL").CellValue(vbmCab))
                    End If
                    
                    oAtributoCab.modificado = True
                End If
            Next
            End If
            If Not g_oOrdenEntrega.AtributosModificados Is Nothing Then
            For Each oAtribCab In g_oOrdenEntrega.AtributosModificados
                If oAtribCab.Id = CInt(.Columns("ID").CellValue(vbmCab)) Then
                    oAtribCab.modificado = True
                End If
            Next
            End If
    
        Else
            'Atributos que no se quieren actualizar
            If Not g_oOrdenEntrega.ATRIBUTOS Is Nothing Then
            For Each oAtributoCab In g_oOrdenEntrega.ATRIBUTOS
                If oAtributoCab.objeto.Id = CInt(.Columns("ID").CellValue(vbmCab)) Then
                
                    oAtributoCab.Tipo = oAtributoCab.objeto.Tipo
                
                    'Si el atributo es de tipo Boolean, se asigna true/false dependiendo del valor del grid('si/no') mediante la funci�n msIdiToBoolean
                    If oAtributoCab.objeto.Tipo = TipoBoolean Then
                        oAtributoCab.objeto.valor = msIdiToBoolean(.Columns("VALOR_PED").CellValue(vbmCab))
                        oAtributoCab.objeto.setvalor msIdiToBoolean((.Columns("VALOR_PED").CellValue(vbmCab)))
                        oAtributoCab.setvalor msIdiToBoolean((.Columns("VALOR_PED").CellValue(vbmCab)))
                    Else
                        'Si el atributo no es de tipo Boolean, recogemos el valor tal cual del grid
                        oAtributoCab.objeto.valor = .Columns("VALOR_PED").CellValue(vbmCab)
                        oAtributoCab.objeto.setvalor (.Columns("VALOR_PED").CellValue(vbmCab))
                        oAtributoCab.setvalor (.Columns("VALOR_PED").CellValue(vbmCab))
                    End If
                
                    oAtributoCab.modificado = False
                End If
            Next
            End If
            If Not g_oOrdenEntrega.AtributosModificados Is Nothing Then
            For Each oAtribCab In g_oOrdenEntrega.AtributosModificados
                If oAtribCab.Id = CInt(.Columns("ID").CellValue(vbmCab)) Then
                    oAtribCab.modificado = False
                End If
            Next
            End If
        End If
    Next
End With



'Se recorre el grid de l�neas para comprobar si se ha checkeado alg�n atributo
With sdbgAtribItem
    For i = 0 To .Rows - 1
        vbmLin = .AddItemBookmark(i)

        'Buscamos la l�nea en el proceso
        For Each oLinea In g_oOrdenEntrega.LineasPedido
            If oLinea.Num = .Columns("NUM_LINEA").CellValue(vbmLin) Then

                'Dentro de cada fila en la grid, vemos cuantos grupos hay. Cada grupo, tiene un check por atributo
                If .Groups.Count > 2 Then
                    For j = 4 To .Groups.Count - 1 Step 3     'Los atributos est�n a partir del tercer grupo (Primer check-5�Grupo-Groups(4))

                        'Recogemos el c�digo del atributo
                        Cod = .Groups(j).Columns(1).CellValue(vbmLin)

                        If .Groups(j).Columns(0).CellValue(vbmLin) = -1 Or .Groups(j).Columns(0).CellValue(vbmLin) = 1 Then
                            'Aqui entra en cada atributo de l�nea que se ha de actualizar


                            'Buscamos ese atributo en las l�neas del proceso para actualizarlo
                            If Not oLinea.ATRIBUTOS Is Nothing Then
                            For Each oAtributoLin In oLinea.ATRIBUTOS
                                If oAtributoLin.objeto.idAtribProce = Cod Then
                                    If oAtributoLin.objeto.Tipo = TipoNumerico Then
                                        oAtributoLin.objeto.valorNum = .Groups(j - 1).Columns(0).CellValue(vbmLin)
                                        oAtributoLin.valorNum = .Groups(j - 1).Columns(0).CellValue(vbmLin)
                                        oAtributoLin.modificado = True
                                    Else
                                        If oAtributoLin.objeto.Tipo = TipoString Then
                                            oAtributoLin.objeto.valorText = .Groups(j - 1).Columns(0).CellValue(vbmLin)
                                            oAtributoLin.valorText = .Groups(j - 1).Columns(0).CellValue(vbmLin)
                                            oAtributoLin.modificado = True
                                        Else
                                            If oAtributoLin.objeto.Tipo = TipoFecha Then
                                                oAtributoLin.objeto.valorFec = .Groups(j - 1).Columns(0).CellValue(vbmLin)
                                                oAtributoLin.valorFec = .Groups(j - 1).Columns(0).CellValue(vbmLin)
                                                oAtributoLin.modificado = True
                                            Else
                                                If oAtributoLin.objeto.Tipo = TipoBoolean Then
                                                    oAtributoLin.objeto.valorBool = msIdiToBoolean(.Groups(j - 1).Columns(0).CellValue(vbmLin))
                                                    oAtributoLin.valorBool = msIdiToBoolean(.Groups(j - 1).Columns(0).CellValue(vbmLin))
                                                    oAtributoLin.modificado = True
                                                End If
                                            End If
                                        End If
                                    End If

                                End If
                            Next
                            End If
                            If Not oLinea.AtributosModificados Is Nothing Then
                            For Each oAtribLin In oLinea.AtributosModificados
                                If oAtribLin.Id = Cod Then
                                    oAtribLin.modificado = True
                                End If
                            Next
                            End If
                            
                            If Not g_oOrdenEntrega.AtributosLineas Is Nothing Then
                            For Each oAtribLin In g_oOrdenEntrega.AtributosLineas
                                If oAtribLin.Id = Cod Then
                                    If oAtribLin.Tipo = TipoNumerico Then
                                        oAtribLin.valorNum = .Groups(j - 1).Columns(0).CellValue(vbmLin)
                                        oAtribLin.modificado = True
                                    Else
                                        If oAtribLin.Tipo = TipoString Then
                                            oAtribLin.valorText = .Groups(j - 1).Columns(0).CellValue(vbmLin)
                                            oAtribLin.modificado = True
                                        Else
                                            If oAtribLin.Tipo = TipoFecha Then
                                                oAtribLin.valorFec = .Groups(j - 1).Columns(0).CellValue(vbmLin)
                                                oAtribLin.modificado = True
                                            Else
                                                If oAtribLin.Tipo = TipoBoolean Then
                                                    oAtribLin.valorBool = msIdiToBoolean(.Groups(j - 1).Columns(0).CellValue(vbmLin))
                                                    oAtribLin.modificado = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                            End If
                        Else
                            'Atributos que no se quieren actualizar
                            Cod = .Groups(j).Columns(1).CellValue(vbmLin)

                            If Not oLinea.ATRIBUTOS Is Nothing Then
                            For Each oAtributoLin In oLinea.ATRIBUTOS
                                If oAtributoLin.objeto.idAtribProce = Cod Then
                                    If oAtributoLin.objeto.Tipo = TipoNumerico Then
                                        oAtributoLin.objeto.valorNum = .Groups(j - 2).Columns(0).CellValue(vbmLin)
                                        oAtributoLin.valorNum = .Groups(j - 2).Columns(0).CellValue(vbmLin)
                                        oAtributoLin.modificado = False
                                    Else
                                        If oAtributoLin.objeto.Tipo = TipoString Then
                                            oAtributoLin.objeto.valorText = .Groups(j - 2).Columns(0).CellValue(vbmLin)
                                            oAtributoLin.valorText = .Groups(j - 2).Columns(0).CellValue(vbmLin)
                                            oAtributoLin.modificado = False
                                        Else
                                            If oAtributoLin.objeto.Tipo = TipoFecha Then
                                                oAtributoLin.objeto.valorFec = .Groups(j - 2).Columns(0).CellValue(vbmLin)
                                                oAtributoLin.valorFec = .Groups(j - 2).Columns(0).CellValue(vbmLin)
                                                oAtributoLin.modificado = False
                                            Else
                                                If oAtributoLin.objeto.Tipo = TipoBoolean Then
                                                    oAtributoLin.objeto.valorBool = msIdiToBoolean(.Groups(j - 2).Columns(0).CellValue(vbmLin))
                                                    oAtributoLin.valorBool = msIdiToBoolean(.Groups(j - 2).Columns(0).CellValue(vbmLin))
                                                    oAtributoLin.modificado = False
                                                End If
                                            End If
                                        End If
                                    End If

                                End If
                            Next
                            End If
                            If Not oLinea.AtributosModificados Is Nothing Then
                            For Each oAtribLin In oLinea.AtributosModificados
                                If oAtribLin.Id = Cod Then
                                    oAtribLin.modificado = False
                                End If
                            Next
                            End If
                            
                            If Not g_oOrdenEntrega.AtributosLineas Is Nothing Then
                            For Each oAtribLin In g_oOrdenEntrega.AtributosLineas
                                If oAtribLin.Id = Cod Then
                                    If oAtribLin.Tipo = TipoNumerico Then
                                        oAtribLin.valorNum = .Groups(j - 2).Columns(0).CellValue(vbmLin)
                                        oAtribLin.modificado = False
                                    Else
                                        If oAtribLin.Tipo = TipoString Then
                                            oAtribLin.valorText = .Groups(j - 2).Columns(0).CellValue(vbmLin)
                                            oAtribLin.modificado = False
                                        Else
                                            If oAtribLin.Tipo = TipoFecha Then
                                                oAtribLin.valorFec = .Groups(j - 2).Columns(0).CellValue(vbmLin)
                                                oAtribLin.modificado = False
                                            Else
                                                If oAtribLin.Tipo = TipoBoolean Then
                                                    oAtribLin.valorBool = msIdiToBoolean(.Groups(j - 2).Columns(0).CellValue(vbmLin))
                                                    oAtribLin.modificado = False
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                            End If
                        End If


                    Next
                End If

            End If
        Next
    Next
End With


Screen.MousePointer = vbNormal


End Sub

''' <summary>
''' Cargar los grids con los atributos pertenecientes a cada orden del pedido
''' </summary>
Private Sub sstabAtrib_Click()

    Dim i As Long
    Dim j As Long

    If sdbgAtribProce.GrpPosition(0) > 0 Then
        j = sdbgAtribProce.Groups(0).Position
        sdbgAtribProce.Scroll -j, 0
        sdbgAtribProce.Update
        sdbgAtribProce.AllowColumnMoving = ssRelocateWithinGroup
    End If
    
    If sdbgAtribItem.GrpPosition(0) > 0 Then
        j = sdbgAtribItem.Groups(0).Position
        sdbgAtribItem.Scroll -j, 0
        sdbgAtribItem.Update
        sdbgAtribItem.AllowColumnMoving = ssRelocateWithinGroup
    End If

    sdbgAtribProce.RemoveAll
    sdbgAtribItem.RemoveAll
    
    'Borrado de las columnas de sdbgAtribItem (Columnas = Atributos que tiene)
    For i = (sdbgAtribItem.Groups.Count - 1) To 2 Step -1
        sdbgAtribItem.Groups.Remove i
    Next

    'Cargar los grids con los datos
    CargarGridAtributosCab
    CargarGridAtributosLin
    
    sdbgAtribProce.AllowUpdate = True
    sdbgAtribItem.AllowUpdate = True

    sdbgAtribProce.Update
    sdbgAtribItem.Update

End Sub

''' <summary>Carga las variables de seguridad seg�n el usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRRestReceptorUsu)) Is Nothing) Then
        m_bRReceptorUsu = True
    End If
End Sub

''' <summary>Rellena el grid sdbgAtribProce con los datos de los atributos de cabecera que han cambiado de valor</summary>
''' <remarks>Llamada desde: Form_Load, sstabAtrib_Click</remarks>
Private Sub CargarGridAtributosCab()

    Dim oatrib As CAtributo
    Dim oOrden As COrdenEntrega
    Dim vValor As Variant
    Dim vValorSolic As Variant
    
    If g_oOrdenEntrega.AtributosModificados Is Nothing Then Exit Sub
    
    Set g_oOrdenEntrega = g_oOrigen.g_oOrdenesTemporales.Item(CStr(sstabAtrib.selectedItem.caption))

    
    'Si hay mas de 6 atributos, aparecer� el scroll vertical. Se redimensionan el tama�o de las columnas
    If Not g_oOrdenEntrega.AtributosModificados Is Nothing Then
        If g_oOrdenEntrega.AtributosModificados.Count > 6 Then
            sdbgAtribProce.ScrollBars = 2
            sdbgAtribProce.Columns("COD").Width = 3000
            sdbgAtribProce.Columns("DEN").Width = 3500
            sdbgAtribProce.Columns("VALOR_PED").Width = 2500
            sdbgAtribProce.Columns("VALOR_SOL").Width = 2500
            sdbgAtribProce.Columns("CHECK").Width = 1500
        End If
    End If

    If Not g_oOrdenEntrega.AtributosModificados Is Nothing Then
        For Each oatrib In g_oOrdenEntrega.AtributosModificados
            'Atributo Tipo Boolean
            If oatrib.Tipo = TipoBoolean Then
                Select Case oatrib.valor
                    Case 0, False
                        vValor = m_sIdiFalse
                    Case 1, True
                        vValor = m_sIdiTrue
                    Case Else
                        vValor = ""
                End Select
                
                
                Select Case oatrib.ValorSolic
                    Case 0, False
                        vValorSolic = m_sIdiFalse
                    Case 1, True
                        vValorSolic = m_sIdiTrue
                    Case Else
                        vValorSolic = ""
                End Select
                
            Else
                vValor = oatrib.valor
                vValorSolic = oatrib.ValorSolic
            End If
        
            'Si el atributo tiene el check marcado porque se ha marcado anteriormente y se ha vuelto a cambiar de pesta�a,
            'que se mantenga el valor si se vuelve a acceder
            If oatrib.modificado Then
                sdbgAtribProce.AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & vValor & Chr(m_lSeparador) & vValorSolic & Chr(m_lSeparador) & "1"
            Else
                sdbgAtribProce.AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & vValor & Chr(m_lSeparador) & vValorSolic & Chr(m_lSeparador) & "0"
            End If
            
        Next
    End If

    sdbgAtribProce.MoveFirst
    
End Sub

''' <summary>Rellena el grid sdbgAtribItem con los datos de los atributos en las l�neas de pedido que han cambiado de valor</summary>
''' <remarks>Llamada desde: Form_Load, sstabAtrib_Click</remarks>
Private Sub CargarGridAtributosLin()

    Dim oLinea As CLineaPedido
    Dim oAtributo As CAtributo
    Dim sLinea As String
    Dim iGroupIndex As Integer
    Dim ogroup As SSDataWidgets_B.Group
    Dim oColumn As SSDataWidgets_B.Column
    Dim vValor As Variant
    Dim sColumnasCreadas() As String 'Para que no vuelva a crearlas en la siquientes lineas
    Dim bColumnasCreadas As Boolean
    Dim i As Integer
    Dim bEncontrado As Boolean
    Dim bColumnasCreadasPres1 As Boolean
    Dim bColumnasCreadasPres2 As Boolean
    Dim bColumnasCreadasPres3 As Boolean
    Dim bColumnasCreadasPres4 As Boolean
    Dim Ador As ADODB.Recordset
    Dim cumpleIf As Boolean
    Dim AlgunPresup1 As Boolean, AlgunPresup2 As Boolean, AlgunPresup3 As Boolean, AlgunPresup4 As Boolean
    
    iGroupIndex = 2
    'Primero creamos todas las columnas segun los atributos modificados de todas las lineas
    ReDim sColumnasCreadas(0)
    Set g_oOrdenEntrega = g_oOrigen.g_oOrdenesTemporales.Item(CStr(sstabAtrib.selectedItem.caption))
    
    For Each oLinea In g_oOrdenEntrega.LineasPedido
        If oLinea.Pres1SolCambio Then
            CrearColumnasPres bColumnasCreadasPres1, iGroupIndex, gParametrosGenerales.gsSingPres1
            AlgunPresup1 = True
        End If
        If oLinea.Pres2SolCambio Then
            CrearColumnasPres bColumnasCreadasPres2, iGroupIndex, gParametrosGenerales.gsSingPres2
            AlgunPresup2 = True
        End If
        If oLinea.Pres3SolCambio Then
            CrearColumnasPres bColumnasCreadasPres3, iGroupIndex, gParametrosGenerales.gsSingPres3
            AlgunPresup3 = True
        End If
        If oLinea.Pres4SolCambio Then
            CrearColumnasPres bColumnasCreadasPres4, iGroupIndex, gParametrosGenerales.gsSingPres4
            AlgunPresup4 = True
        End If
    Next
    '1ro los presup 2do los atribs
    For Each oLinea In g_oOrdenEntrega.LineasPedido
        m_iGrupoFinAtrib = iGroupIndex
        'Cargamos los atributos diferentes
        If Not oLinea.AtributosModificados Is Nothing Then
            For Each oAtributo In oLinea.AtributosModificados
                'Tenemos que mirar cada atributos si esta creado en el grid, porque cada linea puede tener distintos atribs modificados
                bColumnasCreadas = False
                For i = 0 To UBound(sColumnasCreadas) - 1
                    If oAtributo.Cod = sColumnasCreadas(i) Then
                        bColumnasCreadas = True
                        Exit For
                    End If
                Next
            
                'Por cada atributo, se crean 3 grupos (Valor Pedido, Valor Solicitud, Check)
                'Grupo "Valor Pedido"
                '--------------------
                If Not bColumnasCreadas Then
                    sdbgAtribItem.Groups.Add iGroupIndex
                    Set ogroup = sdbgAtribItem.Groups(iGroupIndex)
                    ogroup.caption = sdbgAtribProce.Columns("VALOR_PED").caption
                    ogroup.Width = 2500
                    sdbgAtribItem.Groups(iGroupIndex).CaptionAlignment = ssCaptionAlignmentLeft
                    
                    ogroup.Columns.Add 0
                    Set oColumn = ogroup.Columns(0)
                    oColumn.caption = oAtributo.Cod & "-" & oAtributo.Den
                    oColumn.Alignment = ssCaptionAlignmentRight
                    'Se mete este texto para que a la hora de pulsar el b�ton se sepa que es para mostrar la ventana del valor completo en caso de que sea texto libre
                    oColumn.Width = 2500
                    DoEvents
                    sColumnasCreadas(UBound(sColumnasCreadas)) = oAtributo.Cod
                    ReDim Preserve sColumnasCreadas(UBound(sColumnasCreadas) + 1)
                    iGroupIndex = iGroupIndex + 1
                End If
                
                Select Case oAtributo.Tipo
                    Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
                        If oAtributo.TipoIntroduccion = IntroLibre Then
                            oColumn.TagVariant = "VALOR_TEXT"
                        End If
                End Select
                                

                
                'Grupo "Valor Solicitud"
                '-----------------------
                If Not bColumnasCreadas Then
                    sdbgAtribItem.Groups.Add iGroupIndex
                    
                    Set ogroup = sdbgAtribItem.Groups(iGroupIndex)
                    
                    ogroup.caption = sdbgAtribProce.Columns("VALOR_SOL").caption
                    ogroup.Width = 2500
                    sdbgAtribItem.Groups(iGroupIndex).CaptionAlignment = ssCaptionAlignmentLeft
                    
                    ogroup.Columns.Add 0
                    Set oColumn = ogroup.Columns(0)
                    oColumn.caption = oAtributo.Cod & "-" & oAtributo.Den
                    oColumn.Alignment = ssCaptionAlignmentRight
                    'Se mete este texto para que a la hora de pulsar el b�ton se sepa que es para mostrar la ventana del valor completo en caso de que sea texto libre
                    oColumn.TagVariant = "VALOR"
                    oColumn.Width = 2500
                    DoEvents
                    iGroupIndex = iGroupIndex + 1
                End If
                
                Select Case oAtributo.Tipo
                    Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
                        If oAtributo.TipoIntroduccion = IntroLibre Then
                            oColumn.TagVariant = "VALOR_TEXT"
                        End If
                End Select
                
                
                'Grupo "Check"
                '------------
                If Not bColumnasCreadas Then
                    sdbgAtribItem.Groups.Add iGroupIndex
                    
                    Set ogroup = sdbgAtribItem.Groups(iGroupIndex)
                    
                    ogroup.caption = ""
                    ogroup.Width = 500
                    sdbgAtribItem.Groups(iGroupIndex).CaptionAlignment = ssCaptionAlignmentLeft
                    
                    ogroup.Columns.Add 0
                    Set oColumn = ogroup.Columns(0)
                    oColumn.caption = ""
                    oColumn.Alignment = ssCaptionAlignmentRight
                    oColumn.Style = ssStyleCheckBox
                    oColumn.Width = 500
                    DoEvents
                    
                    ogroup.Columns.Add 1
                    Set oColumn = ogroup.Columns(1)
                    oColumn.Visible = False
                    DoEvents
                    iGroupIndex = iGroupIndex + 1
                End If
            Next
        End If
    Next
    For Each oLinea In g_oOrdenEntrega.LineasPedido
        If oLinea.Pres1SolCambio Or oLinea.Pres2SolCambio Or oLinea.Pres3SolCambio Or oLinea.Pres4SolCambio Then
            cumpleIf = True
        ElseIf Not oLinea.AtributosModificados Is Nothing Then
            If oLinea.AtributosModificados.Count > 0 Then
                cumpleIf = True
            Else
                cumpleIf = False
            End If
        Else
            cumpleIf = False
        End If
        If cumpleIf Then
            sLinea = oLinea.Num & Chr(m_lSeparador) & oLinea.ArtCod_Interno & "-" & oLinea.ArtDen
            sLinea = sLinea & Chr(m_lSeparador) & oLinea.PrecioUP & Chr(m_lSeparador) & oLinea.CantidadPedida & Chr(m_lSeparador) & oLinea.UnidadPedido
            'Cargamos los Presupuestos de GS diferentes
            If AlgunPresup1 Then 'oLinea.Pres1SolCambio Then
                sLinea = sLinea & CargarColumnasPres(oLinea.Pres1SolCambio, oLinea.Presupuestos1Proce, oLinea.PRES1Instancia, 1)
            End If
            If AlgunPresup2 Then 'oLinea.Pres2SolCambio Then
                sLinea = sLinea & CargarColumnasPres(oLinea.Pres2SolCambio, oLinea.Presupuestos2Proce, oLinea.PRES2Instancia, 2)
            End If
            If AlgunPresup3 Then 'oLinea.Pres3SolCambio Then
                sLinea = sLinea & CargarColumnasPres(oLinea.Pres3SolCambio, oLinea.Presupuestos3Proce, oLinea.PRES3Instancia, 3)
            End If
            If AlgunPresup4 Then 'oLinea.Pres4SolCambio Then
                sLinea = sLinea & CargarColumnasPres(oLinea.Pres4SolCambio, oLinea.Presupuestos4Proce, oLinea.PRES4Instancia, 4)
            End If
            
            If Not oLinea.AtributosModificados Is Nothing Then
                For i = 0 To UBound(sColumnasCreadas) - 1
                    bEncontrado = False
                    For Each oAtributo In oLinea.AtributosModificados
                        If oAtributo.Cod = sColumnasCreadas(i) Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next
                    If bEncontrado Then
                        'Por cada atributo, se crean 3 grupos (Valor Pedido, Valor Solicitud, Check)
                        'Grupo "Valor Pedido"
                        '--------------------
                        vValor = ""
                        Select Case oAtributo.Tipo
                            Case TipoBoolean
                                Select Case oAtributo.valor
                                    Case 0, False
                                    vValor = m_sIdiFalse
                                Case 1, True
                                    vValor = m_sIdiTrue
                                End Select
                            Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
                                vValor = oAtributo.valor
                            Case Else
                                vValor = oAtributo.valor
                        End Select
                        
                        sLinea = sLinea & Chr(m_lSeparador) & vValor
                        
                        vValor = ""
                        Select Case oAtributo.Tipo
                            Case TipoBoolean
                                Select Case oAtributo.ValorSolic
                                    Case 0, False
                                    vValor = m_sIdiFalse
                                Case 1, True
                                    vValor = m_sIdiTrue
                                End Select
                            Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
                                vValor = oAtributo.ValorSolic
                            Case Else
                                vValor = oAtributo.ValorSolic
                        End Select
        
                        sLinea = sLinea & Chr(m_lSeparador) & vValor
                        
                        'Grupo "Check"
                        '------------
                        'Si el atributo tiene el check marcado porque se ha marcado anteriormente y se ha vuelto a cambiar de pesta�a,
                        'que se mantenga el valor si se vuelve a acceder
                        If oAtributo.modificado Then
                            sLinea = sLinea & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & oAtributo.Id
                        Else
                            sLinea = sLinea & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & oAtributo.Id
                        End If
                    Else
                        sLinea = sLinea & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0"
                    End If
                Next
            End If
            sdbgAtribItem.AddItem sLinea
        End If
    Next
    sdbgAtribItem.MoveFirst

End Sub

''' <summary>Rellena las columnas de la grid en funcion de si hay cambios de presupuesto entre la instancia y el proceso</summary>
''' <param name="PresSolCambio">Si hay cambios o no entre los presupuestos de instancia y de proceso</param>
''' <param name="PresupuestosProce">Coleccion de presupuestos del proceso</param>
''' <param name="PRESInstancia">Valor_Text de la instancia en el campo referente al presupuesto</param>
''' <param name="iTipo">tipo de presupuesto 1, 2, 3, 4</param>
''' <returns>Devuelve el String para a�adir a la l�nea de la grid</returns>
''' <remarks>Llamada desde: CargarGridAtributosLin</remarks>
Private Function CargarColumnasPres(ByVal PresSolCambio As Boolean, ByVal PresupuestosProce As Object, ByVal PRESInstancia As String, ByVal iTipo As Integer) As String
Dim s As String
Dim sPres As String

If PresSolCambio Then
    s = Chr(m_lSeparador) & StringDePresupuestos(PresupuestosProce, iTipo, True)
    sPres = Trim(GenerarDenominacionPresupuesto(PRESInstancia, iTipo, , True))
    If Right(sPres, 1) = ";" Then sPres = Left(sPres, Len(sPres) - 1)
    s = s & Chr(m_lSeparador) & sPres & Chr(m_lSeparador) & Chr(m_lSeparador)
Else
    s = Chr(m_lSeparador) & ""
    s = s & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & Chr(m_lSeparador)
End If
CargarColumnasPres = s
End Function
''' <summary>Rellena las columnas de la grid en funcion de si hay cambios de presupuesto entre la instancia y el proceso</summary>
''' <param name="bColumnasCreadas">Para saber si se han creado ya los grupo y las columnas</param>
''' <param name="iGroupIndex">El grupo que toca crear</param>
''' <param name="sCaption">El titulo de la culumna de la grid</param>
''' <remarks>Llamada desde: CargarGridAtributosLin</remarks>
Private Sub CrearColumnasPres(ByRef bColumnasCreadas As Boolean, ByRef iGroupIndex As Integer, ByVal sCaption As String)
Dim i As Integer
Dim sCampo As String
Dim iWidth As Integer
Dim ogroup As SSDataWidgets_B.Group
Dim oColumn As SSDataWidgets_B.Column
If Not bColumnasCreadas Then
    If Not bColumnasCreadas Then
        For i = 0 To 2
            Select Case i
                Case 0  'Grupo "Valor Pedido"
                    sCampo = "VALOR_PED"
                    iWidth = 2500
                Case 1  'Grupo "Valor Solicitud"
                    sCampo = "VALOR_SOL"
                    iWidth = 2500
                Case 2  'Grupo "Check"
                    sCampo = ""
                    iWidth = 500
            End Select
            sdbgAtribItem.Groups.Add iGroupIndex
            Set ogroup = sdbgAtribItem.Groups(iGroupIndex)
            ogroup.caption = sdbgAtribProce.Columns(sCampo).caption
            ogroup.Width = iWidth
            sdbgAtribItem.Groups(iGroupIndex).CaptionAlignment = ssCaptionAlignmentLeft
            
            ogroup.Columns.Add 0
            Set oColumn = ogroup.Columns(0)
            oColumn.caption = sCaption
            oColumn.Alignment = ssCaptionAlignmentRight
            If i = 2 Then
                oColumn.Style = ssStyleCheckBox
                oColumn.caption = ""
            End If
            oColumn.Width = iWidth
            DoEvents
            If i = 2 Then
                ogroup.Columns.Add 1
                Set oColumn = ogroup.Columns(1)
                oColumn.Visible = False
                DoEvents
            End If
            iGroupIndex = iGroupIndex + 1
        Next i
    End If
End If
bColumnasCreadas = True

End Sub

''' <summary>Devuelve el String del presupuesto de la instancia</summary>
''' <param name="PRESInstancia">Valor_Text de la instancia en el campo referente al presupuesto</param>
''' <returns>Devuelve el String del presupuesto de la instancia</returns>
''' <remarks>Llamada desde: CargarColumnasPres</remarks>
Private Function SacarDenPres(PRESInstancia) As String

End Function

''' <summary>Intercambia un 'si/no' por 'true/false'</summary>
''' <param name="msIdi">'Si/no' en el idioma correspondiente</param>
''' <returns>True/false</returns>
''' <remarks>Llamada desde: cmdSiguiente_Click, sstabAtrib_BeforeClick</remarks>
Private Function msIdiToBoolean(ByVal msIdi As String) As Boolean

If msIdi = m_sIdiTrue Then
    msIdiToBoolean = True
Else
    msIdiToBoolean = False
End If
    
End Function
