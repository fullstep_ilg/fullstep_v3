VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPROCEBusqPer 
   BackColor       =   &H00808000&
   Caption         =   "A�adir aprovisionador"
   ClientHeight    =   5880
   ClientLeft      =   7155
   ClientTop       =   2805
   ClientWidth     =   5550
   Icon            =   "frmPROCEBusqPer.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5880
   ScaleWidth      =   5550
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   5400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEBusqPer.frx":0CB2
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEBusqPer.frx":107A
            Key             =   "Persona"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEBusqPer.frx":1184
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEBusqPer.frx":14D8
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEBusqPer.frx":182C
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEBusqPer.frx":1B80
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEBusqPer.frx":1F14
            Key             =   "PerUsu"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEBusqPer.frx":201E
            Key             =   "PersonaBaja"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEBusqPer.frx":20A9
            Key             =   "PerUsuBaja"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picEstrOrg 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   5370
      Left            =   45
      ScaleHeight     =   5370
      ScaleWidth      =   5565
      TabIndex        =   4
      Top             =   60
      Width           =   5565
      Begin MSComctlLib.TreeView tvwEstrOrg 
         Height          =   5205
         Left            =   15
         TabIndex        =   0
         Top             =   30
         Width           =   5400
         _ExtentX        =   9525
         _ExtentY        =   9181
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   5550
      TabIndex        =   3
      Top             =   5460
      Width           =   5550
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1755
         TabIndex        =   1
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2955
         TabIndex        =   2
         Top             =   30
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmPROCEBusqPer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Variables para interactuar con otros forms
Public sOrigen As String
Public oAprovSeleccionado As cAprovisionador
Public oAprovPadre As cAprovisionador
Public sCodPer As String
Public bRUO As Boolean
Public bRPerfUO As Boolean
Public bRDep As Boolean
Public dLimitePadre As Double
Public dLimiteHijos As Double
Public giTipo  As Integer

' Variable de control de flujo
Public Accion As accionessummit

'Variables de idiomas
Private sIdiAnyadirAp As String
Private sIdiModificarAp As String
Private sIdiSustituirAp As String
Private sIdiDetalle As String
Private sIdiSelPer As String

Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node

Set nodx = tvwEstrOrg.selectedItem

Dim nodxAux As MSComctlLib.node

Set nodxAux = nodx

    
Dim sDep As Variant
Dim sUON1 As Variant
Dim sUON2 As Variant
Dim sUON3 As Variant
Dim sPer As Variant

sDep = Null
sUON1 = Null
sUON2 = Null
sUON3 = Null
sPer = Null

If nodxAux.Tag <> "UON0" Then
    Do
        
        Select Case Left(nodxAux.Tag, 4)
    
            Case "DEP0", "DEP1", "DEP2", "DEP3"
                
                    sDep = Right(nodxAux.Tag, Len(nodxAux.Tag) - 4)
                
            Case "UON1"
                    sUON1 = Right(nodxAux.Tag, Len(nodxAux.Tag) - 4)
                
            Case "UON2"
                    sUON2 = Right(nodxAux.Tag, Len(nodxAux.Tag) - 4)
                
            Case "UON3"
                    sUON3 = Right(nodxAux.Tag, Len(nodxAux.Tag) - 4)
                
            Case "PER0", "PER1", "PER2", "PER3"
                    sPer = Right(nodxAux.Tag, Len(nodxAux.Tag) - 4)
            
        End Select
        Set nodxAux = nodxAux.Parent
    Loop Until nodxAux.Tag = "UON0"
End If

Select Case sOrigen

    Case "frmPROCEBuscar"
        frmPROCEBuscar.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
        
    Case "frmLstPROCEA2B1"
        
        frmListados.ofrmLstProce1.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
        
    Case "frmLstPROCEA2B2"
        
        frmListados.ofrmlstProce2.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
        
    Case "frmLstPROCEA2B3"
        
        frmListados.ofrmLstProce3.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
        
    
    Case "frmLstPROCEA2B4C2"
    
        frmListados.ofrmLstProce4.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
    
    Case "frmLstPROCEA2B5"
        
        frmListados.ofrmlstProce5.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
    Case "frmLstPROCEA6B3C1"
        
        frmListados.ofrmLstProce6.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
        
    Case "frmLstPROCEfrmPROCE"
        
        frmPROCE.g_ofrmLstPROCE.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
        
    Case "frmLstPROCEfrmSELPROVE"
        
        frmSELPROVE.ofrmLstPROCE.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
        
    Case "frmLstPROCEfrmOFEPet"
        
        frmOFEPet.ofrmLstPROCE.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
        
    Case "frmLstPROCEfrmOFERec"
        
        frmOFERec.ofrmLstPROCE.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
        
    Case "frmLstPROCEfrmOFEHistWeb"
        
        frmOFEHistWeb.ofrmLstPROCE.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
        
    Case Else
        
        frmPROCEBuscar.PersonaSeleccionada giTipo, sUON1, sUON2, sUON3, sDep, sPer
    
    
End Select


Unload Me

End Sub

Private Sub cmdCancelar_Click()
         
    Unload Me
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELPERWORKFLOW, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Ador.MoveNext
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sIdiDetalle = Ador(0).Value
        Ador.MoveNext
        sIdiAnyadirAp = Ador(0).Value
        Ador.MoveNext
        sIdiModificarAp = Ador(0).Value
        Ador.MoveNext
        sIdiSustituirAp = Ador(0).Value
        Ador.MoveNext
        sIdiSelPer = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()

Me.Width = 5670
Me.Height = 6645
Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

CargarRecursos

GenerarEstructuraOrg False

picEstrOrg.Enabled = True

caption = sIdiSelPer
    
End Sub

Private Sub Form_Resize()

If Me.Height < 1500 Then Exit Sub
If Me.Width < 800 Then Exit Sub
        
    picEstrOrg.Height = Me.Height - picNavigate.Height - 485
    tvwEstrOrg.Height = picEstrOrg.Height - 50
    picEstrOrg.Width = Me.Width - 205
    tvwEstrOrg.Width = picEstrOrg.Width - 50

End Sub

Private Sub Form_Unload(Cancel As Integer)

Set oAprovSeleccionado = Nothing
Set oAprovPadre = Nothing
    
End Sub

Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    ' Departamentos asociados
    Dim oDepsAsocN0 As CDepAsociados
    Dim oDepsAsocN1 As CDepAsociados
    Dim oDepsAsocN2 As CDepAsociados
    Dim oDepsAsocN3 As CDepAsociados
    Dim oDepAsoc As CDepAsociado
    ' Unidades organizativas
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3
    ' Personas
    Dim oPersN0 As CPersonas
    Dim oPersN1 As CPersonas
    Dim oPersN2 As CPersonas
    Dim oPersN3 As CPersonas
    Dim oPer As CPersona
    ' Otras
    Dim nodx As node
    Dim varCodPersona As Variant
    Dim lIdPerfil As Long

    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
     
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    
    Set oPersN0 = oFSGSRaiz.Generar_CPersonas
    Set oPersN1 = oFSGSRaiz.Generar_CPersonas
    Set oPersN2 = oFSGSRaiz.Generar_CPersonas
    Set oPersN3 = oFSGSRaiz.Generar_CPersonas
         
    If (((oUsuarioSummit.Tipo = TipoDeUsuario.Persona Or oUsuarioSummit.Tipo = TipoDeUsuario.comprador) And _
       (bRUO Or bRDep Or bRPerfUO Or basOptimizacion.gPYMEUsuario <> 0))) Then
        
        varCodPersona = oUsuarioSummit.Persona.Cod
        If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
        oDepsAsocN0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 0, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
        
        ' Cargamos las personas  de esos departamentos.
        For Each oDepAsoc In oDepsAsocN0
             oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
        Next
            
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , False, , bOrdenadoPorDen, False, , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
            Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , , bRPerfUO, lIdPerfil
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
            Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , bRPerfUO, lIdPerfil
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , , bRPerfUO, lIdPerfil
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
                oDepsAsocN3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 3, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
                ' Cargamos las personas  de esos departamentos.
        
                'Personas
                For Each oDepAsoc In oDepsAsocN1
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN2
                   oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN3
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
        End Select
    Else
        oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
        oPersN0.CargarTodasLasPersonas , , , , 0, , True, , , False, , True
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False, , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
            Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False, , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , bRPerfUO, lIdPerfil
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False, , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
            Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , bRPerfUO, lIdPerfil
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , , bRPerfUO, lIdPerfil
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, , , , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False, , basOptimizacion.gPYMEUsuario, bRPerfUO, lIdPerfil
        End Select
                
        For Each oDepAsoc In oDepsAsocN1
            oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
        Next
        
        For Each oDepAsoc In oDepsAsocN2
           oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
        Next
    End If
    
   '************************************************************
    'Generamos la estructura arborea
    tvwEstrOrg.Nodes.clear

    ' Unidades organizativas
    Set nodx = tvwEstrOrg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
    Next
    
    ' Departamentos
    
    If (((basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador) _
        And (bRUO Or bRDep)) Or (Not IsEmpty(basOptimizacion.gPYMEUsuario))) Then
    
        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
            Next
        Next
                
        For Each oDepAsoc In oDepsAsocN1
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER1" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
            Next
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER2" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
            Next
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
            Next
        Next
    Else
        'Departamentos
        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
        Next
                
        For Each oDepAsoc In oDepsAsocN1
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
        Next
                
        'Personas
        For Each oPer In oPersN0
            scod1 = oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER0" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN1
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER1" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN2
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
            scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER2" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN3
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
            scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
            scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER3" & CStr(oPer.Cod)
        Next
    End If
    
    Set nodx = Nothing
    Set nodx = Nothing
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    Set oDepAsoc = Nothing
    Set oPersN0 = Nothing
    Set oPersN1 = Nothing
    Set oPersN2 = Nothing
    Set oPersN3 = Nothing
    Set oPer = Nothing
End Sub


Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

Select Case Left(node.Tag, 4)

Case "DEP0", "DEP1", "DEP2", "DEP3"
    
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "UON1"
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "UON2"
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "UON3"
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "PER0", "PER1", "PER2", "PER3"
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
End Select

End Function




