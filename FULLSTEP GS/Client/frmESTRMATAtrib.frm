VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmESTRMATAtrib 
   Caption         =   "DAtributos del"
   ClientHeight    =   11460
   ClientLeft      =   60
   ClientTop       =   2655
   ClientWidth     =   17640
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRMATAtrib.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   11460
   ScaleWidth      =   17640
   Begin TabDlg.SSTab SSTabAtributos 
      Height          =   11415
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   17535
      _ExtentX        =   30930
      _ExtentY        =   20135
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "dAtributos"
      TabPicture(0)   =   "frmESTRMATAtrib.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "picAtributos"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "PicEdit"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "dAtributos/Unidad Org."
      TabPicture(1)   =   "frmESTRMATAtrib.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdActualizarAtributosUon"
      Tab(1).Control(1)=   "picAtributosUon"
      Tab(1).ControlCount=   2
      Begin VB.CommandButton cmdActualizarAtributosUon 
         Caption         =   "DActualizar"
         Height          =   345
         Left            =   -74760
         TabIndex        =   11
         Top             =   4080
         Width           =   1050
      End
      Begin VB.PictureBox PicEdit 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   435
         Left            =   120
         ScaleHeight     =   435
         ScaleWidth      =   8850
         TabIndex        =   5
         Top             =   4080
         Width           =   8850
         Begin VB.CommandButton cmdAsignar 
            Caption         =   "D&Asignar"
            Height          =   345
            Left            =   0
            TabIndex        =   8
            Top             =   30
            Width           =   1050
         End
         Begin VB.CommandButton cmdRestaurar 
            Caption         =   "D&Restaurar"
            Height          =   345
            Left            =   2175
            TabIndex        =   7
            Top             =   30
            Width           =   1050
         End
         Begin VB.CommandButton cmdDesAsignar 
            Caption         =   "D&DesAsignar"
            Height          =   345
            Left            =   1080
            TabIndex        =   6
            Top             =   30
            Width           =   1050
         End
      End
      Begin VB.PictureBox picAtributosUon 
         Height          =   3495
         Left            =   -74880
         ScaleHeight     =   3435
         ScaleWidth      =   8475
         TabIndex        =   2
         Top             =   480
         Width           =   8535
         Begin SSDataWidgets_B.SSDBDropDown SSDBValorUon 
            Height          =   840
            Left            =   2640
            TabIndex        =   10
            Top             =   1320
            Width           =   3735
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmESTRMATAtrib.frx":0CEA
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "VALOR"
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6588
            _ExtentY        =   1482
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAtributosUon 
            Height          =   4170
            Left            =   0
            TabIndex        =   9
            Top             =   0
            Width           =   9090
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   3
            stylesets.count =   6
            stylesets(0).Name=   "Yellow"
            stylesets(0).BackColor=   14745599
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmESTRMATAtrib.frx":0D06
            stylesets(1).Name=   "Gris"
            stylesets(1).ForeColor=   -2147483633
            stylesets(1).BackColor=   -2147483633
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmESTRMATAtrib.frx":0D22
            stylesets(2).Name=   "bold"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmESTRMATAtrib.frx":0D3E
            stylesets(3).Name=   "Normal"
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmESTRMATAtrib.frx":0D5A
            stylesets(4).Name=   "BLUE"
            stylesets(4).BackColor=   16777184
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "frmESTRMATAtrib.frx":0D76
            stylesets(5).Name=   "Green"
            stylesets(5).BackColor=   7190198
            stylesets(5).HasFont=   -1  'True
            BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(5).Picture=   "frmESTRMATAtrib.frx":0D92
            AllowDelete     =   -1  'True
            AllowColumnMoving=   0
            AllowColumnSwapping=   0
            SelectTypeRow   =   3
            BalloonHelp     =   0   'False
            CellNavigation  =   1
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "DC�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).Style=   1
            Columns(0).HasHeadForeColor=   -1  'True
            Columns(0).HasHeadBackColor=   -1  'True
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).HeadForeColor=   16777215
            Columns(0).HeadBackColor=   32896
            Columns(0).BackColor=   16777184
            Columns(1).Width=   7250
            Columns(1).Caption=   "DNombre"
            Columns(1).Name =   "NOMBRE"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasHeadForeColor=   -1  'True
            Columns(1).HasHeadBackColor=   -1  'True
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).HeadForeColor=   16777215
            Columns(1).HeadBackColor=   32896
            Columns(1).StyleSet=   "BLUE"
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "ID"
            Columns(2).Name =   "ID"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   16034
            _ExtentY        =   7355
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.PictureBox picAtributos 
         Height          =   3495
         Left            =   120
         ScaleHeight     =   3435
         ScaleWidth      =   8475
         TabIndex        =   1
         Top             =   480
         Width           =   8535
         Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
            Height          =   360
            Left            =   2520
            TabIndex        =   4
            Top             =   1320
            Width           =   3015
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmESTRMATAtrib.frx":0DAE
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "VALOR"
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5318
            _ExtentY        =   635
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
            Height          =   4170
            Left            =   0
            TabIndex        =   3
            Top             =   0
            Width           =   9090
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   16
            stylesets.count =   6
            stylesets(0).Name=   "Gris"
            stylesets(0).BackColor=   12632256
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmESTRMATAtrib.frx":0DCA
            stylesets(1).Name=   "Yellow"
            stylesets(1).BackColor=   14745599
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmESTRMATAtrib.frx":0DE6
            stylesets(2).Name=   "bold"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmESTRMATAtrib.frx":0E02
            stylesets(3).Name=   "Normal"
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmESTRMATAtrib.frx":0E1E
            stylesets(4).Name=   "BLUE"
            stylesets(4).BackColor=   16777184
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "frmESTRMATAtrib.frx":0E3A
            stylesets(5).Name=   "Green"
            stylesets(5).BackColor=   7190198
            stylesets(5).HasFont=   -1  'True
            BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(5).Picture=   "frmESTRMATAtrib.frx":0E56
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   2
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            BalloonHelp     =   0   'False
            CellNavigation  =   1
            StyleSet        =   "Normal"
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   16
            Columns(0).Width=   2434
            Columns(0).Caption=   "DC�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).Style=   1
            Columns(0).ButtonsAlways=   -1  'True
            Columns(0).HasHeadForeColor=   -1  'True
            Columns(0).HasHeadBackColor=   -1  'True
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).HeadForeColor=   16777215
            Columns(0).HeadBackColor=   32896
            Columns(0).BackColor=   16777184
            Columns(1).Width=   11298
            Columns(1).Caption=   "DNombre"
            Columns(1).Name =   "NOMBRE"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasHeadForeColor=   -1  'True
            Columns(1).HasHeadBackColor=   -1  'True
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).HeadForeColor=   16777215
            Columns(1).HeadBackColor=   32896
            Columns(1).StyleSet=   "BLUE"
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "HER"
            Columns(2).Name =   "HER"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   11
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "ID_ATRIB"
            Columns(3).Name =   "ID_ATRIB"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   1270
            Columns(4).Caption=   "Descr."
            Columns(4).Name =   "DESCR"
            Columns(4).Alignment=   2
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Style=   4
            Columns(4).ButtonsAlways=   -1  'True
            Columns(4).HasHeadForeColor=   -1  'True
            Columns(4).HasHeadBackColor=   -1  'True
            Columns(4).HeadForeColor=   16777215
            Columns(4).HeadBackColor=   32896
            Columns(4).StyleSet=   "bold"
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "HIDDENDESCR"
            Columns(5).Name =   "HIDENDESCR"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Caption=   "VALOR"
            Columns(6).Name =   "VALOR"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).HasHeadForeColor=   -1  'True
            Columns(6).HasHeadBackColor=   -1  'True
            Columns(6).HasForeColor=   -1  'True
            Columns(6).HeadForeColor=   16777215
            Columns(6).HeadBackColor=   32896
            Columns(7).Width=   3200
            Columns(7).Caption=   "PROCESO"
            Columns(7).Name =   "PROCESO"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(7).Style=   2
            Columns(7).HasHeadForeColor=   -1  'True
            Columns(7).HasHeadBackColor=   -1  'True
            Columns(7).HeadForeColor=   16777215
            Columns(7).HeadBackColor=   32896
            Columns(8).Width=   3200
            Columns(8).Caption=   "PEDIDO"
            Columns(8).Name =   "PEDIDO"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(8).Style=   2
            Columns(8).HasHeadForeColor=   -1  'True
            Columns(8).HasHeadBackColor=   -1  'True
            Columns(8).HeadForeColor=   16777215
            Columns(8).HeadBackColor=   32896
            Columns(9).Width=   3200
            Columns(9).Caption=   "RECEP"
            Columns(9).Name =   "RECEP"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(9).Style=   2
            Columns(9).HasHeadForeColor=   -1  'True
            Columns(9).HasHeadBackColor=   -1  'True
            Columns(9).HasForeColor=   -1  'True
            Columns(9).HasBackColor=   -1  'True
            Columns(9).HeadForeColor=   16777215
            Columns(9).HeadBackColor=   32896
            Columns(9).BackColor=   16777215
            Columns(10).Width=   3200
            Columns(10).Caption=   "OBLIGATORIO"
            Columns(10).Name=   "OBLIGATORIO"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(10).Style=   2
            Columns(10).HasHeadForeColor=   -1  'True
            Columns(10).HasHeadBackColor=   -1  'True
            Columns(10).HeadForeColor=   16777215
            Columns(10).HeadBackColor=   32896
            Columns(11).Width=   3200
            Columns(11).Caption=   "INTERNO"
            Columns(11).Name=   "INTERNO"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(11).Style=   2
            Columns(11).HasHeadForeColor=   -1  'True
            Columns(11).HasHeadBackColor=   -1  'True
            Columns(11).HeadForeColor=   16777215
            Columns(11).HeadBackColor=   32896
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "TIPO_VALOR"
            Columns(12).Name=   "TIPO_VALOR"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(13).Width=   3200
            Columns(13).Visible=   0   'False
            Columns(13).Caption=   "MIN"
            Columns(13).Name=   "MIN"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(14).Width=   3200
            Columns(14).Visible=   0   'False
            Columns(14).Caption=   "MAX"
            Columns(14).Name=   "MAX"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(15).Width=   3200
            Columns(15).Visible=   0   'False
            Columns(15).Caption=   "INTRO"
            Columns(15).Name=   "TIPO_INTRO"
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            _ExtentX        =   16034
            _ExtentY        =   7355
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
End
Attribute VB_Name = "frmESTRMATAtrib"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variable de control de flujo de proceso
Public Accion As accionessummit

Public g_sGmn1 As String
Public g_sGmn2 As String
Public g_sGmn3 As String
Public g_sGmn4 As String
Public g_vCodArt As Variant
Public g_vIdPed As Variant
Public g_sOrigen As String
Public g_oAtributos As CAtributos
Public g_sCodPed As String
Public g_sDenPed As String

Public g_oAtributo As CAtributo
Public g_sCodigoNuevo As String
Public g_bCodigoCancelar As Boolean
Public g_iNumIdiomas As Integer

Private m_sTitulo As String
Private m_sIdiMenAtributo As String
Private m_sNomArt As String
Private m_sNomMat As String
Private m_sDescripcion  As String
Private m_sTipoDePedido  As String

Private m_asAtrib(4) As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_sProceso As String
Private m_sPedido As String
Private m_sInterno As String
Private m_sObligatorio As String
Private m_sRecepcion As String
Private m_aBookmarks As Collection

Private m_bModificacion As Boolean
Public g_ofrmATRIB As frmAtrib
Public g_bModoEdicion As Boolean

Public dicNuevosAtrib As Dictionary
'atributos uons
Private m_oArticulo As CArticulo
Private m_oAtributosUon As CAtributos
Private m_oAtributoUon As CAtributo
Private m_oAtributosUonValorados As CAtributos
Private m_oUonsAtributo As CUnidadesOrganizativas
Private m_oUonsRestringidas As CUnidadesOrganizativas
Private m_bRestrMantAtribUsu As Boolean
Private m_bRestrMantAtribPerf As Boolean


Public Property Set Articulo(ByRef oArticulo As CArticulo)
    Set m_oArticulo = oArticulo
End Property

Public Property Get Articulo() As CArticulo
    Set Articulo = m_oArticulo
End Property


Private Sub cmdActualizarAtributosUon_Click()
    cargarGridAtributosUon
End Sub

Private Sub cmdAsignar_Click()
        
    Screen.MousePointer = vbHourglass
        
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If
    
    If sdbgAtributos.DataChanged Then
        sdbgAtributos.Update
    End If
        
    Set g_oAtributos = Nothing
    Set g_oAtributos = oFSGSRaiz.Generar_CAtributos
    
    If g_sOrigen <> "TIPPED" Then
        g_sGmn1 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
        g_sGmn2 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
        g_sGmn3 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.Cells("COD").Value
        g_sGmn4 = frmESTRMAT.ssMateriales.ActiveRow.Cells("COD").Value
    End If
    
    Set g_ofrmATRIB = New frmAtrib
        
    '3328
    g_ofrmATRIB.g_sOrigen = g_sOrigen
    If g_sOrigen = "ART4" Then
        g_ofrmATRIB.g_sArtCod = frmESTRMAT.sdbgArticulos.Columns("COD").Value
    End If
    If g_sOrigen = "TIPPED" Then
        g_ofrmATRIB.g_sPedId = g_vIdPed 'frmMantPedidos.sdbgTiposPedido.Columns("COD").Value
    End If
    If g_sOrigen <> "TIPPED" Then
        g_ofrmATRIB.g_sGmn1 = g_sGmn1
        g_ofrmATRIB.g_sGmn2 = g_sGmn2
        g_ofrmATRIB.g_sGmn3 = g_sGmn3
        g_ofrmATRIB.g_sGmn4 = g_sGmn4
    End If
    g_ofrmATRIB.sstabGeneral.Tab = 0
    g_ofrmATRIB.Icon = frmPROCEBuscar.Icon
    g_ofrmATRIB.Backcolor = &H808000
    g_ofrmATRIB.sstabGeneral.Backcolor = &H808000
    g_ofrmATRIB.sstabGeneral.Tab = 1
    g_ofrmATRIB.sdbgAtributosGrupo.SelectTypeRow = ssSelectionTypeMultiSelectRange
    
    Screen.MousePointer = vbNormal
    
    g_ofrmATRIB.Show
End Sub


Private Sub cmdDesAsignar_Click()

 If sdbgAtributos.Columns("COD").Value = "" Or IsEmpty(sdbgAtributos.Columns("COD").Value) Then Exit Sub
 
 If sdbgAtributos.DataChanged Then
    sdbgAtributos.Update
 End If

 If sdbgAtributos.Rows > 0 Then

        Screen.MousePointer = vbHourglass

        If sdbgAtributos.SelBookmarks.Count = 0 Then
            sdbgAtributos.SelBookmarks.Add sdbgAtributos.Bookmark
        End If
        DesasignarAtributosSeleccionados
        Accion = ACCArtAtribMod
        Screen.MousePointer = vbNormal
        sdbgAtributos.Refresh
        If sdbgAtributos.Rows = 0 Then
            cmdDesAsignar.Enabled = False
        End If
 End If

End Sub

Public Sub cmdRestaurar_Click()
 
''' * Objetivo: Restaurar el contenido de la grid
''' * Objetivo: desde la base de datos
Dim oGrupoMat As CGrupoMatNivel4
Dim oArticulo As CArticulo
Dim oTipoPedido As CTipoPedido
    
Screen.MousePointer = vbHourglass
    
If sdbgAtributos.DataChanged Then
    sdbgAtributos.Update
End If

Set g_oAtributos = Nothing

Set g_oAtributos = oFSGSRaiz.Generar_CAtributos

'3328
If g_sOrigen <> "TIPPED" Then
    g_sGmn1 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
    g_sGmn2 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
    g_sGmn3 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.Cells("COD").Value
    g_sGmn4 = frmESTRMAT.ssMateriales.ActiveRow.Cells("COD").Value
End If

If g_sOrigen = "GMN4" Then
    frmESTRMATAtrib.caption = m_sTitulo & " " & m_sNomMat & ": " & g_sGmn1 & "-" & g_sGmn2 & "-" & g_sGmn3 & "-" & g_sGmn4
    
    Set oGrupoMat = oFSGSRaiz.Generar_CGrupoMatNivel4
    oGrupoMat.GMN1Cod = g_sGmn1
    oGrupoMat.GMN2Cod = g_sGmn2
    oGrupoMat.GMN3Cod = g_sGmn3
    oGrupoMat.Cod = g_sGmn4
    
    Set g_oAtributos = oGrupoMat.DevolverTodosLosAtributosDelMaterial(False)
End If

If g_sOrigen = "ART4" Or g_sOrigen = "FRMARTCENTRALUNIFICAR" Or g_sOrigen = "FRMARTCENTRALUNIFCONFIRM" Then
    frmESTRMATAtrib.caption = m_sTitulo & " " & m_sNomArt & ": " & g_sGmn1 & "-" & g_sGmn2 & "-" & g_sGmn3 & "-" & g_sGmn4 & "-" & g_vCodArt
    
    Set oArticulo = oFSGSRaiz.Generar_CArticulo
    oArticulo.GMN1Cod = g_sGmn1
    oArticulo.GMN2Cod = g_sGmn2
    oArticulo.GMN3Cod = g_sGmn3
    oArticulo.GMN4Cod = g_sGmn4
    oArticulo.Cod = g_vCodArt
    
    Set g_oAtributos = oArticulo.DevolverTodosLosAtributosDelArticulo
    Set m_oAtributosUon = m_oArticulo.getAtributosUon
    
End If

'3328
If g_sOrigen = "TIPPED" Then
    Set oTipoPedido = Nothing
    
    Set oTipoPedido = oFSGSRaiz.Generar_CTipoPedido
    oTipoPedido.Id = g_vIdPed
    oTipoPedido.DevolverTodosLosAtributosDelTipoPedido
    
    frmESTRMATAtrib.caption = m_sTitulo & " " & m_sTipoDePedido & " " & g_sCodPed & " - " & g_sDenPed
    
    Set g_oAtributos = oTipoPedido.ATRIBUTOS
End If

sdbgAtributos.RemoveAll

CargarGridEspecificaciones

Screen.MousePointer = vbNormal
 
End Sub



Private Sub Form_Initialize()
    Set m_oAtributoUon = oFSGSRaiz.Generar_CAtributo
    Set m_oAtributosUon = oFSGSRaiz.Generar_CAtributos
    Set m_oUonsAtributo = oFSGSRaiz.Generar_CUnidadesOrganizativas
    Set m_oUonsRestringidas = oFSGSRaiz.Generar_CUnidadesOrganizativas
End Sub


Private Sub Form_Resize()
    If Me.Width > 500 And Me.Height > 1900 Then
        Me.SSTabAtributos.Width = frmESTRMATAtrib.Width - 140
        Me.SSTabAtributos.Height = frmESTRMATAtrib.Height - 900
        
        Me.picAtributos.Width = frmESTRMATAtrib.Width - 900
        Me.picAtributos.Height = frmESTRMATAtrib.Height - Me.cmdAsignar.Height - 1500
        
        Me.picAtributosUon.Width = frmESTRMATAtrib.Width - 900
        Me.picAtributosUon.Height = frmESTRMATAtrib.Height - Me.cmdActualizarAtributosUon.Height - 1500
        
        Me.cmdActualizarAtributosUon.Top = Me.picAtributosUon.Top + Me.picAtributosUon.Height + 10
        
        Me.picEdit.Top = Me.picAtributos.Top + picAtributos.Height + 10
        
        sdbgAtributosUon.Width = Me.picAtributosUon.Width - 50
        sdbgAtributosUon.Height = Me.picAtributosUon.Height - 50
        
        sdbgAtributos.Width = Me.picAtributos.Width - 50
        sdbgAtributos.Height = Me.picAtributos.Height - 50

        'sdbgAtributos.Top = 20
        
        sdbgAtributos.Columns("COD").Width = (sdbgAtributos.Width - 570) * 0.18
        sdbgAtributos.Columns("NOMBRE").Width = (sdbgAtributos.Width - 570) * 0.37
        sdbgAtributos.Columns("DESCR").Width = (sdbgAtributos.Width - 570) * 0.07
        sdbgAtributos.Columns("VALOR").Width = (sdbgAtributos.Width - 570) * 0.18
        
        
        '3328
        If g_sOrigen = "TIPPED" Then
            sdbgAtributos.Columns("OBLIGATORIO").Width = (sdbgAtributos.Width - 570) * 0.05
        Else
            sdbgAtributos.Columns("PROCESO").Width = (sdbgAtributos.Width - 570) * 0.05
            sdbgAtributos.Columns("PEDIDO").Width = (sdbgAtributos.Width - 570) * 0.05
            sdbgAtributos.Columns("RECEP").Width = (sdbgAtributos.Width - 570) * 0.05
        End If
        sdbgAtributos.Columns("INTERNO").Width = (sdbgAtributos.Width - 570) * 0.05
        picEdit.Width = sdbgAtributos.Width

    End If
End Sub


Private Sub Form_Terminate()
    Set m_oAtributoUon = Nothing
    Set m_oAtributosUon = Nothing
    Set m_oArticulo = Nothing
    Set m_oUonsAtributo = Nothing
    Set m_oUonsRestringidas = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)

    'Si hay cambios sin guardar
    If sdbgAtributos.DataChanged = True Then
        sdbgAtributos.Update
    End If
         
    '3328
    If g_sOrigen <> "GMN4" And g_sOrigen <> "TIPPED" Then
        If sdbgAtributos.Rows > 0 Or Me.Articulo.getAtributosUonValorados.Count > 0 Then
           frmESTRMAT.g_oArticulos.Item(CStr(frmESTRMAT.sdbgArticulos.Bookmark)).ConAtributos = True
           
           'Al cerrar la ventana de asignaci�n de atributos al mantenimiento de materiales, se hace la llamada a FSIS
           If frmESTRMAT.g_oArticulos.Item(CStr(frmESTRMAT.sdbgArticulos.Bookmark)).uons.Count <> 0 Then
                Articulo.LlamarFSIS
           End If

        Else
           frmESTRMAT.g_oArticulos.Item(CStr(frmESTRMAT.sdbgArticulos.Bookmark)).ConAtributos = False
        End If
        If frmESTRMAT.sdbgArticulos.Rows > 0 Then
            frmESTRMAT.sdbgArticulos.Refresh
            frmESTRMAT.sdbgArticulos.col = 7
        End If
    End If
    
    '3328
    If g_sOrigen = "TIPPED" Then
        If sdbgAtributos.Rows > 0 Then
           frmMantPedidos.g_oTiposPedido.Item(CStr(frmMantPedidos.sdbgTiposPedido.Bookmark)).ConAtributos = True
        Else
           frmMantPedidos.g_oTiposPedido.Item(CStr(frmMantPedidos.sdbgTiposPedido.Bookmark)).ConAtributos = False
        End If
        If frmMantPedidos.sdbgTiposPedido.Rows > 0 Then
            frmMantPedidos.sdbgTiposPedido.Refresh
            frmMantPedidos.sdbgTiposPedido.col = 1
        End If
    End If
    
    If g_sOrigen = "FRMARTCENTRALUNIFICAR" Then
        frmArtCentralUnificar.SetFocus
    End If
    
    If g_sOrigen = "FRMARTCENTRALUNIFCONFIRM" Then
        frmArtCentralUnifConfirm.SetFocus
    End If
    
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If
    'Por si hubieramos deshabilitado los botones de asignar desasignar atributos los volvemos a habilitar
    'ya que al abrirse siempre la misma instancia de formulario se conservar�a el disable y puede no ser el
    'efecto deseado
    Me.cmdAsignar.Enabled = True
    Me.cmdDesAsignar.Enabled = True
    
    Me.Visible = False
    
    Set m_aBookmarks = Nothing
    m_bModificacion = False
    Set Articulo = Nothing
    
End Sub

Private Sub Form_Load()
    
    Me.Top = 100 'MDI.Top + MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = 100 'MDI.ScaleWidth / 2 - Me.Width / 2
    'Me.Width = 10755
    'Me.Height = 4950
    CargarRecursos
    
    PonerFieldSeparator Me
     
    '3328
    If g_sOrigen <> "TIPPED" Then
        g_sGmn1 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
        g_sGmn2 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
        g_sGmn3 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.Cells("COD").Value
        g_sGmn4 = frmESTRMAT.ssMateriales.ActiveRow.Cells("COD").Value
        sdbgAtributos.Columns("OBLIGATORIO").Visible = False
    Else
        sdbgAtributos.Columns("OBLIGATORIO").Visible = True
        sdbgAtributos.Columns("PEDIDO").Visible = False
        sdbgAtributos.Columns("PROCESO").Visible = False
    End If
    
    cmdDesAsignar.Enabled = False
    
    sdbgAtributos.ReBind
    
    If Not m_oArticulo Is Nothing Then
        If m_bRestrMantAtribUsu Or m_bRestrMantAtribPerf Then
            m_oUonsRestringidas.cargarTodasLasUnidadesUsuPerf oUsuarioSummit, m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        End If
        cargarGridAtributosUon
        Me.SSTabAtributos.TabVisible(1) = True
    Else
        Me.SSTabAtributos.TabVisible(1) = False
    End If
    
    
    CargarGridEspecificaciones
    
    
    ConfigurarSeguridad
    
    
    sdbddValor.AddItem ""
    
    '3328
    If g_sOrigen = "GMN4" Then
        frmESTRMATAtrib.caption = m_sTitulo & " " & m_sNomMat & ": " & g_sGmn1 & "-" & g_sGmn2 & "-" & g_sGmn3 & "-" & g_sGmn4
    ElseIf g_sOrigen <> "TIPPED" Then
        frmESTRMATAtrib.caption = m_sTitulo & " " & m_sNomArt & ": " & g_sGmn1 & "-" & g_sGmn2 & "-" & g_sGmn3 & "-" & g_sGmn4 & "-" & g_vCodArt
    ElseIf g_sOrigen = "TIPPED" Then
        frmESTRMATAtrib.caption = m_sTitulo & " " & m_sTipoDePedido & " " & g_sCodPed & " - " & g_sDenPed
    End If

    sdbgAtributos.AllowAddNew = False
    sdbgAtributos.AllowDelete = False
    sdbgAtributos.AllowUpdate = True
        
    If Not g_bModoEdicion Then ModoConsulta
    
    sdbgAtributos.Columns("COD").Locked = True
    sdbgAtributos.Columns("NOMBRE").Locked = True
    
    Set dicNuevosAtrib = New Dictionary
End Sub

''' <summary>Establece las propiedades de los controles de pantalla para que sea de s�lo consulta</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub ModoConsulta()
    sdbgAtributos.Columns("VALOR").Locked = True
    '3328
    If g_sOrigen <> "TIPPED" Then
        sdbgAtributos.Columns("PROCESO").Locked = True
        sdbgAtributos.Columns("PEDIDO").Locked = True
    Else
        sdbgAtributos.Columns("OBLIGATORIO").Locked = True
    End If
    sdbgAtributos.Columns("INTERNO").Locked = True
    
    cmdAsignar.Enabled = False
End Sub

''' <summary>
''' Evento producido en el campo del valor del atributo al hacer click para expandir la lista de valores del atributo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click en la lista de valores de un atributo </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValor_DropDown()

Dim oLista As CValorPond
Dim iIndice As Integer


    If sdbgAtributos.Columns("VALOR").Locked Then
        sdbddValor.DroppedDown = False
        Exit Sub
    End If
    
    
    iIndice = 1

    sdbddValor.RemoveAll

    Select Case sdbgAtributos.Columns("TIPO_VALOR").Value
    
    Case 1, 2, 3
            g_oAtributos.Item(sdbgAtributos.Columns("ID_ATRIB").Value).CargarListaDeValoresDef
            For Each oLista In g_oAtributos.Item(sdbgAtributos.Columns("ID_ATRIB").Value).ListaPonderacion
                sdbddValor.AddItem iIndice & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
            'End If
        
    Case 4
        sdbddValor.AddItem "" & Chr(m_lSeparador) & ""
        sdbddValor.AddItem 1 & Chr(m_lSeparador) & m_sIdiTrue
        sdbddValor.AddItem 0 & Chr(m_lSeparador) & m_sIdiFalse
        
        
    End Select


End Sub

''' <summary>
''' Inicializacion de las columnas del combo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Posicionamiento en la lista del combo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click en la lista de valores de un atributo </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns("VALOR").Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

''' <summary>
''' Validacion de la seleccion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click en la lista de valores de un atributo </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
Dim bExiste As Boolean
Dim oLista As CValoresPond
Dim oElem As CValorPond
Dim oatrib As CAtributo

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        
        Set oatrib = g_oAtributos.Item(sdbgAtributos.Columns("ID_ATRIB").Value)
        If oatrib Is Nothing Then Exit Sub

        If oatrib.TipoIntroduccion = Introselec Then
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If oatrib.Tipo = TipoBoolean Then
                If UCase(m_sIdiTrue) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                    bExiste = True
                End If
                If UCase(m_sIdiFalse) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                    bExiste = True
                End If

            End If
        End If
        If Not bExiste Then
            sdbgAtributos.Columns("VALOR").Text = ""
            oMensajes.NoValido sdbgAtributos.Columns("VALOR").caption
            RtnPassed = False
'            m_bModError = True
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub


Private Sub sdbgAtributos_AfterDelete(RtnDispErrMsg As Integer)
  
  ''' * Objetivo: Posicionar el bookmark en la fila
  ''' * Objetivo: actual despues de eliminar
    
    RtnDispErrMsg = 0
    If Me.Visible Then sdbgAtributos.SetFocus
    sdbgAtributos.Bookmark = sdbgAtributos.RowBookmark(sdbgAtributos.Row)
    Accion = ACCArtAtribMod
    
End Sub

Private Sub sdbgAtributos_AfterInsert(RtnDispErrMsg As Integer)
''' * Objetivo: Si no hay error, volver a la
''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
    
End Sub


Private Sub sdbgatributos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

''' <summary>
''' Evento de la grid de la tabla de atributos de especificacion, comprueba que los valores son correctos
''' </summary>
''' <returns></returns>
''' <param name="Cancel">Cancelacion de la actualizacion </param>
''' <remarks>Llamada desde: Al hacer un cambio en la grid, al cambiar de linea</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
''' <remarks>Revisado por: LTG  Fecha: 05/09/2011</remarks>
'
Private Sub sdbgatributos_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim m_oAtributoEnEdicion As CAtributo
    Dim iRet As Integer
    Dim bMensaje As Boolean
    Dim bCambiarCaractArt As Boolean
    Dim bInterno As Boolean
    Dim bProceso As Boolean
    Dim bPedido As Boolean
    Dim bOgligatorio As Boolean
    Dim bCambioInterno As Boolean
    Dim bCambioProceso As Boolean
    Dim bCambioPedido As Boolean
    Dim bCambioRecepcion As Boolean
    Dim bCambioObligatorio As Boolean
    Dim bRecepcion As Boolean
    If sdbgAtributos.col = -1 Then Exit Sub
    
    Cancel = False
  
    'Validaci�n de valor.
    If UCase(sdbgAtributos.Columns("VALOR").Text) <> "" Then
    
        Select Case UCase(sdbgAtributos.Columns("TIPO_VALOR").Text)
    
            Case 2 'Numero
                        If (Not IsNumeric(sdbgAtributos.Columns("Valor").Text)) Then
                            oMensajes.AtributoValorNoValido ("TIPO2")
                            Cancel = True
                            Exit Sub
                        Else
                            If sdbgAtributos.Columns("MIN").Text <> "" And sdbgAtributos.Columns("MAX").Text <> "" Then
                                If StrToDbl0(sdbgAtributos.Columns("MIN").Text) > StrToDbl0(sdbgAtributos.Columns("Valor").Text) Or StrToDbl0(sdbgAtributos.Columns("MAX").Text) < StrToDbl0(sdbgAtributos.Columns("Valor").Text) Then
                                    oMensajes.ValorEntreMaximoYMinimo sdbgAtributos.Columns("MIN").Text, sdbgAtributos.Columns("MAX").Text
                                    Cancel = True
                                    Exit Sub
                                End If
                            End If
                        
                        End If
    
            Case 3 'Fecha
                            If (Not IsDate(sdbgAtributos.Columns("Valor").Text) And sdbgAtributos.Columns("Valor").Text <> "") Then
                                oMensajes.AtributoValorNoValido ("TIPO3")
                                    Cancel = True
                                    Exit Sub
                            Else
                                If sdbgAtributos.Columns("MIN").Text <> "" And sdbgAtributos.Columns("MAX").Text <> "" Then
                                    If CDate(sdbgAtributos.Columns("MIN").Text) > CDate(sdbgAtributos.Columns("Valor").Text) Or CDate(sdbgAtributos.Columns("MAX").Text) < CDate(sdbgAtributos.Columns("Valor").Text) Then
                                        oMensajes.ValorEntreMaximoYMinimo sdbgAtributos.Columns("MIN").Text, sdbgAtributos.Columns("MAX").Text
                                        Cancel = True
                                        Exit Sub
                                    End If
                                End If
                        End If
        End Select
    
    End If
    
 
    'Modificaci�n de atributo.
    Set m_oAtributoEnEdicion = Nothing
    Set m_oAtributoEnEdicion = g_oAtributos.Item(CStr(sdbgAtributos.Columns("ID_ATRIB").Value))
    
    If m_oAtributoEnEdicion Is Nothing Then Exit Sub
              
    If sdbgAtributos.Columns("INTERNO").Value = "0" Or sdbgAtributos.Columns("INTERNO").Value = "" Then
        bInterno = False
    Else
        bInterno = True
    End If
    
    '3328
    If g_sOrigen <> "TIPPED" Then
        If sdbgAtributos.Columns("PROCESO").Value = "0" Or sdbgAtributos.Columns("PROCESO").Value = "" Then
            bProceso = False
        Else
            bProceso = True
        End If
        
        If sdbgAtributos.Columns("PEDIDO").Value = "0" Or sdbgAtributos.Columns("PEDIDO").Value = "" Then
            bPedido = False
        Else
            bPedido = True
            bProceso = True
        End If
        If NullToDbl0(sdbgAtributos.Columns("RECEP").Value) = 0 Then
            bRecepcion = False
        Else
            bRecepcion = True
        End If
    Else
        If sdbgAtributos.Columns("OBLIGATORIO").Value = "0" Or sdbgAtributos.Columns("OBLIGATORIO").Value = "" Then
            bOgligatorio = False
        Else
            bOgligatorio = True
        End If
    End If
    
    'Comprobar si se ha cambiado las caracter�sticas proceso, pedido o interno
    '3328
    bCambioInterno = m_oAtributoEnEdicion.interno <> bInterno
    
    If g_sOrigen <> "TIPPED" Then
        bCambioProceso = m_oAtributoEnEdicion.proceso <> bProceso
        bCambioPedido = m_oAtributoEnEdicion.pedido <> bPedido
        bCambioRecepcion = m_oAtributoEnEdicion.Recepcion <> bRecepcion
        bCambiarCaractArt = bCambioInterno Or bCambioProceso Or bCambioPedido Or bCambioRecepcion
    Else
        bCambioObligatorio = m_oAtributoEnEdicion.Obligatorio <> bOgligatorio
        bCambiarCaractArt = bCambioInterno Or bCambioObligatorio
    End If
    
    m_oAtributoEnEdicion.interno = bInterno
    If g_sOrigen <> "TIPPED" Then
        m_oAtributoEnEdicion.proceso = bProceso
        m_oAtributoEnEdicion.pedido = bPedido
        m_oAtributoEnEdicion.Recepcion = bRecepcion
    Else
        m_oAtributoEnEdicion.Obligatorio = bOgligatorio
    End If
                                       
    Select Case sdbgAtributos.Columns("TIPO_VALOR").Value
        Case 1: m_oAtributoEnEdicion.valorText = sdbgAtributos.Columns("VALOR").Text

        Case 2: m_oAtributoEnEdicion.valorNum = sdbgAtributos.Columns("VALOR").Text
        Case 3: m_oAtributoEnEdicion.valorFec = sdbgAtributos.Columns("VALOR").Text
        Case 4:
                Select Case sdbgAtributos.Columns("VALOR").Text
                    Case m_sIdiTrue: m_oAtributoEnEdicion.valorBool = 1
                    Case m_sIdiFalse: m_oAtributoEnEdicion.valorBool = 0
                    Case Else: m_oAtributoEnEdicion.valorBool = ""
                End Select
    End Select
    
    '3328
    If g_sOrigen = "GMN4" Then
        bMensaje = bCambiarCaractArt
        'Comprobar si se est� modificando un atributo a�adido recientemente
        If bCambiarCaractArt Then
            If Not dicNuevosAtrib Is Nothing Then
                If dicNuevosAtrib.Count > 0 Then
                    If dicNuevosAtrib.Exists(sdbgAtributos.Columns("ID_ATRIB").Value) Then
                        bCambiarCaractArt = True
                        bMensaje = False
                    End If
                End If
            End If
        End If
        
        If bMensaje Then
            'Mensaje de aviso de que se actualizar� el valor en todos los art�culos
            iRet = oMensajes.AvisoModifiAtribDeArticulosDeMaterial(bCambioProceso, bCambioPedido, bCambioInterno, m_sProceso, m_sPedido, m_sInterno, bCambioRecepcion, m_sRecepcion)
            If iRet = vbNo Then bCambiarCaractArt = False
        End If
    
        bCambioProceso = bCambioProceso And bCambiarCaractArt
        bCambioPedido = bCambioPedido And bCambiarCaractArt
        bCambioRecepcion = bCambioRecepcion And bCambiarCaractArt
        bCambioInterno = bCambioInterno And bCambiarCaractArt
        teserror = m_oAtributoEnEdicion.ModificarAtributo(, bCambioProceso, bCambioPedido, bCambioInterno, oUsuarioSummit.Cod)
    ElseIf g_sOrigen <> "TIPPED" Then
        teserror = m_oAtributoEnEdicion.ModificarAtributo(g_vCodArt, , , , oUsuarioSummit.Cod)
    ElseIf g_sOrigen = "TIPPED" Then
        '3328
        teserror = m_oAtributoEnEdicion.ModificarAtributoTipoPedido(g_vIdPed)
    End If

    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        If Me.Visible Then sdbgAtributos.SetFocus
    End If

    If g_sOrigen = "GMN4" Then
        RegistrarAccion ACCAtributosMod, "Atributo modificado: " & sdbgAtributos.Columns("ID_ATRIB").Value & " GMN1: " & m_oAtributoEnEdicion.GMN1 & " GMN2: " & m_oAtributoEnEdicion.GMN2 & " GMN3: " & m_oAtributoEnEdicion.GMN3 & " GMN4: " & m_oAtributoEnEdicion.GMN4
    ElseIf g_sOrigen <> "TIPPED" Then
        RegistrarAccion ACCAtributosMod, "Atributo modificado: " & sdbgAtributos.Columns("ID_ATRIB").Value & " GMN1: " & m_oAtributoEnEdicion.GMN1 & " GMN2: " & m_oAtributoEnEdicion.GMN2 & " GMN3: " & m_oAtributoEnEdicion.GMN3 & " GMN4: " & m_oAtributoEnEdicion.GMN4 & " ART: " & g_vCodArt
    ElseIf g_sOrigen = "TIPPED" Then
        '3328
        RegistrarAccion ACCAtributosMod, "Atributo de Tipo de Pedido modificado: " & sdbgAtributos.Columns("ID_ATRIB").Value & " TIP_PED: " & g_vIdPed
    End If
            
End Sub

Private Sub sdbgAtributos_BtnClick()
    
    
    Select Case sdbgAtributos.Columns(sdbgAtributos.col).Name
        Case "DESCR"
            If Trim(sdbgAtributos.Columns("HIDENDESCR").Value) <> "" Then
                frmATRIBDescr.g_sOrigen = "ESTRMAT_ATRIB"
                frmATRIBDescr.g_bEdicion = False
                frmATRIBDescr.txtDescr.Text = sdbgAtributos.Columns("HIDENDESCR").Value
                frmATRIBDescr.Show 1
            Else
                oMensajes.MensajeOKOnly 644
                Exit Sub
            End If
        Case "VALOR"
            If sdbgAtributos.Columns("TIPO_VALOR").Value = TiposDeAtributos.TipoString Or _
               sdbgAtributos.Columns("TIPO_VALOR").Value = TiposDeAtributos.TipoTextoLargo Or _
               sdbgAtributos.Columns("TIPO_VALOR").Value = TiposDeAtributos.TipoTextoMedio Then
                frmATRIBDescr.g_bEdicion = True
                frmATRIBDescr.g_sOrigen = "ESTRMAT_ATRIB"
                frmATRIBDescr.caption = sdbgAtributos.Columns("NOMBRE").Value
                frmATRIBDescr.txtDescr.Text = NullToStr(sdbgAtributos.Columns("VALOR").Value)
                frmATRIBDescr.Show vbModal
            End If
        Case Else
            Screen.MousePointer = vbHourglass
            
            If Not g_ofrmATRIB Is Nothing Then
                Unload g_ofrmATRIB
                Set g_ofrmATRIB = Nothing
            End If
 
            '3328
            If g_sOrigen <> "TIPPED" Then
                g_sGmn1 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
                g_sGmn2 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
                g_sGmn3 = frmESTRMAT.ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                g_sGmn4 = frmESTRMAT.ssMateriales.ActiveRow.Cells("COD").Value
            End If
            
            Set g_ofrmATRIB = New frmAtrib
                
            '3328
            If g_sOrigen <> "TIPPED" Then
                g_ofrmATRIB.g_sGmn1 = g_sGmn1
                g_ofrmATRIB.g_sGmn2 = g_sGmn2
                g_ofrmATRIB.g_sGmn3 = g_sGmn3
                g_ofrmATRIB.g_sGmn4 = g_sGmn4
            End If
            
            If g_sOrigen = "ART4" Or g_sOrigen = "FRMARTCENTRALUNIFICAR" Or g_sOrigen = "FRMARTCENTRALUNIFCONFIRM" Then
             g_ofrmATRIB.g_sArtCod = g_vCodArt
            End If
            
            '3328
            If g_sOrigen = "TIPPED" Then
             g_ofrmATRIB.g_sPedId = g_vIdPed
            End If
            g_ofrmATRIB.txtCod = sdbgAtributos.Columns("COD").Value
            g_ofrmATRIB.g_sOrigen = "POSI"
            g_ofrmATRIB.sstabGeneral.Tab = 0
            g_ofrmATRIB.Icon = frmPROCEBuscar.Icon
            g_ofrmATRIB.Backcolor = &H808000
            g_ofrmATRIB.sstabGeneral.Backcolor = &H808000
            g_ofrmATRIB.g_idAtribCod = sdbgAtributos.Columns("ID_ATRIB").Value
            
            g_ofrmATRIB.sstabGeneral.Tab = 1
            g_ofrmATRIB.sdbgAtributosGrupo.SelectTypeRow = ssSelectionTypeSingleSelect
            g_ofrmATRIB.cmdSeleccionar.Visible = False
            
            Screen.MousePointer = vbHourglass
            'Unload frmATRIB
            MDI.MostrarFormulario g_ofrmATRIB
            Screen.MousePointer = vbNormal
    End Select
End Sub


''' <summary>
''' Evento que se produce al haber cambios en el grid
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde Evento que se produce al haber cambios en el grid; Tiempo m�ximo</remarks>
Private Sub sdbgatributos_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    Dim bk As Variant
    m_bModificacion = True
    
    If Not m_aBookmarks Is Nothing Then
        '3328 a�adido OR sdbgAtributos.Columns(sdbgAtributos.col).Name = "OBLIGATORIO"
        If sdbgAtributos.Columns(sdbgAtributos.col).Name = "INTERNO" Or sdbgAtributos.Columns(sdbgAtributos.col).Name = "PROCESO" Or sdbgAtributos.Columns(sdbgAtributos.col).Name = "PEDIDO" Or sdbgAtributos.Columns(sdbgAtributos.col).Name = "OBLIGATORIO" Or sdbgAtributos.Columns(sdbgAtributos.col).Name = "RECEP" Then
            If m_aBookmarks.Count > 0 Then
                Screen.MousePointer = vbHourglass
                If g_sOrigen = "GMN4" Then
                    teserror = g_oAtributos.ActualizarCheckAtributo(sdbgAtributos.Columns(sdbgAtributos.col).Value, m_aBookmarks, sdbgAtributos.Columns(sdbgAtributos.col).Name, g_sGmn1, g_sGmn2, g_sGmn3, g_sGmn4)
                ElseIf g_sOrigen <> "TIPPED" Then
                    teserror = g_oAtributos.ActualizarCheckAtributo(sdbgAtributos.Columns(sdbgAtributos.col).Value, m_aBookmarks, sdbgAtributos.Columns(sdbgAtributos.col).Name, , , , , g_vCodArt)
                ElseIf g_sOrigen = "TIPPED" Then
                    '3328
                    teserror = g_oAtributos.ActualizarCheckAtributoTipoPedido(sdbgAtributos.Columns(sdbgAtributos.col).Value, m_aBookmarks, sdbgAtributos.Columns(sdbgAtributos.col).Name, g_vIdPed)
                End If
                If teserror.Arg2 Then
                    Screen.MousePointer = vbNormal
                    oMensajes.ImposibleModificarMultiplesAtributos teserror.Arg1
        
                    'Quito la selecci�n multiple del grid.
                    If m_aBookmarks.Count > 0 Then
                        For Each bk In m_aBookmarks
                            sdbgAtributos.SelBookmarks.Add bk
                        Next
                    End If
                End If
                cmdRestaurar_Click
                If m_aBookmarks.Count > 0 Then
                    For Each bk In m_aBookmarks
                        sdbgAtributos.SelBookmarks.Remove bk
                    Next
                End If
                Set m_aBookmarks = Nothing
                
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
                    
        If m_aBookmarks.Count > 0 Then Exit Sub
    Else
        '3328
        If g_sOrigen <> "TIPPED" Then
            'Si se marca un atributo para pedido, se marca tambi�n automaticamente para proceso.
            If sdbgAtributos.Columns(sdbgAtributos.col).Name = "PEDIDO" Then
                If sdbgAtributos.Columns(sdbgAtributos.col).Value = "1" Then
                    sdbgAtributos.Columns("PROCESO").Value = "1"
                End If
            End If
            'Si est� marcado para pedido (y por defecto tambi�n para proceso) no dejar� desmarcar para proceso.
            If sdbgAtributos.Columns(sdbgAtributos.col).Name = "PROCESO" Then
                 If sdbgAtributos.Columns(sdbgAtributos.col).Value = "0" And sdbgAtributos.Columns("PEDIDO").Value = "1" Then
                     sdbgAtributos.Columns("PROCESO").Value = "1"
                 End If
            End If
        End If
    End If
        
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbgAtributos_Click()
Dim i As Integer
If sdbgAtributos.col = -1 Then Exit Sub
If sdbgAtributos.Columns("TIPO_INTRO").Value = 0 Then 'Libre
    If sdbgAtributos.Columns("TIPO_VALOR").Value = 1 Then
        'Este control se hace porque la primeravez que se pincha en esta columna, sin haber estado ubicado en otra, no carga bien el bot�n
        'as� que lo que hacemo es colocarla en la primera columna y despu�s volver a ubicarla.
        i = sdbgAtributos.col
        sdbgAtributos.col = 0
        sdbgAtributos.col = i
        DoEvents
        sdbgAtributos.Columns("VALOR").Style = ssStyleEditButton
        DoEvents
    End If
End If
End Sub

Private Sub sdbgAtributos_InitColumnProps()
    sdbgAtributos.Columns("COD").StyleSet = "BLUE"
    sdbgAtributos.Columns("NOMBRE").StyleSet = "BLUE"
End Sub


''' <summary>
''' Evento que se produce al cambiar de columna o de fila de la grid
''' </summary>
''' <returns></returns>
''' <param name="LastRow">Linea</param>
''' <param name="LastCol">Columna</param>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>

Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim i As Integer
Dim bk As Variant
    
    If sdbgAtributos.Columns("HER").Value = True Then
        cmdDesAsignar.Enabled = False
    Else
        If sdbgAtributos.Rows > 0 Then
            If g_bModoEdicion Then cmdDesAsignar.Enabled = True
        Else
            cmdDesAsignar.Enabled = False
        End If
    End If

    If sdbgAtributos.col = -1 Then
        Set m_aBookmarks = New Collection
        i = 1
        For Each bk In sdbgAtributos.SelBookmarks
            m_aBookmarks.Add sdbgAtributos.Columns("ID_ATRIB").CellValue(bk)
        Next
    Else
        '3328 a�ado OR sdbgAtributos.Columns(sdbgAtributos.col).Name = "OBLIGATORIO"
        If sdbgAtributos.Columns(sdbgAtributos.col).Name = "INTERNO" Or sdbgAtributos.Columns(sdbgAtributos.col).Name = "PROCESO" Or sdbgAtributos.Columns(sdbgAtributos.col).Name = "PEDIDO" Or sdbgAtributos.Columns(sdbgAtributos.col).Name = "OBLIGATORIO" Then
            If sdbgAtributos.SelBookmarks.Count = 0 And Not m_aBookmarks Is Nothing Then
                For Each bk In m_aBookmarks
                    sdbgAtributos.SelBookmarks.Add bk
                Next
            End If
        Else
            Set m_aBookmarks = Nothing
        End If
        
        If NullToStr(LastRow) <> NullToStr(sdbgAtributos.Bookmark) Or sdbgAtributos.Columns(sdbgAtributos.col).Name = "VALOR" Then
            If sdbgAtributos.Columns("TIPO_VALOR").Value = 8 Then
                sdbgAtributos.Columns("VALOR").Locked = True
                sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
            Else
                sdbgAtributos.Columns("VALOR").Locked = False
                If sdbgAtributos.Columns("TIPO_INTRO").Value = 0 Then 'Libre
                    If sdbgAtributos.Columns("TIPO_VALOR").Value = TipoBoolean Then
                        sdbddValor.RemoveAll
                        sdbddValor.AddItem ""
                        sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                        sdbddValor.Enabled = True
                    Else
                        sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
                        sdbddValor.Enabled = False
                    
                        If sdbgAtributos.Columns("TIPO_VALOR").Value = 1 Then
                            sdbgAtributos.Columns("VALOR").Style = ssStyleEditButton
                        End If
                    End If
                Else 'Lista
                    sdbddValor.RemoveAll
                    sdbddValor.AddItem ""
                    sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                    sdbddValor.Enabled = True
                    sdbddValor.DroppedDown = True
                    sdbddValor_DropDown
                    sdbddValor.DroppedDown = True
                End If
            End If
        End If
    End If
    
End Sub

Private Sub sdbgAtributos_RowLoaded(ByVal Bookmark As Variant)

    If sdbgAtributos.Columns("HER").Value = True Then
        sdbgAtributos.Columns("COD").CellStyleSet "YELLOW"
        sdbgAtributos.Columns("NOMBRE").CellStyleSet "YELLOW"
        cmdDesAsignar.Enabled = False
    Else
        If sdbgAtributos.Rows > 0 Then
            If g_bModoEdicion Then cmdDesAsignar.Enabled = True
        Else
            cmdDesAsignar.Enabled = False
            cmdAsignar.Enabled = False
        End If
    End If
    If sdbgAtributos.Columns("TIPO_VALOR").Value = 8 Then
        sdbgAtributos.Columns("VALOR").CellStyleSet "Gris"
    End If
End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

If node Is Nothing Then Exit Function

Select Case Left(node.Tag, 4)

Case "GMN1"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "GMN2"
    
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "GMN3"
    
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "GMN4"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
End Select

End Function
Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRMAT_ATRIB, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        m_sTitulo = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("NOMBRE").caption = Ador(0).Value
        Ador.MoveNext
        m_sNomMat = Ador(0).Value
        Ador.MoveNext
        m_sNomArt = Ador(0).Value
        Ador.MoveNext
        cmdAsignar.caption = Ador(0).Value
        Ador.MoveNext
        cmdDesAsignar.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        cmdActualizarAtributosUon.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiMenAtributo = Ador(0).Value
        Ador.MoveNext
        m_sDescripcion = Ador(0).Value
        Ador.MoveNext
        m_asAtrib(0) = Ador(0).Value ' Texto
        Ador.MoveNext
        m_asAtrib(1) = Ador(0).Value ' Num�rico
        Ador.MoveNext
        m_asAtrib(2) = Ador(0).Value ' Fecha
        Ador.MoveNext
        m_asAtrib(3) = Ador(0).Value ' Si/No
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value ' Si
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value ' No
        Ador.MoveNext
        sdbgAtributos.Columns("VALOR").caption = Ador(0).Value ' Valor
        Ador.MoveNext
        sdbgAtributos.Columns("PROCESO").caption = Ador(0).Value ' Valor
        m_sProceso = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("PEDIDO").caption = Ador(0).Value ' Valor
        m_sPedido = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("INTERNO").caption = Ador(0).Value ' Valor
        m_sInterno = Ador(0).Value
        '3328 Nueva columna OBLIGATORIO
        Ador.MoveNext
        sdbgAtributos.Columns("OBLIGATORIO").caption = Ador(0).Value ' Obligatorio
        m_sObligatorio = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("NOMBRE").caption = Ador(0).Value ' aTRIBUTOS
        SSTabAtributos.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTabAtributos.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sdbgAtributosUon.Columns("NOMBRE").caption = Ador(0).Value
        Ador.MoveNext
        m_sTipoDePedido = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("RECEP").caption = Ador(0).Value ' Valor
        m_sRecepcion = Ador(0).Value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub


'--<summary>
'--Desasigna los atributos seleccionados
'--</summary>
'--<remarks>Llamada desde Bot�n Desasignar; Tiempo m�ximo</remarks>
'--<revision>DPD 28/03/2011</revision>

Public Sub DesasignarAtributosSeleccionados()
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit
Dim oGrupoMatNivel4 As CGrupoMatNivel4
Dim sArtCod As Variant
'3328
Dim oTipoPedido As CTipoPedido

    If sdbgAtributos.SelBookmarks.Count > 1 Then
        'DPD (18101) Desasignar multiples atributos
        irespuesta = oMensajes.PreguntaDesasignarMultiples()
    Else
        'DPD Desasignar un solo atributo
        irespuesta = oMensajes.PreguntaDesasignar(m_sIdiMenAtributo & " " & sdbgAtributos.Columns(0).Value & " - " & sdbgAtributos.Columns(1).Value)
    End If
    If irespuesta = vbYes Then
        '3328 A�ado el ELSE para tratar en caso de que sea TIPO DE PEDIDO
        Dim i As Long
        Dim myBookmarks As Collection
        Dim atributosADeasignar As Collection: Set atributosADeasignar = New Collection
        
        If g_sOrigen <> "TIPPED" Then
            Set oGrupoMatNivel4 = oFSGSRaiz.Generar_CGrupoMatNivel4
            oGrupoMatNivel4.Cod = frmESTRMAT.ssMateriales.ActiveRow.Cells("COD").Value
            oGrupoMatNivel4.GMN3Cod = frmESTRMAT.ssMateriales.ActiveRow.GetParent.Cells("COD").Value
            oGrupoMatNivel4.GMN2Cod = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
            oGrupoMatNivel4.GMN1Cod = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
            
            If g_sOrigen = "GMN4" Then
                sArtCod = Null
            Else
                sArtCod = g_vCodArt 'frmESTRMAT.sdbgArticulos.Columns("COD").Value
            End If
            
            'Dim i As Long
            'Dim myBookmarks As Collection
            Set myBookmarks = New Collection
            Dim atributosConArticulos As Collection: Set atributosConArticulos = New Collection
            Dim iatributosConArticulos As Collection: Set iatributosConArticulos = New Collection
            'Dim atributosADeasignar As Collection: Set atributosADeasignar = New Collection
            Dim codAtributosConArticulos As Collection: Set codAtributosConArticulos = New Collection
            For i = sdbgAtributos.SelBookmarks.Count - 1 To 0 Step -1
                sdbgAtributos.Bookmark = sdbgAtributos.SelBookmarks(i)
                If oGrupoMatNivel4.ExistenValoresDeAtributos(StrToDbl0(sdbgAtributos.Columns("ID_ATRIB").Value), sArtCod) Then
                    atributosConArticulos.Add sdbgAtributos.Columns("ID_ATRIB").Value
                    codAtributosConArticulos.Add sdbgAtributos.Columns(0).Value & " - " & sdbgAtributos.Columns(1).Value
                    iatributosConArticulos.Add i
                Else
                    atributosADeasignar.Add sdbgAtributos.Columns("ID_ATRIB").Value
                End If
            Next i
            
            If atributosConArticulos.Count > 0 Then
                'Preguntamos que hacer con los atributos para los que ya haya art�culos
                If IsNull(sArtCod) Then
                    irespuesta = oMensajes.PreguntaDesasignarMaterialProveMultiples(codAtributosConArticulos)
                Else
                    irespuesta = oMensajes.PreguntaDesasignarMaterialProveMultiples(codAtributosConArticulos)
                End If
                If irespuesta = vbYes Then
                    'Les a�adimos a la lista de procesado
                    For i = 1 To atributosConArticulos.Count
                        atributosADeasignar.Add atributosConArticulos(i)
                    Next
                Else
                    'Les quitamos de la selecci�n
                    For i = 1 To atributosConArticulos.Count
                        sdbgAtributos.SelBookmarks.Remove (iatributosConArticulos(i))
                    Next
                End If
            End If
            teserror = oGrupoMatNivel4.DesAsignarAtributos(atributosADeasignar, sArtCod, oUsuarioSummit.Cod)
        '3328
        Else
            Set oTipoPedido = oFSGSRaiz.Generar_CTipoPedido
            oTipoPedido.Id = g_vIdPed 'frmMantPedidos.sdbgTiposPedido.ActiveRow.Cells("COD").Value

            
            For i = sdbgAtributos.SelBookmarks.Count - 1 To 0 Step -1
                sdbgAtributos.Bookmark = sdbgAtributos.SelBookmarks(i)
                atributosADeasignar.Add sdbgAtributos.Columns("ID_ATRIB").Value
            Next i

            teserror = oTipoPedido.DesAsignarAtributos(atributosADeasignar, g_vIdPed)
        End If
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
        'Quitamos los elementos del grid
        sdbgAtributos.DeleteSelected
    End If

End Sub

Private Sub sdbgAtributos_Validate(Cancel As Boolean)

If sdbgAtributos.Row > 9 Then Exit Sub

End Sub


''' <summary>
''' Carga el grid de los atributos de especificacion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:sdbcGrupoEsp_CloseUp;cmdModoEdicionAtrEsp_Click;CargarGridPlantillas;lstvwplantillas_ItemClick;CargarEspecificaciones</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub CargarGridEspecificaciones()
Dim g_oAtributo As CAtributo
Dim sInterno As String
Dim sProceso As String
Dim sPedido As String
Dim sLinea As String
Dim sValor As String
Dim sLineaTipoNum As String
'3328
Dim sObligatorio As String
Dim sRecepcion As String

    For Each g_oAtributo In g_oAtributos
        If g_oAtributo.interno Then
            sInterno = "1"
        Else
            sInterno = "0"
        End If
        If g_oAtributo.proceso Then
            sProceso = "1"
        Else
            sProceso = "0"
        End If

        If g_oAtributo.pedido Then
            sPedido = "1"
        Else
            sPedido = "0"
        End If
        
        '3328
        If g_oAtributo.Obligatorio Then
            sObligatorio = "1"
        Else
            sObligatorio = "0"
        End If
        If g_oAtributo.Recepcion Then
            sRecepcion = "1"
        Else
            sRecepcion = "0"
        End If
        sLinea = g_oAtributo.Cod & Chr(m_lSeparador) & g_oAtributo.Den & Chr(m_lSeparador) & g_oAtributo.Heredado & Chr(m_lSeparador) & g_oAtributo.Id & Chr(m_lSeparador) & IIf(IsNull(g_oAtributo.Descripcion), "", "...") & Chr(m_lSeparador) & g_oAtributo.Descripcion & Chr(m_lSeparador)
        sLineaTipoNum = Chr(m_lSeparador) & Chr(m_lSeparador) & g_oAtributo.TipoIntroduccion
        Select Case g_oAtributo.Tipo
            Case 1  'Texto
                sValor = NullToStr(g_oAtributo.valorText)
            Case 2  'Num�rico
                sValor = NullToStr(g_oAtributo.valorNum)
                sLineaTipoNum = g_oAtributo.Minimo & Chr(m_lSeparador) & g_oAtributo.Maximo & Chr(m_lSeparador) & g_oAtributo.TipoIntroduccion
            Case 3  'Fecha
                sValor = NullToStr(g_oAtributo.valorFec)
            Case 4  'Boolean
                If IsNull(g_oAtributo.valorBool) Then
                    sValor = ""
                Else
                    If g_oAtributo.valorBool Then
                        sValor = m_sIdiTrue
                    Else
                        sValor = m_sIdiFalse
                    End If
                End If
            Case 8  'Archivo
                sValor = ""
        End Select
        sLinea = sLinea & sValor & Chr(m_lSeparador) & sProceso & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & sRecepcion & Chr(m_lSeparador) & sObligatorio & Chr(m_lSeparador) & sInterno & Chr(m_lSeparador) & g_oAtributo.Tipo & Chr(m_lSeparador) & sLineaTipoNum
        sdbgAtributos.AddItem sLinea
    Next
End Sub

Private Sub montarColumnasGridAtributosUon()
    Dim oUON As IUon
    Dim icolumns As Integer
    Dim oColumn As SSDataWidgets_B.Column
    icolumns = sdbgAtributosUon.Columns.Count
    m_oUonsAtributo.clear
    If Articulo.uons.Count > 0 Then
        For Each oUON In m_oArticulo.uons
            If m_oUonsRestringidas.CONTIENE(oUON) Or m_oUonsRestringidas.Count = 0 Then
                m_oUonsAtributo.Add oUON, oUON.key
                sdbgAtributosUon.Columns.Add icolumns
                Set oColumn = sdbgAtributosUon.Columns(icolumns)
                oColumn.Name = oUON.key
                oColumn.caption = oUON.titulo
                oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                oColumn.Alignment = ssCaptionAlignmentLeft
                oColumn.Style = ssStyleEdit
                oColumn.FieldLen = 100
                icolumns = icolumns + 1
                Set oColumn = Nothing
            End If
        Next
    End If
    
End Sub
Private Sub cargarGridAtributosUon()
    If Not Articulo Is Nothing Then
        Dim oAtributo As CAtributo
        LockWindowUpdate Me.hWnd
        Set m_oAtributosUon = Articulo.getAtributosUon
        sdbgAtributosUon.RemoveAll
        
        limpiarGridAtributosUon
        montarColumnasGridAtributosUon
        
        For Each oAtributo In m_oAtributosUon
            Dim strLinea As String
            strLinea = oAtributo.Id & Chr(m_lSeparador)
            strLinea = strLinea & oAtributo.CodDen & Chr(m_lSeparador)
            strLinea = strLinea & oAtributo.Id & Chr(m_lSeparador)
            sdbgAtributosUon.AddItem strLinea
        Next
        
        cargarDatosAtributosUon
        LockWindowUpdate 0&
    End If
End Sub
'''<summary>Cargar el grid de atributos por uon (filas = atributos posibles para las uons del art�culo)
'''(columnas= uons del art�culo )</summary>
Private Sub cargarDatosAtributosUon()
    Dim oUON As IUon
    Dim vbm As Variant
    Set m_oAtributosUonValorados = Articulo.getAtributosUonValorados
    
    LockWindowUpdate Me.hWnd
    sdbgAtributosUon.MoveFirst
    Dim i As Integer
    'vamos recorriendo las filas del grid
    For i = 0 To sdbgAtributosUon.Rows - 1
        'recorremos las uons del art�culo (que adem�s van a ser nuestras columnas del grid)
        For Each oUON In m_oUonsAtributo
            
            'si existe valor para el atributo correspondiente a la fila y a la uon correspondiente a la columna
            If m_oAtributosUonValorados.existe(sdbgAtributosUon.Columns("ID").Value & " " & oUON.key) Then
                Dim oAtributoValorado As CAtributo
                'cargamos el atributo de la coordenada correspondiente
                Set oAtributoValorado = m_oAtributosUonValorados.Item(sdbgAtributosUon.Columns("ID").Value & " " & oUON.key)
                'rellenamos la celda con su valor
                If oAtributoValorado.Tipo = TipoBoolean Then
                    If oAtributoValorado.getValor = 1 Then
                        sdbgAtributosUon.Columns(oUON.key).Text = m_sIdiTrue
                    Else
                        sdbgAtributosUon.Columns(oUON.key).Text = m_sIdiFalse
                    End If
                Else
                    sdbgAtributosUon.Columns(oUON.key).Text = NullToStr(oAtributoValorado.getValor)
                End If
                Set oAtributoValorado = Nothing
            Else
                'si no existe le damos el valor por defecto (guardado en atributosuon)
                Dim oAtributo As CAtributo
                Set oAtributo = m_oAtributosUon.Item(sdbgAtributosUon.Columns("ID").Value)
                If oAtributo.Tipo = TipoBoolean Then
                    If oAtributo.getValor = 1 Then
                        sdbgAtributosUon.Columns(oUON.key).Text = m_sIdiTrue
                    Else
                        sdbgAtributosUon.Columns(oUON.key).Text = m_sIdiFalse
                    End If
                Else
                    sdbgAtributosUon.Columns(oUON.key).Text = NullToStr(oAtributo.getValor)
                End If
                
                Set oAtributo = Nothing
            End If
        Next
        sdbgAtributosUon.MoveNext
    Next i
    sdbgAtributosUon.MoveFirst
    LockWindowUpdate 0&
    Set oUON = Nothing
End Sub



Private Sub sdbgAtributosUon_BeforeRowColChange(Cancel As Integer)
    If sdbgAtributosUon.DataChanged Then
        If Not actualizarValorGridAtributosUon Then
            Cancel = True
        End If
    End If
End Sub

Private Sub sdbgAtributosUon_BtnClick()
    If sdbgAtributosUon.Columns(sdbgAtributosUon.col).Locked = False Then
    Select Case Me.AtributoUonSeleccionado.Tipo
        Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoNumerico
            frmATRIBDescr.g_bEdicion = True
            frmATRIBDescr.g_sOrigen = "ESTRMAT_ATRIB_UON"
            frmATRIBDescr.caption = sdbgAtributosUon.Columns("NOMBRE").Value
            frmATRIBDescr.setTexto (NullToStr(sdbgAtributosUon.Columns(UonSeleccionada.key).Value))
            frmATRIBDescr.Show vbModal
    End Select
    End If
End Sub

Private Sub sdbgAtributosUon_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    If sdbgAtributosUon.col <> -1 Then
        If Not IsEmpty(Me.UonSeleccionada) Then
            If Not Me.AtributoUonSeleccionado.incluyeUon(Me.UonSeleccionada) Then
                sdbgAtributosUon.Columns(sdbgAtributosUon.col).Locked = True
                sdbgAtributosUon.Columns(sdbgAtributosUon.col).Style = ssStyleEditButton
                Exit Sub
            Else
                sdbgAtributosUon.Columns(sdbgAtributosUon.col).Locked = False
                sdbgAtributosUon.Columns(sdbgAtributosUon.col).Style = ssStyleEditButton
            End If
        End If
    End If
    If sdbgAtributosUon.col <> -1 Then
        'Si la columna es de tipo uon, en funci�n del tipo de atributo mostramos la celda de manera diferente
        If Me.Articulo.uons.existe(sdbgAtributosUon.Columns(sdbgAtributosUon.col).Name) Then
            Dim oUON As IUon
            Set oUON = Me.Articulo.uons.Item(sdbgAtributosUon.Columns(sdbgAtributosUon.col).Name)
            Dim Atrib As CAtributo
            If Not sdbgAtributosUon.Columns(sdbgAtributosUon.col).Locked And m_oAtributosUon.existe(sdbgAtributosUon.Columns("id").Value) Then
                Set Atrib = m_oAtributosUon.Item(sdbgAtributosUon.Columns("ID").Value)
                Select Case Atrib.Tipo
                    Case TiposDeAtributos.TipoBoolean
                        SSDBValorUon.RemoveAll
                        SSDBValorUon.AddItem ""
                        sdbgAtributosUon.Columns(oUON.key).Style = ssStyleComboBox
                        sdbgAtributosUon.Columns(oUON.key).DropDownHwnd = SSDBValorUon.hWnd
                        SSDBValorUon.Enabled = True
                        
                    Case TiposDeAtributos.TipoNumerico
                        sdbgAtributosUon.Columns(oUON.key).DropDownHwnd = 0
                        sdbgAtributosUon.Columns(oUON.key).Style = ssStyleEditButton
                        SSDBValorUon.Enabled = False
                    Case TiposDeAtributos.TipoString
                        If Me.AtributoUonSeleccionado.TipoIntroduccion = IntroLibre Then
                            sdbgAtributosUon.Columns(oUON.key).DropDownHwnd = 0
                            SSDBValorUon.Enabled = True
                            sdbgAtributosUon.Columns(oUON.key).Style = ssStyleEditButton
                        Else
                            SSDBValorUon.RemoveAll
                            SSDBValorUon.AddItem ""
                            sdbgAtributosUon.Columns(oUON.key).Style = ssStyleComboBox
                            sdbgAtributosUon.Columns(oUON.key).DropDownHwnd = SSDBValorUon.hWnd
                            SSDBValorUon.Enabled = True
                            SSDBValorUon.DroppedDown = True
                            SSDBValorUon_DropDown
                            SSDBValorUon.DroppedDown = True
                        End If
                        
                    Case TiposDeAtributos.TipoFecha
                        sdbgAtributosUon.Columns(oUON.key).DropDownHwnd = 0
                        sdbgAtributosUon.Columns(oUON.key).Style = ssStyleEditButton
                        SSDBValorUon.Enabled = False
                End Select
            End If
            Set Atrib = Nothing
            Set oUON = Nothing
        End If
    End If
End Sub

Private Sub sdbgAtributosUon_RowLoaded(ByVal Bookmark As Variant)
    Dim oUON As IUon
    'celdas editables o no
    For Each oUON In m_oUonsAtributo
        Dim Atrib As CAtributo
        Set Atrib = m_oAtributosUon.Item(sdbgAtributosUon.Columns("ID").Value)
        'si la uon del atributo no incluye a la uon de la unidad org. no podemos editar
        If Not Atrib.incluyeUon(oUON) Then
            sdbgAtributosUon.Columns(oUON.key).CellStyleSet "Gris", sdbgAtributosUon.Row

        Else
            sdbgAtributosUon.Columns(oUON.key).CellStyleSet "Normal", sdbgAtributosUon.Row

        End If

    Next
    
End Sub

Private Sub SSDBValorUon_DropDown()
    Dim iIndice As Integer
    Dim oLista As CValorPond
    If Not sdbgAtributosUon.Columns(sdbgAtributosUon.col).Locked Then
        iIndice = 1
    
        SSDBValorUon.RemoveAll
    
        Select Case Me.AtributoUonSeleccionado.Tipo
        
        Case 1, 2, 3
                Me.AtributoUonSeleccionado.CargarListaDeValoresDef
                For Each oLista In Me.AtributoUonSeleccionado.ListaPonderacion
                    SSDBValorUon.AddItem iIndice & Chr(m_lSeparador) & oLista.ValorLista
                    iIndice = iIndice + 1
                Next
        Case 4
            SSDBValorUon.RemoveAll
        
            SSDBValorUon.AddItem 1 & Chr(m_lSeparador) & m_sIdiTrue
            SSDBValorUon.AddItem 0 & Chr(m_lSeparador) & m_sIdiFalse
    
        End Select
    End If
End Sub

Private Sub SSDBValorUon_InitColumnProps()
    SSDBValorUon.DataFieldList = "Column 0"
    SSDBValorUon.DataFieldToDisplay = "Column 1"
End Sub

Public Property Get UonSeleccionada() As Variant
    If sdbgAtributosUon.col > 2 Then
        Set UonSeleccionada = Me.Articulo.uons.Item(sdbgAtributosUon.Columns(sdbgAtributosUon.col).Name)
    End If
End Property

Public Property Get AtributoUonSeleccionado() As CAtributo
    Set AtributoUonSeleccionado = m_oAtributosUon.Item(sdbgAtributosUon.Columns("ID").Value)
End Property

Public Sub setCurrentCellValue(ByVal Value As Variant)
    sdbgAtributosUon.Columns(sdbgAtributosUon.col).Value = Value
End Sub

Public Sub setCurrentCellText(ByVal Value As Variant)
    sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text = Value
End Sub

Public Function getCurrentCellValue() As String
    getCurrentCellValue = sdbgAtributosUon.Columns(sdbgAtributosUon.col).Value
End Function

Public Function getCurrentCellText() As String
    getCurrentCellText = sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text
End Function

Public Sub ConfigurarSeguridad()
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBModificarAtribPorUon)) Is Nothing) Then
        Me.sdbgAtributosUon.Enabled = False
    Else
        Me.sdbgAtributosUon.Enabled = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATAtribRestrMantAtribUonUsu)) Is Nothing) Then
        m_bRestrMantAtribUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATAtribRestrMantAtribUonPer)) Is Nothing) Then
        m_bRestrMantAtribPerf = True
    End If
End Sub

Private Function actualizarValorGridAtributosUon() As Boolean
    actualizarValorGridAtributosUon = False
    If sdbgAtributosUon.col >= 1 Then
          Dim oAtributo As CAtributo
          Dim oUON As IUon
          
          Set oAtributo = Me.AtributoUonSeleccionado
          Set oUON = Me.UonSeleccionada
          
             If UCase(sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text) <> "" Then
                 
                 Select Case UCase(oAtributo.Tipo)
             
                     Case 2 'Numero
                                 If (Not IsNumeric(sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text)) Then
                                      oMensajes.AtributoValorNoValido ("TIPO2")
                                      
                                     Exit Function
                                 Else
                                     If NullToStr(oAtributo.Minimo) <> "" And NullToStr(oAtributo.Maximo) <> "" Then
                                         If StrToDbl0(oAtributo.Minimo) > StrToDbl0(sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text) Or StrToDbl0(oAtributo.Maximo) < StrToDbl0(sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text) Then
                                             oMensajes.ValorEntreMaximoYMinimo oAtributo.Minimo, oAtributo.Maximo
                                             Exit Function
                                         End If
                                     End If
                                 
                                 End If
             
                     Case 3 'Fecha
                         If (Not IsDate(sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text) And sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text <> "") Then
                             oMensajes.AtributoValorNoValido ("TIPO3")
                                 actualizarValorGridAtributosUon = False
                                 Exit Function
                         Else
                             If NullToStr(oAtributo.Minimo) <> "" And NullToStr(oAtributo.Maximo) <> "" Then
                                 If CDate(Me.AtributoUonSeleccionado.Minimo) > CDate(sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text) Or CDate(oAtributo.Maximo) < CDate(sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text) Then
                                     oMensajes.ValorEntreMaximoYMinimo oAtributo.Minimo, oAtributo.Maximo
                                     actualizarValorGridAtributosUon = False
                                     Exit Function
                                 End If
                             End If
                     End If
                     
                     Case TiposDeAtributos.TipoBoolean
                         If sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text = "" Then
                             
                             Exit Function
                         End If
                 End Select
             End If
             'Actualizaci�n del valor de atributo
             
        On Error GoTo err:
        If Me.AtributoUonSeleccionado.TipoIntroduccion = IntroLibre Then
            Me.AtributoUonSeleccionado.setvalor (sdbgAtributosUon.Columns(sdbgAtributosUon.col).Value)
        Else
            Me.AtributoUonSeleccionado.setvalor (sdbgAtributosUon.Columns(sdbgAtributosUon.col).Text)
        End If
        If sdbgAtributosUon.Columns(sdbgAtributosUon.col).Value <> "" Then
            Me.Articulo.addAtributoUon Me.AtributoUonSeleccionado, Me.UonSeleccionada, oUsuarioSummit.Cod
        Else
            Dim oUons As CUnidadesOrganizativas
            Set oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
            oUons.Add (Me.UonSeleccionada)
            Me.Articulo.eliminarAtributoArticuloEnUons oUons, Me.AtributoUonSeleccionado.Id
            Set oUons = Nothing
        End If
        Set oUON = Nothing
        Set oAtributo = Nothing
        
    End If
    actualizarValorGridAtributosUon = True
    Exit Function
err:
    actualizarValorGridAtributosUon = False
End Function

Private Sub limpiarGridAtributosUon()
    Dim i As Integer
    For i = Me.sdbgAtributosUon.Cols - 1 To 3 Step -1
        sdbgAtributosUon.Columns.Remove (i)
    Next
    'recargarmos las uons del art�culo por si hubiera habido actualizaci�n por parte de otros usuarios
    Articulo.getuons
End Sub

