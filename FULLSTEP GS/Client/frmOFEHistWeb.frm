VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmOFEHistWeb 
   Caption         =   "DRegistro de ofertas recibidas a trav�s del web"
   ClientHeight    =   4650
   ClientLeft      =   -240
   ClientTop       =   1350
   ClientWidth     =   9975
   Icon            =   "frmOFEHistWeb.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4650
   ScaleWidth      =   9975
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Listado"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   60
      TabIndex        =   10
      Top             =   4260
      Width           =   1095
   End
   Begin VB.PictureBox picSel 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3735
      Left            =   60
      ScaleHeight     =   3735
      ScaleWidth      =   10035
      TabIndex        =   8
      Top             =   480
      Width           =   10035
      Begin SSDataWidgets_B.SSDBDropDown sdbddPrecios 
         Height          =   1995
         Left            =   120
         TabIndex        =   9
         Top             =   1080
         Width           =   9735
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         UseGroups       =   -1  'True
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterPos     =   1
         Groups.Count    =   3
         Groups(0).Width =   6059
         Groups(0).Caption=   "Items"
         Groups(0).Columns.Count=   2
         Groups(0).Columns(0).Width=   1535
         Groups(0).Columns(0).Caption=   "Art�culo"
         Groups(0).Columns(0).Name=   "ART"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(1).Width=   4524
         Groups(0).Columns(1).Caption=   "Descripci�n"
         Groups(0).Columns(1).Name=   "DESCR"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(1).Width =   9446
         Groups(1).Caption=   "Detalle de items"
         Groups(1).Columns.Count=   7
         Groups(1).Columns(0).Width=   953
         Groups(1).Columns(0).Caption=   "Dest"
         Groups(1).Columns(0).Name=   "DEST"
         Groups(1).Columns(0).DataField=   "Column 2"
         Groups(1).Columns(0).DataType=   8
         Groups(1).Columns(0).FieldLen=   256
         Groups(1).Columns(1).Width=   847
         Groups(1).Columns(1).Caption=   "Uni"
         Groups(1).Columns(1).Name=   "UNI"
         Groups(1).Columns(1).DataField=   "Column 3"
         Groups(1).Columns(1).DataType=   8
         Groups(1).Columns(1).FieldLen=   256
         Groups(1).Columns(2).Width=   1720
         Groups(1).Columns(2).Caption=   "Inicio "
         Groups(1).Columns(2).Name=   "INI"
         Groups(1).Columns(2).DataField=   "Column 4"
         Groups(1).Columns(2).DataType=   8
         Groups(1).Columns(2).FieldLen=   256
         Groups(1).Columns(3).Width=   1535
         Groups(1).Columns(3).Caption=   "Fin"
         Groups(1).Columns(3).Name=   "FIN"
         Groups(1).Columns(3).DataField=   "Column 5"
         Groups(1).Columns(3).DataType=   8
         Groups(1).Columns(3).FieldLen=   256
         Groups(1).Columns(4).Width=   1535
         Groups(1).Columns(4).Caption=   "Cantidad"
         Groups(1).Columns(4).Name=   "CANT"
         Groups(1).Columns(4).DataField=   "Column 6"
         Groups(1).Columns(4).DataType=   8
         Groups(1).Columns(4).NumberFormat=   "standard"
         Groups(1).Columns(4).FieldLen=   256
         Groups(1).Columns(5).Width=   900
         Groups(1).Columns(5).Caption=   "Pag"
         Groups(1).Columns(5).Name=   "PAG"
         Groups(1).Columns(5).DataField=   "Column 7"
         Groups(1).Columns(5).DataType=   8
         Groups(1).Columns(5).FieldLen=   256
         Groups(1).Columns(6).Width=   1958
         Groups(1).Columns(6).Caption=   "Presu.unitario"
         Groups(1).Columns(6).Name=   "PRESUNI"
         Groups(1).Columns(6).DataField=   "Column 8"
         Groups(1).Columns(6).DataType=   8
         Groups(1).Columns(6).NumberFormat=   "standard"
         Groups(1).Columns(6).FieldLen=   256
         Groups(2).Width =   3784
         Groups(2).Caption=   "Oferta"
         Groups(2).Columns.Count=   2
         Groups(2).Columns(0).Width=   1905
         Groups(2).Columns(0).Caption=   "Precio"
         Groups(2).Columns(0).Name=   "PREC"
         Groups(2).Columns(0).DataField=   "Column 9"
         Groups(2).Columns(0).DataType=   8
         Groups(2).Columns(0).NumberFormat=   "standard"
         Groups(2).Columns(0).FieldLen=   256
         Groups(2).Columns(0).HasBackColor=   -1  'True
         Groups(2).Columns(0).BackColor=   13170165
         Groups(2).Columns(1).Width=   1879
         Groups(2).Columns(1).Caption=   "Cant.m�xima"
         Groups(2).Columns(1).Name=   "CANTMAX"
         Groups(2).Columns(1).DataField=   "Column 10"
         Groups(2).Columns(1).DataType=   8
         Groups(2).Columns(1).NumberFormat=   "standard"
         Groups(2).Columns(1).FieldLen=   256
         Groups(2).Columns(1).HasBackColor=   -1  'True
         Groups(2).Columns(1).BackColor=   13170165
         _ExtentX        =   17171
         _ExtentY        =   3519
         _StockProps     =   77
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgOfertas 
         Height          =   3675
         Left            =   0
         TabIndex        =   12
         Top             =   0
         Width           =   9915
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   8
         stylesets.count =   1
         stylesets(0).Name=   "Asignado"
         stylesets(0).BackColor=   11007475
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmOFEHistWeb.frx":0CB2
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   8
         Columns(0).Width=   2884
         Columns(0).Caption=   "C�digo de proveedor"
         Columns(0).Name =   "PROVECOD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "PROVEDEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1217
         Columns(2).Caption=   "Oferta"
         Columns(2).Name =   "NUMOFE"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   2381
         Columns(3).Caption=   "Fecha recepci�n"
         Columns(3).Name =   "FECREC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   1879
         Columns(4).Caption=   "V�lida hasta"
         Columns(4).Name =   "FECVAL"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   953
         Columns(5).Caption=   "Mon"
         Columns(5).Name =   "MON"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   635
         Columns(6).Caption=   "Precios"
         Columns(6).Name =   "PRECIOS"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(6).ButtonsAlways=   -1  'True
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "INDICE"
         Columns(7).Name =   "INDICE"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   17489
         _ExtentY        =   6482
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picProceA�a 
      BorderStyle     =   0  'None
      Height          =   525
      Left            =   60
      ScaleHeight     =   525
      ScaleWidth      =   10365
      TabIndex        =   4
      Top             =   0
      Width           =   10365
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   9550
         Picture         =   "frmOFEHistWeb.frx":0CCE
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   105
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   2265
         TabIndex        =   1
         Top             =   105
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   600
         TabIndex        =   0
         Top             =   105
         Width           =   960
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1693
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
         Height          =   285
         Left            =   4320
         TabIndex        =   2
         Top             =   105
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
         Height          =   285
         Left            =   5385
         TabIndex        =   3
         Top             =   105
         Width           =   4100
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5054
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7232
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label2 
         Caption         =   "Proceso:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   3500
         TabIndex        =   7
         Top             =   150
         Width           =   750
      End
      Begin VB.Label lblA�o 
         Caption         =   "A�o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   0
         TabIndex        =   6
         Top             =   150
         Width           =   555
      End
      Begin VB.Label lblGMN1_4 
         Caption         =   "Comm:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1590
         TabIndex        =   5
         Top             =   150
         Width           =   600
      End
   End
End
Attribute VB_Name = "frmOFEHistWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables para func. combos
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean

'Anyo seleccionado
Private iAnyoSeleccionado As Integer

'Proceso seleccionado
Public oProcesoSeleccionado As CProceso
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGruposMN1 As CGruposMatNivel1
Private oOfertaSeleccionada As COferta

' Interface para obener las ofertas de un proveedor
Private oIOfertas As iOfertas
Private oOfertas As COfertas
Private oOfertaAA�adir As COferta
Private oLineaEnEdicion As CPrecioItem
Private oIBaseDatos As IBaseDatos

' variable para listado
Public ofrmLstPROCE As frmLstPROCE
' Coleccion de procesos a cargar
Private oProcesos As CProcesos

'Varibles de idioma
Private sIdioma(1 To 3) As String

'Variables de seguridad (Se aplican a la combo de procesos)
Private bRMat As Boolean
Private bRCompResp As Boolean
Public bREqp As Boolean
Public bRComp As Boolean



Private Sub ListarProceso()


    Set ofrmLstPROCE = New frmLstPROCE
    ofrmLstPROCE.sOrigen = "frmOFEHistWeb"
  
    
    If Not oProcesoSeleccionado Is Nothing Then
        ofrmLstPROCE.sdbcAnyo.Text = oProcesoSeleccionado.Anyo
        ofrmLstPROCE.sdbcGMN1Proce_4Cod.Text = oProcesoSeleccionado.GMN1Cod
        ofrmLstPROCE.sdbcGMN1Proce_4Cod_Validate False
        ofrmLstPROCE.txtCOD.Text = CStr(oProcesoSeleccionado.Cod)
        ofrmLstPROCE.txtDen.Text = oProcesoSeleccionado.Den
    End If
        
    ofrmLstPROCE.Show vbModal

End Sub

Private Sub cmdBuscar_Click()
    
    
        frmPROCEBuscar.bRDest = False
        frmPROCEBuscar.bRUsuAper = False
        frmPROCEBuscar.bRUsuDep = False
        frmPROCEBuscar.bRUsuUON = False
        frmPROCEBuscar.m_bProveAsigComp = False
        frmPROCEBuscar.m_bProveAsigEqp = False
    
        frmPROCEBuscar.bRCompResponsable = bRCompResp
        frmPROCEBuscar.bRAsig = bRComp
        frmPROCEBuscar.bREqpAsig = bREqp
        frmPROCEBuscar.bRMat = bRMat
        frmPROCEBuscar.sOrigen = "frmOFEHistWeb"
        frmPROCEBuscar.sdbcAnyo = sdbcAnyo
        frmPROCEBuscar.sdbcGMN1Proce_Cod = sdbcGMN1_4Cod
        frmPROCEBuscar.sdbcGMN1Proce_Cod_Validate False
        frmPROCEBuscar.Show 1
        
    
End Sub

Private Sub cmdImprimir_Click()
    ListarProceso
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_OFE_HISTWEB, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        sIdioma(1) = Ador(0).Value '1
        Ador.MoveNext
        sIdioma(2) = Ador(0).Value
        Label2.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sIdioma(3) = Ador(0).Value
        Ador.MoveNext
        cmdImprimir.caption = Ador(0).Value
        Ador.MoveNext
        frmOFEHistWeb.caption = Ador(0).Value '5
        Ador.MoveNext
        lblA�o.caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value '7 Cod
        sdbcProceCod.Columns(0).caption = Ador(0).Value
        sdbcProceDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value '8 Denominaci�n
        sdbcProceCod.Columns(1).caption = Ador(0).Value
        sdbcProceDen.Columns(0).caption = Ador(0).Value
        sdbddPrecios.Groups(0).Columns(1).caption = Ador(0).Value
        sdbgOfertas.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPrecios.Groups(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPrecios.Groups(0).Columns(0).caption = Ador(0).Value '10
        Ador.MoveNext
        sdbddPrecios.Groups(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPrecios.Groups(1).Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPrecios.Groups(1).Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPrecios.Groups(1).Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPrecios.Groups(1).Columns(3).caption = Ador(0).Value '15
        Ador.MoveNext
        sdbddPrecios.Groups(1).Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPrecios.Groups(1).Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPrecios.Groups(1).Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPrecios.Groups(2).caption = Ador(0).Value
        sdbgOfertas.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPrecios.Groups(2).Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPrecios.Groups(2).Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgOfertas.Columns(0).caption = Ador(0).Value '22 Cod prove
        Ador.MoveNext
        sdbgOfertas.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgOfertas.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgOfertas.Columns(5).caption = Ador(0).Value '25 Mon
        Ador.MoveNext
        sdbgOfertas.Columns(6).caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()
    
    Me.Height = 5055
    Me.Width = 10095
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    CargarAnyos
    
    ConfigurarSeguridad
    
    lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
    
    Set oProcesos = oFSGSRaiz.generar_CProcesos
   
    'Enlazo la drop down
    
    
    sdbddPrecios.AddItem ""
    
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set oGMN1Seleccionado = Nothing
    Set oProcesoSeleccionado = Nothing
    Set oProcesos = Nothing
    Set oGruposMN1 = Nothing
    Set ofrmLstPROCE = Nothing
    Me.Visible = False
    
End Sub





Private Sub sdbcAnyo_Click()
    
    sdbcProceCod = ""
    
End Sub

Private Sub sdbcProceCod_Click()
    If Not sdbcProceCod.DroppedDown Then
        sdbcProceDen = ""
        sdbcProceCod = ""
    End If
End Sub

Private Sub sdbcProceCod_Change()
    
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcProceDen.Text = ""
        bRespetarCombo = False
        Set oProcesoSeleccionado = Nothing
        cmdImprimir.Enabled = False
        bCargarComboDesde = True
    
    End If
    
    sdbgOfertas.RemoveAll

End Sub

Private Sub sdbcProceCod_CloseUp()
    
    If sdbcProceCod.Value = "..." Or Trim(sdbcProceCod.Value) = "" Then
        sdbcProceCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
    sdbcProceCod.Text = sdbcProceCod.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcProceCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcProceCod.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    Screen.MousePointer = vbHourglass
    
    Set oProcesoSeleccionado = Nothing
    cmdImprimir.Enabled = False
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, conofertas, Cerrado, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo, sdbcGMN1_4Cod, , , , val(sdbcProceCod), , , , , , , , , , False
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, conofertas, Cerrado, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo, sdbcGMN1_4Cod, , , , , , , , , , , , , , False
    End If
    
    
    Codigos = oProcesos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    'If bCargarComboDesde And Not oProcesos.EOF Then
    If Not oProcesos.EOF Then
        sdbcProceCod.AddItem "..."
    End If

    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Text)
    sdbcProceCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceCod_InitColumnProps()

    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProceCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProceCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceCod.Rows - 1
            bm = sdbcProceCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcProceCod_Validate(Cancel As Boolean)

    If Trim(sdbcProceCod.Text = "") Then Exit Sub
    
    If Not IsNumeric(sdbcProceCod) Then
        oMensajes.NoValido sIdioma(1)
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Text = "" Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcProceCod.Value > giMaxProceCod Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdioma(2)
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceCod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proceso
        Screen.MousePointer = vbHourglass
        oProcesos.CargarTodosLosProcesosDesde 1, conofertas, Cerrado, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo, sdbcGMN1_4Cod, , , , val(sdbcProceCod), , True, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, bRComp, bREqp, False
        Screen.MousePointer = vbNormal
        
        If oProcesos.Count = 0 Then
            sdbcProceCod.Text = ""
            oMensajes.NoValido sIdioma(2)
            Set oProcesoSeleccionado = Nothing
        Else
            bRespetarCombo = True
            sdbcProceDen.Text = oProcesos.Item(1).Den
            
            sdbcProceCod.Columns(0).Text = sdbcProceCod.Text
            sdbcProceCod.Columns(1).Text = sdbcProceDen.Text
            
            bRespetarCombo = False
            Set oProcesoSeleccionado = Nothing
            Set oProcesoSeleccionado = oProcesos.Item(1)
            
            ProcesoSeleccionado
        End If
    
    
End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub

Private Sub ProcesoSeleccionado()
Dim sCod As String
Dim b As Integer
Dim i As Integer
Dim oOferta As COferta

    sCod = sdbcGMN1_4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod))
    sCod = CStr(sdbcAnyo) & sCod & sdbcProceCod
       
    Set oProcesoSeleccionado = Nothing
    Set oProcesoSeleccionado = oProcesos.Item(sCod)
    
    If oProcesoSeleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    oProcesoSeleccionado.CargarDatosGeneralesProceso bSinpres:=True, bSinGRupos:=True
    If oProcesoSeleccionado.AdminPublica Then
        sdbgOfertas.Columns(6).DropDownHwnd = 0
    Else
        sdbgOfertas.Columns(6).DropDownHwnd = sdbddPrecios.hWnd
    End If
    Set oIOfertas = oProcesoSeleccionado
    
    cmdImprimir.Enabled = True
    
    Set oOfertas = oIOfertas.DevolverOfertasWeb(, , True, , , True)
    sdbgOfertas.RemoveAll
    
    If Not oOfertas Is Nothing Then
        For Each oOferta In oOfertas
            sdbgOfertas.AddItem oOferta.Prove & Chr(m_lSeparador) & oOferta.ProveDen & Chr(m_lSeparador) & oOferta.Num & Chr(m_lSeparador) & Format(oOferta.FechaRec, "Short Date") & Chr(m_lSeparador) & Format(oOferta.FechaHasta, "Short Date") & Chr(m_lSeparador) & oOferta.CodMon & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oOferta.indice
        Next
    End If
    
    
    'sdbgOfertas.SetFocus
    'sdbgOfertas.col = 6
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_Change()
    
    If Not bRespetarCombo Then
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        sdbcProceCod = ""
        sdbcProceDen = ""
    End If
    
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    sdbcProceCod = ""
    sdbcProceDen = ""
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    bRespetarCombo = False
    
    GMN1Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    sdbcGMN1_4Cod.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
   
    
    If bCargarComboDesde Then
    
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
          
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
    
    Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
           
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    'If bCargarComboDesde And Not oGruposMN1.EOF Then
    If Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()

    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Cod.Rows - 1
            bm = sdbcGMN1_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
        
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
     
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            oMensajes.NoValido sIdioma(3)
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
        
            bCargarComboDesde = False
            
        End If
    
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    
End Sub

Private Sub GMN1Seleccionado()
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
   
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
End Sub

Private Sub Arrange()
    
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
           
    If Height >= 1560 Then picSel.Height = Height - 1370
    
    If Width >= 225 Then picSel.Width = Width - 225
    
    sdbgOfertas.Width = picSel.Width
    sdbgOfertas.Columns(0).Width = sdbgOfertas.Width * 10 / 100
    sdbgOfertas.Columns(1).Width = sdbgOfertas.Width * 26 / 100
    sdbgOfertas.Columns(2).Width = sdbgOfertas.Width * 7 / 100
    sdbgOfertas.Columns(3).Width = sdbgOfertas.Width * 14 / 100
    sdbgOfertas.Columns(4).Width = sdbgOfertas.Width * 14 / 100
    sdbgOfertas.Columns(5).Width = sdbgOfertas.Width * 8.5 / 100
    sdbgOfertas.Columns(6).Width = sdbgOfertas.Width * 17.3 / 100
    sdbgOfertas.Height = picSel.Height
    
    cmdImprimir.Top = picSel.Top + picSel.Height + 100
    
End Sub


Private Sub sdbcProceDen_Click()
    If Not sdbcProceDen.DroppedDown Then
        sdbcProceDen = ""
        sdbcProceCod = ""
    End If
End Sub

Private Sub sdbcProceDen_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcProceCod.Text = ""
        Set oProcesoSeleccionado = Nothing
        bRespetarCombo = False
        
        sdbgOfertas.RemoveAll
        cmdImprimir.Enabled = False
        bCargarComboDesde = True
    
    End If
    
End Sub

Private Sub sdbcProceDen_CloseUp()
    
    If sdbcProceDen.Value = "....." Or Trim(sdbcProceDen.Value) = "" Then
        sdbcProceDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcProceCod.Text = sdbcProceDen.Columns(1).Text
    sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcProceDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcProceDen.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProcesoSeleccionado = Nothing
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, conofertas, Cerrado, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo, sdbcGMN1_4Cod, , , , , sdbcProceDen
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, conofertas, Cerrado, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo, sdbcGMN1_4Cod
    End If
    
    Codigos = oProcesos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    'If bCargarComboDesde And Not oProcesos.EOF Then
    If Not oProcesos.EOF Then
        sdbcProceDen.AddItem "..."
    End If

    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Text)
    sdbcProceDen.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceDen_InitColumnProps()

    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProceDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProceDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceDen.Rows - 1
            bm = sdbcProceDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub



Private Sub sdbddPrecios_CloseUp()

    sdbgOfertas.DataChanged = False
    sdbddPrecios.Columns("ART").Text = ""
End Sub

Private Sub sdbddPrecios_DropDown()
Dim oPrecio As CPrecioItem
        

        Screen.MousePointer = vbHourglass

        OfertaSeleccionada
        sdbddPrecios.RemoveAll
        oOfertaSeleccionada.CargarTodasLasLineasWeb OrdItemPorCodArt, , , , , True
        
            For Each oPrecio In oOfertaSeleccionada.Lineas
                sdbddPrecios.AddItem NullToStr(oPrecio.ArticuloCod) & Chr(m_lSeparador) & oPrecio.Descr & Chr(m_lSeparador) & oPrecio.DestCod & Chr(m_lSeparador) & oPrecio.UniCod & Chr(m_lSeparador) & oPrecio.FechaInicioSuministro & Chr(m_lSeparador) & oPrecio.FechaFinSuministro & Chr(m_lSeparador) & oPrecio.Cantidad & Chr(m_lSeparador) & oPrecio.PagCod & Chr(m_lSeparador) & DblToStr(oPrecio.Presupuesto / oPrecio.Cantidad) & Chr(m_lSeparador) & oPrecio.PrecioOferta & Chr(m_lSeparador) & NullToStr(oPrecio.CantidadMaxima)
            Next
        
        sdbddPrecios.DroppedDown = True
        
        Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddPrecios_InitColumnProps()
    sdbddPrecios.DataFieldList = "Column 0"
    sdbddPrecios.DataFieldToDisplay = "Column 0"
End Sub

Private Sub OfertaSeleccionada()
    
    Set oOfertaSeleccionada = Nothing
    Set oOfertaSeleccionada = oOfertas.Item(CStr(sdbgOfertas.Columns("INDICE").Value))
   
    
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
        bREqp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
        bRComp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
        bRMat = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
        bRCompResp = True
    End If
End Sub

Public Sub CargarProcesoConBusqueda()

    Set oProcesos = Nothing
    Set oProcesos = frmPROCEBuscar.oProceEncontrados
 
    Set frmPROCEBuscar.oProceEncontrados = Nothing
    sdbcAnyo = oProcesos.Item(1).Anyo
    bRespetarCombo = True
    sdbcGMN1_4Cod = oProcesos.Item(1).GMN1Cod
    sdbcGMN1_4Cod_Validate False
    
    sdbcProceCod = oProcesos.Item(1).Cod
    sdbcProceDen = oProcesos.Item(1).Den
    bRespetarCombo = False
    ProcesoSeleccionado
    'If Not oProcesoSeleccionado Is Nothing Then
        'If oProcesoSeleccionado.Estado >= tipoestadoproceso.validado And oProcesoSeleccionado.Estado < tipoestadoproceso.conasignacionvalida Then
        '    ProceSelector1.Seleccion = 0
        'Else
            '����Hay que a�adir tambi�n el proceselector????
            'If oProcesoSeleccionado.Estado >= tipoestadoproceso.conasignacionvalida And oProcesoSeleccionado.Estado < tipoestadoproceso.conadjudicaciones Then
            '    ProceSelector1.Seleccion = 1
            'Else
            '    ProceSelector1.Seleccion = 2
            'End If
        'End If
    'End If
End Sub

