VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmDesglose 
   Caption         =   "DDesglose de material"
   ClientHeight    =   5625
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11970
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDesglose.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5625
   ScaleWidth      =   11970
   Begin VB.PictureBox picItems 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   60
      ScaleHeight     =   375
      ScaleWidth      =   12000
      TabIndex        =   0
      Top             =   60
      Width           =   12000
      Begin VB.CheckBox chkPermitirExcel 
         Caption         =   "DPermitir cargar l�neas desde  excel"
         Height          =   195
         Left            =   10560
         TabIndex        =   12
         Top             =   60
         Width           =   3100
      End
      Begin VB.CheckBox chkVincular 
         Caption         =   "DVincular con otras solicitudes"
         Height          =   195
         Left            =   7680
         TabIndex        =   11
         Top             =   60
         Width           =   2655
      End
      Begin VB.CommandButton cmdVincular 
         Height          =   312
         Left            =   10320
         Picture         =   "frmDesglose.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   0
         Width           =   432
      End
      Begin VB.CheckBox chkPOPUP 
         Caption         =   "DMostrar desglose en una ventana emergente"
         Height          =   195
         Left            =   3960
         TabIndex        =   7
         Top             =   60
         Width           =   4575
      End
      Begin VB.CommandButton cmdAnyaCalculo 
         Height          =   312
         Left            =   2160
         Picture         =   "frmDesglose.frx":0D58
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   0
         Width           =   432
      End
      Begin VB.CommandButton cmdElimItem 
         Height          =   312
         Left            =   1620
         Picture         =   "frmDesglose.frx":0DAF
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   0
         Width           =   432
      End
      Begin VB.CommandButton cmdAnyaItem 
         Height          =   312
         Left            =   1080
         Picture         =   "frmDesglose.frx":0E41
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   0
         Width           =   432
      End
      Begin VB.CommandButton cmdSubir 
         Height          =   312
         Left            =   0
         Picture         =   "frmDesglose.frx":0EC3
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   432
      End
      Begin VB.CommandButton cmdBajar 
         Height          =   312
         Left            =   540
         Picture         =   "frmDesglose.frx":1205
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   432
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcFecha 
         Height          =   285
         Left            =   5280
         TabIndex        =   8
         Top             =   0
         Width           =   2235
         DataFieldList   =   "Column 1"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   -2147483640
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4736
         Columns(1).Caption=   "TIPO"
         Columns(1).Name =   "TIPO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3942
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblFecha 
         Caption         =   "DFecha limite solicitud acciones:"
         Height          =   195
         Left            =   2880
         TabIndex        =   9
         Top             =   60
         Width           =   2355
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
      Height          =   5145
      Left            =   60
      TabIndex        =   6
      Top             =   435
      Width           =   11895
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   10
      stylesets.count =   28
      stylesets(0).Name=   "ECSPA"
      stylesets(0).Picture=   "frmDesglose.frx":1547
      stylesets(0).AlignmentPicture=   1
      stylesets(1).Name=   "PiezasDefectuosasGER"
      stylesets(1).Picture=   "frmDesglose.frx":1899
      stylesets(1).AlignmentPicture=   1
      stylesets(2).Name=   "PDSumatorioGER"
      stylesets(2).BackColor=   16766421
      stylesets(2).Picture=   "frmDesglose.frx":1900
      stylesets(2).AlignmentPicture=   1
      stylesets(3).Name=   "Calculado"
      stylesets(3).BackColor=   16766421
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmDesglose.frx":191C
      stylesets(3).AlignmentPicture=   1
      stylesets(4).Name=   "PiezasDefectuosasSPA"
      stylesets(4).Picture=   "frmDesglose.frx":1938
      stylesets(4).AlignmentPicture=   1
      stylesets(5).Name=   "PDSumatorioSPA"
      stylesets(5).BackColor=   16766421
      stylesets(5).Picture=   "frmDesglose.frx":1E3A
      stylesets(5).AlignmentPicture=   1
      stylesets(6).Name=   "PiezasDefectuosasSumatorioENG"
      stylesets(6).BackColor=   16766421
      stylesets(6).Picture=   "frmDesglose.frx":1E56
      stylesets(6).AlignmentPicture=   1
      stylesets(7).Name=   "PDENG"
      stylesets(7).Picture=   "frmDesglose.frx":1E72
      stylesets(7).AlignmentPicture=   1
      stylesets(8).Name=   "ECSumatorioGER"
      stylesets(8).BackColor=   16766421
      stylesets(8).Picture=   "frmDesglose.frx":1E8E
      stylesets(8).AlignmentPicture=   1
      stylesets(9).Name=   "Gris"
      stylesets(9).BackColor=   -2147483633
      stylesets(9).HasFont=   -1  'True
      BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(9).Picture=   "frmDesglose.frx":1EAA
      stylesets(10).Name=   "ECSumatorioSPA"
      stylesets(10).BackColor=   16766421
      stylesets(10).Picture=   "frmDesglose.frx":1EC6
      stylesets(10).AlignmentPicture=   1
      stylesets(11).Name=   "EnviosCorrectosSumatorioENG"
      stylesets(11).BackColor=   16766421
      stylesets(11).Picture=   "frmDesglose.frx":23F8
      stylesets(11).AlignmentPicture=   1
      stylesets(12).Name=   "Amarillo"
      stylesets(12).BackColor=   12648447
      stylesets(12).HasFont=   -1  'True
      BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(12).Picture=   "frmDesglose.frx":372E
      stylesets(13).Name=   "PiezasDefectuosasSumatorioGER"
      stylesets(13).BackColor=   16766421
      stylesets(13).Picture=   "frmDesglose.frx":374A
      stylesets(13).AlignmentPicture=   1
      stylesets(14).Name=   "PDGER"
      stylesets(14).Picture=   "frmDesglose.frx":3A9C
      stylesets(14).AlignmentPicture=   1
      stylesets(15).Name=   "PiezasDefectuosasSumatorioSPA"
      stylesets(15).BackColor=   16766421
      stylesets(15).Picture=   "frmDesglose.frx":3AB8
      stylesets(15).AlignmentPicture=   1
      stylesets(16).Name=   "PDSPA"
      stylesets(16).Picture=   "frmDesglose.frx":4B4A
      stylesets(16).AlignmentPicture=   1
      stylesets(17).Name=   "EnviosCorrectosENG"
      stylesets(17).Picture=   "frmDesglose.frx":4B66
      stylesets(17).AlignmentPicture=   1
      stylesets(18).Name=   "ECENG"
      stylesets(18).Picture=   "frmDesglose.frx":4B82
      stylesets(18).AlignmentPicture=   1
      stylesets(19).Name=   "EnviosCorrectosSumatorioGER"
      stylesets(19).BackColor=   16766421
      stylesets(19).Picture=   "frmDesglose.frx":4F7C
      stylesets(19).AlignmentPicture=   1
      stylesets(20).Name=   "PiezasDefectuosasENG"
      stylesets(20).Picture=   "frmDesglose.frx":62B2
      stylesets(20).AlignmentPicture=   1
      stylesets(21).Name=   "PDSumatorioENG"
      stylesets(21).BackColor=   16766421
      stylesets(21).Picture=   "frmDesglose.frx":7114
      stylesets(21).AlignmentPicture=   1
      stylesets(22).Name=   "EnviosCorrectosSumatorioSPA"
      stylesets(22).BackColor=   16766421
      stylesets(22).Picture=   "frmDesglose.frx":7130
      stylesets(22).AlignmentPicture=   1
      stylesets(23).Name=   "EnviosCorrectosGER"
      stylesets(23).Picture=   "frmDesglose.frx":714C
      stylesets(23).AlignmentPicture=   1
      stylesets(24).Name=   "ECSumatorioENG"
      stylesets(24).BackColor=   16766421
      stylesets(24).Picture=   "frmDesglose.frx":81DE
      stylesets(24).AlignmentPicture=   1
      stylesets(25).Name=   "ECGER"
      stylesets(25).Picture=   "frmDesglose.frx":8E00
      stylesets(25).AlignmentPicture=   1
      stylesets(26).Name=   "Azul"
      stylesets(26).BackColor=   16777160
      stylesets(26).HasFont=   -1  'True
      BeginProperty stylesets(26).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(26).Picture=   "frmDesglose.frx":8E1C
      stylesets(27).Name=   "EnviosCorrectosSPA"
      stylesets(27).Picture=   "frmDesglose.frx":8E38
      stylesets(27).AlignmentPicture=   1
      DividerType     =   0
      MultiLine       =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   10
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2646
      Columns(1).Caption=   "ATRIBUTO"
      Columns(1).Name =   "ATRIBUTO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   1773
      Columns(2).Caption=   "APROCESO"
      Columns(2).Name =   "APROCESO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   2
      Columns(3).Width=   1773
      Columns(3).Caption=   "APEDIDO"
      Columns(3).Name =   "APEDIDO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      Columns(4).Width=   1588
      Columns(4).Caption=   "AYUDA"
      Columns(4).Name =   "AYUDA"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   4
      Columns(4).ButtonsAlways=   -1  'True
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "TIPO"
      Columns(5).Name =   "TIPO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "SUBTIPO"
      Columns(6).Name =   "SUBTIPO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "CAMPO_GS"
      Columns(7).Name =   "CAMPO_GS"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "IDATRIB"
      Columns(8).Name =   "IDATRIB"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "TABLA_EXTERNA"
      Columns(9).Name =   "TABLA_EXTERNA"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   2
      Columns(9).FieldLen=   256
      _ExtentX        =   20981
      _ExtentY        =   9075
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDesglose"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas:
Public g_Accion As accionessummit
Public g_oCampoDesglose As CFormItem
Public g_bModif As Boolean
Public g_sOrigen As String
Public g_ofrmATRIB As frmAtrib
Public g_Instancia As Long

Public g_strPK As String

'Variables privadas:
Private m_oIBaseDatos As IBaseDatos
Public m_oIdiomas As CIdiomas
Private m_oCampoEnEdicion As CFormItem
Private m_sCamposExterno(1) As String               'aqui iran las definiciones de la tabla en los 3 idiomas de momento solo pongo 1 en spa
Private m_oTablaExterna As CTablaExterna            'clase que almacena funciones
Private m_intTabla As Integer
Private m_blnExisteTablaExternaParaArticulo As Boolean

'Variables para idiomas
Private m_sDato As String
Private m_sIdiConf As String
Private m_sCamposGS(26) As String
Private m_bExisteCodArticulo As Boolean
Private m_bConEnviosCorrectos As Boolean
Private m_bConPiezasDefectuosas As Boolean
Private m_bArt As Boolean
Private m_sImposibleAnyadircampo As String
Private m_sNoPuedenExistirDosCampoMoneda As String
Dim m_bOcultarSelCualquierMatPortal As Boolean


Private Sub chkPermitirExcel_Click()
    Dim teserror As TipoErrorSummit
    'Mostrar� o no el desglose en el PM y QA  importar desde excel
    If m_oIdiomas Is Nothing Then Exit Sub 'si est� haciendo el load
    
    Screen.MousePointer = vbHourglass
    If chkPermitirExcel.Value = vbChecked Then
        g_oCampoDesglose.PermitirExcel = True
    Else
        g_oCampoDesglose.PermitirExcel = False
    End If
    
    teserror = g_oCampoDesglose.ModificarDesglosePermitirExcel
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If

End Sub

Private Sub chkPOPUP_Click()
Dim teserror As TipoErrorSummit



    'Mostrar� o no el desglose en el PM y QA en una ventana emergente:
    If m_oIdiomas Is Nothing Then Exit Sub 'si est� haciendo el load
    
    Screen.MousePointer = vbHourglass
    If chkPOPUP.Value = vbChecked Then
        g_oCampoDesglose.MostrarPOPUP = True
    Else
        g_oCampoDesglose.MostrarPOPUP = False
    End If
    
    teserror = g_oCampoDesglose.ModificarDesgloseEnPOPUP
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
End Sub

Private Sub cmdAnyaCalculo_Click()
Dim oCampo As CFormItem

    'Muestra la pantalla para crear los campos calculados:
    If g_sOrigen <> "frmFormularios" Then Exit Sub
    
    frmFormularioDesgloseCalculo.g_bModif = g_bModif
    frmFormularioDesgloseCalculo.g_sOrigen = "frmDesglose"
    
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    oCampo.Id = g_oCampoDesglose.Id
    Set oCampo.Grupo = g_oCampoDesglose.Grupo
    Set oCampo.Denominaciones = g_oCampoDesglose.Denominaciones
    Set frmFormularioDesgloseCalculo.g_oCampoDesglose = oCampo
    
    frmFormularioDesgloseCalculo.Show vbModal
    
    Set oCampo = Nothing
                
End Sub


''' Revisado por: blp. Fecha: 23/10/2012
''' <summary>
''' Procedimiento que oculta/visualiza las opciones de men� para a�adir campos.
''' </summary>
''' Llamada desde el evento Click. M�x. 0,1 seg
Private Sub cmdAnyaItem_Click()

Dim ADORs As Ador.Recordset
Dim oTiposPredef As CTiposSolicit
Dim oArbolPres As cPresConceptos5Nivel0
Dim lTipos As Integer
Dim i As Integer

    'Si hay cambios los guarda antes de perder el foco.
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
    End If
    
    'validar
    
    MDI.mnuAnyaCampoGS(7).Visible = False 'no muestra el campo de GS de desglose
    MDI.mnuAnyaCampoGS(28).Visible = False 'no muestra el campo de GS de Desglose de factura
    MDI.mnuAnyaCampoGS(30).Visible = False 'no muestra el campo de GS de Inicio de abono
    MDI.mnuAnyaCampoGS(31).Visible = False 'no muestra el campo de GS de Fin de abono
    MDI.mnuAnyaCampoGS(32).Visible = False 'no muestra el campo de GS de Retenci�n en garant�a
    MDI.mnuAnyaCampoGS(33).Visible = True 'no muestra el campo de GS de Unidad de pedido
    MDI.mnuAnyaCampoGS(35).Visible = False 'no muestra el campo de GS de Comprador
            
    If Not basParametros.gParametrosGenerales.gbUsarPres1 Then
        MDI.mnuAnyaCampoGS.Item(11).Visible = False
    Else
        MDI.mnuAnyaCampoGS.Item(11).Visible = True
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres2 Then
        MDI.mnuAnyaCampoGS.Item(12).Visible = False
    Else
        MDI.mnuAnyaCampoGS.Item(12).Visible = True
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres3 Then
        MDI.mnuAnyaCampoGS.Item(13).Visible = False
    Else
        MDI.mnuAnyaCampoGS.Item(13).Visible = True
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres4 Then
        MDI.mnuAnyaCampoGS.Item(14).Visible = False
    Else
        MDI.mnuAnyaCampoGS.Item(14).Visible = True
    End If

    'Dependiendo del acceso se podr� seleccionar el a�adir campos de distintos tipos.Configura el men�:
    If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then
        MDI.mnuAnyaCampo(2).Visible = True
        MDI.mnuAnyaCampo(3).Visible = False
        MDI.mnuAnyaCampo(5).Visible = False
        MDI.PopupMenu MDI.mnuPOPUPAnyaCampo
    End If
    
    If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSCompleto _
    Or gParametrosGenerales.gsAccesoContrato = TipoAccesoContrato.AccesoContrato _
    Or gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM _
    Then
        MDI.mnuAnyaCampo(2).Visible = False
        MDI.mnuAnyaCampo(3).Visible = True
        MDI.mnuAnyaCampo(5).Visible = True
        
        'A�ade los tipos de solicitudes din�micamente al men� (menos la de solicitudes de compras):
        MDI.mnuAnyaCampoPredef(1).Tag = TipoSolicitud.SolicitudCompras
        
        Set oTiposPredef = oFSGSRaiz.Generar_CTiposSolicit
        Set ADORs = oTiposPredef.DevolverTiposSolicitudes(True)
    
        If Not ADORs Is Nothing Then
            lTipos = ADORs.RecordCount
            While Not ADORs.EOF
                Load MDI.mnuAnyaCampoPredef(MDI.mnuAnyaCampoPredef.Count + 1)
                MDI.mnuAnyaCampoPredef(MDI.mnuAnyaCampoPredef.Count).caption = ADORs("DESCR_" & gParametrosInstalacion.gIdioma).Value
                MDI.mnuAnyaCampoPredef(MDI.mnuAnyaCampoPredef.Count).Tag = ADORs("ID").Value
                ADORs.MoveNext
            Wend
            ADORs.Close
            Set ADORs = Nothing
        End If
        
        If gParametrosGenerales.gbUsarOrgCompras Then
            MDI.mnuAnyaCampoGS.Item(19).Visible = True 'Opcion de Menu Organizacion de Compras
            MDI.mnuAnyaCampoGS.Item(20).Visible = True 'Opcion de Menu Departamento
        Else
            MDI.mnuAnyaCampoGS.Item(19).Visible = False
            MDI.mnuAnyaCampoGS.Item(20).Visible = False
        End If
        
        MDI.mnuAnyaCampoGS.Item(22).Visible = False 'Opcion de Menu Importe solicitudes vinculadas
        MDI.mnuAnyaCampoGS.Item(23).Visible = False 'Opcion de Menu Referencia a solicitud
        
        MDI.mnuAnyaCampoGS.Item(24).Visible = False 'Opcion de Tipo de Pedido
        
        MDI.mnuAnyaCampoGS.Item(27).Visible = False 'Opcion de Desglose de actividad
        
        MDI.mnuAnyaCampoGS.Item(28).Visible = False 'Opcion de Unidad de pedido

        If Not g_oParametrosSM Is Nothing Then
            'Habr� un campo de sistema nuevo por cada �rbol presupuestario que hayamos definido en la secci�n presupuestaria del SM.
            Set oArbolPres = oFSGSRaiz.Generar_CPresConceptos5Nivel0
            Set ADORs = oArbolPres.DevolverArbolesPresupuestarios
               
            'Por si hay arboles presupuestarios, los descarga todos
            Dim cont As Integer
            cont = MDI.mnuAnyaCampoGS.Count
            While cont >= 39
                Unload MDI.mnuAnyaCampoGS(cont)
                cont = cont - 1
            Wend
            If Not ADORs Is Nothing Then
                While Not ADORs.EOF
                    Load MDI.mnuAnyaCampoGS(MDI.mnuAnyaCampoGS.Count + 1)
                    MDI.mnuAnyaCampoGS(MDI.mnuAnyaCampoGS.Count).caption = NullToStr(ADORs("DEN").Value)
                    MDI.mnuAnyaCampoGS(MDI.mnuAnyaCampoGS.Count).Tag = tipocampogs.PartidaPresupuestaria & "#" & ADORs("COD").Value
                    ADORs.MoveNext
                Wend
                ADORs.Close
                Set ADORs = Nothing
            End If
        End If
        
        Dim ador1 As Ador.Recordset

        MDI.mnuAnyaCampo(6).Visible = True 'Campos Externos
        MDI.mnuAnyaCampoExterno(1).Visible = True 'Carga bajo demanda
        Dim oServicio As CServicio
        Set oServicio = oFSGSRaiz.Generar_CServicio
        If oServicio.TieneResultados() Then
            Set ador1 = oServicio.DenominacionesIdioma(basParametros.gParametrosGenerales.gIdioma)
            If Not ador1 Is Nothing Then
                ador1.MoveFirst
                'Por si hay Submenu cargados, los descarga a todos menos la opci�n Nuevo
                For i = 1 To MDI.mnuAnyaCampoExternoDemanda.Count - 1
                    Unload MDI.mnuAnyaCampoExternoDemanda(MDI.mnuAnyaCampoExternoDemanda.Count)
                Next
                Do While Not ador1.EOF
                    Load MDI.mnuAnyaCampoExternoDemanda(MDI.mnuAnyaCampoExternoDemanda.Count + 1)
                    MDI.mnuAnyaCampoExternoDemanda(MDI.mnuAnyaCampoExternoDemanda.Count).caption = NullToStr(ador1.Fields(1).Value)  'denominacion
                    MDI.mnuAnyaCampoExternoDemanda(MDI.mnuAnyaCampoExternoDemanda.Count).Tag = ador1.Fields(0).Value               'id
                    ador1.MoveNext
                Loop
                ador1.Close
                Set ador1 = Nothing
            End If
        End If
        If m_oTablaExterna.TieneResultados() Then
            MDI.mnuAnyaCampoExterno(2).Visible = True 'Carga peri�dica
            Set ador1 = m_oTablaExterna.Denominaciones()
            If Not ador1 Is Nothing Then
                ador1.MoveFirst
                'Por si hay Submenu cargados, los descarga a todos
                For i = 1 To MDI.mnuAnyaCampoExternoPeriodico.Count - 1
                    Unload MDI.mnuAnyaCampoExternoPeriodico(MDI.mnuAnyaCampoExternoPeriodico.Count)
                Next
                Do While Not ador1.EOF
                    MDI.mnuAnyaCampoExternoPeriodico(MDI.mnuAnyaCampoExternoPeriodico.Count).caption = NullToStr(ador1.Fields(1).Value)   'denominacion
                    MDI.mnuAnyaCampoExternoPeriodico(MDI.mnuAnyaCampoExternoPeriodico.Count).Tag = ador1.Fields(0).Value                  'id
                    Load MDI.mnuAnyaCampoExternoPeriodico(MDI.mnuAnyaCampoExternoPeriodico.Count + 1)
                    ador1.MoveNext
                Loop
                Unload MDI.mnuAnyaCampoExternoPeriodico(MDI.mnuAnyaCampoExternoPeriodico.Count)
                ador1.Close
                Set ador1 = Nothing
            End If
        Else
            MDI.mnuAnyaCampoExterno(2).Visible = False
        End If

        'Opcion menu Desglose de actividad
        MDI.mnuAnyaCampoGS(27).Visible = False
        
        MDI.PopupMenu MDI.mnuPOPUPAnyaCampo
        
        'borra los tipos de solicitudes del men� (menos la de solicitudes de compras):
        For i = 1 To lTipos
            Unload MDI.mnuAnyaCampoPredef(MDI.mnuAnyaCampoPredef.Count)
        Next i
    End If
    
    
End Sub

Private Sub cmdBajar_Click()
Dim i As Integer
Dim arrValoresMat() As Variant
Dim arrValoresArt() As Variant
Dim arrValoresDen() As Variant
Dim arrValoresAux() As Variant
Dim bOrdenoMat, bOrdenoPais As Boolean
Dim bOrdenoArt, bOrdenoProvi As Boolean
Dim bOrdenoDen As Boolean
Dim bOrdenoCampoMat, bOrdenoCampoPais As Boolean
Dim vbm As Variant
    
    'si hay cambios los guarda
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
    End If
    
    If sdbgCampos.SelBookmarks.Count = 0 Then Exit Sub
    If sdbgCampos.AddItemRowIndex(sdbgCampos.SelBookmarks.Item(0)) = sdbgCampos.Rows - 1 Then Exit Sub
        
    ReDim arrValoresMat(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresArt(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresDen(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresAux(sdbgCampos.Columns.Count - 1)
    
    'Ordenacion del Material // Provincia
    If sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Pais Or sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.material Then
        vbm = sdbgCampos.GetBookmark(1)
        If (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.provincia) Or _
            (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.material And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.NuevoCodArticulo) Or _
           (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.material And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.CodArticulo) Then
            If sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.material And (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = tipocampogs.DenArticulo) Then
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 3 Then Exit Sub
                bOrdenoMat = True
            Else
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                bOrdenoPais = True
            End If
                
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).Value
            Next i
        End If
    'Ordenacion del articulo // Provincia
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.provincia Or sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.NuevoCodArticulo Then
        vbm = sdbgCampos.GetBookmark(-1)
        If (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.provincia And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.Pais) Or _
           (((sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.CodArticulo) Or (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.NuevoCodArticulo)) And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.material) Then
            If sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.provincia Or sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) <> tipocampogs.DenArticulo Then
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
                bOrdenoProvi = True
            Else
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                bOrdenoArt = True
            End If
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).Value
            Next i
        End If
    'Ordenacion de la denominacion
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.DenArticulo) Then
        If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = tipocampogs.NuevoCodArticulo Then
            If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
            bOrdenoDen = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresDen(i) = sdbgCampos.Columns(i).Value
            Next i
        End If
    Else
        vbm = sdbgCampos.GetBookmark(1)
        
        If (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = tipocampogs.provincia) Or _
           (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.material And ((sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = tipocampogs.CodArticulo) Or (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = tipocampogs.NuevoCodArticulo))) Then
            'Por si debajo del campo a bajar, esta el Material // Provincia
            If (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = tipocampogs.DenArticulo) Then
                bOrdenoCampoMat = True
            Else
                bOrdenoCampoPais = True
            End If
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                arrValoresDen(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(3))
            Next i
        End If
    End If
    
    If bOrdenoMat Or bOrdenoPais Then
        'Ordeno conjuntamente los campos de material y art�culo y de pa�s y provincia
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresArt(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)  'pongo el Material//pa�s en la fila siguiente
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)  'pongo la Articulo//provincia en la fila siguiente
        Next i
        
        If bOrdenoMat Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresDen(i)  'pongo la provincia en la fila siguiente
            Next i
        sdbgCampos.MovePrevious
        End If
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If bOrdenoMat Then
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresAux(i)  'pongo la 3� fila donde antes estaba el pa�s.
            Next i
        Else
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)  'pongo la 3� fila donde antes estaba el pa�s.
            Next i
        End If
        
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext

    ElseIf bOrdenoArt Or bOrdenoProvi Then
        'Ordeno conjuntamente los campos de material y art�culo y de pa�s y provincia
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)  'donde est� la provincia pongo el pa�s
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdenoArt Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
        sdbgCampos.MovePrevious
        End If
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If bOrdenoArt Then
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresAux(i)  'donde estaba el pa�s pongo el contenido de la 3� fila
            Next i
        Else
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)  'donde estaba el pa�s pongo el contenido de la 3� fila
            Next i
        End If
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
    
    ElseIf bOrdenoDen = True Then
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresDen(i)
        Next i
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        
        
    ElseIf bOrdenoCampoMat Or bOrdenoCampoPais Then
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        If Not bOrdenoCampoPais Then
            sdbgCampos.MoveNext
        End If
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        
        
        If Not bOrdenoCampoPais Then
            sdbgCampos.MovePrevious
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
        End If
      
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        If Not bOrdenoCampoPais Then
            sdbgCampos.MoveNext
        End If
        sdbgCampos.SelBookmarks.RemoveAll
        
    Else  'Ordeno normal,de 1 en 1
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
        Next i
        sdbgCampos.MoveNext
            
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        
        sdbgCampos.MovePrevious
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
            
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        
        If sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = tipocampogs.Centro Then
            g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(1)))).EliminarCentroNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).Id
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = tipocampogs.Almacen Then
                g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(2)))).EliminarAlmacenNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(1)))).Id
            End If
        ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = tipocampogs.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Centro Then
            g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).EliminarCentroNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(-1)))).Id
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = tipocampogs.Almacen Then
                g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(1)))).EliminarAlmacenNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).Id
            End If
        ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = tipocampogs.Centro And sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Almacen Then
            g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).EliminarAlmacenNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(-1)))).Id
        Else
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = tipocampogs.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = tipocampogs.Centro Then
                g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(-1)))).EliminarCentroNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(-2)))).Id
            ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = tipocampogs.Centro And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = tipocampogs.Almacen Then
                g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(-1)))).EliminarAlmacenNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(-2)))).Id
            End If
        End If
        
    End If
    
    sdbgCampos.SelBookmarks.Add sdbgCampos.Bookmark
 
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos
End Sub

''' <summary>
''' Borrar un campo seleccionado del grid sdbgCampos.
''' No importa la posici�n de los campos QA relacionados con art�culo deben borrarse con el art�culo.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub cmdElimItem_Click()
Dim udtTeserror As TipoErrorSummit
Dim irespuesta As Integer
Dim vbm As Variant
Dim bEliminar2Campos As Boolean
Dim bEliminar3Campos As Boolean
Dim bPaisProvi As Boolean
Dim bQaImpPie As Boolean
Dim bQaUno As Boolean
Dim bHayFechaImputacionDefecto As Boolean
Dim iTipo As Integer
Dim bm As Variant
Dim j As Integer
Dim bExiste As Boolean

Dim i As Integer
Dim aIdentificadores As Variant
Dim aBookmarks As Variant
Dim vBookmark As Variant
Dim oIBaseDatos As IBaseDatos
Dim bConEnvios As Boolean

On Error GoTo Cancelar:
    
    'Si hay cambios los guarda antes de perder el foco.
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
    End If
    
    If sdbgCampos.Rows = 0 Then Exit Sub
    If sdbgCampos.SelBookmarks.Count = 0 Then sdbgCampos.SelBookmarks.Add sdbgCampos.Bookmark
    
    g_Accion = ACCDesgloseCampoEliminar

    vbm = sdbgCampos.GetBookmark(1)
    If sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.provincia Then
        bEliminar2Campos = True
        bPaisProvi = True
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, tipocampogs.Pais)
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.material And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.CodArticulo Then
        bEliminar2Campos = True
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, tipocampogs.material)
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.material And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.NuevoCodArticulo Then
        bEliminar3Campos = True
        bConEnvios = m_bConEnviosCorrectos
        bQaImpPie = False
        bQaUno = False
        bHayFechaImputacionDefecto = False
        
        If HayCamposQARelacionados(bQaImpPie, bQaUno, iTipo, bHayFechaImputacionDefecto) Then
            irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, tipocampogs.material, iTipo)
        Else
            irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, tipocampogs.material)
        End If
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.NuevoCodArticulo And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.DenArticulo Then
        bEliminar2Campos = True
        bConEnvios = m_bConEnviosCorrectos
        bQaImpPie = False
        bQaUno = False
        bHayFechaImputacionDefecto = False
        
        If HayCamposQARelacionados(bQaImpPie, bQaUno, iTipo, bHayFechaImputacionDefecto) Then
            irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, tipocampogs.NuevoCodArticulo, iTipo)
        Else
            irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, tipocampogs.CodArticulo)
        End If
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.DenArticulo Then
        bEliminar2Campos = True
        vbm = sdbgCampos.GetBookmark(-1)
        bConEnvios = m_bConEnviosCorrectos
        bQaImpPie = False
        bQaUno = False
        bHayFechaImputacionDefecto = False
        
        If HayCamposQARelacionados(bQaImpPie, bQaUno, iTipo, bHayFechaImputacionDefecto) Then
            irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, tipocampogs.DenArticulo, iTipo)
        Else
            irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, tipocampogs.DenArticulo)
        End If
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.EnviosCorrectos Then
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value)
        If irespuesta = vbYes Then m_bConEnviosCorrectos = False
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.PiezasDefectuosas Then
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value)
        If irespuesta = vbYes Then m_bConPiezasDefectuosas = False
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.CentroCoste Then
        Dim oCentroCoste As CFormItem
        Set oCentroCoste = ComprobarCentroDeCoste(sdbgCampos.Columns("ID").Value)
        If Not oCentroCoste Is Nothing Then
            oMensajes.ImposibleEliminarCentroDeCoste oCentroCoste, gParametrosGenerales.gIdioma
            irespuesta = vbNo
        Else
            irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value)
        End If
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.OrganizacionCompras Then
        'Si se quiere eliminar la organizacion de compras, se comprobara antes que no exista el campoGS Proveedor ERP, si existe se deberia eliminar este campo primero
        bExiste = g_oCampoDesglose.Desglose.ExisteCampoGS(tipocampogs.ProveedorERP)
        If bExiste Then
            'Si existe se mirara si existe el campo Org.Compras a nivel de formulario, si existe, si se permite eliminar
            'ya que el campo proveedor ERP pasaria a depender del campo Org.compras del formulario
            If ComprobarOrgCompras(True) = False Then
                oMensajes.ImposibleEliminarProveedor (tipocampogs.OrganizacionCompras)
                irespuesta = vbNo
            Else
                irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value)
            End If
        Else
            irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value)
        End If
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Proveedor Then
        'Si se quiere eliminar el proveedor, se comprobara antes que no exista el campoGS Proveedor ERP, si existe se deberia eliminar este campo primero
        
        bExiste = g_oCampoDesglose.Desglose.ExisteCampoGS(tipocampogs.ProveedorERP)
        If bExiste Then
            oMensajes.ImposibleEliminarProveedor (tipocampogs.Proveedor)
            irespuesta = vbNo
        Else
            irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value)
        End If
    Else
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value)
    End If
    
    If irespuesta = vbNo Then
        g_Accion = ACCDesgloseCons
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    Dim iCont As Integer

    If bEliminar3Campos Then
        If bQaImpPie = True Then
            iCont = 4
        ElseIf bQaUno = True Then
            iCont = 3
        Else
            iCont = 2
        End If
        m_bConPiezasDefectuosas = False
        m_bArt = False
    ElseIf bEliminar2Campos Then
        If bQaImpPie Then
            iCont = 3
        ElseIf bQaUno Then
           iCont = 2
        Else
            iCont = 1
        End If
        m_bConPiezasDefectuosas = False
        If Not bPaisProvi Then m_bArt = False
    Else
        iCont = 0
    End If
    If bConEnvios Then
        iCont = iCont + 1
        m_bConEnviosCorrectos = False
    End If
    If bHayFechaImputacionDefecto Then
        iCont = iCont + 1
    End If
    ReDim aIdentificadores(sdbgCampos.SelBookmarks.Count + iCont)
    ReDim aBookmarks(sdbgCampos.SelBookmarks.Count + iCont)
        
    i = 0
    While i < sdbgCampos.SelBookmarks.Count
        sdbgCampos.Bookmark = sdbgCampos.SelBookmarks(i)
        aIdentificadores(i + 1) = sdbgCampos.Columns("ID").Value
        aBookmarks(i + 1) = sdbgCampos.SelBookmarks(i)
        i = i + 1
    Wend
    If bEliminar3Campos = True Or bEliminar2Campos Then
        aIdentificadores(i + 1) = sdbgCampos.Columns("ID").CellValue(vbm)
        aBookmarks(i + 1) = vbm
        If bEliminar3Campos Then
            aIdentificadores(i + 2) = sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(2))
            aBookmarks(i + 2) = sdbgCampos.GetBookmark(2)
        End If
    End If
    
    '''
    If bQaImpPie Or bQaUno Or bConEnvios Or bHayFechaImputacionDefecto Then
        If bEliminar2Campos Then
            i = i + 1
        Else
            i = i + 2
        End If
        
        For j = 0 To sdbgCampos.Rows - 1
            bm = sdbgCampos.RowBookmark(j)
            If sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoNoConformidad.PiezasDefectuosas _
            Or sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoNoConformidad.ImporteRepercutido _
            Or sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoNoConformidad.EnviosCorrectos _
            Or sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoNoConformidad.FechaImputacionDefecto Then
                aIdentificadores(i + 1) = sdbgCampos.Columns("ID").CellValue(bm)
                aBookmarks(i + 1) = bm
                
                i = i + 1
            End If
        Next
    End If
    '''
    
    'Elimina de base de datos:
    udtTeserror = g_oCampoDesglose.Desglose.EliminarCamposDeBaseDatos(aIdentificadores)
    
    If udtTeserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError udtTeserror
    Else
        'Elimina los campos de la colecci�n:
        For i = 1 To UBound(aIdentificadores)
             g_oCampoDesglose.Desglose.Remove (CStr(aIdentificadores(i)))
             basSeguridad.RegistrarAccion accionessummit.ACCDesgloseCampoEliminar, aIdentificadores(i)
        Next i
        
        'Elimina los campos de la grid:
        For i = 1 To UBound(aBookmarks)
            sdbgCampos.RemoveItem (sdbgCampos.AddItemRowIndex(aBookmarks(i)))
        Next i

        'Se posiciona en la fila correspondiente:
        If sdbgCampos.Rows > 0 Then
            If IsEmpty(sdbgCampos.RowBookmark(sdbgCampos.Row)) Then
                vBookmark = sdbgCampos.RowBookmark(sdbgCampos.Row - 1)
            Else
                vBookmark = sdbgCampos.RowBookmark(sdbgCampos.Row)
            End If
        End If
        
        'Miramos si despu�s de la eliminaci�n queda algun campo tipo Art�culo
        If Not ExisteCodArticulo(g_oCampoDesglose.Desglose) Then
            'Ponemos todos los campos con Proceso y Pedido a 0
            Dim oCampo As CFormItem
            For Each oCampo In g_oCampoDesglose.Desglose
                
                If oCampo.APROCESO = True Or oCampo.APEDIDO = True Then
                    oCampo.APROCESO = False
                    oCampo.APEDIDO = False
               
            
                    Set oIBaseDatos = oCampo
                    udtTeserror = oIBaseDatos.FinalizarEdicionModificando
                    If udtTeserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError udtTeserror
                        g_Accion = ACCFormularioCons
                        Exit Sub
                    End If
                 End If
            Next
        End If

        sdbgCampos.RemoveAll
        AnyadirCampos g_oCampoDesglose.Desglose, True
        
        sdbgCampos.Bookmark = vBookmark
        If Me.Visible Then sdbgCampos.SetFocus

    End If
    
    g_Accion = ACCFormularioCons

    Screen.MousePointer = vbNormal
    
    Exit Sub
    
Cancelar:
    Screen.MousePointer = vbNormal
    Set m_oIBaseDatos = Nothing

End Sub

Private Sub cmdSubir_Click()
Dim i As Integer
Dim arrValoresMat() As Variant
Dim arrValoresArt() As Variant
Dim arrValoresDen() As Variant
Dim arrValoresAux() As Variant
Dim bOrdenoMat, bOrdenoPais As Boolean
Dim bOrdenoArt, bOrdenoProv As Boolean
Dim bOrdenoDen As Boolean
Dim bOrdenoCampoMat, bOrdenoCampoProv As Boolean
Dim vbm As Variant


    'si hay cambios los guarda
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
    End If
    
    If sdbgCampos.SelBookmarks.Count = 0 Then Exit Sub
    If sdbgCampos.AddItemRowIndex(sdbgCampos.SelBookmarks.Item(0)) = 0 Then Exit Sub
    
    
    ReDim arrValoresMat(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresArt(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresDen(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresAux(sdbgCampos.Columns.Count - 1)

    '''Ordenacion del Material // Pais
    If (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.material) Or (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Pais) Then
        vbm = sdbgCampos.GetBookmark(1)
         If (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.provincia) Or _
           (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.material And ((sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.CodArticulo) Or sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.NuevoCodArticulo)) Then
           
            If (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.provincia) Or _
            (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2))) <> tipocampogs.DenArticulo Then
                bOrdenoPais = True
                For i = 0 To sdbgCampos.Columns.Count - 1
                   arrValoresMat(i) = sdbgCampos.Columns(i).Value
                   arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(vbm)
                
                Next i
            Else
                bOrdenoMat = True
                For i = 0 To sdbgCampos.Columns.Count - 1
                    arrValoresMat(i) = sdbgCampos.Columns(i).Value
                    arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(vbm)
                    arrValoresDen(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                Next i
            End If
        Else
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
            Next i
           
        End If
    'Ordenacion del c�digo del art�culo // Provincia
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.CodArticulo Or _
        sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.NuevoCodArticulo Or _
        sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.provincia Then
        If val(sdbgCampos.Row) = 1 Then Exit Sub
        vbm = sdbgCampos.GetBookmark(-1)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.Pais Or sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.provincia Or sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) <> tipocampogs.DenArticulo Then
            bOrdenoProv = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).Value
            Next i
        ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.material Then
            bOrdenoArt = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).Value
                arrValoresDen(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(1))
            Next i
        End If

    'Ordenacion de la denominacion
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.DenArticulo) Then
        If val(sdbgCampos.Row) = 2 Then Exit Sub
        If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = tipocampogs.NuevoCodArticulo Then
            bOrdenoDen = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresDen(i) = sdbgCampos.Columns(i).Value
            Next i
        End If
    Else
        vbm = sdbgCampos.GetBookmark(-1)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.DenArticulo Or _
         sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.provincia Or _
         sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.CodArticulo Or _
         sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.NuevoCodArticulo Then
           If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = tipocampogs.DenArticulo Then
               bOrdenoCampoMat = True
           Else
               bOrdenoCampoProv = True
           End If
        End If
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
        Next i
       
         
    End If
    
    If bOrdenoMat Or bOrdenoArt Or bOrdenoDen Or bOrdenoPais Or bOrdenoProv Then
        'Ordeno conjuntamente los campos de material y art�culo
        sdbgCampos.MovePrevious
        If bOrdenoArt Or bOrdenoDen Or bOrdenoProv Then
            sdbgCampos.MovePrevious
        End If
        
        If bOrdenoDen Then
            sdbgCampos.MovePrevious
        End If
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        If Not bOrdenoPais And Not bOrdenoProv Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
        End If
        
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        
        sdbgCampos.SelBookmarks.RemoveAll
        
        
        If Not bOrdenoArt And Not bOrdenoDen And Not bOrdenoPais And Not bOrdenoProv Then
            sdbgCampos.MovePrevious
        End If
        If Not bOrdenoDen And Not bOrdenoProv Then
            sdbgCampos.MovePrevious
        End If
        sdbgCampos.MovePrevious
    ElseIf bOrdenoCampoMat Or bOrdenoCampoProv Then
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If Not bOrdenoCampoProv Then
            sdbgCampos.MovePrevious
        End If

        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i

        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresArt(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i

        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        
        If Not bOrdenoCampoProv Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
        End If

        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If Not bOrdenoCampoProv Then
            sdbgCampos.MovePrevious
        End If


    Else  'Ordeno normal,de 1 en 1
        
        'For i = 0 To sdbgCampos.Columns.Count - 1
            'arrValoresAux(i) = sdbgCampos.Columns(i).Value
        'Next i

        sdbgCampos.MovePrevious

        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i

        sdbgCampos.MoveNext

        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i

        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MovePrevious
        
        If sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = tipocampogs.Centro Then
            g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(1)))).EliminarCentroNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).Id
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = tipocampogs.Almacen Then
                g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(2)))).EliminarAlmacenNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(1)))).Id
            End If
        ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = tipocampogs.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Centro Then
            g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).EliminarCentroNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(-1)))).Id
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = tipocampogs.Almacen Then
                g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(1)))).EliminarAlmacenNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).Id
            End If
        ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = tipocampogs.Centro And sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Almacen Then
            g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).EliminarAlmacenNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(-1)))).Id
        Else
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = tipocampogs.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = tipocampogs.Centro Then
                g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(2)))).EliminarCentroNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(1)))).Id
                If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = tipocampogs.Almacen Then
                    g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(3)))).EliminarAlmacenNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(2)))).Id
                End If
            ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = tipocampogs.Centro And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = tipocampogs.Almacen Then
                g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(2)))).EliminarAlmacenNoRelacionado g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(1)))).Id
            End If
        End If
        
        
    End If
    sdbgCampos.SelBookmarks.Add sdbgCampos.Bookmark
 
    
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos
    
End Sub
''' <summary>
''' Carga de la pantalla.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
    Dim oIdioma As CIdioma
    Dim i As Integer
    Dim iPosition As Integer
    
    m_bOcultarSelCualquierMatPortal = False
    
    Set m_oTablaExterna = oFSGSRaiz.Generar_CTablaExterna
    'fin Tarea 811


    Me.Height = 6135
    Me.Width = 12195
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    PonerFieldSeparator Me
    
    CargarRecursos
    
    If g_sOrigen = "frmFormularios" Then
        Me.caption = g_oCampoDesglose.Grupo.Formulario.Den & "/" & g_oCampoDesglose.Grupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & "/" & g_oCampoDesglose.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " " & m_sIdiConf
        
        If g_oCampoDesglose.TipoPredef = NoConformidad Then
            Me.lblFecha.Visible = True
            Me.lblFecha.Left = 2880
            Me.sdbcFecha.Visible = True
            Me.sdbcFecha.Left = Me.lblFecha.Left + Me.lblFecha.Width + 20
            Me.chkPOPUP.Left = Me.sdbcFecha.Left + Me.sdbcFecha.Width + 300
            
            Me.picItems.Width = Me.picItems.Width + Me.lblFecha.Width + Me.sdbcFecha.Width + 320
        
            Me.sdbcFecha.Value = IIf(IsNull(g_oCampoDesglose.FechaAccion), -1, g_oCampoDesglose.FechaAccion)
            
            chkVincular.Visible = False
            cmdVincular.Visible = False
        ElseIf Not g_oCampoDesglose.TipoPredef = TipoCampoPredefinido.Certificado Then
            Me.chkPOPUP.Left = 3960
            
            Me.lblFecha.Visible = False
            Me.sdbcFecha.Visible = False
            
            chkVincular.Visible = True
            cmdVincular.Visible = True
            chkVincular.Enabled = Not g_oCampoDesglose.Grupo.Formulario.TieneSolicitudesEmitidas(g_oCampoDesglose.Id)
            cmdVincular.Enabled = False
            
            chkVincular.Value = vbUnchecked
            
            chkVincular.Left = Me.chkPOPUP.Left + Me.chkPOPUP.Width + 20
            cmdVincular.Left = Me.chkVincular.Left + Me.chkVincular.Width + 20
            
            Me.picItems.Width = Me.picItems.Width + Me.chkVincular.Width + Me.cmdVincular.Width + 320
            
            Me.chkPermitirExcel.Left = cmdVincular.Left + cmdVincular.Width + 50
        Else
            Me.chkPOPUP.Left = 3960
            
            Me.lblFecha.Visible = False
            Me.sdbcFecha.Visible = False
            
            chkVincular.Visible = False
            cmdVincular.Visible = False
        End If
        
        If g_oCampoDesglose.Vinculado Then
            chkVincular.Value = vbChecked
        Else
            chkVincular.Value = vbUnchecked
        End If
        
        Me.cmdVincular.Enabled = (chkVincular.Value = vbChecked)

        If g_oCampoDesglose.MostrarPOPUP Then
            chkPOPUP.Value = vbChecked
        Else
            chkPOPUP.Value = vbUnchecked
        End If
        
        If g_oCampoDesglose.PermitirExcel Then
            chkPermitirExcel.Value = vbChecked
        Else
            chkPermitirExcel.Value = vbUnchecked
        End If
                
        With sdbgCampos
            'Carga las columnas de los diferentes idiomas de la grid (la 1� ser� la del idioma de la aplicaci�n):
            Set m_oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
            i = .Columns.Count
            iPosition = 2
            
            .Columns.Add i
            .Columns(i).Name = gParametrosInstalacion.gIdioma
            If g_oCampoDesglose.Grupo.Formulario.Multiidioma Then
                .Columns(i).caption = m_oIdiomas.Item(CStr(gParametrosInstalacion.gIdioma)).Den
            Else
                .Columns(i).caption = m_sDato
            End If
            .Columns(i).Style = ssStyleEditButton
            .Columns(i).ButtonsAlways = True
            .Columns(i).Position = iPosition
            If Not g_bModif Then .Columns(i).Locked = True
            .Columns(i).FieldLen = 300
            
            For Each oIdioma In m_oIdiomas
                If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                    i = i + 1
                    iPosition = iPosition + 1
                    .Columns.Add i
                    .Columns(i).Name = oIdioma.Cod
                    .Columns(i).caption = oIdioma.Den
                    .Columns(i).Style = ssStyleEditButton
                    .Columns(i).ButtonsAlways = True
                    .Columns(i).Position = iPosition
                    .Columns(i).Visible = g_oCampoDesglose.Grupo.Formulario.Multiidioma
                    If Not g_bModif Then .Columns(i).Locked = True
                    .Columns(i).FieldLen = 300
                End If
            Next
        End With
    Else   'Viene de una instancia
        Me.caption = g_oCampoDesglose.Grupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & "/" & g_oCampoDesglose.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " " & m_sIdiConf
        
        With sdbgCampos
            .Columns("ATRIBUTO").Visible = False
            .Columns("APROCESO").Visible = False
            .Columns("APEDIDO").Visible = False
            
            'A�ade la columna de idioma
            i = .Columns.Count
            iPosition = 1
            
            .Columns.Add i
            .Columns(i).Name = gParametrosInstalacion.gIdioma
            .Columns(i).caption = m_sDato
            .Columns(i).Style = ssStyleEditButton
            .Columns(i).ButtonsAlways = True
            .Columns(i).Position = iPosition
            .Columns(i).Locked = True
        End With
    End If
    
    'Si tiene acceso b�sico al FSWS (solo solicitudes de compras) no ver� el bot�n para crear campos calculados:
    If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then cmdAnyaCalculo.Visible = False
    
    m_bArt = False
    m_bConEnviosCorrectos = False
    m_bConPiezasDefectuosas = False
    CargarCampos
    
    If Not g_bModif Then picItems.Visible = False
    
    g_Accion = ACCDesgloseCons
End Sub

''' <summary>
''' Carga las etiquetas de los diferentes controles
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:Form_load; Tiempo m�ximo:0</remarks>
Private Sub CargarRecursos()

    Dim Ador As Ador.Recordset
    Dim i As Integer

    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DESGLOSE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        cmdAnyaItem.ToolTipText = Ador(0).Value ' 1 A�adir campo
        Ador.MoveNext
        cmdElimItem.ToolTipText = Ador(0).Value ' 2 Eliminar campo
        Ador.MoveNext
        m_sDato = Ador(0).Value '3 Dato
        Ador.MoveNext
        sdbgCampos.Columns("AYUDA").caption = Ador(0).Value '4 Ayuda
        Ador.MoveNext
        m_sIdiConf = Ador(0).Value '5 Configuraci�n
        
        For i = 1 To 10
            Ador.MoveNext
            m_sCamposGS(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        chkPOPUP.caption = Ador(0).Value    '16 mostrar el desglose en una ventana emergente
                        
        Ador.MoveNext
        m_sCamposGS(12) = Ador(0).Value     '18 Denominacion de articulo
        
        Ador.MoveNext
        m_sCamposGS(11) = Ador(0).Value     '17 persona
        
        Ador.MoveNext
        m_sCamposGS(13) = Ador(0).Value     '19 numero solicitud ERP

        Ador.MoveNext
        m_sCamposGS(14) = Ador(0).Value     '20 Unidad Organizativa
        Ador.MoveNext
        m_sCamposGS(15) = Ador(0).Value     '21 Departamento
        Ador.MoveNext
        m_sCamposGS(16) = Ador(0).Value     '22 Organizaci�n de compras
        Ador.MoveNext
        m_sCamposGS(17) = Ador(0).Value     '23 Centro
        Ador.MoveNext
        m_sCamposGS(18) = Ador(0).Value     '24 Almac�n
                
        Ador.MoveNext
        sdbgCampos.Columns("ATRIBUTO").caption = Ador(0).Value '25 Atributo
        Ador.MoveNext
        sdbgCampos.Columns("APROCESO").caption = Ador(0).Value '26 A proceso
        Ador.MoveNext
        sdbgCampos.Columns("APEDIDO").caption = Ador(0).Value '27 A pedido
        Ador.MoveNext
        Me.lblFecha.caption = Ador(0).Value
        
        sdbcFecha.AddItem -1 & Chr(m_lSeparador) & ""
        For i = 0 To 16
            Ador.MoveNext
            If i <= 7 Then
                sdbcFecha.AddItem i & Chr(m_lSeparador) & Ador(0).Value
            ElseIf i >= 8 And i < 10 Then
                sdbcFecha.AddItem (7 * (i - 6)) & Chr(m_lSeparador) & Ador(0).Value
            ElseIf i >= 10 And i < 16 Then
                sdbcFecha.AddItem (30 * (i - 9)) & Chr(m_lSeparador) & Ador(0).Value
            ElseIf i = 16 Then
                sdbcFecha.AddItem 365 & Chr(m_lSeparador) & Ador(0).Value
            End If
        Next i
        
        Ador.MoveNext
        m_sCamposGS(19) = Ador(0).Value     '46 Centro de coste
        
        Ador.MoveNext
        m_sCamposGS(20) = Ador(0).Value  'Activo
        
        Ador.MoveNext
        m_sCamposGS(21) = Ador(0).Value  '48 Tipo de pedido
        
        Ador.MoveNext
        chkVincular.caption = Ador(0).Value    '49 Vincular con otras solicitudes
        
        Ador.MoveNext
        m_sCamposGS(22) = Ador(0).Value  '50 Factura
        
        Ador.MoveNext
        m_sCamposGS(23) = Ador(0).Value  '51 Unidad de pedido
        
        Ador.MoveNext
        m_sCamposGS(24) = Ador(0).Value  '52  "Proveedor ERP"
        
        Ador.MoveNext
        m_sImposibleAnyadircampo = Ador(0).Value
        
        Ador.MoveNext
        m_sNoPuedenExistirDosCampoMoneda = Ador(0).Value
        
        Ador.MoveNext
        m_sCamposGS(25) = Ador(0).Value  '55  "Empresa"
        Ador.MoveNext
        m_sCamposGS(26) = Ador(0).Value  'A�o de imputaci�n
        
        Ador.MoveNext
        chkPermitirExcel.caption = Ador(0).Value    '57 Permitir cargar l�neas desde EXCEL
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Resize()
Dim oIdioma As CIdioma
Dim dblWGrid As Double

    If Me.Height < 1500 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    If g_oCampoDesglose Is Nothing Then Exit Sub
    
    If g_bModif = True Then
        sdbgCampos.Height = Me.Height - 985
    Else
        sdbgCampos.Top = picItems.Top
        sdbgCampos.Height = Me.Height - 610
    End If
    
    sdbgCampos.Width = Me.Width - 230
    
    sdbgCampos.Columns("AYUDA").Width = sdbgCampos.Width / 12
        
    If g_sOrigen = "frmFormularios" Then
        dblWGrid = sdbgCampos.Width - sdbgCampos.Columns("ATRIBUTO").Width - sdbgCampos.Columns("APROCESO").Width - sdbgCampos.Columns("APEDIDO").Width - sdbgCampos.Columns("AYUDA").Width - 600

        If Not m_oIdiomas Is Nothing Then
            If g_oCampoDesglose.Grupo.Formulario.Multiidioma = True Then
                For Each oIdioma In m_oIdiomas
                    sdbgCampos.Columns(oIdioma.Cod).Width = dblWGrid / m_oIdiomas.Count
                Next
            Else
                sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Width = dblWGrid
            End If
        End If
    Else
        dblWGrid = sdbgCampos.Width - sdbgCampos.Columns("AYUDA").Width - 600
        
        If Not sdbgCampos.Columns(gParametrosInstalacion.gIdioma) Is Nothing Then
            sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Width = dblWGrid
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'Si hay cambios los guarda antes de perder el foco.
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
    End If
    
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If
    
    Set m_oIdiomas = Nothing
    Set m_oIBaseDatos = Nothing
    Set m_oCampoEnEdicion = Nothing
    Set g_oCampoDesglose = Nothing
    Set m_oTablaExterna = Nothing
    
    g_sOrigen = ""
    m_bExisteCodArticulo = False
    
    Me.Visible = False
End Sub

Private Sub sdbcFecha_CloseUp()
'jbg 31/03/09
'Descripci�n: Modifica la fecha limite de una acci�n
'Param. Entrada: -
'Param. Salida: -
'Llamadas: evento de sdbcFecha
'Tiempo: instantes

    g_oCampoDesglose.FechaAccion = IIf(Me.sdbcFecha.Columns(0).Value = -1, Null, Me.sdbcFecha.Columns(0).Value)
    g_oCampoDesglose.ModicarFechaAccion
    
End Sub

Private Sub sdbcFecha_InitColumnProps()
    sdbcFecha.DataFieldList = "Column 0"
    sdbcFecha.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">Texto del combo donde posicionarse</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbcFecha_PositionList(ByVal Text As String)

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcFecha.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbcFecha.Rows - 1
            bm = sdbcFecha.GetBookmark(i)
            If Trim(UCase(Text)) = UCase(Mid(sdbcFecha.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcFecha.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub



Private Sub sdbgCampos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgCampos_BeforeUpdate(Cancel As Integer)
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim oDenominaciones As CMultiidiomas
Dim oIdioma As CIdioma
    
    
    On Error Resume Next
    'es por cuando se cambian las denominaciones
    
    If m_oCampoEnEdicion Is Nothing Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    'REALIZA LAS COMPROBACIONES CORRESPONDIENTES ANTES DE GUARDAR:
    'La denominaci�n (del idioma de la instalaci�n) no puede ser nula
    If Trim(sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value) = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).caption
        Cancel = True
        Exit Sub
    End If
    
    'Rellenamos la colecci�n con los nuevos valores dependiendo del tipo:
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In m_oIdiomas
        If g_oCampoDesglose.Grupo.Formulario.Multiidioma = True Then
            oDenominaciones.Add oIdioma.Cod, Trim(sdbgCampos.Columns(CStr(oIdioma.Cod)).Value)
        Else
            oDenominaciones.Add oIdioma.Cod, Trim(sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value)
        End If
    Next
    Set m_oCampoEnEdicion.Denominaciones = oDenominaciones
        
    
    If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
        'ocampo.TipoPredef -->      FORM_CAMPO.TIPO
        'ocampo.Tipo        -->     FORM_CAMPO.SUBTIPO
        m_oCampoEnEdicion.TipoPredef = TipoCampoPredefinido.externo
        m_oCampoEnEdicion.TablaExterna = CInt(sdbgCampos.Columns(9).Value)
        Dim intTabla As Integer
        Dim strNombreTabla As String, strPK As String
        Dim vTipoDeDatoPK As TiposDeAtributos
        intTabla = CInt(sdbgCampos.Columns(9).Value)
        m_oCampoEnEdicion.TablaExterna = intTabla
        strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
        strPK = m_oTablaExterna.SacarPK(intTabla)
        vTipoDeDatoPK = m_oTablaExterna.SacarTipoDeDatoPK(strNombreTabla, strPK)
        Select Case vTipoDeDatoPK
            Case TipoFecha
                m_oCampoEnEdicion.Tipo = TipoFecha
                m_oCampoEnEdicion.valorFec = IIf(g_strPK <> "", CDate(g_strPK), Null)
                m_oCampoEnEdicion.valorBool = Null: m_oCampoEnEdicion.valorNum = Null: m_oCampoEnEdicion.valorText = Null
            Case TipoNumerico
                m_oCampoEnEdicion.Tipo = TipoNumerico
                m_oCampoEnEdicion.valorNum = IIf(g_strPK <> "", CInt(g_strPK), Null)
                m_oCampoEnEdicion.valorBool = Null: m_oCampoEnEdicion.valorFec = Null: m_oCampoEnEdicion.valorText = Null
            Case TipoTextoMedio
                m_oCampoEnEdicion.Tipo = TipoTextoMedio
                m_oCampoEnEdicion.valorText = g_strPK
                m_oCampoEnEdicion.valorBool = Null: m_oCampoEnEdicion.valorFec = Null: m_oCampoEnEdicion.valorNum = Null
        End Select
        
    End If
    
    If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
        '??????
    Else
        If CamposSistemaAProceso(sdbgCampos.Columns("CAMPO_GS").Value) Then
            m_oCampoEnEdicion.APROCESO = False
            m_oCampoEnEdicion.APEDIDO = False
        Else
            m_oCampoEnEdicion.APROCESO = GridCheckToBoolean(sdbgCampos.Columns("APROCESO").Value)
            m_oCampoEnEdicion.APEDIDO = GridCheckToBoolean(sdbgCampos.Columns("APEDIDO").Value)
        End If
    End If
    
    Set oIBaseDatos = m_oCampoEnEdicion
    teserror = oIBaseDatos.FinalizarEdicionModificando
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        sdbgCampos.CancelUpdate
        If Me.Visible Then sdbgCampos.SetFocus
        g_Accion = ACCDesgloseCons
        sdbgCampos.DataChanged = False

    Else
        ''' Registro de acciones
        basSeguridad.RegistrarAccion accionessummit.ACCDesgloseCampoModif, "Id" & sdbgCampos.Columns("ID").Value
        g_Accion = ACCDesgloseCons
        
        Set oIBaseDatos = Nothing
        Set m_oCampoEnEdicion = Nothing
    End If
        
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbgCampos_BtnClick()
'jbg 31/03/09
'Descripci�n: Seg�n donde pulses, te muestra la ayuda � el detalle de un campo.
'Param. Entrada: -
'Param. Salida: -
'Llamadas: evento de sdbgCampos
'Tiempo: instantes

Dim oCampo As CFormItem

    If sdbgCampos.col < 0 Then Exit Sub

    If g_oCampoDesglose Is Nothing Then Exit Sub

    'Si hay cambios los guarda antes de perder el foco.
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
    End If
    
    Select Case sdbgCampos.Columns(sdbgCampos.col).Name

        Case "AYUDA"
            'Muestra el formulario con la ayuda:
            Dim udtTipoAyuda As FormCampoAyudaTipoAyuda
                            
            If g_sOrigen = "frmFormularios" Then
                udtTipoAyuda = CampoFormulario
            Else
                udtTipoAyuda = CampoInstancia
            End If
            
            MostrarFormFormCampoAyuda oGestorIdiomas, gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, oGestorSeguridad, oUsuarioSummit.Cod, gParametrosGenerales, _
                udtTipoAyuda, g_bModif, g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)), m_oIdiomas

        Case Else
            'Detalle de campo:
            If sdbgCampos.Columns("CAMPO_GS").Value <> "" _
            And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoNoConformidad.Subtipo _
            And sdbgCampos.Columns("TIPO").Value <> TipoCampoPredefinido.Calculado Then
                If g_oCampoDesglose.Grupo.Formulario Is Nothing Then
                    'frmDetalleCampoPredef.g_sDenCampo = g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                    frmDetalleCampoPredef.g_sDenCampo = ObtenerNombreCampo(g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).CampoGS, g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef, g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo, False, g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).PRES5)
                Else
                    frmDetalleCampoPredef.g_sDenCampo = ObtenerNombreCampo(g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).CampoGS, g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef, g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo, g_oCampoDesglose.Grupo.Formulario.Multiidioma, g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).PRES5)
                End If
                If sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.material And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = tipocampogs.NuevoCodArticulo Then
                    m_bOcultarSelCualquierMatPortal = True
                Else
                    m_bOcultarSelCualquierMatPortal = False
                End If
                frmDetalleCampoPredef.g_bOcultarSelCualquierMatPortal = m_bOcultarSelCualquierMatPortal
                frmDetalleCampoPredef.g_iTipo = g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo
                frmDetalleCampoPredef.g_iTipoCampo = sdbgCampos.Columns("CAMPO_GS").Value
                frmDetalleCampoPredef.g_iTipoSolicit = sdbgCampos.Columns("TIPO").Value
                frmDetalleCampoPredef.g_lCampoId = g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).Id
                frmDetalleCampoPredef.g_lCampoPadre = g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).CampoPadre.Id
                If Me.g_sOrigen = "frmSolicitudDetalle" Then
                    frmDetalleCampoPredef.g_sOrigen = "frmSolicitudDetalleDesglose"
                Else
                    frmDetalleCampoPredef.g_sOrigen = "frmDesglose"
                End If
                frmDetalleCampoPredef.Show vbModal

            
            ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
                'Muestra la pantalla frmDetalleCampoExterno:
                m_intTabla = CInt(sdbgCampos.Columns(9).Value)
                
                MostrarFormDetalleCampoExterno oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, m_oTablaExterna.SacarDenominacionTabla(m_intTabla), sdbgCampos.Columns("SUBTIPO").Value
            
            ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Servicio Then
                'Muestra la pantalla frmServicio:
                If g_sOrigen = "frmFormularios" Then
                    frmServicio.lIdFormulario = frmFormularios.g_oFormSeleccionado.Id
                    frmServicio.lGrupoFormulario = frmFormularios.g_oGrupoSeleccionado.Id
                    frmServicio.g_multiidioma = frmFormularios.g_oFormSeleccionado.Multiidioma
                    frmServicio.lCampoForm = CLng(sdbgCampos.Columns("ID").Value)
                    Set frmServicio.g_oIdiomas = frmFormularios.m_oIdiomas
                ElseIf g_sOrigen = "frmSolicitudDetalle" Then
                    'Muestra la pantalla frmServicio pero sin opci�n a editar el contenido:
                    frmServicio.g_multiidioma = False
                    frmServicio.lCampoForm = g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).campoOrigen
                    frmServicio.g_Instancia = g_Instancia
                End If
                frmServicio.g_sOrigen = "frmDesglose"
                frmServicio.iServAntiguo = g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).Servicio
                frmServicio.Show vbModal

            ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                frmFormularioDesgloseCalculo.g_bModif = False
                frmFormularioDesgloseCalculo.g_sOrigen = "frmDesglose"
                Set oCampo = oFSGSRaiz.Generar_CFormCampo
                If g_sOrigen = "frmFormularios" Then
                    oCampo.Id = g_oCampoDesglose.Id
                Else
                    oCampo.IdCampoDef = g_oCampoDesglose.IdCampoDef
                End If
                Set oCampo.Grupo = g_oCampoDesglose.Grupo
                Set oCampo.Denominaciones = g_oCampoDesglose.Denominaciones
                Set frmFormularioDesgloseCalculo.g_oCampoDesglose = oCampo
                frmFormularioDesgloseCalculo.Show vbModal
                Set oCampo = Nothing
                
            Else
                If g_sOrigen = "frmFormularios" Then
                    frmDetalleCampos.g_sOrigen = "frmDesglose"
                Else
                    frmDetalleCampos.g_sOrigen = "frmSolicitudDetalle"
                End If
                frmDetalleCampos.g_bModif = g_bModif
                Set frmDetalleCampos.g_oIdiomas = m_oIdiomas
                Set frmDetalleCampos.g_oCampo = g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value))
                frmDetalleCampos.Show vbModal
            End If
    End Select
End Sub

''' <summary>
''' Comprueba si el campo se puede marcar para su env�o a proceso o a pedido
''' </summary>
''' <remarks>Es un evento, no se llama desde ning�n sitio. tiempo m�ximo:1 seg</remarks
Private Sub sdbgCampos_Change()
    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    If g_oCampoDesglose Is Nothing Then Exit Sub
    
    DoEvents
    
    Set m_oCampoEnEdicion = Nothing

    If g_oCampoDesglose.Desglose Is Nothing Then Exit Sub
    If g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)) Is Nothing Then Exit Sub
    
    Set m_oCampoEnEdicion = g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value))
        
    g_Accion = ACCDesgloseCampoModif
    
    Select Case sdbgCampos.Columns(sdbgCampos.col).Name
    
        Case "APROCESO"
            If m_bExisteCodArticulo Then
                If CamposSistemaAProceso(sdbgCampos.Columns("CAMPO_GS").Value) Then
                    sdbgCampos.Columns("APROCESO").Value = True
                    
                ElseIf sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoArchivo Or _
                    sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo Then
                    
                ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Atributo Then
                    If GridCheckToBoolean(sdbgCampos.Columns("APROCESO").Value) = False Then
                        If GridCheckToBoolean(sdbgCampos.Columns("APEDIDO").Value) = True Then
                            sdbgCampos.Columns("APEDIDO").Value = False
                        End If
                    Else
                        'Comprobaremos que no haya en este �mbito el mismo atributo con marca de "a proceso"
                        If ExisteAtributoAProceso Then
                            sdbgCampos.Columns("APROCESO").Value = False
                            oMensajes.MensajeOKOnly 943, TipoIconoMensaje.Information
                        End If
                    End If
                Else
                    sdbgCampos.Columns("APROCESO").Value = False
                End If
            Else
                sdbgCampos.Columns("APROCESO").Value = False
            End If
            sdbgCampos.Update
            
        Case "APEDIDO"
            If Not m_bExisteCodArticulo Then
                If Not (sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.CampoGS And sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.ProveedorERP) Then
                    sdbgCampos.Columns("APEDIDO").Value = False
                End If
            Else
                If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Atributo Then
                    If GridCheckToBoolean(sdbgCampos.Columns("APEDIDO").Value) = True Then
                        If gParametrosGenerales.gbPedidosDirectos = False Then
                            sdbgCampos.Columns("APEDIDO").Value = False
                            Exit Sub
                        Else
                            If GridCheckToBoolean(sdbgCampos.Columns("APROCESO").Value) = False Then
                                'Comprobaremos que no haya en este �mbito el mismo atributo con marca de "a proceso"
                                If ExisteAtributoAProceso Then
                                    sdbgCampos.Columns("APROCESO").Value = False
                                    sdbgCampos.Columns("APEDIDO").Value = False
                                    oMensajes.MensajeOKOnly 943, TipoIconoMensaje.Information
                                Else
                                    If m_oCampoEnEdicion.ComprobarPermitirMarcarAtributoAPedido Then
                                        sdbgCampos.Columns("APROCESO").Value = True
                                    Else
                                        sdbgCampos.Columns("APEDIDO").Value = False
                                        oMensajes.MensajeOKOnly 950, TipoIconoMensaje.Information
                                    End If
                                End If
                            Else
                                If m_oCampoEnEdicion.ComprobarPermitirMarcarAtributoAPedido Then
                                Else
                                    sdbgCampos.Columns("APEDIDO").Value = False
                                    oMensajes.MensajeOKOnly 950, TipoIconoMensaje.Information
                                End If
                            End If
                        End If
                    End If
                Else
                    If Not (sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.CampoGS And (sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Activo Or sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.CentroCoste Or sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.PartidaPresupuestaria Or sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Almacen Or sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.TipoPedido Or sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.Factura Or sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.UnidadPedido Or sdbgCampos.Columns("CAMPO_GS").Value = tipocampogs.ProveedorERP)) Then
                        sdbgCampos.Columns("APEDIDO").Value = False
                    End If
                End If
            End If
            
            sdbgCampos.Update
    End Select
        
    
End Sub

Private Sub sdbgCampos_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        If sdbgCampos.DataChanged = False Then
            sdbgCampos.CancelUpdate
            sdbgCampos.DataChanged = False
            
            Set m_oCampoEnEdicion = Nothing

            g_Accion = ACCDesgloseCons
        End If
    End If
End Sub

''' <summary>
''' Muestra el menu contextual de Marcar como piezas defectuosas/Marcar como envios correctos/Desmarcar como ...
''' cuando pinchemos en un campo de tipo numerico o calculado (para marcar) o si pinchamos en un campo de tipo QA
''' piezas defectuosas o envios correctos (para desmarcar)
''' </summary>
''' <param name="button">que boton del raton hemos pulsado</param>
''' <param name="shift">accion del shift</param>
''' <param name="X">posicion X</param>
''' <param name="Y">posicion Y</param>
''' <remarks>Llamada desde=propio formulario; Tiempo m�ximo=0seg.</remarks>
Private Sub sdbgCampos_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim iIndex As Integer
    Dim NEnabled As Integer
 
    If Button = 2 Then
        iIndex = sdbgCampos.RowContaining(Y)
        If iIndex > -1 Then
            sdbgCampos.Row = iIndex
            
            If sdbgCampos.Columns("CAMPO_GS").Value <> tipocampogs.Desglose And sdbgCampos.Columns("SUBTIPO").Value <> TiposDeAtributos.TipoDesglose _
               And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoSC.ArchivoEspecific And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoSC.importe _
               And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoSC.PrecioUnitario And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoSC.Cantidad _
               And sdbgCampos.Columns("CAMPO_GS").Value <> tipocampogs.Pres1 And sdbgCampos.Columns("CAMPO_GS").Value <> tipocampogs.Pres2 _
               And sdbgCampos.Columns("CAMPO_GS").Value <> tipocampogs.Pres3 And sdbgCampos.Columns("CAMPO_GS").Value <> tipocampogs.Pres4 _
               And sdbgCampos.Columns("CAMPO_GS").Value <> tipocampogs.FormaPago And sdbgCampos.Columns("CAMPO_GS").Value <> tipocampogs.Unidad _
               And sdbgCampos.Columns("CAMPO_GS").Value <> tipocampogs.UnidadOrganizativa _
               And sdbgCampos.Columns("SUBTIPO").Value <> TiposDeAtributos.TipoArchivo And sdbgCampos.Columns("SUBTIPO").Value <> TiposDeAtributos.TipoBoolean Then
               
               If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoNumerico And sdbgCampos.Columns("CAMPO_GS").Value <> tipocampogs.Almacen Then
                    If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoNumerico Then
                        If sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoNoConformidad.EnviosCorrectos And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoNoConformidad.PiezasDefectuosas And sdbgCampos.Columns("CAMPO_GS").Value = "" Then
                            MDI.mnuPopUpFSQAVarCalDesg.Item(1).Enabled = True
                            MDI.mnuPopUpFSQAVarCalDesg.Item(2).Enabled = True
                            MDI.mnuPopUpFSQAVarCalDesg.Item(3).Enabled = False
                            MDI.mnuPopUpFSQAVarCalDesg.Item(4).Enabled = False
                            NEnabled = 2
                            If m_bArt Then
                                If m_bConPiezasDefectuosas Then
                                    MDI.mnuPopUpFSQAVarCalDesg.Item(1).Enabled = False
                                    NEnabled = NEnabled - 1
                                End If
                                If m_bConEnviosCorrectos Then
                                    MDI.mnuPopUpFSQAVarCalDesg.Item(2).Enabled = False
                                    NEnabled = NEnabled - 1
                                End If
                            Else
                                MDI.mnuPopUpFSQAVarCalDesg.Item(1).Enabled = False
                                MDI.mnuPopUpFSQAVarCalDesg.Item(2).Enabled = False
                                NEnabled = NEnabled - 2
                            End If

                            If NEnabled > 0 Then PopupMenu MDI.mnuPopUpFSQAVarCalDesgCab
                        Else
                            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.EnviosCorrectos Then
                                MDI.mnuPopUpFSQAVarCalDesg.Item(1).Enabled = False
                                MDI.mnuPopUpFSQAVarCalDesg.Item(2).Enabled = False
                                MDI.mnuPopUpFSQAVarCalDesg.Item(3).Enabled = False
                                MDI.mnuPopUpFSQAVarCalDesg.Item(4).Enabled = True
    
                                PopupMenu MDI.mnuPopUpFSQAVarCalDesgCab
                            ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.PiezasDefectuosas Then
                                MDI.mnuPopUpFSQAVarCalDesg.Item(1).Enabled = False
                                MDI.mnuPopUpFSQAVarCalDesg.Item(2).Enabled = False
                                MDI.mnuPopUpFSQAVarCalDesg.Item(3).Enabled = True
                                MDI.mnuPopUpFSQAVarCalDesg.Item(4).Enabled = False
    
                                PopupMenu MDI.mnuPopUpFSQAVarCalDesgCab
                            End If
                        End If
                    End If
               End If
            End If
        End If
    End If

End Sub

Private Sub sdbgCampos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim oIdioma As CIdioma

    If sdbgCampos.col < 0 Then Exit Sub

    If g_bModif = True Then
        If g_oCampoDesglose.Grupo.Formulario.Multiidioma = True Then
            For Each oIdioma In m_oIdiomas
                sdbgCampos.Columns(CStr(oIdioma.Cod)).Locked = False
            Next
        Else
            sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Locked = False
        End If
        
        
        If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
            sdbgCampos.Columns(11).Style = ssStyleEditButton    'necesario???
        End If
        
                
        If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Atributo Then
            If g_oCampoDesglose.Grupo.Formulario.Multiidioma = True Then
                For Each oIdioma In m_oIdiomas
                    sdbgCampos.Columns(CStr(oIdioma.Cod)).Locked = True
                Next
            Else
                sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Locked = True
            End If
        ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.FechaImputacionDefecto Then
            sdbgCampos.Columns("VALOR").Locked = True
        End If
    End If
    
End Sub

''' <summary>
''' Estilo que se le va a dar a la fila cuando se cargue.
''' </summary>
''' <returns>nada</returns>
''' <remarks>Llamada desde=propio formulario; Tiempo m�ximo=0seg.</remarks>
Private Sub sdbgCampos_RowLoaded(ByVal Bookmark As Variant)
    Dim oIdioma As CIdioma

    If g_sOrigen = "frmFormularios" Then
        If sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
            For Each oIdioma In m_oIdiomas
                sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "Amarillo"
            Next
        ElseIf sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
            For Each oIdioma In m_oIdiomas
                sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "Calculado"
            Next
        Else
            For Each oIdioma In m_oIdiomas
                sdbgCampos.Columns(oIdioma.Cod).CellStyleSet ""
            Next
        End If
        
    Else  'viene de una instancia de solicitud
        If sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
            sdbgCampos.Columns(gParametrosInstalacion.gIdioma).CellStyleSet "Amarillo"
        ElseIf sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
            sdbgCampos.Columns(gParametrosInstalacion.gIdioma).CellStyleSet "Calculado"
        Else
            sdbgCampos.Columns(gParametrosInstalacion.gIdioma).CellStyleSet ""
        End If
        
    End If
    
    'Si no existe un campo Art�culo en el desglose, pondremos las columnas "A Proceso" y "A Pedido" a 0
    If Not m_bExisteCodArticulo Then
        Select Case sdbgCampos.Columns("CAMPO_GS").CellValue(Bookmark)
            Case tipocampogs.ProveedorERP
                sdbgCampos.Columns("APEDIDO").CellStyleSet ""
                sdbgCampos.Columns("APROCESO").CellStyleSet "Gris"
            Case Else
                sdbgCampos.Columns("APEDIDO").CellStyleSet "Gris"
                sdbgCampos.Columns("APROCESO").CellStyleSet "Gris"
        End Select
        
        If sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Atributo Then
            sdbgCampos.Columns("ATRIBUTO").CellStyleSet "Azul"
            
            If g_sOrigen = "frmFormularios" Then
                For Each oIdioma In m_oIdiomas
                    sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "Azul"
                Next
            Else
                sdbgCampos.Columns(gParametrosInstalacion.gIdioma).CellStyleSet "Azul"
            End If
        End If

    Else
        
        '(tablas externas) de hacer algo iria aqui
        
        If sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Atributo Then
            sdbgCampos.Columns("ATRIBUTO").CellStyleSet "Azul"
            sdbgCampos.Columns("APROCESO").CellStyleSet ""
            sdbgCampos.Columns("APEDIDO").CellStyleSet ""
            
            If g_sOrigen = "frmFormularios" Then
                For Each oIdioma In m_oIdiomas
                    sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "Azul"
                Next
            Else
                sdbgCampos.Columns(gParametrosInstalacion.gIdioma).CellStyleSet "Azul"
            End If
        ElseIf CamposSistemaAProceso(sdbgCampos.Columns("CAMPO_GS").CellValue(Bookmark)) Or _
            sdbgCampos.Columns("SUBTIPO").CellValue(Bookmark) = TiposDeAtributos.TipoTextoLargo Or _
            sdbgCampos.Columns("SUBTIPO").CellValue(Bookmark) = TiposDeAtributos.TipoArchivo Then
            sdbgCampos.Columns("APROCESO").CellStyleSet ""
            sdbgCampos.Columns("APEDIDO").CellStyleSet "Gris"
        Else
            sdbgCampos.Columns("APROCESO").CellStyleSet "Gris"
            Select Case sdbgCampos.Columns("CAMPO_GS").CellValue(Bookmark)
                Case tipocampogs.Activo, tipocampogs.CentroCoste, tipocampogs.PartidaPresupuestaria, tipocampogs.Almacen, tipocampogs.TipoPedido, tipocampogs.Factura, tipocampogs.UnidadPedido, tipocampogs.ProveedorERP
                    sdbgCampos.Columns("APEDIDO").CellStyleSet ""
                Case Else
                    sdbgCampos.Columns("APEDIDO").CellStyleSet "Gris"
            End Select
        End If
    End If
    
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.EnviosCorrectos Then
        If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
            If Not g_oCampoDesglose.Grupo.Formulario.Multiidioma Then
                sdbgCampos.Columns(gParametrosInstalacion.gIdioma).CellStyleSet "EnviosCorrectosSumatorio" & gParametrosInstalacion.gIdioma
            Else
                For Each oIdioma In m_oIdiomas
                   sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "ECSumatorio" & oIdioma.Cod
                Next
                
            End If
        Else
            If Not g_oCampoDesglose.Grupo.Formulario.Multiidioma Then
                sdbgCampos.Columns(gParametrosInstalacion.gIdioma).CellStyleSet "EnviosCorrectos" & gParametrosInstalacion.gIdioma
            Else
                For Each oIdioma In m_oIdiomas
                   sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "EC" & oIdioma.Cod
                Next
                
            End If
        End If
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.PiezasDefectuosas Then
        If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
            If Not g_oCampoDesglose.Grupo.Formulario.Multiidioma Then
                sdbgCampos.Columns(gParametrosInstalacion.gIdioma).CellStyleSet "PiezasDefectuosasSumatorio" & gParametrosInstalacion.gIdioma
            Else
                For Each oIdioma In m_oIdiomas
                   sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "PDSumatorio" & oIdioma.Cod
                Next
                
            End If
        Else
            If Not g_oCampoDesglose.Grupo.Formulario.Multiidioma Then
                sdbgCampos.Columns(gParametrosInstalacion.gIdioma).CellStyleSet "PiezasDefectuosas" & gParametrosInstalacion.gIdioma
            Else
                For Each oIdioma In m_oIdiomas
                   sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "PD" & oIdioma.Cod
                Next
            End If
        End If
    End If


End Sub

Private Sub GuardarOrdenCampos()
    Dim oCampos As CFormItems
    Dim i As Integer
    Dim teserror As TipoErrorSummit
    Dim vbm As Variant
    Dim oCampo As CFormItem
    
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:

    Set oCampos = oFSGSRaiz.Generar_CFormCampos

    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
        
        If Not g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))) Is Nothing Then
            Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm)))
            If NullToDbl0(oCampo.Orden) <> i + 1 Then
                oCampo.Orden = i + 1
                oCampos.Add oCampo.Id, g_oCampoDesglose.Grupo, oCampo.Denominaciones, , , , , , , , , , , oCampo.Orden
            End If
            Set oCampo = Nothing
        End If
    Next i

    'Guarda en BD solo el orden de los atributos que han cambiado
    teserror = oCampos.GuardarOrdenCampos

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    Set oCampos = Nothing
End Sub

''' <summary>
''' A�ade los campos de la coleccion a la grid
''' </summary>
''' <param name="oCampos">coleccion de campos</param>
''' <param name="bCargarCol">Si tiene que a�adirlos a la coleccion</param>
''' <returns>nada</returns>
''' <remarks>Llamada desde=AnyadirCampoExterno,AnyadirCampoGS,GrupoSeleccionado; Tiempo m�ximo=0,3seg.</remarks>
Public Sub AnyadirCampos(ByVal oCampos As CFormItems, Optional ByVal bCargarCol As Boolean = False)
    Dim oCampo As CFormItem
    Dim oCampo2 As CFormItem
    Dim sCadena As String
    Dim oIdioma As CIdioma
    Dim vAProceso As Variant
    Dim vAPedido As Variant
    Dim intTabla As Integer
    
    
    If oCampos Is Nothing Then Exit Sub
        
    For Each oCampo In oCampos
        If Not bCargarCol Then
            'Lo a�ade a la colecci�n:
            Set oCampo2 = g_oCampoDesglose.Desglose.Add(oCampo.Id, g_oCampoDesglose.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , g_oCampoDesglose, oCampo.EsSubCampo, , , , , , , , , , , , , , , , , , oCampo.MaxLength, oCampo.CodAtrib, oCampo.DenAtrib, oCampo.APROCESO, oCampo.APEDIDO, oCampo.TablaExterna, oCampo.PRES5, , , , oCampo.RecordarValores, , , , oCampo.Servicio)
            oCampo2.NoFecAntSis = oCampo.NoFecAntSis
            
        Else
            'Miramos si existe un campo art�culo
            m_bExisteCodArticulo = False
            If g_oCampoDesglose.Desglose.Count = 0 Then
                If oCampos.Count > 0 Then
                    m_bExisteCodArticulo = ExisteCodArticulo(oCampos)
                End If
            Else
                m_bExisteCodArticulo = ExisteCodArticulo(g_oCampoDesglose.Desglose)
                If m_bExisteCodArticulo = False Then
                    If oCampos.Count > 0 Then
                        m_bExisteCodArticulo = ExisteCodArticulo(oCampos)
                    End If
                End If
            End If
        
            If Not m_bExisteCodArticulo Then
                vAProceso = 0
                vAPedido = 0
            Else
                If CamposSistemaAProceso(oCampo.CampoGS) Then
                    vAProceso = 1
                Else
                    vAProceso = oCampo.APROCESO
                End If
                vAPedido = oCampo.APEDIDO
            End If
            
            sCadena = oCampo.Id & Chr(m_lSeparador) & oCampo.CodAtrib & Chr(m_lSeparador) & BooleanToSQLBinary(vAProceso) & Chr(m_lSeparador) & BooleanToSQLBinary(vAPedido) & Chr(m_lSeparador) & " " & Chr(m_lSeparador) & oCampo.TipoPredef & Chr(m_lSeparador) & oCampo.Tipo & Chr(m_lSeparador) & oCampo.CampoGS & Chr(m_lSeparador) & oCampo.idAtrib
            
            
            If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
                intTabla = CInt(NullToStr(oCampo.TablaExterna))
                sCadena = sCadena & Chr(m_lSeparador) & intTabla
            Else
                sCadena = sCadena & Chr(m_lSeparador) & "0"
            End If
            
            
            'Denominaci�n del campo en los diferentes idiomas:
            sCadena = sCadena & Chr(m_lSeparador) & oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
            
            If g_sOrigen = "frmFormularios" Then
                For Each oIdioma In m_oIdiomas
                    If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                        sCadena = sCadena & Chr(m_lSeparador) & oCampo.Denominaciones.Item(CStr(oIdioma.Cod)).Den
                    End If
                Next
            End If
            
            If oCampo.CampoGS = TipoCampoNoConformidad.EnviosCorrectos Then
                m_bConEnviosCorrectos = True
            ElseIf oCampo.CampoGS = TipoCampoNoConformidad.PiezasDefectuosas Then
                m_bConPiezasDefectuosas = True
            ElseIf oCampo.CampoGS = tipocampogs.NuevoCodArticulo Then
                m_bArt = True
            End If
            
            
            'a�ade la cadena a la grid:
            sdbgCampos.AddItem sCadena
        End If
    Next
    
    If Not bCargarCol Then
        sdbgCampos.RemoveAll
        AnyadirCampos g_oCampoDesglose.Desglose, True
    End If

End Sub

Private Sub CargarCampos()
    If g_oCampoDesglose Is Nothing Then Exit Sub
    
    g_oCampoDesglose.CargarDesglose
    
    AnyadirCampos g_oCampoDesglose.Desglose, True
    
    sdbgCampos.MoveFirst

End Sub

Public Sub AnyadirCampoPredefinido(ByVal lTipo As Long)
    'Llama a la pantalla para a�adir campos predefinidos de un tipo de solicitud en concreto
    g_Accion = ACCDesgloseCampoAnyadir
    
    frmCamposSolic.g_lTipoCampoPredef = lTipo
    frmCamposSolic.g_sOrigen = "frmDesglose"
    Set frmCamposSolic.g_oDesglose = g_oCampoDesglose
    frmCamposSolic.Show vbModal
    
End Sub


Public Sub AnyadirCampoAtributoGS()
    'Tipo de atributo de GS

    g_Accion = ACCDesgloseCampoAnyadir
    
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If

    Set g_ofrmATRIB = New frmAtrib
    g_ofrmATRIB.g_sOrigen = "frmDesglose"

    Screen.MousePointer = vbHourglass
    MDI.MostrarFormulario g_ofrmATRIB
    Screen.MousePointer = vbNormal
    
End Sub

Public Sub AnyadirCampoNuevo()
    'LLama a la pantalla para crear un campo nuevo:
    g_Accion = ACCDesgloseCampoAnyadir
    
    frmFormAnyaCampos.g_sOrigen = "frmDesglose"
    Set frmFormAnyaCampos.g_oIdiomas = m_oIdiomas
    frmFormAnyaCampos.Show vbModal
End Sub


Public Sub AnyadirCampoGS(ByVal iTipoCampoGS As Integer, Optional ByVal sPRES5 As String)
Dim oCampos As CFormItems
Dim oCampo As CFormItem
Dim udtTeserror As TipoErrorSummit

    'A�ade un campo del sistema al campo desglose seleccionado:
    g_Accion = ACCDesgloseCampoAnyadir
    
    Set oCampos = oFSGSRaiz.Generar_CFormCampos
    
    If iTipoCampoGS = tipocampogs.provincia Then
        Set oCampo = GenerarEstructuraCampoSistema(tipocampogs.Pais)
        oCampos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , oCampo.CampoPadre, oCampo.EsSubCampo
    
        Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS)
        oCampos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , oCampo.CampoPadre, oCampo.EsSubCampo
    ElseIf iTipoCampoGS = tipocampogs.NuevoCodArticulo Then
        Set oCampo = GenerarEstructuraCampoSistema(tipocampogs.material)
        oCampos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , oCampo.CampoPadre, oCampo.EsSubCampo
         
        Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS)
        oCampos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , oCampo.CampoPadre, oCampo.EsSubCampo
         
        Set oCampo = GenerarEstructuraCampoSistema(tipocampogs.DenArticulo)
        oCampos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , oCampo.CampoPadre, oCampo.EsSubCampo
    ElseIf iTipoCampoGS = tipocampogs.PartidaPresupuestaria Then
        Set oCampo = GenerarEstructuraArbolPresupuestario(iTipoCampoGS, sPRES5, False)
        oCampos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , oCampo.CampoPadre, oCampo.EsSubCampo, , , , , , , , , , , , , , , , , , , , , , , , oCampo.PRES5
    ElseIf iTipoCampoGS = tipocampogs.ProveedorERP Then
        'En ese grupo deberia existir el campo de sistema Proveedor
        Dim bCampoGSProveedorEncontrado As Boolean
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.CampoGS = tipocampogs.Proveedor Then
                bCampoGSProveedorEncontrado = True
                Exit For
            End If
        Next
        'Si en el desglose no esta el campo de sistema Proveedor,sacamos un mensaje
        If bCampoGSProveedorEncontrado = False Then
            oMensajes.NoExisteCampoGsProveedor
            Exit Sub
        Else
            If ComprobarOrgCompras Then
                Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS)
                oCampos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , oCampo.CampoPadre, oCampo.EsSubCampo
            Else
                oMensajes.NoExisteCampoGsOrgCompras
                Exit Sub
            End If
        End If
    ElseIf iTipoCampoGS = tipocampogs.Moneda Then
            ' Tarea 2918 -- No podemos a�adir m�s de un campo moneda por nivel
            Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS)
            oCampos.addCampo oCampo, oCampo.Grupo
    ElseIf iTipoCampoGS = tipocampogs.AnyoImputacion Then
        Set oCampo = GenerarEstructuraCampoSistema(tipocampogs.AnyoImputacion)
        oCampos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , oCampo.CampoPadre, oCampo.EsSubCampo
    Else
        Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS)
        oCampos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , oCampo.CampoPadre, oCampo.EsSubCampo
    End If
        
    
    'A�ade el campo a BD:
    udtTeserror = oCampos.AnyadirCamposDeGSoAtributos(False)
    If udtTeserror.NumError = TESnoerror Then
        'Lo a�ade a la colecci�n y grid de formularios
        AnyadirCampos oCampos
        RegistrarAccion ACCDesgloseCampoAnyadir, "Id:" & oCampo.Id
        
    Else
        Screen.MousePointer = vbNormal
        TratarError udtTeserror
        Exit Sub
    End If
    
    Set oCampos = Nothing
    Set oCampo = Nothing
    
    g_Accion = ACCDesgloseCons
    
End Sub

'''
Public Sub AnyadirCampoExterno(ByVal pIdTabla As Integer)
Dim oCampos As CFormItems
Dim oCampo As CFormItem
Dim udtTeserror As TipoErrorSummit

Dim strTipoDeDatoPK As TiposDeAtributos
Dim strPrimerCampoNoPK As String
Dim strNombreTabla As String, strPK As String, strDenoTabla As String

    '**********************************************************************
    m_blnExisteTablaExternaParaArticulo = IIf(m_oTablaExterna.SacarTablaTieneART(pIdTabla), True, False)
    strNombreTabla = m_oTablaExterna.SacarNombreTabla(pIdTabla)
    strDenoTabla = m_oTablaExterna.SacarDenominacionTabla(pIdTabla)
    strPrimerCampoNoPK = m_oTablaExterna.SacarPrimerCampoDistinto(pIdTabla)
    strPK = m_oTablaExterna.SacarPK(pIdTabla)
    strTipoDeDatoPK = m_oTablaExterna.SacarTipoDeDatoPK(strNombreTabla, strPK)
    'FORM_CAMPO.TIPO = TipoCampoPredefinido.externo
    'FORM_CAMPO.SUBTIPO = ocampo.Tipo
    '************************************************************************************
    
    'A�ade un campo del sistema al grupo seleccionado:
    g_Accion = ACCFormItemAnyadir
    Set oCampos = oFSGSRaiz.Generar_CFormCampos
    
    'genero la estructura del Campo
    'If m_oTablaExterna.SacarTablaTieneART(pIdTabla) Then
    '    Set oCampo = GenerarEstructuraCampoExterno(pIdTabla, TipoCampoExterno.TablaConArticulo)
    'Else
    '    Set oCampo = GenerarEstructuraCampoExterno(pIdTabla, TipoCampoExterno.TablaSinArticulo)
    'End If
    
    Set oCampo = GenerarEstructuraCampoExterno(pIdTabla, strTipoDeDatoPK)
    'a�ado el campo a la coleccion de campos
    oCampos.Add oCampo.Id, g_oCampoDesglose.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , oCampo.valorText, , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, , oCampo.FECACT, , oCampo.CampoPadre, oCampo.EsSubCampo, , , , , , , , , , , , , , , , , , , , , , , oCampo.TablaExterna
    'A�ade el campo a BD:
    udtTeserror = oCampos.AnyadirCamposExternosoAtributos(False)
    
    If udtTeserror.NumError = TESnoerror Then
        'AnyadirCampoExterno2 oCampos
        AnyadirCampos oCampos
        'AnyadirCampos oCampos, True
        RegistrarAccion ACCFormItemAnyadir, "Id:" & oCampo.Id
    Else
        Screen.MousePointer = vbNormal
        TratarError udtTeserror
        Exit Sub
    End If
    
    'aqui FALTA grabar el atributo
    

    Set oCampos = Nothing
    Set oCampo = Nothing

    g_Accion = ACCFormularioCons

End Sub
''

Public Function GenerarEstructuraCampoSistema(ByVal iTipoCampoGS) As CFormItem
Dim oDenominaciones As CMultiidiomas
Dim oIdioma As CIdioma
Dim oParametros As CLiterales
Dim Ador As Ador.Recordset
Dim oCampo As CFormItem

    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    Set oCampo.Grupo = g_oCampoDesglose.Grupo
    oCampo.Id = iTipoCampoGS
    oCampo.EsSubCampo = True
    Set oCampo.CampoPadre = g_oCampoDesglose
    
    Select Case iTipoCampoGS
        Case tipocampogs.Almacen, tipocampogs.Empresa, tipocampogs.AnyoImputacion
            oCampo.Tipo = TiposDeAtributos.TipoNumerico
        Case Else
            oCampo.Tipo = TiposDeAtributos.TipoTextoMedio
    End Select
     
    oCampo.CampoGS = iTipoCampoGS
    oCampo.TipoPredef = TipoCampoPredefinido.CampoGS
    
    'Carga las denominaciones del grupo de datos generales en todos los idiomas
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In m_oIdiomas
        If oIdioma.Cod = gParametrosInstalacion.gIdioma Then
            Select Case iTipoCampoGS
                Case tipocampogs.NuevoCodArticulo
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(5)
                Case tipocampogs.DenArticulo
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(11)
                Case tipocampogs.Dest
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(10)
                Case tipocampogs.FormaPago
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(2)
                Case tipocampogs.material
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(4)
                Case tipocampogs.Moneda
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(3)
                Case tipocampogs.Pais
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(8)
                Case tipocampogs.Proveedor
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(1)
                Case tipocampogs.provincia
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(9)
                Case tipocampogs.Unidad
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(6)
                Case tipocampogs.Pres1
                    oDenominaciones.Add oIdioma.Cod, gParametrosGenerales.gsSingPres1
                Case tipocampogs.Pres2
                    oDenominaciones.Add oIdioma.Cod, gParametrosGenerales.gsSingPres2
                Case tipocampogs.Pres3
                    oDenominaciones.Add oIdioma.Cod, gParametrosGenerales.gsSingPres3
                Case tipocampogs.Pres4
                    oDenominaciones.Add oIdioma.Cod, gParametrosGenerales.gsSingPres4
                Case tipocampogs.CampoPersona
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(12)
                Case tipocampogs.NumSolicitERP
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(13)
                Case tipocampogs.UnidadOrganizativa
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(14)
                Case tipocampogs.Departamento
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(15)
                Case tipocampogs.OrganizacionCompras
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(16)
                Case tipocampogs.Centro
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(17)
                Case tipocampogs.Almacen
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(18)
                Case tipocampogs.CentroCoste
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(19)
                Case tipocampogs.Activo
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(20)
                Case tipocampogs.TipoPedido
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(21)
                Case tipocampogs.Factura
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(22)
                Case tipocampogs.UnidadPedido
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(23)
                Case tipocampogs.ProveedorERP
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(24)
                Case tipocampogs.Empresa
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(25)
                Case tipocampogs.AnyoImputacion
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(26)
            End Select
        Else
            Select Case iTipoCampoGS
                Case tipocampogs.Pres1
                    Set oParametros = oGestorParametros.DevolverLiterales(20, 20, oIdioma.Cod)
                    oDenominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case tipocampogs.Pres2
                    Set oParametros = oGestorParametros.DevolverLiterales(21, 21, oIdioma.Cod)
                    oDenominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case tipocampogs.Pres3
                    Set oParametros = oGestorParametros.DevolverLiterales(27, 27, oIdioma.Cod)
                    oDenominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case tipocampogs.Pres4
                    Set oParametros = oGestorParametros.DevolverLiterales(28, 28, oIdioma.Cod)
                    oDenominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case Else
                    Select Case iTipoCampoGS
                        Case tipocampogs.NuevoCodArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 5)
                        Case tipocampogs.DenArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 36)
                        Case tipocampogs.Dest
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 10)
                        Case tipocampogs.FormaPago
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 2)
                        Case tipocampogs.material
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 4)
                        Case tipocampogs.Moneda
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 3)
                        Case tipocampogs.Pais
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 8)
                        Case tipocampogs.Proveedor
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 1)
                        Case tipocampogs.provincia
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 9)
                        Case tipocampogs.Unidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 6)
                        Case tipocampogs.CampoPersona
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 35)
                        Case tipocampogs.NumSolicitERP
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 37)
                        Case tipocampogs.UnidadOrganizativa
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 38)
                        Case tipocampogs.Departamento
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 39)
                        Case tipocampogs.OrganizacionCompras
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 40)
                        Case tipocampogs.Centro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 41)
                        Case tipocampogs.Almacen
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 42)
                        Case tipocampogs.CentroCoste
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 48)
                        Case tipocampogs.Activo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 49)
                        Case tipocampogs.TipoPedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 50)
                        Case tipocampogs.Factura
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 53)
                        Case tipocampogs.UnidadPedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 57)
                        Case tipocampogs.ProveedorERP
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 58)
                        Case tipocampogs.Empresa
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 65)
                        Case tipocampogs.AnyoImputacion
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 67)
                    End Select
                    oDenominaciones.Add oIdioma.Cod, Ador(0).Value
                    Ador.Close
            End Select
            Set oParametros = Nothing
        End If
    Next
    Set oCampo.Denominaciones = oDenominaciones
    Set Ador = Nothing
    
    Set GenerarEstructuraCampoSistema = oCampo
End Function

Private Function GenerarEstructuraArbolPresupuestario(ByVal iTipoCampoGS As Integer, ByVal sPRES5 As String, Optional ByVal bSubcampo As Boolean = False) As CFormItem
    Dim oDenominaciones As CMultiidiomas
    Dim oIdioma As CIdioma
    Dim oCampo As CFormItem
    Dim oPres5Niv0 As cPresConceptos5Nivel0
    Dim Adores As ADODB.Recordset
    
    
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    Set oCampo.Grupo = g_oCampoDesglose.Grupo
    oCampo.Id = iTipoCampoGS
    oCampo.EsSubCampo = True
    Set oCampo.CampoPadre = g_oCampoDesglose
    
    Select Case iTipoCampoGS
        Case tipocampogs.Almacen
            oCampo.Tipo = TiposDeAtributos.TipoNumerico
        Case Else
            oCampo.Tipo = TiposDeAtributos.TipoTextoMedio
    End Select
     
    oCampo.CampoGS = iTipoCampoGS
    oCampo.TipoPredef = TipoCampoPredefinido.CampoGS
    oCampo.PRES5 = sPRES5
    
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    Set oPres5Niv0 = oFSGSRaiz.Generar_CPresConceptos5Nivel0
    For Each oIdioma In m_oIdiomas
        Set Adores = oPres5Niv0.DevolverDenominacion(oIdioma.Cod, sPRES5)
        oDenominaciones.Add oIdioma.Cod, Adores(0).Value
    Next
    
    Set oCampo.Denominaciones = oDenominaciones
    Set GenerarEstructuraArbolPresupuestario = oCampo
End Function

Private Function GenerarEstructuraCampoExterno(ByVal pIdTabla As Integer, pTipoDeDatoPK As TiposDeAtributos) As CFormItem
Dim oDenominaciones As CMultiidiomas
Dim oIdioma As CIdioma
Dim oParametros As CLiterales
Dim oCampo As CFormItem

    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    Set oCampo.Grupo = g_oCampoDesglose.Grupo           '
    oCampo.Id = TipoCampoPredefinido.externo                                    '
    oCampo.EsSubCampo = True                            '
    '------------------------------------------------
    oCampo.Tipo = pTipoDeDatoPK
    oCampo.TipoPredef = TipoCampoPredefinido.externo    '6
    oCampo.TablaExterna = pIdTabla
    '------------------------------------------------
    
    Set oCampo.CampoPadre = g_oCampoDesglose            '
    
    Dim strDenoTabla As String
    strDenoTabla = m_oTablaExterna.SacarDenominacionTabla(pIdTabla)
     
    'FALTA Carga las denominaciones del grupo de datos generales en todos los idiomas
    m_sCamposExterno(1) = strDenoTabla
    
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In m_oIdiomas
        oDenominaciones.Add oIdioma.Cod, m_sCamposExterno(1)
    Next
    
    Set oParametros = Nothing
    Set oCampo.Denominaciones = oDenominaciones
    Set GenerarEstructuraCampoExterno = oCampo
End Function



Private Function ObtenerNombreCampo(ByVal lCampoGS As Long, ByVal iTipo As Integer, ByVal iTipoPredef As Integer, ByVal bMultiIdioma As Boolean, Optional ByVal sPRES5 As String) As String
'*************************************************************
'Funcion que devuelve el Nombre del CampoGS, certificado o No Conformidad
'y si es o no MultiIdioma carga adem�s los nombres en los distintos idiomas
'*************************************************************

Dim sNombre As String
Dim sNombreIdiomas As String
Dim sNombreAux As String

Dim oIdioma As CIdioma
Dim oParametros As CLiterales
Dim oPres5Niv0 As cPresConceptos5Nivel0
Dim Ador As Ador.Recordset

If m_oIdiomas Is Nothing Then
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
End If

Select Case iTipo

    Case TipoCampoPredefinido.CampoGS
        For Each oIdioma In m_oIdiomas
            Select Case lCampoGS
                Case tipocampogs.Pres1
                    Set oParametros = oGestorParametros.DevolverLiterales(20, 20, oIdioma.Cod)
                    sNombreAux = oParametros.Item(1).Den
                Case tipocampogs.Pres2
                    Set oParametros = oGestorParametros.DevolverLiterales(21, 21, oIdioma.Cod)
                    sNombreAux = oParametros.Item(1).Den
                Case tipocampogs.Pres3
                    Set oParametros = oGestorParametros.DevolverLiterales(27, 27, oIdioma.Cod)
                    sNombreAux = oParametros.Item(1).Den
                Case tipocampogs.Pres4
                    Set oParametros = oGestorParametros.DevolverLiterales(28, 28, oIdioma.Cod)
                    sNombreAux = oParametros.Item(1).Den
                
                Case Else
                    Select Case lCampoGS
                    
                        Case tipocampogs.CodArticulo, tipocampogs.NuevoCodArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 5)
                        Case tipocampogs.DenArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 36)
                        Case tipocampogs.Desglose
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 7)
                        Case tipocampogs.Dest
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 10)
                        Case tipocampogs.FormaPago
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 2)
                        Case tipocampogs.material
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 4)
                        Case tipocampogs.Moneda
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 3)
                        Case tipocampogs.Pais
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 8)
                        Case tipocampogs.Proveedor
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 1)
                        Case tipocampogs.provincia
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 9)
                        Case tipocampogs.Unidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 6)
                        Case tipocampogs.CampoPersona
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 35)
                        Case TipoCampoSC.DescrBreve
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 26)
                        Case TipoCampoSC.DescrDetallada
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 27)
                        Case TipoCampoSC.importe
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 28)
                        Case TipoCampoSC.Cantidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 29)
                        Case TipoCampoSC.FecNecesidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 30)
                        Case TipoCampoSC.IniSuministro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 31)
                        Case TipoCampoSC.FinSuministro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 32)
                        Case TipoCampoSC.ArchivoEspecific
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 33)
                        Case TipoCampoSC.PrecioUnitario
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 34)
                        Case tipocampogs.NumSolicitERP
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 37)
                        Case tipocampogs.UnidadOrganizativa
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 38)
                        Case tipocampogs.Departamento
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 39)
                        Case tipocampogs.OrganizacionCompras
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 40)
                        Case tipocampogs.Centro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 41)
                        Case tipocampogs.Almacen
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 42)
                        Case TipoCampoSC.PrecioUnitarioAdj
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 45)
                        Case TipoCampoSC.ProveedorAdj
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 46)
                        Case TipoCampoSC.CantidadAdj
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 47)
                        Case tipocampogs.CentroCoste
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 48)
                        Case tipocampogs.Activo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 49)
                        Case tipocampogs.TipoPedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 50)
                        Case tipocampogs.Factura
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 53)
                        Case tipocampogs.UnidadPedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 57)
                        Case tipocampogs.ProveedorERP
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 58)
                        Case tipocampogs.PartidaPresupuestaria
                            Set oPres5Niv0 = oFSGSRaiz.Generar_CPresConceptos5Nivel0
                            Set Ador = oPres5Niv0.DevolverDenominacion(oIdioma.Cod, sPRES5)
                            Set oPres5Niv0 = Nothing
                        Case TipoCampoSC.TotalLineaAdj
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 61)
                        Case TipoCampoSC.TotalLineaPreadj
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 62)
                        Case tipocampogs.Empresa
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 65)
                        Case tipocampogs.AnyoImputacion
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 67)
                        End Select
                        sNombreAux = Ador(0).Value
                        Ador.Close
                End Select
                
                If (oIdioma.Cod = gParametrosInstalacion.gIdioma) Then
                    sNombre = sNombreAux
                Else
                    sNombreIdiomas = sNombreIdiomas & " / " & sNombreAux
                End If
                Set oParametros = Nothing
            Next
            
            If bMultiIdioma Then
                sNombre = sNombre & sNombreIdiomas
            End If

Case TipoCampoPredefinido.Certificado, TipoCampoPredefinido.NoConformidad

    Dim oTipoSolic As CTipoSolicit
    
    Set oTipoSolic = oFSGSRaiz.Generar_CTipoSolicit
    oTipoSolic.CargarTipoSolicitud lCampoGS, iTipo, iTipoPredef
    
    While oTipoSolic.Campos.Item(1).Denominaciones.Count > 0
        If oTipoSolic.Campos.Item(1).Denominaciones.Item(1).Cod = gParametrosInstalacion.gIdioma Then
            sNombre = oTipoSolic.Campos.Item(1).Denominaciones.Item(1).Den
        Else
            sNombreIdiomas = sNombreIdiomas & " / " & oTipoSolic.Campos.Item(1).Denominaciones.Item(1).Den
        End If
        oTipoSolic.Campos.Item(1).Denominaciones.Remove (oTipoSolic.Campos.Item(1).Denominaciones.Item(1).Cod)
    Wend
    
    If bMultiIdioma Then
        sNombre = sNombre & sNombreIdiomas
    End If
    Set oTipoSolic = Nothing
End Select

ObtenerNombreCampo = sNombre

        
End Function

Private Function ExisteAtributoAProceso() As Boolean
    Dim i As Integer
    Dim vbm As Variant
    Dim bExiste As Boolean
    
    bExiste = False
    
    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
        If sdbgCampos.Columns("IDATRIB").CellValue(vbm) = sdbgCampos.Columns("IDATRIB").Value _
        And sdbgCampos.Columns("APROCESO").CellValue(vbm) = True _
        And sdbgCampos.Row <> i Then
            bExiste = True
            Exit For
        End If
    Next
    
    ExisteAtributoAProceso = bExiste
    
End Function



Public Function ExisteCodArticulo(ByVal oCampos As CFormItems) As Boolean
    Dim oCampo As CFormItem
    Dim bExiste As Boolean
    
    If oCampos Is Nothing Then
        ExisteCodArticulo = False
        Exit Function
    End If

    For Each oCampo In oCampos
        If oCampo.CampoGS = "104" _
        Or oCampo.CampoGS = "119" Then
            bExiste = True
            Exit For
        End If
    Next
    
    ExisteCodArticulo = bExiste
    
End Function

'Funcion que devuelve true si existe ese campo GS en el desglose
Private Function ExisteCampoGS(tipoGs As tipocampogs) As Boolean
    Dim oCampo As CFormItem

    For Each oCampo In g_oCampoDesglose.Desglose
        If oCampo.CampoGS = tipoGs Then
            ExisteCampoGS = True
            Exit Function
        End If
    Next
    ExisteCampoGS = False
End Function

Function CamposSistemaAProceso(Tipo As Variant) As Boolean
  
    Select Case Tipo
    Case TipoCampoSC.DescrBreve, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro, tipocampogs.Dest, tipocampogs.FormaPago, tipocampogs.Proveedor, tipocampogs.Pres1, tipocampogs.Pres2, tipocampogs.Pres3, tipocampogs.Pres4, tipocampogs.material, tipocampogs.CodArticulo, tipocampogs.NuevoCodArticulo, tipocampogs.DenArticulo, TipoCampoSC.Cantidad, TipoCampoSC.PrecioUnitario, tipocampogs.Unidad
        CamposSistemaAProceso = True
    Case Else
        CamposSistemaAProceso = False
    End Select

End Function

''' <summary>
''' Cuando se va a borrar un campo art�culo se debe comprobar que los campos de qa relacionados con el art�culo
''' se borren. Esta funci�n dice si hay campos qa relacionados y cual/cuales es/son.
''' </summary>
''' <param name="bQaDos">Hay tanto Piezas Defectuosas como Importe Repercutido</param>
''' <param name="bQaUno">Hay Piezas Defectuosas � Importe Repercutido</param>
''' <param name="iTipo">3 Hay tanto Piezas Defectuosas como Importe Repercutido
'''     1 Hay Piezas Defectuosas    2 Hay Importe Repercutido</param>
'''     4 Hay envios correctos      5 Envios correctos y piezas defectuosas
'''     6 Importe repercutido y envios correctos
'''     7 Hay los 3 tipos (piezas, importe y envio)
''' <returns>S�/No hay campos qa relacionados. Los parametros bQaDos, bQaUno e iTipo son de salida</returns>
''' <remarks>Llamada desde:cmdElimItem_Click; Tiempo m�ximo:0</remarks>
Private Function HayCamposQARelacionados(ByRef bQaDos As Boolean, ByRef bQaUno As Boolean, ByRef iTipo As Integer, ByRef bHayFechaImputacionDefecto As Boolean) As Boolean
    Dim j As Integer
    Dim pos As Integer
    
    LockWindowUpdate Me.hWnd
    
    pos = Me.sdbgCampos.Row
    
    Me.sdbgCampos.MoveFirst
    For j = 0 To sdbgCampos.Rows - 1
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.PiezasDefectuosas Then
            If bQaUno Then
                bQaDos = True
                iTipo = 3
            Else
                bQaUno = True
                iTipo = 1
            End If
        ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.ImporteRepercutido Then
            If bQaUno Then
                bQaDos = True
                iTipo = 3
            Else
                bQaUno = True
                iTipo = 2
            End If
        ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.FechaImputacionDefecto Then
            bHayFechaImputacionDefecto = True
        End If
        
        Me.sdbgCampos.MoveNext
    Next
    
    Me.sdbgCampos.Row = pos

    LockWindowUpdate 0&
      
    If m_bConEnviosCorrectos Then
        If iTipo = 1 Then
            iTipo = 5
        ElseIf iTipo = 2 Then
            iTipo = 6
        ElseIf iTipo = 3 Then
            iTipo = 7
        Else
            iTipo = 4
        End If
    End If
    
    If bHayFechaImputacionDefecto Then
        Select Case iTipo
            Case 1
                iTipo = 8
            Case 2
                iTipo = 9
            Case 3
                iTipo = 10
            Case 4
                iTipo = 11
            Case 5
                iTipo = 12
            Case 6
                iTipo = 13
            Case 7
                iTipo = 14
            Case Else
                iTipo = 15
        End Select
    End If
    
    HayCamposQARelacionados = bQaDos Or bQaUno Or m_bConEnviosCorrectos Or bHayFechaImputacionDefecto
End Function

''' <summary>
''' Indica si un campo va a ser de tipo Envios correctos o lo desmarca
''' </summary>
''' <param name="bMarcar">Indica si marca o desmarca el campo</param>
''' <returns>nada</returns>
''' <remarks>Llamada desde=MDI -->mnuPopUpFSQAVarCalDesg_Click; Tiempo m�ximo=0,3seg.</remarks>
Public Sub MarcarEnviosCorrectos(ByVal bMarcar As Boolean)
    Dim iTipo As Integer
    iTipo = -1
    
    If bMarcar Then
        sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.EnviosCorrectos
        If sdbgCampos.Columns("TIPO").Value <> TipoCampoPredefinido.Calculado Then
            sdbgCampos.Columns("TIPO").Value = NoConformidad
            iTipo = NoConformidad
        End If
    Else
        sdbgCampos.Columns("CAMPO_GS").Value = Null
        If sdbgCampos.Columns("TIPO").Value <> TipoCampoPredefinido.Calculado Then
            sdbgCampos.Columns("TIPO").Value = 0
            iTipo = 0
        End If
    End If
    sdbgCampos.Update
    Set m_oCampoEnEdicion = g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value))
    
    m_oCampoEnEdicion.CampoGS = TipoCampoNoConformidad.EnviosCorrectos
    m_oCampoEnEdicion.ModificarTipoGS_CampoFSQA bMarcar, iTipo
    If iTipo > -1 Then
        g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef = iTipo
    End If
    Set m_oCampoEnEdicion = Nothing
    m_bConEnviosCorrectos = bMarcar
End Sub

''' <summary>
''' Indica si un campo va a ser de tipo Piezas defectuosas o lo desmarca
''' </summary>
''' <param name="bMarcar">Indica si marca o desmarca el campo</param>
''' <returns>nada</returns>
''' <remarks>Llamada desde=MDI -->mnuPopUpFSQAVarCalDesg_Click; Tiempo m�ximo=0,3seg.</remarks>
Public Sub marcarPiezasDefectuosas(ByVal bMarcar As Boolean)
    Dim iTipo As Integer
    iTipo = -1
    
    If bMarcar Then
        sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.PiezasDefectuosas
        If sdbgCampos.Columns("TIPO").Value <> TipoCampoPredefinido.Calculado Then
            sdbgCampos.Columns("TIPO").Value = NoConformidad
            iTipo = NoConformidad
        End If
    Else
        sdbgCampos.Columns("CAMPO_GS").Value = Null
        If sdbgCampos.Columns("TIPO").Value <> TipoCampoPredefinido.Calculado Then
            sdbgCampos.Columns("TIPO").Value = 0
            iTipo = 0
        End If
    End If
    sdbgCampos.Update
    Set m_oCampoEnEdicion = g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value))
    
    m_oCampoEnEdicion.CampoGS = TipoCampoNoConformidad.PiezasDefectuosas
    m_oCampoEnEdicion.ModificarTipoGS_CampoFSQA bMarcar, iTipo
    If iTipo > -1 Then
        g_oCampoDesglose.Desglose.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef = iTipo
    End If
    Set m_oCampoEnEdicion = Nothing
    m_bConPiezasDefectuosas = bMarcar
End Sub

''' <summary>
''' Comprueba si el centro de coste pasado como par�metro est� asociado a una partida o a un activo
''' </summary>
''' <param name="IDCentroCoste">Id del centro de coste a comprobar</param>
''' <returns>el objeto CFormItem con el campo que tiene el centro de coste relacionado, si no, nada</returns>
''' <remarks>Llamada desde: cmdElimItem_Click; Tiempo m�ximo=0,6 seg.</remarks>
Private Function ComprobarCentroDeCoste(IDCentroCoste As Long) As CFormItem
    Dim g As CFormGrupo
    Dim c As CFormItem
    Dim r As CFormItem
    
    If Not frmFormularios.g_oFormSeleccionado Is Nothing Then
        For Each g In frmFormularios.g_oFormSeleccionado.Grupos
            g.CargarTodosLosCampos
            
            If Not g.Campos Is Nothing Then
                For Each c In g.Campos
                    If c.CampoGS = tipocampogs.Activo Or c.CampoGS = tipocampogs.PartidaPresupuestaria Then
                        If c.CentroCoste = IDCentroCoste Then
                            Set r = c
                            Exit For
                        End If
                    End If
                Next
                If Not r Is Nothing Then
                    Exit For
                End If
            End If
        Next
    End If
    
    If r Is Nothing Then
        Set g = frmFormularios.g_oGrupoSeleccionado
        frmFormularios.g_ofrmDesglose.g_oCampoDesglose.CargarDesglose
        For Each c In frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Desglose
            If c.CampoGS = tipocampogs.Activo Or c.CampoGS = tipocampogs.PartidaPresupuestaria Then
                If c.CentroCoste = IDCentroCoste Then
                    Set r = c
                    Exit For
                End If
            End If
        Next
    End If
    
    Set ComprobarCentroDeCoste = r
End Function
''' <summary>
''' Permitir el acceso a vincular
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub chkVincular_Click()
    Dim teserror As TipoErrorSummit
    
    Me.cmdVincular.Enabled = (Me.chkVincular.Value = vbChecked)
    
    If m_oIdiomas Is Nothing Then Exit Sub 'si est� haciendo el load
    
    Screen.MousePointer = vbHourglass
    If chkVincular.Value = vbChecked Then
        g_oCampoDesglose.Vinculado = True
    Else
        g_oCampoDesglose.Vinculado = False
    End If
    
    teserror = g_oCampoDesglose.ModificarDesgloseVinculado
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
End Sub
''' <summary>
''' Muestra la pantalla para vincular
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub cmdVincular_Click()
    Dim oCampo As CFormItem

    If g_sOrigen <> "frmFormularios" Then Exit Sub
    
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    oCampo.Id = g_oCampoDesglose.Id
    Set oCampo.Grupo = g_oCampoDesglose.Grupo
    Set oCampo.Denominaciones = g_oCampoDesglose.Denominaciones
    Set frmFormularioVincularDesglose.g_oCampoDesglose = oCampo
    frmFormularioVincularDesglose.g_bModificar = chkVincular.Enabled
    frmFormularioVincularDesglose.Show vbModal
    
    Set oCampo = Nothing
End Sub
''' <summary>
''' Comprueba si existe el campo Gs organizacion de compras en el desglose, o luego a nivel de formulario
''' </summary>
'''<param:name="OmitirEnDesglose">True cuando no se quiere buscar el campo Org.Compras en el desglose</param>
''' <returns>True si existe el campo Org.compras,false si no</returns>
Private Function ComprobarOrgCompras(Optional ByVal OmitirEnDesglose As Boolean = False) As Boolean
    Dim g As CFormGrupo
    Dim c As CFormItem
    
    Set g = frmFormularios.g_oGrupoSeleccionado
    If Not OmitirEnDesglose Then
        frmFormularios.g_ofrmDesglose.g_oCampoDesglose.CargarDesglose
        For Each c In frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Desglose
            If c.CampoGS = tipocampogs.OrganizacionCompras Then
                ComprobarOrgCompras = True
                Exit Function
            End If
        Next
    End If
    If Not frmFormularios.g_oFormSeleccionado Is Nothing Then
        For Each g In frmFormularios.g_oFormSeleccionado.Grupos
            g.CargarTodosLosCampos
            If Not g.Campos Is Nothing Then
                For Each c In g.Campos
                    If c.CampoGS = tipocampogs.OrganizacionCompras Then
                        ComprobarOrgCompras = True
                        Exit Function
                    End If
                Next
            End If
        Next
    End If
    
    ComprobarOrgCompras = False
End Function
