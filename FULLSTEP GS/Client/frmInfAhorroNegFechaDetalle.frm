VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroNegFechaDetalle 
   BackColor       =   &H00808000&
   Caption         =   "Detalle de resultados "
   ClientHeight    =   2580
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9285
   FillStyle       =   0  'Solid
   Icon            =   "frmInfAhorroNegFechaDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2580
   ScaleWidth      =   9285
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2415
      Left            =   120
      ScaleHeight     =   2415
      ScaleWidth      =   9015
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   60
      Width           =   9015
      Begin VB.PictureBox picTipoGrafico 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   420
         ScaleHeight     =   315
         ScaleWidth      =   2595
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   60
         Visible         =   0   'False
         Width           =   2595
         Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
            Height          =   285
            Left            =   0
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   0
            Width           =   1515
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            Row.Count       =   5
            Row(0)          =   "Barras 2D"
            Row(1)          =   "Barras 3D"
            Row(2)          =   "Lineas 2D"
            Row(3)          =   "Lineas 3D"
            Row(4)          =   "Tarta"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   3200
            Columns(0).Caption=   "TIPO"
            Columns(0).Name =   "TIPO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2672
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.CommandButton cmdActualizar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroNegFechaDetalle.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   480
         Width           =   315
      End
      Begin VB.CommandButton cmdImprimir 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroNegFechaDetalle.frx":0D3D
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   480
         Visible         =   0   'False
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgRes 
         Height          =   2115
         Left            =   420
         TabIndex        =   1
         Top             =   120
         Width           =   8430
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   8
         stylesets.count =   3
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmInfAhorroNegFechaDetalle.frx":107F
         stylesets(1).Name=   "Red"
         stylesets(1).BackColor=   4744445
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmInfAhorroNegFechaDetalle.frx":109B
         stylesets(2).Name=   "Green"
         stylesets(2).BackColor=   10409634
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmInfAhorroNegFechaDetalle.frx":10B7
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorEven   =   -2147483633
         BackColorOdd    =   -2147483633
         RowHeight       =   423
         Columns.Count   =   8
         Columns(0).Width=   1058
         Columns(0).Caption=   "A�o"
         Columns(0).Name =   "A�O"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasHeadForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).HeadBackColor=   12632256
         Columns(0).BackColor=   16776960
         Columns(1).Width=   1005
         Columns(1).Caption=   "GMN1"
         Columns(1).Name =   "GMN1"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).HeadBackColor=   12632256
         Columns(1).BackColor=   16776960
         Columns(2).Width=   2064
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).HeadBackColor=   12632256
         Columns(2).BackColor=   16776960
         Columns(3).Width=   2858
         Columns(3).Caption=   "Presupuesto"
         Columns(3).Name =   "PRES"
         Columns(3).Alignment=   1
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "Standard"
         Columns(3).FieldLen=   256
         Columns(3).HasHeadForeColor=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).HeadBackColor=   12632256
         Columns(3).BackColor=   15400959
         Columns(4).Width=   2805
         Columns(4).Caption=   "Adjudicado"
         Columns(4).Name =   "ADJ"
         Columns(4).Alignment=   1
         Columns(4).CaptionAlignment=   2
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).NumberFormat=   "Standard"
         Columns(4).FieldLen=   256
         Columns(4).HasHeadForeColor=   -1  'True
         Columns(4).HasBackColor=   -1  'True
         Columns(4).HeadBackColor=   12632256
         Columns(4).BackColor=   15400959
         Columns(5).Width=   3043
         Columns(5).Caption=   "Ahorro"
         Columns(5).Name =   "AHO"
         Columns(5).Alignment=   1
         Columns(5).CaptionAlignment=   2
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).NumberFormat=   "Standard"
         Columns(5).FieldLen=   256
         Columns(5).HasHeadForeColor=   -1  'True
         Columns(5).HeadBackColor=   12632256
         Columns(6).Width=   1111
         Columns(6).Caption=   "%"
         Columns(6).Name =   "PORCEN"
         Columns(6).Alignment=   1
         Columns(6).CaptionAlignment=   2
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).NumberFormat=   "0.0#\%"
         Columns(6).FieldLen=   256
         Columns(6).HasHeadForeColor=   -1  'True
         Columns(6).HeadBackColor=   12632256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "Fecha"
         Columns(7).Name =   "Fecha"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   14870
         _ExtentY        =   3731
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSChart20Lib.MSChart MSChart1 
         Height          =   1815
         Left            =   420
         OleObjectBlob   =   "frmInfAhorroNegFechaDetalle.frx":10D3
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   420
         Width           =   8415
      End
      Begin VB.CommandButton cmdGrafico 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroNegFechaDetalle.frx":2AF9
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   120
         Width           =   315
      End
      Begin VB.CommandButton cmdGrid 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroNegFechaDetalle.frx":2E3B
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   315
      End
   End
End
Attribute VB_Name = "frmInfAhorroNegFechaDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public dequivalencia As Double

'Variables utilizadas para el multilenguaje
Private sIdiTipoGrafico(5) As String
Private sIdiDetProc As String

Public g_sOrigen As String

Public sGmn1 As String
Public sGmn2 As String
Public sGmn3 As String
Public sGmn4 As String
Public sDetalle As String
Public sFecha As String


Private Sub cmdActualizar_Click()
    If cmdGrid.Visible Then
        MostrarGrafico sdbcTipoGrafico.value
    End If
End Sub

Private Sub cmdGrafico_Click()
        
        If sdbgRes.Rows = 0 Then
            Exit Sub
        End If
'        sdbcTipoGrafico.Value = "Barras 3D"
        
        Screen.MousePointer = vbHourglass
        sdbcTipoGrafico.value = sIdiTipoGrafico(2)
        
        picTipoGrafico.Visible = True
        MostrarGrafico sdbcTipoGrafico.value
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        MSChart1.Visible = True
        Screen.MousePointer = vbNormal

End Sub

Private Sub cmdGrid_Click()
        
        picTipoGrafico.Visible = False
        cmdGrafico.Visible = True
        cmdGrid.Visible = False
        sdbgRes.Visible = True
        MSChart1.Visible = False
End Sub

Private Sub Form_Activate()
    sdbgRes.SelBookmarks.RemoveAll
End Sub

Private Sub Form_Load()
    
    Me.Height = 2985
    Me.Width = 9405
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    CargarRecursos
    
    PonerFieldSeparator Me
End Sub

Private Sub Form_Resize()
    If Me.Height < 1100 Then Exit Sub
    If Me.Width < 1100 Then Exit Sub
    
    Picture1.Width = Me.Width - 360
    Picture1.Height = Me.Height - 570
    sdbgRes.Width = Picture1.Width - 660
    sdbgRes.Height = Picture1.Height - 300
    
    sdbgRes.Columns(0).Width = sdbgRes.Width * 0.1
    sdbgRes.Columns(1).Width = sdbgRes.Width * 0.1
    sdbgRes.Columns(2).Width = sdbgRes.Width * 0.11
    sdbgRes.Columns(3).Width = sdbgRes.Width * 0.18
    sdbgRes.Columns(4).Width = sdbgRes.Width * 0.18
    sdbgRes.Columns(5).Width = sdbgRes.Width * 0.18
    sdbgRes.Columns(6).Width = sdbgRes.Width * 0.15 - 500

    
    MSChart1.Width = sdbgRes.Width
    MSChart1.Height = sdbgRes.Height - 300
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    g_sOrigen = ""
End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.value
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.value
End Sub

Private Sub sdbgRes_DblClick()
Dim ADORs As Ador.Recordset
    
    If sdbgRes.Rows = 0 Then Exit Sub
    If sdbgRes.Row < 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    frmInfAhorroNegProceDetalle.g_dEquivalencia = dequivalencia
    frmInfAhorroNegProceDetalle.g_iAnyo = sdbgRes.Columns("A�O").value
    frmInfAhorroNegProceDetalle.g_sGmn1 = sdbgRes.Columns("GMN1").value
    frmInfAhorroNegProceDetalle.g_iCod = sdbgRes.Columns("COD").value
    frmInfAhorroNegProceDetalle.sGmn1 = Me.sGmn1
    frmInfAhorroNegProceDetalle.sGmn2 = Me.sGmn2
    frmInfAhorroNegProceDetalle.sGmn3 = Me.sGmn3
    frmInfAhorroNegProceDetalle.sGmn4 = Me.sGmn4
    frmInfAhorroNegProceDetalle.sDetalle = Me.sDetalle
    If sFecha <> "" Then
        frmInfAhorroNegProceDetalle.sFecha = sFecha
    Else
        frmInfAhorroNegProceDetalle.sFecha = sdbgRes.Columns("Fecha").value
    End If
        
    sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
    
    If g_sOrigen = "Concep3Reu" Or g_sOrigen = "Concep4Reu" Or g_sOrigen = "Concep3Desde" Or g_sOrigen = "Concep4Desde" Or g_sOrigen = "Concep3" Or g_sOrigen = "Concep4" Then
        frmInfAhorroNegProceDetalle.g_sOrigen = g_sOrigen
    Else
        frmInfAhorroNegProceDetalle.g_sOrigen = "Proceso"
    End If
    
    frmInfAhorroNegProceDetalle.caption = sIdiDetProc & "     " & sdbgRes.Columns(0).Text & "/" & sdbgRes.Columns(1).Text & "/" & sdbgRes.Columns(2).Text
    
    frmInfAhorroNegProceDetalle.Top = sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + sdbgRes.Top + Top + Picture1.Top + 360
    frmInfAhorroNegProceDetalle.Left = sdbgRes.Left + Left + Picture1.Left + 325
    
    Screen.MousePointer = vbNormal
    
    frmInfAhorroNegProceDetalle.Show 1
    
    Set ADORs = Nothing

End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
     If sdbgRes.Columns("AHO").value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    MSChart1.Visible = True
    Select Case Tipo
    
'        Case "Barras 2D", "Barras 3D"
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & "/" & sdbgRes.Columns(1).value & "/" & sdbgRes.Columns(2).value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").value) > CDbl(sdbgRes.Columns("ADJ").value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").value) - CDbl(sdbgRes.Columns("ADJ").value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").value) - CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").value) - CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
'                If Tipo = "Barras 3D" Then
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                    
                
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                
'                If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Text & " " & sdbgRes.Columns(2).Text
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").value) > CDbl(sdbgRes.Columns("ADJ").value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").value) - CDbl(sdbgRes.Columns("ADJ").value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").value) - CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").value) - CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                 'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                  
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
    
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRO_NEGFECHADETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).value
        Ador.MoveNext
        sdbgRes.Columns(0).caption = Ador(0).value
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).value
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).value
        Ador.MoveNext
        sdbgRes.Columns(5).caption = Ador(0).value
        Ador.MoveNext
        sdbcTipoGrafico.RemoveAll
        sdbcTipoGrafico.AddItem Ador(0).value
        sIdiTipoGrafico(1) = Ador(0).value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).value
        sIdiTipoGrafico(2) = Ador(0).value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).value
        sIdiTipoGrafico(3) = Ador(0).value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).value
        sIdiTipoGrafico(4) = Ador(0).value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).value
        sIdiTipoGrafico(5) = Ador(0).value
        Ador.MoveNext
        sIdiDetProc = Ador(0).value
       
        Ador.Close
    
    End If

    Set Ador = Nothing

End Sub
