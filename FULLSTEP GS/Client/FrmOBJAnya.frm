VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form FrmOBJAnya 
   BackColor       =   &H00808000&
   Caption         =   "Comunicaci�n de Objetivos"
   ClientHeight    =   6390
   ClientLeft      =   675
   ClientTop       =   2475
   ClientWidth     =   11280
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmOBJAnya.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6390
   ScaleWidth      =   11280
   Begin VB.CheckBox chkExcelEspe 
      BackColor       =   &H00808000&
      Caption         =   "DIncluir hoja excel del petici�n de ofertas"
      ForeColor       =   &H00FFFFFF&
      Height          =   600
      Left            =   9435
      TabIndex        =   38
      Top             =   2760
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.TextBox txtForm 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   2310
      MaxLength       =   255
      TabIndex        =   8
      Top             =   1710
      Width           =   6585
   End
   Begin VB.CommandButton cmdForm 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8940
      Picture         =   "FrmOBJAnya.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   1710
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.CheckBox chkExcel 
      BackColor       =   &H00808000&
      Caption         =   "Incluir hoja excel de petici�n de ofertas"
      ForeColor       =   &H00FFFFFF&
      Height          =   630
      Left            =   9435
      TabIndex        =   20
      Top             =   1910
      Width           =   1755
   End
   Begin VB.TextBox txtHoraLimite 
      Height          =   285
      Left            =   6975
      TabIndex        =   17
      Top             =   3240
      Width           =   885
   End
   Begin VB.CheckBox chkEsp 
      BackColor       =   &H00808000&
      Caption         =   "Incluir especificaciones de items"
      ForeColor       =   &H00FFFFFF&
      Height          =   675
      Left            =   9435
      TabIndex        =   18
      Top             =   165
      Width           =   1755
   End
   Begin VB.CheckBox chkFich 
      BackColor       =   &H00808000&
      Caption         =   "Incluir archivos adjuntos"
      ForeColor       =   &H00FFFFFF&
      Height          =   630
      Left            =   9435
      TabIndex        =   19
      Top             =   1060
      Width           =   1755
   End
   Begin VB.CommandButton cmdCalendar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6015
      Picture         =   "FrmOBJAnya.frx":0FF4
      Style           =   1  'Graphical
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   3255
      Width           =   315
   End
   Begin VB.TextBox txtFecLimOfe 
      Height          =   285
      Left            =   4500
      TabIndex        =   15
      Top             =   3240
      Width           =   1470
   End
   Begin VB.CommandButton cmdDOT 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8940
      Picture         =   "FrmOBJAnya.frx":157E
      Style           =   1  'Graphical
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   240
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.TextBox txtDOT 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   2310
      MaxLength       =   255
      TabIndex        =   2
      Top             =   240
      Width           =   6585
   End
   Begin VB.TextBox txtMailDot 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   2310
      MaxLength       =   255
      TabIndex        =   4
      Top             =   660
      Width           =   6585
   End
   Begin VB.TextBox txtWebDot 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   2310
      MaxLength       =   255
      TabIndex        =   6
      Top             =   1080
      Width           =   6585
   End
   Begin VB.CommandButton cmdMailDot 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8940
      Picture         =   "FrmOBJAnya.frx":163D
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   660
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.CommandButton cmdWebDot 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8940
      Picture         =   "FrmOBJAnya.frx":16FC
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1080
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   11280
      TabIndex        =   24
      Top             =   5895
      Width           =   11280
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   5738
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   4538
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPet 
      Height          =   2175
      Left            =   60
      TabIndex        =   21
      Top             =   3690
      Width           =   11175
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   15
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "FrmOBJAnya.frx":17BB
      stylesets(1).Name=   "ProvPortal"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "FrmOBJAnya.frx":17D7
      UseGroups       =   -1  'True
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   3
      Groups(0).Width =   6747
      Groups(0).Caption=   "Proveedores"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadForeColor=   16777215
      Groups(0).HeadBackColor=   8421504
      Groups(0).Columns.Count=   2
      Groups(0).Columns(0).Width=   2408
      Groups(0).Columns(0).Caption=   "C�digo"
      Groups(0).Columns(0).Name=   "CODPROVE"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   4339
      Groups(0).Columns(1).Caption=   "Denominaci�n"
      Groups(0).Columns(1).Name=   "DENPROVE"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(1).Width =   5715
      Groups(1).Caption=   "Contactos"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadForeColor=   16777215
      Groups(1).HeadBackColor=   8421504
      Groups(1).Columns.Count=   2
      Groups(1).Columns(0).Width=   3731
      Groups(1).Columns(0).Caption=   "Apellidos"
      Groups(1).Columns(0).Name=   "APE"
      Groups(1).Columns(0).DataField=   "Column 2"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(1).Width=   1984
      Groups(1).Columns(1).Caption=   "Nombre"
      Groups(1).Columns(1).Name=   "NOM"
      Groups(1).Columns(1).DataField=   "Column 3"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(2).Width =   3625
      Groups(2).Caption=   "Via de notificaci�n"
      Groups(2).HasHeadForeColor=   -1  'True
      Groups(2).HasHeadBackColor=   -1  'True
      Groups(2).HeadForeColor=   16777215
      Groups(2).HeadBackColor=   8421504
      Groups(2).Columns.Count=   11
      Groups(2).Columns(0).Width=   1164
      Groups(2).Columns(0).Caption=   "Web"
      Groups(2).Columns(0).Name=   "WEB"
      Groups(2).Columns(0).DataField=   "Column 4"
      Groups(2).Columns(0).DataType=   8
      Groups(2).Columns(0).FieldLen=   256
      Groups(2).Columns(0).Style=   2
      Groups(2).Columns(1).Width=   1138
      Groups(2).Columns(1).Caption=   "Mail"
      Groups(2).Columns(1).Name=   "MAIL"
      Groups(2).Columns(1).DataField=   "Column 5"
      Groups(2).Columns(1).DataType=   8
      Groups(2).Columns(1).FieldLen=   256
      Groups(2).Columns(1).Style=   2
      Groups(2).Columns(2).Width=   1323
      Groups(2).Columns(2).Caption=   "Carta"
      Groups(2).Columns(2).Name=   "IMP"
      Groups(2).Columns(2).DataField=   "Column 6"
      Groups(2).Columns(2).DataType=   8
      Groups(2).Columns(2).FieldLen=   256
      Groups(2).Columns(2).Style=   2
      Groups(2).Columns(3).Width=   661
      Groups(2).Columns(3).Visible=   0   'False
      Groups(2).Columns(3).Caption=   "EMAIL"
      Groups(2).Columns(3).Name=   "EMAIL"
      Groups(2).Columns(3).DataField=   "Column 7"
      Groups(2).Columns(3).DataType=   8
      Groups(2).Columns(3).FieldLen=   256
      Groups(2).Columns(4).Width=   1640
      Groups(2).Columns(4).Visible=   0   'False
      Groups(2).Columns(4).Caption=   "CODCON"
      Groups(2).Columns(4).Name=   "CODCON"
      Groups(2).Columns(4).DataField=   "Column 8"
      Groups(2).Columns(4).DataType=   8
      Groups(2).Columns(4).FieldLen=   256
      Groups(2).Columns(5).Width=   1058
      Groups(2).Columns(5).Visible=   0   'False
      Groups(2).Columns(5).Caption=   "TFNO"
      Groups(2).Columns(5).Name=   "TFNO"
      Groups(2).Columns(5).DataField=   "Column 9"
      Groups(2).Columns(5).DataType=   8
      Groups(2).Columns(5).FieldLen=   256
      Groups(2).Columns(6).Width=   1138
      Groups(2).Columns(6).Visible=   0   'False
      Groups(2).Columns(6).Caption=   "TFNO2"
      Groups(2).Columns(6).Name=   "TFNO2"
      Groups(2).Columns(6).DataField=   "Column 10"
      Groups(2).Columns(6).DataType=   8
      Groups(2).Columns(6).FieldLen=   256
      Groups(2).Columns(7).Width=   1323
      Groups(2).Columns(7).Visible=   0   'False
      Groups(2).Columns(7).Caption=   "FAX"
      Groups(2).Columns(7).Name=   "FAX"
      Groups(2).Columns(7).DataField=   "Column 11"
      Groups(2).Columns(7).DataType=   8
      Groups(2).Columns(7).FieldLen=   256
      Groups(2).Columns(8).Width=   1746
      Groups(2).Columns(8).Visible=   0   'False
      Groups(2).Columns(8).Caption=   "TFNOMOVIL"
      Groups(2).Columns(8).Name=   "TFNO_MOVIL"
      Groups(2).Columns(8).DataField=   "Column 12"
      Groups(2).Columns(8).DataType=   8
      Groups(2).Columns(8).FieldLen=   256
      Groups(2).Columns(9).Width=   1085
      Groups(2).Columns(9).Visible=   0   'False
      Groups(2).Columns(9).Caption=   "PORTALCON"
      Groups(2).Columns(9).Name=   "PORTALCON"
      Groups(2).Columns(9).DataField=   "Column 13"
      Groups(2).Columns(9).DataType=   8
      Groups(2).Columns(9).FieldLen=   256
      Groups(2).Columns(10).Width=   1429
      Groups(2).Columns(10).Visible=   0   'False
      Groups(2).Columns(10).Caption=   "TIPOEMAIL"
      Groups(2).Columns(10).Name=   "TIPOEMAIL"
      Groups(2).Columns(10).DataField=   "Column 14"
      Groups(2).Columns(10).DataType=   8
      Groups(2).Columns(10).FieldLen=   256
      _ExtentX        =   19711
      _ExtentY        =   3836
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddConApe 
      Height          =   1515
      Left            =   1140
      TabIndex        =   22
      Top             =   3795
      Width           =   5715
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   1
      stylesets(0).Name=   "Tan"
      stylesets(0).BackColor=   10079487
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "FrmOBJAnya.frx":17F3
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   10
      Columns(0).Width=   4022
      Columns(0).Caption=   "Apellidos"
      Columns(0).Name =   "APE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1852
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "NOM"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3757
      Columns(2).Caption=   "Mail"
      Columns(2).Name =   "EMAIL"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "TFNO"
      Columns(4).Name =   "TFNO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "TFNO2"
      Columns(5).Name =   "TFNO2"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "FAX"
      Columns(6).Name =   "FAX"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "TFNOMOVIL"
      Columns(7).Name =   "TFNO_MOVIL"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "PORT"
      Columns(8).Name =   "PORT"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "TIPOEMAIL"
      Columns(9).Name =   "TIPOEMAIL"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      _ExtentX        =   10081
      _ExtentY        =   2672
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveCod 
      Height          =   1635
      Left            =   900
      TabIndex        =   23
      Top             =   3870
      Width           =   5175
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   1
      stylesets(0).Name=   "ProvPortal"
      stylesets(0).BackColor=   10079487
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "FrmOBJAnya.frx":180F
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2725
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6006
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9128
      _ExtentY        =   2884
      _StockProps     =   77
   End
   Begin MSComDlg.CommonDialog cmmdDot 
      Left            =   9480
      Top             =   3300
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveDen 
      Height          =   1575
      Left            =   1620
      TabIndex        =   25
      Top             =   3870
      Width           =   5055
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   1
      stylesets(0).Name=   "ProvPortal"
      stylesets(0).BackColor=   10079487
      stylesets(0).Picture=   "FrmOBJAnya.frx":182B
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5477
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2990
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8916
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddConNom 
      Height          =   1515
      Left            =   2520
      TabIndex        =   26
      Top             =   3840
      Width           =   5775
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   1
      stylesets(0).Name=   "Tan"
      stylesets(0).BackColor=   10079487
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "FrmOBJAnya.frx":1847
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   10
      Columns(0).Width=   1852
      Columns(0).Caption=   "Nombre"
      Columns(0).Name =   "NOM"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4022
      Columns(1).Caption=   "Apellidos"
      Columns(1).Name =   "APE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3942
      Columns(2).Caption=   "Mail"
      Columns(2).Name =   "EMAIL"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "TFNO"
      Columns(4).Name =   "TFNO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "TFNO2"
      Columns(5).Name =   "TFNO2"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "FAX"
      Columns(6).Name =   "FAX"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "TFNOMOVIL"
      Columns(7).Name =   "TFNO_MOVIL"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "PORT"
      Columns(8).Name =   "PORT"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "TIPOEMAIL"
      Columns(9).Name =   "TIPOEMAIL"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   2672
      _StockProps     =   77
   End
   Begin VB.Label lblTZHoraFin 
      BackColor       =   &H00808000&
      Caption         =   "Label11"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8040
      TabIndex        =   39
      Top             =   3255
      Width           =   1215
   End
   Begin VB.Shape Shape4 
      BorderColor     =   &H00FFFFFF&
      Height          =   480
      Left            =   45
      Top             =   1620
      Width           =   9315
   End
   Begin VB.Shape Shape2 
      BorderColor     =   &H00FFFFFF&
      Height          =   1410
      Left            =   45
      Top             =   120
      Width           =   9315
   End
   Begin VB.Label lblForm 
      BackColor       =   &H00808000&
      Caption         =   "Formulario de objetivos:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   150
      TabIndex        =   37
      Top             =   1755
      Width           =   2190
   End
   Begin VB.Shape Shape3 
      BorderColor     =   &H00FFFFFF&
      Height          =   3525
      Left            =   9405
      Top             =   120
      Width           =   1830
   End
   Begin VB.Label lblHoraLimite 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   2445
      TabIndex        =   14
      Top             =   3240
      Width           =   885
   End
   Begin VB.Label lblFecReu 
      BackColor       =   &H00808000&
      Caption         =   "Fijaci�n de objetivos:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   2445
      TabIndex        =   36
      Top             =   2295
      Width           =   1980
   End
   Begin VB.Label LblFecReuBox 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   2445
      TabIndex        =   11
      Top             =   2550
      Width           =   1470
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00808000&
      Caption         =   "DHora"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1665
      TabIndex        =   35
      Top             =   3255
      Width           =   750
   End
   Begin VB.Label lblFecLimOfeNew 
      BackColor       =   &H00808000&
      Caption         =   "DNueva fecha l�mite de ofertas:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4500
      TabIndex        =   34
      Top             =   2955
      Width           =   2295
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00FFFFFF&
      Height          =   1455
      Left            =   45
      Top             =   2190
      Width           =   9315
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00808000&
      Caption         =   "Hora"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6345
      TabIndex        =   33
      Top             =   3255
      Width           =   585
   End
   Begin VB.Label lblFecLimit 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   150
      TabIndex        =   13
      Top             =   3240
      Width           =   1470
   End
   Begin VB.Label lblFecNecBox 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   4500
      TabIndex        =   12
      Top             =   2550
      Width           =   1470
   End
   Begin VB.Label lblFecPres 
      BackColor       =   &H00808000&
      Caption         =   "Presentaci�n:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   150
      TabIndex        =   31
      Top             =   2295
      Width           =   1410
   End
   Begin VB.Label lblFecPresBox 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   150
      TabIndex        =   10
      Top             =   2550
      Width           =   1470
   End
   Begin VB.Label lblFecLimOfeOld 
      BackColor       =   &H00808000&
      Caption         =   "Fecha l�mite de ofertas anterior:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   150
      TabIndex        =   30
      Top             =   2955
      Width           =   3240
   End
   Begin VB.Label lblDOT 
      BackColor       =   &H00808000&
      Caption         =   "Plantilla para carta:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   150
      TabIndex        =   29
      Top             =   300
      Width           =   2115
   End
   Begin VB.Label lblMailDot 
      BackColor       =   &H00808000&
      Caption         =   "Plantilla para e-mail:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   150
      TabIndex        =   28
      Top             =   720
      Width           =   2115
   End
   Begin VB.Label lblWebDot 
      BackColor       =   &H00808000&
      Caption         =   "Plantilla para web:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   150
      TabIndex        =   27
      Top             =   1140
      Width           =   2115
   End
   Begin VB.Label lblFecNec 
      BackColor       =   &H00808000&
      Caption         =   "Fecha de necesidad:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4500
      TabIndex        =   32
      Top             =   2295
      Width           =   2295
   End
End
Attribute VB_Name = "FrmOBJAnya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oFos As Scripting.FileSystemObject
Private oProves As CProveedores
Private oProve As CProveedor
Private oContactos As CContactos
Private oIasig As IAsignaciones  ' Para cargar las combos de proveedores asignables
Private m_oAsigs As CAsignaciones
Private bCargarComboDesdeDD As Boolean
'Guardaremos los nombres de los temporales aqui, para borrarlos al descargarse el formulario
'Desde frmMensaje se borran los temporales que se adjunten alli mismo
Private sayFileNames() As String
' Nombres para mostrar en list view de frmMensaje
Private Nombres() As String
Private appword  As Object
Private m_iWordVer As Integer

Public bModificarFecLimit As Boolean 'Variable para saber si se puede modificar o no la fecha l�mite de ofertas
Public bModSubasta As Boolean 'Variable para saber si se puede modificar o no la fecha l�mite en subastas
Public Anyo As Integer
Public GMN1 As String
Public Cod As Long
Public vTZHora As Variant

'Multilenguaje
Private sIdiPComunicObj As String
'Plantilla para comunicaci�n de objetivos.
Private sIdiPlantilla As String
'Plantilla
Private sIdiFecLimite As String
'Fecha limite de ofertas
Private sIdiGenComunicObj As String
'Generando comunicaci�n de objetivos...
Private sIdiCreandoPet As String
'Creando peticiones...
Private sIdiContacto As String
'Contacto:
Private sIdiInicSesCorreo As String
'Iniciando sesi�n de correo ...
Private sidiInicWord As String
'Iniciando Microsoft Word...
Private sIdiAbrPlant As String
'Abriendo Plantilla
Private sIdiCopiDocEmail As String
'Copiando documento a e-mail...
Private sIdiEnvMensaje As String
'Enviando mensaje ...
Private sIdiAnyaPlant As String
'A�adiendo Plantilla
Private sidiGenImprPet As String
'Generando impreso de peticiones ...
Private sIdiAdjEspec As String
'Adjuntando especificaci�n:
Private sIdiCerrSesCorreo As String
'Cerrando sesi�n de correo...
Private sIdiCreandoImp As String
'Creando impreso de petici�n ...
Private sIdiVisualImp As String
'Visualizando impreso ...
Private sIdiPlantDoc As String
'Plantillas de documento
Private sIdiPlantPetOfe As String
'Plantilla de petici�n de ofertas.
Private sIdiPlantPetOfeMail As String
'Plantilla de petici�n de ofertas para mail.
Private sIdiEmail As String
Private sImpreso As String
'Direcci�n de correo electr�nico
Private sIdiAlmacenadoBD As String

Private m_stxtOferta As String
Private m_stxtGrupo As String
Private m_stxtItem As String
Private m_stxtUniItem As String
Private m_stxtTexto As String
Private m_stxtNumero As String
Private m_stxtFecha As String
Private m_stxtSi As String
Private m_stxtMinimo As String
Private m_stxtMaximo As String
Private m_sHoraLim As String
Private sIdiPreparandoExcel As String

Private m_sSi As String
Private m_sNo As String

Private oProvesAsignados As CProveedores
Private m_oOfertasProceso As COfertas

Public g_bCancelarMail As Boolean
Private m_sProceso As String
Private m_sGrupo As String
Private m_sItem As String

Private m_vDatoAPasar As Variant

''' <summary>Genera el documento word para la comunicaci�n de objetivos</summary>
''' <param name="oPet">Oferta</param>
''' <param name="appword">Objeto aplicaci�n word</param>
''' <returns>Objeto documento word</returns>
''' <remarks>Llamada desde;cmdAceptar_Click Tiempo m�ximo 0</remarks>
''' <revision>LTG 01/12/2011</revision>

Private Function ImpresoObjetivos(ByVal oPet As CPetOferta) As Object
    Dim i As Integer
    Dim rangeword As Object
    Dim rangewordAtr As Object
    Dim rangewordGR As Object
    Dim rangewordAtrGR As Object
    Dim rangewordIT As Object
    Dim rangewordArt As Object
    Dim rangewordITAtr As Object
    Dim rangewordOptIt As Object
    Dim docword As Object
    Dim blankword As Object
    Dim oItem As CItem
    Dim iNumEsp As Integer
    Dim iNumPago As Integer
    Dim INUMDEST As Integer
    Dim ayDest() As String
    Dim ayPago() As String
    Dim sCod As String
    Dim iNumFich As Integer
    Dim oEsp As CEspecificacion
    Dim oGrupo As CGrupo
    Dim oatrib As CAtributo
    Dim iNumAtr As Integer
    Dim Destino As Boolean
    Dim Pago As Boolean
    Dim Fecha As Boolean
    Dim numItem As Integer
    Dim var1 As String
    Dim var2 As Variant
    Dim oElem As CValorPond
    Dim oProves As CProveedores
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim intcontItems As Integer
    Dim sDenGrupo As String
    Dim rangewordAtrEspePRO As Object
    Dim rangewordAtrEspeGR As Object
    Dim rangewordITEspeAtr As Object
    Dim oEsc As CEscalado
            
    If txtForm.Text = "" Then
        oMensajes.NoValida sIdiPComunicObj
        Set ImpresoObjetivos = Nothing
        Exit Function
    End If
                    
    If Right(txtForm.Text, 3) <> "dot" Then
        oMensajes.NoValida sIdiPlantilla & " " & txtForm.Text
        Set ImpresoObjetivos = Nothing
        Exit Function
    End If
        
    If Not oFos.FileExists(txtForm.Text) Then
        oMensajes.PlantillaNoEncontrada txtForm.Text
        Set ImpresoObjetivos = Nothing
        Exit Function
    End If
    
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.Add oPet.CodProve, oPet.DenProve
    oProves.CargarDatosProveedor oPet.CodProve
    
    Set blankword = appword.Documents.Add(txtForm.Text)
    Set docword = appword.Documents.Add(txtForm.Text)
    'appword.Visible = True
    
    If m_iWordVer >= 9 Then
        If appword.Visible Then blankword.activewindow.WindowState = 2
        If appword.Visible Then docword.activewindow.WindowState = 2
    Else
        appword.Visible = True
        appword.Windows(blankword.Name).WindowState = 1
        appword.Windows(docword.Name).WindowState = 1
    End If
    
    INUMDEST = 0
    ReDim ayDest(0)
    iNumPago = 0
    ReDim ayPago(0)
            
    With docword
        'DATOS PROVEEDOR
        If .Bookmarks.Exists("COD_PROVE") Then
            DatoAWord docword, "COD_PROVE", NullToStr(oProves.Item(1).Cod)
        End If
        If .Bookmarks.Exists("DEN_PROVE") Then
            DatoAWord docword, "DEN_PROVE", NullToStr(oProves.Item(1).Den)
        End If
        If .Bookmarks.Exists("DIR_PROVE") Then
            DatoAWord docword, "DIR_PROVE", NullToStr(oProves.Item(1).Direccion)
        End If
        If .Bookmarks.Exists("POBL_PROVE") Then
            DatoAWord docword, "POBL_PROVE", NullToStr(oProves.Item(1).Poblacion)
        End If
        If .Bookmarks.Exists("CP_PROVE") Then
            DatoAWord docword, "CP_PROVE", NullToStr(oProves.Item(1).cP)
        End If
        If .Bookmarks.Exists("PROV_PROVE") Then
            DatoAWord docword, "PROV_PROVE", NullToStr(oProves.Item(1).DenProvi)
        End If
        If .Bookmarks.Exists("PAIS_PROVE") Then
            DatoAWord docword, "PAIS_PROVE", NullToStr(oProves.Item(1).DenPais)
        End If
        If .Bookmarks.Exists("NIF_PROVE") Then
            DatoAWord docword, "NIF_PROVE", NullToStr(oProves.Item(1).NIF)
        End If
        ' DATOS PERSONA CONTACTO
        If .Bookmarks.Exists("NOM_CONTACTO") Then
            DatoAWord docword, "NOM_CONTACTO", NullToStr(oPet.nombre)
        End If
        If .Bookmarks.Exists("APE_CONTACTO") Then
            DatoAWord docword, "APE_CONTACTO", NullToStr(oPet.Apellidos)
        End If
        If .Bookmarks.Exists("TFNO_CONTACTO") Then
            DatoAWord docword, "TFNO_CONTACTO", NullToStr(oPet.Tfno)
        End If
        If .Bookmarks.Exists("FAX_CONTACTO") Then
            DatoAWord docword, "FAX_CONTACTO", NullToStr(oPet.Fax)
        End If
        If .Bookmarks.Exists("MAIL_CONTACTO") Then
            DatoAWord docword, "MAIL_CONTACTO", NullToStr(oPet.Email)
        End If
        If .Bookmarks.Exists("TFNO_MOVIL_CONTACTO") Then
            DatoAWord docword, "TFNO_MOVIL_CONTACTO", NullToStr(oPet.tfnomovil)
        End If
        'DATOS PROCESO
        If .Bookmarks.Exists("NUM_PROCESO") Then
            DatoAWord docword, "NUM_PROCESO", frmOFEPet.sdbcAnyo.Text & "/" & frmOFEPet.sdbcGMN1_4Cod.Text & "/" & frmOFEPet.sdbcProceCod.Text
        End If
        If .Bookmarks.Exists("DEN_PROCESO") Then
            DatoAWord docword, "DEN_PROCESO", frmOFEPet.sdbcProceDen.Text & " "
        End If
        If .Bookmarks.Exists("MAT1_PROCESO") Then
            DatoAWord docword, "MAT1_PROCESO", frmOFEPet.oProcesoSeleccionado.GMN1Cod
        End If
        If .Bookmarks.Exists("FEC_LIMITE") Then
            'DatoAWord docword, "FEC_LIMITE", txtFecLimOfe.Text & " " & Trim(txtHoraLimite.Text)
            DatoAWord docword, "FEC_LIMITE", m_vDatoAPasar
        End If
        
        If frmOFEPet.oProcesoSeleccionado.DefDestino = EnProceso Then
            ayDest(INUMDEST) = frmOFEPet.oProcesoSeleccionado.DestCod
            If .Bookmarks.Exists("COD_DEST_PROCE") Then
                .Bookmarks("COD_DEST_PROCE").Range.Text = ayDest(INUMDEST)
            End If
            If .Bookmarks.Exists("DEN_DEST_PROCE") Then
                .Bookmarks("DEN_DEST_PROCE").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            If .Bookmarks.Exists("DESTINO") Then
                If .Bookmarks.Exists("DEST_COD") Then .Bookmarks("DEST_COD").Range.Text = ayDest(INUMDEST)
                If .Bookmarks.Exists("DEST_DIR") Then .Bookmarks("DEST_DIR").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).dir)
                If .Bookmarks.Exists("DEST_POB") Then .Bookmarks("DEST_POB").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).POB)
                If .Bookmarks.Exists("DEST_CP") Then .Bookmarks("DEST_CP").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).cP)
                If .Bookmarks.Exists("DEST_DEN") Then .Bookmarks("DEST_DEN").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                If .Bookmarks.Exists("DEST_PROV") Then .Bookmarks("DEST_PROV").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Provi)
                If .Bookmarks.Exists("DEST_PAIS") Then .Bookmarks("DEST_PAIS").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Pais)
                .Bookmarks("DESTINO").Delete
            End If
            If .Bookmarks.Exists("PROCE_DEST") Then .Bookmarks("PROCE_DEST").Delete
        Else
            If .Bookmarks.Exists("PROCE_DEST") Then
                .Bookmarks("PROCE_DEST").Select
                .Application.Selection.Delete
            End If
        End If
        
        If frmOFEPet.oProcesoSeleccionado.DefFormaPago = EnProceso Then
            If .Bookmarks.Exists("COD_PAGO_PROCE") Then
                .Bookmarks("COD_PAGO_PROCE").Range.Text = frmOFEPet.oProcesoSeleccionado.PagCod
            End If
            If .Bookmarks.Exists("DEN_PAGO_PROCE") Then
                .Bookmarks("DEN_PAGO_PROCE").Range.Text = NullToStr(frmOFEPet.oPagos.Item(frmOFEPet.oProcesoSeleccionado.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            If .Bookmarks.Exists("PROCE_PAGO") Then .Bookmarks("PROCE_PAGO").Delete
            If .Bookmarks.Exists("PAGO") Then
                .Bookmarks("PAGO").Select
                .Application.Selection.cut
            End If
        Else
            If .Bookmarks.Exists("PROCE_PAGO") Then
                .Bookmarks("PROCE_PAGO").Select
                .Application.Selection.cut
            End If
        End If
        
        If frmOFEPet.oProcesoSeleccionado.DefFechasSum = EnProceso Then
            If .Bookmarks.Exists("FINI_SUM_PROCE") Then
                .Bookmarks("FINI_SUM_PROCE").Range.Text = Format(NullToStr(frmOFEPet.oProcesoSeleccionado.FechaInicioSuministro), "Short Date")
            End If
            If .Bookmarks.Exists("FFIN_SUM_PROCE") Then
                .Bookmarks("FFIN_SUM_PROCE").Range.Text = Format(NullToStr(frmOFEPet.oProcesoSeleccionado.FechaFinSuministro), "Short Date")
            End If
            If .Bookmarks.Exists("PROCE_FECSUM") Then .Bookmarks("PROCE_FECSUM").Delete
        Else
            If .Bookmarks.Exists("PROCE_FECSUM") Then
                .Bookmarks("PROCE_FECSUM").Select
                .Application.Selection.cut
            End If
        End If
        
        'ATRIBUTOS DE PROCESO
        frmOFEPet.oProcesoSeleccionado.CargarAtributos frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, True, AmbProceso, , True
        
        If .Bookmarks.Exists("PROCE_ATR") Then
            .Bookmarks("PROCE_ATR").Select
            Set rangewordAtr = blankword.Bookmarks("PROCE_ATR").Range
            rangewordAtr.Copy
            .Application.Selection.cut
    
            If frmOFEPet.oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
                If .Bookmarks.Exists("PROCE_ATRIBUTOS") Then
                    .Bookmarks("PROCE_ATRIBUTOS").Select
                    .Application.Selection.cut
                End If
            Else
                 For Each oatrib In frmOFEPet.oProcesoSeleccionado.ATRIBUTOS
                    rangewordAtr.Copy
                    .Application.Selection.Paste
                    If .Bookmarks.Exists("COD_ATR_PROCE") Then
                        .Bookmarks("COD_ATR_PROCE").Range.Text = NullToStr(oatrib.Cod)
                    End If
                    If .Bookmarks.Exists("DEN_ATR_PROCE") Then
                        .Bookmarks("DEN_ATR_PROCE").Range.Text = NullToStr(oatrib.Den)
                    End If
                    If .Bookmarks.Exists("TIPO_ATR_PROCE") Then
                         
                        Select Case oatrib.Tipo
                            Case 1:
                                .Bookmarks("TIPO_ATR_PROCE").Range.Text = NullToStr(m_stxtTexto)
    
                            Case 2:
                                .Bookmarks("TIPO_ATR_PROCE").Range.Text = NullToStr(m_stxtNumero)
    
                            Case 3:
                                .Bookmarks("TIPO_ATR_PROCE").Range.Text = NullToStr(m_stxtFecha)
    
                            Case 4:
                                .Bookmarks("TIPO_ATR_PROCE").Range.Text = NullToStr(m_stxtSi)
    
                        End Select
    
                    End If
                    
                    If .Bookmarks.Exists("VALOR_ATR_PROCE") Then
                        
                        If oatrib.TipoIntroduccion = IntroLibre Then
                            If Not IsNull(oatrib.Minimo) And Not IsNull(oatrib.Maximo) Then
                                var1 = m_stxtMinimo & " : " & oatrib.Minimo & vbCrLf & m_stxtMaximo & " : " & oatrib.Maximo
                                .Bookmarks("VALOR_ATR_PROCE").Range.Text = var1
                            ElseIf Not IsNull(oatrib.Minimo) Then
                                var1 = m_stxtMinimo & " : " & oatrib.Minimo
                                .Bookmarks("VALOR_ATR_PROCE").Range.Text = var1
                            ElseIf Not IsNull(oatrib.Maximo) Then
                                var1 = m_stxtMaximo & " : " & oatrib.Maximo
                                .Bookmarks("VALOR_ATR_PROCE").Range.Text = var1
                            End If
                        End If
                        If oatrib.TipoIntroduccion = Introselec Then
                            var1 = ""
                            For Each oElem In oatrib.ListaPonderacion
                                var1 = var1 & "#@@# " & CStr(oElem.ValorLista) & vbCrLf
                            Next
                            If var1 <> "" Then
                                .Bookmarks("VALOR_ATR_PROCE").Range.Text = var1
                            End If
                        End If
                        .Bookmarks("VALOR_ATR_PROCE").Delete
                    
                    End If
                    
                    If .Bookmarks.Exists("OP_ATR_PROCE") Then
                        .Bookmarks("OP_ATR_PROCE").Range.Text = NullToStr(oatrib.PrecioFormula)
                        .Bookmarks("OP_ATR_PROCE").Delete
                    End If
                    If .Bookmarks.Exists("APL_ATR_PROCE") Then
                        Select Case oatrib.PrecioAplicarA
                        Case 0:
                            .Bookmarks("APL_ATR_PROCE").Range.Text = NullToStr(m_stxtOferta)
                        Case 1:
                            .Bookmarks("APL_ATR_PROCE").Range.Text = NullToStr(m_stxtGrupo)
                        Case 2:
                            .Bookmarks("APL_ATR_PROCE").Range.Text = NullToStr(m_stxtItem)
                        Case 3:
                            .Bookmarks("APL_ATR_PROCE").Range.Text = NullToStr(m_stxtUniItem)
                        End Select
                        .Bookmarks("APL_ATR_PROCE").Delete
                    End If
                    
                Next
    
            End If
        End If 'ATRIBUTOS
        
        'GRUPOS
        numItem = 1
        If .Bookmarks.Exists("GRUPO") Then
            .Bookmarks("GRUPO").Select
            Set rangewordGR = blankword.Bookmarks("GRUPO").Range
        End If
    
        If frmOFEPet.oProcesoSeleccionado.Grupos.Count = 0 Then
            If .Bookmarks.Exists("GRUPO") Then
                .Bookmarks("GRUPO").Select
                .Application.Selection.cut
            End If
        Else
        
            If docword.Bookmarks.Exists("GRUPO") Then
                scodProve = oProves.Item(1).Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProves.Item(1).Cod))
                iNumEsp = 1 'cONTADOR PARA LOS ITEMS
                For Each oGrupo In frmOFEPet.oProcesoSeleccionado.Grupos
                bCargar = True
                If gParametrosGenerales.gbProveGrupos Then
                    If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                        bCargar = False
                    End If
                End If
                If bCargar Then
                    If oGrupo.UsarEscalados Then oGrupo.CargarEscalados
                    
                    oGrupo.CargarTodosLosItems OrdItemPorOrden
                    rangewordGR.Copy
                    .Application.Selection.Paste
                    If .Bookmarks.Exists("COD_GRUPO") Then
                        If IsNull(oGrupo.Sobre) Then
                            .Bookmarks("COD_GRUPO").Range.Text = oGrupo.Codigo
                        Else
                            .Bookmarks("COD_GRUPO").Range.Text = "(" & oGrupo.Sobre & ") " & oGrupo.Codigo
                        End If
                    End If
                    If .Bookmarks.Exists("DEN_GRUPO") Then .Bookmarks("DEN_GRUPO").Range.Text = oGrupo.Den
                    If oGrupo.DefDestino Then
                        If .Bookmarks.Exists("COD_DEST_GR") Then
                            .Bookmarks("COD_DEST_GR").Range.Text = NullToStr(oGrupo.DestCod)
                        End If
                        If .Bookmarks.Exists("DEN_DEST_GR") Then
                            .Bookmarks("DEN_DEST_GR").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                        End If
                        If .Bookmarks.Exists("DESTINO") Then
                            If Not BuscarEnArray(ayDest, oGrupo.DestCod) Then
                                ayDest(INUMDEST) = oGrupo.DestCod
                                INUMDEST = INUMDEST + 1
                                ReDim Preserve ayDest(UBound(ayDest) + 1)
                            End If
                        End If
                        If .Bookmarks.Exists("GR_DEST") Then .Bookmarks("GR_DEST").Delete
                    Else
                        If .Bookmarks.Exists("GR_DEST") Then
                            .Bookmarks("GR_DEST").Select
                            .Application.Selection.cut
                        End If
                    End If
                    If oGrupo.DefFormaPago Then
                        If .Bookmarks.Exists("COD_PAGO_GR") Then
                            .Bookmarks("COD_PAGO_GR").Range.Text = NullToStr(oGrupo.PagCod)
                        End If
                        If .Bookmarks.Exists("DEN_PAGO_GR") Then
                            .Bookmarks("DEN_PAGO_GR").Range.Text = NullToStr(frmOFEPet.oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                        End If
                        If .Bookmarks.Exists("PAGO") Then
                            If Not BuscarEnArray(ayPago, oGrupo.PagCod) Then
                                ayPago(iNumPago) = oGrupo.PagCod
                                iNumPago = iNumPago + 1
                                ReDim Preserve ayPago(UBound(ayPago) + 1)
                            End If
                        End If
                        If .Bookmarks.Exists("GR_PAGO") Then .Bookmarks("GR_PAGO").Delete
                    Else
                        If .Bookmarks.Exists("GR_PAGO") Then
                            .Bookmarks("GR_PAGO").Select
                            .Application.Selection.cut
                        End If
                    End If
                    If oGrupo.DefFechasSum Then
                        If .Bookmarks.Exists("FINI_SUM_GR") Then
                            .Bookmarks("FINI_SUM_GR").Range.Text = Format(NullToStr(oGrupo.FechaInicioSuministro), "Short Date")
                        End If
                        If .Bookmarks.Exists("FFIN_SUM_GR") Then
                            .Bookmarks("FFIN_SUM_GR").Range.Text = Format(NullToStr(oGrupo.FechaFinSuministro), "Short Date")
                        End If
                        If .Bookmarks.Exists("GR_FECSUM") Then .Bookmarks("GR_FECSUM").Delete
                    Else
                        If .Bookmarks.Exists("GR_FECSUM") Then
                            .Bookmarks("GR_FECSUM").Select
                            .Application.Selection.cut
                        End If
                    End If
                     
                    oGrupo.CargarAtributos oGrupo.Codigo, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, True, AmbGrupo, True
                    
                    'ATRIBUTOS DE GRUPO
                    If .Bookmarks.Exists("GRUPO_ATR") Then
                        .Bookmarks("GRUPO_ATR").Select
                        Set rangewordAtrGR = blankword.Bookmarks("GRUPO_ATR").Range
                        rangewordAtrGR.Copy
    
                        .Application.Selection.cut
                        If oGrupo.ATRIBUTOS.Count = 0 Then
                            If .Bookmarks.Exists("GRUPO_ATRIBUTOS") Then
                                .Bookmarks("GRUPO_ATRIBUTOS").Select
                                .Application.Selection.cut
                                .Application.Selection.Delete
                            End If
                        Else
                
                             For Each oatrib In oGrupo.ATRIBUTOS
                                 rangewordAtrGR.Copy
                                 
                                 .Application.Selection.Paste
                                                     
                                 If .Bookmarks.Exists("COD_ATR_GRUPO") Then
                                     .Bookmarks("COD_ATR_GRUPO").Range.Text = NullToStr(oatrib.Cod)
                                 End If
                                 If .Bookmarks.Exists("DEN_ATR_GRUPO") Then
                                     .Bookmarks("DEN_ATR_GRUPO").Range.Text = NullToStr(oatrib.Den)
                                 End If
                                 If .Bookmarks.Exists("TIPO_ATR_GRUPO") Then
                                     Select Case oatrib.Tipo
                                         Case 1:
                                             .Bookmarks("TIPO_ATR_GRUPO").Range.Text = NullToStr(m_stxtTexto)
                                         Case 2:
                                             .Bookmarks("TIPO_ATR_GRUPO").Range.Text = NullToStr(m_stxtNumero)
                                         Case 3:
                                             .Bookmarks("TIPO_ATR_GRUPO").Range.Text = NullToStr(m_stxtFecha)
                                         Case 4:
                                             .Bookmarks("TIPO_ATR_GRUPO").Range.Text = NullToStr(m_stxtSi)
                                     End Select
                                 
                                 End If
                                 If .Bookmarks.Exists("VALOR_ATR_GRUPO") Then
                                     
                                     If oatrib.TipoIntroduccion = IntroLibre Then
                                         If Not IsNull(oatrib.Minimo) And Not IsNull(oatrib.Maximo) Then
                    
                                             var1 = m_stxtMinimo & " : " & oatrib.Minimo & vbCrLf & m_stxtMaximo & " : " & oatrib.Maximo
                                             .Bookmarks("VALOR_ATR_GRUPO").Range.Text = var1
                                         End If
                                         If Not IsNull(oatrib.Minimo) And IsNull(oatrib.Maximo) Then
                                             
                                             var1 = m_stxtMinimo & " : " & oatrib.Minimo
                                             .Bookmarks("VALOR_ATR_GRUPO").Range.Text = var1
                                         
                                         End If
                                         If IsNull(oatrib.Minimo) And Not IsNull(oatrib.Maximo) Then
                                             
                                             var1 = m_stxtMaximo & " : " & oatrib.Maximo
                                             .Bookmarks("VALOR_ATR_GRUPO").Range.Text = var1
                                         
                                         End If
                                     End If
                                     
                                     If oatrib.TipoIntroduccion = Introselec Then
                                      
                                         var1 = ""
                                         var2 = "#@@#"
                                         For Each oElem In oatrib.ListaPonderacion
                                             var1 = var1 & var2 & " " & CStr(oElem.ValorLista) & vbCrLf
                                         Next
                                         If var1 <> "" Then
                                             .Bookmarks("VALOR_ATR_GRUPO").Range.Text = var1
                                         End If
                                     End If
                                    .Bookmarks("VALOR_ATR_GRUPO").Delete
                                 
                                 End If
                                 
                                 If .Bookmarks.Exists("OP_ATR_GRUPO") Then
                                     .Bookmarks("OP_ATR_GRUPO").Range.Text = NullToStr(oatrib.PrecioFormula)
                                     .Bookmarks("OP_ATR_GRUPO").Delete
                                 End If
                                 If .Bookmarks.Exists("APL_ATR_GRUPO") Then
                                     Select Case oatrib.PrecioAplicarA
                                         Case 0:
                                             .Bookmarks("APL_ATR_GRUPO").Range.Text = NullToStr(m_stxtOferta)
                                         Case 1:
                                             .Bookmarks("APL_ATR_GRUPO").Range.Text = NullToStr(m_stxtGrupo)
                                         Case 2:
                                             .Bookmarks("APL_ATR_GRUPO").Range.Text = NullToStr(m_stxtItem)
                                         Case 3:
                                             .Bookmarks("APL_ATR_GRUPO").Range.Text = NullToStr(m_stxtUniItem)
                                     End Select
                                     .Bookmarks("APL_ATR_GRUPO").Delete
                                 End If
    
                            Next
                            If .Bookmarks.Exists("GRUPO_ATR") Then .Bookmarks("GRUPO_ATR").Delete
                            If .Bookmarks.Exists("GRUPO_ATRIBUTOS") Then .Bookmarks("GRUPO_ATRIBUTOS").Delete
       
                        End If
                    End If
                    
                    If (frmOFEPet.oProcesoSeleccionado.DefDestino = EnItem) Or (frmOFEPet.oProcesoSeleccionado.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
                        Destino = True
                    Else
                        Destino = False
                    End If
                    If frmOFEPet.oProcesoSeleccionado.DefFormaPago = EnItem Or (frmOFEPet.oProcesoSeleccionado.DefFormaPago = EnGrupo And Not oGrupo.DefFormaPago) Then
                        Pago = True
                    Else
                        Pago = False
                    End If
                    If frmOFEPet.oProcesoSeleccionado.DefFechasSum = EnItem Or (frmOFEPet.oProcesoSeleccionado.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
                        Fecha = True
                    Else
                        Fecha = False
                    End If
                   
                    oGrupo.CargarAtributos oGrupo.Codigo, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, True, AmbItem, True
                        
                    If docword.Bookmarks.Exists("ITEM_TOD") Then
                        .Bookmarks("ITEM_TOD").Select
                        .Application.Selection.cut
                        Set rangewordIT = blankword.Bookmarks("ITEM_TOD").Range
    
                        If oGrupo.Items.Count = 0 Then
                            .Bookmarks("ITEM_TOD").Delete
                        Else
                            For Each oItem In oGrupo.Items
                                rangewordIT.Copy
                                .Application.Selection.Paste
                                                                                                
                                If .Bookmarks.Exists("ARTICULO") Then
                                    If numItem > 1 Then
                                        .Bookmarks("ARTICULO").Select
                                        .Application.Selection.cut
                                    Else
                                        .Bookmarks("ARTICULO").Delete
                                    End If
                                End If
                                
                                FSGSLibrary.DatosConfigurablesAWord docword, "ITEM_DEST", "ITEM_PAGO", "ITEM_FECSUM", Destino, Pago, Fecha, "ESCALADO", (oGrupo.UsarEscalados = 1)
                                
                                If .Bookmarks.Exists("ITEM") Then
                                    Set rangewordArt = docword.Bookmarks("ITEM").Range
                                    .Bookmarks("ITEM").Select
                                    .Application.Selection.cut
                                        
                                    If oGrupo.UsarEscalados Then
                                        For Each oEsc In oGrupo.Escalados
                                            ImpresoObjetivosArticulo docword, rangewordArt, iNumEsp, oItem, Destino, Pago, Fecha, ayDest, INUMDEST, ayPago, iNumPago, oEsc, oGrupo.TipoEscalados
                                        Next
                                    Else
                                        ImpresoObjetivosArticulo docword, rangewordArt, iNumEsp, oItem, Destino, Pago, Fecha, ayDest, INUMDEST, ayPago, iNumPago
                                    End If
                                    .Bookmarks("ITEM").Delete
                                End If
                                
                                iNumEsp = iNumEsp + 1
                                numItem = numItem + 1
                        
                                'ATRIBUTOS DE ITEM
                                If .Bookmarks.Exists("ITEM_ATR") Then
                                    If .Bookmarks.Exists("ITEM_ATR_ESC") Then
                                        If oGrupo.UsarEscalados Then
                                            MostrarMarcador docword, "ITEM_ATR_ESC", True
                                        Else
                                            MostrarMarcador docword, "ITEM_ATR_ESC", False
                                        End If
                                    End If
                                    
                                    .Bookmarks("ITEM_ATR").Select
                                    Set rangewordITAtr = blankword.Bookmarks("ITEM_ATR").Range
                                    rangewordITAtr.Copy
                                    .Application.Selection.cut
            
                                    If oGrupo.AtributosItem.Count = 0 Then
                                        If .Bookmarks.Exists("ITEM_ATRIBUTOS") Then
                                            .Bookmarks("ITEM_ATRIBUTOS").Select
                                            .Application.Selection.cut
                                        End If
                                    Else
                                        iNumAtr = 1
                                        For Each oatrib In oGrupo.AtributosItem
                                            If oatrib.ambito = AmbItem Then
                                                If oGrupo.UsarEscalados Then
                                                    For Each oEsc In oGrupo.Escalados
                                                        ImpresoOfertaAtributo docword, rangewordITAtr, oatrib, oEsc, oGrupo.TipoEscalados
                                                    Next
                                                    Set oEsc = Nothing
                                                Else
                                                    ImpresoOfertaAtributo docword, rangewordITAtr, oatrib
                                                End If
                                                                                            
                                                iNumAtr = iNumAtr + 1
                                            Else
                                                .Bookmarks("ITEM_ATR").Delete
                                                If .Bookmarks.Exists("ITEM_ATRIBUTOS") Then .Bookmarks("ITEM_ATRIBUTOS").Delete
                                            End If
                                            .Bookmarks("ITEM_ATR").Delete
                                            If .Bookmarks.Exists("ITEM_ATRIBUTOS") Then .Bookmarks("ITEM_ATRIBUTOS").Delete
                                        Next
                                    End If
                                End If 'FIN DE ATRIBUTOS DE ITEM
                                
                            'ATRIBUTOS DE ESPECIFICACION DE ITEM
                            If .Bookmarks.Exists("ATRIB_I_ESP") Then
                                .Bookmarks("ATRIB_I_ESP").Select
                                Set rangewordITAtr = .Bookmarks("ATRIB_I_ESP").Range
                                rangewordITAtr.Copy
                                .Application.Selection.cut
                                oItem.CargarTodosLosAtributosEspecificacion
                                If oItem.AtributosEspecificacion.Count = 0 Then
                                    If .Bookmarks.Exists("ITEM_ATRIB_ESP") Then
                                        .Bookmarks("ITEM_ATRIB_ESP").Select
                                        .Application.Selection.cut
                                    End If
                                Else
                                    iNumAtr = 0
                                    For Each oatrib In oItem.AtributosEspecificacion
                                        If Not oatrib.interno And _
                                            (oatrib.Tipo = TiposDeAtributos.TipoString And Not IsNull(oatrib.valorText) Or oatrib.Tipo = TiposDeAtributos.TipoNumerico And Not IsNull(oatrib.valorNum) Or oatrib.Tipo = TiposDeAtributos.TipoFecha And Not IsNull(oatrib.valorFec) Or oatrib.Tipo = TiposDeAtributos.TipoBoolean And Not IsNull(oatrib.valorBool)) Then
                                            .Application.Selection.Paste
                                            If .Bookmarks.Exists("COD_ATR_I_ESP") Then _
                                                .Bookmarks("COD_ATR_I_ESP").Range.Text = oatrib.Cod
                                            If .Bookmarks.Exists("DEN_ATR_I_ESP") Then _
                                                .Bookmarks("DEN_ATR_I_ESP").Range.Text = oatrib.Den
                                            If .Bookmarks.Exists("VALOR_ATR_I_ESP") Then
                                                Select Case oatrib.Tipo
                                                    Case TiposDeAtributos.TipoString:
                                                        .Bookmarks("VALOR_ATR_I_ESP").Range.Text = oatrib.valorText
                                                    Case TiposDeAtributos.TipoNumerico:
                                                        .Bookmarks("VALOR_ATR_I_ESP").Range.Text = oatrib.valorNum
                                                    Case TiposDeAtributos.TipoFecha:
                                                        .Bookmarks("VALOR_ATR_I_ESP").Range.Text = oatrib.valorFec
                                                    Case TiposDeAtributos.TipoBoolean:
                                                        .Bookmarks("VALOR_ATR_I_ESP").Range.Text = IIf(oatrib.valorBool, m_sSi, m_sNo)
                                                End Select
                                            End If
                                            iNumAtr = iNumAtr + 1
                                            .Bookmarks("ATRIB_I_ESP").Delete
                                            If .Bookmarks.Exists("ITEM_ATRIB_ESP") Then .Bookmarks("ITEM_ATRIB_ESP").Delete
                                        End If
                                    Next
                                    If iNumAtr = 0 Then
                                        '.Application.Selection.Paste
                                        If .Bookmarks.Exists("ITEM_ATRIB_ESP") Then
                                            .Bookmarks("ITEM_ATRIB_ESP").Select
                                            .Application.Selection.cut
                                        End If
                                    End If
                                End If
                            End If
                                
                            If .Bookmarks.Exists("ITEM_TOD") Then .Bookmarks("ITEM_TOD").Delete
                        
                            Next 'ITEM
                            If .Bookmarks.Exists("ITEM") Then .Bookmarks("ITEM").Delete
                        End If
                    End If
                    'ESPECIFICACIONES A NIVEL DE ITEM
                    iNumEsp = 1
                    If frmOFEPet.oProcesoSeleccionado.DefEspItems = True Then
                        If chkEsp.Value = vbChecked Then
                            If .Bookmarks.Exists("ITEM_ESP") Then
                                .Bookmarks("ITEM_ESP").Select
                                Set rangeword = .Bookmarks("ITEM_ESP").Range
                                rangeword.Copy
                                .Application.Selection.cut
                                 iNumFich = 0
                                For Each oItem In oGrupo.Items
                                     If oItem.CargarEspGeneral <> "" Then
                                        .Application.Selection.Paste
                                        If .Bookmarks.Exists("DEN_ESP_ITEM") Then .Bookmarks("DEN_ESP_ITEM").Range.Text = NullToStr(oItem.esp)
                                        If .Bookmarks.Exists("NUM_ESP_ITEM") Then .Bookmarks("NUM_ESP_ITEM").Range.Text = iNumEsp
                                        If .Bookmarks.Exists("ITEM_ESP") Then .Bookmarks("ITEM_ESP").Delete
                                        iNumFich = iNumFich + 1
                                    End If
                                    iNumEsp = iNumEsp + 1
                                Next
                                If iNumFich = 0 Then
                                    If .Bookmarks.Exists("ITEM_ESPEC") Then
                                        .Bookmarks("ITEM_ESPEC").Range.cut
                                    End If
                                Else
                                    .Bookmarks("ITEM_ESPEC").Delete
                                End If
                            End If
                    
                        Else
                            If .Bookmarks.Exists("ITEM_ESPEC") Then
                                .Bookmarks("ITEM_ESPEC").Range.cut
                            End If
                        End If
                    
                        iNumEsp = 1
                        'FICHEROS ADJUNTOS
                        If chkFich.Value = vbChecked Then
                            If .Bookmarks.Exists("ITEM_ADJ") Then
                                .Bookmarks("ITEM_ADJ").Select
                                Set rangeword = .Bookmarks("ITEM_ADJ").Range
                                rangeword.Copy
                                .Application.Selection.cut
                                iNumFich = 0
                                For Each oItem In oGrupo.Items
                                    oItem.CargarTodasLasEspecificaciones
                                    If oItem.especificaciones.Count <> 0 Then
                                        For Each oEsp In oItem.especificaciones
                                                .Application.Selection.Paste
                                                If .Bookmarks.Exists("NUM_ADJ_ITEM") Then .Bookmarks("NUM_ADJ_ITEM").Range.Text = iNumEsp
                                                If .Bookmarks.Exists("NOM_ADJ_ITEM") Then .Bookmarks("NOM_ADJ_ITEM").Range.Text = CStr(oEsp.nombre)
                                                If .Bookmarks.Exists("FEC_ADJ_ITEM") Then .Bookmarks("FEC_ADJ_ITEM").Range.Text = Format(oEsp.Fecha, "Short Date")
                                                If .Bookmarks.Exists("DEN_ADJ_ITEM") Then .Bookmarks("DEN_ADJ_ITEM").Range.Text = NullToStr(oEsp.Comentario)
                                                If .Bookmarks.Exists("ITEM_ADJ") Then .Bookmarks("ITEM_ADJ").Delete
                                        Next
                                        iNumFich = iNumFich + 1
                                    End If
                                    iNumEsp = iNumEsp + 1
                                Next
                                If iNumFich = 0 Then
                                    If .Bookmarks.Exists("ITEM_ADJ") Then
                                        .Bookmarks("ITEM_ADJ").Range.cut
                                    End If
                                    If .Bookmarks.Exists("ITEM_ADJUNTOS") Then
                                        .Bookmarks("ITEM_ADJUNTOS").Range.cut
                                    End If
                                Else
                                    If .Bookmarks.Exists("ITEM_ADJUNTOS") Then .Bookmarks("ITEM_ADJUNTOS").Delete
                                End If
                    
                            Else
                                If .Bookmarks.Exists("ITEM_ADJ") Then
                                    .Bookmarks("ITEM_ADJ").Range.cut
                                End If
                                If .Bookmarks.Exists("ITEM_ADJUNTOS") Then
                                    .Bookmarks("ITEM_ADJUNTOS").Range.cut
                                End If
                            End If
                        Else
                            If .Bookmarks.Exists("ITEM_ADJ") Then
                                .Bookmarks("ITEM_ADJ").Range.cut
                            End If
                            If .Bookmarks.Exists("ITEM_ADJUNTOS") Then
                                .Bookmarks("ITEM_ADJUNTOS").Range.cut
                            End If
                        End If
                    
                    Else
                        If .Bookmarks.Exists("ITEM_ADJUNTOS") Then
                            .Bookmarks("ITEM_ADJUNTOS").Range.cut
                        End If
                        If .Bookmarks.Exists("ITEM_ESPEC") Then
                            .Bookmarks("ITEM_ESPEC").Range.cut
                        End If
                    End If
                    
                    'ATRIBUTOS DE ESPECIFICACION DE GRUPO
                    If .Bookmarks.Exists("ATRIB_G_ESP") Then
                        .Bookmarks("ATRIB_G_ESP").Select
                        Set rangewordITAtr = .Bookmarks("ATRIB_G_ESP").Range
                        rangewordITAtr.Copy
                        .Application.Selection.cut
                        oGrupo.CargarAEspecificacionesG
                        If oGrupo.AtributosEspecificacion.Count = 0 Then
                            .Application.Selection.Paste
                            If .Bookmarks.Exists("GRUPO_ATRIB_ESP") Then
                                .Bookmarks("GRUPO_ATRIB_ESP").Select
                                .Application.Selection.cut
                            End If
                        Else
                            iNumAtr = 0
                            For Each oatrib In oGrupo.AtributosEspecificacion
                                If Not oatrib.interno Then
                                    .Application.Selection.Paste
                                    If .Bookmarks.Exists("COD_ATR_G_ESP") Then _
                                        .Bookmarks("COD_ATR_G_ESP").Range.Text = oatrib.Cod
                                    If .Bookmarks.Exists("DEN_ATR_G_ESP") Then _
                                        .Bookmarks("DEN_ATR_G_ESP").Range.Text = oatrib.Den
                                    If .Bookmarks.Exists("VALOR_ATR_G_ESP") Then
                                        Select Case oatrib.Tipo
                                            Case TiposDeAtributos.TipoString:
                                                .Bookmarks("VALOR_ATR_G_ESP").Range.Text = oatrib.valorText
                                            Case TiposDeAtributos.TipoNumerico:
                                                .Bookmarks("VALOR_ATR_G_ESP").Range.Text = oatrib.valorNum
                                            Case TiposDeAtributos.TipoFecha:
                                                .Bookmarks("VALOR_ATR_G_ESP").Range.Text = oatrib.valorFec
                                            Case TiposDeAtributos.TipoBoolean:
                                                .Bookmarks("VALOR_ATR_G_ESP").Range.Text = IIf(oatrib.valorBool, m_sSi, m_sNo)
                                        End Select
                                    End If
                                    iNumAtr = iNumAtr + 1
                                    If .Bookmarks.Exists("ATRIB_G_ESP") Then .Bookmarks("ATRIB_G_ESP").Delete
                                    If .Bookmarks.Exists("GRUPO_ATRIB_ESP") Then .Bookmarks("GRUPO_ATRIB_ESP").Delete
                                End If
                            Next
                            If iNumAtr = 0 Then
                                If .Bookmarks.Exists("GRUPO_ATRIB_ESP") Then
                                    .Bookmarks("GRUPO_ATRIB_ESP").Select
                                    .Application.Selection.cut
                                End If
                            End If
                        End If
                    End If
    
                    If .Bookmarks.Exists("GRUPO") Then .Bookmarks("GRUPO").Delete
                End If 'bCargar
                Next 'GRUPO
            End If
        End If
    
    
        If .Bookmarks.Exists("DESTINO") Then
            Set rangeword = blankword.Bookmarks("DESTINO").Range
             .Bookmarks("DESTINO").Select
             .Application.Selection.cut
             
             For INUMDEST = 0 To UBound(ayDest) - 1
                 rangeword.Copy
                 .Application.Selection.Paste
                 If .Bookmarks.Exists("DEST_COD") Then .Bookmarks("DEST_COD").Range.Text = ayDest(INUMDEST)
                 If .Bookmarks.Exists("DEST_DIR") Then .Bookmarks("DEST_DIR").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).dir)
                 If .Bookmarks.Exists("DEST_POB") Then .Bookmarks("DEST_POB").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).POB)
                 If .Bookmarks.Exists("DEST_CP") Then .Bookmarks("DEST_CP").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).cP)
                 If .Bookmarks.Exists("DEST_DEN") Then .Bookmarks("DEST_DEN").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                 If .Bookmarks.Exists("DEST_PROV") Then .Bookmarks("DEST_PROV").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Provi)
                 If .Bookmarks.Exists("DEST_PAIS") Then .Bookmarks("DEST_PAIS").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Pais)
             Next
         End If
         
         If .Bookmarks.Exists("PAGO") Then
                .Bookmarks("PAGO").Select
                .Application.Selection.cut
                Set rangeword = blankword.Bookmarks("PAGO").Range
                For iNumPago = 0 To UBound(ayPago) - 1
                     rangeword.Copy
                     .Application.Selection.Paste
                     If .Bookmarks.Exists("PAGO_COD") Then .Bookmarks("PAGO_COD").Range.Text = ayPago(iNumPago)
                     If .Bookmarks.Exists("PAGO_DEN") Then .Bookmarks("PAGO_DEN").Range.Text = NullToStr(frmOFEPet.oPagos.Item(ayPago(iNumPago)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                 Next
         End If
       
        'Especificaciones a nivel de grupo
        If frmOFEPet.oProcesoSeleccionado.DefEspGrupos = True Then
            If chkEsp.Value = vbChecked Then
                If .Bookmarks.Exists("GRUPO_ESP") Then
                    .Bookmarks("GRUPO_ESP").Select
                    Set rangeword = blankword.Bookmarks("GRUPO_ESP").Range
                    rangeword.Copy
                    .Application.Selection.cut
                    iNumFich = 0
                    For Each oGrupo In frmOFEPet.oProcesoSeleccionado.Grupos
                         If oGrupo.esp <> "" Then
                            .Application.Selection.Paste
                            
                            If .Bookmarks.Exists("NUM_ESP_GRUPO") Then .Bookmarks("NUM_ESP_GRUPO").Range.Text = NullToStr(oGrupo.Codigo)
                            If .Bookmarks.Exists("DEN_ESP_GRUPO") Then .Bookmarks("DEN_ESP_GRUPO").Range.Text = NullToStr(oGrupo.esp)
                            .Bookmarks("GRUPO_ESP").Delete
                            iNumFich = iNumFich + 1
                        End If
                    Next
                    If iNumFich = 0 Then
                        If .Bookmarks.Exists("GRUPO_ESPEC") Then
                            .Bookmarks("GRUPO_ESPEC").Range.cut
                        End If
                    Else
                        If .Bookmarks.Exists("GRUPO_ESPEC") Then .Bookmarks("GRUPO_ESPEC").Delete
                    End If
                    
                End If
            Else
                If .Bookmarks.Exists("GRUPO_ESPEC") Then
                    .Bookmarks("GRUPO_ESPEC").Range.cut
                End If
            End If
                    
            'FICHEROS ADJUNTOS
            If chkFich.Value = vbChecked Then
                If .Bookmarks.Exists("GRUPO_ADJ") Then
                    .Bookmarks("GRUPO_ADJ").Select
                    Set rangeword = blankword.Bookmarks("GRUPO_ADJ").Range
                    rangeword.Copy
                    .Application.Selection.cut
                    iNumFich = 0
                    For Each oGrupo In frmOFEPet.oProcesoSeleccionado.Grupos
                        oGrupo.CargarTodasLasEspecificaciones
                        If oGrupo.especificaciones.Count <> 0 Then
                            For Each oEsp In oGrupo.especificaciones
                                    .Application.Selection.Paste
                                    If .Bookmarks.Exists("NUM_ADJ_GRUPO") Then .Bookmarks("NUM_ADJ_GRUPO").Range.Text = oGrupo.Codigo
                                    If .Bookmarks.Exists("NOM_ADJ_GRUPO") Then .Bookmarks("NOM_ADJ_GRUPO").Range.Text = CStr(oEsp.nombre)
                                    If .Bookmarks.Exists("DEN_ADJ_GRUPO") Then .Bookmarks("DEN_ADJ_GRUPO").Range.Text = NullToStr(oEsp.Comentario)
                                    If .Bookmarks.Exists("FEC_ADJ_GRUPO") Then .Bookmarks("FEC_ADJ_GRUPO").Range.Text = Format(oEsp.Fecha, "Short Date")
                                    .Bookmarks("GRUPO_ADJ").Delete
                            Next
                            iNumFich = iNumFich + 1
                        End If
                    Next
                    If iNumFich = 0 Then
                        If .Bookmarks.Exists("GRUPO_ADJ") Then
                            .Bookmarks("GRUPO_ADJ").Range.cut
                        End If
                        If .Bookmarks.Exists("GRUPO_ADJUNTOS") Then
                            .Bookmarks("GRUPO_ADJUNTOS").Range.cut
                        End If
                    Else
                        If .Bookmarks.Exists("GRUPO_ADJUNTOS") Then .Bookmarks("GRUPO_ADJUNTOS").Delete
                    End If
                
                Else
                    If .Bookmarks.Exists("GRUPO_ADJ") Then
                        .Bookmarks("GRUPO_ADJ").Range.cut
                    End If
                    If .Bookmarks.Exists("GRUPO_ADJUNTOS") Then
                        .Bookmarks("GRUPO_ADJUNTOS").Range.cut
                    End If
                End If
            Else
                If .Bookmarks.Exists("GRUPO_ADJ") Then
                    .Bookmarks("GRUPO_ADJ").Range.cut
                End If
                If .Bookmarks.Exists("GRUPO_ADJUNTOS") Then
                    .Bookmarks("GRUPO_ADJUNTOS").Range.cut
                End If
            End If
        Else
            If .Bookmarks.Exists("GRUPO_ADJUNTOS") Then
                .Bookmarks("GRUPO_ADJUNTOS").Range.cut
            End If
            If .Bookmarks.Exists("GRUPO_ESPEC") Then
                .Bookmarks("GRUPO_ESPEC").Range.cut
            End If
        End If
       
        ' ESPECIFICACIONES DE PROCESO
        If frmOFEPet.oProcesoSeleccionado.DefEspecificaciones = True Then
            If Not frmOFEPet.oProcesoSeleccionado.esp = "" Then
                If .Bookmarks.Exists("ESPEC_PROCESO") Then .Bookmarks("ESPEC_PROCESO").Range.Text = frmOFEPet.oProcesoSeleccionado.esp
                If .Bookmarks.Exists("OPC_ESP_PROCESO") Then .Bookmarks("OPC_ESP_PROCESO").Delete
            Else
                If .Bookmarks.Exists("OPC_ESP_PROCESO") Then
                    .Bookmarks("OPC_ESP_PROCESO").Range.cut
                Else
                    If .Bookmarks.Exists("ESPEC_PROCESO") Then .Bookmarks("ESPEC_PROCESO").Range.Text = ""
                End If
            End If
            'FICHEROS ADJUNTOS
            If chkFich.Value = vbChecked Then
                frmOFEPet.oProcesoSeleccionado.CargarTodasLasEspecificaciones
                If frmOFEPet.oProcesoSeleccionado.especificaciones.Count > 0 Then
                    If .Bookmarks.Exists("PROC_ADJ") Then
                        .Bookmarks("PROC_ADJ").Select
                        Set rangeword = blankword.Bookmarks("PROC_ADJ").Range
                        rangeword.Copy
                        .Application.Selection.cut
                        iNumFich = 0
                        For Each oEsp In frmOFEPet.oProcesoSeleccionado.especificaciones
                            .Application.Selection.Paste
                            If .Bookmarks.Exists("NOM_ADJ_PROC") Then .Bookmarks("NOM_ADJ_PROC").Range.Text = oEsp.nombre
                            If .Bookmarks.Exists("FEC_ADJ_PROC") Then .Bookmarks("FEC_ADJ_PROC").Range.Text = Format(oEsp.Fecha, "Short Date")
                            If .Bookmarks.Exists("DEN_ADJ_PROC") Then .Bookmarks("DEN_ADJ_PROC").Range.Text = NullToStr(oEsp.Comentario)
                            If .Bookmarks.Exists("PROC_ADJ") Then .Bookmarks("PROC_ADJ").Delete
                            iNumFich = iNumFich + 1
                        Next
                        If iNumFich = 0 Then
                            If .Bookmarks.Exists("PROCESO_ADJ") Then
                                .Bookmarks("PROCESO_ADJ").Range.cut
                            End If
                        Else
                            If .Bookmarks.Exists("PROCESO_ADJ") Then .Bookmarks("PROCESO_ADJ").Delete
                        End If
                    End If
                Else
                    If .Bookmarks.Exists("PROCESO_ADJ") Then
                        .Bookmarks("PROCESO_ADJ").Range.cut
                    End If
                End If
            Else
                If .Bookmarks.Exists("PROCESO_ADJ") Then
                    .Bookmarks("PROCESO_ADJ").Range.cut
                End If
            End If
        Else
            If .Bookmarks.Exists("OPC_ESP_PROCESO") Then
                .Bookmarks("OPC_ESP_PROCESO").Range.cut
            End If
            If .Bookmarks.Exists("PROCESO_ADJ") Then
                .Bookmarks("PROCESO_ADJ").Range.cut
            End If
        End If
       
       'ATRIBUTOS DE ESPECIFICACION DEL PROCESO
        frmOFEPet.oProcesoSeleccionado.CargarAEspecificacionesP
        
        If .Bookmarks.Exists("ATRIB_P_ESP") Then
            .Bookmarks("ATRIB_P_ESP").Select
            .Application.Selection.cut
    
            If frmOFEPet.oProcesoSeleccionado.AtributosEspecificacion.Count = 0 Then
                If .Bookmarks.Exists("PROCE_ATRIB_ESP") Then
                    .Bookmarks("PROCE_ATRIB_ESP").Select
                    .Application.Selection.cut
                    .Application.Selection.Delete
                End If
            Else
                For Each oatrib In frmOFEPet.oProcesoSeleccionado.AtributosEspecificacion
                    .Application.Selection.Paste
    
                    If .Bookmarks.Exists("COD_ATR_P_ESP") Then .Bookmarks("COD_ATR_P_ESP").Range.Text = NullToStr(oatrib.Cod)
                    If .Bookmarks.Exists("DEN_ATR_P_ESP") Then .Bookmarks("DEN_ATR_P_ESP").Range.Text = NullToStr(oatrib.Den)
                    If .Bookmarks.Exists("VALOR_ATR_P_ESP") Then
                        Select Case oatrib.Tipo
                            Case 1:
                                .Bookmarks("VALOR_ATR_P_ESP").Range.Text = NullToStr(oatrib.valorText)
                            Case 2:
                                .Bookmarks("VALOR_ATR_P_ESP").Range.Text = NullToStr(oatrib.valorNum)
                            Case 3:
                                .Bookmarks("VALOR_ATR_P_ESP").Range.Text = NullToStr(oatrib.valorFec)
                            Case 4:
                                If NullToStr(oatrib.valorBool) = "False" Then
                                    .Bookmarks("VALOR_ATR_P_ESP").Range.Text = m_sNo
                                ElseIf NullToStr(oatrib.valorBool) = "True" Then
                                    .Bookmarks("VALOR_ATR_P_ESP").Range.Text = m_sSi
                                End If
                                
                        End Select
                    End If
                Next
                If .Bookmarks.Exists("ATRIB_P_ESP") Then .Bookmarks("ATRIB_P_ESP").Delete
            End If
        End If
       
        .Application.Selection.Find.ClearFormatting
        .Application.Selection.Find.Text = "#@@#"
        .Application.Selection.Find.Replacement.Text = ""
        .Application.Selection.Find.Forward = True
        .Application.Selection.Find.Wrap = 1
        .Application.Selection.Find.Execute
        While .Application.Selection.Find.Found = True
            .Application.Selection.InsertSymbol CharacterNumber:=9633, Unicode:=True
            .Application.Selection.Find.Execute
        Wend
    End With
                            
    Set ImpresoObjetivos = docword
    blankword.Close
    Set blankword = Nothing
    Set oProves = Nothing
    Set oItem = Nothing
    Set oEsp = Nothing
    

End Function

''' <summary>Crea una nueva l�nea de atributo de art�culo en el impreso de oferta</summary>
''' <param name="docword">Documento word</param>
''' <param name="rangewordITAtr">Objeto rango con el bloque del atributo</param>
''' <param name="oatrib">objeto atributo</param>
''' <param name="TipoEscalado">Tipo de escalados</param>
''' <returns></returns>
''' <remarks>Llamada desde;ImpresoOferta</remarks>

Private Sub ImpresoOfertaAtributo(ByRef docword As Object, ByRef rangewordITAtr As Object, ByVal oatrib As CAtributo, _
        Optional ByVal oEsc As CEscalado, Optional ByVal TipoEscalado As TModoEscalado)
    Dim var1 As Variant
    Dim var2 As Variant
    Dim oElem As CValorPond
    
    'Provisional
    On Error Resume Next
    
    With docword
        rangewordITAtr.Copy
        .Application.Selection.Paste
        
        If .Bookmarks.Exists("ITEM_ATR_ESC_RANGO") Then
           If Not oEsc Is Nothing Then
               If TipoEscalado = ModoDirecto Then
                   .Bookmarks("ITEM_ATR_ESC_RANGO").Range.Text = oEsc.Inicial
               Else
                   .Bookmarks("ITEM_ATR_ESC_RANGO").Range.Text = oEsc.Inicial & " - " & oEsc.final
               End If
           End If
        End If

        
        If .Bookmarks.Exists("COD_ATR_ITEM") Then
            .Bookmarks("COD_ATR_ITEM").Range.Text = NullToStr(oatrib.Cod)
        End If
        If .Bookmarks.Exists("DEN_ATR_ITEM") Then
            .Bookmarks("DEN_ATR_ITEM").Range.Text = NullToStr(oatrib.Den)
        End If
        If .Bookmarks.Exists("TIPO_ATR_ITEM") Then
            Select Case oatrib.Tipo
                Case 1:
                    .Bookmarks("TIPO_ATR_ITEM").Range.Text = NullToStr(m_stxtTexto)
                Case 2:
                    .Bookmarks("TIPO_ATR_ITEM").Range.Text = NullToStr(m_stxtNumero)
                Case 3:
                    .Bookmarks("TIPO_ATR_ITEM").Range.Text = NullToStr(m_stxtFecha)
                Case 4:
                    .Bookmarks("TIPO_ATR_ITEM").Range.Text = NullToStr(m_stxtSi)
            End Select
        End If
        
        If .Bookmarks.Exists("VALOR_ATR_ITEM") Then
            If oatrib.TipoIntroduccion = IntroLibre Then
                 If Not IsNull(oatrib.Minimo) And Not IsNull(oatrib.Maximo) Then
                     var1 = m_stxtMinimo & " : " & oatrib.Minimo & vbCrLf & m_stxtMaximo & " : " & oatrib.Maximo
                     .Bookmarks("VALOR_ATR_ITEM").Range.Text = var1
                 End If
                 If Not IsNull(oatrib.Minimo) And IsNull(oatrib.Maximo) Then
                     var1 = m_stxtMinimo & " : " & oatrib.Minimo
                     .Bookmarks("VALOR_ATR_ITEM").Range.Text = var1
                 End If
                 If IsNull(oatrib.Minimo) And Not IsNull(oatrib.Maximo) Then
                     var1 = m_stxtMaximo & " : " & oatrib.Maximo
                     .Bookmarks("VALOR_ATR_ITEM").Range.Text = var1
                 End If
             End If
             
             If oatrib.TipoIntroduccion = Introselec Then
                 var1 = ""
                 var2 = "#@@#"
                 For Each oElem In oatrib.ListaPonderacion
                     var1 = var1 & var2 & " " & CStr(oElem.ValorLista) & vbCrLf
                 Next
                 Set oElem = Nothing
                 If var1 <> "" Then
                     .Bookmarks("VALOR_ATR_ITEM").Range.Text = var1
                 End If
             End If
             .Bookmarks("VALOR_ATR_ITEM").Delete
        End If
        
        
        If .Bookmarks.Exists("OP_ATR_ITEM") Then
            .Bookmarks("OP_ATR_ITEM").Range.Text = NullToStr(oatrib.PrecioFormula)
            .Bookmarks("OP_ATR_ITEM").Delete
        End If
        If .Bookmarks.Exists("APL_ATR_ITEM") Then
                Select Case oatrib.PrecioAplicarA
                    Case 0:
                        .Bookmarks("APL_ATR_ITEM").Range.Text = NullToStr(m_stxtOferta)
                    Case 1:
                        .Bookmarks("APL_ATR_ITEM").Range.Text = NullToStr(m_stxtGrupo)
                    Case 2:
                        .Bookmarks("APL_ATR_ITEM").Range.Text = NullToStr(m_stxtItem)
                    Case 3:
                        .Bookmarks("APL_ATR_ITEM").Range.Text = NullToStr(m_stxtUniItem)
                End Select
                .Bookmarks("APL_ATR_ITEM").Delete
        End If
    End With
End Sub

''' <summary>Crea una nueva l�nea de art�culo en el impreso de oferta</summary>
''' <param name="docword">Documento word</param>
''' <param name="rangewordArt">Rango con las columnas para datos del art�culo</param>
''' <param name="iNumEsp">N� de art�culo</param>
''' <param name="oItem">Objeto item</param>
''' <param name="Destino">Indica si se muestra la columna Destino</param>
''' <param name="Pago">Indica sise muestra la columna F. de Pago</param>
''' <param name="Fecha">Indica si se muestra la columna F. suministro</param>
''' <param name="ayDest">array de destinos</param>
''' <param name="INUMDEST">n� de destinos</param>
''' <param name="ayPago">array de formas de pago</param>
''' <param name="iNumPago">n� de formas de pago</param>
''' <param name="oEsc">Objeto escalado si hay</param>
''' <param name="TipoEscalado">Tipo de escalados</param>
''' <returns></returns>
''' <remarks>Llamada desde;ImpresoOferta</remarks>

Private Sub ImpresoObjetivosArticulo(ByRef docword As Object, ByRef rangewordArt As Object, ByVal iNumEsp As Integer, ByVal oItem As CItem, _
        ByVal Destino As Boolean, ByVal Pago As Boolean, ByVal Fecha As Boolean, ByRef ayDest As Variant, ByRef INUMDEST As Integer, _
        ByRef ayPago As Variant, ByRef iNumPago As Integer, Optional ByVal oEsc As CEscalado, Optional ByVal TipoEscalado As TModoEscalado)

    'Provisional
    On Error Resume Next
    
    With docword
        rangewordArt.Copy
        .Application.Selection.Paste
    
        If .Bookmarks.Exists("ESC_RANGO") Then
            If Not oEsc Is Nothing Then
                If TipoEscalado = ModoDirecto Then
                    .Bookmarks("ESC_RANGO").Range.Text = oEsc.Inicial
                Else
                    .Bookmarks("ESC_RANGO").Range.Text = oEsc.Inicial & " - " & oEsc.final
                End If
            End If
        End If
        
        If .Bookmarks.Exists("NUM_ART") Then .Bookmarks("NUM_ART").Range.Text = iNumEsp
        If .Bookmarks.Exists("COD_ART") Then .Bookmarks("COD_ART").Range.Text = NullToStr(oItem.ArticuloCod)
        If .Bookmarks.Exists("DEN_ART") Then .Bookmarks("DEN_ART").Range.Text = NullToStr(oItem.Descr)
        If .Bookmarks.Exists("COD_UNI") Then .Bookmarks("COD_UNI").Range.Text = frmOFEPet.oUnidades.Item(oItem.UniCod).Cod
        If .Bookmarks.Exists("DEN_UNI") Then .Bookmarks("DEN_UNI").Range.Text = frmOFEPet.oUnidades.Item(oItem.UniCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        If .Bookmarks.Exists("CANTIDAD") Then .Bookmarks("CANTIDAD").Range.Text = FormateoNumerico(oItem.Cantidad)
        If .Bookmarks.Exists("OBJETIVO") Then
            If IsNull(oItem.Objetivo) Or oItem.Objetivo = "" Then
                .Bookmarks("OBJETIVO").Range.Text = ""
            Else
                .Bookmarks("OBJETIVO").Range.Text = FormateoNumerico(NullToStr(oItem.Objetivo))
            End If
        End If
        If Destino Then
            If .Bookmarks.Exists("COD_DEST") Then
                .Bookmarks("COD_DEST").Range.Text = NullToStr(oItem.DestCod)
            End If
    
            If .Bookmarks.Exists("DEN_DEST") Then
                .Bookmarks("DEN_DEST").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(oItem.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            
            If .Bookmarks.Exists("DESTINO") Then
                If Not BuscarEnArray(ayDest, oItem.DestCod) Then
                    ayDest(INUMDEST) = oItem.DestCod
                    INUMDEST = INUMDEST + 1
                    ReDim Preserve ayDest(UBound(ayDest) + 1)
                End If
            End If
        End If
                
        If Pago Then
            If .Bookmarks.Exists("COD_PAGO") Then
                .Bookmarks("COD_PAGO").Range.Text = NullToStr(oItem.PagCod)
            End If
    
            If .Bookmarks.Exists("DEN_PAGO") Then
                .Bookmarks("DEN_PAGO").Range.Text = NullToStr(frmOFEPet.oPagos.Item(oItem.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            
            If .Bookmarks.Exists("PAGO") Then
                If Not BuscarEnArray(ayPago, oItem.PagCod) Then
                    ayPago(iNumPago) = oItem.PagCod
                    iNumPago = iNumPago + 1
                    ReDim Preserve ayPago(UBound(ayPago) + 1)
                End If
            End If
        End If
                
        If Fecha Then
            If .Bookmarks.Exists("FINI_SUMINISTRO") Then
                .Bookmarks("FINI_SUMINISTRO").Range.Text = Format(NullToStr(oItem.FechaInicioSuministro), "Short Date")
            End If
    
            If .Bookmarks.Exists("FFIN_SUMINISTRO") Then
                .Bookmarks("FFIN_SUMINISTRO").Range.Text = Format(NullToStr(oItem.FechaFinSuministro), "Short Date")
            End If
        End If
    End With

End Sub

''' <summary>
''' Crear el word para el cuerpo de la notificaci�n
''' </summary>
''' <param name="appword">objeto word con el q trabajamos</param>
''' <param name="splantilla">plantilla de la notificacion</param>
''' <param name="oPet">prove notificado</param>
''' <param name="oEquipo">Equipo del notificado</param>
''' <returns>fichero generado</returns>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Function CartaImpreso(splantilla As String, ByVal oPet As CPetOferta, oEquipo As CEquipo) As Object
    Dim docword As Object
    
    ' CARGAR DATOS GENERALES DE PROVEEDOR
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.Add oPet.CodProve, oPet.DenProve
    oProves.CargarDatosProveedor oPet.CodProve
    
    Set docword = appword.Documents.Add(splantilla)
    'appword.Visible = True
    
    If m_iWordVer >= 9 Then
        If appword.Visible Then docword.activewindow.WindowState = 2
    Else
        appword.Visible = True
        appword.Windows(docword.Name).WindowState = 1
    End If
    
' DATOS GENERALES
With docword
    If .Bookmarks.Exists("NUM_PROCESO") Then
        DatoAWord docword, "NUM_PROCESO", frmOFEPet.sdbcAnyo.Text & "/" & frmOFEPet.sdbcGMN1_4Cod.Text & "/" & frmOFEPet.sdbcProceCod.Text
    End If
    If .Bookmarks.Exists("DEN_PROCESO") Then
        DatoAWord docword, "DEN_PROCESO", frmOFEPet.sdbcProceDen.Text & " "
    End If
    If .Bookmarks.Exists("MAT1_PROCESO") Then
        DatoAWord docword, "MAT1_PROCESO", frmOFEPet.sdbcGMN1_4Cod.Text
    End If
    If .Bookmarks.Exists("FECHA_REUNION") Then
        DatoAWord docword, "FECHA_REUNION", FrmOBJAnya.LblFecReuBox
    End If
    If .Bookmarks.Exists("FECHA_LIMITE") Then
        'DatoAWord docword, "FECHA_LIMITE", FrmOBJAnya.txtFecLimOfe.Text & " " & Trim(txtHoraLimite.Text)
        DatoAWord docword, "FECHA_LIMITE", m_vDatoAPasar
    End If
    If .Bookmarks.Exists("SOLICITUD") Then
        DatoAWord docword, "SOLICITUD", NullToStr(frmOFEPet.oProcesoSeleccionado.Referencia)
    End If
    'DATOS PROVEEDOR
    If .Bookmarks.Exists("COD_PROVE") Then
        DatoAWord docword, "COD_PROVE", oPet.CodProve
    End If
    If .Bookmarks.Exists("DEN_PROVE") Then
        DatoAWord docword, "DEN_PROVE", oPet.DenProve
    End If
    If .Bookmarks.Exists("DIR_PROVE") Then
        DatoAWord docword, "DIR_PROVE", NullToStr(oProves.Item(1).Direccion)
    End If
    If .Bookmarks.Exists("POBL_PROVE") Then
        DatoAWord docword, "POBL_PROVE", NullToStr(oProves.Item(1).Poblacion)
    End If
    If .Bookmarks.Exists("CP_PROVE") Then
        DatoAWord docword, "CP_PROVE", NullToStr(oProves.Item(1).cP)
    End If
    If .Bookmarks.Exists("PROV_PROVE") Then
        DatoAWord docword, "PROV_PROVE", NullToStr(oProves.Item(1).DenProvi)
    End If
    If .Bookmarks.Exists("PAIS_PROVE") Then
        DatoAWord docword, "PAIS_PROVE", NullToStr(oProves.Item(1).DenPais)
    End If
    If .Bookmarks.Exists("NIF_PROVE") Then
        DatoAWord docword, "NIF_PROVE", NullToStr(oProves.Item(1).NIF)
    End If
    ' DATOS PERSONA CONTACTO
    If .Bookmarks.Exists("NOM_CONTACTO") Then
        DatoAWord docword, "NOM_CONTACTO", NullToStr(oPet.nombre)
    End If
    If .Bookmarks.Exists("APE_CONTACTO") Then
        DatoAWord docword, "APE_CONTACTO", NullToStr(oPet.Apellidos)
    End If
    If .Bookmarks.Exists("TFNO_CONTACTO") Then
        DatoAWord docword, "TFNO_CONTACTO", NullToStr(oPet.Tfno)
    End If
    If .Bookmarks.Exists("FAX_CONTACTO") Then
        DatoAWord docword, "FAX_CONTACTO", NullToStr(oPet.Fax)
    End If
    If .Bookmarks.Exists("MAIL_CONTACTO") Then
        DatoAWord docword, "MAIL_CONTACTO", NullToStr(oPet.Email)
    End If
    If .Bookmarks.Exists("TFNO_MOVIL_CONTACTO") Then
        DatoAWord docword, "TFNO_MOVIL_CONTACTO", NullToStr(oPet.tfnomovil)
    End If
    ' DATOS COMPRADOR
    If .Bookmarks.Exists("APE_COMPRADOR") Then
        DatoAWord docword, "APE_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Apel)
    End If
    If .Bookmarks.Exists("NOM_COMPRADOR") Then
        DatoAWord docword, "NOM_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).nombre)
    End If
    If .Bookmarks.Exists("TFNO_COMPRADOR") Then
        DatoAWord docword, "TFNO_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Tfno)
    End If
    If .Bookmarks.Exists("FAX_COMPRADOR") Then
        DatoAWord docword, "FAX_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Fax)
    End If
    If .Bookmarks.Exists("MAIL_COMPRADOR") Then
        DatoAWord docword, "MAIL_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).mail)
    End If
            
End With
Set oProves = Nothing

Set CartaImpreso = docword
Set docword = Nothing
 

End Function

Private Sub Arrange()
    
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
    On Error Resume Next
    
    If Me.Height - Shape2.Height - Shape1.Height - Shape4.Height - picEdit.Height - 750 > 10 Then
        sdbgPet.Height = Me.Height - Shape2.Height - Shape1.Height - Shape4.Height - picEdit.Height - 750
    End If
    If Width >= 360 Then sdbgPet.Width = Width - 360
    sdbgPet.Groups(0).Width = sdbgPet.Width * 0.4
    sdbgPet.Groups(1).Width = sdbgPet.Width * 0.35
    sdbgPet.Groups(2).Width = sdbgPet.Width * 0.205
    
    cmdAceptar.Left = Me.Width / 2 - 1400
    cmdCancelar.Left = cmdAceptar.Left + 1200
    
    
End Sub
    
''' <summary>
''' * Objetivo: Cargar combo con la coleccion de Contactos
''' </summary>
''' <remarks>Llamada desde: sdbddConApe_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridConContactosApe()
Dim oCon As CContacto
    
    sdbddConApe.RemoveAll
    
    For Each oCon In oContactos
        sdbddConApe.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Tfno2 & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & IIf(oCon.Port, "1", "0") & Chr(m_lSeparador) & oCon.TipoEmail
    Next
    
    If sdbddConApe.Rows = 0 Then
        sdbddConApe.AddItem ""
    End If
    
    sdbddConApe.MoveFirst
    
End Sub

''' <summary>
''' * Objetivo: Cargar combo con la coleccion de Contactos
''' </summary>
''' <remarks>Llamada desde: sdbddConNom_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridConContactosNom()
Dim oCon As CContacto
    
    sdbddConNom.RemoveAll
    
    For Each oCon In oContactos
        sdbddConNom.AddItem oCon.nombre & Chr(m_lSeparador) & oCon.Apellidos & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Tfno2 & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & IIf(oCon.Port, "1", "0") & Chr(m_lSeparador) & oCon.TipoEmail
    Next
    
    If sdbddConNom.Rows = 0 Then
        sdbddConNom.AddItem ""
    End If
    
    sdbddConNom.MoveFirst
    
End Sub
Private Sub CargarGridConProveCod()
Dim oProve As CProveedor
    
    sdbddProveCod.RemoveAll
    
    For Each oProve In oProves
        sdbddProveCod.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den
    Next
    
    If sdbddProveCod.Rows = 0 Then
        sdbddProveCod.AddItem ""
    End If
    
    sdbddProveCod.MoveFirst
    
End Sub
Private Sub CargarGridConProveDen()
Dim oProve As CProveedor
    
    sdbddProveDen.RemoveAll
    
    For Each oProve In oProves
        sdbddProveDen.AddItem oProve.Den & Chr(m_lSeparador) & oProve.Cod
    Next
    
    If sdbddProveDen.Rows = 0 Then
        sdbddProveDen.AddItem ""
    End If
    
    sdbddProveDen.MoveFirst
    
End Sub

Private Sub chkEsp_Click()

    If chkEsp.Value = vbUnchecked Then
        chkFich.Value = vbUnchecked
    End If
    
End Sub

Private Sub chkExcel_Click()
    If chkExcel.Value = vbChecked Then
        chkExcelEspe.Value = vbChecked
        chkExcelEspe.Visible = True
    Else
        chkExcelEspe.Visible = False
    End If
End Sub

Private Sub chkFich_Click()

    If chkEsp.Value = vbUnchecked Then
        chkFich.Value = vbUnchecked
    End If
    
End Sub

''' <summary>
''' Valida los cambios
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema (bt cmdAceptar); Tiempo m�ximo: 0</remarks>
Private Sub cmdAceptar_Click()
    Dim oPet As CPetOferta
    Dim oPets As CPetOfertas
    Dim oIPet As IPeticiones
    Dim oEsp As CEspecificacion
    Dim oItem As CItem
    Dim oGrupo As CGrupo
    Dim oiasignaciones As IAsignaciones
    Dim oAsignaciones As CAsignaciones
    Dim oEquipo As CEquipo
    Dim sIdCon As String
    Dim NoComunicados() As Variant
    Dim NoEmail() As Variant
    Dim NoC As Variant
    Dim NoE As Variant
    Dim EsAutorizado As Variant
    Dim iDes As Integer
    Dim iMai As Integer
    Dim bNoComunicar As Boolean
    Dim bVisibleWord As Boolean
    Dim docword As Object
    Dim docPetWord As Object
    Dim sTempNombreWord  As String
    Dim rangeword As Object
    Dim Section As Object
    Dim splantilla As String
    Dim sPlantillaWeb As String
    Dim sPlantillaMail As String
    Dim bSesionIniciada As Boolean   'Indica si se ha iniciado una sesion de correo
    Dim i As Integer
    Dim bWeb As Boolean
    Dim bEMail As Boolean
    Dim bImp As Boolean
    Dim sTemp As String
    Dim bAdjuntarImpreso As Boolean
    Dim iNumPet As Integer
    Dim iNumPets As Integer
    Dim bSalvar As Boolean
    Dim errormail As TipoErrorSummit
    Dim teserror As TipoErrorSummit
    Dim dFechaLimite As Variant
    Dim bPublicar As Boolean
    Dim dFechaSobres As Date
    Dim sCuerpo As String
    Dim sVersion As String
    Dim sTarget As String
    Dim oAtribsEspec As CAtributosEspecificacion
    Dim oAtribEsp As CAtributoEspecificacion
    Dim sCadena As String
    Dim sCadenaTmp As String
    Dim sMailSubject As String
    Dim dtFechaLimiteUTC As Date
    Dim dtHoraLimiteUTC As Date
    Dim sTempCarta As String
    Dim ResulGrabaError As TipoErrorSummit
    Dim bSiguiente As Boolean
    Dim bEsHTML As Boolean
    Dim oError As TipoErrorSummit
    Dim bComunicando As Boolean
    Dim oCEmailProce As CEmailProce
    
    On Error GoTo Error:
        
    'Comprobamos que haya una fecha de validez
    If bModificarFecLimit Then
        If txtFecLimOfe.Text = "" Then
            dFechaLimite = Null
        Else
            If Not IsDate(txtFecLimOfe.Text) Then
                oMensajes.NoValida sIdiFecLimite
                Exit Sub
            End If
            If Trim(txtHoraLimite.Text) <> "" Then
                If Not IsTime(txtHoraLimite.Text) Then
                    oMensajes.NoValida m_sHoraLim
                    Exit Sub
                End If
            Else
                oMensajes.NoValida m_sHoraLim
                Exit Sub
            End If
            If CDate(txtFecLimOfe.Text) < frmOFEPet.oProcesoSeleccionado.FechaApertura Then
                oMensajes.FechaLimOfeMenorQueApertura IIf(frmOFEPet.oProcesoSeleccionado.ModoSubasta, 2, 1), frmOFEPet.oProcesoSeleccionado.FechaApertura
                Exit Sub
            End If
            
            dFechaLimite = CDate(FormatDateTime(txtFecLimOfe.Text, vbShortDate) & " " & FormatDateTime(CDate(txtHoraLimite.Text), vbShortTime))

            ConvertirTZaUTC oUsuarioSummit.TimeZone, DateValue(dFechaLimite), TimeValue(dFechaLimite), dtFechaLimiteUTC, dtHoraLimiteUTC
            dtFechaLimiteUTC = dtFechaLimiteUTC + dtHoraLimiteUTC
            If dtFechaLimiteUTC < ObtenerFechaUTC(Now) Then
                oMensajes.FechaLimiteCaducada
                Exit Sub
            End If
        End If
        If frmOFEPet.oProcesoSeleccionado.AdminPublica Then
            If IsNull(dFechaLimite) Then
                oMensajes.FaltanDatos 35 'Fecha limite
                Exit Sub
            Else
                dFechaSobres = frmOFEPet.FechaMinimaDeSobre
                If CDate(dFechaLimite) > dFechaSobres Then
                    oMensajes.FechaSobreMenorQueLimOfer dFechaSobres, 128
                    Exit Sub
                End If
            End If
        End If
    Else
        dFechaLimite = frmOFEPet.oProcesoSeleccionado.FechaMinimoLimOfertas
        dtFechaLimiteUTC = dFechaLimite
    End If
    
    'Si se va a publicar
    bPublicar = False
    For i = 0 To sdbgPet.Rows - 1
        If sdbgPet.Columns(4).Value <> "0" Then
            bPublicar = True
            Exit For
        End If
    Next
        
    If gParametrosGenerales.giINSTWEB <> SinWeb And Not bModificarFecLimit Then
        If IsDate(dFechaLimite) Then
            If CDate(Format(dtFechaLimiteUTC, "short date")) < DateValue(ObtenerFechaUTC(Date)) And bPublicar Then
                oMensajes.ImposiblePublicarCaducado
                Exit Sub
            End If
        End If
    End If

    If IsDate(dFechaLimite) Then
        If IsDate(frmOFEPet.oProcesoSeleccionado.FechaPresentacion) Then
            If CDate(Format(frmOFEPet.oProcesoSeleccionado.FechaPresentacion, "short date")) < CDate(Format(dFechaLimite, "short date")) Then
                oMensajes.FechaMayorQuePresentacion (Not frmOFEPet.oProcesoSeleccionado.PermitirAdjDirecta)
                Exit Sub
            End If
        End If
        
        If bModificarFecLimit Then
            If CDate(dtFechaLimiteUTC) < ObtenerFechaUTC(Now) Then
                If frmOFEPet.oProcesoSeleccionado.AdminPublica Then
                    oMensajes.ImposiblePublicarCaducado (Not bPublicar)
                    Exit Sub
                Else
                    i = oMensajes.PreguntaEliminarFecLimite((Not bPublicar))
                    If i = vbYes Then
                        dFechaLimite = Null
                        dtFechaLimiteUTC = Null
                    Else
                        Exit Sub
                    End If
                End If
            End If
        End If
    End If
    
    'Comprobamos que la plantilla  de word es v�lida
'  Peticion de ofertas via carta.
    If Trim(txtDOT.Text) = "" Then
        oMensajes.NoValida sIdiPlantilla
        If Me.Visible Then txtDOT.SetFocus
        Exit Sub
    Else
        If Right(txtDOT.Text, 3) <> "dot" Then
            oMensajes.NoValida sIdiPlantilla
            If Me.Visible Then txtDOT.SetFocus
            Exit Sub
        End If
    End If
    
    If Not oFos.FileExists(txtDOT.Text) Then
        oMensajes.PlantillaNoEncontrada txtDOT.Text
        If Me.Visible Then txtDOT.SetFocus
        Exit Sub
    End If
    
    splantilla = txtDOT.Text
 
' ? Peticion de ofertas via web.
    If gParametrosGenerales.giINSTWEB = ConPortal Or gParametrosGenerales.giINSTWEB = conweb Then
        'Almacenamos la plantilla de word
        If Trim(txtWebDot.Text) = "" Then
            oMensajes.NoValida sIdiPlantilla
            If Me.Visible Then txtDOT.SetFocus
            Exit Sub
        Else
            If LCase(Right(txtWebDot.Text, 3)) <> "htm" Then
                oMensajes.NoValida sIdiPlantilla
                If Me.Visible Then txtWebDot.SetFocus
                Exit Sub
            End If
        End If
        
        If Not oFos.FileExists(txtWebDot.Text) Then
            oMensajes.PlantillaNoEncontrada txtWebDot.Text
            If Me.Visible Then txtWebDot.SetFocus
            Exit Sub
        End If
        splantilla = Left(txtWebDot.Text, Len(txtWebDot.Text) - 4) & ".txt"
        If Not oFos.FileExists(splantilla) Then
            oMensajes.PlantillaNoEncontrada splantilla
            If Me.Visible Then txtWebDot.SetFocus
            Exit Sub
        End If
    
    End If
 
  ' ? Peticion de ofertas via Mail
    If gParametrosGenerales.giMail <> 0 Then
        sPlantillaWeb = txtWebDot.Text
    
        If Trim(txtMailDot.Text) = "" Then
            oMensajes.NoValida sIdiPlantilla
            If Me.Visible Then txtMailDot.SetFocus
            Exit Sub
        Else
            If LCase(Right(txtMailDot.Text, 3)) <> "htm" Then
                oMensajes.NoValida sIdiPlantilla
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
            If Not oFos.FileExists(txtMailDot.Text) Then
                oMensajes.PlantillaNoEncontrada txtMailDot.Text
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
            splantilla = Left(txtMailDot.Text, Len(txtMailDot.Text) - 4) & ".txt"
            If Not oFos.FileExists(splantilla) Then
                oMensajes.PlantillaNoEncontrada splantilla
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
        End If
        
        sPlantillaMail = txtMailDot.Text
    End If
    
    Screen.MousePointer = vbHourglass
    
    frmESPERA.lblGeneral.caption = sIdiGenComunicObj
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Show
    DoEvents
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblDetalle = sIdiCreandoPet
    
    'Creamos las peticiones de ofertas
    Set oPets = Nothing
    Set oPets = oFSGSRaiz.generar_CPetOfertas
       
    'Generamos las peticiones correspondientes
    sdbgPet.MoveFirst
    iNumPet = 0
    For i = 0 To sdbgPet.Rows - 1
        If sdbgPet.Columns(0).Value <> "" And (sdbgPet.Columns(4).Value <> "0" Or sdbgPet.Columns(5).Value <> "0" Or sdbgPet.Columns(6).Value <> "0") Then
            If (sdbgPet.Columns(4).Value <> "0" And sdbgPet.Columns(2).Value <> "") Or (sdbgPet.Columns(5).Value <> "0" And sdbgPet.Columns(2).Value <> "") Or (sdbgPet.Columns(6).Value <> "0") Then
                bWeb = False
                bEMail = False
                bImp = False
                bEsHTML = (sdbgPet.Columns("TIPOEMAIL").Value = 1)
                
                If sdbgPet.Columns(4).Value <> "0" Then
                    bWeb = True
                Else
                    If sdbgPet.Columns(5).Value <> "0" Then
                        bEMail = True
                    Else
                        bImp = True
                    End If
                End If
                
                'Comprobar que el email existe en los usuarios del portal
                bNoComunicar = False
                If gParametrosGenerales.giINSTWEB = ConPortal Then
                    Set oProves = oFSGSRaiz.generar_CProveedores
                    If bWeb Then
                        Set oProve = oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").Value)
                        sIdCon = CStr(sdbgPet.Columns("CODCON").Value)
                        EsAutorizado = oProve.ComprobarAutorizacionPortal
                        If Not IsNull(EsAutorizado) Then
                            ReDim Preserve NoComunicados(2, iDes)
                            NoComunicados(0, iDes) = EsAutorizado(0)
                            NoComunicados(1, iDes) = EsAutorizado(1)
                            NoComunicados(2, iDes) = EsAutorizado(2)
                            iDes = iDes + 1
                            bNoComunicar = True
                        Else
                            If oProve.Contactos Is Nothing Then
                                oProve.CargarTodosLosContactos
                            Else
                                If oProve.Contactos.Item(sIdCon) Is Nothing Then
                                    oProve.CargarTodosLosContactos
                                End If
                            End If
                            If NullToStr(oProve.Contactos.Item(sIdCon).mail) <> "" And oProve.Contactos.Item(sIdCon).Port = True Then
                                If Not oProves.ComprobarEmailEnPortal(NullToStr(oProve.CodPortal), NullToStr(oProve.Contactos.Item(sIdCon).mail)) Then
                                    ReDim Preserve NoEmail(2, iMai)
                                    NoEmail(0, iMai) = oProve.Cod
                                    NoEmail(1, iMai) = oProve.Contactos.Item(sIdCon).mail
                                    NoEmail(2, iMai) = "EMAIL"
                                    iMai = iMai + 1
                                    bNoComunicar = True
                                End If
                            End If
                        End If
                        Set oProve = Nothing
                    End If
                End If
                
                If Not bNoComunicar Then
                    'Se reusa AntesDeCierreParcial para guardar si es html � txt
                    oPets.Add sdbgPet.Columns(0).Value, sdbgPet.Columns(1).Value, bWeb, bEMail, bImp, Date, Int(val(sdbgPet.Columns(8).Value)), sdbgPet.Columns(2).Value, sdbgPet.Columns(3).Value, sdbgPet.Columns(7).Value, iNumPet, sdbgPet.Columns("TFNO").Value, sdbgPet.Columns("TFNO2").Value, sdbgPet.Columns("FAX").Value, CDate(FrmOBJAnya.LblFecReuBox), 1, sdbgPet.Columns("TFNO_MOVIL").Value, bEsHTML
                    iNumPet = iNumPet + 1
                End If
            End If
        End If
        sdbgPet.MoveNext
    Next
    
    If iDes > 0 Then NoC = NoComunicados
    If iMai > 0 Then NoE = NoEmail
    If iDes > 0 Or iMai > 0 Then
        oMensajes.ImposiblePublicarPremiums NoC, NoE
    End If
    
    
    If bPublicar And Not frmOFEPet.oProcesoSeleccionado.AtributosEspValidados Then
        'Comprobacion de atributo de epecificacion obligatorios con validacion en la publicacion
        Set oAtribsEspec = frmOFEPet.oProcesoSeleccionado.ValidarAtributosEspObligatorios(TValidacionAtrib.Publicacion)
        If frmOFEPet.oProcesoSeleccionado.TError.NumError <> TESnoerror Then
            teserror = frmOFEPet.oProcesoSeleccionado.TError
            frmOFEPet.oProcesoSeleccionado.TError.NumError = TESnoerror
            GoTo Error
        End If
        If Not oAtribsEspec.Count = 0 Then
            sCadena = ""
            sCadenaTmp = ""
            For Each oAtribEsp In oAtribsEspec
                sCadenaTmp = oAtribEsp.Den
                Select Case oAtribEsp.ambito
                    Case TipoAmbitoProceso.AmbProceso
                        sCadenaTmp = sCadenaTmp & " " & m_sProceso & vbCrLf
                    Case TipoAmbitoProceso.AmbGrupo
                        sCadenaTmp = sCadenaTmp & " " & m_sGrupo & vbCrLf
                    Case TipoAmbitoProceso.AmbItem
                        sCadenaTmp = sCadenaTmp & " " & m_sItem & vbCrLf
                End Select
                If InStr(1, sCadena, sCadenaTmp) = 0 Then
                    sCadena = sCadena & sCadenaTmp
                End If
            Next
            oMensajes.AtributosEspObl sCadena, TValidacionAtrib.Publicacion
            Screen.MousePointer = vbNormal
            Unload frmESPERA
            Exit Sub
        End If
        
        teserror = frmOFEPet.oProcesoSeleccionado.ActualizarValidacionAtribEsp(True)
        If teserror.NumError <> TESnoerror Then GoTo Error
        frmOFEPet.oProcesoSeleccionado.AtributosEspValidados = True
    End If
    
    If chkExcel.Value = vbChecked Then
        Dim oIOfertas As iOfertas
        Set oIOfertas = frmOFEPet.oProcesoSeleccionado
        Set m_oOfertasProceso = oIOfertas.CargarOfertasDelProceso
        If frmOFEPet.oProcesoSeleccionado.TError.NumError <> TESnoerror Then
            teserror = frmOFEPet.oProcesoSeleccionado.TError
            frmOFEPet.oProcesoSeleccionado.TError.NumError = TESnoerror
            GoTo Error
        End If
    End If
    
    ' Recorremos las peticiones para enviar un mail u obtener un impreso
    iNumPet = 0
    iNumPets = oPets.Count
    sTemp = DevolverPathFichTemp
    m_vDatoAPasar = txtFecLimOfe.Text & " " & Trim(txtHoraLimite.Text)
    
    For Each oPet In oPets
        sCuerpo = ""
        Set docword = Nothing
    
        g_bCancelarMail = False
        
        ReDim Nombres(0)
        iNumPet = iNumPet + 1
        
        frmESPERA.lblContacto = sIdiContacto & " " & oPet.nombre & " " & oPet.Apellidos
        frmESPERA.lblContacto.Refresh
        DoEvents
        frmESPERA.ProgressBar2.Value = val((iNumPet / iNumPets) * 100)
        frmESPERA.ProgressBar1.Max = 10
        frmESPERA.ProgressBar1.Value = 1
      
        frmESPERA.lblDetalle = sIdiInicSesCorreo
        frmESPERA.lblDetalle.Refresh
        
        ' DATOS PARA COMPRADORES
        Set oiasignaciones = frmOFEPet.oProcesoSeleccionado
        Set oAsignaciones = oiasignaciones.DevolverAsignaciones(, , , oPet.CodProve)
        If frmOFEPet.oProcesoSeleccionado.TError.NumError <> TESnoerror Then
            teserror = frmOFEPet.oProcesoSeleccionado.TError
            frmOFEPet.oProcesoSeleccionado.TError.NumError = TESnoerror
            GoTo Error
        End If
        Set oEquipo = Nothing
        Set oEquipo = oFSGSRaiz.generar_CEquipo
        oEquipo.Cod = oAsignaciones.Item(1).codEqp
        oEquipo.CargarTodosLosCompradoresDesde 1, oAsignaciones.Item(1).CodComp
        If oEquipo.TError.NumError <> TESnoerror Then
            teserror = oEquipo.TError
            oEquipo.TError.NumError = TESnoerror
            GoTo Error
        End If
        
        DoEvents
        
        '********** PETICIONES VIA WEB ***************
        
        If oPet.ViaWeb = True And oPet.Email <> "" Then
                
            'Realizamos el envio de mensajes desde el cliente
            ' Enviamos un mail al proveedor
            If Not bSesionIniciada Or oIdsMail Is Nothing Then
                frmESPERA.ProgressBar1.Value = 2
                frmESPERA.lblDetalle = sIdiInicSesCorreo
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set oIdsMail = IniciarSesionMail
                bSesionIniciada = True
            End If
            
            frmESPERA.ProgressBar1.Value = 4
            frmESPERA.lblDetalle = sIdiAbrPlant & " " & sPlantillaWeb & " ..."

            frmESPERA.lblDetalle.Refresh
            DoEvents
            
            'Se reusa AntesDeCierreParcial para saber si es html � txt
            Set oCEmailProce = GenerarCEmailProce(oFSGSRaiz, gParametrosGenerales, basPublic.gParametrosInstalacion, oMensajes, basOptimizacion.gTipoDeUsuario, oUsuarioSummit, basParametros.gLongitudesDeCodigos)
            sCuerpo = oCEmailProce.GenerarMensajeObjetivo(True, oPet, oEquipo, IIf(bWeb, Me.txtWebDot.Text, Me.txtMailDot.Text), frmOFEPet.sdbcAnyo.Text, frmOFEPet.sdbcGMN1_4Cod.Text, frmOFEPet.sdbcProceCod.Text, _
                            frmOFEPet.sdbcProceDen.Text, FrmOBJAnya.LblFecReuBox, m_vDatoAPasar, frmOFEPet.sdbcGMN1_4Cod.Text, frmOFEPet.oProcesoSeleccionado.Referencia)
            Set oCEmailProce = Nothing
            
            frmESPERA.ProgressBar1.Value = 5
            frmESPERA.lblDetalle = sIdiCopiDocEmail
            frmESPERA.lblDetalle.Refresh
            DoEvents
                    
            If chkFich.Value = vbChecked Then
                AdjuntarEspecificaciones sTemp, oPet.CodProve
            End If
                    
                    
            'Si hay que adjuntar la hoja excel de petici�n de ofertas:
            If chkExcel.Value = vbChecked Then
                AdjuntarExcel oPet
            End If
                    
            ' enviar mensaje
            sMailSubject = ComponerAsuntoEMail(gParametrosInstalacion.gsObjMailSubject, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, frmOFEPet.oProcesoSeleccionado.Den)
            bComunicando = True
            errormail = ComponerMensaje(oPet.Email, sMailSubject, sCuerpo, Nombres, "FrmOBJAnya", oPet.AntesDeCierreParcial, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, _
                                            entidadNotificacion:=ProcesoCompra, _
                                            tipoNotificacion:=PetOferta, _
                                            Anyo:=frmOFEPet.oProcesoSeleccionado.Anyo, _
                                            GMN1:=frmOFEPet.oProcesoSeleccionado.GMN1Cod, _
                                            Proce:=frmOFEPet.oProcesoSeleccionado.Cod, _
                                            sToName:=oPet.nombre & " " & oPet.Apellidos)
            If errormail.NumError <> TESnoerror Then
                oPet.MailCancel = True
                GoTo Error
            ElseIf g_bCancelarMail = True Then
                oPet.MailCancel = True
            End If
                
            frmESPERA.ProgressBar1.Value = 10
            frmESPERA.lblDetalle = sIdiEnvMensaje
            

            frmESPERA.lblDetalle.Refresh
            DoEvents
                   
        End If
            
        ' Env�o del mail al proveedor con las especificaciones adjuntas del proceso y de los items
        If oPet.viaEMail And oPet.Email <> "" Then
                            
            If Not bSesionIniciada Or oIdsMail Is Nothing Then
                frmESPERA.ProgressBar1.Value = 2
                frmESPERA.lblDetalle = sIdiInicSesCorreo
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set oIdsMail = IniciarSesionMail
                bSesionIniciada = True
            End If
            
            If appword Is Nothing Then
                frmESPERA.ProgressBar1.Value = 3
                frmESPERA.lblDetalle = sidiInicWord
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set appword = CreateObject("Word.Application")
                bSalvar = appword.Options.SavePropertiesPrompt
                appword.Options.SavePropertiesPrompt = False
                bVisibleWord = appword.Visible
                appword.WindowState = 2
                sVersion = appword.VERSION
                If InStr(1, sVersion, ".") > 1 Then
                    sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
                    m_iWordVer = CInt(sVersion)
                Else
                    m_iWordVer = 9
                End If
            Else 'Si han cerrado word da casque y se vuelve a iniciar
                sVersion = appword.VERSION
            End If
                                                              
            frmESPERA.ProgressBar1.Value = 4
            frmESPERA.lblDetalle = sIdiAnyaPlant & " " & sPlantillaMail & " ..."
            frmESPERA.lblDetalle.Refresh
            DoEvents
            
            'Se reusa AntesDeCierreParcial para saber si es html � txt
                        
            Set oCEmailProce = GenerarCEmailProce(oFSGSRaiz, gParametrosGenerales, basPublic.gParametrosInstalacion, oMensajes, basOptimizacion.gTipoDeUsuario, oUsuarioSummit, basParametros.gLongitudesDeCodigos)
            sCuerpo = oCEmailProce.GenerarMensajeObjetivo(False, oPet, oEquipo, IIf(bWeb, Me.txtWebDot.Text, Me.txtMailDot.Text), frmOFEPet.sdbcAnyo.Text, frmOFEPet.sdbcGMN1_4Cod.Text, frmOFEPet.sdbcProceCod.Text, _
                            frmOFEPet.sdbcProceDen.Text, FrmOBJAnya.LblFecReuBox, m_vDatoAPasar, frmOFEPet.sdbcGMN1_4Cod.Text, frmOFEPet.oProcesoSeleccionado.Referencia)
            Set oCEmailProce = Nothing
            
            ' Primero adjunto el documento con el impreso a rellenar
            frmESPERA.ProgressBar1.Value = 5
            frmESPERA.lblDetalle = sidiGenImprPet
            frmESPERA.lblDetalle.Refresh
            DoEvents
            
            Set docPetWord = ImpresoObjetivos(oPet)
            If Not docPetWord Is Nothing Then
                sTemp = DevolverPathFichTemp
                sTempNombreWord = DevolverNombreArchivo(sImpreso, sTemp, "doc")
                docPetWord.SaveAs sTemp & sTempNombreWord
                docPetWord.Close False
                
                sayFileNames(UBound(sayFileNames)) = sTemp & sTempNombreWord
                ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                Nombres(UBound(Nombres)) = sTempNombreWord
                ReDim Preserve Nombres(UBound(Nombres) + 1)
            End If
            
            If chkFich.Value = vbChecked Then
                AdjuntarEspecificaciones sTemp, oPet.CodProve
            End If
            
            'Si hay que adjuntar la hoja excel de petici�n de ofertas:
            If chkExcel.Value = vbChecked Then
                AdjuntarExcel oPet
            End If
            
            frmESPERA.ProgressBar1.Value = 10
            frmESPERA.lblDetalle = sIdiEnvMensaje
            frmESPERA.lblDetalle.Refresh
            DoEvents
            sMailSubject = ComponerAsuntoEMail(gParametrosInstalacion.gsObjMailSubject, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, frmOFEPet.oProcesoSeleccionado.Den)
            bComunicando = True
            errormail = ComponerMensaje(oPet.Email, sMailSubject, sCuerpo, Nombres, "FrmOBJAnya", oPet.AntesDeCierreParcial, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, _
                                            entidadNotificacion:=ProcesoCompra, _
                                            tipoNotificacion:=PetOferta, _
                                            Anyo:=frmOFEPet.oProcesoSeleccionado.Anyo, _
                                            GMN1:=frmOFEPet.oProcesoSeleccionado.GMN1Cod, _
                                            Proce:=frmOFEPet.oProcesoSeleccionado.Cod, _
                                            sToName:=oPet.nombre & " " & oPet.Apellidos)
            If errormail.NumError <> TESnoerror Then
                oPet.MailCancel = True
                GoTo Error
            ElseIf g_bCancelarMail = True Then
                oPet.MailCancel = True
            End If
            
        End If
                
        ' Notificaciones impresas
        'Generamos un documento de notificaci�n impresa para cada contacto
        
        ' *************** PETICIONES VIA IMPRESO **********************
        
        If oPet.ViaImp Then
            
            bAdjuntarImpreso = True
            
            oPet.MailCancel = False
            
            If appword Is Nothing Then
                frmESPERA.ProgressBar1.Value = 2
                frmESPERA.lblDetalle = sidiInicWord
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set appword = CreateObject("Word.Application")
                bSalvar = appword.Options.SavePropertiesPrompt
                appword.Options.SavePropertiesPrompt = False
                bVisibleWord = appword.Visible
                appword.WindowState = 2
                sVersion = appword.VERSION
                If InStr(1, sVersion, ".") > 1 Then
                    sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
                    m_iWordVer = CInt(sVersion)
                Else
                    m_iWordVer = 9
                End If
            Else
                sVersion = appword.VERSION
            End If
            
            
            frmESPERA.ProgressBar1.Value = 5
            frmESPERA.lblDetalle = sIdiAbrPlant & " " & splantilla
            frmESPERA.lblDetalle.Refresh
            DoEvents
                
            Set docword = CartaImpreso(splantilla, oPet, oEquipo)

            frmESPERA.ProgressBar1.Value = 8
            frmESPERA.lblDetalle = sidiGenImprPet
            frmESPERA.lblDetalle.Refresh
            DoEvents
            
            Set docPetWord = ImpresoObjetivos(oPet)
            If Not docPetWord Is Nothing Then
                ' Tengo que pegar este �ltimo documento en las peticiones anteriores
                docPetWord.Range.Select
                docPetWord.Range.Copy
                docPetWord.Close False
                
                Set rangeword = docword.Range
                rangeword.Collapse 0
                Set Section = docword.Sections.Add(rangeword, 0)
                docword.Sections(2).Range.Paste
            End If
            frmESPERA.ProgressBar1.Value = 10
            frmESPERA.lblDetalle = sidiGenImprPet
            frmESPERA.lblDetalle.Refresh
            DoEvents
        End If
        
Siguiente:
        bComunicando = False
        
        If oPet.MailCancel = False Then
            basSeguridad.RegistrarAccion accionessummit.ACCPetObjAnya, "Anyo:" & CStr(frmOFEPet.sdbcAnyo.Value) & "GMN1:" & CStr(frmOFEPet.sdbcGMN1_4Cod.Value) & "Proce:" & Int(frmOFEPet.sdbcProceCod.Value) & "Prove:" & oPet.CodProve & "Con:" & oPet.Email & " " & oPet.Apellidos
        End If
        
    Next
    
    bComunicando = False
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblDetalle = sIdiAlmacenadoBD
    
    'Modificamos la fecha l�mite de ofertas del proceso
    Set oIPet = frmOFEPet.oProcesoSeleccionado
    
    If bModificarFecLimit Then
        Dim iCancel As Integer
        iCancel = 0
        For Each oPet In oPets
            If oPet.MailCancel = True Then
                iCancel = iCancel + 1
            End If
        Next
        If iCancel < oPets.Count Then
            If (Not IsNull(dFechaLimite) And Not IsNull(frmOFEPet.oProcesoSeleccionado.FechaMinimoLimOfertas) And dFechaLimite <> frmOFEPet.oProcesoSeleccionado.FechaMinimoLimOfertas) Or _
             (Not IsNull(dFechaLimite) And IsNull(frmOFEPet.oProcesoSeleccionado.FechaMinimoLimOfertas)) Or _
             (IsNull(dFechaLimite) And Not IsNull(frmOFEPet.oProcesoSeleccionado.FechaMinimoLimOfertas)) Then
                teserror = oIPet.ModificarFechaLimiteDeOfertas(dtFechaLimiteUTC)
                If teserror.NumError <> TESnoerror Then
                    GoTo Error
                Else
                    basSeguridad.RegistrarAccion accionessummit.ACCPetOfeModFecLimit, "Anyo:" & CStr(frmOFEPet.sdbcAnyo.Value) & "GMN1:" & CStr(frmOFEPet.sdbcGMN1_4Cod.Value) & "Proce:" & Int(frmOFEPet.sdbcProceCod.Value) & "Prove:" & sdbgPet.Columns(0).Value & "Con:" & sdbgPet.Columns(7).Value & " " & sdbgPet.Columns(3).Value
                End If
            End If
        End If
    End If

    
    ' Almacenamos las peticiones en la BD
    ' AHORA SON LOS OBJETIVOS
    NoC = Null
    teserror = oIPet.RealizarComunicacion(oPets)
    
    Dim oIPub As IPublicaciones
    Set oIPub = frmOFEPet.oProcesoSeleccionado
    For Each oPet In oPets
        If oPet.MailCancel = False Then
            If oPet.ViaWeb Then
                oIPub.IActivarPublicacion oPet.CodProve, TPublicacionObjetivo
            Else
                oIPub.IActivarPublicacion oPet.CodProve, TPublicacionObjetivoNoViaWeb
            End If
        End If
    Next
    
    If teserror.NumError <> TESnoerror Then
        GoTo Error
    Else
        If IsArray(teserror.Arg1) Then
            NoC = teserror.Arg1
        End If
    End If
    
    If bSesionIniciada Then
        frmESPERA.ProgressBar1.Value = 5
        frmESPERA.lblDetalle = sIdiCerrSesCorreo
        FinalizarSesionMail
        bSesionIniciada = False
    End If
            
    If Not appword Is Nothing Then appword.Options.SavePropertiesPrompt = bSalvar
    
    ' Miramos si se ha realizado alguna petici�n impresa
    If bAdjuntarImpreso Then
        frmESPERA.ProgressBar1.Value = 10
        frmESPERA.lblDetalle = sIdiVisualImp & sPlantillaWeb & " ..."
        frmESPERA.lblDetalle.Refresh
        DoEvents
        If Not appword Is Nothing Then
            appword.Selection.HomeKey Unit:=6
            appword.WindowState = 1
            appword.Visible = True
        End If
        bVisibleWord = True
    End If
    
    If IsArray(NoC) Then
        oMensajes.ImposiblePublicarPremiums (NoC)
    End If
    
FinalControlado:
    If bSesionIniciada Then
        FinalizarSesionMail
    End If
    
    Set oPets = Nothing
    Set oPet = Nothing
    Set oIPet = Nothing
    Set oProve = Nothing
    Set oItem = Nothing
    Set oEsp = Nothing
    Set oContactos = Nothing
    Set oProves = Nothing
    Set oEquipo = Nothing
    Set oiasignaciones = Nothing
    Set oAsignaciones = Nothing
    If bVisibleWord = False And Not (appword Is Nothing) Then
        ' cuando se realizan solo peticiones via web, queda una tarea
        ' de word abierta que no se destruye con appword = nothing
        appword.Quit
    End If
    Set docword = Nothing
    Set docPetWord = Nothing
    Set rangeword = Nothing
    Set appword = Nothing
               
    Unload frmESPERA
    
    frmOFEPet.cmdRestaurar_Click
    
    Screen.MousePointer = vbNormal
    
    Unload Me
       
    Exit Sub
                    
Error:
    oError.Arg1 = err.Number
    oError.Arg2 = err.Description

    If bComunicando And errormail.NumError <> TESnoerror Then
        'viene de un error en componermensaje con lo que el error ya se ha guardado en BD, muestro un mensaje y siguiente
        If Right(CStr(errormail.Arg2), 3) = "$$4" Then
            oMensajes.ErrorComunicacionMail_MalMail False, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, Left(CStr(errormail.Arg2), Len(CStr(errormail.Arg2)) - 3)
        ElseIf Right(CStr(errormail.Arg2), 3) = "$$5" Then
            oMensajes.ErrorComunicacionMail_MalMail True, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, Left(CStr(errormail.Arg2), Len(CStr(errormail.Arg2)) - 3), errormail.Arg1
        Else
            oMensajes.ErrorComunicacionMail errormail.Arg1, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre
        End If
        bSiguiente = True
    ElseIf teserror.NumError <> TESnoerror Then
        TratarError teserror
        bSiguiente = False
    Else
        If oError.Arg1 = 462 Then
            ResulGrabaError = basErrores.GrabarError("frmObjAnya.cmdAceptar", CStr(oError.Arg1), "Descr:" & oError.Arg2)
            ' the remote server machine... " han cerrado el word"
            Set appword = Nothing
            DoEvents
            Set appword = CreateObject("Word.Application")
            bSalvar = appword.Options.SavePropertiesPrompt
            appword.Options.SavePropertiesPrompt = False
            bVisibleWord = appword.Visible
            appword.WindowState = 2
            sVersion = appword.VERSION
            If InStr(1, sVersion, ".") > 1 Then
                sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
                m_iWordVer = CInt(sVersion)
            Else
                m_iWordVer = 9
            End If
            Resume Next
        Else
            If oPet Is Nothing Then
                ResulGrabaError = basErrores.GrabarError("frmObjAnya.cmdAceptar", CStr(oError.Arg1), "Descr:" & oError.Arg2)
            Else
                ResulGrabaError = basErrores.GrabarError("frmObjAnya.cmdAceptar", CStr(oError.Arg1), "Prove:" & oPet.CodProve & " Con:" & oPet.Apellidos & ", " & oPet.nombre & " Descr:" & oError.Arg2)
            End If
            ResulGrabaError.NumError = TESErrorCodigo
            TratarError ResulGrabaError
            bSiguiente = False
        End If
    End If
    
    If bSiguiente Then
        GoTo Siguiente
    Else
        GoTo FinalControlado
    End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub cmdCalendar_Click()
    AbrirFormCalendar Me, txtFecLimOfe
End Sub

Private Sub cmdCancelar_Click()
   Unload Me
End Sub

Private Sub cmdDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
'    cmmdDot.Filter = "Plantillas de documento (*.dot)|*.dot"
    cmmdDot.Filter = sIdiPlantDoc & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtDOT.Text = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub
End Sub

Private Sub cmdForm_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sIdiPlantDoc & " (*.dot)|*.dot"

    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtForm.Text = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub

End Sub

''' <summary>
''' Elegir la plantilla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdMailDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sIdiPlantDoc & " (*.htm)|*.htm"
    cmmdDot.DefaultExt = "htm"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtMailDot.Text = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub

End Sub

''' <summary>
''' Elegir la plantilla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdWebDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sIdiPlantDoc & " (*.htm)|*.htm"
    cmmdDot.DefaultExt = "htm"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtWebDot.Text = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub

End Sub

Private Sub Form_Activate()
    Dim dcListaZonasHorarias As Dictionary
    Dim oProceso As CProceso
    Dim dtFechaIniSub As Date
    Dim dtHoraIniSub As Date
    Dim dtFechaFinSub As Date
    Dim dtHoraFinSub As Date
    Dim tzInfo As TIME_ZONE_INFO
    
    'Es posible que la zona horaria haya cambiado en otra pantalla, actualizarla
    If oUsuarioSummit.TimeZone <> vTZHora Then
        vTZHora = oUsuarioSummit.TimeZone
        Set dcListaZonasHorarias = ObtenerZonasHorarias
        Set oProceso = oFSGSRaiz.Generar_CProceso
        oProceso.Anyo = Anyo
        oProceso.GMN1Cod = GMN1
        oProceso.Cod = Cod
        oProceso.CargarDatosGeneralesProceso bSinpres:=True, bSinGRupos:=True, sUsu:=oUsuarioSummit.Cod
            
        If Not oProceso Is Nothing Then
            tzInfo = GetTimeZoneByKey(vTZHora)
            lblTZHoraFin = Mid(tzInfo.DisplayName, 2, 9)
            
            'Fecha l�mite ofertas
            If IsNull(oProceso.FechaMinimoLimOfertas) Then
                lblFecLimit = vbNullString
                lblHoraLimite = vbNullString
                    
                If Not oProceso.ModoSubasta Then
                    txtFecLimOfe = ""
                    txtHoraLimite.Text = ""
                End If
            Else
                ConvertirUTCaTZ DateValue(oProceso.FechaMinimoLimOfertas), TimeValue(oProceso.FechaMinimoLimOfertas), vTZHora, dtFechaFinSub, dtHoraFinSub
                    
                lblFecLimit = Format(dtFechaFinSub, "short date")
                lblHoraLimite = Format(dtHoraFinSub, "short time")
                                                
                ConvertirUTCaTZ DateValue(oProceso.FechaMinimoLimOfertas), TimeValue(oProceso.FechaMinimoLimOfertas), vTZHora, dtFechaFinSub, dtHoraFinSub
                
                txtFecLimOfe = Format(dtFechaFinSub, "short date")
                txtHoraLimite.Text = Format(dtHoraFinSub, "short time")
            End If
        End If
        
        Set oProceso = Nothing
        Set dcListaZonasHorarias = Nothing
    End If
End Sub

Private Sub Form_Load()
    
    On Error GoTo Error:
    
    Me.Width = 9885
    Me.Height = 6795
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    

    If Not bModSubasta Then
        If frmOFEPet.oProcesoSeleccionado.ModoSubasta Then
            bModificarFecLimit = False
        End If
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
        
    If bModificarFecLimit Then
        txtFecLimOfe.Enabled = True
        cmdCalendar.Visible = True
        txtHoraLimite.Locked = False
    Else
        txtFecLimOfe.Enabled = False
        cmdCalendar.Visible = False
        txtHoraLimite.Locked = True
    End If
    
    'Inicializar el array de ficheros
    ReDim sayFileNames(0)
    ReDim Nombres(0)
            
    Set oFos = New Scripting.FileSystemObject
    
    Set oProve = oFSGSRaiz.generar_CProveedor
    
    If gParametrosGenerales.giINSTWEB = SinWeb Then
        txtWebDot.Enabled = False
        cmdWebDot.Enabled = False
    End If
    
    If gParametrosGenerales.giMail = 0 Then
        txtMailDot.Enabled = False
        cmdMailDot.Enabled = False
        txtWebDot.Enabled = False
        cmdWebDot.Enabled = False
    End If
    
    sdbgPet.Columns(0).DropDownHwnd = sdbddProveCod.hWnd
    sdbgPet.Columns(1).DropDownHwnd = sdbddProveDen.hWnd
    sdbgPet.Columns(2).DropDownHwnd = sdbddConApe.hWnd
    sdbgPet.Columns(3).DropDownHwnd = sdbddConNom.hWnd
    
    sdbddConApe.AddItem ""
    sdbddConNom.AddItem ""
    sdbddProveCod.AddItem ""
    sdbddProveDen.AddItem ""
    
    Set oIasig = frmOFEPet.oProcesoSeleccionado
    
    Set oProvesAsignados = oFSGSRaiz.generar_CProveedores
    If frmOFEPet.bComunAsigComp Then
        Set oProvesAsignados = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve, gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
    ElseIf frmOFEPet.bComunAsigEqp Then
        Set oProvesAsignados = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve, gCodEqpUsuario)
    Else
        Set oProvesAsignados = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve)
    End If
    
    If gParametrosGenerales.gbProveGrupos Then
        Set m_oAsigs = oIasig.DevolverAsignaciones(OrdAsigPorCodProve, , , , , True)
    End If

    If gParametrosInstalacion.gsObjCarta = "" Then
        If gParametrosGenerales.gsOBJCARTADOT = "" Then
            oMensajes.NoValido sIdiPlantPetOfe
        Else
            gParametrosInstalacion.gsObjCarta = gParametrosGenerales.gsOBJCARTADOT
            g_GuardarParametrosIns = True
        End If
    End If
    
    If gParametrosInstalacion.gsObj = "" Then
        If gParametrosGenerales.gsOBJDOT = "" Then
            oMensajes.NoValida sIdiPComunicObj
        Else
            gParametrosInstalacion.gsObj = gParametrosGenerales.gsOBJDOT
            g_GuardarParametrosIns = True
        End If
    End If
    
    If gParametrosGenerales.giMail <> 0 Then
        If gParametrosInstalacion.gsObjMail = "" Then
            If gParametrosGenerales.gsOBJMAILDOT = "" Then
                oMensajes.NoValida sIdiPlantPetOfeMail
            Else
                gParametrosInstalacion.gsObjMail = gParametrosGenerales.gsOBJMAILDOT
                g_GuardarParametrosIns = True
            End If
        End If
    End If
    
    If gParametrosGenerales.giINSTWEB = conweb Or gParametrosGenerales.giINSTWEB = ConPortal Then
        If gParametrosInstalacion.gsObjWeb = "" Then
            If gParametrosGenerales.gsOBJWEBDOT = "" Then
                oMensajes.NoValida sIdiPlantPetOfe
            Else
                gParametrosInstalacion.gsObjWeb = gParametrosGenerales.gsOBJWEBDOT
                g_GuardarParametrosIns = True
            End If
        End If
    End If
    Dim oEmpresas As CEmpresas
    Dim sCarpeta As String
    Dim sMarcaPlantillas As String
    If InStr(gParametrosInstalacion.gsObjCarta, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsObjMail, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsObjWeb, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsObj, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
        'Buscamos la empresa CARPETA_PLANTILLAS
        
        Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
        sCarpeta = oEmpresas.DevolverCarpetaPlantillaProce_o_Ped(frmOFEPet.sdbcAnyo.Text, frmOFEPet.sdbcGMN1_4Cod.Text, frmOFEPet.sdbcProceCod.Text, 0)
        Set oEmpresas = Nothing
    End If
    If sCarpeta = "" And gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    txtDOT.Text = Replace(gParametrosInstalacion.gsObjCarta, sMarcaPlantillas, sCarpeta)
    txtMailDot.Text = Replace(gParametrosInstalacion.gsObjMail, sMarcaPlantillas, sCarpeta)
    txtWebDot.Text = Replace(gParametrosInstalacion.gsObjWeb, sMarcaPlantillas, sCarpeta)
    txtForm.Text = Replace(gParametrosInstalacion.gsObj, sMarcaPlantillas, sCarpeta)
    
    Exit Sub

Error:
    
    MsgBox err.Description
    
    
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim irespuesta As Integer
Dim i As Integer

On Error GoTo Error:
    
    
    'Borramos los archivos temporales que hayamos creado
    Set oProvesAsignados = Nothing
    g_bCancelarMail = False
    
    i = 0
    While i < UBound(sayFileNames)
        
        If oFos.FileExists(sayFileNames(i)) Then
            oFos.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    
    
    Set oFos = Nothing
    Me.Visible = False
    Exit Sub

Error:
    basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
    ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
    Resume Next
End Sub

''' <summary>
''' CLose up del combo del apellido de contacto
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbddConApe_CloseUp()
Dim oProves As CProveedores

    If sdbddConApe.Columns(0).Value <> "" Then
        Screen.MousePointer = vbHourglass
        
        sdbgPet.Columns(2).Value = sdbddConApe.Columns(0).Value
        sdbgPet.Columns(3).Value = sdbddConApe.Columns(1).Value
        sdbgPet.Columns(7).Value = sdbddConApe.Columns(2).Value
        sdbgPet.Columns(8).Value = sdbddConApe.Columns(3).Value
        sdbgPet.Columns("TFNO").Value = sdbddConApe.Columns("TFNO").Value
        sdbgPet.Columns("TFNO2").Value = sdbddConApe.Columns("TFNO2").Value
        sdbgPet.Columns("FAX").Value = sdbddConApe.Columns("FAX").Value
        sdbgPet.Columns("TFNO_MOVIL").Value = sdbddConApe.Columns("TFNO_MOVIL").Value
        sdbgPet.Columns("PORTALCON").Value = sdbddConApe.Columns("PORT").Value
        sdbgPet.Columns("TIPOEMAIL").Value = sdbddConApe.Columns("TIPOEMAIL").Value
        
        If sdbddConApe.Columns(2).Value <> "" Then
        ' contacto con e-mail
            If gParametrosGenerales.giMail = 0 Then
                ' instalacion SIN clte mail
                sdbgPet.Columns(4).Value = 0
                sdbgPet.Columns(5).Value = 0
                sdbgPet.Columns(6).Value = 1
            Else
                If gParametrosGenerales.giINSTWEB = ConPortal Then
                    ' peticion via web para proveedores del portal
                    Set oProves = Nothing
                    Set oProves = oFSGSRaiz.generar_CProveedores
                    oProves.Add sdbgPet.Columns(0).Value, sdbgPet.Columns(1).Value
                    oProves.CargarDatosProveedor sdbgPet.Columns(0).Value
                    If IsNull(oProves.Item(1).CodPortal) Or oProves.Item(1).CodPortal = "" Then
                        sdbgPet.Columns(4).Value = 0
                        sdbgPet.Columns(5).Value = 1
                        sdbgPet.Columns(6).Value = 0
                    Else
                        sdbgPet.Columns(4).Value = 1
                        sdbgPet.Columns(5).Value = 0
                        sdbgPet.Columns(6).Value = 0
                    End If
                Else
                    If gParametrosGenerales.giINSTWEB = conweb Then
                        sdbgPet.Columns(4).Value = 1
                        sdbgPet.Columns(5).Value = 0
                        sdbgPet.Columns(6).Value = 0
                    Else
                        sdbgPet.Columns(4).Value = 0
                        sdbgPet.Columns(5).Value = 1
                        sdbgPet.Columns(6).Value = 0
                    End If
                End If  'instalacion fullstep
            End If 'clte mail instalado
        Else ' contacto sin e-mail
            sdbgPet.Columns(4).Value = 0
            sdbgPet.Columns(5).Value = 0
            sdbgPet.Columns(6).Value = 1
        End If
        
        Screen.MousePointer = vbNormal
    End If

End Sub
Private Sub sdbddConApe_DropDown()
    
    If sdbgPet.Columns(0).Value = "" Then Exit Sub
    Screen.MousePointer = vbHourglass
    
    oProve.Cod = sdbgPet.Columns(0).Value
    
    Set oContactos = Nothing
    If bCargarComboDesdeDD Then
        Set oContactos = oProve.CargarTodosLosContactos(sdbgPet.ActiveCell.Value, , , , True)
    Else
        Set oContactos = oProve.CargarTodosLosContactos(, , , , True)
    End If
    
        
    CargarGridConContactosApe
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddConApe_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddConApe.DataFieldList = "Column 0"
    sdbddConApe.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbddConApe_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    If sdbddConApe.Columns("PORT").Value = "1" Then
        For i = 0 To sdbddConApe.Cols - 1
            sdbddConApe.Columns(i).CellStyleSet "Tan"
        Next
    End If
End Sub

''' <summary>
''' CLose up del combo del nombre de contacto
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbddConNom_CloseUp()
Dim oProves As CProveedores

    If sdbddConNom.Columns(1).Value <> "" Then
        Screen.MousePointer = vbHourglass
        
        sdbgPet.Columns(3).Value = sdbddConNom.Columns(0).Value
        sdbgPet.Columns(2).Value = sdbddConNom.Columns(1).Value
        sdbgPet.Columns(7).Value = sdbddConNom.Columns(2).Value
        sdbgPet.Columns(8).Value = sdbddConNom.Columns(3).Value
        sdbgPet.Columns("TFNO").Value = sdbddConNom.Columns("TFNO").Value
        sdbgPet.Columns("TFNO2").Value = sdbddConNom.Columns("TFNO2").Value
        sdbgPet.Columns("FAX").Value = sdbddConNom.Columns("FAX").Value
        sdbgPet.Columns("TFNO_MOVIL").Value = sdbddConNom.Columns("TFNO_MOVIL").Value
        sdbgPet.Columns("PORTALCON").Value = sdbddConNom.Columns("PORT").Value
        sdbgPet.Columns("TIPOEMAIL").Value = sdbddConNom.Columns("TIPOEMAIL").Value
        
        If sdbddConNom.Columns(2).Value <> "" Then
        ' contacto con e-mail
            If gParametrosGenerales.giMail = 0 Then
                ' instalacion SIN clte mail
                sdbgPet.Columns(4).Value = 0
                sdbgPet.Columns(5).Value = 0
                sdbgPet.Columns(6).Value = 1
            Else
                If gParametrosGenerales.giINSTWEB = ConPortal Then
                    ' peticion via web para proveedores del portal
                    Set oProves = Nothing
                    Set oProves = oFSGSRaiz.generar_CProveedores
                    oProves.Add sdbgPet.Columns(0).Value, sdbgPet.Columns(1).Value
                    oProves.CargarDatosProveedor sdbgPet.Columns(0).Value
                    If IsNull(oProves.Item(1).CodPortal) Or oProves.Item(1).CodPortal = "" Then
                        sdbgPet.Columns(4).Value = 0
                        sdbgPet.Columns(5).Value = 1
                        sdbgPet.Columns(6).Value = 0
                    Else
                        sdbgPet.Columns(4).Value = 1
                        sdbgPet.Columns(5).Value = 0
                        sdbgPet.Columns(6).Value = 0
                    End If
                Else
                    If gParametrosGenerales.giINSTWEB = conweb Then
                        sdbgPet.Columns(4).Value = 1
                        sdbgPet.Columns(5).Value = 0
                        sdbgPet.Columns(6).Value = 0
                    Else
                        sdbgPet.Columns(4).Value = 0
                        sdbgPet.Columns(5).Value = 1
                        sdbgPet.Columns(6).Value = 0
                    End If
                End If  'instalacion fullstep
            End If 'clte mail instalado
        Else 'contacto sin e-mail
            sdbgPet.Columns(4).Value = 0
            sdbgPet.Columns(5).Value = 0
            sdbgPet.Columns(6).Value = 1
        End If
        
        Screen.MousePointer = vbNormal
    End If
    
End Sub

Private Sub sdbddConNom_DropDown()
    
    If sdbgPet.Columns(0).Value = "" Then Exit Sub
    Screen.MousePointer = vbHourglass
    
    oProve.Cod = sdbgPet.Columns(0).Value
    
    Set oContactos = Nothing
    If bCargarComboDesdeDD Then
        Set oContactos = oProve.CargarTodosLosContactos(, sdbgPet.ActiveCell.Value, , True)
    Else
        Set oContactos = oProve.CargarTodosLosContactos(, , , True)
    End If
    
        
    CargarGridConContactosNom
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddConNom_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddConNom.DataFieldList = "Column 0"
    sdbddConNom.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub XsdbddConNom_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddConNom.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddConNom.Rows - 1
            bm = sdbddConNom.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddConNom.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddConNom.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub XsdbddConNom_ValidateList(Text As String, RtnPassed As Integer)
Dim sCod As String

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddConNom.Columns(0).Value = Text Then
            sdbgPet.Columns(2).Value = sdbddConNom.Columns(1).Value
            sdbgPet.Columns(6).Value = sdbddConNom.Columns(2).Value
            sdbgPet.Columns(7).Value = sdbddConNom.Columns(3).Value
            RtnPassed = True
            Exit Sub
        End If
        
        Set oContactos = oProve.CargarTodosLosContactos(, Text, True)
        
        If Not oContactos.Item(1) Is Nothing Then
            RtnPassed = True
            
            sdbgPet.Columns(2).Value = oContactos.Item(1).Apellidos
            sdbgPet.Columns(6).Value = oContactos.Item(1).mail
            sdbgPet.Columns(7).Value = oContactos.Item(1).Id
            
            
        Else
            sdbgPet.Columns(2).Value = ""
            sdbgPet.Columns(3).Value = ""
            sdbgPet.Columns(6).Value = ""
            sdbgPet.Columns(7).Value = ""
        End If
    
    End If
    
End Sub

Private Sub sdbddConNom_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    If sdbddConNom.Columns("PORT").Value = "1" Then
        For i = 0 To sdbddConNom.Cols - 1
            sdbddConNom.Columns(i).CellStyleSet "Tan"
        Next
    End If
End Sub

Private Sub sdbddProveCod_CloseUp()
    
    If sdbddProveCod.Columns(0).Value <> "" Then
        sdbgPet.Columns(1).Value = sdbddProveCod.Columns(1).Value
    End If
    
End Sub
Private Sub sdbddProveCod_DropDown()

    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    
    Screen.MousePointer = vbHourglass
    
    If frmOFEPet.bComunAsigComp Then
    
        If bCargarComboDesdeDD Then
            Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", , OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            Set oProves = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve, gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        End If
    Else
    
        If frmOFEPet.bComunAsigEqp Then
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", , OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve, gCodEqpUsuario)
            End If
        Else
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "")
            Else
                Set oProves = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve)
            End If
        End If
    
    End If
        
    CargarGridConProveCod
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddProveCod_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddProveCod.DataFieldList = "Column 0"
    sdbddProveCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddProveCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveCod.Rows - 1
            bm = sdbddProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbddProveCod_RowLoaded(ByVal Bookmark As Variant)
    If oProvesAsignados Is Nothing Then Exit Sub
    If oProvesAsignados.Item(sdbddProveCod.Columns("COD").Value) Is Nothing Then Exit Sub
    If NullToStr(oProvesAsignados.Item(sdbddProveCod.Columns("COD").Value).CodPortal) <> "" And _
      gParametrosGenerales.giINSTWEB = ConPortal Then
        sdbddProveCod.Columns("COD").CellStyleSet "ProvPortal"
        sdbddProveCod.Columns("DEN").CellStyleSet "ProvPortal"
    Else
        sdbddProveCod.Columns("COD").CellStyleSet "Normal"
        sdbddProveCod.Columns("DEN").CellStyleSet "Normal"
    End If
End Sub

Private Sub sdbddProveCod_ValidateList(Text As String, RtnPassed As Integer)
Dim sCod As String
Dim oProves As CProveedores
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
        
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddProveCod.Columns(0).Value = Text Then
            RtnPassed = True
            sdbgPet.Columns(1).Value = sdbddProveCod.Columns(1).Value
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        
        If frmOFEPet.bComunAsigComp Then
            Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", True, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If frmOFEPet.bComunAsigEqp Then
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", True, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", True)
            End If
        End If
        
        If Not oProves.Item(1) Is Nothing Then
            RtnPassed = True
            sdbgPet.Columns(0).Value = oProves.Item(1).Cod
            sdbgPet.Columns(1).Value = oProves.Item(1).Den
        Else
            sdbgPet.Columns(0).Value = ""
            sdbgPet.Columns(1).Value = ""
        End If
    
    End If
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbddProveDen_CloseUp()
    
    If sdbddProveDen.Columns(0).Value <> "" Then
        sdbgPet.Columns(0).Value = sdbddProveDen.Columns(1).Value
    End If
    
End Sub

Private Sub sdbddProveDen_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddProveDen.DataFieldList = "Column 0"
    sdbddProveDen.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbddProveDen_DropDown()

    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    Screen.MousePointer = vbHourglass
    
    If frmOFEPet.bComunAsigComp Then
    
        If bCargarComboDesdeDD Then
            Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, , OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            Set oProves = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorDenProve, gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        End If
    Else
    
        If frmOFEPet.bComunAsigEqp Then
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, , OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorDenProve, gCodEqpUsuario)
            End If
        Else
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, , OrdAsigPorDenProve)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde("", "", OrdAsigPorDenProve)
            End If
        End If
    
    End If
        
    CargarGridConProveDen
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddProveDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveDen.Rows - 1
            bm = sdbddProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbddProveDen_RowLoaded(ByVal Bookmark As Variant)
    If oProvesAsignados Is Nothing Then Exit Sub
    If oProvesAsignados.Item(sdbddProveDen.Columns("COD").Value) Is Nothing Then Exit Sub
    If NullToStr(oProvesAsignados.Item(sdbddProveDen.Columns("COD").Value).CodPortal) <> "" And _
      gParametrosGenerales.giINSTWEB = ConPortal Then
        sdbddProveDen.Columns("COD").CellStyleSet "ProvPortal"
        sdbddProveDen.Columns("DEN").CellStyleSet "ProvPortal"
    Else
        sdbddProveDen.Columns("COD").CellStyleSet "Normal"
        sdbddProveDen.Columns("DEN").CellStyleSet "Normal"
    End If
End Sub

Private Sub sdbddProveDen_ValidateList(Text As String, RtnPassed As Integer)
Dim sCod As String
Dim oProves As CProveedores
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
        
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddProveDen.Columns(0).Value = Text Then
            RtnPassed = True
            sdbgPet.Columns(0).Value = sdbddProveDen.Columns(1).Value
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        
        If frmOFEPet.bComunAsigComp Then
            Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If frmOFEPet.bComunAsigEqp Then
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve)
            End If
        End If
        
        If Not oProves.Item(1) Is Nothing Then
            RtnPassed = True
            sdbgPet.Columns(0).Value = oProves.Item(1).Cod
            sdbgPet.Columns(1).Value = oProves.Item(1).Den
        Else
            sdbgPet.Columns(0).Value = ""
            sdbgPet.Columns(1).Value = ""
        End If
    
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgPet_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgPet_Change()
    
    Select Case sdbgPet.col
    
    Case 0, 1
        
        sdbgPet.Columns(2).Value = ""
        sdbgPet.Columns(3).Value = ""
        sdbgPet.Columns(4).Value = 0
        sdbgPet.Columns(5).Value = 0
        sdbgPet.Columns(6).Value = 0
        sdbgPet.Columns(7).Value = ""
        sdbgPet.Columns(8).Value = ""
        sdbgPet.Columns("PORTALCON").Value = ""
        
    Case 4
        
        If sdbgPet.Columns(4).Value = "1" Or sdbgPet.Columns(4).Value = "-1" Then
            If Not bModificarFecLimit And IsDate(lblFecLimit) Then
                If CDate(lblFecLimit) < Date Then
                    oMensajes.NoValida sIdiFecLimite
                    sdbgPet.DataChanged = False
                    Exit Sub
                End If
            End If

            If sdbgPet.Columns("EMAIL").Value = "" Then
'               oMensajes.NoValida "Direcci�n de correo electr�nico"
                oMensajes.NoValida sIdiEmail
                sdbgPet.DataChanged = False
                Exit Sub
            Else
                sdbgPet.Columns(5).Value = 0
                sdbgPet.Columns(6).Value = 0
            End If
        End If
    
    Case 5
        
        If sdbgPet.Columns(5).Value = "-1" Or sdbgPet.Columns(5).Value = "1" Then
            If sdbgPet.Columns("EMAIL").Value = "" Then
'                oMensajes.NoValida "Direcci�n de correo electr�nico"
                oMensajes.NoValida sIdiEmail
                sdbgPet.DataChanged = False
                Exit Sub
            Else
                sdbgPet.Columns(4).Value = 0
                sdbgPet.Columns(6).Value = 0
            End If
        End If
        
    Case 6
        
        If sdbgPet.Columns(6).Value = "1" Or sdbgPet.Columns(6).Value = "-1" Then
            sdbgPet.Columns(5).Value = 0
            sdbgPet.Columns(4).Value = 0
        End If
        
        
    End Select
    
    
End Sub

Private Sub sdbgPet_InitColumnProps()
    
    sdbgPet.Columns(2).Locked = True
    sdbgPet.Columns(3).Locked = True
    
    If gParametrosGenerales.giINSTWEB = SinWeb Then
        sdbgPet.Columns("WEB").Locked = True
    End If
    
    If gParametrosGenerales.giMail = 0 Then
        sdbgPet.Columns("WEB").Locked = True
        sdbgPet.Columns("MAIL").Locked = True
    End If
    

End Sub

''' <summary>
''' Carga de los recursos de la pagina
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde;Load de la pagina Tiempo m�ximo 0</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_OBJANYA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    caption = Ador(0).Value
    Ador.MoveNext
    
    lblFecNec.caption = Ador(0).Value
    Ador.MoveNext
    lblFecPres.caption = Ador(0).Value
    Ador.MoveNext
    lblFecLimOfeOld.caption = Ador(0).Value
    Ador.MoveNext
    lblFecReu.caption = Ador(0).Value
    Ador.MoveNext
    lblDOT.caption = Ador(0).Value
    Ador.MoveNext
    lblMailDot.caption = Ador(0).Value
    Ador.MoveNext
    lblWebDot.caption = Ador(0).Value
    Ador.MoveNext
    chkEsp.caption = Ador(0).Value
    Ador.MoveNext
    chkFich.caption = Ador(0).Value
    Ador.MoveNext
    sdbgPet.Groups(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbgPet.Groups(1).caption = Ador(0).Value
    Ador.MoveNext
    sdbgPet.Groups(2).caption = Ador(0).Value
    Ador.MoveNext
    
    sdbgPet.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbgPet.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    sdbgPet.Columns(2).caption = Ador(0).Value
    Ador.MoveNext
    sdbgPet.Columns(3).caption = Ador(0).Value
    Ador.MoveNext
    sdbgPet.Columns(4).caption = Ador(0).Value
    Ador.MoveNext
    sdbgPet.Columns(5).caption = Ador(0).Value
    Ador.MoveNext
    sdbgPet.Columns(6).caption = Ador(0).Value
    Ador.MoveNext
    sdbddProveCod.Columns(0).caption = Ador(0).Value
    sdbddProveDen.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    
    sdbddProveCod.Columns(1).caption = Ador(0).Value
    sdbddProveDen.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    
    sdbddConApe.Columns(0).caption = Ador(0).Value
    sdbddConNom.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    
    sdbddConApe.Columns(1).caption = Ador(0).Value
    sdbddConNom.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    
    sdbddConApe.Columns(2).caption = Ador(0).Value
    sdbddConNom.Columns(2).caption = Ador(0).Value
    Ador.MoveNext
    
    
    cmdAceptar.caption = Ador(0).Value
    Ador.MoveNext
    
    cmdCancelar.caption = Ador(0).Value
    Ador.MoveNext
    
    sIdiPComunicObj = Ador(0).Value
    Ador.MoveNext
    'Plantilla para comunicaci�n de objetivos.
    sIdiPlantilla = Ador(0).Value
    Ador.MoveNext
    'Plantilla
    sIdiFecLimite = Ador(0).Value
    Ador.MoveNext
    'Fecha limite de ofertas
    sIdiGenComunicObj = Ador(0).Value
    Ador.MoveNext
    'Generando comunicaci�n de objetivos...
    sIdiCreandoPet = Ador(0).Value
    Ador.MoveNext
    'Creando peticiones...
    sIdiContacto = Ador(0).Value
    Ador.MoveNext
    'Contacto:
    sIdiInicSesCorreo = Ador(0).Value
    Ador.MoveNext
    'Iniciando sesi�n de correo ...
    sidiInicWord = Ador(0).Value
    Ador.MoveNext
    'Iniciando Microsoft Word...
    sIdiAbrPlant = Ador(0).Value
    Ador.MoveNext
    'Abriendo Plantilla
    sIdiCopiDocEmail = Ador(0).Value
    Ador.MoveNext
    'Copiando documento a e-mail...
    sIdiEnvMensaje = Ador(0).Value
    Ador.MoveNext
    'Enviando mensaje ...
    sIdiAnyaPlant = Ador(0).Value
    Ador.MoveNext
    'A�adiendo Plantilla
    sidiGenImprPet = Ador(0).Value
    Ador.MoveNext
    'Generando impreso de peticiones ...
    sIdiAdjEspec = Ador(0).Value
    Ador.MoveNext
    'Adjuntando especificaci�n:
    sIdiCerrSesCorreo = Ador(0).Value
    Ador.MoveNext
    'Cerrando sesi�n de correo...
    sIdiCreandoImp = Ador(0).Value
    Ador.MoveNext
    'Creando impreso de petici�n ...
    sIdiVisualImp = Ador(0).Value
    Ador.MoveNext
    'Visualizando impreso ...
    sIdiPlantDoc = Ador(0).Value
    Ador.MoveNext
    'Plantillas de documento
    sIdiPlantPetOfe = Ador(0).Value
    Ador.MoveNext
    'Plantilla de petici�n de ofertas.
    sIdiPlantPetOfeMail = Ador(0).Value
    Ador.MoveNext
    'Plantilla de petici�n de ofertas para mail.
    sIdiEmail = Ador(0).Value
    Ador.MoveNext
    sImpreso = Ador(0).Value
    'Direcci�n de correo electr�nico
    Ador.MoveNext
    m_stxtOferta = Ador(0).Value
    Ador.MoveNext
    m_stxtGrupo = Ador(0).Value
    Ador.MoveNext
    m_stxtItem = Ador(0).Value
    Ador.MoveNext
    m_stxtUniItem = Ador(0).Value
    
    Ador.MoveNext
    m_stxtTexto = Ador(0).Value
    Ador.MoveNext
    m_stxtNumero = Ador(0).Value
    Ador.MoveNext
    m_stxtFecha = Ador(0).Value
    Ador.MoveNext
    m_stxtSi = Ador(0).Value
    Ador.MoveNext
    m_stxtMinimo = Ador(0).Value
    Ador.MoveNext
    m_stxtMaximo = Ador(0).Value
    Ador.MoveNext
    m_sHoraLim = Ador(0).Value
    Ador.MoveNext
    Label5.caption = Ador(0).Value
    Label2.caption = Ador(0).Value
    Ador.MoveNext
    sIdiAlmacenadoBD = Ador(0).Value
    Ador.MoveNext
    sIdiPreparandoExcel = Ador(0).Value
    Ador.MoveNext
    chkExcel.caption = Ador(0).Value
    
    Ador.MoveNext
    lblFecLimOfeNew.caption = Ador(0).Value
    Ador.MoveNext
    lblForm.caption = Ador(0).Value
    
        Ador.MoveNext
        m_sSi = Ador(0).Value 'S�
        Ador.MoveNext
        m_sNo = Ador(0).Value 'No
        Ador.MoveNext
        chkExcelEspe.caption = Ador(0).Value
        Ador.Close
        
        Ador.MoveNext
        m_sProceso = Ador(0).Value
        Ador.MoveNext
        m_sGrupo = Ador(0).Value
        Ador.MoveNext
        m_sItem = Ador(0).Value
        
    End If

    Set Ador = Nothing



End Sub


Private Sub sdbgPet_RowLoaded(ByVal Bookmark As Variant)
    If Not oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").Value) Is Nothing Then
        If NullToStr(oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").Value).CodPortal) <> "" And _
          gParametrosGenerales.giINSTWEB = ConPortal Then
            sdbgPet.Columns("CODPROVE").CellStyleSet "ProvPortal"
            sdbgPet.Columns("DENPROVE").CellStyleSet "ProvPortal"
        Else
            sdbgPet.Columns("CODPROVE").CellStyleSet "Normal"
            sdbgPet.Columns("DENPROVE").CellStyleSet "Normal"
        End If
    End If
    
    If sdbgPet.Columns("PORTALCON").Value = "1" Then
        sdbgPet.Columns("APE").CellStyleSet "ProvPortal"
        sdbgPet.Columns("NOM").CellStyleSet "ProvPortal"
    Else
        sdbgPet.Columns("APE").CellStyleSet "Normal"
        sdbgPet.Columns("NOM").CellStyleSet "Normal"
    End If
End Sub

''' <summary>
''' Genera el excel de la oferta
''' </summary>
''' <param name="oPet">Oferta</param>
''' <param name="sTarget">Path en el que se copia el excel</param>
''' <returns></returns>
''' <remarks>Llamada desde;cmdAceptar_Click Tiempo m�ximo 0</remarks>

Private Function GenerarExcel(ByVal oPet As CPetOferta, ByVal sTarget As String) As Object
Dim sConnect As String
Dim oExcelAdoConn As ADODB.Connection
Dim oExcelAdoRS As ADODB.Recordset
Dim adoComm As ADODB.Command
Dim adoParam As ADODB.Parameter
Dim SQL As String
Dim xls() As String
Dim iNumCols As Integer
Dim oGrupo As CGrupo
Dim oOfeGrupo As COfertaGrupo
Dim oatrib As CAtributo
Dim oItem As CItem
Dim oEsp As CEspecificacion
Dim oValor As CValorPond
Dim oAtribOfe As CAtributoOfertado
Dim oAtribsOfe As CAtributosOfertados
Dim i As Integer
Dim oMon As CMoneda
Dim iFila As Integer
Dim iFilaInicio As Integer
Dim iNumFilasPorAtrib As Integer
Dim oAtribEsp As CAtributo
Dim lCountAtrEspeGrupo  As Long

iNumFilasPorAtrib = 13
Dim oProceso As CProceso
Dim oOferta As COferta
Dim oPrecioItem As CPrecioItem
Dim lCountEspecsGrupo  As Long
Dim sPathXls As String
Dim sCod As String
Dim oFSO As Object
Dim oFile As Scripting.File
Dim Ador As Ador.Recordset
Dim oOfe As COferta
Dim j As Integer
Dim bGenerarHoja As Boolean
Dim bSinOfertas As Boolean
Dim scodProve As String
Dim bCargar As Boolean
Dim lNumGrupos As Long
Dim sTexto As String

On Error GoTo Error

Set oProceso = frmOFEPet.oProcesoSeleccionado

Screen.MousePointer = vbHourglass

bGenerarHoja = True
    
Set oOferta = oProceso.CargarUltimaOfertaProveedor(oPet.CodProve)

If oOferta Is Nothing Then
    Set oOferta = oFSGSRaiz.Generar_COferta
    oOferta.Anyo = oProceso.Anyo
    oOferta.GMN1Cod = oProceso.GMN1Cod
    oOferta.Proce = oProceso.Cod
    oOferta.Prove = oPet.CodProve
    oOferta.Num = 1
    oOferta.CodMon = oProceso.MonCod
        
    bSinOfertas = True
    'comprueba si se ha generado ya la hoja excel:
    If gParametrosGenerales.gbProveGrupos = False Then
        Set oFSO = CreateObject("Scripting.filesystemobject")
        If oFSO.FileExists(DevolverPathFichTemp & ValidFilename(oProceso.Anyo & "_" & oProceso.GMN1Cod & "_" & oProceso.Cod & ".xls")) Then
            bGenerarHoja = False
        End If
        Set oFSO = Nothing
    End If
Else
    sCod = oPet.CodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oPet.CodProve))
    sCod = sCod & CStr(oOferta.Num)
    Set oOferta = m_oOfertasProceso.Item(sCod)
End If

If bGenerarHoja = True Then
    'Genera la excel
    Dim dcListaZonasHorarias As Dictionary
    Set dcListaZonasHorarias = ObtenerZonasHorarias
    
    If IsNull(oUsuarioSummit.TimeZone) Or IsEmpty(oUsuarioSummit.TimeZone) Then
        oUsuarioSummit.TimeZone = GetTimeZone.key
    End If
    
    Dim oExcel As CExcelOfertas
    Set oExcel = New CExcelOfertas
    If oExcel.FSXLSOfertaOffline(oProceso, oPet.CodProve, m_oAsigs, sTarget, basParametros.gLongitudesDeCodigos.giLongCodPROVE, oFSGSRaiz, basPublic.gParametrosInstalacion.gIdioma, gParametrosGenerales.gbProveGrupos, _
            oGestorIdiomas, gParametrosInstalacion.giCargaMaximaCombos, False, gParametrosInstalacion.giAnyadirEspecArticulo, , Mid(dcListaZonasHorarias.Item(oUsuarioSummit.TimeZone), 2, 9)) Then
        Set oFSO = CreateObject("Scripting.filesystemobject")
        Set oFile = oFSO.GetFile(sTarget)
    
        'Copia la hoja y la renombra sin el prov al temporal.Se mantiene ah� para el siguiente prov. sin ofertas:
        If bSinOfertas = True Then
            sPathXls = DevolverPathFichTemp & oProceso.Anyo & "_" & oProceso.GMN1Cod & "_" & oProceso.Cod & ".xls"
            Set oFSO = CreateObject("Scripting.filesystemobject")
            oFSO.CopyFile sTarget, sPathXls
            Set oFSO = Nothing
        End If
    End If
    Set dcListaZonasHorarias = Nothing
    Set oExcel = Nothing
Else
    'Copia la excel y la renombra
    sPathXls = DevolverPathFichTemp & oProceso.Anyo & "_" & oProceso.GMN1Cod & "_" & oProceso.Cod & ".xls"
    Set oFSO = CreateObject("Scripting.filesystemobject")
    oFSO.CopyFile sPathXls, sTarget
    Set oFile = oFSO.GetFile(sTarget)
    Set oFSO = Nothing
End If

Set oOferta = Nothing

Set GenerarExcel = oFile

Screen.MousePointer = vbNormal

fin:
Exit Function

Error:
If err.Number = 9 Then
    ReDim Preserve xls(0 To UBound(xls, 1), 0 To UBound(xls, 2) + 10) As String
    Resume 0
ElseIf err.Number = 32755 Then
    Resume fin
Else
    Resume Next
    Resume 0
End If

End Function

Private Sub txtFecLimOfe_LostFocus()
    If txtFecLimOfe.Text = "" Then
        txtHoraLimite.Text = ""
    Else
        If txtHoraLimite.Text = "" Then txtHoraLimite.Text = TimeValue("23:59")
    End If
End Sub

''' <summary>
''' Adjuntar Especificaciones
''' </summary>
''' <param name="sTemp">ruta archivos</param>
''' <param name="scodProve">Proveedor</param>
''' <remarks>Llamada desde: cmdAceptar_click ; Tiempo m�ximo: 0,2</remarks>
Private Sub AdjuntarEspecificaciones(ByVal sTemp As String, ByVal scodProve As String)
    Dim oEsp As CEspecificacion
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    
    'Primero adjuntamos las del proceso
    If frmOFEPet.oProcesoSeleccionado.especificaciones Is Nothing Then frmOFEPet.oProcesoSeleccionado.CargarTodasLasEspecificaciones
    If frmOFEPet.oProcesoSeleccionado.especificaciones.Count > 0 Then
        For Each oEsp In frmOFEPet.oProcesoSeleccionado.especificaciones
            frmESPERA.ProgressBar1.Value = 6
            frmESPERA.lblDetalle = sIdiAdjEspec & oEsp.nombre
            frmESPERA.lblDetalle.Refresh
            DoEvents

            EscribirEspecificacionADisco oEsp, TipoEspecificacion.EspProceso

            sayFileNames(UBound(sayFileNames)) = sTemp & oEsp.nombre
            ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
            Nombres(UBound(Nombres)) = oEsp.nombre
            ReDim Preserve Nombres(UBound(Nombres) + 1)
        Next

    End If

    'Ahora adjuntamos las de los grupos e items del proceso
    If frmOFEPet.oProcesoSeleccionado.Grupos Is Nothing Then frmOFEPet.oProcesoSeleccionado.CargarGrupos bSoloAbiertos:=True
    For Each oGrupo In frmOFEPet.oProcesoSeleccionado.Grupos
        If oGrupo.especificaciones Is Nothing Then oGrupo.CargarTodasLasEspecificaciones
        If oGrupo.especificaciones.Count = 0 Then oGrupo.CargarTodasLasEspecificaciones
        If oGrupo.especificaciones.Count > 0 Then
            For Each oEsp In oGrupo.especificaciones
                frmESPERA.ProgressBar1.Value = 7
                frmESPERA.lblDetalle = sIdiAdjEspec & oEsp.nombre
                frmESPERA.lblDetalle.Refresh
                DoEvents

                EscribirEspecificacionADisco oEsp, TipoEspecificacion.EspGrupo

                sayFileNames(UBound(sayFileNames)) = sTemp & oEsp.nombre
                ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                Nombres(UBound(Nombres)) = oEsp.nombre
                ReDim Preserve Nombres(UBound(Nombres) + 1)
            Next
        End If
        If oGrupo.Items Is Nothing Then oGrupo.CargarTodosLosItems udtcriterioOrdenacion:=OrdItemPorOrden, bSoloConfirmados:=True, TipoComunicacion:=2
        For Each oItem In oGrupo.Items
            If oItem.especificaciones Is Nothing Then oItem.CargarTodasLasEspecificaciones
            If oItem.especificaciones.Count > 0 Then
                For Each oEsp In oItem.especificaciones
                    frmESPERA.ProgressBar1.Value = 8
                    frmESPERA.lblDetalle = sIdiAdjEspec & oEsp.nombre
                    frmESPERA.lblDetalle.Refresh
                    DoEvents

                    EscribirEspecificacionADisco oEsp, TipoEspecificacion.EspItem

                    sayFileNames(UBound(sayFileNames)) = sTemp & oEsp.nombre
                    ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                    Nombres(UBound(Nombres)) = oEsp.nombre
                    ReDim Preserve Nombres(UBound(Nombres) + 1)
                Next
            End If
        Next
    Next
End Sub

''' <summary>
''' Adjuntar Excel
''' </summary>
''' <param name="oPet">comunicaci�n</param>
''' <remarks>Llamada desde: cmdAceptar_click ; Tiempo m�ximo: 0,2</remarks>
Private Sub AdjuntarExcel(ByVal oPet As CPetOferta)
    Dim sTemp As String
    Dim sTarget As String
    Dim docPetExcel As Object

    frmESPERA.ProgressBar1.Value = 9
    frmESPERA.lblDetalle = sIdiPreparandoExcel
    frmESPERA.lblDetalle.Refresh
    DoEvents

    If sTemp = "" Then
        sTemp = DevolverPathFichTemp
    End If
    sTarget = frmOFEPet.oProcesoSeleccionado.Anyo & "_" & frmOFEPet.oProcesoSeleccionado.GMN1Cod & "_" & frmOFEPet.oProcesoSeleccionado.Cod & "_" & oPet.CodProve & ".xls"
    Set docPetExcel = GenerarExcel(oPet, sTemp & sTarget)
    If Not docPetExcel Is Nothing Then
        sayFileNames(UBound(sayFileNames)) = sTemp & sTarget
        ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
        Nombres(UBound(Nombres)) = sTarget
        ReDim Preserve Nombres(UBound(Nombres) + 1)
    End If
    Set docPetExcel = Nothing
End Sub

