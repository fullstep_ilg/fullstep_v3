VERSION 5.00
Begin VB.Form frmINTWebService 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DConexi�n webservice para X"
   ClientHeight    =   2130
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8640
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmINTWebService.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2130
   ScaleWidth      =   8640
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   1455
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   8415
      Begin VB.TextBox txtDirUrl 
         Height          =   285
         Left            =   1320
         MaxLength       =   1000
         TabIndex        =   0
         Top             =   360
         Width           =   6765
      End
      Begin VB.TextBox txtPwd 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   5400
         MaxLength       =   50
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   840
         Width           =   2685
      End
      Begin VB.TextBox txtUsu 
         Height          =   285
         Left            =   1320
         MaxLength       =   50
         TabIndex        =   1
         Top             =   840
         Width           =   2685
      End
      Begin VB.Label LblDirIP 
         BackColor       =   &H00808000&
         Caption         =   "DDirecci�n IP:"
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   120
         TabIndex        =   8
         Top             =   382
         Width           =   1170
      End
      Begin VB.Label lblPwd 
         BackColor       =   &H00808000&
         Caption         =   "DPassword:"
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   4320
         TabIndex        =   7
         Top             =   862
         Width           =   1170
      End
      Begin VB.Label lblUsu 
         BackColor       =   &H00808000&
         Caption         =   "DUsuario:"
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   120
         TabIndex        =   6
         Top             =   862
         Width           =   1170
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   4365
      TabIndex        =   4
      Top             =   1710
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   345
      Left            =   3240
      TabIndex        =   3
      Top             =   1710
      Width           =   1005
   End
End
Attribute VB_Name = "frmINTWebService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public m_bConsulta As Boolean
Public m_sEntidad As String
Public m_sfrmLlamada As String
Public m_tipodestino As tipodestinointegracion

''' <summary>
''' Cargar las variables de pantalla multiidioma
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo:0 </remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_WEB_INTEGRACION, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        caption = Ador(0).Value & " " & m_sEntidad
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        Me.LblDirIP.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblUsu = Ador(0).Value & ":"
        Ador.MoveNext
        lblPwd = Ador(0).Value & ":"
        
        Ador.Close
    End If

    Set Ador = Nothing

End Sub

''' <summary>
''' Responde a la pulsaci�n del bot�n Aceptar. Graba en bbdd los datos de la configuraci�n WebService .
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1 </remarks>
Private Sub cmdAceptar_Click()
    If Me.m_tipodestino = WCF Then
        If Not validarUrl(Me.txtDirUrl) Then
            Exit Sub
        End If
    End If
    
    Dim teserror As TipoErrorSummit
    
    If txtDirUrl.Text <> "" And txtPwd.Text = "" And txtUsu.Text <> "" Then
        Exit Sub
    End If
    
    If txtDirUrl.Text = "" Then
        oMensajes.FaltanDatos LblDirIP
        If Me.Visible Then txtDirUrl.SetFocus
        Exit Sub
    End If
    
    If txtUsu.Text = "" Then
        oMensajes.FaltanDatos lblUsu
        If Me.Visible Then txtUsu.SetFocus
        Exit Sub
    End If
    
    If txtPwd.Text = "" Then
        oMensajes.FaltanDatos lblPwd
        If Me.Visible Then txtPwd.SetFocus
        Exit Sub
    End If
 
    ''antes de guardar la contrase�a encriptamos
    Dim dFecha As Date
    dFecha = Now
    Dim sPasEncriptada As String
    sPasEncriptada = FSGSLibrary.EncriptarAES(txtUsu.Text, txtPwd.Text, True, dFecha, 1)
    
    If (m_tipodestino = WEBSERVICE) Then
        If Not m_bConsulta Then
            If m_sfrmLlamada = "frmINTDestino" Then
                teserror = frmINTDestino.m_oEntidadInt.ModificarWEBDestino(txtDirUrl.Text, txtUsu.Text, txtPwd.Text)
            Else
                teserror = frmINTOrigen.m_oEntidadInt.ModificarWEBOrigen(txtDirUrl.Text, txtUsu.Text, txtPwd.Text)
            End If
        
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            End If
            
            If m_sfrmLlamada = "frmINTDestino" Then
                frmCONFIntegracion.m_oEntidadesInt.CargarTodasLasEntidades (frmINTDestino.m_oEntidadInt.CodERP)
            Else
                frmCONFIntegracion.m_oEntidadesInt.CargarTodasLasEntidades (frmINTOrigen.m_oEntidadInt.CodERP)
            End If
        End If
    Else
        '''WCF
        If Not m_bConsulta Then
            If m_sfrmLlamada = "frmINTDestino" Then
                teserror = frmINTDestino.m_oEntidadInt.ModificarWCFDestino(txtDirUrl.Text, txtUsu.Text, sPasEncriptada, dFecha)
            Else
                teserror = frmINTOrigen.m_oEntidadInt.ModificarWCFOrigen(txtDirUrl.Text, txtUsu.Text, sPasEncriptada, dFecha)
            End If
        
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            End If
            
            If m_sfrmLlamada = "frmINTDestino" Then
                frmCONFIntegracion.m_oEntidadesInt.CargarTodasLasEntidades (frmINTDestino.m_oEntidadInt.CodERP)
                '''cerramos el form frmintdestino
                Unload Me
                frmINTDestino.cmdAceptar = True
            Else
                frmCONFIntegracion.m_oEntidadesInt.CargarTodasLasEntidades (frmINTOrigen.m_oEntidadInt.CodERP)
                Unload Me
                frmINTOrigen.cmdAceptar = True
            End If
        End If
    End If
    
    Unload Me
End Sub

Private Sub cmdCancelar_Click()

    Unload Me
    
End Sub

''' <summary>
''' Carga e inicializa la pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0 </remarks>
Private Sub Form_Load()
 
    CargarRecursos
    
    '''diferenciar carga si estamos con WebService o WCF
    If Me.m_tipodestino = WEBSERVICE Then
        If m_sfrmLlamada = "frmINTDestino" Then
            txtDirUrl = NullToStr(frmINTDestino.m_oEntidadInt.WEB_Dest_Url)
            txtUsu = NullToStr(frmINTDestino.m_oEntidadInt.WEB_Dest_usu)
            txtPwd = NullToStr(frmINTDestino.m_oEntidadInt.WEB_Dest_pwd)
        Else
            txtDirUrl = NullToStr(frmINTOrigen.m_oEntidadInt.WEB_Orig_Url)
            txtUsu = NullToStr(frmINTOrigen.m_oEntidadInt.WEB_Orig_usu)
            txtPwd = NullToStr(frmINTOrigen.m_oEntidadInt.WEB_Orig_pwd)
        End If
        
        If m_bConsulta Then ' En modo consulta todo protegido
            txtDirUrl.Enabled = False
            txtUsu.Enabled = False
            txtPwd.Enabled = False
            cmdAceptar.Visible = False
            cmdCancelar.Visible = False
        Else
            txtDirUrl.Enabled = True
            txtUsu.Enabled = True
            txtPwd.Enabled = True
            cmdAceptar.Visible = True
            cmdCancelar.Visible = True
        End If
    Else
        If m_sfrmLlamada = "frmINTDestino" Then
            txtDirUrl = NullToStr(frmINTDestino.m_oEntidadInt.WCF_Dest_Url)
            txtUsu = NullToStr(frmINTDestino.m_oEntidadInt.WCF_Dest_usu)
            txtPwd = NullToStr(frmINTDestino.m_oEntidadInt.WCF_Dest_pwd)
        Else
            txtDirUrl = NullToStr(frmINTOrigen.m_oEntidadInt.WCF_Orig_Url)
            txtUsu = NullToStr(frmINTOrigen.m_oEntidadInt.WCF_Orig_usu)
            txtPwd = NullToStr(frmINTOrigen.m_oEntidadInt.WCF_Orig_pwd)
        End If
        
        If m_bConsulta Then ' En modo consulta todo protegido
            txtDirUrl.Enabled = False
            txtUsu.Enabled = False
            txtPwd.Enabled = False
            cmdAceptar.Visible = False
            cmdCancelar.Visible = False
        Else
            txtDirUrl.Enabled = True
            txtUsu.Enabled = True
            txtPwd.Enabled = True
            cmdAceptar.Visible = True
            cmdCancelar.Visible = True
        End If
    End If

End Sub

Private Function validarUrl(ByVal sURL As String) As Boolean
    On Error GoTo Fallo
        Dim oURL As New WinHttpRequest
        With oURL
            .Open "GET", sURL, False
            .Send
        End With
        validarUrl = (oURL.Status = 200)
        If Not validarUrl Then
            oMensajes.UrlIncorrecta
        End If
        Exit Function
Fallo:
    oMensajes.UrlIncorrecta
    validarUrl = False
End Function


