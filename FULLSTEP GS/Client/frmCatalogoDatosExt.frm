VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCatalogoDatosExt 
   Caption         =   "DGesti�n de datos externos"
   ClientHeight    =   7260
   ClientLeft      =   600
   ClientTop       =   2610
   ClientWidth     =   11880
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCatalogoDatosExt.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   7260
   ScaleWidth      =   11880
   Begin VB.CommandButton cmdEsp 
      Height          =   345
      Left            =   9930
      Picture         =   "frmCatalogoDatosExt.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   18
      ToolTipText     =   "DCargar especificaciones"
      Top             =   30
      Width           =   645
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   2000
      Left            =   11400
      Top             =   120
   End
   Begin VB.CommandButton cmdImag 
      Height          =   345
      Left            =   8370
      Picture         =   "frmCatalogoDatosExt.frx":0573
      Style           =   1  'Graphical
      TabIndex        =   14
      ToolTipText     =   "Cargar imagenes"
      Top             =   30
      Width           =   645
   End
   Begin VB.CommandButton cmdThumb 
      Height          =   345
      Left            =   9150
      Picture         =   "frmCatalogoDatosExt.frx":065F
      Style           =   1  'Graphical
      TabIndex        =   13
      ToolTipText     =   "Cargar thumbnails"
      Top             =   30
      Width           =   645
   End
   Begin VB.PictureBox picEdicion 
      BorderStyle     =   0  'None
      Height          =   370
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   8535
      TabIndex        =   8
      Top             =   6840
      Width           =   8535
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "Restaurar"
         Height          =   345
         Left            =   5280
         TabIndex        =   17
         Top             =   0
         Width           =   1170
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "Listado"
         Height          =   345
         Left            =   6600
         TabIndex        =   16
         Top             =   0
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "&Modificar"
         Height          =   345
         Left            =   4005
         TabIndex        =   15
         Top             =   0
         Width           =   1170
      End
      Begin VB.CommandButton cmdSolicitar 
         Caption         =   "Daten &anfordern"
         Height          =   345
         Left            =   0
         TabIndex        =   11
         Top             =   0
         Width           =   1350
      End
      Begin VB.CommandButton cmdCargar 
         Caption         =   "cCargarDatos"
         Height          =   345
         Left            =   1455
         TabIndex        =   10
         Top             =   0
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "cBorrarDatos"
         Height          =   345
         Left            =   2730
         TabIndex        =   9
         Top             =   0
         Width           =   1170
      End
   End
   Begin VB.CommandButton cmdSELMAT 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7800
      Picture         =   "frmCatalogoDatosExt.frx":0760
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   480
      Width           =   285
   End
   Begin VB.CommandButton cmdBorrar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7440
      Picture         =   "frmCatalogoDatosExt.frx":07CC
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   480
      Width           =   285
   End
   Begin VB.CheckBox chkSoloArt 
      Caption         =   "DMostrar solo art�culos cat�logo"
      Height          =   255
      Left            =   8370
      TabIndex        =   5
      Top             =   480
      Width           =   3495
   End
   Begin VB.TextBox txtMat 
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   1400
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   480
      Width           =   5940
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
      Height          =   285
      Left            =   3360
      TabIndex        =   2
      Top             =   60
      Width           =   4695
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5450
      Columns(0).Caption=   "Denominacion"
      Columns(0).Name =   "Denominacion"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "Codigo"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8281
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
      Height          =   285
      Left            =   1400
      TabIndex        =   1
      Top             =   60
      Width           =   1855
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2778
      Columns(0).Caption=   "Codigo"
      Columns(0).Name =   "Codigo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5636
      Columns(1).Caption=   "Denominacion"
      Columns(1).Name =   "Denominacion"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3272
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgArt 
      Height          =   5775
      Left            =   0
      TabIndex        =   12
      Top             =   960
      Width           =   11895
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   42
      stylesets.count =   3
      stylesets(0).Name=   "Imagen"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCatalogoDatosExt.frx":0872
      stylesets(1).Name=   "Normal"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCatalogoDatosExt.frx":08F6
      stylesets(1).AlignmentText=   1
      stylesets(1).AlignmentPicture=   1
      stylesets(2).Name=   "Espec"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmCatalogoDatosExt.frx":0912
      UseGroups       =   -1  'True
      MultiLine       =   0   'False
      ActiveCellStyleSet=   "Normal"
      RowSelectionStyle=   1
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      UseExactRowCount=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      CellNavigation  =   1
      MaxSelectedRows =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      Levels          =   2
      RowHeight       =   741
      ExtraHeight     =   265
      ActiveRowStyleSet=   "Normal"
      SplitterVisible =   -1  'True
      Groups.Count    =   3
      Groups(0).Width =   5636
      Groups(0).Caption=   "Definicion"
      Groups(0).Columns.Count=   6
      Groups(0).Columns(0).Width=   3598
      Groups(0).Columns(0).Caption=   "Articulo"
      Groups(0).Columns(0).Name=   "Articulo"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).BackColor=   16777152
      Groups(0).Columns(1).Width=   2037
      Groups(0).Columns(1).Caption=   "Codigo"
      Groups(0).Columns(1).Name=   "Codigo"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).BackColor=   16777152
      Groups(0).Columns(2).Width=   3598
      Groups(0).Columns(2).Caption=   "ESP"
      Groups(0).Columns(2).Name=   "ESP"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).Level=   1
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(2).Locked=   -1  'True
      Groups(0).Columns(2).Style=   4
      Groups(0).Columns(2).ButtonsAlways=   -1  'True
      Groups(0).Columns(3).Width=   2037
      Groups(0).Columns(3).Caption=   "Imagen"
      Groups(0).Columns(3).Name=   "Imagen"
      Groups(0).Columns(3).DataField=   "Column 3"
      Groups(0).Columns(3).DataType=   8
      Groups(0).Columns(3).Level=   1
      Groups(0).Columns(3).FieldLen=   256
      Groups(0).Columns(3).Locked=   -1  'True
      Groups(0).Columns(3).Style=   4
      Groups(0).Columns(3).ButtonsAlways=   -1  'True
      Groups(0).Columns(3).HeadStyleSet=   "Normal"
      Groups(0).Columns(4).Width=   2831
      Groups(0).Columns(4).Visible=   0   'False
      Groups(0).Columns(4).Caption=   "CONIMAGEN"
      Groups(0).Columns(4).Name=   "CONIMAGEN"
      Groups(0).Columns(4).DataField=   "Column 4"
      Groups(0).Columns(4).DataType=   8
      Groups(0).Columns(4).Level=   1
      Groups(0).Columns(4).FieldLen=   256
      Groups(0).Columns(4).Locked=   -1  'True
      Groups(0).Columns(4).Style=   4
      Groups(0).Columns(4).ButtonsAlways=   -1  'True
      Groups(0).Columns(4).HeadStyleSet=   "Normal"
      Groups(0).Columns(5).Width=   2778
      Groups(0).Columns(5).Visible=   0   'False
      Groups(0).Columns(5).Caption=   "CONESP"
      Groups(0).Columns(5).Name=   "CONESP"
      Groups(0).Columns(5).DataField=   "Column 5"
      Groups(0).Columns(5).DataType=   8
      Groups(0).Columns(5).Level=   1
      Groups(0).Columns(5).FieldLen=   256
      Groups(0).Columns(5).Locked=   -1  'True
      Groups(1).Width =   14420
      Groups(1).Caption=   "Articulos"
      Groups(1).AllowSizing=   0   'False
      Groups(1).HeadStyleSet=   "Normal"
      Groups(1).StyleSet=   "Normal"
      Groups(1).Columns.Count=   10
      Groups(1).Columns(0).Width=   2858
      Groups(1).Columns(0).Caption=   "Atributo1"
      Groups(1).Columns(0).Name=   "Atributo1"
      Groups(1).Columns(0).DataField=   "Column 6"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Locked=   -1  'True
      Groups(1).Columns(0).HasBackColor=   -1  'True
      Groups(1).Columns(0).BackColor=   13816530
      Groups(1).Columns(1).Width=   2858
      Groups(1).Columns(1).Caption=   "Atributo2"
      Groups(1).Columns(1).Name=   "Atributo2"
      Groups(1).Columns(1).DataField=   "Column 7"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Locked=   -1  'True
      Groups(1).Columns(1).HasBackColor=   -1  'True
      Groups(1).Columns(1).BackColor=   13816530
      Groups(1).Columns(2).Width=   2858
      Groups(1).Columns(2).Caption=   "Atributo3"
      Groups(1).Columns(2).Name=   "Atributo3"
      Groups(1).Columns(2).DataField=   "Column 8"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(2).Locked=   -1  'True
      Groups(1).Columns(2).HasBackColor=   -1  'True
      Groups(1).Columns(2).BackColor=   13816530
      Groups(1).Columns(3).Width=   2858
      Groups(1).Columns(3).Caption=   "Atributo4"
      Groups(1).Columns(3).Name=   "Atributo4"
      Groups(1).Columns(3).DataField=   "Column 9"
      Groups(1).Columns(3).DataType=   8
      Groups(1).Columns(3).FieldLen=   256
      Groups(1).Columns(3).Locked=   -1  'True
      Groups(1).Columns(3).HasBackColor=   -1  'True
      Groups(1).Columns(3).BackColor=   13816530
      Groups(1).Columns(4).Width=   2858
      Groups(1).Columns(4).Caption=   "Atributo5"
      Groups(1).Columns(4).Name=   "Atributo5"
      Groups(1).Columns(4).DataField=   "Column 10"
      Groups(1).Columns(4).DataType=   8
      Groups(1).Columns(4).FieldLen=   256
      Groups(1).Columns(4).Locked=   -1  'True
      Groups(1).Columns(4).HasBackColor=   -1  'True
      Groups(1).Columns(4).BackColor=   13816530
      Groups(1).Columns(5).Width=   2858
      Groups(1).Columns(5).Caption=   "Valor1"
      Groups(1).Columns(5).Name=   "Valor1"
      Groups(1).Columns(5).DataField=   "Column 11"
      Groups(1).Columns(5).DataType=   8
      Groups(1).Columns(5).Level=   1
      Groups(1).Columns(5).Locked=   -1  'True
      Groups(1).Columns(5).HasBackColor=   -1  'True
      Groups(1).Columns(5).BackColor=   16777215
      Groups(1).Columns(6).Width=   2858
      Groups(1).Columns(6).Caption=   "Valor2"
      Groups(1).Columns(6).Name=   "Valor2"
      Groups(1).Columns(6).DataField=   "Column 12"
      Groups(1).Columns(6).DataType=   8
      Groups(1).Columns(6).Level=   1
      Groups(1).Columns(6).Locked=   -1  'True
      Groups(1).Columns(6).HasBackColor=   -1  'True
      Groups(1).Columns(6).BackColor=   16777215
      Groups(1).Columns(7).Width=   2858
      Groups(1).Columns(7).Caption=   "Valor3"
      Groups(1).Columns(7).Name=   "Valor3"
      Groups(1).Columns(7).DataField=   "Column 13"
      Groups(1).Columns(7).DataType=   8
      Groups(1).Columns(7).Level=   1
      Groups(1).Columns(7).Locked=   -1  'True
      Groups(1).Columns(7).HasBackColor=   -1  'True
      Groups(1).Columns(7).BackColor=   16777215
      Groups(1).Columns(8).Width=   2858
      Groups(1).Columns(8).Caption=   "Valor4"
      Groups(1).Columns(8).Name=   "Valor4"
      Groups(1).Columns(8).DataField=   "Column 14"
      Groups(1).Columns(8).DataType=   8
      Groups(1).Columns(8).Level=   1
      Groups(1).Columns(8).Locked=   -1  'True
      Groups(1).Columns(8).HasBackColor=   -1  'True
      Groups(1).Columns(8).BackColor=   16777215
      Groups(1).Columns(9).Width=   2858
      Groups(1).Columns(9).Caption=   "Valor5"
      Groups(1).Columns(9).Name=   "Valor5"
      Groups(1).Columns(9).DataField=   "Column 15"
      Groups(1).Columns(9).DataType=   8
      Groups(1).Columns(9).Level=   1
      Groups(1).Columns(9).Locked=   -1  'True
      Groups(1).Columns(9).HasBackColor=   -1  'True
      Groups(1).Columns(9).BackColor=   16777215
      Groups(2).Width =   14420
      Groups(2).Caption=   "Articulos1"
      Groups(2).AllowSizing=   0   'False
      Groups(2).Columns.Count=   26
      Groups(2).Columns(0).Width=   2858
      Groups(2).Columns(0).Caption=   "Atributo6"
      Groups(2).Columns(0).Name=   "Atributo6"
      Groups(2).Columns(0).DataField=   "Column 16"
      Groups(2).Columns(0).DataType=   8
      Groups(2).Columns(0).FieldLen=   256
      Groups(2).Columns(0).Locked=   -1  'True
      Groups(2).Columns(0).HasBackColor=   -1  'True
      Groups(2).Columns(0).BackColor=   13816530
      Groups(2).Columns(1).Width=   2858
      Groups(2).Columns(1).Caption=   "Atributo7"
      Groups(2).Columns(1).Name=   "Atributo7"
      Groups(2).Columns(1).DataField=   "Column 17"
      Groups(2).Columns(1).DataType=   8
      Groups(2).Columns(1).FieldLen=   256
      Groups(2).Columns(1).Locked=   -1  'True
      Groups(2).Columns(1).HasBackColor=   -1  'True
      Groups(2).Columns(1).BackColor=   13816530
      Groups(2).Columns(2).Width=   2858
      Groups(2).Columns(2).Caption=   "Atributo8"
      Groups(2).Columns(2).Name=   "Atributo8"
      Groups(2).Columns(2).DataField=   "Column 18"
      Groups(2).Columns(2).DataType=   8
      Groups(2).Columns(2).FieldLen=   256
      Groups(2).Columns(2).Locked=   -1  'True
      Groups(2).Columns(2).HasBackColor=   -1  'True
      Groups(2).Columns(2).BackColor=   13816530
      Groups(2).Columns(3).Width=   2858
      Groups(2).Columns(3).Caption=   "Atributo9"
      Groups(2).Columns(3).Name=   "Atributo9"
      Groups(2).Columns(3).DataField=   "Column 19"
      Groups(2).Columns(3).DataType=   8
      Groups(2).Columns(3).FieldLen=   256
      Groups(2).Columns(3).Locked=   -1  'True
      Groups(2).Columns(3).HasBackColor=   -1  'True
      Groups(2).Columns(3).BackColor=   13816530
      Groups(2).Columns(4).Width=   2858
      Groups(2).Columns(4).Caption=   "Atributo10"
      Groups(2).Columns(4).Name=   "Atributo10"
      Groups(2).Columns(4).DataField=   "Column 20"
      Groups(2).Columns(4).DataType=   8
      Groups(2).Columns(4).FieldLen=   256
      Groups(2).Columns(4).Locked=   -1  'True
      Groups(2).Columns(4).HasBackColor=   -1  'True
      Groups(2).Columns(4).BackColor=   13816530
      Groups(2).Columns(5).Width=   2858
      Groups(2).Columns(5).Caption=   "Valor6"
      Groups(2).Columns(5).Name=   "Valor6"
      Groups(2).Columns(5).DataField=   "Column 21"
      Groups(2).Columns(5).DataType=   8
      Groups(2).Columns(5).Level=   1
      Groups(2).Columns(5).Locked=   -1  'True
      Groups(2).Columns(6).Width=   2858
      Groups(2).Columns(6).Caption=   "Valor7"
      Groups(2).Columns(6).Name=   "Valor7"
      Groups(2).Columns(6).AllowSizing=   0   'False
      Groups(2).Columns(6).DataField=   "Column 22"
      Groups(2).Columns(6).DataType=   8
      Groups(2).Columns(6).Level=   1
      Groups(2).Columns(6).Locked=   -1  'True
      Groups(2).Columns(7).Width=   2858
      Groups(2).Columns(7).Caption=   "Valor8"
      Groups(2).Columns(7).Name=   "Valor8"
      Groups(2).Columns(7).DataField=   "Column 23"
      Groups(2).Columns(7).DataType=   8
      Groups(2).Columns(7).Level=   1
      Groups(2).Columns(7).Locked=   -1  'True
      Groups(2).Columns(8).Width=   2858
      Groups(2).Columns(8).Caption=   "Valor9"
      Groups(2).Columns(8).Name=   "Valor9"
      Groups(2).Columns(8).DataField=   "Column 24"
      Groups(2).Columns(8).DataType=   8
      Groups(2).Columns(8).Level=   1
      Groups(2).Columns(8).Locked=   -1  'True
      Groups(2).Columns(9).Width=   2858
      Groups(2).Columns(9).Caption=   "Valor10"
      Groups(2).Columns(9).Name=   "Valor10"
      Groups(2).Columns(9).DataField=   "Column 25"
      Groups(2).Columns(9).DataType=   8
      Groups(2).Columns(9).Level=   1
      Groups(2).Columns(9).Locked=   -1  'True
      Groups(2).Columns(10).Width=   2514
      Groups(2).Columns(10).Visible=   0   'False
      Groups(2).Columns(10).Caption=   "GMN1"
      Groups(2).Columns(10).Name=   "GMN1"
      Groups(2).Columns(10).DataField=   "Column 26"
      Groups(2).Columns(10).DataType=   8
      Groups(2).Columns(10).Level=   1
      Groups(2).Columns(10).FieldLen=   256
      Groups(2).Columns(11).Width=   2196
      Groups(2).Columns(11).Visible=   0   'False
      Groups(2).Columns(11).Caption=   "GMN2"
      Groups(2).Columns(11).Name=   "GMN2"
      Groups(2).Columns(11).DataField=   "Column 27"
      Groups(2).Columns(11).DataType=   8
      Groups(2).Columns(11).Level=   1
      Groups(2).Columns(11).FieldLen=   256
      Groups(2).Columns(12).Width=   2514
      Groups(2).Columns(12).Visible=   0   'False
      Groups(2).Columns(12).Caption=   "GMN3"
      Groups(2).Columns(12).Name=   "GMN3"
      Groups(2).Columns(12).DataField=   "Column 28"
      Groups(2).Columns(12).DataType=   8
      Groups(2).Columns(12).Level=   1
      Groups(2).Columns(12).FieldLen=   256
      Groups(2).Columns(13).Width=   2196
      Groups(2).Columns(13).Visible=   0   'False
      Groups(2).Columns(13).Caption=   "GMN4"
      Groups(2).Columns(13).Name=   "GMN4"
      Groups(2).Columns(13).DataField=   "Column 29"
      Groups(2).Columns(13).DataType=   8
      Groups(2).Columns(13).Level=   1
      Groups(2).Columns(13).FieldLen=   256
      Groups(2).Columns(14).Width=   1826
      Groups(2).Columns(14).Visible=   0   'False
      Groups(2).Columns(14).Caption=   "ID1"
      Groups(2).Columns(14).Name=   "ID1"
      Groups(2).Columns(14).DataField=   "Column 30"
      Groups(2).Columns(14).DataType=   8
      Groups(2).Columns(14).Level=   1
      Groups(2).Columns(14).FieldLen=   256
      Groups(2).Columns(15).Width=   1720
      Groups(2).Columns(15).Visible=   0   'False
      Groups(2).Columns(15).Caption=   "ID2"
      Groups(2).Columns(15).Name=   "ID2"
      Groups(2).Columns(15).DataField=   "Column 31"
      Groups(2).Columns(15).DataType=   8
      Groups(2).Columns(15).Level=   1
      Groups(2).Columns(15).FieldLen=   256
      Groups(2).Columns(16).Width=   1561
      Groups(2).Columns(16).Visible=   0   'False
      Groups(2).Columns(16).Caption=   "ID3"
      Groups(2).Columns(16).Name=   "ID3"
      Groups(2).Columns(16).DataField=   "Column 32"
      Groups(2).Columns(16).DataType=   8
      Groups(2).Columns(16).Level=   1
      Groups(2).Columns(16).FieldLen=   256
      Groups(2).Columns(17).Width=   1455
      Groups(2).Columns(17).Visible=   0   'False
      Groups(2).Columns(17).Caption=   "ID4"
      Groups(2).Columns(17).Name=   "ID4"
      Groups(2).Columns(17).DataField=   "Column 33"
      Groups(2).Columns(17).DataType=   8
      Groups(2).Columns(17).Level=   1
      Groups(2).Columns(17).FieldLen=   256
      Groups(2).Columns(18).Width=   1323
      Groups(2).Columns(18).Visible=   0   'False
      Groups(2).Columns(18).Caption=   "ID5"
      Groups(2).Columns(18).Name=   "ID5"
      Groups(2).Columns(18).DataField=   "Column 34"
      Groups(2).Columns(18).DataType=   8
      Groups(2).Columns(18).Level=   1
      Groups(2).Columns(18).FieldLen=   256
      Groups(2).Columns(19).Width=   1402
      Groups(2).Columns(19).Visible=   0   'False
      Groups(2).Columns(19).Caption=   "ID6"
      Groups(2).Columns(19).Name=   "ID6"
      Groups(2).Columns(19).DataField=   "Column 35"
      Groups(2).Columns(19).DataType=   8
      Groups(2).Columns(19).Level=   1
      Groups(2).Columns(19).FieldLen=   256
      Groups(2).Columns(20).Width=   1270
      Groups(2).Columns(20).Visible=   0   'False
      Groups(2).Columns(20).Caption=   "ID7"
      Groups(2).Columns(20).Name=   "ID7"
      Groups(2).Columns(20).DataField=   "Column 36"
      Groups(2).Columns(20).DataType=   8
      Groups(2).Columns(20).Level=   1
      Groups(2).Columns(20).FieldLen=   256
      Groups(2).Columns(21).Width=   1535
      Groups(2).Columns(21).Visible=   0   'False
      Groups(2).Columns(21).Caption=   "ID8"
      Groups(2).Columns(21).Name=   "ID8"
      Groups(2).Columns(21).DataField=   "Column 37"
      Groups(2).Columns(21).DataType=   8
      Groups(2).Columns(21).Level=   1
      Groups(2).Columns(21).FieldLen=   256
      Groups(2).Columns(22).Width=   2090
      Groups(2).Columns(22).Visible=   0   'False
      Groups(2).Columns(22).Caption=   "ID9"
      Groups(2).Columns(22).Name=   "ID9"
      Groups(2).Columns(22).DataField=   "Column 38"
      Groups(2).Columns(22).DataType=   8
      Groups(2).Columns(22).Level=   1
      Groups(2).Columns(22).FieldLen=   256
      Groups(2).Columns(23).Width=   3307
      Groups(2).Columns(23).Visible=   0   'False
      Groups(2).Columns(23).Caption=   "ID10"
      Groups(2).Columns(23).Name=   "ID10"
      Groups(2).Columns(23).DataField=   "Column 39"
      Groups(2).Columns(23).DataType=   8
      Groups(2).Columns(23).Level=   1
      Groups(2).Columns(23).FieldLen=   256
      Groups(2).Columns(24).Width=   4974
      Groups(2).Columns(24).Visible=   0   'False
      Groups(2).Columns(24).Caption=   "COD"
      Groups(2).Columns(24).Name=   "COD"
      Groups(2).Columns(24).DataField=   "Column 40"
      Groups(2).Columns(24).DataType=   8
      Groups(2).Columns(24).Level=   1
      Groups(2).Columns(24).FieldLen=   256
      Groups(2).Columns(25).Width=   4419
      Groups(2).Columns(25).Visible=   0   'False
      Groups(2).Columns(25).Caption=   "DEN"
      Groups(2).Columns(25).Name=   "DEN"
      Groups(2).Columns(25).DataField=   "Column 41"
      Groups(2).Columns(25).DataType=   8
      Groups(2).Columns(25).Level=   1
      Groups(2).Columns(25).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   20981
      _ExtentY        =   10186
      _StockProps     =   79
      Caption         =   "DArt�culos del material asignado al proveedor"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblMaterial 
      Caption         =   "DMaterial"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   500
      Width           =   1095
   End
   Begin VB.Label lblProv 
      Caption         =   "DProveedor"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   100
      Width           =   1095
   End
End
Attribute VB_Name = "frmCatalogoDatosExt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Valor m�ximo de NVarcha(MAX)
Private Const cLongAtribEsp_Texto As Long = 32471

Private bMod As Boolean
Private bMRMat As Boolean
Private m_bProvMat As Boolean
Private RespetarComboProve As Boolean
Public m_oArticulos As CArticulos
'variables para la coleccion de proveedores
Private oProves As CProveedores
Private oProveSeleccionado As CProveedor

'idiomas
Private sArticulo As String
Private sIdiSi As String
Private sIdiNo As String
Private m_sIdiAtributos As String
Private txtAtributo As String
Private txtDen As String
Private txtValor As String
Private txtTitulo As String
Private txtDe As String
Private txtPag As String
Private sEspera(1 To 3) As String
Private m_sIdiEspec As String
Private m_sIdiProv As String

'Estructura de materiales
Public sGMN1Cod As String
Public sGMN2Cod As String
Public sGMN3Cod As String
Public sGMN4Cod As String

Public bNoHayImagen As Boolean

Public g_ador As Ador.Recordset

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Private Sub ConfigurarSeguridad()
    m_bProvMat = False
        
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtGestion)) Is Nothing) Then
        bMod = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtRestMAtComp)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
        bMRMat = True
    End If
    
    If gParametrosGenerales.gbPymes And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtRestProvMatComp)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
        m_bProvMat = True
    End If
    
    If Not bMod Then
        cmdImag.Visible = False
        cmdThumb.Visible = False
        cmdEsp.Visible = False
        cmdSolicitar.Visible = False
        cmdCargar.Visible = False
        cmdEliminar.Visible = False
        cmdModificar.Visible = False
        cmdRestaurar.Left = cmdSolicitar.Left
        cmdListado.Left = cmdCargar.Left
    End If
End Sub

Private Sub chkSoloArt_Click()
    If Me.txtMat <> "" Then
        MostrarArticulos
    End If
End Sub

Private Sub cmdBorrar_Click()
    txtMat.Text = ""
    sdbgArt.RemoveAll
    
    'Inhabilita los botones
    HabilitarBotones False

End Sub

Private Sub cmdCargar_Click()
    
    frmCATDatExtCargar.Show vbModal
End Sub

Private Sub cmdEliminar_Click()
    Dim i As Integer
    Dim oArticulos As CArticulos
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    
    'Debe haber seleccionado al menos 1 art�culo
    If sdbgArt.SelBookmarks.Count = 0 Then
        oMensajes.SeleccioneArticulo
        Exit Sub
    End If
    
    irespuesta = oMensajes.PreguntaEliminarDatosExt
    If irespuesta = vbNo Then Exit Sub
                
    teserror.NumError = TESnoerror
    
    If oProveSeleccionado Is Nothing Then Exit Sub
    
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
    
    'Rellena la colecci�n de art�culos
    For i = 0 To sdbgArt.SelBookmarks.Count - 1
        oArticulos.Add sdbgArt.Columns("GMN1").CellValue(sdbgArt.SelBookmarks.Item(i)), sdbgArt.Columns("GMN2").CellValue(sdbgArt.SelBookmarks.Item(i)), sdbgArt.Columns("GMN3").CellValue(sdbgArt.SelBookmarks.Item(i)), sdbgArt.Columns("GMN4").CellValue(sdbgArt.SelBookmarks.Item(i)), sdbgArt.Columns("COD").CellValue(sdbgArt.SelBookmarks.Item(i)), sdbgArt.Columns("DEN").CellValue(sdbgArt.SelBookmarks.Item(i)), , , , , , , sdbgArt.Columns("Codigo").CellValue(sdbgArt.SelBookmarks.Item(i))
    Next i
    
    Screen.MousePointer = vbHourglass
    
    teserror = oProveSeleccionado.BorrarDatosExternosArticulos(oArticulos)

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    basSeguridad.RegistrarAccion accionessummit.ACCArticDatExtEliminar, "Proveedor:" & sdbcProveCod.Text
     
    Set oArticulos = Nothing
    
    'Elimina de la grid los elementos seleccionados que se han borrado de BD
    'sdbgArt.DeleteSelected
    MostrarArticulos
    
    DoEvents
    sdbgArt.SelBookmarks.RemoveAll
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdEsp_Click()
    'Carga las especificaciones desde una carpeta
    Set frmCATDatExtEsp.g_oProve = oProveSeleccionado
    frmCATDatExtEsp.Show 1
End Sub

Private Sub cmdImag_Click()
    'Carga las �magenes desde una carpeta
    Set frmCATDatExtImagenes1.g_oProve = oProveSeleccionado
    frmCATDatExtImagenes1.g_bImagenes = True
    frmCATDatExtImagenes1.Show 1
End Sub

Private Sub cmdlistado_Click()
    Dim RepPath As String
    Dim oReport As CRAXDRT.Report
    Dim oFos As FileSystemObject
    Dim pv As Preview
    
    If oProveSeleccionado Is Nothing Then Exit Sub
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAtributosProve.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    Screen.MousePointer = vbHourglass
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    ' FORMULA FIELDS A NIVEL DE REPORT
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & txtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProveedor")).Text = """" & lblProv.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMaterial")).Text = """" & lblMaterial.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtArticulo")).Text = """" & sdbgArt.Columns("Articulo").caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & txtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDen")).Text = """" & txtDen & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodExterno")).Text = """" & sdbgArt.Columns("Codigo").caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAtrib")).Text = """" & txtAtributo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtValor")).Text = """" & txtValor & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & txtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "PROVE")).Text = """" & oProveSeleccionado.Cod & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "DENPROVE")).Text = """" & oProveSeleccionado.Den & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MATERIAL")).Text = """" & txtMat.Text & """"
    RellenarAdoParaListado

    If g_ador Is Nothing Then
        Screen.MousePointer = vbNormal
        oMensajes.NoHayDatos
        Set oReport = Nothing
        Exit Sub
    Else
        oReport.Database.SetDataSource g_ador
    End If
        
    DoEvents
    frmESPERA.Show
    frmESPERA.lblGeneral.caption = sEspera(1)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    
    Set pv = New Preview
    pv.Hide
    pv.g_sOrigen = "frmDatosExt"
    pv.caption = txtTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    Timer1.Enabled = True
    
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Set oReport = Nothing
    DoEvents
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdModificar_Click()
Dim sCod As String

sCod = sdbgArt.Columns("GMN1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbgArt.Columns("GMN1").Value))
sCod = sCod & sdbgArt.Columns("GMN2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(sdbgArt.Columns("GMN2").Value))
sCod = sCod & sdbgArt.Columns("GMN3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(sdbgArt.Columns("GMN3").Value))
sCod = sCod & sdbgArt.Columns("GMN4").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(sdbgArt.Columns("GMN4").Value))
sCod = sCod & sdbgArt.Columns("COD").Value

If Not m_oArticulos.Item(sCod).ATRIBUTOS Is Nothing Then
    If m_oArticulos.Item(sCod).ATRIBUTOS.Count > 0 Then
        Set frmCATDatExtModificar.g_oArticulo = m_oArticulos.Item(sCod)
        Set frmCATDatExtModificar.g_oProveedor = oProveSeleccionado
        frmCATDatExtModificar.g_bSoloCodExt = False
        frmCATDatExtModificar.bRespetarCombo = True
        frmCATDatExtModificar.txtCodExt.Text = sdbgArt.Columns("Codigo").Value
        frmCATDatExtModificar.bRespetarCombo = False
        frmCATDatExtModificar.caption = sdbgArt.Columns(0).Value & "; " & oProveSeleccionado.Cod & " - " & oProveSeleccionado.Den
        frmCATDatExtModificar.Show 1
        sdbgArt.Refresh
    Else
        ModificarSoloCodExterno (sCod)
    End If
Else
    ModificarSoloCodExterno (sCod)
End If

End Sub
Private Sub ModificarSoloCodExterno(ByVal sCod As String)

        Set frmCATDatExtModificar.g_oArticulo = m_oArticulos.Item(sCod)
        Set frmCATDatExtModificar.g_oProveedor = oProveSeleccionado
        frmCATDatExtModificar.g_bSoloCodExt = True
        frmCATDatExtModificar.bRespetarCombo = True
        frmCATDatExtModificar.txtCodExt.Text = sdbgArt.Columns("Codigo").Value
        frmCATDatExtModificar.bRespetarCombo = False
        frmCATDatExtModificar.caption = sdbgArt.Columns(0).Value & "; " & oProveSeleccionado.Cod & " - " & oProveSeleccionado.Den
        frmCATDatExtModificar.Show 1
        sdbgArt.Refresh

End Sub
Public Sub cmdRestaurar_Click()
    MostrarArticulos
End Sub

Private Sub cmdSelMat_Click()
    Dim oProveedores As CProveedores
    
    Set oProveedores = oFSGSRaiz.generar_CProveedores
    oProveedores.Add sdbcProveCod.Text, sdbcProveDen.Text
    oProveedores.CargarDatosProveedor sdbcProveCod, False
    
    Set frmSELMATProve.oProveedor = oProveedores.Item(sdbcProveCod.Text)
    frmSELMATProve.sOrigen = "frmCatalogoDatosExt"
    frmSELMATProve.bRMat = bMRMat
    
    Set oProveedores = Nothing
    
    frmSELMATProve.Show 1
    
End Sub


Private Sub cmdSolicitar_Click()
    Dim i As Integer
    Dim oArticulos As CArticulos
    Dim oAtribs As CAtributos
    
    'Debe haber seleccionado al menos 1 art�culo
    If sdbgArt.SelBookmarks.Count = 0 Then
        oMensajes.SeleccioneArticulo
        Exit Sub
    End If
    
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
    
    'Rellena la colecci�n de art�culos
    For i = 0 To sdbgArt.SelBookmarks.Count - 1
        oArticulos.Add sdbgArt.Columns("GMN1").CellValue(sdbgArt.SelBookmarks.Item(i)), sdbgArt.Columns("GMN2").CellValue(sdbgArt.SelBookmarks.Item(i)), sdbgArt.Columns("GMN3").CellValue(sdbgArt.SelBookmarks.Item(i)), sdbgArt.Columns("GMN4").CellValue(sdbgArt.SelBookmarks.Item(i)), sdbgArt.Columns("COD").CellValue(sdbgArt.SelBookmarks.Item(i)), sdbgArt.Columns("DEN").CellValue(sdbgArt.SelBookmarks.Item(i)), , , , , , , StrToNull(sdbgArt.Columns("Codigo").CellValue(sdbgArt.SelBookmarks.Item(i)))
        
        'Atributos
        Set oAtribs = oArticulos.Item(i + 1).DevolverTodosLosAtributosDelArticulo(oProveSeleccionado.Cod)
        Set oArticulos.Item(i + 1).ATRIBUTOS = oAtribs
     
    Next i
    
    frmCATDatExtSolicitar.sProve = oProveSeleccionado.Cod
    frmCATDatExtSolicitar.sProveDen = oProveSeleccionado.Den
    frmCATDatExtSolicitar.sGMN1 = sGMN1Cod
    frmCATDatExtSolicitar.sGMN2 = sGMN2Cod
    frmCATDatExtSolicitar.sGMN3 = sGMN3Cod
    frmCATDatExtSolicitar.sGMN4 = sGMN4Cod
    
    Set frmCATDatExtSolicitar.oArticulos = oArticulos
    Set oArticulos = Nothing
    
    frmCATDatExtSolicitar.Show vbModal
End Sub

Private Sub cmdThumb_Click()
    'Carga las thumbnails desde una carpeta
    Set frmCATDatExtImagenes1.g_oProve = oProveSeleccionado
    frmCATDatExtImagenes1.g_bImagenes = False
    frmCATDatExtImagenes1.Show 1

End Sub

Private Sub Form_Load()

    Me.Width = 12100
    Me.Height = 7650
    
    
    CargarRecursos

    ConfigurarSeguridad
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
         Me.Top = 0
         Me.Left = 0
    End If
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    cmdListado.Enabled = False
    HabilitarBotones False
    
    PonerFieldSeparator Me
End Sub

Public Sub PonerMatSeleccionado()
Dim sDen As String
    
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    
    'Pone en el textbox la estructura de material seleccionada en FRMSELMATPROVE
    sGMN1Cod = frmSELMATProve.sGMN1Cod
    If sGMN1Cod <> "" Then
        txtMat = sGMN1Cod
        sDen = frmSELMATProve.sGMN1Den
    End If

    sGMN2Cod = frmSELMATProve.sGMN2Cod
    If sGMN2Cod <> "" Then
        txtMat = txtMat & " - " & sGMN2Cod
        sDen = frmSELMATProve.sGMN2Den
    End If

    sGMN3Cod = frmSELMATProve.sGMN3Cod
    If sGMN3Cod <> "" Then
        txtMat = txtMat & " - " & sGMN3Cod
        sDen = frmSELMATProve.sGMN3Den
    End If

    sGMN4Cod = frmSELMATProve.sGMN4Cod
    If sGMN4Cod <> "" Then
        txtMat = txtMat & " - " & sGMN4Cod
        sDen = frmSELMATProve.sGMN4Den
    End If
    txtMat = txtMat & "   " & sDen
    'Muestra los art�culos
    MostrarArticulos
End Sub

Private Sub Form_Resize()
    Dim i As Integer
    
    If Me.Width < 4000 Then Exit Sub
    
    If Me.Height < 4000 Then Exit Sub
    
    'Tama�o de la grid
    sdbgArt.Width = Me.Width - 205
    sdbgArt.Height = Me.Height - 1915
    
    'Tama�o de los grupos:
    sdbgArt.Groups("Definicion").Width = sdbgArt.Width / 3 - 769.787
    sdbgArt.Groups("Articulos").Width = sdbgArt.Width - 3719.881
    sdbgArt.Groups("Articulos1").Width = sdbgArt.Width - 3719.881
    
    'Columnas de la grid:
    sdbgArt.Columns("Articulo").Width = (sdbgArt.Groups("Definicion").Width / 3) * 2
    sdbgArt.Columns("Codigo").Width = sdbgArt.Groups("Definicion").Width / 3
    sdbgArt.Columns("ESP").Width = (sdbgArt.Groups("Definicion").Width / 3) * 2
    sdbgArt.Columns("IMAGEN").Width = sdbgArt.Groups("Definicion").Width / 3
    
    'atributos:
    For i = 1 To 5
        sdbgArt.Columns("Atributo" & i).Width = sdbgArt.Groups("Articulos").Width / 5
    Next i
    
    For i = 6 To 10
        sdbgArt.Columns("Atributo" & i).Width = sdbgArt.Groups("Articulos1").Width / 5 ''''.4
    Next i
    
    'Valores:
    For i = 1 To 10
        sdbgArt.Columns("Valor" & i).Width = sdbgArt.Columns("Atributo" & i).Width
    Next i
    
    'Los botones
    picEdicion.Top = sdbgArt.Top + sdbgArt.Height + 105
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oProves = Nothing
    Set oProveSeleccionado = Nothing
    Me.Visible = False
End Sub

Private Sub sdbcProveCod_Change()
    If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveDen.Text = ""
        sdbgArt.RemoveAll
        txtMat = ""
        cmdSELMAT.Enabled = False
        cmdListado.Enabled = False
        HabilitarBotones False
        
        RespetarComboProve = False
        
        Set oProveSeleccionado = Nothing
    End If
  
End Sub

Private Sub HabilitarBotones(ByVal bB As Boolean)

    If bB Then
        cmdImag.Enabled = True
        cmdThumb.Enabled = True
        cmdEsp.Enabled = True
        cmdSolicitar.Enabled = True
        cmdEliminar.Enabled = True
        cmdModificar.Enabled = True
        cmdRestaurar.Enabled = True
    Else
        cmdImag.Enabled = False
        cmdThumb.Enabled = False
        cmdEsp.Enabled = False
        cmdSolicitar.Enabled = False
        cmdEliminar.Enabled = False
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False

    End If
End Sub


Private Sub sdbcProveCod_CloseUp()
    If sdbcProveCod.Value = "..." Or sdbcProveCod.Text = "" Then
        sdbcProveCod.Text = ""
        sdbgArt.RemoveAll
        txtMat = ""
        cmdSELMAT.Enabled = False
        cmdListado.Enabled = False
        HabilitarBotones False
        Exit Sub
    End If
    
    If sdbcProveCod.Text <> sdbcProveDen.Columns(1).Value Then
        txtMat = ""
        sdbgArt.RemoveAll
        cmdSELMAT.Enabled = False
        cmdListado.Enabled = False
        HabilitarBotones False
    End If
    
    RespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    cmdListado.Enabled = True
    cmdSELMAT.Enabled = True
    cmdListado.Enabled = True
End Sub

Private Sub sdbcProveCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcProveCod.RemoveAll
    
    If sdbcProveCod.Text <> "" Then
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, sdbcProveCod.Text, , False, False, False, m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , , False, False, False, m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    End If

    Codigos = oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next
    
    If Not oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If
    
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveCod.Rows - 1
            bm = sdbcProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    If sdbcProveCod.Text = sdbcProveCod.Columns(0).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
        cmdSELMAT.Enabled = True
        cmdListado.Enabled = True
        RespetarComboProve = False
        If oProveSeleccionado Is Nothing Then
            Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
        End If
        Exit Sub
    End If

    If sdbcProveCod.Text = sdbcProveDen.Columns(1).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
        cmdSELMAT.Enabled = True
        cmdListado.Enabled = True
        RespetarComboProve = False
        If oProveSeleccionado Is Nothing Then
            Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
        End If
        Exit Sub
    End If
    
    
    ''' Solo continuamos si existe el proveedor
    Screen.MousePointer = vbHourglass
    oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, sdbcProveCod.Text, , True, False, False, m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
        cmdSELMAT.Enabled = False
        cmdListado.Enabled = False
        HabilitarBotones False
        txtMat = ""
        sdbgArt.RemoveAll
        
    Else
        RespetarComboProve = True
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Text = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Text = sdbcProveDen.Text
        cmdSELMAT.Enabled = True
        cmdListado.Enabled = True
        
        txtMat = ""
        sdbgArt.RemoveAll
        RespetarComboProve = False
        
        Screen.MousePointer = vbHourglass
        Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
        Screen.MousePointer = vbNormal
    End If

End Sub



Private Sub sdbcProveDen_Change()
    If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveCod.Text = ""
        cmdSELMAT.Enabled = False
        cmdListado.Enabled = False
        HabilitarBotones False
        txtMat = ""
        sdbgArt.RemoveAll
        RespetarComboProve = False
        
        Set oProveSeleccionado = Nothing
    End If
    
End Sub


Private Sub sdbcProveDen_CloseUp()
    If sdbcProveDen.Value = "..." Or sdbcProveDen.Text = "" Then
        sdbcProveDen.Text = ""
        cmdSELMAT.Enabled = False
        cmdListado.Enabled = False
        HabilitarBotones False
        sdbgArt.RemoveAll
        Exit Sub
    End If
    
    If sdbcProveCod.Text <> sdbcProveDen.Columns(0).Text Then
        txtMat = ""
        sdbgArt.RemoveAll
    End If
    
    RespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    
    cmdSELMAT.Enabled = True
    cmdListado.Enabled = True

End Sub

Private Sub sdbcProveDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcProveDen.RemoveAll
    
    If sdbcProveDen.Text <> "" Then
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveDen.Text, False, True, False, m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , , False, True, False, m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    End If
    
    
    Codigos = oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next
    
    If Not oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveDen.Rows - 1
            bm = sdbcProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub MostrarArticulos()
    Dim oArt As CArticulo
    Dim i As Integer
    Dim Den(10) As String
    Dim val(10) As String
    Dim Id(10) As String
    Dim sLinea As String
    
    Screen.MousePointer = vbHourglass

    Set m_oArticulos = oFSGSRaiz.Generar_CArticulos

    m_oArticulos.CargarArticulosConAtributos sdbcProveCod.Text, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, chkSoloArt.Value
    
    i = 0

    sdbgArt.RemoveAll
    
    For Each oArt In m_oArticulos
        'Limpia los arrays
        LimpiarArrayTexto Den
        LimpiarArrayTexto val
        LimpiarArrayTexto Id
        
        'Carga los arrays
        For i = 0 To oArt.ATRIBUTOS.Count - 1
             If i > 10 Then Exit For
            Den(i) = oArt.ATRIBUTOS.Item(i + 1).Den
            Id(i) = oArt.ATRIBUTOS.Item(i + 1).Id
            If IsNull(oArt.ATRIBUTOS.Item(i + 1).valor) Then
                val(i) = ""
            Else
                Select Case oArt.ATRIBUTOS.Item(i + 1).Tipo
                Case TiposDeAtributos.TipoBoolean
                    If oArt.ATRIBUTOS.Item(i + 1).valor = "0" Then
                        val(i) = sIdiNo
                    ElseIf oArt.ATRIBUTOS.Item(i + 1).valor = "1" Then
                        val(i) = sIdiSi
                    End If
                Case TiposDeAtributos.TipoNumerico
                    val(i) = oArt.ATRIBUTOS.Item(i + 1).valor
                Case TiposDeAtributos.TipoFecha
                    val(i) = oArt.ATRIBUTOS.Item(i + 1).valor
                Case Else
                    val(i) = oArt.ATRIBUTOS.Item(i + 1).valor
                End Select
            End If
        Next
        
        sLinea = oArt.Cod & " - " & oArt.Den & Chr(m_lSeparador) & oArt.CodigoExterno & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & BooleanToSQLBinary(oArt.ConImagen) & Chr(m_lSeparador) & BooleanToSQLBinary(oArt.ConEspecific) & Chr(m_lSeparador) & Den(0) & Chr(m_lSeparador) & Den(1) & Chr(m_lSeparador) & Den(2)
        sLinea = sLinea & Chr(m_lSeparador) & Den(3) & Chr(m_lSeparador) & Den(4) & Chr(m_lSeparador) & val(0) & Chr(m_lSeparador) & val(1) & Chr(m_lSeparador) & val(2) & Chr(m_lSeparador) & val(3) & Chr(m_lSeparador) & val(4) & Chr(m_lSeparador) & Den(5) & Chr(m_lSeparador) & Den(6) & Chr(m_lSeparador) & Den(7) & Chr(m_lSeparador) & Den(8) & Chr(m_lSeparador) & Den(9) & Chr(m_lSeparador) & val(5) & Chr(m_lSeparador) & val(6) & Chr(m_lSeparador) & val(7) & Chr(m_lSeparador) & val(8) & Chr(m_lSeparador) & val(9) & Chr(m_lSeparador) & oArt.GMN1Cod & Chr(m_lSeparador) & oArt.GMN2Cod & Chr(m_lSeparador) & oArt.GMN3Cod & Chr(m_lSeparador) & oArt.GMN4Cod & Chr(m_lSeparador) & Id(0) & Chr(m_lSeparador) & Id(1) & Chr(m_lSeparador) & Id(2) & Chr(m_lSeparador) & Id(3) & Chr(m_lSeparador) & Id(4) & Chr(m_lSeparador) & Id(5) & Chr(m_lSeparador) & Id(6) & Chr(m_lSeparador) & Id(7) & Chr(m_lSeparador) & Id(8) & Chr(m_lSeparador) & Id(9) & Chr(m_lSeparador) & oArt.Cod & Chr(m_lSeparador) & oArt.Den
        sdbgArt.AddItem sLinea
    Next
    
    sdbgArt.AllowAddNew = False
    sdbgArt.AllowDelete = False
    
    basSeguridad.RegistrarAccion accionessummit.ACCArticDatExtConsultar, "Proveedor:" & sdbcProveCod.Text
    
    'Si hay datos habilita los botones
    If m_oArticulos.Count > 0 Then
        HabilitarBotones True
    Else
        HabilitarBotones False
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgArt_AfterDelete(RtnDispErrMsg As Integer)
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    RtnDispErrMsg = 0
    If Me.Visible Then sdbgArt.SetFocus
End Sub

Private Sub sdbgArt_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgArt_BtnClick()
    Dim oImagen As CImagen
    Dim sCod As String
    Dim oArt As CArticulo
    Dim iRow As Long
    Dim vBookmark As Variant
    
    If sdbgArt.Col = -1 Then Exit Sub
    
    If sdbgArt.Columns(sdbgArt.Col).Name = "Imagen" Then
        'Muestra la imagen asociada al art�culo
        bNoHayImagen = False
        
        Set oImagen = oFSGSRaiz.Generar_CImagen
        
        oImagen.Articulo.GMN1Cod = sdbgArt.Columns("GMN1").Value
        oImagen.Articulo.GMN2Cod = sdbgArt.Columns("GMN2").Value
        oImagen.Articulo.GMN3Cod = sdbgArt.Columns("GMN3").Value
        oImagen.Articulo.GMN4Cod = sdbgArt.Columns("GMN4").Value
        oImagen.Articulo.Cod = sdbgArt.Columns("COD").Value
        oImagen.proveedor = sdbcProveCod.Text
        
        Set frmCATDatExtImagen.oImg = oImagen
        
        'Caption del formulario
        frmCATDatExtImagen.caption = sArticulo & ": " & sdbgArt.Columns("Articulo").Value
        frmCATDatExtImagen.Show 1
        
    ElseIf sdbgArt.Columns(sdbgArt.Col).Name = "ESP" Then
        'Especificaciones
        iRow = sdbgArt.Row

        vBookmark = sdbgArt.RowBookmark(iRow)

        If IsNull(vBookmark) Or IsEmpty(vBookmark) Then
            vBookmark = 0
        End If

        sdbgArt.Bookmark = vBookmark
        
        sCod = sdbgArt.Columns("GMN1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbgArt.Columns("GMN1").Value))
        sCod = sCod & sdbgArt.Columns("GMN2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(sdbgArt.Columns("GMN2").Value))
        sCod = sCod & sdbgArt.Columns("GMN3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(sdbgArt.Columns("GMN3").Value))
        sCod = sCod & sdbgArt.Columns("GMN4").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(sdbgArt.Columns("GMN4").Value))
        sCod = sCod & sdbgArt.Columns("COD").Value
        
        Set oArt = m_oArticulos.Item(sCod)
        If oArt Is Nothing Then Exit Sub

        Set frmCATDatExtAdjun.g_oArtProve = oArt
        If frmCATDatExtAdjun.g_oArtProve Is Nothing Then Exit Sub
        
        Set frmCATDatExtAdjun.g_oProve = oProveSeleccionado
        If frmCATDatExtAdjun.g_oProve Is Nothing Then Exit Sub
                
        frmCATDatExtAdjun.caption = sArticulo & ": " & sdbgArt.Columns("COD").Value & " - " & sdbgArt.Columns("DEN").Value & "  /  " & m_sIdiProv & " " & oProveSeleccionado.Cod & " - " & oProveSeleccionado.Den

        frmCATDatExtAdjun.Show 1

        'Refresca la grid
        If frmCATDatExtAdjun.g_oArtProve.ConEspecific = True Then
            sdbgArt.Columns("CONESP").Value = "1"
            sdbgArt.Columns("ESP").CellStyleSet "Espec"
        Else
            sdbgArt.Columns("CONESP").Value = "0"
            sdbgArt.Columns("ESP").CellStyleSet ""
        End If
        DoEvents
        sdbgArt.Update
        If Me.Visible Then sdbgArt.SetFocus
        
    End If
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO_DATOS_EXT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        lblProv.caption = Ador(0).Value
        m_sIdiProv = Ador(0).Value
        Ador.MoveNext
        lblMaterial.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkSoloArt.caption = Ador(0).Value
        Ador.MoveNext
        cmdSolicitar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCargar.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        sdbgArt.caption = Ador(0).Value
        Ador.MoveNext
        sdbgArt.Columns("Articulo").caption = Ador(0).Value
        sArticulo = Ador(0).Value
        Ador.MoveNext
        sdbgArt.Columns("Codigo").caption = Ador(0).Value
        Ador.MoveNext
        txtAtributo = Ador(0).Value
        For i = 1 To 10
            sdbgArt.Columns("Atributo" & i).caption = Ador(0).Value & i
        Next i
        
        Ador.MoveNext
        txtValor = Ador(0).Value
        For i = 1 To 10
            sdbgArt.Columns("Valor" & i).caption = Ador(0).Value & i
        Next i
        
        Ador.MoveNext
        sdbcProveCod.Columns("Codigo").caption = Ador(0).Value
        sdbcProveDen.Columns("Codigo").caption = Ador(0).Value
        Ador.MoveNext
        sdbcProveCod.Columns("Denominacion").caption = Ador(0).Value
        sdbcProveDen.Columns("Denominacion").caption = Ador(0).Value
        txtDen = Ador(0).Value
        Ador.MoveNext
        sIdiSi = Ador(0).Value
        Ador.MoveNext
        sIdiNo = Ador(0).Value
        Ador.MoveNext
        cmdImag.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdThumb.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiAtributos = Ador(0).Value
        Ador.MoveNext
        txtTitulo = Ador(0).Value
        Ador.MoveNext
        txtDe = Ador(0).Value
        Ador.MoveNext
        txtPag = Ador(0).Value
        Ador.MoveNext
        sEspera(1) = Ador(0).Value
        Ador.MoveNext
        sEspera(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(3) = Ador(0).Value
        
        Ador.MoveNext
        cmdEsp.ToolTipText = Ador(0).Value
        Ador.MoveNext
        sdbgArt.Columns("ESP").caption = Ador(0).Value
        m_sIdiEspec = Ador(0).Value
        Ador.MoveNext
        sdbgArt.Columns("Imagen").caption = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
    
End Sub


Public Sub CargarNuevosAtributosProv(ByVal oProve As CProveedor, ByVal sGMN1 As String, ByVal sGMN2 As String, ByVal sGMN3 As String, ByVal sGMN4 As String)
    
    If oProve.Cod = "" Then
        Set oProve = Nothing
        Exit Sub
    End If
    If sGMN1 = "" Then Exit Sub
    
    'Selecciona el proveedor en la combo
    RespetarComboProve = True
    sdbcProveCod.Text = oProve.Cod
    sdbcProveDen.Text = oProve.Den
    
    If oProves Is Nothing Then
        Set oProveSeleccionado = oProve
    Else
        If oProves.Count = 0 Then
            oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , , False, False, False, m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
        End If
        Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    End If
    
    Set oProve = Nothing
    
    cmdSELMAT.Enabled = True
    cmdListado.Enabled = True
    RespetarComboProve = False
    
    'Selecciona el material
    txtMat = sGMN1
    If sGMN2 <> "" Then
        txtMat = txtMat & " - " & sGMN2
    End If
    If sGMN3 <> "" Then
        txtMat = txtMat & " - " & sGMN3
    End If
    If sGMN4 <> "" Then
        txtMat = txtMat & " - " & sGMN4
    End If
    
    sGMN1Cod = sGMN1
    sGMN2Cod = sGMN2
    sGMN3Cod = sGMN3
    sGMN4Cod = sGMN4
    
    'Muestra los art�culos para el proveedor y material
    MostrarArticulos
End Sub

Private Sub sdbgArt_InitColumnProps()
    sdbgArt.SplitterVisible = True
    sdbgArt.SplitterPos = 1
End Sub

Private Sub sdbgArt_RowLoaded(ByVal Bookmark As Variant)
    If sdbgArt.Columns("CONIMAGEN").Value <> "0" Then
        sdbgArt.Columns("Imagen").CellStyleSet "Imagen"
    Else
        sdbgArt.Columns("Imagen").CellStyleSet ""
    End If

    If sdbgArt.Columns("CONESP").Value <> "0" Then
        sdbgArt.Columns("ESP").CellStyleSet "Espec"
    Else
        sdbgArt.Columns("ESP").CellStyleSet ""
    End If
End Sub

Private Sub sdbgArt_SplitterMove(Cancel As Integer)
    Cancel = True
End Sub

Private Sub LimpiarArrayTexto(ByRef a() As String)
'*********************************************************************************
'*** Descripci�n: Recibe un vector de tipo string y a todos los elementos de   ***
'***              ese vector les asigna la cadena vac�a ""; les limpia.        ***
'***                                                                           ***
'*** Par�metros: a() ::>> Un vector de tipo String que es pasado por           ***
'***                      referencia puesto que el contenido del vector ha de  ***
'***                      ser modificado y devuelto el vector.                 ***
'***                                                                           ***
'*** Valor que devuelve: El vector que recibe tras ser "limpiado", es decir,   ***
'***                     tras haber sido asignada la cadena vac�a a todos sus  ***
'***                     elementos.                                            ***
'*********************************************************************************
    Dim iLimiteSuperior As Integer
    Dim iLimiteInferior As Integer
    Dim i As Integer
    
    iLimiteInferior = LBound(a)
    iLimiteSuperior = UBound(a)
    For i = iLimiteInferior To iLimiteSuperior
        a(i) = ""
    Next i
End Sub

Private Sub RellenarAdoParaListado()
Dim oArticulo As CArticulo
Dim oatrib As CAtributo

If m_oArticulos Is Nothing Then Exit Sub

Set g_ador = New Ador.Recordset
g_ador.Fields.Append "ART", adVarChar, 50
g_ador.Fields.Append "DENART", adVarChar, 200
g_ador.Fields.Append "CODEXT", adVarChar, 100
g_ador.Fields.Append "COD", adVarChar, 100
g_ador.Fields.Append "DEN", adVarChar, 100
g_ador.Fields.Append "VALOR", adVarChar, cLongAtribEsp_Texto
g_ador.Open
      
For Each oArticulo In m_oArticulos
                    
    If Not oArticulo.ATRIBUTOS Is Nothing Then
    For Each oatrib In oArticulo.ATRIBUTOS
        g_ador.AddNew
        g_ador("ART").Value = oArticulo.Cod
        g_ador("DENART").Value = oArticulo.Den
        g_ador("CODEXT").Value = NullToStr(oArticulo.CodigoExterno)
    
        g_ador("COD").Value = oatrib.Cod
        g_ador("DEN").Value = oatrib.Den
        Select Case oatrib.Tipo '= TipoNumerico
        Case TiposDeAtributos.TipoString
            g_ador("VALOR").Value = NullToStr(oatrib.valor)
        Case TiposDeAtributos.TipoNumerico
            g_ador("VALOR").Value = NullToStr(oatrib.valor)
        Case TiposDeAtributos.TipoFecha
            g_ador("VALOR").Value = NullToStr(oatrib.valor)
        Case TiposDeAtributos.TipoBoolean
            Select Case oatrib.valor
            Case 1, True
                g_ador("VALOR").Value = sIdiSi
            Case 0, False
                g_ador("VALOR").Value = sIdiNo
            Case Else
                g_ador("VALOR").Value = ""
            End Select
        End Select
    Next
    End If
Next

End Sub
Private Sub Timer1_Timer()

   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub

