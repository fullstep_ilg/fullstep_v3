VERSION 5.00
Begin VB.Form frmMsgboxOptima 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FULLSTEP"
   ClientHeight    =   2610
   ClientLeft      =   2910
   ClientTop       =   3300
   ClientWidth     =   8085
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2610
   ScaleWidth      =   8085
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   4140
      TabIndex        =   2
      Top             =   2190
      Width           =   1105
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   2910
      TabIndex        =   1
      Top             =   2190
      Width           =   1105
   End
   Begin VB.Image Image2 
      Height          =   480
      Left            =   75
      Picture         =   "frmMsgboxOptima.frx":0000
      Top             =   150
      Width           =   480
   End
   Begin VB.Label lblLetraG 
      Caption         =   "�Atenci�n! "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   825
      Left            =   750
      TabIndex        =   3
      Top             =   240
      Width           =   7140
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblLetraN 
      Caption         =   "Mensaje"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   750
      TabIndex        =   4
      Top             =   1110
      Width           =   7140
      WordWrap        =   -1  'True
   End
   Begin VB.Image Image1 
      Height          =   555
      Left            =   75
      Picture         =   "frmMsgboxOptima.frx":00F6
      Top             =   120
      Width           =   585
   End
   Begin VB.Label lblMensaje 
      AutoSize        =   -1  'True
      Caption         =   "Mensaje"
      Height          =   495
      Left            =   720
      TabIndex        =   0
      Top             =   240
      Width           =   6900
   End
End
Attribute VB_Name = "frmMsgboxOptima"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public sOrigen As String
Public sMensaje As String

Private sMensajeTexto1 As String
Private sMensajeTexto2 As String
Private sMensajePond As String
Private sMensajeProce(1 To 6) As String
Private sMensajeAbrirProc(1 To 4) As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub cmdCancel_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMsgboxOptima", "cmdCancel_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdOK_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sOrigen = "frmPROCE" Then
        frmPROCE.g_bCancelar = False
    ElseIf sOrigen = "frmSOLAbrirProc" Then
        frmSOLAbrirProc.g_bIrA = True
    End If
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMsgboxOptima", "cmdOK_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMsgboxOptima", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
    'Posiciona el formulario en la mitad del control de la �ptima,de forma que cuando se
    'cierre este formulario no se oculte el picture de la �ptima.
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.Width = 2895
    Me.Height = 1830
    lblMensaje.Visible = True
    cmdCancel.Visible = False
    lblLetraG.Visible = False
    lblLetraN.Visible = False
    lblMensaje.Height = 675
    lblMensaje.Width = 2040
    cmdOK.Left = 840
    cmdOK.Top = 960
    
    
    Select Case sOrigen
        Case "frmADJ"

            Me.Top = frmADJ.picControlOptima.Top + (frmADJ.picControlOptima.Height / 2)
            Me.Left = frmADJ.picControlOptima.Left + (frmADJ.picControlOptima.Width / 4)
            
        Case "frmRESREU"

            Me.Top = frmRESREU.picControlOptima.Top + (frmRESREU.picControlOptima.Height / 2)
            Me.Left = frmRESREU.picControlOptima.Left + (frmRESREU.picControlOptima.Width / 4)
            
        Case "frmPROCE"
            Me.Width = 8205
            Me.Height = 3015
            lblMensaje.Visible = False
            lblLetraG.Visible = True
            lblLetraG.Height = 825
            lblLetraG.Width = 7140
            lblLetraN.Visible = True
            lblLetraN.Height = 945
            lblLetraN.Width = 7140
            cmdOK.Left = 2910
            cmdOK.Top = 2190
            cmdCancel.Top = 2190
            cmdCancel.Left = 4140
            cmdCancel.Visible = True
            
        Case "frmSOLAbrirProc"
            Image1.Visible = False
            Image2.Visible = True
            cmdOK.Width = 2400
            If sMensaje = "OFERTAS" Then
                Me.Width = 4500
                lblMensaje.Width = 4200
            Else
                Me.Width = 5200
                lblMensaje.Width = 5000
            End If
    End Select
    
    CargarRecursos
    
    'Muestra un mensaje otro dependiendo del par�metro pasado
    Select Case sMensaje
        Case "POND"
            lblMensaje.caption = sMensajePond
        Case "TEXTO"
            lblMensaje.caption = sMensajeTexto1 & vbCrLf & sMensajeTexto2
        Case "VALIDAR"
            cmdOK.caption = sMensajeProce(1)
            cmdCancel.caption = sMensajeProce(2)
            lblLetraG.caption = sMensajeProce(3) & vbCrLf & sMensajeProce(4)
            lblLetraN.caption = sMensajeProce(5) & vbCrLf & sMensajeProce(6)
        Case "OFERTAS"
            lblMensaje.caption = sMensajeAbrirProc(1)
            cmdOK.caption = sMensajeAbrirProc(2)
        Case "ADJUDICACION"
            lblMensaje.caption = sMensajeAbrirProc(3)
            cmdOK.caption = sMensajeAbrirProc(4)
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMsgboxOptima", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_MSGBOX_OPTIMA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sMensajePond = Ador(0).Value
        Ador.MoveNext
        sMensajeTexto1 = Ador(0).Value
        Ador.MoveNext
        sMensajeTexto2 = Ador(0).Value
        For i = 1 To 6
            Ador.MoveNext
            sMensajeProce(i) = Ador(0).Value
        Next
        
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        
        For i = 1 To 4
            Ador.MoveNext
            sMensajeAbrirProc(i) = Ador(0).Value
        Next
        
        Ador.Close
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMsgboxOptima", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
        
End Sub

Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sOrigen = "frmPROCE" Then
            Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
            If (MDI.ScaleHeight / 2 - Me.Height / 2) > 200 Then
                Me.Top = (MDI.ScaleHeight / 2 - Me.Height / 2) - 500
            End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub

Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMsgboxOptima", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


