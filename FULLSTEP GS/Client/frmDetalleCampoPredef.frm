VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmDetalleCampoPredef 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DDetalle de campo"
   ClientHeight    =   6450
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6255
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDetalleCampoPredef.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6450
   ScaleWidth      =   6255
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraAnyoImputacion 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   120
      TabIndex        =   33
      Top             =   1050
      Visible         =   0   'False
      Width           =   6195
      Begin SSDataWidgets_B.SSDBCombo sdbcPartidas 
         Height          =   285
         Left            =   2070
         TabIndex        =   34
         Top             =   90
         Width           =   3855
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         HeadLines       =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "VALUE"
         Columns(0).Name =   "VALUE"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   7673
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6800
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label lblPartidas 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DPartida presupuestaria:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   35
         Top             =   90
         Width           =   1800
      End
   End
   Begin VB.Frame fraCentroCoste 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   120
      TabIndex        =   15
      Top             =   960
      Visible         =   0   'False
      Width           =   6015
      Begin VB.CheckBox chkVerUON 
         BackColor       =   &H00808000&
         Caption         =   "DVer estructura organizativa"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         MaskColor       =   &H00FFFFFF&
         TabIndex        =   16
         Top             =   120
         Visible         =   0   'False
         Width           =   5295
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCentroCoste 
         Height          =   285
         Left            =   2280
         TabIndex        =   17
         Top             =   600
         Width           =   3495
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID_CAMPO"
         Columns(0).Name =   "ID_CAMPO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DGrupo"
         Columns(1).Name =   "DEN_GRUPO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HeadBackColor=   8421504
         Columns(2).Width=   3200
         Columns(2).Caption=   "DCampo desglose"
         Columns(2).Name =   "DEN_DESGLOSE"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HeadBackColor=   8421504
         Columns(3).Width=   3200
         Columns(3).Caption=   "DCampo"
         Columns(3).Name =   "DEN_CAMPO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).HasHeadBackColor=   -1  'True
         Columns(3).HeadBackColor=   8421504
         _ExtentX        =   6165
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 3"
      End
      Begin VB.Label lblCentroCoste 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DCentro de coste relacionado:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   18
         Top             =   600
         Width           =   2190
      End
   End
   Begin VB.CheckBox chkSelCualquierMatPortal 
      BackColor       =   &H00808000&
      Caption         =   "DSeleccionar cualquier material en el PORTAL"
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      MaskColor       =   &H00FFFFFF&
      TabIndex        =   32
      Top             =   1200
      Width           =   5175
   End
   Begin VB.Frame fraLista 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   3255
      Left            =   0
      TabIndex        =   24
      Top             =   3120
      Width           =   6135
      Begin VB.CommandButton cmdAnyaValor 
         Height          =   285
         Left            =   4560
         Picture         =   "frmDetalleCampoPredef.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   240
         Width           =   321
      End
      Begin VB.CommandButton cmdElimValor 
         Height          =   285
         Left            =   4920
         Picture         =   "frmDetalleCampoPredef.frx":0FF4
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   240
         Width           =   321
      End
      Begin VB.CommandButton cmdMvtoElemLista 
         Height          =   285
         Index           =   0
         Left            =   5280
         Picture         =   "frmDetalleCampoPredef.frx":1336
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   321
      End
      Begin VB.CommandButton cmdMvtoElemLista 
         Height          =   285
         Index           =   1
         Left            =   5640
         Picture         =   "frmDetalleCampoPredef.frx":1678
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   321
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "D&Cancelar"
         Height          =   315
         Left            =   3240
         TabIndex        =   26
         Top             =   2760
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "D&Aceptar"
         Height          =   315
         Left            =   2040
         TabIndex        =   25
         Top             =   2760
         Width           =   1005
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgLista 
         Height          =   1875
         Left            =   120
         TabIndex        =   31
         Top             =   600
         Width           =   5895
         ScrollBars      =   3
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   4
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmDetalleCampoPredef.frx":19BA
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         RowSelectionStyle=   1
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   9790
         Columns(0).Caption=   "VALOR"
         Columns(0).Name =   "VALOR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "EXISTE"
         Columns(1).Name =   "EXISTE"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ORDEN"
         Columns(2).Name =   "ORDEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "ORDEN_ANT"
         Columns(3).Name =   "ORDEN_ANT"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         _ExtentX        =   10398
         _ExtentY        =   3307
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraNivelSeleccion 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   120
      TabIndex        =   19
      Top             =   2520
      Visible         =   0   'False
      Width           =   6015
      Begin SSDataWidgets_B.SSDBCombo sdbcNivelSeleccion 
         Height          =   285
         Left            =   2280
         TabIndex        =   20
         Top             =   90
         Width           =   3495
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         HeadLines       =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "VALUE"
         Columns(0).Name =   "VALUE"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   7673
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6165
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         Enabled         =   0   'False
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label lblNivelSeleccionObligatorio 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DNivel de selecci�n obligatorio:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   21
         Top             =   90
         Width           =   2220
      End
   End
   Begin VB.Frame FraRefSolicitud1 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   240
      TabIndex        =   9
      Top             =   1300
      Width           =   5295
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoSolicit 
         Height          =   285
         Left            =   1440
         TabIndex        =   10
         Top             =   120
         Width           =   3735
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2249
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   9419
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblTextoTipoSOL 
         BackColor       =   &H00808000&
         Caption         =   "Label2"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   1440
         TabIndex        =   12
         Top             =   120
         Width           =   3705
      End
      Begin VB.Label lblTipoSolicitud 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DTipo de solicitud:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   11
         Top             =   120
         Width           =   1305
      End
   End
   Begin VB.Frame FraRefSolicitud2 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   615
      Left            =   120
      TabIndex        =   13
      Top             =   1800
      Visible         =   0   'False
      Width           =   5175
      Begin VB.OptionButton optAvisoBloqueoImpAcum 
         BackColor       =   &H00808000&
         Caption         =   "DAvisar cuando el importe acumulado supere el de la solicitud"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Width           =   5175
      End
      Begin VB.OptionButton optAvisoBloqueoImpAcum 
         BackColor       =   &H00808000&
         Caption         =   "DBloquear cuando el importe acumulado supere el de la solicitud"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   0
         Left            =   240
         MaskColor       =   &H00808000&
         TabIndex        =   7
         Top             =   0
         Width           =   5055
      End
   End
   Begin VB.CheckBox chkFechaValorDefecto 
      BackColor       =   &H00808000&
      Caption         =   "DIntroducir fechas concretas como valores por defecto"
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   870
      MaskColor       =   &H00FFFFFF&
      TabIndex        =   5
      Top             =   960
      Visible         =   0   'False
      Width           =   5145
   End
   Begin VB.CheckBox chkAnyadirArticulo 
      BackColor       =   &H00808000&
      Caption         =   "DPermitir c�digos que no est�n en el maestro de art�culos"
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaskColor       =   &H00FFFFFF&
      TabIndex        =   4
      Top             =   960
      Width           =   5175
   End
   Begin VB.CheckBox chkCargarUltADJ 
      BackColor       =   &H00808000&
      Caption         =   "DCargar con el �ltimo precio / proveedor adjudicado"
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaskColor       =   &H00FFFFFF&
      TabIndex        =   6
      Top             =   960
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.CheckBox chkCargarSuminArt 
      BackColor       =   &H00808000&
      Caption         =   "DCargar con el proveedor que suministra el art�culo"
      ForeColor       =   &H00FFFFFF&
      Height          =   465
      Left            =   120
      MaskColor       =   &H00FFFFFF&
      TabIndex        =   23
      Top             =   1260
      Visible         =   0   'False
      Width           =   6015
   End
   Begin VB.CheckBox chkNoFecAntSis 
      BackColor       =   &H00808000&
      Caption         =   "DNo permitir fecha anterior a la fecha del sistema"
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   870
      MaskColor       =   &H00FFFFFF&
      TabIndex        =   22
      Top             =   1350
      Visible         =   0   'False
      Width           =   5175
   End
   Begin VB.Label lblCargarUltADJ 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "DCargar con el �ltimo precio / proveedor adjudicado"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   360
      TabIndex        =   14
      Top             =   1080
      Width           =   465
   End
   Begin VB.Line oLinea 
      BorderColor     =   &H00FFFFFF&
      Visible         =   0   'False
      X1              =   240
      X2              =   5300
      Y1              =   1080
      Y2              =   1080
   End
   Begin VB.Label lblTipoCampo 
      BackColor       =   &H00808000&
      Caption         =   "Label2"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   1700
      TabIndex        =   3
      Top             =   600
      Width           =   3700
   End
   Begin VB.Label lblPredef 
      BackColor       =   &H00808000&
      Caption         =   "Label1"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   1700
      TabIndex        =   2
      Top             =   160
      Width           =   3700
   End
   Begin VB.Label lblLitTipo 
      BackColor       =   &H00808000&
      Caption         =   "DTipo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   1400
   End
   Begin VB.Label lblLitPredef 
      BackColor       =   &H00808000&
      Caption         =   "DPredefinido:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   160
      Width           =   1400
   End
End
Attribute VB_Name = "frmDetalleCampoPredef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_iTipo As TiposDeAtributos
Public g_sDenCampo As String
Public g_iTipoCampo As Long
Public g_iTipoSolicit As TipoCampoPredefinido
Public g_lCampoId As Long
Public g_sOrigen As String
Public g_lCampoPadre As Long
Public g_oCampoPredef As CCampoPredef
Public g_bOcultarSelCualquierMatPortal As Boolean

Private m_sIdiTipo(2 To 12) As String
Private m_sIdiTipoCampo(4) As String
Private m_sIdiCheckBox(2) As String
Private oCampo As CFormItem
Private m_lIdSolicitud As Long
Private m_lIDCentroCoste As Long
Private m_bVerUON As Boolean
Private m_ListaCentros As Collection
Private m_ListaPartidas As Collection
Private m_sNinguno As String
Private m_inivelSeleccion As Integer
Private m_bpermitirCambiarNivelSeleccion As Boolean
Private m_bhaCambiadoNivelSeleccion As Boolean
Private i As Integer
Private bNoFecAntSisModificado As Boolean
Private m_bModifLista As Boolean
Private m_bRespetarLista As Boolean
Private m_oIdiomas As CIdiomas
Private m_bError As Boolean
Private m_lIDPartidas As Long

Private Sub chkAnyadirArticulo_Click()
    Dim irespuesta As Integer
    
    If chkAnyadirArticulo.Value = vbUnchecked Then
        irespuesta = oMensajes.PreguntaEliminarArticulosSinCodificar
    End If

    If (irespuesta = vbYes) Or (chkAnyadirArticulo.Value = vbChecked) Then
        If irespuesta = vbYes Then
            oCampo.PonerNuloCodigoDenominacion g_lCampoPadre
        End If
        oCampo.AnyadirArt = chkAnyadirArticulo.Value
        oCampo.ModificarAnyadirArt
    Else
        chkAnyadirArticulo.Value = vbChecked
    End If
End Sub

Private Sub chkSelCualquierMatPortal_Click()
    
    
    oCampo.SelCualquierMatPortal = chkSelCualquierMatPortal.Value
    oCampo.ModificarSelCualquierMatPortal

End Sub

Private Sub chkCargarSuminArt_Click()
oCampo.CargarSuminArt = chkCargarSuminArt.Value
oCampo.ModificarCampoTipoBool "CARGAR_REL_PROVE_ART4", oCampo.CargarSuminArt
If oCampo.CargarSuminArt Then
    chkCargarUltADJ.Value = vbUnchecked
    ChkCargarUltADJ_Click
End If
End Sub

Private Sub chkFechaValorDefecto_Validate(Cancel As Boolean)
    i = i + 1
End Sub

Private Sub chkNoFecAntSis_Click()
oCampo.NoFecAntSis = SQLBinaryToBoolean(chkNoFecAntSis.Value)
oCampo.ModificarCampoTipoFecha "FECHA_VALOR_NO_ANT_SIS", oCampo.NoFecAntSis, False
End Sub

Private Sub chkNoFecAntSis_Validate(Cancel As Boolean)
bNoFecAntSisModificado = True
End Sub




Private Sub chkVerUON_Click()
    If CBool(chkVerUON.Value) <> m_bVerUON Then
        m_bVerUON = CBool(chkVerUON.Value)
        oCampo.VerUON = m_bVerUON
        oCampo.ModificarVerUON
    End If
End Sub

Private Sub cmdAceptar_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim oIdioma As CIdioma
    Dim i As Integer
    Dim vbm As Variant
    Dim oValores As CCampoValorListas
    Dim oValoresTxt As CMultiidiomas
    Dim oCampos As CFormItems
                        
    With sdbgLista
        If .DataChanged Then
            m_bError = False
            .Update
            If m_bError Then
                If Me.Visible Then sdbgLista.SetFocus
                Exit Sub
            End If
        End If
                
        If .Rows = 0 Then
            oMensajes.IntroducirListaValores
            Exit Sub
        Else
            .MoveFirst
            If m_bError Then
                If Me.Visible Then .SetFocus
                Exit Sub
            End If
        End If
            
        'A�ade el campo nuevo a BD:
        Screen.MousePointer = vbHourglass
        
        .MoveFirst
        Set oValores = oFSGSRaiz.Generar_CCampoValorListas
                    
        For i = 0 To .Rows - 1
            vbm = .AddItemBookmark(i)
                          
            Dim iTipo As TiposDeAtributos
            If Not g_oCampoPredef Is Nothing Then
                iTipo = g_oCampoPredef.Tipo
            Else
                iTipo = g_iTipo
            End If
            Select Case iTipo
                Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                    Set oValoresTxt = oFSGSRaiz.Generar_CMultiidiomas
                    For Each oIdioma In m_oIdiomas
                        If .Columns(oIdioma.Cod).CellValue(vbm) <> "" Then
                            oValoresTxt.Add oIdioma.Cod, .Columns(oIdioma.Cod).CellValue(vbm)
                        End If
                    Next
                    If Not g_oCampoPredef Is Nothing Then
                        oValores.Add i + 1, , , oValoresTxt, , , , g_oCampoPredef
                    Else
                        oValores.Add i + 1, oCampo, , oValoresTxt
                    End If
                    Set oValoresTxt = Nothing
                Case TiposDeAtributos.TipoNumerico
                    If Trim(.Columns("VALOR").CellValue(vbm)) <> "" Then
                        If Not g_oCampoPredef Is Nothing Then
                            oValores.Add i + 1, , Trim(.Columns("VALOR").CellValue(vbm)), , , , , g_oCampoPredef
                        Else
                            oValores.Add i + 1, oCampo, Trim(.Columns("VALOR").CellValue(vbm))
                        End If
                    End If
                    
                Case TiposDeAtributos.TipoFecha
                    If Trim(.Columns("VALOR").CellValue(vbm)) <> "" Then
                        If Not g_oCampoPredef Is Nothing Then
                            oValores.Add i + 1, , , , Trim(.Columns("VALOR").CellValue(vbm)), , , g_oCampoPredef
                        Else
                            oValores.Add i + 1, oCampo, , , Trim(.Columns("VALOR").CellValue(vbm))
                        End If
                    End If
            End Select
        Next i
        
        If Not g_oCampoPredef Is Nothing Then
            Set g_oCampoPredef.ValoresLista = oValores
            udtTeserror = g_oCampoPredef.ActualizarValoresLista
        Else
            Set oCampo.ValoresLista = oValores
            udtTeserror = oCampo.ActualizarValoresLista
        End If
        
        If udtTeserror.NumError <> TESnoerror Then TratarError udtTeserror
    End With
            
    Set oValores = Nothing
    
    Screen.MousePointer = vbNormal
        
    Unload Me
End Sub

Private Sub cmdAnyaValor_Click()
    With sdbgLista
        .Scroll 0, .Rows - .Row
        
        If .VisibleRows > 0 Then
            If .VisibleRows > .Rows Then
                .Row = .Rows
            Else
                .Row = .Rows - (.Rows - .VisibleRows) - 1
            End If
            
            m_bModifLista = True
        End If
    
        If Me.Visible Then .SetFocus
    End With
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdElimValor_Click()
    Dim sCampo As String

    With sdbgLista
        If .Rows > 0 Then
            Screen.MousePointer = vbHourglass
                
            .SelBookmarks.Add sdbgLista.Bookmark
            If .AddItemRowIndex(.Bookmark) > -1 Then
                .RemoveItem (.AddItemRowIndex(sdbgLista.Bookmark))
            Else
                .RemoveItem (0)
            End If
    
            If .Rows > 0 Then
                If IsEmpty(.RowBookmark(.Row)) Then
                    .Bookmark = .RowBookmark(.Row - 1)
                Else
                    .Bookmark = .RowBookmark(.Row)
                End If
            End If
            .SelBookmarks.RemoveAll
            If Me.Visible Then .SetFocus
    
            m_bModifLista = True
    
            Screen.MousePointer = vbNormal
        End If
    End With
End Sub

Private Sub cmdMvtoElemLista_Click(Index As Integer)
    Dim sArrText() As String
    Dim sArrText2() As String
    Dim i As Integer
    Dim iTope As Integer

    With sdbgLista
      If .SelBookmarks.Count = 0 Then Exit Sub
      If Index = 0 Then
          If .AddItemRowIndex(.SelBookmarks.Item(0)) = 0 Then Exit Sub
      Else
          If .AddItemRowIndex(.SelBookmarks.Item(0)) = .Rows - 1 Then Exit Sub
      End If
    
      iTope = .Columns.Count - 1
      ReDim Preserve sArrText(iTope)
      ReDim Preserve sArrText2(iTope)
      
      For i = 0 To iTope
          sArrText(i) = sdbgLista.Columns(i).Value
      Next
      If Index = 0 Then
          .MovePrevious
      Else
          .MoveNext
      End If
      For i = 0 To iTope
          sArrText2(i) = .Columns(i).Value
          .Columns(i).Value = sArrText(i)
      Next
      m_bRespetarLista = True
      If Index = 0 Then
          .MoveNext
      Else
          .MovePrevious
      End If
      For i = 0 To iTope
          .Columns(i).Value = sArrText2(i)
      Next
       
      .SelBookmarks.RemoveAll
      If Index = 0 Then
          .MovePrevious
      Else
          .MoveNext
      End If
      m_bRespetarLista = False
      .SelBookmarks.Add sdbgLista.Bookmark
    End With
End Sub

Private Sub Form_Load()
Dim j As Integer
    Me.Height = 1485
    Me.Width = 5625
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    chkFechaValorDefecto.Visible = False
    chkNoFecAntSis.Visible = False
    i = 1
    Me.chkAnyadirArticulo.Visible = False
    Me.chkSelCualquierMatPortal.Visible = False

    lblCargarUltADJ.Visible = False
    FraRefSolicitud2.Visible = False
    fraLista.Visible = False
    Select Case g_iTipoCampo
        Case TipoCampoGS.Pres1
            Me.caption = Me.caption & " " & basParametros.gParametrosGenerales.gsSingPres1
        Case TipoCampoGS.Pres2
            Me.caption = Me.caption & " " & basParametros.gParametrosGenerales.gsSingPres2
        Case TipoCampoGS.Pres3
            Me.caption = Me.caption & " " & basParametros.gParametrosGenerales.gsSingPres3
        Case TipoCampoGS.Pres4
            Me.caption = Me.caption & " " & basParametros.gParametrosGenerales.gsSingPres4
        Case TipoCampoGS.CodArticulo
            Me.caption = Me.caption & " " & g_sDenCampo
            
        Case TipoCampoGS.NuevoCodArticulo
            Me.caption = Me.caption & " " & g_sDenCampo
            
            If g_sOrigen = "frmFormularios" Or g_sOrigen = "frmDesglose" Then
                Set oCampo = oFSGSRaiz.Generar_CFormCampo
                oCampo.Id = g_lCampoId
                oCampo.CargarAnyadirArt
                Me.chkAnyadirArticulo.Value = IIf(oCampo.AnyadirArt, vbChecked, vbUnchecked)
            
                chkAnyadirArticulo.Visible = True
                chkAnyadirArticulo.Enabled = True
            ElseIf g_sOrigen = "frmSolicitudDetalle" Or g_sOrigen = "frmSolicitudDetalleDesglose" Then
                Set oCampo = oFSGSRaiz.Generar_CFormCampo
                oCampo.Id = g_lCampoId
                oCampo.CargarCopiaAnyadirArt
                Me.chkAnyadirArticulo.Value = IIf(oCampo.AnyadirArt, vbChecked, vbUnchecked)
                chkAnyadirArticulo.Visible = True
                chkAnyadirArticulo.Enabled = False
            End If
            Me.Height = 1800
        Case TipoCampoSC.ArchivoEspecific, TipoCampoSC.DescrBreve
            Me.caption = Me.caption & " " & g_sDenCampo
            lblPredef.Width = 5000
            Me.Width = 6600
        Case TipoCampoSC.FecNecesidad, TipoCampoSC.FinSuministro, TipoCampoSC.IniSuministro
            '' Si es fecha de inicio/fin de suministro o fecha de necesidad mostramos el check
            FraRefSolicitud1.Visible = False
            Me.caption = Me.caption & " " & g_sDenCampo
            Set oCampo = oFSGSRaiz.Generar_CFormCampo
            oCampo.Id = g_lCampoId
            oCampo.CargarFECHAVALORDEFECTO
            chkFechaValorDefecto.Value = IIf(oCampo.FECHAVALORDEFECTO, vbChecked, vbUnchecked)
            chkNoFecAntSis.Value = IIf(oCampo.NoFecAntSis, vbChecked, vbUnchecked)
            If g_sOrigen = "frmSolicitudDetalle" Or g_sOrigen = "frmSolicitudDetalleDesglose" Or oCampo.Id = 0 Then
                chkFechaValorDefecto.Enabled = False
                chkNoFecAntSis.Enabled = False
            End If
            chkFechaValorDefecto.Visible = True
            chkNoFecAntSis.Visible = True
            Me.Height = 2200
            lblPredef.Width = 5000
            Me.Width = 6600
         Case TipoCampoGS.RefSolicitud
            Me.caption = Me.caption & " " & g_sDenCampo
            Me.oLinea.Visible = True
            Me.Height = 3000

            Set oCampo = oFSGSRaiz.Generar_CFormCampo
            oCampo.Id = g_lCampoId
            oCampo.CargarDatosRefSolicitud g_sOrigen
            optAvisoBloqueoImpAcum(oCampo.AvisoBloqueoImpAcum).Value = True
            If oCampo.CampoEnUso Then
                sdbcTipoSolicit.Enabled = False
            End If
            If oCampo.IDRefSolicitud <> 0 Then
                Dim oSolicitud As CSolicitud
                Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
                oSolicitud.Id = oCampo.IDRefSolicitud
                oSolicitud.CargarDatosSolicitud
                m_lIdSolicitud = oCampo.IDRefSolicitud
                If g_sOrigen = "frmFormularios" Then
                    Me.sdbcTipoSolicit.Text = oSolicitud.Denominaciones.Item(gParametrosGenerales.gIdioma).Den
                    FraRefSolicitud1.Enabled = True
                    FraRefSolicitud2.Enabled = True
                    sdbcTipoSolicit.Visible = True
                    lblTextoTipoSOL.Visible = False
                Else
                    lblTextoTipoSOL.caption = oSolicitud.Codigo & " - " & oSolicitud.Denominaciones.Item(gParametrosGenerales.gIdioma).Den
                    sdbcTipoSolicit.Visible = False
                    lblTextoTipoSOL.Visible = True
                    FraRefSolicitud2.Enabled = False
                End If
            Else
                If g_sOrigen = "frmFormularios" Then
                    FraRefSolicitud1.Enabled = True
                    FraRefSolicitud2.Enabled = True
                    sdbcTipoSolicit.Visible = True
                    lblTextoTipoSOL.Visible = False
                Else
                    FraRefSolicitud1.Visible = False
                    FraRefSolicitud2.Enabled = False
                    FraRefSolicitud2.Top = FraRefSolicitud2.Top - FraRefSolicitud1.Height
                    Me.Height = Me.Height - FraRefSolicitud1.Height
                End If
            End If
            FraRefSolicitud2.Visible = True
        Case TipoCampoSC.PrecioUnitario, TipoCampoGS.Proveedor
            Me.caption = Me.caption & " " & g_sDenCampo
            Set oCampo = oFSGSRaiz.Generar_CFormCampo
            oCampo.Id = g_lCampoId
            FraRefSolicitud1.Visible = False
            If g_sOrigen = "frmSolicitudDetalle" Or g_sOrigen = "frmSolicitudDetalleDesglose" Then
                lblCargarUltADJ.Visible = False
                chkCargarUltADJ.Visible = False
                chkCargarSuminArt.Enabled = False
                oCampo.Cargar_SI_Ult_ADJ 1
                If (oCampo.CargarUltADJ = True) Then
                    If g_iTipoCampo = TipoCampoSC.PrecioUnitario Then
                        Me.lblCargarUltADJ.caption = m_sIdiCheckBox(1)
                    Else
                        Me.lblCargarUltADJ.caption = m_sIdiCheckBox(2)
                    End If
                    Me.lblCargarUltADJ.Visible = True
                    Me.Height = 1800
                End If
            Else
                
                oCampo.Cargar_SI_Ult_ADJ 0
                chkCargarUltADJ.Enabled = True
                chkCargarUltADJ.Value = IIf(oCampo.CargarUltADJ, vbChecked, vbUnchecked)
                lblCargarUltADJ.Visible = False
                chkCargarUltADJ.Visible = True
                Me.Height = 2100
                If g_iTipoCampo = TipoCampoSC.PrecioUnitario Then
                    Me.chkCargarUltADJ.caption = m_sIdiCheckBox(1)
                Else
                    Me.chkCargarUltADJ.caption = m_sIdiCheckBox(2)
                End If
            End If
            If g_iTipoCampo = TipoCampoGS.Proveedor Then
                chkCargarSuminArt.Value = IIf(oCampo.CargarSuminArt, vbChecked, vbUnchecked)
                fraCentroCoste.Visible = False
                FraRefSolicitud1.Visible = False
                chkCargarSuminArt.Visible = True
            End If
        Case PrecioUnitarioAdj, ProveedorAdj, CantidadAdj
            lblPredef.Width = 5000
            Me.Width = 6600
            Me.caption = Me.caption & " " & g_sDenCampo
            
        Case TipoCampoGS.Activo, TipoCampoGS.PartidaPresupuestaria
            Me.caption = Me.caption & " " & g_sDenCampo
            Set oCampo = oFSGSRaiz.Generar_CFormCampo
            If oCampo.CampoEnUso(g_lCampoId) Then
                sdbcCentroCoste.Enabled = False
            End If
            FraRefSolicitud1.Visible = False
            FraRefSolicitud2.Visible = False
            Set oCampo = Nothing
            If g_sOrigen = "frmFormularios" Or g_sOrigen = "frmDesglose" Then
                fraCentroCoste.Visible = True
                chkVerUON.Visible = False
                lblCentroCoste.Top = chkVerUON.Top
                sdbcCentroCoste.Top = lblCentroCoste.Top
                Me.Height = 1965
                Me.Width = 6345
            
                CargarCentrosDeCoste
            
                Set oCampo = oFSGSRaiz.Generar_CFormCampo
                oCampo.Id = g_lCampoId
                m_lIDCentroCoste = oCampo.CargarCentroCoste()
                
                If Not m_ListaCentros Is Nothing Then
                    sdbcCentroCoste.RemoveAll
                    sdbcCentroCoste.AddItem 0 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                
                    For j = 1 To m_ListaCentros.Count
                        sdbcCentroCoste.AddItem m_ListaCentros(j)
                    Next
                
                    sdbcCentroCoste.Value = m_lIDCentroCoste
                    sdbcCentroCoste.SelStart = 0
                    sdbcCentroCoste.SelLength = Len(sdbcTipoSolicit.Text)
                    sdbcCentroCoste.Refresh
                End If
            End If
        Case TipoCampoGS.CentroCoste
            FraRefSolicitud1.Visible = False
            Me.caption = Me.caption & " " & g_sDenCampo
            If g_sOrigen = "frmFormularios" Or g_sOrigen = "frmDesglose" Then
                fraCentroCoste.Visible = True
                sdbcCentroCoste.Visible = False
                chkVerUON.Visible = True
                Me.Height = 1965
                Me.Width = 6345
                
                Set oCampo = oFSGSRaiz.Generar_CFormCampo
                oCampo.Id = g_lCampoId
                m_bVerUON = oCampo.CargarVerUON()
                If m_bVerUON Then
                    chkVerUON.Value = 1
                End If
            End If
        Case TipoCampoGS.material
            
            Me.caption = Me.caption & " " & g_sDenCampo
            If g_bOcultarSelCualquierMatPortal = False Then
                Set oCampo = oFSGSRaiz.Generar_CFormCampo
                oCampo.Id = g_lCampoId
                oCampo.CargarSelCualquierMatPortal
                Me.chkSelCualquierMatPortal.Value = IIf(oCampo.SelCualquierMatPortal, vbChecked, vbUnchecked)
                chkSelCualquierMatPortal.Visible = True
            End If
                
            If g_sOrigen <> "frmDesglose" Then

                Me.fraNivelSeleccion.Visible = True
                Me.FraRefSolicitud1.Visible = False
                Me.Width = 6345
                For i = 0 To 4
                    Me.sdbcNivelSeleccion.AddItem i & Chr(m_lSeparador) & IIf(i = 0, m_sNinguno, CStr(i))
                Next
                Me.sdbcNivelSeleccion.Value = 0
                Me.sdbcNivelSeleccion.Text = m_sNinguno
                Me.fraNivelSeleccion.Top = Me.fraCentroCoste.Top
                Me.Height = Me.Height + Me.fraNivelSeleccion.Height + 500
                Me.sdbcNivelSeleccion.Enabled = Me.permitirCambiarNivelSeleccion
                If g_bOcultarSelCualquierMatPortal = False Then
                    Me.chkSelCualquierMatPortal.Top = Me.fraNivelSeleccion.Top + 500
                Else
                    Me.Height = 2000
                    Me.chkSelCualquierMatPortal.Top = Me.fraNivelSeleccion.Top + 100
                End If
            Else
                
                Me.fraNivelSeleccion.Visible = False
                Me.FraRefSolicitud1.Visible = False
                If g_bOcultarSelCualquierMatPortal = False Then
                    Me.Height = 2000
                    Me.chkSelCualquierMatPortal.Top = Me.FraRefSolicitud1.Top - 200
                Else
                    Me.Height = 1500
                    Me.chkSelCualquierMatPortal.Top = Me.FraRefSolicitud1.Top - 500
                End If
                
            End If
            
           
                    
        Case TipoCampoGS.EstadoHomologacion
            Me.caption = Me.caption & " " & g_sDenCampo
            fraLista.Visible = True
            fraLista.Top = lblLitTipo.Top + 500
            Me.Width = 6250
            Me.Height = 4800
            
            If g_sOrigen = "frmFormularios" Or g_sOrigen = "frmDesglose" Then
                Set oCampo = oFSGSRaiz.Generar_CFormCampo
                oCampo.Id = g_lCampoId
                oCampo.Tipo = g_iTipo
                oCampo.CargarValoresLista
            End If
            
            CrearGridLista
            CargarGridLista
        Case TipoCampoGS.AnyoImputacion
            Me.caption = Me.caption & " " & g_sDenCampo
            fraAnyoImputacion.Visible = True
            fraLista.Top = lblLitTipo.Top + 500
            Me.Width = 6250
            Me.Height = 1965
            Set oCampo = oFSGSRaiz.Generar_CFormCampo
            If oCampo.CampoEnUso(g_lCampoId) Then
                sdbcPartidas.Enabled = False
            End If
            
            oCampo.Id = g_lCampoId
            m_lIDPartidas = oCampo.CargarPartida()

            CargarPartidas
            If Not m_ListaPartidas Is Nothing Then
                sdbcPartidas.RemoveAll
                sdbcPartidas.AddItem 0 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                For j = 1 To m_ListaPartidas.Count
                    sdbcPartidas.AddItem m_ListaPartidas(j)
                Next
            
                sdbcPartidas.Value = m_lIDPartidas
                
            End If
        Case Else
            Me.caption = Me.caption & " " & g_sDenCampo
    End Select
    
    If g_iTipoCampo = TipoCampoGS.DenArticulo Then
        lblTipoCampo.caption = m_sIdiTipo(10)
    ElseIf g_iTipoCampo = TipoCampoGS.DesgloseActividad Then
        lblTipoCampo.caption = m_sIdiTipo(9)
    ElseIf g_iTipoCampo = TipoCampoGS.PeriodoRenovacion Or g_iTipoCampo = TipoCampoGS.EstadoHomologacion Then
        lblTipoCampo.caption = m_sIdiTipo(11)
    ElseIf g_iTipo = TiposDeAtributos.TipoEnlace Then
        lblTipoCampo.caption = m_sIdiTipo(12)
    Else
        lblTipoCampo.caption = m_sIdiTipo(g_iTipo)
    End If
    
    Select Case g_iTipoSolicit
        Case TipoCampoPredefinido.CampoGS
            If g_iTipoCampo < TipoCampoGS.Proveedor And Not (g_iTipoCampo = TipoCampoSC.TotalLineaAdj Or g_iTipoCampo = TipoCampoSC.TotalLineaPreadj) Then
                lblPredef.caption = m_sIdiTipoCampo(2) & " (" & g_sDenCampo & ")"
            Else
                lblPredef.caption = m_sIdiTipoCampo(1)
            End If
            
        Case TipoCampoPredefinido.Certificado
            lblPredef.caption = m_sIdiTipoCampo(3) & " (" & g_sDenCampo & ")"
            
        Case TipoCampoPredefinido.NoConformidad
            lblPredef.caption = m_sIdiTipoCampo(4) & " (" & g_sDenCampo & ")"
    End Select
    
    sdbcNivelSeleccion.Enabled = m_bpermitirCambiarNivelSeleccion
End Sub

Private Sub CrearGridLista()
    Dim i As Integer
    Dim oIdioma As CIdioma
        
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    
    Dim bLista As Boolean
    If Not g_oCampoPredef Is Nothing Then
        bLista = (g_oCampoPredef.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampoPredef.Tipo = TiposDeAtributos.TipoTextoMedio Or g_oCampoPredef.Tipo = TiposDeAtributos.TipoTextoLargo)
    Else
        bLista = (g_iTipo = TiposDeAtributos.TipoTextoCorto Or g_iTipo = TiposDeAtributos.TipoTextoMedio Or g_iTipo = TiposDeAtributos.TipoTextoLargo)
    End If
    
    If bLista Then
        With sdbgLista
            .Columns.RemoveAll
            
            i = 0
            For Each oIdioma In m_oIdiomas
                .Columns.Add i
                .Columns.Item(i).Name = oIdioma.Cod
                .Columns.Item(i).caption = oIdioma.Den
                .Columns.Item(i).Width = .Width / m_oIdiomas.Count - 100
                i = i + 1
            Next
                                    
            .Columns.Add i
            .Columns.Item(i).Name = "EXISTE"
            .Columns.Item(i).Visible = False
            i = i + 1
            .Columns.Add i
            .Columns.Item(i).Name = "ORDEN"
            .Columns.Item(i).Visible = False
            'Se a�ade una columna nueva para mantener el valor original de ORDEN (Esta columna s�lo tiene sentido cuando el campo est� marcado como Peso)
            i = i + 1
            .Columns.Add i
            .Columns.Item(i).Name = "ORDEN_ANT"
            .Columns.Item(i).Visible = False
            
            .AllowUpdate = True
        End With
    End If
End Sub

Private Sub CargarGridLista()
    Dim oValoresLista As CCampoValorListas
    Dim iTipo As TiposDeAtributos
    
    If Not g_oCampoPredef Is Nothing Then
        If g_oCampoPredef.ValoresLista Is Nothing Then g_oCampoPredef.CargarValoresLista
        Set oValoresLista = g_oCampoPredef.ValoresLista
        iTipo = g_oCampoPredef.Tipo
    Else
        Set oValoresLista = oCampo.ValoresLista
        iTipo = g_iTipo
    End If
        
    Dim oValor As CCampoValorLista
    For Each oValor In oValoresLista
        Select Case iTipo
            Case TiposDeAtributos.TipoFecha
                sdbgLista.AddItem oValor.valorFec
                
            Case TiposDeAtributos.TipoNumerico
                sdbgLista.AddItem oValor.valorNum
                
            Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                Dim sCadena As String
                Dim oIdioma As CIdioma
                
                sCadena = ""
                For Each oIdioma In m_oIdiomas
                    sCadena = sCadena & oValor.valorText.Item(CStr(oIdioma.Cod)).Den & Chr(m_lSeparador)
                Next
                sdbgLista.AddItem sCadena
        End Select
    Next
    Set oValor = Nothing
End Sub

Private Sub CargarRecursos()
    Dim i As Integer
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DETALLE_CAMPO_PREDEF, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value & ":"    '1 Detalle de campo
        Ador.MoveNext
        lblLitPredef.caption = Ador(0).Value '2 Predefinido
        Ador.MoveNext
        lblLitTipo.caption = Ador(0).Value  '3 Tipo
    
        For i = 2 To 9
            Ador.MoveNext
            m_sIdiTipo(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        m_sIdiTipoCampo(1) = Ador(0).Value & " (" & g_sDenCampo & ") " '12 Campo del sistema
        Ador.MoveNext
        m_sIdiTipoCampo(2) = Ador(0).Value '13 Campo de solicitudes de compras
        Ador.MoveNext
        m_sIdiTipoCampo(3) = Ador(0).Value '14 Campo de certificados
        Ador.MoveNext
        m_sIdiTipoCampo(4) = Ador(0).Value '15 Campo de no conformidad
        
        Ador.MoveNext
        chkAnyadirArticulo.caption = Ador(0).Value     '16 Permitir c�digos que no est�n en el maestro de art�culos
        
        Ador.MoveNext
        m_sIdiTipo(10) = Ador(0).Value   '17 Texto medio (200 car.) (Para la denominaci�n)
        
        Ador.MoveNext
        chkFechaValorDefecto.caption = Ador(0).Value '18 Introducir fechas concretas como valores por defecto
        Ador.MoveNext
        lblTipoSolicitud.caption = Ador(0).Value '19 Tipo de solicitud:
        Ador.MoveNext
        Me.optAvisoBloqueoImpAcum(0).caption = Ador(0).Value '20 Bloquear cuando el importe acumulado supere el de la solicitud
        Ador.MoveNext
        Me.optAvisoBloqueoImpAcum(1).caption = Ador(0).Value '21 Avisar cuando el importe acumulado supere el de la solicitud
        Ador.MoveNext
        m_sIdiCheckBox(1) = Ador(0).Value '22 Cargar con el �ltimo precio adjudicado
        Ador.MoveNext
        m_sIdiCheckBox(2) = Ador(0).Value '23 Cargar con el �ltimo proveedor adjudicado
        Ador.MoveNext
        chkVerUON.caption = Ador(0).Value '24 Ver estructura organizativa
        Ador.MoveNext
        lblCentroCoste.caption = Ador(0).Value & ":" '25 Centro de coste relacionado
        Ador.MoveNext
        sdbcCentroCoste.Columns(1).caption = Ador(0).Value '26 Grupo
        Ador.MoveNext
        sdbcCentroCoste.Columns(2).caption = Ador(0).Value '27 Campo desglose
        Ador.MoveNext
        sdbcCentroCoste.Columns(3).caption = Ador(0).Value '28 Campo
        Ador.MoveNext
        m_sNinguno = Ador(0).Value
        Ador.MoveNext
        lblNivelSeleccionObligatorio = Ador(0).Value
        Ador.MoveNext
        chkNoFecAntSis.caption = Ador(0).Value 'No permitir fecha anterior a la fecha del sistema
        Ador.MoveNext
        chkCargarSuminArt.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiTipo(11) = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value  '7 &Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '8 &Cancelar
        Ador.MoveNext
        chkSelCualquierMatPortal.caption = Ador(0).Value '36 Seleccionar cualquier material en el PORTAL
        Ador.MoveNext
        m_sIdiTipo(12) = Ador(0).Value
        Ador.MoveNext
        lblPartidas.caption = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing

End Sub
Private Sub chkFechaValorDefecto_Click()
    oCampo.FECHAVALORDEFECTO = Me.chkFechaValorDefecto.Value
    oCampo.ModificarCampoTipoFecha "FECHA_VALOR_DEFECTO", oCampo.FECHAVALORDEFECTO, False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim j As Long
    j = i Mod 2
    If j = 0 Then
        frmFormularios.sdbgCampos.Columns("VALOR").Value = ""
        oCampo.FECHAVALORDEFECTO = Me.chkFechaValorDefecto.Value
        oCampo.ModificarCampoTipoFecha "FECHA_VALOR_DEFECTO", oCampo.FECHAVALORDEFECTO, Not (g_sOrigen = "frmFormularios")
    End If
    If bNoFecAntSisModificado Then
        oCampo.NoFecAntSis = chkNoFecAntSis.Value
        oCampo.ModificarCampoTipoFecha "FECHA_VALOR_NO_ANT_SIS", oCampo.NoFecAntSis, Not (g_sOrigen = "frmFormularios")
    End If
    g_sDenCampo = ""
    g_iTipo = 0
    g_sOrigen = ""
    g_lCampoPadre = 0
    g_lCampoId = 0
    Set oCampo = Nothing
    Set m_ListaPartidas = Nothing
    Set m_ListaCentros = Nothing
    Set g_oCampoPredef = Nothing
End Sub

Private Sub sdbcCentroCoste_CloseUp()
    ModificarCentroCoste
End Sub

Private Sub sdbcCentroCoste_DropDown()
    Dim bmk As Variant
    If m_lIDCentroCoste <> 0 Then
        For i = 0 To sdbcCentroCoste.Rows - 1
            bmk = sdbcCentroCoste.GetBookmark(i)
            If sdbcCentroCoste.Columns(0).CellText(bmk) = m_lIDCentroCoste Then
                sdbcCentroCoste.Bookmark = bmk
                Exit For
            End If
        Next
    End If
End Sub

Private Sub sdbcCentroCoste_KeyUp(KeyCode As Integer, Shift As Integer)
    sdbcCentroCoste.Value = 0
    ModificarCentroCoste
End Sub

Private Sub ModificarCentroCoste()
    If IsNumeric(sdbcCentroCoste.Columns(0).Value) Then
        If CLng(sdbcCentroCoste.Columns(0).Value) <> m_lIDCentroCoste Then
            m_lIDCentroCoste = CLng(sdbcCentroCoste.Columns(0).Value)
            oCampo.CentroCoste = m_lIDCentroCoste
            oCampo.ModificarCentroCoste
        End If
    End If
End Sub

Private Sub ModificarPartidaPres()
    If IsNumeric(sdbcPartidas.Columns(0).Value) Then
        If CLng(sdbcPartidas.Columns(0).Value) <> m_lIDPartidas Then
            m_lIDPartidas = CLng(sdbcPartidas.Columns(0).Value)
            oCampo.PartidaPres = m_lIDPartidas
            oCampo.ModificarPartidaPres
        End If
    End If
End Sub

Private Sub sdbcNivelSeleccion_CloseUp()
    Me.sdbcNivelSeleccion.Value = Me.sdbcNivelSeleccion.Columns("VALUE").Value
    Me.sdbcNivelSeleccion.Text = Me.sdbcNivelSeleccion.Columns("DEN").Value
    If m_inivelSeleccion <> Me.sdbcNivelSeleccion.Value Then
        Me.haCambiadoNivelSeleccion = True
    End If
    m_inivelSeleccion = Me.sdbcNivelSeleccion.Value
End Sub

Private Sub sdbcPartidas_CloseUp()
    ModificarPartidaPres
End Sub

Private Sub sdbcPartidas_DropDown()
Dim bmk As Variant
    If m_lIDPartidas <> 0 Then
        For i = 0 To sdbcPartidas.Rows - 1
            bmk = sdbcPartidas.GetBookmark(i)
            If sdbcPartidas.Columns(0).CellText(bmk) = m_lIDPartidas Then
                sdbcPartidas.Bookmark = bmk
                Exit For
            End If
        Next
    End If
End Sub

Private Sub sdbcPartidas_KeyUp(KeyCode As Integer, Shift As Integer)
    sdbcPartidas.Value = 0
    ModificarPartidaPres
End Sub

Private Sub sdbcTipoSolicit_CloseUp()
    oCampo.IDRefSolicitud = sdbcTipoSolicit.Columns(2).Value
    oCampo.ModicarIDRefSolicitud
    m_lIdSolicitud = sdbcTipoSolicit.Columns(2).Value
End Sub

Private Sub sdbcTipoSolicit_DropDown()
    'Cargar la combo con todos los usuarios con acceso al EP
    Dim oSolic As CSolicitud
    Dim oSolicitudes As CSolicitudes
    
    Screen.MousePointer = vbHourglass
    
    sdbcTipoSolicit.RemoveAll
    
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    oSolicitudes.CargarTodasSolicitudes

    sdbcTipoSolicit.AddItem Null & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & 0

    For Each oSolic In oSolicitudes
        sdbcTipoSolicit.AddItem oSolic.Codigo & Chr(m_lSeparador) & NullToStr(oSolic.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & Chr(m_lSeparador) & oSolic.Id
    Next
        
    Set oSolicitudes = Nothing
    
    sdbcTipoSolicit.SelStart = 0
    sdbcTipoSolicit.SelLength = Len(sdbcTipoSolicit.Text)
    sdbcTipoSolicit.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcTipoSolicit_InitColumnProps()
    sdbcTipoSolicit.DataFieldList = "Column 1"
    sdbcTipoSolicit.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcTipoSolicit_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
        oCampo.IDRefSolicitud = 0
        oCampo.ModicarIDRefSolicitud
        sdbcTipoSolicit.Text = ""
    End If
End Sub

Private Sub sdbcTipoSolicit_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcTipoSolicit.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipoSolicit.Rows - 1
            bm = sdbcTipoSolicit.GetBookmark(i)
             If sdbcTipoSolicit.Columns(2).CellText(bm) = m_lIdSolicitud Then
                sdbcTipoSolicit.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub optAvisoBloqueoImpAcum_Click(Index As Integer)
    If Index = 0 Then
        oCampo.AvisoBloqueoImpAcum = BloqueoImp
    Else
        oCampo.AvisoBloqueoImpAcum = AvisoImp
    End If
    oCampo.ModificarAvisoBloqueo
End Sub

Private Sub ChkCargarUltADJ_Click()
    oCampo.CargarUltADJ = Me.chkCargarUltADJ.Value
    oCampo.ModificarCampoTipoBool "CARGAR_ULT_ADJ", oCampo.CargarUltADJ
    If oCampo.CargarUltADJ Then
        chkCargarSuminArt.Value = vbUnchecked
        chkCargarSuminArt_Click
    End If
End Sub

Private Sub CargarCentrosDeCoste()
    Dim g As CFormGrupo
    Dim c As CFormItem
    
    Set m_ListaCentros = New Collection
    
    If Not frmFormularios.g_oFormSeleccionado Is Nothing Then
        For Each g In frmFormularios.g_oFormSeleccionado.Grupos
            If g.Campos Is Nothing Then
                g.CargarTodosLosCampos
            End If
            
            If Not g.Campos Is Nothing Then
                For Each c In g.Campos
                    If c.CampoGS = TipoCampoGS.CentroCoste Then
                        m_ListaCentros.Add c.Id & Chr(m_lSeparador) & NullToStr(g.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(c.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
                    End If
                Next
            End If
        Next
    End If
    
    If g_sOrigen = "frmDesglose" Then
        Set g = frmFormularios.g_oGrupoSeleccionado
        For Each c In frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Desglose
            If c.CampoGS = TipoCampoGS.CentroCoste Then
                m_ListaCentros.Add c.Id & Chr(m_lSeparador) & NullToStr(g.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den) & Chr(m_lSeparador) & NullToStr(c.CampoPadre.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den) & Chr(m_lSeparador) & NullToStr(c.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
            End If
        Next
    End If
End Sub

Private Sub CargarPartidas()
    Dim g As CFormGrupo
    Dim c As CFormItem
    
    Set m_ListaPartidas = New Collection
    
    If Not frmFormularios.g_oGrupoSeleccionado Is Nothing Then
        
            If frmFormularios.g_oGrupoSeleccionado.Campos Is Nothing Then
                frmFormularios.g_oGrupoSeleccionado.CargarTodosLosCampos
            End If
            
            If Not frmFormularios.g_oGrupoSeleccionado.Campos Is Nothing Then
                For Each c In frmFormularios.g_oGrupoSeleccionado.Campos
                    If c.CampoGS = TipoCampoGS.PartidaPresupuestaria Then
                        m_ListaPartidas.Add c.Id & Chr(m_lSeparador) & NullToStr(c.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
                    End If
                Next
            End If
        
    End If
    
    If g_sOrigen = "frmDesglose" Then
        Set g = frmFormularios.g_oGrupoSeleccionado
        For Each c In frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Desglose
            If c.CampoGS = TipoCampoGS.PartidaPresupuestaria Then
                m_ListaPartidas.Add c.Id & Chr(m_lSeparador) & NullToStr(c.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
            End If
        Next
    End If
End Sub

Public Property Get nivelSeleccion() As Integer
    nivelSeleccion = m_inivelSeleccion
End Property

Public Property Let nivelSeleccion(Value As Integer)
    m_inivelSeleccion = Value
    Me.sdbcNivelSeleccion.Value = Value
End Property

Public Property Get permitirCambiarNivelSeleccion() As Boolean
    permitirCambiarNivelSeleccion = m_bpermitirCambiarNivelSeleccion
End Property

Public Property Let permitirCambiarNivelSeleccion(ByVal bpermitirCambiarNivelSeleccion As Boolean)
    m_bpermitirCambiarNivelSeleccion = bpermitirCambiarNivelSeleccion
    m_bhaCambiadoNivelSeleccion = False
End Property

Public Property Get haCambiadoNivelSeleccion() As Boolean
    haCambiadoNivelSeleccion = m_bhaCambiadoNivelSeleccion
End Property

Public Property Let haCambiadoNivelSeleccion(Value As Boolean)
    m_bhaCambiadoNivelSeleccion = Value
End Property

Private Sub sdbgLista_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgLista_BeforeUpdate(Cancel As Integer)
    Dim i As Integer
    Dim vbm As Variant
    Dim oIdioma As CIdioma
    
    If m_bRespetarLista Then Exit Sub
    
    With sdbgLista
        For i = 0 To .Rows - 1
            vbm = .AddItemBookmark(i)
            If CStr(vbm) <> CStr(.Bookmark) Then
                For Each oIdioma In m_oIdiomas
                    If .Columns(oIdioma.Cod).Value = "" Then
                        m_bError = True
                        oMensajes.NoValido .Columns(oIdioma.Cod).caption
                        If Me.Visible Then .SetFocus
                        Cancel = True
                        Exit Sub
                    End If
                Next
            End If
        Next i
    End With
End Sub

Private Sub sdbgLista_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        If sdbgLista.col = -1 Then KeyCode = 0
        Exit Sub
    End If
End Sub

Private Sub sdbgLista_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If Not sdbgLista.AllowUpdate Then Exit Sub
        
    cmdElimValor.Enabled = Not sdbgLista.IsAddRow
End Sub

Private Sub sdbgLista_RowLoaded(ByVal Bookmark As Variant)
    cmdElimValor.Enabled = sdbgLista.IsAddRow
End Sub

Private Sub sdbgLista_SelChange(ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
    cmdMvtoElemLista(0).Enabled = (sdbgLista.SelBookmarks.Count > 0)
    cmdMvtoElemLista(1).Enabled = (sdbgLista.SelBookmarks.Count > 0)
End Sub

