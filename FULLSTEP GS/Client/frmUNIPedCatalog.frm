VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmUNIPedCatalog 
   BackColor       =   &H00808000&
   Caption         =   "Unidades de pedido v�lidas para el cat�logo"
   ClientHeight    =   4590
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6330
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmUNIPedCatalog.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4590
   ScaleWidth      =   6330
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   6330
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   4035
      Width           =   6330
      Begin VB.CommandButton cmdAnyadir 
         Caption         =   "&A�adir"
         Height          =   345
         Left            =   60
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2130
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   5300
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   1100
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddUnidadesPedido 
      Height          =   1575
      Left            =   840
      TabIndex        =   4
      Top             =   1980
      Width           =   4890
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   1
      stylesets(0).Name=   "Actuales"
      stylesets(0).BackColor=   10944511
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmUNIPedCatalog.frx":014A
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1667
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   10
      Columns(1).Width=   4868
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      Columns(2).Width=   1720
      Columns(2).Caption=   "FC"
      Columns(2).Name =   "FC"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   5
      Columns(2).NumberFormat=   "Standard"
      Columns(2).FieldLen=   256
      _ExtentX        =   8625
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgUnidades 
      Height          =   2715
      Left            =   60
      TabIndex        =   3
      Top             =   1200
      Width           =   6270
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   6
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmUNIPedCatalog.frx":0166
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      BalloonHelp     =   0   'False
      MaxSelectedRows =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   2328
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "UP"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777152
      Columns(1).Width=   4419
      Columns(1).Caption=   "Denomimaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777152
      Columns(2).Width=   1746
      Columns(2).Caption=   "FC"
      Columns(2).Name =   "FC"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   5
      Columns(2).FieldLen=   256
      Columns(3).Width=   1561
      Columns(3).Caption=   "Cant. m�n."
      Columns(3).Name =   "CANTMIN"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "PROVE_ART_UP"
      Columns(4).Name =   "PROVE_ART_UP"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ID"
      Columns(5).Name =   "ID"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   11060
      _ExtentY        =   4789
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblUBValor 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1680
      TabIndex        =   7
      Top             =   780
      Width           =   1875
   End
   Begin VB.Label lblProveDen 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1680
      TabIndex        =   6
      Top             =   420
      Width           =   4515
   End
   Begin VB.Label lblArticuloDen 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1680
      TabIndex        =   5
      Top             =   60
      Width           =   4515
   End
   Begin VB.Label lblArticulo 
      BackColor       =   &H00808000&
      Caption         =   "Art�culo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label lblUB 
      BackColor       =   &H00808000&
      Caption         =   "Unidad de compra:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   1455
   End
   Begin VB.Label lblProve 
      BackColor       =   &H00808000&
      Caption         =   "Proveedor:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   1455
   End
End
Attribute VB_Name = "frmUNIPedCatalog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private oUnidadesPedido As CUnidadesPedido

'Idiomas
Private sIdiUnidad As String
Private sIdiUniDefecto As String
Private sIdiCod As String
Private sIdiDen As String
Private sIdiFC As String
Private sIdiCantMin As String
Private sIdiModos(1) As String

''' Control de errores
Private bModError As Boolean
Private bAnyaError As Boolean
Private bValError As Boolean
Private bAnyadir As Boolean

''' Variables de control
Public Accion As accionessummit
Private bModoEdicion As Boolean

Private bNueva As Boolean
Public Sub CargarComboUnidades()
Dim Adores As Ador.Recordset

    sdbddUnidadesPedido.RemoveAll
    
    Set Adores = frmCatalogo.oLineaCatalogoSeleccionada.DevolverUnidades
    If Not Adores Is Nothing Then
        While Not Adores.EOF
            sdbddUnidadesPedido.AddItem Adores("COD").Value & Chr(m_lSeparador) & Adores("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & Adores("FC").Value
            Adores.MoveNext
        Wend
        Adores.Close
        Set Adores = Nothing
    End If

    If sdbddUnidadesPedido.Rows = 0 Then
        sdbddUnidadesPedido.AddItem ""
    End If
    
End Sub

Public Sub CargarGridConUnidadesPedido()
Dim oUniPed As CUnidadPedido

    sdbgUnidades.RemoveAll
    
    sdbgUnidades.AllowAddNew = True
    
    frmCatalogo.oLineaCatalogoSeleccionada.CargarUnidadesDePedido False
    
    Set oUnidadesPedido = oFSGSRaiz.Generar_CUnidadesPedido
    Set oUnidadesPedido = frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido
    
    For Each oUniPed In oUnidadesPedido
        sdbgUnidades.AddItem oUniPed.UnidadPedido & Chr(m_lSeparador) & oUniPed.UPDen & Chr(m_lSeparador) & oUniPed.FactorConversion & Chr(m_lSeparador) & oUniPed.CantidadMinima & Chr(m_lSeparador) & IIf(oUniPed.FactorConversion <> 0, 1, 0) & Chr(m_lSeparador) & oUniPed.indice
    Next

    sdbgUnidades.AllowAddNew = False

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_UNIPEDCATALOG, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        lblArticulo.caption = Ador(0).Value
        Ador.MoveNext
        lblProve.caption = Ador(0).Value
        Ador.MoveNext
        lblUB.caption = Ador(0).Value
        Ador.MoveNext
        sdbgUnidades.Columns("UP").caption = Ador(0).Value
        sIdiCod = Ador(0).Value
        Ador.MoveNext
        sdbgUnidades.Columns("DEN").caption = Ador(0).Value
        sdbddUnidadesPedido.Columns("DEN").caption = Ador(0).Value
        sIdiDen = Ador(0).Value
        Ador.MoveNext
        sdbgUnidades.Columns("FC").caption = Ador(0).Value
        sdbddUnidadesPedido.Columns("FC").caption = Ador(0).Value
        sIdiFC = Ador(0).Value
        Ador.MoveNext
        sdbgUnidades.Columns("CANTMIN").caption = Ador(0).Value
        sIdiCantMin = Ador(0).Value
        Ador.MoveNext
        sdbddUnidadesPedido.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        cmdAnyadir.caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        cmdModoEdicion.caption = Ador(0).Value
        sIdiModos(0) = Ador(0).Value 'Edici�n
        Ador.MoveNext
        sIdiModos(1) = Ador(0).Value 'Consulta
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        sIdiUniDefecto = Ador(0).Value
        Ador.MoveNext
        sIdiUnidad = Ador(0).Value
        
        Ador.Close
        
    End If

    Set Ador = Nothing

End Sub



Private Sub cmdAnyadir_Click()
    
    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgUnidades.Scroll 0, sdbgUnidades.Rows - sdbgUnidades.Row

    If sdbgUnidades.VisibleRows > 0 Then

        If sdbgUnidades.VisibleRows >= sdbgUnidades.Rows Then
            sdbgUnidades.Row = sdbgUnidades.Rows
         Else
            sdbgUnidades.Row = sdbgUnidades.Rows - (sdbgUnidades.Rows - sdbgUnidades.VisibleRows) - 1
         End If

    End If

    bAnyadir = True

    sdbgUnidades.SetFocus
    
End Sub

Private Sub cmdDeshacer_Click()
    ''' * Objetivo: Deshacer la edicion en la Linea de catalogo actual
    
    sdbgUnidades.CancelUpdate
    sdbgUnidades.DataChanged = False
    bAnyadir = False

'    If Not sdbgUnidades.IsAddRow Then
'        cmdAnyadir.Enabled = True
'        cmdEliminar.Enabled = True
'        cmdDeshacer.Enabled = False
'    Else
'        cmdDeshacer.Enabled = False
'    End If

    cmdAnyadir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False


End Sub


Private Sub cmdEliminar_Click()
    ''' * Objetivo: Eliminar la Unidad de Pedido actual
    
    If sdbgUnidades.Rows = 0 Then Exit Sub
    sdbgUnidades.SelBookmarks.Add sdbgUnidades.Bookmark
    If sdbgUnidades.SelBookmarks.Count = 0 Then Exit Sub
    sdbgUnidades.DeleteSelected
    sdbgUnidades.SelBookmarks.RemoveAll

End Sub

Private Sub cmdModoEdicion_Click()
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant

    If Not bModoEdicion Then

        sdbgUnidades.AllowAddNew = True
        sdbgUnidades.AllowUpdate = True
        sdbgUnidades.AllowDelete = True

'        cmdModoEdicion.Caption = "&Consulta"
        cmdModoEdicion.caption = sIdiModos(1)

        sdbgUnidades.Columns("FC").Locked = False
        sdbgUnidades.Columns("CANTMIN").Locked = False
        
        cmdRestaurar.Visible = False
        cmdAnyadir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True

        bModoEdicion = True

        'inicializar las dropdown
        sdbddUnidadesPedido.AddItem ""

    Else

        If sdbgUnidades.DataChanged = True Then

            v = sdbgUnidades.ActiveCell.Value
            sdbgUnidades.SetFocus
            If (v <> "") Then
                sdbgUnidades.ActiveCell.Value = v
            End If
            
            bValError = False
            bAnyaError = False
            bModError = False

            sdbgUnidades.Update

            If bValError Or bAnyaError Or bModError Then
                Exit Sub
            End If

        End If

        sdbgUnidades.Columns("FC").Locked = True
        sdbgUnidades.Columns("CANTMIN").Locked = True
        
        sdbgUnidades.AllowAddNew = False
        sdbgUnidades.AllowUpdate = False
        sdbgUnidades.AllowDelete = False

        cmdAnyadir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdRestaurar.Visible = True

'        cmdModoEdicion.Caption = "&Edicion"
        cmdModoEdicion.caption = sIdiModos(0)

        cmdRestaurar_Click

        bModoEdicion = False

    End If

    sdbgUnidades.SetFocus

End Sub

Private Sub cmdRestaurar_Click()
    ''' * Objetivo: Restaurar el contenido de la grid
    ''' * Objetivo: desde la base de datos
    
    CargarGridConUnidadesPedido

End Sub

Private Sub Form_Load()
    Me.Top = 2000
    Me.Left = 2500
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    sdbgUnidades.Columns("UP").DropDownHwnd = sdbddUnidadesPedido.hwnd
    
'    Set oUnidadesPedido = oFSGSRaiz.Generar_CUnidadesPedido
'    Set oUnidadesPedido = frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido
    
'    CargarComboUnidades

    sdbgUnidades.Columns("FC").Locked = True
    sdbgUnidades.Columns("CANTMIN").Locked = True
    
    bModoEdicion = False

End Sub


Private Sub Form_Resize()

    On Error Resume Next
    
    sdbgUnidades.Width = Me.Width - 200
    sdbgUnidades.Height = Me.Height / 2 + 200
    sdbgUnidades.Columns("UP").Width = sdbgUnidades.Width * 0.2
    sdbgUnidades.Columns("DEN").Width = sdbgUnidades.Width * 0.4
    sdbgUnidades.Columns("FC").Width = sdbgUnidades.Width * 0.15
    If sdbgUnidades.Width > 8000 Then
        sdbgUnidades.Columns("CANTMIN").Width = sdbgUnidades.Width * 0.21
    Else
        sdbgUnidades.Columns("CANTMIN").Width = sdbgUnidades.Width * 0.16
    End If
    cmdModoEdicion.Left = sdbgUnidades.Width - cmdModoEdicion.Width

End Sub


Private Sub Form_Unload(Cancel As Integer)
    ''' * Objetivo: Descargar el formulario si no
    ''' * Objetivo: hay cambios pendientes
    
    Dim v As Variant
        
    If sdbgUnidades.DataChanged = True Then
    
        v = sdbgUnidades.ActiveCell.Value
        sdbgUnidades.SetFocus
        sdbgUnidades.ActiveCell.Value = v
        
        sdbgUnidades.Update
        
        bValError = False
        bAnyaError = False
        bModError = False
        
        If bValError Or bAnyaError Or bModError Then
            Cancel = True
            Exit Sub
        End If
    
    End If
    
    If frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Count > 0 Then
        If frmCatalogo.sdbgAdjudicaciones.Columns("HAYUNIPED").Value <> "1" Then
            frmCatalogo.g_bRefrescandoUniPed = True
            frmCatalogo.oLineaCatalogoSeleccionada.HayUnidadesPedido = True
            frmCatalogo.sdbgAdjudicaciones.Columns("HAYUNIPED").Value = "1"
            frmCatalogo.sdbgAdjudicaciones.Columns("OTRAS").CellStyleSet "HayUniPed"
            frmCatalogo.sdbgAdjudicaciones.Update
            frmCatalogo.g_bRefrescandoUniPed = False
        End If
    Else
        If frmCatalogo.sdbgAdjudicaciones.Columns("HAYUNIPED").Value <> "0" Then
            frmCatalogo.g_bRefrescandoUniPed = True
            frmCatalogo.oLineaCatalogoSeleccionada.HayUnidadesPedido = False
            frmCatalogo.sdbgAdjudicaciones.Columns("HAYUNIPED").Value = "0"
            frmCatalogo.sdbgAdjudicaciones.Columns("OTRAS").CellStyleSet "Normal"
            frmCatalogo.sdbgAdjudicaciones.Update
            frmCatalogo.g_bRefrescandoUniPed = False
        End If
    End If
    
    Set oUnidadesPedido = Nothing
    
End Sub

Private Sub sdbddUnidadesPedido_CloseUp()
    sdbgUnidades.Columns("UP").Value = sdbddUnidadesPedido.Columns("COD").Value
    sdbgUnidades.Columns("DEN").Value = sdbddUnidadesPedido.Columns("DEN").Value
    If sdbddUnidadesPedido.Columns("FC").Value <> "" Then
        sdbgUnidades.Columns("FC").Value = sdbddUnidadesPedido.Columns("FC").Value
        bNueva = False
    Else
        bNueva = True
    End If
    
End Sub

Private Sub sdbddUnidadesPedido_DropDown()
    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
        
    CargarComboUnidades
    
    sdbgUnidades.ActiveCell.SelStart = 0
    sdbgUnidades.ActiveCell.SelLength = Len(sdbgUnidades.ActiveCell.Text)

End Sub

Private Sub sdbddUnidadesPedido_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddUnidadesPedido.DataFieldList = "Column 0"
    sdbddUnidadesPedido.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbddUnidadesPedido_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddUnidadesPedido.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddUnidadesPedido.Rows - 1
            bm = sdbddUnidadesPedido.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddUnidadesPedido.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddUnidadesPedido.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


Private Sub sdbddUnidadesPedido_RowLoaded(ByVal Bookmark As Variant)
    If sdbddUnidadesPedido.Columns("FC").Value <> "" Then
        sdbddUnidadesPedido.Columns("COD").CellStyleSet "Actuales"
        sdbddUnidadesPedido.Columns("DEN").CellStyleSet "Actuales"
        sdbddUnidadesPedido.Columns("FC").CellStyleSet "Actuales"
    End If
End Sub


Private Sub sdbddUnidadesPedido_ValidateList(Text As String, RtnPassed As Integer)
Dim sCod As String

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddUnidadesPedido.Columns(0).Value = Text Then
            RtnPassed = True
            Exit Sub
        End If
        
'        oUnidades.CargarTodasLasUnidades Text, , True
'        If Not oUnidades.Item(Text) Is Nothing Then
'            RtnPassed = True
'        Else
'            sdbgUnidades.Columns("COD").Value = ""
'        End If
    
    End If

End Sub

Private Sub sdbgUnidades_AfterDelete(RtnDispErrMsg As Integer)
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    sdbgUnidades.SetFocus
    If IsEmpty(sdbgUnidades.RowBookmark(sdbgUnidades.Row)) Then
        sdbgUnidades.Bookmark = sdbgUnidades.RowBookmark(sdbgUnidades.Row - 1)
    Else
        sdbgUnidades.Bookmark = sdbgUnidades.RowBookmark(sdbgUnidades.Row)
    End If

End Sub

Private Sub sdbgUnidades_AfterInsert(RtnDispErrMsg As Integer)
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If Not bModoEdicion Then Exit Sub
    
    If bAnyaError = False Then
        cmdAnyadir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If

    bAnyadir = False

End Sub

Private Sub sdbgUnidades_AfterUpdate(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If Not bModoEdicion Then Exit Sub
    
    If bAnyaError = False And bModError = False Then
        cmdAnyadir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If
       
End Sub

Private Sub sdbgUnidades_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    ''' * Objetivo: Confirmacion antes de eliminar
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer

    DispPromptMsg = 0

    If sdbgUnidades.IsAddRow Then 'Or Accion = ACCArtadjMod Then
        Cancel = True
        Exit Sub
    End If

    irespuesta = basMensajes.PreguntaEliminar(sIdiUnidad & " " & sdbgUnidades.Columns("UP").Value)
    If irespuesta = vbNo Then Cancel = True

    If sdbgUnidades.Rows > 0 Then
        sdbgUnidades.SelBookmarks.Add sdbgUnidades.Bookmark
        '''Eliminar de Base de datos
        teserror = frmCatalogo.oLineaCatalogoSeleccionada.EliminarUnidadDePedido(sdbgUnidades.Columns("UP").Value, True)
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            sdbgUnidades.Refresh
            sdbgUnidades.SetFocus
            Exit Sub
        Else
            ''' Registro de acciones
             basSeguridad.RegistrarAccion accionessummit.ACCCatUniPedEli, "Cod:" & sdbgUnidades.Columns("UP").Value
            ''' Eliminar de la coleccion
            frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Remove (CStr(sdbgUnidades.Columns("ID").Value))
        End If
    End If

End Sub


Private Sub sdbgUnidades_BeforeInsert(Cancel As Integer)
    
    If Not bModoEdicion Then Exit Sub
        
    bAnyadir = True
    bAnyaError = False

End Sub




Private Sub sdbgUnidades_BeforeUpdate(Cancel As Integer)
Dim teserror As TipoErrorSummit
Dim v As Variant
    
    ''' * Objetivo: Validar los datos
    
    bValError = False
    bAnyaError = False
    bModError = False
    Cancel = False
    
    'No dejamos a�adir la unidad de pedido que tenga la l�nea de cat�logo
    If sdbgUnidades.Columns("UP").Value = frmCatalogo.oLineaCatalogoSeleccionada.UnidadPedido Then
        basMensajes.ImposibleAnyadir sIdiUnidad, sIdiUniDefecto
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgUnidades.Columns("UP").Value) = "" Then
        basMensajes.NoValido sIdiCod
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgUnidades.Columns("DEN").Value) = "" Then
        basMensajes.NoValido sIdiDen
        Cancel = True
        GoTo Salir
    End If
    
    If Not IsNumeric(sdbgUnidades.Columns("FC").Value) Or IsEmpty(sdbgUnidades.Columns("FC").Value) Then
        basMensajes.NoValido sIdiFC
        Cancel = True
        GoTo Salir
    End If
    
    If Not IsNumeric(sdbgUnidades.Columns("CANTMIN").Value) Then
        basMensajes.NoValido sIdiCantMin
        Cancel = True
        GoTo Salir
    End If

    If sdbgUnidades.Columns("PROVE_ART_UP").Value = 1 Then
        If Not IsNumeric(sdbgUnidades.Columns("CANTMIN").Value) Or IsEmpty(sdbgUnidades.Columns("CANTMIN").Value) Then
            basMensajes.CantMinObligatoria sdbgUnidades.Columns("UP").Value
            Cancel = True
            GoTo Salir
        End If
    End If
        

    'Diferenciamos si estamos a�adiendo o modificando
    
    If bAnyadir And sdbgUnidades.IsAddRow Then
    
        bAnyaError = False
        
        ''' Anyadir a la base de datos
        Screen.MousePointer = vbHourglass
        If sdbgUnidades.Columns("CANTMIN").Value = "" Then
            teserror = frmCatalogo.oLineaCatalogoSeleccionada.AnyadirUnidadDePedido(sdbgUnidades.Columns("UP").Value, sdbgUnidades.Columns("DEN").Value, CDbl(sdbgUnidades.Columns("FC").Value), StrToNull(sdbgUnidades.Columns("CANTMIN").Value), bNueva)
        Else
            teserror = frmCatalogo.oLineaCatalogoSeleccionada.AnyadirUnidadDePedido(sdbgUnidades.Columns("UP").Value, sdbgUnidades.Columns("DEN").Value, CDbl(sdbgUnidades.Columns("FC").Value), CDbl(sdbgUnidades.Columns("CANTMIN").Value), bNueva)
        End If
        Screen.MousePointer = vbNormal
        If teserror.NumError <> TESnoerror Then
            v = sdbgUnidades.ActiveCell.Value
            TratarError teserror
            cmdDeshacer_Click
            sdbgUnidades.SetFocus
            bAnyaError = True
            sdbgUnidades.ActiveCell.Value = v
            Exit Sub
        Else
        ''' Anyadir a la coleccion
            If frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Count > 0 Then
                If sdbgUnidades.Columns("CANTMIN").Value = "" Then
                    frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Add frmCatalogo.oLineaCatalogoSeleccionada.Id, frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Item(frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Count).indice + 1, frmCatalogo.oLineaCatalogoSeleccionada.ProveCod, , frmCatalogo.oLineaCatalogoSeleccionada.ArtCod_Interno, , frmCatalogo.oLineaCatalogoSeleccionada.ArtDen, frmCatalogo.oLineaCatalogoSeleccionada.UnidadCompra, , sdbgUnidades.Columns("UP").Value, sdbgUnidades.Columns("DEN").Value, CDbl(sdbgUnidades.Columns("FC").Value)
                Else
                    frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Add frmCatalogo.oLineaCatalogoSeleccionada.Id, frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Item(frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Count).indice + 1, frmCatalogo.oLineaCatalogoSeleccionada.ProveCod, , frmCatalogo.oLineaCatalogoSeleccionada.ArtCod_Interno, , frmCatalogo.oLineaCatalogoSeleccionada.ArtDen, frmCatalogo.oLineaCatalogoSeleccionada.UnidadCompra, , sdbgUnidades.Columns("UP").Value, sdbgUnidades.Columns("DEN").Value, CDbl(sdbgUnidades.Columns("FC").Value), CDbl(StrToDbl0(sdbgUnidades.Columns("CANTMIN").Value))
                End If
            Else
                If sdbgUnidades.Columns("CANTMIN").Value = "" Then
                    frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Add frmCatalogo.oLineaCatalogoSeleccionada.Id, 0, frmCatalogo.oLineaCatalogoSeleccionada.ProveCod, , frmCatalogo.oLineaCatalogoSeleccionada.ArtCod_Interno, , frmCatalogo.oLineaCatalogoSeleccionada.ArtDen, frmCatalogo.oLineaCatalogoSeleccionada.UnidadCompra, , sdbgUnidades.Columns("UP").Value, sdbgUnidades.Columns("DEN").Value, CDbl(sdbgUnidades.Columns("FC").Value)
                Else
                    frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Add frmCatalogo.oLineaCatalogoSeleccionada.Id, 0, frmCatalogo.oLineaCatalogoSeleccionada.ProveCod, , frmCatalogo.oLineaCatalogoSeleccionada.ArtCod_Interno, , frmCatalogo.oLineaCatalogoSeleccionada.ArtDen, frmCatalogo.oLineaCatalogoSeleccionada.UnidadCompra, , sdbgUnidades.Columns("UP").Value, sdbgUnidades.Columns("DEN").Value, CDbl(sdbgUnidades.Columns("FC").Value), CDbl(StrToDbl0(sdbgUnidades.Columns("CANTMIN").Value))
                End If
            End If
            sdbgUnidades.Columns("ID").Value = frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Item(frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Count).indice
            
            bAnyadir = False
            ''' Registro de acciones
            basSeguridad.RegistrarAccion accionessummit.ACCCatUniPedAnya, "Cod:" & sdbgUnidades.Columns("UP").Value
            Accion = ACCCatUniPedAnya
'            cmdRestaurar_Click
'            sdbgUnidades.SetFocus
        End If
        
    Else

        'Estamos modificando una l�nea
        bModError = False
            
        ''' Modificamos en la base de datos
        Screen.MousePointer = vbHourglass
        teserror = frmCatalogo.oLineaCatalogoSeleccionada.ModificarUnidadDePedido(sdbgUnidades.Columns("UP").Value, sdbgUnidades.Columns("FC").Value, sdbgUnidades.Columns("CANTMIN").Value, True)
        Screen.MousePointer = vbNormal
        If teserror.NumError <> TESnoerror Then
            v = sdbgUnidades.ActiveCell.Value
            TratarError teserror
            sdbgUnidades.SetFocus
            bModError = True
            sdbgUnidades.ActiveCell.Value = v
            Exit Sub
        Else
            frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Item(sdbgUnidades.Row + 1).FactorConversion = sdbgUnidades.Columns("FC").Value
    '        frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido.Item(sdbgUnidades.Row + 1).CantidadMinima = sdbgUnidades.Columns("CANTMIN").Value
            ''' Registro de acciones
            basSeguridad.RegistrarAccion accionessummit.ACCCatUniPedMod, "Cod:" & sdbgUnidades.Columns("UP").Value
            Accion = ACCCatUniPedMod
        End If

End If

Salir:
        
    bValError = Cancel
    bAnyaError = Cancel

End Sub


Private Sub sdbgUnidades_Change()
    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    
    If cmdDeshacer.Enabled = False Then
    
        cmdAnyadir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
        
    End If
    
    If Accion = ACCArtAdjCon And Not sdbgUnidades.IsAddRow Then
    
        Screen.MousePointer = vbHourglass
        
        teserror = frmCatalogo.oLineaCatalogoSeleccionada.AnyadirUnidadDePedido(sdbgUnidades.Columns("UP").Value, sdbgUnidades.Columns("DEN").Value, sdbgUnidades.Columns("FC").Value, CDbl(sdbgUnidades.Columns("CANTMIN").Value))
        
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            sdbgUnidades.DataChanged = False
            sdbgUnidades.SetFocus
            Exit Sub
'            Unload Me
        Else
'            Accion = ACCArtadjMod
        End If
            
    End If

End Sub






Private Sub sdbgUnidades_HeadClick(ByVal ColIndex As Integer)
    ''' * Objetivo: Ordenar el grid segun la columna
    Dim oUniPed As CUnidadPedido
    
    If bModoEdicion Then Exit Sub
    
    sdbgUnidades.RemoveAll
    
    sdbgUnidades.AllowAddNew = True
    
    Screen.MousePointer = vbHourglass
    frmCatalogo.oLineaCatalogoSeleccionada.CargarUnidadesDePedido False, ColIndex
    
    Set oUnidadesPedido = oFSGSRaiz.Generar_CUnidadesPedido
    Set oUnidadesPedido = frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido
    
    For Each oUniPed In oUnidadesPedido
        sdbgUnidades.AddItem oUniPed.UnidadPedido & Chr(m_lSeparador) & oUniPed.UPDen & Chr(m_lSeparador) & oUniPed.FactorConversion & Chr(m_lSeparador) & oUniPed.CantidadMinima & Chr(m_lSeparador) & IIf(oUniPed.FactorConversion <> 0, 1, 0)
    Next

    sdbgUnidades.AllowAddNew = False
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbgUnidades_KeyPress(KeyAscii As Integer)
    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgUnidades.DataChanged = False Then
            
            sdbgUnidades.CancelUpdate
            sdbgUnidades.DataChanged = False
            
            If Not sdbgUnidades.IsAddRow Then
                cmdAnyadir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
            Else
                cmdAnyadir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = False
            End If
            
'            Accion = ACCArtAdjCon
            
        Else
        
            If sdbgUnidades.IsAddRow Then
           
                cmdAnyadir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
           
'                Accion = ACCArtAdjCon
            
            End If
            
        End If
        
    End If

End Sub


Private Sub sdbgUnidades_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
'    If sdbgUnidades.col = 0 Then
'        sdbgUnidades.DroppedDown = True
'        sdbddUnidadesPedido.MoveFirst
'    End If

    If Not sdbgUnidades.IsAddRow Then
        If sdbgUnidades.DataChanged = False Then
            cmdAnyadir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
        End If
        sdbgUnidades.Columns("UP").Locked = True
        Me.sdbddUnidadesPedido.Enabled = False
        
    Else
        sdbgUnidades.Columns("UP").Locked = False
        Me.sdbddUnidadesPedido.Enabled = True
        If sdbgUnidades.DataChanged = True Then cmdDeshacer.Enabled = True
        If Not IsNull(LastRow) Then
            If CStr(LastRow) <> CStr(sdbgUnidades.Bookmark) Then
                sdbgUnidades.col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdAnyadir.Enabled = False
        cmdEliminar.Enabled = False
        
    End If

End Sub
