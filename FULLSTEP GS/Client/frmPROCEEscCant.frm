VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPROCENoEsc 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Para los items en los que tengamos que establecer cantidades"
   ClientHeight    =   5370
   ClientLeft      =   5985
   ClientTop       =   4515
   ClientWidth     =   5610
   Icon            =   "frmPROCEEscCant.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   5610
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   450
      Left            =   1433
      ScaleHeight     =   450
      ScaleWidth      =   2745
      TabIndex        =   7
      Top             =   4905
      Width           =   2745
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   9
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   8
         Top             =   60
         Width           =   1050
      End
   End
   Begin VB.PictureBox picCant 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   4860
      Left            =   45
      ScaleHeight     =   4860
      ScaleWidth      =   5520
      TabIndex        =   0
      Top             =   45
      Width           =   5520
      Begin VB.CheckBox chkPres 
         BackColor       =   &H00808000&
         Caption         =   "Sustituir por el precio de la �ltima adjudicaci�n si est� disponible"
         ForeColor       =   &H80000005&
         Height          =   400
         Left            =   315
         TabIndex        =   14
         Top             =   2115
         Width           =   4935
      End
      Begin VB.CheckBox chkPresPlanif 
         BackColor       =   &H00808000&
         Caption         =   "Sustituir por el presupuesto unitario planificado si est� disponible"
         ForeColor       =   &H00FFFFFF&
         Height          =   370
         Left            =   315
         TabIndex        =   13
         Top             =   2520
         Width           =   4980
      End
      Begin VB.TextBox txtPres 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1958
         TabIndex        =   10
         Top             =   1770
         Width           =   1695
      End
      Begin VB.CheckBox chkCant 
         BackColor       =   &H00808000&
         Caption         =   "Intoducir automaticamente la cantidad estimada para el articulo"
         ForeColor       =   &H80000005&
         Height          =   400
         Left            =   315
         TabIndex        =   6
         Top             =   810
         Width           =   5070
      End
      Begin VB.TextBox txtCant 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1958
         TabIndex        =   1
         Top             =   465
         Width           =   1695
      End
      Begin SSDataWidgets_B.SSDBGrid SSDBGridEscalados 
         Height          =   1245
         Left            =   585
         TabIndex        =   4
         Top             =   3420
         Width           =   4440
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   4
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmPROCEEscCant.frx":014A
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6403
         Columns(1).Caption=   "Cantidades directas"
         Columns(1).Name =   "DIR"
         Columns(1).Alignment=   1
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).NumberFormat=   "standard"
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Caption=   "Cantidad inicial"
         Columns(2).Name =   "INI"
         Columns(2).Alignment=   1
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).NumberFormat=   "standard"
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3200
         Columns(3).Caption=   "Cantidad final"
         Columns(3).Name =   "FIN"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "standard"
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         _ExtentX        =   7832
         _ExtentY        =   2196
         _StockProps     =   79
         ForeColor       =   0
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblPres 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Pres.Unitario:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   315
         TabIndex        =   12
         Top             =   1815
         Width           =   1485
      End
      Begin VB.Label lblSeleccionePresupuesto 
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione el presupuesto unitario para los elementos que no tengan:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   135
         TabIndex        =   11
         Top             =   1440
         Width           =   5220
      End
      Begin VB.Label lblEscalados 
         BackStyle       =   0  'Transparent
         Caption         =   "Escalados de precios que se dejan de usar:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   135
         TabIndex        =   5
         Top             =   3060
         Width           =   5220
      End
      Begin VB.Label lblSeleccioneCantidad 
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione la cantidad para los elementos que no tengan:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   135
         TabIndex        =   3
         Top             =   135
         Width           =   5220
      End
      Begin VB.Label lblCant 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Cantidad:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   510
         Width           =   1440
      End
   End
End
Attribute VB_Name = "frmPROCENoEsc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub chkPres_Click()
    If chkPres.Value = vbChecked Then
        Me.chkPresPlanif.Value = vbUnchecked
    End If

End Sub

Private Sub chkPresPlanif_Click()
    If chkPresPlanif.Value = vbChecked Then
        Me.chkPres.Value = vbUnchecked
    End If
End Sub

Private Sub cmdAceptar_Click()
    If ComprobarFormulario Then
        GuardarDatos ' Guarda los valores de los campos
        g_iRespuesta = 1 ' Indica que se continua con la operaci�n en frmproce
        Unload Me        ' al cerra la ventana modal
    End If
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    'Posicionar formulario
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    CargarRecursos 'Cargar textos
    PonerFieldSeparator Me
    If frmPROCE.g_oGrupoSeleccionado.TipoEscalados = ModoRangos Then
        ModoARangos
    Else
        ModoADirecto
    End If
    PrepararLista  'Cargar el Grid
   'Ajustar las columnas del grid
    Dim Scroll As Long
        Scroll = 200
    Dim Marcadores As Long
        Marcadores = 370
    If frmPROCE.g_oGrupoSeleccionado.TipoEscalados = ModoRangos Then
        Me.SSDBGridEscalados.Columns("INI").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.5
        Me.SSDBGridEscalados.Columns("FIN").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.5
    Else
        Me.SSDBGridEscalados.Columns("DIR").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores)
        
    End If
End Sub

'--<summary>
'--Realiza la comprobaci�n de los datos introducidos en le formulario
'--</summary>
'--<returns>True si los datos introducidos son correctos. False en caso contrario</returns>
'--<remarks>Llamada desde bot�n Aceptar</remarks>
'--<revision>DPD 12/08/11</revision>
Function ComprobarFormulario() As Boolean

    If frmPROCE.bHayQueReemplazarCantidades Then
        If Not IsNumeric(Me.txtCant.Text) Then
            basMensajes.valorCampoNoValido
            Me.txtCant.SelStart = 0
            Me.txtCant.SelLength = Len(Me.txtCant.Text)
            Me.txtCant.SetFocus
            ComprobarFormulario = False
            Exit Function
        End If
    End If
    If frmPROCE.bHayQueReemplazarImportes Then
        If Not IsNumeric(Me.txtPres.Text) Then
            basMensajes.valorCampoNoValido
            Me.txtPres.SelStart = 0
            Me.txtPres.SelLength = Len(Me.txtPres.Text)
            Me.txtPres.SetFocus
            ComprobarFormulario = False
            Exit Function
        End If
    End If
    ComprobarFormulario = True
End Function



Sub CargarRecursos()
Dim ador As ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCE_NOESC, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
    
        Me.caption = ador(0).Value 'Valores por defecto
        ador.MoveNext
        lblSeleccioneCantidad.caption = ador(0).Value 'Seleccione la cantidad para los elementos que no tengan:
        ador.MoveNext
        lblCant.caption = ador(0).Value    'Cantidad:
        ador.MoveNext
        chkCant.caption = ador(0).Value    'Intoducir automaticamente la cantidad estimada para el articulo
        ador.MoveNext
        lblSeleccionePresupuesto.caption = ador(0).Value     'Seleccione el presupuesto unitario para los elementos que no tengan:
        ador.MoveNext
        lblPres.caption = ador(0).Value     'pres.Unitario:
        ador.MoveNext
        chkPres.caption = ador(0).Value     'Sustituir por el precio de la �ltima adjudicaci�n si est� disponible
        ador.MoveNext
        chkPresPlanif.caption = ador(0).Value     'Sustituir por el presupuesto unitario planificado si est� disponible
        ador.MoveNext
        lblEscalados.caption = ador(0).Value     'Escalados de precios que se dejan de usar:
        ador.MoveNext
        Me.SSDBGridEscalados.Columns("DIR").caption = ador(0).Value     'Cantidades directas
        ador.MoveNext
        Me.SSDBGridEscalados.Columns("INI").caption = ador(0).Value     'Cantidad inicial
        ador.MoveNext
        Me.SSDBGridEscalados.Columns("FIN").caption = ador(0).Value     'Cantidad final
        ador.MoveNext
        cmdAceptar.caption = ador(0).Value  'Aceptar
        ador.MoveNext
        cmdCancelar.caption = ador(0).Value ' Cancelar
        ador.Close
        
    End If
    
    Set ador = Nothing
    







End Sub

'Almacena en variables de frmproce los datos introducidos en el formulario
Sub GuardarDatos()
    frmPROCE.m_vCantAAsignar = StrToDblOrNull(Me.txtCant.Text)
    frmPROCE.m_vPrecAAsignar = StrToDblOrNull(Me.txtPres.Text)
    frmPROCE.m_bCantAAsignar = Me.chkCant.Value
    frmPROCE.m_bPresAAsignar = Me.chkPres.Value
    frmPROCE.m_bPresPlanifAAsignar = Me.chkPresPlanif.Value
End Sub



''' <summary>
''' Inicializa la lista de Escalados
''' </summary>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo: 0,1</remarks>
Sub PrepararLista()
    If Not frmPROCE.g_oGrupoSeleccionado.Escalados Is Nothing Then
        Dim cEsc As CEscalado
        Dim i As Long
        Dim Linea As String
        For Each cEsc In frmPROCE.g_oGrupoSeleccionado.Escalados
            Linea = cEsc.Id & Chr(m_lSeparador)
            'Set cEsc = Me.g_oGrupoSeleccionado.Escalados.Item(i)
            Linea = Linea & cEsc.Inicial & Chr(m_lSeparador) & cEsc.Inicial & Chr(m_lSeparador)
            If frmPROCE.g_oGrupoSeleccionado.TipoEscalados = ModoRangos Then
                Linea = Linea & cEsc.final & Chr(m_lSeparador)
            Else
                Linea = Linea & "" & Chr(m_lSeparador)
            End If
            Linea = Linea & cEsc.Presupuesto
            Me.SSDBGridEscalados.AddItem Linea
        Next
    End If
End Sub

'Establece el modo de escalado en Rangos
Sub ModoARangos()
    SSDBGridEscalados.Columns("DIR").Visible = False
    SSDBGridEscalados.Columns("INI").Visible = True
    SSDBGridEscalados.Columns("FIN").Visible = True
    modo = ModoRangos
End Sub

'Establece el modo de escalado en Cantidades directas
Sub ModoADirecto()
    SSDBGridEscalados.Columns("DIR").Visible = True
    SSDBGridEscalados.Columns("INI").Visible = False
    SSDBGridEscalados.Columns("FIN").Visible = False
    modo = ModoDirecto

End Sub


