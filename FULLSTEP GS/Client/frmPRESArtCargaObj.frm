VERSION 5.00
Begin VB.Form frmPRESArtCargaObj 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DCargar objetivos"
   ClientHeight    =   3975
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5730
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPRESArtCargaObj.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3975
   ScaleWidth      =   5730
   StartUpPosition =   3  'Windows Default
   Begin VB.OptionButton optTraspasar 
      BackColor       =   &H00808000&
      Caption         =   "DTraspasar los valores de presupuestos actuales al campo de objetivo"
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   300
      TabIndex        =   0
      Top             =   80
      Value           =   -1  'True
      Width           =   4575
   End
   Begin VB.OptionButton optIntroducir 
      BackColor       =   &H00808000&
      Caption         =   "DIntroducir un valor específico"
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   300
      TabIndex        =   6
      Top             =   2450
      Width           =   4000
   End
   Begin VB.Frame fraValor 
      BackColor       =   &H00808000&
      Caption         =   "                                                                                    "
      Height          =   1000
      Left            =   120
      TabIndex        =   11
      Top             =   2500
      Width           =   5535
      Begin VB.TextBox txtValor 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   2480
         TabIndex        =   7
         Top             =   420
         Width           =   1215
      End
      Begin VB.Label lblValor 
         BackColor       =   &H00808000&
         Caption         =   "DValor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   215
         Left            =   1500
         TabIndex        =   12
         Top             =   480
         Width           =   900
      End
   End
   Begin VB.Frame fraTraspasar 
      BackColor       =   &H00808000&
      Caption         =   "                                                                                                           "
      Height          =   2295
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   5535
      Begin VB.PictureBox picOptions 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   855
         Left            =   2840
         ScaleHeight     =   855
         ScaleWidth      =   2535
         TabIndex        =   14
         Top             =   740
         Width           =   2535
         Begin VB.OptionButton optCant 
            BackColor       =   &H00808000&
            Caption         =   "DEn cantidad"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   0
            TabIndex        =   3
            Top             =   0
            Value           =   -1  'True
            Width           =   1695
         End
         Begin VB.OptionButton optPorcen 
            BackColor       =   &H00808000&
            Caption         =   "DEn porcentaje (0-100)"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   0
            TabIndex        =   4
            Top             =   460
            Width           =   2535
         End
      End
      Begin VB.TextBox txtValorTras 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2480
         TabIndex        =   5
         Top             =   1800
         Width           =   1215
      End
      Begin VB.OptionButton optModificar 
         BackColor       =   &H00808000&
         Caption         =   "DDecrementar"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   1
         Left            =   400
         TabIndex        =   2
         Top             =   1200
         Width           =   1335
      End
      Begin VB.OptionButton optModificar 
         BackColor       =   &H00808000&
         Caption         =   "DIncrementar"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   0
         Left            =   400
         TabIndex        =   1
         Top             =   740
         Value           =   -1  'True
         Width           =   1815
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FFFFFF&
         X1              =   2440
         X2              =   2440
         Y1              =   700
         Y2              =   1500
      End
      Begin VB.Label lblValorTras 
         BackColor       =   &H00808000&
         Caption         =   "DValor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   215
         Left            =   1500
         TabIndex        =   13
         Top             =   1860
         Width           =   855
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1700
      TabIndex        =   8
      Top             =   3600
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   2840
      TabIndex        =   9
      Top             =   3600
      Width           =   1005
   End
End
Attribute VB_Name = "frmPRESArtCargaObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables de idiomas:
Private m_stxtValor As String

Private Sub cmdAceptar_Click()
    Dim i As Integer
    Dim oRow As SSRow
    
    'Validación de los datos
    If optIntroducir.value = True Then
        If Trim(txtValor.Text) = "" Then
            oMensajes.FaltaValor
            If Me.Visible Then txtValor.SetFocus
            Exit Sub
        End If
        
        If Not IsNumeric(Trim(txtValor.Text)) Then
            oMensajes.NoValido m_stxtValor
            If Me.Visible Then txtValor.SetFocus
            Exit Sub
        End If
    Else
        If Trim(txtValorTras.Text) = "" Then
            oMensajes.FaltaValor
            If Me.Visible Then txtValorTras.SetFocus
            Exit Sub
        End If
        
        If Not IsNumeric(Trim(txtValorTras.Text)) Then
            oMensajes.NoValido m_stxtValor
            If Me.Visible Then txtValorTras.SetFocus
            Exit Sub
        End If
        
        If optPorcen.value = True And (StrToDbl0(Trim(txtValorTras.Text)) < 0 Or StrToDbl0(Trim(txtValorTras.Text)) > 100) Then
            oMensajes.NoValido m_stxtValor
            If Me.Visible Then txtValorTras.SetFocus
            Exit Sub
        End If
    End If
    
    'Actualización de la grid de presupuestos:
    Screen.MousePointer = vbHourglass
        
    For Each oRow In frmPRESArt.sdbgUltraPres.Selected.Rows
        
        If optIntroducir.value = True Then
            oRow.Cells("OBJV").value = Trim(txtValor.Text)
            
        ElseIf optModificar(0).value = True Then
            'incrementar
            If oRow.Cells("PRESV").value = "" Then
                oRow.Cells("OBJV").value = ""
            Else
                If optCant.value = True Then
                    'En cantidad
                    oRow.Cells("OBJV").value = StrToDbl0(oRow.Cells("PRESV").value) + StrToDbl0(Trim(txtValorTras.Text))
                Else
                    'En porcentaje
                    oRow.Cells("OBJV").value = StrToDbl0(oRow.Cells("PRESV").value) + (StrToDbl0(oRow.Cells("PRESV").value) * (StrToDbl0(Trim(txtValorTras.Text)) / 100))
                End If
            End If
            
        ElseIf optModificar(1).value = True Then
            'decrementar
            If oRow.Cells("PRESV").value = "" Then
                oRow.Cells("OBJV").value = ""
            Else
                If optCant.value = True Then
                    'En cantidad
                    oRow.Cells("OBJV").value = StrToDbl0(oRow.Cells("PRESV").value) - StrToDbl0(Trim(txtValorTras.Text))
                Else
                    'En porcentaje
                    oRow.Cells("OBJV").value = StrToDbl0(oRow.Cells("PRESV").value) - (StrToDbl0(oRow.Cells("PRESV").value) * (StrToDbl0(Trim(txtValorTras.Text)) / 100))
                End If
            End If
            
        End If
    Next
    
    frmPRESArt.sdbgUltraPres.Selected.ClearAll
    frmPRESArt.sdbgUltraPres.Update
    
    Screen.MousePointer = vbNormal
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    frmPRESArt.sdbgUltraPres.Selected.ClearAll
    Unload Me
End Sub

Private Sub Form_Load()
    CargarRecursos

End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next

    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESART_CARGAOBJ, basPublic.gParametrosInstalacion.gIdioma)

    If Not ador Is Nothing Then

        Me.caption = ador(0).value
        ador.MoveNext
        optTraspasar.caption = ador(0).value
        ador.MoveNext
        optModificar(0).caption = ador(0).value
        ador.MoveNext
        optModificar(1).caption = ador(0).value
        ador.MoveNext
        optCant.caption = ador(0).value
        ador.MoveNext
        optPorcen.caption = ador(0).value
        ador.MoveNext
        lblValorTras.caption = ador(0).value & ":"
        lblValor.caption = ador(0).value & ":"
        m_stxtValor = ador(0).value
        ador.MoveNext
        optIntroducir.caption = ador(0).value
        ador.MoveNext
        cmdAceptar.caption = ador(0).value
        ador.MoveNext
        cmdCancelar.caption = ador(0).value
        
        ador.Close

    End If

    Set ador = Nothing

End Sub


Private Sub optIntroducir_Click()
    If optIntroducir.value = True Then
        txtValor.Enabled = True
        txtValorTras.Text = ""
        txtValorTras.Enabled = False
        
        optTraspasar.value = False
        optModificar(0).Enabled = False
        optModificar(1).Enabled = False
        optCant.Enabled = False
        optPorcen.Enabled = False
    End If
End Sub

Private Sub optTraspasar_Click()
    
    If optTraspasar.value = True Then
        txtValorTras.Enabled = True
        txtValor.Text = ""
        txtValor.Enabled = False
        
        optIntroducir.value = False
        optModificar(0).Enabled = True
        optModificar(1).Enabled = True
        optCant.Enabled = True
        optPorcen.Enabled = True
    End If
    
End Sub
