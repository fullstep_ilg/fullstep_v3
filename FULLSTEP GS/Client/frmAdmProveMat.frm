VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAdmProveMat 
   Caption         =   "Asignaci�n de material"
   ClientHeight    =   4995
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5385
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAdmProveMat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4995
   ScaleWidth      =   5385
   Begin VB.PictureBox Picture2 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   120
      Picture         =   "frmAdmProveMat.frx":014A
      ScaleHeight     =   210
      ScaleWidth      =   195
      TabIndex        =   8
      Top             =   900
      Width           =   200
   End
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   120
      ScaleHeight     =   510
      ScaleWidth      =   5700
      TabIndex        =   5
      Top             =   4425
      Width           =   5700
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   345
         Left            =   2790
         TabIndex        =   7
         Top             =   105
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   345
         Left            =   1650
         TabIndex        =   6
         Top             =   105
         Width           =   1005
      End
   End
   Begin MSComctlLib.TreeView tvwEstrMatMod 
      Height          =   3615
      Left            =   60
      TabIndex        =   4
      Top             =   840
      Width           =   5300
      _ExtentX        =   9340
      _ExtentY        =   6376
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      Checkboxes      =   -1  'True
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraProveedor 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   5300
      Begin VB.TextBox txtProveDen 
         Height          =   285
         Left            =   2115
         TabIndex        =   3
         Top             =   240
         Width           =   3000
      End
      Begin VB.TextBox txtProveCod 
         Height          =   285
         Left            =   900
         TabIndex        =   2
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label lblProveedor 
         Caption         =   "Proveedor:"
         Height          =   255
         Left            =   60
         TabIndex        =   1
         Top             =   300
         Width           =   870
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAdmProveMat.frx":0344
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAdmProveMat.frx":0698
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAdmProveMat.frx":0AEC
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAdmProveMat.frx":0E40
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAdmProveMat.frx":1194
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAdmProveMat.frx":14E8
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAdmProveMat.frx":183C
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAdmProveMat.frx":1B90
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAdmProveMat.frx":1EE4
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwEstrMat 
      Height          =   3615
      Left            =   60
      TabIndex        =   9
      Top             =   840
      Visible         =   0   'False
      Width           =   5475
      _ExtentX        =   9657
      _ExtentY        =   6376
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      SingleSel       =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAdmProveMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variable para no ejecutar el node check si se ha disparado el mousedown
Private bMouseDown As Boolean
Public SelNode As MSComctlLib.Node
Private bload As Boolean
' Variables de seguridad
Private bREqp As Boolean
Private bRMatAsig As Boolean
Private bModif As Boolean
'variable para saber si hemos pulsado aceptar
Public bAceptar As Boolean
Public sOrigen As String

Public oGruposMN1 As CGruposMatNivel1
Public oGruposMN2 As CGruposMatNivel2
Public oGruposMN3 As CGruposMatNivel3
Public oGruposMN4 As CGruposMatNivel4

Private oGrupsMN1 As CGruposMatNivel1

Private oGruposMN1Seleccionados As CGruposMatNivel1
Private oGruposMN2Seleccionados As CGruposMatNivel2
Private oGruposMN3Seleccionados As CGruposMatNivel3
Private oGruposMN4Seleccionados As CGruposMatNivel4
Private oGruposMN1DesSeleccionados As CGruposMatNivel1
Private oGruposMN2DesSeleccionados As CGruposMatNivel2
Private oGruposMN3DesSeleccionados As CGruposMatNivel3
Private oGruposMN4DesSeleccionados As CGruposMatNivel4

Private oGrupsMN1Comp As CGruposMatNivel1
Private oGrupsMN2Comp As CGruposMatNivel2
Private oGrupsMN3Comp As CGruposMatNivel3
Private oGrupsMN4Comp As CGruposMatNivel4

Private oProves As CProveedores
Private oProveSeleccionado As CProveedor
Private oIMaterialAsignado As IMaterialAsignado

Private sIdioma() As String
Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.Node)

Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = True
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = True
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = True
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = True
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = True
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub
Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.Node)

Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = False
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = False
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = False
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = False
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = False
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub

Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.Node)
    
    Set nodx = nodx.Parent
    While Not nodx Is Nothing
        nodx.Checked = False
        Set nodx = nodx.Parent
    Wend
    DoEvents
End Sub

Private Sub CargarEstructuraMateriales()

    If Not oProveSeleccionado Is Nothing Then
        GenerarEstructuraDeMateriales
    End If

    Me.Show
    Set oGrupsMN1 = oFSGSRaiz.generar_CGruposMatNivel1
    oGrupsMN1.GenerarEstructuraMateriales False
    If (oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And bRMatAsig) Then
        GenerarEstructuraMatAsignable
    Else
        GenerarEstructuraDeMaterialesCompleta
    End If
        
    If tvwEstrMat.Nodes.Count = 0 Then Exit Sub

    On Error Resume Next
    If Me.Visible Then tvwEstrMat.SetFocus
    On Error GoTo 0

End Sub

Private Sub GenerarEstructuraDeMateriales()

Dim oGMN1 As CGrupoMatNivel1
Dim nodx As MSComctlLib.Node
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String


If oProveSeleccionado Is Nothing Then Exit Sub

tvwEstrMat.Nodes.clear
Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", "Materiales", "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True

On Error GoTo Error

Set oIMaterialAsignado = oProveSeleccionado

Set oGrupsMN1 = oFSGSRaiz.generar_CGruposMatNivel1
oGrupsMN1.GenerarEstructuraMateriales False


With oIMaterialAsignado


'************** GruposMN1 ***********
Set oGruposMN1 = .DevolverGruposMN1Asignados(, , , , False)
    
        For Each oGMN1 In oGruposMN1
    
            scod1 = oGMN1.Cod & Mid$("                         ", 1, 20 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1A")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod) ' Con la X1X indicamos que el GMN1 ha sido asignado.
            
        Next
        
      
    
    '************** GruposMN2 ***********
    Set oGruposMN2 = .DevolverGruposMN2Asignados(, , , , False)
    
        For Each oGMN2 In oGruposMN2
            scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, 20 - Len(oGMN2.GMN1Cod))
            scod2 = oGMN2.Cod & Mid$("                         ", 1, 20 - Len(oGMN2.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN2.GMN1Cod) & " - " & oGMN2.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN2.GMN1Cod)
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2A")
            nodx.Tag = "GMN2" & CStr(oGMN2.Cod) ' Con la X1X indicamos que la GMN2 ha sido asignada directamente.
            If nodx.Parent.Image = "GMN1" Then
                nodx.Parent.Expanded = True
            End If
        Next
    
    
    '************** GruposMN3 ***********
    Set oGruposMN3 = .DevolverGruposMN3Asignados(, , , , False)
    
    If Not oGruposMN3 Is Nothing Then
        For Each oGMN3 In oGruposMN3
            scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, 20 - Len(oGMN3.GMN1Cod))
            scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, 20 - Len(oGMN3.GMN2Cod))
            scod3 = oGMN3.Cod & Mid$("                         ", 1, 20 - Len(oGMN3.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN3.GMN1Cod) & " - " & oGMN3.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN3.GMN1Cod)  ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN3.GMN2Cod) & " - " & oGMN3.GMN2Den, "GMN2")
            nodx.Tag = "GMN2" & CStr(oGMN3.GMN2Cod)   ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3A")
            nodx.Tag = "GMN3" & CStr(oGMN3.Cod) ' Con la X1X indicamos que la GMN3 ha sido asignada directamente.
            If nodx.Parent.Image = "GMN2" Then
                nodx.Parent.Parent.Expanded = True
                nodx.Parent.Expanded = True
            End If
        Next
    
    End If
    
    
    '************** GruposMN4 ***********
    Set oGruposMN4 = .DevolverGruposMN4Asignados(, , , , False)
    
    If Not oGruposMN4 Is Nothing Then
        
        For Each oGMN4 In oGruposMN4

            scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, 20 - Len(oGMN4.GMN1Cod))
            scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, 20 - Len(oGMN4.GMN2Cod))
            scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, 20 - Len(oGMN4.GMN3Cod))
            scod4 = oGMN4.Cod & Mid$("                         ", 1, 20 - Len(oGMN4.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN4.GMN1Cod) & " - " & oGMN4.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN4.GMN1Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN4.GMN2Cod) & " - " & oGMN4.GMN2Den, "GMN2")
            nodx.Tag = "GMN2" & CStr(oGMN4.GMN2Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN4.GMN3Cod) & " - " & oGMN4.GMN3Den, "GMN3")
            nodx.Tag = "GMN3" & CStr(oGMN4.GMN3Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4A")
            nodx.Tag = "GMN4" & CStr(oGMN4.Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            
            If (nodx.Parent.Image) = "GMN3" Then
                nodx.Parent.Parent.Parent.Expanded = True
                nodx.Parent.Parent.Expanded = True
                nodx.Parent.Expanded = True
            End If
        
        Next
    
    End If
 
 End With
 
Exit Sub

Error:
    Set nodx = Nothing
    Resume Next
End Sub



Private Sub GenerarEstructuraDeMaterialesCompleta()
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Dim oGrupsMN1 As CGruposMatNivel1
Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4

Dim nodx As MSComctlLib.Node

Set oIMaterialAsignado = oProveSeleccionado

tvwEstrMatMod.Nodes.clear

Set nodx = tvwEstrMatMod.Nodes.Add(, , "Raiz", sIdioma(1), "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True
    
    Set oGrupsMN1 = oFSGSRaiz.generar_CGruposMatNivel1
    oGrupsMN1.GenerarEstructuraMateriales False
        
    Screen.MousePointer = vbHourglass

        
        Select Case gParametrosGenerales.giNEM
        
        Case 1
                
                For Each oGMN1 In oGrupsMN1
                    scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                    Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                Next
        
        Case 2
            
            For Each oGMN1 In oGrupsMN1
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                nodx.Tag = "GMN1" & oGMN1.Cod
                
                For Each oGMN2 In oGMN1.GruposMatNivel2
                    scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                    Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                    nodx.Tag = "GMN2" & oGMN2.Cod
                Next
            Next
        
        Case 3
            
            For Each oGMN1 In oGrupsMN1
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                nodx.Tag = "GMN1" & oGMN1.Cod
                
                For Each oGMN2 In oGMN1.GruposMatNivel2
                    scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                    Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                    nodx.Tag = "GMN2" & oGMN2.Cod
                        
                        For Each oGMN3 In oGMN2.GruposMatNivel3
                            scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                            Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                            nodx.Tag = "GMN3" & oGMN3.Cod
                          
                        Next
                Next
        Next
            
            
        Case 4
            
            For Each oGMN1 In oGrupsMN1
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
                
                For Each oGMN2 In oGMN1.GruposMatNivel2
                    scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                    Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                    nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
                        
                    For Each oGMN3 In oGMN2.GruposMatNivel3
                        scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                        Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                        nodx.Tag = "GMN3" & CStr(oGMN3.Cod)
                                    
                        For Each oGMN4 In oGMN3.GruposMatNivel4
                            scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                            Set nodx = tvwEstrMatMod.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                            nodx.Tag = "GMN4" & CStr(oGMN4.Cod)
                        Next
                    
                    Next
                
                Next
        
        Next
            
    End Select
    
    nodx.Root.Selected = True

    Me.Show
    
    '************** GruposMN1 ***********
    Set oGruposMN1 = oIMaterialAsignado.DevolverGruposMN1Asignados(, , , , False)
    If Not oGruposMN1 Is Nothing Then
        For Each oGMN1 In oGruposMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Item("GMN1" & scod1)
            nodx.Checked = True
            tvwEstrMatMod.Nodes.Item("GMN1" & scod1).Checked = True
        Next
    End If

    If gParametrosGenerales.giNEM <= 1 Then
        Exit Sub
    End If


    '************** GruposMN2 ***********
    Set oGruposMN2 = oIMaterialAsignado.DevolverGruposMN2Asignados(, , , , False)

    If Not oGruposMN2 Is Nothing Then
        For Each oGMN2 In oGruposMN2
            scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
            scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
            tvwEstrMatMod.Nodes.Item("GMN2" & scod1 & scod2).Checked = True

            If Not tvwEstrMatMod.Nodes.Item("GMN1" & scod1).Checked Then
                tvwEstrMatMod.Nodes.Item("GMN1" & scod1).Expanded = True
            End If
        Next
    End If

    If gParametrosGenerales.giNEM <= 2 Then
        Exit Sub
    End If


    '************** GruposMN3 ***********
    Set oGruposMN3 = oIMaterialAsignado.DevolverGruposMN3Asignados(, , , , False)

     If Not oGruposMN3 Is Nothing Then
        For Each oGMN3 In oGruposMN3
            scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
            scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
            scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
            tvwEstrMatMod.Nodes.Item("GMN3" & scod1 & scod2 & scod3).Checked = True

            If Not tvwEstrMatMod.Nodes.Item("GMN2" & scod1 & scod2).Checked Then
                tvwEstrMatMod.Nodes.Item("GMN2" & scod1 & scod2).Expanded = True
                tvwEstrMatMod.Nodes.Item("GMN1" & scod1).Expanded = True
            End If
        Next
    End If

    If gParametrosGenerales.giNEM <= 3 Then
        Exit Sub
    End If

    '************** GruposMN4 ***********
    Set oGruposMN4 = oIMaterialAsignado.DevolverGruposMN4Asignados(, , , , False)

    If Not oGruposMN4 Is Nothing Then
        For Each oGMN4 In oGruposMN4
            scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
            scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
            scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
            scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
            tvwEstrMatMod.Nodes.Item("GMN4" & scod1 & scod2 & scod3 & scod4).Checked = True

            If Not tvwEstrMatMod.Nodes.Item("GMN3" & scod1 & scod2 & scod3).Checked Then
                tvwEstrMatMod.Nodes.Item("GMN3" & scod1 & scod2 & scod3).Expanded = True
                tvwEstrMatMod.Nodes.Item("GMN2" & scod1 & scod2).Expanded = True
                tvwEstrMatMod.Nodes.Item("GMN1" & scod1).Expanded = True
            End If
        Next
    End If

    Screen.MousePointer = vbNormal

    Set oGrupsMN1 = Nothing

Exit Sub

Error:
    Set nodx = Nothing
    Resume Next

End Sub

Private Sub GenerarEstructuraMatAsignable()
Dim oIMaterialProve As IMaterialAsignado

Dim oGrupsMN1 As CGruposMatNivel1
Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim nodx As MSComctlLib.Node
Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

On Error GoTo Error

    tvwEstrMatMod.Nodes.clear

    Set nodx = tvwEstrMatMod.Nodes.Add(, , "Raiz", sIdioma(1), "Raiz")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
    ' Carga del material asignado al Prove
    Set oIMaterialProve = oUsuarioSummit.comprador
   
    With oIMaterialProve

        '************** GruposMN1 ***********
        'Set oGruposMN1 = .DevolverGruposMN1Asignados(, , , , False)
        Set oGrupsMN1Comp = .DevolverGruposMN1Asignados(, , , , False)
        
'        For Each oGMN1 In oGruposMN1
        For Each oGMN1 In oGrupsMN1Comp
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod) ' Con la X1X indicamos que el GMN1 ha sido asignado.
        Next
        
        If gParametrosGenerales.giNEM <= 1 Then
            Exit Sub
        End If
    
    
        '************** GruposMN2 ***********
'        Set oGruposMN2 = .DevolverGruposMN2Asignados(, , , , False)
        Set oGrupsMN2Comp = .DevolverGruposMN2Asignados(, , , , False)
         
'        For Each oGMN2 In oGruposMN2
        For Each oGMN2 In oGrupsMN2Comp
            scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
            scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN2.GMN1Cod) & " - " & oGMN2.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN2.GMN1Cod)
            Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
            nodx.Tag = "GMN2" & CStr(oGMN2.Cod) ' Con la X1X indicamos que la GMN2 ha sido asignada directamente.
        Next

        If gParametrosGenerales.giNEM <= 2 Then
            Exit Sub
        End If
        
    
        '************** GruposMN3 ***********
'        Set oGruposMN3 = oIMaterialProve.DevolverGruposMN3Asignados(, , , , False)
'        If Not oGruposMN3 Is Nothing Then
             
        Set oGrupsMN3Comp = oIMaterialProve.DevolverGruposMN3Asignados(, , , , False)
        If Not oGrupsMN3Comp Is Nothing Then
             
'             For Each oGMN3 In oGruposMN3
             For Each oGMN3 In oGrupsMN3Comp
                scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
                scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
                scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN3.GMN1Cod) & " - " & oGMN3.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & CStr(oGMN3.GMN1Cod)  ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN3.GMN2Cod) & " - " & oGMN3.GMN2Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN3.GMN2Cod)   ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
                nodx.Tag = "GMN3" & CStr(oGMN3.Cod) ' Con la X1X indicamos que la GMN3 ha sido asignada directamente.
            Next
    
        End If

        If gParametrosGenerales.giNEM <= 3 Then
            Exit Sub
        End If
    
        '************** GruposMN4 ***********
'        Set oGruposMN4 = oIMaterialProve.DevolverGruposMN4Asignados(, , , , False)
'        If Not oGruposMN4 Is Nothing Then
'
'            For Each oGMN4 In oGruposMN4
        Set oGrupsMN4Comp = oIMaterialProve.DevolverGruposMN4Asignados(, , , , False)
        If Not oGrupsMN4Comp Is Nothing Then

            For Each oGMN4 In oGrupsMN4Comp
                
                scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
                scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
                scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                
                Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN4.GMN1Cod) & " - " & oGMN4.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & CStr(oGMN4.GMN1Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN4.GMN2Cod) & " - " & oGMN4.GMN2Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN4.GMN2Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN4.GMN3Cod) & " - " & oGMN4.GMN3Den, "GMN3")
                nodx.Tag = "GMN3" & CStr(oGMN4.GMN3Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4")
                nodx.Tag = "GMN4" & CStr(oGMN4.Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            Next
           
        End If
        
    End With
     
    nodx.Root.Selected = True
    
    Me.Show
    
    Set oIMaterialAsignado = oProveSeleccionado
    
    '************** GruposMN1 ***********
    Set oGruposMN1 = oIMaterialAsignado.DevolverGruposMN1Asignados(, , , , False)
    If Not oGruposMN1 Is Nothing Then
        For Each oGMN1 In oGruposMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Item("GMN1" & scod1)
            nodx.Checked = True
            tvwEstrMatMod.Nodes.Item("GMN1" & scod1).Checked = True
        Next
    End If
    
    If gParametrosGenerales.giNEM <= 1 Then
        Exit Sub
    End If
    
    
    '************** GruposMN2 ***********
    Set oGruposMN2 = oIMaterialAsignado.DevolverGruposMN2Asignados(, , , , False)
    
    If Not oGruposMN2 Is Nothing Then
        For Each oGMN2 In oGruposMN2
            scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
            scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
            tvwEstrMatMod.Nodes.Item("GMN2" & scod1 & scod2).Checked = True
            
            If Not tvwEstrMatMod.Nodes.Item("GMN1" & scod1).Checked Then
                tvwEstrMatMod.Nodes.Item("GMN1" & scod1).Expanded = True
            End If
        Next
    End If
    
    If gParametrosGenerales.giNEM <= 2 Then
        Exit Sub
    End If
    
    
    '************** GruposMN3 ***********
    Set oGruposMN3 = oIMaterialAsignado.DevolverGruposMN3Asignados(, , , , False)
    
     If Not oGruposMN3 Is Nothing Then
        For Each oGMN3 In oGruposMN3
            scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
            scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
            scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
            tvwEstrMatMod.Nodes.Item("GMN3" & scod1 & scod2 & scod3).Checked = True
            
            If Not tvwEstrMatMod.Nodes.Item("GMN2" & scod1 & scod2).Checked Then
                tvwEstrMatMod.Nodes.Item("GMN2" & scod1 & scod2).Expanded = True
                tvwEstrMatMod.Nodes.Item("GMN1" & scod1).Expanded = True
            End If
        Next
    End If
    
    If gParametrosGenerales.giNEM <= 3 Then
        Exit Sub
    End If
    
    '************** GruposMN4 ***********
    Set oGruposMN4 = oIMaterialAsignado.DevolverGruposMN4Asignados(, , , , False)
    
    If Not oGruposMN4 Is Nothing Then
        For Each oGMN4 In oGruposMN4
            scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
            scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
            scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
            scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
            tvwEstrMatMod.Nodes.Item("GMN4" & scod1 & scod2 & scod3 & scod4).Checked = True
            
            If Not tvwEstrMatMod.Nodes.Item("GMN3" & scod1 & scod2 & scod3).Checked Then
                tvwEstrMatMod.Nodes.Item("GMN3" & scod1 & scod2 & scod3).Expanded = True
                tvwEstrMatMod.Nodes.Item("GMN2" & scod1 & scod2).Expanded = True
                tvwEstrMatMod.Nodes.Item("GMN1" & scod1).Expanded = True
            End If
        Next
    End If

'    For Each nodx In tvwEstrMatMod.Nodes
'        If Right(tvwEstrMat.Nodes(nodx.key).Image, 1) = "A" Then
'            nodx.Checked = True
'            If nodx.Visible Then
'                nodx.EnsureVisible
'            End If
'            DoEvents
'        End If
'    Next

    Set oGrupsMN1 = Nothing

    Exit Sub

Error:
    Set nodx = Nothing
    Resume Next
    
End Sub

Private Sub cmdAceptar_Click()
Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node
Dim Node As MSComctlLib.Node
Dim teserror As TipoErrorSummit
Dim oIMaterial As IMaterialAsignado
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim sCod As String
Dim i As Integer

    teserror.NumError = TESnoerror

    'Recorremos todo el �rbol por si despu�s de checkear nos hemos movido
    For i = 1 To tvwEstrMatMod.Nodes.Count
        If tvwEstrMatMod.Nodes(i).Checked Then
            Set Node = tvwEstrMatMod.Nodes(i)
            Exit For
        End If
    Next
        
    If ((oGruposMN1.Count = 0) And (oGruposMN2.Count = 0) And (oGruposMN3.Count = 0) And (oGruposMN4.Count = 0) And (gParametrosGenerales.gbOblProveMat) And (Node Is Nothing) And (bModif)) Then
    'El proveedor no ten�a material asignado y la asignaci�n es obligatoria
        oMensajes.AsignacionMatProveObligatoria
        Exit Sub
     End If
        
    Screen.MousePointer = vbHourglass
       
    Set Node = tvwEstrMatMod.Nodes(1)
    If Node.Children = 0 Then
         Screen.MousePointer = vbNormal
         Exit Sub
    End If
            
    Set nod1 = Node.Child
    
    While Not nod1 Is Nothing
        scod1 = DevolverCod(nod1) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(nod1)))
        ' No asignado
        If Not nod1.Checked Then
            If Not oGruposMN1.Item(scod1) Is Nothing Then
                ' Antes si
                oGruposMN1DesSeleccionados.Add DevolverCod(nod1), ""
            End If
                
            Set nod2 = nod1.Child
                
            While Not nod2 Is Nothing
                scod2 = DevolverCod(nod2) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(nod2)))
                ' No asignado
                If Not nod2.Checked Then
                    If Not oGruposMN2.Item(scod1 & scod2) Is Nothing Then
                        ' Antes si
                        oGruposMN2DesSeleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                    End If
            
                    Set nod3 = nod2.Child
                    While Not nod3 Is Nothing
                        scod3 = DevolverCod(nod3) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(nod3)))
                        If Not nod3.Checked Then
                            If Not oGruposMN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                ' Antes si
                                oGruposMN3DesSeleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                            End If
                            
                            Set nod4 = nod3.Child
                            While Not nod4 Is Nothing
                                scod4 = DevolverCod(nod4) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(DevolverCod(nod4)))
                                If Not nod4.Checked Then
                                    If Not oGruposMN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                    ' Antes si
                                    oGruposMN4DesSeleccionados.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""
                                    End If
                                Else
                                    If oGruposMN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                        oGruposMN4Seleccionados.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""
                                    End If
                                End If
                                    
                                Set nod4 = nod4.Next
                            Wend
                        Else
                            ' Si esta checked GMN3
                            If oGruposMN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                'No estaba asignado antes.
                                oGruposMN3Seleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                            End If
                        End If
                        
                        Set nod3 = nod3.Next
                    Wend
                Else
                    ' Si esta checked GMN2
                    If oGruposMN2.Item(scod1 & scod2) Is Nothing Then
                        'No estaba asignado antes.
                        oGruposMN2Seleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                    End If
                End If
                    
                Set nod2 = nod2.Next
            Wend
                    
        Else
        ' Si esta checked GMN1
            If oGruposMN1.Item(scod1) Is Nothing Then
                'No estaba asignado antes.
                oGruposMN1Seleccionados.Add DevolverCod(nod1), ""
            End If
        End If
        Set nod1 = nod1.Next
    Wend
    
    If oGruposMN1DesSeleccionados.Count = 0 And oGruposMN1Seleccionados.Count = 0 And oGruposMN2DesSeleccionados.Count = 0 And oGruposMN2Seleccionados.Count = 0 And oGruposMN3DesSeleccionados.Count = 0 And oGruposMN3Seleccionados.Count = 0 And oGruposMN4DesSeleccionados.Count = 0 And oGruposMN4Seleccionados.Count = 0 Then
        'hab�a una seleccci�n anterior y no ha cambiado
        Set oGruposMN1Seleccionados = Nothing
        Set oGruposMN2Seleccionados = Nothing
        Set oGruposMN3Seleccionados = Nothing
        Set oGruposMN4Seleccionados = Nothing
        Set oGruposMN1DesSeleccionados = Nothing
        Set oGruposMN2DesSeleccionados = Nothing
        Set oGruposMN3DesSeleccionados = Nothing
        Set oGruposMN4DesSeleccionados = Nothing
        Screen.MousePointer = vbNormal
        Unload Me
        Exit Sub
    Else
        'hab�a una seleccci�n anterior y s� ha cambiado pero no tiene permiso de modificai�n
        If Not bModif Then
            oMensajes.PermisoAsignarMatProve
            Set oGruposMN1Seleccionados = Nothing
            Set oGruposMN2Seleccionados = Nothing
            Set oGruposMN3Seleccionados = Nothing
            Set oGruposMN4Seleccionados = Nothing
            Set oGruposMN1DesSeleccionados = Nothing
            Set oGruposMN2DesSeleccionados = Nothing
            Set oGruposMN3DesSeleccionados = Nothing
            Set oGruposMN4DesSeleccionados = Nothing
            Screen.MousePointer = vbNormal
            Unload Me
            Exit Sub
        End If
    End If
            
    Set oIMaterial = oProveSeleccionado
 
    Set oIMaterial.GruposMN1 = oGruposMN1DesSeleccionados
    Set oIMaterial.GruposMN2 = oGruposMN2DesSeleccionados
    Set oIMaterial.GruposMN3 = oGruposMN3DesSeleccionados
    Set oIMaterial.GruposMN4 = oGruposMN4DesSeleccionados
  
    teserror = oIMaterial.DesAsignarMaterial

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oIMaterial.GruposMN1 = Nothing
        Set oIMaterial.GruposMN2 = Nothing
        Set oIMaterial.GruposMN3 = Nothing
        Set oIMaterial.GruposMN4 = Nothing
        Set oGruposMN1DesSeleccionados = Nothing
        Set oGruposMN2DesSeleccionados = Nothing
        Set oGruposMN3DesSeleccionados = Nothing
        Set oGruposMN4DesSeleccionados = Nothing
        Set oGruposMN1DesSeleccionados = oFSGSRaiz.generar_CGruposMatNivel1
        Set oGruposMN2DesSeleccionados = oFSGSRaiz.generar_cgruposmatnivel2
        Set oGruposMN3DesSeleccionados = oFSGSRaiz.generar_cgruposmatnivel3
        Set oGruposMN4DesSeleccionados = oFSGSRaiz.generar_cgruposmatnivel4
        Exit Sub
    End If
  
    Set oIMaterial.GruposMN1 = oGruposMN1Seleccionados
    Set oIMaterial.GruposMN2 = oGruposMN2Seleccionados
    Set oIMaterial.GruposMN3 = oGruposMN3Seleccionados
    Set oIMaterial.GruposMN4 = oGruposMN4Seleccionados

    teserror = oIMaterial.AsignarMaterial
        
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oIMaterial.GruposMN1 = Nothing
        Set oIMaterial.GruposMN2 = Nothing
        Set oIMaterial.GruposMN3 = Nothing
        Set oIMaterial.GruposMN4 = Nothing
        Set oGruposMN1Seleccionados = Nothing
        Set oGruposMN2Seleccionados = Nothing
        Set oGruposMN3Seleccionados = Nothing
        Set oGruposMN4Seleccionados = Nothing
        Set oGruposMN1Seleccionados = oFSGSRaiz.generar_CGruposMatNivel1
        Set oGruposMN2Seleccionados = oFSGSRaiz.generar_cgruposmatnivel2
        Set oGruposMN3Seleccionados = oFSGSRaiz.generar_cgruposmatnivel3
        Set oGruposMN4Seleccionados = oFSGSRaiz.generar_cgruposmatnivel4
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    RegistrarAccion accionessummit.ACCMatPorProveMod, "Cod:" & Trim(txtProveCod.Text)
    
    fraProveedor.Enabled = True
    
    Set oGruposMN1Seleccionados = Nothing
    Set oGruposMN2Seleccionados = Nothing
    Set oGruposMN3Seleccionados = Nothing
    Set oGruposMN4Seleccionados = Nothing
    Set oGruposMN1DesSeleccionados = Nothing
    Set oGruposMN2DesSeleccionados = Nothing
    Set oGruposMN3DesSeleccionados = Nothing
    Set oGruposMN4DesSeleccionados = Nothing
    bAceptar = True
  
    Screen.MousePointer = vbNormal
    Unload Me

End Sub

Private Sub cmdCancelar_Click()
    
    If Not bAceptar And ((oGruposMN1.Count = 0) And (oGruposMN2.Count = 0) And (oGruposMN3.Count = 0) And (oGruposMN4.Count = 0)) Then
            If (gParametrosGenerales.gbOblProveMat And bModif) Then
                bAceptar = False
                oMensajes.AsignacionMatProveObligatoria
                Exit Sub
            Else
                bAceptar = True
                Unload Me
            End If
    Else
        bAceptar = True
        Unload Me
    End If

End Sub
Private Sub Form_Load()
    
    bload = True
    Me.Height = 5400
    Me.Width = 5500
    Me.Top = 1400
    Me.Left = 2700
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    ConfigurarSeguridad
       
    CargarRecursos
    
    If sOrigen = "frmAdmProvePortalresult" Then
        txtProveCod.Text = frmAdmPROVEPortalResult.sdbgProve.Columns("C�digo FSGS").Value
        txtProveDen.Text = frmAdmPROVEPortalResult.sdbgProve.Columns("Denominaci�n").Value
    End If

    fraProveedor.Enabled = False
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.Add txtProveCod.Text, txtProveDen.Text
    oProves.CargarDatosProveedor (txtProveCod.Text)
    
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(txtProveCod.Text)
        
    Set oGruposMN1DesSeleccionados = oFSGSRaiz.generar_CGruposMatNivel1
    Set oGruposMN2DesSeleccionados = oFSGSRaiz.generar_cgruposmatnivel2
    Set oGruposMN3DesSeleccionados = oFSGSRaiz.generar_cgruposmatnivel3
    Set oGruposMN4DesSeleccionados = oFSGSRaiz.generar_cgruposmatnivel4
    
    Set oGruposMN1Seleccionados = oFSGSRaiz.generar_CGruposMatNivel1
    Set oGruposMN2Seleccionados = oFSGSRaiz.generar_cgruposmatnivel2
    Set oGruposMN3Seleccionados = oFSGSRaiz.generar_cgruposmatnivel3
    Set oGruposMN4Seleccionados = oFSGSRaiz.generar_cgruposmatnivel4

    CargarEstructuraMateriales
    bAceptar = False

End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer
    
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADMPROVEMAT, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 1)
         
        Me.caption = Ador(0).Value
        Ador.MoveNext
        lblProveedor.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sIdioma(1) = Ador(0).Value
        
        Ador.Close
    
    End If
    
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 10/12/2012< 1seg </revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVEModificar)) Is Nothing) Then
        bModif = True
    End If

    If ((basOptimizacion.gTipoDeUsuario = comprador) And (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVERestEquipo)) Is Nothing)) And (basParametros.gParametrosGenerales.gbOblProveEqp)) Then
        bREqp = True
    End If

    If ((basOptimizacion.gTipoDeUsuario = comprador) And (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVERestMatComp)) Is Nothing))) Then
        bRMatAsig = True
    End If
End Sub

Public Function DevolverCod(ByVal Node As MSComctlLib.Node) As Variant

    Select Case Left(Node.Tag, 4)
    
    Case "GMN1"
            
            DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
        
    Case "GMN2"
            
            DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
        
    Case "GMN3"
            
            DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
       
    Case "GMN4"
            
            DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
       
    End Select

End Function

Private Sub Form_Resize()
    If Me.Height < 2500 Then Exit Sub
    If Me.Width < 1000 Then Exit Sub
    
    tvwEstrMatMod.Width = Me.Width - 220
    tvwEstrMatMod.Height = Me.Height - 1800
    Picture1.Top = Me.Height - 910
    Picture1.Width = tvwEstrMatMod.Width
    cmdCancelar.Left = tvwEstrMatMod.Width / 2
    cmdAceptar.Left = cmdCancelar.Left - 1100
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If Not bAceptar Then
        If ((oGruposMN1.Count = 0) And (oGruposMN2.Count = 0) And (oGruposMN3.Count = 0) And (oGruposMN4.Count = 0)) Then
            If (gParametrosGenerales.gbOblProveMat And bModif) Then
                oMensajes.AsignacionMatProveObligatoria
                Cancel = True
                Exit Sub
            End If
        End If
    Else
        Set oGrupsMN1Comp = Nothing
        Set oGrupsMN2Comp = Nothing
        Set oGrupsMN3Comp = Nothing
        Set oGrupsMN4Comp = Nothing
        Set oGruposMN1 = Nothing
        Set oGruposMN2 = Nothing
        Set oGruposMN3 = Nothing
        Set oGruposMN4 = Nothing
        Set oProves = Nothing
        Set oProveSeleccionado = Nothing
        Set oIMaterialAsignado = Nothing
    End If
        
    Me.Visible = False
End Sub
Private Sub tvwEstrMatMod_KeyUp(KeyCode As Integer, Shift As Integer)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

If bModif Then

    If bMouseDown Then
        
        bMouseDown = False
        
        If SelNode.Tag = "Raiz" Then
            SelNode.Checked = Not SelNode.Checked
            DoEvents
            Exit Sub
        End If
        
        If bRMatAsig Then
            Select Case Left(SelNode.Tag, 4)
            
                Case "GMN1"
                    
                        SelNode.Checked = Not SelNode.Checked
                        oMensajes.PermisoDenegadoDesAsignarMat
                        Exit Sub
                    
                Case "GMN2"
                    scod1 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent)))
                    If oGrupsMN1Comp.Item(scod1) Is Nothing Then
                        SelNode.Checked = Not SelNode.Checked
                        oMensajes.PermisoDenegadoDesAsignarMat
                        Exit Sub
                    End If
                    
                Case "GMN3"
                    
                    scod1 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent)))
                    scod2 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent)))
                    
                    If oGrupsMN2Comp.Item(scod1 & scod2) Is Nothing _
                    And oGrupsMN2Comp.Item(scod1) Is Nothing Then
                        SelNode.Checked = Not SelNode.Checked
                        oMensajes.PermisoDenegadoDesAsignarMat
                        Exit Sub
                    End If
                    
                Case "GMN4"
                    
                    scod1 = DevolverCod(SelNode.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent.Parent)))
                    scod2 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent.Parent)))
                    scod3 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(SelNode.Parent)))
                                        
                    If oGrupsMN3Comp.Item(scod1 & scod2 & scod3) Is Nothing _
                    And oGrupsMN3Comp.Item(scod1 & scod2) Is Nothing _
                    And oGrupsMN3Comp.Item(scod1) Is Nothing Then
                        SelNode.Checked = Not SelNode.Checked
                        oMensajes.PermisoDenegadoDesAsignarMat
                        Exit Sub
                    End If
            
            End Select
            
        End If
    
    End If
    
    If SelNode.Checked Then
            
            MarcarTodosLosHijos SelNode
            
    Else
            QuitarMarcaTodosSusPadres SelNode
            QuitarMarcaTodosLosHijos SelNode
        
    End If

End If

End Sub


Private Sub tvwEstrMatMod_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nod As MSComctlLib.Node
Dim tvhti As TVHITTESTINFO

                  
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwEstrMatMod.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwEstrMatMod.hWnd, TVM_HITTEST, 0, tvhti)) Then
            
                If (tvhti.FLAGS And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiar� al finalizar este procedimiento
                             
                bMouseDown = True
                Set SelNode = nod
                             
                End If
            End If
        End If
    End If

End Sub


Private Sub tvwEstrMatMod_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

If bModif Then

    If bMouseDown Then
        
        bMouseDown = False
        
        If SelNode.Tag = "Raiz" Then
            SelNode.Checked = Not SelNode.Checked
            DoEvents
            Exit Sub
        End If
        
        If bRMatAsig Then
            Select Case Left(SelNode.Tag, 4)
            
                Case "GMN1"
                    
                        SelNode.Checked = Not SelNode.Checked
                        oMensajes.PermisoDenegadoDesAsignarMat
                        Exit Sub
                    
                Case "GMN2"
                    scod1 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent)))
                    If oGrupsMN1Comp.Item(scod1) Is Nothing Then
                        SelNode.Checked = Not SelNode.Checked
                        oMensajes.PermisoDenegadoDesAsignarMat
                        Exit Sub
                    End If
                    
                Case "GMN3"
                    
                    scod1 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent)))
                    scod2 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent)))
                    
                    If oGrupsMN2Comp.Item(scod1 & scod2) Is Nothing _
                    And oGrupsMN2Comp.Item(scod1) Is Nothing Then
                        SelNode.Checked = Not SelNode.Checked
                        oMensajes.PermisoDenegadoDesAsignarMat
                        Exit Sub
                    End If
                    
                Case "GMN4"
                    
                    scod1 = DevolverCod(SelNode.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent.Parent)))
                    scod2 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent.Parent)))
                    scod3 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(SelNode.Parent)))
                    
                    If oGrupsMN3Comp.Item(scod1 & scod2 & scod3) Is Nothing _
                    And oGrupsMN3Comp.Item(scod1 & scod2) Is Nothing _
                    And oGrupsMN3Comp.Item(scod1) Is Nothing Then
                        
                        SelNode.Checked = Not SelNode.Checked
                        oMensajes.PermisoDenegadoDesAsignarMat
                        Exit Sub
                    End If
            
            End Select
            
        End If
    
    End If
    
    If SelNode.Checked Then
            
            MarcarTodosLosHijos SelNode
            
    Else
            QuitarMarcaTodosSusPadres SelNode
            QuitarMarcaTodosLosHijos SelNode
        
    End If

End If

End Sub


Private Sub tvwEstrMatMod_NodeCheck(ByVal Node As MSComctlLib.Node)
If bMouseDown Then
    Exit Sub
End If

bMouseDown = True
Set SelNode = Node

End Sub


Private Sub txtProveCod_Change()
    tvwEstrMatMod.Nodes.clear
End Sub


Private Sub txtProveDen_Change()
    tvwEstrMatMod.Nodes.clear
End Sub


