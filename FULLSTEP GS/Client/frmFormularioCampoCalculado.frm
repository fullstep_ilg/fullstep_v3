VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFormularioCampoCalculado 
   BackColor       =   &H00808000&
   Caption         =   "DCampos calculados"
   ClientHeight    =   5625
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11775
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFormularioCampoCalculado.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5625
   ScaleWidth      =   11775
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAyuda 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   60
      Picture         =   "frmFormularioCampoCalculado.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   1640
      Width           =   350
   End
   Begin VB.CommandButton cmdSimular 
      Caption         =   "DSimulaci�n"
      Height          =   315
      Left            =   450
      TabIndex        =   6
      Top             =   5250
      Width           =   1500
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTipo 
      Height          =   975
      Left            =   8880
      TabIndex        =   9
      Top             =   4200
      Width           =   2295
      DataFieldList   =   "Column 1"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3757
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4048
      _ExtentY        =   1720
      _StockProps     =   77
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddOrigen 
      Height          =   1095
      Left            =   8160
      TabIndex        =   8
      Top             =   4320
      Width           =   2655
      DataFieldList   =   "Column 1"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "NOMBRE"
      Columns(1).Name =   "NOMBRE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "GRUPO"
      Columns(2).Name =   "GRUPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   4683
      _ExtentY        =   1931
      _StockProps     =   77
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddGrupos 
      Height          =   1095
      Left            =   5400
      TabIndex        =   7
      Top             =   4560
      Width           =   3375
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5927
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5953
      _ExtentY        =   1931
      _StockProps     =   77
      BackColor       =   16777215
   End
   Begin VB.CommandButton cmdElimCampo 
      Height          =   300
      Left            =   60
      Picture         =   "frmFormularioCampoCalculado.frx":0EE3
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1260
      Width           =   350
   End
   Begin VB.CommandButton cmdAnyaCampo 
      Height          =   300
      Left            =   60
      Picture         =   "frmFormularioCampoCalculado.frx":0F75
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   880
      Width           =   350
   End
   Begin VB.CommandButton cmdBajar 
      Height          =   300
      Left            =   60
      Picture         =   "frmFormularioCampoCalculado.frx":0FF7
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   500
      UseMaskColor    =   -1  'True
      Width           =   350
   End
   Begin VB.CommandButton cmdSubir 
      Height          =   300
      Left            =   60
      Picture         =   "frmFormularioCampoCalculado.frx":1339
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   120
      UseMaskColor    =   -1  'True
      Width           =   350
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCalculados 
      Height          =   5075
      Left            =   450
      TabIndex        =   0
      Top             =   120
      Width           =   11085
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   12
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   12
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID_GRUPO"
      Columns(1).Name =   "ID_GRUPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   953
      Columns(2).Caption=   "ID_CALCULO"
      Columns(2).Name =   "ID_CALCULO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   13619151
      Columns(3).Width=   2990
      Columns(3).Caption=   "GRUPO"
      Columns(3).Name =   "GRUPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   3
      Columns(4).Width=   3200
      Columns(4).Caption=   "NOMBRE"
      Columns(4).Name =   "NOMBRE"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   1931
      Columns(5).Caption=   "TIPO"
      Columns(5).Name =   "TIPO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   3
      Columns(6).Width=   3200
      Columns(6).Caption=   "ORIGEN"
      Columns(6).Name =   "ORIGEN"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Style=   3
      Columns(7).Width=   4498
      Columns(7).Caption=   "FORMULA"
      Columns(7).Name =   "FORMULA"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(7).Style=   1
      Columns(8).Width=   1826
      Columns(8).Caption=   "WORKFLOW"
      Columns(8).Name =   "WORKFLOW"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "ID_TIPO"
      Columns(9).Name =   "ID_TIPO"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "ID_ORIGEN"
      Columns(10).Name=   "ID_ORIGEN"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "ID_GR_ORIGEN"
      Columns(11).Name=   "ID_GR_ORIGEN"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      _ExtentX        =   19553
      _ExtentY        =   8952
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmFormularioCampoCalculado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_bModif As Boolean
Public g_lCampoPosic As Long
Public g_sOrigen As String
Private m_oFormulario As CFormulario
Private m_oInstancia As CInstancia
Private m_bUpdate As Boolean
'variables de idiomas:
Private m_sIdiFormulaClick As String
Private m_sIdiTipo(3) As String
Private m_sIdiErrorFormula(12) As String
Private m_bErrorDesgloseSinCampoOrigen As Boolean

Private Sub cmdAnyaCampo_Click()
    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update

    'A�ade una fila a la grid:
    sdbgCalculados.AddNew
    
    sdbgCalculados.Scroll 0, sdbgCalculados.Rows - sdbgCalculados.Row

    If sdbgCalculados.VisibleRows > 0 Then
        If sdbgCalculados.VisibleRows > sdbgCalculados.Rows Then
            sdbgCalculados.Row = sdbgCalculados.Rows
        Else
            sdbgCalculados.Row = sdbgCalculados.Rows - (sdbgCalculados.Rows - sdbgCalculados.VisibleRows) - 1
        End If
    End If
            
    sdbgCalculados.Columns("ID_CALCULO").Value = "X" & m_oFormulario.ObtenerIDCalculo()

    If Me.Visible Then sdbgCalculados.SetFocus
End Sub
Private Sub cmdAyuda_Click()
    'Muestra la ayuda:
    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
    
    MostrarFormSOLAyudaCalculos oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub
Private Sub cmdBajar_Click()
    Dim i As Integer
    Dim arrValores() As Variant
    Dim arrValores2() As Variant
    
    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
    If sdbgCalculados.SelBookmarks.Count = 0 Then Exit Sub
    If sdbgCalculados.AddItemRowIndex(sdbgCalculados.SelBookmarks.Item(0)) = sdbgCalculados.Rows - 1 Then Exit Sub
    
    m_bUpdate = False
    
    ReDim arrValores(sdbgCalculados.Columns.Count - 1)
    ReDim arrValores2(sdbgCalculados.Columns.Count - 1)
   
    For i = 0 To sdbgCalculados.Columns.Count - 1
        arrValores(i) = sdbgCalculados.Columns(i).Value
    Next i
    sdbgCalculados.MoveNext
        
    For i = 0 To sdbgCalculados.Columns.Count - 1
        arrValores2(i) = sdbgCalculados.Columns(i).Value
        sdbgCalculados.Columns(i).Value = arrValores(i)
    Next i
    
    sdbgCalculados.MovePrevious
    
    For i = 0 To sdbgCalculados.Columns.Count - 1
        sdbgCalculados.Columns(i).Value = arrValores2(i)
    Next i
        
    sdbgCalculados.SelBookmarks.RemoveAll
    sdbgCalculados.MoveNext
    sdbgCalculados.SelBookmarks.Add sdbgCalculados.Bookmark
        
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos
    
    m_bUpdate = True
End Sub
Private Sub cmdElimCampo_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim irespuesta As Integer
    Dim i As Integer
    Dim aIdentificadores As Variant
    Dim aBookmarks As Variant
    Dim vbm As Variant
    Dim j As Integer

On Error GoTo Cancelar:
    
    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
        
    If sdbgCalculados.Rows = 0 Then Exit Sub
    If sdbgCalculados.SelBookmarks.Count = 0 Then sdbgCalculados.SelBookmarks.Add sdbgCalculados.Bookmark

    If sdbgCalculados.Columns("ID").Value = "" Then
        sdbgCalculados.CancelUpdate
        sdbgCalculados.DataChanged = False
    Else
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCalculados.Columns("NOMBRE").Value)
        If irespuesta = vbNo Then Exit Sub
    
        Screen.MousePointer = vbHourglass
        
        'Elimina de base de datos:
        ReDim aIdentificadores(sdbgCalculados.SelBookmarks.Count)
        ReDim aBookmarks(sdbgCalculados.SelBookmarks.Count)
    
        i = 0
        While i < sdbgCalculados.SelBookmarks.Count
            sdbgCalculados.Bookmark = sdbgCalculados.SelBookmarks(i)
            aIdentificadores(i + 1) = sdbgCalculados.Columns("ID").Value
            aBookmarks(i + 1) = sdbgCalculados.SelBookmarks(i)
            i = i + 1
        Wend
        
        udtTeserror = m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.EliminarCamposDeBaseDatos(aIdentificadores)
    
        If udtTeserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError udtTeserror
        Else
            'Elimina los campos de la colecci�n:
            For i = 1 To UBound(aIdentificadores)
                 m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Remove (CStr(aIdentificadores(i)))
                 
                 'Si est� posicionado en el mismo grupo en los formularios lo elimina de la colecci�n y del formulario:
                 If m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Id = frmFormularios.g_oGrupoSeleccionado.Id Then
                    For j = 0 To frmFormularios.sdbgCampos.Rows - 1
                        vbm = frmFormularios.sdbgCampos.AddItemBookmark(j)
                        If frmFormularios.sdbgCampos.Columns("ID").CellValue(vbm) = aIdentificadores(1) Then
                            frmFormularios.sdbgCampos.RemoveItem (frmFormularios.sdbgCampos.AddItemRowIndex(vbm))
                            Exit For
                        End If
                    Next j
                    'Se posiciona en la fila correspondiente:
                    If frmFormularios.sdbgCampos.Rows > 0 Then
                        If IsEmpty(frmFormularios.sdbgCampos.RowBookmark(frmFormularios.sdbgCampos.Row)) Then
                            frmFormularios.sdbgCampos.Bookmark = frmFormularios.sdbgCampos.RowBookmark(sdbgCalculados.Row - 1)
                        Else
                            frmFormularios.sdbgCampos.Bookmark = frmFormularios.sdbgCampos.RowBookmark(sdbgCalculados.Row)
                        End If
                    End If
                 End If
                 If Not frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos Is Nothing Then
                    frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Remove (CStr(aIdentificadores(i)))
                 End If
                 
                 basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemEliminar, aIdentificadores(i)
            Next i
            
            'Elimina los campos de la grid:
            For i = 1 To UBound(aBookmarks)
                sdbgCalculados.RemoveItem (sdbgCalculados.AddItemRowIndex(aBookmarks(i)))
            Next i
            
            'Se posiciona en la fila correspondiente:
            If sdbgCalculados.Rows > 0 Then
                If IsEmpty(sdbgCalculados.RowBookmark(sdbgCalculados.Row)) Then
                    sdbgCalculados.Bookmark = sdbgCalculados.RowBookmark(sdbgCalculados.Row - 1)
                Else
                    sdbgCalculados.Bookmark = sdbgCalculados.RowBookmark(sdbgCalculados.Row)
                End If
            End If
        End If
    End If
    
    sdbgCalculados.SelBookmarks.RemoveAll
    If Me.Visible Then sdbgCalculados.SetFocus

    Screen.MousePointer = vbNormal
    Exit Sub
    
Cancelar:
    Screen.MousePointer = vbNormal
End Sub
Private Sub cmdSimular_Click()
    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
    
    frmFormularioSimulacion.g_bModif = g_bModif
    Set frmFormularioSimulacion.g_oFormulario = m_oFormulario
    frmFormularioSimulacion.Show vbModal
End Sub
Private Sub cmdSubir_Click()
    Dim i As Integer
    Dim arrValores() As Variant
    Dim arrValores2() As Variant
    
    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
    
    If sdbgCalculados.SelBookmarks.Count = 0 Then Exit Sub
    If sdbgCalculados.AddItemRowIndex(sdbgCalculados.SelBookmarks.Item(0)) = 0 Then Exit Sub
    
    m_bUpdate = False
    
    ReDim arrValores(sdbgCalculados.Columns.Count - 1)
    ReDim arrValores2(sdbgCalculados.Columns.Count - 1)
    
    For i = 0 To sdbgCalculados.Columns.Count - 1
        arrValores(i) = sdbgCalculados.Columns(i).Value
    Next i

    sdbgCalculados.MovePrevious
        
    For i = 0 To sdbgCalculados.Columns.Count - 1
        arrValores2(i) = sdbgCalculados.Columns(i).Value
        sdbgCalculados.Columns(i).Value = arrValores(i)
    Next i
    
    sdbgCalculados.MoveNext
    
    For i = 0 To sdbgCalculados.Columns.Count - 1
        sdbgCalculados.Columns(i).Value = arrValores2(i)
    Next i
    
    sdbgCalculados.SelBookmarks.RemoveAll
    sdbgCalculados.MovePrevious
    sdbgCalculados.SelBookmarks.Add sdbgCalculados.Bookmark
        

    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos
    
    m_bUpdate = True
End Sub
Private Sub Form_Load()
    Dim i As Integer
    Dim vbm As Variant

    Me.Height = 6135
    Me.Width = 14500
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    If g_bModif = False Then
        cmdAnyaCampo.Visible = False
        cmdAyuda.Visible = False
        cmdBajar.Visible = False
        cmdElimCampo.Visible = False
        cmdSubir.Visible = False
        
        sdbgCalculados.Left = 120
        cmdSimular.Left = 120
        For i = 0 To sdbgCalculados.Columns.Count - 1
            sdbgCalculados.Columns(i).Locked = True
        Next i
    End If
    
    If g_sOrigen = "frmSolicitudDetalle" Then
        cmdSimular.Visible = False
    End If
    
    'Carga los campos calculados del formulario
    Select Case g_sOrigen
        Case "frmFormularios"
            Set m_oFormulario = oFSGSRaiz.Generar_CFormulario
            m_oFormulario.Id = frmFormularios.g_oFormSeleccionado.Id
            m_oFormulario.Den = frmFormularios.g_oFormSeleccionado.Den
        Case "frmPROCE"
            Set m_oInstancia = oFSGSRaiz.Generar_CInstancia
            m_oInstancia.Id = frmPROCE.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Id
            m_oInstancia.NumVersion = frmPROCE.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.NumVersion
        Case "frmPedidos"
            Set m_oInstancia = oFSGSRaiz.Generar_CInstancia
            m_oInstancia.Id = frmPEDIDOS.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Id
            m_oInstancia.NumVersion = frmPEDIDOS.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.NumVersion
        Case "frmSeguimiento"
            Set m_oInstancia = oFSGSRaiz.Generar_CInstancia
            m_oInstancia.Id = frmSeguimiento.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Id
            m_oInstancia.NumVersion = frmSeguimiento.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.NumVersion
        Case Else
            Set m_oInstancia = oFSGSRaiz.Generar_CInstancia
            m_oInstancia.Id = frmSolicitudes.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Id
            m_oInstancia.NumVersion = frmSolicitudes.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.NumVersion
    End Select
    
    CargarCamposCalculados
    
    'Se posiciona en el campo desde el que se llama a la pantalla en frmFormularios
    If g_lCampoPosic <> 0 Then
        For i = 0 To sdbgCalculados.Rows - 1
            vbm = sdbgCalculados.AddItemBookmark(i)
            If sdbgCalculados.Columns("ID").CellValue(vbm) = g_lCampoPosic Then
                sdbgCalculados.Bookmark = vbm
                Exit For
            End If
        Next i
    End If
    
    sdbddGrupos.AddItem ""
    sdbddTipo.AddItem ""
    sdbddOrigen.AddItem ""

    sdbgCalculados.Columns("TIPO").DropDownHwnd = sdbddTipo.hWnd
    
    m_bUpdate = True
End Sub

Private Sub Form_Resize()
    If Me.Width < 1000 Then Exit Sub
    If Me.Height < 1300 Then Exit Sub
    
    If g_bModif = True Then
        sdbgCalculados.Width = Me.Width - 615
    Else
        sdbgCalculados.Width = Me.Width - 285
    End If
    
    sdbgCalculados.Columns("ID_CALCULO").Width = sdbgCalculados.Width / 25
    sdbgCalculados.Columns("GRUPO").Width = sdbgCalculados.Width / 7.5
    sdbgCalculados.Columns("NOMBRE").Width = sdbgCalculados.Width / 7.5
    sdbgCalculados.Columns("TIPO").Width = sdbgCalculados.Width / 11
    sdbgCalculados.Columns("ORIGEN").Width = sdbgCalculados.Width / 4.1
    sdbddOrigen.Columns("NOMBRE").Width = sdbgCalculados.Columns("ORIGEN").Width
    sdbgCalculados.Columns("FORMULA").Width = sdbgCalculados.Width / 4.1
    sdbgCalculados.Columns("WORKFLOW").Width = sdbgCalculados.Width / 13.7
    
    If g_sOrigen = "frmSolicitudDetalle" Then
        sdbgCalculados.Height = Me.Height - 700
    Else
        sdbgCalculados.Height = Me.Height - 1015
        cmdSimular.Top = sdbgCalculados.Top + sdbgCalculados.Height + 50
    End If
End Sub
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FORMCAMPOS_CALCULADOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value   '1 Campos calculados
        Ador.MoveNext
        sdbgCalculados.Columns("ID_CALCULO").caption = Ador(0).Value '2 Id.
        Ador.MoveNext
        sdbgCalculados.Columns("GRUPO").caption = Ador(0).Value '3 Grupo
        Ador.MoveNext
        sdbgCalculados.Columns("NOMBRE").caption = Ador(0).Value '4 Nombre
        Ador.MoveNext
        sdbgCalculados.Columns("TIPO").caption = Ador(0).Value '5 Tipo
        Ador.MoveNext
        sdbgCalculados.Columns("ORIGEN").caption = Ador(0).Value '6 Campo origen
        Ador.MoveNext
        sdbgCalculados.Columns("FORMULA").caption = Ador(0).Value '7 F�rmula
        Ador.MoveNext
        sdbgCalculados.Columns("WORKFLOW").caption = Ador(0).Value '8 Workflow
        Ador.MoveNext
        m_sIdiFormulaClick = Ador(0).Value  '9 (Haga click sobre el bot�n para introducir la f�rmula)
        Ador.MoveNext
        m_sIdiTipo(1) = Ador(0).Value '10 Num�rico
        Ador.MoveNext
        m_sIdiTipo(2) = Ador(0).Value '11 Calculado
        Ador.MoveNext
        m_sIdiTipo(3) = Ador(0).Value '12 Desglose
        Ador.MoveNext
        cmdSimular.caption = Ador(0).Value '13 &Simular
        
        For i = 1 To 11
            Ador.MoveNext
            m_sIdiErrorFormula(i) = Ador(0).Value
        Next i
                
        Ador.MoveNext
        sdbgCalculados.Columns("WORKFLOW").caption = Ador(0).Value '8 Importe
                
        Ador.MoveNext
        Ador.MoveNext
        m_sIdiErrorFormula(i) = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub
Private Sub Form_Unload(Cancel As Integer)
    If sdbgCalculados.DataChanged = True Then
        sdbgCalculados.Update
    End If
    
    g_bModif = False
    g_lCampoPosic = 0
    g_sOrigen = ""
    
    Set m_oFormulario = Nothing
    Set m_oInstancia = Nothing
End Sub
Private Sub GuardarOrdenCampos()
    Dim oCampos As CFormItems
    Dim i As Integer
    Dim teserror As TipoErrorSummit
    Dim vbm As Variant
    Dim oCampo As CFormItem

    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    Set oCampos = oFSGSRaiz.Generar_CFormCampos

    For i = 0 To sdbgCalculados.Rows - 1
        vbm = sdbgCalculados.AddItemBookmark(i)

        'Guarda en BD solo el orden de los campos que han cambiado
        If NullToDbl0(m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm))).Campos.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).OrdenCalculo) <> i + 1 Then
            m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm))).Campos.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).OrdenCalculo = i + 1

            oCampos.Add sdbgCalculados.Columns("ID").CellValue(vbm), m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm))), m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm))).Campos.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).Denominaciones, , , , , , , , , , , , , , , , , , , , , , , , , , , i + 1
        End If
    Next i

    teserror = oCampos.GuardarOrdenCalculoCampos

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        For Each oCampo In oCampos
            m_oFormulario.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos.Item(CStr(oCampo.Id)).FECACT = oCampo.FECACT
        Next
    End If

    Set oCampos = Nothing
End Sub
Private Sub CargarCamposCalculados()
    Dim Ador As Ador.Recordset
    Dim sCadena As String

    sdbgCalculados.RemoveAll
    
    If g_sOrigen = "frmFormularios" Then
        m_oFormulario.CargarTodosLosGrupos
        Set Ador = m_oFormulario.CargarCamposCalculados(False)
    Else
        Set Ador = m_oInstancia.CargarCamposCalculados(True)
    End If
        
    If Not Ador Is Nothing Then
        If Ador.RecordCount > 0 Then Ador.MoveFirst
        
        While Not Ador.EOF
            If g_sOrigen = "frmFormularios" Then
                sCadena = Ador.Fields("ID").Value
            Else
                sCadena = Ador.Fields("ID_CAMPO").Value
            End If
            sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("GRUPO").Value & Chr(m_lSeparador) & Ador.Fields("ID_CALCULO").Value & Chr(m_lSeparador) & Ador.Fields("DENGRUPO").Value & Chr(m_lSeparador) & Ador.Fields("DEN_" & gParametrosInstalacion.gIdioma).Value
            
            'Tipo:
            If Ador.Fields("TIPO").Value = TipoCampoPredefinido.Calculado Then
                If IsNull(Ador.Fields("ORIGEN_CALC_DESGLOSE").Value) Then  'C�lculo simple
                    sCadena = sCadena & Chr(m_lSeparador) & m_sIdiTipo(2) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & Ador.Fields("FORMULA").Value
                Else 'C�lculo desglose
                    sCadena = sCadena & Chr(m_lSeparador) & m_sIdiTipo(3) & Chr(m_lSeparador) & Ador.Fields("DENORIGEN").Value & Chr(m_lSeparador) & m_sIdiFormulaClick
                End If
            Else 'Num�rico
                sCadena = sCadena & Chr(m_lSeparador) & m_sIdiTipo(1) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
            End If

            sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("ES_IMPORTE_TOTAL").Value
            
            If Ador.Fields("TIPO").Value = TipoCampoPredefinido.Calculado Then
                If IsNull(Ador.Fields("ORIGEN_CALC_DESGLOSE").Value) Then  'C�lculo simple
                    sCadena = sCadena & Chr(m_lSeparador) & "2" & Chr(m_lSeparador) & Ador.Fields("ORIGEN_CALC_DESGLOSE").Value
                Else 'C�lculo desglose
                    sCadena = sCadena & Chr(m_lSeparador) & "3" & Chr(m_lSeparador) & Ador.Fields("ORIGEN_CALC_DESGLOSE").Value
                End If
            Else 'Num�rico
                sCadena = sCadena & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & Ador.Fields("ORIGEN_CALC_DESGLOSE").Value
            End If
            sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("GR_ORIGEN").Value
            
            sdbgCalculados.AddItem sCadena
            Ador.MoveNext
        Wend

        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub
Private Sub sdbddGrupos_CloseUp()
    sdbgCalculados.Columns("GRUPO").Value = sdbddGrupos.Columns("DEN").Value
    sdbgCalculados.Columns("ID_GRUPO").Value = sdbddGrupos.Columns("ID").Value
End Sub
Private Sub sdbddGrupos_DropDown()
    Dim oGrupo As CFormGrupo
    Dim oForm As CFormulario

    ''' * Objetivo: Cargar combo con la coleccion de formas de pago
    If Not g_bModif Then
        sdbddGrupos.DroppedDown = False
        Exit Sub
    End If

    sdbddGrupos.RemoveAll

    Set oForm = oFSGSRaiz.Generar_CFormulario
    oForm.Id = m_oFormulario.Id
    oForm.CargarTodosLosGrupos
    
    For Each oGrupo In oForm.Grupos
        sdbddGrupos.AddItem oGrupo.Id & Chr(m_lSeparador) & oGrupo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
    Next

    Set oForm = Nothing
    
    If sdbddGrupos.Rows = 0 Then
        sdbddGrupos.AddItem ""
    End If
    
    sdbgCalculados.ActiveCell.SelStart = 0
    sdbgCalculados.ActiveCell.SelLength = Len(sdbgCalculados.ActiveCell.Value)
End Sub
Private Sub sdbddGrupos_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    sdbddGrupos.DataFieldList = "Column 1"
    sdbddGrupos.DataFieldToDisplay = "Column 1"
End Sub
Private Sub sdbddGrupos_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddGrupos.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddGrupos.Rows - 1
            bm = sdbddGrupos.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddGrupos.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbddGrupos.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
Private Sub sdbddOrigen_CloseUp()
    sdbgCalculados.Columns("ORIGEN").Value = sdbddOrigen.Columns("NOMBRE").Value
    sdbgCalculados.Columns("ID_ORIGEN").Value = sdbddOrigen.Columns("ID").Value
    sdbgCalculados.Columns("ID_GR_ORIGEN").Value = sdbddOrigen.Columns("GRUPO").Value
End Sub
Private Sub sdbddOrigen_DropDown()
    Dim Ador As Ador.Recordset

    ''' * Objetivo: Cargar combo con la coleccion de formas de pago
    If Not g_bModif Then
        sdbddOrigen.DroppedDown = False
        Exit Sub
    End If

    sdbddOrigen.RemoveAll
    
    Set Ador = m_oFormulario.DevolverCamposDesglose
    While Not Ador.EOF
        sdbddOrigen.AddItem Ador.Fields("ID").Value & Chr(m_lSeparador) & "(" & Ador.Fields("DEN_GRUPO").Value & ") " & Ador.Fields("DEN_CAMPO").Value & Chr(m_lSeparador) & Ador.Fields("GRUPO").Value
        Ador.MoveNext
    Wend
    Ador.Close
    Set Ador = Nothing
    
    If sdbddOrigen.Rows = 0 Then
        sdbddOrigen.AddItem ""
    End If
    
    sdbgCalculados.ActiveCell.SelStart = 0
    sdbgCalculados.ActiveCell.SelLength = Len(sdbgCalculados.ActiveCell.Value)
End Sub
Private Sub sdbddOrigen_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    sdbddOrigen.DataFieldList = "Column 1"
    sdbddOrigen.DataFieldToDisplay = "Column 1"
End Sub
Private Sub sdbddOrigen_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddOrigen.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddOrigen.Rows - 1
            bm = sdbddOrigen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOrigen.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbddOrigen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
Private Sub sdbddTipo_CloseUp()
    If sdbgCalculados.Columns("ID_TIPO").Value = sdbddTipo.Columns("ID").Value Then Exit Sub
    
    sdbgCalculados.Columns("ID_TIPO").Value = sdbddTipo.Columns("ID").Value
    
    If sdbddTipo.Columns("ID").Value = "3" Then
        sdbgCalculados.Columns("FORMULA").Value = m_sIdiFormulaClick
    Else
        sdbgCalculados.Columns("ORIGEN").Value = ""
        sdbgCalculados.Columns("ID_ORIGEN").Value = ""
        sdbgCalculados.Columns("FORMULA").Value = ""
    End If
End Sub
Private Sub sdbddTipo_DropDown()
    ''' * Objetivo: Cargar combo con la coleccion de formas de pago
    If Not g_bModif Then
        sdbddTipo.DroppedDown = False
        Exit Sub
    End If

    sdbddTipo.RemoveAll
    
    If sdbgCalculados.IsAddRow Then
        sdbddTipo.AddItem "1" & Chr(m_lSeparador) & m_sIdiTipo(1)  'Num�rico
    End If
    sdbddTipo.AddItem "2" & Chr(m_lSeparador) & m_sIdiTipo(2)  'Calculado
    sdbddTipo.AddItem "3" & Chr(m_lSeparador) & m_sIdiTipo(3)  'Desglose
        
    If sdbddTipo.Rows = 0 Then
        sdbddTipo.AddItem ""
    End If
    
    sdbgCalculados.ActiveCell.SelStart = 0
    sdbgCalculados.ActiveCell.SelLength = Len(sdbgCalculados.ActiveCell.Value)
End Sub
Private Sub sdbddTipo_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    sdbddTipo.DataFieldList = "Column 1"
    sdbddTipo.DataFieldToDisplay = "Column 1"
End Sub
Private Sub sdbddTipo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddTipo.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddTipo.Rows - 1
            bm = sdbddTipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddTipo.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbddTipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
Private Sub sdbgCalculados_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim sVariables() As String
    Dim lErrCode As Integer
    Dim lIndex As Integer
    Dim iEq As USPExpression
    Dim sCaracter As String
    Dim i As Integer
    Dim vbm As Variant
    Dim iHasta As Integer

    If Not m_bUpdate Then Exit Sub
    
    If sdbgCalculados.Columns(ColIndex).Name <> "FORMULA" Then Exit Sub
    If sdbgCalculados.Columns(ColIndex).Value = "" Then Exit Sub
    If sdbgCalculados.Columns(ColIndex).Value = m_sIdiFormulaClick Then Exit Sub
    
    Set iEq = New USPExpression
    
    ReDim sVariables(sdbgCalculados.Rows - 1)
    
    If sdbgCalculados.IsAddRow Then
        iHasta = sdbgCalculados.Rows - 2
    Else
        iHasta = sdbgCalculados.Rows - 1
    End If
    
    For i = 0 To iHasta
        vbm = sdbgCalculados.AddItemBookmark(i)
        sVariables(i) = sdbgCalculados.Columns("ID_CALCULO").CellValue(vbm)
    Next i
    
    lIndex = iEq.Parse(sdbgCalculados.Columns(ColIndex).Value, sVariables, lErrCode)

    If lErrCode <> USPEX_NO_ERROR Then
        ' Parsing error handler
        Select Case lErrCode
            Case USPEX_DIVISION_BY_ZERO
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(1))
            Case USPEX_EMPTY_EXPRESSION
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(2))
            Case USPEX_MISSING_OPERATOR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(3))
            Case USPEX_SYNTAX_ERROR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(4))
            Case USPEX_UNKNOWN_FUNCTION
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(5))
            Case USPEX_UNKNOWN_OPERATOR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(6))
            Case USPEX_WRONG_PARAMS_NUMBER
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(7))
            Case USPEX_UNKNOWN_IDENTIFIER
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(8))
            Case USPEX_UNKNOWN_VAR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(9))
            Case USPEX_VARIABLES_NOTUNIQUE
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(10))
            Case USPEX_UNBALANCED_PAREN
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(12))
            Case Else
                sCaracter = Mid(sdbgCalculados.Columns(ColIndex).Value, lIndex + 1)
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(11) & vbCrLf & sCaracter)
        End Select
        Cancel = True
    End If

    Set iEq = Nothing
End Sub
Private Sub sdbgCalculados_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim oCampo As CFormItem
    Dim oCampos As CFormItems
    Dim oIdi As CIdioma
    Dim i As Integer
    Dim vbm As Variant

    If Not m_bUpdate Then Exit Sub
    
    If sdbgCalculados.Columns("GRUPO").Value = "" Then
        oMensajes.NoValido sdbgCalculados.Columns("GRUPO").caption
        Cancel = True
        Exit Sub
    ElseIf sdbgCalculados.Columns("ID_GRUPO").Value = "" Then
        oMensajes.NoValido sdbgCalculados.Columns("GRUPO").caption
        Cancel = True
        Exit Sub
    End If
    
    If sdbgCalculados.Columns("NOMBRE").Value = "" Then
        oMensajes.NoValido sdbgCalculados.Columns("NOMBRE").caption
        Cancel = True
        Exit Sub
    End If
    
    Select Case sdbgCalculados.Columns("ID_TIPO").Value
        Case ""
            oMensajes.NoValido sdbgCalculados.Columns("TIPO").caption
            Cancel = True
            Exit Sub
        Case "3"  'Es un campo calculado de desglose
            If sdbgCalculados.Columns("ORIGEN").Value = "" Then
                If m_bErrorDesgloseSinCampoOrigen = False Then
                    oMensajes.NoValido sdbgCalculados.Columns("ORIGEN").caption
                    m_bErrorDesgloseSinCampoOrigen = True
                Else
                    m_bErrorDesgloseSinCampoOrigen = False
                End If
                
                Cancel = True
                Exit Sub
            End If
        Case "2"  'Es un campo calculado normal
            If sdbgCalculados.Columns("FORMULA").Value = "" Then
                oMensajes.FaltaFormula
                Cancel = True
                Exit Sub
            End If
    End Select
    
    Screen.MousePointer = vbHourglass
    
    If sdbgCalculados.IsAddRow Then
        'Es una inserci�n:
        Set oCampo = oFSGSRaiz.Generar_CFormCampo
        
        oCampo.CampoGS = Null
        oCampo.idAtrib = Null
        oCampo.EsSubCampo = False
        oCampo.Tipo = TipoNumerico
        oCampo.TipoIntroduccion = IntroLibre
        If sdbgCalculados.Columns("ID_TIPO").Value = "1" Then
            oCampo.TipoPredef = TipoCampoPredefinido.Normal
        Else
            oCampo.TipoPredef = TipoCampoPredefinido.Calculado
        End If
        
        Set oCampo.Grupo = oFSGSRaiz.Generar_CFormGrupo
        oCampo.Grupo.Id = sdbgCalculados.Columns("ID_GRUPO").Value
        Set oCampo.Grupo.Formulario = oFSGSRaiz.Generar_CFormulario
        oCampo.Grupo.Formulario.Id = m_oFormulario.Id
        
        Set oCampo.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas
        For Each oIdi In frmFormularios.m_oIdiomas
            oCampo.Denominaciones.Add oIdi.Cod, sdbgCalculados.Columns("NOMBRE").Value
        Next
        
        oCampo.IdCalculo = sdbgCalculados.Columns("ID_CALCULO").Value
        oCampo.OrdenCalculo = sdbgCalculados.Row + 1
        If sdbgCalculados.Columns("ID_TIPO").Value = "2" Then
            oCampo.Formula = sdbgCalculados.Columns("FORMULA").Value
        Else
            oCampo.Formula = Null
        End If
        oCampo.OrigenCalcDesglose = StrToNull(sdbgCalculados.Columns("ID_ORIGEN").Value)
        oCampo.EsImporteTotal = GridCheckToBoolean(sdbgCalculados.Columns("WORKFLOW").Value)
        
        Set oIBaseDatos = oCampo
        teserror = oIBaseDatos.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            sdbgCalculados.CancelUpdate
            If Me.Visible Then sdbgCalculados.SetFocus
            sdbgCalculados.DataChanged = False
        Else
            sdbgCalculados.Columns("ID").Value = oCampo.Id
            If m_oFormulario.Grupos Is Nothing Then Set m_oFormulario.Grupos = oFSGSRaiz.Generar_CFormGrupos
            If m_oFormulario.Grupos.Item(CStr(oCampo.Grupo.Id)) Is Nothing Then
                m_oFormulario.Grupos.Add oCampo.Grupo.Id, oCampo.Grupo.Denominaciones
            End If
            If m_oFormulario.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos Is Nothing Then
                Set m_oFormulario.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos = oFSGSRaiz.Generar_CFormCampos
            End If
            If m_oFormulario.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos.Item(CStr(oCampo.Id)) Is Nothing Then m_oFormulario.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, , oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , , , oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , , False, , , , , , , , oCampo.IdCalculo, oCampo.Formula, oCampo.OrdenCalculo, oCampo.EsImporteTotal, oCampo.OrigenCalcDesglose
            'Si estamos posicionados en ese grupo en frmFormularios a�adimos el campo a la grid y la colecci�n:
            If oCampo.Grupo.Id = frmFormularios.g_oGrupoSeleccionado.Id Then
                Set oCampos = oFSGSRaiz.Generar_CFormCampos
                oCampos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , , , oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , , False, , , , , , , , oCampo.IdCalculo, oCampo.Formula, oCampo.OrdenCalculo, oCampo.EsImporteTotal, oCampo.OrigenCalcDesglose
                frmFormularios.AnyadirCampos oCampos, False
                Set oCampos = Nothing
            Else
                'Esto no es necesario, al cambiar el tab recarga todos los campos, y de hecho en ocasiones casca porque ya est�n metidos los campos
                'ESTO S� ES NECESARIO PORQUE SE HACE REFERENCIA DESPU�S: Si se agrega un campo a un grupo en el que no se est� ubicado y despu�s interactuamos
                'con �l, por ejemplo marcando la columna importe, va al else que est� aqu� abajo y hace referencia a la colecci�n del formulario
                If Not frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos Is Nothing Then
                    frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , , , oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , , False, , , , , , , , oCampo.IdCalculo, oCampo.Formula, oCampo.OrdenCalculo, oCampo.EsImporteTotal, oCampo.OrigenCalcDesglose
                End If
            End If
            
            ''' Registro de acciones
            basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemAnyadir, "Id" & sdbgCalculados.Columns("ID").Value
        End If
    Else
        'Es una modificaci�n:
        Set oCampo = m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value))
        If sdbgCalculados.Columns("ID_TIPO").Value <> "3" Then
            oCampo.Formula = sdbgCalculados.Columns("FORMULA").Value
        End If
        oCampo.OrigenCalcDesglose = StrToNull(sdbgCalculados.Columns("ID_ORIGEN").Value)
        oCampo.EsImporteTotal = GridCheckToBoolean(sdbgCalculados.Columns("WORKFLOW").Value)
        If sdbgCalculados.Columns("ID_TIPO").Value = "1" Then
            oCampo.TipoPredef = TipoCampoPredefinido.Normal
        Else
            oCampo.TipoPredef = TipoCampoPredefinido.Calculado
        End If
        
        teserror = oCampo.ModificarDefinicionCalculo

        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            sdbgCalculados.CancelUpdate
            If Me.Visible Then sdbgCalculados.SetFocus
            sdbgCalculados.DataChanged = False
        Else
            'Si estamos posicionados en ese grupo en frmFormularios modificamos el campo en la grid y en la colecci�n:
            If oCampo.Grupo.Id = frmFormularios.g_oGrupoSeleccionado.Id Then
                For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                    vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                    If frmFormularios.sdbgCampos.Columns("ID").CellValue(vbm) = oCampo.Id Then
                        frmFormularios.sdbgCampos.Bookmark = vbm
                        frmFormularios.sdbgCampos.Columns("TIPO").Value = oCampo.TipoPredef
                        frmFormularios.g_bUpdate = False
                        frmFormularios.sdbgCampos.Update
                        frmFormularios.g_bUpdate = True
                        Exit For
                    End If
                Next i
            End If
            If Not frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos Is Nothing Then
                frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos.Item(CStr(oCampo.Id)).Formula = oCampo.Formula
                frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos.Item(CStr(oCampo.Id)).OrigenCalcDesglose = oCampo.OrigenCalcDesglose
                frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos.Item(CStr(oCampo.Id)).EsImporteTotal = oCampo.EsImporteTotal
                frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(oCampo.Grupo.Id)).Campos.Item(CStr(oCampo.Id)).TipoPredef = oCampo.TipoPredef
            End If

            ''' Registro de acciones
            basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemModif, "Id" & sdbgCalculados.Columns("ID").Value
        End If
    End If

    Set oCampo = Nothing
    Set oIBaseDatos = Nothing
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbgCalculados_BtnClick()
    Dim oCampo As CFormItem
    Dim oDen As CMultiidiomas

    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
    
    'Muestra la f�rmula de los campos de desglose:
    If g_sOrigen = "frmFormularios" Then
        If m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)) Is Nothing Then Exit Sub
        If m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos Is Nothing Then Exit Sub
        If m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value)) Is Nothing Then Exit Sub
        If CStr(sdbgCalculados.Columns("ORIGEN").Value) = "" Then Exit Sub
        frmFormularioDesgloseCalculo.g_bModif = g_bModif And Not m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value)).CampoEnUso(CLng(sdbgCalculados.Columns("ID").Value))
    Else
        If m_oInstancia.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)) Is Nothing Then Exit Sub
        If m_oInstancia.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos Is Nothing Then Exit Sub
        If m_oInstancia.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value)) Is Nothing Then Exit Sub
        frmFormularioDesgloseCalculo.g_bModif = g_bModif And Not m_oInstancia.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value)).CampoEnUso(CLng(sdbgCalculados.Columns("ID").Value))
    End If
    
    
    frmFormularioDesgloseCalculo.g_sOrigen = "frmFormularioCampoCalculado"
    If g_sOrigen = "frmFormularios" Then
        Set frmFormularioDesgloseCalculo.g_oCampo = m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value))

        If m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GR_ORIGEN").Value)) Is Nothing Then
            m_oFormulario.Grupos.Add sdbgCalculados.Columns("ID_GR_ORIGEN").Value, Nothing
            Set m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GR_ORIGEN").Value)).Formulario = m_oFormulario
        End If
        If m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GR_ORIGEN").Value)).Campos Is Nothing Then
            Set m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GR_ORIGEN").Value)).Campos = oFSGSRaiz.Generar_CFormCampos
        End If
        If m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GR_ORIGEN").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID_ORIGEN").Value)) Is Nothing Then
            Set oDen = oFSGSRaiz.Generar_CMultiidiomas
            oDen.Add gParametrosInstalacion.gIdioma, sdbgCalculados.Columns("ORIGEN").Value
            m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GR_ORIGEN").Value)).Campos.Add sdbgCalculados.Columns("ID_ORIGEN").Value, m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GR_ORIGEN").Value)), oDen
            Set oDen = Nothing
        End If

        Set frmFormularioDesgloseCalculo.g_oCampoDesglose = m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GR_ORIGEN").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID_ORIGEN").Value))

    Else
        Set frmFormularioDesgloseCalculo.g_oCampo = m_oInstancia.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value))
        Set oCampo = oFSGSRaiz.Generar_CFormCampo
        oCampo.IdCampoDef = sdbgCalculados.Columns("ID_ORIGEN").Value
        Set oCampo.Grupo = m_oInstancia.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value))
        Set frmFormularioDesgloseCalculo.g_oCampoDesglose = oCampo
    End If
    
    frmFormularioDesgloseCalculo.Show vbModal
End Sub
Private Sub sdbgCalculados_Change()
    Dim vbm As Variant
    Dim i As Integer
    Dim oCampo As CFormItem
    Dim sGrupos As String
    
    If sdbgCalculados.col < 0 Then Exit Sub

    If sdbgCalculados.Columns(sdbgCalculados.col).Name = "WORKFLOW" Then
        If GridCheckToBoolean(sdbgCalculados.Columns(sdbgCalculados.col).Value) = True Then
            'Primero miramos si hay algun campo que est� en uso en alguna instancia para no permitir modificar el importe
            sGrupos = "@"
            For i = 0 To sdbgCalculados.Rows - 1
                vbm = sdbgCalculados.AddItemBookmark(i)
                If InStr(sGrupos, "@" & sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm) & "@") = 0 Then
                    sGrupos = sGrupos & sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm) & "@"
                    For Each oCampo In m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm))).Campos
                        If oCampo.Id <> sdbgCalculados.Columns("ID").Value Then
                            If oCampo.ImpWorkflow Then
                                If oCampo.CampoEnUso(oCampo.Id) Then
                                    sdbgCalculados.Columns("WORKFLOW").Value = False
                                    oMensajes.BloqueoModifImporteWorkflow
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next
                End If
            Next i
            'Si se hab�a indicado otro campo como importe del workflow se quita:
            For i = 0 To sdbgCalculados.Rows - 1
                vbm = sdbgCalculados.AddItemBookmark(i)
                If sdbgCalculados.Columns("ID").CellValue(vbm) <> sdbgCalculados.Columns("ID").Value Then
                    If GridCheckToBoolean(sdbgCalculados.Columns("WORKFLOW").CellValue(vbm)) = True Then
                        sdbgCalculados.Bookmark = vbm
                        sdbgCalculados.Columns("WORKFLOW").Value = "0"
                        sdbgCalculados.Update
                        Exit For
                    End If
                End If
            Next i
        End If
    End If
End Sub
Private Sub sdbgCalculados_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim b As Boolean
    If g_bModif = False Then
        sdbgCalculados.Columns("GRUPO").Style = ssStyleEdit
        sdbgCalculados.Columns("TIPO").Style = ssStyleEdit
        sdbgCalculados.Columns("ORIGEN").Style = ssStyleEdit
        If sdbgCalculados.Columns("ID_TIPO").Value = "3" Then
            sdbgCalculados.Columns("FORMULA").Style = ssStyleEditButton
        Else
            sdbgCalculados.Columns("FORMULA").Style = ssStyleEdit
        End If
        Exit Sub
    End If
    
    If sdbgCalculados.IsAddRow Then
        sdbgCalculados.Columns("GRUPO").Style = ssStyleComboBox
        sdbgCalculados.Columns("GRUPO").DropDownHwnd = sdbddGrupos.hWnd
        sdbgCalculados.Columns("GRUPO").Locked = False
        sdbgCalculados.Columns("NOMBRE").Locked = False
    Else
        sdbgCalculados.Columns("GRUPO").Style = ssStyleEdit
        sdbgCalculados.Columns("GRUPO").Locked = True
        sdbgCalculados.Columns("NOMBRE").Locked = True
    End If
    
    If sdbgCalculados.Columns("ID_TIPO").Value = "3" Then
        sdbgCalculados.Columns("ORIGEN").Style = ssStyleComboBox
        sdbgCalculados.Columns("ORIGEN").DropDownHwnd = sdbddOrigen.hWnd
        sdbgCalculados.Columns("ORIGEN").Locked = False
        sdbgCalculados.Columns("FORMULA").Locked = True
        sdbgCalculados.Columns("FORMULA").Style = ssStyleEditButton
    Else
        sdbgCalculados.Columns("ORIGEN").Style = ssStyleEdit
        sdbgCalculados.Columns("ORIGEN").Locked = True
        
        If sdbgCalculados.Columns("ID_TIPO").Value = "1" Then
            sdbgCalculados.Columns("FORMULA").Locked = True
        Else
            sdbgCalculados.Columns("FORMULA").Locked = False
        End If
        sdbgCalculados.Columns("FORMULA").Style = ssStyleEdit
    End If
    sdbgCalculados.Columns("WORKFLOW").Locked = False
    
    'Si el campo esta en uso en una instancia no se puede modificar
    If sdbgCalculados.IsAddRow Then
        sdbgCalculados.Columns("TIPO").Style = ssStyleComboBox
        sdbgCalculados.Columns("TIPO").Locked = False
    Else
        If m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value)) Is Nothing Then
            'Si el campo no est� en la coleccion es porque lo acabamos de grabar ahora, con lo cual no puede estar en uso
            b = False
        Else
            b = m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value)).CampoEnUso(CLng(sdbgCalculados.Columns("ID").Value))
        End If
        If b Then
            sdbgCalculados.Columns("TIPO").Locked = True
            sdbgCalculados.Columns("TIPO").Style = ssStyleEdit
            sdbgCalculados.Columns("FORMULA").Locked = True
            If sdbgCalculados.Columns("ID_TIPO").Value = "3" Then
                sdbgCalculados.Columns("FORMULA").Style = ssStyleEditButton
            Else
                sdbgCalculados.Columns("FORMULA").Style = ssStyleEdit
            End If
            sdbgCalculados.Columns("ORIGEN").Locked = True
            sdbgCalculados.Columns("ORIGEN").Style = ssStyleEdit
            sdbgCalculados.Columns("WORKFLOW").Locked = True
        Else
            sdbgCalculados.Columns("TIPO").Style = ssStyleEdit
            sdbgCalculados.Columns("TIPO").Locked = True
            If Not m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)) Is Nothing Then
                If Not m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos Is Nothing Then
                    If Not m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value)) Is Nothing Then
                        If m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value)).TipoPredef = TipoCampoPredefinido.Calculado _
                           Or m_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value)).TipoPredef = TipoCampoPredefinido.Normal Then
                            sdbgCalculados.Columns("TIPO").Style = ssStyleComboBox
                            sdbgCalculados.Columns("TIPO").Locked = False
                        End If
                    End If
                End If
            End If
        End If
    End If
End Sub
