VERSION 5.00
Object = "{57A1F96E-5A81-4063-8193-6E7BB254EDBD}#1.0#0"; "DXAnimatedGIF.ocx"
Begin VB.Form frmPROCEXLSItems 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FULLSTEP"
   ClientHeight    =   2640
   ClientLeft      =   3480
   ClientTop       =   3900
   ClientWidth     =   8415
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROCEXLSItems.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2640
   ScaleWidth      =   8415
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin DXAnimatedGIF.DXGif DXGif1 
      Height          =   375
      Left            =   3780
      TabIndex        =   2
      Top             =   1080
      Width           =   435
      _ExtentX        =   767
      _ExtentY        =   661
      BackColor       =   16777215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   2610
      Picture         =   "frmPROCEXLSItems.frx":0CB2
      ScaleHeight     =   735
      ScaleWidth      =   2940
      TabIndex        =   3
      Top             =   1350
      Width           =   2940
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "D&Cancelar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3570
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   2160
      Width           =   1005
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   690
      Left            =   315
      TabIndex        =   1
      Top             =   225
      Width           =   7800
   End
End
Attribute VB_Name = "frmPROCEXLSItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public m_oItems As CItems
Public m_oMalos As CItems
Public m_oBuenos As CItems
Public lNum As Long
Public bDist As Boolean
Public bPres As Boolean

Private lNumCargados As Long
Private m_bCancel As Boolean
Private m_bCalculando As Boolean

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bCancel = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEXLSItems", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Calcular()
    Dim teserror As TipoErrorSummit
    Dim oItem As CItem
    Dim oItems As CItems
    Dim oItemsMod As CItems
    Dim i As Long
    Dim sMensaje As String
    Dim oproce As CProceso
    Dim bPermProcMultiMaterial As Boolean
    Dim bSincArt As Boolean
    Dim oEscalado As CEscalado
    

    ''
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEPermProcMultimaterial)) Is Nothing) Then
            If gParametrosGenerales.gbMultiMaterial Then
                bPermProcMultiMaterial = True
            Else
                bPermProcMultiMaterial = False
            End If
        Else
            bPermProcMultiMaterial = False
        End If
    Else
        If gParametrosGenerales.gbMultiMaterial Then
            bPermProcMultiMaterial = True
        Else
            bPermProcMultiMaterial = False
        End If
    End If
    ''
    
    bSincArt = False
    lNumCargados = 0
    Set m_oBuenos = oFSGSRaiz.Generar_CItems
    
    Screen.MousePointer = vbHourglass
    
    
    For i = 1 To m_oItems.Count
    
        Label1.caption = m_oItems.Item(i).ArticuloCod & " - " & m_oItems.Item(i).Descr
        DoEvents
        Set oItem = m_oItems.Item(i)
        Set oItems = oFSGSRaiz.Generar_CItems
        Set oItemsMod = oFSGSRaiz.Generar_CItems
                
        If oItem.Id = 0 Then
            'No existe.
            
            oItems.Add oItem.proceso, 0, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , oItem.esp, , oItem.ProveAct, , oItem.GrupoCod, , oItem.SolicitudId, lGrupoID:=oItem.GrupoID, oAtributosEspecificacion:=oItem.AtributosEspecificacion
            oItems.Item(oItems.Count).GMN1Cod = oItem.GMN1Cod
            oItems.Item(oItems.Count).GMN2Cod = oItem.GMN2Cod
            oItems.Item(oItems.Count).GMN3Cod = oItem.GMN3Cod
            oItems.Item(oItems.Count).GMN4Cod = oItem.GMN4Cod
        
            'Si los atributos de especificación no tienen valor se asigna el valor del atributo del artículo
            ComprobarValoresAtributosEspecificacion oItems.Item(oItems.Count)
            
            If oItem.UsarEscalados Then
                Set oItems.Item(oItems.Count).Escalados = oFSGSRaiz.Generar_CEscalados
                For Each oEscalado In oItem.Escalados
                    oItems.Item(oItems.Count).Escalados.Add oEscalado.Id, oEscalado.Inicial, oEscalado.final, oEscalado.Presupuesto
                Next
                oItems.Item(oItems.Count).UsarEscalados = True
            End If
            Set oItems.Item(oItems.Count).proceso = oItem.proceso
            
            
        Else
            'Existe, se trata de una modificación.
            
            oItemsMod.Add oItem.proceso, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , oItem.esp, , oItem.ProveAct, , oItem.GrupoCod, , oItem.SolicitudId, lGrupoID:=oItem.GrupoID, oAtributosEspecificacion:=oItem.AtributosEspecificacion
            oItemsMod.Item(oItemsMod.Count).GMN1Cod = oItem.GMN1Cod
            oItemsMod.Item(oItemsMod.Count).GMN2Cod = oItem.GMN2Cod
            oItemsMod.Item(oItemsMod.Count).GMN3Cod = oItem.GMN3Cod
            oItemsMod.Item(oItemsMod.Count).GMN4Cod = oItem.GMN4Cod
            If oItem.UsarEscalados Then
                Set oItemsMod.Item(oItemsMod.Count).Escalados = oFSGSRaiz.Generar_CEscalados
                For Each oEscalado In oItem.Escalados
                    oItemsMod.Item(oItemsMod.Count).Escalados.Add oEscalado.Id, oEscalado.Inicial, oEscalado.final, oEscalado.Presupuesto
                Next
                oItemsMod.Item(oItemsMod.Count).UsarEscalados = True
            End If
            Set oItemsMod.Item(oItemsMod.Count).proceso = oItem.proceso
            oItemsMod.Item(oItemsMod.Count).FECACT = oItem.FECACT
        End If
        
        
        
        DoEvents
        
        
        
        If oItem.Id = 0 Then
                
            teserror = oItems.AnyadirMultiplesItems(, , bDist, IIf(frmPROCE.g_oDistsNivel1Item Is Nothing, oItem.DistsNivel1, frmPROCE.g_oDistsNivel1Item), IIf(frmPROCE.g_oDistsNivel2Item Is Nothing, oItem.DistsNivel2, frmPROCE.g_oDistsNivel2Item), _
                                                    IIf(frmPROCE.g_oDistsNivel3Item Is Nothing, oItem.DistsNivel3, frmPROCE.g_oDistsNivel3Item), bPres, frmPROCE.g_oPresupuestos1ItemModif, frmPROCE.g_oPresupuestos2ItemModif, frmPROCE.g_oPresupuestos3ItemModif, frmPROCE.g_oPresupuestos4ItemModif, , , basOptimizacion.gvarCodUsuario, bPermProcMultiMaterial)
            
        Else
    '        teserror = oItemsMod.ModificarValores(False, False, False, 1, , , , , True, True, , True, , basOptimizacion.gvarCodUsuario)
            teserror = oItemsMod.ModificarValores(oEscalados:=oItem.Escalados, bImportarExcel:=True)
            Dim oatrib As Catributo
            For Each oatrib In oItem.AtributosEspecificacion
                'El atributo se sincroniza en la cabecera
                If Not frmPROCE.g_oProcesoSeleccionado.Grupos.Item(oItem.GrupoCod).AtributosDefinicionEspecItem Is Nothing Then
                    bSincArt = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(oItem.GrupoCod).AtributosDefinicionEspecItem.Item(oatrib.Atrib).SincronizarArticulo
                End If
                'Si no se sincroniza en la cabecera, veamos si lo hace para ese artículo en concreto.
                If Not bSincArt Then
                    If Not frmPROCE.g_oProcesoSeleccionado.Grupos.Item(oItem.GrupoCod).Items.Item(CStr(oItem.Id)).AtributosEspecificacion Is Nothing Then
                    If Not frmPROCE.g_oProcesoSeleccionado.Grupos.Item(oItem.GrupoCod).Items.Item(CStr(oItem.Id)).AtributosEspecificacion.Item(oatrib.Atrib) Is Nothing Then
                        bSincArt = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(oItem.GrupoCod).Items.Item(CStr(oItem.Id)).AtributosEspecificacion.Item(oatrib.Atrib).SincronizarArticulo
                    End If
                    End If
                End If
                oatrib.SincronizarArticulo = bSincArt
                Select Case oatrib.Tipo
                Case TipoFecha
                    oItem.ActualizarAtributoXLS oatrib, oatrib.valorFec, frmPROCE.m_bModAtrib, oatrib.interno, oatrib.pedido
                Case TipoBoolean
                    oItem.ActualizarAtributoXLS oatrib, oatrib.valorBool, frmPROCE.m_bModAtrib, oatrib.interno, oatrib.pedido
                Case TipoString
                    oItem.ActualizarAtributoXLS oatrib, oatrib.valorText, frmPROCE.m_bModAtrib, oatrib.interno, oatrib.pedido
                Case Else
                    oItem.ActualizarAtributoXLS oatrib, oatrib.valorNum, frmPROCE.m_bModAtrib, oatrib.interno, oatrib.pedido
                End Select
            Next
            'actualizamos las unidades organizativas de la distribución, con los datos que haya cargados en oitem sim la distribución es de item
            If frmPROCE.g_oProcesoSeleccionado.DefDistribUON = EnItem Then oItem.updateDistribucion
        End If
        
        'Si la fecha de necesidad no ha sido modificada por el usuario se puede cargar una fecha de inicio suministro anterior
        'y se actualizará la fecha de necesidada del proceso.
        If frmPROCE.g_oProcesoSeleccionado.FechaNecesidad > oItem.FechaInicioSuministro Then
            frmPROCE.g_oProcesoSeleccionado.FechaNecesidad = oItem.FechaInicioSuministro
            If frmPROCE.m_oIBAseDatosEnEdicionProceso Is Nothing Then
                Set frmPROCE.m_oIBAseDatosEnEdicionProceso = frmPROCE.g_oProcesoSeleccionado
            End If
            teserror = frmPROCE.m_oIBAseDatosEnEdicionProceso.FinalizarEdicionModificando
            If teserror.NumError = TESDatoEliminado Then
                If teserror.Arg1 = 47 Then
                    Screen.MousePointer = vbNormal
                    oMensajes.ImposibleModMatProce
                    Exit Sub
                End If
            End If
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
            End If
        End If
        DoEvents
        
        If teserror.NumError <> TESnoerror Then
            If oItem.proceso.PermSaltarseVolMaxAdjDir = True And teserror.NumError = TESVolumenAdjDirSuperado Then
                lNumCargados = lNumCargados + 1
                If oItem.Id = 0 Then
                    m_oBuenos.Add oItem.proceso, oItems.Item(1).Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , oItem.esp, , oItem.ProveAct, , oItem.GrupoCod, , oItem.SolicitudId, lGrupoID:=oItem.GrupoID
                Else
                    m_oBuenos.Add oItem.proceso, oItemsMod.Item(1).Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , oItem.esp, , oItem.ProveAct, , oItem.GrupoCod, , oItem.SolicitudId, lGrupoID:=oItem.GrupoID
                End If
            Else
                If teserror.NumError = TESVolumenAdjDirSuperado Then
                    sMensaje = oMensajes.CargarTextoMensaje(79) & vbLf & oMensajes.CargarTextoMensaje(80) & " " & DblToStr(teserror.Arg1) & vbLf & oMensajes.CargarTextoMensaje(81) & " " & DblToStr(teserror.Arg2)
                Else
                    sMensaje = oMensajes.CargarTextoMensaje(72) & vbLf & oMensajes.CargarTextoMensaje(73) & " " & teserror.NumError & vbLf & oMensajes.CargarTextoMensaje(74) & " " & teserror.Arg1 & vbLf & oMensajes.CargarTextoMensaje(75)
                End If
                'En el ID meto la fila de la excel y en el destino el numero de mensaje y los cargo con indice iNumItemsMalos
                lNum = lNum + 1
                m_oMalos.Add oproce, i + 1, sMensaje, 0, 0, 0, 0, 0, m_oItems.Item(i).ArticuloCod, m_oItems.Item(i).Descr, , , lNum, , , , , , , , , m_oItems.Item(i).GrupoID
            End If
        Else
            lNumCargados = lNumCargados + 1
            If oItem.Id = 0 Then
                m_oBuenos.Add oItem.proceso, oItems.Item(1).Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , oItem.esp, , oItem.ProveAct, , oItem.GrupoCod, , oItem.SolicitudId, lGrupoID:=oItem.GrupoID
            Else
                m_oBuenos.Add oItem.proceso, oItemsMod.Item(1).Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , oItem.esp, , oItem.ProveAct, , oItem.GrupoCod, , oItem.SolicitudId, lGrupoID:=oItem.GrupoID
            End If
        End If
        DoEvents
        Set oItems = Nothing
        Set oItemsMod = Nothing
        Set oItem = Nothing
        If m_bCancel Then
            Exit For
        End If
    Next
    Screen.MousePointer = vbNormal
    If m_bCancel Then
        oMensajes.MensajeOKOnly 749
    End If
    
    frmPROCEXLSResumen.g_lNumBuenos = lNumCargados
    frmPROCEXLSResumen.g_lNumMalos = lNum
    Set frmPROCEXLSResumen.g_oItemBuenos = m_oBuenos
    Set frmPROCEXLSResumen.g_oItemMalos = m_oMalos
    frmPROCEXLSResumen.Show 1
    
    Set m_oMalos = Nothing
    Set m_oBuenos = Nothing
    Set m_oItems = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEXLSItems", "Calcular", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Comprueba los valores de los atributos de especificación.
'''          Si los atributos de especificación no tienen valor se asigna el valor del atributo del artículo</summary>
''' <param name="oItem">Objeto item</param>
''' <remarks>Llamada desde: Calcular</remarks>
''' <revision>LTG 11/06/2014</revision>

Private Sub ComprobarValoresAtributosEspecificacion(ByRef oItem As CItem)
    Dim oArt As CArticulo
    Dim oAtribArt As CAtributos
    Dim oatrib As Catributo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oArt = oFSGSRaiz.Generar_CArticulo
    oArt.GMN1Cod = oItem.GMN1Cod
    oArt.GMN2Cod = oItem.GMN2Cod
    oArt.GMN3Cod = oItem.GMN3Cod
    oArt.GMN4Cod = oItem.GMN4Cod
    oArt.Cod = oItem.ArticuloCod
    Set oAtribArt = oArt.DevolverTodosLosAtributosDelArticulo(, True)
    
    'Comprobar los valores de los atributos
    If Not oItem.AtributosEspecificacion Is Nothing Then
        If oItem.AtributosEspecificacion.Count > 0 Then
            For Each oatrib In oItem.AtributosEspecificacion
                Select Case oatrib.Tipo
                    Case TipoFecha
                        If IsNull(oatrib.valorFec) Or IsEmpty(oatrib.valorFec) Then
                            If Not oAtribArt.Item(oatrib.Id) Is Nothing Then oatrib.valorFec = oAtribArt.Item(oatrib.Id).valorFec
                        End If
                    Case TipoBoolean
                        If IsNull(oatrib.valorBool) Or IsEmpty(oatrib.valorBool) Then
                            If Not oAtribArt.Item(oatrib.Id) Is Nothing Then oatrib.valorBool = oAtribArt.Item(oatrib.Id).valorBool
                        End If
                    Case TipoString
                        If IsNull(oatrib.valorText) Or IsEmpty(oatrib.valorText) Then
                            If Not oAtribArt.Item(oatrib.Id) Is Nothing Then oatrib.valorText = oAtribArt.Item(oatrib.Id).valorText
                        End If
                    Case Else
                        If IsNull(oatrib.valorNum) Or IsEmpty(oatrib.valorNum) Then
                            If Not oAtribArt.Item(oatrib.Id) Is Nothing Then oatrib.valorNum = oAtribArt.Item(oatrib.Id).valorNum
                        End If
                End Select
            Next
        Else
            Set oItem.AtributosEspecificacion = oAtribArt
        End If
    Else
        Set oItem.AtributosEspecificacion = oAtribArt
    End If
    
    Set oArt = Nothing
    Set oAtribArt = Nothing
    Set oatrib = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEXLSItems", "ComprobarValoresAtributosEspecificacion", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

If Not m_bCalculando Then
    m_bCalculando = True
    Calcular
    Unload Me
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEXLSItems", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.DXGif1.filename = App.Path & "\cursor-espera_grande.gif"

    Me.caption = oMensajes.CargarTexto(OTROS, 164)
    cmdCancelar.caption = frmPROCE.cmdCancelar.caption
    
    m_bCancel = False
    m_bCalculando = False
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEXLSItems", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub




Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEXLSItems", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


