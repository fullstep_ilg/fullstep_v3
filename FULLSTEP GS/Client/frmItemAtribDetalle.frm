VERSION 5.00
Object = "{1E47E980-2496-11D3-ACB1-C0A64FC10000}#1.52#0"; "Pajant.dll"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmItemAtribDetalle 
   BackColor       =   &H00808000&
   Caption         =   "DDatos del art�culo"
   ClientHeight    =   9345
   ClientLeft      =   1890
   ClientTop       =   1755
   ClientWidth     =   7215
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmItemAtribDetalle.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9345
   ScaleWidth      =   7215
   Begin VB.Frame Frame3 
      BackColor       =   &H00808000&
      Caption         =   "DImagen"
      ForeColor       =   &H8000000E&
      Height          =   4065
      Left            =   3720
      TabIndex        =   36
      Top             =   5160
      Width           =   3465
      Begin PAJANTLibCtl.PajantImageCtrl PajantImagen 
         Height          =   3705
         Left            =   75
         OleObjectBlob   =   "frmItemAtribDetalle.frx":0CB2
         TabIndex        =   37
         Top             =   240
         Width           =   3300
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Caption         =   "DDatos"
      ForeColor       =   &H8000000E&
      Height          =   4065
      Left            =   75
      TabIndex        =   15
      Top             =   5160
      Width           =   3465
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "DAtributo5"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   35
         Top             =   1800
         Visible         =   0   'False
         Width           =   885
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "DAtributo4"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   34
         Top             =   1440
         Visible         =   0   'False
         Width           =   885
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "DAtributo3"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   33
         Top             =   1080
         Visible         =   0   'False
         Width           =   885
      End
      Begin VB.Label Label13 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   975
         TabIndex        =   32
         Top             =   1020
         Visible         =   0   'False
         Width           =   2400
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "DAtributo2"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   31
         Top             =   720
         Visible         =   0   'False
         Width           =   885
      End
      Begin VB.Label Labe12 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   975
         TabIndex        =   30
         Top             =   660
         Visible         =   0   'False
         Width           =   2400
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "DAtributo1"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   29
         Top             =   360
         Visible         =   0   'False
         Width           =   885
      End
      Begin VB.Label Label11 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   975
         TabIndex        =   28
         Top             =   300
         Visible         =   0   'False
         Width           =   2400
      End
      Begin VB.Label Label15 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   975
         TabIndex        =   27
         Top             =   1740
         Visible         =   0   'False
         Width           =   2400
      End
      Begin VB.Label Label14 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   975
         TabIndex        =   26
         Top             =   1380
         Visible         =   0   'False
         Width           =   2400
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "DAtributo10"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   25
         Top             =   3600
         Visible         =   0   'False
         Width           =   885
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "DAtributo9"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   24
         Top             =   3240
         Visible         =   0   'False
         Width           =   885
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "DAtributo8"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   23
         Top             =   2880
         Visible         =   0   'False
         Width           =   885
      End
      Begin VB.Label Label18 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   975
         TabIndex        =   22
         Top             =   2820
         Visible         =   0   'False
         Width           =   2400
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "DAtributo7"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   21
         Top             =   2520
         Visible         =   0   'False
         Width           =   885
      End
      Begin VB.Label Label17 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   975
         TabIndex        =   20
         Top             =   2460
         Visible         =   0   'False
         Width           =   2400
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "DAtributo6"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   19
         Top             =   2160
         Visible         =   0   'False
         Width           =   885
      End
      Begin VB.Label Label16 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   975
         TabIndex        =   18
         Top             =   2100
         Visible         =   0   'False
         Width           =   2400
      End
      Begin VB.Label Label20 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   975
         TabIndex        =   17
         Top             =   3540
         Visible         =   0   'False
         Width           =   2400
      End
      Begin VB.Label Label19 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   975
         TabIndex        =   16
         Top             =   3180
         Visible         =   0   'False
         Width           =   2400
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Caption         =   "frmItemAtribDetalle.Fram"
      ForeColor       =   &H8000000E&
      Height          =   3615
      Left            =   75
      TabIndex        =   0
      Top             =   0
      Width           =   7065
      Begin VB.TextBox lblEsp 
         BackColor       =   &H80000018&
         Height          =   645
         Left            =   2250
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   1720
         Width           =   4665
      End
      Begin VB.Label lblCodCentral 
         BackStyle       =   0  'Transparent
         Caption         =   "Cod. Central:"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   120
         TabIndex        =   49
         Top             =   3120
         Width           =   975
      End
      Begin VB.Label txtCodCentral 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Enabled         =   0   'False
         Height          =   285
         Left            =   1110
         TabIndex        =   48
         Top             =   3120
         Width           =   1065
      End
      Begin VB.Label lblProAdjCod 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1110
         TabIndex        =   47
         Top             =   2445
         Width           =   570
      End
      Begin VB.Label lblProAdj 
         BackStyle       =   0  'Transparent
         Caption         =   "DProveedor adjudicado:"
         ForeColor       =   &H00FFFFFF&
         Height          =   360
         Left            =   120
         TabIndex        =   46
         Top             =   2400
         Width           =   885
      End
      Begin VB.Label lblProAdjDen 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1695
         TabIndex        =   45
         Top             =   2440
         Width           =   5205
      End
      Begin VB.Label lblMoneda 
         BackStyle       =   0  'Transparent
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   4680
         TabIndex        =   44
         Top             =   2830
         Width           =   675
      End
      Begin VB.Label lblPrecioAdjud 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   3300
         TabIndex        =   43
         Top             =   2800
         Width           =   1245
      End
      Begin VB.Label lblPrecioAdj 
         BackStyle       =   0  'Transparent
         Caption         =   "DPrecioAdjun:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   2250
         TabIndex        =   42
         Top             =   2830
         Width           =   1095
      End
      Begin VB.Label lblCantAdj 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1110
         TabIndex        =   41
         Top             =   2805
         Width           =   1065
      End
      Begin VB.Label lblCantA 
         BackStyle       =   0  'Transparent
         Caption         =   "DCant.Adj.:"
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Left            =   150
         TabIndex        =   40
         Top             =   2835
         Width           =   855
      End
      Begin VB.Label lblProce 
         BackStyle       =   0  'Transparent
         Caption         =   "DProceso:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   39
         Top             =   300
         Width           =   885
      End
      Begin VB.Label lblProceso 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1110
         TabIndex        =   38
         Top             =   300
         Width           =   5805
      End
      Begin VB.Label lblFecFinl 
         BackStyle       =   0  'Transparent
         Caption         =   "DFin:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   13
         Top             =   2140
         Width           =   885
      End
      Begin VB.Label lblFecFin 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1110
         TabIndex        =   12
         Top             =   2085
         Width           =   1080
      End
      Begin VB.Label lblFecInil 
         BackStyle       =   0  'Transparent
         Caption         =   "DInicio:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   11
         Top             =   1780
         Width           =   885
      End
      Begin VB.Label lblFecIni 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1110
         TabIndex        =   10
         Top             =   1725
         Width           =   1080
      End
      Begin VB.Label lblDenPag 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1695
         TabIndex        =   9
         Top             =   1360
         Width           =   5205
      End
      Begin VB.Label lblCodPagl 
         BackStyle       =   0  'Transparent
         Caption         =   "DPago:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   8
         Top             =   1420
         Width           =   885
      End
      Begin VB.Label lblDenUni 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1695
         TabIndex        =   7
         Top             =   1000
         Width           =   5205
      End
      Begin VB.Label lblCodUnil 
         BackStyle       =   0  'Transparent
         Caption         =   "DUnidad:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   6
         Top             =   1060
         Width           =   885
      End
      Begin VB.Label lblDenDest 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1695
         TabIndex        =   5
         Top             =   640
         Width           =   5205
      End
      Begin VB.Label lblCodDestl 
         BackStyle       =   0  'Transparent
         Caption         =   "DDestino:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   4
         Top             =   700
         Width           =   885
      End
      Begin VB.Label lblCodPag 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1110
         TabIndex        =   3
         Top             =   1365
         Width           =   570
      End
      Begin VB.Label lblCodUni 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1110
         TabIndex        =   2
         Top             =   1005
         Width           =   570
      End
      Begin VB.Label lblCodDest 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1110
         TabIndex        =   1
         Top             =   645
         Width           =   570
      End
   End
   Begin SSDataWidgets_B.SSDBGrid ssdbArticulosAgregados 
      Height          =   1215
      Left            =   240
      TabIndex        =   50
      Top             =   3840
      Width           =   6825
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   3
      stylesets.count =   4
      stylesets(0).Name=   "Yellow"
      stylesets(0).BackColor=   11862015
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmItemAtribDetalle.frx":0CD6
      stylesets(1).Name=   "Gris"
      stylesets(1).BackColor=   -2147483633
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmItemAtribDetalle.frx":0CF2
      stylesets(2).Name=   "Normal"
      stylesets(2).ForeColor=   0
      stylesets(2).BackColor=   16777215
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmItemAtribDetalle.frx":0D0E
      stylesets(3).Name=   "Header"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmItemAtribDetalle.frx":0D2A
      AllowDelete     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      HeadStyleSet    =   "Header"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3201
      Columns(0).Caption=   "UON"
      Columns(0).Name =   "UON"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "COD"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "UONKEY"
      Columns(2).Name =   "UONKEY"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   12039
      _ExtentY        =   2143
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmItemAtribDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmItemAtribDetalle
''' *** Creacion: 27/07/2001


Option Explicit

Public sOrigen As String
Public DenArt As String
Public CodProve As String
Public ArItem As Variant
Public CodArt As String
Public bError As Boolean
Public g_bEliminado  As Boolean
Public g_bAceptoSinImagen As Boolean
Public g_sGmn1 As String
Public g_iIdEmpresa As Integer
Public bImagen As Boolean
Public bAtribs As Boolean
Private m_bMostrarUons As Boolean
'Proveedor adjudicado
Public CodProveAdj As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
'Separadores
Private Const lSeparador = 127

Private m_oArticulo As CArticulo
Private m_oAtributos As CAtributos
Private m_oUons As CUnidadesOrganizativas

Public m_bDescargarFrm As Boolean

Dim m_bActivado As Boolean

Private m_bUnload As Boolean



Private m_sMsgError As String
Public Sub cargarArticulo()
    
    Dim oArticulos As CArticulos
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
    oArticulos.DevolverArticulosDesde 1, , , , , , CodArt
    Set m_oArticulo = oArticulos.Item(1)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemAtribDetalle", "cargarArticulo", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


Public Function cargarDatosEmpresa()
'obtenemos la uon de la empresa y a partir de ella las uon del art�culo que se correspondan con ella
    Dim oUons As CUnidadesOrganizativas
    Dim oUON As IUon
    Dim oUonArt As IUon
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
    oUons.cargarUonsEmpresa (g_iIdEmpresa)
    For Each oUON In oUons
        For Each oUonArt In m_oArticulo.uons
            If (oUON.incluye(oUonArt) Or oUonArt.incluye(oUON)) And Not m_oUons.existe(oUonArt.key) Then
                m_oUons.Add oUonArt
            End If
        Next
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemAtribDetalle", "cargarDatosEmpresa", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
''' <summary>Carga de los literales multiidioma</summary>
''' <remarks>Llamada desde:frmItemAtribDetalle.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub CargarRecursos()

Dim Ador As Ador.Recordset


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ITEMATRIBDETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value '1
        Ador.MoveNext
        lblCodDestl.caption = Ador(0).Value
        Ador.MoveNext
        lblCodUnil.caption = Ador(0).Value
        Ador.MoveNext
        lblCodPagl.caption = Ador(0).Value
        Ador.MoveNext
        lblFecInil.caption = Ador(0).Value '5
        Ador.MoveNext
        lblFecFinl.caption = Ador(0).Value
        Ador.MoveNext
        Frame1.caption = Ador(0).Value
        Ador.MoveNext
        Frame2.caption = Ador(0).Value
        Ador.MoveNext
        Frame3.caption = Ador(0).Value
        Ador.MoveNext
        lblProce.caption = Ador(0).Value
        Ador.MoveNext
        lblCantA.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblPrecioAdj.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblProAdj.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value
        Ador.MoveNext
        Me.txtCodCentral.caption = Ador(0).Value
        Ador.Close
    
    End If


    Set Ador = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemAtribDetalle", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub Form_Activate()
   If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    Arrange
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    
    If err.Number <> 0 Then
        m_bUnload = True
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemAtribDetalle", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
   End If
End Sub

Private Sub Form_Initialize()
    Set m_oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
    Set m_oAtributos = oFSGSRaiz.Generar_CAtributos
End Sub

''' <summary>
''' Evento que salta al llamar al formulario.
''' carga los idiomas del formulario
''' carga la informacion del proceso
''' carga la informacion de las combos de atributos
''' carga la informacion de las imagenes
''' </summary>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Sub Form_Load()
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    m_bDescargarFrm = False
    m_bActivado = False
    m_bUnload = False
    
    PonerFieldSeparator Me

    'Tarea 3299
    
    
    CargarRecursos
    cargarArticulo
    If g_iIdEmpresa <> 0 Then
        m_oUons.clear
        Set m_oAtributos = Nothing
        Set m_oAtributos = oFSGSRaiz.Generar_CAtributos
        cargarDatosEmpresa
        CargarArticulosAgregados
        montarColumnasGridAtributosUon
        cargarValoresGridArticulosAgregados
    End If
    Me.txtCodCentral.caption = m_oArticulo.CodArticuloCentral

    DoEvents
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemAtribDetalle", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Procedimiento que el tamanyo y posicion de los controles del formulario cuando se cambian las dimensiones del mismo
''' </summary>
''' <remarks>Llamada desde el evento Resize del formulario: form_resize; Tiempo m�ximo < 1 seg.</remarks>
Private Sub Arrange()
    Dim Fl As Long
    Dim teserror As TipoErrorSummit
    Dim imgBD As Variant
    Dim oImagen As CImagen
    
    
    'Me.Width = 7305

    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    Me.txtCodCentral.Top = lblCantAdj.Top + txtCodCentral.Height + 10
    Me.lblCodCentral.Top = Me.txtCodCentral.Top
    
    If Me.Frame1.Visible And Not m_bMostrarUons Then
        Me.Frame2.Top = Me.ssdbArticulosAgregados.Top
        Me.Frame3.Top = Me.ssdbArticulosAgregados.Top
    ElseIf Me.Frame1.Visible And g_iIdEmpresa <> 0 Then
        Me.ssdbArticulosAgregados.Top = Me.Frame1.Top + Me.Frame1.Height + 10
        Me.Frame2.Top = Me.ssdbArticulosAgregados.Top + Me.ssdbArticulosAgregados.Height + 50
        Me.Frame3.Top = Me.ssdbArticulosAgregados.Top + Me.ssdbArticulosAgregados.Height + 50
    ElseIf Me.Frame1.Visible And Not (g_iIdEmpresa <> 0) Then
        Me.ssdbArticulosAgregados.Visible = False
        Me.Frame2.Top = Me.Frame1.Top + Me.Frame1.Height + 10
        Me.Frame3.Top = Me.Frame1.Top + Me.Frame1.Height + 10
    ElseIf g_iIdEmpresa <> 0 Then
        Me.ssdbArticulosAgregados.Top = Me.Frame2.Top
        Me.Frame2.Top = Me.ssdbArticulosAgregados.Top + Me.ssdbArticulosAgregados.Height + 50
        Me.Frame3.Top = Me.ssdbArticulosAgregados.Top + Me.ssdbArticulosAgregados.Height + 50
    Else
        Me.ssdbArticulosAgregados.Visible = False
        Me.Frame2.Top = Me.Frame1.Top
        Me.Frame3.Top = Me.Frame1.Top
    End If
    
    If bImagen Then
        '    Comienza la lectura de los datos de la imagen
        Set oImagen = oFSGSRaiz.Generar_CImagen
        oImagen.Articulo.Cod = CodArt
        oImagen.Proveedor = CodProve
        
        If oImagen.Articulo.Cod <> "" Then
            If sOrigen = "GRANDE" And Not g_bEliminado Then
                teserror = oImagen.ComenzarLecturaData
                If teserror.NumError <> TESnoerror Then
                    If teserror.NumError = TESDatoEliminado Then
                        If Not g_bAceptoSinImagen Then
                            oMensajes.NoExistenValoresAtributosDeArticuloProve CodArt, CodProve
                            g_bEliminado = True
                        End If
                    Else
                        basErrores.TratarError teserror
                    End If
                End If
            End If
        End If
        
        Fl = oImagen.DataSize
        imgBD = oImagen.ReadData(Fl)
        
        oImagen.FinalizarLecturaData
        
        'Muestra la imagen
        PajantImagen.pi.Register "FULLSTEP NETWORKS S.L.", "3C91D6B17E242021664AB9C6"
    
        If IsNull(imgBD) Then
            PajantImagen.pi.DisplayMode = PJT_NONE
        Else
            PajantImagen.pi.DisplayMode = PJT_SCROLL
        End If
    
        'Fondo verde de la imagen
        PajantImagen.pi.SetBackground &H808000, 0
         
        PajantImagen.pi.ImportFrom PJT_MEMPNG, 0, 0, imgBD, PJT_TONEWIMAGE
        
        'Me.ScaleMode = vbPixels
        
        Set oImagen = Nothing
        
        If IsNull(imgBD) Then
            PajantImagen.Visible = False
        End If
        
        Me.Height = IIf(Me.Frame1.Visible, Me.Frame1.Height, 0) + Me.Frame2.Height + 720 + IIf(Me.ssdbArticulosAgregados.Visible, Me.ssdbArticulosAgregados.Height, 150)
        Me.Frame2.Visible = True
        Me.Frame3.Visible = True
    ElseIf bAtribs Then
        Me.Height = IIf(Me.Frame1.Visible, Me.Frame1.Height, 0) + Me.Frame2.Height + 720 + IIf(Me.ssdbArticulosAgregados.Visible, Me.ssdbArticulosAgregados.Height, 150)
        Me.Frame2.Visible = True
        Me.Frame3.Visible = True
    Else
        Me.Height = IIf(Me.Frame1.Visible, Me.Frame1.Height, 0) + 670 + IIf(Me.ssdbArticulosAgregados.Visible, Me.ssdbArticulosAgregados.Height + 160, 0)
        Me.Frame2.Visible = False
        Me.Frame3.Visible = False
    End If

    Exit Sub
Error:
    
    MsgBox err.Description, vbCritical + vbOKOnly, "FULLSTEP"

End Sub

''' <summary>
''' Procedimiento que vac�a de contenido una serie de variables y objetos usados en el formulario
''' </summary>
''' <remarks>Llamada desde el evento Unload del formulario; Tiempo m�ximo < 1 seg.</remarks>
Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sOrigen = ""
    DenArt = ""
    CodProve = ""
    ArItem = Null
    CodArt = ""
    bError = False
    g_bEliminado = False
    g_bAceptoSinImagen = False
    'ocultamos el grid de atributos al descargar, ya que no se crea una nueva instancia del form
    Me.mostrarAtributosuon = False
    
    bImagen = False
    bAtribs = False
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemAtribDetalle", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub CargarArticulosAgregados()
    Dim oUON As IUon
        
    Dim sLinea As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.ssdbArticulosAgregados.RemoveAll
    For Each oUON In m_oUons
        sLinea = oUON.titulo & Chr(lSeparador)
        sLinea = sLinea & "" & Chr(lSeparador) 'La columna de codigo de planta se deja vac�a y luego se oculta
        sLinea = sLinea & oUON.key ' codificacion de uon
        ssdbArticulosAgregados.AddItem sLinea
    Next
    Set oUON = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemAtribDetalle", "CargarArticulosAgregados", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
Private Sub montarColumnasGridAtributosUon()
    Dim icolumns As Integer
    Dim oColumn As SSDataWidgets_B.Column
    
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    icolumns = ssdbArticulosAgregados.Columns.Count
    Dim oAtributo As CAtributo
    
    For Each oAtributo In m_oArticulo.getAtributosUon
        If Not m_oAtributos.existe(oAtributo.Id) Then
            m_oAtributos.addAtributo oAtributo
            ssdbArticulosAgregados.Columns.Add icolumns
            Set oColumn = ssdbArticulosAgregados.Columns(icolumns)
            oColumn.Name = oAtributo.Id
            oColumn.caption = oAtributo.Den
            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
            oColumn.Alignment = ssCaptionAlignmentLeft
            oColumn.Style = ssStyleEdit
            oColumn.Locked = True
            oColumn.FieldLen = 100
            m_oAtributos.addAtributo oAtributo
            icolumns = icolumns + 1
            Set oColumn = Nothing
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemAtribDetalle", "montarColumnasGridAtributosUon", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub cargarValoresGridArticulosAgregados()
        Dim oAtributo As CAtributo
    Dim oAtributos As CAtributos
    Dim oArticulo As CArticulo
    Dim i, j As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    LockWindowUpdate Me.hWnd
    If m_oArticulo.isCentral Then
        Me.ssdbArticulosAgregados.MoveFirst
        For i = 0 To ssdbArticulosAgregados.Rows - 1
            Set oArticulo = m_oArticulo.articulosAgregados.Item(ssdbArticulosAgregados.Columns("COD").Value)
            Set oAtributos = oArticulo.AtributosUonValorados
            For j = 3 To ssdbArticulosAgregados.Cols - 1
                If oAtributos.existe(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value) Then
                    Set oAtributo = oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value)
                    If oAtributo.Tipo = TipoBoolean Then
                        If oAtributo.getValor = 1 Then
                            ssdbArticulosAgregados.Columns(j).Value = m_sIdiTrue
                        Else
                            ssdbArticulosAgregados.Columns(j).Value = m_sIdiFalse
                        End If
                    Else
                        ssdbArticulosAgregados.Columns(j).Value = oAtributo.getValor
                    End If
                Else
                    'si no existe le damos el valor por defecto (guardado en atributosuon)
                    Set oAtributo = m_oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name)
                    If oAtributo.uons.CONTIENE(m_oUons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)) Then
                        If oAtributo.Tipo = TipoBoolean Then
                            If oAtributo.getValor = 1 Then
                                ssdbArticulosAgregados.Columns(j).Text = m_sIdiTrue
                            Else
                                ssdbArticulosAgregados.Columns(j).Text = m_sIdiFalse
                            End If
                        Else
                            ssdbArticulosAgregados.Columns(j).Text = NullToStr(oAtributo.getValor)
                        End If
                    End If
                    Set oAtributo = Nothing
                End If
                
            Next
            
            ssdbArticulosAgregados.MoveNext
        Next
    Else
        Me.ssdbArticulosAgregados.MoveFirst
        Set oAtributos = m_oArticulo.AtributosUonValorados
        For i = 0 To ssdbArticulosAgregados.Rows - 1
            
            For j = 3 To ssdbArticulosAgregados.Cols - 1
                If oAtributos.existe(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value) Then
                    Set oAtributo = oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value)
                    If oAtributo.Tipo = TipoBoolean Then
                        If oAtributo.getValor = 1 Then
                            ssdbArticulosAgregados.Columns(j).Value = m_sIdiTrue
                        Else
                            ssdbArticulosAgregados.Columns(j).Value = m_sIdiFalse
                        End If
                    Else
                        ssdbArticulosAgregados.Columns(j).Value = oAtributo.getValor
                    End If
                
                Else
                    'si no existe le damos el valor por defecto (guardado en atributosuon)
                    Set oAtributo = m_oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name)
                    If oAtributo.uons.CONTIENE(m_oUons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)) Then
                        If oAtributo.Tipo = TipoBoolean Then
                            If oAtributo.getValor = 1 Then
                                ssdbArticulosAgregados.Columns(j).Text = m_sIdiTrue
                            Else
                                ssdbArticulosAgregados.Columns(j).Text = m_sIdiFalse
                            End If
                        Else
                            ssdbArticulosAgregados.Columns(j).Text = NullToStr(oAtributo.getValor)
                        End If
                    End If
                    Set oAtributo = Nothing
                End If
            Next
            ssdbArticulosAgregados.MoveNext
        Next
    End If
    ssdbArticulosAgregados.MoveFirst
    LockWindowUpdate 0&
    Set oAtributo = Nothing
    Set oAtributos = Nothing
    Set oArticulo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemAtribDetalle", "cargarValoresGridArticulosAgregados", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub ssdbArticulosAgregados_RowLoaded(ByVal Bookmark As Variant)
    Dim oUON As IUon
    Dim oatrib As CAtributo
    Dim oArticulo As CArticulo
    
  If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    'celdas editables o no
  
    If m_oArticulo.isCentral Then
        'tomamos el art�culo agregado y la uon que se corresponden con la fila que estamos cargando
        Set oArticulo = m_oArticulo.articulosAgregados.Item(ssdbArticulosAgregados.Columns("COD").Value)
        Set oUON = m_oUons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)
        For Each oatrib In oArticulo.getAtributosUon
            If Not oatrib.uons.CONTIENE(oUON) Then
                ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Gris", ssdbArticulosAgregados.Row
            Else
                ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Normal", ssdbArticulosAgregados.Row
            End If
        Next
    Else
        'uon de la fila
        If ssdbArticulosAgregados.Columns("UONKEY").Value <> "" Then
            Set oUON = m_oUons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)
            'para cada atributo(columna atributo)
            For Each oatrib In m_oArticulo.getAtributosUon
                If Not ssdbArticulosAgregados.Columns(CStr(oatrib.Id)) Is Nothing Then
                    'si las uons del atributo contienen a la uon del art�culo puden tener valor , si no "no aplica"
                    If Not oatrib.uons.CONTIENE(oUON) Then
                        ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Gris", ssdbArticulosAgregados.Row
                    Else
                        ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Normal", ssdbArticulosAgregados.Row
                    End If
                End If
            Next
       End If
    End If
 
    Set oUON = Nothing
    Set oatrib = Nothing
    Set oArticulo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemAtribDetalle", "ssdbArticulosAgregados_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Public Property Let mostrarAtributosuon(Value As Boolean)
    m_bMostrarUons = Value
    ssdbArticulosAgregados.Visible = m_bMostrarUons
End Property

Public Property Get mostrarAtributosuon() As Boolean
    mostrarAtributosuon = m_bMostrarUons
End Property
