VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSolicitudAsignarComp 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DAsignarComprador"
   ClientHeight    =   3570
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5775
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSolicitudAsignarComp.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3570
   ScaleWidth      =   5775
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtComentario 
      Height          =   1575
      Left            =   120
      MaxLength       =   500
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   1460
      Width           =   5535
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Height          =   315
      Left            =   1470
      TabIndex        =   3
      Top             =   3140
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   2790
      TabIndex        =   5
      Top             =   3140
      Width           =   1095
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcComprador 
      Height          =   285
      Left            =   1320
      TabIndex        =   1
      Top             =   600
      Width           =   3015
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      HeadLines       =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5318
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5318
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcEquipo 
      Height          =   285
      Left            =   1320
      TabIndex        =   0
      Top             =   120
      Width           =   3015
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      HeadLines       =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5318
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5318
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblEquipo 
      BackColor       =   &H00808000&
      Caption         =   "DEquipo:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblComent 
      BackColor       =   &H00808000&
      Caption         =   "DIntroduzca un comentario para el comprador si lo desea:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   1200
      Width           =   4815
   End
   Begin VB.Label lblComprador 
      BackColor       =   &H00808000&
      Caption         =   "DComprador:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   1095
   End
End
Attribute VB_Name = "frmSolicitudAsignarComp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public g_oSolicitud As CInstancia
Public g_bRestricEquipo As Boolean
Public g_sOrigen As String
Public g_sOrigen2 As String

'Variables privadas
Private m_bRespetarCombo As Boolean
Private m_oEqpSeleccionado As CEquipo
Private m_oEquipos As CEquipos

Private m_oCompSeleccionado As CComprador
Private m_oComps As CCompradores

''' <summary>
''' Asignar comprador
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
Dim arMensaje As Variant
Dim oEmails As CEmailSolicitudes
Dim teserror As TipoErrorSummit
Dim sComprador As String
Dim iRes As Integer
Dim bSesionIniciada  As Boolean

    'Validaci�n de datos
    If sdbcEquipo.Text = "" Then
        oMensajes.IntroduzcaComprador
        If Me.Visible Then sdbcEquipo.SetFocus
        Exit Sub
    End If
    
    If sdbcComprador.Text = "" Then
        oMensajes.IntroduzcaComprador
        If Me.Visible Then sdbcComprador.SetFocus
        Exit Sub
    End If
    
    If Not g_oSolicitud.comprador Is Nothing Then
        If sdbcComprador.Columns(0).Value = g_oSolicitud.comprador.Cod Then
            sComprador = g_oSolicitud.comprador.Cod & "-" & NullToStr(g_oSolicitud.comprador.nombre) & " " & NullToStr(g_oSolicitud.comprador.Apel)
            oMensajes.SeleccioneComprador (sComprador)
            If Me.Visible Then sdbcComprador.SetFocus
            Exit Sub
        End If
    End If
    
    If m_oCompSeleccionado Is Nothing Then Exit Sub
    
    'Asigna al comprador la solicitud que est� seleccionada en la grid de solicitudes
    cmdAceptar.Enabled = False
    Screen.MousePointer = vbHourglass
    
    'Env�o de emails al comprador
    Set oEmails = oFSGSRaiz.Generar_CEmailSolicitudes
     
    arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 0)
    If arMensaje(0) = "" Then
    ElseIf arMensaje(2) = "1010" Then 'No encontr� la ruta o plantilla
        iRes = oMensajes.PreguntaRutaSolicitudesNoEncontrada
        If iRes = vbNo Then
            Set oEmails = Nothing
            Screen.MousePointer = vbNormal
            Unload Me
            cmdAceptar.Enabled = True
            Exit Sub
        Else
            arMensaje(2) = ""
        End If
    End If
    
    teserror = g_oSolicitud.AsignarComprador(basOptimizacion.gCodPersonaUsuario, sdbcComprador.Columns(0).Value, txtComentario.Text)
            
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        
    Else
        arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 0)
        If arMensaje(2) = "1010" Then arMensaje(2) = ""
        
        If Not bSesionIniciada Or oIdsMail Is Nothing Then
            Set oIdsMail = IniciarSesionMail
            bSesionIniciada = True
        End If
        'doevents
        
        If g_oSolicitud.comprador Is Nothing Then  'Esto es para notificar al nuevo comprador. Y tener en el objeto la informaci�n actualizada.
            Set g_oSolicitud.comprador = m_oCompSeleccionado
        End If
        
        teserror = ComponerMensaje(arMensaje(0), arMensaje(1), arMensaje(2), , , arMensaje(4), , , arMensaje(5), _
                            lIdInstancia:=g_oSolicitud.Id, _
                            entidadNotificacion:=entidadNotificacion.Solicitud, _
                            tipoNotificacion:=AsignacionSolicitudCompra, _
                            sToName:=NullToStr(g_oSolicitud.comprador.nombre) & " " & NullToStr(g_oSolicitud.comprador.Apel))

        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        End If
    
        If bSesionIniciada Then
            FinalizarSesionMail
        End If
    
        Set oEmails = Nothing
        
        'Actualiza la colecci�n y la grid de solicitudes con el nuevo comprador seleccionado:
        Select Case g_sOrigen2
            Case "frmSolicitudes"   's�lo si el formulario origen es el de solicitudes
                frmSolicitudes.ActualizarCompradorEnGrid g_oSolicitud.Id, m_oCompSeleccionado
                If g_sOrigen = "frmSolicitudDetalle" Then
                    frmSolicitudes.g_ofrmDetalleSolic.ActualizarCompradorEnGrid m_oCompSeleccionado
                End If
        
            Case "frmPROCE"
                frmPROCE.g_ofrmDetalleSolic.ActualizarCompradorEnGrid m_oCompSeleccionado
                
            Case "frmCatalogo"
                frmCatalogo.g_ofrmDetalleSolic.ActualizarCompradorEnGrid m_oCompSeleccionado
                
            Case "frmPedidos"
                frmPEDIDOS.g_ofrmDetalleSolic.ActualizarCompradorEnGrid m_oCompSeleccionado
                
            Case "frmSeguimiento"
                frmSeguimiento.g_ofrmDetalleSolic.ActualizarCompradorEnGrid m_oCompSeleccionado
        End Select
        
        
        ''' Registro de acciones
        basSeguridad.RegistrarAccion AccionesSummit.ACCSolicAsignar, "Id:" & g_oSolicitud.Id
    End If
    
    Screen.MousePointer = vbNormal
    
    cmdAceptar.Enabled = True
    Unload Me
    
End Sub


Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    CargarRecursos

    If g_oSolicitud Is Nothing Then Exit Sub
    cmdAceptar.Enabled = True
    'Muestra en las combos el equipo y comprador que se han seleccionado desde el EP:
    If Not g_oSolicitud.comprador Is Nothing Then
        If g_oSolicitud.comprador.codEqp <> "" Then
            sdbcEquipo.Columns(0).Value = g_oSolicitud.comprador.codEqp
            sdbcEquipo.Text = g_oSolicitud.comprador.codEqp & " - " & g_oSolicitud.comprador.DenEqp
                
            sdbcComprador.Columns(0).Value = g_oSolicitud.comprador.Cod
            sdbcComprador.Text = g_oSolicitud.comprador.Cod & " - " & NullToStr(g_oSolicitud.comprador.nombre) & " " & NullToStr(g_oSolicitud.comprador.Apel)
        End If
    End If
    
    'Selecciona el equipo:
    Set m_oEquipos = Nothing
    Set m_oEquipos = oFSGSRaiz.Generar_CEquipos
        
    'Si tiene restricciones de equipo
    If g_bRestricEquipo = True Then
        m_oEquipos.CargarTodosLosEquipos g_oSolicitud.comprador.codEqp, , True
        Set m_oEqpSeleccionado = m_oEquipos.Item(1)
        
        'Hace invisible el combo de equipos y redimensiona el resto de la grid
        sdbcEquipo.Visible = False
        lblEquipo.Visible = False
        sdbcComprador.Top = sdbcEquipo.Top
        lblComprador.Top = lblEquipo.Top
        lblComent.Top = lblComent.Top - sdbcEquipo.Height - sdbcEquipo.Top
        txtComentario.Top = txtComentario.Top - sdbcEquipo.Height - sdbcEquipo.Top
        cmdAceptar.Top = cmdAceptar.Top - sdbcEquipo.Height - sdbcEquipo.Top
        cmdCancelar.Top = cmdCancelar.Top - sdbcEquipo.Height - sdbcEquipo.Top
        Me.Height = Me.Height - sdbcEquipo.Height - sdbcEquipo.Top
        
    Else
        m_oEquipos.CargarTodosLosEquipos , , False, True, False
        If Not g_oSolicitud.comprador Is Nothing Then
            Set m_oEqpSeleccionado = m_oEquipos.Item(CStr(g_oSolicitud.comprador.codEqp))
        End If
    End If
    
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOLIC_ASIGNAR_COMP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value        'Asignar comprador
        Ador.MoveNext
        Me.lblComent.caption = Ador(0).Value   'Comentario
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value  'Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value  'Cancelar
        Ador.MoveNext
        lblEquipo.caption = Ador(0).Value   'Equipo
        Ador.MoveNext
        lblComprador.caption = Ador(0).Value  'Comprador
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set m_oEqpSeleccionado = Nothing
    Set m_oEquipos = Nothing
    Set m_oCompSeleccionado = Nothing
    Set m_oComps = Nothing
    Set g_oSolicitud = Nothing
End Sub

Private Sub sdbcComprador_Change()
    If Not m_bRespetarCombo Then
        Set m_oCompSeleccionado = Nothing
    End If
End Sub

Private Sub sdbcComprador_Click()
    If Not sdbcComprador.DroppedDown Then
        sdbcComprador = ""
    End If
End Sub


Private Sub sdbcComprador_CloseUp()
    Dim sCod As String
    
    If sdbcComprador.Value = "..." Then
        sdbcComprador.Text = ""
        Exit Sub
    End If
    
    If sdbcComprador.Value = "" Then Exit Sub
    
    sCod = m_oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(m_oEqpSeleccionado.Cod))
    
    Set m_oCompSeleccionado = m_oComps.Item(sCod & sdbcComprador.Columns(0).Text)
        
    DoEvents
    
End Sub


Private Sub sdbcComprador_DropDown()
    Dim oComp As CComprador
    
    sdbcComprador.RemoveAll
    
    If m_oEqpSeleccionado Is Nothing Then Exit Sub
    
    Set m_oComps = Nothing
    
    Screen.MousePointer = vbHourglass

    Set m_oComps = oFSGSRaiz.generar_CCompradores
    

    m_oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, False, False
    
    Set m_oComps = m_oEqpSeleccionado.Compradores
    
    For Each oComp In m_oComps
        sdbcComprador.AddItem oComp.Cod & Chr(9) & oComp.Cod & " - " & NullToStr(oComp.nombre) & " " & NullToStr(oComp.Apel)
    Next
    
    sdbcComprador.SelStart = 0
    sdbcComprador.SelLength = Len(sdbcComprador.Text)
    sdbcComprador.Refresh

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcComprador_InitColumnProps()
    sdbcComprador.DataFieldList = "Column 1"
    sdbcComprador.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcComprador_PositionList(ByVal Text As String)
     ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcComprador.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcComprador.Rows - 1
            bm = sdbcComprador.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcComprador.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcComprador.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbcComprador_Validate(Cancel As Boolean)
    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
    
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    
    If sdbcComprador.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el comprador
    
    Screen.MousePointer = vbHourglass
   
    m_oEqpSeleccionado.CargarTodosLosCompradores sdbcComprador.Columns(0).Value, , , True, False, False, False
    Set oCompradores = m_oEqpSeleccionado.Compradores
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcComprador = ""
    Else
        Set m_oCompSeleccionado = oCompradores.Item(1)
    End If
    
    Set oCompradores = Nothing
    
    Screen.MousePointer = vbNormal
    
    
End Sub

Private Sub sdbcEquipo_Change()
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcComprador = ""
        sdbcComprador.RemoveAll
        m_bRespetarCombo = False
    
        Set m_oEqpSeleccionado = Nothing
        
    End If
End Sub

Private Sub sdbcEquipo_Click()
    If Not sdbcEquipo.DroppedDown Then
        sdbcEquipo = ""
    End If
End Sub

Private Sub sdbcEquipo_CloseUp()
    If sdbcEquipo.Value = "..." Then
        sdbcEquipo.Columns(0).Value = ""
        sdbcEquipo.Columns(1).Value = ""
        Exit Sub
    End If
    
    If sdbcEquipo.Value = "" Then Exit Sub
    
    sdbcComprador = ""
    sdbcComprador.RemoveAll
    
    Screen.MousePointer = vbHourglass

    Set m_oEqpSeleccionado = m_oEquipos.Item(sdbcEquipo.Columns(0).Text)
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcEquipo_DropDown()
    Dim oeqp As CEquipo
    
    Set m_oEquipos = Nothing
    Set m_oEquipos = oFSGSRaiz.Generar_CEquipos
    
    Screen.MousePointer = vbHourglass
    
    sdbcEquipo.RemoveAll
    
    m_oEquipos.CargarTodosLosEquipos , , False, True, False
    
    For Each oeqp In m_oEquipos
        sdbcEquipo.AddItem oeqp.Cod & Chr(9) & oeqp.Cod & " - " & oeqp.Den
    Next
    
    sdbcEquipo.SelStart = 0
    sdbcEquipo.SelLength = Len(sdbcEquipo.Columns(0).Value)
    sdbcEquipo.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEquipo_InitColumnProps()
    sdbcEquipo.DataFieldList = "Column 1"
    sdbcEquipo.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcEquipo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEquipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEquipo.Rows - 1
            bm = sdbcEquipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEquipo.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcEquipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbcEquipo_Validate(Cancel As Boolean)
    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEquipo.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
   
    oEquipos.CargarTodosLosEquipos sdbcEquipo.Columns(0).Value, , True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEquipo.Text = ""
        sdbcComprador = ""
        sdbcComprador.RemoveAll

    Else
        Set m_oEqpSeleccionado = oEquipos.Item(1)
    End If
    
    Set oEquipos = Nothing
    
    Screen.MousePointer = vbNormal
End Sub



