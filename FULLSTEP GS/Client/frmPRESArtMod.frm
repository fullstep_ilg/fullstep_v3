VERSION 5.00
Begin VB.Form frmPRESArtMod 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DModificar "
   ClientHeight    =   3615
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5265
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPRESArtMod.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3615
   ScaleWidth      =   5265
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   2720
      TabIndex        =   9
      Top             =   3260
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1580
      TabIndex        =   8
      Top             =   3260
      Width           =   1005
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Height          =   1270
      Left            =   120
      TabIndex        =   10
      Top             =   1920
      Width           =   5055
      Begin VB.OptionButton optValor 
         BackColor       =   &H00808000&
         Caption         =   "DIntroducir un valor fijo"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Value           =   -1  'True
         Width           =   4000
      End
      Begin VB.TextBox txtValor 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2565
         TabIndex        =   7
         Top             =   740
         Width           =   1215
      End
      Begin VB.Label lblValor 
         BackColor       =   &H00808000&
         Caption         =   "DValor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Left            =   1575
         TabIndex        =   12
         Top             =   800
         Width           =   900
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   1715
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5055
      Begin VB.PictureBox picOptions 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   855
         Left            =   2500
         ScaleHeight     =   855
         ScaleWidth      =   2415
         TabIndex        =   13
         Top             =   360
         Width           =   2415
         Begin VB.OptionButton optPorcen 
            BackColor       =   &H00808000&
            Caption         =   "DEn porcentaje (0-100)"
            Enabled         =   0   'False
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   0
            TabIndex        =   4
            Top             =   465
            Width           =   2400
         End
         Begin VB.OptionButton optCantidad 
            BackColor       =   &H00808000&
            Caption         =   "DEn cantidad"
            Enabled         =   0   'False
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   0
            TabIndex        =   3
            Top             =   0
            Value           =   -1  'True
            Width           =   1695
         End
      End
      Begin VB.OptionButton optModificar 
         BackColor       =   &H00808000&
         Caption         =   "DIncrementar"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   1815
      End
      Begin VB.OptionButton optModificar 
         BackColor       =   &H00808000&
         Caption         =   "DDecrementar"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   2
         Top             =   825
         Width           =   1335
      End
      Begin VB.TextBox txtValorTras 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   2565
         TabIndex        =   5
         Top             =   1300
         Width           =   1215
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FFFFFF&
         X1              =   2140
         X2              =   2140
         Y1              =   340
         Y2              =   1080
      End
      Begin VB.Label lblValorTras 
         BackColor       =   &H00808000&
         Caption         =   "DValor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Left            =   1575
         TabIndex        =   11
         Top             =   1340
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmPRESArtMod"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_iModif As Integer

'Variables de idiomas:
Private m_stxtTitulo(4) As String
Private m_stxtValor As String


Private Sub cmdAceptar_Click()
    Dim i As Integer
    Dim oRow As SSRow
    Dim sColumna As String
    
    'Validación de los datos
    If optValor.value = True Then
        If Trim(txtValor.Text) = "" Then
            oMensajes.FaltaValor
            If Me.Visible Then txtValor.SetFocus
            Exit Sub
        End If
        
        If Not IsNumeric(Trim(txtValor.Text)) Then
            oMensajes.NoValido m_stxtValor
            If Me.Visible Then txtValor.SetFocus
            Exit Sub
        End If
    Else
        If Trim(txtValorTras.Text) = "" Then
            oMensajes.FaltaValor
            If Me.Visible Then txtValorTras.SetFocus
            Exit Sub
        End If
        
        If Not IsNumeric(Trim(txtValorTras.Text)) Then
            oMensajes.NoValido m_stxtValor
            If Me.Visible Then txtValorTras.SetFocus
            Exit Sub
        End If
        
        If optPorcen.value = True And (StrToDbl0(Trim(txtValorTras.Text)) < 0 Or StrToDbl0(Trim(txtValorTras.Text)) > 100) Then
            oMensajes.NoValido m_stxtValor
            If Me.Visible Then txtValorTras.SetFocus
            Exit Sub
        End If
    End If
    
    'Actualización de la grid de presupuestos:
    Screen.MousePointer = vbHourglass
        
    Select Case g_iModif
        Case 0
            'modificar presupuesto unitario
            sColumna = "PRESV"
        Case 1
            'modificar cantidad
             sColumna = "CANTV"
        Case 2
            'modificar objetivo unitario
            sColumna = "OBJV"
        Case 3
            'modificar presupuesto total
            sColumna = "PRESTOT"
        Case 4
            'modificar objetivo total
            sColumna = "OBJTOT"
    End Select
        
    For Each oRow In frmPRESArt.sdbgUltraPres.Selected.Rows
        
        If optValor.value = True Then
            oRow.Cells(sColumna).value = Trim(txtValor.Text)
            
        ElseIf optModificar(0).value = True Then
            'incrementar
            If oRow.Cells(sColumna).value = "" Then
                oRow.Cells(sColumna).value = ""
            Else
                If optCantidad.value = True Then
                    'En cantidad
                    oRow.Cells(sColumna).value = StrToDbl0(oRow.Cells(sColumna).value) + StrToDbl0(Trim(txtValorTras.Text))
                Else
                    'En porcentaje
                    oRow.Cells(sColumna).value = StrToDbl0(oRow.Cells(sColumna).value) + (StrToDbl0(oRow.Cells(sColumna).value) * (StrToDbl0(Trim(txtValorTras.Text)) / 100))
                End If
            End If
            
        ElseIf optModificar(1).value = True Then
            'decrementar
            If oRow.Cells(sColumna).value = "" Then
                oRow.Cells(sColumna).value = ""
            Else
                If optCantidad.value = True Then
                    'En cantidad
                    oRow.Cells(sColumna).value = StrToDbl0(oRow.Cells(sColumna).value) - StrToDbl0(Trim(txtValorTras.Text))
                Else
                    'En porcentaje
                    oRow.Cells(sColumna).value = StrToDbl0(oRow.Cells(sColumna).value) - (StrToDbl0(oRow.Cells(sColumna).value) * (StrToDbl0(Trim(txtValorTras.Text)) / 100))
                End If
            End If
        End If
    Next
    
    frmPRESArt.sdbgUltraPres.Selected.ClearAll
    frmPRESArt.sdbgUltraPres.Update
    
    Screen.MousePointer = vbNormal
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()

    frmPRESArt.sdbgUltraPres.Selected.ClearAll
    Unload Me
End Sub

Private Sub Form_Load()
    CargarRecursos
    
    Me.caption = m_stxtTitulo(g_iModif)
    
    optModificar(0).value = False
    optModificar(1).value = False
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next

    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESART_CARGAOBJ, basPublic.gParametrosInstalacion.gIdioma)

    If Not ador Is Nothing Then

        ador.MoveNext
        ador.MoveNext
        
        optModificar(0).caption = ador(0).value
        ador.MoveNext
        optModificar(1).caption = ador(0).value
        ador.MoveNext
        optCantidad.caption = ador(0).value
        ador.MoveNext
        optPorcen.caption = ador(0).value
        ador.MoveNext
        lblValorTras.caption = ador(0).value & ":"
        lblValor.caption = ador(0).value & ":"
        m_stxtValor = ador(0).value
        ador.MoveNext
        ador.MoveNext
        cmdAceptar.caption = ador(0).value
        ador.MoveNext
        cmdCancelar.caption = ador(0).value
        
        ador.MoveNext
        optValor.caption = ador(0).value
        ador.MoveNext
        m_stxtTitulo(0) = ador(0).value
        ador.MoveNext
        m_stxtTitulo(1) = ador(0).value
        ador.MoveNext
        m_stxtTitulo(2) = ador(0).value
        ador.MoveNext
        m_stxtTitulo(3) = ador(0).value
        ador.MoveNext
        m_stxtTitulo(4) = ador(0).value
        
        ador.Close

    End If

    Set ador = Nothing

End Sub


Private Sub optModificar_Click(Index As Integer)
    If optModificar(Index).value = True Then
        txtValorTras.Enabled = True
        txtValor.Text = ""
        txtValor.Enabled = False
        
        optValor.value = False
        optCantidad.Enabled = True
        optPorcen.Enabled = True
        
    End If
End Sub

Private Sub optValor_Click()
    
    If optValor.value = True Then
        txtValor.Enabled = True
        txtValorTras.Text = ""
        txtValorTras.Enabled = False
                
        optModificar(0).value = False
        optModificar(1).value = False
        optCantidad.Enabled = False
        optPorcen.Enabled = False
    End If
End Sub
