VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroNegPresDetalle 
   BackColor       =   &H00808000&
   Caption         =   "Detalle de conceptos 3 y 4"
   ClientHeight    =   2595
   ClientLeft      =   360
   ClientTop       =   3120
   ClientWidth     =   9435
   Icon            =   "frmInfAhorroNegPresDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2595
   ScaleWidth      =   9435
   Begin VB.PictureBox picgreen 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2595
      Left            =   30
      ScaleHeight     =   2595
      ScaleWidth      =   9405
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   9405
      Begin SSDataWidgets_B.SSDBGrid sdbgRes 
         Height          =   2295
         Left            =   420
         TabIndex        =   3
         Top             =   150
         Width           =   8865
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   10
         stylesets.count =   6
         stylesets(0).Name=   "Boton"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmInfAhorroNegPresDetalle.frx":0CB2
         stylesets(1).Name=   "White"
         stylesets(1).BackColor=   15400959
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmInfAhorroNegPresDetalle.frx":0CCE
         stylesets(2).Name=   "Normal"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmInfAhorroNegPresDetalle.frx":0CEA
         stylesets(3).Name=   "Red"
         stylesets(3).BackColor=   4744445
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmInfAhorroNegPresDetalle.frx":0D06
         stylesets(4).Name=   "Blue"
         stylesets(4).BackColor=   16777152
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmInfAhorroNegPresDetalle.frx":0D22
         stylesets(5).Name=   "Green"
         stylesets(5).BackColor=   10409634
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmInfAhorroNegPresDetalle.frx":0D3E
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   10
         Columns(0).Width=   1588
         Columns(0).Caption=   "Artículo"
         Columns(0).Name =   "ITEM"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasHeadForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).HeadBackColor=   12632256
         Columns(0).BackColor=   16776960
         Columns(1).Width=   3572
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16776960
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "Procesos"
         Columns(2).Name =   "NUMPROCE"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   12632256
         Columns(3).Width=   2566
         Columns(3).Caption=   "Presupuesto"
         Columns(3).Name =   "PRES"
         Columns(3).Alignment=   1
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "Standard"
         Columns(3).FieldLen=   256
         Columns(3).HasHeadForeColor=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).HeadBackColor=   12632256
         Columns(3).BackColor=   15400959
         Columns(4).Width=   2143
         Columns(4).Caption=   "Adjudicado"
         Columns(4).Name =   "ADJ"
         Columns(4).Alignment=   1
         Columns(4).CaptionAlignment=   2
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).NumberFormat=   "Standard"
         Columns(4).FieldLen=   256
         Columns(4).HasHeadForeColor=   -1  'True
         Columns(4).HasBackColor=   -1  'True
         Columns(4).HeadBackColor=   12632256
         Columns(4).BackColor=   15400959
         Columns(5).Width=   1984
         Columns(5).Caption=   "Ahorro"
         Columns(5).Name =   "AHO"
         Columns(5).Alignment=   1
         Columns(5).CaptionAlignment=   2
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).NumberFormat=   "Standard"
         Columns(5).FieldLen=   256
         Columns(5).HasHeadForeColor=   -1  'True
         Columns(5).HeadBackColor=   12632256
         Columns(6).Width=   1693
         Columns(6).Caption=   "%"
         Columns(6).Name =   "PORCEN"
         Columns(6).Alignment=   1
         Columns(6).CaptionAlignment=   2
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).NumberFormat=   "0.0#\%"
         Columns(6).FieldLen=   256
         Columns(6).HasHeadForeColor=   -1  'True
         Columns(6).HeadBackColor=   12632256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "CODOCULTO"
         Columns(7).Name =   "CODOCULTO"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ASIG"
         Columns(8).Name =   "ASIG"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   1138
         Columns(9).Caption=   "Detalle"
         Columns(9).Name =   "DET"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).Style=   4
         Columns(9).ButtonsAlways=   -1  'True
         Columns(9).StyleSet=   "Boton"
         _ExtentX        =   15637
         _ExtentY        =   4048
         _StockProps     =   79
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdActualizar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   30
         Picture         =   "frmInfAhorroNegPresDetalle.frx":0D5A
         Style           =   1  'Graphical
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   510
         Width           =   315
      End
      Begin VB.PictureBox picTipoGrafico 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   420
         ScaleHeight     =   315
         ScaleWidth      =   2595
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   60
         Visible         =   0   'False
         Width           =   2595
         Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
            Height          =   285
            Left            =   0
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   0
            Width           =   1515
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            Row.Count       =   5
            Row(0)          =   "Barras 2D"
            Row(1)          =   "Barras 3D"
            Row(2)          =   "Lineas 2D"
            Row(3)          =   "Lineas 3D"
            Row(4)          =   "Tarta"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   3200
            Columns(0).Caption=   "TIPO"
            Columns(0).Name =   "TIPO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2672
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin MSChart20Lib.MSChart MSChart1 
         Height          =   2025
         Left            =   420
         OleObjectBlob   =   "frmInfAhorroNegPresDetalle.frx":0DE5
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   420
         Width           =   8865
      End
      Begin VB.CommandButton cmdImprimir 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   30
         Picture         =   "frmInfAhorroNegPresDetalle.frx":280B
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   510
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.CommandButton cmdGrafico 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   30
         Picture         =   "frmInfAhorroNegPresDetalle.frx":2B4D
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   150
         Width           =   315
      End
      Begin VB.CommandButton cmdGrid 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   30
         Picture         =   "frmInfAhorroNegPresDetalle.frx":2E8F
         Style           =   1  'Graphical
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   150
         Visible         =   0   'False
         Width           =   315
      End
   End
End
Attribute VB_Name = "frmInfAhorroNegPresDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_dFecha As Date
Public g_bSoloDeAdjDir As Boolean
Public g_bSoloEnReunion As Boolean
Public g_sUON1 As String
Public g_sUON2 As String
Public g_sUON3 As String
Public g_dEquivalencia As Double

'Presupuestos
Public g_sPRES1 As String
Public g_sPRES2 As String
Public g_sPRES3 As String
Public g_sPRES4 As String

'Variables utilizadas para el multilenguaje
Private sIdiTipoGrafico(5) As String
Private m_sIdiDetalle As String
Private sIdiDetResult As String
Public g_sOrigen As String


Private Sub cmdActualizar_Click()
    If cmdGrid.Visible Then
        MostrarGrafico sdbcTipoGrafico.Value
    End If
    
End Sub

Private Sub Form_Load()
    
    Me.Height = 3000
    Me.Width = 9555
    CargarRecursos
        
    PonerFieldSeparator Me
End Sub
Private Sub cmdGrafico_Click()
        
        If sdbgRes.Rows = 0 Then
            Exit Sub
        End If
    
        Screen.MousePointer = vbHourglass
        picTipoGrafico.Visible = True
        sdbcTipoGrafico.Value = sIdiTipoGrafico(2) '"Barras 3D"
        MostrarGrafico sdbcTipoGrafico.Value
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        MSChart1.Visible = True
        Screen.MousePointer = vbNormal

End Sub

Private Sub cmdGrid_Click()
        
        picTipoGrafico.Visible = False
        
        cmdGrafico.Visible = True
        cmdGrid.Visible = False
        sdbgRes.Visible = True
        MSChart1.Visible = False
End Sub

Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    MSChart1.Visible = True
    Select Case Tipo
    
'        Case "Barras 2D", "Barras 3D"
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
'                If Tipo = "Barras 3D" Then
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                
'                If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
        
    
    
End Sub


Private Sub Form_Resize()
    
    If Me.Height < 1100 Then Exit Sub
    If Me.Width < 1100 Then Exit Sub
    
    picgreen.Width = Me.Width - 150
    picgreen.Height = Me.Height - 405
    sdbgRes.Width = picgreen.Width - 540
    sdbgRes.Height = picgreen.Height - 300
    
    If sdbgRes.Columns(2).Visible = False Then
        sdbgRes.Columns(0).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(1).Width = sdbgRes.Width * 0.2
        sdbgRes.Columns(3).Width = sdbgRes.Width * 0.13
        sdbgRes.Columns(4).Width = sdbgRes.Width * 0.13
        sdbgRes.Columns(5).Width = sdbgRes.Width * 0.12
        sdbgRes.Columns(6).Width = sdbgRes.Width * 0.12
        sdbgRes.Columns(7).Width = sdbgRes.Width * 0.1 - 470
    Else
        sdbgRes.Columns(0).Width = sdbgRes.Width * 0.1
        sdbgRes.Columns(1).Width = sdbgRes.Width * 0.2
        sdbgRes.Columns(2).Width = sdbgRes.Width * 0.1
        sdbgRes.Columns(3).Width = sdbgRes.Width * 0.13
        sdbgRes.Columns(4).Width = sdbgRes.Width * 0.13
        sdbgRes.Columns(5).Width = sdbgRes.Width * 0.12
        sdbgRes.Columns(6).Width = sdbgRes.Width * 0.12
        sdbgRes.Columns(6).Width = sdbgRes.Width * 0.1 - 470
    
    End If
    
    MSChart1.Width = sdbgRes.Width
    MSChart1.Height = sdbgRes.Height - 240
        
End Sub


Private Sub Form_Unload(Cancel As Integer)
    g_sOrigen = ""
End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbgRes_BtnClick()
Dim frm2 As frmInfAhorroNegFechaDetalle
Dim ADORs As Ador.Recordset

    If sdbgRes.Rows = 0 Then Exit Sub
    
    Set ADORs = Nothing
    
    Screen.MousePointer = vbHourglass
    
    Select Case sdbgRes.Columns(0).caption
        
    Case basParametros.gParametrosGenerales.gsSingPres3
    
        If g_sPRES4 <> "" Then
            Set ADORs = oGestorInformes.AhorroNegociadoFechaCon3(Me.g_dFecha, Me.g_sPRES1, Me.g_sPRES2, Me.g_sPRES3, Me.g_sPRES4, , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
        ElseIf g_sPRES3 <> "" Then
            Set ADORs = oGestorInformes.AhorroNegociadoFechaCon3(Me.g_dFecha, Me.g_sPRES1, Me.g_sPRES2, Me.g_sPRES3, sdbgRes.Columns(0).Value, , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
        ElseIf g_sPRES2 <> "" Then
            Set ADORs = oGestorInformes.AhorroNegociadoFechaCon3(Me.g_dFecha, Me.g_sPRES1, Me.g_sPRES2, sdbgRes.Columns(0).Value, , , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
        ElseIf g_sPRES1 <> "" Then
            Set ADORs = oGestorInformes.AhorroNegociadoFechaCon3(Me.g_dFecha, Me.g_sPRES1, sdbgRes.Columns(0).Value, , , , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
        Else
            Set ADORs = oGestorInformes.AhorroNegociadoFechaCon3(Me.g_dFecha, sdbgRes.Columns(0).Value, , , , , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
        End If
        If ADORs Is Nothing Then Exit Sub
        sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
        Set frm2 = New frmInfAhorroNegFechaDetalle
        frm2.g_sOrigen = g_sOrigen
        frm2.dequivalencia = Me.g_dEquivalencia
        frm2.sFecha = Me.g_dFecha
        frm2.caption = m_sIdiDetalle & " " & gParametrosGenerales.gsSingPres3 & ": " & sdbgRes.Columns(0).Value
        While Not ADORs.EOF
            frm2.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & Me.g_dEquivalencia * ADORs(3).Value & Chr(m_lSeparador) & Me.g_dEquivalencia * ADORs(4).Value & Chr(m_lSeparador) & Me.g_dEquivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
            ADORs.MoveNext
        Wend
        ADORs.Close
        Set ADORs = Nothing
        
        frm2.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
        frm2.Left = sdbgRes.Left + Left + 400 + MDI.Left
        
        Screen.MousePointer = vbNormal
        
        frm2.Show 1
        sdbgRes.SelBookmarks.RemoveAll
        
    Case basParametros.gParametrosGenerales.gsSingPres4
        If g_sPRES4 <> "" Then
            Set ADORs = oGestorInformes.AhorroNegociadoFechaCon4(Me.g_dFecha, Me.g_sPRES1, Me.g_sPRES2, Me.g_sPRES3, Me.g_sPRES4, , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
        ElseIf g_sPRES3 <> "" Then
            Set ADORs = oGestorInformes.AhorroNegociadoFechaCon4(Me.g_dFecha, Me.g_sPRES1, Me.g_sPRES2, Me.g_sPRES3, sdbgRes.Columns(0).Value, , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
        ElseIf g_sPRES2 <> "" Then
            Set ADORs = oGestorInformes.AhorroNegociadoFechaCon4(Me.g_dFecha, Me.g_sPRES1, Me.g_sPRES2, sdbgRes.Columns(0).Value, , , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
        ElseIf g_sPRES1 <> "" Then
            Set ADORs = oGestorInformes.AhorroNegociadoFechaCon4(Me.g_dFecha, Me.g_sPRES1, sdbgRes.Columns(0).Value, , , , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
        Else
            Set ADORs = oGestorInformes.AhorroNegociadoFechaCon4(Me.g_dFecha, sdbgRes.Columns(0).Value, , , , , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
        End If
        If ADORs Is Nothing Then Exit Sub
        sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
        Set frm2 = New frmInfAhorroNegFechaDetalle
        frm2.g_sOrigen = g_sOrigen
        frm2.dequivalencia = Me.g_dEquivalencia
        frm2.sFecha = Me.g_dFecha
        frm2.caption = m_sIdiDetalle & " " & gParametrosGenerales.gsSingPres3 & ": " & sdbgRes.Columns(0).Value
        While Not ADORs.EOF
            frm2.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & Me.g_dEquivalencia * ADORs(3).Value & Chr(m_lSeparador) & Me.g_dEquivalencia * ADORs(4).Value & Chr(m_lSeparador) & Me.g_dEquivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
            ADORs.MoveNext
        Wend
        ADORs.Close
        Set ADORs = Nothing
        
        frm2.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
        frm2.Left = sdbgRes.Left + Left + 400 + MDI.Left
        
        Screen.MousePointer = vbNormal
        
        frm2.Show 1
        sdbgRes.SelBookmarks.RemoveAll
    
    End Select

End Sub

Private Sub sdbgRes_DblClick()
Dim frm As frmInfAhorroNegPresDetalle
Dim ADORs As Ador.Recordset
    
    If sdbgRes.Rows = 0 Then Exit Sub
    If sdbgRes.Row < 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Select Case sdbgRes.Columns(0).caption
        
        Case basParametros.gParametrosGenerales.gsSingPres3
            If Me.g_sPRES4 <> "" Then
                Exit Sub
            ElseIf Me.g_sPRES3 <> "" Then 'Si g_sPRES3 <> "" en la grid están los de nivel 4
                Exit Sub
            ElseIf Me.g_sPRES2 <> "" Then
                Set ADORs = oGestorInformes.AhorroNegociadoConcep3Fecha(Me.g_dFecha, Me.g_sPRES1, Me.g_sPRES2, sdbgRes.Columns(0).Value, , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
            ElseIf Me.g_sPRES1 <> "" Then
                Set ADORs = oGestorInformes.AhorroNegociadoConcep3Fecha(Me.g_dFecha, Me.g_sPRES1, sdbgRes.Columns(0).Value, , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
            Else
                Set ADORs = oGestorInformes.AhorroNegociadoConcep3Fecha(Me.g_dFecha, sdbgRes.Columns(0).Value, , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
            End If
            If ADORs Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            Set frm = New frmInfAhorroNegPresDetalle

            frm.g_dFecha = Me.g_dFecha
            frm.g_bSoloDeAdjDir = Me.g_bSoloDeAdjDir
            frm.g_bSoloEnReunion = Me.g_bSoloEnReunion
    
            If Me.g_sPRES2 <> "" Then
                frm.g_sPRES1 = Me.g_sPRES1
                frm.g_sPRES2 = Me.g_sPRES2
                frm.g_sPRES3 = sdbgRes.Columns(0).Value
                frm.g_sPRES4 = ""
            ElseIf Me.g_sPRES1 <> "" Then
                frm.g_sPRES1 = Me.g_sPRES1
                frm.g_sPRES2 = sdbgRes.Columns(0).Value
                frm.g_sPRES3 = ""
                frm.g_sPRES4 = ""
            Else
                frm.g_sPRES1 = sdbgRes.Columns(0).Value
                frm.g_sPRES2 = ""
                frm.g_sPRES3 = ""
                frm.g_sPRES4 = ""
            End If
            frm.g_sUON1 = Me.g_sUON1
            frm.g_sUON2 = Me.g_sUON2
            frm.g_sUON3 = Me.g_sUON3
            frm.g_dEquivalencia = g_dEquivalencia
            frm.g_sOrigen = g_sOrigen
            frm.caption = sIdiDetResult & "      " & sdbgRes.Columns(0).Value
            frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsSingPres3
            
            While Not ADORs.EOF
                frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs(3).Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs(4).Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                ADORs.MoveNext
            Wend
            ADORs.Close
            Set ADORs = Nothing
            frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
            frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
            Screen.MousePointer = vbNormal
            
            frm.Show 1
            
            sdbgRes.SelBookmarks.RemoveAll
        
        Case basParametros.gParametrosGenerales.gsSingPres4
            If Me.g_sPRES4 <> "" Then
                Exit Sub
            ElseIf Me.g_sPRES3 <> "" Then
                Exit Sub
            ElseIf Me.g_sPRES2 <> "" Then
                Set ADORs = oGestorInformes.AhorroNegociadoConcep4Fecha(Me.g_dFecha, Me.g_sPRES1, Me.g_sPRES2, sdbgRes.Columns(0).Value, , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
            ElseIf Me.g_sPRES1 <> "" Then
                Set ADORs = oGestorInformes.AhorroNegociadoConcep4Fecha(Me.g_dFecha, Me.g_sPRES1, sdbgRes.Columns(0).Value, , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
            Else
                Set ADORs = oGestorInformes.AhorroNegociadoConcep4Fecha(Me.g_dFecha, sdbgRes.Columns(0).Value, , , , , , True, , Me.g_bSoloEnReunion, Me.g_bSoloDeAdjDir, Me.g_sUON1, Me.g_sUON2, Me.g_sUON3)
            End If
            If ADORs Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            Set frm = New frmInfAhorroNegPresDetalle
            
            frm.g_dFecha = Me.g_dFecha
            frm.g_bSoloDeAdjDir = Me.g_bSoloDeAdjDir
            frm.g_bSoloEnReunion = Me.g_bSoloEnReunion
            frm.caption = sIdiDetResult & "      " & sdbgRes.Columns(0).Value
            frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsSingPres4
            If Me.g_sPRES2 <> "" Then
                frm.g_sPRES1 = Me.g_sPRES1
                frm.g_sPRES2 = Me.g_sPRES2
                frm.g_sPRES3 = sdbgRes.Columns(0).Value
                frm.g_sPRES4 = ""
            ElseIf Me.g_sPRES1 <> "" Then
                frm.g_sPRES1 = g_sPRES1
                frm.g_sPRES2 = sdbgRes.Columns(0).Value
                frm.g_sPRES3 = ""
                frm.g_sPRES4 = ""
            Else
                frm.g_sPRES1 = sdbgRes.Columns(0).Value
                frm.g_sPRES2 = ""
                frm.g_sPRES3 = ""
                frm.g_sPRES4 = ""
            End If
            frm.g_sUON1 = Me.g_sUON1
            frm.g_sUON2 = Me.g_sUON2
            frm.g_sUON3 = Me.g_sUON3
            frm.g_dEquivalencia = g_dEquivalencia
            frm.g_sOrigen = g_sOrigen
            
            While Not ADORs.EOF
                frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs(3).Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs(4).Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                ADORs.MoveNext
            Wend
            ADORs.Close
            Set ADORs = Nothing
            frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
            frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
            Screen.MousePointer = vbNormal
            
            frm.Show 1
            
            sdbgRes.SelBookmarks.RemoveAll
            
        
    End Select
        
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
    
    If g_sOrigen = "Concep3" Or g_sOrigen = "Concep4" Then
        If sdbgRes.Columns("ASIG").Value = "0" Then
            For i = 0 To 4
                sdbgRes.Columns(i).CellStyleSet "Blue"
            Next
        Else
            For i = 0 To 4
                sdbgRes.Columns(i).CellStyleSet "White"
            Next
        End If
    End If
    
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRO_NEGDETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        m_sIdiDetalle = Ador(0).Value
        Ador.MoveNext

        Ador.MoveNext
        sdbgRes.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        
        
        sdbcTipoGrafico.RemoveAll
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        sIdiDetResult = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(6).caption = Ador(0).Value
        
        Ador.Close
    
    End If

    Set Ador = Nothing
    
'    If g_sOrigen = "Concep3" Then
'        lblItemAsig.Caption = Replace(sIdiItemAsig, "CONCEP", LCase(gParametrosGenerales.gsSingPres3))
'        lblItemSinAsig.Caption = Replace(sIdiItemSinAsig, "CONCEP", LCase(gParametrosGenerales.gsSingPres3))
'    End If
'    If g_sOrigen = "Concep4" Then
'        lblItemAsig.Caption = Replace(sIdiItemAsig, "CONCEP", LCase(gParametrosGenerales.gsSingPres4))
'        lblItemSinAsig.Caption = Replace(sIdiItemSinAsig, "CONCEP", LCase(gParametrosGenerales.gsSingPres4))
'    End If
'
End Sub








