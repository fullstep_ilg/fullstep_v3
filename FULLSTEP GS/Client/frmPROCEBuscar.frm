VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPROCEBuscar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar proceso"
   ClientHeight    =   8295
   ClientLeft      =   5160
   ClientTop       =   2010
   ClientWidth     =   8190
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROCEBuscar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8295
   ScaleWidth      =   8190
   ShowInTaskbar   =   0   'False
   Begin TabDlg.SSTab sstabProce 
      Height          =   8130
      Left            =   45
      TabIndex        =   0
      Top             =   15
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   14340
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BackColor       =   8421376
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Datos de busqueda"
      TabPicture(0)   =   "frmPROCEBuscar.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "sstabFiltro"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Procesos"
      TabPicture(1)   =   "frmPROCEBuscar.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "sdbgProce"
      Tab(1).Control(1)=   "cmdAceptar"
      Tab(1).Control(2)=   "cmdCancelar"
      Tab(1).Control(3)=   "sdbgItems"
      Tab(1).ControlCount=   4
      Begin TabDlg.SSTab sstabFiltro 
         Height          =   7650
         Left            =   90
         TabIndex        =   49
         Top             =   330
         Width           =   7905
         _ExtentX        =   13944
         _ExtentY        =   13494
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "DFiltro de datos generales"
         TabPicture(0)   =   "frmPROCEBuscar.frx":0182
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraGenerales"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraArt"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraPedidos"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "fraMat"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "fraGrupo"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "fraProve"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Filtro de fechas, presupuestos y personas relacionadas"
         TabPicture(1)   =   "frmPROCEBuscar.frx":019E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Frame3"
         Tab(1).Control(1)=   "fraResp"
         Tab(1).Control(2)=   "fraUsuAper"
         Tab(1).Control(3)=   "fraPresupuestos"
         Tab(1).Control(4)=   "fraFechas"
         Tab(1).ControlCount=   5
         Begin VB.Frame fraProve 
            Caption         =   "Filtro por proveedor"
            Height          =   645
            Left            =   45
            TabIndex        =   124
            Top             =   6930
            Width           =   7815
            Begin VB.CommandButton cmdSelProveBuscar 
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   7425
               Picture         =   "frmPROCEBuscar.frx":01BA
               Style           =   1  'Graphical
               TabIndex        =   125
               TabStop         =   0   'False
               Top             =   210
               Width           =   315
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
               Height          =   285
               Left            =   3690
               TabIndex        =   32
               Top             =   210
               Width           =   3705
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6535
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
               Height          =   285
               Left            =   1860
               TabIndex        =   31
               Top             =   210
               Width           =   1815
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
         End
         Begin VB.Frame Frame3 
            Caption         =   "Comprador"
            Height          =   1605
            Left            =   -74955
            TabIndex        =   98
            Top             =   5550
            Width           =   7815
            Begin VB.OptionButton optResp 
               Caption         =   "Responsable"
               Height          =   195
               Left            =   2100
               TabIndex        =   42
               Top             =   315
               Width           =   1635
            End
            Begin VB.OptionButton optComp 
               Caption         =   "Asignado"
               Height          =   195
               Left            =   240
               TabIndex        =   41
               Top             =   315
               Width           =   1815
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
               Height          =   285
               Left            =   1875
               TabIndex        =   43
               Top             =   705
               Width           =   4995
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1640
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   8811
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcCompDen 
               Height          =   285
               Left            =   1875
               TabIndex        =   44
               Top             =   1125
               Width           =   4995
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   5424
               Columns(0).Caption=   "Apellido y nombre"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1296
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   8811
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label Label7 
               BackStyle       =   0  'Transparent
               Caption         =   "Comprador:"
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   135
               TabIndex        =   122
               Top             =   1140
               Width           =   1800
            End
            Begin VB.Label Label6 
               BackStyle       =   0  'Transparent
               Caption         =   "Equipo:"
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   135
               TabIndex        =   121
               Top             =   720
               Width           =   1800
            End
         End
         Begin VB.Frame fraResp 
            Caption         =   "Datos del responsable del proceso"
            Height          =   720
            Left            =   -74955
            TabIndex        =   97
            Top             =   4770
            Width           =   7815
            Begin VB.CommandButton cmdSelResp 
               Height          =   285
               Left            =   7305
               Picture         =   "frmPROCEBuscar.frx":0247
               Style           =   1  'Graphical
               TabIndex        =   119
               TabStop         =   0   'False
               Top             =   270
               Width           =   315
            End
            Begin VB.CommandButton cmdBorrarResp 
               Height          =   285
               Left            =   6945
               Picture         =   "frmPROCEBuscar.frx":02B3
               Style           =   1  'Graphical
               TabIndex        =   118
               TabStop         =   0   'False
               Top             =   270
               Width           =   315
            End
            Begin VB.Label lblResp 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1860
               TabIndex        =   120
               Top             =   270
               Width           =   4995
            End
         End
         Begin VB.Frame fraUsuAper 
            Caption         =   "Datos del usuario que abre el proceso"
            Height          =   720
            Left            =   -74955
            TabIndex        =   96
            Top             =   3975
            Width           =   7815
            Begin VB.CommandButton cmdSelUsuAper 
               Height          =   285
               Left            =   7305
               Picture         =   "frmPROCEBuscar.frx":0358
               Style           =   1  'Graphical
               TabIndex        =   116
               TabStop         =   0   'False
               Top             =   270
               Width           =   315
            End
            Begin VB.CommandButton cmdBorrarUsuAper 
               Height          =   285
               Left            =   6945
               Picture         =   "frmPROCEBuscar.frx":03C4
               Style           =   1  'Graphical
               TabIndex        =   115
               TabStop         =   0   'False
               Top             =   270
               Width           =   315
            End
            Begin VB.Label lblUsuAper 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1860
               TabIndex        =   117
               Top             =   270
               Width           =   4995
            End
         End
         Begin VB.Frame fraPresupuestos 
            Caption         =   "Conceptos presupuestarios"
            Height          =   1800
            Left            =   -74955
            TabIndex        =   95
            Top             =   2130
            Width           =   7815
            Begin VB.CommandButton cmdSelPresup4 
               Height          =   285
               Left            =   7305
               Picture         =   "frmPROCEBuscar.frx":0469
               Style           =   1  'Graphical
               TabIndex        =   112
               TabStop         =   0   'False
               Top             =   1395
               Width           =   315
            End
            Begin VB.CommandButton cmdBorrarPresup4 
               Height          =   285
               Left            =   6945
               Picture         =   "frmPROCEBuscar.frx":04D5
               Style           =   1  'Graphical
               TabIndex        =   111
               TabStop         =   0   'False
               Top             =   1395
               Width           =   315
            End
            Begin VB.CommandButton cmdSelPresup3 
               Height          =   285
               Left            =   7305
               Picture         =   "frmPROCEBuscar.frx":057A
               Style           =   1  'Graphical
               TabIndex        =   108
               TabStop         =   0   'False
               Top             =   990
               Width           =   315
            End
            Begin VB.CommandButton cmdBorrarPresup3 
               Height          =   285
               Left            =   6945
               Picture         =   "frmPROCEBuscar.frx":05E6
               Style           =   1  'Graphical
               TabIndex        =   107
               TabStop         =   0   'False
               Top             =   990
               Width           =   315
            End
            Begin VB.CommandButton cmdSelPresup2 
               Height          =   285
               Left            =   7305
               Picture         =   "frmPROCEBuscar.frx":068B
               Style           =   1  'Graphical
               TabIndex        =   104
               TabStop         =   0   'False
               Top             =   585
               Width           =   315
            End
            Begin VB.CommandButton cmdBorrarPresup2 
               Height          =   285
               Left            =   6945
               Picture         =   "frmPROCEBuscar.frx":06F7
               Style           =   1  'Graphical
               TabIndex        =   103
               TabStop         =   0   'False
               Top             =   585
               Width           =   315
            End
            Begin VB.CommandButton cmdSelPresup1 
               Height          =   285
               Left            =   7305
               Picture         =   "frmPROCEBuscar.frx":079C
               Style           =   1  'Graphical
               TabIndex        =   100
               TabStop         =   0   'False
               Top             =   195
               Width           =   315
            End
            Begin VB.CommandButton cmdBorrarPresup1 
               Height          =   285
               Left            =   6945
               Picture         =   "frmPROCEBuscar.frx":0808
               Style           =   1  'Graphical
               TabIndex        =   99
               TabStop         =   0   'False
               Top             =   195
               Width           =   315
            End
            Begin VB.Label lblPresup4 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1860
               TabIndex        =   114
               Top             =   1395
               Width           =   4995
            End
            Begin VB.Label lblPresup 
               Caption         =   "Presupuesto tipo4:"
               Height          =   225
               Index           =   3
               Left            =   120
               TabIndex        =   113
               Top             =   1455
               Width           =   1755
            End
            Begin VB.Label lblPresup3 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1860
               TabIndex        =   110
               Top             =   990
               Width           =   4995
            End
            Begin VB.Label lblPresup 
               Caption         =   "Presupuesto tipo3:"
               Height          =   225
               Index           =   2
               Left            =   120
               TabIndex        =   109
               Top             =   1050
               Width           =   1755
            End
            Begin VB.Label lblPresup2 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1860
               TabIndex        =   106
               Top             =   585
               Width           =   4995
            End
            Begin VB.Label lblPresup 
               Caption         =   "Presupuesto tipo2:"
               Height          =   225
               Index           =   1
               Left            =   120
               TabIndex        =   105
               Top             =   645
               Width           =   1755
            End
            Begin VB.Label lblPresup1 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1860
               TabIndex        =   102
               Top             =   195
               Width           =   4995
            End
            Begin VB.Label lblPresup 
               Caption         =   "Presupuesto tipo1:"
               Height          =   225
               Index           =   0
               Left            =   135
               TabIndex        =   101
               Top             =   255
               Width           =   1755
            End
         End
         Begin VB.Frame fraGrupo 
            Caption         =   "Grupo"
            Height          =   600
            Left            =   45
            TabIndex        =   92
            Top             =   3405
            Width           =   7815
            Begin VB.TextBox txtCodGrupo 
               Height          =   285
               Left            =   1860
               MaxLength       =   100
               TabIndex        =   16
               Top             =   225
               Width           =   1575
            End
            Begin VB.TextBox txtDenGrupo 
               Height          =   285
               Left            =   4020
               MaxLength       =   100
               TabIndex        =   17
               Top             =   225
               Width           =   3705
            End
            Begin VB.Label lblDenGr 
               Caption         =   "Den.:"
               Height          =   225
               Left            =   3570
               TabIndex        =   94
               Top             =   285
               Width           =   420
            End
            Begin VB.Label lblCodGr 
               Caption         =   "C�digo:"
               Height          =   225
               Left            =   120
               TabIndex        =   93
               Top             =   255
               Width           =   1095
            End
         End
         Begin VB.Frame fraMat 
            Caption         =   "Material"
            Height          =   1710
            Left            =   45
            TabIndex        =   87
            Top             =   4590
            Width           =   7815
            Begin VB.CommandButton cmdSelMat 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   7410
               Picture         =   "frmPROCEBuscar.frx":08AD
               Style           =   1  'Graphical
               TabIndex        =   88
               TabStop         =   0   'False
               Top             =   180
               Width           =   345
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
               Height          =   285
               Left            =   1860
               TabIndex        =   23
               Top             =   615
               Width           =   1260
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   900
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2222
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
               Height          =   285
               Left            =   1860
               TabIndex        =   25
               Top             =   960
               Width           =   1260
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   900
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2222
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
               Height          =   285
               Left            =   1860
               TabIndex        =   27
               Top             =   1290
               Width           =   1260
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   900
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2222
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
               Height          =   285
               Left            =   3135
               TabIndex        =   24
               Top             =   615
               Width           =   3765
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4498
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1164
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6641
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
               Height          =   285
               Left            =   3135
               TabIndex        =   26
               Top             =   945
               Width           =   3765
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4498
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1164
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6641
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
               Height          =   285
               Left            =   3135
               TabIndex        =   28
               Top             =   1290
               Width           =   3765
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4498
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1164
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6641
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
               Height          =   285
               Left            =   1860
               TabIndex        =   21
               Top             =   285
               Width           =   1260
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   900
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2222
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
               Height          =   285
               Left            =   3135
               TabIndex        =   22
               Top             =   285
               Width           =   3765
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4498
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1164
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6641
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblGMN1_4 
               Caption         =   "Familia:"
               Height          =   225
               Left            =   120
               TabIndex        =   126
               Top             =   300
               Width           =   1680
            End
            Begin VB.Label lblGMN3_4 
               Caption         =   "Subfamilia:"
               Height          =   225
               Left            =   120
               TabIndex        =   91
               Top             =   990
               Width           =   1650
            End
            Begin VB.Label lblGMN2_4 
               Caption         =   "Familia:"
               Height          =   225
               Left            =   120
               TabIndex        =   90
               Top             =   660
               Width           =   1680
            End
            Begin VB.Label lblGMN4_4 
               Caption         =   "Grupo:"
               Height          =   225
               Left            =   135
               TabIndex        =   89
               Top             =   1350
               Width           =   1650
            End
         End
         Begin VB.Frame fraPedidos 
            Caption         =   "Pedidos"
            Height          =   585
            Left            =   45
            TabIndex        =   85
            Top             =   4005
            Visible         =   0   'False
            Width           =   7815
            Begin VB.OptionButton optSinPedios 
               Caption         =   "Sin pedidos"
               Height          =   225
               Left            =   330
               TabIndex        =   18
               Top             =   240
               Width           =   1995
            End
            Begin VB.OptionButton optConPedidos 
               Caption         =   "Con pedidos"
               Height          =   225
               Left            =   2445
               TabIndex        =   19
               Top             =   240
               Width           =   2085
            End
            Begin VB.OptionButton optExcedida 
               Caption         =   "Excedidia adj. en pedidos"
               Height          =   270
               Left            =   4635
               TabIndex        =   20
               Top             =   210
               Width           =   2580
            End
            Begin VB.CommandButton cmdBorrar 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   7305
               Picture         =   "frmPROCEBuscar.frx":0919
               Style           =   1  'Graphical
               TabIndex        =   86
               TabStop         =   0   'False
               Top             =   165
               Width           =   345
            End
         End
         Begin VB.Frame fraArt 
            Caption         =   "Art�culo"
            Height          =   600
            Left            =   45
            TabIndex        =   82
            Top             =   6300
            Width           =   7815
            Begin VB.TextBox txtDenArticulo 
               Height          =   285
               Left            =   4020
               MaxLength       =   100
               TabIndex        =   30
               Top             =   195
               Width           =   3705
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcCodArticulo 
               Height          =   285
               Left            =   1860
               TabIndex        =   29
               Top             =   195
               Width           =   1650
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2381
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   7170
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2910
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin VB.Label lblCodArticulo 
               Caption         =   "C�digo:"
               Height          =   225
               Left            =   120
               TabIndex        =   84
               Top             =   225
               Width           =   1095
            End
            Begin VB.Label lblDenArticulo 
               Caption         =   "Den.:"
               Height          =   225
               Left            =   3570
               TabIndex        =   83
               Top             =   255
               Width           =   420
            End
         End
         Begin VB.Frame fraFechas 
            Caption         =   "Fechas"
            Height          =   1725
            Left            =   -74955
            TabIndex        =   65
            Top             =   360
            Width           =   7815
            Begin VB.CommandButton cmdCalFecPresDesde 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3000
               Picture         =   "frmPROCEBuscar.frx":09BE
               Style           =   1  'Graphical
               TabIndex        =   73
               TabStop         =   0   'False
               Top             =   975
               Width           =   315
            End
            Begin VB.TextBox txtFecPresDesde 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1860
               TabIndex        =   37
               Top             =   960
               Width           =   1110
            End
            Begin VB.CommandButton cmdCalFecPresHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6240
               Picture         =   "frmPROCEBuscar.frx":0F48
               Style           =   1  'Graphical
               TabIndex        =   72
               TabStop         =   0   'False
               Top             =   960
               Width           =   315
            End
            Begin VB.TextBox txtFecPresHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5100
               TabIndex        =   38
               Top             =   960
               Width           =   1110
            End
            Begin VB.CommandButton cmdCalFecNecDesde 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3000
               Picture         =   "frmPROCEBuscar.frx":14D2
               Style           =   1  'Graphical
               TabIndex        =   71
               TabStop         =   0   'False
               Top             =   615
               Width           =   315
            End
            Begin VB.TextBox txtFecNeceDesde 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1860
               TabIndex        =   35
               Top             =   600
               Width           =   1110
            End
            Begin VB.CommandButton cmdCalfecNecHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6240
               Picture         =   "frmPROCEBuscar.frx":1A5C
               Style           =   1  'Graphical
               TabIndex        =   70
               TabStop         =   0   'False
               Top             =   615
               Width           =   315
            End
            Begin VB.TextBox txtFecNeceHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5100
               TabIndex        =   36
               Top             =   600
               Width           =   1110
            End
            Begin VB.CommandButton cmdCalFecApeDesde 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3000
               Picture         =   "frmPROCEBuscar.frx":1FE6
               Style           =   1  'Graphical
               TabIndex        =   69
               TabStop         =   0   'False
               Top             =   255
               Width           =   315
            End
            Begin VB.TextBox txtFecApeDesde 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1860
               TabIndex        =   33
               Top             =   240
               Width           =   1110
            End
            Begin VB.CommandButton cmdCalFecApeHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6240
               Picture         =   "frmPROCEBuscar.frx":2570
               Style           =   1  'Graphical
               TabIndex        =   68
               TabStop         =   0   'False
               Top             =   255
               Width           =   315
            End
            Begin VB.TextBox txtFecApeHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5100
               TabIndex        =   34
               Top             =   240
               Width           =   1110
            End
            Begin VB.CommandButton cmdCalFecUltReuDesde 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3000
               Picture         =   "frmPROCEBuscar.frx":2AFA
               Style           =   1  'Graphical
               TabIndex        =   67
               TabStop         =   0   'False
               Top             =   1335
               Width           =   315
            End
            Begin VB.TextBox txtFecUltReuDesde 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1860
               TabIndex        =   39
               Top             =   1320
               Width           =   1110
            End
            Begin VB.CommandButton cmdcalFecUltReuHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6240
               Picture         =   "frmPROCEBuscar.frx":3084
               Style           =   1  'Graphical
               TabIndex        =   66
               TabStop         =   0   'False
               Top             =   1320
               Width           =   315
            End
            Begin VB.TextBox txtFecUltReuHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5100
               TabIndex        =   40
               Top             =   1320
               Width           =   1110
            End
            Begin VB.Label lblFecApeDesde 
               BackStyle       =   0  'Transparent
               Caption         =   "Apertura:"
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   120
               TabIndex        =   81
               Top             =   300
               Width           =   1800
            End
            Begin VB.Label lblFecNeceDesde 
               BackStyle       =   0  'Transparent
               Caption         =   "Necesidad:"
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   120
               TabIndex        =   80
               Top             =   660
               Width           =   1800
            End
            Begin VB.Label lblFecPresDesde 
               BackStyle       =   0  'Transparent
               Caption         =   "Presentaci�n:"
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   120
               TabIndex        =   79
               Top             =   1005
               Width           =   1800
            End
            Begin VB.Label lblFecApeHasta 
               Caption         =   "Hasta:"
               Height          =   225
               Left            =   4245
               TabIndex        =   78
               Top             =   300
               Width           =   840
            End
            Begin VB.Label lblFecNeceHasta 
               Caption         =   "Hasta:"
               Height          =   225
               Left            =   4245
               TabIndex        =   77
               Top             =   660
               Width           =   840
            End
            Begin VB.Label lblFecPresHasta 
               Caption         =   "Hasta:"
               Height          =   225
               Left            =   4245
               TabIndex        =   76
               Top             =   1020
               Width           =   840
            End
            Begin VB.Label lblFecUltReuDesde 
               BackStyle       =   0  'Transparent
               Caption         =   "Ultima reuni�n:"
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   120
               TabIndex        =   75
               Top             =   1365
               Width           =   1800
            End
            Begin VB.Label lblFecUltReuHasta 
               Caption         =   "Hasta:"
               Height          =   225
               Left            =   4245
               TabIndex        =   74
               Top             =   1380
               Width           =   840
            End
         End
         Begin VB.Frame fraGenerales 
            Height          =   3090
            Left            =   45
            TabIndex        =   50
            Top             =   315
            Width           =   7815
            Begin VB.TextBox txtCod 
               Height          =   285
               Left            =   6000
               MaxLength       =   20
               TabIndex        =   3
               Top             =   135
               Width           =   930
            End
            Begin VB.TextBox txtDen 
               Height          =   285
               Left            =   1860
               MaxLength       =   100
               TabIndex        =   4
               Top             =   465
               Width           =   5070
            End
            Begin VB.TextBox txtPresDesde 
               Height          =   285
               Left            =   1860
               MaxLength       =   20
               TabIndex        =   8
               Top             =   1455
               Width           =   1600
            End
            Begin VB.TextBox txtPresHasta 
               Height          =   285
               Left            =   4905
               MaxLength       =   100
               TabIndex        =   9
               Top             =   1455
               Width           =   2025
            End
            Begin VB.CheckBox chkAdjDir 
               Caption         =   "De adjudicaci�n directa"
               Height          =   195
               Left            =   1860
               TabIndex        =   14
               Top             =   2820
               Value           =   1  'Checked
               Width           =   2490
            End
            Begin VB.CheckBox chkAdjReu 
               Caption         =   "De adjudicaci�n mediante reuni�n"
               Height          =   195
               Left            =   4680
               TabIndex        =   15
               Top             =   2790
               Value           =   1  'Checked
               Width           =   3100
            End
            Begin VB.TextBox txtReferencia 
               Height          =   285
               Left            =   1860
               MaxLength       =   20
               TabIndex        =   10
               Top             =   1785
               Width           =   1600
            End
            Begin VB.CheckBox chkSubasta 
               Caption         =   "Recibe ofertas en modo subasta"
               Height          =   195
               Left            =   3600
               TabIndex        =   11
               Top             =   1830
               Width           =   4000
            End
            Begin VB.CommandButton cmdBuscarSolic 
               Height          =   285
               Left            =   4320
               Picture         =   "frmPROCEBuscar.frx":360E
               Style           =   1  'Graphical
               TabIndex        =   52
               TabStop         =   0   'False
               Top             =   2445
               Width           =   315
            End
            Begin VB.CommandButton cmdBorrarSolic 
               Height          =   285
               Left            =   3960
               Picture         =   "frmPROCEBuscar.frx":369B
               Style           =   1  'Graphical
               TabIndex        =   51
               TabStop         =   0   'False
               Top             =   2445
               Width           =   315
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN1Proce_Cod 
               Height          =   285
               Left            =   3915
               TabIndex        =   2
               Top             =   150
               Width           =   870
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   900
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1535
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
               Height          =   285
               Left            =   1860
               TabIndex        =   1
               Top             =   135
               Width           =   930
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns(0).Width=   1693
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               _ExtentX        =   1640
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
               Height          =   285
               Left            =   1860
               TabIndex        =   6
               Top             =   1125
               Width           =   945
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1667
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonDen 
               Height          =   285
               Left            =   2790
               TabIndex        =   7
               Top             =   1125
               Width           =   4140
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   7302
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcDestCodProce 
               Height          =   285
               Left            =   1860
               TabIndex        =   12
               Top             =   2115
               Width           =   1275
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   7
               Columns(0).Width=   1931
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   8017
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Caption=   "Direcci�n"
               Columns(2).Name =   "DIR"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   2143
               Columns(3).Caption=   "Poblaci�n"
               Columns(3).Name =   "POB"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   1217
               Columns(4).Caption=   "CP"
               Columns(4).Name =   "CP"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               Columns(5).Width=   1296
               Columns(5).Caption=   "Pais"
               Columns(5).Name =   "PAIS"
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).FieldLen=   256
               Columns(6).Width=   1429
               Columns(6).Caption=   "Provincia"
               Columns(6).Name =   "PROVI"
               Columns(6).DataField=   "Column 6"
               Columns(6).DataType=   8
               Columns(6).FieldLen=   256
               _ExtentX        =   2249
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcDestDenProce 
               Height          =   285
               Left            =   3135
               TabIndex        =   13
               Top             =   2115
               Width           =   3795
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   7
               Columns(0).Width=   7673
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2143
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   2487
               Columns(2).Caption=   "Direcci�n"
               Columns(2).Name =   "DIR"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   1958
               Columns(3).Caption=   "Poblaci�n"
               Columns(3).Name =   "POB"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   1164
               Columns(4).Caption=   "CP"
               Columns(4).Name =   "CP"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               Columns(5).Width=   1032
               Columns(5).Caption=   "Pais"
               Columns(5).Name =   "PAIS"
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).FieldLen=   256
               Columns(6).Width=   1349
               Columns(6).Caption=   "Provincia"
               Columns(6).Name =   "PROVI"
               Columns(6).DataField=   "Column 6"
               Columns(6).DataType=   8
               Columns(6).FieldLen=   256
               _ExtentX        =   6694
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEst 
               Height          =   285
               Left            =   1860
               TabIndex        =   5
               Top             =   795
               Width           =   5070
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   8361
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "COD"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   8943
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblDest 
               Caption         =   "Destino"
               Height          =   225
               Left            =   120
               TabIndex        =   123
               Top             =   2145
               Width           =   1605
            End
            Begin VB.Label lblEst 
               Caption         =   "Estado:"
               Height          =   225
               Left            =   120
               TabIndex        =   64
               Top             =   885
               Width           =   1590
            End
            Begin VB.Label lblCod 
               Caption         =   "C�digo:"
               Height          =   225
               Left            =   4935
               TabIndex        =   63
               Top             =   180
               Width           =   1050
            End
            Begin VB.Label lblDen 
               Caption         =   "Den.:"
               Height          =   225
               Left            =   120
               TabIndex        =   62
               Top             =   540
               Width           =   1590
            End
            Begin VB.Label lblAnyo 
               Caption         =   "A�o:"
               ForeColor       =   &H00000000&
               Height          =   165
               Left            =   120
               TabIndex        =   61
               Top             =   195
               Width           =   1590
            End
            Begin VB.Label lblPresDesde 
               Caption         =   "Presupuesto:"
               Height          =   225
               Left            =   120
               TabIndex        =   60
               Top             =   1515
               Width           =   1590
            End
            Begin VB.Label lblPresHasta 
               Caption         =   "Hasta:"
               Height          =   225
               Left            =   4095
               TabIndex        =   59
               Top             =   1515
               Width           =   780
            End
            Begin VB.Label lblMon 
               Caption         =   "Moneda:"
               Height          =   240
               Left            =   120
               TabIndex        =   58
               Top             =   1185
               Width           =   1515
            End
            Begin VB.Label lblGMN1Proce 
               Caption         =   "Commodity:"
               Height          =   225
               Left            =   2880
               TabIndex        =   57
               Top             =   180
               Width           =   930
            End
            Begin VB.Label lblReferencia 
               Caption         =   "DReferencia:"
               Height          =   225
               Left            =   120
               TabIndex        =   56
               Top             =   1845
               Width           =   1590
            End
            Begin VB.Label lblSolicitud 
               Caption         =   "DSolicitud:"
               Height          =   255
               Left            =   120
               TabIndex        =   55
               Top             =   2475
               Width           =   1335
            End
            Begin VB.Label lblSolicCompras 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1860
               TabIndex        =   54
               Top             =   2445
               Width           =   2025
            End
            Begin VB.Label lblIdSolic 
               Height          =   255
               Left            =   4920
               TabIndex        =   53
               Top             =   2235
               Visible         =   0   'False
               Width           =   735
            End
         End
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgProce 
         Height          =   6840
         Left            =   -74865
         TabIndex        =   47
         Top             =   480
         Width           =   7860
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   8
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmPROCEBuscar.frx":3740
         UseGroups       =   -1  'True
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         UseExactRowCount=   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Groups(0).Width =   12912
         Groups(0).Caption=   "Procesos"
         Groups(0).HasHeadForeColor=   -1  'True
         Groups(0).HasHeadBackColor=   -1  'True
         Groups(0).HeadForeColor=   16777215
         Groups(0).HeadBackColor=   8421504
         Groups(0).Columns.Count=   8
         Groups(0).Columns(0).Width=   953
         Groups(0).Columns(0).Visible=   0   'False
         Groups(0).Columns(0).Caption=   "Cod"
         Groups(0).Columns(0).Name=   "COD"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(0).Locked=   -1  'True
         Groups(0).Columns(1).Width=   1323
         Groups(0).Columns(1).Caption=   "ANYO"
         Groups(0).Columns(1).Name=   "ANYO"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(0).Columns(2).Width=   1217
         Groups(0).Columns(2).Caption=   "GMN1"
         Groups(0).Columns(2).Name=   "GMN1"
         Groups(0).Columns(2).DataField=   "Column 2"
         Groups(0).Columns(2).DataType=   8
         Groups(0).Columns(2).FieldLen=   256
         Groups(0).Columns(3).Width=   1217
         Groups(0).Columns(3).Caption=   "Cod"
         Groups(0).Columns(3).Name=   "PROCESO"
         Groups(0).Columns(3).DataField=   "Column 3"
         Groups(0).Columns(3).DataType=   8
         Groups(0).Columns(3).FieldLen=   256
         Groups(0).Columns(4).Width=   3519
         Groups(0).Columns(4).Caption=   "Descr"
         Groups(0).Columns(4).Name=   "DESCR"
         Groups(0).Columns(4).DataField=   "Column 4"
         Groups(0).Columns(4).DataType=   8
         Groups(0).Columns(4).FieldLen=   256
         Groups(0).Columns(4).Locked=   -1  'True
         Groups(0).Columns(5).Width=   2884
         Groups(0).Columns(5).Caption=   "Estado"
         Groups(0).Columns(5).Name=   "EST"
         Groups(0).Columns(5).DataField=   "Column 5"
         Groups(0).Columns(5).DataType=   8
         Groups(0).Columns(5).FieldLen=   256
         Groups(0).Columns(6).Width=   2752
         Groups(0).Columns(6).Caption=   "Resp"
         Groups(0).Columns(6).Name=   "RESP"
         Groups(0).Columns(6).DataField=   "Column 6"
         Groups(0).Columns(6).DataType=   8
         Groups(0).Columns(6).FieldLen=   256
         Groups(0).Columns(6).Style=   4
         Groups(0).Columns(6).ButtonsAlways=   -1  'True
         Groups(0).Columns(7).Width=   6271
         Groups(0).Columns(7).Visible=   0   'False
         Groups(0).Columns(7).Caption=   "ESTID"
         Groups(0).Columns(7).Name=   "ESTID"
         Groups(0).Columns(7).DataField=   "Column 7"
         Groups(0).Columns(7).DataType=   8
         Groups(0).Columns(7).FieldLen=   256
         _ExtentX        =   13864
         _ExtentY        =   12065
         _StockProps     =   79
         BackColor       =   -2147483644
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Seleccionar"
         Default         =   -1  'True
         Height          =   315
         Left            =   -72030
         TabIndex        =   45
         Top             =   7380
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cerrar"
         Height          =   315
         Left            =   -70890
         TabIndex        =   46
         Top             =   7380
         Width           =   1005
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgItems 
         Height          =   2010
         Left            =   -74880
         TabIndex        =   48
         Top             =   4545
         Width           =   7860
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   6
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmPROCEBuscar.frx":375C
         stylesets(1).Name=   "Cerrado"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   9408399
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmPROCEBuscar.frx":3778
         UseGroups       =   -1  'True
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         UseExactRowCount=   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Groups(0).Width =   12965
         Groups(0).Caption=   "Items"
         Groups(0).HasHeadForeColor=   -1  'True
         Groups(0).HasHeadBackColor=   -1  'True
         Groups(0).HeadForeColor=   16777215
         Groups(0).HeadBackColor=   8421504
         Groups(0).Columns.Count=   6
         Groups(0).Columns(0).Width=   1535
         Groups(0).Columns(0).Caption=   "Cod"
         Groups(0).Columns(0).Name=   "Cod"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(1).Width=   5821
         Groups(0).Columns(1).Caption=   "Descr"
         Groups(0).Columns(1).Name=   "Descr"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(0).Columns(2).Width=   1826
         Groups(0).Columns(2).Caption=   "Destino"
         Groups(0).Columns(2).Name=   "Destino"
         Groups(0).Columns(2).DataField=   "Column 2"
         Groups(0).Columns(2).DataType=   8
         Groups(0).Columns(2).FieldLen=   256
         Groups(0).Columns(3).Width=   1879
         Groups(0).Columns(3).Caption=   "Inicio"
         Groups(0).Columns(3).Name=   "Inicio"
         Groups(0).Columns(3).DataField=   "Column 3"
         Groups(0).Columns(3).DataType=   8
         Groups(0).Columns(3).FieldLen=   256
         Groups(0).Columns(4).Width=   1905
         Groups(0).Columns(4).Caption=   "Fin"
         Groups(0).Columns(4).Name=   "Fin"
         Groups(0).Columns(4).DataField=   "Column 4"
         Groups(0).Columns(4).DataType=   8
         Groups(0).Columns(4).FieldLen=   256
         Groups(0).Columns(5).Width=   6350
         Groups(0).Columns(5).Visible=   0   'False
         Groups(0).Columns(5).Caption=   "Cerrado"
         Groups(0).Columns(5).Name=   "Cerrado"
         Groups(0).Columns(5).DataField=   "Column 5"
         Groups(0).Columns(5).DataType=   11
         Groups(0).Columns(5).FieldLen=   256
         _ExtentX        =   13864
         _ExtentY        =   3545
         _StockProps     =   79
         BackColor       =   -2147483644
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmPROCEBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public oProceEncontrados As CProcesos
Public g_oProceSeleccionado As CProceso

' Variables de restricciones
Public bRMat As Boolean
Public bRAsig As Boolean
Public bRCompResponsable As Boolean
Public bREqpAsig As Boolean
Public bRUsuAper As Boolean
Public bRUsuUON As Boolean
Public bRPerfUON As Boolean
Public bRUsuDep As Boolean
Public bRDest As Boolean
Public bRDestPerfUO As Boolean
Public bRestProvMatComp As Boolean
Public bRestProvEquComp As Boolean
'Variables para materiales
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4

Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4

Public sProveAdj As String

Private oEqps As CEquipos
Private oEqpSeleccionado As CEquipo

Dim oComps As CCompradores
Private oCompSeleccionado As CComprador
Public m_bProveAsigComp As Boolean
Public m_bProveAsigEqp As Boolean


Dim oProves As CProveedores

'Variables para Art�culos
Private oArticulos As CArticulos
Private oArticuloSeleccionado As CArticulo

' Variables para el manejo de combos
Private bRespetarCombo As Boolean
Public bRespetarComboGMN2 As Boolean
Public bRespetarComboGMN3 As Boolean
Public bRespetarComboGMN4 As Boolean
Public bRespetarComboArt As Boolean
Private bRespetarComboEst As Boolean
Private GMN1CargarComboDesde As Boolean
Private GMN1RespetarCombo As Boolean
Private GMN2CargarComboDesde As Boolean
Private GMN2RespetarCombo As Boolean
Private GMN3CargarComboDesde As Boolean
Private GMN3RespetarCombo As Boolean
Private GMN4CargarComboDesde As Boolean
Private GMN4RespetarCombo As Boolean

Private bCargarComboDesde As Boolean

Private m_bDestRespetarCombo As Boolean
Private bCargarComboDesdeEqp As Boolean
Private bCargarComboDesdeComp As Boolean
Private bRespetarComboProve As Boolean


Private oMonedas As CMonedas
Private m_oDestinos As CDestinos

'Variable para saber el origen
Public sOrigen As String
Public g_oOrigen As Form

Public DesdeEst As TipoEstadoProceso
Public HastaEst As TipoEstadoProceso

Dim dPresDesde As Double
Dim dPresHasta As Double



'Presupuestos:
Private m_vPresup1_1 As Variant
Private m_vPresup1_2 As Variant
Private m_vPresup1_3 As Variant
Private m_vPresup1_4 As Variant

Private m_vPresup2_1 As Variant
Private m_vPresup2_2 As Variant
Private m_vPresup2_3 As Variant
Private m_vPresup2_4 As Variant

Private m_vPresup3_1 As Variant
Private m_vPresup3_2 As Variant
Private m_vPresup3_3 As Variant
Private m_vPresup3_4 As Variant

Private m_vPresup4_1 As Variant
Private m_vPresup4_2 As Variant
Private m_vPresup4_3 As Variant
Private m_vPresup4_4 As Variant


Private m_sUON1Pres1 As Variant

Private m_sUON2Pres1 As Variant
Private m_sUON3Pres1 As Variant

Private m_sUON1Pres2 As Variant
Private m_sUON2Pres2 As Variant
Private m_sUON3Pres2 As Variant

Private m_sUON1Pres3 As Variant
Private m_sUON2Pres3 As Variant
Private m_sUON3Pres3 As Variant

Private m_sUON1Pres4 As Variant
Private m_sUON2Pres4 As Variant
Private m_sUON3Pres4 As Variant

Private m_sCodResp As Variant
Private m_sCodPerAper As Variant

Private m_sUON1Aper As Variant
Private m_sUON2Aper As Variant
Private m_sUON3Aper As Variant

Private m_sUON1Resp As Variant
Private m_sUON2Resp As Variant
Private m_sUON3Resp As Variant

Public m_sDepAper As Variant
Public m_sDepResp As Variant

Private g_bSoloInvitado As Boolean 'Si es true el usuario no tiene acceso a esta pantalla, solo como invitado
Private bPermProcMultiMaterial As Boolean

'Multilenguaje
Private sIdiEst(1 To 10) As String
Private sIdiMoneda As String
Private sIdiMaterial As String
Private sIdiDetalle As String
Private sIdiResponsable As String
Private sIdiFecha As String
Private sIdiPresupuesto As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub cmdBorrar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
optSinPedios.Value = False
optConPedidos.Value = False
optExcedida.Value = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdBorrar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBorrarPresup1_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Me.lblPresup1.caption = ""

m_vPresup1_1 = Null
m_vPresup1_2 = Null
m_vPresup1_3 = Null
m_vPresup1_4 = Null
m_sUON1Pres1 = Null
m_sUON2Pres1 = Null
m_sUON3Pres1 = Null
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdBorrarPresup1_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBorrarPresup2_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_vPresup2_1 = Null
m_vPresup2_2 = Null
m_vPresup2_3 = Null
m_vPresup2_4 = Null
m_sUON1Pres2 = Null
m_sUON2Pres2 = Null
m_sUON3Pres2 = Null

Me.lblPresup2.caption = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdBorrarPresup2_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBorrarPresup3_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Me.lblPresup3.caption = ""
        m_vPresup3_1 = Null
        m_vPresup3_2 = Null
        m_vPresup3_3 = Null
        m_vPresup3_4 = Null
        m_sUON1Pres3 = Null
        m_sUON2Pres3 = Null
        m_sUON3Pres3 = Null
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdBorrarPresup3_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBorrarPresup4_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Me.lblPresup4.caption = ""
m_vPresup4_1 = Null
m_vPresup4_2 = Null
m_vPresup4_3 = Null
m_vPresup4_4 = Null
m_sUON1Pres4 = Null
m_sUON2Pres4 = Null
m_sUON3Pres4 = Null
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdBorrarPresup4_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBorrarResp_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_sCodResp = Null
m_sUON1Resp = Null
m_sUON2Resp = Null
m_sUON3Resp = Null
m_sDepResp = Null
Me.lblResp.caption = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdBorrarResp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBorrarSolic_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblSolicCompras.caption = ""
    lblIdSolic.caption = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdBorrarSolic_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBorrarUsuAper_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_sCodPerAper = Null
m_sUON1Aper = Null
m_sUON2Aper = Null
m_sUON3Aper = Null
m_sDepAper = Null
Me.lblUsuAper.caption = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdBorrarUsuAper_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBuscarSolic_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSolicitudBuscar.g_sOrigen = "frmPROCEBuscar"
    frmSolicitudBuscar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdBuscarSolic_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecNecDesde_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecNeceDesde
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdCalFecNecDesde_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' Click Boton Aceptar devuelve el proceso seleccionado
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Click del boton Tiempo m�ximo: 0,3</remarks>

Private Sub cmdAceptar_Click()
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgProce.Rows = 0 Then
        Unload Me
        Exit Sub
    End If
      
    If sdbgProce.SelBookmarks.Count = 0 Then Exit Sub
    
    Set oProceEncontrados = Nothing
    Set oProceEncontrados = oFSGSRaiz.generar_CProcesos
                
    If basPublic.gParametrosInstalacion.giLockTimeOut <= 0 Then
        basPublic.gParametrosInstalacion.giLockTimeOut = basParametros.gParametrosGenerales.giLockTimeOut
    End If

    oProceEncontrados.EstablecerTiempoDeBloqueo basPublic.gParametrosInstalacion.giLockTimeOut
                  
    Select Case sOrigen
        
        Case "frmPROCE"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
            frmPROCE.CargarProcesoConBusqueda
         
        Case "frmSELPROVE"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
            Me.Hide
            Exit Sub
        
        Case "frmOFEPet"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
            frmOFEPet.CargarProcesoConBusqueda
        
        Case "frmOFERec"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
            frmOFERec.CargarProcesoConBusqueda
        
        Case "frmOfeComp"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
            frmADJ.CargarProcesoConBusqueda
        
        Case "frmREU"
            i = 0
            While i < sdbgProce.SelBookmarks.Count
                oProceEncontrados.Add sdbgProce.Columns("ANYO").CellValue(sdbgProce.SelBookmarks(i)), val(sdbgProce.Columns("COD").CellValue(sdbgProce.SelBookmarks(i))), sdbgProce.Columns("GMN1").CellValue(sdbgProce.SelBookmarks(i)), sdbgProce.Columns("DESCR").CellValue(sdbgProce.SelBookmarks(i)), , , , , , sdbgProce.Columns("ESTID").Value
                i = i + 1
            Wend
            frmREU.CargarProcesoConBusqueda
                
        Case "frmOFEHistWeb"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
            frmOFEHistWeb.CargarProcesoConBusqueda
                
        Case "frmOFEBuzon"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
            frmOFEBuzon.CargarProcesoConBusqueda
    
        Case "frmLstOFEBuzon"
            If sdbcGMN1_4Cod.Value <> frmLstOFEBuzon.sdbcGMN1_4Cod Then
                frmLstOFEBuzon.txtEstMat.Text = ""
            End If
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value

            frmLstOFEBuzon.CargarProcesoConBusqueda
         
         Case "frmSeguimiento"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , sdbcGMN2_4Cod.Value, sdbcGMN3_4Cod.Value, sdbcGMN4_4Cod.Value, sdbgProce.Columns("ESTID").Value
            frmSeguimiento.sdbcArtiCod.Text = sdbcCodArticulo.Text
            frmSeguimiento.sdbcArtiDen.Text = txtDenArticulo.Text
            frmSeguimiento.CargarProcesoConBusqueda
          
         Case "frmPEDIDOS"
            If optSinPedios Then
                g_oOrigen.PedidoSelector.Seleccion = 0
            ElseIf optConPedidos Then
                g_oOrigen.PedidoSelector.Seleccion = 1
            ElseIf optExcedida Then
                g_oOrigen.PedidoSelector.Seleccion = 2
            Else
                g_oOrigen.PedidoSelector.Seleccion = 3
            End If
            
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
                        
            g_oOrigen.CargarProcesoConBusqueda
                
        Case "frmCATALAnyaAdj"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
            g_oOrigen.CargarProcesoConBusqueda
        Case "frmLstCatalogo"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
            frmLstCatalogo.CargarProcesoConBusqueda
        
        Case "frmLstPedidos"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
            frmLstPedidos.CargarProcesoConBusqueda
        
        Case "frmSOLEnviarAProc"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
            frmSOLEnviarAProc.CargarProcesoConBusqueda
                
        Case "frmCopiarGrupo"
            oProceEncontrados.Add val(sdbgProce.Columns("ANYO").Value), val(sdbgProce.Columns("COD").Value), sdbgProce.Columns("GMN1").Value, sdbgProce.Columns("DESCR").Value, , , , , , sdbgProce.Columns("ESTID").Value
                
    End Select
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeDesde_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecApeDesde
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdCalFecApeDesde_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeHasta_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecApeHasta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdCalFecApeHasta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalfecNecHasta_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecNeceHasta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdCalfecNecHasta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecPresDesde_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecPresDesde
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdCalFecPresDesde_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecPresHasta_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecPresHasta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdCalFecPresHasta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecUltReuDesde_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecUltReuDesde
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdCalFecUltReuDesde_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdcalFecUltReuHasta_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecUltReuHasta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdcalFecUltReuHasta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProceEncontrados = Nothing
    Set oMonedas = Nothing
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdSelMat_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELMAT.sOrigen = "frmPROCEBuscar"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.bRCompResponsable = bRCompResponsable
    frmSELMAT.bRUsuAper = bRUsuAper
    frmSELMAT.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdSelMat_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Click Boton Presupuestos tipo 1
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Click del boton Tiempo m�ximo: 0</remarks>
Private Sub cmdSelPresup1_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELPresAnuUON.sOrigen = "frmPROCEBuscar"
    frmSELPresAnuUON.g_iTipoPres = 1
    If sOrigen = "frmPEDIDOS" Then
        frmSELPresAnuUON.bRUO = g_oOrigen.m_bRPresup1UO
        frmSELPresAnuUON.bRuoRama = g_oOrigen.m_bRPresup1UORama
    ElseIf sOrigen = "frmSeguimiento" Then
        frmSELPresAnuUON.bRUO = frmSeguimiento.m_bRPresup1UO
        frmSELPresAnuUON.bRuoRama = frmSeguimiento.m_bRPresup1UORama
    End If
    frmSELPresAnuUON.Show 1
            
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdSelPresup1_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Click Boton Presupuestos tipo 2
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Click del boton Tiempo m�ximo: 0</remarks>
Private Sub cmdSelPresup2_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELPresAnuUON.sOrigen = "frmPROCEBuscar"
    frmSELPresAnuUON.g_iTipoPres = 2
    If sOrigen = "frmPEDIDOS" Then
        frmSELPresAnuUON.bRUO = g_oOrigen.m_bRPresup2UO
        frmSELPresAnuUON.bRuoRama = g_oOrigen.m_bRPresup2UORama
    ElseIf sOrigen = "frmSeguimiento" Then
        frmSELPresAnuUON.bRUO = frmSeguimiento.m_bRPresup2UO
        frmSELPresAnuUON.bRuoRama = frmSeguimiento.m_bRPresup2UORama
    End If
    frmSELPresAnuUON.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdSelPresup2_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
            
End Sub

''' <summary>
''' Click Boton Presupuestos tipo 3
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Click del boton Tiempo m�ximo: 0</remarks>
Private Sub cmdSelPresup3_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELPresUO.sOrigen = "frmPROCEBuscar"
    frmSELPresUO.g_iTipoPres = 3
    If sOrigen = "frmPEDIDOS" Then
        frmSELPresUO.bRUO = g_oOrigen.m_bRPresup3UO
        frmSELPresUO.bRuoRama = g_oOrigen.m_bRPresup3UORama
    ElseIf sOrigen = "frmSeguimiento" Then
        frmSELPresUO.bRUO = frmSeguimiento.m_bRPresup3UO
        frmSELPresUO.bRuoRama = frmSeguimiento.m_bRPresup3UORama
    End If
    Screen.MousePointer = vbNormal
    frmSELPresUO.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdSelPresup3_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Click Boton Presupuestos tipo 4
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Click del boton Tiempo m�ximo: 0</remarks>
Private Sub cmdSelPresup4_Click()
            
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELPresUO.sOrigen = "frmPROCEBuscar"
    frmSELPresUO.g_iTipoPres = 4
        If sOrigen = "frmPEDIDOS" Then
        frmSELPresUO.bRUO = g_oOrigen.m_bRPresup4UO
        frmSELPresUO.bRuoRama = g_oOrigen.m_bRPresup4UORama
    ElseIf sOrigen = "frmSeguimiento" Then
        frmSELPresUO.bRUO = frmSeguimiento.m_bRPresup4UO
        frmSELPresUO.bRuoRama = frmSeguimiento.m_bRPresup4UORama
    End If
    frmSELPresUO.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdSelPresup4_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdSelProveBuscar_Click()
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    Dim lIdPerfil As Long

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") Then
        If Trim(sdbcAnyo.Text) <> "" And Trim(txtCod.Text) <> "" And Trim(sdbcGMN1_4Cod.Text) <> "" Then
            If Not g_oProceSeleccionado Is Nothing Then
                If Trim(sdbcAnyo.Text) <> g_oProceSeleccionado.Anyo Or Trim(txtCod.Text) <> g_oProceSeleccionado.Cod Or Trim(sdbcGMN1_4Cod.Text) <> g_oProceSeleccionado.GMN1Cod Then
                    bNuevoProce = True
                End If
            Else
                bNuevoProce = True
            End If
            
            If bNuevoProce = True Then
                Set g_oProceSeleccionado = Nothing
                Set g_oProceSeleccionado = oFSGSRaiz.Generar_CProceso
                g_oProceSeleccionado.Anyo = Trim(sdbcAnyo.Text)
                g_oProceSeleccionado.GMN1Cod = Trim(sdbcGMN1_4Cod.Text)
                g_oProceSeleccionado.Cod = Trim(Trim(txtCod.Text))
                If Not oUsuarioSummit.EsAdmin Then
                    If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
                    
                    Set oProces = oFSGSRaiz.generar_CProcesos
                    oProces.CargarTodosLosProcesosDesde 1, 1, 20, TipoOrdenacionProcesos.OrdPorCod, g_oProceSeleccionado.Anyo, g_oProceSeleccionado.GMN1Cod, , , , g_oProceSeleccionado.Cod, , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, bRUsuUON, bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , bRPerfUON, lIdPerfil
                    If oProces.Count = 0 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.PermisoDenegadoProceso
                        sdbcProveCod.Text = ""
                        Set g_oProceSeleccionado = Nothing
                        Set oProces = Nothing
                        Exit Sub
                    End If
                    Set oProces = Nothing
                End If
            End If
        Else
            Set g_oProceSeleccionado = Nothing
        End If
        Set frmPROVEBuscar.g_oProcesoSeleccionado = g_oProceSeleccionado
    End If
    If sOrigen = "frmSELPROVE" Then
        frmPROVEBuscar.bRMat = False
        frmPROVEBuscar.bREqp = False
    Else
        frmPROVEBuscar.bRMat = bRestProvMatComp
        frmPROVEBuscar.bREqp = bRestProvEquComp
    End If
    
    frmPROVEBuscar.sOrigen = "frmPROCEBuscar"
    frmPROVEBuscar.Show 1

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdSelProveBuscar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdSelResp_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmPROCEBusqPer.sOrigen = ""
    frmPROCEBusqPer.giTipo = 2
    frmPROCEBusqPer.bRUO = bRUsuUON
    frmPROCEBusqPer.bRPerfUO = bRPerfUON
    frmPROCEBusqPer.bRDep = bRUsuDep
    frmPROCEBusqPer.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdSelResp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdSelUsuAper_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmPROCEBusqPer.sOrigen = ""
    frmPROCEBusqPer.giTipo = 1
    frmPROCEBusqPer.bRUO = bRUsuUON
    frmPROCEBusqPer.bRPerfUO = bRPerfUON
    frmPROCEBusqPer.bRDep = bRUsuDep
    frmPROCEBusqPer.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "cmdSelUsuAper_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
    Select Case sOrigen
        
        Case "frmPROCE"
                
            sdbcEst.Enabled = True
            DesdeEst = sinitems
            HastaEst = Cerrado

        Case "frmSELPROVE"
            
            DesdeEst = validado
            HastaEst = Cerrado
            sdbcEst.Enabled = True
        
        Case "frmOFEPet"
            
            DesdeEst = Conproveasignados
            HastaEst = Cerrado
            sdbcEst.Enabled = True
        
        Case "frmOFERec"
            
            DesdeEst = conasignacionvalida
            HastaEst = Cerrado
            sdbcEst.Enabled = True
            
        Case "frmOFEBuzon", "frmLstOFEBuzon"
            DesdeEst = conasignacionvalida
            HastaEst = Cerrado
            sdbcEst.Enabled = True

        Case "frmOfeComp"
            
            DesdeEst = conofertas
            HastaEst = Cerrado
            sdbcEst.Enabled = True
            If frmADJ.g_bSoloAdjDir Then
                chkAdjDir.Value = vbChecked
                chkAdjReu.Value = vbUnchecked
                chkAdjDir.Visible = False
                chkAdjReu.Visible = False
            ElseIf frmADJ.g_bSoloAdjReu Then
                chkAdjReu.Value = vbChecked
                chkAdjDir.Value = vbUnchecked
                chkAdjReu.Visible = False
                chkAdjDir.Visible = False
            End If
                    
        Case "frmRESREU"
            
            chkAdjReu.Value = vbChecked
            chkAdjReu.Enabled = False
            chkAdjDir.Value = vbUnchecked
            chkAdjDir.Enabled = False
        
        Case "frmREU"
            
            chkAdjReu.Value = vbChecked
            chkAdjReu.Enabled = False
            chkAdjDir.Value = vbUnchecked
            chkAdjDir.Enabled = False
            sdbgProce.SelectTypeRow = ssSelectionTypeMultiSelectRange
            DesdeEst = validado
            HastaEst = PreadjYConObjNotificados
            
        Case "frmOFEHistWeb"
            
            DesdeEst = conofertas
            HastaEst = Cerrado
            sdbcEst.Enabled = True
        
        Case "frmPEDIDOS"
        
            sdbcEst.Enabled = False

        Case "frmCATALAnyaAdj", "frmCONTRATOS", "frmLstCatalogo", "frmLstPedidos", "frmSeguimiento"
            DesdeEst = ParcialmenteCerrado
            HastaEst = ConAdjudicacionesNotificadas
            sdbcEst.Enabled = True
            
        Case "frmSOLEnviarAProc"
            DesdeEst = sinitems
            HastaEst = PreadjYConObjNotificados
            sdbcEst.Enabled = True
        
        Case Else
            
            chkAdjReu.Enabled = True
            chkAdjDir.Enabled = True
            
    End Select
 
    sdbgProce.Columns(1).Locked = True
    sdbgProce.Columns(2).Locked = True
    sdbgProce.Columns(3).Locked = True
    sdbgProce.Columns(4).Locked = True
    sdbgProce.Columns(5).Locked = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Set oProceEncontrados = oFSGSRaiz.generar_CProcesos

    Set oMonedas = oFSGSRaiz.Generar_CMonedas

    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
        
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2 + 425
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
       
    PonerFieldSeparator Me
    
    If sOrigen = "frmPEDIDOS" Then
        fraPedidos.Visible = True
        
        cmdAceptar.Top = 7380
        cmdCancelar.Top = 7380
    Else
        fraPedidos.Visible = False
        fraMat.Top = 4005
        fraArt.Top = fraMat.Top + fraMat.Height + 30
        fraProve.Top = fraArt.Top + fraArt.Height + 30
        cmdAceptar.Top = 7380
        cmdCancelar.Top = 7380
    End If

    If gParametrosGenerales.gbSubasta Then
        chkSubasta.Visible = True
        
    ElseIf Not gParametrosGenerales.gbSubasta Then
        chkSubasta.Visible = False
        chkSubasta.Value = vbUnchecked
    End If
    
    'Comprueba si se visualizar�n o no los presupuestos
    If Not gParametrosGenerales.gbUsarPres1 And Not gParametrosGenerales.gbUsarPres2 And Not gParametrosGenerales.gbUsarPres3 And Not gParametrosGenerales.gbUsarPres4 Then
        fraPresupuestos.Visible = False
        fraUsuAper.Top = fraPresupuestos.Top
        fraResp.Top = fraUsuAper.Top + fraUsuAper.Height + 75
        Frame3.Top = fraResp.Top + fraResp.Height + 75
    Else
    Dim dblfraPesH As Double
        dblfraPesH = fraPresupuestos.Height / 4
        fraPresupuestos.Visible = True
        If Not gParametrosGenerales.gbUsarPres1 Then
            lblPresup(0).Visible = False
            lblPresup1.Visible = False
            cmdBorrarPresup1.Visible = False
            cmdSelPresup1.Visible = False
            lblPresup(3).Top = lblPresup(2).Top
            lblPresup4.Top = lblPresup3.Top
            cmdBorrarPresup4.Top = cmdBorrarPresup3.Top
            cmdSelPresup4.Top = cmdSelPresup3.Top
            lblPresup(2).Top = lblPresup(1).Top
            lblPresup3.Top = lblPresup2.Top
            cmdBorrarPresup3.Top = cmdBorrarPresup2.Top
            cmdSelPresup3.Top = cmdSelPresup2.Top
            lblPresup(1).Top = lblPresup(0).Top
            lblPresup2.Top = lblPresup1.Top
            cmdBorrarPresup2.Top = cmdBorrarPresup1.Top
            cmdSelPresup2.Top = cmdSelPresup1.Top
            fraPresupuestos.Height = fraPresupuestos.Height - dblfraPesH
        End If
        If Not gParametrosGenerales.gbUsarPres2 Then
            lblPresup(1).Visible = False
            lblPresup2.Visible = False
            cmdBorrarPresup2.Visible = False
            cmdSelPresup2.Visible = False
            lblPresup(3).Top = lblPresup(2).Top
            lblPresup4.Top = lblPresup3.Top
            cmdBorrarPresup4.Top = cmdBorrarPresup3.Top
            cmdSelPresup4.Top = cmdSelPresup3.Top
            lblPresup(2).Top = lblPresup(1).Top
            lblPresup3.Top = lblPresup2.Top
            cmdBorrarPresup3.Top = cmdBorrarPresup2.Top
            cmdSelPresup3.Top = cmdSelPresup2.Top
            fraPresupuestos.Height = fraPresupuestos.Height - dblfraPesH
        End If
        If Not gParametrosGenerales.gbUsarPres3 Then
            lblPresup(2).Visible = False
            lblPresup3.Visible = False
            cmdBorrarPresup3.Visible = False
            cmdSelPresup3.Visible = False
            lblPresup(3).Top = lblPresup(2).Top
            lblPresup4.Top = lblPresup3.Top
            cmdBorrarPresup4.Top = cmdBorrarPresup3.Top
            cmdSelPresup4.Top = cmdSelPresup3.Top
            fraPresupuestos.Height = fraPresupuestos.Height - dblfraPesH
        End If
        If Not gParametrosGenerales.gbUsarPres4 Then
            lblPresup(2).Visible = False
            lblPresup3.Visible = False
            cmdBorrarPresup3.Visible = False
            cmdSelPresup3.Visible = False
            fraPresupuestos.Height = fraPresupuestos.Height - dblfraPesH
        End If
        If Not gParametrosGenerales.gbUsarPres1 Or Not gParametrosGenerales.gbUsarPres2 Or Not gParametrosGenerales.gbUsarPres3 Or Not gParametrosGenerales.gbUsarPres4 Then
            fraPresupuestos.Height = fraPresupuestos.Height + 75
            fraUsuAper.Top = fraPresupuestos.Top + fraPresupuestos.Height + 75
            fraResp.Top = fraUsuAper.Top + fraUsuAper.Height + 75
            Frame3.Top = fraResp.Top + fraResp.Height + 75
        End If
    End If
    
    'Comprueba si se visualizar�n o no las solicitudes de compras
    If FSEPConf Then
        lblSolicCompras.Visible = False
        lblSolicitud.Visible = False
        cmdBorrarSolic.Visible = False
        cmdBuscarSolic.Visible = False
    Else
        If basParametros.gParametrosGenerales.gbSolicitudesCompras = False Then
            lblSolicCompras.Visible = False
            lblSolicitud.Visible = False
            cmdBorrarSolic.Visible = False
            cmdBuscarSolic.Visible = False
        End If
    End If
    'Si no es el usuario administrador
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        'Si no tiene permisos de consulta de las solicitudes no podr� visualizarlas
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APESolicAsignar)) Is Nothing) Then
            lblSolicCompras.Visible = False
            lblSolicitud.Visible = False
            cmdBorrarSolic.Visible = False
            cmdBuscarSolic.Visible = False
        End If
    End If
   
   If bRAsig Or bRCompResponsable Then
        sdbcEqpDen.AddItem oUsuarioSummit.comprador.DenEqp & Chr(m_lSeparador) & basOptimizacion.gCodEqpUsuario
        
        Set oEqps = oFSGSRaiz.Generar_CEquipos
        
        oEqps.Add basOptimizacion.gCodEqpUsuario, oUsuarioSummit.comprador.DenEqp
        Set oEqpSeleccionado = oEqps.Item(1)
        
        Set oEqpSeleccionado.Compradores = oFSGSRaiz.generar_CCompradores
        oEqpSeleccionado.Compradores.Add oEqpSeleccionado.Cod, oUsuarioSummit.comprador.DenEqp, basOptimizacion.gCodCompradorUsuario, oUsuarioSummit.comprador.nombre, oUsuarioSummit.comprador.Apel, "", "", ""
        
        Set oComps = oEqpSeleccionado.Compradores
        Set oCompSeleccionado = oComps.Item(1)
        sdbcCompDen.AddItem oUsuarioSummit.comprador.Apel & ", " & oUsuarioSummit.comprador.nombre & Chr(m_lSeparador) & basOptimizacion.gCodCompradorUsuario
    End If

        
    lblReferencia.caption = gParametrosGenerales.gsDenSolicitudCompra & ":"
    
    Select Case sOrigen
    
        Case "frmREU"
            
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(2) & Chr(m_lSeparador) & 2
            sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 3
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
        
        Case "frmSELPROVE"
        
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(2) & Chr(m_lSeparador) & 2
            sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 3
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
        
        Case "frmOFEPet"
             
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 3
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
            
        Case "frmOFERec"
            
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
            
        Case "frmOFEBuzon", "frmLstOFEBuzon"
                   
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
    
        Case "frmOfeComp"
            
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
                        
        Case "frmOFEHistWeb"
                   
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
            
        Case "frmCATALAnyaAdj", "frmCONTRATOS"
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
        
        Case "frmLstCatalogo", "frmLstPedidos", "frmSeguimiento"
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
        
        Case "frmSOLEnviarAProc"
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(1) & Chr(m_lSeparador) & 1
            sdbcEst.AddItem sIdiEst(2) & Chr(m_lSeparador) & 2
            sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 3
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
             
        Case Else
            
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(1) & Chr(m_lSeparador) & 1
            sdbcEst.AddItem sIdiEst(2) & Chr(m_lSeparador) & 2
            sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 3
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
            sdbcEst.AddItem sIdiEst(10) & Chr(m_lSeparador) & 10
    
    End Select
    
    g_bSoloInvitado = False
    If oUsuarioSummit.EsInvitado Then
        If oUsuarioSummit.Acciones Is Nothing Then
            g_bSoloInvitado = True
        Else
            If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEConsultar)) Is Nothing Then
                g_bSoloInvitado = True
            End If
        End If
    End If
    
    bPermProcMultiMaterial = False
    If gParametrosGenerales.gbMultiMaterial Then
        If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEPermProcMultimaterial)) Is Nothing) Then
                bPermProcMultiMaterial = True
            Else
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEPermProcMultimaterial)) Is Nothing) Then
                    bPermProcMultiMaterial = True
                End If
            End If
        Else
            bPermProcMultiMaterial = True
        End If
    Else
        bPermProcMultiMaterial = True
    End If
    
    If Not bPermProcMultiMaterial Then
        sdbcGMN1_4Cod.Visible = False
        lblGMN1_4.Visible = False
        sdbcGMN1_4Den.Visible = False
        lblGMN2_4.Top = lblGMN1_4.Top
        sdbcGMN2_4Cod.Top = sdbcGMN1_4Cod.Top
        sdbcGMN2_4Den.Top = sdbcGMN2_4Cod.Top
        lblGMN3_4.Top = lblGMN2_4.Top + lblGMN2_4.Height + 75
        sdbcGMN3_4Cod.Top = sdbcGMN2_4Cod.Top + sdbcGMN2_4Cod.Height + 50
        sdbcGMN3_4Den.Top = sdbcGMN3_4Cod.Top
        lblGMN4_4.Top = lblGMN3_4.Top + lblGMN3_4.Height + 75
        sdbcGMN4_4Cod.Top = sdbcGMN3_4Cod.Top + sdbcGMN3_4Cod.Height + 50
        sdbcGMN4_4Den.Top = sdbcGMN4_4Cod.Top
        fraMat.Height = 1375
        fraArt.Top = fraMat.Top + fraMat.Height + 50
        fraProve.Top = fraArt.Top + fraArt.Height + 50
        sstabFiltro.Height = 7550
        sstabProce.Height = 7920
        Me.Height = 8500
    Else
        sdbcGMN1_4Cod.Visible = True
        sdbcGMN1_4Den.Visible = True
        lblGMN1_4.Visible = True
        sdbcGMN2_4Cod.Top = sdbcGMN1_4Cod.Top + sdbcGMN1_4Cod.Height + 50
        sdbcGMN2_4Den.Top = sdbcGMN2_4Cod.Top
        lblGMN3_4.Top = lblGMN2_4.Top + lblGMN2_4.Height + 50
        sdbcGMN3_4Cod.Top = sdbcGMN2_4Cod.Top + sdbcGMN2_4Cod.Height + 50
        sdbcGMN3_4Den.Top = sdbcGMN3_4Cod.Top
        lblGMN4_4.Top = lblGMN3_4.Top + lblGMN3_4.Height + 50
        sdbcGMN4_4Cod.Top = sdbcGMN3_4Cod.Top + sdbcGMN3_4Cod.Height + 50
        sdbcGMN4_4Den.Top = sdbcGMN4_4Cod.Top
        fraMat.Height = 1710
        fraArt.Top = fraMat.Top + fraMat.Height + 50
        fraProve.Top = fraArt.Top + fraArt.Height + 50
    End If
    
    
    CargarAnyos
    
    sdbgProce.Columns("GMN1").caption = gParametrosGenerales.gsabr_GMN1
    lblGMN1Proce.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN1_4.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN2_4.caption = gParametrosGenerales.gsDEN_GMN2 & ":"
    lblGMN3_4.caption = gParametrosGenerales.gsDEN_GMN3 & ":"
    lblGMN4_4.caption = gParametrosGenerales.gsDEN_GMN4 & ":"
    
    Me.lblPresup(0).caption = gParametrosGenerales.gsPlurPres1
    Me.lblPresup(1).caption = gParametrosGenerales.gsPlurPres2
    Me.lblPresup(2).caption = gParametrosGenerales.gsPlurPres3
    Me.lblPresup(3).caption = gParametrosGenerales.gsPlurPres4
     'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador And bRMat Then
        CargarGMN1Automaticamente
    End If
    InicializarVariables
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub InicializarVariables()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_sCodPerAper = Null
m_sUON1Aper = Null
m_sUON2Aper = Null
m_sUON3Aper = Null
m_sDepAper = Null
m_sCodResp = Null
m_sUON1Resp = Null
m_sUON2Resp = Null
m_sUON3Resp = Null
m_sDepResp = Null

m_vPresup1_1 = Null
m_vPresup1_2 = Null
m_vPresup1_3 = Null
m_vPresup1_4 = Null
m_vPresup2_1 = Null
m_vPresup2_2 = Null
m_vPresup2_3 = Null
m_vPresup2_4 = Null
m_vPresup3_1 = Null
m_vPresup3_2 = Null
m_vPresup3_3 = Null
m_vPresup3_4 = Null
m_vPresup4_1 = Null
m_vPresup4_2 = Null
m_vPresup4_3 = Null
m_vPresup4_4 = Null
m_sUON1Pres1 = Null
m_sUON2Pres1 = Null
m_sUON3Pres1 = Null
m_sUON1Pres2 = Null
m_sUON2Pres2 = Null
m_sUON3Pres2 = Null
m_sUON1Pres3 = Null
m_sUON2Pres3 = Null
m_sUON3Pres3 = Null
m_sUON1Pres4 = Null
m_sUON2Pres4 = Null
m_sUON3Pres4 = Null
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "InicializarVariables", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
        Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, False

VaciarVables
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcCodArticulo_Change()
         
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboArt Then
    
        bRespetarComboArt = True
        txtDenArticulo.Text = ""
        bRespetarComboArt = False
        
        If sdbcCodArticulo.Value = "" Then
            bCargarComboDesde = False
        Else
            bCargarComboDesde = True
        End If
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcCodArticulo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcCodArticulo_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcCodArticulo.Value = "..." Then
        sdbcCodArticulo.Text = ""
        Exit Sub
    End If
    
    bRespetarComboArt = True
    sdbcCodArticulo.Text = sdbcCodArticulo.Columns(0).Text
    txtDenArticulo.Text = sdbcCodArticulo.Columns(1).Text
    bRespetarComboArt = False
    bCargarComboDesde = False
    
    Set oArticuloSeleccionado = oFSGSRaiz.Generar_CArticulo
    oArticuloSeleccionado.Cod = sdbcCodArticulo.Columns("COD").Value
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcCodArticulo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcCodArticulo_DropDown()

Dim sGMN1Cod As String
Dim sGMN2Cod As String
Dim sGMN3Cod As String
Dim sGMN4Cod As String
       
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcCodArticulo.RemoveAll
    sdbcCodArticulo.Columns(0).Value = ""
    sdbcCodArticulo.Columns(1).Value = ""
    
    Set oArticulos = Nothing
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
     
    If Not oGMN1Seleccionado Is Nothing Then
        sGMN1Cod = oGMN1Seleccionado.Cod
    Else
        sGMN1Cod = ""
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
    Else
        sGMN2Cod = ""
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
    Else
        sGMN3Cod = ""
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
    Else
        sGMN4Cod = ""
    End If
    
    If bRMat Then
        If oGMN4Seleccionado Is Nothing And sdbcCodArticulo.Text = "" Then Exit Sub
        oArticulos.DevolverArticulosDesde 32000, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, False, Trim(sdbcCodArticulo), , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
    Else
        If oGMN4Seleccionado Is Nothing Then
            oArticulos.DevolverArticulosDesde 32000, sGMN1Cod, sGMN2Cod, sGMN3Cod, , False, Trim(sdbcCodArticulo.Text)
        Else
            oGMN4Seleccionado.CargarTodosLosArticulos 32000, Trim(sdbcCodArticulo), , , , , , False
            Set oArticulos = oGMN4Seleccionado.ARTICULOS
        End If
    End If

    Dim oArt As CArticulo
    Set oArt = oFSGSRaiz.Generar_CArticulo
    For Each oArt In oArticulos
        sdbcCodArticulo.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den
    Next
    Set oArt = Nothing

    
    sdbcCodArticulo.SelStart = 0
    sdbcCodArticulo.SelLength = Len(sdbcCodArticulo.Text)
    sdbcCodArticulo.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcCodArticulo_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
      
End Sub

Private Sub sdbcCodArticulo_InitColumnProps()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcCodArticulo.DataFieldList = "Column 0"
    sdbcCodArticulo.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcCodArticulo_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcCodArticulo_Validate(Cancel As Boolean)
    
    Dim oArts As CArticulos
    Dim bExiste As Boolean
    Dim sGMN1Cod As String
    Dim sGMN2Cod As String
    Dim sGMN3Cod As String
    Dim sGMN4Cod As String

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcCodArticulo.Text = "" Then Exit Sub
        
    sdbcCodArticulo.RemoveAll
    Set oArts = Nothing
    Set oArts = oFSGSRaiz.Generar_CArticulos
           
    If Not oGMN1Seleccionado Is Nothing Then
        sGMN1Cod = oGMN1Seleccionado.Cod
    Else
        sGMN1Cod = ""
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
    Else
        sGMN2Cod = ""
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
    Else
        sGMN3Cod = ""
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
    Else
        sGMN4Cod = ""
    End If
        

    ''' Solo continuamos si existe el grupo
    ''' Solo continuamos si existe el grupo
    If bRMat Then
        oArts.DevolverArticulosDesde 32000, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, False, Trim(sdbcCodArticulo), , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , True
    Else
        If oGMN4Seleccionado Is Nothing Then
            oArts.DevolverArticulosDesde 32000, sGMN1Cod, sGMN2Cod, sGMN3Cod, , False, Trim(sdbcCodArticulo.Text), , , , , , , , , , , True
        Else
            oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcCodArticulo), , , , , , False
            Set oArts = oGMN4Seleccionado.ARTICULOS
        End If
    End If
    
    bExiste = Not (oArts.Count = 0)
    
    If Not bExiste Then
        oMensajes.NoValido 21
        sdbcCodArticulo.Text = ""
        txtDenArticulo = ""
    Else
        sdbcCodArticulo.Columns(0).Text = sdbcCodArticulo.Text
    End If
    
    Set oArts = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcCodArticulo_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub



Private Sub sdbcEst_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboEst Then
      
        bRespetarComboEst = False
        sdbcEst.Text = ""
        bRespetarComboEst = True
           
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcEst_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub



Private Sub sdbcEst_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcEst.Value = "..." Or Trim(sdbcEst.Value) = "" Then
        sdbcEst.Text = ""
        Exit Sub
    End If
    
    bRespetarComboEst = True
    sdbcEst.Text = sdbcEst.Columns(0).Text
    bRespetarComboEst = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcEst_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcEst_InitColumnProps()
  
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcEst.DataFieldList = "Column 0"
    sdbcEst.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcEst_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub



Private Sub sdbcEst_Validate(Cancel As Boolean)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcEst.Text <> "" Then
        sdbcEst.Text = sdbcEst.Columns(0).Text
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcEst_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN1_4Den_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN1RespetarCombo Then

        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1_4Den_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Den.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1_4Den_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN1_4Den_DropDown()
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass

    sdbcGMN1_4Den.RemoveAll

    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1

    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
    Else
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
    End If
    bExiste = Not (oGruposMN1.Count = 0)
    
    Codigos = oGruposMN1.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
 
    If Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If
    
    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Den.Value)
    sdbcGMN1_4Cod.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1_4Den_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1_4Den_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1Proce_Cod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN1Proce_Cod.Value = "" Then
        If Not bPermProcMultiMaterial Then
            sdbcGMN1_4Cod.Value = ""
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1Proce_Cod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1Proce_Cod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN1Proce_Cod.Value = "..." Then
        sdbcGMN1Proce_Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1Proce_Cod.Value = "" Then Exit Sub
    
    sdbcGMN1Proce_Cod.Text = sdbcGMN1Proce_Cod.Columns(0).Text
    If Not bPermProcMultiMaterial Then
        GMN1Seleccionado
        sdbcGMN1_4Cod.Value = sdbcGMN1Proce_Cod.Text
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1Proce_Cod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1Proce_Cod_DropDown()
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass

    sdbcGMN1Proce_Cod.RemoveAll

    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1

    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
    Else
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
    End If
    bExiste = Not (oGruposMN1.Count = 0)
    
    Codigos = oGruposMN1.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1Proce_Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
 
    If Not oGruposMN1.EOF Then
        sdbcGMN1Proce_Cod.AddItem "..."
    End If
    
    sdbcGMN1Proce_Cod.SelStart = 0
    sdbcGMN1Proce_Cod.SelLength = Len(sdbcGMN1Proce_Cod.Value)
    sdbcGMN1Proce_Cod.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1Proce_Cod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1Proce_Cod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1Proce_Cod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1Proce_Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1Proce_Cod, Text
End Sub

Public Sub sdbcGMN1Proce_Cod_Validate(Cancel As Boolean)
Dim oGMN1s As CGruposMatNivel1
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sdbcGMN1Proce_Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        If IsNumeric(sdbcAnyo.Value) Then
            Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        Else
            Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        End If
    Else
        oGMN1s.CargarTodosLosGruposMat sdbcGMN1Proce_Cod.Text, , True, , False, True
    End If
    
    Screen.MousePointer = vbNormal
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        sdbcGMN1Proce_Cod.Text = ""
        sdbcGMN1Proce_Cod.Text = ""
    Else
        sdbcGMN1Proce_Cod.Columns(0).Value = sdbcGMN1Proce_Cod.Text
        GMN1Seleccionado
    End If
    
    Set oGMN1s = Nothing
    Set oIMAsig = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1Proce_Cod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN2_4Cod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN2_4Cod.DataFieldList = "Column 0"
    sdbcGMN2_4Cod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN2_4Cod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub

Private Sub sdbcGMN2_4Den_Validate(Cancel As Boolean)
    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN2_4Den.Text = "" Then Exit Sub
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    ''' Solo continuamos si existe el grupo
    If Not oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, , Trim(sdbcGMN2_4Den.Text), True, , False, , , , True)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN2_4Den.Text, True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Den.Text = ""
    Else
        GMN2RespetarCombo = True
        
        sdbcGMN2_4Den.Columns(0).Value = sdbcGMN2_4Den.Text
        sdbcGMN2_4Den.Columns(1).Value = sdbcGMN2_4Cod.Text
                    
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN2_4Den_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN3_4Den_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, , Trim(sdbcGMN3_4Den), True, , False, , , , True)
        Else
            
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN3_4Den.Text, True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Den.Text = ""
   Else
        GMN3RespetarCombo = True
        
        sdbcGMN3_4Den.Columns(0).Value = sdbcGMN3_4Den.Text
        sdbcGMN3_4Den.Columns(1).Value = sdbcGMN3_4Cod.Text
                    
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4Den_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN4_4Den_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN4_4Den.Text = "" Then Exit Sub
     
     Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, , Trim(sdbcGMN4_4Den), , True, False)
        Else
           
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN4_4Den.Text, True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Den.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = oGMN4s.Item(1).Cod
        
        sdbcGMN4_4Den.Columns(0).Value = sdbcGMN4_4Den.Text
        sdbcGMN4_4Den.Columns(1).Value = sdbcGMN4_4Cod.Text
                    
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN4_4Den_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN3_4Cod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN3_4Cod.DataFieldList = "Column 0"
    sdbcGMN3_4Cod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4Cod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN4_4Cod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN4_4Cod.DataFieldList = "Column 0"
    sdbcGMN4_4Cod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN4_4Cod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcMonCod_Change()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not bRespetarCombo Then
        
        bRespetarCombo = False
        sdbcMonDen.Text = ""
        bRespetarCombo = True
        
        bCargarComboDesde = True
       
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcMonCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcMonCod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcMonCod.Value = "..." Or Trim(sdbcMonCod.Value) = "" Then
        sdbcMonCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcMonDen.Text = sdbcMonCod.Columns(1).Text
    sdbcMonCod.Text = sdbcMonCod.Columns(0).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcMonCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcMonCod_DropDown()

Dim Codigos As TipoDatosCombo
Dim i As Integer
Dim sDesde As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bCargarComboDesde Then
        sDesde = sdbcMonCod.Value
    Else
        sDesde = ""
    End If
    
    oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, sDesde, , , True
        
    Codigos = oMonedas.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcMonCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcMonCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcMonCod.DataFieldList = "Column 0"
    sdbcMonCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcMonCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcMonCod_PositionList(ByVal Text As String)
PositionList sdbcMonCod, Text
End Sub

Private Sub sdbcMonCod_Validate(Cancel As Boolean)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Trim(sdbcMonCod.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcMonCod.Value)) = UCase(Trim(sdbcMonCod.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcMonDen = sdbcMonCod.Columns(1).Value
        bRespetarCombo = False
       
        Exit Sub
    End If
    
    oMonedas.CargarTodasLasMonedasDesde 1, sdbcMonCod.Text, , , True, , True
    
    If oMonedas.Item(1) Is Nothing Then
        oMensajes.NoValida sIdiMoneda
        sdbcMonCod = ""
        Exit Sub
    Else
        If UCase(oMonedas.Item(1).Cod) <> UCase(sdbcMonCod.Value) Then
            oMensajes.NoValida sIdiMoneda
            sdbcMonCod = ""
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcMonDen = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            sdbcMonCod.Columns(0).Value = sdbcMonCod.Text
            sdbcMonCod.Columns(1).Value = sdbcMonDen.Text
            bRespetarCombo = False
           
        End If
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcMonCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonden_Change()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcMonCod.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
       
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcMonden_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcMonDen_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcMonDen.Value = "..." Or Trim(sdbcMonDen.Value) = "" Then
        sdbcMonDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcMonDen.Text = sdbcMonDen.Columns(0).Text
    sdbcMonCod.Text = sdbcMonDen.Columns(1).Text
    bRespetarCombo = False
   
    bCargarComboDesde = False
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcMonDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcMonDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
Dim sDesde As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If bCargarComboDesde Then
    sDesde = sdbcMonDen.Value
Else
    sDesde = ""
End If

    
    oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, , sDesde, True, True
        
    Codigos = oMonedas.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcMonDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcMonDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcMonDen.DataFieldList = "Column 0"
    sdbcMonDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcMonDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcMonDen_PositionList(ByVal Text As String)
PositionList sdbcMonDen, Text
End Sub

Private Sub sdbcMonDen_Validate(Cancel As Boolean)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Trim(sdbcMonDen.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcMonDen.Value)) = UCase(Trim(sdbcMonDen.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcMonCod.Text = sdbcMonDen.Columns(1).Value
        bRespetarCombo = False
       
        Exit Sub
    End If

    oMonedas.CargarTodasLasMonedasDesde 1, , sdbcMonDen.Text, True, True, , True
    
    If oMonedas.Item(1) Is Nothing Then
        oMensajes.NoValida sIdiMoneda
        sdbcMonDen.Text = ""
        Exit Sub
    Else
        If UCase(oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) <> UCase(sdbcMonDen.Value) Then
            oMensajes.NoValida sIdiMoneda
            sdbcMonDen.Text = ""
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcMonCod.Text = oMonedas.Item(1).Cod
            sdbcMonDen.Columns(0).Value = sdbcMonDen.Text
            sdbcMonDen.Columns(1).Value = sdbcMonCod.Text
            bRespetarCombo = False
           
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcMonDen_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN1_4Cod_Change()
           
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oArticuloSeleccionado = Nothing
    If bPermProcMultiMaterial Then
        If Not GMN1RespetarCombo Then
            GMN1RespetarCombo = True
            sdbcGMN2_4Cod.Text = ""
            sdbcGMN1_4Den.Text = ""
            GMN1RespetarCombo = False
            Set oGMN1Seleccionado = Nothing
            GMN1CargarComboDesde = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1_4Cod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1_4Cod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
      
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN1_4Cod.RemoveAll
        
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
 
    Screen.MousePointer = vbHourglass
    
    If GMN1CargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN1 = Nothing
            If IsNumeric(sdbcAnyo.Value) Then
                Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
            Else
                Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
            End If
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
    
            Set oGruposMN1 = Nothing
            If IsNumeric(sdbcAnyo.Value) Then
                Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
            Else
                Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
            End If
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1_4Cod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1_4Cod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub
'---------------------------------------------------------------------------------------
' Procedure : sdbcGMN1_4Cod_Validate
' Author    : gfa
' Date      : 18/03/2015
' Purpose   :
'---------------------------------------------------------------------------------------
'
Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)
Dim oGMN1s As CGruposMatNivel1
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        If IsNumeric(sdbcAnyo.Value) Then
            Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        Else
            Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        End If
    Else
        oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4Cod.Text, , True, , False, True
    End If
    
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        GMN1RespetarCombo = False
        GMN1Seleccionado
        GMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing
    Set oIMAsig = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN1_4Cod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub
Private Sub sdbcGMN2_4Cod_Change()
       
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN2RespetarCombo Then
    
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        
        GMN2CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN2_4Cod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Public Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)
Dim oGMN2s As CGruposMatNivel2
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    
    If Not oGMN1Seleccionado Is Nothing Then
        
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4Cod.Text, , True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = oGMN2s.Item(1).Den
        sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
        sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    Set oIMAsig = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN2_4Cod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Cod.Value = "" Then Exit Sub
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN2_4Cod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN2_4Cod.RemoveAll

    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN2 = Nothing
        
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
           
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4Cod, , , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        End If
    Else
       
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh

    Set oIMAsig = Nothing
    Set oGruposMN2 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN2_4Cod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN2RespetarCombo Then

        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        GMN2CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN2_4Den_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Den.Value = "" Then Exit Sub
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN2_4Den_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN2_4Den.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass

    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN2 = Nothing
        
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(Trim(sdbcGMN1_4Cod), , , , False, , , , , True)
        End If
    
    Else
        
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
            
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh
    
    Set oIMAsig = Nothing
    Set oGruposMN2 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN2_4Den_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub
Private Sub sdbcGMN2_4Den_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcGMN2_4Den.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN2_4Den_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN2_4Den_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN3_4Cod_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN3RespetarCombo Then
    
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4Cod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Cod_CloseUp()
  
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Cod.Value = "" Then Exit Sub
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4Cod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN3_4Cod.RemoveAll

    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN3 = Nothing
        
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4Cod, sdbcGMN2_4Cod, , , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        End If
    Else
       
        If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Set oIMAsig = Nothing
    Set oGruposMN3 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4Cod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub
Private Sub sdbcGMN3_4Cod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcGMN3_4Cod.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4Cod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub

Public Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)
Dim oGMN3s As CGruposMatNivel3
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
        
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4Cod.Text, , True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Cod.Text = ""
    Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = oGMN3s.Item(1).Den
        sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
        sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing
    Set oIMAsig = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4Cod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN3_4den_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN3RespetarCombo Then

        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4den_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Den_Click()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcGMN3_4Den.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4Den_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
  
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Den.Value = "" Then Exit Sub
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4Den_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN3_4Den.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN3 = Nothing
        
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        End If
    
    Else
             
        If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    Screen.MousePointer = vbNormal

    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    
    Set oIMAsig = Nothing
    Set oGruposMN3 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4Den_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN3_4Den_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub


Private Sub sdbcGMN4_4Cod_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN4RespetarCombo Then
    
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        sdbcCodArticulo.RemoveAll
        sdbcCodArticulo.Text = ""
        txtDenArticulo.Text = ""
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN4_4Cod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcGMN4_4Cod_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Cod.Value = "" Then Exit Sub
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN4_4Cod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN4_4Cod.RemoveAll

    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN4 = Nothing
        
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        End If
    
    Else
       
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN4_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    
    Set oIMAsig = Nothing
    Set oGruposMN4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN4_4Cod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN4_4Cod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcGMN4_4Cod.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN4_4Cod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub

Public Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)
Dim oGMN4s As CGruposMatNivel4
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4Cod.Text, , True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Cod.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = oGMN4s.Item(1).Den
        sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text
        sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing
    Set oIMAsig = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN4_4Cod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Private Sub sdbcGMN4_4Den_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN4RespetarCombo Then

        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN4_4Den_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Den.Value = "" Then Exit Sub
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN4_4Den_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN4_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN4_4Den.RemoveAll
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN4 = Nothing
        
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        End If
    Else
             
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Set oIMAsig = Nothing
    Set oGruposMN4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN4_4Den_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN4_4Den_InitColumnProps()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcGMN4_4Den_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub

Private Sub sdbgItems_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgItems.Columns(5).Value = True Then
        For i = 0 To 5
            sdbgItems.Columns(i).CellStyleSet "CERRADO"
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbgItems_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbgProce_BtnClick()
Dim oproce As CProceso
Dim sCod As String
Dim oIasig As IAsignaciones
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = sdbgProce.Columns("GMN1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbgProce.Columns("GMN1").Value))
sCod = sdbgProce.Columns("ANYO").Value & sCod & sdbgProce.Columns("COD").Text
       
   Set oproce = oProceEncontrados.Item(sCod)
   
   If oproce Is Nothing Then Exit Sub
   
   Set oIasig = oproce
   oIasig.DevolverResponsable
   
    If Not oproce.responsable Is Nothing Then
        frmESTRCOMPDetalleCom.caption = sIdiDetalle
        frmESTRCOMPDetalleCom.txtCod = oproce.responsable.Cod
        frmESTRCOMPDetalleCom.txtNom = oproce.responsable.nombre
        frmESTRCOMPDetalleCom.txtApel = oproce.responsable.Apel
        frmESTRCOMPDetalleCom.txtTfno = oproce.responsable.Tfno
        frmESTRCOMPDetalleCom.txtMail = oproce.responsable.mail
        frmESTRCOMPDetalleCom.txtFax = oproce.responsable.Fax
        frmESTRCOMPDetalleCom.txtCargo = oproce.responsable.Cargo
        frmESTRCOMPDetalleCom.txtTfno2 = oproce.responsable.Tfno2
        frmESTRCOMPDetalleCom.UON1 = oproce.responsable.UON1
        frmESTRCOMPDetalleCom.UON2 = oproce.responsable.UON2
        frmESTRCOMPDetalleCom.UON3 = oproce.responsable.UON3
        frmESTRCOMPDetalleCom.Dep = oproce.responsable.Dep
        frmESTRCOMPDetalleCom.sOrigen = "frmPROCEBuscar"
        
        frmESTRCOMPDetalleCom.Show 1
    Else
        oMensajes.FaltanDatos sIdiResponsable

    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbgProce_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbgProce_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim Adores As Ador.Recordset
Dim fldCod As Ador.Field
Dim fldArtDen As Ador.Field
Dim fldDest As Ador.Field
Dim fldFecIni As Ador.Field
Dim fldFecFin As Ador.Field
Dim fldEst As Ador.Field

Dim sCod As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If txtDenArticulo <> "" Then
        sdbgItems.RemoveAll
      If sdbgProce.Rows = 0 Then
        Exit Sub
      Else
        LockWindowUpdate Me.hWnd
        sCod = Trim(sdbgProce.Columns("GMN1").Value) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbgProce.Columns("GMN1").Value))
        Set Adores = oProceEncontrados.Item(CStr(sdbgProce.Columns("ANYO").Value) & sCod & CStr(sdbgProce.Columns("COD").Value)).CargarItemsPorDenominacion("", txtDenArticulo, OrdItemPorDen, False)
        
        Set fldCod = Adores.Fields("ART")
        Set fldArtDen = Adores.Fields("DESCR")
        Set fldDest = Adores.Fields("DEST")
        Set fldFecIni = Adores.Fields("FECINI")
        Set fldFecFin = Adores.Fields("FECFIN")
        Set fldEst = Adores.Fields("EST")
        
        While Not Adores.EOF
            sdbgItems.AddItem fldCod & Chr(m_lSeparador) & fldArtDen & Chr(m_lSeparador) & fldDest & Chr(m_lSeparador) & fldFecIni & Chr(m_lSeparador) & fldFecFin & Chr(m_lSeparador) & fldEst
            Adores.MoveNext
            DoEvents
        Wend
        
        Adores.Close
        Set Adores = Nothing
        LockWindowUpdate &O0
      End If
   End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbgProce_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sstabProce_Click(PreviousTab As Integer)

    Static recorrido As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    recorrido = False
    
    If Not IsNumeric(txtCod) And txtCod <> "" Then
        sstabProce.Tab = 0
        If Me.Visible Then txtCod.SetFocus
        Exit Sub
    End If
    
        
    If txtCod.Text <> "" Then
        If txtCod.Text > giMaxProceCod Then
            sstabProce.Tab = 0
            Exit Sub
        End If
    End If
    
    If txtPresDesde = "" Then
        dPresDesde = 0
    Else
        If IsNumeric(txtPresDesde.Text) Then
            dPresDesde = CDbl(txtPresDesde.Text)
        Else
            sstabProce.Tab = 0
            If recorrido = False Then
                recorrido = True
                oMensajes.NoValido (oMensajes.CargarTexto(FRM_PROCE_BUSCAR, 44))
            End If
            Exit Sub
        End If
    End If
    
     If txtPresHasta = "" Then
        dPresHasta = 0
    Else
        If IsNumeric(txtPresHasta.Text) Then
            dPresHasta = CDbl(txtPresHasta.Text)
        Else
            sstabProce.Tab = 0
            If recorrido = False Then
                recorrido = True
                oMensajes.NoValido (oMensajes.CargarTexto(FRM_PROCE_BUSCAR, 44))
            End If
            Exit Sub
        End If
    End If
        
    sdbgItems.RemoveAll
    sdbgProce.RemoveAll
        
    If PreviousTab = 0 And sstabProce.Tab = 1 Then
        Cargar
        DoEvents
        sdbgProce_RowColChange 1, 4
    End If
    
    If txtDenArticulo.Text = "" Then
        sdbgItems.Visible = False
        sdbgProce.Height = 6840
    Else
        sdbgItems.Visible = True
        sdbgProce.Height = 3900
        sdbgItems.Height = 6840 - 4060
        sdbgItems.Top = 4585
        
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sstabProce_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Cargar()
Dim oProceso As CProceso
Dim sestado As String
Dim pedido As Variant
Dim udtBusquedaProc As TipoBusquedaProcesos

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcEst <> "" Then
            Select Case sdbcEst.Columns("COD").Value
        
                Case 1
                    'Pendiente de validar Apertura
                    DesdeEst = sinitems
                    HastaEst = ConItemsSinValidar
                
                Case 2
                    'Pendiente de asignar proveedores
                    DesdeEst = validado
                    HastaEst = Conproveasignados
                    
                Case 3
                    'Pendiente de enviar peticiones
                    DesdeEst = conasignacionvalida
                    HastaEst = conasignacionvalida
                    
                Case 4
                    ' en rec ofertas
                    If sOrigen = "frmOfeComp" Or sOrigen = "frmOFEHistWeb" Then
                        DesdeEst = conofertas
                        HastaEst = conofertas
                    Else
                        DesdeEst = conpeticiones
                        HastaEst = conofertas
                    End If
                                   
                Case 5
                    'pendiente de comunicar objetivos
                    DesdeEst = ConObjetivosSinNotificar
                    HastaEst = ConObjetivosSinNotificarYPreadjudicado
                    
                Case 6
                    'Pendiente de adjudicar
                    DesdeEst = ConObjetivosSinNotificarYPreadjudicado
                    HastaEst = PreadjYConObjNotificados
                Case 7
                    'Parcialmente cerrado
                    DesdeEst = ParcialmenteCerrado
                    HastaEst = ParcialmenteCerrado
                Case 8
                    'Pendiente de notificar adjudicaciones
                    DesdeEst = conadjudicaciones
                    HastaEst = conadjudicaciones
                Case 9
                    'Adjudicado y notificado
                    DesdeEst = ConAdjudicacionesNotificadas
                    HastaEst = ConAdjudicacionesNotificadas
                Case 10
                    'Anulado
                    DesdeEst = Cerrado
                    HastaEst = Cerrado
            
            End Select
    
    Else
        
        Select Case sOrigen
        
            Case "frmREU"
                
                DesdeEst = validado
                HastaEst = ParcialmenteCerrado
            
            Case "frmSELPROVE"
                        
                DesdeEst = validado
                HastaEst = ConAdjudicacionesNotificadas
                   
            Case "frmOFEPet"
            
                DesdeEst = conasignacionvalida
                HastaEst = ConAdjudicacionesNotificadas
                 
            Case "frmOFERec", "frmOFEBuzon", "frmLstOFEBuzon"
                
                DesdeEst = conasignacionvalida
                HastaEst = ConAdjudicacionesNotificadas
                
            Case "frmOfeComp", "frmOFEHistWeb"
                       
                DesdeEst = conofertas
                HastaEst = ConAdjudicacionesNotificadas
            
            Case "frmLstCatalogo", "frmSeguimiento", "frmLstPedidos"
    
                DesdeEst = ConObjetivosSinNotificarYPreadjudicado
                HastaEst = ConAdjudicacionesNotificadas
                
            Case "frmCATALAnyaAdj", "frmCONTRATOS"
            
                DesdeEst = ParcialmenteCerrado
                HastaEst = ConAdjudicacionesNotificadas
                      
            Case "frmSOLEnviarAProc"
            
                DesdeEst = sinitems
                HastaEst = ParcialmenteCerrado
                
            Case Else
                
                DesdeEst = sinitems
                HastaEst = Cerrado
                            
        End Select
    
    End If
    
    If optSinPedios Then
        pedido = 0
    ElseIf optConPedidos Then
        pedido = 1
    ElseIf optExcedida Then
        pedido = 2
    Else
        pedido = Null
    End If

    With udtBusquedaProc
        .DesdeFechaApertura = txtFecApeDesde
        .HastaFechaApertura = txtFecApeHasta
        .DesdeFechaNecesidad = txtFecNeceDesde
        .HastaFechaNecesidad = txtFecNeceHasta
        .DesdeFechaPresentacion = txtFecPresDesde
        .HastaFechaPresentacion = txtFecPresHasta
        .DesdeFechaUltReu = txtFecUltReuDesde
        .HastaFechaUltReu = txtFecUltReuHasta
        
        .sGMN1_Proce = sdbcGMN1Proce_Cod.Value
        .GMN1 = sdbcGMN1_4Cod.Text
        .GMN2 = sdbcGMN2_4Cod.Text
        .GMN3 = sdbcGMN3_4Cod.Text
        .GMN4 = sdbcGMN4_4Cod.Text

        .Cod = val(txtCod)
        .CarIniDen = txtDen.Text
        .mon = sdbcMonCod.Text
        .UsarIndice = False
        .CoincidenciaTotal = IIf(txtCod <> "" And txtDen = "", True, False)
        .PresGlobalDesde = dPresDesde
        .PresGlobalHasta = dPresHasta
        .Eqp = basOptimizacion.gCodEqpUsuario
        .Comp = basOptimizacion.gCodPersonaUsuario
        .CodUsu = basOptimizacion.gvarCodUsuario
        .bRMat = bRMat
        .bRAsig = bRAsig
        .bRCompResponsable = bRCompResponsable
        .bREqpAsig = bREqpAsig
        .bRUsuAper = bRUsuAper
        .bRUsuUON = bRUsuUON
        .bRPerfUON = bRPerfUON
        .bRUsuDep = bRUsuDep
        .UON1 = basOptimizacion.gUON1Usuario
        .UON2 = basOptimizacion.gUON2Usuario
        .UON3 = basOptimizacion.gUON3Usuario
        .Dep = basOptimizacion.gCodDepUsuario
        .DeAdjDirecta = (chkAdjDir.Value = vbChecked)
        .DeAdjReu = (chkAdjReu.Value = vbChecked)
        .Referencia = txtReferencia.Text
        .CodArt = sdbcCodArticulo.Text
        .DenArt = "%" & txtDenArticulo.Text & "%"
        .bSoloModoSubasta = (chkSubasta.Value = vbChecked)
        .Solicit = lblIdSolic.caption
        .sCodGrupo = Me.txtCodGrupo.Text
        .sDenGrupo = Me.txtDenGrupo.Text
        .vPresup1_1 = m_vPresup1_1
        .vPresup1_2 = m_vPresup1_2
        .vPresup1_3 = m_vPresup1_3
        .vPresup1_4 = m_vPresup1_4
        .vPresup2_1 = m_vPresup2_1
        .vPresup2_2 = m_vPresup2_2
        .vPresup2_3 = m_vPresup2_3
        .vPresup2_4 = m_vPresup2_4
        .vPresup3_1 = m_vPresup3_1
        .vPresup3_2 = m_vPresup3_2
        .vPresup3_3 = m_vPresup3_3
        .vPresup3_4 = m_vPresup3_4
        .vPresup4_1 = m_vPresup4_1
        .vPresup4_2 = m_vPresup4_2
        .vPresup4_3 = m_vPresup4_3
        .vPresup4_4 = m_vPresup4_4
        .sUON1Pres1 = m_sUON1Pres1
        .sUON2Pres1 = m_sUON2Pres1
        .sUON3Pres1 = m_sUON3Pres1
        .sUON1Pres2 = m_sUON1Pres2
        .sUON2Pres2 = m_sUON2Pres2
        .sUON3Pres2 = m_sUON3Pres2
        .sUON1Pres3 = m_sUON1Pres3
        .sUON2Pres3 = m_sUON2Pres3
        .sUON3Pres3 = m_sUON3Pres3
        .sUON1Pres4 = m_sUON1Pres4
        .sUON2Pres4 = m_sUON2Pres4
        .sUON3Pres4 = m_sUON3Pres4
        .sCodResp = m_sCodResp
        .sCodPerAper = m_sCodPerAper
        .sUON1Aper = m_sUON1Aper
        .sUON2Aper = m_sUON2Aper
        .sUON3Aper = m_sUON3Aper
        .sUON1Resp = m_sUON1Resp
        .sUON2Resp = m_sUON2Resp
        .sUON3Resp = m_sUON3Resp
        .sDepAper = m_sDepAper
        .sDepResp = m_sDepResp
        .iPedido = pedido
        .sCodDest = Me.sdbcDestCodProce.Columns("COD").Value
        If Me.sdbcEqpDen.Text <> "" Then
            .sCodEqp = Me.sdbcEqpDen.Columns("COD").Value
        End If
        If Me.sdbcCompDen.Text <> "" Then
            .sCodComp = Me.sdbcCompDen.Columns("COD").Value
        End If
        
        .bCompAdj = Me.optComp.Value
        
        .scodProve = Me.sdbcProveCod.Text
        .sCodProveAdj = sProveAdj
        .lIdPerfil = oUsuarioSummit.perfil.Id
    End With


    If sOrigen = "frmPEDIDOS" Then
        oProceEncontrados.BuscarTodosLosProcesosPARAPEDIDO val(sdbcAnyo), udtBusquedaProc
    Else
        If sOrigen = "frmCATALAnyaAdj" Then
            oProceEncontrados.BuscarTodosLosProcesosPARAPEDIDO val(sdbcAnyo), udtBusquedaProc
        ElseIf sOrigen = "frmPROCE" Or sOrigen = "frmSELPROVE" Or sOrigen = "frmOFEPet" Or sOrigen = "frmOFERec" Or sOrigen = "frmOfeComp" Or sOrigen = "frmCONTRATOS" Then
            ' si entra por aqui es que es de un formulario al que puede acceder como invitado
            oProceEncontrados.BuscarTodosLosProcesos 32000, val(sdbcAnyo), DesdeEst, HastaEst, TipoOrdenacionProcesos.OrdPorAnyoMaterialCodigo, udtBusquedaProc, True, g_bSoloInvitado
        Else
            oProceEncontrados.BuscarTodosLosProcesos 32000, val(sdbcAnyo), DesdeEst, HastaEst, TipoOrdenacionProcesos.OrdPorAnyoMaterialCodigo, udtBusquedaProc
        End If
    End If

    For Each oProceso In oProceEncontrados
        Select Case oProceso.Estado
        
            Case TipoEstadoProceso.sinitems, TipoEstadoProceso.ConItemsSinValidar
                sestado = sIdiEst(1)
                    
            Case TipoEstadoProceso.validado, TipoEstadoProceso.Conproveasignados
                sestado = sIdiEst(2)
                    
            Case TipoEstadoProceso.conasignacionvalida
                sestado = sIdiEst(3)
            
            Case TipoEstadoProceso.conpeticiones, TipoEstadoProceso.conofertas
                sestado = sIdiEst(4)
                    
            Case TipoEstadoProceso.ConObjetivosSinNotificar, TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado
                sestado = sIdiEst(5)
            
            Case TipoEstadoProceso.PreadjYConObjNotificados
                sestado = sIdiEst(6)
            
            Case TipoEstadoProceso.ParcialmenteCerrado
                sestado = sIdiEst(7)
                
            Case TipoEstadoProceso.conadjudicaciones
                sestado = sIdiEst(8)
            
            Case TipoEstadoProceso.ConAdjudicacionesNotificadas
                sestado = sIdiEst(9)
            
            Case TipoEstadoProceso.Cerrado
                sestado = sIdiEst(10)
                
        End Select
        
        sdbgProce.AddItem oProceso.Cod & Chr(m_lSeparador) & oProceso.Anyo & Chr(m_lSeparador) & oProceso.GMN1Cod & Chr(m_lSeparador) & oProceso.Cod & Chr(m_lSeparador) & oProceso.Den & Chr(m_lSeparador) & sestado & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oProceso.Estado
    
    Next

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "Cargar", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Public Sub PonerMatSeleccionadoEnCombos()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Text = ""
        
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not oGMN2Seleccionado Is Nothing Then
'        bRespetarComboGMN2 = True
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
'        bRespetarComboGMN2 = False
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Text = ""
        
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    
    If Not oGMN3Seleccionado Is Nothing Then
'        bRespetarComboGMN3 = True
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
'        bRespetarComboGMN3 = False
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Text = ""
        
    End If
    
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN4Seleccionado Is Nothing Then
'        bRespetarComboGMN4 = True
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
'        bRespetarComboGMN4 = False
        sdbcGMN4_4Cod_Validate False
    Else
        sdbcGMN4_4Cod.Text = ""
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "PonerMatSeleccionadoEnCombos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


Private Sub txtCod_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsNumeric(txtCod) And txtCod <> "" Then
        oMensajes.NoValida "C�digo"
        Cancel = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub txtDenArticulo_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboArt Then
        sdbcCodArticulo.Text = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtDenArticulo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub txtFecApeDesde_Validate(Cancel As Boolean)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecApeDesde <> "" Then
        If Not IsDate(txtFecApeDesde) Then
            oMensajes.NoValida " " & sIdiFecha & " "
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtFecApeDesde_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecApeHasta_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecApeHasta <> "" Then
        If Not IsDate(txtFecApeHasta) Then
            oMensajes.NoValida " " & sIdiFecha & " "
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtFecApeHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecNeceDesde_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecNeceDesde <> "" Then
        If Not IsDate(txtFecNeceDesde) Then
            oMensajes.NoValida " " & sIdiFecha & " "
           Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtFecNeceDesde_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecNeceHasta_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecNeceHasta <> "" Then
        If Not IsDate(txtFecNeceHasta) Then
            oMensajes.NoValida " " & sIdiFecha & " "
           Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtFecNeceHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecPresDesde_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecPresDesde <> "" Then
        If Not IsDate(txtFecPresDesde) Then
            oMensajes.NoValida " " & sIdiFecha & " "
           Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtFecPresDesde_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecPresHasta_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecPresHasta <> "" Then
        If Not IsDate(txtFecPresHasta) Then
            oMensajes.NoValida " " & sIdiFecha & " "
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtFecPresHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecUltReuDesde_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If txtFecUltReuDesde <> "" Then
        If Not IsDate(txtFecUltReuDesde) Then
            oMensajes.NoValido sIdiFecha

            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtFecUltReuDesde_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecUltReuHasta_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecUltReuHasta <> "" Then
        If Not IsDate(txtFecUltReuHasta) Then
            oMensajes.NoValido sIdiFecha
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtFecUltReuHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtPresDesde_Validate(Cancel As Boolean)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtPresDesde <> "" Then
        If Not IsNumeric(txtPresDesde) Then
            oMensajes.NoValido sIdiPresupuesto
            Cancel = True
            If Me.Visible Then txtPresDesde.SetFocus
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtPresDesde_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub txtPresHasta_Validate(Cancel As Boolean)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtPresHasta <> "" Then
        If Not IsNumeric(txtPresHasta) Then
            oMensajes.NoValido sIdiPresupuesto
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "txtPresHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Private Sub GMN1Seleccionado()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
       
    sdbcGMN2_4Cod = ""
    sdbcGMN2_4Den = ""
    sdbcGMN3_4Cod = ""
    sdbcGMN3_4Den = ""
    sdbcGMN4_4Cod = ""
    sdbcGMN4_4Den = ""
    sdbcCodArticulo = ""
    txtDenArticulo = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "GMN1Seleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub GMN2Seleccionado()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
  
    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod
    If bPermProcMultiMaterial Then
        oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    Else
        oGMN2Seleccionado.GMN1Cod = sdbcGMN1Proce_Cod
    End If
    sdbcGMN3_4Cod = ""
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "GMN2Seleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If
   
End Sub
Private Sub GMN3Seleccionado()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN4Seleccionado = Nothing
    
    Set oGMN3Seleccionado = Nothing
    Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
  
    If bPermProcMultiMaterial Then
        oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    Else
        oGMN3Seleccionado.GMN1Cod = sdbcGMN1Proce_Cod
    End If
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod

    sdbcGMN4_4Cod = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "GMN3Seleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub GMN4Seleccionado()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN4Seleccionado = Nothing
    
    If sdbcGMN4_4Cod.Text = "" Or sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    
    Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    
    oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    If bPermProcMultiMaterial Then
        oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    Else
        oGMN4Seleccionado.GMN1Cod = sdbcGMN1Proce_Cod
    End If
    oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod
    
    sdbcCodArticulo = ""
    txtDenArticulo = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "GMN4Seleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
    sdbcAnyo.AllowInput = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "CargarAnyos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCE_BUSCAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    caption = Ador(0).Value
    Ador.MoveNext
    sstabProce.TabCaption(0) = Ador(0).Value
    Ador.MoveNext
    sstabProce.TabCaption(1) = Ador(0).Value
    Ador.MoveNext
    fraGenerales.caption = Ador(0).Value
    Ador.MoveNext
    sdbgProce.Columns("ANYO").caption = Ador(0).Value
    lblAnyo.caption = Ador(0).Value & ":"
    Ador.MoveNext
    lblCodArticulo.caption = Ador(0).Value
    Me.lblCodGr.caption = Ador(0).Value
    Ador.MoveNext
    Me.lblDenGr.caption = Ador(0).Value
    lblDenArticulo.caption = Ador(0).Value
    Ador.MoveNext
    lblEst.caption = Ador(0).Value
    Ador.MoveNext
    lblMon.caption = Ador(0).Value
    Ador.MoveNext
    lblPresDesde.caption = Ador(0).Value
    Ador.MoveNext
    lblPresHasta.caption = Ador(0).Value
    lblFecApeHasta.caption = Ador(0).Value
    lblFecNeceHasta.caption = Ador(0).Value
    lblFecPresHasta.caption = Ador(0).Value
    lblFecUltReuHasta.caption = Ador(0).Value
    Ador.MoveNext
    
    lblSolicitud.caption = Ador(0).Value
    Ador.MoveNext
    chkAdjDir.caption = Ador(0).Value
    Ador.MoveNext
    chkAdjReu.caption = Ador(0).Value
    Ador.MoveNext
    chkSubasta.caption = Ador(0).Value
    Ador.MoveNext
    
    
    fraMat.caption = Ador(0).Value
    Ador.MoveNext
    fraFechas.caption = Ador(0).Value
    Ador.MoveNext
    lblFecApeDesde.caption = Ador(0).Value
    Ador.MoveNext
    lblFecNeceDesde.caption = Ador(0).Value
    Ador.MoveNext
    lblFecPresDesde.caption = Ador(0).Value
    Ador.MoveNext
    lblFecUltReuDesde.caption = Ador(0).Value
    Ador.MoveNext
    
    sdbgProce.Groups(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbgProce.Columns("PROCESO").caption = Ador(0).Value
    Ador.MoveNext
    sdbgProce.Columns("DESCR").caption = Ador(0).Value
    Ador.MoveNext
    sdbgProce.Columns("EST").caption = Ador(0).Value
    Ador.MoveNext
    sdbgProce.Columns("RESP").caption = Ador(0).Value
    Ador.MoveNext

'C�d.
    sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
    sdbcGMN2_4Cod.Columns(0).caption = Ador(0).Value
    sdbcGMN3_4Cod.Columns(0).caption = Ador(0).Value
    sdbcGMN4_4Cod.Columns(0).caption = Ador(0).Value
    sdbcGMN2_4Den.Columns(1).caption = Ador(0).Value
    sdbcGMN3_4Den.Columns(1).caption = Ador(0).Value
    sdbcGMN4_4Den.Columns(1).caption = Ador(0).Value
    sdbcMonCod.Columns(0).caption = Ador(0).Value
    sdbcMonDen.Columns(1).caption = Ador(0).Value
    sdbcCodArticulo.Columns(0).caption = Ador(0).Value
    sdbgItems.Columns(0).caption = Ador(0).Value
    Me.sdbcDestCodProce.Columns("COD").caption = Ador(0).Value
    Me.sdbcDestDenProce.Columns("COD").caption = Ador(0).Value
    Me.sdbcProveCod.Columns("COD").caption = Ador(0).Value
    Me.sdbcProveDen.Columns("COD").caption = Ador(0).Value
    sdbcEqpDen.Columns("COD").caption = Ador(0).Value
    sdbcCompDen.Columns("COD").caption = Ador(0).Value

    Ador.MoveNext

'Denominaci�n
    sdbcEst.Columns(0).caption = Ador(0).Value
    sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
    sdbcGMN2_4Cod.Columns(1).caption = Ador(0).Value
    sdbcGMN3_4Cod.Columns(1).caption = Ador(0).Value
    sdbcGMN4_4Cod.Columns(1).caption = Ador(0).Value
    sdbcGMN2_4Den.Columns(0).caption = Ador(0).Value
    sdbcGMN3_4Den.Columns(0).caption = Ador(0).Value
    sdbcGMN4_4Den.Columns(0).caption = Ador(0).Value
    sdbcMonCod.Columns(1).caption = Ador(0).Value
    sdbcMonDen.Columns(0).caption = Ador(0).Value
    sdbcCodArticulo.Columns(1).caption = Ador(0).Value
    sdbgItems.Columns(1).caption = Ador(0).Value
    Me.sdbcDestCodProce.Columns("DEN").caption = Ador(0).Value
    Me.sdbcDestDenProce.Columns("DEN").caption = Ador(0).Value
    Me.sdbcProveCod.Columns("DEN").caption = Ador(0).Value
    Me.sdbcProveDen.Columns("DEN").caption = Ador(0).Value
    sdbcEqpDen.Columns("DEN").caption = Ador(0).Value
    sdbcCompDen.Columns("DEN").caption = Ador(0).Value

    Ador.MoveNext
    
    cmdAceptar.caption = Ador(0).Value
    Ador.MoveNext
    cmdCancelar.caption = Ador(0).Value
    Ador.MoveNext
    
    sIdiEst(1) = Ador(0).Value
    Ador.MoveNext
    sIdiEst(2) = Ador(0).Value
    Ador.MoveNext
    sIdiEst(3) = Ador(0).Value
    Ador.MoveNext
    sIdiEst(4) = Ador(0).Value
    Ador.MoveNext
    sIdiEst(5) = Ador(0).Value
    Ador.MoveNext
    sIdiEst(6) = Ador(0).Value
    Ador.MoveNext
    sIdiEst(7) = Ador(0).Value
    Ador.MoveNext
    sIdiEst(8) = Ador(0).Value
    Ador.MoveNext
    sIdiEst(9) = Ador(0).Value
    Ador.MoveNext
    sIdiEst(10) = Ador(0).Value
    Ador.MoveNext
    
    sIdiMoneda = Ador(0).Value
    Ador.MoveNext
    sIdiMaterial = Ador(0).Value
    Ador.MoveNext
    sIdiDetalle = Ador(0).Value
    Ador.MoveNext
    sIdiResponsable = Ador(0).Value
    Ador.MoveNext
    sIdiFecha = Ador(0).Value
    Ador.MoveNext
    sIdiPresupuesto = Ador(0).Value
    Ador.MoveNext
       
    fraArt.caption = Ador(0).Value
    Ador.MoveNext
    sdbgItems.Groups(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbgItems.Columns(2).caption = Ador(0).Value
    Me.lblDest.caption = Ador(0).Value & ":"
    Ador.MoveNext
    sdbgItems.Columns(3).caption = Ador(0).Value
    Ador.MoveNext
    sdbgItems.Columns(4).caption = Ador(0).Value
    Ador.MoveNext
    optSinPedios.caption = Ador(0).Value
    Ador.MoveNext
    optExcedida.caption = Ador(0).Value
    Ador.MoveNext
    optConPedidos.caption = Ador(0).Value
    Ador.MoveNext
    fraPedidos.caption = Ador(0).Value
    
    Ador.MoveNext
    sstabFiltro.TabCaption(0) = Ador(0).Value
    Ador.MoveNext
    sstabFiltro.TabCaption(1) = Ador(0).Value
    Ador.MoveNext
    Me.fraGrupo.caption = Ador(0).Value
    Ador.MoveNext
    Me.fraUsuAper.caption = Ador(0).Value
    Ador.MoveNext
    Me.fraResp.caption = Ador(0).Value
    Ador.MoveNext
    Me.fraPresupuestos.caption = Ador(0).Value
    Ador.MoveNext
    Ador.MoveNext
    Me.fraProve.caption = Ador(0).Value
    Ador.MoveNext
    Me.sdbcDestCodProce.Columns("DIR").caption = Ador(0).Value
    Me.sdbcDestDenProce.Columns("DIR").caption = Ador(0).Value
    Ador.MoveNext
    Me.sdbcDestCodProce.Columns("POB").caption = Ador(0).Value
    Me.sdbcDestDenProce.Columns("POB").caption = Ador(0).Value
    Ador.MoveNext
    Me.sdbcDestCodProce.Columns("CP").caption = Ador(0).Value
    Me.sdbcDestDenProce.Columns("CP").caption = Ador(0).Value
    Ador.MoveNext
    Me.sdbcDestCodProce.Columns("PAIS").caption = Ador(0).Value
    Me.sdbcDestDenProce.Columns("PAIS").caption = Ador(0).Value
    Ador.MoveNext
    Me.sdbcDestCodProce.Columns("PROVI").caption = Ador(0).Value
    Me.sdbcDestDenProce.Columns("PROVI").caption = Ador(0).Value
    Ador.MoveNext
    Frame3.caption = Ador(0).Value
    Ador.MoveNext
    optComp.caption = Ador(0).Value
    Ador.MoveNext
    optResp.caption = Ador(0).Value
    Ador.MoveNext
    Label6.caption = Ador(0).Value
    Ador.MoveNext
    Label7.caption = Ador(0).Value
    Ador.MoveNext
    lblCod.caption = Ador(0).Value
    Ador.MoveNext
    lblDen.caption = Ador(0).Value
   
   Ador.Close
    
    End If

    Set Ador = Nothing


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


Public Sub PonerSolicitudSeleccionada(ByVal oSolic As CInstancia)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblSolicCompras.caption = oSolic.Id & " " & oSolic.DescrBreve
    lblIdSolic.caption = oSolic.Id
    
    Set oSolic = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "PonerSolicitudSeleccionada", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub MostrarPresSeleccionado12(ByVal iTipo As Integer)
Dim sLblUO As String
Dim sPresup1 As String
Dim sPresup2 As String
Dim sPresup3 As String
Dim sPresup4 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If iTipo = 1 Then
        m_vPresup1_1 = Null
        m_vPresup1_2 = Null
        m_vPresup1_3 = Null
        m_vPresup1_4 = Null
    Else
        m_vPresup2_1 = Null
        m_vPresup2_2 = Null
        m_vPresup2_3 = Null
        m_vPresup2_4 = Null
    End If
    
    If iTipo = 1 Then
        m_sUON1Pres1 = frmSELPresAnuUON.g_sUON1
        m_sUON2Pres1 = frmSELPresAnuUON.g_sUON2
        m_sUON3Pres1 = frmSELPresAnuUON.g_sUON3
    Else
        m_sUON1Pres2 = frmSELPresAnuUON.g_sUON1
        m_sUON2Pres2 = frmSELPresAnuUON.g_sUON2
        m_sUON3Pres2 = frmSELPresAnuUON.g_sUON3
    End If
    
    
    sLblUO = ""
    If iTipo = 1 Then
        If m_sUON1Pres1 <> "" Then
            sLblUO = "(" & m_sUON1Pres1
            If m_sUON2Pres1 <> "" Then
                sLblUO = sLblUO & " - " & m_sUON2Pres1
                If m_sUON3Pres1 <> "" Then
                    sLblUO = sLblUO & " - " & m_sUON3Pres1 & ") "
                Else
                    sLblUO = sLblUO & ") "
                End If
            Else
                sLblUO = sLblUO & ") "
            End If
        End If
    Else
        If m_sUON1Pres2 <> "" Then
            sLblUO = "(" & m_sUON1Pres2
            If m_sUON2Pres2 <> "" Then
                sLblUO = sLblUO & " - " & m_sUON2Pres2
                If m_sUON3Pres2 <> "" Then
                    sLblUO = sLblUO & " - " & m_sUON3Pres2 & ") "
                Else
                    sLblUO = sLblUO & ") "
                End If
            Else
                sLblUO = sLblUO & ") "
            End If
        End If
    End If
    
    
    sPresup1 = frmSELPresAnuUON.g_sPRES1
    sPresup2 = frmSELPresAnuUON.g_sPRES2
    sPresup3 = frmSELPresAnuUON.g_sPRES3
    sPresup4 = frmSELPresAnuUON.g_sPRES4

    If iTipo = 1 Then
        If sPresup4 <> "" Then
            Me.lblPresup1.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup3 <> "" Then
            Me.lblPresup1.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup2 <> "" Then
            Me.lblPresup1.caption = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup1 <> "" Then
            Me.lblPresup1.caption = sLblUO & sPresup1 & " " & frmSELPresAnuUON.g_sDenPres
        End If
    Else
        If sPresup4 <> "" Then
            Me.lblPresup2.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup3 <> "" Then
            Me.lblPresup2.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup2 <> "" Then
            Me.lblPresup2.caption = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup1 <> "" Then
            Me.lblPresup2.caption = sLblUO & sPresup1 & " " & frmSELPresAnuUON.g_sDenPres
        End If
    End If
    
    If iTipo = 1 Then
        m_vPresup1_1 = frmSELPresAnuUON.g_vIdPRES1
        m_vPresup1_2 = frmSELPresAnuUON.g_vIdPRES2
        m_vPresup1_3 = frmSELPresAnuUON.g_vIdPRES3
        m_vPresup1_4 = frmSELPresAnuUON.g_vIdPRES4
    Else
        m_vPresup2_1 = frmSELPresAnuUON.g_vIdPRES1
        m_vPresup2_2 = frmSELPresAnuUON.g_vIdPRES2
        m_vPresup2_3 = frmSELPresAnuUON.g_vIdPRES3
        m_vPresup2_4 = frmSELPresAnuUON.g_vIdPRES4
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "MostrarPresSeleccionado12", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


Public Sub MostrarPresSeleccionado34(ByVal iTipo As Integer)
    Dim sLblUO As String
    Dim sPresup1 As String
    Dim sPresup2 As String
    Dim sPresup3 As String
    Dim sPresup4 As String
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If iTipo = 3 Then
        m_vPresup3_1 = Null
        m_vPresup3_2 = Null
        m_vPresup3_3 = Null
        m_vPresup3_4 = Null
        m_sUON1Pres3 = frmSELPresUO.g_sUON1
        m_sUON2Pres3 = frmSELPresUO.g_sUON2
        m_sUON3Pres3 = frmSELPresUO.g_sUON3
    Else
        m_vPresup4_1 = Null
        m_vPresup4_2 = Null
        m_vPresup4_3 = Null
        m_vPresup4_4 = Null
        m_sUON1Pres4 = frmSELPresUO.g_sUON1
        m_sUON2Pres4 = frmSELPresUO.g_sUON2
        m_sUON3Pres4 = frmSELPresUO.g_sUON3
    End If
    
    
    sLblUO = ""
    If iTipo = 3 Then
        If m_sUON1Pres3 <> "" Then
            sLblUO = "(" & m_sUON1Pres3
            If m_sUON2Pres3 <> "" Then
                sLblUO = sLblUO & " - " & m_sUON2Pres3
                If m_sUON3Pres3 <> "" Then
                    sLblUO = sLblUO & " - " & m_sUON3Pres3 & ") "
                Else
                    sLblUO = sLblUO & ") "
                End If
            Else
                sLblUO = sLblUO & ") "
            End If
        End If
    Else
        If m_sUON1Pres4 <> "" Then
            sLblUO = "(" & m_sUON1Pres4
            If m_sUON2Pres4 <> "" Then
                sLblUO = sLblUO & " - " & m_sUON2Pres4
                If m_sUON3Pres4 <> "" Then
                    sLblUO = sLblUO & " - " & m_sUON3Pres4 & ") "
                Else
                    sLblUO = sLblUO & ") "
                End If
            Else
                sLblUO = sLblUO & ") "
            End If
        End If
    End If
    
    sPresup1 = frmSELPresUO.g_sPRES1
    sPresup2 = frmSELPresUO.g_sPRES2
    sPresup3 = frmSELPresUO.g_sPRES3
    sPresup4 = frmSELPresUO.g_sPRES4

    If iTipo = 3 Then
        If sPresup4 <> "" Then
            lblPresup3.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup3 <> "" Then
            lblPresup3.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup2 <> "" Then
            lblPresup3.caption = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup1 <> "" Then
            lblPresup3.caption = sLblUO & sPresup1 & " " & frmSELPresUO.g_sDenPres
        End If
    Else
    
        If sPresup4 <> "" Then
            lblPresup4.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup3 <> "" Then
            lblPresup4.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup2 <> "" Then
            lblPresup4.caption = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup1 <> "" Then
            lblPresup4.caption = sLblUO & sPresup1 & " " & frmSELPresUO.g_sDenPres
        End If
    End If
    
    If iTipo = 3 Then
        m_vPresup3_1 = frmSELPresUO.g_vIdPRES1
        m_vPresup3_2 = frmSELPresUO.g_vIdPRES2
        m_vPresup3_3 = frmSELPresUO.g_vIdPRES3
        m_vPresup3_4 = frmSELPresUO.g_vIdPRES4
    Else
        m_vPresup4_1 = frmSELPresUO.g_vIdPRES1
        m_vPresup4_2 = frmSELPresUO.g_vIdPRES2
        m_vPresup4_3 = frmSELPresUO.g_vIdPRES3
        m_vPresup4_4 = frmSELPresUO.g_vIdPRES4
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "MostrarPresSeleccionado34", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Public Sub PersonaSeleccionada(iTipo As Integer, sUON1 As Variant, sUON2 As Variant, sUON3 As Variant, sDep As Variant, sPer As Variant)
Dim sUON As String
Dim iNivel As Integer
Dim oUons As Object

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If IsNull(sUON3) Then
    If IsNull(sUON2) Then
        If Not IsNull(sUON1) Then
            sUON = "(" & sUON1
            If IsNull(sPer) And IsNull(sDep) Then
                Set oUons = oFSGSRaiz.generar_CUnidadesOrgNivel1
                sUON = sUON & " - " & oUons.DevolverDenominacion(CStr(sUON1)) & ")"
                Set oUons = Nothing
            Else
                sUON = sUON & ")"
            End If
            iNivel = 1
        Else
            If iTipo = 1 Then
                cmdBorrarUsuAper_Click
            Else
                cmdBorrarResp_Click
            End If
        
        End If
    Else
        iNivel = 2
        sUON = "(" & sUON1 & " - " & sUON2
        If IsNull(sPer) And IsNull(sDep) Then
            Set oUons = oFSGSRaiz.generar_CUnidadesOrgNivel2
            sUON = sUON & " - " & oUons.DevolverDenominacion(CStr(sUON1), CStr(sUON2)) & ")"
            Set oUons = Nothing
        Else
            sUON = sUON & ")"
        End If
    End If
Else
    iNivel = 3
    sUON = "(" & sUON1 & " - " & sUON2 & " - " & sUON3
    If IsNull(sPer) And IsNull(sDep) Then
        Set oUons = oFSGSRaiz.generar_CUnidadesOrgNivel3
        sUON = sUON & " - " & oUons.DevolverDenominacion(CStr(sUON1), CStr(sUON2), CStr(sUON3)) & ")"
        Set oUons = Nothing
    Else
        sUON = sUON & ")"
    End If
End If

If Not IsNull(sDep) Then
    If sUON <> "" Then
        sUON = sUON & " " & sDep
    Else
        sUON = sDep
    End If
    If Not IsNull(sPer) Then
        Dim oPersonas As CPersonas
        Dim oPer As CPersona
        Set oPersonas = oFSGSRaiz.Generar_CPersonas
        oPersonas.CargarTodasLasPersonas CStr(sPer), , , True, 4
        Set oPer = oPersonas.Item(1)
        sUON = sUON & " - " & oPer.nombre & " " & oPer.Apellidos
    Else
        Dim oDeps As CDepartamentos
        Set oDeps = oFSGSRaiz.Generar_CDepartamentos
        oDeps.CargarTodosLosDepartamentos CStr(sDep), , True
        If oDeps.Count > 0 Then sUON = sUON & " - " & oDeps.Item(1).Den
        Set oDeps = Nothing
    End If

End If

If iTipo = 1 Then
    Me.lblUsuAper.caption = sUON
    m_sCodPerAper = sPer
    m_sUON1Aper = sUON1
    m_sUON2Aper = sUON2
    m_sUON3Aper = sUON3
    m_sDepAper = sDep
Else
    Me.lblResp.caption = sUON
    m_sCodResp = sPer
    m_sUON1Resp = sUON1
    m_sUON2Resp = sUON2
    m_sUON3Resp = sUON3
    m_sDepResp = sDep
End If
    


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "PersonaSeleccionada", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestCodProce_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestDenProce.Value = ""
        sdbcDestCodProce.RemoveAll
        m_bDestRespetarCombo = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestCodProce_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCodProce_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcDestCodProce.DroppedDown Then
        sdbcDestCodProce.Value = ""
        sdbcDestDenProce.Value = ""
        sdbcDestCodProce.RemoveAll
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestCodProce_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCodProce_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestCodProce.Value = "..." Then
        sdbcDestCodProce.Value = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestDenProce.Value = sdbcDestCodProce.Columns(1).Value
    sdbcDestCodProce.Value = sdbcDestCodProce.Columns(0).Value
    m_bDestRespetarCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestCodProce_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestCodProce_DropDown()
    Dim ADORs As Ador.Recordset
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcDestCodProce.RemoveAll
    
    If Not oUsuarioSummit.Persona Is Nothing Then
        If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
        
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , basOptimizacion.gPYMEUsuario, bRDestPerfUO, lIdPerfil)
    Else
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest)
    End If
    
    If ADORs Is Nothing Then
        sdbcDestCodProce.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestCodProce.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestCodProce.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbcDestCodProce.Rows = 0 Then
        sdbcDestCodProce.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestCodProce_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestCodProce_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDestCodProce.DataFieldList = "Column 0"
    sdbcDestCodProce.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestCodProce_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCodProce_PositionList(ByVal Text As String)
PositionList sdbcDestCodProce, Text
End Sub


Private Sub sdbcDestCodProce_Validate(Cancel As Boolean)
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestCodProce.Value = "" Then Exit Sub

    If sdbcDestCodProce.Value = sdbcDestCodProce.Columns(0).Value Then
        m_bDestRespetarCombo = True
        sdbcDestDenProce.Value = sdbcDestCodProce.Columns(1).Value
        m_bDestRespetarCombo = False
        Exit Sub
    End If

    If sdbcDestCodProce.Value = sdbcDestDenProce.Columns(1).Value Then
        m_bDestRespetarCombo = True
        sdbcDestDenProce.Value = sdbcDestDenProce.Columns(0).Value
        m_bDestRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    ''' Solo continuamos si existe el destino
    If bRDest Or bRDestPerfUO Then
        If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
        
        m_oDestinos.CargarTodosLosDestinosUON oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , UCase(CStr(sdbcDestCodProce.Value)), , True, , , , , , , , , , , bRDestPerfUO, lIdPerfil
    Else
        m_oDestinos.CargarTodosLosDestinos UCase(CStr(sdbcDestCodProce.Value)), , True, , , , , , , , True
    End If
    
    If m_oDestinos.Count = 0 Then
        sdbcDestCodProce.Value = ""
        Screen.MousePointer = vbNormal
    Else
        m_bDestRespetarCombo = True
        sdbcDestDenProce.Value = m_oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den

        sdbcDestCodProce.Columns(0).Value = sdbcDestCodProce.Value
        sdbcDestCodProce.Columns(1).Value = sdbcDestDenProce.Value

        m_bDestRespetarCombo = False
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestCodProce_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDenProce_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestCodProce.Value = ""
        sdbcDestCodProce.RemoveAll
        m_bDestRespetarCombo = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestDenProce_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDenProce_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcDestDenProce.DroppedDown Then
        sdbcDestDenProce.Value = ""
        sdbcDestCodProce.Value = ""
        sdbcDestCodProce.RemoveAll
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestDenProce_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDenProce_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestDenProce.Value = "..." Then
        sdbcDestDenProce.Value = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestCodProce.Value = sdbcDestDenProce.Columns(1).Value
    sdbcDestCodProce.Columns("COD").Value = sdbcDestDenProce.Columns(1).Value
    sdbcDestDenProce.Value = sdbcDestDenProce.Columns(0).Value
   ' sdbcDestCodProce_Validate False
    m_bDestRespetarCombo = False
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestDenProce_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestDenProce_DropDown()
    Dim ADORs As Ador.Recordset
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcDestDenProce.RemoveAll
    
    If Not oUsuarioSummit.Persona Is Nothing Then
        If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
        
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , True, basOptimizacion.gPYMEUsuario, bRDestPerfUO, lIdPerfil)
    Else
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest, , , , , True)
    End If
    
    If ADORs Is Nothing Then
        sdbcDestDenProce.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestDenProce.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestDenProce.AddItem ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbcDestDenProce.Rows = 0 Then
        sdbcDestDenProce.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestDenProce_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestDenProce_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDestDenProce.DataFieldList = "Column 0"
    sdbcDestDenProce.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestDenProce_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDenProce_PositionList(ByVal Text As String)
PositionList sdbcDestDenProce, Text
End Sub

Private Sub sdbcDestDenProce_Validate(Cancel As Boolean)
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestDenProce.Text = "" Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    ''' Solo continuamos si existe el destino
    If bRDest Or bRDestPerfUO Then
        If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
        
         m_oDestinos.CargarTodosLosDestinosUON oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , UCase(CStr(sdbcDestDenProce.Value)), True, , , , , , , , , , , bRDestPerfUO, lIdPerfil
    Else
         m_oDestinos.CargarTodosLosDestinos , UCase(CStr(sdbcDestDenProce.Value)), True, , , , , , , , True
    End If
    
    If m_oDestinos.Count = 0 Then
        sdbcDestCodProce.Value = ""
        Screen.MousePointer = vbNormal
    Else
        m_bDestRespetarCombo = True
        sdbcDestCodProce.Value = m_oDestinos.Item(1).Cod

        sdbcDestCodProce.Columns(0).Value = sdbcDestCodProce.Value
        sdbcDestCodProce.Columns(1).Value = sdbcDestDenProce.Value
        sdbcDestDenProce.Columns(1).Value = sdbcDestCodProce.Value
        sdbcDestDenProce.Columns(0).Value = sdbcDestDenProce.Value

        m_bDestRespetarCombo = False
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcDestDenProce_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcEqpDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcCompDen = ""
        sdbcEqpDen.RemoveAll
        bRespetarCombo = False
        
        bCargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcEqpDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcEqpDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcEqpDen.DroppedDown Then
        sdbcEqpDen = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcEqpDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcEqpDen_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcEqpDen.Value = "..." Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpDen.Value = "" Then Exit Sub
    
    sdbcCompDen = ""
    sdbcCompDen.RemoveAll
    optComp.Enabled = True
    optResp.Enabled = True
        
    bRespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    bRespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    Set oEqpSeleccionado = oEqps.Item(sdbcEqpDen.Columns(1).Text)
    Screen.MousePointer = vbNormal
    
    bCargarComboDesdeEqp = False

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcEqpDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcEqpDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bRAsig Or bRCompResponsable Then
        Exit Sub
    End If
        
    Screen.MousePointer = vbHourglass
    Set oEqps = Nothing
    Set oEqps = oFSGSRaiz.Generar_CEquipos
    
    sdbcEqpDen.RemoveAll
    
    If bCargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), False, False
    Else
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
            oEqps.CargarTodosLosEquipos , , False, True, False, basOptimizacion.gPYMEUsuario, NullToStr(oUsuarioSummit.Persona.codEqp)
        Else
            oEqps.CargarTodosLosEquipos , , False, True, False
        End If
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcEqpDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcEqpDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcEqpDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcEqpDen_PositionList(ByVal Text As String)
PositionList sdbcEqpDen, Text
End Sub

Private Sub sdbcEqpDen_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    If bRAsig Or bRCompResponsable Then
        If UCase(oUsuarioSummit.comprador.DenEqp) = UCase(sdbcEqpDen.Text) Then
            oEquipos.Add basOptimizacion.gCodEqpUsuario, oUsuarioSummit.comprador.DenEqp
        End If
    Else
        oEquipos.CargarTodosLosEquipos , sdbcEqpDen.Text, True, , False
    End If
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpDen.Text = ""
        sdbcCompDen = ""
        sdbcCompDen.RemoveAll
    Else
        bRespetarCombo = True
        Set oEqpSeleccionado = oEquipos.Item(1)

        sdbcEqpDen.Columns(1).Text = oEqpSeleccionado.Cod
        sdbcEqpDen.Columns(0).Text = oEqpSeleccionado.Den
        
        bRespetarCombo = False
        bCargarComboDesdeEqp = False
   
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcEqpDen_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcCompDen_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCompDen.RemoveAll
        bRespetarCombo = False
        
        bCargarComboDesdeComp = True
        Set oCompSeleccionado = Nothing
               
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcCompDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcCompDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcCompDen.DroppedDown Then
        sdbcCompDen = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcCompDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcCompDen_CloseUp()
    Dim sCod As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcCompDen.Value = "..." Then
        sdbcCompDen.Text = ""
        Exit Sub
    End If
    
    If sdbcCompDen.Value = "" Then Exit Sub
    If oEqpSeleccionado Is Nothing Then Exit Sub
    
    bRespetarCombo = True
    sdbcCompDen.Text = sdbcCompDen.Columns(0).Text
    bRespetarCombo = False
    
    sCod = oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oEqpSeleccionado.Cod))
    
    Screen.MousePointer = vbHourglass
    Set oCompSeleccionado = oComps.Item(sCod & sdbcCompDen.Columns(1).Text)
    Screen.MousePointer = vbNormal
      
    bCargarComboDesdeComp = False
    
    DoEvents
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcCompDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
       
End Sub

Private Sub sdbcCompDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Dim EqpCod As String
    
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcCompDen.RemoveAll
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
    
    If bRAsig Or bRCompResponsable Then
        Set oEqpSeleccionado.Compradores = oFSGSRaiz.generar_CCompradores
        oEqpSeleccionado.Compradores.Add oEqpSeleccionado.Cod, oUsuarioSummit.comprador.DenEqp, basOptimizacion.gCodCompradorUsuario, oUsuarioSummit.comprador.nombre, oUsuarioSummit.comprador.Apel, "", "", ""
        
        Set oComps = oEqpSeleccionado.Compradores
        Set oCompSeleccionado = oComps.Item(1)
        sdbcCompDen.AddItem oUsuarioSummit.comprador.Apel & ", " & oUsuarioSummit.comprador.nombre & Chr(m_lSeparador) & basOptimizacion.gCodCompradorUsuario
        Exit Sub
    End If

        
    Screen.MousePointer = vbHourglass
    Set oComps = Nothing
    Set oComps = oFSGSRaiz.generar_CCompradores

    If bCargarComboDesdeComp Then
        oEqpSeleccionado.CargarTodosLosCompradoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(Apellidos(sdbcEqpDen.Text)), True, False, True
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, True, False
    End If
    
    Set oComps = oEqpSeleccionado.Compradores
    Codigos = oComps.DevolverLosCodigos
    Set oEqpSeleccionado.Compradores = Nothing
    Screen.MousePointer = vbNormal
    
    If basOptimizacion.gTipoDeUsuario = comprador Then
        
        If bREqpAsig Then
            
            EqpCod = basOptimizacion.gCodEqpUsuario
            
            For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
            Next
            
        Else
            
            EqpCod = sdbcEqpDen.Columns("COD").Value
            
            For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
            Next
        
        End If
    Else
        
        For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
    End If
    
    If bCargarComboDesdeComp And Not oComps.EOF Then
        sdbcCompDen.AddItem "..."
    End If

    sdbcCompDen.SelStart = 0
    sdbcCompDen.SelLength = Len(sdbcCompDen.Text)
    sdbcCompDen.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcCompDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcCompDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcCompDen.DataFieldList = "Column 0"
    sdbcCompDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcCompDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcCompDen_PositionList(ByVal Text As String)
PositionList sdbcCompDen, Text
End Sub
Private Sub sdbcCompDen_Validate(Cancel As Boolean)

    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oEqpSeleccionado Is Nothing Then
        sdbcCompDen.Text = ""
        Exit Sub
    End If
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    Set oEqpSeleccionado.Compradores = oCompradores
    
    If sdbcCompDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    
    Screen.MousePointer = vbHourglass
    
    
    If bRAsig Or bRCompResponsable Then
        If UCase(oUsuarioSummit.comprador.Apel & ", " & oUsuarioSummit.comprador.nombre) = UCase(sdbcCompDen.Text) Then
            oEqpSeleccionado.Compradores.Add oEqpSeleccionado.Cod, oUsuarioSummit.comprador.DenEqp, basOptimizacion.gCodCompradorUsuario, oUsuarioSummit.comprador.nombre, oUsuarioSummit.comprador.Apel, "", "", ""
        End If
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , nombre(sdbcCompDen.Text), Apellidos(sdbcCompDen.Text), True, False, False, False
    End If
    Set oCompradores = oEqpSeleccionado.Compradores
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcCompDen.Text = ""
    Else
        bRespetarCombo = True
        Set oCompSeleccionado = oCompradores.Item(1)
        sdbcCompDen.Columns(1).Text = oCompSeleccionado.Cod
        sdbcCompDen.Columns(0).Text = oCompSeleccionado.DenEqp
        bRespetarCombo = False
        bCargarComboDesdeComp = False
    End If
    
    Set oEqpSeleccionado.Compradores = Nothing
    Set oCompradores = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcCompDen_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcProveCod_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If Not bRespetarComboProve Then
    
        bRespetarComboProve = True
        sdbcProveDen.Text = ""
        bRespetarComboProve = False
        
        bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcProveCod_Click()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
        bCargarComboDesde = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub
''' <summary>
''' Validacion del codigo del proveedor
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProveCod_Validate(Cancel As Boolean)
    Dim lIdPerfil As Long
    Dim bExiste As Boolean
    Dim oIasig As IAsignaciones
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") Then
        If Trim(sdbcAnyo.Text) <> "" And Trim(txtCod.Text) <> "" And Trim(sdbcGMN1_4Cod.Text) <> "" Then
            If Not g_oProceSeleccionado Is Nothing Then
                If Trim(sdbcAnyo.Text) <> g_oProceSeleccionado.Anyo Or Trim(txtCod.Text) <> g_oProceSeleccionado.Cod Or Trim(sdbcGMN1_4Cod.Text) <> g_oProceSeleccionado.GMN1Cod Then
                    bNuevoProce = True
                End If
            Else
                bNuevoProce = True
            End If
            
            If bNuevoProce = True Then
                Set g_oProceSeleccionado = Nothing
                Set g_oProceSeleccionado = oFSGSRaiz.Generar_CProceso
                g_oProceSeleccionado.Anyo = Trim(sdbcAnyo.Text)
                g_oProceSeleccionado.GMN1Cod = Trim(sdbcGMN1_4Cod.Text)
                g_oProceSeleccionado.Cod = Trim(Trim(txtCod.Text))
                If Not oUsuarioSummit.EsAdmin Then
                    If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
                                
                    Set oProces = oFSGSRaiz.generar_CProcesos
                    oProces.CargarTodosLosProcesosDesde 1, 1, 20, TipoOrdenacionProcesos.OrdPorCod, g_oProceSeleccionado.Anyo, g_oProceSeleccionado.GMN1Cod, , , , g_oProceSeleccionado.Cod, , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, bRUsuUON, bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , bRPerfUON, lIdPerfil
                    If oProces.Count = 0 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.PermisoDenegadoProceso
                        sdbcProveCod.Text = ""
                        Set g_oProceSeleccionado = Nothing
                        Set oProces = Nothing
                        Exit Sub
                    End If
                    Set oProces = Nothing
                End If
            End If
        Else
            Set g_oProceSeleccionado = Nothing
        End If
    End If
    Set oProves = oFSGSRaiz.generar_CProveedores
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") And Not g_oProceSeleccionado Is Nothing Then
        ' Ofertas por proceso
        Set oIasig = g_oProceSeleccionado

        If (m_bProveAsigComp) Then
            Set oProves = oIasig.DevolverProveedoresDesde(Trim(sdbcProveCod.Text), , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If (m_bProveAsigEqp) Then
                Set oProves = oIasig.DevolverProveedoresDesde(Trim(sdbcProveCod.Text), , True, , basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde(Trim(sdbcProveCod.Text), , True)
            End If
        End If
        Set oIasig = Nothing
        
    Else
        If bRestProvMatComp Then
            oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False, bRestProvMatComp, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod
        Else
            oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False
        End If
        
    End If
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        bRespetarComboProve = True
        sdbcProveCod.Text = oProves.Item(1).Cod
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Text = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Text = sdbcProveDen.Text
            
        bRespetarComboProve = False
    End If
    bCargarComboDesde = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboProve Then
    
        bRespetarComboProve = True
        sdbcProveCod.Text = ""
        bRespetarComboProve = False
        bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcProveDen_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
        bCargarComboDesde = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub

Private Sub sdbcProveDen_Validate(Cancel As Boolean)
    Dim lIdPerfil As Long
    Dim bExiste As Boolean
    Dim oIasig As IAsignaciones
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveDen.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") Then
        If Trim(sdbcAnyo.Text) <> "" And Trim(txtCod.Text) <> "" And Trim(sdbcGMN1_4Cod.Text) <> "" Then
            If Not g_oProceSeleccionado Is Nothing Then
                If Trim(sdbcAnyo.Text) <> g_oProceSeleccionado.Anyo Or Trim(txtCod.Text) <> g_oProceSeleccionado.Cod Or Trim(sdbcGMN1_4Cod.Text) <> g_oProceSeleccionado.GMN1Cod Then
                    bNuevoProce = True
                End If
            Else
                bNuevoProce = True
            End If
            
            If bNuevoProce = True Then
                Set g_oProceSeleccionado = Nothing
                Set g_oProceSeleccionado = oFSGSRaiz.Generar_CProceso
                g_oProceSeleccionado.Anyo = Trim(sdbcAnyo.Text)
                g_oProceSeleccionado.GMN1Cod = Trim(sdbcGMN1_4Cod.Text)
                g_oProceSeleccionado.Cod = Trim(Trim(txtCod.Text))
                If Not oUsuarioSummit.EsAdmin Then
                    If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
                                                    
                    Set oProces = oFSGSRaiz.generar_CProcesos
                    oProces.CargarTodosLosProcesosDesde 1, 1, 20, TipoOrdenacionProcesos.OrdPorCod, g_oProceSeleccionado.Anyo, g_oProceSeleccionado.GMN1Cod, , , , g_oProceSeleccionado.Cod, , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, bRUsuUON, bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , bRPerfUON, lIdPerfil
                    If oProces.Count = 0 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.PermisoDenegadoProceso
                        sdbcProveCod.Text = ""
                        Set g_oProceSeleccionado = Nothing
                        Set oProces = Nothing
                        Exit Sub
                    End If
                    Set oProces = Nothing
                End If
            End If
        Else
            Set g_oProceSeleccionado = Nothing
        End If
    End If
    Set oProves = oFSGSRaiz.generar_CProveedores
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") And Not g_oProceSeleccionado Is Nothing Then
        ' Ofertas por proceso
        Set oIasig = g_oProceSeleccionado

        If (m_bProveAsigComp) Then
            Set oProves = oIasig.DevolverProveedoresDesde(, Trim(sdbcProveDen.Text), True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If (m_bProveAsigEqp) Then
                Set oProves = oIasig.DevolverProveedoresDesde(, Trim(sdbcProveDen.Text), True, , basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde(, Trim(sdbcProveDen.Text), True)
            End If
        End If
        Set oIasig = Nothing
    Else
    
        If bRestProvMatComp Then
            oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProveDen.Text), True, , False, bRestProvMatComp, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod
        Else
            oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProveDen.Text), True, , False
        End If
                    
    End If
    
    bExiste = Not (oProves.Count = 0)
    
    If Not bExiste Then
        sdbcProveDen.Text = ""
    Else
        bRespetarComboProve = True
        sdbcProveCod.Text = oProves.Item(1).Cod
        sdbcProveDen.Text = oProves.Item(1).Den
        sdbcProveDen.Columns(1).Text = sdbcProveCod.Text
        sdbcProveDen.Columns(0).Text = sdbcProveDen.Text
        
        bRespetarComboProve = False
    End If
    Screen.MousePointer = vbNormal
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveDen_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub sdbcProveDen_CloseUp()

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    bRespetarComboProve = False
    bCargarComboDesde = False
    DoEvents
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
            
End Sub

''' <summary>
''' DropDown del combo de denominacion de proveedores, carga la lista
''' </summary>
'''
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en el combo de denominacion de proveedor; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveDen_DropDown()
    Dim lIdPerfil As Long
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oIasig As IAsignaciones
    Dim sDen As String
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
       
    If gParametrosGenerales.gbOblProveEqp Then
        If sdbcEqpDen.Text <> "" Then
            sEqp = Trim(sdbcEqpDen.Columns(0).Value)
        Else
            sEqp = ""
        End If
    Else
        sEqp = ""
    End If
       
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") Then
        If Trim(sdbcAnyo.Text) <> "" And Trim(txtCod.Text) <> "" And Trim(sdbcGMN1_4Cod.Text) <> "" Then
            If Not g_oProceSeleccionado Is Nothing Then
                If Trim(sdbcAnyo.Text) <> g_oProceSeleccionado.Anyo Or Trim(txtCod.Text) <> g_oProceSeleccionado.Cod Or Trim(sdbcGMN1_4Cod.Text) <> g_oProceSeleccionado.GMN1Cod Then
                    bNuevoProce = True
                End If
            Else
                bNuevoProce = True
            End If
            
            If bNuevoProce = True Then
                Set g_oProceSeleccionado = Nothing
                Set g_oProceSeleccionado = oFSGSRaiz.Generar_CProceso
                g_oProceSeleccionado.Anyo = Trim(sdbcAnyo.Text)
                g_oProceSeleccionado.GMN1Cod = Trim(sdbcGMN1_4Cod.Text)
                g_oProceSeleccionado.Cod = Trim(Trim(txtCod.Text))
                If Not oUsuarioSummit.EsAdmin Then
                    If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
                                                                        
                    Set oProces = oFSGSRaiz.generar_CProcesos
                    oProces.CargarTodosLosProcesosDesde 1, 1, 20, TipoOrdenacionProcesos.OrdPorCod, g_oProceSeleccionado.Anyo, g_oProceSeleccionado.GMN1Cod, , , , g_oProceSeleccionado.Cod, , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, bRUsuUON, bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , bRPerfUON, lIdPerfil
                    If oProces.Count = 0 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.PermisoDenegadoProceso
                        sdbcProveCod.Text = ""
                        Set g_oProceSeleccionado = Nothing
                        Set oProces = Nothing
                        Exit Sub
                    End If
                    Set oProces = Nothing
                End If
            End If
        Else
            Set g_oProceSeleccionado = Nothing
        End If
    End If
    
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") And Not g_oProceSeleccionado Is Nothing Then
        ' Ofertas por proceso
        If bCargarComboDesde Then
            sDen = Trim(sdbcProveDen.Text)
        Else
            sDen = ""
        End If
        Set oIasig = g_oProceSeleccionado

        If (m_bProveAsigComp) Then
            Set oProves = oIasig.DevolverProveedoresDesde(, sDen, , OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If (m_bProveAsigEqp) Then
                Set oProves = oIasig.DevolverProveedoresDesde(, sDen, , OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde(, sDen, , OrdAsigPorDenProve)
            End If
        End If
        Set oIasig = Nothing
    Else
        If bRestProvMatComp Then
            If bCargarComboDesde Then
                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , Trim(sdbcProveDen.Text)
            Else
                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
            End If
        Else
            If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
                If bCargarComboDesde Then
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProveDen.Text), , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
                Else
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
                End If
            Else
            
                If bCargarComboDesde Then
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProveDen.Text), , , , , , , , , , , True
                Else
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True
                End If
                
            End If
        End If
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    bRespetarComboProve = False
    bCargarComboDesde = False
    DoEvents
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub
Private Sub sdbcProveCod_DropDown()
    Dim lIdPerfil As Long
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oIasig As IAsignaciones
    Dim sCod As String
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    If gParametrosGenerales.gbOblProveEqp Then
        If sdbcEqpDen.Text <> "" Then
            sEqp = Trim(sdbcEqpDen.Columns("cod").Value)
        Else
            sEqp = ""
        End If
    Else
        sEqp = ""
    End If
    
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") Then
        If Trim(sdbcAnyo.Text) <> "" And Trim(txtCod.Text) <> "" And Trim(sdbcGMN1_4Cod.Text) <> "" Then
            If Not g_oProceSeleccionado Is Nothing Then
                If Trim(sdbcAnyo.Text) <> g_oProceSeleccionado.Anyo Or Trim(txtCod.Text) <> g_oProceSeleccionado.Cod Or Trim(sdbcGMN1_4Cod.Text) <> g_oProceSeleccionado.GMN1Cod Then
                    bNuevoProce = True
                End If
            Else
                bNuevoProce = True
            End If
            
            If bNuevoProce = True Then
                Set g_oProceSeleccionado = Nothing
                Set g_oProceSeleccionado = oFSGSRaiz.Generar_CProceso
                g_oProceSeleccionado.Anyo = Trim(sdbcAnyo.Text)
                g_oProceSeleccionado.GMN1Cod = Trim(sdbcGMN1_4Cod.Text)
                g_oProceSeleccionado.Cod = Trim(Trim(txtCod.Text))
                If Not oUsuarioSummit.EsAdmin Then
                    If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
                                                                                            
                    Set oProces = oFSGSRaiz.generar_CProcesos
                    oProces.CargarTodosLosProcesosDesde 1, 1, 20, TipoOrdenacionProcesos.OrdPorCod, g_oProceSeleccionado.Anyo, g_oProceSeleccionado.GMN1Cod, , , , g_oProceSeleccionado.Cod, , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, bRUsuUON, bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , bRPerfUON, lIdPerfil
                    If oProces.Count = 0 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.PermisoDenegadoProceso
                        sdbcProveCod.Text = ""
                        Set g_oProceSeleccionado = Nothing
                        Set oProces = Nothing
                        Exit Sub
                    End If
                    Set oProces = Nothing
                End If
            End If
        Else
            Set g_oProceSeleccionado = Nothing
        End If
    End If
    
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") And Not g_oProceSeleccionado Is Nothing Then
        ' Ofertas por proceso
        If bCargarComboDesde Then
            sCod = Trim(sdbcProveCod.Text)
        Else
            sCod = ""
        End If
        
        Set oIasig = g_oProceSeleccionado

        If (m_bProveAsigComp) Then
            Set oProves = oIasig.DevolverProveedoresDesde(sCod, , , OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If (m_bProveAsigEqp) Then
                Set oProves = oIasig.DevolverProveedoresDesde(sCod, , , OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde(sCod, , , OrdAsigPorCodProve)
            End If
        End If
        Set oIasig = Nothing
    Else
        If bRestProvMatComp Then
            If bCargarComboDesde Then
                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, Trim(sdbcProveCod.Text)
            Else
                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
            End If
        Else
            If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
                If bCargarComboDesde Then
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProveCod.Text), , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
                Else
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
                End If
            Else
            
                If bCargarComboDesde Then
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProveCod.Text)
                Else
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp
                End If
                
            End If
        End If
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next

    If Not oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If

    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcProveCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "sdbcProveCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Public Sub CargarProveBusqueda()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveCod_Validate False
    Set frmPROVEBuscar.oProveEncontrados = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "CargarProveBusqueda", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub
'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
Private Sub CargarGMN1Automaticamente()
    Dim oGMN1s As CGruposMatNivel1
    Dim m_oIMAsig As IMaterialAsignado
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oIMAsig = oUsuarioSummit.comprador
    Set oGMN1s = m_oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
    If Not oGMN1s.Count = 0 Then
        bRespetarCombo = True
        sdbcGMN1Proce_Cod.Text = oGMN1s.Item(1).Cod
        sdbcGMN1Proce_Cod.Columns(0).Text = sdbcGMN1Proce_Cod.Text
        bRespetarCombo = False
        GMN1Seleccionado
    End If
    Set oGMN1s = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscar", "CargarGMN1Automaticamente", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub VaciarVables()


' Variables de restricciones
bRMat = False
bRAsig = False
bRCompResponsable = False
bREqpAsig = False
bRUsuAper = False
bRUsuUON = False
bRPerfUON = False
bRUsuDep = False
bRDest = False
bRDestPerfUO = False
bRestProvMatComp = False
bRestProvEquComp = False
'Variables para materiales
Set oGruposMN1 = Nothing
Set oGruposMN2 = Nothing
Set oGruposMN3 = Nothing
Set oGruposMN4 = Nothing

Set oGMN1Seleccionado = Nothing
Set oGMN2Seleccionado = Nothing
Set oGMN3Seleccionado = Nothing
Set oGMN4Seleccionado = Nothing

sProveAdj = ""

Set oEqps = Nothing
Set oEqpSeleccionado = Nothing

Set oComps = Nothing
Set oCompSeleccionado = Nothing
m_bProveAsigComp = False
m_bProveAsigEqp = False


Set oProves = Nothing

'Variables para Art�culos
Set oArticulos = Nothing
Set oArticuloSeleccionado = Nothing

' Variables para el manejo de combos
bRespetarCombo = False
bRespetarComboGMN2 = False
bRespetarComboGMN3 = False
bRespetarComboGMN4 = False
bRespetarComboArt = False
bRespetarComboEst = False
GMN1CargarComboDesde = False
GMN1RespetarCombo = False
GMN2CargarComboDesde = False
GMN2RespetarCombo = False
GMN3CargarComboDesde = False
GMN3RespetarCombo = False
GMN4CargarComboDesde = False
GMN4RespetarCombo = False

bCargarComboDesde = False

m_bDestRespetarCombo = False
bCargarComboDesdeEqp = False
bCargarComboDesdeComp = False
bRespetarComboProve = False


Set oMonedas = Nothing
Set m_oDestinos = Nothing

'Variable para saber el origen
sOrigen = ""
Set g_oOrigen = Nothing

DesdeEst = 0
HastaEst = 0

dPresDesde = 0
dPresHasta = 0

'Presupuestos:
m_vPresup1_1 = Empty
m_vPresup1_2 = Empty
m_vPresup1_3 = Empty
m_vPresup1_4 = Empty

m_vPresup2_1 = Empty
m_vPresup2_2 = Empty
m_vPresup2_3 = Empty
m_vPresup2_4 = Empty

m_vPresup3_1 = Empty
m_vPresup3_2 = Empty
m_vPresup3_3 = Empty
m_vPresup3_4 = Empty

m_vPresup4_1 = Empty
m_vPresup4_2 = Empty
m_vPresup4_3 = Empty
m_vPresup4_4 = Empty


m_sUON1Pres1 = Empty

m_sUON2Pres1 = Empty
m_sUON3Pres1 = Empty

m_sUON1Pres2 = Empty
m_sUON2Pres2 = Empty
m_sUON3Pres2 = Empty

m_sUON1Pres3 = Empty
m_sUON2Pres3 = Empty
m_sUON3Pres3 = Empty

m_sUON1Pres4 = Empty
m_sUON2Pres4 = Empty
m_sUON3Pres4 = Empty

m_sCodResp = Empty
m_sCodPerAper = Empty

m_sUON1Aper = Empty
m_sUON2Aper = Empty
m_sUON3Aper = Empty

m_sUON1Resp = Empty
m_sUON2Resp = Empty
m_sUON3Resp = Empty

m_sDepAper = Empty
m_sDepResp = Empty

g_bSoloInvitado = False 'Si es true el usuario no tiene acceso a esta pantalla, solo como invitado
bPermProcMultiMaterial = False

'Multilenguaje
sIdiMoneda = ""
sIdiMaterial = ""
sIdiDetalle = ""
sIdiResponsable = ""
sIdiFecha = ""
sIdiPresupuesto = ""

m_bActivado = False
m_bDescargarFrm = False
m_sMsgError = ""

End Sub


