VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPRESItem 
   BackColor       =   &H00808000&
   Caption         =   "Presupuestos asignados al item"
   ClientHeight    =   6315
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7275
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPRESItem.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6315
   ScaleWidth      =   7275
   Begin VB.CommandButton cmdRestaurar 
      Caption         =   "&Restaurar"
      Height          =   345
      Left            =   1140
      TabIndex        =   2
      Top             =   5940
      Width           =   1005
   End
   Begin VB.CommandButton cmdModifPres 
      Caption         =   "&Modificar"
      Height          =   345
      Left            =   60
      TabIndex        =   1
      Top             =   5940
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPresupuestos 
      Height          =   5775
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   7170
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   5
      stylesets.count =   9
      stylesets(0).Name=   "Pres2"
      stylesets(0).BackColor=   10351792
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPRESItem.frx":0CB2
      stylesets(1).Name=   "BLPresAnu1"
      stylesets(1).BackColor=   12369018
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(1).Picture=   "frmPRESItem.frx":0CCE
      stylesets(2).Name=   "BLPresAnu2"
      stylesets(2).BackColor=   10342143
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(2).Picture=   "frmPRESItem.frx":0CEA
      stylesets(3).Name=   "PresAnu1"
      stylesets(3).BackColor=   12369018
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmPRESItem.frx":0D06
      stylesets(4).Name=   "PresAnu2"
      stylesets(4).BackColor=   10342143
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmPRESItem.frx":0D22
      stylesets(5).Name=   "Normal"
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmPRESItem.frx":0D3E
      stylesets(6).Name=   "BLPres1"
      stylesets(6).BackColor=   52428
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(6).Picture=   "frmPRESItem.frx":0D5A
      stylesets(7).Name=   "BLPres2"
      stylesets(7).BackColor=   10351792
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(7).Picture=   "frmPRESItem.frx":0D76
      stylesets(8).Name=   "Pres1"
      stylesets(8).BackColor=   52428
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmPRESItem.frx":0D92
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   2381
      Columns(0).Caption=   "Tipo"
      Columns(0).Name =   "TIPO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2858
      Columns(1).Caption=   "UO"
      Columns(1).Name =   "UO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   4921
      Columns(2).Caption=   "Presupuesto"
      Columns(2).Name =   "PRESUP"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1640
      Columns(3).Caption=   "Porcentaje"
      Columns(3).Name =   "PORCEN"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "NUMTIPO"
      Columns(4).Name =   "NUMTIPO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   12647
      _ExtentY        =   10186
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPRESItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oItem As CItem

Private m_bRuo As Boolean
Private m_bRUOPresAnuTipo1 As Boolean
Private m_bRUOPresAnuTipo2 As Boolean
Private m_bRUOPresTipo1 As Boolean
Private m_bRUOPresTipo2 As Boolean

Public g_bHayPresAnu1 As Boolean
Public g_bHayPresAnu2 As Boolean
Public g_bHayPres1 As Boolean
Public g_bHayPres2 As Boolean

Public g_bModif As Boolean
Public g_sOrigen As String
Public g_sGrupo As String

Public g_bIniEdicionProce As Boolean

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESITEM, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        sdbgPresupuestos.Columns("TIPO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPresupuestos.Columns("UO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPresupuestos.Columns("PRESUP").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPresupuestos.Columns("PORCEN").caption = Ador(0).Value
        Ador.MoveNext
        cmdModifPres.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        If g_sOrigen = "PROCE_MODIFITEMS" Then
            Me.caption = Ador(0).Value
        End If
        Ador.Close

    End If

    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Public Sub ModificarPres(ByVal iTipo As Integer)
Dim dblAbierto As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmPROCE.g_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    If Not g_bIniEdicionProce Then
        If Not frmPROCE.BloquearProceso("IT", 1) Then Exit Sub
        frmPROCE.m_udtOrigBloqueo = ModificarItems
    End If
    
    dblAbierto = NullToDbl0(g_oItem.Presupuesto)
    frmPRESAsig.g_sOrigen = "MODIF_PRES"
    frmPRESAsig.g_iAmbitoPresup = 3
    frmPRESAsig.g_iTipoPres = iTipo
    frmPRESAsig.g_dblAbierto = dblAbierto
    frmPRESAsig.g_bModif = True
    Select Case iTipo
        Case 1
            frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo1
            If g_bHayPresAnu1 Then
                frmPRESAsig.g_dblAsignado = dblAbierto
                frmPRESAsig.g_bHayPres = True
            Else
                frmPRESAsig.g_dblAsignado = 0
                frmPRESAsig.g_bHayPres = False
            End If
        Case 2
            frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo2
            If g_bHayPresAnu2 Then
                frmPRESAsig.g_dblAsignado = dblAbierto
                frmPRESAsig.g_bHayPres = True
            Else
                frmPRESAsig.g_dblAsignado = 0
                frmPRESAsig.g_bHayPres = False
            End If
        Case 3
            frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo1
            If g_bHayPres1 Then
                frmPRESAsig.g_dblAsignado = dblAbierto
                frmPRESAsig.g_bHayPres = True
            Else
                frmPRESAsig.g_dblAsignado = 0
                frmPRESAsig.g_bHayPres = False
            End If
        Case 4
            frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo2
            If g_bHayPres2 Then
                frmPRESAsig.g_dblAsignado = dblAbierto
                frmPRESAsig.g_bHayPres = True
            Else
                frmPRESAsig.g_dblAsignado = 0
                frmPRESAsig.g_bHayPres = False
            End If
    End Select
        
    frmPRESAsig.g_bHayPresBajaLog = False
    frmPRESAsig.Show 1
    
    Select Case iTipo
        Case 1
                If g_bHayPresAnu1 Then
                    frmPROCE.sdbgItems.Columns("PANU1").Value = 1
                Else
                    frmPROCE.sdbgItems.Columns("PANU1").Value = 0
                End If
        Case 2
                If g_bHayPresAnu2 Then
                    frmPROCE.sdbgItems.Columns("PANU2").Value = 1
                Else
                    frmPROCE.sdbgItems.Columns("PANU2").Value = 0
                End If
        Case 3
                If g_bHayPres1 Then
                    frmPROCE.sdbgItems.Columns("PRES1").Value = 1
                Else
                    frmPROCE.sdbgItems.Columns("PRES1").Value = 0
                End If
        Case 4
                If g_bHayPres2 Then
                    frmPROCE.sdbgItems.Columns("PRES2").Value = 1
                Else
                    frmPROCE.sdbgItems.Columns("PRES2").Value = 0
                End If
    End Select
    
    frmPROCE.g_bUpdate = False
    frmPROCE.sdbgItems.Update
    frmPROCE.g_bUpdate = True
       
    If Not g_bIniEdicionProce Then
        frmPROCE.DesbloquearProceso ("IT")
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "ModificarPres", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Public Sub ModificarPresMultiplesItems(ByVal iTipo As Integer)

Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmPROCE.g_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    If Not g_bIniEdicionProce Then
        If Not frmPROCE.BloquearProceso("IT", 1) Then Exit Sub
        frmPROCE.m_udtOrigBloqueo = ModificarItems
    End If
    
    frmPRESAsig.g_iAmbitoPresup = 3
    frmPRESAsig.g_iTipoPres = iTipo
    frmPRESAsig.g_dblAbierto = 0
    frmPRESAsig.g_dblAsignado = 0
    frmPRESAsig.g_bModif = True
    Select Case iTipo
        Case 1
            frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo1
        Case 2
            frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo2
        Case 3
            frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo1
        Case 4
            frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo2
    End Select
    
    frmPRESAsig.g_sOrigen = "PROCE_MODIFITEMS"
    frmPRESAsig.g_bHayPres = False
    frmPRESAsig.g_bHayPresBajaLog = False
    
    frmPRESAsig.Show 1
    
    frmPROCE.g_bUpdate = False   'para evitar el beforeUpdate
    
    Select Case iTipo
        Case 1
                If g_bHayPresAnu1 Then
                    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                        frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                        frmPROCE.sdbgItems.Columns("PANU1").Value = 1
                    Next
                Else
                    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                        frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                        frmPROCE.sdbgItems.Columns("PANU1").Value = 0
                    Next
                End If
        Case 2
                If g_bHayPresAnu2 Then
                    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                        frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                        frmPROCE.sdbgItems.Columns("PANU2").Value = 1
                    Next
                Else
                    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                        frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                        frmPROCE.sdbgItems.Columns("PANU2").Value = 0
                    Next
                End If
        Case 3
                If g_bHayPres1 Then
                    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                        frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                        frmPROCE.sdbgItems.Columns("PRES1").Value = 1
                    Next
                Else
                    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                        frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                        frmPROCE.sdbgItems.Columns("PRES1").Value = 0
                    Next
                End If
        Case 4
                If g_bHayPres2 Then
                    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                        frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                        frmPROCE.sdbgItems.Columns("PRES2").Value = 1
                    Next
                Else
                    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                        frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                        frmPROCE.sdbgItems.Columns("PRES2").Value = 0
                    Next
                End If
    End Select
    
    frmPROCE.sdbgItems.Update
    frmPROCE.g_bUpdate = True
    
    
    If Not g_bIniEdicionProce Then frmPROCE.DesbloquearProceso ("IT")
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "ModificarPresMultiplesItems", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub cmdModifPres_Click()
            
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmPROCE.sdbgItems.Columns("PCERRA").Value <> "1" Then
        MDI.mnuPopUpModifPresup(0).Visible = True
        MDI.mnuPopUpModifPresup(1).Visible = True
        MDI.mnuPopUpModifPresup(2).Visible = True
        MDI.mnuPopUpModifPresup(3).Visible = True
        
        If Not gParametrosGenerales.gbUsarPres1 Then
            MDI.mnuPopUpModifPresup(0).Visible = False
        Else
            If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso Or frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupo).DefPresAnualTipo1 Then
                MDI.mnuPopUpModifPresup(0).Visible = False
            End If
        End If
        
        If Not gParametrosGenerales.gbUsarPres2 Then
            MDI.mnuPopUpModifPresup(1).Visible = False
        Else
            If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso Or frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupo).DefPresAnualTipo2 Then
                MDI.mnuPopUpModifPresup(1).Visible = False
            End If
        End If
        
        If Not gParametrosGenerales.gbUsarPres3 Then
            MDI.mnuPopUpModifPresup(2).Visible = False
        Else
            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso Or frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupo).DefPresTipo1 Then
                MDI.mnuPopUpModifPresup(2).Visible = False
            End If
        End If
        
        If Not gParametrosGenerales.gbUsarPres4 Then
            MDI.mnuPopUpModifPresup(3).Visible = False
        Else
            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Or frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupo).DefPresTipo2 Then
                MDI.mnuPopUpModifPresup(3).Visible = False
            End If
        End If
        
        frmPROCE.g_bHaPulsadoModifPres = False
        PopupMenu MDI.POPUPModifPresup, , cmdModifPres.Left, cmdModifPres.Top - cmdModifPres.Height
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "cmdModifPres_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub cmdRestaurar_Click()
    '"PROCE_MODIFITEMS"=origen popup de items, opci�n "Presupuestos"
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_sOrigen <> "PROCE_MODIFITEMS" Then
        CargarGridPresupuestos 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "cmdRestaurar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Height = 6720
    Me.Width = 7395
    
    CargarRecursos
    
    PonerFieldSeparator Me
        
    If Not frmPROCE.g_bModifItems Then
        cmdRestaurar.Left = cmdModifPres.Left
        cmdModifPres.Visible = False
    End If
    
    'Si tiene restricci�n de UO y s�lo puede ver su UO
    '(est� en una UO hoja dentro del �rbol y no puede ver m�s que la suya)
    m_bRuo = frmPROCE.g_bRUO
    If m_bRuo Then
        If Not oUsuarioSummit.ExisteUODebajo Then
            sdbgPresupuestos.Columns("UO").Visible = False
        End If
    End If
    
    m_bRUOPresAnuTipo1 = frmPROCE.g_bRUOPresAnuTipo1
    m_bRUOPresAnuTipo2 = frmPROCE.g_bRUOPresAnuTipo2
    m_bRUOPresTipo1 = frmPROCE.g_bRUOPresTipo1
    m_bRUOPresTipo2 = frmPROCE.g_bRUOPresTipo2
    
    '"PROCE_MODIFITEMS"=origen popup de items, opci�n "Presupuestos"
    If g_sOrigen <> "PROCE_MODIFITEMS" Then
        CargarGridPresupuestos 0
    End If
    
    'HabilitarBotonesInvitado
    'El item y el grupo del item seleccionado en frmPROCE no tienen
    'definido ninguno de los presupuestos
    If Not g_bModif Then
        cmdModifPres.Visible = False
        cmdRestaurar.Visible = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Public Function HabilitarBotonesInvitado()
' funci�n para mostrar/ocultar los botones si se es invtado

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If GridCheckToBoolean(frmPROCE.sdbcProceCod.Columns("INVI").Value) = True Or GridCheckToBoolean(frmPROCE.sdbcProceDen.Columns("INVI").Value) Then

    cmdModifPres.Visible = False
    cmdRestaurar.Visible = False
Else

    cmdModifPres.Visible = True
    cmdRestaurar.Visible = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "HabilitarBotonesInvitado", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Sub Form_Resize()

On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If Me.Width < 2700 Then Exit Sub
    
    sdbgPresupuestos.Width = Me.Width - 215
    If sdbgPresupuestos.Columns("UO").Visible Then
        sdbgPresupuestos.Columns(0).Width = sdbgPresupuestos.Width * 0.2
        sdbgPresupuestos.Columns(1).Width = sdbgPresupuestos.Width * 0.2
        sdbgPresupuestos.Columns(2).Width = sdbgPresupuestos.Width * 0.4
        If Me.Width < 7800 Then
            sdbgPresupuestos.Columns(3).Width = sdbgPresupuestos.Width * 0.155
        Else
            sdbgPresupuestos.Columns(3).Width = sdbgPresupuestos.Width * 0.18
        End If
    Else
        sdbgPresupuestos.Columns(0).Width = sdbgPresupuestos.Width * 0.3
        sdbgPresupuestos.Columns(2).Width = sdbgPresupuestos.Width * 0.5
        If Me.Width < 7800 Then
            sdbgPresupuestos.Columns(3).Width = sdbgPresupuestos.Width * 0.155
        Else
            sdbgPresupuestos.Columns(3).Width = sdbgPresupuestos.Width * 0.18
        End If
    End If
    
    sdbgPresupuestos.Height = Me.Height - (cmdModifPres.Height + 800)
    
    cmdModifPres.Top = sdbgPresupuestos.Height + 300
    cmdModifPres.Left = sdbgPresupuestos.Left
    cmdRestaurar.Top = sdbgPresupuestos.Height + 300

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    frmPROCE.DesbloquearProceso ("IT")

    frmPROCE.sdbgItems.SelBookmarks.RemoveAll
    g_bIniEdicionProce = False
    g_bModif = True
    g_bHayPresAnu1 = False
    g_bHayPresAnu2 = False
    g_bHayPres1 = False
    g_bHayPres2 = False
    Set g_oItem = Nothing
    Me.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbgPresupuestos_HeadClick(ByVal ColIndex As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    CargarGridPresupuestos ColIndex
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "sdbgPresupuestos_HeadClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgPresupuestos_RowLoaded(ByVal Bookmark As Variant)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case sdbgPresupuestos.Columns("NUMTIPO").Value
        
        Case 1
                sdbgPresupuestos.Columns(0).CellStyleSet "PresAnu1"
                sdbgPresupuestos.Columns(1).CellStyleSet "PresAnu1"
                sdbgPresupuestos.Columns(2).CellStyleSet "PresAnu1"
                sdbgPresupuestos.Columns(3).CellStyleSet "PresAnu1"
                
        Case 2
                sdbgPresupuestos.Columns(0).CellStyleSet "PresAnu2"
                sdbgPresupuestos.Columns(1).CellStyleSet "PresAnu2"
                sdbgPresupuestos.Columns(2).CellStyleSet "PresAnu2"
                sdbgPresupuestos.Columns(3).CellStyleSet "PresAnu2"
                
        Case 3
                sdbgPresupuestos.Columns(0).CellStyleSet "Pres1"
                sdbgPresupuestos.Columns(1).CellStyleSet "Pres1"
                sdbgPresupuestos.Columns(2).CellStyleSet "Pres1"
                sdbgPresupuestos.Columns(3).CellStyleSet "Pres1"
                
        Case 4
                sdbgPresupuestos.Columns(0).CellStyleSet "Pres2"
                sdbgPresupuestos.Columns(1).CellStyleSet "Pres2"
                sdbgPresupuestos.Columns(2).CellStyleSet "Pres2"
                sdbgPresupuestos.Columns(3).CellStyleSet "Pres2"
                
        Case 5
                sdbgPresupuestos.Columns(0).CellStyleSet "BLPresAnu1"
                sdbgPresupuestos.Columns(1).CellStyleSet "BLPresAnu1"
                sdbgPresupuestos.Columns(2).CellStyleSet "BLPresAnu1"
                sdbgPresupuestos.Columns(3).CellStyleSet "BLPresAnu1"
                
        Case 6
                sdbgPresupuestos.Columns(0).CellStyleSet "BLPresAnu2"
                sdbgPresupuestos.Columns(1).CellStyleSet "BLPresAnu2"
                sdbgPresupuestos.Columns(2).CellStyleSet "BLPresAnu2"
                sdbgPresupuestos.Columns(3).CellStyleSet "BLPresAnu2"
                
        Case 7
                sdbgPresupuestos.Columns(0).CellStyleSet "BLPres1"
                sdbgPresupuestos.Columns(1).CellStyleSet "BLPres1"
                sdbgPresupuestos.Columns(2).CellStyleSet "BLPres1"
                sdbgPresupuestos.Columns(3).CellStyleSet "BLPres1"
                
        Case 8
                sdbgPresupuestos.Columns(0).CellStyleSet "BLPres2"
                sdbgPresupuestos.Columns(1).CellStyleSet "BLPres2"
                sdbgPresupuestos.Columns(2).CellStyleSet "BLPres2"
                sdbgPresupuestos.Columns(3).CellStyleSet "BLPres2"
        
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "sdbgPresupuestos_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarGridPresupuestos(ByVal iCol As Integer)
'********************************************************************
' Carga en la grid de presupuestos todos los presupuestos del item.
'********************************************************************
Dim adoRecordset As Ador.Recordset
Dim sUON As String
Dim sPresup As String
Dim sTipo As String
Dim iTipo As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oItem Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    sdbgPresupuestos.RemoveAll
    
    Select Case sdbgPresupuestos.Columns(iCol).Name

        Case "TIPO"
                Set adoRecordset = g_oItem.DevolverTodosLosPresupuestos(, , , , 0, , , , IIf(Not frmPROCE.lblPresGrupo.Visible, True, False))
        Case "UO"
                Set adoRecordset = g_oItem.DevolverTodosLosPresupuestos(, , , , 1, , , , IIf(Not frmPROCE.lblPresGrupo.Visible, True, False))
        Case "PRESUP"
                Set adoRecordset = g_oItem.DevolverTodosLosPresupuestos(, , , , 2, , , , IIf(Not frmPROCE.lblPresGrupo.Visible, True, False))
        Case "PORCEN"
                Set adoRecordset = g_oItem.DevolverTodosLosPresupuestos(, , , , 3, , , , IIf(Not frmPROCE.lblPresGrupo.Visible, True, False))
    End Select

    g_bHayPresAnu1 = False
    g_bHayPresAnu2 = False
    g_bHayPres1 = False
    g_bHayPres2 = False
    
    If Not adoRecordset Is Nothing Then
        While Not adoRecordset.EOF
            sUON = NullToStr(adoRecordset("UON1").Value)
            If Not IsNull(adoRecordset("UON2").Value) Then
                sUON = sUON & "-" & adoRecordset("UON2").Value
                If Not IsNull(adoRecordset("UON3").Value) Then
                    sUON = sUON & "-" & adoRecordset("UON3").Value
                End If
            End If
            sPresup = NullToStr(adoRecordset("A").Value)
            If Not IsNull(adoRecordset("P1").Value) Then
                If sPresup <> "" Then
                    sPresup = sPresup & "-" & adoRecordset("P1").Value
                Else
                    sPresup = adoRecordset("P1").Value
                End If
            End If
            If Not IsNull(adoRecordset("P2").Value) Then
                If sPresup <> "" Then
                    sPresup = sPresup & "-" & adoRecordset("P2").Value
                Else
                    sPresup = adoRecordset("P2").Value
                End If
            End If
            If Not IsNull(adoRecordset("P3").Value) Then
                If sPresup <> "" Then
                    sPresup = sPresup & "-" & adoRecordset("P3").Value
                Else
                    sPresup = adoRecordset("P3").Value
                End If
            End If
            If Not IsNull(adoRecordset("P4").Value) Then
                If sPresup <> "" Then
                    sPresup = sPresup & "-" & adoRecordset("P4").Value
                Else
                    sPresup = adoRecordset("P4").Value
                End If
            End If
            sPresup = sPresup & "-" & adoRecordset("DEN").Value
            Select Case adoRecordset("TIPO").Value
                Case 1
                        sTipo = gParametrosGenerales.gsSingPres1
                        iTipo = 1
                        g_bHayPresAnu1 = True

                Case 2
                        sTipo = gParametrosGenerales.gsSingPres2
                        iTipo = 2
                        g_bHayPresAnu2 = True
                
                Case 3
                        sTipo = gParametrosGenerales.gsSingPres3
                        iTipo = 3
                        g_bHayPres1 = True
                
                Case 4
                        sTipo = gParametrosGenerales.gsSingPres4
                        iTipo = 4
                        g_bHayPres2 = True
                        
                Case 5
                        sTipo = gParametrosGenerales.gsSingPres1
                        iTipo = 5
                        g_bHayPresAnu1 = True

                Case 6
                        sTipo = gParametrosGenerales.gsSingPres2
                        iTipo = 6
                        g_bHayPresAnu2 = True
                
                Case 7
                        sTipo = gParametrosGenerales.gsSingPres3
                        iTipo = 7
                        g_bHayPres1 = True
                
                Case 8
                        sTipo = gParametrosGenerales.gsSingPres4
                        iTipo = 8
                        g_bHayPres2 = True
            
            End Select
            sdbgPresupuestos.AddItem sTipo & Chr(m_lSeparador) & sUON & Chr(m_lSeparador) & sPresup & Chr(m_lSeparador) & adoRecordset("PORCEN").Value * 100 & "%" & Chr(m_lSeparador) & iTipo
            adoRecordset.MoveNext
        Wend
        
        adoRecordset.Close
    
    End If
    
    Set adoRecordset = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESItem", "CargarGridPresupuestos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

