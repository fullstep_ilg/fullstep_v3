VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmSELMATProve 
   BackColor       =   &H00808000&
   Caption         =   "DSelección de Material por Proveedor"
   ClientHeight    =   4935
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5235
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELMATProve.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4935
   ScaleWidth      =   5235
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "DCerrar"
      Height          =   315
      Left            =   2625
      TabIndex        =   2
      Top             =   4560
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "DSeleccionar"
      Height          =   315
      Left            =   1440
      TabIndex        =   1
      Top             =   4560
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwEstrMat 
      Height          =   4335
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4995
      _ExtentX        =   8811
      _ExtentY        =   7646
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":0CB2
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":1106
            Key             =   "ACT"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":1218
            Key             =   "ACTASIG"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":132A
            Key             =   "Raiz2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":177C
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":182C
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":18EE
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":199E
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":1A60
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":1B10
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":1BD2
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATProve.frx":1C9A
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSELMATProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4
Public sGMN1Cod, sGMN1Den As String
Public sGMN2Cod, sGMN2Den As String
Public sGMN3Cod, sGMN3Den As String
Public sGMN4Cod, sGMN4Den As String

Public sOrigen As String
Public bRMat As Boolean
Public oProveedor As CProveedor
Private sIdioma() As String
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELMATPROVE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 4)
        For i = 1 To 4
            sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        Me.Caption = sIdioma(1)
        cmdAceptar.Caption = sIdioma(3)
        
        cmdCancelar.Caption = sIdioma(4)
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub
Public Function DevolverCod(ByVal Node As MSComctlLib.Node) As Variant

If Node Is Nothing Then Exit Function

DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)

End Function
Public Function DevolverId(ByVal Node As MSComctlLib.Node) As Variant

If Node Is Nothing Then Exit Function

DevolverId = val(Right(Node.Tag, Len(Node.Tag) - 4))

End Function

Private Sub cmdAceptar_Click()
Dim Node As MSComctlLib.Node

    sGMN1Cod = ""
    sGMN1Den = ""
    sGMN2Cod = ""
    sGMN2Den = ""
    sGMN3Cod = ""
    sGMN3Den = ""
    sGMN4Cod = ""
    sGMN4Den = ""
    
    Set Node = tvwEstrMat.SelectedItem
    
    If Node Is Nothing Or Node.Tag = "Raiz" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    Select Case Left(Node.Tag, 4)
                    
        Case "GMN1"
            sGMN1Cod = DevolverCod(Node)
            sGMN1Den = Mid$(Node.Text, Len(DevolverCod(Node)) + 4, Len(Node.Text) - Len(DevolverCod(Node)))
                                
        Case "GMN2"
            sGMN1Cod = DevolverCod(Node.Parent)
            sGMN1Den = Mid$(Node.Parent.Text, Len(CStr(DevolverCod(Node.Parent))) + 4, Len(Node.Parent.Text) - Len(CStr(DevolverCod(Node.Parent))))
            sGMN2Cod = DevolverCod(Node)
            sGMN2Den = Mid$(Node.Text, Len(CStr(DevolverCod(Node))) + 4, Len(Node.Text) - Len(CStr(DevolverCod(Node))))
            
                            
        Case "GMN3"
            sGMN1Cod = DevolverCod(Node.Parent.Parent)
            sGMN1Den = Mid$(Node.Parent.Parent.Text, Len(CStr(DevolverCod(Node.Parent.Parent))) + 4, Len(Node.Parent.Parent.Text) - Len(CStr(DevolverCod(Node.Parent.Parent))))
            sGMN2Cod = DevolverCod(Node.Parent)
            sGMN2Den = Mid$(Node.Parent.Text, Len(CStr(DevolverCod(Node.Parent))) + 4, Len(Node.Parent.Text) - Len(CStr(DevolverCod(Node.Parent))))
            sGMN3Cod = DevolverCod(Node)
            sGMN3Den = Mid$(Node.Text, Len(CStr(DevolverCod(Node))) + 4, Len(Node.Text) - Len(CStr(DevolverCod(Node))))
        Case "GMN4"
            sGMN1Cod = DevolverCod(Node.Parent.Parent.Parent)
            sGMN1Den = Mid$(Node.Parent.Parent.Parent.Text, Len(CStr(DevolverCod(Node.Parent.Parent.Parent))) + 4, Len(Node.Parent.Parent.Parent.Text) - Len(CStr(DevolverCod(Node.Parent.Parent.Parent))))
            sGMN2Cod = DevolverCod(Node.Parent.Parent)
            sGMN2Den = Mid$(Node.Parent.Parent.Text, Len(CStr(DevolverCod(Node.Parent.Parent))) + 4, Len(Node.Parent.Parent.Text) - Len(CStr(DevolverCod(Node.Parent.Parent))))
            sGMN3Cod = DevolverCod(Node.Parent)
            sGMN3Den = Mid$(Node.Parent.Text, Len(CStr(DevolverCod(Node.Parent))) + 4, Len(Node.Parent.Text) - Len(CStr(DevolverCod(Node.Parent))))
            sGMN4Cod = DevolverCod(Node)
            sGMN4Den = Mid$(Node.Text, Len(CStr(DevolverCod(Node))) + 4, Len(Node.Text) - Len(CStr(DevolverCod(Node))))
        End Select
    
        Screen.MousePointer = vbNormal
        
        If sOrigen = "frmCatalogoDatosExt" Then
            frmCatalogoDatosExt.PonerMatSeleccionado
        End If
        
        If sOrigen = "frmCATALAnyaArt" Then
            frmCATALAnyaArt.sGMN1Cod = sGMN1Cod
            frmCATALAnyaArt.sGMN2Cod = sGMN2Cod
            frmCATALAnyaArt.sGMN3Cod = sGMN3Cod
            frmCATALAnyaArt.sGMN4Cod = sGMN4Cod
            frmCATALAnyaArt.PonerMatProveSeleccionado
        End If
        
        If sOrigen = "frmCATLINBuscar" Then
            frmCATLINBuscar.sGMN1Cod = sGMN1Cod
            frmCATLINBuscar.sGMN2Cod = sGMN2Cod
            frmCATLINBuscar.sGMN3Cod = sGMN3Cod
            frmCATLINBuscar.sGMN4Cod = sGMN4Cod
            frmCATLINBuscar.PonerMatProveSeleccionado
        End If
        
        If sOrigen = "frmLstCatalogo" Then
            frmLstCatalogo.sGMN1Cod = sGMN1Cod
            frmLstCatalogo.sGMN2Cod = sGMN2Cod
            frmLstCatalogo.sGMN3Cod = sGMN3Cod
            frmLstCatalogo.sGMN4Cod = sGMN4Cod
            frmLstCatalogo.PonerMatProveSeleccionado
        End If
        
        Set Node = Nothing
        Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    Me.Height = 5385
    Me.Width = 5355
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    CargarRecursos
    If sOrigen = "frmCatalogoDatosExt" Or sOrigen = "frmCATALAnyaArt" Or sOrigen = "frmCATLINBuscar" Or sOrigen = "frmLstCatalogo" Then
        GenerarEstructuraMat
    End If
End Sub
Private Sub GenerarEstructuraMat()

    Dim oGrupsMN1 As CGruposMatNivel1
    Dim oGrupsMN2 As CGruposMatNivel2
    Dim oGrupsMN3 As CGruposMatNivel3
    Dim oGrupsMN4 As CGruposMatNivel4
    
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    Dim oIMaterialProveedor As IMaterialAsignado
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    
    Dim nodx As MSComctlLib.Node
    
    On Error GoTo Error
    
    tvwEstrMat.Nodes.Clear
    
    
    Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(2), "Raiz")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    Set oIMaterialProveedor = oProveedor
    If bRMat Then
    'tiene restriccion de materiales
        Set oGrupsMN1 = oIMaterialProveedor.DevolverGruposMN1Visibles(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, False, False, False)
        If Not oGrupsMN1 Is Nothing Then
            For Each oGMN1 In oGrupsMN1
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1A")
                nodx.Tag = "GMN1" & oGMN1.Cod ' Con la X1X indicamos que el GMN1 ha sido asignado.
                nodx.Parent.Expanded = True
            Next
        End If
        If gParametrosGenerales.giNEM <= 1 Then
            Exit Sub
        End If
               '************** GruposMN2 ***********
        Set oGrupsMN2 = oIMaterialProveedor.DevolverGruposMN2Visibles(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , False, False, False)
        If Not oGrupsMN2 Is Nothing Then
        
            For Each oGMN2 In oGrupsMN2
                scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN2.GMN1Cod & " - " & oGMN2.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & oGMN2.GMN1Cod
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2A")
                nodx.Tag = "GMN2" & oGMN2.Cod ' Con la X1X indicamos que la GMN2 ha sido asignada directamente.
            Next
        
        End If
        If gParametrosGenerales.giNEM <= 2 Then
            Exit Sub
        End If
         '************** GruposMN3 ***********
        Set oGrupsMN3 = oIMaterialProveedor.DevolverGruposMN3Visibles(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , False, False, False)
        
        If Not oGrupsMN3 Is Nothing Then
            
            For Each oGMN3 In oGrupsMN3
                scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
                scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
                scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN3.GMN1Cod & " - " & oGMN3.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & oGMN3.GMN1Cod  ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN3.GMN2Cod & " - " & oGMN3.GMN2Den, "GMN2")
                nodx.Tag = "GMN2" & oGMN3.GMN2Cod   ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3A")
                nodx.Tag = "GMN3" & oGMN3.Cod ' Con la X1X indicamos que la GMN3 ha sido asignada directamente.
            Next
        
        End If
        
        If gParametrosGenerales.giNEM <= 3 Then
            Exit Sub
        End If
        
        '************** GruposMN4 ***********
        
        Set oGrupsMN4 = oIMaterialProveedor.DevolverGruposMN4Visibles(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, "", "", "", False, False, False)
        
        If Not oGrupsMN4 Is Nothing Then
            
            For Each oGMN4 In oGrupsMN4
                        
                scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
                scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
                scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN4.GMN1Cod & " - " & oGMN4.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & oGMN4.GMN1Cod ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN4.GMN2Cod & " - " & oGMN4.GMN2Den, "GMN2")
                nodx.Tag = "GMN2" & oGMN4.GMN2Cod ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN4.GMN3Cod & " - " & oGMN4.GMN3Den, "GMN3")
                nodx.Tag = "GMN3" & oGMN4.GMN3Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4A")
                nodx.Tag = "GMN4" & oGMN4.Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            Next
            End If
        
    Else
    'no tiene restrición de materiales

        Set oGrupsMN1 = oIMaterialProveedor.DevolverGruposMN1Asignados(, , False, False, False)
        If Not oGrupsMN1 Is Nothing Then
            
            For Each oGMN1 In oGrupsMN1
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1A")
                nodx.Tag = "GMN1" & oGMN1.Cod ' Con la X1X indicamos que el GMN1 ha sido asignado.
                nodx.Parent.Expanded = True
            Next
            
        End If
        
        If gParametrosGenerales.giNEM <= 1 Then
            Exit Sub
        End If
    
        '************** GruposMN2 ***********
        Set oGrupsMN2 = oIMaterialProveedor.DevolverGruposMN2Asignados(, , False, False, False)
        If Not oGrupsMN2 Is Nothing Then
        
            For Each oGMN2 In oGrupsMN2
                scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN2.GMN1Cod & " - " & oGMN2.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & oGMN2.GMN1Cod
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2A")
                nodx.Tag = "GMN2" & oGMN2.Cod ' Con la X1X indicamos que la GMN2 ha sido asignada directamente.
            Next
        
        End If
        If gParametrosGenerales.giNEM <= 2 Then
            Exit Sub
        End If
         '************** GruposMN3 ***********
        Set oGrupsMN3 = oIMaterialProveedor.DevolverGruposMN3Asignados(, , , False, False)
        
        If Not oGrupsMN3 Is Nothing Then
            
            For Each oGMN3 In oGrupsMN3
                scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
                scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
                scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN3.GMN1Cod & " - " & oGMN3.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & oGMN3.GMN1Cod  ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN3.GMN2Cod & " - " & oGMN3.GMN2Den, "GMN2")
                nodx.Tag = "GMN2" & oGMN3.GMN2Cod   ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3A")
                nodx.Tag = "GMN3" & oGMN3.Cod ' Con la X1X indicamos que la GMN3 ha sido asignada directamente.
            Next
        
        End If
        
        If gParametrosGenerales.giNEM <= 3 Then
            Exit Sub
        End If
        
        '************** GruposMN4 ***********
        
        Set oGrupsMN4 = oIMaterialProveedor.DevolverGruposMN4Asignados(, , , False, False)
        
        If Not oGrupsMN4 Is Nothing Then
            
            For Each oGMN4 In oGrupsMN4
                        
                scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
                scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
                scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN4.GMN1Cod & " - " & oGMN4.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & oGMN4.GMN1Cod ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN4.GMN2Cod & " - " & oGMN4.GMN2Den, "GMN2")
                nodx.Tag = "GMN2" & oGMN4.GMN2Cod ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN4.GMN3Cod & " - " & oGMN4.GMN3Den, "GMN3")
                nodx.Tag = "GMN3" & oGMN4.GMN3Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4A")
                nodx.Tag = "GMN4" & oGMN4.Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            Next
        
        End If
    
    End If
    Set oGMN1 = Nothing
    Set oGMN2 = Nothing
    Set oGMN3 = Nothing
    Set oGMN4 = Nothing
    Set oGrupsMN1 = Nothing
    Set oGrupsMN2 = Nothing
    Set oGrupsMN3 = Nothing
    Set oGrupsMN4 = Nothing
    Set nodx = Nothing
    Exit Sub
Error:
    Set nodx = Nothing
    Resume Next
End Sub

Private Sub Form_Resize()
    If Me.Height < 1200 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    tvwEstrMat.Height = Me.Height - 1030
    tvwEstrMat.Width = Me.Width - 360
    cmdAceptar.Top = Me.Height - 825
    cmdCancelar.Top = cmdAceptar.Top
    cmdAceptar.Left = (tvwEstrMat.Width / 2) - (cmdAceptar.Width / 2) - 300
    cmdCancelar.Left = (tvwEstrMat.Width / 2) + 300
End Sub

Private Sub Form_Unload(Cancel As Integer)
Set oGMN1Seleccionado = Nothing
Set oGMN2Seleccionado = Nothing
Set oGMN3Seleccionado = Nothing
Set oGMN4Seleccionado = Nothing
sGMN1Cod = ""
sGMN1Den = ""
sGMN2Cod = ""
sGMN2Den = ""
sGMN3Cod = ""
sGMN3Den = ""
sGMN4Cod = ""
sGMN4Den = ""

sOrigen = ""
bRMat = False
Set oProveedor = Nothing

End Sub
