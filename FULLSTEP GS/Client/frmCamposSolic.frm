VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCamposSolic 
   BackColor       =   &H00808000&
   Caption         =   "DCampos para solicitud de compras"
   ClientHeight    =   5070
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8715
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCamposSolic.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5070
   ScaleWidth      =   8715
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
      Height          =   4520
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   8530
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   4
      DividerType     =   0
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   1852
      Columns(0).Caption=   "SEL"
      Columns(0).Name =   "SEL"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   10610
      Columns(1).Caption=   "DATO"
      Columns(1).Name =   "DATO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).Style=   1
      Columns(1).ButtonsAlways=   -1  'True
      Columns(2).Width=   1588
      Columns(2).Caption=   "AYUDA"
      Columns(2).Name =   "AYUDA"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   4
      Columns(2).ButtonsAlways=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   15046
      _ExtentY        =   7973
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   380
      Left            =   3000
      ScaleHeight     =   375
      ScaleWidth      =   2535
      TabIndex        =   0
      Top             =   4680
      Width           =   2535
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   315
         Left            =   0
         TabIndex        =   2
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   1200
         TabIndex        =   1
         Top             =   0
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmCamposSolic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String
Public g_lTipoCampoPredef As Long
Public g_bConCampos As Boolean 'Indica si el formulario tiene campos en ese grupo al que se a�adira este campo predefinido

Private m_oTipoSolicit As CTipoSolicit
Private m_sTitulo As String

'Private bBuscarArt As Boolean
Private bBuscarPrecioAdj As Boolean
Private bBuscarProvAdj As Boolean
Private bBuscarCantAdj As Boolean
Private bBuscarPiezasDef As Boolean
Private bBuscarImporteRep As Boolean
Private bBuscarSubtipo As Boolean
Private bBuscarEnviosCorrectos As Boolean
Private bBuscarFechaImputacionDefecto As Boolean
Private bBuscarDesgloseActividad As Boolean
Private bBuscarDesglosePedido As Boolean
Private bBuscarComprador As Boolean
Private m_sImposibleAnyadircampo As String
Private m_sYaExisteTotalLineaAdj As String
Private m_sYaExisteTotalLineaPreadj As String
Public g_oDesglose As CFormItem

''' <summary>
''' Evento del boton aceptar. Primero comprueba si se pueden a�adir los campos/atributos y luego los a�ade a frmFormularios
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub cmdAceptar_Click()
    Dim i As Integer
    Dim iId As Integer
    Dim vbm As Variant
    Dim oCampos As CFormItems
    Dim vMaximo As Variant
    Dim vMinimo As Variant
    Dim teserror As TipoErrorSummit
    Dim iIdDesglose As Integer
    Dim oCamposDesglose As CCamposPredef
    Dim oDesglose As CCampoPredef
    Dim arrDesglose() As Variant
    Dim iarrDesglose As Integer
    Dim oLista As CCampoValorLista
    Dim bEncontrado As Boolean
    Dim bMostrarPOPUP As Boolean
    Dim bAnyadido As Boolean
    '**************************************************************
    'FSN_DES_QA_PRT_3253 � Nuevo estado para validar la cumplimentaci�n de certificados.
    '**************************************************************
    If g_lTipoCampoPredef = tiposolicitud.Certificados Then
        For i = 0 To sdbgCampos.Rows - 1
            vbm = sdbgCampos.AddItemBookmark(i)
            If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.ValidadoCertificados Then
                sdbgCampos.Bookmark = vbm
                    If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                        bEncontrado = True
                        Exit For
                    End If
            End If
        Next i
    
        'Buscamos en la pantalla llamante
        If ((g_sOrigen = "frmFormularios") And bEncontrado) Then
            bEncontrado = False
            
            Dim TagSeleccionadoPreComprobacion As Variant
            Dim i_grupos As Integer
            TagSeleccionadoPreComprobacion = frmFormularios.ssTabGrupos.selectedItem
            
            'Recorrer todos los campos de cada grupo buscando un campo de tipo "Validado"
            For i_grupos = 1 To (frmFormularios.ssTabGrupos.Tabs.Count)
                'g_oGrupoSeleccionado = g_oFormSeleccionado.Grupos.Item(CStr(frmFormularios.ssTabGrupos.Tabs(i_grupos).Tag))
                Set frmFormularios.ssTabGrupos.selectedItem = frmFormularios.ssTabGrupos.Tabs(i_grupos)
            For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                    If frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.ValidadoCertificados Then
                        bEncontrado = True
                        Exit For
                    End If
            Next i
            Next i_grupos
            
            'Restaurar el grupo seleccionado antes de realizar la comprobaci�n.
            For i_grupos = 1 To (frmFormularios.ssTabGrupos.Tabs.Count)
                If (frmFormularios.ssTabGrupos.Tabs(i_grupos).caption = TagSeleccionadoPreComprobacion) Then
                    Set frmFormularios.ssTabGrupos.selectedItem = frmFormularios.ssTabGrupos.Tabs(i_grupos)
                End If
            Next i_grupos
                
            If bEncontrado Then
                oMensajes.MensajeOKOnly 1363   'MOD 201 - ID 1363 - �Imposible a�adir el campo! Ya existe un campo de tipo Valido
                Exit Sub
            End If
        End If
    End If
    '**************************************************************
    
    If bBuscarPrecioAdj Or bBuscarProvAdj Or bBuscarCantAdj Or bBuscarPiezasDef Or bBuscarImporteRep Or bBuscarEnviosCorrectos Or bBuscarFechaImputacionDefecto Then
        bEncontrado = False
        
        'Buscamos en la propia pantalla
        For i = 0 To sdbgCampos.Rows - 1
            vbm = sdbgCampos.AddItemBookmark(i)
            If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.CodArticulo Or _
                m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.NuevoCodArticulo Then
                sdbgCampos.Bookmark = vbm
                If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                    bEncontrado = True
                    Exit For
                End If
            End If
        Next i

        'Buscamos en la pantalla llamante
        If g_sOrigen = "frmFormularios" Then
            For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo Or _
                    frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        ElseIf g_sOrigen = "frmDesglose" Then
            For i = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo Or _
                    frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        End If
        
        If Not bEncontrado Then
            oMensajes.MensajeOKOnly 898   '�Imposible a�adir el campo! No ha seleccionado ning�n campo de tipo Art�culo
            Exit Sub
        End If
    End If
    
    If bBuscarPrecioAdj Then
        bEncontrado = False
        If g_sOrigen = "frmFormularios" Then
            For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoSC.PrecioUnitarioAdj Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        ElseIf g_sOrigen = "frmDesglose" Then
            For i = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoSC.PrecioUnitarioAdj Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        End If
        
        If bEncontrado Then
            oMensajes.MensajeOKOnly 911   '�Imposible a�adir el campo! Ya existe un campo de tipo Precio Unitario Adjudicado
            Exit Sub
        End If
    End If
    
    If bBuscarProvAdj Then
        bEncontrado = False
        If g_sOrigen = "frmFormularios" Then
            For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoSC.ProveedorAdj Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        ElseIf g_sOrigen = "frmDesglose" Then
            For i = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoSC.ProveedorAdj Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        End If
        
        If bEncontrado Then
            oMensajes.MensajeOKOnly 912   '�Imposible a�adir el campo! Ya existe un campo de tipo Proveedor Adjudicado
            Exit Sub
        End If
    End If
    
    If bBuscarCantAdj Then
        bEncontrado = False
        If g_sOrigen = "frmFormularios" Then
            For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoSC.CantidadAdj Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        ElseIf g_sOrigen = "frmDesglose" Then
            For i = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoSC.CantidadAdj Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        End If
        
        If bEncontrado Then
            oMensajes.MensajeOKOnly 913   '�Imposible a�adir el campo! Ya existe un campo de tipo Cantidad Adjudicada
            Exit Sub
        End If
    End If
    
    If bBuscarPiezasDef Then
        bEncontrado = False
        If g_sOrigen = "frmFormularios" Then
            For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoNoConformidad.PiezasDefectuosas Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        ElseIf g_sOrigen = "frmDesglose" Then
            For i = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoNoConformidad.PiezasDefectuosas Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        End If
        
        If bEncontrado Then
            oMensajes.MensajeOKOnly 1018
            Exit Sub
        End If
    End If
    
    If bBuscarImporteRep Then
        bEncontrado = False
        If g_sOrigen = "frmFormularios" Then
            For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoNoConformidad.ImporteRepercutido Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        ElseIf g_sOrigen = "frmDesglose" Then
            For i = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoNoConformidad.ImporteRepercutido Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        End If
        
        If bEncontrado Then
            oMensajes.MensajeOKOnly 1019
            Exit Sub
        End If
    End If
    
    If bBuscarSubtipo Then
        If g_sOrigen = "frmFormularios" Then
            If frmFormularios.g_oFormSeleccionado.ExisteCampoSubtipo Then
                oMensajes.MensajeOKOnly 1032
                Exit Sub
            End If
        End If
    End If
    
    If bBuscarDesgloseActividad Then
        If g_sOrigen = "frmFormularios" Then
            If frmFormularios.g_oFormSeleccionado.ExisteCampoDesgloseActividad Then
                oMensajes.MensajeOKOnly 1209
                Exit Sub
            End If
        End If
    End If
    If bBuscarDesglosePedido Then
        If g_sOrigen = "frmFormularios" Then
            If g_bConCampos Then
                oMensajes.ImposibleAnyadirCampoGrupo (2)
                Exit Sub
            End If
        End If
    End If
    
    If bBuscarEnviosCorrectos Then
        bEncontrado = False
        If g_sOrigen = "frmFormularios" Then
            For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoNoConformidad.EnviosCorrectos Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        ElseIf g_sOrigen = "frmDesglose" Then
            For i = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoNoConformidad.EnviosCorrectos Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        End If
        
        If bEncontrado Then
            oMensajes.MensajeOKOnly 1035
            Exit Sub
        End If
    End If
    
    If bBuscarFechaImputacionDefecto Then
        bEncontrado = False
        If g_sOrigen = "frmFormularios" Then
            For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoNoConformidad.FechaImputacionDefecto Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        ElseIf g_sOrigen = "frmDesglose" Then
            For i = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(i)
                If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoNoConformidad.FechaImputacionDefecto Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
        End If
        
        If bEncontrado Then
            oMensajes.MensajeOKOnly 1185
            Exit Sub
        End If
    End If
    
    If g_lTipoCampoPredef = tiposolicitud.Contrato Then
        bEncontrado = False
        'Renovaci�n autom�tica
        'Buscamos en la propia pantalla
        For i = 0 To sdbgCampos.Rows - 1
            vbm = sdbgCampos.AddItemBookmark(i)
            If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.RenovacionAutomatica Then
                sdbgCampos.Bookmark = vbm
                If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                    bEncontrado = True
                    Exit For
                End If
            End If
        Next i
        If bEncontrado Then
            If g_sOrigen = "frmFormularios" Then
                For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                    vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                    If frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.RenovacionAutomatica Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
            ElseIf g_sOrigen = "frmDesglose" Then
                For i = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                    vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(i)
                    If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.RenovacionAutomatica Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
            End If
        End If
        If bEncontrado Then
            oMensajes.MensajeOKOnly 1483   '�Imposible a�adir el campo! Ya existe un campo de tipo Renovaci�n autom�tica
            Exit Sub
        End If
        
        'Periodo de renovaci�n
        For i = 0 To sdbgCampos.Rows - 1
            vbm = sdbgCampos.AddItemBookmark(i)
            If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.PeriodoRenovacion Then
                sdbgCampos.Bookmark = vbm
                If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                    bEncontrado = True
                    Exit For
                End If
            End If
        Next i
        If bEncontrado Then
            If g_sOrigen = "frmFormularios" Then
                For i = 0 To frmFormularios.sdbgCampos.Rows - 1
                    vbm = frmFormularios.sdbgCampos.AddItemBookmark(i)
                    If frmFormularios.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.PeriodoRenovacion Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
            ElseIf g_sOrigen = "frmDesglose" Then
                For i = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                    vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(i)
                    If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.PeriodoRenovacion Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
            End If
        End If
        If bEncontrado Then
            oMensajes.MensajeOKOnly 1484   '�Imposible a�adir el campo! Ya existe un campo de tipo Per�odo de renovaci�n
            Exit Sub
        End If
    End If
    
    'A�ade los campos seleccionados al formulario:
    Set oCampos = oFSGSRaiz.Generar_CFormCampos
    
    sdbgCampos.MoveFirst
    iId = 1
    
    For i = 0 To sdbgCampos.Rows - 1
        bAnyadido = True
        vbm = sdbgCampos.AddItemBookmark(i)
        If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = True Then
            If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Tipo = TipoFecha Then
                vMaximo = m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).MaxFec
                vMinimo = m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).MinFec
            ElseIf m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Tipo = TipoNumerico Then
                vMaximo = m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).MaxNum
                vMinimo = m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).MinNum
            Else
                vMaximo = Null
                vMinimo = Null
            End If
            
            If g_sOrigen = "frmFormularios" Then
                If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.Desglose Then
                    bMostrarPOPUP = True
                Else
                    bMostrarPOPUP = False
                End If
                oCampos.Add iId, frmFormularios.g_oGrupoSeleccionado, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Denominaciones, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Ayudas, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoPredef, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Tipo, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoIntroduccion, , , , , vMinimo, vMaximo, , m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).idAtrib, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS, , , , , , , , , , , , , , , , , bMostrarPOPUP
            Else
                oCampos.Add iId, frmFormularios.g_oGrupoSeleccionado, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Denominaciones, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Ayudas, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoPredef, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Tipo, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoIntroduccion, , , , , vMinimo, vMaximo, , m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).idAtrib, m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS, , , frmFormularios.g_ofrmDesglose.g_oCampoDesglose, True
            End If
            
            'Si tiene valores de lista
            m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).CargarValoresLista
            If Not m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).ValoresLista Is Nothing Then
                If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).ValoresLista.Count > 0 Then
                    Set oCampos.Item(iId).ValoresLista = oFSGSRaiz.Generar_CCampoValorListas
                    For Each oLista In m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).ValoresLista
                        oCampos.Item(iId).ValoresLista.Add oLista.Orden, oCampos.Item(iId), oLista.valorNum, oLista.valorText, oLista.valorFec
                    Next
                End If
            End If
            
            iId = iId + 1
            
            'Si se a�ade un desglose de material por defecto se le a�ade su estructura por defecto:
            If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Tipo = TiposDeAtributos.TipoDesglose Then
                iIdDesglose = iId - 1
                Set oCamposDesglose = oFSGSRaiz.Generar_CCamposPredef
                oCamposDesglose.CargarTodosLosCamposDesglose m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Id
                
                For Each oDesglose In oCamposDesglose
                    If oDesglose.Tipo = TipoFecha Then
                        vMaximo = oDesglose.MaxFec
                        vMinimo = oDesglose.MinFec
                    ElseIf oDesglose.Tipo = TipoNumerico Then
                        vMaximo = oDesglose.MaxNum
                        vMinimo = oDesglose.MinNum
                    Else
                        vMaximo = Null
                        vMinimo = Null
                    End If
                    
                    oCampos.Add iId, frmFormularios.g_oGrupoSeleccionado, oDesglose.Denominaciones, oDesglose.Ayudas, oDesglose.TipoPredef, oDesglose.Tipo, oDesglose.TipoIntroduccion, , , , , vMinimo, vMaximo, , oDesglose.idAtrib, oDesglose.TipoCampoGS, , , oCampos.Item(iIdDesglose), True
                    iarrDesglose = iarrDesglose + 1
                    ReDim Preserve arrDesglose(iarrDesglose)
                    arrDesglose(iarrDesglose) = iId
                    
                    iId = iId + 1
                Next
                Set oCamposDesglose = Nothing
            End If
        End If
        
    Next i
        
    If oCampos.Count > 0 Then
        teserror = oCampos.AnyadirCamposDeGSoAtributos
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            Exit Sub
        End If
        
        For i = 1 To iarrDesglose  'Elimina de la colecci�n los subcampos del campo desglose
            oCampos.Remove (CStr(arrDesglose(i)))
        Next i
        
        If g_sOrigen = "frmFormularios" Then
            'Carga los nuevos campos en la grid y en la colecci�n:
            frmFormularios.AnyadirCampos oCampos
        Else
            frmFormularios.g_ofrmDesglose.AnyadirCampos oCampos
        End If
    End If
    
    Set oCampos = Nothing
    
    If g_sOrigen = "frmFormularios" Then
        frmFormularios.g_Accion = ACCFormularioCons
    Else
        frmFormularios.g_ofrmDesglose.g_Accion = ACCDesgloseCons
    End If
    
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub
''' <summary>
''' Carga la pagina
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub Form_Load()
    
    Me.Height = 5580
    Me.Width = 8835
    

    bBuscarPrecioAdj = False
    bBuscarProvAdj = False
    bBuscarCantAdj = False
    bBuscarPiezasDef = False
    bBuscarImporteRep = False
    bBuscarSubtipo = False
    bBuscarEnviosCorrectos = False
    bBuscarFechaImputacionDefecto = False
    bBuscarDesgloseActividad = False
    bBuscarComprador = False
    
    CargarRecursos
    
    Set m_oTipoSolicit = oFSGSRaiz.Generar_CTipoSolicit
    m_oTipoSolicit.Id = g_lTipoCampoPredef
    
    Me.caption = m_sTitulo & " " & m_oTipoSolicit.CargarDenominacionTipo
    
    PonerFieldSeparator Me
    
    CargarCampos
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAMPO_SOLICIT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        m_sTitulo = Ador(0).Value   '1 Campos de tipo solicitud:
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value   '2 &Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '3 &Cancelar
        Ador.MoveNext
        sdbgCampos.Columns("SEL").caption = Ador(0).Value  '4 Seleccionar
        Ador.MoveNext
        sdbgCampos.Columns("DATO").caption = Ador(0).Value  '5 Dato
        Ador.MoveNext
        sdbgCampos.Columns("AYUDA").caption = Ador(0).Value  '6 Ayuda
        Ador.MoveNext
        m_sImposibleAnyadircampo = Ador(0).Value
        Ador.MoveNext
        m_sYaExisteTotalLineaAdj = Ador(0).Value
        Ador.MoveNext
        m_sYaExisteTotalLineaPreadj = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub


''' <summary>
''' Carga los campos del tipo de solicitud.
''' Si se van a seleccionar subcampos de desglose de material no muestra algunos campos como: el desglose de material,
''' importe solicitudes vinculadas, referencia a solicitud y tipo de pedido
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0</remarks>
Private Sub CargarCampos()
    Dim oCampo As CCampoPredef
    
    m_oTipoSolicit.CargarCamposTipo , , g_sOrigen = "frmDesglose"
    
    For Each oCampo In m_oTipoSolicit.Campos
        If g_sOrigen = "frmDesglose" And (oCampo.TipoCampoGS = TipoCampoGS.Desglose Or oCampo.Tipo = TiposDeAtributos.TipoDesglose Or oCampo.TipoCampoGS = TipoCampoGS.ImporteSolicitudesVinculadas Or oCampo.TipoCampoGS = TipoCampoGS.RefSolicitud Or oCampo.TipoCampoGS = TipoCampoGS.TipoPedido Or oCampo.TipoCampoGS = TipoCampoNoConformidad.Subtipo Or oCampo.TipoCampoGS = TipoCampoGS.RetencionEnGarantia Or oCampo.TipoCampoGS = TipoCampoGS.CodComprador) Then
        Else
            sdbgCampos.AddItem "" & Chr(m_lSeparador) & oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCampo.Id
        End If
    Next
    
    sdbgCampos.MoveFirst
End Sub

Private Sub Form_Resize()
    If Me.Width < 500 Then Exit Sub
    If Me.Height < 1500 Then Exit Sub
    
    sdbgCampos.Height = Me.Height - 1060
    sdbgCampos.Width = Me.Width - 305
    
    sdbgCampos.Columns("SEL").Width = sdbgCampos.Width / 9
    sdbgCampos.Columns("DATO").Width = sdbgCampos.Width / 1.4
    sdbgCampos.Columns("AYUDA").Width = sdbgCampos.Width / 10
    
    picEdit.Top = sdbgCampos.Top + sdbgCampos.Height + 65
    picEdit.Left = Me.Width / 3
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oTipoSolicit = Nothing
    Set g_oDesglose = Nothing
    Me.Visible = False
End Sub

Private Sub sdbgCampos_BtnClick()

    If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)) Is Nothing Then Exit Sub
    
    If sdbgCampos.Columns(sdbgCampos.Col).Name = "DATO" Then
        'Detalle de campo:
        If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS <> "" And Not IsNull(m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS) Then
            'Es un campo predefinido de GS:
            frmDetalleCampoPredef.g_iTipo = m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo
            frmDetalleCampoPredef.g_sDenCampo = m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            frmDetalleCampoPredef.g_iTipoCampo = m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS
            frmDetalleCampoPredef.g_iTipoSolicit = m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef
            frmDetalleCampoPredef.Show vbModal
            
        Else
            'Es un campo normal o un atributo de GS:
            If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo = TiposDeAtributos.TipoDesglose Then
            Else
                frmDetalleCampos.g_sOrigen = "frmCamposSolic"
                frmDetalleCampos.g_bModif = False
                Set frmDetalleCampos.g_oIdiomas = frmFormularios.m_oIdiomas
                Set frmDetalleCampos.g_oCampoPredef = m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                frmDetalleCampos.Show vbModal
            End If
        End If
                    
    ElseIf sdbgCampos.Columns(sdbgCampos.Col).Name = "AYUDA" Then
        'Muestra el formulario con la ayuda:
        MostrarFormFormCampoAyuda oGestorIdiomas, gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, oGestorSeguridad, oUsuarioSummit.Cod, gParametrosGenerales, _
                FormCampoAyudaTipoAyuda.CampoFormulario, False, , , m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
    End If
End Sub

''' <summary>
''' Evento del grid sdbgCampos responde al cambio del check. Se selecciona/deselecciona campos relacionados al q ha
''' producido el evento � marca/desmarca variable de formulario para control de que puede ir y q no en cmdAceptar_click
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub sdbgCampos_Change()
    Dim vbm As Variant
    Dim i As Integer
    Dim bEncontrado As Boolean
    Dim oCampo As CFormItem
    Dim oGrupo As CFormGrupo

    bEncontrado = False
    If sdbgCampos.Columns(sdbgCampos.Col).Name <> "SEL" Then Exit Sub
    
    Select Case m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS
    
        Case TipoCampoGS.provincia
            'Si seleccionamos una provincia seleccionamos tambi�n el pa�s (si no estaba ya seleccionado)
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                For i = 0 To sdbgCampos.Rows - 1
                    vbm = sdbgCampos.AddItemBookmark(i)
                    If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.Pais Then
                        If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = False Then
                            sdbgCampos.Bookmark = vbm
                            sdbgCampos.Columns("SEL").Value = "1"
                        End If
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                
                'si no hay campo pa�s no se selecciona la provincia:
                If bEncontrado = False Then
                    sdbgCampos.Columns("SEL").Value = "0"
                    oMensajes.ImposibleSeleccionarProv
                End If
            End If
            
        Case TipoCampoGS.Pais
            'Si deseleccionamos un pa�s le quitamos el check tambi�n a la provincia
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = False Then
                For i = 0 To sdbgCampos.Rows - 1
                    vbm = sdbgCampos.AddItemBookmark(i)
                    If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.provincia Then
                        If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = True Then
                            sdbgCampos.Bookmark = vbm
                            sdbgCampos.Columns("SEL").Value = "0"
                        End If
                        Exit For
                    End If
                Next i
            End If
            
        Case TipoCampoGS.CodArticulo
            'Si seleccionamos una provincia seleccionamos tambi�n el pa�s (si no estaba ya seleccionado)
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                For i = 0 To sdbgCampos.Rows - 1
                    vbm = sdbgCampos.AddItemBookmark(i)
                    If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.material Then
                        If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = False Then
                            sdbgCampos.Bookmark = vbm
                            sdbgCampos.Columns("SEL").Value = "1"
                        End If
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                
                'si no hay campo material no se selecciona el art�culo:
                If bEncontrado = False Then
                    sdbgCampos.Columns("SEL").Value = "0"
                    oMensajes.ImposibleSeleccionarArticulo
                End If
            End If
            
        Case TipoCampoGS.NuevoCodArticulo
            'Si seleccionamos el Articulo seleccionamos tambi�n el Material (si no estaba ya seleccionado)
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                    Dim bSeleccionado As Boolean
                    vbm = sdbgCampos.GetBookmark(-1)
                    If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.material Then
                        If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = False Then
                            sdbgCampos.Bookmark = vbm
                            sdbgCampos.Columns("SEL").Value = "1"
                            bSeleccionado = True
                        End If
                        bEncontrado = True
                    End If
                    If bSeleccionado Then
                        vbm = sdbgCampos.GetBookmark(2)
                    Else
                        vbm = sdbgCampos.GetBookmark(1)
                    End If
                    
                    If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.DenArticulo Then
                        If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = False Then
                            sdbgCampos.Bookmark = vbm
                            sdbgCampos.Columns("SEL").Value = "1"
                        End If
                        bEncontrado = True

                    End If
                
                'si no hay campo material no se selecciona el art�culo:
                If bEncontrado = False Then
                    sdbgCampos.Columns("SEL").Value = "0"
                    oMensajes.ImposibleSeleccionarArticulo
                End If
            Else
                    vbm = sdbgCampos.GetBookmark(1)
                    If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.DenArticulo Then
                        If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = True Then
                            sdbgCampos.Bookmark = vbm
                            sdbgCampos.Columns("SEL").Value = "0"
                        End If
                        bEncontrado = True
                        'Exit For
                    End If
            End If
          
        Case TipoCampoGS.DenArticulo
            'Si seleccionamos la denominacion del articulo seleccionamos tambi�n el Codigo y el Material (si no estaba ya seleccionado)
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                    vbm = sdbgCampos.GetBookmark(-2)
                    If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.material Then
                        If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = False Then
                            sdbgCampos.Bookmark = vbm
                            sdbgCampos.Columns("SEL").Value = "1"
                            bSeleccionado = True
                        End If
                        bEncontrado = True
                    End If
                    If bSeleccionado Then
                        vbm = sdbgCampos.GetBookmark(1)
                    Else
                        vbm = sdbgCampos.GetBookmark(-1)
                    End If
                    If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.NuevoCodArticulo Then
                        If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = False Then
                            sdbgCampos.Bookmark = vbm
                            sdbgCampos.Columns("SEL").Value = "1"
                        End If
                        bEncontrado = True
                    End If
                
                'si no hay campo material no se selecciona el art�culo:
                If bEncontrado = False Then
                    sdbgCampos.Columns("SEL").Value = "0"
                    oMensajes.ImposibleSeleccionarArticulo
                End If
            Else
                'Si desmarcarmos la denominacion, desmarcamos tambien el Cod.Articulo(Nuevo)
                vbm = sdbgCampos.GetBookmark(-1)
                If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.NuevoCodArticulo Then
                    If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = True Then
                        sdbgCampos.Bookmark = vbm
                        sdbgCampos.Columns("SEL").Value = "0"
                        bSeleccionado = True
                    End If
                    bEncontrado = True
                End If
                
            End If
          
        Case TipoCampoGS.material
            'Si deseleccionamos el material le quitamos el check tambi�n al art�culo:
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = False Then
                vbm = sdbgCampos.GetBookmark(1)
                If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.CodArticulo Or m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.NuevoCodArticulo Then
                    If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = True Then
                        sdbgCampos.Bookmark = vbm
                        sdbgCampos.Columns("SEL").Value = "0"
                    End If

                End If
                
                vbm = sdbgCampos.GetBookmark(1)
                
                If m_oTipoSolicit.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.DenArticulo Then
                    If GridCheckToBoolean(sdbgCampos.Columns("SEL").CellValue(vbm)) = True Then
                        sdbgCampos.Bookmark = vbm
                        sdbgCampos.Columns("SEL").Value = "0"
                    End If
                End If
            End If
    
        Case TipoCampoGS.Desglose
            'Si es el m�dulo b�sico (s�lo solicitudes de compras) y ya existe un campo desglose en el grupo no dejar� seleccionar otro
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                If g_sOrigen = "frmFormularios" And basParametros.gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then
                    For Each oCampo In frmFormularios.g_oGrupoSeleccionado.Campos
                        If oCampo.CampoGS = TipoCampoGS.Desglose Then
                            oMensajes.SoloUnCampoDesglosePorGrupo
                            sdbgCampos.Columns("SEL").Value = "0"
                            Exit For
                        End If
                    Next
                End If
            End If
            
        Case TipoCampoSC.PrecioUnitarioAdj
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
'                bBuscarArt = True
                bBuscarPrecioAdj = True
            Else
'                bBuscarArt = False
                bBuscarPrecioAdj = False
            End If

        Case TipoCampoSC.ProveedorAdj
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
'                bBuscarArt = True
                bBuscarProvAdj = True
            Else
'                bBuscarArt = False
                bBuscarProvAdj = False
            End If

        Case TipoCampoSC.CantidadAdj
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
'                bBuscarArt = True
                bBuscarCantAdj = True
            Else
'                bBuscarArt = False
                bBuscarCantAdj = False
            End If
        Case TipoCampoNoConformidad.PiezasDefectuosas
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                bBuscarPiezasDef = True
            Else
                bBuscarPiezasDef = False
            End If
        Case TipoCampoNoConformidad.ImporteRepercutido
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                bBuscarImporteRep = True
            Else
                bBuscarImporteRep = False
            End If
        Case TipoCampoNoConformidad.Subtipo
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                bBuscarSubtipo = True
            Else
                bBuscarSubtipo = False
            End If
        Case TipoCampoNoConformidad.EnviosCorrectos
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                bBuscarEnviosCorrectos = True
            Else
                bBuscarEnviosCorrectos = False
            End If
        Case TipoCampoNoConformidad.FechaImputacionDefecto
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                bBuscarFechaImputacionDefecto = True
            Else
                bBuscarFechaImputacionDefecto = False
            End If
        Case TipoCampoGS.DesgloseActividad
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                bBuscarDesgloseActividad = True
            Else
                bBuscarDesgloseActividad = False
            End If
        Case TipoCampoGS.DesgloseDePedido
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                bBuscarDesglosePedido = True
            Else
                bBuscarDesglosePedido = False
            End If
        Case TipoCampoGS.CodComprador
            If bBuscarComprador = True Then
            'Si est� a true ya hay un campo seleccionado y no podr� seleccionar m�s
                If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                    sdbgCampos.Columns("SEL").Value = "0"
                    oMensajes.ExisteComprador   '�Imposible a�adir el campo! Ya existe un campo de tipo Comprador
                ElseIf GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = False Then
                    'Si descheckeo el campo comprador pongo a false la variable para poder volver a seleccionarlo
                    bBuscarComprador = False
                End If
            Else
                If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                'Si ya hay un campo de tipo comprador en la solicitud no me dejar� seleccionarlo
                    If g_sOrigen = "frmFormularios" Then
                        For Each oGrupo In frmFormularios.g_oFormSeleccionado.Grupos
                            If Not oGrupo.Campos Is Nothing Then
                                For Each oCampo In oGrupo.Campos
                                    If oCampo.CampoGS = TipoCampoGS.CodComprador Then
                                        oMensajes.ExisteComprador   '�Imposible a�adir el campo! Ya existe un campo de tipo Comprador
                                        sdbgCampos.Columns("SEL").Value = "0"
                                        Exit For
                                    End If
                                Next
                            End If
                        Next
                    End If
                End If
                If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                    bBuscarComprador = True
                Else
                    bBuscarComprador = False
                End If
            End If
        Case TipoCampoSC.TotalLineaAdj
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                If g_oDesglose.Desglose.ExisteCampoGS(TipoCampoSC.TotalLineaAdj) Then
                    oMensajes.mensajeGenericoOkOnly (m_sImposibleAnyadircampo & vbCrLf & m_sYaExisteTotalLineaAdj)
                    sdbgCampos.CancelUpdate
                End If
            End If
        Case TipoCampoSC.TotalLineapreAdj
            If GridCheckToBoolean(sdbgCampos.Columns("SEL").Value) = True Then
                If g_oDesglose.Desglose.ExisteCampoGS(TipoCampoSC.TotalLineapreAdj) Then
                    oMensajes.mensajeGenericoOkOnly (m_sImposibleAnyadircampo & vbCrLf & m_sYaExisteTotalLineaPreadj)
                    sdbgCampos.CancelUpdate
                End If
            End If
    End Select
End Sub
