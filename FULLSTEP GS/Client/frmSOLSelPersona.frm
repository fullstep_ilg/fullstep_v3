VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSOLSelPersona 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DSeleccione peticionario"
   ClientHeight    =   4500
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5550
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSOLSelPersona.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   5550
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   5550
      TabIndex        =   2
      Top             =   4080
      Width           =   5550
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   2955
         TabIndex        =   4
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   1755
         TabIndex        =   3
         Top             =   30
         Width           =   1005
      End
   End
   Begin VB.PictureBox picEstrOrg 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   4095
      Left            =   45
      ScaleHeight     =   4095
      ScaleWidth      =   5565
      TabIndex        =   0
      Top             =   0
      Width           =   5565
      Begin VB.CommandButton cmdBuscarCodigo 
         Height          =   295
         Left            =   4020
         Picture         =   "frmSOLSelPersona.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   90
         Width           =   335
      End
      Begin VB.TextBox tbBuscarCodigo 
         Height          =   285
         Left            =   1930
         TabIndex        =   6
         Top             =   90
         Width           =   1980
      End
      Begin MSComctlLib.TreeView tvwEstrOrg 
         Height          =   3510
         Left            =   30
         TabIndex        =   1
         Top             =   480
         Width           =   5400
         _ExtentX        =   9525
         _ExtentY        =   6191
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblBusqueda 
         BackColor       =   &H00808000&
         Caption         =   "B�squeda por c�digo :"
         ForeColor       =   &H8000000E&
         Height          =   285
         Left            =   210
         TabIndex        =   5
         Top             =   120
         Width           =   1665
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersona.frx":0D03
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersona.frx":10CB
            Key             =   "Persona"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersona.frx":11D5
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersona.frx":1529
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersona.frx":187D
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersona.frx":1BD1
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersona.frx":1F65
            Key             =   "PerUsu"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersona.frx":206F
            Key             =   "PersonaBaja"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersona.frx":20FA
            Key             =   "PerUsuBaja"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSOLSelPersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables para interactuar con otros forms
Public g_sOrigen As String
Public bRUO As Boolean
Public bRDep As Boolean
Public bAllowSelUON As Boolean
Public bAllowSelUON0 As Boolean 'Nos indicar� si se puede seleccionar el UON0 (toda la organizaci�n)
Public g_sPer As String
Public g_sUON0 As String
Public g_sUON1 As String
Public g_sUON2 As String
Public g_sUON3 As String
Public g_sDEP As String
Public g_sEmpresas As String
Public g_oTipoAprobador As TipoAprobadorSolicitudPedido
Public g_sAprobadoroPeticionario As String

' Variable de control de flujo
Public Accion As accionessummit

'Variables privadas
Private m_sTituloSolConf As String
Private m_sTituloUsuarios As String
Private m_sTituloPersona As String
Private m_sTituloSolNotif As String

''' Revisado por: blp. Fecha: 23/07/2012
''' <summary>
''' Validaciones que se efect�an al pulsar aceptar en el formulario para permitir o no la selecci�n efectuada
''' </summary>
''' <remarks>Llamada desde:evento click del bot�n; Tiempo m�ximo;0,3seg.</remarks>
Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim oPer As CPersona

    Set nodx = tvwEstrOrg.selectedItem
    
    If nodx Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            scod1 = DevolverCod(nodx)
            
            With frmSOLConfiguracion
            Select Case nodx.Image
                Case "UON1"
                    'a�ade una nueva fila a la grid:
                    If (.m_TipoContactoSolicitado = 1) Then
                        .g_udtAccion = accionessummit.ACCSolConfAnyaPet
                    ElseIf (.m_TipoContactoSolicitado = 2) Then
                        .g_udtAccion = accionessummit.ACCSolConfAnyaNotif
                    End If
                    .picEdicion.Visible = True
                    .picNavigate.Visible = False
                    .ssSOLConfig.Bands(.m_TipoContactoSolicitado).AddNew  'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
                    
                    .ssSOLConfig.ActiveRow.Cells("UON1").Value = scod1
                    .ssSOLConfig.ActiveRow.Cells("UON2").Value = ""
                    .ssSOLConfig.ActiveRow.Cells("UON3").Value = ""
                    .ssSOLConfig.ActiveRow.Cells("UON1DEN").Value = DevolverDen(nodx)
        
                Case "UON2"
                    'a�ade una nueva fila a la grid:
                    If (.m_TipoContactoSolicitado = 1) Then
                        .g_udtAccion = accionessummit.ACCSolConfAnyaPet
                    ElseIf (.m_TipoContactoSolicitado = 2) Then
                        .g_udtAccion = accionessummit.ACCSolConfAnyaNotif
                    End If
                    .picEdicion.Visible = True
                    .picNavigate.Visible = False
                    .ssSOLConfig.Bands(.m_TipoContactoSolicitado).AddNew  'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
                    
                    .ssSOLConfig.ActiveRow.Cells("UON1").Value = DevolverCod(nodx.Parent)
                    .ssSOLConfig.ActiveRow.Cells("UON2").Value = scod1
                    .ssSOLConfig.ActiveRow.Cells("UON3").Value = ""
                    .ssSOLConfig.ActiveRow.Cells("UON2DEN").Value = DevolverDen(nodx)
        
                Case "UON3"
                    'a�ade una nueva fila a la grid:
                    If (.m_TipoContactoSolicitado = 1) Then
                        .g_udtAccion = accionessummit.ACCSolConfAnyaPet
                    ElseIf (.m_TipoContactoSolicitado = 2) Then
                        .g_udtAccion = accionessummit.ACCSolConfAnyaNotif
                    End If
                    .picEdicion.Visible = True
                    .picNavigate.Visible = False
                    .ssSOLConfig.Bands(.m_TipoContactoSolicitado).AddNew  'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.

                    .ssSOLConfig.ActiveRow.Cells("UON1").Value = DevolverCod(nodx.Parent.Parent)
                    .ssSOLConfig.ActiveRow.Cells("UON2").Value = DevolverCod(nodx.Parent)
                    .ssSOLConfig.ActiveRow.Cells("UON3").Value = scod1
                    .ssSOLConfig.ActiveRow.Cells("UON3DEN").Value = DevolverDen(nodx)
        
                Case "Departamento"
                    'a�ade una nueva fila a la grid:
                    If (.m_TipoContactoSolicitado = 1) Then
                        .g_udtAccion = accionessummit.ACCSolConfAnyaPet
                    ElseIf (.m_TipoContactoSolicitado = 2) Then
                        .g_udtAccion = accionessummit.ACCSolConfAnyaNotif
                    End If
                    .picEdicion.Visible = True
                    .picNavigate.Visible = False
                    .ssSOLConfig.Bands(.m_TipoContactoSolicitado).AddNew  'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
                    
                    Select Case Left(nodx.Tag, 4)
                        Case "DEP0"
                            .ssSOLConfig.ActiveRow.Cells("UON1").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON2").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON3").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON1DEN").Value = ""
                        Case "DEP1"
                            .ssSOLConfig.ActiveRow.Cells("UON1").Value = DevolverCod(nodx.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON2").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON3").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON1DEN").Value = DevolverDen(nodx.Parent)
                        Case "DEP2"
                            .ssSOLConfig.ActiveRow.Cells("UON1").Value = DevolverCod(nodx.Parent.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON2").Value = DevolverCod(nodx.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON3").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON2DEN").Value = DevolverDen(nodx.Parent)
                        Case "DEP3"
                            .ssSOLConfig.ActiveRow.Cells("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON2").Value = DevolverCod(nodx.Parent.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON3").Value = DevolverCod(nodx.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON3DEN").Value = DevolverDen(nodx.Parent)
                    End Select
                                  
                    .ssSOLConfig.ActiveRow.Cells("DEPCOD").Value = scod1
                    .ssSOLConfig.ActiveRow.Cells("DEPDEN").Value = DevolverDen(nodx)
                    
                    
                Case "PerUsu"
                    'a�ade una nueva fila a la grid:
                    If (.m_TipoContactoSolicitado = 1) Then
                        .g_udtAccion = accionessummit.ACCSolConfAnyaPet
                    ElseIf (.m_TipoContactoSolicitado = 2) Then
                        .g_udtAccion = accionessummit.ACCSolConfAnyaNotif
                    End If
                    
                    .picEdicion.Visible = True
                    .picNavigate.Visible = False
                    .ssSOLConfig.Bands(.m_TipoContactoSolicitado).AddNew  'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
                    
                    Set oPer = oFSGSRaiz.Generar_CPersona
                    oPer.Cod = scod1
                    oPer.CargarTodosLosDatos
                    
                    Select Case Left(nodx.Tag, 4)
                        Case "PER0"
                            .ssSOLConfig.ActiveRow.Cells("UON1").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON2").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON3").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON1DEN").Value = ""
                        Case "PER1"
                            .ssSOLConfig.ActiveRow.Cells("UON1").Value = DevolverCod(nodx.Parent.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON2").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON3").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON1DEN").Value = DevolverDen(nodx.Parent.Parent)
                        Case "PER2"
                            .ssSOLConfig.ActiveRow.Cells("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON2").Value = DevolverCod(nodx.Parent.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON3").Value = ""
                            .ssSOLConfig.ActiveRow.Cells("UON2DEN").Value = DevolverDen(nodx.Parent.Parent)
                        Case "PER3"
                            .ssSOLConfig.ActiveRow.Cells("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON2").Value = DevolverCod(nodx.Parent.Parent.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON3").Value = DevolverCod(nodx.Parent.Parent)
                            .ssSOLConfig.ActiveRow.Cells("UON3DEN").Value = DevolverDen(nodx.Parent.Parent)
                    End Select
                    
                    .ssSOLConfig.ActiveRow.Cells("DEPCOD").Value = DevolverCod(nodx.Parent)
                    .ssSOLConfig.ActiveRow.Cells("DEPDEN").Value = DevolverDen(nodx.Parent)
                    
                    .ssSOLConfig.ActiveRow.Cells("PER").Value = scod1
                    .ssSOLConfig.ActiveRow.Cells("NOM").Value = NullToStr(oPer.Apellidos)
                    .ssSOLConfig.ActiveRow.Cells("APE").Value = NullToStr(oPer.nombre)
                    .ssSOLConfig.ActiveRow.Cells("PER_DEN").Value = NullToStr(oPer.nombre) & " " & NullToStr(oPer.Apellidos)
                    
                    Set oPer = Nothing
                    
                Case Else
                    oMensajes.PersonaNoValida
                    Screen.MousePointer = vbNormal
                    If Me.Visible Then tvwEstrOrg.SetFocus
                    Exit Sub
            End Select
            End With
        Case "frmCatalogoConfiguracion"
            scod1 = DevolverCod(nodx)
            
            If nodx.Image = "PerUsu" Then
                Set oPer = oFSGSRaiz.Generar_CPersona
                oPer.Cod = scod1
                oPer.CargarTodosLosDatos
                frmCatalogoConfiguracion.txtAprovisionador.Tag = scod1
                frmCatalogoConfiguracion.txtAprovisionador.Text = oPer.nombre & " " & oPer.Apellidos & " (" & oPer.DenDep & ")"
                Set oPer = Nothing
            Else
                oMensajes.PersonaNoValida
                Screen.MousePointer = vbNormal
                If Me.Visible Then tvwEstrOrg.SetFocus
                Exit Sub
            End If
        Case "frmUsuarios"
            scod1 = DevolverCod(nodx)
            
            If nodx.Image = "PerUsu" Then
                Set oPer = oFSGSRaiz.Generar_CPersona
                oPer.Cod = scod1
                oPer.CargarTodosLosDatos
                frmUSUARIOS.txtSustituto.Tag = scod1
                frmUSUARIOS.txtSustituto.Text = oPer.nombre & " " & oPer.Apellidos & " (" & oPer.DenDep & ")"
                Set oPer = Nothing
            Else
                oMensajes.PersonaNoValida
                Screen.MousePointer = vbNormal
                If Me.Visible Then tvwEstrOrg.SetFocus
                Exit Sub
            End If
        Case "frmSOLSelPersonaEnFuncionDe"
            scod1 = DevolverCod(nodx)
            Select Case nodx.Image
                Case "UON0"
                    oMensajes.PersonaNoValida
                    Screen.MousePointer = vbNormal
                    If Me.Visible Then tvwEstrOrg.SetFocus
                    Exit Sub
                Case "UON1"
                    Select Case g_oTipoAprobador
                        Case TipoAprobadorSolicitudPedido.PersonaEnFuncionEmpresa
                            With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionEmpresa
                                .Columns("UON1").Value = scod1
                                .Columns("PER_DEP_UON").Value = scod1 & " - " & DevolverDen(nodx)
                                .Columns("UON2").Value = ""
                                .Columns("UON3").Value = ""
                                .Columns("DEP").Value = ""
                                .Columns("PER").Value = ""
                            End With
                        Case TipoAprobadorSolicitudPedido.PersonaEnFuncionCentroDeCoste
                            With frmSOLSelPersonaEnFuncionDe.sdbgPersonaEnFuncionCentroDeCoste
                                .Columns("UON1").Value = scod1
                                .Columns("PER_DEP_UON_DEN").Value = scod1 & " - " & DevolverDen(nodx)
                                
                                .Columns("UON2").Value = ""
                                .Columns("UON3").Value = ""
                                .Columns("DEP").Value = ""
                                .Columns("PER").Value = ""
                            End With
                        Case TipoAprobadorSolicitudPedido.PersonaEnFuncionPeticionario
                            With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionDePeticionario
                                If g_sAprobadoroPeticionario = "APROB" Then
                                    'Si se ha elegido el aprobador
                                    .Columns("APROB_UON1").Value = scod1
                                    .Columns("APROB_DEN").Value = scod1 & " - " & DevolverDen(nodx)
                                    .Columns("APROB_UON2").Value = ""
                                    .Columns("APROB_UON3").Value = ""
                                    .Columns("APROB_DEP").Value = ""
                                    .Columns("APROB_PER").Value = ""
                                Else
                                    'Si se ha elegido el peticionario
                                    .Columns("PET_UON1").Value = scod1
                                    .Columns("PET_DEN").Value = scod1 & " - " & DevolverDen(nodx)
                                    .Columns("PET_UON2").Value = ""
                                    .Columns("PET_UON3").Value = ""
                                    .Columns("PET_DEP").Value = ""
                                    .Columns("PET_PER").Value = ""
                                End If
                            End With
                    End Select
                Case "UON2"
                    Select Case g_oTipoAprobador
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionEmpresa
                        With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionEmpresa
                            .Columns("UON1").Value = DevolverCod(nodx.Parent)
                            .Columns("PER_DEP_UON").Value = DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
                            
                            .Columns("UON2").Value = scod1
                            .Columns("UON3").Value = ""
                            .Columns("DEP").Value = ""
                            .Columns("PER").Value = ""
                        End With
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionCentroDeCoste
                        With frmSOLSelPersonaEnFuncionDe.sdbgPersonaEnFuncionCentroDeCoste
                            .Columns("UON1").Value = DevolverCod(nodx.Parent)
                            .Columns("PER_DEP_UON_DEN").Value = DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
                            
                            .Columns("UON2").Value = scod1
                            .Columns("UON3").Value = ""
                            .Columns("DEP").Value = ""
                            .Columns("PER").Value = ""
                        End With
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionPeticionario
                        With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionDePeticionario
                            If g_sAprobadoroPeticionario = "APROB" Then
                                'Si se ha elegido el aprobador
                                .Columns("APROB_UON1").Value = DevolverCod(nodx.Parent)
                                .Columns("APROB_DEN").Value = DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
                                .Columns("APROB_UON2").Value = scod1
                                .Columns("APROB_UON3").Value = ""
                                .Columns("APROB_DEP").Value = ""
                                .Columns("APROB_PER").Value = ""
                            Else
                                'Si se ha elegido el peticionario
                                .Columns("PET_UON1").Value = DevolverCod(nodx.Parent)
                                .Columns("PET_DEN").Value = DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
                                .Columns("PET_UON2").Value = scod1
                                .Columns("PET_UON3").Value = ""
                                .Columns("PET_DEP").Value = ""
                                .Columns("PET_PER").Value = ""
                            End If
                        End With
                    End Select
                Case "UON3"
                    Select Case g_oTipoAprobador
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionEmpresa
                        With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionEmpresa
                            .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
                            .Columns("PER_DEP_UON").Value = DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
                            
                            .Columns("UON2").Value = DevolverCod(nodx.Parent)
                            .Columns("UON3").Value = scod1
                            .Columns("DEP").Value = ""
                            .Columns("PER").Value = ""
                        End With
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionCentroDeCoste
                        With frmSOLSelPersonaEnFuncionDe.sdbgPersonaEnFuncionCentroDeCoste
                            .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
                            .Columns("PER_DEP_UON_DEN").Value = DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
                            
                            .Columns("UON2").Value = DevolverCod(nodx.Parent)
                            .Columns("UON3").Value = scod1
                            .Columns("DEP").Value = ""
                            .Columns("PER").Value = ""
                        End With
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionPeticionario
                        With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionDePeticionario
                            If g_sAprobadoroPeticionario = "APROB" Then
                                'Si se ha elegido el aprobador
                                .Columns("APROB_UON1").Value = DevolverCod(nodx.Parent.Parent)
                                .Columns("APROB_DEN").Value = DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
                                .Columns("APROB_UON2").Value = DevolverCod(nodx.Parent)
                                .Columns("APROB_UON3").Value = scod1
                                .Columns("APROB_DEP").Value = ""
                                .Columns("APROB_PER").Value = ""
                            Else
                                'Si se ha elegido el peticionario
                                .Columns("PET_UON1").Value = DevolverCod(nodx.Parent.Parent)
                                .Columns("PET_DEN").Value = DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
                                .Columns("PET_UON2").Value = DevolverCod(nodx.Parent)
                                .Columns("PET_UON3").Value = scod1
                                .Columns("PET_DEP").Value = ""
                                .Columns("PET_PER").Value = ""
                            End If
                        End With
                    End Select
                            
                Case "Departamento"
                    Select Case g_oTipoAprobador
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionEmpresa
                        With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionEmpresa
                        .Columns("PER_DEP_UON").Value = scod1 & " - " & DevolverDen(nodx)
                        .Columns("DEP").Value = scod1
                        .Columns("PER").Value = ""
                        Select Case Left(nodx.Tag, 4)
                            Case "DEP0"
                                .Columns("UON1").Value = ""
                                .Columns("UON2").Value = ""
                                .Columns("UON3").Value = ""
                            Case "DEP1"
                                .Columns("UON1").Value = DevolverCod(nodx.Parent)
                                .Columns("UON2").Value = ""
                                .Columns("UON3").Value = ""
                            Case "DEP2"
                                .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
                                .Columns("UON2").Value = DevolverCod(nodx.Parent)
                                .Columns("UON3").Value = ""
                            Case "DEP3"
                                .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                .Columns("UON2").Value = DevolverCod(nodx.Parent.Parent)
                                .Columns("UON3").Value = DevolverCod(nodx.Parent)
                        End Select
                        End With
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionCentroDeCoste
                        With frmSOLSelPersonaEnFuncionDe.sdbgPersonaEnFuncionCentroDeCoste
                            .Columns("PER_DEP_UON_DEN").Value = scod1 & " - " & DevolverDen(nodx)
                            .Columns("DEP").Value = scod1
                            .Columns("PER").Value = ""
                            Select Case Left(nodx.Tag, 4)
                                Case "DEP0"
                                    .Columns("UON1").Value = ""
                                    .Columns("UON2").Value = ""
                                    .Columns("UON3").Value = ""
                                Case "DEP1"
                                    .Columns("UON1").Value = DevolverCod(nodx.Parent)
                                    .Columns("UON2").Value = ""
                                    .Columns("UON3").Value = ""
                                Case "DEP2"
                                    .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
                                    .Columns("UON2").Value = DevolverCod(nodx.Parent)
                                    .Columns("UON3").Value = ""
                                Case "DEP3"
                                    .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                    .Columns("UON2").Value = DevolverCod(nodx.Parent.Parent)
                                    .Columns("UON3").Value = DevolverCod(nodx.Parent)
                            End Select
                        End With
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionPeticionario
                        If g_sAprobadoroPeticionario = "APROB" Then
                            With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionDePeticionario
                                .Columns("APROB_DEN").Value = scod1 & " - " & DevolverDen(nodx)
                                .Columns("APROB_DEP").Value = scod1
                                .Columns("APROB_PER").Value = ""
                                Select Case Left(nodx.Tag, 4)
                                    Case "DEP0"
                                        .Columns("APROB_UON1").Value = ""
                                        .Columns("APROB_UON2").Value = ""
                                        .Columns("APROB_UON3").Value = ""
                                    Case "DEP1"
                                        .Columns("APROB_UON1").Value = DevolverCod(nodx.Parent)
                                        .Columns("APROB_UON2").Value = ""
                                        .Columns("APROB_UON3").Value = ""
                                    Case "DEP2"
                                        .Columns("APROB_UON1").Value = DevolverCod(nodx.Parent.Parent)
                                        .Columns("APROB_UON2").Value = DevolverCod(nodx.Parent)
                                        .Columns("APROB_UON3").Value = ""
                                    Case "DEP3"
                                        .Columns("APROB_UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                        .Columns("APROB_UON2").Value = DevolverCod(nodx.Parent.Parent)
                                        .Columns("APROB_UON3").Value = DevolverCod(nodx.Parent)
                                End Select
                            End With
                        Else
                            With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionDePeticionario
                                .Columns("PET_DEN").Value = scod1 & " - " & DevolverDen(nodx)
                                .Columns("PET_DEP").Value = scod1
                                .Columns("PET_PER").Value = ""
                                Select Case Left(nodx.Tag, 4)
                                    Case "DEP0"
                                        .Columns("PET_UON1").Value = ""
                                        .Columns("PET_UON2").Value = ""
                                        .Columns("PET_UON3").Value = ""
                                    Case "DEP1"
                                        .Columns("PET_UON1").Value = DevolverCod(nodx.Parent)
                                        .Columns("PET_UON2").Value = ""
                                        .Columns("PET_UON3").Value = ""
                                    Case "DEP2"
                                        .Columns("PET_UON1").Value = DevolverCod(nodx.Parent.Parent)
                                        .Columns("PET_UON2").Value = DevolverCod(nodx.Parent)
                                        .Columns("PET_UON3").Value = ""
                                    Case "DEP3"
                                        .Columns("PET_UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                        .Columns("PET_UON2").Value = DevolverCod(nodx.Parent.Parent)
                                        .Columns("PET_UON3").Value = DevolverCod(nodx.Parent)
                                End Select
                            End With
                        End If
                        
                    End Select
                                        
                Case "PerUsu"
                    Select Case g_oTipoAprobador
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionEmpresa
                        Set oPer = oFSGSRaiz.Generar_CPersona
                        oPer.Cod = scod1
                        oPer.CargarTodosLosDatos
                        
                        With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionEmpresa
                        .Columns("PER").Value = scod1
                        .Columns("PER_DEP_UON").Value = NullToStr(oPer.Cod) & "-" & NullToStr(oPer.nombre) & " " & NullToStr(oPer.Apellidos)
                        
                        Select Case Left(nodx.Tag, 4)
                            Case "PER0"
                                .Columns("UON1").Value = ""
                                .Columns("UON2").Value = ""
                                .Columns("UON3").Value = ""
                            Case "PER1"
                                .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
                                .Columns("UON2").Value = ""
                                .Columns("UON3").Value = ""
                            Case "PER2"
                                .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                .Columns("UON2").Value = DevolverCod(nodx.Parent.Parent)
                                .Columns("UON3").Value = ""
                            Case "PER3"
                                .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                                .Columns("UON2").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                .Columns("UON3").Value = DevolverCod(nodx.Parent.Parent)
                        End Select
                        .Columns("DEP").Value = DevolverCod(nodx.Parent)
                        End With
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionCentroDeCoste
                        Set oPer = oFSGSRaiz.Generar_CPersona
                        oPer.Cod = scod1
                        oPer.CargarTodosLosDatos
                        
                        With frmSOLSelPersonaEnFuncionDe.sdbgPersonaEnFuncionCentroDeCoste
                            .Columns("PER").Value = scod1
                            .Columns("PER_DEP_UON_DEN").Value = NullToStr(oPer.Cod) & "-" & NullToStr(oPer.nombre) & " " & NullToStr(oPer.Apellidos)
                            
                            Select Case Left(nodx.Tag, 4)
                                Case "PER0"
                                    .Columns("UON1").Value = ""
                                    .Columns("UON2").Value = ""
                                    .Columns("UON3").Value = ""
                                Case "PER1"
                                    .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
                                    .Columns("UON2").Value = ""
                                    .Columns("UON3").Value = ""
                                Case "PER2"
                                    .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                    .Columns("UON2").Value = DevolverCod(nodx.Parent.Parent)
                                    .Columns("UON3").Value = ""
                                Case "PER3"
                                    .Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                                    .Columns("UON2").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                    .Columns("UON3").Value = DevolverCod(nodx.Parent.Parent)
                            End Select
                            .Columns("DEP").Value = DevolverCod(nodx.Parent)
                        End With
                    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionPeticionario
                        Set oPer = oFSGSRaiz.Generar_CPersona
                        oPer.Cod = scod1
                        oPer.CargarTodosLosDatos
                        
                        If g_sAprobadoroPeticionario = "APROB" Then
                            With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionDePeticionario
                                .Columns("APROB_PER").Value = scod1
                                .Columns("APROB_DEN").Value = NullToStr(oPer.Cod) & "-" & NullToStr(oPer.nombre) & " " & NullToStr(oPer.Apellidos)
                                
                                Select Case Left(nodx.Tag, 4)
                                    Case "PER0"
                                        .Columns("APROB_UON1").Value = ""
                                        .Columns("APROB_UON2").Value = ""
                                        .Columns("APROB_UON3").Value = ""
                                    Case "PER1"
                                        .Columns("APROB_UON1").Value = DevolverCod(nodx.Parent.Parent)
                                        .Columns("APROB_UON2").Value = ""
                                        .Columns("APROB_UON3").Value = ""
                                    Case "PER2"
                                        .Columns("APROB_UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                        .Columns("APROB_UON2").Value = DevolverCod(nodx.Parent.Parent)
                                        .Columns("APROB_UON3").Value = ""
                                    Case "PER3"
                                        .Columns("APROB_UON1").Value = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                                        .Columns("APROB_UON2").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                        .Columns("APROB_UON3").Value = DevolverCod(nodx.Parent.Parent)
                                End Select
                                .Columns("APROB_DEP").Value = DevolverCod(nodx.Parent)
                            End With
                        Else
                            With frmSOLSelPersonaEnFuncionDe.sdbgPerEnFuncionDePeticionario
                                .Columns("PET_PER").Value = scod1
                                .Columns("PET_DEN").Value = NullToStr(oPer.Cod) & "-" & NullToStr(oPer.nombre) & " " & NullToStr(oPer.Apellidos)
                                
                                Select Case Left(nodx.Tag, 4)
                                    Case "PER0"
                                        .Columns("PET_UON1").Value = ""
                                        .Columns("PET_UON2").Value = ""
                                        .Columns("PET_UON3").Value = ""
                                    Case "PER1"
                                        .Columns("PET_UON1").Value = DevolverCod(nodx.Parent.Parent)
                                        .Columns("PET_UON2").Value = ""
                                        .Columns("PET_UON3").Value = ""
                                    Case "PER2"
                                        .Columns("PET_UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                        .Columns("PET_UON2").Value = DevolverCod(nodx.Parent.Parent)
                                        .Columns("PET_UON3").Value = ""
                                    Case "PER3"
                                        .Columns("PET_UON1").Value = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                                        .Columns("PET_UON2").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                        .Columns("PET_UON3").Value = DevolverCod(nodx.Parent.Parent)
                                End Select
                                .Columns("PET_DEP").Value = DevolverCod(nodx.Parent)
                            End With
                        End If
                    End Select
                Case Else
                    oMensajes.PersonaNoValida
                    Screen.MousePointer = vbNormal
                    If Me.Visible Then tvwEstrOrg.SetFocus
                    Exit Sub
            End Select
        Case "frmFlujosRoles"
            scod1 = DevolverCod(nodx)
            With frmFlujosRoles
            If bAllowSelUON Then
                Select Case nodx.Image
                    Case "UON0"
                        If bAllowSelUON0 Then
                            .sdbgRoles.Columns("UON0").Value = "True"
                            .sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = nodx.Text
                            .sdbgRoles.Columns("UON1").Value = ""
                            .sdbgRoles.Columns("UON2").Value = ""
                            .sdbgRoles.Columns("UON3").Value = ""
                            .sdbgRoles.Columns("DEP").Value = ""
                            .sdbgRoles.Columns("PER").Value = ""
                        Else
                            oMensajes.PersonaNoValida
                            Screen.MousePointer = vbNormal
                            If Me.Visible Then tvwEstrOrg.SetFocus
                            Exit Sub
                        End If
                    Case "UON1"
                        .sdbgRoles.Columns("UON0").Value = "False"
                        .sdbgRoles.Columns("UON1").Value = scod1
                        .sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = scod1 & " - " & DevolverDen(nodx)
                        
                        .sdbgRoles.Columns("UON2").Value = ""
                        .sdbgRoles.Columns("UON3").Value = ""
                        .sdbgRoles.Columns("DEP").Value = ""
                        .sdbgRoles.Columns("PER").Value = ""
            
                    Case "UON2"
                        .sdbgRoles.Columns("UON0").Value = "False"
                        .sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent)
                        .sdbgRoles.Columns("UON2").Value = scod1
                        .sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
                        
                        .sdbgRoles.Columns("UON3").Value = ""
                        .sdbgRoles.Columns("DEP").Value = ""
                        .sdbgRoles.Columns("PER").Value = ""
                        
                    Case "UON3"
                        .sdbgRoles.Columns("UON0").Value = "False"
                        .sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
                        .sdbgRoles.Columns("UON2").Value = DevolverCod(nodx.Parent)
                        .sdbgRoles.Columns("UON3").Value = scod1
                        .sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
                        
                        .sdbgRoles.Columns("DEP").Value = ""
                        .sdbgRoles.Columns("PER").Value = ""
                                
                    Case "Departamento"
                        .sdbgRoles.Columns("UON0").Value = "False"
                        .sdbgRoles.Columns("DEP").Value = scod1
                        .sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = scod1 & " - " & DevolverDen(nodx)
                        
                        Select Case Left(nodx.Tag, 4)
                            Case "DEP0"
                                .sdbgRoles.Columns("UON1").Value = ""
                                .sdbgRoles.Columns("UON2").Value = ""
                                .sdbgRoles.Columns("UON3").Value = ""
                            Case "DEP1"
                                .sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent)
                                .sdbgRoles.Columns("UON2").Value = ""
                                .sdbgRoles.Columns("UON3").Value = ""
                            Case "DEP2"
                                .sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
                                .sdbgRoles.Columns("UON2").Value = DevolverCod(nodx.Parent)
                                .sdbgRoles.Columns("UON3").Value = ""
                            Case "DEP3"
                                .sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                .sdbgRoles.Columns("UON2").Value = DevolverCod(nodx.Parent.Parent)
                                .sdbgRoles.Columns("UON3").Value = DevolverCod(nodx.Parent)
                        End Select
                        .sdbgRoles.Columns("PER").Value = ""
                                            
                    Case "PerUsu"
                        .sdbgRoles.Columns("UON0").Value = "False"
                        Set oPer = oFSGSRaiz.Generar_CPersona
                        oPer.Cod = scod1
                        oPer.CargarTodosLosDatos
                        
                        .sdbgRoles.Columns("PER").Value = scod1
                        .sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = NullToStr(oPer.nombre) & " " & NullToStr(oPer.Apellidos) & " ( " & oPer.mail & " )"
                        
                        Select Case Left(nodx.Tag, 4)
                            Case "PER0"
                                .sdbgRoles.Columns("UON1").Value = ""
                                .sdbgRoles.Columns("UON2").Value = ""
                                .sdbgRoles.Columns("UON3").Value = ""
                            Case "PER1"
                                .sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
                                .sdbgRoles.Columns("UON2").Value = ""
                                .sdbgRoles.Columns("UON3").Value = ""
                            Case "PER2"
                                .sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                .sdbgRoles.Columns("UON2").Value = DevolverCod(nodx.Parent.Parent)
                                .sdbgRoles.Columns("UON3").Value = ""
                            Case "PER3"
                                .sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                                .sdbgRoles.Columns("UON2").Value = DevolverCod(nodx.Parent.Parent.Parent)
                                .sdbgRoles.Columns("UON3").Value = DevolverCod(nodx.Parent.Parent)
                        End Select
                        .sdbgRoles.Columns("DEP").Value = DevolverCod(nodx.Parent)
                    
                    Case Else
                        oMensajes.PersonaNoValida
                        Screen.MousePointer = vbNormal
                        If Me.Visible Then tvwEstrOrg.SetFocus
                        Exit Sub
                End Select
            Else
                If nodx.Image = "PerUsu" Then
                    Set oPer = oFSGSRaiz.Generar_CPersona
                    oPer.Cod = scod1
                    oPer.CargarTodosLosDatos
                    .sdbgRoles.Columns("PER").Value = scod1
                    .sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = NullToStr(oPer.nombre) & " " & NullToStr(oPer.Apellidos) & " ( " & oPer.mail & " )"
                    Set oPer = Nothing
                Else
                    oMensajes.PersonaNoValida
                    Screen.MousePointer = vbNormal
                    If Me.Visible Then tvwEstrOrg.SetFocus
                    Exit Sub
                End If
            End If
            End With
        Case "frmFlujosRolesListaPersonas"
            scod1 = DevolverCod(nodx)
            
            If nodx.Image = "PerUsu" Then
                Set oPer = oFSGSRaiz.Generar_CPersona
                oPer.Cod = scod1
                oPer.CargarTodosLosDatos
                On Error GoTo Error 'Si estamos intentando meter 2 veces la misma persona dar� error:
                frmFlujosRolesListaPersonas.lstPersonas.ListItems.Add , "P" & oPer.Cod, oPer.nombre & " " & oPer.Apellidos & " (" & oPer.mail & ")"
                Set oPer = Nothing
            Else
                oMensajes.PersonaNoValida
                Screen.MousePointer = vbNormal
                If Me.Visible Then tvwEstrOrg.SetFocus
                Exit Sub
            End If
        Case "frmFormularios" 'En este caso se puede seleccionar una persona NO usuaria
            scod1 = DevolverCod(nodx)
            
            If nodx.Image = "PerUsu" Or nodx.Image = "Persona" Then
                Set oPer = oFSGSRaiz.Generar_CPersona
                oPer.Cod = scod1
                oPer.CargarTodosLosDatos
                frmFormularios.sdbgCampos.Columns("COD_VALOR").Value = oPer.Cod
                frmFormularios.sdbgCampos.Columns("VALOR").Value = oPer.nombre & " " & oPer.Apellidos & " (" & oPer.mail & ")"
                Set oPer = Nothing
            Else
                oMensajes.FormPersonaNoValida
                Screen.MousePointer = vbNormal
                If Me.Visible Then tvwEstrOrg.SetFocus
                Exit Sub
            End If
        
        Case "frmDesgloseValores" 'En este caso se puede seleccionar una persona NO usuaria
            scod1 = DevolverCod(nodx)
            
                Set oPer = oFSGSRaiz.Generar_CPersona
                oPer.Cod = scod1
                oPer.CargarTodosLosDatos
                frmDesgloseValores.m_sPerCod = oPer.Cod
                frmDesgloseValores.ssLineas.ActiveCell.Value = oPer.nombre & " " & oPer.Apellidos & " (" & oPer.mail & ")"
                Set oPer = Nothing
            
        Case "frmSolicitudDetalle" 'En este caso se puede seleccionar una persona NO usuaria
            scod1 = DevolverCod(nodx)
                
            Set oPer = oFSGSRaiz.Generar_CPersona
            oPer.Cod = scod1
            oPer.CargarTodosLosDatos
            frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("COD_VALOR").Value = oPer.Cod
            frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("VALOR").Value = oPer.nombre & " " & oPer.Apellidos & " (" & oPer.mail & ")"
            frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Update
            Set oPer = Nothing
            
        Case "frmSolicitudDesglose" 'En este caso se puede seleccionar una persona NO usuaria
            scod1 = DevolverCod(nodx)
            
            Set oPer = oFSGSRaiz.Generar_CPersona
            oPer.Cod = scod1
            oPer.CargarTodosLosDatos
            frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.m_sPerCod = oPer.Cod
            frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = oPer.nombre & " " & oPer.Apellidos & " (" & oPer.mail & ")"
            
            'Muestra el bot�n de deshacer
            frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.cmdDeshacer.Visible = True
            frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
            frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
            Set oPer = Nothing

        Case "frmSolicitudDesgloseP" 'En este caso se puede seleccionar una persona NO usuaria
            scod1 = DevolverCod(nodx)
            
            Set oPer = oFSGSRaiz.Generar_CPersona
            oPer.Cod = scod1
            oPer.CargarTodosLosDatos
            frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.m_sPerCod = oPer.Cod
            frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = oPer.nombre & " " & oPer.Apellidos & " (" & oPer.mail & ")"
            
            'Muestra el bot�n de deshacer
            frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.cmdDeshacer.Visible = True
            frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = True
            frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = False
            Set oPer = Nothing
    End Select
    
    Screen.MousePointer = vbNormal
    
    Unload Me
    
Error:
    Set oPer = Nothing
    Screen.MousePointer = vbNormal
    
    Unload Me
End Sub

Private Sub cmdBuscarCodigo_Click()
    Dim blnEncontrado As Boolean
    Dim oNodo As node
    
    blnEncontrado = False
    
    For Each oNodo In tvwEstrOrg.Nodes
        If UCase(Mid(oNodo.Tag, 5)) = UCase(tbBuscarCodigo.Text) Then
            blnEncontrado = True
            Exit For
        End If
    Next
    
    If blnEncontrado Then
        Set tvwEstrOrg.selectedItem = oNodo
        oNodo.EnsureVisible
    Else
        Set tvwEstrOrg.selectedItem = tvwEstrOrg.Nodes(1)
    End If
    
    Set oNodo = Nothing
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOL_SELEC_PERSONA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        m_sTituloSolConf = Ador(0).Value  '1 Seleccione peticionario
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        m_sTituloUsuarios = Ador(0).Value '4 Seleccione sustituto temporal
        Ador.MoveNext
        m_sTituloPersona = Ador(0).Value
        Ador.MoveNext
        lblBusqueda.caption = Ador(0).Value
        Ador.MoveNext
        m_sTituloSolNotif = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
    
End Sub

''' Revisado por: blp. Fecha: 23/07/2012
''' <summary>
''' Carga el formulario
''' </summary>
''' <remarks>Llamada desde:evento de carga del formulario; Tiempo m�ximo;0,3seg.</remarks>
Private Sub Form_Load()
On Error GoTo Error

    Dim nodx As node
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    
    Me.Width = 5640
    Me.Height = 4980
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    GenerarEstructuraOrg False
        
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            If frmSOLConfiguracion.m_TipoContactoSolicitado = 2 Then
                Me.caption = m_sTituloSolNotif
            Else
                Me.caption = m_sTituloSolConf
            End If
    
        Case "frmUsuarios"
            Me.caption = m_sTituloUsuarios
        Case "frmFlujosRoles"
            If bAllowSelUON Then
                Me.caption = m_sTituloSolConf
            Else
                Me.caption = m_sTituloPersona
            End If
            
            scod1 = g_sUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(g_sUON1))
            scod2 = g_sUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(g_sUON2))
            scod3 = g_sUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(g_sUON3))
            scod4 = g_sDEP & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(g_sDEP))
            
            If g_sPer <> "" Then 'Seleccionar la persona en la estructura de la organizacion
                Set nodx = Me.tvwEstrOrg.Nodes.Item("PERS" & g_sPer)
                nodx.Parent.Expanded = True
                nodx.Selected = True
            ElseIf g_sDEP <> "" Then 'Seleccionar el departamento en la estructura de la organizacion
                Set nodx = Me.tvwEstrOrg.Nodes.Item("DEPA" & LTrim(scod1) & LTrim(scod2) & LTrim(scod3) & scod4)
                nodx.Parent.Expanded = True
                nodx.Selected = True
            ElseIf g_sUON3 <> "" Then 'Seleccionar el uon3 en la estructura de la organizacion
                Set nodx = Me.tvwEstrOrg.Nodes.Item("UON3" & scod1 & scod2 & scod3)
                nodx.Parent.Expanded = True
                nodx.Selected = True
            ElseIf g_sUON2 <> "" Then 'Seleccionar el uon2 en la estructura de la organizacion
                Set nodx = Me.tvwEstrOrg.Nodes.Item("UON2" & scod1 & scod2)
                nodx.Parent.Expanded = True
                nodx.Selected = True
            ElseIf g_sUON1 <> "" Then 'Seleccionar el uon1 en la estructura de la organizacion
                Set nodx = Me.tvwEstrOrg.Nodes.Item("UON1" & scod1)
                nodx.Parent.Expanded = True
                nodx.Selected = True
            ElseIf g_sUON0 <> "" Then 'Seleccionar el uon0 en la estructura de la organizacion
                Set nodx = Me.tvwEstrOrg.Nodes.Item("UON0")
                nodx.Selected = True
            End If
        Case "frmFlujosRolesListaPersonas", "frmFormularios", "frmDesgloseValores", "frmSolicitudDetalle", "frmSolicitudDesglose"
            Me.caption = m_sTituloPersona
    End Select
    Exit Sub
    
Error:
    Select Case err.Number
        Case 35601
            Exit Sub
    End Select
End Sub

''' <summary>
''' Genera el arbol
''' </summary>
''' <param name="bOrdenadoPorDen">Si se desea ordenar por nombre</param>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo;1,2seg.</remarks>
''' <revisado por>mmv(05/10/2011)</remarks>
Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Departamentos asociados
Dim oDepsAsocN0 As CDepAsociados
Dim oDepsAsocN1 As CDepAsociados
Dim oDepsAsocN2 As CDepAsociados
Dim oDepsAsocN3 As CDepAsociados
Dim oDepAsoc As CDepAsociado

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3

' Personas
Dim oPersN0 As CPersonas
Dim oPersN1 As CPersonas
Dim oPersN2 As CPersonas
Dim oPersN3 As CPersonas
Dim oPer As CPersona

' Otras
Dim nodx As node
Dim varCodPersona As Variant


Dim sUON1Empresas As String
Dim sUON2Empresas As String
Dim sUON3Empresas As String
    
    
    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
     
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    
    Set oPersN0 = oFSGSRaiz.Generar_CPersonas
    Set oPersN1 = oFSGSRaiz.Generar_CPersonas
    Set oPersN2 = oFSGSRaiz.Generar_CPersonas
    Set oPersN3 = oFSGSRaiz.Generar_CPersonas
         
    If (oUsuarioSummit.Tipo = TipoDeUsuario.Persona Or oUsuarioSummit.Tipo = TipoDeUsuario.comprador) And _
       (bRUO Or bRDep) Then
        
        varCodPersona = oUsuarioSummit.Persona.Cod
        oDepsAsocN0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 0, , , , bOrdenadoPorDen, False
        
        ' Cargamos las personas  de esos departamentos.
        For Each oDepAsoc In oDepsAsocN0
             oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
        Next
            
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , False, , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False
                oDepsAsocN3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 3, , , , bOrdenadoPorDen, False
                ' Cargamos las personas  de esos departamentos.
        
                'Personas
                
                For Each oDepAsoc In oDepsAsocN1
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN2
                   oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN3
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                    
                Next
        End Select
        
        
    Else
        Dim bsoloPM
        
        If g_sOrigen = "frmUsuarios" Then
            bsoloPM = True
        Else
            bsoloPM = False
        End If
        
        oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False
        oPersN0.CargarTodasLasPersonas , , , , 0, , True, , , False, , True, , , , bsoloPM
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False, , , , , , , , , , , , g_sEmpresas, sUON1Empresas
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True, , , , bsoloPM
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False, , , , , , , , , , , , g_sEmpresas, sUON1Empresas
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False, , , , , , , , , , , , g_sEmpresas, sUON1Empresas, sUON2Empresas
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True, , , , bsoloPM
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True, , , , bsoloPM
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, , , , , , , , , , , , , g_sEmpresas, sUON1Empresas
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, , , , , , , , , , , , , g_sEmpresas, sUON1Empresas, sUON2Empresas
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen, , , , , , , , , , , , , g_sEmpresas, sUON1Empresas, sUON2Empresas, sUON3Empresas
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True, , , , bsoloPM
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True, , , , bsoloPM
                oPersN3.CargarTodasLasPersonas , , , , 3, , True, , , False, , True, , , , bsoloPM
                
        End Select
        
    End If
    
   '************************************************************
    'Generamos la estructura arborea
    tvwEstrOrg.Nodes.clear

    ' Unidades organizativas
    Set nodx = tvwEstrOrg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    Dim bEncontrado As Boolean
    bEncontrado = False
    
    'g_sEmpresas = ""
    
    If g_sEmpresas <> "" Then
        For Each oUON1 In oUnidadesOrgN1
            bEncontrado = False
            If ((InStr(1, sUON1Empresas, oUON1.Cod & ",") > 0) Or (InStr(1, sUON2Empresas, oUON1.Cod & "#") > 0) Or (InStr(1, sUON3Empresas, oUON1.Cod & "#") > 0)) Then
                bEncontrado = True
            End If
            If bEncontrado Then
                scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
                Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
                nodx.Tag = "UON1" & CStr(oUON1.Cod)
            End If
        Next
        
        For Each oUON2 In oUnidadesOrgN2
            bEncontrado = False
            If InStr(1, sUON2Empresas, oUON2.CodUnidadOrgNivel1 & "#" & oUON2.Cod & ",") > 0 Then
                bEncontrado = True
            End If
            If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                If InStr(1, sUON1Empresas, oUON2.CodUnidadOrgNivel1 & ",") > 0 Then
                    bEncontrado = True
                End If
            End If
            'o Inferior
            If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                If InStr(1, sUON3Empresas, oUON2.CodUnidadOrgNivel1 & "#" & oUON2.Cod & "#") > 0 Then
                    bEncontrado = True
                End If
            End If
            If bEncontrado Then
                scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
                scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
                Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
                nodx.Tag = "UON2" & CStr(oUON2.Cod)
            End If
                
        Next
        
        For Each oUON3 In oUnidadesOrgN3
            bEncontrado = False
            If InStr(1, sUON3Empresas, oUON3.CodUnidadOrgNivel1 & "#" & oUON3.CodUnidadOrgNivel2 & "#" & oUON3.Cod & ",") > 0 Then
                bEncontrado = True
            End If
            
            If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                If (InStr(1, sUON2Empresas, oUON3.CodUnidadOrgNivel1 & "#" & oUON3.CodUnidadOrgNivel2 & ",") > 0) Then
                    bEncontrado = True
                End If
            End If
            
            If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                If InStr(1, sUON1Empresas, oUON3.CodUnidadOrgNivel1 & ",") > 0 Then
                    bEncontrado = True
                End If
            End If
            
            
            If bEncontrado Then
                scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
                scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
                scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
                Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
                nodx.Tag = "UON3" & CStr(oUON3.Cod)
            End If
                
        Next
    
    Else
    
    
        For Each oUON1 In oUnidadesOrgN1
            scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
            Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
            nodx.Tag = "UON1" & CStr(oUON1.Cod)
        Next
        
        For Each oUON2 In oUnidadesOrgN2
            
            scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
            scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
            Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
            nodx.Tag = "UON2" & CStr(oUON2.Cod)
                
        Next
        
        For Each oUON3 In oUnidadesOrgN3
            
            scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
            scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
            scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
            Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
            nodx.Tag = "UON3" & CStr(oUON3.Cod)
                
        Next
    
    
    End If
    
    
    
    ' Departamentos
    
    If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador) _
        And (bRUO Or bRDep) Then
        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
            Next
        Next
                   
        If g_sEmpresas <> "" Then
            For Each oDepAsoc In oDepsAsocN1
                
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                bEncontrado = False
                If ((InStr(1, sUON1Empresas, oDepAsoc.CodUON1 & ",") > 0) Or (InStr(1, sUON2Empresas, oDepAsoc.CodUON1 & "#") > 0) Or (InStr(1, sUON3Empresas, oDepAsoc.CodUON1 & "#") > 0)) Then
                    bEncontrado = True
                End If
                
                If (bEncontrado) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
                    For Each oPer In oDepAsoc.Personas
                        If Not IsNull(oPer.Usuario) Then
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                        Else
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                        End If
                        nodx.Tag = "PER1" & CStr(oPer.Cod)
                    Next
                End If
            Next
        Else
            For Each oDepAsoc In oDepsAsocN1
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
                For Each oPer In oDepAsoc.Personas
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                    Else
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                    nodx.Tag = "PER1" & CStr(oPer.Cod)
                Next
            Next
        
        End If
        
        If g_sEmpresas <> "" Then
            
            For Each oDepAsoc In oDepsAsocN2
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                  
                bEncontrado = Buscar(sUON2Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oDepAsoc.CodUON1 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON3Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & "#")
                End If
                    
                                   
                If bEncontrado Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
                    For Each oPer In oDepAsoc.Personas
                        If Not IsNull(oPer.Usuario) Then
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                        Else
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                        End If
                        nodx.Tag = "PER2" & CStr(oPer.Cod)
                    Next
                End If
            Next
        Else
            
            For Each oDepAsoc In oDepsAsocN2
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                    
                Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
                For Each oPer In oDepAsoc.Personas
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                    Else
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                    nodx.Tag = "PER2" & CStr(oPer.Cod)
                Next
            Next
        
        End If
        
        If g_sEmpresas <> "" Then
            For Each oDepAsoc In oDepsAsocN3
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
                scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                bEncontrado = Buscar(sUON3Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & "#" & oDepAsoc.CodUON3 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON2Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oDepAsoc.CodUON1 & ",")
                End If
                
                If bEncontrado Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
                    For Each oPer In oDepAsoc.Personas
                        If Not IsNull(oPer.Usuario) Then
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                        Else
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                        End If
                        nodx.Tag = "PER3" & CStr(oPer.Cod)
                    Next
                End If
            Next
        Else
            For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
            Next
        Next
        End If
    
    Else
    
        'Departamentos
        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
        
        Next
        'Departamentos de la UON1
        If g_sEmpresas <> "" Then
            For Each oDepAsoc In oDepsAsocN1
                bEncontrado = False
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                If ((InStr(1, sUON1Empresas, oDepAsoc.CodUON1 & ",") > 0) Or (InStr(1, sUON2Empresas, oDepAsoc.CodUON1 & "#") > 0) Or (InStr(1, sUON3Empresas, oDepAsoc.CodUON1 & "#") > 0)) Then
                    bEncontrado = True
                End If
                
                If (bEncontrado) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
                End If
                
            Next
        Else
            For Each oDepAsoc In oDepsAsocN1
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
                
            Next
        End If
                
        'Departamentos de la UON2
        If g_sEmpresas <> "" Then
            For Each oDepAsoc In oDepsAsocN2
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                bEncontrado = Buscar(sUON2Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oDepAsoc.CodUON1 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON3Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & "#")
                End If
                    
                                   
                If bEncontrado Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
                End If
                
            Next
            
        Else
            For Each oDepAsoc In oDepsAsocN2
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                    
                Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            Next
        
        End If
        
        
        'Departamentos de la UON3
        If g_sEmpresas <> "" Then
            For Each oDepAsoc In oDepsAsocN3
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
                scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                
                bEncontrado = Buscar(sUON3Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & "#" & oDepAsoc.CodUON3 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON2Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oDepAsoc.CodUON1 & ",")
                End If
                
                
                If bEncontrado Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
                End If
                
            Next
        Else
            For Each oDepAsoc In oDepsAsocN3
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
                scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
                
            Next
        
        End If
                
        'Personas
        'UON0
        For Each oPer In oPersN0
            scod1 = oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER0" & CStr(oPer.Cod)
        Next
        'UON1
        If g_sEmpresas <> "" Then
            
            For Each oPer In oPersN1
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                bEncontrado = False
                If ((InStr(1, sUON1Empresas, oPer.UON1 & ",") > 0) Or (InStr(1, sUON2Empresas, oPer.UON1 & "#") > 0) Or (InStr(1, sUON3Empresas, oPer.UON1 & "#") > 0)) Then
                    bEncontrado = True
                End If
                
                If bEncontrado Then
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                    Else
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                    nodx.Tag = "PER1" & CStr(oPer.Cod)
                End If
            Next
        Else
            For Each oPer In oPersN1
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER1" & CStr(oPer.Cod)
            Next
        
        End If
        'UON2
        If g_sEmpresas <> "" Then
            For Each oPer In oPersN2
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                
                
                bEncontrado = Buscar(sUON2Empresas, oPer.UON1 & "#" & oPer.UON2 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oPer.UON1 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON3Empresas, oPer.UON1 & "#" & oPer.UON2 & "#")
                End If
                    
                                   
                If bEncontrado Then
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                    Else
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                    nodx.Tag = "PER2" & CStr(oPer.Cod)
                End If
            Next
        Else
            For Each oPer In oPersN2
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER2" & CStr(oPer.Cod)
            Next
        End If
        'UON3
        If g_sEmpresas <> "" Then
            For Each oPer In oPersN3
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
                scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                
                
                bEncontrado = Buscar(sUON3Empresas, oPer.UON1 & "#" & oPer.UON2 & "#" & oPer.UON3 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON2Empresas, oPer.UON1 & "#" & oPer.UON2 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oPer.UON1 & ",")
                End If
                
                
                If bEncontrado Then
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                    Else
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                    nodx.Tag = "PER3" & CStr(oPer.Cod)
                End If
            Next
        Else
            For Each oPer In oPersN3
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
                scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
            Next
        
        End If
    End If
    
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    Set oDepAsoc = Nothing
    
    Set oPersN0 = Nothing
    Set oPersN1 = Nothing
    Set oPersN2 = Nothing
    Set oPersN3 = Nothing
    Set oPer = Nothing
    
End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
    If node Is Nothing Then Exit Function
    
    If Len(node.Tag) < 4 Then
        DevolverCod = node.Tag
        Exit Function
    End If
    DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
        
End Function


Private Function DevolverDen(ByVal node As MSComctlLib.node) As Variant

    If node Is Nothing Then Exit Function
    
    DevolverDen = Right(node.Text, (Len(node.Text) - (Len(node.Tag) - 1)))
    
End Function


Private Sub tvwestrorg_Click()
'''
End Sub


''' <summary>
''' Nos indica si el parametro2 se encuentra en la cadena del parametro1
''' </summary>
''' <param name="sCadenaBuscar">Cadena donde se busca</param>
''' <param name="sCadenaABuscar">que queremos buscar en la cadenada param1</param>
''' <returns>Explicaci�n retorno de la funci�n</returns>
''' <remarks>Llamada desde:GenerarEstructuraOrg; Tiempo m�ximo:0seg.</remarks>
Function Buscar(ByVal sCadenaBuscar As String, ByVal sCadenaABuscar) As Boolean
    
    Dim bResultado As Boolean
    bResultado = False
    
    If InStr(1, sCadenaBuscar, sCadenaABuscar) > 0 Then
        bResultado = True
    
    End If
    Buscar = bResultado

End Function
