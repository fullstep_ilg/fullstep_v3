VERSION 5.00
Begin VB.Form frmESTRORGBuscar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar"
   ClientHeight    =   2070
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3105
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRORGBuscar.frx":0000
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2070
   ScaleWidth      =   3105
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.OptionButton optDep 
      BackColor       =   &H00808000&
      Caption         =   "Departamentos"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   270
      TabIndex        =   2
      Top             =   1080
      Width           =   2310
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   405
      TabIndex        =   3
      Top             =   1665
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   1545
      TabIndex        =   4
      Top             =   1665
      Width           =   1005
   End
   Begin VB.OptionButton optPer 
      BackColor       =   &H00808000&
      Caption         =   "Personas"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   270
      TabIndex        =   0
      Top             =   180
      Value           =   -1  'True
      Width           =   2400
   End
   Begin VB.OptionButton optUO 
      BackColor       =   &H00808000&
      Caption         =   "Unidades organizativas"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   270
      TabIndex        =   1
      Top             =   630
      Width           =   2310
   End
End
Attribute VB_Name = "frmESTRORGBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
    
    Me.Hide
    
    If optPer Then
        frmESTRORGBuscarPersona.Show 1
        Unload Me
    Else
        If optUO Then
            frmESTRORGBuscarUO.Show 1
        Else
            frmESTRORGBuscarDep.Show 1
        End If
        
    End If
    
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

If gParametrosGenerales.giNEO = 0 Then
    optUO.Visible = False
    optDep.Top = optUO.Top
    cmdAceptar.Top = cmdAceptar.Top - cmdAceptar.Height
    cmdCancelar.Top = cmdCancelar.Top - cmdCancelar.Height
    Me.Height = Me.Height - cmdCancelar.Height
End If

CargarRecursos

    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTROG_BUSCAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        optPer.caption = Ador(0).Value
        Ador.MoveNext
        optUO.caption = Ador(0).Value
        Ador.MoveNext
        optDep.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        
        Ador.Close
    
    End If

    Set Ador = Nothing



End Sub

