VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPARAsis 
   Caption         =   "Asistentes fijos a reuniones"
   ClientHeight    =   4035
   ClientLeft      =   1290
   ClientTop       =   2760
   ClientWidth     =   8490
   Icon            =   "frmPARAsis.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4035
   ScaleWidth      =   8490
   Begin VB.CommandButton cmdListado 
      Caption         =   "&Listado"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   3480
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   3660
      Width           =   1005
   End
   Begin VB.CommandButton cmdRestaurarPersona 
      Caption         =   "&Restaurar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2340
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   3660
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPersonas 
      Height          =   3525
      Left            =   60
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   60
      Width           =   8355
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   10
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPARAsis.frx":014A
      stylesets(1).Name=   "Selected"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   8388608
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmPARAsis.frx":0166
      AllowAddNew     =   -1  'True
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      ActiveRowStyleSet=   "Selected"
      SplitterPos     =   1
      SplitterVisible =   -1  'True
      Columns.Count   =   10
      Columns(0).Width=   1746
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   3122
      Columns(1).Caption=   "Apellidos"
      Columns(1).Name =   "APE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   2514
      Columns(2).Caption=   "Nombre"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2805
      Columns(3).Caption=   "Unidad organizativa"
      Columns(3).Name =   "UON"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   2037
      Columns(4).Caption=   "Departamento"
      Columns(4).Name =   "DEP"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   2064
      Columns(5).Caption=   "Cargo"
      Columns(5).Name =   "CAR"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   1931
      Columns(6).Caption=   "Tel�fono"
      Columns(6).Name =   "TFNO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   1879
      Columns(7).Caption=   "Tel�fono 2"
      Columns(7).Name =   "TFNO2"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   1693
      Columns(8).Caption=   "Fax"
      Columns(8).Name =   "FAX"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   2990
      Columns(9).Caption=   "Mail"
      Columns(9).Name =   "MAIL"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      _ExtentX        =   14737
      _ExtentY        =   6218
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdEliminarPersona 
      Caption         =   "&Eliminar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1200
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   3660
      Width           =   1005
   End
   Begin VB.CommandButton cmdA�adirPersona 
      Caption         =   "&A�adir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   60
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3660
      Width           =   1005
   End
End
Attribute VB_Name = "frmPARAsis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oPers As CPersonas
''' Listado de Firmas
Public bOrdenListadoDen As Boolean

Private sIdioma(1 To 2) As String
Private sIdiPersonas As String

Private Sub cmdA�adirPersona_Click()
    frmSELPer.bRDep = False
    frmSELPer.bRUO = False
    frmSELPer.caption = sIdioma(1)
    frmSELPer.lstvwPer.ColumnHeaders.Remove (2)
    frmSELPer.sOrigen = "frmPARASIS"
    frmSELPer.sdbcRolCod.Visible = False
    frmSELPer.sdbcRolDen.Visible = False
    frmSELPer.lblRol.Visible = False
    frmSELPer.lstvwPer.ColumnHeaders(1).Width = frmSELPer.lstvwPer.Width - 100
    frmSELPer.Show 1
End Sub

Private Sub cmdEliminarPersona_Click()
Dim aIdentificadores As Variant
Dim udtTeserror As TipoErrorSummit
Dim irespuesta As Integer
Dim oPersSel As CPersonas
Dim i As Integer

Set oPersSel = oFSGSRaiz.Generar_CPersonas
udtTeserror.NumError = TESnoerror
If sdbgPersonas.Rows <= 0 Then Exit Sub
        
Screen.MousePointer = vbHourglass
'iDifBoomark = 0
   
If sdbgPersonas.SelBookmarks.Count > 1 Then
   
     irespuesta = oMensajes.PreguntaEliminar(sIdiPersonas)
                
     If irespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
     End If
     
     i = 0
      
     While i < sdbgPersonas.SelBookmarks.Count
        sdbgPersonas.Bookmark = sdbgPersonas.SelBookmarks(i)
        oPersSel.Add sdbgPersonas.Columns(0).Value, sdbgPersonas.Columns(1).Value, sdbgPersonas.Columns(4).Value
        i = i + 1
     Wend
   
        
     udtTeserror = oPersSel.EliminarDeAsistentesFijosAReuniones
     If udtTeserror.NumError <> TESnoerror Then
             
         TratarError udtTeserror
         Screen.MousePointer = vbNormal
         Set oPersSel = Nothing
         Exit Sub
     
     Else
         sdbgPersonas.DeleteSelected
         DoEvents
     End If
            
Else
        
     sdbgPersonas.SelBookmarks.Add sdbgPersonas.Bookmark
     irespuesta = oMensajes.PreguntaEliminar(sIdioma(2) & " " & sdbgPersonas.Columns(1).Value & " (" & sdbgPersonas.Columns(2).Value & ")")
    
     If irespuesta = vbNo Then
         Screen.MousePointer = vbNormal
         Exit Sub
     End If
         
     oPersSel.Add sdbgPersonas.Columns(0).Value, sdbgPersonas.Columns(1).Value, sdbgPersonas.Columns(4).Value
     Screen.MousePointer = vbHourglass
     udtTeserror = oPersSel.EliminarDeAsistentesFijosAReuniones
        
     If udtTeserror.NumError <> TESnoerror Then
         basErrores.TratarError udtTeserror
         Screen.MousePointer = vbNormal
         Set oPersSel = Nothing
         Exit Sub
     End If
        
     sdbgPersonas.RemoveItem (sdbgPersonas.AddItemRowIndex(sdbgPersonas.Bookmark))
        
     basSeguridad.RegistrarAccion accionessummit.ACCProcePerEli, "Per:" & sdbgPersonas.Columns(0).Text
    
End If
    
sdbgPersonas.SelBookmarks.RemoveAll
Screen.MousePointer = vbNormal
End Sub

Private Sub cmdlistado_Click()
    AbrirLstParametros "frmPARAsis", bOrdenListadoDen
End Sub

Private Sub cmdRestaurarPersona_Click()
    Dim oPer As CPersona
    
    Screen.MousePointer = vbHourglass
    
    sdbgPersonas.RemoveAll
    
    oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorCod
    For Each oPer In oPers
        sdbgPersonas.AddItem oPer.Cod & Chr(m_lSeparador) & oPer.Apellidos & Chr(m_lSeparador) & oPer.nombre & Chr(m_lSeparador) & oPer.UON1 & CStr(" - " & oPer.UON2) & CStr(" - " & oPer.UON3) & Chr(m_lSeparador) & oPer.CodDep & Chr(m_lSeparador) & oPer.Cargo & Chr(m_lSeparador) & oPer.Tfno & Chr(m_lSeparador) & oPer.Tfno2 & Chr(m_lSeparador) & oPer.Fax & Chr(m_lSeparador) & oPer.mail
    Next
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PARASIS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sIdioma(1) = Ador(0).Value '1
        Ador.MoveNext
        sIdioma(2) = Ador(0).Value '2
        Ador.MoveNext
        cmdA�adirPersona.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminarPersona.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurarPersona.caption = Ador(0).Value
        Ador.MoveNext
        frmPARAsis.caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns(0).caption = Ador(0).Value '8 C�digo
        Ador.MoveNext
        sdbgPersonas.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns(2).caption = Ador(0).Value '10
        Ador.MoveNext
        sdbgPersonas.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns(5).caption = Ador(0).Value '
        Ador.MoveNext
        sdbgPersonas.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns(7).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns(8).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns(9).caption = Ador(0).Value
        Ador.MoveNext
        sIdiPersonas = Ador(0).Value

        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()

Dim oPer As CPersona
    
    Me.Width = 8610
    Me.Height = 4440
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    sdbgPersonas.Visible = True
    
    Set oPers = oFSGSRaiz.Generar_CPersonas
    
    Screen.MousePointer = vbHourglass
    oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorCod
    Screen.MousePointer = vbNormal
    
    For Each oPer In oPers
                     
        sdbgPersonas.AddItem oPer.Cod & Chr(m_lSeparador) & oPer.Apellidos & Chr(m_lSeparador) & oPer.nombre & Chr(m_lSeparador) & oPer.UON1 & CStr(" - " & oPer.UON2) & CStr(" - " & oPer.UON3) & Chr(m_lSeparador) & oPer.CodDep & Chr(m_lSeparador) & oPer.Cargo & Chr(m_lSeparador) & oPer.Tfno & Chr(m_lSeparador) & oPer.Tfno2 & Chr(m_lSeparador) & oPer.Fax & Chr(m_lSeparador) & oPer.mail
    
    Next
    
    bOrdenListadoDen = False

End Sub


Private Sub Form_Resize()
    If Me.Height < 1000 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    sdbgPersonas.Width = Me.Width - 200
    sdbgPersonas.Height = Me.Height - 900
    cmdA�adirPersona.Top = Me.Height - 800
    cmdEliminarPersona.Top = cmdA�adirPersona.Top
    cmdRestaurarPersona.Top = cmdA�adirPersona.Top
    cmdListado.Top = cmdA�adirPersona.Top
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Me.Visible = False
End Sub

Private Sub sdbgPersonas_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgPersonas_Click()
    
    sdbgPersonas.ActiveRowStyleSet = "Selected"
End Sub

Private Sub sdbgPersonas_HeadClick(ByVal ColIndex As Integer)
    
Dim oPer As CPersona
    
    Screen.MousePointer = vbHourglass
    
    sdbgPersonas.RemoveAll
        
    Select Case ColIndex
        
        Case 0 'Cod
                oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorCod
                bOrdenListadoDen = False
        Case 1 'Ape
                oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorApe
                bOrdenListadoDen = True
        Case 2  'Nom
                oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorNom
                bOrdenListadoDen = True
        Case 3  'Uo
                oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorUO
                bOrdenListadoDen = False
        Case 4 'Dep
                oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorDep
                bOrdenListadoDen = False
        Case 5 'Carg
                oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorCargo
                bOrdenListadoDen = False
        Case 6 'Tfno
                oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPortfno
                bOrdenListadoDen = False
        Case 7 'Tfno2
                oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPortfno2
                bOrdenListadoDen = False
        Case 8 'Fax
                oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorFax
                bOrdenListadoDen = False
        Case 9 'Mail
                oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPormail
                bOrdenListadoDen = False
    End Select
    
    For Each oPer In oPers
        sdbgPersonas.AddItem oPer.Cod & Chr(m_lSeparador) & oPer.Apellidos & Chr(m_lSeparador) & oPer.nombre & Chr(m_lSeparador) & oPer.UON1 & CStr(" - " & oPer.UON2) & CStr(" - " & oPer.UON3) & Chr(m_lSeparador) & oPer.CodDep & Chr(m_lSeparador) & oPer.Cargo & Chr(m_lSeparador) & oPer.Tfno & Chr(m_lSeparador) & oPer.Fax & Chr(m_lSeparador) & oPer.mail
    Next
    
    Screen.MousePointer = vbNormal

End Sub
Public Sub AnyadirPersonasSeleccionadas()
Dim teserror As TipoErrorSummit
Dim oPers As CPersonas
Dim i As Integer
    
    
    Set oPers = oFSGSRaiz.Generar_CPersonas
    
    For i = 1 To frmSELPer.lstvwPer.ListItems.Count
        
        oPers.Add frmSELPer.lstvwPer.ListItems(i).Tag, "", ""
    Next
    
    Screen.MousePointer = vbHourglass
    teserror = oPers.convertirenasistentesfijosareuniones
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oPers = Nothing
        cmdRestaurarPersona_Click
        Exit Sub
    End If
    
    cmdRestaurarPersona_Click
      
    
End Sub

Private Sub sdbgPersonas_KeyDown(KeyCode As Integer, Shift As Integer)

''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: poder eliminar desde el teclado

If KeyCode = vbKeyDelete Then
    
    cmdEliminarPersona_Click
    
End If

End Sub

