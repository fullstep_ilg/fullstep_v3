VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCatalogoBuscar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar categorias"
   ClientHeight    =   3720
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6825
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCatalogoBuscar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3720
   ScaleWidth      =   6825
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   3465
      TabIndex        =   5
      Top             =   3345
      Width           =   1290
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1905
      TabIndex        =   4
      Top             =   3345
      Width           =   1290
   End
   Begin VB.CommandButton cmdCargar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6390
      Picture         =   "frmCatalogoBuscar.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Cargar"
      Top             =   450
      Width           =   315
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      Height          =   645
      Left            =   90
      ScaleHeight     =   585
      ScaleWidth      =   6105
      TabIndex        =   6
      Top             =   90
      Width           =   6165
      Begin VB.TextBox txtCod 
         Height          =   285
         Left            =   780
         MaxLength       =   50
         TabIndex        =   0
         Top             =   120
         Width           =   1155
      End
      Begin VB.TextBox txtDen 
         Height          =   285
         Left            =   3300
         MaxLength       =   150
         TabIndex        =   1
         Top             =   120
         Width           =   2745
      End
      Begin VB.Label lblDen 
         BackColor       =   &H00808000&
         Caption         =   "Denominación:"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   2025
         TabIndex        =   8
         Top             =   180
         Width           =   1365
      End
      Begin VB.Label lblCod 
         BackColor       =   &H00808000&
         Caption         =   "Código:"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   45
         TabIndex        =   7
         Top             =   180
         Width           =   705
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCategorias 
      Height          =   2400
      Left            =   120
      TabIndex        =   3
      Top             =   900
      Width           =   6570
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   6
      stylesets.count =   2
      stylesets(0).Name=   "ActiveRow"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   8421376
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCatalogoBuscar.frx":01D6
      stylesets(1).Name=   "ActiveRowBlue"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   8388608
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCatalogoBuscar.frx":01F2
      AllowUpdate     =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "ActiveRowBlue"
      Columns.Count   =   6
      Columns(0).Width=   1217
      Columns(0).Caption=   "Nivel1"
      Columns(0).Name =   "CAT1"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16449500
      Columns(1).Width=   1217
      Columns(1).Caption=   "Nivel2"
      Columns(1).Name =   "CAT2"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16449500
      Columns(2).Width=   1217
      Columns(2).Caption=   "Nivel3"
      Columns(2).Name =   "CAT3"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16449500
      Columns(3).Width=   1217
      Columns(3).Caption=   "Nivel4"
      Columns(3).Name =   "CAT4"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16449500
      Columns(4).Width=   1217
      Columns(4).Caption=   "Nivel5"
      Columns(4).Name =   "CAT5"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   16449500
      Columns(5).Width=   5398
      Columns(5).Caption=   "Denominación"
      Columns(5).Name =   "DEN"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   11589
      _ExtentY        =   4233
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCatalogoBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oCategoriasN1 As CCategoriaSN1
Private oCategoriasN2 As CCategoriasN2
Private oCategoriasN3 As CCategoriasN3
Private oCategoriasN4 As CCategoriasN4
Private oCategoriasN5 As CCategoriasN5

Public bRUsuAprov As Boolean

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO_BUSCAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        lblCod.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblDen.caption = Ador(0).Value & ":"
        sdbgCategorias.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCategorias.Columns("CAT1").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCategorias.Columns("CAT2").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCategorias.Columns("CAT3").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCategorias.Columns("CAT4").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCategorias.Columns("CAT5").caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing

End Sub

Private Sub cmdAceptar_Click()
    Dim nodx As MSComctlLib.node
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    'Dim i As Integer
    Dim j As Integer
    On Error GoTo NoSeEncuentra
    
    If Trim(sdbgCategorias.Columns("CAT5").Value) <> "" Then
        j = 5
    Else
        If Trim(sdbgCategorias.Columns("CAT4").Value) <> "" Then
            j = 4
        Else
            If Trim(sdbgCategorias.Columns("CAT3").Value) <> "" Then
                j = 3
            Else
                If Trim(sdbgCategorias.Columns("CAT2").Value) <> "" Then
                    j = 2
                Else
                    If Trim(sdbgCategorias.Columns("CAT1").Value) <> "" Then
                        j = 1
                    End If
                End If
            End If
        End If
    End If
    
    
    Select Case j
    
    Case 1
    
        scod1 = sdbgCategorias.Columns("CAT1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(sdbgCategorias.Columns("CAT1").Value))
        
        Set nodx = frmCatalogo.tvwCategorias.Nodes("CAT1" & scod1)
        nodx.Selected = True
        nodx.EnsureVisible
    
    Case 2
    
        scod1 = sdbgCategorias.Columns("CAT1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(sdbgCategorias.Columns("CAT1").Value))
        scod2 = sdbgCategorias.Columns("CAT2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(sdbgCategorias.Columns("CAT2").Value))
        Set nodx = frmCatalogo.tvwCategorias.Nodes("CAT2" & scod1 & scod2)
        nodx.Selected = True
        nodx.EnsureVisible
    
    Case 3
    
        scod1 = sdbgCategorias.Columns("CAT1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(sdbgCategorias.Columns("CAT1").Value))
        scod2 = sdbgCategorias.Columns("CAT2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(sdbgCategorias.Columns("CAT2").Value))
        scod3 = sdbgCategorias.Columns("CAT3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(sdbgCategorias.Columns("CAT3").Value))
    
        Set nodx = frmCatalogo.tvwCategorias.Nodes("CAT3" & scod1 & scod2 & scod3)
        nodx.Selected = True
        nodx.EnsureVisible
    
    Case 4
    
        scod1 = sdbgCategorias.Columns("CAT1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(sdbgCategorias.Columns("CAT1").Value))
        scod2 = sdbgCategorias.Columns("CAT2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(sdbgCategorias.Columns("CAT2").Value))
        scod3 = sdbgCategorias.Columns("CAT3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(sdbgCategorias.Columns("CAT3").Value))
        scod4 = sdbgCategorias.Columns("CAT4").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(sdbgCategorias.Columns("CAT4").Value))
        
        Set nodx = frmCatalogo.tvwCategorias.Nodes("CAT4" & scod1 & scod2 & scod3 & scod4)
        nodx.Selected = True
        nodx.EnsureVisible
    
    Case 5
        scod1 = sdbgCategorias.Columns("CAT1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(sdbgCategorias.Columns("CAT1").Value))
        scod2 = sdbgCategorias.Columns("CAT2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(sdbgCategorias.Columns("CAT2").Value))
        scod3 = sdbgCategorias.Columns("CAT3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(sdbgCategorias.Columns("CAT3").Value))
        scod4 = sdbgCategorias.Columns("CAT4").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(sdbgCategorias.Columns("CAT4").Value))
        scod5 = sdbgCategorias.Columns("CAT5").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(sdbgCategorias.Columns("CAT5").Value))
    
        If sdbgCategorias.Columns("CAT5").Value = "" Then Exit Sub
        Set nodx = frmCatalogo.tvwCategorias.Nodes("CAT5" & scod1 & scod2 & scod3 & scod4 & scod5)
        nodx.Selected = True
        nodx.EnsureVisible
    
    End Select
    frmCatalogo.tvwCategorias_NodeClick nodx
    frmCatalogo.tabCatalogo.TabVisible(1) = True
    
    Unload Me
    If frmCatalogo.Visible Then frmCatalogo.tvwCategorias.SetFocus
    Exit Sub

NoSeEncuentra:
    oMensajes.CategoriaNueva
    Unload Me

End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub


Private Sub cmdCargar_Click()
Dim RS As Ador.Recordset

    Screen.MousePointer = vbHourglass

    sdbgCategorias.RemoveAll
    
    Set oCategoriasN1 = oFSGSRaiz.Generar_CCategoriasN1
    If bRUsuAprov Then
        Set RS = oCategoriasN1.DevolverTodasLasCategorias(Trim(txtCod.Text), Trim(txtDen.Text), , , oUsuarioSummit.Persona.Cod, frmCatalogo.chkVerBajas.Value)
    Else
        Set RS = oCategoriasN1.DevolverTodasLasCategorias(Trim(txtCod.Text), Trim(txtDen.Text), , , , frmCatalogo.chkVerBajas.Value)
    End If
    If Not RS Is Nothing Then
        While Not RS.EOF
            sdbgCategorias.AddItem RS("COD").Value & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & RS("DEN").Value
            RS.MoveNext
        Wend
    End If
    
    Set oCategoriasN2 = oFSGSRaiz.Generar_CCategoriasN2
    If bRUsuAprov Then
        Set RS = oCategoriasN2.DevolverTodasLasCategorias(Trim(txtCod.Text), Trim(txtDen.Text), , , oUsuarioSummit.Persona.Cod, frmCatalogo.chkVerBajas.Value)
    Else
        Set RS = oCategoriasN2.DevolverTodasLasCategorias(Trim(txtCod.Text), Trim(txtDen.Text), , , , frmCatalogo.chkVerBajas.Value)
    End If
    If Not RS Is Nothing Then
        While Not RS.EOF
            sdbgCategorias.AddItem RS("CAT1").Value & Chr(m_lSeparador) & RS("COD").Value & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & Chr(m_lSeparador) & RS("DEN").Value
            RS.MoveNext
        Wend
    End If
    
    Set oCategoriasN3 = oFSGSRaiz.Generar_CCategoriasN3
    If bRUsuAprov Then
        Set RS = oCategoriasN3.DevolverTodasLasCategorias(Trim(txtCod.Text), Trim(txtDen.Text), , , oUsuarioSummit.Persona.Cod, frmCatalogo.chkVerBajas.Value)
    Else
        Set RS = oCategoriasN3.DevolverTodasLasCategorias(Trim(txtCod.Text), Trim(txtDen.Text), , , , frmCatalogo.chkVerBajas.Value)
    End If
    If Not RS Is Nothing Then
        While Not RS.EOF
            sdbgCategorias.AddItem RS("CAT1").Value & Chr(m_lSeparador) & RS("CAT2").Value & Chr(m_lSeparador) & RS("COD").Value & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & RS("DEN").Value
            RS.MoveNext
        Wend
    End If
    
    Set oCategoriasN4 = oFSGSRaiz.Generar_CCategoriasN4
    If bRUsuAprov Then
        Set RS = oCategoriasN4.DevolverTodasLasCategorias(Trim(txtCod.Text), Trim(txtDen.Text), , , oUsuarioSummit.Persona.Cod, frmCatalogo.chkVerBajas.Value)
    Else
        Set RS = oCategoriasN4.DevolverTodasLasCategorias(Trim(txtCod.Text), Trim(txtDen.Text), , , , frmCatalogo.chkVerBajas.Value)
    End If
    If Not RS Is Nothing Then
        While Not RS.EOF
            sdbgCategorias.AddItem RS("CAT1").Value & Chr(m_lSeparador) & RS("CAT2").Value & Chr(m_lSeparador) & RS("CAT3").Value & Chr(m_lSeparador) & RS("COD").Value & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & RS("DEN").Value
            RS.MoveNext
        Wend
    End If
    
    Set oCategoriasN5 = oFSGSRaiz.Generar_CCategoriasN5
    If bRUsuAprov Then
        Set RS = oCategoriasN5.DevolverTodasLasCategorias(Trim(txtCod.Text), Trim(txtDen.Text), , , oUsuarioSummit.Persona.Cod, frmCatalogo.chkVerBajas.Value)
    Else
        Set RS = oCategoriasN5.DevolverTodasLasCategorias(Trim(txtCod.Text), Trim(txtDen.Text), , , , frmCatalogo.chkVerBajas.Value)
    End If
    If Not RS Is Nothing Then
        While Not RS.EOF
            sdbgCategorias.AddItem RS("CAT1").Value & Chr(m_lSeparador) & RS("CAT2").Value & Chr(m_lSeparador) & RS("CAT3").Value & Chr(m_lSeparador) & RS("CAT4").Value & Chr(m_lSeparador) & RS("COD").Value & Chr(m_lSeparador) & RS("DEN").Value
            RS.MoveNext
        Wend
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub Form_Load()
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
    CargarRecursos
    
    PonerFieldSeparator Me
End Sub
