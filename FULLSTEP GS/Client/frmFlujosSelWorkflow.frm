VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFlujosSelWorkflow 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DConfigurar Flujo de Trabajo"
   ClientHeight    =   3300
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7905
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFlujosSelWorkflow.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3300
   ScaleWidth      =   7905
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.OptionButton optWorkflow 
      BackColor       =   &H00808000&
      Caption         =   "DDejar de usar el flujo de trabajo actual"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Index           =   3
      Left            =   240
      TabIndex        =   10
      Top             =   1320
      Value           =   -1  'True
      Width           =   5175
   End
   Begin VB.CommandButton cmdEliminarWorkflow 
      Height          =   312
      Left            =   7380
      Picture         =   "frmFlujosSelWorkflow.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   960
      Visible         =   0   'False
      Width           =   432
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcWorkflows 
      Height          =   285
      Left            =   3960
      TabIndex        =   1
      Top             =   960
      Width           =   3375
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   8493
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "HUERFANO"
      Columns(2).Name =   "HUERFANO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   11
      Columns(2).FieldLen=   256
      _ExtentX        =   5953
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      Enabled         =   0   'False
   End
   Begin VB.OptionButton optWorkflow 
      BackColor       =   &H00808000&
      Caption         =   "DCrear un nuevo flujo de trabajo basado en:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   8
      Top             =   960
      Width           =   5175
   End
   Begin VB.TextBox txtNombre 
      Height          =   285
      Left            =   1680
      TabIndex        =   4
      Top             =   2280
      Width           =   3720
   End
   Begin VB.OptionButton optWorkflow 
      BackColor       =   &H00808000&
      Caption         =   "DUsar flujo de trabajo actual."
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   5175
   End
   Begin VB.OptionButton optWorkflow 
      BackColor       =   &H00808000&
      Caption         =   "DCrear un nuevo flujo de trabajo desde cero."
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   2
      Top             =   600
      Width           =   5175
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   2280
      ScaleHeight     =   420
      ScaleWidth      =   2625
      TabIndex        =   7
      Top             =   2760
      Width           =   2625
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   6
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   5
         Top             =   60
         Width           =   1050
      End
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      X1              =   240
      X2              =   7200
      Y1              =   1800
      Y2              =   1800
   End
   Begin VB.Label lblNombre 
      BackColor       =   &H00808000&
      Caption         =   "DNombre"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1680
      TabIndex        =   3
      Top             =   1920
      Width           =   3720
   End
End
Attribute VB_Name = "frmFlujosSelWorkflow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private oWorkflows As CWorkflows
Private oWork As cworkflow
Private oWorkActual As cworkflow
Private m_oIBaseDatos As IBaseDatos
Private m_oDenominacionesBloquePeticionario As CMultiidiomas
Private m_oDenominacionesBloqueFin As CMultiidiomas

Public lIdWorkflow_Act As Long
Public lIdWorkflow_Sel As Long
Public lIdFormulario As Long
Public sTipoWorkflow As String
Public CSN1 As Long
Public CSN2 As Long
Public CSN3 As Long
Public CSN4 As Long
Public sSolicitud As String
Public m_bModifFlujo As Boolean
Public g_lSolicitud As Long
Public g_lSolicitudTipo As Integer

Public g_iSolicitudTipoTipo As Integer 'Solicitud Compra, Contrato...NoConformidad, Certificado

''' <summary>
''' Llama a los formularios correspondientes a la accion
''' </summary>
''' <remarks>Tiempo m�ximo:0,2seg.</remarks>
Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim bOk As Boolean
    Dim oBloque As CBloque
    Dim oWorFlows As CWorkflows
    bOk = False
    If optWorkflow(3).Value Then
        DesasociarFlujoDeSolicitud
        bOk = True
    ElseIf txtNombre.Text <> "" Then
        If optWorkflow(0).Value Then
            If Not oWorkActual Is Nothing Then
                Set oWork = oWorkActual
                If oWork.Den <> txtNombre.Text Then
                    oWork.Den = txtNombre.Text
                    Set m_oIBaseDatos = oWork
                    teserror = m_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oWork = Nothing
                        Set m_oIBaseDatos = Nothing
                        Exit Sub
                    End If
                    Set m_oIBaseDatos = Nothing
                End If
                
                Select Case sTipoWorkflow
                    Case "ALTA"
                        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells.Item("WORKFL").Value = oWork.Den
                    Case "MODIF"
                        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells.Item("WORKFL_MODIF").Value = oWork.Den
                    Case "BAJA"
                        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells.Item("WORKFL_BAJA").Value = oWork.Den
                End Select
                                
                bOk = True
            End If
        Else
            If optWorkflow(1).Value Then
                'Crear un Nuevo WorkFlow en BD
                'Pasar el ID y la DEN
                Set oWork = oFSGSRaiz.Generar_CWorkflow
                oWork.Den = txtNombre.Text
                            
                Set m_oIBaseDatos = oWork
                teserror = m_oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set oWork = Nothing
                    Set m_oIBaseDatos = Nothing
                    Exit Sub
                End If
                
                'ahora creamos los Bloques PETICIONARIO y FIN que deben tener todos los workflows
                Set oBloque = oFSGSRaiz.Generar_CBloque
                oBloque.Workflow = oWork.Id
                Set oBloque.Denominaciones = m_oDenominacionesBloquePeticionario
                oBloque.Tipo = TipoBloque.Peticionario
                oBloque.Top = 0
                oBloque.Left = 0
                oBloque.Height = 500
                oBloque.Width = 1500
                
                Set m_oIBaseDatos = oBloque
                teserror = m_oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set oBloque = Nothing
                    Set m_oIBaseDatos = Nothing
                    Exit Sub
                End If
                
                Set oBloque = oFSGSRaiz.Generar_CBloque
                oBloque.Workflow = oWork.Id
                Set oBloque.Denominaciones = m_oDenominacionesBloqueFin
                oBloque.Tipo = TipoBloque.final
                oBloque.Top = 1000
                oBloque.Left = 0
                oBloque.Height = 500
                oBloque.Width = 1500
                
                Set m_oIBaseDatos = oBloque
                teserror = m_oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set oBloque = Nothing
                    Set m_oIBaseDatos = Nothing
                    Exit Sub
                End If
                
                Select Case sTipoWorkflow
                    Case "ALTA"
                        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("ID_WORKF").Value = oWork.Id
                        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells.Item("WORKFL").Value = oWork.Den
                    Case "MODIF"
                        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("ID_WORKF_MODIF").Value = oWork.Id
                        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells.Item("WORKFL_MODIF").Value = oWork.Den
                    Case "BAJA"
                        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("ID_WORKF_BAJA").Value = oWork.Id
                        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells.Item("WORKFL_BAJA").Value = oWork.Den
                End Select
                Set oBloque = Nothing
                Set m_oIBaseDatos = Nothing
                bOk = True
            Else
                'Copiar el WorkFlow entero (Bloques, enlaces,....) creando uno nuevo
                If lIdWorkflow_Sel > 0 Then
                    oMensajes.ConfigurarCopiaDeWorkFlow
                    Set oWork = oFSGSRaiz.Generar_CWorkflow
                    oWork.Id = lIdWorkflow_Sel
                                
                    teserror = oWork.CopiarWorkflow(txtNombre.Text, lIdFormulario)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oWork = Nothing
                        Exit Sub
                    End If
                    
                    Select Case sTipoWorkflow
                        Case "ALTA"
                            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("ID_WORKF").Value = oWork.Id
                            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells.Item("WORKFL").Value = oWork.Den
                        Case "MODIF"
                            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("ID_WORKF_MODIF").Value = oWork.Id
                            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells.Item("WORKFL_MODIF").Value = oWork.Den
                        Case "BAJA"
                            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("ID_WORKF_BAJA").Value = oWork.Id
                            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells.Item("WORKFL_BAJA").Value = oWork.Den
                    End Select
                End If
                bOk = True
            End If
            If bOk Then
                Set oWorFlows = oFSGSRaiz.generar_CWorkflows
                teserror = oWorFlows.Crear_Copia(oWork.Id)
                If teserror.NumError <> TESnoerror Then
                    Set oWorFlows = Nothing
                    Exit Sub
                End If
                Set oWorFlows = Nothing
            End If
        End If
    End If
    
    If bOk Then
        If frmSOLConfiguracion.picEdicion.Visible Then
            Me.Hide
            frmSOLConfiguracion.cmdAceptar_Click
        Else
            If optWorkflow(3).Value Then
                frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("FECACT").Value = frmSOLConfiguracion.g_oSolicitudSeleccionada.FECACT
                frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells.Item("WORKFL").Value = ""
                frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("ID_WORKF").Value = Null
                Me.Hide
            End If
            frmSOLConfiguracion.ssSOLConfig.Update
        End If
        If Not oWorkActual Is Nothing And Not optWorkflow(0).Value Then
            'Si tenemos un workflow actual y vamos a crear uno nuevo
            'Primero miramos que el workflow no exista en ninguna instancia
            If oWorkActual.EnUsoPorInstancia Then
                'Preguntamos si borramos el anterior
                If oMensajes.PreguntaEliminarWorkflow(oWorkActual.Den) = vbYes Then
                    'Borrar el workflow actual
                    Set m_oIBaseDatos = oWorkActual
                    teserror = m_oIBaseDatos.FinalizarEdicionEliminando
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oWorkActual = Nothing
                        Set m_oIBaseDatos = Nothing
                        Exit Sub
                    End If
                    Set m_oIBaseDatos = Nothing
                End If
            End If
        End If
        If Not frmSOLConfiguracion.g_oSolicitudSeleccionada Is Nothing And Not optWorkflow(3).Value Then
            frmFlujos.m_bModifFlujo = m_bModifFlujo
            frmFlujos.m_lIdFlujo = oWork.Id
            frmFlujos.m_lIdFormulario = lIdFormulario
            frmFlujos.m_sSolicitud = sSolicitud
            frmFlujos.g_lSolicitud = g_lSolicitud
            frmFlujos.g_lSolicitudTipo = g_lSolicitudTipo
            frmFlujos.g_iSolicitudTipoTipo = g_iSolicitudTipoTipo
            frmFlujos.lblDenFlujo.caption = oWork.Den
            frmFlujos.g_sTipoWorkflow = sTipoWorkflow
            Me.Hide
            frmFlujos.Show vbModal
        End If
    End If
    
    Set oWork = Nothing
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdEliminarWorkflow_Click()
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit

    If lIdWorkflow_Sel > 0 Then
        If oMensajes.PreguntaEliminar(sdbcWorkflows.Text) = vbYes Then
            Set oWork = oFSGSRaiz.Generar_CWorkflow
            oWork.Id = lIdWorkflow_Sel
                        
            Set oIBaseDatos = oWork
            teserror = oIBaseDatos.FinalizarEdicionEliminando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oWork = Nothing
                Exit Sub
            End If
            lIdWorkflow_Sel = 0
            sdbcWorkflows.Text = ""
        End If
    End If
End Sub

Private Sub Form_Load()
    Me.Top = frmSOLConfiguracion.Top + (frmSOLConfiguracion.Height / 2) - (Me.Height / 2)
    If Me.Top < 0 Then
        Me.Top = 0
    End If
    Me.Left = frmSOLConfiguracion.Left + (frmSOLConfiguracion.Width / 2) - (Me.Width / 2)
    If Me.Left < 0 Then
        Me.Left = 0
    End If
    If g_iSolicitudTipoTipo <> TipoSolicitud.NoConformidades Then
        optWorkflow(3).Visible = False
    End If

    CargarRecursos
    
    PonerFieldSeparator Me
    
    CargarWorkflowActual
    
End Sub

Private Sub CargarRecursos()
   Dim Ador As Ador.Recordset
   Dim oIdiomas As CIdiomas
   Dim oIdioma As CIdioma
   Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSSELWORKFLOW, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value '1
        Ador.MoveNext
        optWorkflow(0).caption = Ador(0).Value
        Ador.MoveNext
        optWorkflow(1).caption = Ador(0).Value
        Ador.MoveNext
        optWorkflow(2).caption = Ador(0).Value
        Ador.MoveNext
        lblNombre.caption = Ador(0).Value '5
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminarWorkflow.ToolTipText = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        optWorkflow(3).caption = Ador(0).Value
                
        Ador.Close
    End If
    
    
    Set m_oDenominacionesBloquePeticionario = oFSGSRaiz.Generar_CMultiidiomas
    Set m_oDenominacionesBloqueFin = oFSGSRaiz.Generar_CMultiidiomas
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    For Each oIdioma In oIdiomas
        Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSSELWORKFLOW, oIdioma.Cod)
        If Not Ador Is Nothing Then
            For i = 1 To 8
                Ador.MoveNext
            Next
            m_oDenominacionesBloquePeticionario.Add oIdioma.Cod, Ador(0).Value
            Ador.MoveNext
            m_oDenominacionesBloqueFin.Add oIdioma.Cod, Ador(0).Value
            Ador.Close
        End If
    Next
End Sub

Private Sub CargarWorkflowActual()
    Dim teserror As TipoErrorSummit

    If lIdWorkflow_Act > 0 Then
        optWorkflow(0).Enabled = True
        optWorkflow(0).Value = True
        Set oWorkActual = oFSGSRaiz.Generar_CWorkflow
        oWorkActual.Id = lIdWorkflow_Act
        Set m_oIBaseDatos = oWorkActual
        teserror = m_oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oWorkActual = Nothing
            Set m_oIBaseDatos = Nothing
            Exit Sub
        End If
        Set m_oIBaseDatos = Nothing
        txtNombre.Text = oWorkActual.Den
    Else
        optWorkflow(0).Enabled = False
        optWorkflow(1).Value = True
    End If
End Sub


Private Sub Form_Unload(Cancel As Integer)
    lIdWorkflow_Sel = 0
    lIdWorkflow_Act = 0
    g_lSolicitud = 0
    Set oWork = Nothing
    Set oWorkActual = Nothing
    Set oWorkflows = Nothing
    Set m_oIBaseDatos = Nothing
    sdbcWorkflows.RemoveAll
End Sub

Private Sub sdbcWorkflows_CloseUp()
    If sdbcWorkflows.Value = "" Then
        lIdWorkflow_Sel = 0
    Else
        lIdWorkflow_Sel = sdbcWorkflows.Value
    End If
   
End Sub

Private Sub sdbcWorkflows_DropDown()
    CargarDropDownWorkflows
End Sub

Private Sub CargarDropDownWorkflows()
    Screen.MousePointer = vbHourglass

    Set oWorkflows = oFSGSRaiz.generar_CWorkflows

    sdbcWorkflows.RemoveAll

    oWorkflows.CargarWorkflowsCarpetaSolicitudYHuerfanos CSN1, CSN2, CSN3, CSN4
    For Each oWork In oWorkflows
        sdbcWorkflows.AddItem oWork.Id & Chr(m_lSeparador) & oWork.Den & Chr(m_lSeparador) & True
    Next
    
    Set oWorkflows = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcWorkflows_InitColumnProps()
    sdbcWorkflows.DataFieldList = "Column 0"
    sdbcWorkflows.DataFieldToDisplay = "Column 1"

End Sub

Private Sub sdbcWorkflows_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcWorkflows.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcWorkflows.Rows - 1
            bm = sdbcWorkflows.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcWorkflows.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcWorkflows.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub optWorkFlow_Click(Index As Integer)
    sdbcWorkflows.Enabled = optWorkflow(2).Value
    If optWorkflow(0).Value Or optWorkflow(3).Value Then
        If Not oWorkActual Is Nothing Then
            txtNombre.Text = oWorkActual.Den
        End If
    Else
        txtNombre.Text = ""
    End If
End Sub

Private Sub DesasociarFlujoDeSolicitud()

    If lIdWorkflow_Act > 0 Then
        frmSOLConfiguracion.g_oSolicitudSeleccionada.DesasociarFlujoDeSolicitud lIdWorkflow_Act
    End If
End Sub
