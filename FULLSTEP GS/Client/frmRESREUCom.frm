VERSION 5.00
Begin VB.Form frmRESREUCom 
   BackColor       =   &H00808000&
   Caption         =   "Comentario de reuni�n"
   ClientHeight    =   2790
   ClientLeft      =   2565
   ClientTop       =   4845
   ClientWidth     =   5865
   Icon            =   "frmRESREUCom.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2790
   ScaleWidth      =   5865
   Begin VB.TextBox txtComActual 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   60
      MaxLength       =   4000
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   60
      Width           =   5715
   End
   Begin VB.TextBox lblComAnterior 
      BackColor       =   &H80000018&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   60
      Locked          =   -1  'True
      MaxLength       =   4000
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   1380
      Width           =   5715
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2820
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   2400
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1620
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2400
      Width           =   1005
   End
   Begin VB.Label lblCom 
      BackColor       =   &H00808000&
      Caption         =   "Comentario anterior"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   60
      TabIndex        =   0
      Top             =   1080
      Width           =   2370
   End
End
Attribute VB_Name = "frmRESREUCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private bError As Boolean
Private bChange As Boolean
Private m_bSoloLectura As Boolean

Public Property Get SoloLectura() As Boolean
    SoloLectura = m_bSoloLectura
End Property
Public Property Let SoloLectura(ByVal vNewValue As Boolean)
    m_bSoloLectura = vNewValue
End Property

Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit
Dim vbm As Variant
Dim i As Integer

    bChange = False
    
    Screen.MousePointer = vbHourglass
    If frmRESREU.m_bModif Then
        teserror = frmRESREU.m_oReuSeleccionada.FijarNuevoComentario(frmRESREU.sdbcProceCod.Columns("ANYO").Value, frmRESREU.sdbcProceCod.Columns("GMN1").Text, frmRESREU.sdbcProceCod.Columns("COD").Value, txtComActual)
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            bError = True
            Exit Sub
        Else
            bError = False
            frmRESREU.CargarComboProcesos
            frmRESREU.sdbcProceCod.BackColor = 10867194
            frmRESREU.lblHoraReu.BackColor = 10867194
            Unload Me
        End If
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdCancelar_Click()
    bChange = False
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarRecursos
    HabilitarBotones
End Sub

''' <summary>Funci�n para ocultar/mostrar botones</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 07/06/2012</revision>

Public Function HabilitarBotones()
    Dim bSoloLectura As Boolean
    
    bSoloLectura = m_bSoloLectura Or (Not m_bSoloLectura And frmRESREU.sdbcProceCod.Columns("INVI").Value)
            
    txtComActual.Locked = bSoloLectura
    cmdAceptar.Visible = Not bSoloLectura
    cmdCancelar.Visible = Not bSoloLectura
End Function

Private Sub Form_Resize()
    On Error Resume Next
    'Me.picComen.Width = Me.Width - 270
    'Me.picComen.Height = (Me.Height - Me.cmdAceptar.Height - 540) / 2
    Me.txtComActual.Width = Me.Width - 270
    Me.txtComActual.Height = (Me.Height - Me.cmdAceptar.Height - 540) / 2
    
    Me.lblCom.Top = Me.txtComActual.Top + Me.txtComActual.Height + 115
    Me.lblComAnterior.Top = Me.lblCom.Top + Me.lblCom.Height
    Me.lblComAnterior.Height = (Me.Height - Me.cmdAceptar.Height - 1380) / 2
    Me.lblComAnterior.Width = Me.Width - 270
    
    Me.cmdAceptar.Top = lblComAnterior.Top + lblComAnterior.Height + 50
    Me.cmdCancelar.Top = cmdAceptar.Top
    Me.cmdAceptar.Left = Me.ScaleWidth / 2 - 2190 / 2
    Me.cmdCancelar.Left = Me.ScaleWidth / 2 + 2190 / 2 - Me.cmdAceptar.Width
End Sub

Private Sub Form_Unload(Cancel As Integer)

If bChange Then
    bError = False
    cmdAceptar_Click
    If bError Then
        Cancel = True
    End If
End If

End Sub

Private Sub txtComActual_Change()
bChange = True
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_RESREUCOM, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        
    caption = ador(0).Value
    ador.MoveNext
    lblCom.caption = ador(0).Value
    ador.MoveNext
    cmdAceptar.caption = ador(0).Value
    ador.MoveNext
    cmdCancelar.caption = ador(0).Value
    
    ador.Close
    
    End If

    Set ador = Nothing

End Sub
