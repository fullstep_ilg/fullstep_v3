VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCatalogoAtribEspYAdj 
   Caption         =   "Form1"
   ClientHeight    =   8505
   ClientLeft      =   1290
   ClientTop       =   2775
   ClientWidth     =   9735
   Icon            =   "frmCatalogoAtribEspYAdj.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8505
   ScaleWidth      =   9735
   Begin VB.Frame Frame1 
      Height          =   8295
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9495
      Begin VB.CommandButton cmdEliminarCampo 
         Caption         =   "DEliminar"
         Height          =   375
         Left            =   8160
         TabIndex        =   16
         Top             =   7800
         Width           =   1095
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
         Height          =   360
         Left            =   4920
         TabIndex        =   15
         Top             =   6960
         Width           =   3015
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogoAtribEspYAdj.frx":0CB2
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "VALOR"
         Columns(0).Name =   "VALOR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DESC"
         Columns(1).Name =   "DESC"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5318
         _ExtentY        =   635
         _StockProps     =   77
      End
      Begin VB.CommandButton cmdAnyadirCampo 
         Caption         =   "DA�adir"
         Height          =   375
         Left            =   6840
         TabIndex        =   14
         Top             =   7800
         Width           =   1095
      End
      Begin VB.PictureBox Picture3 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   6960
         ScaleHeight     =   315
         ScaleWidth      =   2355
         TabIndex        =   6
         Top             =   5040
         Width           =   2355
         Begin VB.CommandButton cmdModificarAdj 
            Height          =   300
            Left            =   960
            Picture         =   "frmCatalogoAtribEspYAdj.frx":0CCE
            Style           =   1  'Graphical
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAnyadirAdj 
            Height          =   300
            Left            =   0
            Picture         =   "frmCatalogoAtribEspYAdj.frx":0E18
            Style           =   1  'Graphical
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdEliminarAdj 
            Height          =   300
            Left            =   480
            Picture         =   "frmCatalogoAtribEspYAdj.frx":0E79
            Style           =   1  'Graphical
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdSalvarAdj 
            Height          =   300
            Left            =   1440
            Picture         =   "frmCatalogoAtribEspYAdj.frx":0EFF
            Style           =   1  'Graphical
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAbrirAdj 
            Height          =   300
            Left            =   1905
            Picture         =   "frmCatalogoAtribEspYAdj.frx":0F80
            Style           =   1  'Graphical
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
      End
      Begin VB.CommandButton cmdAnyadirEsp 
         Caption         =   "DA�adir"
         Height          =   375
         Left            =   8160
         TabIndex        =   5
         Top             =   2400
         Width           =   1095
      End
      Begin VB.TextBox txtArticuloEsp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000012&
         Height          =   1785
         Index           =   2
         Left            =   120
         MaxLength       =   2000
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   3
         Top             =   480
         Width           =   9180
      End
      Begin MSComctlLib.ListView lstvwArticuloEsp 
         Height          =   2025
         Left            =   120
         TabIndex        =   4
         Top             =   2880
         Width           =   9195
         _ExtentX        =   16219
         _ExtentY        =   3572
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   3233
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Comentario"
            Object.Width           =   6920
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha"
            Object.Width           =   2868
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgCamposEsp 
         Height          =   2130
         Left            =   120
         TabIndex        =   13
         Top             =   5520
         Width           =   9255
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   11
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16777215
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogoAtribEspYAdj.frx":0FFC
         stylesets(1).Name=   "Selected"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8388608
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCatalogoAtribEspYAdj.frx":1018
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   -2147483630
         ForeColorOdd    =   -2147483630
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         Columns.Count   =   11
         Columns(0).Width=   2090
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16776960
         Columns(1).Width=   4445
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16776960
         Columns(2).Width=   900
         Columns(2).Caption=   "Valor"
         Columns(2).Name =   "VAL"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   900
         Columns(3).Caption=   "DOrigen"
         Columns(3).Name =   "ORI"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   16776960
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "TIPO_INTRODUCCION"
         Columns(4).Name =   "TIPO_INTRODUCCION"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "TIPO_DATOS"
         Columns(5).Name =   "TIPO_DATOS"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "MIN"
         Columns(6).Name =   "MIN"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "MAX"
         Columns(7).Name =   "MAX"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ATRIBID"
         Columns(8).Name =   "ATRIBID"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "ID"
         Columns(9).Name =   "ID"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "ORICOD"
         Columns(10).Name=   "ORICOD"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   2
         Columns(10).FieldLen=   256
         _ExtentX        =   16325
         _ExtentY        =   3757
         _StockProps     =   79
         BackColor       =   -2147483643
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblCamposEsp 
         Caption         =   "DCampos de especificaci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   120
         TabIndex        =   12
         Top             =   5280
         Width           =   6225
      End
      Begin VB.Label lblArticuloFich 
         Caption         =   "DArchivos adjuntos:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   2
         Left            =   120
         TabIndex        =   2
         Top             =   2640
         Width           =   6225
      End
      Begin VB.Label lblArticuloEsp 
         Caption         =   "DEspecificaciones de la l�nea de cat�logo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   6225
      End
   End
   Begin MSComDlg.CommonDialog cmmdEsp 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
   End
End
Attribute VB_Name = "frmCatalogoAtribEspYAdj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_oLinea         As CLineaCatalogo
Public g_oArticulo      As CArticulo
Public g_oArticuloProve As CArticulo
Public g_oProveedor     As CProveedor
Public g_oIBaseDatos    As IBaseDatos

Private m_oEsp          As CEspecificacion

Public g_bCancelarEsp   As Boolean
Public g_sComentario    As String
Public g_bRespetarCombo As Boolean
Public g_bSoloRuta      As Boolean

'Restricciones
Private m_bModifLinea    As Boolean

Private m_iPasado        As Integer
Private sayFileNames()   As String
Private Accion           As AccionesSummit
'Idiomas
Private m_sIdiArticulo      As String
Private m_sIdiProveedor     As String
Private m_sIdiDialogTitle   As String
Private m_sIdiAllFiles      As String
Private m_sIdiArchivo       As String
Private m_sIdiGuardarEsp    As String
Private m_sIdiTipoOrig      As String
Private m_sIdiNombre        As String
Private m_sIdiProveedorArt  As String
Private m_sIdiProceso       As String
Private m_skb               As String

Private WithEvents cPAnyadirEsp As cPopupMenu
Attribute cPAnyadirEsp.VB_VarHelpID = -1
Private WithEvents cPAnyadirAtr As cPopupMenu
Attribute cPAnyadirAtr.VB_VarHelpID = -1
Private m_sAdjNuevo As String
Private m_sAdjDesdeArticOProve As String
Private m_sCaption As String
Private m_sAtrDesdeMaestroAtr As String
Private m_sAtrDesdeArtOProce As String
Public g_ofrmATRIBMod As frmAtribMod
Public g_oAtributosLinea As CAtributos
Public m_oAtributosLinea As CAtributos
Private sConfirmEliminarCampos As String
Private m_sMaestroAtributos As String
Private sCamposNoSeleccionados As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String

''' <summary>Elimina los campos de especificaci�n (atributos) seleccionados</summary>
''' <remarks>Llamada desde Evento</remarks>
Private Sub cmdEliminarCampo_Click()
    Screen.MousePointer = vbHourglass
    Dim inum As Integer
    Dim oAtributos As CAtributos
    Dim tsError As TipoErrorSummit
    Dim i As Integer
    
    sdbgCamposEsp.Columns("VAL").DropDownHwnd = 0
    With sdbgCamposEsp
        If .SelBookmarks.Count > 0 Then
            i = oMensajes.PreguntaEliminar(sConfirmEliminarCampos)
            If i = vbYes Then
                inum = 0
                Set oAtributos = oFSGSRaiz.Generar_CAtributos
                While inum < .SelBookmarks.Count
                    '1.ELIMINAR DEL GRID --> Con deleteSelected es m�s sencillo
                    .Bookmark = .SelBookmarks(inum)
                    oAtributos.Add .Columns("ID").Value, .Columns("COD").Text, .Columns("DEN").Text, .Columns("TIPO_DATOS").Value
                inum = inum + 1
                Wend
                '3. ELIMINAR DE BDD
                tsError = g_oLinea.EliminarAtributos(oAtributos)
                '2.ELIMINAR DEL GRID
                .DeleteSelected
                .Update
                .Refresh
                '4. RECARGAR LA VARIABLE DE LOS ATRIBUTOS
                Set m_oAtributosLinea = Nothing
                Set m_oAtributosLinea = oFSGSRaiz.Generar_CAtributos
                Set m_oAtributosLinea = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, g_oProveedor)
            Else
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            Screen.MousePointer = vbNormal
            'NO HAY SELECCIONADO NING�N ATRIBUTO
            MsgBox sCamposNoSeleccionados, vbInformation, lblCamposEsp.caption
            Exit Sub
        End If
    End With
    'frmCatalogo.g_sOrigen = ""
    sdbgCamposEsp.MoveLast
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Carga inicial formulario</summary>
''' <remarks>Llamada desde el evento</remarks>
Private Sub Form_Load()
              
    If g_oLinea Is Nothing Then Unload Me
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    CargarEsp
    
    If g_oLinea.ArtCod_Interno = "" Then
'        Me.caption = m_sIdiProveedor & ": " & g_oProveedor.Cod & " - " & g_oProveedor.Den & _
'             " / " & m_sIdiArticulo & ": " & g_oLinea.ArtDen
        Me.caption = m_sCaption & " - " & m_sIdiProveedor & ": " & g_oProveedor.Cod & _
             " / " & m_sIdiArticulo & ": " & g_oLinea.ArtDen
    Else
'        Me.caption = m_sIdiProveedor & ": " & g_oProveedor.Cod & " - " & g_oProveedor.Den & _
'             " / " & m_sIdiArticulo & ": " & g_oArticulo.Cod & " - " & g_oArticulo.Den
        Me.caption = m_sCaption & " - " & m_sIdiProveedor & ": " & g_oProveedor.Cod & " - " & _
             " / " & m_sIdiArticulo & ": " & g_oArticulo.Cod
    End If
    ConfigurarSeguridad
    
    m_iPasado = 0
    
    Set cPAnyadirAtr = New cPopupMenu
    cPAnyadirAtr.hWndOwner = Me.hWnd
    Set cPAnyadirEsp = New cPopupMenu
    cPAnyadirEsp.hWndOwner = Me.hWnd
    cargarCPAnyadirEsp
    cargarcPAnyadirAtr
    
    'Configurar parametros del commondialog
    cmmdEsp.FLAGS = cdlOFNHideReadOnly
    ReDim sayFileNames(0)
End Sub

''' <summary>Carga Especificaciones</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub CargarEsp()
Dim oProves As CProveedores

    If g_oLinea Is Nothing Then Unload Me

    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.CargarTodosLosProveedoresDesde3 1, g_oLinea.ProveCod, , True
    If oProves.Count = 0 Then
        oMensajes.DatoEliminado m_sIdiProveedor
        Set oProves = Nothing
        Set g_oProveedor = Nothing
    End If
    Set g_oProveedor = oProves.Item(1)

    If g_oLinea.ArtCod_Interno <> "" Then
        'Carga las especificaciones de todos
        Set g_oArticulo = oFSGSRaiz.Generar_CArticulo
        g_oArticulo.GMN1Cod = g_oLinea.GMN1Cod
        g_oArticulo.GMN2Cod = g_oLinea.GMN2Cod
        g_oArticulo.GMN3Cod = g_oLinea.GMN3Cod
        g_oArticulo.GMN4Cod = g_oLinea.GMN4Cod
        g_oArticulo.Cod = g_oLinea.ArtCod_Interno
        g_oArticulo.Den = g_oLinea.ArtDen
        g_oArticulo.CodigoUnidad = g_oLinea.CodigoUnidadArticulo
        If Not IsNull(g_oLinea.Concepto) Then
            g_oArticulo.Concepto = g_oLinea.Concepto
        End If
        If Not IsNull(g_oLinea.Almacenable) Then
            g_oArticulo.Almacenable = g_oLinea.Almacenable
        End If
        If Not IsNull(g_oLinea.Recepcionable) Then
            g_oArticulo.Recepcionable = g_oLinea.Recepcionable
        End If
        
        
        g_oArticulo.CargarTodasLasEspecificaciones , , True
        Set g_oIBaseDatos = g_oArticulo
        g_oIBaseDatos.IniciarEdicion
        Set g_oIBaseDatos = Nothing
        
        'CARGAR ESPECIFICACIONES
        g_bRespetarCombo = True
        txtArticuloEsp(2).Text = NullToStr(g_oArticulo.esp)
        g_bRespetarCombo = False
            
        Set g_oArticuloProve = oFSGSRaiz.Generar_CArticulo
        g_oArticuloProve.GMN1Cod = g_oLinea.GMN1Cod
        g_oArticuloProve.GMN2Cod = g_oLinea.GMN2Cod
        g_oArticuloProve.GMN3Cod = g_oLinea.GMN3Cod
        g_oArticuloProve.GMN4Cod = g_oLinea.GMN4Cod
        g_oArticuloProve.Cod = g_oLinea.ArtCod_Interno
        g_oArticuloProve.Den = g_oLinea.ArtDen
        g_oArticuloProve.CodigoUnidad = g_oLinea.CodigoUnidadArticulo
        If Not IsNull(g_oLinea.Concepto) Then
            g_oArticuloProve.Concepto = g_oLinea.Concepto
        End If
        If Not IsNull(g_oLinea.Almacenable) Then
            g_oArticuloProve.Almacenable = g_oLinea.Almacenable
        End If
        If Not IsNull(g_oLinea.Recepcionable) Then
            g_oArticuloProve.Recepcionable = g_oLinea.Recepcionable
        End If
        g_oArticuloProve.CargarTodasLasEspecificaciones , , True, g_oLinea.ProveCod
    End If
    
    'CARGAR ARCHIVOS ADJUNTOS
    g_oLinea.CargarTodasLasEspecificaciones , , True
    g_bRespetarCombo = True
    txtArticuloEsp(2).Text = NullToStr(g_oLinea.esp)
    g_bRespetarCombo = False
    AnyadirEspsALista (2)

    'CAMPOS DE ESPECIFICACI�N (ATRIBUTOS) DE LA L�NEA DE CAT�LOGO
    Set m_oAtributosLinea = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, g_oProveedor)
    AnyadirAtribEspsALista
    
    If g_oProveedor Is Nothing Then
        oMensajes.NoValido m_sIdiProveedor
        Unload Me
    End If
End Sub

''' <summary>Redimensionamiento del formulario</summary>
''' <remarks>Llamada desde evento</remarks>
Private Sub Form_Resize()
    Dim dblAux As Double
    Dim iSeparacionEntreControles As Integer
    iSeparacionEntreControles = 100
    If Me.Height < 1500 Then Exit Sub
    If Me.Width < 1500 Then Exit Sub
    
    On Error Resume Next
    
    Frame1.Height = Me.Height - 800
    Frame1.Width = Me.Width - 400
    'WIDTH
    txtArticuloEsp(2).Width = Frame1.Width - 180
    lstvwArticuloEsp.Width = txtArticuloEsp(2).Width
    sdbgCamposEsp.Width = txtArticuloEsp(2).Width
    'HEIGHT
    dblAux = Frame1.Height - (lblArticuloEsp(2).Height + cmdAnyadirEsp.Height + lblArticuloFich(2).Height + Picture3.Height + cmdAnyadirCampo.Height + cmdEliminarCampo.Height + (6 * iSeparacionEntreControles))
    txtArticuloEsp(2).Height = dblAux / 3
    lstvwArticuloEsp.Height = txtArticuloEsp(2).Height
    sdbgCamposEsp.Height = txtArticuloEsp(2).Height
    'TOP
    lblArticuloEsp(2).Top = Frame1.Top + iSeparacionEntreControles
    txtArticuloEsp(2).Top = lblArticuloEsp(2).Top + lblArticuloEsp(2).Height
    cmdAnyadirEsp.Top = txtArticuloEsp(2).Top + txtArticuloEsp(2).Height + iSeparacionEntreControles
    lblArticuloFich(2).Top = cmdAnyadirEsp.Top + cmdAnyadirEsp.Height - 50
    lstvwArticuloEsp.Top = lblArticuloFich(2).Top + lblArticuloFich(2).Height
    Picture3.Top = lstvwArticuloEsp.Top + lstvwArticuloEsp.Height + iSeparacionEntreControles
    lblCamposEsp.Top = Picture3.Top + Picture3.Height - 50
    sdbgCamposEsp.Top = lblCamposEsp.Top + lblCamposEsp.Height
    cmdAnyadirCampo.Top = sdbgCamposEsp.Top + sdbgCamposEsp.Height + iSeparacionEntreControles
    cmdEliminarCampo.Top = sdbgCamposEsp.Top + sdbgCamposEsp.Height + iSeparacionEntreControles
    'LEFT
    cmdAnyadirEsp.Left = txtArticuloEsp(2).Left + txtArticuloEsp(2).Width - cmdAnyadirEsp.Width - iSeparacionEntreControles
    Picture3.Left = lstvwArticuloEsp.Left + lstvwArticuloEsp.Width - Picture3.Width - iSeparacionEntreControles
    cmdEliminarCampo.Left = cmdAnyadirEsp.Left
    cmdAnyadirCampo.Left = cmdEliminarCampo.Left - cmdAnyadirCampo.Width - iSeparacionEntreControles
    
    formatoAtributos
    sdbgCamposEsp.Columns("VAL").DropDownHwnd = 0
End Sub

''' <summary>descarga del formulario</summary>
''' <remarks>Llamada desde evento</remarks>
Private Sub Form_Unload(Cancel As Integer)
    Dim i As Integer
    Dim FOSFile As Scripting.FileSystemObject
    Dim bBorrando As Boolean

    Select Case Accion
        Case AccionesSummit.ACCArtadjMod
            If g_oLinea.ArtCod_Interno <> "" Then
                txtArticuloEsp_Validate 0, False
            End If
        Case AccionesSummit.ACCMatPorProveAdjMod
            If g_oLinea.ArtCod_Interno <> "" Then
                txtArticuloEsp_Validate 1, False
            End If
        Case AccionesSummit.ACCCatAdjudAdjunMod
                txtArticuloEsp_Validate 2, False
    End Select
        
    On Error GoTo Error:
    
    'Borramos los archivos temporales que hayamos creado
    Set FOSFile = New Scripting.FileSystemObject
    i = 0
    While i < UBound(sayFileNames)
        bBorrando = True
        If FOSFile.FileExists(sayFileNames(i)) Then
            FOSFile.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
    Set FOSFile = Nothing
    
    If sdbgCamposEsp.DataChanged Then 'Si cambian el valor de un atributo y directamente cierran el formulario no salta el evento de update de columna
        sdbgCamposEsp_AfterColUpdate (2)
    End If
    Exit Sub
Error:
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If
End Sub


Private Sub sdbgCamposEsp_BeforeUpdate(Cancel As Integer)
    sdbgCamposEsp.Columns("VAL").DropDownHwnd = 0
End Sub

Private Sub sdbgCamposEsp_RowLoaded(ByVal Bookmark As Variant)

    sdbgCamposEsp.Columns("VAL").DropDownHwnd = 0
    
    If sdbgCamposEsp.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoBoolean Then
        If sdbgCamposEsp.Columns("VALOR").CellValue(Bookmark) = 1 Then
            sdbgCamposEsp.Columns("VALOR").Text = m_sIdiTrue
        ElseIf sdbgCamposEsp.Columns("VALOR").CellValue(Bookmark) = 0 Then
            sdbgCamposEsp.Columns("VALOR").Text = m_sIdiFalse
        End If
    End If
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub txtArticuloEsp_Change(Index As Integer)
    If Not g_bRespetarCombo Then
        If Accion <> ACCCatAdjudAdjunMod Then
            Accion = ACCCatAdjudAdjunMod
            m_iPasado = 0
        End If
    End If
End Sub

''' <summary>valida las especificaciones</summary>
''' <remarks>Llamada desde unload</remarks>
Private Sub txtArticuloEsp_Validate(Index As Integer, Cancel As Boolean)
Dim teserror As TipoErrorSummit

    If Accion = ACCCatAdjudAdjunMod Then
    
        If StrComp(NullToStr(g_oLinea.esp), txtArticuloEsp(2).Text, vbTextCompare) <> 0 Then
                
            g_oLinea.esp = StrToNull(txtArticuloEsp(2).Text)
            If txtArticuloEsp(2).Text <> "" Then
                g_oLinea.EspAdj = 1
            Else
                If g_oLinea.especificaciones.Count = 0 Then
                    g_oLinea.EspAdj = 0
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            teserror = g_oLinea.ModificarEspecificacion
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Cancel = True
                Accion = ACCCatAdjudAdjunCon
                Exit Sub
            End If
            
            'Registrar accion
            basSeguridad.RegistrarAccion Accion, "Linea:" & g_oLinea.Id & " Esp:" & Left(g_oLinea.esp, 50)
            Accion = ACCCatAdjudAdjunCon
            Screen.MousePointer = vbNormal
        
        End If
    
    End If
End Sub
''' <summary>Cargar Textos</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO_ATRIB_ESP_ADJ, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then

        m_sCaption = Ador(0).Value '1 Ficha del art�culo
        Ador.MoveNext
        lblArticuloEsp(2).caption = Ador(0).Value '2 Especificaciones de la l�nea del cat�logo
        Ador.MoveNext
        lblArticuloFich(2).caption = Ador(0).Value '3 Archivos adjuntos
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders.Item(1).Text = Ador(0).Value '4 Archivo
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders.Item(2).Text = Ador(0).Value '5 Tama�o
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders.Item(3).Text = Ador(0).Value '6 Comentario
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders.Item(4).Text = Ador(0).Value '7 Fecha
        Ador.MoveNext
        cmdAnyadirEsp.caption = Ador(0).Value '8 A�adir
        cmdAnyadirCampo.caption = Ador(0).Value '8 A�adir
        Ador.MoveNext
        cmdEliminarCampo.caption = Ador(0).Value '9 Eliminar
        Ador.MoveNext
        m_sAdjNuevo = Ador(0).Value '10 Nuevo
        Ador.MoveNext
        m_sAdjDesdeArticOProve = Ador(0).Value '11 Desde art�culo o proveedor/art�culo
        Ador.MoveNext
        m_sAtrDesdeMaestroAtr = Ador(0).Value  '12 Desde el maestro de atributos
        Ador.MoveNext
        m_sAtrDesdeArtOProce = Ador(0).Value '13 Desde art�culo, proveedor o proceso
        Ador.MoveNext
        m_sIdiArticulo = Ador(0).Value '14 Art�culo
        Ador.MoveNext
        m_sIdiProveedor = Ador(0).Value '15 Proveedor
        Ador.MoveNext
        lblCamposEsp.caption = Ador(0).Value '16 Campos de especificaci�n
        Ador.MoveNext
        sdbgCamposEsp.Columns("COD").caption = Ador(0).Value '17 C�digo
        Ador.MoveNext
        sdbgCamposEsp.Columns("DEN").caption = Ador(0).Value '18 DEnominaci�n
        Ador.MoveNext
        sdbgCamposEsp.Columns("VAL").caption = Ador(0).Value '19 Valor
        Ador.MoveNext
        sdbgCamposEsp.Columns("ORI").caption = Ador(0).Value '20 Origen
        Ador.MoveNext
        m_sIdiProveedorArt = Ador(0).Value '21  Proveedor/Art.
        Ador.MoveNext
        m_sIdiProceso = Ador(0).Value '22  Proceso
        Ador.MoveNext
        sConfirmEliminarCampos = Ador(0).Value '23 �Desea eliminar los campos de especificaci�n seleccionados?
        Ador.MoveNext
        m_sIdiAllFiles = Ador(0).Value '24 TODOS LOS ARCHIVOS
        Ador.MoveNext
        m_sMaestroAtributos = Ador(0).Value '25 Maestro Atrib.
        Ador.MoveNext
        sCamposNoSeleccionados = Ador(0).Value '26 No ha seleccionado ning�n registro
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value  '27 S�
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value '28 No
        Ador.Close
    End If
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Public Sub ConfigurarSeguridad()
    m_bModifLinea = True
    
    'Linea
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGAdjModificar)) Is Nothing) Then
        m_bModifLinea = False
    End If

    If Not m_bModifLinea Then
        txtArticuloEsp(2).Locked = True
        cmdAnyadirEsp.Visible = False
        
        cmdAnyadirAdj.Visible = False
        cmdEliminarAdj.Visible = False
        cmdModificarAdj.Visible = False
        
        cmdAnyadirCampo.Visible = False
        cmdEliminarCampo.Visible = False
    End If
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Public Sub AnyadirEspsALista(Index As Integer)
Dim oEsp As CEspecificacion

    Screen.MousePointer = vbHourglass
    lstvwArticuloEsp.ListItems.clear
    
    For Each oEsp In g_oLinea.especificaciones
        lstvwArticuloEsp.ListItems.Add , "LINEA" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next

    Screen.MousePointer = vbNormal
End Sub
''' <summary>
''' Muestra el menu contextual del bot�n
''' </summary>
Private Sub cmdAnyadirEsp_Click()
    'ABRIR PANEL PARA A�ADIR ESPECIFICACION
    txtArticuloEsp(2).Text = FSGSForm.MostrarFormCatalogoEsp(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, gParametrosGenerales, oGestorSeguridad, _
                                                    oUsuarioSummit, g_oLinea, basOptimizacion.gvarCodUsuario, basOptimizacion.gTipoDeUsuario, basOptimizacion.gCodEqpUsuario, _
                                                    basOptimizacion.gCodCompradorUsuario)
    
End Sub

''' <summary>
''' Muestra el menu contextual del bot�n
''' </summary>
Private Sub cmdAnyadirCampo_Click()
    MostrarcPAnyadirAtr cmdAnyadirCampo.Left, cmdAnyadirCampo.Top + cmdAnyadirCampo.Height
End Sub

''' <summary>
''' Lanza las opciones del menu contextual
''' </summary>
''' <param name="ItemNumber">Indice del men�</param>
''' <remarks>Llamada desde: Click de cmdOtrasAcciones; Tiempo m�ximo:0,1</remarks>
Private Sub cPAnyadirAtr_Click(ItemNumber As Long)
    'Dim frmVariable As Form
    'Set frmVariable = Me
    Select Case cPAnyadirAtr.itemKey(ItemNumber)
        Case "MAESTROATR"
            Dim oProves As CProveedores
            
            If g_oLinea Is Nothing Then Unload Me
            
            Set oProves = oFSGSRaiz.generar_CProveedores
            oProves.CargarTodosLosProveedoresDesde3 1, g_oLinea.ProveCod, , True
            If oProves.Count = 0 Then
                oMensajes.DatoEliminado m_sIdiProveedor
                Set oProves = Nothing
                Set g_oProveedor = Nothing
            End If
            Set g_oProveedor = oProves.Item(1)
            
            Set g_oAtributosLinea = Nothing
            Set g_oAtributosLinea = oFSGSRaiz.Generar_CAtributos
            'A�ade un campo de tipo atributo de GS
            If Not g_ofrmATRIBMod Is Nothing Then
                Unload g_ofrmATRIBMod
                Set g_ofrmATRIBMod = Nothing
            End If
        
            Set g_ofrmATRIBMod = New frmAtribMod
            g_ofrmATRIBMod.g_sOrigen = "frmCatalogoAtribEspYAdj"
            
            g_ofrmATRIBMod.Show vbModal
            
            'GUARDAMOS LOS CAMPOS A�ADIDOS EN BDD
            Dim tsError As TipoErrorSummit
            Set m_oAtributosLinea = oFSGSRaiz.Generar_CAtributos
            Set m_oAtributosLinea = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, g_oProveedor)
            tsError = g_oLinea.AnyadirAtributos(AtributosLineaCatalogo.Linea, g_oAtributosLinea)
            'RECARGAMOS LAS L�NEAS
            Set m_oAtributosLinea = Nothing
            Set m_oAtributosLinea = oFSGSRaiz.Generar_CAtributos
            Set m_oAtributosLinea = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, g_oProveedor)
            sdbgCamposEsp.Refresh
            'Ponemos el ID a cada l�nea del grid
            With sdbgCamposEsp
                .MoveFirst
                If .Rows > 0 Then
                    Dim oAtr As CAtributo
                    For Each oAtr In m_oAtributosLinea
                        Dim i As Integer
                        For i = 0 To .Rows
                            If (.Columns("ID").Value = CStr(0) Or .Columns("ID").Value = "") And .Columns("COD").Value = oAtr.Cod And .Columns("ATRIBID").Value = CStr(oAtr.Atrib) Then
                                .Columns("ID").Value = oAtr.Id
                            End If
                            .MoveNext
                        Next
                    Next
                End If
            End With

            'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
            If tsError.NumError <> TESnoerror Then
                TratarError tsError
            End If
            
        Case "ARTOPROVEOPROCE"
            Set frmCatalogoAtrib.g_oLinea = Me.g_oLinea
            frmCatalogoAtrib.Show vbModal
            Set Me.g_oLinea = frmCatalogoAtrib.g_oLinea
    End Select
End Sub
''' <summary>
''' Carga del submen�
''' </summary>
''' <remarks>Llamada desde: frmCatalogoAtribEspyAdj; Tiempo m�ximo:0,1</remarks>
Private Sub cargarcPAnyadirAtr()
    cPAnyadirAtr.clear
    Dim indice As Integer
    indice = 1
    cPAnyadirAtr.AddItem m_sAtrDesdeMaestroAtr, , indice, , , , , "MAESTROATR"
    indice = indice + 1
    cPAnyadirAtr.AddItem m_sAtrDesdeArtOProce, , indice, , , , , "ARTOPROVEOPROCE"
End Sub

''' <summary>
''' muestra el submen�
''' </summary>
''' <param name="X">Coordenada horizontal</param>
''' <param name="Y">Coordenada vertical</param>
''' <remarks>Llamada desde: frmCatalogoAtribEspYAdj; Tiempo m�ximo:0,1</remarks>
Private Sub MostrarcPAnyadirAtr(ByVal X As Single, ByVal Y As Single)
    cPAnyadirAtr.ShowPopupMenu X, Y
End Sub

''' <summary>Ancho de las columnas de la grid de campos de especificaci�n (atributos)</summary>
''' <remarks>Llamada desde Resize</remarks>
Private Sub formatoAtributos()
    sdbgCamposEsp.Columns("COD").Width = (sdbgCamposEsp.Width - 570) * 0.15
    sdbgCamposEsp.Columns("DEN").Width = (sdbgCamposEsp.Width - 570) * 0.4
    sdbgCamposEsp.Columns("VAL").Width = (sdbgCamposEsp.Width - 570) * 0.3
    sdbgCamposEsp.Columns("ORI").Width = (sdbgCamposEsp.Width - 570) * 0.15
End Sub

''' <summary>Carga la grid de los campos de especificaci�n (atributos)</summary>
''' <remarks>Llamada desde CargarAtrib</remarks>
Public Sub AnyadirAtribEspsALista()
    Dim oatrib As CAtributo
    Screen.MousePointer = vbHourglass
    sdbgCamposEsp.RemoveAll
    For Each oatrib In m_oAtributosLinea
        Dim sOrigen As String
        sOrigen = ""
        Select Case oatrib.Origen
            Case AtributosLineaCatalogo.Articulo:
                sOrigen = m_sIdiArticulo
            Case AtributosLineaCatalogo.ProveArtYProceOfer:
                sOrigen = m_sIdiProveedorArt
            Case AtributosLineaCatalogo.ProceEsp:
                sOrigen = m_sIdiProceso
            Case AtributosLineaCatalogo.MaestroAtributos:
                sOrigen = m_sMaestroAtributos
        End Select
        sdbgCamposEsp.AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & NullToStr(oatrib.valor) & Chr(m_lSeparador) & sOrigen & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & NullToStr(oatrib.Minimo) & Chr(m_lSeparador) & NullToStr(oatrib.Maximo) & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Origen
    Next
    Screen.MousePointer = vbNormal
End Sub
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'               FUNCIONALIDAD A�ADIR/ELIMINAR/DESCARGAR/ETC ADJUNTOS
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''' <summary>
''' Muestra el menu contextual del bot�n
''' </summary>
Private Sub cmdAnyadirAdj_Click()
    'A�ADIR ADJUNTO: MOSTRAR MENU CONTEXTUAL DE OPCIONES
    MostrarcPAnyadirEsp Picture3.Left, Picture3.Top + Picture3.Height + 100
End Sub

''' <summary>
''' Lanza las opciones del menu contextual
''' </summary>
''' <param name="ItemNumber">Indice del men�</param>
''' <remarks>Llamada desde: Click de cmdOtrasAcciones; Tiempo m�ximo:0,1</remarks>
Private Sub cPAnyadirEsp_Click(ItemNumber As Long)
    Select Case cPAnyadirEsp.itemKey(ItemNumber)
        Case "NUEVO" 'A�ADIR ADJUNTO DESDE DISCO DURO
            Dim sFileName As String
            Dim sFileTitle As String
            Dim arrFileNames As Variant
            Dim iFile As Integer
            Dim oFos As Scripting.FileSystemObject
            Dim oAdjun As CAdjunto
            Dim sAdjunto As String
            Dim ArrayAdjunto() As String
            On Error GoTo Cancelar:
                
            cmmdEsp.filename = ""
            cmmdEsp.DialogTitle = m_sIdiDialogTitle
            cmmdEsp.Filter = m_sIdiAllFiles & "|*.*"
            cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
            
            cmmdEsp.ShowOpen
            
            sFileName = cmmdEsp.filename
            sFileTitle = cmmdEsp.FileTitle
            If sFileName = "" Then Exit Sub
            
            arrFileNames = ExtraerFicheros(sFileName)
            
            ' Ahora obtenemos el comentario para la especificacion
            frmPROCEComFich.chkProcFich.Visible = False
            If UBound(arrFileNames) = 1 Then
                frmPROCEComFich.lblFich = sFileTitle
            Else
                frmPROCEComFich.lblFich = ""
                frmPROCEComFich.Label1.Visible = False
                frmPROCEComFich.lblFich.Visible = False
                frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
                frmPROCEComFich.txtCom.Height = 2300
            End If
            frmPROCEComFich.sOrigen = "frmCatalogoAtribEspYAdj"
            frmPROCEComFich.Show 1
            
            If Not g_bCancelarEsp Then
                Set oFos = New Scripting.FileSystemObject
                For iFile = 1 To UBound(arrFileNames)
                    sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                    sFileTitle = arrFileNames(iFile)
                    Set oAdjun = oFSGSRaiz.generar_cadjunto
                    oAdjun.nombre = sFileTitle
                    oAdjun.Comentario = g_sComentario
                    oAdjun.Tipo = TipoAdjunto.EspecificacionLineaCatalogo
                    sAdjunto = oAdjun.GrabarAdjunto(arrFileNames(0) & "\", "", g_oLinea.Id)
                    
                    'Creamos un array, cada "substring" se asignar� a un elemento del array
                    ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                    oAdjun.Id = ArrayAdjunto(0)
                    oAdjun.SacarFecAct g_oLinea.Id
                
                    g_oLinea.especificaciones.Add oAdjun.nombre, oAdjun.FECACT, oAdjun.Id, , , oAdjun.Comentario, , , , , , , oAdjun.DataSize
                    
                    g_oLinea.EspAdj = 1
                    lstvwArticuloEsp.ListItems.Add , "LINEA" & CStr(oAdjun.Id), sFileTitle, , "ESP"
                    lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).ToolTipText = oAdjun.Comentario
                    lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oAdjun.DataSize / 1024) & " " & m_skb
                    lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).ListSubItems.Add , "Com", oAdjun.Comentario
                    lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).ListSubItems.Add , "Fec", oAdjun.FECACT
                    lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).Tag = oAdjun.Id
                Next
                
                basSeguridad.RegistrarAccion AccionesSummit.ACCCatAdjudAdjunAnya, "Linea:" & g_oLinea.Id & " Archivo:" & oAdjun.nombre
                lstvwArticuloEsp.Refresh
            
                Set m_oEsp = Nothing
            End If
        
            Screen.MousePointer = vbNormal
            Exit Sub
Cancelar:
                
            If err.Number <> 32755 Then '32755 = han pulsado cancel
                MsgBox err.Description
                Set m_oEsp = Nothing
                Set g_oIBaseDatos = Nothing
            End If
            Screen.MousePointer = vbNormal
            Exit Sub
        Case "ARTOPROVE" 'A�ADIR ADJUNTO DESDE PANEL DE ADJUNTOS DE ARTICULO Y PROVEEDOR
            Set frmCatalogoAdjun.g_oLinea = Me.g_oLinea
            frmCatalogoAdjun.Show vbModal
            Set Me.g_oLinea = frmCatalogoAdjun.g_oLinea
    End Select
End Sub

''' <summary>
''' Carga del submen�
''' </summary>
''' <remarks>Llamada desde: frmCatalogoAtribEspyAdj; Tiempo m�ximo:0,1</remarks>
Private Sub cargarCPAnyadirEsp()
    cPAnyadirEsp.clear
    Dim indice As Integer
    indice = 1
    cPAnyadirEsp.AddItem m_sAdjNuevo, , indice, , , , , "NUEVO"
    indice = indice + 1
    cPAnyadirEsp.AddItem m_sAdjDesdeArticOProve, , indice, , , , , "ARTOPROVE"
End Sub

''' <summary>
''' muestra el submen�
''' </summary>
''' <param name="X">Coordenada horizontal</param>
''' <param name="Y">Coordenada vertical</param>
''' <remarks>Llamada desde: frmCatalogoAtribEspYAdj; Tiempo m�ximo:0,1</remarks>
Private Sub MostrarcPAnyadirEsp(ByVal X As Single, ByVal Y As Single)
    cPAnyadirEsp.ShowPopupMenu X, Y
End Sub

''' <summary>Abrir adjunto</summary>
''' <remarks>Llamada desde evento</remarks>
Private Sub cmdAbrirAdj_Click()
Dim Item As MSComctlLib.listItem
Dim sFileName As String
Dim sFileTitle As String
Dim oAdjun As CAdjunto
On Error GoTo Error

    Set Item = lstvwArticuloEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
        
        'Comprueba si existe el fichero y si existe se borra:
        Dim FOSFile As Scripting.FileSystemObject
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing
        
        ' Cargamos el contenido en la esp.
        Set m_oEsp = g_oLinea.especificaciones.Item(CStr(lstvwArticuloEsp.selectedItem.Tag))
        Set m_oEsp.LineaCatalogo = g_oLinea
        
        If IsNull(m_oEsp.Ruta) Then 'Archivo adjunto, se lee de la base de datos
            Screen.MousePointer = vbHourglass
            Set oAdjun = oFSGSRaiz.generar_cadjunto
            oAdjun.Id = m_oEsp.Id
            oAdjun.DataSize = m_oEsp.DataSize
            oAdjun.Tipo = TipoAdjunto.EspecificacionLineaCatalogo
            oAdjun.LeerAdjunto sFileName, g_oLinea.Id
            sayFileNames(UBound(sayFileNames)) = sFileName
            ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
            
            'Lanzamos la aplicacion
            ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
        Else 'Archivo vinculado
            ArchivoAbrir True, sFileName
        End If
        
    End If
    
    
    Set m_oEsp = Nothing
    Screen.MousePointer = vbNormal
    Exit Sub
    
Error:
    If err.Number = 70 Then
        Resume Next
    End If
End Sub
''' <summary>Abrir archivo</summary>
''' <remarks>Llamada desde cmdAbrir_click</remarks>
Private Sub ArchivoAbrir(ByVal bSoloRuta As Boolean, ByVal sFileName As String)
Dim DataFile As Integer
Dim Fl As Long
Dim Chunks As Integer
Dim Fragment As Integer
Dim Chunk() As Byte
Dim indChunk As Integer
Dim oFos As FileSystemObject
    
On Error GoTo Cancelar:

    If Not bSoloRuta Then
            DataFile = 1
            
            'Abrimos el fichero para escritura binaria
            Open sFileName For Binary Access Write As DataFile
            
            Fl = m_oEsp.DataSize
            Chunks = Fl \ giChunkSize
            Fragment = Fl Mod giChunkSize
            
            If Fragment > 0 Then
                ReDim Chunk(Fragment - 1)
                Chunk() = m_oEsp.ReadData(Fragment)
                Put DataFile, , Chunk()
            End If
            
            ReDim Chunk(giChunkSize - 1)
            
            For indChunk = 1 To Chunks
                Chunk() = m_oEsp.ReadData(giChunkSize)
                Put DataFile, , Chunk()
            Next
            
            m_oEsp.FinalizarLecturaData
            
            'Verificar escritura
            If LOF(DataFile) <> m_oEsp.DataSize Then
                Screen.MousePointer = vbNormal
                oMensajes.ErrorDeEscrituraEnDisco
            End If
            
            Close DataFile
            
            sayFileNames(UBound(sayFileNames)) = sFileName
            ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
            
            'Lanzamos la aplicacion
            ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
            
            DataFile = 0

    Else
            Set oFos = New FileSystemObject
            If oFos.FolderExists(m_oEsp.Ruta) And oFos.FileExists(m_oEsp.Ruta & m_oEsp.nombre) Then
                ShellExecute MDI.hWnd, "Open", m_oEsp.Ruta & m_oEsp.nombre, 0&, m_oEsp.Ruta, 1
            Else
                oMensajes.ImposibleModificarEsp (m_oEsp.Ruta & m_oEsp.nombre)
            End If
    
    End If
    Set m_oEsp = Nothing
    Screen.MousePointer = vbNormal
    Exit Sub
    
Cancelar:
    
    If DataFile <> 0 Then
        Close DataFile
        DataFile = 0
    End If
    Screen.MousePointer = vbNormal
    Set m_oEsp = Nothing
End Sub

''' <summary>Eliminar adjunto</summary>
''' <remarks>Llamada desde evento</remarks>
Private Sub cmdEliminarAdj_Click()
Dim Item As MSComctlLib.listItem
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
        
On Error GoTo Cancelar:

    Set Item = lstvwArticuloEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        irespuesta = oMensajes.PreguntaEliminar(m_sIdiArchivo & ": " & lstvwArticuloEsp.selectedItem.Text)
        If irespuesta = vbNo Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        Set m_oEsp = g_oLinea.especificaciones.Item(lstvwArticuloEsp.selectedItem.Index)
        Set m_oEsp.LineaCatalogo = g_oLinea
        Set g_oIBaseDatos = m_oEsp
        teserror = g_oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            g_oLinea.especificaciones.Remove (lstvwArticuloEsp.selectedItem.Index)
            If g_oLinea.especificaciones.Count = 0 Then
                'If txtArticuloEsp(2).Text = "" Then
                    g_oLinea.EspAdj = 0
                'End If
            End If
            basSeguridad.RegistrarAccion AccionesSummit.ACCCatAdjudAdjunEli, "Linea:" & CStr(g_oLinea.Id) & " Archivo:" & m_oEsp.nombre
            lstvwArticuloEsp.ListItems.Remove (CStr(lstvwArticuloEsp.selectedItem.key))
        End If
    End If
Cancelar:
    Set m_oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Modificar adjunto</summary>
''' <remarks>Llamada desde evento</remarks>
Private Sub cmdModificarAdj_Click()
Dim teserror As TipoErrorSummit
 
    If lstvwArticuloEsp.selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        
        Screen.MousePointer = vbHourglass
        Set g_oLinea.especificaciones.Item(CStr(lstvwArticuloEsp.selectedItem.Tag)).LineaCatalogo = g_oLinea
        Set g_oIBaseDatos = g_oLinea.especificaciones.Item(CStr(lstvwArticuloEsp.selectedItem.Tag))
        teserror = g_oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set g_oIBaseDatos = Nothing
            Exit Sub
        End If
        
        basSeguridad.RegistrarAccion AccionesSummit.ACCCatAdjudAdjunMod, "Linea:" & g_oLinea.Id & "Archivo:" & CStr(lstvwArticuloEsp.selectedItem.Text)
        
        Set frmPROCEEspMod.g_oIBaseDatos = g_oIBaseDatos
        frmPROCEEspMod.g_sOrigen = "frmCatalogoAtribEspYAdj"
        Screen.MousePointer = vbNormal
        frmPROCEEspMod.Show 1
    End If
End Sub

''' <summary>Guardar en local un adjunto</summary>
''' <remarks>Llamada desde evento</remarks>
Private Sub cmdSalvarAdj_Click()
Dim Item As MSComctlLib.listItem
Dim DataFile As Integer
Dim sFileName As String
Dim sFileTitle As String
Dim oFos As FileSystemObject
Dim oAdjun As CAdjunto
On Error GoTo Cancelar:

    Set Item = lstvwArticuloEsp.selectedItem
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        cmmdEsp.DialogTitle = m_sIdiGuardarEsp
        cmmdEsp.Filter = m_sIdiTipoOrig & "|*.*"
    
        cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave
                
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdiNombre
            Exit Sub
        End If
        DataFile = 1
        ' Cargamos el contenido en la esp.
        Set m_oEsp = g_oLinea.especificaciones.Item(CStr(lstvwArticuloEsp.selectedItem.Tag))
        Set m_oEsp.LineaCatalogo = g_oLinea
        
        If IsNull(m_oEsp.Ruta) Then 'Archivo adjunto
            Set oAdjun = oFSGSRaiz.generar_cadjunto
            oAdjun.Id = m_oEsp.Id
            oAdjun.DataSize = m_oEsp.DataSize
            oAdjun.Tipo = TipoAdjunto.EspecificacionLineaCatalogo
            oAdjun.LeerAdjunto sFileName, g_oLinea.Id
        Else 'Archivo vinculado
            Set oFos = New FileSystemObject
            If oFos.FolderExists(m_oEsp.Ruta) And oFos.FileExists(m_oEsp.Ruta & m_oEsp.nombre) Then
                oFos.CopyFile m_oEsp.Ruta & m_oEsp.nombre, sFileName, False
                Screen.MousePointer = vbNormal
                Exit Sub
            Else
                oMensajes.ImposibleModificarEsp (m_oEsp.Ruta & m_oEsp.nombre)
            End If
            Set oFos = Nothing
        End If
        Set m_oEsp = Nothing
    End If
Cancelar:
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        If DataFile <> 0 Then
            Close DataFile
        End If
        Set m_oEsp = Nothing
    End If
    Screen.MousePointer = vbNormal
End Sub

'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DEL GRID DE CAMPOS DE ESPECIFICACI�N DE L�NEA
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''' <summary>
''' Evento que salta al hacer cambios en la grid
''' </summary>
''' <param name="Cancel">Cancelacion del cambio</param>
''' <returns></returns>
''' <remarks>Llamada desde;Evento Tiempo m�ximo</remarks>
Private Sub sdbgCamposEsp_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Cancel = False
    If sdbgCamposEsp.Columns("VAL").Text <> "" Then
        Select Case UCase(sdbgCamposEsp.Columns("TIPO_DATOS").Text)
            Case 2 'Numero
                If (Not IsNumeric(sdbgCamposEsp.Columns("VAL").Text)) Then
                    oMensajes.AtributoValorNoValido ("TIPO2")
                    Cancel = True
                    GoTo Salir
                Else
                    If sdbgCamposEsp.Columns("MIN").Text <> "" And sdbgCamposEsp.Columns("MAX").Text <> "" Then
                        If StrToDbl0(sdbgCamposEsp.Columns("MIN").Text) > StrToDbl0(sdbgCamposEsp.Columns("VAL").Text) Or StrToDbl0(sdbgCamposEsp.Columns("MAX").Text) < StrToDbl0(sdbgCamposEsp.Columns("VAL").Text) Then
                            oMensajes.ValorEntreMaximoYMinimo sdbgCamposEsp.Columns("MIN").Text, sdbgCamposEsp.Columns("MAX").Text
                            Cancel = True
                            GoTo Salir
                        End If
                    End If
                End If
            Case 3 'Fecha
                If (Not IsDate(sdbgCamposEsp.Columns("VAL").Text) And sdbgCamposEsp.Columns("VAL").Text <> "") Then
                    oMensajes.AtributoValorNoValido ("TIPO3")
                    Cancel = True
                    GoTo Salir
                Else
                    If sdbgCamposEsp.Columns("MIN").Text <> "" And sdbgCamposEsp.Columns("MAX").Text <> "" Then
                        If CDate(sdbgCamposEsp.Columns("MIN").Text) > CDate(sdbgCamposEsp.Columns("VAL").Text) Or CDate(sdbgCamposEsp.Columns("MAX").Text) < CDate(sdbgCamposEsp.Columns("VAL").Text) Then
                         oMensajes.ValorEntreMaximoYMinimo sdbgCamposEsp.Columns("MIN").Text, sdbgCamposEsp.Columns("MAX").Text
                            Cancel = True
                            GoTo Salir
                        End If
                    End If
                End If
            Case 4 'Si/No
                If sdbgCamposEsp.Columns("VAL").Text <> m_sIdiTrue And sdbgCamposEsp.Columns("VAL").Text <> m_sIdiFalse And Trim(sdbgCamposEsp.Columns("VAL").Text) <> "" Then
                    oMensajes.AtributoValorNoValido ("TIPO4")
                    Cancel = True
                    GoTo Salir
                End If
        End Select
    End If
    Exit Sub
Salir:
    If Me.Visible Then sdbgCamposEsp.SetFocus
End Sub

Private Sub sdbgCamposEsp_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If IsEmpty(sdbgCamposEsp.RowBookmark(sdbgCamposEsp.Row)) Then
        sdbgCamposEsp.Bookmark = sdbgCamposEsp.RowBookmark(sdbgCamposEsp.Row - 1)
    Else
        sdbgCamposEsp.Bookmark = sdbgCamposEsp.RowBookmark(sdbgCamposEsp.Row)
    End If
End Sub

Private Sub sdbgCamposEsp_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

'''' <summary>
'''' Evento que salta al hacer click en el boton de un atruto de texto largo
'''' </summary>
'''' <param name="Cancel">Click en un atributo de tipo texto largo</param>
'''' <returns></returns>
'''' <remarks>Llamada desde;Evento Tiempo m�ximo</remarks>
Private Sub sdbgCamposEsp_BtnClick()
    If sdbgCamposEsp.col < 0 Then Exit Sub

    If sdbgCamposEsp.Columns(sdbgCamposEsp.col).Name = "VAL" Then
        frmATRIBDescr.g_bEdicion = True
        frmATRIBDescr.caption = sdbgCamposEsp.Columns("COD").Value & ": " & sdbgCamposEsp.Columns("DEN").Value
        frmATRIBDescr.txtDescr.Text = sdbgCamposEsp.Columns(sdbgCamposEsp.col).Value
        frmATRIBDescr.g_sOrigen = "frmCatalogoAtribEspYAdj"
        frmATRIBDescr.Show 1
        If sdbgCamposEsp.DataChanged Then
            sdbgCamposEsp.Update
        End If
    End If
End Sub

Private Sub sdbgCamposEsp_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        If sdbgCamposEsp.DataChanged = False Then
            sdbgCamposEsp.CancelUpdate
            sdbgCamposEsp.DataChanged = False
        End If
    End If
End Sub

Private Sub sdbgCamposEsp_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If sdbgCamposEsp.Columns("TIPO_INTRODUCCION").Value = 0 Then 'Libre
        If sdbgCamposEsp.Columns("TIPO_DATOS").Value = TipoBoolean Then
            sdbddValor.RemoveAll
            sdbddValor.AddItem ""
            sdbgCamposEsp.Columns("VAL").DropDownHwnd = sdbddValor.hWnd
            sdbddValor.Enabled = True
        Else
            sdbgCamposEsp.Columns("VAL").DropDownHwnd = 0
            sdbddValor.Enabled = False
            If sdbgCamposEsp.Columns("TIPO_DATOS").Value = 1 Then
                sdbgCamposEsp.Columns("VAL").Style = ssStyleEditButton
            End If
        End If
    Else 'Lista
        sdbddValor.RemoveAll
        sdbddValor.AddItem ""
        sdbgCamposEsp.Columns("VAL").DropDownHwnd = sdbddValor.hWnd
        sdbddValor.Enabled = True
        sdbddValor.DroppedDown = True
        sdbddValor_DropDown
        sdbddValor.DroppedDown = True
    End If
End Sub

Private Sub sdbgCamposEsp_Scroll(Cancel As Integer)
    sdbgCamposEsp.Columns("VAL").DropDownHwnd = 0
End Sub

Private Sub sdbgCamposEsp_AfterColUpdate(ByVal ColIndex As Integer)
    Dim i As Integer
    
    With sdbgCamposEsp
        '1. ACTUALIZAR BDD
        Dim tsError As TipoErrorSummit
        tsError = g_oLinea.ActualizarAtributo(.Columns("ID").Value, .Columns("VAL").Value, .Columns("TIPO_DATOS").Value)
        '2.ACTUALIZAR VARIABLE ATRIBUTOS L�NEA
        For i = 1 To m_oAtributosLinea.Count
            If m_oAtributosLinea.Item(i).Id = .Columns("ID").Value Then
                m_oAtributosLinea.Item(i).valor = .Columns("VAL").Value
            End If
        Next
        
        'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
        If tsError.NumError <> TESnoerror Then
            TratarError tsError
            If Me.Visible Then .SetFocus
            .DataChanged = False
        End If
    End With
End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant
    On Error Resume Next
    sdbddValor.MoveFirst
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbgCamposEsp.Columns("VAL").Value = Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_DropDown()
    Dim oAtributo As CAtributo
    Dim oLista As CValorPond
    Dim iIndice As Integer
    iIndice = 1
    
    If sdbgCamposEsp.Rows = 0 Then Exit Sub
   
    If sdbgCamposEsp.Columns("VAL").Locked Then
        sdbddValor.DroppedDown = False
        Exit Sub
    End If
    sdbddValor.RemoveAll
    sdbddValor.DroppedDown = True
    
    Set oAtributo = oFSGSRaiz.Generar_CAtributo
    oAtributo.Id = sdbgCamposEsp.Columns("ATRIBID").Value
    
    If oAtributo.Id > 0 Then
        If sdbgCamposEsp.Columns("TIPO_INTRODUCCION").Value = "1" Then
            oAtributo.CargarDatosAtributo
            oAtributo.CargarListaDeValores AtributoParent.Linea_Catalogo
            For Each oLista In oAtributo.ListaPonderacion
                sdbddValor.AddItem oLista.ValorLista & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
        Else
            If sdbgCamposEsp.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoBoolean Then
                sdbddValor.AddItem "1" & Chr(m_lSeparador) & m_sIdiTrue
                sdbddValor.AddItem "0" & Chr(m_lSeparador) & m_sIdiFalse
            End If
        End If
    End If
    
    sdbddValor.Width = sdbgCamposEsp.Columns("VAL").Width
    sdbddValor.Columns("DESC").Width = sdbddValor.Width
End Sub
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

