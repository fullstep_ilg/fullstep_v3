VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroNegConcep3Reu 
   Caption         =   "Informe de ahorros negociados por concepto3 en reuni�n"
   ClientHeight    =   5820
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11205
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInfAhorroNegConcep3Reu.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5820
   ScaleWidth      =   11205
   Begin VB.Frame fraSel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   9675
      Begin VB.PictureBox picTipoGrafico 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   6780
         ScaleHeight     =   435
         ScaleWidth      =   1695
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   150
         Visible         =   0   'False
         Width           =   1695
         Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
            Height          =   285
            Left            =   60
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   90
            Width           =   1575
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            Row.Count       =   5
            Row(0)          =   "Barras 2D"
            Row(1)          =   "Barras 3D"
            Row(2)          =   "Lineas 2D"
            Row(3)          =   "Lineas 3D"
            Row(4)          =   "Tarta"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   3200
            Columns(0).Caption=   "TIPO"
            Columns(0).Name =   "TIPO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.CommandButton cmdImprimir 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9270
         Picture         =   "frmInfAhorroNegConcep3Reu.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.CommandButton cmdGrid 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8490
         Picture         =   "frmInfAhorroNegConcep3Reu.frx":0DB4
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   240
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.CommandButton cmdGrafico 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8490
         Picture         =   "frmInfAhorroNegConcep3Reu.frx":0EFE
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdCalendar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2745
         Picture         =   "frmInfAhorroNegConcep3Reu.frx":1240
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdActualizar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8880
         Picture         =   "frmInfAhorroNegConcep3Reu.frx":17CA
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   240
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcFecReu 
         Height          =   285
         Left            =   570
         TabIndex        =   15
         Top             =   240
         Width           =   2130
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Fecha"
         Columns(0).Name =   "FECHA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3519
         Columns(1).Caption=   "Fecha"
         Columns(1).Name =   "FECHACORTA"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   5556
         Columns(2).Caption=   "Referencia"
         Columns(2).Name =   "REF"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   3757
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMon 
         Height          =   285
         Left            =   7380
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   240
         Width           =   1035
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1164
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3651
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1879
         Columns(2).Caption=   "Equivalencia"
         Columns(2).Name =   "EQUIV"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblref 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4020
         TabIndex        =   18
         Top             =   240
         Width           =   2670
      End
      Begin VB.Label lblrefl 
         Caption         =   "Referencia:"
         Height          =   195
         Left            =   3150
         TabIndex        =   17
         Top             =   270
         Width           =   855
      End
      Begin VB.Label lblFecReu 
         Caption         =   "Fecha:"
         Height          =   255
         Left            =   60
         TabIndex        =   16
         Top             =   270
         Width           =   555
      End
   End
   Begin VB.Frame fraPresup 
      Caption         =   "Mostrar resultados desde presupuesto"
      Height          =   700
      Left            =   0
      TabIndex        =   5
      Top             =   720
      Width           =   11175
      Begin VB.CommandButton cmdSelPres 
         Height          =   315
         Left            =   6650
         Picture         =   "frmInfAhorroNegConcep3Reu.frx":1855
         Style           =   1  'Graphical
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   240
         Width           =   345
      End
      Begin VB.CommandButton cmdBorrar 
         Height          =   315
         Left            =   6250
         Picture         =   "frmInfAhorroNegConcep3Reu.frx":18C1
         Style           =   1  'Graphical
         TabIndex        =   0
         Top             =   240
         Width           =   345
      End
      Begin VB.Label lblPres 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   570
         TabIndex        =   8
         Top             =   260
         Width           =   5490
      End
   End
   Begin VB.PictureBox picLegend 
      BorderStyle     =   0  'None
      Height          =   775
      Left            =   9645
      Picture         =   "frmInfAhorroNegConcep3Reu.frx":1966
      ScaleHeight     =   780
      ScaleWidth      =   1620
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   10
      Visible         =   0   'False
      Width           =   1620
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRes 
      Height          =   3915
      Left            =   0
      TabIndex        =   2
      Top             =   1530
      Width           =   11175
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   8
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegConcep3Reu.frx":5668
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4744445
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegConcep3Reu.frx":5684
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroNegConcep3Reu.frx":56A0
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   1111
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "GMN1"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16776960
      Columns(1).Width=   5054
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16776960
      Columns(2).Width=   1773
      Columns(2).Caption=   "Procesos"
      Columns(2).Name =   "PROCE"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   5
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   12632256
      Columns(3).Width=   2884
      Columns(3).Caption=   "Presupuesto"
      Columns(3).Name =   "PRES"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   2
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   15400959
      Columns(4).Width=   2858
      Columns(4).Caption=   "Adjudicado"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   15400959
      Columns(5).Width=   2461
      Columns(5).Caption=   "Ahorro"
      Columns(5).Name =   "AHO"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   2
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "Standard"
      Columns(5).FieldLen=   256
      Columns(6).Width=   1323
      Columns(6).Caption=   "%"
      Columns(6).Name =   "PORCEN"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "0.0#\%"
      Columns(6).FieldLen=   256
      Columns(7).Width=   1270
      Columns(7).Caption=   "Detalle"
      Columns(7).Name =   "DETALLE"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Style=   4
      Columns(7).ButtonsAlways=   -1  'True
      _ExtentX        =   19711
      _ExtentY        =   6906
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   0
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   5490
      Width           =   11175
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   7
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegConcep3Reu.frx":56BC
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegConcep3Reu.frx":56D8
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroNegConcep3Reu.frx":56F4
      DividerType     =   1
      DividerStyle    =   2
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   7
      Columns(0).Width=   2275
      Columns(0).Caption=   "CAPTION"
      Columns(0).Name =   "CAPTION"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2646
      Columns(1).Caption=   "PRESCAPTION"
      Columns(1).Name =   "PRESCAPTION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "PRES"
      Columns(2).Name =   "PRES"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "#,#"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2646
      Columns(3).Caption=   "ADJCAPTION"
      Columns(3).Name =   "ADJCAPTION"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "ADJ"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "#,#"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2090
      Columns(5).Caption=   "AHOCAPTION"
      Columns(5).Name =   "AHOCAPTION"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "AHO"
      Columns(6).Name =   "AHO"
      Columns(6).Alignment=   1
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "#,#"
      Columns(6).FieldLen=   256
      _ExtentX        =   19711
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   3915
      Left            =   0
      OleObjectBlob   =   "frmInfAhorroNegConcep3Reu.frx":5710
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1530
      Width           =   10125
   End
   Begin VB.PictureBox picLegend2 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   9660
      Picture         =   "frmInfAhorroNegConcep3Reu.frx":7136
      ScaleHeight     =   615
      ScaleWidth      =   1635
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   90
      Visible         =   0   'False
      Width           =   1635
   End
End
Attribute VB_Name = "frmInfAhorroNegConcep3Reu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Formulario de listados
Public ofrmLstAhorroNeg As frmLstINFAhorrosNeg

'Variables para func. combos
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean

Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean

Private oMonedas As CMonedas
Private dequivalencia As Double
Private oMon As CMoneda

'Recordset
Private ADORs As Ador.Recordset

'Variable de control de flujo de proceso
Private oGestorReuniones As CGestorReuniones
Private oReuniones As CReuniones

'Variables para el presupuesto4
Public g_sPRES1 As String
Public g_sPRES2 As String
Public g_sPRES3 As String
Public g_sPRES4 As String
Public g_sUON1 As String
Public g_sUON2 As String
Public g_sUON3 As String
Private bRUO As Boolean
Private m_sLblUO  As String
'Variables utilizadas para el multilenguaje
Private sIdiTipoGrafico(5) As String
Private sIdiFecha As String
Private sIdiMonCent As String
Private sIdiTotal As String
Private sIdiReu As String
Private sIdiPresup As String
Private sIdiAdj As String
Private sIdiAhor As String
Private sIdiDetResult As String
Private m_sIdiDetalle As String

Private Sub cmdSelPres_Click()
    frmSELPresUO.sOrigen = "frmInfAhorroNegConcep3Reu"
    frmSELPresUO.bRUO = bRUO
    frmSELPresUO.g_iTipoPres = 3
    frmSELPresUO.bMostrarBajas = True
    frmSELPresUO.Show 1
End Sub

Private Sub BorrarDatosTotales()
    sdbgTotales.RemoveAll
    sdbgTotales.Columns(6).CellStyleSet "Normal"
    sdbgTotales.Refresh
End Sub

Private Sub CargarGrid()
    Dim dpres As Double
    Dim dadj As Double

    dpres = 0
    dadj = 0
    
    sdbgRes.RemoveAll
    sdbgTotales.RemoveAll
        
    While Not ADORs.EOF
        sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
        dpres = dpres + dequivalencia * ADORs(3).Value
        dadj = dadj + dequivalencia * ADORs(4).Value
        ADORs.MoveNext
    Wend
    ADORs.Close
    Set ADORs = Nothing
    
   sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & sIdiPresup & Chr(m_lSeparador) & dpres & Chr(m_lSeparador) & sIdiAdj & Chr(m_lSeparador) & dadj & Chr(m_lSeparador) & sIdiAhor & Chr(m_lSeparador) & dpres - dadj
   
End Sub

Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    Select Case Tipo
    
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                picLegend.Visible = True
                picLegend2.Visible = False
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                picLegend.Visible = False
                picLegend2.Visible = True
                
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                picLegend.Visible = True
                picLegend2.Visible = False
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
        
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset
Dim sCaption1 As String
Dim sCaption2 As String

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRONEG_PRES34_REU, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sCaption1 = Ador(0).Value
        Ador.MoveNext
        lblFecReu.caption = Ador(0).Value & ":"
        sIdiFecha = Ador(0).Value
        sdbcFecReu.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        lblrefl.caption = Ador(0).Value
        sdbcFecReu.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(0).caption = Ador(0).Value
        sdbgRes.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(1).caption = Ador(0).Value
        
        sdbgRes.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.RemoveAll
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        
        Ador.MoveNext
        sCaption2 = Ador(0).Value
        fraPresup.caption = gParametrosGenerales.gsSingPres3
        Ador.MoveNext
        
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).Value
        sIdiPresup = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).Value
        sIdiAdj = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(5).caption = Ador(0).Value
        sIdiAhor = Ador(0).Value

        Ador.MoveNext
        sIdiMonCent = Ador(0).Value
        Ador.MoveNext
        sIdiTotal = Ador(0).Value
        Ador.MoveNext
        sIdiReu = Ador(0).Value

        Ador.MoveNext
        sIdiDetResult = Ador(0).Value
        Ador.MoveNext
        m_sIdiDetalle = Ador(0).Value
        sdbgRes.Columns(7).caption = Ador(0).Value
        Ador.Close
    
    End If

    Me.caption = sCaption1 & " " & gParametrosGenerales.gsSingPres3 & " " & sCaption2
    
    picLegend.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_NEGMATREU_PICLEGEND + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)
    picLegend2.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_NEGMATREU_PICLEGEND2 + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)

    Set Ador = Nothing

End Sub

Private Sub cmdActualizar_Click()

    If sdbcFecReu.Text = "" Then
        oMensajes.NoValido sIdiFecha
        If Me.Visible Then sdbcFecReu.SetFocus
        Exit Sub
    End If
            
    If Not IsDate(sdbcFecReu.Text) Then
        oMensajes.NoValido sIdiFecha
        If Me.Visible Then sdbcFecReu.SetFocus
        Exit Sub
    End If
    If bRUO Then
        If basOptimizacion.gUON1Usuario <> "" And lblPres.caption = "" Then
            ColocarUODeUsuario
        End If
    End If
            
    Screen.MousePointer = vbHourglass
        
    Set ADORs = Nothing
    
    Set ADORs = oGestorInformes.AhorroNegociadoConcep3Fecha(CDate(sdbcFecReu.Columns(0).Value), g_sPRES1, g_sPRES2, g_sPRES3, g_sPRES4, , , True, , True, , g_sUON1, g_sUON2, g_sUON3)
    Screen.MousePointer = vbNormal
    
    If ADORs Is Nothing Then Exit Sub
    
    CargarGrid
    
    If cmdGrid.Visible = True Then
        MSChart1.Visible = True
        MostrarGrafico sdbcTipoGrafico.Value
    End If
    
End Sub

Private Sub cmdBorrar_Click()
    lblPres = ""
    g_sPRES1 = ""
    g_sPRES2 = ""
    g_sPRES3 = ""
    g_sPRES4 = ""
    g_sUON1 = ""
    g_sUON2 = ""
    g_sUON3 = ""
    'BorrarDatosTotales
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalendar_Click()
    AbrirFormCalendar Me, sdbcFecReu
    sdbcFecReu_Validate (True)
End Sub

Private Sub cmdGrafico_Click()
    
        If sdbgRes.Rows = 0 Then
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        sdbcTipoGrafico.Value = sIdiTipoGrafico(2)
        MostrarGrafico sdbcTipoGrafico.Value
        picTipoGrafico.Visible = True
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        MSChart1.Visible = True
        sdbcMon.Visible = False
        Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdGrid_Click()
        
        picTipoGrafico.Visible = False
        sdbcMon.Visible = True
        picLegend.Visible = False
        picLegend2.Visible = False
        
        picLegend.Visible = False
        picLegend2.Visible = False
        
        cmdGrafico.Visible = True
        cmdGrid.Visible = False
        sdbgRes.Visible = True
        MSChart1.Visible = False
    
End Sub

Private Sub cmdImprimir_Click()
    Set ofrmLstAhorroNeg = New frmLstINFAhorrosNeg
    ofrmLstAhorroNeg.sOrigen = "frmInfAhorroNegConcep3Reu"
  
    ofrmLstAhorroNeg.WindowState = vbNormal
    
    If sdbcFecReu <> "" Then
        ofrmLstAhorroNeg.sdbcFecReu.Columns(0).Value = sdbcFecReu.Columns(0).Value
        ofrmLstAhorroNeg.sdbcFecReu.Text = sdbcFecReu.Text
    End If
    ofrmLstAhorroNeg.optReu.Value = True
    ofrmLstAhorroNeg.sdbcMon.Text = sdbcMon.Text
    ofrmLstAhorroNeg.sdbcMon_Validate False

    ofrmLstAhorroNeg.txtEstMat.Text = lblPres.caption
    ofrmLstAhorroNeg.m_sUON1 = g_sUON1
    ofrmLstAhorroNeg.m_sUON2 = g_sUON2
    ofrmLstAhorroNeg.m_sUON3 = g_sUON3
    ofrmLstAhorroNeg.Show 1

End Sub

Private Sub Form_Activate()
    sdbgRes.SelBookmarks.RemoveAll
End Sub

Private Sub Form_Load()
    Me.Height = 6225
    Me.Width = 11325
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    g_sUON1 = ""
    g_sUON2 = ""
    g_sUON3 = ""
    ConfigurarSeguridad
        
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
   
    oMonedas.CargarTodasLasMonedas basParametros.gParametrosGenerales.gsMONCEN, , True, , , , True
    
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiMonCent
    Else
        For Each oMon In oMonedas
            sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
        Next
        sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
        dequivalencia = oMonedas.Item(1).Equiv
    End If
    
    Set oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
    
    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & ""
    
    g_sPRES1 = ""
    g_sPRES2 = ""
    g_sPRES3 = ""
    g_sPRES4 = ""
    
End Sub

Private Sub Form_Resize()
    If Me.Width > 135 Then
        
        sdbgRes.Width = Me.Width - 135
        
        sdbgRes.Columns(0).Width = sdbgRes.Width * 0.1
        sdbgRes.Columns(1).Width = sdbgRes.Width * 0.2
        sdbgRes.Columns(2).Width = sdbgRes.Width * 0.075
        sdbgRes.Columns(3).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(4).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(5).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(6).Width = sdbgRes.Width * 0.1
        sdbgRes.Columns(7).Width = sdbgRes.Width * 0.075 - 300
        
        sdbgTotales.Top = Me.Height - 735
        sdbgTotales.Width = sdbgRes.Width
        
        sdbgTotales.Columns(0).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(1).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(2).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(3).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(4).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(5).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(6).Width = sdbgTotales.Width / 7
        
        MSChart1.Width = Me.Width - 135
        
        Me.fraPresup.Width = sdbgRes.Width
    End If
    
    If Me.Height > 2310 Then
        sdbgRes.Height = Me.Height - 2310
        MSChart1.Height = Me.Height - 2310
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oGestorReuniones = Nothing
    Set oReuniones = Nothing
    Set ADORs = Nothing
    Set oMonedas = Nothing
    Set oMon = Nothing
    g_sPRES1 = ""
    g_sPRES2 = ""
    g_sPRES3 = ""
    g_sPRES4 = ""
    g_sUON1 = ""
    g_sUON2 = ""
    g_sUON3 = ""
    Me.Visible = False
    
End Sub


Private Sub lblPres_Change()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcFecReu_Change()
 
    If Not bRespetarCombo Then
        bCargarComboDesde = True
        If cmdGrafico.Visible Then
            sdbgRes.RemoveAll
        Else
            MSChart1.Visible = False
        End If
        BorrarDatosTotales
        lblref = ""
        Set ADORs = Nothing
    End If

End Sub

Private Sub sdbcFecReu_CloseUp()
    
    If sdbcFecReu.Value = "" Then
        Exit Sub
    End If
    
    MSChart1.Visible = False
    lblref = sdbcFecReu.Columns(2).Text
    sdbcFecReu.Text = sdbcFecReu.Columns(1).Text
    sdbgRes.RemoveAll
    BorrarDatosTotales
    bCargarComboDesde = False
    bRespetarCombo = False
    
End Sub

Private Sub sdbcFecReu_DropDown()
    Dim oReu As CReunion

    Screen.MousePointer = vbHourglass
    sdbcFecReu.RemoveAll
        
    If bCargarComboDesde Then
            
        If IsDate(sdbcFecReu) Then
            Set oReuniones = oGestorReuniones.DevolverReuniones(sdbcFecReu, , , True)
        Else
            Set oReuniones = oGestorReuniones.DevolverReuniones(, , , True)
        End If
    
    Else
        Set oReuniones = oGestorReuniones.DevolverReuniones(, , , True)
        
    End If
    
    For Each oReu In oReuniones
        sdbcFecReu.AddItem oReu.Fecha & Chr(m_lSeparador) & Format(oReu.Fecha, "short date") & " " & Format(oReu.Fecha, "short time") & Chr(m_lSeparador) & oReu.Referencia
    Next
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcFecReu_InitColumnProps()
    
    sdbcFecReu.DataFieldList = "Column 0"
    sdbcFecReu.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcFecReu_PositionList(ByVal Text As String)
 ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcFecReu.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcFecReu.Rows - 1
            bm = sdbcFecReu.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcFecReu.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcFecReu.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub

Private Sub sdbcFecReu_Validate(Cancel As Boolean)

    If Trim(sdbcFecReu.Text = "") Then Exit Sub
    
    If Not IsDate(sdbcFecReu) Then
        oMensajes.NoValido sIdiFecha
        Exit Sub
    End If
    
    If sdbcFecReu.Text = sdbcFecReu.Columns(1).Text Then
        lblref = sdbcFecReu.Columns(2).Text
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    Set oReuniones = oGestorReuniones.DevolverReuniones(sdbcFecReu.Text, sdbcFecReu.Text, , True)
    Screen.MousePointer = vbNormal
    
    If oReuniones.Count = 0 Then
        sdbcFecReu.Text = ""
        oMensajes.NoValido sIdiReu
    Else
        lblref = oReuniones.Item(1).Referencia
    End If
    
End Sub

Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
        bMonRespetarCombo = True
        dequivalencia = 0
        bMonRespetarCombo = False
        bMonCargarComboDesde = True
    End If
    
End Sub

Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
        
End Sub

Private Sub sdbcMon_DropDown()

    sdbcMon.RemoveAll
    
    oMonedas.CargarTodasLasMonedas , , , , , False, True
    
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next

    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcMon.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMon.Rows - 1
            bm = sdbcMon.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMon.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcMon.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
    End If
    
End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbgTotales_RowLoaded(ByVal Bookmark As Variant)
    If sdbgTotales.Columns(6).Value = "" Then Exit Sub
    
    If sdbgTotales.Columns(6).Value < 0 Then
        sdbgTotales.Columns(6).CellStyleSet "Red"
    Else
        If sdbgTotales.Columns(6).Value > 0 Then
            sdbgTotales.Columns(6).CellStyleSet "Green"
        End If
    End If
End Sub
Private Sub sdbgRes_BtnClick()
Dim frm2 As frmInfAhorroNegFechaDetalle

    If sdbgRes.Rows = 0 Then Exit Sub
    
    Set ADORs = Nothing
    
    Screen.MousePointer = vbHourglass
        
    Set ADORs = oGestorInformes.AhorroNegociadoFechaCon3(CDate(sdbcFecReu.Columns(0).Value), g_sPRES1, g_sPRES2, g_sPRES3, g_sPRES4, , , , , , True, , True, , g_sUON1, g_sUON2, g_sUON3)
    
    If ADORs Is Nothing Then Exit Sub
    sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
    Set frm2 = New frmInfAhorroNegFechaDetalle
    frm2.g_sOrigen = "Concep3Reu"
    frm2.dequivalencia = dequivalencia
    frm2.sFecha = sdbcFecReu.Columns(0).Value
    frm2.caption = m_sIdiDetalle & " " & gParametrosGenerales.gsSingPres3 & ": " & sdbgRes.Columns(0).Value
    While Not ADORs.EOF
        frm2.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
        ADORs.MoveNext
    Wend
    ADORs.Close
    Set ADORs = Nothing
    
    frm2.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
    frm2.Left = sdbgRes.Left + Left + 400 + MDI.Left
    
    Screen.MousePointer = vbNormal
    
    frm2.Show 1
    sdbgRes.SelBookmarks.RemoveAll
    
End Sub


Private Sub sdbgRes_DblClick()

    If sdbgRes.Rows = 0 Then Exit Sub
    If sdbgRes.Row < 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass

    If g_sPRES4 <> "" Then

    Else
        If g_sPRES3 <> "" Then
            g_sPRES4 = sdbgRes.Columns(0).Value
            lblPres = m_sLblUO & " " & g_sPRES1 & " - " & g_sPRES2 & " - " & g_sPRES3 & " - " & g_sPRES4 & "  " & sdbgRes.Columns(1).Value
            cmdActualizar_Click
            Screen.MousePointer = vbNormal
        Else
            If g_sPRES2 <> "" Then
                g_sPRES3 = sdbgRes.Columns(0).Value
                lblPres = m_sLblUO & " " & g_sPRES1 & " - " & g_sPRES2 & " - " & g_sPRES3 & "  " & sdbgRes.Columns(1).Value
                cmdActualizar_Click
                Screen.MousePointer = vbNormal
            Else
                If g_sPRES1 <> "" Then
                    g_sPRES2 = sdbgRes.Columns(0).Value
                    lblPres = m_sLblUO & " " & g_sPRES1 & " - " & g_sPRES2 & " - " & sdbgRes.Columns(1).Value
                    cmdActualizar_Click
                    Screen.MousePointer = vbNormal
                Else
                    g_sPRES1 = sdbgRes.Columns(0).Value
                    lblPres = m_sLblUO & " " & g_sPRES1 & " - " & sdbgRes.Columns(1).Value
                    cmdActualizar_Click
                    Screen.MousePointer = vbNormal
                End If
            End If
        End If
    End If

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
    
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
 
End Sub


Public Sub MostrarPresSeleccionado()
    
    g_sPRES1 = frmSELPresUO.g_sPRES1
    g_sPRES2 = frmSELPresUO.g_sPRES2
    g_sPRES3 = frmSELPresUO.g_sPRES3
    g_sPRES4 = frmSELPresUO.g_sPRES4
    g_sUON1 = frmSELPresUO.g_sUON1
    g_sUON2 = frmSELPresUO.g_sUON2
    g_sUON3 = frmSELPresUO.g_sUON3
    
    m_sLblUO = ""
    If g_sUON1 <> "" Then
        m_sLblUO = "(" & g_sUON1
        If g_sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & g_sUON2
            If g_sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & g_sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = m_sLblUO & ") "
        End If
    End If
    
    If g_sPRES4 <> "" Then
        lblPres = m_sLblUO & g_sPRES1 & " - " & g_sPRES2 & " - " & g_sPRES3 & " - " & g_sPRES4 & " " & frmSELPresUO.g_sDenPres
    ElseIf g_sPRES3 <> "" Then
        lblPres = m_sLblUO & g_sPRES1 & " - " & g_sPRES2 & " - " & g_sPRES3 & " " & frmSELPresUO.g_sDenPres
    ElseIf g_sPRES2 <> "" Then
        lblPres = m_sLblUO & g_sPRES1 & " - " & g_sPRES2 & " " & frmSELPresUO.g_sDenPres
    ElseIf g_sPRES1 <> "" Then
        lblPres = m_sLblUO & g_sPRES1 & " " & frmSELPresUO.g_sDenPres
    Else
        lblPres = m_sLblUO
    End If
        
    
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
    
End Sub

Private Sub ColocarUODeUsuario()
    g_sUON1 = NullToStr(basOptimizacion.gUON1Usuario)
    g_sUON2 = NullToStr(basOptimizacion.gUON2Usuario)
    g_sUON3 = NullToStr(basOptimizacion.gUON3Usuario)
    m_sLblUO = ""
    If g_sUON1 <> "" Then
        m_sLblUO = "(" & g_sUON1
        If g_sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & g_sUON2
            If g_sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & g_sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = m_sLblUO & ") "
        End If
    End If
    lblPres = m_sLblUO

End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    bRUO = False
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegCon3RestUO)) Is Nothing) Then
        bRUO = True
        ColocarUODeUsuario
    End If
End Sub

