VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{14ACBB92-9C4A-4C45-AFD2-7AE60E71E5B3}#4.0#0"; "IGSplitter40.ocx"
Begin VB.Form frmPedidosEmision2 
   Caption         =   "Emisi�n de pedidos directos"
   ClientHeight    =   7680
   ClientLeft      =   2055
   ClientTop       =   2940
   ClientWidth     =   10575
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPedidosEmision2.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7680
   ScaleWidth      =   10575
   Begin SSSplitter.SSSplitter SSSplitter1 
      Height          =   6720
      Left            =   135
      TabIndex        =   8
      Top             =   855
      Width           =   10275
      _ExtentX        =   18124
      _ExtentY        =   11853
      _Version        =   262144
      PaneTree        =   "frmPedidosEmision2.frx":014A
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   3555
         Left            =   30
         ScaleHeight     =   3555
         ScaleWidth      =   10215
         TabIndex        =   9
         Top             =   30
         Width           =   10215
         Begin VB.TextBox txtCodPedERPFact 
            BackColor       =   &H00C0FFFF&
            Height          =   285
            Left            =   7140
            Locked          =   -1  'True
            TabIndex        =   31
            Top             =   1050
            Width           =   2955
         End
         Begin VB.TextBox txtViaPago 
            BackColor       =   &H00C0FFFF&
            Height          =   285
            Left            =   1275
            Locked          =   -1  'True
            TabIndex        =   29
            Top             =   1080
            Width           =   3075
         End
         Begin VB.TextBox txtNumEntrega 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            Height          =   285
            Left            =   1275
            Locked          =   -1  'True
            TabIndex        =   17
            Text            =   " "
            Top             =   75
            Width           =   1470
         End
         Begin VB.TextBox txtImporteTotal 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            Height          =   285
            Left            =   4950
            Locked          =   -1  'True
            TabIndex        =   16
            Top             =   75
            Width           =   1470
         End
         Begin VB.TextBox txtReceptor 
            BackColor       =   &H00C0FFFF&
            Height          =   285
            Left            =   1275
            Locked          =   -1  'True
            TabIndex        =   15
            Top             =   410
            Width           =   3075
         End
         Begin VB.TextBox txtCodProveERP 
            BackColor       =   &H00C0FFFF&
            Height          =   285
            Left            =   7140
            Locked          =   -1  'True
            TabIndex        =   14
            Top             =   390
            Width           =   2955
         End
         Begin VB.TextBox txtObs 
            BackColor       =   &H00C0FFFF&
            Height          =   810
            Left            =   1275
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   13
            Top             =   1395
            Width           =   8880
         End
         Begin VB.TextBox txtPago 
            BackColor       =   &H00C0FFFF&
            Height          =   285
            Left            =   1275
            Locked          =   -1  'True
            TabIndex        =   12
            Top             =   745
            Width           =   3075
         End
         Begin VB.TextBox txtMon 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            Height          =   285
            Left            =   9135
            Locked          =   -1  'True
            TabIndex        =   11
            Top             =   75
            Width           =   975
         End
         Begin VB.TextBox txtCodPedERP 
            BackColor       =   &H00C0FFFF&
            Height          =   285
            Left            =   7140
            Locked          =   -1  'True
            TabIndex        =   10
            Top             =   720
            Width           =   2955
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
            Height          =   1200
            Left            =   1305
            TabIndex        =   18
            Top             =   2250
            Width           =   8850
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   5
            stylesets.count =   2
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPedidosEmision2.frx":019C
            stylesets(1).Name=   "NormalInterno"
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmPedidosEmision2.frx":01B8
            stylesets(1).AlignmentPicture=   1
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   2725
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HasHeadForeColor=   -1  'True
            Columns(0).HasHeadBackColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).HeadBackColor=   -2147483633
            Columns(0).BackColor=   16777152
            Columns(1).Width=   5821
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   200
            Columns(1).Locked=   -1  'True
            Columns(1).HasHeadForeColor=   -1  'True
            Columns(1).HasHeadBackColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).HeadBackColor=   -2147483633
            Columns(1).BackColor=   16777152
            Columns(2).Width=   6006
            Columns(2).Caption=   "Valor"
            Columns(2).Name =   "VALOR"
            Columns(2).Alignment=   1
            Columns(2).CaptionAlignment=   0
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   800
            Columns(2).Locked=   -1  'True
            Columns(2).HasHeadForeColor=   -1  'True
            Columns(2).HasHeadBackColor=   -1  'True
            Columns(2).HeadBackColor=   -2147483633
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "ID_A"
            Columns(3).Name =   "ID"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "INTERNO"
            Columns(4).Name =   "INTERNO"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   11
            Columns(4).FieldLen=   256
            _ExtentX        =   15610
            _ExtentY        =   2117
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblCodPedERPFact 
            Caption         =   "DC�d. ERP proveedor factura:"
            Height          =   240
            Left            =   4710
            TabIndex        =   32
            Top             =   1095
            Width           =   2400
         End
         Begin VB.Label lblViaPago 
            Caption         =   "Via Pago:"
            Height          =   240
            Left            =   60
            TabIndex        =   30
            Top             =   1102
            Width           =   1110
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "Orden de entrega:"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   60
            TabIndex        =   27
            Top             =   97
            Width           =   1110
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Importe total:"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   3870
            TabIndex        =   26
            Top             =   105
            Width           =   1455
         End
         Begin VB.Label lblReceptor 
            Caption         =   "DReceptor:"
            Height          =   240
            Left            =   60
            TabIndex        =   25
            Top             =   432
            Width           =   1110
         End
         Begin VB.Label lblCodProveERP 
            Caption         =   "DC�digo proveedor en el ERP:"
            Height          =   240
            Left            =   4710
            TabIndex        =   24
            Top             =   465
            Width           =   2400
         End
         Begin VB.Label lblObs 
            Caption         =   "Observaciones:"
            Height          =   240
            Left            =   60
            TabIndex        =   23
            Top             =   1440
            Width           =   1110
         End
         Begin VB.Label lblMon 
            Caption         =   "Moneda de emisi�n:"
            Height          =   240
            Left            =   7290
            TabIndex        =   22
            Top             =   105
            Width           =   1740
         End
         Begin VB.Label lblPago 
            AutoSize        =   -1  'True
            Caption         =   "Forma pago:"
            Height          =   315
            Left            =   60
            TabIndex        =   21
            Top             =   645
            Width           =   1110
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblCodPedERP 
            Caption         =   "DC�digo pedido en el ERP:"
            Height          =   240
            Left            =   4710
            TabIndex        =   20
            Top             =   765
            Width           =   2400
         End
         Begin VB.Label lblMasDatos 
            Caption         =   "M�s datos:"
            Height          =   255
            Left            =   90
            TabIndex        =   19
            Top             =   2205
            Width           =   1035
         End
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgMiPedido 
         Height          =   3015
         Left            =   30
         TabIndex        =   28
         Top             =   3675
         Width           =   10215
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   16
         stylesets.count =   3
         stylesets(0).Name=   "gris"
         stylesets(0).ForeColor=   12632256
         stylesets(0).BackColor=   12632256
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmPedidosEmision2.frx":0405
         stylesets(1).Name=   "NormalInterno"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmPedidosEmision2.frx":0421
         stylesets(1).AlignmentPicture=   1
         stylesets(2).Name=   "Tahoma"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmPedidosEmision2.frx":066E
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         HeadStyleSet    =   "Tahoma"
         StyleSet        =   "Tahoma"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   16
         Columns(0).Width=   3916
         Columns(0).Caption=   "Art�culo"
         Columns(0).Name =   "Art"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).ButtonsAlways=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777152
         Columns(1).Width=   1667
         Columns(1).Caption=   "Destino"
         Columns(1).Name =   "Destino"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777152
         Columns(2).Width=   1455
         Columns(2).Caption=   "Unidad"
         Columns(2).Name =   "Unidad"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16777152
         Columns(3).Width=   2064
         Columns(3).Caption=   "Cantidad"
         Columns(3).Name =   "Cantidad"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "Standard"
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   16777152
         Columns(4).Width=   1931
         Columns(4).Caption=   "P.U."
         Columns(4).Name =   "P.U."
         Columns(4).Alignment=   1
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).NumberFormat=   "Standard"
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(4).HasBackColor=   -1  'True
         Columns(4).BackColor=   12778492
         Columns(5).Width=   2249
         Columns(5).Caption=   "DImporte"
         Columns(5).Name =   "IMPORTE"
         Columns(5).Alignment=   1
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(5).HasBackColor=   -1  'True
         Columns(5).BackColor=   16777152
         Columns(6).Width=   1984
         Columns(6).Caption=   "Fecha entrega"
         Columns(6).Name =   "Fecha entrega"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(6).HasBackColor=   -1  'True
         Columns(6).BackColor=   16777152
         Columns(7).Width=   1164
         Columns(7).Caption=   "Obligatoria"
         Columns(7).Name =   "Obligatoria"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).Style=   2
         Columns(7).HasBackColor=   -1  'True
         Columns(7).BackColor=   16777152
         Columns(8).Width=   767
         Columns(8).Caption=   "Observaciones"
         Columns(8).Name =   "Observaciones"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Style=   4
         Columns(8).ButtonsAlways=   -1  'True
         Columns(8).HasBackColor=   -1  'True
         Columns(8).BackColor=   16777152
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "OBSERVAC"
         Columns(9).Name =   "OBSERVAC"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Caption=   "PRES1"
         Columns(10).Name=   "PRES1"
         Columns(10).CaptionAlignment=   0
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(10).Style=   1
         Columns(10).ButtonsAlways=   -1  'True
         Columns(11).Width=   3200
         Columns(11).Caption=   "PRES2"
         Columns(11).Name=   "PRES2"
         Columns(11).CaptionAlignment=   0
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(11).Style=   1
         Columns(11).ButtonsAlways=   -1  'True
         Columns(12).Width=   3200
         Columns(12).Caption=   "PRES3"
         Columns(12).Name=   "PRES3"
         Columns(12).CaptionAlignment=   0
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(12).Style=   1
         Columns(12).ButtonsAlways=   -1  'True
         Columns(13).Width=   3200
         Columns(13).Caption=   "PRES4"
         Columns(13).Name=   "PRES4"
         Columns(13).CaptionAlignment=   0
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(13).Style=   1
         Columns(13).ButtonsAlways=   -1  'True
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "PedidoAbierto"
         Columns(14).Name=   "PedidoAbierto"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "TIPORECEPCION"
         Columns(15).Name=   "TIPORECEPCION"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         _ExtentX        =   18018
         _ExtentY        =   5318
         _StockProps     =   79
         ForeColor       =   0
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdImprimir 
      Height          =   285
      Left            =   10080
      Picture         =   "frmPedidosEmision2.frx":068A
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   345
   End
   Begin VB.TextBox txtEmpresa 
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   5940
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   75
      Width           =   4080
   End
   Begin VB.TextBox txtAnyo 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   1220
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   60
      Width           =   650
   End
   Begin VB.TextBox txtNumPedido 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   3220
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   60
      Width           =   1470
   End
   Begin TabDlg.SSTab sstabPedido 
      Height          =   7245
      Left            =   45
      TabIndex        =   0
      Top             =   420
      Width           =   10470
      _ExtentX        =   18468
      _ExtentY        =   12779
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmPedidosEmision2.frx":0B9C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).ControlCount=   0
   End
   Begin VB.Label lblEmpresa 
      Caption         =   "DEmpresa:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4785
      TabIndex        =   6
      Top             =   75
      Width           =   1095
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Cesta:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   2220
      TabIndex        =   5
      Top             =   90
      Width           =   900
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "A�o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   60
      TabIndex        =   4
      Top             =   90
      Width           =   615
   End
End
Attribute VB_Name = "frmPedidosEmision2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Valor m�ximo de NVarcha(MAX)
Private Const cLongAtribEsp_Texto As Long = 32471

Public oPedido As CPedido
Public sObservaciones As String
Public oDestinos As CDestinos
Public oPagos As CPagos
Public oViaPagos As CViaPagos
Public oUnidades As CUnidades
Public sOrigen As String

Private dequivalencia As Double
Private oOrden As COrdenEntrega
Private m_sLayOut As String
Private m_bCargarLayout As Boolean
Private oMonedas As CMonedas
Private splantilla As String
Private dImporteTotal As Double
Private sFormatoNumber As String
Private oProves As CProveedores
Private sIdiMonCent As String
Private m_sIdiFalse As String
Private m_sIdiTrue As String
Private m_sdbgItemPorProve(8) As String

Private m_bRPresup1UO As Boolean
Private m_bRPresup1UORama As Boolean
Private m_bRPresup2UO As Boolean
Private m_bRPresup2UORama As Boolean
Private m_bRPresup3UO As Boolean
Private m_bRPresup3UORama As Boolean
Private m_bRPresup4UO As Boolean
Private m_bRPresup4UORama As Boolean

Private m_oEmpresas As CEmpresas
Private m_sDenPais As String
Private m_sDenProvi As String
Private Sub cmdImprimir_Click()

Dim appexcel  As Object
Dim docPetExcel As Object

    If appexcel Is Nothing Then
        Set appexcel = CreateObject("Excel.Application")
    End If
    Set docPetExcel = NotificacionPedidoExcel(appexcel)
    If Not docPetExcel Is Nothing Then
        appexcel.Visible = True
    End If
End Sub

''' <summary>
''' Sacar en formato excel el pedido directo
''' </summary>
''' <param name="appexcel">Objeto excel</param>
''' <returns>Excel con el pedido directo</returns>
''' <remarks>Llamada desde: cmdImprimir_Click; Tiempo m�ximo:0,3</remarks>
''' <revision>LTG 12/07/2012</revision>

Private Function NotificacionPedidoExcel(appexcel As Object) As Object
    Dim xlBook As Object
    Dim xlSheet As Object
    Dim j As Integer
    Dim i As Integer
    Dim oFos As Scripting.FileSystemObject
    Dim inum As Integer
    Dim oAlmacenes As CAlmacenes
    Dim xlSheetPl As Object
    Dim xlSheetP2 As Object
    Dim lRet As Long
    Dim oRPedido As CRPedido
    
    Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
    oAlmacenes.CargarTodosLosAlmacenes
    
    Dim oEmpresas As CEmpresas
    Dim sCarpeta As String
    Dim sMarcaPlantillas As String
    If InStr(gParametrosInstalacion.gsPedidoXLT, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
        'Buscamos la empresa CARPETA_PLANTILLAS
        Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
        oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , , , , oOrden.Empresa
        If oEmpresas.Count > 0 Then sCarpeta = oEmpresas.Item(1).Carpeta_Plantillas
    End If
    If sCarpeta = "" And gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    
    Set oFos = New Scripting.FileSystemObject
    If gParametrosInstalacion.gsPedidoXLT = "" Then
        
        If gParametrosGenerales.gsPedidoXLT = "" Then
            oMensajes.NoValido splantilla
            Set NotificacionPedidoExcel = Nothing
            Exit Function
        Else
            gParametrosInstalacion.gsPedidoXLT = gParametrosGenerales.gsPedidoXLT
            g_GuardarParametrosIns = True
        End If
    
    End If
    
    If Right(gParametrosInstalacion.gsPedidoXLT, 3) <> "xlt" Then
        oMensajes.NoValido splantilla & ":" & Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)
        Set NotificacionPedidoExcel = Nothing
        Exit Function
    End If
        
    If Not oFos.FileExists(Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)) Then
        oMensajes.PlantillaNoEncontrada Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)
        Set NotificacionPedidoExcel = Nothing
        Exit Function
    End If
        
    Set xlBook = appexcel.Workbooks.Add(Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta))
      
    For j = 2 To oPedido.OrdenesEntrega.Count
       
        xlBook.Sheets.Item(1).Copy xlBook.Sheets.Item(1)
        xlBook.Sheets.Item(3).Copy xlBook.Sheets.Item(2)
    Next
    
    If gParametrosGenerales.gbPedidosDirFac Then m_oEmpresas.Item(1).CargarDireccionesEnvio
    
    j = 1
    
    For i = 1 To oPedido.OrdenesEntrega.Count
    
        Set oProves = Nothing
        Set oProves = oFSGSRaiz.generar_CProveedores
        
        oProves.Add oPedido.OrdenesEntrega.Item(i).ProveCod, oPedido.OrdenesEntrega.Item(i).ProveDen
        oProves.CargarDatosProveedor oPedido.OrdenesEntrega.Item(i).ProveCod
    
        If Not IsNull(oPedido.OrdenesEntrega.Item(i).ProveCod) Then
            oPagos.CargarTodosLosPagosDesde 1, NullToStr(oProves.Item(1).FormaPagoCod), , , True
        End If
        
        If Not IsNull(oPedido.OrdenesEntrega.Item(i).ProveCod) Then
            oViaPagos.CargarTodosLosPagosDesde 1, NullToStr(oProves.Item(1).ViaPagoCod), , , True
        End If

        oPedido.OrdenesEntrega.Item(i).LeerPersonaProceso
        oPedido.OrdenesEntrega.Item(i).LeerReceptorProceso
        
        Set oDestinos = oFSGSRaiz.Generar_CDestinos
        oDestinos.CargarTodosLosDestinos , , , , , , , , , , True, True
    
        xlBook.Sheets.Item(j).Name = oProves.Item(1).Cod
    
        Set xlSheet = xlBook.Sheets.Item(j)
        j = j + 1
        Set xlSheetPl = xlBook.Sheets.Item(j)
        j = j + 1
        Set xlSheetP2 = xlBook.Sheets.Item(j)
        j = j + 1
        inum = InStr(xlSheetPl.Name, "(") - 2
        xlSheetPl.Name = Left(Left(xlSheetPl.Name, IIf(inum > -1, inum, 100)) & " " & oProves.Item(1).Cod, 31)
        
        Set oRPedido = New CRPedido
        lRet = oRPedido.HojaPedidoExcel(xlSheet, xlSheetPl, oPedido.OrdenesEntrega.Item(i), m_oEmpresas.Item(1), oProves.Item(1), "#,##0.00", oAlmacenes, _
                                        oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 64), oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 63), oFSGSRaiz, gParametrosGenerales, gParametrosInstalacion, xlSheetP2, g_oParametrosSM)
        If lRet <> 0 Then
            If lRet = 7 Then
                NotificacionPedidoExcel = xlBook
            End If
            
            appexcel.Quit
            Set appexcel = Nothing
            Exit Function
        End If
    Next 'For Ordenes
    
    xlBook.Sheets.Item(1).Select
    
    Set NotificacionPedidoExcel = xlBook
    Set oFos = Nothing
End Function

Private Sub Arrange()

On Error Resume Next

    If Me.Height < 2200 Then Exit Sub
    If Me.Width < 2000 Then Exit Sub
    
    sstabPedido.Height = Me.Height - 1050
    sstabPedido.Width = Me.Width - 250
    SSSplitter1.Width = sstabPedido.Width - 270
    SSSplitter1.Height = sstabPedido.Height - 550
    'sdbgMiPedido.Width = sstabPedido.Width - 270
    
    'sdbgMiPedido.Height = sstabPedido.Height - Picture1.Height - 600
     
    sdbgMiPedido.Columns(0).Width = sdbgMiPedido.Width * 20 / 100
    sdbgMiPedido.Columns(1).Width = sdbgMiPedido.Width * 11 / 100
    sdbgMiPedido.Columns(2).Width = sdbgMiPedido.Width * 11 / 100
    sdbgMiPedido.Columns(3).Width = sdbgMiPedido.Width * 11 / 100
    sdbgMiPedido.Columns(4).Width = sdbgMiPedido.Width * 11 / 100
    sdbgMiPedido.Columns(5).Width = sdbgMiPedido.Width * 11 / 100
    sdbgMiPedido.Columns(6).Width = sdbgMiPedido.Width * 12.2 / 100
    sdbgMiPedido.Columns(7).Width = sdbgMiPedido.Width * 6.8 / 100
    sdbgMiPedido.Columns(8).Width = sdbgMiPedido.Width * 4.5 / 100

End Sub
Private Sub Form_Load()
Dim i As Integer
Dim j As Integer
Dim oPaises As CPaises
Dim sTemp As String

    Me.Width = 10695
    Me.Height = 8085

    Arrange
    
    ConfigurarSeguridad
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    For i = 0 To 8
        sdbgMiPedido.Columns(i).caption = m_sdbgItemPorProve(i)
    Next
  
    sdbgMiPedido.Columns("PRES1").caption = gParametrosGenerales.gsSingPres1
    sdbgMiPedido.Columns("PRES2").caption = gParametrosGenerales.gsSingPres2
    sdbgMiPedido.Columns("PRES3").caption = gParametrosGenerales.gsSingPres3
    sdbgMiPedido.Columns("PRES4").caption = gParametrosGenerales.gsSingPres4
    If gParametrosGenerales.gbUsarPres1 And gParametrosGenerales.gbUsarPedPres1 Then
        sdbgMiPedido.Columns("PRES1").Visible = True
    Else
        sdbgMiPedido.Columns("PRES1").Visible = False
    End If
    If gParametrosGenerales.gbUsarPres2 And gParametrosGenerales.gbUsarPedPres2 Then
        sdbgMiPedido.Columns("PRES2").Visible = True
    Else
        sdbgMiPedido.Columns("PRES2").Visible = False
    End If
    If gParametrosGenerales.gbUsarPres3 And gParametrosGenerales.gbUsarPedPres3 Then
        sdbgMiPedido.Columns("PRES3").Visible = True
    Else
        sdbgMiPedido.Columns("PRES3").Visible = False
    End If
    If gParametrosGenerales.gbUsarPres4 And gParametrosGenerales.gbUsarPedPres4 Then
        sdbgMiPedido.Columns("PRES4").Visible = True
    Else
        sdbgMiPedido.Columns("PRES4").Visible = False
    End If
    

    If gParametrosGenerales.gbPedidosIncluirViaPago Then
        lblViaPago.Visible = True
        txtViaPago.Visible = True
    Else
        lblViaPago.Visible = False
        txtViaPago.Visible = False
    End If
    
    'Tengo que guardar la estructura original de la grid para los atributos
    sTemp = DevolverPathFichTemp
    'He puesto el gui�n bajo porque desde frmPEDIDOS se utiliza tambi�n ese nombre y concatena un n�mero en el caso deque exista el fichero, si por lo que fuera coincide casca
    m_sLayOut = sTemp & "LayMiPedido_2"
    sdbgMiPedido.SaveLayout m_sLayOut, ssSaveLayoutAll
        
    sFormatoNumber = "#,##0.########"
    
    'Carga la empresa
    Set m_oEmpresas = oFSGSRaiz.Generar_CEmpresas
    m_oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , , , , oPedido.OrdenesEntrega.Item(1).Empresa
    If Not IsNull(m_oEmpresas.Item(1).CodPais) Then
        Set oPaises = oFSGSRaiz.Generar_CPaises
        oPaises.CargarTodosLosPaises m_oEmpresas.Item(1).CodPais, , True
        m_sDenPais = oPaises.Item(1).Den
    
        If Not IsNull(m_oEmpresas.Item(1).CodProvi) Then
            oPaises.Item(1).CargarTodasLasProvincias m_oEmpresas.Item(1).CodProvi, , True
            m_sDenProvi = oPaises.Item(1).Provincias.Item(1).Den
        End If
    End If
    Set oPaises = Nothing
    
    Set oPagos = oFSGSRaiz.generar_CPagos
    Set oViaPagos = oFSGSRaiz.Generar_CViaPagos
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
        
    'Carga la cabecera
    txtAnyo.Text = oPedido.Anyo
    txtNumPedido.Text = oPedido.Numero
    txtEmpresa.Text = m_oEmpresas.Item(1).Den
    
    'Carga los tabs de proveedores y rellena el primero de ellos
    sstabPedido.Tabs = oPedido.OrdenesEntrega.Count
    sstabPedido.TabsPerRow = oPedido.OrdenesEntrega.Count
    

    For i = 1 To oPedido.OrdenesEntrega.Count
        sstabPedido.TabCaption(j) = oPedido.OrdenesEntrega.Item(i).ProveCod
        j = j + 1
    Next
    
    
    If gParametrosGenerales.gbTraspasoPedidoERP = False Or (gParametrosGenerales.gbTraspasoPedidoERP = True And m_oEmpresas.Item(1).erp = False) Then
        'Si la empresa seleccionada no est� marcada para integraci�n no se muestra el c�digo ERP
        lblCodProveERP.Visible = False
        txtCodProveERP.Visible = False
        txtCodProveERP.Text = ""
    Else
        lblCodProveERP.Visible = True
        txtCodProveERP.Visible = True
        txtCodProveERP.Text = ""
    End If
    
    CargarDatosPedido (1)
    

End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim FOSFile As Scripting.FileSystemObject

    Set m_oEmpresas = Nothing
    Set oDestinos = Nothing
    Set oPagos = Nothing
    Set oViaPagos = Nothing
    Set oUnidades = Nothing
    Set oMonedas = Nothing
    
On Error Resume Next
    Set FOSFile = New Scripting.FileSystemObject
    
    FOSFile.DeleteFile m_sLayOut, True 'Borro el layout de la grid
    
    Set FOSFile = Nothing
    
End Sub

Private Sub sdbgAtributos_RowLoaded(ByVal Bookmark As Variant)

    If sdbgAtributos.Columns("INTERNO").Value Then
        sdbgAtributos.Columns("DEN").CellStyleSet "NormalInterno", sdbgAtributos.Row
    End If

End Sub

Private Sub sdbgMiPedido_BtnClick()
Dim inum As Integer
Dim oLineaPedido As CLineaPedido
Dim dblAbierto As Double
Dim iTipo As Integer

inum = sstabPedido.Tab

If sdbgMiPedido.Columns(sdbgMiPedido.col).Name = "Observaciones" Then
    sObservaciones = ""
    If oPedido.OrdenesEntrega.Item(sstabPedido.TabCaption(inum)).LineasPedido.Item(sdbgMiPedido.Row + 1).Observaciones <> "" Then
       sObservaciones = oPedido.OrdenesEntrega.Item(sstabPedido.TabCaption(inum)).LineasPedido.Item(sdbgMiPedido.Row + 1).Observaciones
    Else
        oMensajes.NoHayObservaciones
        Exit Sub
    End If
    sObservaciones = FSGSForm.MostrarFormComenEstado(oGestorIdiomas, sObservaciones, True, basPublic.gParametrosInstalacion.gIdioma, "frmPedidosEmision2")
End If

If Left(sdbgMiPedido.Columns(sdbgMiPedido.col).Name, 4) = "PRES" Then
        iTipo = Right(sdbgMiPedido.Columns(sdbgMiPedido.col).Name, 1)
        frmPRESAsig.g_iTipoPres = iTipo
        
        dblAbierto = NullToDbl0(sdbgMiPedido.Columns("Cantidad").Value * sdbgMiPedido.Columns("P.U.").Value)
        frmPRESAsig.g_dblAbierto = dblAbierto
        Set oLineaPedido = oPedido.OrdenesEntrega.Item(sstabPedido.TabCaption(inum)).LineasPedido.Item(sdbgMiPedido.Row + 1)
        frmPRESAsig.g_bModif = False
        Select Case iTipo
            Case 1
                    frmPRESAsig.g_bRUO = m_bRPresup1UO
                    frmPRESAsig.g_bRUORama = m_bRPresup1UORama
                    If oLineaPedido.Presupuestos1 Is Nothing Then
                        Set oLineaPedido.Presupuestos1 = oFSGSRaiz.Generar_CPresProyectosNivel4
                    End If
                    Set frmPRESAsig.m_oPresupuestos1Asignados = oLineaPedido.Presupuestos1
                    If Not oLineaPedido.Presupuestos1 Is Nothing Then
                        If oLineaPedido.Presupuestos1.Count > 0 Then
                            If dblAbierto = 0 Then
                                frmPRESAsig.g_dblAsignado = 100
                            Else
                                frmPRESAsig.g_dblAsignado = dblAbierto
                            End If
                            frmPRESAsig.g_bHayPres = True
                        Else
                            frmPRESAsig.g_dblAsignado = 0
                            frmPRESAsig.g_bHayPres = False
                        End If
                    Else
                        frmPRESAsig.g_dblAsignado = 0
                        frmPRESAsig.g_bHayPres = False
                    End If
            Case 2
                    frmPRESAsig.g_bRUO = m_bRPresup2UO
                    frmPRESAsig.g_bRUORama = m_bRPresup2UORama
                    If oLineaPedido.Presupuestos2 Is Nothing Then
                        Set oLineaPedido.Presupuestos2 = oFSGSRaiz.Generar_CPresContablesNivel4
                    End If
                    Set frmPRESAsig.m_oPresupuestos2Asignados = oLineaPedido.Presupuestos2
                    If Not oLineaPedido.Presupuestos2 Is Nothing Then
                        If oLineaPedido.Presupuestos2.Count > 0 Then
                            If dblAbierto = 0 Then
                                frmPRESAsig.g_dblAsignado = 100
                            Else
                                frmPRESAsig.g_dblAsignado = dblAbierto
                            End If
                            frmPRESAsig.g_bHayPres = True
                        Else
                            frmPRESAsig.g_dblAsignado = 0
                            frmPRESAsig.g_bHayPres = False
                        End If
                    Else
                        frmPRESAsig.g_dblAsignado = 0
                        frmPRESAsig.g_bHayPres = False
                    End If
            Case 3
                    frmPRESAsig.g_bRUO = m_bRPresup3UO
                    frmPRESAsig.g_bRUORama = m_bRPresup3UORama
                    If oLineaPedido.Presupuestos3 Is Nothing Then
                        Set oLineaPedido.Presupuestos3 = oFSGSRaiz.Generar_CPresConceptos3Nivel4
                    End If
                    Set frmPRESAsig.m_oPresupuestos3Asignados = oLineaPedido.Presupuestos3
                    If Not oLineaPedido.Presupuestos3 Is Nothing Then
                        If oLineaPedido.Presupuestos3.Count > 0 Then
                            If dblAbierto = 0 Then
                                frmPRESAsig.g_dblAsignado = 100
                            Else
                                frmPRESAsig.g_dblAsignado = dblAbierto
                            End If
                            frmPRESAsig.g_bHayPres = True

                        Else
                            frmPRESAsig.g_dblAsignado = 0
                            frmPRESAsig.g_bHayPres = False
                        End If
                    Else
                        frmPRESAsig.g_dblAsignado = 0
                        frmPRESAsig.g_bHayPres = False
                    End If
            Case 4
                    frmPRESAsig.g_bRUO = m_bRPresup4UO
                    frmPRESAsig.g_bRUORama = m_bRPresup4UORama
                    If oLineaPedido.Presupuestos4 Is Nothing Then
                        Set oLineaPedido.Presupuestos4 = oFSGSRaiz.Generar_CPresConceptos4Nivel4
                    End If
                    Set frmPRESAsig.m_oPresupuestos4Asignados = oLineaPedido.Presupuestos4
                    If Not oLineaPedido.Presupuestos4 Is Nothing Then
                        If oLineaPedido.Presupuestos4.Count > 0 Then
                            If dblAbierto = 0 Then
                                frmPRESAsig.g_dblAsignado = 100
                            Else
                                frmPRESAsig.g_dblAsignado = dblAbierto
                            End If
                            frmPRESAsig.g_bHayPres = True
                        Else
                            frmPRESAsig.g_dblAsignado = 0
                            frmPRESAsig.g_bHayPres = False
                        End If
                    Else
                        frmPRESAsig.g_dblAsignado = 0
                        frmPRESAsig.g_bHayPres = False
                    End If
        End Select
        frmPRESAsig.g_sOrigen = "frmPedidosEmision2"
        frmPRESAsig.g_bHayPresBajaLog = False
        frmPRESAsig.Show 1
End If
End Sub

Private Sub sdbgMiPedido_RowLoaded(ByVal Bookmark As Variant)
    With sdbgMiPedido
        If .Columns("PEDIDOABIERTO").Value = 1 Then
            .Columns("Cantidad").CellStyleSet "gris"
        Else
            .Columns("Cantidad").CellStyleSet ""
        End If
        If .Columns("TIPORECEPCION").Value = 1 Then
            .Columns("Cantidad").CellStyleSet "gris", .Row
            .Columns("P.U.").CellStyleSet "gris", .Row
        End If
    End With
    
End Sub

Private Sub SSSplitter1_Resize(ByVal BorderPanes As SSSplitter.Panes)
On Error Resume Next
        sdbgAtributos.Width = SSSplitter1.Panes(0).Width - sdbgAtributos.Left - 200
    sdbgAtributos.Columns("COD").Width = (sdbgAtributos.Width) * 20 / 100
    sdbgAtributos.Columns("DEN").Width = (sdbgAtributos.Width) * 40 / 100
    sdbgAtributos.Columns("VALOR").Width = (sdbgAtributos.Width * 40 / 100) - 560
    
End Sub

Private Sub SSSplitter1_SplitterEndDrag()
On Error Resume Next
If sdbgAtributos.Visible Then
    If SSSplitter1.Panes(0).Height - sdbgAtributos.Top - 200 > 300 Then
        sdbgAtributos.Height = SSSplitter1.Panes(0).Height - sdbgAtributos.Top - 200
    End If
End If

End Sub


Private Sub sstabPedido_Click(PreviousTab As Integer)
    CargarDatosPedido (sstabPedido.Tab + 1)
End Sub

'''<summary>Muestra en pantalla los datos del pedido</summary>
'''<param name="iNumero">Los datos del pedido que se quieren mostrar</param>
'''<remarks>Llamada desde: Form_Load</remarks>
'''<revision>LTG 23/05/2013</revision>

Private Sub CargarDatosPedido(ByVal iNumero As Integer)
    Dim oLineaPedido As CLineaPedido
    Dim sLinea As String
    Dim bAtribOrden As Boolean
    Dim oAtribValor As CAtributoOfertado
    Dim oatrib As CAtributo
    Dim bRecepCantidad As Boolean

    sdbgMiPedido.RemoveAll
    
    Set oOrden = oPedido.OrdenesEntrega.Item(iNumero)
    
    txtNumEntrega.Text = oOrden.Numero
    txtMon.Text = oOrden.Moneda
    oMonedas.CargarTodasLasMonedas oOrden.Moneda, , True, , , , True
    If oMonedas.Count = 0 Then
'''la linea esta en Moneda Central
        dequivalencia = 1
    Else
        dequivalencia = oMonedas.Item(1).Equiv
    End If
    
    If Not oOrden.Receptor Is Nothing Then
        txtReceptor.Text = NullToStr(oOrden.Receptor.nombre) & " " & NullToStr(oOrden.Receptor.Apellidos)
    Else
        txtReceptor.Text = ""
    End If
    txtCodProveERP.Text = NullToStr(oOrden.ProveERP)
    
    
    If Not NullToStr(oOrden.FormaPagoCod) = "" Then
        If NullToStr(oOrden.FormaPagoDen) = "" Then
            oPagos.CargarTodosLosPagosDesde 1, NullToStr(oOrden.FormaPagoCod), , , True
            oOrden.FormaPagoDen = oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        End If
        txtPago.Text = oOrden.FormaPagoCod & IIf(IsNull(oOrden.FormaPagoCod), "", " - ") & oOrden.FormaPagoDen
    Else
        txtPago.Text = ""
    End If
    
    If Not NullToStr(oOrden.ViaPagoCod) = "" Then
        If NullToStr(oOrden.ViaPagoDen) = "" Then
            oViaPagos.CargarTodosLosPagosDesde 1, NullToStr(oOrden.ViaPagoCod), , , True
            oOrden.ViaPagoDen = oViaPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        End If
        txtViaPago.Text = oOrden.ViaPagoCod & IIf(IsNull(oOrden.ViaPagoCod), "", " - ") & oOrden.ViaPagoDen
    Else
        txtViaPago.Text = ""
    End If
    
    txtCodPedERP.Text = NullToStr(oOrden.NumPedidoERP)
    txtCodPedERPFact.Text = NullToStr(oOrden.CodProveERPFactura)
    txtObs.Text = NullToStr(oPedido.OrdenesEntrega.Item(sstabPedido.Tab + 1).Observaciones)
    'Atributos de la orden
    If oOrden.ATRIBUTOS Is Nothing Then
        bAtribOrden = False
    ElseIf oOrden.ATRIBUTOS.Count = 0 Then
        bAtribOrden = False
    Else
        bAtribOrden = True
        sdbgAtributos.Visible = True
    End If
    If bAtribOrden = False Then
        lblMasDatos.Visible = False
        sdbgAtributos.Visible = False
        'ResizearSplitt
    Else
        sdbgAtributos.RemoveAll
        For Each oAtribValor In oOrden.ATRIBUTOS
            If oAtribValor.objeto.pedido Then
                sdbgAtributos.AddItem oAtribValor.objeto.Cod & Chr(m_lSeparador) & oAtribValor.objeto.Den & Chr(m_lSeparador) & MostrarAtributo(oAtribValor, oAtribValor.objeto.Tipo, m_sIdiFalse, m_sIdiTrue) & Chr(m_lSeparador) & oAtribValor.idAtribProce & Chr(m_lSeparador) & oAtribValor.objeto.interno
            End If
        Next
    End If
    dImporteTotal = 0
    
    LimpiarGrid
    ConfigurarGrid
    
    bRecepCantidad = False
    'Miro si todas las lineas del pedido son por importe para ocultar varias columnas
    For Each oLineaPedido In oOrden.LineasPedido
        If oLineaPedido.tipoRecepcion = 0 Then
            bRecepCantidad = True
        End If
    Next
    If bRecepCantidad = False Then
        sdbgMiPedido.Columns("Cantidad").Visible = False
        sdbgMiPedido.Columns("P.U.").Visible = False
    End If
    
    For Each oLineaPedido In oOrden.LineasPedido
        sLinea = oLineaPedido.ArtDen & Chr(m_lSeparador) & oLineaPedido.CodDestino & Chr(m_lSeparador) & oLineaPedido.UnidadPedido & Chr(m_lSeparador)
        sLinea = sLinea & oLineaPedido.CantidadPedida & Chr(m_lSeparador) & oLineaPedido.PrecioUP * dequivalencia & Chr(m_lSeparador)
        
        If oLineaPedido.tipoRecepcion = 1 Then
            sLinea = sLinea & oLineaPedido.importePedido * dequivalencia & Chr(m_lSeparador)
        Else
            sLinea = sLinea & oLineaPedido.CantidadPedida * oLineaPedido.PrecioUP * dequivalencia & Chr(m_lSeparador)
        End If
        sLinea = sLinea & oLineaPedido.FechaEntrega & Chr(m_lSeparador) & oLineaPedido.Obligat & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oLineaPedido.Observaciones)
        
        If gParametrosGenerales.gbUsarPres1 And gParametrosGenerales.gbUsarPedPres1 Then
            sLinea = sLinea & Chr(m_lSeparador) & StringDePresupuestos(oLineaPedido.Presupuestos1, 1)
        Else
            sLinea = sLinea & Chr(m_lSeparador) & ""
        End If
        If gParametrosGenerales.gbUsarPres2 And gParametrosGenerales.gbUsarPedPres2 Then
            sLinea = sLinea & Chr(m_lSeparador) & StringDePresupuestos(oLineaPedido.Presupuestos2, 2)
        Else
            sLinea = sLinea & Chr(m_lSeparador) & ""
        End If
        If gParametrosGenerales.gbUsarPres3 And gParametrosGenerales.gbUsarPedPres3 Then
            sLinea = sLinea & Chr(m_lSeparador) & StringDePresupuestos(oLineaPedido.Presupuestos3, 3)
        Else
            sLinea = sLinea & Chr(m_lSeparador) & ""
        End If
        If gParametrosGenerales.gbUsarPres4 And gParametrosGenerales.gbUsarPedPres4 Then
            sLinea = sLinea & Chr(m_lSeparador) & StringDePresupuestos(oLineaPedido.Presupuestos4, 4)
        Else
            sLinea = sLinea & Chr(m_lSeparador) & ""
        End If
        sLinea = sLinea & Chr(m_lSeparador) & oLineaPedido.PedidoAbierto & Chr(m_lSeparador) & oLineaPedido.tipoRecepcion
        If Not oOrden.AtributosLineas Is Nothing Then
            For Each oatrib In oOrden.AtributosLineas
                If oatrib.pedido Then
                    If Not oLineaPedido.ATRIBUTOS.Item(CStr(oatrib.Id)) Is Nothing Then
                        sLinea = sLinea & Chr(m_lSeparador) & MostrarAtributo(oLineaPedido.ATRIBUTOS.Item(CStr(oatrib.Id)), oatrib.Tipo, m_sIdiFalse, m_sIdiTrue)
                    Else
                        sLinea = sLinea & Chr(m_lSeparador) & ""
                    End If
                End If
            Next
        End If
                
        DoEvents
        sdbgMiPedido.AddItem sLinea
        dImporteTotal = dImporteTotal + (oLineaPedido.CantidadPedida * (oLineaPedido.PrecioUP * dequivalencia))
    Next
            
    txtImporteTotal.Text = Format(oOrden.ImporteTotal * dequivalencia, "standard")
        
    sdbgMiPedido.Update

End Sub
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_APRO_EMISION, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        'Ador.MoveNext
        frmPedidosEmision2.caption = Ador(0).Value
        Ador.MoveNext
        Label4.caption = Ador(0).Value
        Ador.MoveNext
        Label3.caption = Ador(0).Value
        Ador.MoveNext
        'cmdImprimir.Caption = Ador(0).Value
        Ador.MoveNext
        label2.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        lblMon.caption = Ador(0).Value
        Ador.MoveNext
        m_sdbgItemPorProve(0) = Ador(0).Value
        Ador.MoveNext
        m_sdbgItemPorProve(1) = Ador(0).Value
        Ador.MoveNext
        m_sdbgItemPorProve(2) = Ador(0).Value
        Ador.MoveNext
        m_sdbgItemPorProve(3) = Ador(0).Value
        Ador.MoveNext
        m_sdbgItemPorProve(4) = Ador(0).Value
        Ador.MoveNext
        m_sdbgItemPorProve(6) = Ador(0).Value
        Ador.MoveNext
        m_sdbgItemPorProve(7) = Ador(0).Value
        Ador.MoveNext
        m_sdbgItemPorProve(8) = Ador(0).Value
        Ador.MoveNext
        splantilla = Ador(0).Value
        Ador.MoveNext
        lblReceptor.caption = Ador(0).Value
        Ador.MoveNext
        lblCodProveERP.caption = Ador(0).Value
        Ador.MoveNext
        lblEmpresa.caption = Ador(0).Value
        Ador.MoveNext
        sIdiMonCent = Ador(0).Value
        Ador.MoveNext
        lblObs.caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        lblPago.caption = Ador(0).Value
        Ador.MoveNext
        lblMasDatos.caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("VALOR").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value
        Ador.MoveNext
        lblViaPago.caption = Ador(0).Value
        Ador.MoveNext
        m_sdbgItemPorProve(5) = Ador(0).Value  'Importe
        Ador.MoveNext
        lblCodPedERPFact.caption = Ador(0).Value
        Ador.Close
    End If
    
    If gParametrosGenerales.gbCodPersonalizDirecto = False Then
        lblCodPedERP.Visible = False
        txtCodPedERP.Visible = False
        lblCodPedERPFact.Visible = False
        txtCodPedERPFact.Visible = False
    Else
        lblCodPedERP.Visible = True
        txtCodPedERP.Visible = True
        lblCodPedERPFact.Visible = True
        txtCodPedERPFact.Visible = True
        lblCodPedERP.caption = gParametrosGenerales.gsNomCodPersonalizPedido
    End If
End Sub

Private Sub ConfigurarSeguridad()

If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
     If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRRestPresup1UO)) Is Nothing) Then
        m_bRPresup1UO = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRRestPresup1UORama)) Is Nothing) Then
        m_bRPresup1UORama = True
    End If

     If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRRestPresup2UO)) Is Nothing) Then
        m_bRPresup2UO = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRRestPresup2UORama)) Is Nothing) Then
        m_bRPresup2UORama = True
    End If

     If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRRestPresup3UO)) Is Nothing) Then
        m_bRPresup3UO = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRRestPresup3UORama)) Is Nothing) Then
        m_bRPresup3UORama = True
    End If

    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRRestPresup4UO)) Is Nothing) Then
        m_bRPresup4UO = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRRestPresup4UORama)) Is Nothing) Then
        m_bRPresup4UORama = True
    End If
   
    
Else
    m_bRPresup1UO = False
    m_bRPresup1UORama = False
    m_bRPresup2UO = False
    m_bRPresup2UORama = False
    m_bRPresup3UO = False
    m_bRPresup3UORama = False
    m_bRPresup4UO = False
    m_bRPresup4UORama = False
End If

End Sub

Private Sub LimpiarGrid()

sdbgMiPedido.RemoveAll
If m_bCargarLayout Then
    sdbgMiPedido.LoadLayout m_sLayOut
    sdbgMiPedido.FieldSeparator = Chr(m_lSeparador)
    m_bCargarLayout = False
       
End If

End Sub

Private Sub ConfigurarGrid()
Dim oAtribs As CAtributos
Dim oatrib  As CAtributo
Dim i As Integer
Dim oColumn As SSDataWidgets_B.Column
Dim m_iTotalCols As Integer

    m_iTotalCols = 12

    Set oAtribs = oOrden.AtributosLineas
    
    i = sdbgMiPedido.Columns.Count
    
    If Not oAtribs Is Nothing Then
        For Each oatrib In oAtribs
            If oatrib.pedido Then
                DoEvents
                sdbgMiPedido.Columns.Add i
                DoEvents
                m_iTotalCols = m_iTotalCols + 1
                Set oColumn = sdbgMiPedido.Columns(i)
                oColumn.Name = "AT_" & CStr(oatrib.Id)
                oColumn.CaptionAlignment = ssColCapAlignCenter
                Select Case oatrib.Tipo
                Case TiposDeAtributos.TipoString
                    oColumn.Alignment = ssCaptionAlignmentLeft
                    oColumn.FieldLen = cLongAtribEsp_Texto
                    
                Case TiposDeAtributos.TipoNumerico
                    oColumn.Alignment = ssCaptionAlignmentRight
                    oColumn.NumberFormat = FormateoNumericoComp(2)
                    
                Case TiposDeAtributos.TipoFecha
                    oColumn.Alignment = ssCaptionAlignmentRight
                    
                Case TiposDeAtributos.TipoBoolean
                    oColumn.Alignment = ssCaptionAlignmentLeft
                    
                End Select
                oColumn.caption = oatrib.Cod
                If oatrib.interno Then
                    oColumn.HeadStyleSet = "NormalInterno"
                End If
    
                oColumn.StyleSet = "Normal"
                oColumn.Visible = True
                oColumn.Locked = True
                Set oColumn = Nothing
                i = i + 1
            
            End If
        Next
    End If
    
    m_bCargarLayout = True
    
    sdbgMiPedido.AllowUpdate = True
    
    'RedimensionarGridItems
    

End Sub


