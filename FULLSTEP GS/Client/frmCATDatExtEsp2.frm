VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCATDatExtEsp2 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DCargando archivos"
   ClientHeight    =   1680
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5190
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATDatExtEsp2.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1680
   ScaleWidth      =   5190
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   435
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   4955
      _ExtentX        =   8731
      _ExtentY        =   767
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Label lblNumEspec 
      BackColor       =   &H00808000&
      Caption         =   "Archivo 1 de 20"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1380
      Width           =   4995
   End
   Begin VB.Label lblEspec 
      BackColor       =   &H00808000&
      Caption         =   "Imagen: ART1.jpg"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   180
      Width           =   3915
   End
End
Attribute VB_Name = "frmCATDatExtEsp2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oProve As CProveedor
Public g_sPath As String
Public g_bSoloRuta As Boolean
Public g_sCaracter As String

Private m_sIdiArchivo As String
Private m_sDe As String

Private Sub Form_Activate()
Dim oFos As FileSystemObject
Dim oFolder As Scripting.Folder
Dim oFiles As Scripting.Files
Dim oFile As Scripting.File
Dim oArticulos As CArticulos
Dim oArt As CArticulo
Dim oArts As CArticulos
Dim sArticulo As String
Dim arNoExisten() As String
Dim i As Integer
Dim j As Integer
Dim iTotFiles As Integer
Dim teserror As TipoErrorSummit
Dim sCod As String
Dim sExtension As String
Dim dblPorcen As Double
Dim vbook As Variant
Dim vBook1 As Variant
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim oIBaseDatos As IBaseDatos
Dim oAdjun As CAdjunto
    Set oFos = New FileSystemObject
    Set oFolder = oFos.GetFolder(g_sPath)
    Set oFiles = oFolder.Files
    Set oArticulos = oFSGSRaiz.Generar_CArticulos

    Screen.MousePointer = vbHourglass
    On Error GoTo Error
    
    i = 0
    j = 0
    
    iTotFiles = oFiles.Count
    dblPorcen = (100 \ iTotFiles)
    m_sIdiArchivo = Replace(m_sIdiArchivo, "Y", CStr(iTotFiles))
    
    For Each oFile In oFiles
        If ProgressBar1.Value = 100 Then ProgressBar1.Value = 0
        ProgressBar1.Value = ProgressBar1.Value + dblPorcen
        
        lblEspec.caption = m_sIdiArchivo & ": " & oFile.Name
        j = j + 1
        lblNumEspec.caption = m_sIdiArchivo & " " & CStr(j) & " " & m_sDe & " " & iTotFiles
        
        sExtension = Right(oFile.Name, Len(oFile.Name) - InStr(1, oFile.Name, "."))
        If InStr(1, oFile.Name, g_sCaracter) <> 0 Then
            sArticulo = Left(oFile.Name, InStr(1, oFile.Name, g_sCaracter) - 1)
        Else
            sArticulo = ""
        End If
        
        Set oArts = oArticulos.CargarArticuloEnProveedor(g_oProve.Cod, sArticulo)
        
        If oArts Is Nothing Then
            'Si no existe el art�culo en el prove lo meto a un array
            ReDim Preserve arNoExisten(i + 1)
            arNoExisten(i + 1) = oFile.Name
            i = i + 1
            
        Else 'Si existe el art�culo en el proveedor
            'Lo almaceno en BD
            For Each oArt In oArts
                sCod = oArt.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oArt.GMN1Cod))
                sCod = sCod & oArt.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oArt.GMN2Cod))
                sCod = sCod & oArt.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oArt.GMN3Cod))
                sCod = sCod & oArt.GMN4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oArt.GMN4Cod))
                sCod = sCod & oArt.Cod
                
                If Not oArticulos.Item(oArt.Cod) Is Nothing Then
                
                    Set oEsp = oFSGSRaiz.generar_CEspecificacion
                    'Si guarda la ruta se almacena el nombre del fichero tal cual est�.Sino,se le
                    'quita el cod.del art�culo
                    If g_bSoloRuta = True Then
                        oEsp.nombre = oFile.Name
                    Else
                        oEsp.nombre = Mid(oFile.Name, InStr(1, oFile.Name, g_sCaracter) + 1)
                    End If
                    oEsp.Comentario = Null
                    Set oEsp.Proveedor = g_oProve
                    Set oEsp.Articulo = oArt
                    
                    If g_bSoloRuta = True Then
                        oEsp.Ruta = g_sPath & "\"
                        Set oIBaseDatos = oEsp
                        teserror = oIBaseDatos.AnyadirABaseDatos
                        Set oIBaseDatos = Nothing
                    Else
                        sFileName = oFile.Path
                        'sFileTitle = oFile.Name
                        'Grabamos en PROVE_ART4
                        g_oProve.GenerarPROVE_ART4 oArt.Cod
                        Set oAdjun = oFSGSRaiz.Generar_CAdjunto
                        oAdjun.nombre = oFile.Name
                        oAdjun.Tipo = TipoAdjunto.ProveedorArticulo
                        Call oAdjun.GrabarAdjunto(sFileName, "", g_oProve.Cod & "#" & oArt.Cod)
                    End If
                End If
            Next
        End If
    Next
    
    Screen.MousePointer = vbNormal
    If i > 0 Then
        oMensajes.ImposibleCargarEspecificaciones arNoExisten
    End If
    
    'Refresca la grid del cat�logo
    If frmCatalogoDatosExt.sdbgArt.Rows > 0 And Not frmCatalogoDatosExt.m_oArticulos Is Nothing Then
        vBook1 = frmCatalogoDatosExt.sdbgArt.Bookmark
        For Each oArt In oArticulos
            sCod = oArt.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oArt.GMN1Cod))
            sCod = sCod & oArt.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oArt.GMN2Cod))
            sCod = sCod & oArt.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oArt.GMN3Cod))
            sCod = sCod & oArt.GMN4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oArt.GMN4Cod))
            sCod = sCod & oArt.Cod
            
            If Not frmCatalogoDatosExt.m_oArticulos.Item(sCod) Is Nothing Then
                frmCatalogoDatosExt.m_oArticulos.Item(sCod).ConEspecific = True
                vbook = frmCatalogoDatosExt.sdbgArt.AddItemBookmark(frmCatalogoDatosExt.m_oArticulos.Item(sCod).indice)
                frmCatalogoDatosExt.sdbgArt.Bookmark = vbook
                frmCatalogoDatosExt.sdbgArt.Columns("CONESP").Value = "1"
                frmCatalogoDatosExt.sdbgArt.Update
            End If
        Next
        frmCatalogoDatosExt.sdbgArt.Bookmark = vBook1
    End If
    
    Set oArticulos = Nothing
    Set oArts = Nothing
    
    Unload Me
    Exit Sub
    
Error:
    MsgBox err.Description, vbCritical + vbOKOnly, "FULLSTEP"
    Screen.MousePointer = vbNormal
    Unload Me
End Sub

Private Sub Form_Load()

    Me.Height = 2055
    Me.Width = 5280
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos

End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAT_DATOS_EXT_ESP1, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        m_sIdiArchivo = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.MoveNext
        m_sDe = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub




