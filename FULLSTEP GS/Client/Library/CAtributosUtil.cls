VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAtributosUtil"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' <summary>Comprueba si hay que aplicar atributos de grupo</summary>
''' <param name="oAtribsFormulas">Colecci�n de atributos</param>
''' <param name="lItem">Id Item</param>
''' <param name="sCodGrupo">Codigo del grupo</param>
''' <param name="sProv">Codigo del proveedor</param>
''' <remarks>Llamada desde: ctlAdjEscalado.Recalcular; Tiempo m�ximo:0,1</remarks>

Public Function ComprobarAplicarAtribsItem(ByVal oAtribsFormulas As CAtributos, ByVal lItem As Long, ByVal sCodGrupo As String, Optional ByVal sProv As String) As Boolean
    Dim oProv As CProveedor
    Dim bAdj As Boolean
    Dim sCod As String
    Dim sProvAnt As String
    Dim oAdjs As CAdjudicaciones
    Dim oAdj As CAdjudicacion
    Dim bHayAtribs  As Boolean
    Dim oatrib As CAtributo
    
    If oAtribsFormulas Is Nothing Then
        ComprobarAplicarAtribsItem = False
        Exit Function
    End If
    
    If oAtribsFormulas.Count = 0 Then
        ComprobarAplicarAtribsItem = False
        Exit Function
    End If
    
    bHayAtribs = False
    For Each oatrib In oAtribsFormulas
        If oatrib.PrecioAplicarA = TipoAplicarAPrecio.TotalItem Then
            If oatrib.codgrupo = sCodGrupo Or IsNull(oatrib.codgrupo) Then
                ComprobarAplicarAtribsItem = True
                bHayAtribs = True
                Exit For
            End If
        End If
    Next
    If bHayAtribs = False Then
        ComprobarAplicarAtribsItem = False
        Exit Function
    End If
    
End Function

''' <summary>Aplica atributos a los precios</summary>
''' <param name="oProcesoSeleccionado">Objeto proceso</param>
''' <param name="oAtribsFormulas">Colecci�n de atributos</param>
''' <param name="dPrecioAplicFormula"></param>
''' <param name="udtTipoAplicar"></param>
''' <param name="sProve">C�digo proveedor</param>
''' <param name="dblImporteAux"></param>
''' <param name="sGrupo">C�digo grupo</param>
''' <param name="lItem">C�digo item</param>
''' <param name="lIdEscalado">Id de escalado</param>
''' <remarks>Llamada desde: AdjudicarEscalado, DesadjudicarEscalado</remarks>
''' <revision>LTG 13/10/2011</revision>

Public Function AplicarAtributos(ByVal oProcesoSeleccionado As cProceso, ByVal oAtribsFormulas As CAtributos, ByVal dPrecioAplicFormula As Variant, _
        ByVal udtTipoAplicar As TipoAplicarAPrecio, ByVal sProve As String, ByRef dblImporteAux As Double, ByVal iLongCodGRUPOPROCE As Integer, Optional ByVal sGrupo As String, _
        Optional ByVal lItem As Variant, Optional ByVal lIdEscalado As Variant, Optional ByVal vCambio As Variant) As Variant
    Dim iOrden As Integer
    Dim bSiguiente As Boolean
    Dim lngMinimo As Long
    Dim lAtribAnterior As Long
    Dim scod1 As String
    Dim oatrib As CAtributo
    Dim oAtribOfertados As CAtributosOfertados
    Dim oOfertaGr As COferta
    Dim dValorAtrib  As Double
    Dim bAplicar As Boolean
    Dim dblCambio As Double
    
    If oAtribsFormulas Is Nothing Then
        AplicarAtributos = dPrecioAplicFormula
        Exit Function
    End If
    
    If Not IsNull(dPrecioAplicFormula) And oAtribsFormulas.Count > 0 Then
        If Not IsMissing(vCambio) Then
            dblCambio = vCambio
        Else
            dblCambio = 1
        End If
        
        '**** APLICA LOS ATRIBUTOS A APLICAR SEGUN EL PARAMETRO udtTipoAplicar ****
        lAtribAnterior = 0
        iOrden = 1
        bSiguiente = False
        While Not bSiguiente
            lngMinimo = 0
            For Each oatrib In oAtribsFormulas
                bAplicar = False
                'si el atributo aplica la f�rmula al total del item
                If oatrib.PrecioAplicarA = udtTipoAplicar Then
                      If udtTipoAplicar = TipoAplicarAPrecio.TotalGrupo Then
                          If Not oatrib.AtribTotalGrupo.Item(sGrupo) Is Nothing Then
                              If oatrib.AtribTotalGrupo.Item(sGrupo).UsarPrec = 1 Then
                                  If oatrib.AtribTotalGrupo.Item(sGrupo).FlagAplicar Then
                                      bAplicar = True
                                  End If
                              End If
                          End If
                      Else
                          If oatrib.UsarPrec = 1 And oatrib.FlagAplicar = True Then
                             bAplicar = True
                          End If
                      End If
                End If
                If bAplicar Then
                    If oatrib.Orden = iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                        lngMinimo = oatrib.idAtribProce
                        Exit For
                    Else
                        If oatrib.Orden > iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                            If lngMinimo = 0 Then
                                lngMinimo = oatrib.idAtribProce
                            ElseIf (oatrib.Orden < oAtribsFormulas.Item(CStr(lngMinimo)).Orden) Then
                                lngMinimo = oatrib.idAtribProce
        '                      ElseIf udtTipoAplicar = TotalOferta Then
        '                        If oAtrib.idAtribProce = lngMinimo Then
        '                          bSiguiente = True
        '                          Exit For
        '                        End If
                            End If
                        End If
                    End If
                End If
            Next
            
            If (lngMinimo = 0) Then
                bSiguiente = True
            ElseIf (oAtribsFormulas.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then 'Esto no estaba en totalproceso
                bSiguiente = True
            Else
                dValorAtrib = 0
                Set oatrib = oAtribsFormulas.Item(CStr(lngMinimo))
                lAtribAnterior = oatrib.idAtribProce
                iOrden = oatrib.Orden
                
                'si el atributo aplica la f�rmula al precio unitario del item
                Select Case oatrib.ambito
                    Case 1  'Proceso
                        If Not oProcesoSeleccionado.Ofertas Is Nothing Then
                            Set oAtribOfertados = oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados
                            'Comprueba los atributos a aplicar al precio unitario de �mbito proceso
                            If Not oAtribOfertados Is Nothing Then
                                If Not oAtribOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                    dValorAtrib = NullToDbl0Interno(oAtribOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                                End If
                            End If
                            Set oAtribOfertados = Nothing
                        End If
                            
                    Case 2 'Grupo
                        'Comprueba los atributos a aplicar al precio unitario de �mbito grupo
                        If sGrupo <> "" Then
                            If Not oProcesoSeleccionado.Ofertas Is Nothing Then
                                Set oAtribOfertados = oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados
                                If Not oAtribOfertados Is Nothing Then
                                    If sGrupo = oatrib.codgrupo Or IsNull(oatrib.codgrupo) Then
                                        scod1 = sGrupo & Mid$("                         ", 1, iLongCodGRUPOPROCE - Len(sGrupo))
                                        scod1 = scod1 & CStr(oatrib.idAtribProce)
                                        If Not oAtribOfertados.Item(scod1) Is Nothing Then
                                            dValorAtrib = NullToDbl0Interno(oAtribOfertados.Item(scod1).valorNum)
                                        End If
                                    End If
                                End If
                                Set oAtribOfertados = Nothing
                            End If
                        End If
                    Case 3  'Item
                        If Not IsMissing(lItem) Then
                            Set oAtribOfertados = oProcesoSeleccionado.Grupos.Item(sGrupo).UltimasOfertas.Item(Trim(sProve)).AtribItemOfertados
                            If Not oAtribOfertados Is Nothing Then
                                If Not oAtribOfertados.Item(CStr(lItem) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                                    If Not IsMissing(lIdEscalado) Then
                                        If Not oAtribOfertados.Item(CStr(lItem) & "$" & CStr(oatrib.idAtribProce)).Escalados Is Nothing Then
                                            If Not oAtribOfertados.Item(CStr(lItem) & "$" & CStr(oatrib.idAtribProce)).Escalados.Item(CStr(lIdEscalado)) Is Nothing Then
                                                dValorAtrib = NullToDbl0Interno(oAtribOfertados.Item(CStr(lItem) & "$" & CStr(oatrib.idAtribProce)).Escalados.Item(CStr(lIdEscalado)).valorNum)
                                            End If
                                        End If
                                    Else
                                        dValorAtrib = NullToDbl0Interno(oAtribOfertados.Item(CStr(lItem) & "$" & CStr(oatrib.idAtribProce)).valorNum)
                                    End If
                                End If
                            End If
                            Set oAtribOfertados = Nothing
                        End If
                End Select
                
                If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                      Select Case oatrib.PrecioFormula
                          Case "+"
                              dPrecioAplicFormula = dPrecioAplicFormula + (dValorAtrib / dblCambio)
                              dblImporteAux = dblImporteAux + (dValorAtrib / dblCambio)
                          Case "-"
                              dPrecioAplicFormula = dPrecioAplicFormula - (dValorAtrib / dblCambio)
                              dblImporteAux = dblImporteAux - (dValorAtrib / dblCambio)
                          Case "/"
                              dPrecioAplicFormula = dPrecioAplicFormula / dValorAtrib
                              dblImporteAux = dblImporteAux / dValorAtrib
                          Case "*"
                              dPrecioAplicFormula = dPrecioAplicFormula * dValorAtrib
                              dblImporteAux = dblImporteAux * dValorAtrib
                          Case "+%"
                              dPrecioAplicFormula = dPrecioAplicFormula + (dPrecioAplicFormula * (dValorAtrib / 100))
                              dblImporteAux = dblImporteAux + (dblImporteAux * (dValorAtrib / 100))
                          Case "-%"
                              dPrecioAplicFormula = dPrecioAplicFormula - (dPrecioAplicFormula * (dValorAtrib / 100))
                              dblImporteAux = dblImporteAux - (dblImporteAux * (dValorAtrib / 100))
                      End Select
                End If
                    
                Set oatrib = Nothing
                bSiguiente = False
                iOrden = iOrden + 1
        
            End If
        Wend
    End If
    
    AplicarAtributos = dPrecioAplicFormula
End Function

''' <summary>Aplica un atributo</summary>
''' <param name="oAtribOfertados">Colecci�n de atributos ofertados</param>
''' <param name="oAtribsFormulas">Colecci�n de atributos</param>
''' <param name="dPrecioAplicFormula"></param>
''' <remarks>Llamada desde: CProceso.AplicamosAtributo</remarks>
''' <revision>LTG 04/05/2012</revision>

Public Function AplicarAtributo(ByVal sOp As String, ByVal dblImporte As Double, ByVal dblValorNum As Double) As Double
    'Aplicamos el atributo al precio unitario.
    Select Case sOp
        Case "+"
            dblImporte = dblImporte + dblValorNum
            
        Case "-"
            dblImporte = dblImporte - dblValorNum
            
        Case "/"
            dblImporte = CDec(dblImporte / dblValorNum)
            
        Case "*"
            dblImporte = CDec(dblImporte * dblValorNum)
            
        Case "+%"
            dblImporte = dblImporte + CDec(dblImporte * CDec(dblValorNum / 100))
            
        Case "-%"
            dblImporte = dblImporte - CDec(dblImporte * CDec(dblValorNum / 100))
    End Select
    
    AplicarAtributo = dblImporte
End Function

''' <summary>Comprueba que se han rellenado los datos necesarios de los costes/descuentos. Si no los borra</summary>
''' <remarks>Llamada desde: Form_Unload</remarks>
''' <revision>LTG 14/05/2012</revision>

Public Sub ComprobarCDs(ByRef oCostesDescuentos As CAtributos)
    Dim oCosteDesc As CAtributo
    Dim sCod As String
    
    If Not oCostesDescuentos Is Nothing Then
        If oCostesDescuentos.Count > 0 Then
            For Each oCosteDesc In oCostesDescuentos
                If NullToStrInterno(oCosteDesc.valorNum) = "" Or NullToStrInterno(oCosteDesc.PrecioFormula) = "" Then
                    oCostesDescuentos.Remove (CStr(oCosteDesc.Id))
                End If
            Next
            Set oCosteDesc = Nothing
        End If
    End If
End Sub

