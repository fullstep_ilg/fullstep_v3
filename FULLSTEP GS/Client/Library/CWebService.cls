VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CWebService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'''''' <summary>Proceso que llama al webService para cambiar la Password</summary>
'''''' <remarks>Llamada desde=cmdAceptar_Click; Tiempo m�ximo=0</remarks>
Public Function LlamarWebServiceCambioPWD(ByRef ParamGen As ParametrosGenerales, ByVal iAdm As Integer, ByVal iChange As Integer, ByVal sUsuActual As String, ByVal sPwdActual As String, _
                                          ByVal sUsuNuevo As String, ByVal sPwdNuevo As String, ByVal dfechahoracrypt As Date, ByRef oMensajes As CMensajes, Optional ByVal dfechaactual As Date) As Boolean
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim fecpwd As Date
    Dim XmlBody As String
    Dim strUrl As String
    Dim strSoapAction As String
    
    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
        
    If IsMissing(dfechaactual) Or dfechaactual = CDate(0) Then
        fecpwd = Now
    Else
        fecpwd = dfechaactual
    End If
    Dim sMail As String
    sMail = ""
            
    strUrl = ParamGen.gsURL_CHANGE_PWD & "/CambioPWD.asmx"
    
    strSoapAction = "http://tempuri.org/FSNWebServicePWD/CambioPWD/CambiarLogin"
           
    XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
        "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
        "<soap:Body>" & _
       "<CambiarLogin xmlns=""http://tempuri.org/FSNWebServicePWD/CambioPWD"">" & _
            "<adm>" & iAdm & "</adm>" & _
            "<usu>" & sUsuActual & "</usu>" & _
            "<PWDActual>" & URLEncodeInterno(sPwdActual, True) & "</PWDActual>" & _
            "<usuNuevo>" & sUsuNuevo & "</usuNuevo>" & _
            "<PWDNueva>" & URLEncodeInterno(sPwdNuevo, True) & "</PWDNueva>" & _
            "<FechaPWD_year>" & Year(fecpwd) & "</FechaPWD_year>" & _
            "<FechaPWD_month>" & Month(fecpwd) & "</FechaPWD_month>" & _
            "<FechaPWD_day>" & Day(fecpwd) & "</FechaPWD_day>" & _
            "<FechaPWD_hour>" & Hour(fecpwd) & "</FechaPWD_hour>" & _
            "<FechaPWD_min>" & Minute(fecpwd) & "</FechaPWD_min>" & _
            "<FechaPWD_sec>" & Second(fecpwd) & "</FechaPWD_sec>" & _
            "<iChange>" & iChange & "</iChange>" & _
            "<Mail>" & sMail & "</Mail>" & _
            "<FechaAnteriorPWd_year>" & Year(dfechahoracrypt) & "</FechaAnteriorPWd_year>" & _
            "<FechaAnteriorPWd_month>" & Month(dfechahoracrypt) & "</FechaAnteriorPWd_month>" & _
            "<FechaAnteriorPWd_day>" & Day(dfechahoracrypt) & "</FechaAnteriorPWd_day>" & _
            "<FechaAnteriorPWd_hour>" & Hour(dfechahoracrypt) & "</FechaAnteriorPWd_hour>" & _
            "<FechaAnteriorPWd_min>" & Minute(dfechahoracrypt) & "</FechaAnteriorPWd_min>" & _
            "<FechaAnteriorPWd_sec>" & Second(dfechahoracrypt) & "</FechaAnteriorPWd_sec>" & _
        "</CambiarLogin>" & _
        "</soap:Body></soap:Envelope>"
       
    ' Carga XML
    objDom.async = True
    objDom.loadXML XmlBody
    ' Abre el webservice
    objXmlHttp.Open "POST", strUrl, True
    ' Crea headings
    objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
 
    ' Envia XML command
    Dim codError As String
    objXmlHttp.Send objDom.xml
    'Esperar hasta que se termine de ejecutar
    While Not objXmlHttp.ReadyState = 4
        DoEvents
    Wend
    codError = objXmlHttp.ResponseText
    Dim Resultado As Integer
    If IsNumeric(Mid$(codError, InStr(1, codError, "<CambiarLoginResult>", vbTextCompare) + 20, 1)) Then
        Resultado = CInt(Mid$(codError, InStr(1, codError, "<CambiarLoginResult>", vbTextCompare) + 20, 1))
    Else
        Resultado = 99
    End If
    
    ' Cierra object
    Set objXmlHttp = Nothing
    
    LlamarWebServiceCambioPWD = False
    
    Select Case Resultado
      Case 1
          oMensajes.Contrase�aNoCumpleLongitud ParamGen.gbCOMPLEJIDAD_PWD, ParamGen.giMIN_SIZE_PWD
      Case 2
          oMensajes.Contrase�aNoCumpleComplejidad ParamGen.giMIN_SIZE_PWD
      Case 3
          oMensajes.Contrase�aNoSePuedecambiar
      Case 4
          oMensajes.Contrase�aNoCumpleHistorico ParamGen.giHIST_PWD
      Case 5
          oMensajes.Contrase�aNoCumpleLongitud ParamGen.gbCOMPLEJIDAD_PWD, ParamGen.giMIN_SIZE_PWD
      Case 9
          oMensajes.Contrase�aErrorInesperado
      Case 99
          oMensajes.Contrase�aErrorInesperado
      Case 0
        MsgBox oMensajes.CargarTexto(Mensajes, 57), vbOKOnly, "FULLSTEP"
        LlamarWebServiceCambioPWD = True
    End Select
End Function

''' <summary>Proceso que prepara la llamada para el webService</summary>
    ' Inicia internet explorer e ingresa el URL de tu webservice
    ' Por ejemplo: http://localhost/myweb/mywebService.asmx
    ' En esa p�gina da un click en el m�todo que deseas llamar de tu aplicaci�n.
    ' En la primera seccion de SOAP 1.1 selecciona en la primera parte (POST) el c�digo XML desde
    ' <?xml.. hasta </soap:Envelope>
    ' Copia esto a la variable strXml. Cuando haya comillas dobles como " usa otro para escapar la secuencia
    ' luego reemplaza string y int en los campos por los parametros que ingresar�n al web service.
    ' Copia el URL de tu asmx a la variable strUrl y el valor de SOAPAction a la variable strSoapAction
''' <returns>--</returns>
''' <remarks>Llamada desde=cmdAceptar_click(); Tiempo m�ximo=0</remarks>

Public Sub LlamarWebServiceQA(ByRef ParamGen As ParametrosGenerales, ByVal sUsuarioCod As String, ByVal sUsuarioPwd As String)
    Dim strSoapAction As String
    Dim strUrl As String
    Dim strXml As String
    
    strUrl = ParamGen.gsURLWebServiceQA & "/QA_puntuaciones.asmx"
    strSoapAction = "http://tempuri.org/WebServicePM/QA_Puntuaciones/CalcularPuntuaciones"
                
    strXml = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
                "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
                "<soap:Body>" & _
                "<CalcularPuntuaciones xmlns=""http://tempuri.org/WebServicePM/QA_Puntuaciones"">" & _
                    "<sUsu>string</sUsu>" & _
                    "<spwd>string</spwd>" & _
                "</CalcularPuntuaciones>" & _
                "</soap:Body>" & _
                "</soap:Envelope>"
   PostWebservice sUsuarioCod, sUsuarioPwd, strUrl, strSoapAction, strXml
End Sub

'''''' <summary>
'''''' Proceso que llama al webService
'''''' </summary>
'''''' <remarks>Llamada desde=LlamarWebService; Tiempo m�ximo=0</remarks>

Private Sub PostWebservice(ByVal sUsuarioCod As String, ByVal sUsuarioPwd As String, ByVal AsmxUrl As String, ByVal SoapActionUrl As String, ByVal XmlBody As String)
    Dim objDom As Object
    Dim objXmlHttp As Object

    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
 
    ' Carga XML
    objDom.async = True
    objDom.loadXML XmlBody
        
    ' Pase de parametros
    objDom.selectSingleNode("/soap:Envelope/soap:Body/CalcularPuntuaciones/sUsu").Text = sUsuarioCod
    objDom.selectSingleNode("/soap:Envelope/soap:Body/CalcularPuntuaciones/spwd").Text = URLEncodeInterno(sUsuarioPwd)

    ' Abre el webservice
    objXmlHttp.Open "POST", AsmxUrl, True
 
    ' Crea headings
    objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.setRequestHeader "SOAPAction", SoapActionUrl
 
    ' Envia XML command
    objXmlHttp.Send objDom.xml
  
    ' Cierra object
    Set objXmlHttp = Nothing
End Sub

'''''' <summary>Proceso que llama al webService para grabar un adjunto</summary>
'''''' <remarks>Llamada desde=cmdAceptar_Click; Tiempo m�ximo=0</remarks>

Public Function LlamarWebServiceGrabarAdjunto(ByVal sURLSvc As String, strPath As String, sProve As String, sNombre As String) As String
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim parser As Object
    Dim arrData() As Byte
    Dim XmlBody As String
    Dim strUrl As String
    Dim strSoapAction As String
    Dim strResultado As String
    Dim strBase64 As String
    
    ReadByteArrayInterno strPath, arrData
    
    strBase64 = EncodeBase64Interno(arrData)
    
    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
        
    strUrl = sURLSvc & "/FSN_Adjuntos.asmx"
    
    strSoapAction = "http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos/GrabarAdjuntoPM"
           
    XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
            "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
            "<soap:Body>" & _
            "<GrabarAdjuntoPM xmlns=""http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos"">" & _
            "<Adjunto>" & strBase64 & "</Adjunto>" & _
            "<Prove>" & sProve & "</Prove>" & _
            "<Nombre>" & sNombre & "</Nombre>" & _
            "</GrabarAdjuntoPM>" & _
            "</soap:Body>" & _
            "</soap:Envelope>"
    
     
    ' Carga XML
    objDom.async = True
    objDom.loadXML XmlBody
    ' Abre el webservice
    objXmlHttp.Open "POST", strUrl, True
    ' Crea headings
    objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
 
    ' Envia XML command
    objXmlHttp.Send objDom.xml
    'Esperar hasta que se termine de ejecutar
    While Not objXmlHttp.ReadyState = 4
        DoEvents
    Wend
       
    Set parser = CreateObject("MSXML2.DOMDocument")
    strResultado = objXmlHttp.ResponseText
    parser.loadXML strResultado
    
    If InStr(strResultado, "GrabarAdjuntoPMResult") Then
        strResultado = parser.LastChild.Text
    End If
           
    LlamarWebServiceGrabarAdjunto = strResultado
    
    ' Cierra object
    Set objXmlHttp = Nothing
End Function

'''''' <summary>
'''''' Proceso que llama al webService SessionService para obtener el session id para guardar el usuario y la contrase�a y pasarlo en la URL
'''''' </summary>
'''''' <remarks>Llamada desde=MostrarPaginaWeb; Tiempo m�ximo=0</remarks>
Public Function LlamadaWebServiceSessionID(ByVal sURLSvc As String) As String
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim parser As Object
    Dim XmlBody As String
    Dim strUrl As String
    Dim strSoapAction As String
    Dim strResultado As String
    Dim strSessionId As String
    
    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
        
    strUrl = sURLSvc & "/SessionService.asmx"
    
    strSoapAction = "http://tempuri.org/GetNewSessionID"
           
    XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
            "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
            "<soap:Body>" & _
            "<GetNewSessionID xmlns=""http://tempuri.org/"">" & _
            "<sSessionID></sSessionID>" & _
            "</GetNewSessionID>" & _
            "</soap:Body>" & _
            "</soap:Envelope>"
       
    ' Carga XML
    objDom.async = True
    objDom.loadXML XmlBody
    ' Abre el webservice
    objXmlHttp.Open "POST", strUrl, True
    ' Crea headings
    objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
 
    ' Envia XML command
    objXmlHttp.Send objDom.xml
    'Esperar hasta que se termine de ejecutar
    While Not objXmlHttp.ReadyState = 4
        DoEvents
    Wend
       
    Set parser = CreateObject("MSXML2.DOMDocument")
    strResultado = objXmlHttp.ResponseText
    parser.loadXML strResultado
    
    If InStr(strResultado, "GetNewSessionIDResult") Then
        'Si existe el nodo GetNewSessionIDResult significa que se proces� adecuadamente
        strSessionId = parser.selectSingleNode("/soap:Envelope/soap:Body/GetNewSessionIDResponse/GetNewSessionIDResult").Text
    End If
           
    LlamadaWebServiceSessionID = strSessionId
    
    ' Cierra object
    Set objXmlHttp = Nothing
End Function

'''''' <summary>
'''''' Proceso que llama al webService SessionService para guardar el usuario y la contrase�a
'''''' </summary>
'''''' <remarks>Llamada desde=MostrarPaginaWeb; Tiempo m�ximo=0</remarks>
Public Sub LlamadaWebServiceSessionData(ByVal strSessionId As String, ByVal strKey As String, ByVal strValue As String, ByVal sURLSvc As String)
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim XmlBody As String
    Dim strUrl As String
    Dim strSoapAction As String
        
    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
        
    strUrl = sURLSvc & "/SessionService.asmx"
        
    strSoapAction = "http://tempuri.org/SetSessionString"
           
    XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
            "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
            "<soap:Body>" & _
            "<SetSessionString xmlns=""http://tempuri.org/"">" & _
            "<sID>" & strSessionId & "</sID>" & _
            "<sKey>" & strKey & "</sKey>" & _
            "<sValue>" & strValue & "</sValue>" & _
            "</SetSessionString>" & _
            "</soap:Body>" & _
            "</soap:Envelope>"
           
    ' Carga XML
    objDom.async = True
    objDom.loadXML XmlBody
    ' Abre el webservice
    objXmlHttp.Open "POST", strUrl, True
    ' Crea headings
    objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
 
    ' Envia XML command
    objXmlHttp.Send objDom.xml
    'Esperar hasta que se termine de ejecutar
    While Not objXmlHttp.ReadyState = 4
        DoEvents
    Wend
       
    ' Cierra object
    Set objXmlHttp = Nothing
End Sub

''' <summary>Proceso que llama al servico WCF de integraci�n para conprobar el estado de los hitos facturados</summary>
''' <param name="IdOrden">Id Orden</param>
''' <param name="arLineas">Array con Ids de las l�neas</param>
''' <param name="arHitos">Array con Ids de Hitos</param>
''' <remarks>Llamada desde=ValidarLogin</remarks>
Public Function LlamadaWebServiceComprobarHitosFacturados(ByVal sUrlServicio As String, ByRef iError As Integer, ByRef sError As String, ByVal sCodERP As String, ByVal IdOrden As Long, Optional ByVal arLineas As Variant, _
        Optional ByVal arHitos As Variant) As Variant
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim parser As Object
    Dim XmlBody As String
    Dim strSoapAction As String
    Dim strResultado As String
    Dim sResult As String
    Dim sURL As String
    Dim i As Integer
    Dim arResult() As Boolean
    
    If sUrlServicio <> "" Then
        sResult = "False"

        'Obtengo la URL del servicio, la URL pasada puede contener otro servicio
        Dim lPos As Long
        lPos = InStr(1, sUrlServicio, "/")
        While lPos <> 0
            sURL = Left(sUrlServicio, lPos)
            lPos = InStr(lPos + 1, sUrlServicio, "/")
        Wend
        sURL = sURL & "Validaciones.svc"
        
        ' Crea objetos DOMDocument y XMLHTTP
        Set objDom = CreateObject("MSXML2.DOMDocument")
        Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
        
        strSoapAction = "http://tempuri.org/IValidaciones/ValidacionesHitosFacturacion"
        
        XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
                "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
                "<soap:Body>" & _
                "<ValidacionesHitosFacturacion xmlns=""http://tempuri.org/"">" & _
                "<iNumError>0</iNumError>" & _
                "<sError></sError>" & _
                "<arrHitosFacturacion>"
                If Not IsMissing(arHitos) Then
                    If Not IsEmpty(arHitos) Then
                        For i = 0 To UBound(arHitos)
                            XmlBody = XmlBody & "<int xmlns=""http://schemas.microsoft.com/2003/10/Serialization/Arrays"">" & arHitos(i) & "</int>"
                        Next
                    End If
                End If
        XmlBody = XmlBody & "</arrHitosFacturacion>" & _
                "<arrLineasPedido>"
                If Not IsMissing(arLineas) Then
                    If Not IsEmpty(arLineas) Then
                        For i = 0 To UBound(arLineas)
                            XmlBody = XmlBody & "<int xmlns=""http://schemas.microsoft.com/2003/10/Serialization/Arrays"">" & arLineas(i) & "</int>"
                        Next
                    End If
                End If
        XmlBody = XmlBody & "</arrLineasPedido>" & _
                "<lOrdenEntrega>" & IdOrden & "</lOrdenEntrega>" & _
                "<sErp>" & sCodERP & "</sErp>" & _
                "</ValidacionesHitosFacturacion>" & _
                "</soap:Body>" & _
                "</soap:Envelope>"
        
        ' Carga XML
        objDom.async = True
        objDom.loadXML XmlBody
        ' Abre el webservice
        objXmlHttp.Open "POST", sURL, True
        ' Crea headings
        objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
        objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
        
        ' Envia XML command
        objXmlHttp.Send objDom.xml
        'Esperar hasta que se termine de ejecutar
        While Not objXmlHttp.ReadyState = 4
            DoEvents
        Wend

        If objXmlHttp.Status = 200 Then
            'Status = 200 -> OK
            Set parser = CreateObject("MSXML2.DOMDocument")
            strResultado = objXmlHttp.ResponseText
            parser.loadXML strResultado
            
            If InStr(strResultado, "ValidacionesHitosFacturacionResponse") Then
                iError = CLng(parser.selectSingleNode("/s:Envelope/s:Body/ValidacionesHitosFacturacionResponse/iNumError").Text)
                sError = parser.selectSingleNode("/s:Envelope/s:Body/ValidacionesHitosFacturacionResponse/sError").Text
                'Si existe el nodo GetNewSessionIDResult significa que se proces� adecuadamente
                sResult = parser.selectSingleNode("/s:Envelope/s:Body/ValidacionesHitosFacturacionResponse/ValidacionesHitosFacturacionResult").Text
                If sResult <> "" Then
                    For i = 0 To parser.selectSingleNode("/s:Envelope/s:Body/ValidacionesHitosFacturacionResponse/ValidacionesHitosFacturacionResult").childnodes.Length - 1
                        ReDim Preserve arResult(0 To i)
                        arResult(i) = CBool(parser.selectSingleNode("/s:Envelope/s:Body/ValidacionesHitosFacturacionResponse/ValidacionesHitosFacturacionResult").childnodes(i).Text)
                    Next
                End If
            End If
        Else
            iError = -1
            ReDim arResult(0 To 0)
            arResult(0) = False
        End If
    Else
        ReDim arResult(0 To 0)
        arResult(0) = True
    End If
           
    LlamadaWebServiceComprobarHitosFacturados = arResult
    
    ' Cierra object
    Set objXmlHttp = Nothing
End Function

