VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CHelp"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function EnumWindows Lib "user32" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long

Public glHandle As Long

Public Property Get glPid() As Long
    glPid = lPid
End Property
Public Property Let glPid(ByVal lNewValue As Long)
    lPid = lNewValue
End Property

Public Property Get colHandle() As Collection
    Set colHandle = mcolHandle
End Property
Public Property Set colHandle(ByVal colNewValue As Collection)
    Set mcolHandle = colNewValue
End Property


Public Function fEnumWindows() As Boolean
    Dim hwnd As Long
    '
    ' The EnumWindows function enumerates all top-level windows
    ' by passing the handle of each window, in turn, to an
    ' application-defined callback function. EnumWindows
    ' continues until the last top-level window is enumerated or
    ' the callback function returns FALSE.
    '
    'fEnumWindowsCallBack tiene que ser una funci�n en un m�dulo est�ndar de VB6
    Call EnumWindows(AddressOf fEnumWindowsCallBack, hwnd)
End Function

Private Sub Class_Initialize()
    Set mcolHandle = New Collection
End Sub

Private Sub Class_Terminate()
    Set mcolHandle = Nothing
End Sub
