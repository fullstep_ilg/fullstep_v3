VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTratarErrores"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' <summary>Funcion que trata errores de BD para mostrar el mensaje adecuado en cada momento</summary>
''' <param name="TESErr">Error summit que especifica el numero para mostrar el mensaje adecuado </param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:(hay cientos de llamadas); Tiempo máximo:0 </remarks>
''' <remarks>Revisado por: auv. 09/11/2011</remarks>

Public Sub TratarError(TESErr As TipoErrorSummit, ByRef oMensajes As CMensajes, ByRef gParametrosGenerales As ParametrosGenerales)
    Select Case TESErr.NumError
        
        Case erroressummit.TESErrorCodigo
                oMensajes.ErrorGeneral TESErr.Arg1, TESErr.Arg2
        
        Case erroressummit.TESUsuarioNoValido
                
                oMensajes.UsuarioNoAutorizado
                
        Case erroressummit.TESDatoDuplicado
                If IsNumeric(TESErr.Arg1) Then
                    oMensajes.DatoDuplicado TESErr.Arg1
                Else
                    oMensajes.DatoDuplicado 0, TESErr.Arg1
                End If
                
        Case erroressummit.TESDatoEliminado
            
                oMensajes.DatoEliminado TESErr.Arg1
                
        Case erroressummit.TESDatoNoValido
                
               oMensajes.NoValido TESErr.Arg1
               
        Case erroressummit.TESImposibleEliminar
                
            oMensajes.ImposibleEliDatRel TESErr.Arg1
                            
        Case erroressummit.TESInfModificada
                
                oMensajes.DatosModificados
                
        Case erroressummit.TESInfModificadaResum
                
                oMensajes.DatosModificados True
                
        Case erroressummit.TESFaltanDatos
                
                oMensajes.FaltanDatos TESErr.Arg1
        
    '    Case erroressummit.tesCodigoProcesoCambiado
                
                'Indicamos que el numero de proceso ha cambiado mientras se realizaba el alta
                
    '            oMensajes.CodigoProcesoCambiado
                
        Case erroressummit.TESCantDistMayorQueItem
                
                oMensajes.CantDistMayorQueItem
        
        Case erroressummit.TESValidarSinItems, erroressummit.TESValidarYaValidado, erroressummit.TESValidarSelProveSinProve, erroressummit.TESValidarSelProveYaValidado
                
                oMensajes.ImposibleValidar TESErr.Arg1, TESErr.Arg2
                
        Case erroressummit.TESAdjAntDenegada
                oMensajes.AdjAnterioresNoValidas
                
        Case erroressummit.TESVolumenAdjDirSuperado
                oMensajes.VolAdjDirectaSuperado TESErr.Arg1, TESErr.Arg2
                
        Case erroressummit.TESOtroerror
                
                oMensajes.OtroError TESErr.Arg1, TESErr.Arg2
                
        Case erroressummit.TESOfertaAdjudicada
                oMensajes.OfertaAdjudicada
        
        Case TESCierreSinPreAdjudicaciones
                oMensajes.ImposibleValidar TESErr.Arg1
        
        Case erroressummit.TESImposibleReabrir
                oMensajes.ImposibleReabrir TESErr.Arg1, TESErr.Arg2
            
        Case TESImposibleAdjudicarAProveNoHom
                oMensajes.ImposibleValidar TESErr.Arg1
        
        Case tesimposiblecambiarfecha
                oMensajes.ImposibleCambiarFecha TESErr.Arg1
        
        Case erroressummit.TESProcesoBloqueado
                oMensajes.ProcesoBloqueadoPorOtroUsuario TESErr.Arg1, TESErr.Arg2
        
        Case erroressummit.TESInfBloqueadaPorOtro
                oMensajes.InformacionBloqueadaPorOtroUsuario TESErr.Arg1
        
        Case erroressummit.TESImposibleEliminarAsignacion
                oMensajes.ImposibleEliminarAsignacion TESErr.Arg1, TESErr.Arg2
        
        Case erroressummit.tesprocesocerrado
                oMensajes.ProcesoCerradoPorOtroUsuario
        
        Case erroressummit.TESImposibleModificarReunion
                oMensajes.ImposibleModReunion
        
        Case erroressummit.TESImposibleAsignarProveedor
                oMensajes.ImposibleAsignarProveedor TESErr.Arg1
          
        Case erroressummit.TESInfActualModificada
                oMensajes.DatosActualesModificados
                
        Case erroressummit.TESProvePortalNoEnlazado
                oMensajes.ProveNoEnlazado
                
        Case erroressummit.TESProvePortalDesautorizado
                oMensajes.ImposibleAutorizarProvePortal
        
        Case erroressummit.TESProvePortalDesautorizadoEnCia
                oMensajes.ProvePortalDesautorizadoEnCia
            
        Case erroressummit.tesciaportaldesautorizada
                oMensajes.CiaDesautorizadaEnPortal
                
        Case erroressummit.TESPortalProveDesautorizada
                oMensajes.PortalProveDesautorizado
                
        Case erroressummit.TESProveedorHaUsadoElPortal
                oMensajes.ProveedorHaUsadoElPortal TESErr.Arg1
                
        Case erroressummit.TESProveedorEsPremium
                oMensajes.ProveedorEsPremium TESErr.Arg1
               
        Case erroressummit.TESSelProveFUPProveSinEnlace
                oMensajes.FUPExistenProveedoresSinEnlazar TESErr.Arg1
        
        Case erroressummit.TESSelProveBSProveSinEmail
                oMensajes.BSExistenProveedoresSinEmail TESErr.Arg1
        
        Case erroressummit.TESDesPublicarPremiumActivo
                oMensajes.ImposibleDespublicarProveedorPremiumActivo
    
        Case erroressummit.TESImposibleMarcarAprob
                Select Case TESErr.Arg1
                Case "USUELI"
                    oMensajes.ImposibleMarcarAprobador TESErr.Arg2, 226
                End Select
        Case erroressummit.TESImposibleEliminarApro
                oMensajes.ImposibleEliminarAprobaAprovi TESErr.Arg1
        
        Case erroressummit.TESImposiblePublicarLineaCatProveNoFormaPago
                oMensajes.ImposiblePublicarLineaCat 1
                
        Case erroressummit.TESImposiblePublicarLineaCatProveNoContactoAprov
                oMensajes.ImposiblePublicarLineaCat 2
        
        Case erroressummit.TESImposiblePublicarLineaCatProveNoProvAutorizado
                oMensajes.ImposiblePublicarLineaCat 3
                
        Case erroressummit.TESImposibleAnyadirAdjACatAdjYaEnCat
                oMensajes.ImposibleAnyadirAdjACat
    
        Case erroressummit.TESImposibleEliminarCat
                oMensajes.ImposibleEliminarCategoria
    
        Case erroressummit.TESImposibleAnyadirLineaCatNuevaSubCategoria
                oMensajes.ImposibleAnyadirLineaCatNuevaSubCat
    
        Case erroressummit.TESImposibleAnyadirLineaCatPorSeguridad
                oMensajes.ImposibleAnyadirLineaCatPorSeguridad
    
        Case erroressummit.TESImposibleAbrirProcesoSinPermisoMaterial
                oMensajes.ImposibleAbrirProcesoSinPermisoMaterial
                
        Case erroressummit.TESGrupoCerrado
                oMensajes.GrupoCerradoPorOtroUsuario
        
        Case erroressummit.TESImposibleAnyadirProceFaltanDatosObl
                oMensajes.ImposibleAnyadirProceFaltanDatosObl TESErr.Arg1
    
        Case erroressummit.TESImposibleAnyadirProceDesdePlantFaltanDatosObl
                oMensajes.ImposibleAnyadirProceDesdePlantFaltanDatosObl TESErr.Arg1
                
        Case erroressummit.TESImposibleEliminarAtributoMantenimiento
                oMensajes.ImposibleEliminarAtributoMantenimiento
    
        Case erroressummit.TESPresupuestoObligatorio
                oMensajes.ImposibleModificarConfiguracionDato TESErr.Arg1, gParametrosGenerales.gsPlurPres1, gParametrosGenerales.gsPlurPres2, _
                            gParametrosGenerales.gsPlurPres3, gParametrosGenerales.gsPlurPres4
                
        Case erroressummit.TESNingunGrupoDef
                oMensajes.ImposibleConfigDatoEnGrupo
        
        Case erroressummit.TESErrorDeTransferenciaDeArchivo
                oMensajes.ErrorDeTransferenciaDeArchivo
        Case erroressummit.TESADSIExProgramaIni, erroressummit.TESADSIExHomeDir
                oMensajes.ErrorOriginal TESErr
        Case erroressummit.TESRunAsUser, erroressummit.TESMailSMTP
                oMensajes.ErrorOriginal TESErr
        Case erroressummit.TESCambioContrasenya
            oMensajes.MensajeOKOnly 568
            
        Case TESCambiaThreshold
            oMensajes.MensajeOKOnly 573
        
        Case erroressummit.TESImposibleAnyadirGrupoFaltanDatosObl
            oMensajes.ImposibleAnyadirGrupoFaltanDatosObl TESErr.Arg1
            
        Case erroressummit.TESPedidoProcesoUnoSoloEmitido
            oMensajes.ImposibleEmitirPedidoUnoSolo TESErr.Arg1, TESErr.Arg2
    
        Case erroressummit.TESTraspasoAdjERP
            oMensajes.FaltanDatosTablasERP TESErr.Arg1
            
        Case erroressummit.TESNotifUsuMatQA
            oMensajes.FalloNotifUsuMatQA TESErr.Arg1
            
        Case erroressummit.TESValidarPerfilEliminar
            oMensajes.ImposibleEliminarPerfil
            
        Case erroressummit.TESArtGenericoModificado
            oMensajes.NoGrabaAperturaGenerico TESErr.Arg1
            
        Case erroressummit.TESProcesoSinMatNoMultimat
            oMensajes.ProcesoSinMatNoMultimat
            
        Case erroressummit.TESAtributoYaMarcadoAPedido
            oMensajes.AtributoYaMarcadoAPedido TESErr.Arg1
            
        Case erroressummit.TESImposibleAccederAlArchivoVinculado
            oMensajes.ImposibleAccederAlArchivoVinculado TESErr.Arg1, TESErr.Arg2
            
        Case erroressummit.TESImposibleValidarPorAtribsOblProvePremium
            oMensajes.AtributosEspObl TESErr.Arg1, TValidacionAtrib.Publicacion
        
        Case erroressummit.TESErrorEliminarRecepFacturas
            oMensajes.ErrorEliminarRecepFacturas
            
        Case erroressummit.TESNIFRepetido
            oMensajes.MensajeOKOnly 1130
            
        Case erroressummit.TESTelfNoValido
            oMensajes.MensajeOKOnly 1131
        
        Case erroressummit.TESEMailNoValido
            oMensajes.MensajeOKOnly 1132
        
        Case erroressummit.TESCodigoExistente
            oMensajes.MensajeOKOnly 1165, Exclamation
            
        Case erroressummit.TESUSCambioCodigoPdte
            oMensajes.MensajeOKOnly 1160, Exclamation
            
        Case erroressummit.TESUSCambioCodigoPdteNuevoCod
            oMensajes.MensajeOKOnly 1161, Exclamation
                
        Case erroressummit.TESPRCambioCodigoPdte
            oMensajes.MensajeOKOnly 1157, Exclamation
            
        Case erroressummit.TESPRCambioCodigoPdteNuevoCod
            oMensajes.MensajeOKOnly 1159, Exclamation
        
        Case erroressummit.TESPECambioCodigoPdte
            oMensajes.MensajeOKOnly 1156, Exclamation
            
        Case erroressummit.TESPECambioCodigoPdteNuevoCod
            oMensajes.MensajeOKOnly 1158, Exclamation
                
        Case erroressummit.TESTimeout
            oMensajes.MensajeOKOnly 1210, Critical
                
        Case erroressummit.TESErrorModificarRecepFacturas
            oMensajes.ErrorModificarRecepFacturas
              
        Case erroressummit.TESMailSMTP_FileIsOpen
            oMensajes.ErrorFicheroAAdjuntarAbierto
        
        Case erroressummit.TESContraseñaNoCumpleLongitud
            oMensajes.ContraseñaNoCumpleLongitud gParametrosGenerales.gbCOMPLEJIDAD_PWD, gParametrosGenerales.giMIN_SIZE_PWD
            
        Case erroressummit.TESContraseñaNoCumpleComplejidad
            oMensajes.ContraseñaNoCumpleComplejidad gParametrosGenerales.giMIN_SIZE_PWD
            
        Case erroressummit.TESContraseñaNoSePuedecambiar
            oMensajes.ContraseñaNoSePuedecambiar
            
        Case erroressummit.TESContraseñaNoCumpleHistorico
            oMensajes.ContraseñaNoCumpleHistorico gParametrosGenerales.giHIST_PWD
            
        Case erroressummit.TESContraseñaNoCumpleLongitud
            oMensajes.ContraseñaNoCumpleLongitud gParametrosGenerales.gbCOMPLEJIDAD_PWD, gParametrosGenerales.giMIN_SIZE_PWD
            
        Case erroressummit.TESContraseñaErrorInesperado
            oMensajes.ContraseñaErrorInesperado
            
        Case erroressummit.TESContraseñaErrorInesperado
            oMensajes.ContraseñaErrorInesperado
        Case erroressummit.TESASPUserCuentaExiste
            oMensajes.CuentaUsuarioExistente
        Case erroressummit.TESErrorAdjuntarFichero
            oMensajes.ErrorAdjuntarFichero
    End Select
End Sub

''' <summary>
''' Construye un "Mensaje de error" a partir de un mensaje
''' </summary>
''' <param name="TESErr">error</param>
''' <returns>Mensaje de error</returns>
''' <remarks>Llamada desde: frmAhorrosPend/MostrarTexto ; Tiempo máximo: 0,2</remarks>
Public Function TratarErrorSinMensaje(TESErr As TipoErrorSummit, ByRef oMensajes As CMensajes) As String
    Dim sErrMensaje As String
    Dim i As Integer
    
    Select Case TESErr.NumError
        
        Case erroressummit.TESUsuarioNoValido
            sErrMensaje = oMensajes.CargarTextoMensaje(53)
                
        Case erroressummit.TESDatoDuplicado
            sErrMensaje = oMensajes.CargarTextoMensaje(36) & " " & oMensajes.CargarTexto(OTROS, TESErr.Arg1)
                
        Case erroressummit.TESDatoEliminado
            sErrMensaje = oMensajes.CargarTextoMensaje(6) & " " & oMensajes.CargarTexto(OTROS, TESErr.Arg1)
                
        Case erroressummit.TESDatoNoValido
            sErrMensaje = oMensajes.CargarTextoMensaje(5) & " " & oMensajes.CargarTexto(OTROS, TESErr.Arg1)
              
        Case erroressummit.TESImposibleEliminar
            sErrMensaje = oMensajes.CargarTextoMensaje(14) & vbLf & oMensajes.CargarTextoMensaje(15) & oMensajes.CargarTexto(TABLAS, TESErr.Arg1)
                            
        Case erroressummit.TESInfModificada
            sErrMensaje = oMensajes.CargarTextoMensaje(33) & vbCrLf & oMensajes.CargarTextoMensaje(34) & vbCrLf & oMensajes.CargarTextoMensaje(35)
                
        Case erroressummit.TESFaltanDatos
            sErrMensaje = oMensajes.CargarTextoMensaje(30) & vbCrLf & oMensajes.CargarTexto(OTROS, TESErr.Arg1)
                    
        Case erroressummit.TESCantDistMayorQueItem
            sErrMensaje = oMensajes.CargarTextoMensaje(107)
        
        Case erroressummit.TESValidarSinItems, erroressummit.TESValidarYaValidado, erroressummit.TESValidarSelProveSinProve, erroressummit.TESValidarSelProveYaValidado
            sErrMensaje = oMensajes.CargarTextoMensaje(109) & vbLf & oMensajes.CargarTextoMensaje(41) & " " & oMensajes.CargarTexto(OTROS, TESErr.Arg1) & " " & TESErr.Arg2
               
        Case erroressummit.TESAdjAntDenegada
            sErrMensaje = oMensajes.CargarTextoMensaje(120)
        
        Case erroressummit.TESVolumenAdjDirSuperado
            sErrMensaje = oMensajes.CargarTextoMensaje(79) & vbLf & oMensajes.CargarTextoMensaje(80) & " " & DblToStrInterno(TESErr.Arg1) & vbLf & oMensajes.CargarTextoMensaje(81) & " " & DblToStrInterno(TESErr.Arg2)
                
        Case erroressummit.TESOtroerror
            sErrMensaje = oMensajes.CargarTextoMensaje(72) & vbLf & oMensajes.CargarTextoMensaje(73) & " " & TESErr.Arg1 & vbLf & oMensajes.CargarTextoMensaje(74) & " " & TESErr.Arg2 & vbLf & oMensajes.CargarTextoMensaje(75)
                
        Case erroressummit.TESOfertaAdjudicada
            sErrMensaje = oMensajes.CargarTextoMensaje(130)
        
        Case TESCierreSinPreAdjudicaciones
            sErrMensaje = oMensajes.CargarTextoMensaje(109) & vbLf & oMensajes.CargarTextoMensaje(41) & " " & oMensajes.CargarTexto(OTROS, TESErr.Arg1)
        
        Case erroressummit.TESImposibleReabrir
            sErrMensaje = oMensajes.CargarTextoMensaje(110) & vbLf & oMensajes.CargarTextoMensaje(41) & " " & oMensajes.CargarTexto(OTROS, TESErr.Arg1) & " " & TESErr.Arg2
            
        Case TESImposibleAdjudicarAProveNoHom
            sErrMensaje = oMensajes.CargarTextoMensaje(109) & vbLf & oMensajes.CargarTextoMensaje(41) & " " & oMensajes.CargarTexto(OTROS, TESErr.Arg1)
        
        Case tesimposiblecambiarfecha
            sErrMensaje = oMensajes.CargarTextoMensaje(86) & vbLf & oMensajes.CargarTextoMensaje(87) & " " & oMensajes.CargarTexto(OTROS, TESErr.Arg1)
            
        Case erroressummit.TESInfBloqueadaPorOtro
            sErrMensaje = oMensajes.CargarTextoMensaje(77) & vbLf & TESErr.Arg1 & vbLf & oMensajes.CargarTextoMensaje(78)
            
        Case erroressummit.TESImposibleEliminarAsignacion
            sErrMensaje = oMensajes.CargarTextoMensaje(83) & vbLf & oMensajes.CargarTextoMensaje(84)
        
        Case erroressummit.tesprocesocerrado
            sErrMensaje = oMensajes.CargarTexto(Mensajes, 64)
        
        Case erroressummit.TESImposibleModificarReunion
            sErrMensaje = oMensajes.CargarTextoMensaje(104)
        
        Case erroressummit.TESImposibleAsignarProveedor
            sErrMensaje = TESErr.Arg1 & ":" & vbCrLf & oMensajes.CargarTextoMensaje(108)
          
        Case erroressummit.TESInfActualModificada
            sErrMensaje = oMensajes.CargarTextoMensaje(82)
                
        Case erroressummit.TESProvePortalDesautorizado
            sErrMensaje = oMensajes.CargarTextoMensaje(2)
        
        Case erroressummit.TESProvePortalDesautorizadoEnCia
            sErrMensaje = oMensajes.CargarTextoMensaje(158)
            
        Case erroressummit.tesciaportaldesautorizada
            sErrMensaje = oMensajes.CargarTextoMensaje(1)
                
        Case erroressummit.TESProveedorHaUsadoElPortal
            If TESErr.Arg1 = 1 Then
                sErrMensaje = oMensajes.CargarTextoMensaje(148)
            Else
                sErrMensaje = oMensajes.CargarTextoMensaje(149)
            End If
    
        Case erroressummit.TESProveedorEsPremium
            If TESErr.Arg1 = 1 Then
                sErrMensaje = oMensajes.CargarTextoMensaje(150)
            Else
                sErrMensaje = oMensajes.CargarTextoMensaje(151)
            End If
               
        Case erroressummit.TESSelProveFUPProveSinEnlace
            For i = 0 To UBound(TESErr.Arg1, 2)
                sErrMensaje = oMensajes.CargarTextoMensaje(159) & vbLf & oMensajes.CargarTexto(157, 23) & " " & TESErr.Arg1(0, i) & vbTab & " - " & TESErr.Arg1(1, i)
            Next
        
        Case erroressummit.TESSelProveBSProveSinEmail
            'For i = 0 To UBound(Proves, 2)
                sErrMensaje = oMensajes.CargarTextoMensaje(160) & vbLf & oMensajes.CargarTexto(157, 23) & " " & TESErr.Arg1(0, i) & vbTab & " - " & TESErr.Arg1(1, i)
            'Next
        
        Case erroressummit.TESDesPublicarPremiumActivo
            sErrMensaje = oMensajes.CargarTextoMensaje(161) & vbLf & oMensajes.CargarTextoMensaje(162)
              
    End Select
    
    TratarErrorSinMensaje = sErrMensaje
End Function
