VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPWD"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function ContraseñaExpirada(ByVal iEdadMaxPwd As Integer, ByVal dtFecPwd As Date)
    If iEdadMaxPwd = 0 Then
        ContraseñaExpirada = False
    Else
        If iEdadMaxPwd + dtFecPwd < Now Then
            ContraseñaExpirada = True
        Else
            ContraseñaExpirada = False
        End If
    End If
End Function

''' <summary>
''' Funcion que trata de simular server.URLEncode transforma la contraseña para que pueda enviarse por web
''' </summary>
''' <param name="sRawURL">Contraseña a transformar</param>
''' <returns>String con cadena transformada</returns>
''' <remarks>Llamada desde=propio formulario; Tiempo máximo=0seg.</remarks>
Public Function URLEncode(ByVal sRawURL As String) As String
    URLEncode = URLEncodeInterno(sRawURL)
End Function

