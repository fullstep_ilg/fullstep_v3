Attribute VB_Name = "basPublic"
Option Explicit

Public Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, _
     ByVal samDesired As Long, phkResult As Long) As Long
Public Declare Function RegQueryValueExString Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, _
     lpType As Long, ByVal lpData As String, lpcbData As Long) As Long
Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Public Declare Function RegEnumKeyEx Lib "advapi32.dll" Alias "RegEnumKeyExA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpName As String, _
     lpcbName As Long, ByVal lpReserved As Long, ByVal lpClass As String, lpcbClass As Long, lpftLastWriteTime As FILETIME) As Long
Public Declare Function RegQueryValueExAny Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, _
    lpType As Long, ByRef lpData As Any, lpcbData As Long) As Long
Public Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, lpdwProcessId As Long) As Long
Public Declare Function GetParent Lib "user32" (ByVal hwnd As Long) As Long
    
Public Type FILETIME
        dwLowDateTime As Long
        dwHighDateTime As Long
End Type
    
Public Const HKEY_LOCAL_MACHINE As Long = &H80000002
Public Const KEY_QUERY_VALUE As Long = &H1
Public Const HKEY_CURRENT_USER        As Long = &H80000001
Public Const ERROR_NONE As Long = 0
Public Const KEY_ENUMERATE_SUB_KEYS    As Long = &H8
Public Const ERROR_NO_MORE_ITEMS = 259&
Public Const REG_SZ                    As Long = 1

Public lPid As Long
Public mcolHandle As New Collection

''' <summary>Devuelve un valor de tipo string de un clave del registro</summary>
''' <param name="sClaveRegistro">Clave principal del registro</param>
''' <param name="sKey">La clave de la que se quiere el valor</param>
''' <returns>Un string con el valor pedido de la clave pasada como par�metro</returns>
''' <remarks>Llamada desde basUtilidades.ObtenerZonasHorarias</remarks>

Public Function GetStringValueInterno(ByVal sClaveRegistro As String, ByVal sKey As String, ByVal sValue As String) As String
    Dim lRetVal As Long
    Dim lHandle As Long
    Dim sVal As String
    Dim lKeyValSize As Long
    Dim lKeyValType As Long
    
    ' Open up the key
    If sClaveRegistro = "HKEY_LOCAL_MACHINE" Then
        lRetVal = RegOpenKeyEx(HKEY_LOCAL_MACHINE, sKey, 0, KEY_QUERY_VALUE, lHandle)
    Else
        lRetVal = RegOpenKeyEx(HKEY_CURRENT_USER, sKey, 0, KEY_QUERY_VALUE, lHandle)
    End If
        
    If lRetVal = ERROR_NONE Then
        sVal = String$(1024, 0)
        lKeyValSize = 1024
                
        lRetVal = RegQueryValueExString(lHandle, sValue, 0, lKeyValType, sVal, lKeyValSize)
        If lRetVal = ERROR_NONE Then
            sVal = Left(sVal, lKeyValSize)
        Else
            Err.Raise vbObjectError + 2000 + lRetVal, , "Could not get registry value"
        End If
        
        lRetVal = RegCloseKey(lHandle)
    Else
        Err.Raise vbObjectError + 2000 + lRetVal, , "Could not enumerate registry section"
    End If
    
    GetStringValueInterno = sVal
End Function

''' <summary>Devuelve las claves del registro que cuelgan de otra clave</summary>
''' <param name="sClaveRegistro">Clave principal del registro</param>
''' <param name="sKey">La clave de la que se quiere obtener las claves hijas</param>
''' <returns>Un array de strings con las claves hijas</returns>
''' <remarks>Llamada desde basUtilidades.ObtenerZonasHorarias</remarks>

Public Function GetSubKeysInterno(ByVal sClaveRegistro As String, ByVal sKey As String) As String()
    Dim aKeys() As String
    Dim lRetVal As Long
    Dim lRetEnumVal As Long
    Dim lHandle As Long
    Dim iIndex As Integer
    Dim sSubKey As String
    Dim lSubKeysize As Long
    Dim sPClass As String
    Dim lPClassSize As Long
    Dim LWTime As FILETIME
    
    ' Open up the key
    If sClaveRegistro = "HKEY_LOCAL_MACHINE" Then
        lRetVal = RegOpenKeyEx(HKEY_LOCAL_MACHINE, sKey, 0, KEY_ENUMERATE_SUB_KEYS, lHandle)
    Else
        lRetVal = RegOpenKeyEx(HKEY_CURRENT_USER, sKey, 0, KEY_ENUMERATE_SUB_KEYS, lHandle)
    End If
        
    If lRetVal = ERROR_NONE Then
        iIndex = 0
                
        Do
            lSubKeysize = 2000
            sSubKey = String(lSubKeysize, 0)
            lPClassSize = 2000
            sPClass = String(lPClassSize, 0)
            
            lRetEnumVal = RegEnumKeyEx(lHandle, iIndex, sSubKey, lSubKeysize, 0&, sPClass, lPClassSize, LWTime)
            If lRetEnumVal <> ERROR_NONE Then
                If lRetEnumVal <> ERROR_NO_MORE_ITEMS Then Err.Raise vbObjectError + 2000 + lRetEnumVal, , "Could not open registry section"
            Else
                sSubKey = Left(sSubKey, lSubKeysize)
                
                ReDim Preserve aKeys(iIndex)
                aKeys(iIndex) = sSubKey
                            
                iIndex = iIndex + 1
            End If
        Loop While lRetEnumVal = ERROR_NONE
        
        lRetVal = RegCloseKey(lHandle)
    Else
        Err.Raise vbObjectError + 2000 + lRetVal, , "Could not enumerate registry section"
    End If
    
    GetSubKeysInterno = aKeys
End Function

Public Function NullToDbl0Interno(ByVal ValueThatCanBeNull As Variant) As Variant
    If IsNull(ValueThatCanBeNull) Then
        NullToDbl0Interno = 0
    Else
        NullToDbl0Interno = ValueThatCanBeNull
    End If
End Function

Public Function NullToStrInterno(ByVal ValueThatCanBeNull As Variant) As String
    If IsEmpty(ValueThatCanBeNull) Then
        NullToStrInterno = ""
    End If
    If IsNull(ValueThatCanBeNull) Then
        NullToStrInterno = ""
    Else
        NullToStrInterno = CStr(ValueThatCanBeNull)
    End If
End Function

Public Function DblToStrInterno(ByVal dblThatCanBeEmpty As Variant, Optional ByVal Formato As String) As String
    If IsEmpty(dblThatCanBeEmpty) Then
        DblToStrInterno = ""
    Else
        If IsNull(dblThatCanBeEmpty) Then
            DblToStrInterno = ""
        Else
            If Formato = "" Then
                DblToStrInterno = Format(dblThatCanBeEmpty, "standard")
            Else
                DblToStrInterno = Format(dblThatCanBeEmpty, Formato)
            End If
        End If
    End If
End Function

Public Function FormateoNumericoInterno(ByVal dblNum As Double, Optional ByVal sFormato As String) As String
    Dim dblRes As String
    'Formato por defecto a 8 decimales
    On Error GoTo Error:

    If sFormato = "" Then
      dblRes = Format(dblNum, "#,##0.########")
    Else
      dblRes = Format(dblNum, sFormato)
    End If
    'Se quita el simbolo decimal si no hay decimales
    If Not IsNumeric(Right(dblRes, 1)) Then
      dblRes = Left(dblRes, Len(dblRes) - 1)
    End If
    FormateoNumericoInterno = dblRes
    Exit Function
Error:
If Err.Number = 13 Then
    dblRes = Format(dblNum, "#,##0.########")
    Resume Next
End If

End Function

Public Function fEnumWindowsCallBack(ByVal hwnd As Long, ByVal lpData As Long) As Long
    Dim lParent    As Long
    Dim lThreadId  As Long
    Dim lProcessId As Long
    '
    ' This callback function is called by Windows (from the EnumWindows
    ' API call) for EVERY top-level window that exists.  It populates a
    ' collection with the handles of all parent windows owned by the
    ' process that we started.
    '
    fEnumWindowsCallBack = 1
    lThreadId = GetWindowThreadProcessId(hwnd, lProcessId)
    
    If lPid = lProcessId Then
        lParent = GetParent(hwnd)
        If lParent = 0 Then
            mcolHandle.Add hwnd
        End If
    End If
End Function

''' <summary>
''' Funcion que trata de simular server.URLEncode transforma la contrase�a para que pueda enviarse por web
''' </summary>
''' <param name="sRawURL">Contrase�a a transformar</param>
''' <returns>String con cadena transformada</returns>
''' <remarks>Llamada desde=propio formulario; Tiempo m�ximo=0seg.</remarks>
Public Function URLEncodeInterno(ByVal sRawURL As String, Optional ByVal NoAdmiteAnd As Boolean = False) As String
    Dim iLoop As Integer
    Dim sRtn As String
    Dim sTmp As String
    Const sValidChars = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz:/.?=_-$(){}~&"

    If Len(sRawURL) > 0 Then
        For iLoop = 1 To Len(sRawURL)
            sTmp = Mid(sRawURL, iLoop, 1)

            If (InStr(1, sValidChars, sTmp, vbBinaryCompare) = 0) Or (NoAdmiteAnd And sTmp = "&") Then
                sTmp = Hex(Asc(sTmp))

                If sTmp = "20" Then
                    sTmp = "+"
                ElseIf Len(sTmp) = 1 Then
                    sTmp = "%0" & sTmp
                Else
                    sTmp = "%" & sTmp
                End If
            End If
            sRtn = sRtn & sTmp
        Next iLoop
    End If
    
    URLEncodeInterno = sRtn
End Function

Public Sub ReadByteArrayInterno(ByVal strPath As String, ByRef arrData() As Byte)
    Dim lngFile As Long

    ' open the file
    lngFile = FreeFile
    Open strPath For Binary Access Read As lngFile

    ' allocate enough memory to read file in one go
    ReDim arrData(1 To LOF(lngFile)) As Byte

    ' read blob
    Get lngFile, , arrData
    
    ' close file
    Close lngFile
End Sub

Public Function EncodeBase64Interno(ByRef arrData() As Byte) As String
    Dim objXML As Object
    Dim objNode As Object
    
    Set objXML = CreateObject("MSXML2.DOMDocument")
   
    ' byte array to base64
    Set objNode = objXML.createElement("b64")
    objNode.DataType = "bin.base64"
    objNode.nodeTypedValue = arrData
    EncodeBase64Interno = objNode.Text

    ' thanks, bye
    Set objNode = Nothing
    Set objXML = Nothing
End Function
