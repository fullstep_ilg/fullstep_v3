VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNifCif"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function LETRA_CIF(stCIF As String) As String
    '*************************************************************
    'Funci�n usada para calcular la letra del NIF correspondiente
    'Creado por Jos� M� Fueyo.
    '*************************************************************
    On Error GoTo Letra_CifERROR
    Dim lngVALOR As Long, intENTERO As Long
    lngVALOR = CLng(stCIF)
    intENTERO = lngVALOR - ((Fix(lngVALOR / 23)) * 23) + 1
    Select Case intENTERO
        Case 1, 24
            LETRA_CIF = "T"
        Case 2
            LETRA_CIF = "R"
        Case 3
            LETRA_CIF = "W"
        Case 4
            LETRA_CIF = "A"
        Case 5
            LETRA_CIF = "G"
         Case 6
            LETRA_CIF = "M"
        Case 7
            LETRA_CIF = "Y"
        Case 8
            LETRA_CIF = "F"
        Case 9
            LETRA_CIF = "P"
        Case 10
            LETRA_CIF = "D"
        Case 11
            LETRA_CIF = "X"
        Case 12
            LETRA_CIF = "B"
        Case 13
            LETRA_CIF = "N"
        Case 14
            LETRA_CIF = "J"
        Case 15
            LETRA_CIF = "Z"
        Case 16
            LETRA_CIF = "S"
        Case 17
            LETRA_CIF = "Q"
        Case 18
            LETRA_CIF = "V"
        Case 19
            LETRA_CIF = "H"
        Case 20
            LETRA_CIF = "L"
        Case 21
            LETRA_CIF = "C"
        Case 22
            LETRA_CIF = "K"
        Case 23
            LETRA_CIF = "E"
    End Select
    Exit Function
    'Control de errores
Letra_CifERROR:
    If Err.Number = 13 Then
        MsgBox "INTRODUCE UN N�MERO, SIN LETRAS", vbOKOnly + vbExclamation, "!"
    Else
        MsgBox Err.Number & vbCrLf & Err.Description
    End If
End Function

''' <summary>
''' Validar un NIF.
''' Comprobar que tiene 8 n�meros y una letra (entre A-Z). Luego con los 8 numeros comprobar letra.
''' </summary>
''' <param name="sNif">Nif a validar</param>
''' <returns>Si es valido o no</returns>
''' <remarks>Llamada desde: frmCompanias/ValidarNIF; Tiempo m�ximo:0,1</remarks>
Public Function VALIDAR_NIF(sNif As String) As Boolean
    Dim i As Integer
    'FUNCION QUE VALIDA UN VALOR DE NIF
    '9 ALFANUM�RICOS, EL ULTIMO LETRA.
    On Error GoTo error
    Dim lngVALOR As Long, intENTERO As Long, sLetra As String
    If Len(Trim(sNif)) <> 9 Then 'Compruebo longitud=9
        VALIDAR_NIF = False
    Else 'Compruebo si 8 n�meros y si letra para 8 n�meros
        For i = 1 To 8
            If Not IsNumeric(Mid(sNif, i, 1)) Then
                VALIDAR_NIF = False
                Exit Function
            End If
        Next
        
        sLetra = UCase(Right(sNif, 1))
        If sLetra >= "A" And sLetra <= "Z" Then
            lngVALOR = Val(sNif)
            intENTERO = lngVALOR - ((Fix(lngVALOR / 23)) * 23) + 1
            Select Case intENTERO
                Case 1, 24
                    If sLetra = "T" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 2
                    If sLetra = "R" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 3
                    If sLetra = "W" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 4
                    If sLetra = "A" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 5
                    If sLetra = "G" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                 Case 6
                    If sLetra = "M" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 7
                    If sLetra = "Y" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 8
                    If sLetra = "F" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 9
                    If sLetra = "P" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 10
                    If sLetra = "D" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 11
                    If sLetra = "X" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 12
                    If sLetra = "B" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 13
                    If sLetra = "N" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 14
                    If sLetra = "J" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 15
                    If sLetra = "Z" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 16
                    If sLetra = "S" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 17
                    If sLetra = "Q" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 18
                    If sLetra = "V" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 19
                    If sLetra = "H" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 20
                    If sLetra = "L" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 21
                    If sLetra = "C" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 22
                    If sLetra = "K" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
                Case 23
                    If sLetra = "E" Then
                        VALIDAR_NIF = True
                    Else
                        VALIDAR_NIF = False
                    End If
            End Select
        Else
            VALIDAR_NIF = False
        End If
    End If
fin:
    Exit Function
error:
    VALIDAR_NIF = False
    Resume fin

    
End Function

''' <summary>
''' Validar un CIF o NIE.
''' Comprobar que tiene 1 letra seguido de 7 n�meros y 1 numero o letra. Luego con los 7 numeros comprobar letra.
''' NIE 1 letra de entre (XYZ) + 7 numeros + 1 letra
''' </summary>
''' <param name="sCIF">Cif o Nie a validar</param>
''' <returns>Si es valido o no</returns>
''' <remarks>Llamada desde: frmCompanias/ValidarNIF; Tiempo m�ximo:0,1</remarks>
Public Function VALIDAR_CIF(sCIF As String) As Boolean
    'Compruebo si un CIF pasado es v�lido o no
    'Tiene que tener 9 caracteres.
    'Para guardar el primer y �ltimo caracter
    On Error GoTo error
    Dim sPRI As String, sULT As String, sRESTO As String, i As Integer
    'Para la suma de posiciones
    Dim iSUMA As Integer, sIMPARES(1 To 4) As String
    If Len(Trim(sCIF)) <> 9 Then 'Compruebo que sea de 9 caracteres
        VALIDAR_CIF = False
    Else
        sPRI = UCase(Left(sCIF, 1)) 'Guarda letra
        sULT = Right(sCIF, 1) 'Guarda d�gito de control
        sRESTO = Mid(sCIF, 2, 7) 'Guarda cuerpo CIF, usado para c�lculo
        For i = 1 To 7
            If Not IsNumeric(Mid(sRESTO, i, 1)) Then
                VALIDAR_CIF = False
                Exit Function
            End If
        Next
        'Suma de posiciones pares
        iSUMA = CInt(Mid(sRESTO, 2, 1)) + CInt(Mid(sRESTO, 4, 1)) + CInt(Mid(sRESTO, 6, 1))
        'Multiplico posiciones impares por dos,
        'y sumo cifras resultantes
        sIMPARES(1) = Format(2 * CInt(Mid(sRESTO, 1, 1)), "00")
        sIMPARES(2) = Format(2 * CInt(Mid(sRESTO, 3, 1)), "00")
        sIMPARES(3) = Format(2 * CInt(Mid(sRESTO, 5, 1)), "00")
        sIMPARES(4) = Format(2 * CInt(Mid(sRESTO, 7, 1)), "00")
        For i = 1 To 4
            iSUMA = iSUMA + (CInt(Left(sIMPARES(i), 1)) + CInt(Right(sIMPARES(i), 1)))
        Next i
        'Obtengo la posici�n de unidades
        If Not sPRI = "X" And Not sPRI = "Y" And Not sPRI = "Z" And Not sPRI = "P" And Not sPRI = "S" And Not sPRI = "Q" Then 'No es ayuntamiento/extranjero
            If sULT = Right(CStr(10 - CInt(Right(CStr(iSUMA), 1))), 1) Then
                VALIDAR_CIF = True
            Else
                Select Case Right(CStr(10 - CInt(Right(CStr(iSUMA), 1))), 1)
                    Case 1
                        If (sULT = "A") Then
                            VALIDAR_CIF = True
                        Else
                            VALIDAR_CIF = False
                        End If
                    Case 2
                        If (sULT = "B") Then
                            VALIDAR_CIF = True
                        Else
                            VALIDAR_CIF = False
                        End If
                    Case 3
                        If (sULT = "C") Then
                            VALIDAR_CIF = True
                        Else
                            VALIDAR_CIF = False
                        End If
                    Case 4
                        If (sULT = "D") Then
                            VALIDAR_CIF = True
                        Else
                            VALIDAR_CIF = False
                        End If
                    Case 5
                        If (sULT = "E") Then
                            VALIDAR_CIF = True
                        Else
                            VALIDAR_CIF = False
                        End If
                    Case 6
                        If (sULT = "F") Then
                            VALIDAR_CIF = True
                        Else
                            VALIDAR_CIF = False
                        End If
                    Case 7
                        If (sULT = "G") Then
                            VALIDAR_CIF = True
                        Else
                            VALIDAR_CIF = False
                        End If
                    Case 8
                        If (sULT = "H") Then
                            VALIDAR_CIF = True
                        Else
                            VALIDAR_CIF = False
                        End If
                    Case 9
                        If (sULT = "I") Then
                            VALIDAR_CIF = True
                        Else
                            VALIDAR_CIF = False
                        End If
                    Case 0
                        If (sULT = "J") Then
                            VALIDAR_CIF = True
                        Else
                            VALIDAR_CIF = False
                        End If
                    Case Else
                        VALIDAR_CIF = False
                End Select
            End If
        Else 'Ayuntamiento/Extranjero. Busco letra control
            If sPRI = "X" Then
                VALIDAR_CIF = VALIDAR_NIF("0" & sRESTO & sULT)
            ElseIf sPRI = "Y" Then
                VALIDAR_CIF = VALIDAR_NIF("1" & sRESTO & sULT)
            ElseIf sPRI = "Z" Then
                VALIDAR_CIF = VALIDAR_NIF("2" & sRESTO & sULT)
            Else
                If sULT = Chr(64 + (10 - CInt(Right(CStr(iSUMA), 1)))) Then
                    VALIDAR_CIF = True
                Else
                    VALIDAR_CIF = False
                End If
            End If
        End If
    End If
fin:
    Exit Function
error:
    VALIDAR_CIF = False
    Resume fin
End Function

Function iCalculaCif(sCIF As String) As Integer

    ' Objetivo:
        ' Procedimiento que chequea la validez del CIF.

    ' Par�metros:
        ' Nif. String. Contiene el NIF introducido.
        ' iVuelta. String. Devuelve el resultado del chequeo.

    ' Resultado:
        ' True/False si el chequeo es correcto o incorrecto.

    Dim i As Integer, Tr As Integer, Tr1 As Integer, Decena As Integer
    Dim sLetra As String, sAux As String, sAux2 As String, sMultip As String

    If Len(sCIF) < 9 Then
        Exit Function
    Else
        sLetra = UCase$(Left$(sCIF, 1))        'Guarda la primera letra.
       
        'Coge solamente los n�meros.
        sAux = Right$(sCIF, Len(sCIF) - 1)   'Quita la letra del CIF.
        sAux2 = ""
        For i = 1 To Len(sAux)
            If IsNumeric(Mid$(sAux, i, 1)) Then
                sAux2 = sAux2 & Mid$(sAux, i, 1)
            End If
        Next i

        If Len(sAux2) <> 8 Then
            Exit Function
        Else
            sAux = sAux2
            sMultip = "2121212"   'Multiplicar por los 7 primeros d�gitos.
            Tr1 = 0
            For i = 1 To 7
                Tr = Val(Mid$(sAux, i, 1)) * Val(Mid$(sMultip, i, 1))
                If Tr > 9 Then
                    Tr = Tr - 9
                End If
                Tr1 = Tr1 + Tr
            Next i
            
            If Right$(str$(Tr1), 1) = "0" Then
                Decena = Tr1
            Else
                Decena = (Int(Tr1 / 10) + 1) * 10
            End If
            
            If Val(Right$(sAux, 1)) = Decena - Tr1 Then
                ' C.I.F. Correcto
                iCalculaCif = True
                  Else
                Exit Function
            End If
        End If
    End If
End Function

Public Function CalculaCIF(strA As String) As Boolean
Dim strB, strC, strD, aux, ncaux, ncaux2, ncaux3 As String
Dim i, i2, i3, auxim As Integer
Dim a#, b#, c#, d#
Dim d1#, d2#
Dim aimpar(4) As String
If Len(strA) = 0 Or Len(strA) < 9 Then Exit Function
strB = Mid(strA, 1, 1)
strC = Mid(strA, 9, 1)
strD = Mid(strA, 2, 7)
For i = 2 To Len(strD)
    a# = a# + Int(Mid(strD, i, 1))
i = i + 1
Next i
i = 0
i2 = 0
For i = 1 To Len(strD)
    b# = (Int(Mid(strD, i, 1)) * 2)
    aimpar(i2) = b#
    i2 = i2 + 1
    i = i + 1
Next i
For i3 = 0 To UBound(aimpar) - 1
    aux = aimpar(i3)
    d1 = Int(Mid(aux, 1, 1))
    If Len(aux) = 2 Then
        d2 = Int(Mid(aimpar(i3), 2, 1))
    End If
    If Len(aux) = 1 Then
        d2 = 0
    End If
    d1 = d1 + d2
    auxim = auxim + d1
Next i3
ncaux = str(auxim + a#)
ncaux2 = Trim(Mid(ncaux, 3, 1))
ncaux3 = 10 - ncaux2
Dim arrayletras(26) As String
Dim indice As Integer
For indice = 1 To UBound(arrayletras())
    arrayletras(indice) = Chr(64 + indice)
Next indice
Dim Control As String
If (UCase(strB) = "X") Or (UCase(strB) = "P") Then
    ncaux2 = Chr(64 + (10 - Val(ncaux2)))
    For indice = 1 To UBound(arrayletras())
        If (ncaux2 = arrayletras(indice)) Then
            ncaux2 = indice
            Exit For
        End If
    Next indice
    If (strC = indice) Then
        Control = "El cif es correcto el digito de control calculado por corespondencia con la letra coincide con la ultima cifra"
        CalculaCIF = True
    Else
        Control = "El cif no es correcto los digitos de control no coinciden"
        CalculaCIF = False
    End If
    'CalculaCIF = ncaux2
Else
    If (strC = ncaux3) Then
        Control = "El cif es correcto el digito de control calculado coincide con la ultima cifra"
        CalculaCIF = True
    Else
        Control = "El cif no es correcto los digitos de control no coinciden"
        CalculaCIF = False
    End If
End If
End Function

Private Function CalculaNIF(strA As String) As String
Const cCADENA = "TRWAGMYFPDXBNJZSQVHLCKE"
'Const cNUMEROS = "0123456789"
Dim strT As String, strB As String
Dim a#, NIF#, b#, c#
Dim i As Integer
    
strT = Trim$(strA)
If Len(strT) = 0 Then Exit Function
  
a# = 0
NIF# = Val(strA)
Do
    b# = Int(NIF# / 24)
    c# = NIF# - (24 * b#)
    a# = a# + c#
    NIF# = b#
Loop While b# <> 0
b# = Int(a# / 23)
c# = a# - (23 * b#)
strA = Trim$(strT) + Mid$(cCADENA, c# + 1, 1)
CalculaNIF = strA
End Function
