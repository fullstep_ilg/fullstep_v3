VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSesionUsuario"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'''''' <summary>
'''''' Proceso que llama al webService SessionService para obtener el session id para guardar el usuario y la contrase�a y pasarlo en la URL
'''''' </summary>
'''''' <remarks>Llamada desde=MostrarPaginaWeb; Tiempo m�ximo=0</remarks>
Public Function ObtenerSessionId(ByVal sURLSvc As String) As String
    Dim oWebSvc As CWebService
    
    Set oWebSvc = New CWebService
    ObtenerSessionId = oWebSvc.LlamadaWebServiceSessionID(sURLSvc)
    
    Set oWebSvc = Nothing
End Function

'''''' <summary>
'''''' Proceso que llama al webService SessionService para guardar el usuario y la contrase�a
'''''' </summary>
'''''' <remarks>Llamada desde=MostrarPaginaWeb; Tiempo m�ximo=0</remarks>
Public Sub EstablecerSessionData(ByVal strSessionId As String, ByVal strKey As String, ByVal strValue As String, ByVal sURLSvc As String)
    Dim oWebSvc As CWebService
    
    Set oWebSvc = New CWebService
    oWebSvc.LlamadaWebServiceSessionData strSessionId, strKey, strValue, sURLSvc
    
    Set oWebSvc = Nothing
End Sub

''' <summary>
''' Proceso obtiene el session id del servicio "guardar el usuario y la contrase�a" y le establece los parametros usuario y contrase�a
''' </summary>
''' <returns>session id del servicio "guardar el usuario y la contrase�a"</returns>
''' <remarks>Llamada desde: frmSolicitudDetalle/sdbgCampos_BtnClick     frmFormularios/sdbgCampos_BtnClick
''' frmDesgloseValores/ssLineas_ClickCellButton     frmSolicitudDesglose/ssLineas_ClickCellButton; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 11/12/2012</revision>

Public Function DevolverSessionIdEstablecido(ByRef oUsuarioSummit As CUsuario, ByVal sURLSvc As String) As String
    Dim strSessionId As String
    Dim strUsuario As String
    Dim strPwd As String
                            
    strSessionId = ObtenerSessionId(sURLSvc)
    strUsuario = oUsuarioSummit.Cod
    strPwd = oUsuarioSummit.PWDDes
                                 
    EstablecerSessionData strSessionId, "Usuario", strUsuario, sURLSvc
    EstablecerSessionData strSessionId, "Pwd", URLEncodeInterno(strPwd, True), sURLSvc
    EstablecerSessionData strSessionId, "idDBLogin", oUsuarioSummit.IdBDLogin, sURLSvc
    EstablecerSessionData strSessionId, "idDBSecundaria", oUsuarioSummit.IdBDConsulta, sURLSvc
    
    DevolverSessionIdEstablecido = strSessionId
End Function

