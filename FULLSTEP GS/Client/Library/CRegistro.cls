VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRegistro"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal Reserved As Long, _
     ByVal lpClass As String, ByVal dwOptions As Long, ByVal samDesired As Long, ByVal lpSecurityAttributes As Long, phkResult As Long, lpdwDisposition As Long) As Long
Private Declare Function RegQueryValueExNULL Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, _
     lpType As Long, ByVal lpData As Long, lpcbData As Long) As Long
Private Declare Function RegSetValueExString Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, _
     ByVal dwType As Long, ByVal lpData As String, ByVal cbData As Long) As Long
Private Declare Function RegSetValueExDword Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, _
     ByVal dwType As Long, ByRef lpData As Long, ByVal cbData As Long) As Long
Private Declare Function RegSetValueExBinary Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, _
     ByVal dwType As Long, ByRef lpData As Any, ByVal cbData As Long) As Long

Private Const BASE_KEY                  As String = "SOFTWARE\VB and VBA Program Settings"
Private Const REU_KEY                  As String = "Software"
Private Const ERROR_KEY_DOES_NOT_EXIST  As Long = 2
Private Const READ_CONTROL              As Long = &H20000
Private Const STANDARD_RIGHTS_READ      As Long = (READ_CONTROL)
Private Const STANDARD_RIGHTS_ALL       As Long = &H1F0000
Private Const KEY_SET_VALUE             As Long = &H2
Private Const KEY_CREATE_SUB_KEY        As Long = &H4
Private Const KEY_NOTIFY                As Long = &H10
Private Const KEY_CREATE_LINK           As Long = &H20
Private Const SYNCHRONIZE               As Long = &H100000
Private Const KEY_ALL_ACCESS            As Long = ((STANDARD_RIGHTS_ALL Or _
                                                    KEY_QUERY_VALUE Or _
                                                    KEY_SET_VALUE Or _
                                                    KEY_CREATE_SUB_KEY Or _
                                                    KEY_ENUMERATE_SUB_KEYS Or _
                                                    KEY_NOTIFY Or _
                                                    KEY_CREATE_LINK) _
                                                    And (Not SYNCHRONIZE))
Private Const KEY_READ                  As Long = ((STANDARD_RIGHTS_READ Or _
                                                    KEY_QUERY_VALUE Or _
                                                    KEY_ENUMERATE_SUB_KEYS Or _
                                                    KEY_NOTIFY) _
                                                    And (Not SYNCHRONIZE))
                                                    
Private Const REG_BINARY                As Long = 3
Private Const REG_DWORD                 As Long = 4

'*******************************************************************************
' SaveStringSetting (SUB)
'
' DESCRIPTION:
' Own version of VB's SaveStringSetting to store strings under
' HKEY_CURRENT_USER\SOFTWARE instead of
' HKEY_CURRENT_USER\Software\VB and VBA Program Settings
'
' PARAMETERS:
' (In) - sAppName - String - The first level
' (In) - sSection - String - The second level
' (In) - sKey     - String - The key in the second level
' (In) - sSetting - String - The new value for the key
'*******************************************************************************
Public Sub SaveStringSetting(ByVal sAppName As String, _
                             ByVal sInstance As String, _
                             ByVal sSection As String, _
                             ByVal sKey As String, _
                             ByVal sSetting As String, Optional ByVal sClaveRegistro As String)
    Dim lRetVal         As Long
    Dim sNewKey         As String
    Dim lDisposition    As Long
    Dim lHandle         As Long
    Dim lErrNumber      As Long
    Dim sErrDescription As String
    Dim sErrSource      As String
    
    On Error GoTo ERROR_HANDLER
    
    If sSetting = "" Then
        sSetting = " "
    End If

    If Trim(sAppName) = "" Then
        Err.Raise vbObjectError + 1000, , "AppName may not be empty"
    End If
    If Trim(sInstance) = "" Then
        Err.Raise vbObjectError + 1001, , "Instance may not be empty"
    End If
    
    If Trim(sSection) = "" Then
        Err.Raise vbObjectError + 1001, , "Section may not be empty"
    End If
    If Trim(sKey) = "" Then
        Err.Raise vbObjectError + 1002, , "Key may not be empty"
    End If
    
    sNewKey = BASE_KEY & "\" & Trim(sAppName) & "\" & sInstance & "\" & Trim(sSection)
    
    ' Create the key or open it if it already exists
    If sClaveRegistro = "HKEY_LOCAL_MACHINE" Then
        lRetVal = RegCreateKeyEx(HKEY_LOCAL_MACHINE, sNewKey, 0, vbNullString, 0, _
            KEY_ALL_ACCESS, 0, lHandle, lDisposition)
    Else
        lRetVal = RegCreateKeyEx(HKEY_CURRENT_USER, sNewKey, 0, vbNullString, 0, _
            KEY_ALL_ACCESS, 0, lHandle, lDisposition)
    End If
    
    If lRetVal <> ERROR_NONE Then
        Err.Raise vbObjectError + 2000 + lRetVal, , _
            "Could not open/create registry section"
    End If
    
    ' Set the key value
    lRetVal = RegSetValueExString(lHandle, sKey, 0, REG_SZ, sSetting, _
        Len(sSetting))
    
    If lRetVal <> ERROR_NONE Then
        Err.Raise vbObjectError + 2000 + lRetVal, , "Could not set key value"
    End If
    
TIDY_UP:
    On Error Resume Next
    
    RegCloseKey lHandle
    
    If lErrNumber <> 0 Then
        On Error GoTo 0
        
        Err.Raise lErrNumber, sErrSource, sErrDescription
    End If
Exit Sub

ERROR_HANDLER:
    lErrNumber = Err.Number
    sErrSource = Err.Source
    sErrDescription = Err.Description
    Resume TIDY_UP
End Sub

'*******************************************************************************
' GetStringSetting (FUNCTION)
'
' DESCRIPTION:
' Own version of VB's GetStringSetting to retrieve strings under
' HKEY_CURRENT_USER\SOFTWARE instead of
' HKEY_CURRENT_USER\Software\VB and VBA Program Settings
'
' PARAMETERS:
' (In) - sAppName - String - The first level
' (In) - sSection - String - The second level
' (In) - sKey     - String - The key in the second level
' (In) - sDefault - String -
'
' RETURN VALUE:
' String - The value stored in the key or sDefault if not found
'*******************************************************************************
Public Function GetStringSetting(ByVal sAppName As String, _
                                 ByVal sInstance As String, _
                                 ByVal sSection As String, _
                                 ByVal sKey As String, _
                                 Optional ByVal sDefault As String, Optional ByVal sClaveRegistro As String) As String
    Dim lRetVal         As Long
    Dim sFullKey        As String
    Dim lHandle         As Long
    Dim lType           As Long
    Dim lLength         As Long
    Dim sValue          As String
    Dim lErrNumber      As Long
    Dim sErrDescription As String
    Dim sErrSource      As String
    
    On Error GoTo ERROR_HANDLER

    If Trim(sAppName) = "" Then
        Err.Raise vbObjectError + 1000, , "AppName may not be empty"
    End If
    If Trim(sInstance) = "" Then
        Err.Raise vbObjectError + 1001, , "Instance may not be empty"
    End If
    
    If Trim(sSection) = "" Then
        Err.Raise vbObjectError + 1001, , "Section may not be empty"
    End If
    If Trim(sKey) = "" Then
        Err.Raise vbObjectError + 1002, , "Key may not be empty"
    End If
    
    sFullKey = BASE_KEY & "\" & Trim(sAppName) & "\" & sInstance & "\" & Trim(sSection)

    ' Open up the key
    If sClaveRegistro = "HKEY_LOCAL_MACHINE" Then
        lRetVal = RegOpenKeyEx(HKEY_LOCAL_MACHINE, sFullKey, 0, KEY_READ, lHandle)
    Else
        lRetVal = RegOpenKeyEx(HKEY_CURRENT_USER, sFullKey, 0, KEY_READ, lHandle)
    End If
    
    If lRetVal <> ERROR_NONE Then
        If lRetVal = ERROR_KEY_DOES_NOT_EXIST Then
            GetStringSetting = sDefault
            Exit Function
        Else
            Err.Raise vbObjectError + 2000 + lRetVal, , _
                "Could not open registry section"
        End If
    End If
    
    ' Get size and type
    lRetVal = RegQueryValueExNULL(lHandle, sKey, 0, lType, 0, lLength)
    If lRetVal <> ERROR_NONE Then
        GetStringSetting = Trim(sDefault)
        Exit Function
    End If
    
    ' Is it stored as a string in the registry?
    If lType = REG_SZ Then
        sValue = String(lLength, 0)
        
        If lLength = 0 Then
            GetStringSetting = ""
        Else
            lRetVal = RegQueryValueExString(lHandle, sKey, 0, lType, _
                sValue, lLength)
            
            If lRetVal = ERROR_NONE Then
                GetStringSetting = Trim(Left(sValue, lLength - 1))
            Else
                GetStringSetting = Trim(sDefault)
            End If
        End If
    Else
        Err.Raise vbObjectError + 2000 + lType, , _
            "Registry data not a string"
    End If
    
TIDY_UP:
    On Error Resume Next
    
    RegCloseKey lHandle
    
    If lErrNumber <> 0 Then
        On Error GoTo 0
        
        Err.Raise lErrNumber, sErrSource, sErrDescription
    End If
Exit Function

ERROR_HANDLER:
    lErrNumber = Err.Number
    sErrSource = Err.Source
    sErrDescription = Err.Description
    Resume TIDY_UP
End Function


Public Sub SaveDwordSetting(ByVal sAppName As String, _
                             ByVal sInstance As String, _
                             ByVal sSection As String, _
                             ByVal sKey As String, _
                             ByVal sSetting As Long, Optional ByVal sClaveRegistro As String)
    Dim lRetVal         As Long
    Dim sNewKey         As String
    Dim lDisposition    As Long
    Dim lHandle         As Long
    Dim lErrNumber      As Long
    Dim sErrDescription As String
    Dim sErrSource      As String
    
    On Error GoTo ERROR_HANDLER
    
    If Trim(sAppName) = "" Then
        Err.Raise vbObjectError + 1000, , "AppName may not be empty"
    End If
    
    If Trim(sKey) = "" Then
        Err.Raise vbObjectError + 1002, , "Key may not be empty"
    End If
    
    sNewKey = REU_KEY & "\" & Trim(sAppName) & "\"
    
    ' Create the key or open it if it already exists
    If sClaveRegistro = "HKEY_LOCAL_MACHINE" Then
        lRetVal = RegCreateKeyEx(HKEY_LOCAL_MACHINE, sNewKey, 0, vbNullString, 0, _
            KEY_ALL_ACCESS, 0, lHandle, lDisposition)
    Else
        lRetVal = RegCreateKeyEx(HKEY_CURRENT_USER, sNewKey, 0, vbNullString, 0, _
            KEY_ALL_ACCESS, 0, lHandle, lDisposition)
    End If
    
    If lRetVal <> ERROR_NONE Then
        Err.Raise vbObjectError + 2000 + lRetVal, , _
            "Could not open/create registry section"
    End If
    
    ' Set the key value
    lRetVal = RegSetValueExDword(lHandle, sKey, 0, REG_DWORD, sSetting, _
        Len(sSetting))
    
    If lRetVal <> ERROR_NONE Then
        Err.Raise vbObjectError + 2000 + lRetVal, , "Could not set key value"
    End If
    
TIDY_UP:
    On Error Resume Next
    
    RegCloseKey lHandle
    
    If lErrNumber <> 0 Then
        On Error GoTo 0
        
        Err.Raise lErrNumber, sErrSource, sErrDescription
    End If
Exit Sub

ERROR_HANDLER:
    lErrNumber = Err.Number
    sErrSource = Err.Source
    sErrDescription = Err.Description
    Resume TIDY_UP
End Sub
Public Sub SaveBinarySetting(ByVal sAppName As String, _
                             ByVal sInstance As String, _
                             ByVal sSection As String, _
                             ByVal sKey As String, _
                             ByVal sSetting As String, Optional ByVal sClaveRegistro As String)
    Dim lRetVal         As Long
    Dim sNewKey         As String
    Dim lDisposition    As Long
    Dim lHandle         As Long
    Dim lErrNumber      As Long
    Dim sErrDescription As String
    Dim sErrSource      As String
    
    Dim i As Integer
    Dim losBytes() As Byte
    ReDim losBytes(0 To (Len(sSetting) \ 2) - 1)
    
    For i = 0 To UBound(losBytes)
    
        losBytes(i) = Val("&h" & Mid(sSetting, (i * 2) + 1, 2))
    
    Next i
    
    On Error GoTo ERROR_HANDLER
    
    If Trim(sAppName) = "" Then
        Err.Raise vbObjectError + 1000, , "AppName may not be empty"
    End If
    
    If Trim(sKey) = "" Then
        Err.Raise vbObjectError + 1002, , "Key may not be empty"
    End If
    
    sNewKey = REU_KEY & "\" & Trim(sAppName) & "\"
    
    ' Create the key or open it if it already exists
    If sClaveRegistro = "HKEY_LOCAL_MACHINE" Then
        lRetVal = RegCreateKeyEx(HKEY_LOCAL_MACHINE, sNewKey, 0, vbNullString, 0, _
            KEY_ALL_ACCESS, 0, lHandle, lDisposition)
    Else
        lRetVal = RegCreateKeyEx(HKEY_CURRENT_USER, sNewKey, 0, vbNullString, 0, _
            KEY_ALL_ACCESS, 0, lHandle, lDisposition)
    End If
    
    If lRetVal <> ERROR_NONE Then
        Err.Raise vbObjectError + 2000 + lRetVal, , _
            "Could not open/create registry section"
    End If
    
    ' Set the key value
    lRetVal = RegSetValueExBinary(lHandle, sKey, 0, REG_BINARY, losBytes(0), _
        UBound(losBytes) + 1)
    
    If lRetVal <> ERROR_NONE Then
        Err.Raise vbObjectError + 2000 + lRetVal, , "Could not set key value"
    End If
    
TIDY_UP:
    On Error Resume Next
    
    RegCloseKey lHandle
    
    If lErrNumber <> 0 Then
        On Error GoTo 0
        
        Err.Raise lErrNumber, sErrSource, sErrDescription
    End If
Exit Sub

ERROR_HANDLER:
    lErrNumber = Err.Number
    sErrSource = Err.Source
    sErrDescription = Err.Description
    Resume TIDY_UP
End Sub

Public Sub ModificarPuertoRegistro(ByVal iPuertoLibre As Integer)
    ' Activar conexi�n por socket
    SaveDwordSetting "ORL\WinVNC3", "", "", "SocketConnect", 1
    ' Desactivar selecci�n de puerto autom�tica
    SaveDwordSetting "ORL\WinVNC3", "", "", "AutoPortSelect", 0
    ' Puerto
    SaveDwordSetting "ORL\WinVNC3", "", "", "PortNumber", iPuertoLibre
    ' Desactivar conexi�n HTTP
    SaveDwordSetting "ORL\WinVNC3", "", "", "HTTPConnect", 0
    ' Puerto HTTP
    '''SaveDwordSetting "ORL\WinVNC3", "", "", "HTTPPortNumber", 5800 + CInt(txtSala.Text)
    ' Desactivar transferencia de archivos
    SaveDwordSetting "ORL\WinVNC3", "", "", "FileTransferEnabled", 0
    SaveDwordSetting "ORL\WinVNC3", "", "", "FTUserImpersonation", 0
    ' Desactivar acciones desde el cliente
    SaveDwordSetting "ORL\WinVNC3", "", "", "InputsEnabled", 0
    ' Desactivar pregunta para conexi�n
    SaveDwordSetting "ORL\WinVNC3", "", "", "QuerySetting", 2
    
    ' Ocultar el Tray Icon que hacia que no funcionara bien en el Terminal Services
    SaveDwordSetting "ORL\WinVNC3", "", "", "DisableTrayIcon", 1, "HKEY_LOCAL_MACHINE"
    'Permitir conexiones locales
    SaveDwordSetting "ORL\WinVNC3", "", "", "AllowLoopback", 1, "HKEY_LOCAL_MACHINE"
    SaveDwordSetting "ORL\WinVNC3", "", "", "LoopbackOnly", 1, "HKEY_LOCAL_MACHINE"

End Sub

''' <summary>Devuelve las claves del registro que cuelgan de otra clave</summary>
''' <param name="sClaveRegistro">Clave principal del registro</param>
''' <param name="sKey">La clave de la que se quiere obtener las claves hijas</param>
''' <returns>Un array de strings con las claves hijas</returns>
''' <remarks>Llamada desde basUtilidades.ObtenerZonasHorarias</remarks>

Public Function GetSubKeys(ByVal sClaveRegistro As String, ByVal sKey As String) As String()
    GetSubKeys = GetSubKeysInterno(sClaveRegistro, sKey)
End Function

''' <summary>Devuelve un valor de tipo string de un clave del registro</summary>
''' <param name="sClaveRegistro">Clave principal del registro</param>
''' <param name="sKey">La clave de la que se quiere el valor</param>
''' <returns>Un string con el valor pedido de la clave pasada como par�metro</returns>
''' <remarks>Llamada desde basUtilidades.ObtenerZonasHorarias</remarks>

Public Function GetStringValue(ByVal sClaveRegistro As String, ByVal sKey As String, ByVal sValue As String) As String
    GetStringValue = GetStringValueInterno(sClaveRegistro, sKey, sValue)
End Function




