VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTimeZone"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function SystemTimeToTzSpecificLocalTime Lib "kernel32" (lpTimeZoneInformation As TIME_ZONE_INFORMATION, lpUniversalTime As SYSTEMTIME, lpLocalTime As SYSTEMTIME) As Long
Private Declare Function TzSpecificLocalTimeToSystemTime Lib "kernel32" (lpTimeZoneInformation As TIME_ZONE_INFORMATION, ByRef lpLocalTime As SYSTEMTIME, ByRef lpUniversalTime As SYSTEMTIME) As Long
Private Declare Function GetTimeZoneInformation Lib "kernel32" (lpTimeZoneInformation As TIME_ZONE_INFORMATION) As Long
Private Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Long
Private Declare Function MultiByteToWideChar Lib "kernel32" (ByVal CodePage As Long, ByVal dwFlags As Long, lpMultiByteStr As Any, ByVal cchMultiByte As Long, lpWideCharStr As Any, ByVal cchWideChar As Long) As Long

Private Type OSVERSIONINFO
        dwOSVersionInfoSize As Long
        dwMajorVersion As Long
        dwMinorVersion As Long
        dwBuildNumber As Long
        dwPlatformId As Long
        szCSDVersion As String * 128
End Type

Private Type SYSTEMTIME
   wYear As Integer
   wMonth As Integer
   wDayOfWeek As Integer
   wDay As Integer
   wHour As Integer
   wMinute As Integer
   wSecond As Integer
   wMilliseconds As Integer
End Type

Private Type TIME_ZONE_INFORMATION
   Bias As Long
   StandardName(0 To 63) As Byte
   StandardDate As SYSTEMTIME
   StandardBias As Long
   DaylightName(0 To 63) As Byte
   DaylightDate As SYSTEMTIME
   DaylightBias As Long
End Type

Private Type REGTIMEZONEINFORMATION
   Bias As Long
   StandardBias As Long
   DaylightBias As Long
   StandardDate As SYSTEMTIME
   DaylightDate As SYSTEMTIME
End Type

Public Type TIME_ZONE_INFO
    key As String
    DisplayName As String
End Type

Private Const CP_ACP = 0
Private Const MB_PRECOMPOSED = &H1

Private Const TIME_ZONE_ID_INVALID = &HFFFFFFFF
Private Const TIME_ZONE_ID_UNKNOWN = 0
Private Const TIME_ZONE_ID_STANDARD = 1
Private Const TIME_ZONE_ID_DAYLIGHT = 2
Private Const VER_PLATFORM_WIN32_NT = 2
Private Const VER_PLATFORM_WIN32_WINDOWS = 1
Private Const SKEY_NT = "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Time Zones"
Private Const SKEY_9X = "SOFTWARE\Microsoft\Windows\CurrentVersion\Time Zones"

Public Function ObtenerFechaUTC(ByVal Fecha As Date) As Variant
   Dim nRet As Long
   Dim tz As TIME_ZONE_INFORMATION
   Dim FechaUTC As Date

   nRet = GetTimeZoneInformation(tz)
   If nRet <> TIME_ZONE_ID_INVALID Then
    If Fecha > tzDate(tz.StandardDate) And Fecha > tzDate(tz.DaylightDate) Then
            FechaUTC = Fecha + (tz.Bias / 60 / 24) + (tz.StandardBias / 60 / 24) + (tz.DaylightBias / 60 / 24)
    Else
        If Fecha > tzDate(tz.StandardDate) Then
            FechaUTC = Fecha + (tz.Bias / 60 / 24) + (tz.StandardBias / 60 / 24)
        Else
            If Fecha > tzDate(tz.DaylightDate) Then
                FechaUTC = Fecha + (tz.Bias / 60 / 24) + (tz.DaylightBias / 60 / 24)
            Else
                FechaUTC = Fecha + (tz.Bias / 60 / 24)
            End If
        End If
    End If
    ObtenerFechaUTC = FechaUTC
   Else
    ObtenerFechaUTC = ""
   End If
End Function

Private Function tzDate(st As SYSTEMTIME) As Date
   Dim i As Long
   Dim n As Long
   Dim d1 As Long
   Dim d2 As Long
   
   ' This member supports two date formats. Absolute format
   ' specifies an exact date and time when standard time
   ' begins. In this form, the wYear, wMonth, wDay, wHour,
   ' wMinute, wSecond, and wMilliseconds members of the
   ' SYSTEMTIME structure are used to specify an exact date.
   If st.wYear Then
      tzDate = _
         DateSerial(st.wYear, st.wMonth, st.wDay) + _
         TimeSerial(st.wHour, st.wMinute, st.wSecond)
   
   ' Day-in-month format is specified by setting the wYear
   ' member to zero, setting the wDayOfWeek member to an
   ' appropriate weekday, and using a wDay value in the
   ' range 1 through 5 to select the correct day in the
   ' month. Using this notation, the first Sunday in April
   ' can be specified, as can the last Thursday in October
   ' (5 is equal to "the last").
   Else
      ' Get first day of month
      d1 = DateSerial(Year(Now), st.wMonth, 1)
      ' Get last day of month
      d2 = DateSerial(Year(d1), st.wMonth + 1, 0)
      
      ' Match weekday with appropriate week...
      If st.wDay = 5 Then
         ' Work backwards
         For i = d2 To d1 Step -1
            If Weekday(i) = (st.wDayOfWeek + 1) Then
               Exit For
            End If
         Next i
      Else
         ' Start at 1st and work forward
         For i = d1 To d2
            If Weekday(i) = (st.wDayOfWeek + 1) Then
               n = n + 1  'incr week value
               If n = st.wDay Then
                  Exit For
               End If
            End If
         Next i
      End If
      
      ' Got the serial date!  Just format it and
      ' add in the appropriate time.
      tzDate = i + _
         TimeSerial(st.wHour, st.wMinute, st.wSecond)
   End If
End Function

Private Function TrimNull(ByVal StrIn As String) As String
   Dim nul As Long
   '
   ' Truncate input string at first null.
   ' If no nulls, perform ordinary Trim.
   '
   nul = InStr(StrIn, vbNullChar)
   Select Case nul
      Case Is > 1
         TrimNull = Left(StrIn, nul - 1)
      Case 1
         TrimNull = ""
      Case 0
         TrimNull = Trim(StrIn)
   End Select
End Function

''' <summary>Obtiene la zona horaria del equipo</summary>
''' <returns>String con el nombre</returns>
''' <remarks>Llamada desde </remarks>

Public Function GetTimeZone() As TIME_ZONE_INFO
    Dim tzInfo As TIME_ZONE_INFORMATION
    Dim lRetVal As Long
    Dim sTimeZonesKey As String
    Dim sDisplayName As String
    Dim lPos As Long
           
    sTimeZonesKey = GetTimeZonesKey
    lRetVal = GetTimeZoneInformation(tzInfo)
    GetTimeZone.key = Trim(CStr(tzInfo.StandardName))
    lPos = InStr(1, GetTimeZone.key, Chr(0))
    GetTimeZone.key = Left(GetTimeZone.key, lPos - 1)
    sDisplayName = GetStringValueInterno("HKEY_LOCAL_MACHINE", sTimeZonesKey & "\" & CStr(tzInfo.StandardName), "Display")
    sDisplayName = Left(sDisplayName, Len(sDisplayName) - 1)
    GetTimeZone.DisplayName = sDisplayName
End Function

''' <summary>Obtiene la zona horaria del equipo</summary>
''' <returns>String con el nombre</returns>
''' <remarks>Llamada desde </remarks>

Public Function GetTimeZoneByKey(ByVal sTZKey As String) As TIME_ZONE_INFO
    Dim tzInfo As TIME_ZONE_INFORMATION
    Dim sTimeZonesKey As String
    Dim sDisplayName As String
    Dim lPos As Long
           
    sTimeZonesKey = GetTimeZonesKey
    GetTimeZoneByKey.key = sTZKey
    sDisplayName = GetStringValueInterno("HKEY_LOCAL_MACHINE", sTimeZonesKey & "\" & sTZKey, "Display")
    sDisplayName = Left(sDisplayName, Len(sDisplayName) - 1)
    GetTimeZoneByKey.DisplayName = sDisplayName
End Function

''' <summary>Obtiene todas las zonas horarias existentes</summary>
''' <returns>Un array de strings con las zonas horarias</returns>
''' <remarks>Llamada desde frmPROCESubastaConf</remarks>
''' <revision>LTG 28/05/2012</revision>

Public Function ObtenerZonasHorarias() As Dictionary 'TIME_ZONE_INFO()
    Dim aTimeZonesKeys() As String
    Dim sTimeZoneKey As String
    Dim dcTimeZones As Dictionary   'TIME_ZONE_INFO
    Dim iIndex As Integer
    Dim sTimeZonesKey As String
    Dim iDifMin As Integer
    Dim sDisplayName As String
    Dim sTZ As String
    Dim i As Integer
    Dim k As Integer
    Dim arDifMin() As Integer
    Dim arDisplayName() As String
    Dim bCambio As Boolean
        
    sTimeZonesKey = GetTimeZonesKey
    
    aTimeZonesKeys = GetSubKeysInterno("HKEY_LOCAL_MACHINE", sTimeZonesKey)
    ReDim arDifMin(0 To UBound(aTimeZonesKeys))
    ReDim arDisplayName(0 To UBound(aTimeZonesKeys))
    
    'Se utiliza una colecci�n para ordenar las TZs
    For iIndex = LBound(aTimeZonesKeys) To UBound(aTimeZonesKeys)
        sTimeZoneKey = "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Time Zones\" & aTimeZonesKeys(iIndex)
        arDisplayName(iIndex) = GetStringValueInterno("HKEY_LOCAL_MACHINE", sTimeZoneKey, "Display")
        arDisplayName(iIndex) = Left(arDisplayName(iIndex), Len(arDisplayName(iIndex)) - 1) 'El valor viene con un car�cter extra
        'Obtener la dif. en minutos con respecto a la TZ de referencia
        arDifMin(iIndex) = DifMin(arDisplayName(iIndex))
    Next
    
    'Ordenar los arrays, 1� por diferencia horaria y 2� por orden alfab�tico de DisplayName
    For i = LBound(arDifMin) To UBound(arDifMin)
        For k = i + 1 To UBound(arDifMin)
            bCambio = False
            
            If arDifMin(k) < arDifMin(i) Then
                bCambio = True
            ElseIf arDifMin(k) = arDifMin(i) Then
                If arDisplayName(k) < arDisplayName(i) Then
                    bCambio = True
                End If
            End If
            
            If bCambio Then
                iDifMin = arDifMin(i)
                arDifMin(i) = arDifMin(k)
                arDifMin(k) = iDifMin
                
                sDisplayName = arDisplayName(i)
                arDisplayName(i) = arDisplayName(k)
                arDisplayName(k) = sDisplayName
                
                sTZ = aTimeZonesKeys(i)
                aTimeZonesKeys(i) = aTimeZonesKeys(k)
                aTimeZonesKeys(k) = sTZ
            End If
        Next
    Next
    
    'Se devuelve una var. de tipo dictionary
    Set dcTimeZones = New Dictionary
    For i = LBound(aTimeZonesKeys) To UBound(aTimeZonesKeys)
        dcTimeZones.Add aTimeZonesKeys(i), arDisplayName(i)
    Next
     
    Set ObtenerZonasHorarias = dcTimeZones
End Function

''' <summary>Devuelve la dif. en minutos con respecto a la TZ de referencia de una zona horaria</summary>
''' <param name="sTZDisplayName">DisplayName de la TZ</param>
''' <returns>Min. de diferencia</returns>
''' <remarks>Llamada desde: ObtenerZonasHorarias</remarks>
''' <revision>LTG 28/05/2012</revision>

Private Function DifMin(ByVal sTZDisplayName As String) As Integer
    Dim sDifHoraria As String
    Dim sOp As String
    Dim iMin As Integer
    
    If InStr(1, sTZDisplayName, ")") > 5 Then
        sOp = Mid(sTZDisplayName, 5, 1)
        sDifHoraria = Mid(sTZDisplayName, 6, 5)
    Else
        'Las zonas horarias que tienen una dif. de 0 con respecto a la zona horaria de referencia vienen sin la dif. horaria en DisplayName
        sOp = "+"
        sDifHoraria = "00:00"
    End If
    iMin = DateDiff("n", CDate("00:00"), CDate(sDifHoraria))
    If sOp = "-" Then iMin = (-1) * iMin
    
    DifMin = iMin
End Function

''' <summary>Obtiene la clave del registro en la que se encuentran las zonas horarias</summary>
''' <returns>String con la clave del registro</returns>
''' <remarks>Llamada desde GetTimeZoneDisplayName, ObtenerZonasHorarias y GetTZData</remarks>

Private Function GetTimeZonesKey() As String
    Dim osV As OSVERSIONINFO
    
    osV.dwOSVersionInfoSize = Len(osV)
    GetVersionEx osV
    If osV.dwPlatformId = VER_PLATFORM_WIN32_NT Then
       GetTimeZonesKey = SKEY_NT
    Else
       GetTimeZonesKey = SKEY_9X
    End If
End Function

''' <summary>Convierte una fecha en una zona horaria a otra zona horaria</summary>
''' <param name="sTZ1Key">Clave principal del registro de la zona horaria de origen</param>
''' <param name="dtFechaTZ1">Fecha en la zona horaria origen</param>
''' <param name="dtHoraTZ1">Hora en la zona horaria origen</param>
''' <param name="sTZ2Key">Clave principal del registro de la zona horaria de destino</param>
''' <param name="dtFechaTZ2">Fecha en la zona horaria destino</param>
''' <param name="dtHoraTZ2">Hora en la zona horaria destino</param>
''' <remarks>Llamada desde </remarks>

Public Sub ConvertirTZ1aTZ2(ByVal sTZ1Key As String, ByVal dtFechaTZ1 As Date, ByVal dtHoraTZ1 As Date, ByVal sTZ2Key As String, ByRef dtFechaTZ2 As Date, ByRef dtHoraTZ2 As Date)
    Dim dtFechaUTC As Date
    Dim dtHoraUTC As Date
    
    If Len(sTZ1Key) > 0 And Len(sTZ2Key) > 0 Then
        ConvertirTZaUTC sTZ1Key, dtFechaTZ1, dtHoraTZ1, dtFechaUTC, dtHoraUTC
        ConvertirUTCaTZ dtFechaUTC, dtHoraUTC, sTZ2Key, dtFechaTZ2, dtHoraTZ2
    End If
End Sub

''' <summary>Convierte una fecha en una zona horaria a UTC</summary>
''' <param name="sTZOrgKey">Clave principal del registro de la zona horaria de origen</param>
''' <param name="dtFechaTZ">Fecha en la zona horaria origen</param>
''' <param name="dtHoraTZ">Hora en la zona horaria origen</param>
''' <param name="dtFechaUTC">Fecha UTC</param>
''' <param name="dtHoraUTC">Hora UTC</param>
''' <remarks>Llamada desde </remarks>

Public Sub ConvertirTZaUTC(ByVal sTZOrgKey As String, ByVal dtFechaTZ As Date, ByVal dtHoraTZ As Date, ByRef dtFechaUTC As Date, ByRef dtHoraUTC As Date)
    Dim tzOrg As TIME_ZONE_INFORMATION
    Dim lRet As Long
    Dim stOrg As SYSTEMTIME
    Dim stUTC As SYSTEMTIME
    
    If Len(sTZOrgKey) > 0 Then
        'Pasar de TZ a UTC
        tzOrg = GetTZData(sTZOrgKey)
        stOrg.wYear = Year(dtFechaTZ)
        stOrg.wMonth = Month(dtFechaTZ)
        stOrg.wDay = Day(dtFechaTZ)
        stOrg.wHour = Hour(dtHoraTZ)
        stOrg.wMinute = Minute(dtHoraTZ)
        stOrg.wSecond = Second(dtHoraTZ)
        stOrg.wMilliseconds = 0
        lRet = TzSpecificLocalTimeToSystemTime(tzOrg, stOrg, stUTC)
        
        dtFechaUTC = DateSerial(stUTC.wYear, stUTC.wMonth, stUTC.wDay)
        dtHoraUTC = TimeSerial(stUTC.wHour, stUTC.wMinute, stUTC.wSecond)
    End If
End Sub

''' <summary>Convierte una fecha UTC a una zona horaria</summary>
''' <param name="dtFechaUTC">Fecha UTC</param>
''' <param name="dtHoraUTC">Hora UTC</param>
''' <param name="sTZDestKey">Clave principal del registro de la zona horaria destino</param>
''' <param name="dtFechaTZ">Fecha en la zona horaria destino</param>
''' <param name="dtHoraTZ">Hora en la zona horaria destino</param>
''' <remarks>Llamada desde </remarks>

Public Sub ConvertirUTCaTZ(ByVal dtFechaUTC As Date, ByVal dtHoraUTC As Date, ByVal sTZDestKey As String, ByRef dtFechaTZ As Date, ByRef dtHoraTZ As Date)
    Dim tzDest As TIME_ZONE_INFORMATION
    Dim lRet As Long
    Dim stUTC As SYSTEMTIME
    Dim stDest As SYSTEMTIME
    
    If Len(sTZDestKey) > 0 Then
        ''Pasar de UTC a TZ
        stUTC.wYear = Year(dtFechaUTC)
        stUTC.wMonth = Month(dtFechaUTC)
        stUTC.wDay = Day(dtFechaUTC)
        stUTC.wHour = Hour(dtHoraUTC)
        stUTC.wMinute = Minute(dtHoraUTC)
        stUTC.wSecond = Second(dtHoraUTC)
        stUTC.wMilliseconds = 0
            
        tzDest = GetTZData(sTZDestKey)
        lRet = SystemTimeToTzSpecificLocalTime(tzDest, stUTC, stDest)
        
        dtFechaTZ = DateSerial(stDest.wYear, stDest.wMonth, stDest.wDay)
        dtHoraTZ = TimeSerial(stDest.wHour, stDest.wMinute, stDest.wSecond)
    End If
End Sub

''' <summary>Obtiene la estructura Time Zone guardada en el valor TZ de la clave pasada</summary>
''' <param name="strTZKey">Clave de la zona horaria</param>
''' <returns>Estructura TIME_ZONE_INFORMATION con los datos de la zona horaria</returns>
''' <remarks>Llamada desde ConvertirUTCaTZ y ConvertirTZaUTC</remarks>

Private Function GetTZData(ByVal strTZKey As String) As TIME_ZONE_INFORMATION
    Dim lRetVal As Long
    Dim lKey As Long
    Dim rTZI As REGTIMEZONEINFORMATION
    Dim tzInfo As TIME_ZONE_INFORMATION
    Dim sStd As String
    Dim bytDLTName(32) As Byte
    Dim bytSTDName(32) As Byte
    Dim lKeyValSize As Long
    Dim lKeyValType  As Long
    Dim sTimeZonesKey As String
    
    sTimeZonesKey = GetTimeZonesKey
        
    lRetVal = RegOpenKeyEx(HKEY_LOCAL_MACHINE, sTimeZonesKey & "\" & strTZKey, 0, KEY_QUERY_VALUE, lKey)
    If lRetVal = ERROR_NONE Then
        lRetVal = RegQueryValueExAny(lKey, "TZI", 0&, ByVal 0&, rTZI, Len(rTZI))
        If lRetVal = ERROR_NONE Then
            tzInfo.Bias = rTZI.Bias
            tzInfo.StandardBias = rTZI.StandardBias
            tzInfo.DaylightBias = rTZI.DaylightBias
            tzInfo.StandardDate = rTZI.StandardDate
            tzInfo.DaylightDate = rTZI.DaylightDate
        End If
        
        lKeyValSize = 32
        lKeyValType = REG_SZ
        lRetVal = RegQueryValueExAny(lKey, "Std", 0&, lKeyValType, bytSTDName(0), lKeyValSize)
        If lRetVal = ERROR_NONE Then
            MultiByteToWideChar CP_ACP, MB_PRECOMPOSED, bytSTDName(0), lKeyValSize, tzInfo.StandardName(0), 32
        End If
        
        lKeyValSize = 32
        lKeyValType = REG_SZ
        lRetVal = RegQueryValueExAny(lKey, "Dlt", 0&, lKeyValType, bytDLTName(0), lKeyValSize)
        If lRetVal = ERROR_NONE Then
            MultiByteToWideChar CP_ACP, MB_PRECOMPOSED, bytDLTName(0), lKeyValSize, tzInfo.DaylightName(0), 32
        End If
        
        lRetVal = RegCloseKey(lKey)
    End If
    
    GetTZData = tzInfo
End Function

