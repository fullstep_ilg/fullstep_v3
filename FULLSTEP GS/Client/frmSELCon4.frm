VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSELCon4 
   BackColor       =   &H00808000&
   Caption         =   "Form1"
   ClientHeight    =   5775
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7125
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELCon4.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5775
   ScaleWidth      =   7125
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picSepar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   0
      ScaleHeight     =   285
      ScaleWidth      =   6795
      TabIndex        =   2
      Top             =   4860
      Width           =   6855
      Begin VB.TextBox txtObj 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   5880
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   0
         Width           =   915
      End
      Begin VB.TextBox txtPres 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   3420
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   0
         Width           =   1935
      End
      Begin VB.TextBox txtPartida 
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   0
         Locked          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   0
         Width           =   2775
      End
      Begin VB.Label lblObj 
         Caption         =   "Obj.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Left            =   5460
         TabIndex        =   7
         Top             =   60
         Width           =   495
      End
      Begin VB.Label lblPres 
         Caption         =   "Pres.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Left            =   2880
         TabIndex        =   6
         Top             =   60
         Width           =   495
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2175
      TabIndex        =   1
      Top             =   5355
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   3435
      TabIndex        =   0
      Top             =   5355
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwEstrPres 
      Height          =   4665
      Left            =   0
      TabIndex        =   8
      Top             =   240
      Width           =   6870
      _ExtentX        =   12118
      _ExtentY        =   8229
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   960
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCon4.frx":0CB2
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCon4.frx":177C
            Key             =   "PRES1"
            Object.Tag             =   "PRES1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCon4.frx":1BCE
            Key             =   "PRES2"
            Object.Tag             =   "PRES2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCon4.frx":2020
            Key             =   "PRES3"
            Object.Tag             =   "PRES3"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCon4.frx":2472
            Key             =   "PRES4"
            Object.Tag             =   "PRES4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCon4.frx":28C4
            Key             =   "PRESBAJALOGICA"
            Object.Tag             =   "PRESBAJALOGICA"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSELCon4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private iNivelMaximo As Integer

Public oPresupuestos As CPresConceptos4Nivel1
' Variables para interactuar con otros forms

Public sOrigen As String
       
Public oPres4Con1Seleccionado As CPresConcep4Nivel1
Public oPres4Con2Seleccionado As CPresConcep4Nivel2
Public oPres4Con3Seleccionado As CPresConcep4Nivel3
Public oPres4Con4Seleccionado As CPresConcep4Nivel4

Private sIdiSel As String
Private sIdiDet As String

' Unidad organizativa seleccionada
Public m_sUON1Sel As String
Public m_sUON2Sel As String
Public m_sUON3Sel As String

Public m_bVerBajaLog As Boolean

' Variable de control de flujo
Public Accion As AccionesSummit

Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

    Set nodx = tvwEstrPres.selectedItem
    
    If Not nodx Is Nothing Then
        
        Screen.MousePointer = vbHourglass
        Select Case Left(nodx.Tag, 5)
        
            Case "PRES1"
                
                scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx)))
                Set oPres4Con1Seleccionado = oPresupuestos.Item(scod1)
                
            Case "PRES2"
                
                scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx.Parent)))
                scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx)))
                Set oPres4Con2Seleccionado = oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2)
                                
            Case "PRES3"
                
                scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx.Parent)))
                scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(DevolverCod(nodx)))
                Set oPres4Con3Seleccionado = oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3)
                
            Case "PRES4"
                
                scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx.Parent.Parent)))
                scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(DevolverCod(nodx.Parent)))
                scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44 - Len(DevolverCod(nodx)))
                Set oPres4Con4Seleccionado = oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).PresConceptos4Nivel4.Item(scod1 & scod2 & scod3 & scod4)
                
        End Select
        
        Screen.MousePointer = vbNormal
        
        Select Case sOrigen
    
            Case "frmLstPRESPorCon4"
                frmLstPRESPorCon4.MostrarParConSeleccionada
            
            Case "frmInfAhorroApliConcep4"
                frmInfAhorroApliConcep4.MostrarPresSeleccionado
            
            Case "frmInfAhorroNegConcep4Desde"
                frmInfAhorroNegConcep4Desde.MostrarPresSeleccionado
                
            Case "frmInfAhorroNegConcep4Reu"
                frmInfAhorroNegConcep4Reu.MostrarPresSeleccionado
            
            Case "frmLstINFAhorroNegfrmInfAhorroNegConcep4Desde"
                frmInfAhorroNegConcep4Desde.ofrmLstAhorroNeg.MostrarPresSeleccionado4
                
            Case "frmLstINFAhorroNegfrmInfAhorroNegConcep4Reu"
                frmInfAhorroNegConcep4Reu.ofrmLstAhorroNeg.MostrarPresSeleccionado4
                
            Case "frmLstINFAhorroNegA4B1C6D1"
                frmListados.ofrmLstNegConcep4Reu.MostrarPresSeleccionado4
                
            Case "frmLstINFAhorroNegA4B1C6D2"
                frmListados.ofrmLstNegConcep4Desde.MostrarPresSeleccionado4
            
            Case "frmLstINFAhorroAplA4B2C7"
                frmListados.ofrmLstApliConcep4.MostrarPresSeleccionado4
            
            Case "frmLstINFAhorroAplfrmInfAhorroApliConcep4"
                frmInfAhorroApliConcep4.ofrmLstApliProy.MostrarPresSeleccionado4
    End Select
    
    Unload Me
    
    End If

End Sub

Private Sub cmdCancelar_Click()
    
    
    Unload Me
    
End Sub

Private Sub Form_Activate()
    tvwEstrPres_NodeClick tvwEstrPres.selectedItem
    If Me.Visible Then tvwEstrPres.SetFocus
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELCON4, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        sIdiSel = Ador(0).Value
        Ador.MoveNext
        sIdiDet = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblObj.caption = Ador(0).Value
        Ador.MoveNext
        lblPres.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    Me.caption = sIdiSel & " " & gParametrosGenerales.gsSingPres4
        
    tvwEstrPres.Top = 315
    tvwEstrPres.Height = 4425
    
    DoEvents

    GenerarEstructuraPresupuestos False
    
End Sub

Private Sub GenerarEstructuraPresupuestos(ByVal bOrdenadoPorDen As Boolean)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oParCon1 As CPresConcep4Nivel1
Dim oParCon2 As CPresConcep4Nivel2
Dim oParCon3 As CPresConcep4Nivel3
Dim oParCon4 As CPresConcep4Nivel4

Dim nodx As MSComctlLib.node

iNivelMaximo = 0

tvwEstrPres.Nodes.clear

Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsplurpres4, "Raiz")
nodx.Tag = "Raiz "

nodx.Expanded = True
    
    Set oPresupuestos = oFSGSRaiz.Generar_CPresConceptos4Nivel1
   
    oPresupuestos.GenerarEstructuraPresupuestosConceptos4 bOrdenadoPorDen, m_sUON1Sel, m_sUON2Sel, m_sUON3Sel
    
    Select Case gParametrosGenerales.giNEPP
        
        Case 1
                
                For Each oParCon1 In oPresupuestos
                    If Not ((Not m_bVerBajaLog) And oParCon1.BajaLog) Then
                        scod1 = oParCon1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oParCon1.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oParCon1.Cod & " - " & oParCon1.Den, "PRES1")
                        nodx.Tag = "PRES1" & oParCon1.Cod
                        If oParCon1.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                        End If
                        
                        If iNivelMaximo < 1 Then
                            iNivelMaximo = 1
                        End If
                    End If
                Next
        
        Case 2
            
            For Each oParCon1 In oPresupuestos
                If Not ((Not m_bVerBajaLog) And oParCon1.BajaLog) Then
                    scod1 = oParCon1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oParCon1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oParCon1.Cod & " - " & oParCon1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oParCon1.Cod
                    If oParCon1.BajaLog Then
                        nodx.Image = "PRESBAJALOGICA"
                        nodx.BackColor = &H8000000F 'color gris
                    End If
                End If
                    
                For Each oParCon2 In oParCon1.PresConceptos4Nivel2
                    If Not ((Not m_bVerBajaLog) And oParCon2.BajaLog) Then
                        scod2 = oParCon2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oParCon2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oParCon2.Cod & " - " & oParCon2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oParCon2.Cod
                        If oParCon2.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                        End If

                        If iNivelMaximo < 2 Then
                            iNivelMaximo = 2
                        End If
                    End If
                Next
            Next
        
        Case 3
            
            For Each oParCon1 In oPresupuestos
                If Not ((Not m_bVerBajaLog) And oParCon1.BajaLog) Then
                    scod1 = oParCon1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oParCon1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oParCon1.Cod & " - " & oParCon1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oParCon1.Cod
                    If oParCon1.BajaLog Then
                        nodx.Image = "PRESBAJALOGICA"
                        nodx.BackColor = &H8000000F 'color gris
                    End If
                End If
                
                For Each oParCon2 In oParCon1.PresConceptos4Nivel2
                    If Not ((Not m_bVerBajaLog) And oParCon2.BajaLog) Then
                        scod2 = oParCon2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oParCon2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oParCon2.Cod & " - " & oParCon2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oParCon2.Cod
                        If oParCon2.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                        End If
                    End If
                    
                    For Each oParCon3 In oParCon2.PresConceptos4Nivel3
                        If Not ((Not m_bVerBajaLog) And oParCon3.BajaLog) Then
                            scod3 = oParCon3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oParCon3.Cod))
                            Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oParCon3.Cod & " - " & oParCon3.Den, "PRES3")
                            nodx.Tag = "PRES3" & oParCon3.Cod
                            If oParCon3.BajaLog Then
                                nodx.Image = "PRESBAJALOGICA"
                                nodx.BackColor = &H8000000F 'color gris
                            End If

                            If iNivelMaximo < 3 Then
                                iNivelMaximo = 3
                            End If
                        End If
                    Next
                Next
        Next
            
        Case 4
            
            For Each oParCon1 In oPresupuestos
                If Not ((Not m_bVerBajaLog) And oParCon1.BajaLog) Then
                    scod1 = oParCon1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oParCon1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oParCon1.Cod & " - " & oParCon1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oParCon1.Cod
                    If oParCon1.BajaLog Then
                        nodx.Image = "PRESBAJALOGICA"
                        nodx.BackColor = &H8000000F 'color gris
                    End If
                End If

                For Each oParCon2 In oParCon1.PresConceptos4Nivel2
                    If Not ((Not m_bVerBajaLog) And oParCon2.BajaLog) Then
                        scod2 = oParCon2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oParCon2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oParCon2.Cod & " - " & oParCon2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oParCon2.Cod
                        If oParCon2.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                        End If
                    End If
                                            
                    For Each oParCon3 In oParCon2.PresConceptos4Nivel3
                        If Not ((Not m_bVerBajaLog) And oParCon3.BajaLog) Then
                            scod3 = oParCon3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oParCon3.Cod))
                            Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oParCon3.Cod & " - " & oParCon3.Den, "PRES3")
                            nodx.Tag = "PRES3" & oParCon3.Cod
                            If oParCon3.BajaLog Then
                                nodx.Image = "PRESBAJALOGICA"
                                nodx.BackColor = &H8000000F 'color gris
                            End If
                        End If

                        For Each oParCon4 In oParCon3.PresConceptos4Nivel4
                            If Not ((Not m_bVerBajaLog) And oParCon4.BajaLog) Then
                                scod4 = oParCon4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44 - Len(oParCon4.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oParCon4.Cod & " - " & oParCon4.Den, "PRES4")
                                nodx.Tag = "PRES4" & oParCon4.Cod
                                If oParCon4.BajaLog Then
                                    nodx.Image = "PRESBAJALOGICA"
                                    nodx.BackColor = &H8000000F 'color gris
                                End If

                                If iNivelMaximo < 4 Then
                                    iNivelMaximo = 4
                                End If
                            End If
                        Next
                    Next
                Next
            Next
    End Select


    Set oParCon1 = Nothing
    Set oParCon2 = Nothing
    Set oParCon3 = Nothing
    Set oParCon4 = Nothing
    
        
Exit Sub

Error:
    Set nodx = Nothing
    Resume Next
End Sub

    
Private Sub Form_Resize()
On Error Resume Next

    If Me.Width > 250 Then tvwEstrPres.Width = Me.Width - 250
    If Me.Height >= 1600 Then tvwEstrPres.Height = Me.Height - 1600
    picSepar.Top = tvwEstrPres.Top + tvwEstrPres.Height
    tvwEstrPres.Left = 75
    picSepar.Left = tvwEstrPres.Left
    picSepar.Width = tvwEstrPres.Width

    cmdAceptar.Top = tvwEstrPres.Top + tvwEstrPres.Height + picSepar.Height + 50
    cmdCancelar.Top = cmdAceptar.Top

    cmdAceptar.Left = tvwEstrPres.Left + tvwEstrPres.Width / 2 - 1100
    cmdCancelar.Left = tvwEstrPres.Left + tvwEstrPres.Width / 2
End Sub


Private Sub Form_Unload(Cancel As Integer)
    m_sUON1Sel = ""
    m_sUON2Sel = ""
    m_sUON3Sel = ""
        
    Set oPresupuestos = Nothing
    Set oPres4Con1Seleccionado = Nothing
    Set oPres4Con2Seleccionado = Nothing
    Set oPres4Con3Seleccionado = Nothing
    Set oPres4Con4Seleccionado = Nothing
    
End Sub



Private Sub tvwEstrPres_Collapse(ByVal node As MSComctlLib.node)
    
    Set oPres4Con1Seleccionado = Nothing
    Set oPres4Con2Seleccionado = Nothing
    Set oPres4Con3Seleccionado = Nothing
    
    
    
    MostrarDatosBarraInf
    
End Sub

Private Sub tvwEstrPres_NodeClick(ByVal node As MSComctlLib.node)
    
    Set oPres4Con1Seleccionado = Nothing
    Set oPres4Con2Seleccionado = Nothing
    Set oPres4Con3Seleccionado = Nothing
    
    MostrarDatosBarraInf
    
End Sub
Public Sub MostrarDatosBarraInf()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim vImporte As Variant
Dim vObjetivo As Variant

    Set nodx = tvwEstrPres.selectedItem
    
    If Not nodx Is Nothing Then
        
        Select Case Left(nodx.Tag, 5)
        
            Case "Raiz "
            
                txtPartida = ""
                txtPres = ""
                txtObj = ""
                
            Case "PRES1"
                scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx)))
                txtPartida = DevolverCod(nodx) & " (" & oPresupuestos.Item(scod1).Den & ")"
                vImporte = oPresupuestos.Item(scod1).importe
                vObjetivo = oPresupuestos.Item(scod1).Objetivo
                
                If Not IsMissing(vImporte) And Not IsNull(vImporte) Then
                        txtPres = Format(DblToStr(vImporte), "Standard")
                Else
                    txtPres = ""
                End If
                
                If Not IsMissing(vObjetivo) And Not IsNull(vObjetivo) Then
                    txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
                Else
                    txtObj = ""
                End If
            
            Case "PRES2"
                
                scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx.Parent)))
                scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx)))
                
                txtPartida = DevolverCod(nodx.Parent) & " (" & oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).Den & ")"
                vImporte = oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).importe
                vObjetivo = oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).Objetivo
                
                If Not IsMissing(vImporte) And Not IsNull(vImporte) Then
                    txtPres = Format(DblToStr(vImporte), "Standard")
                Else
                    txtPres = ""
                End If
                If Not IsMissing(vObjetivo) And Not IsNull(vObjetivo) Then
                    txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
                Else
                    txtObj = ""
                End If
                
            Case "PRES3"
                
                scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx.Parent)))
                scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(DevolverCod(nodx)))
                txtPartida = DevolverCod(nodx.Parent.Parent) & " (" & oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).Den & ")"
                vImporte = oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).importe
                vObjetivo = oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).Objetivo
                    
                If Not IsMissing(vImporte) And Not IsNull(vImporte) Then
                    txtPres = Format(DblToStr(vImporte), "Standard")
                Else
                    txtPres = ""
                End If
                
                If Not IsMissing(vObjetivo) And Not IsNull(vObjetivo) Then
                    txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
                Else
                    txtObj = ""
                End If
                
                
            Case "PRES4"
                
                scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx.Parent.Parent)))
                scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(DevolverCod(nodx.Parent)))
                scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44 - Len(DevolverCod(nodx)))
                txtPartida = DevolverCod(nodx.Parent.Parent.Parent) & " (" & oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).PresConceptos4Nivel4.Item(scod1 & scod2 & scod3 & scod4).Den & ")"
                vImporte = oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).PresConceptos4Nivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                vObjetivo = oPresupuestos.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).PresConceptos4Nivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo

                
                If Not IsMissing(vImporte) And Not IsNull(vImporte) Then
                    txtPres = Format(DblToStr(vImporte), "Standard")
                Else
                    txtPres = ""
                End If
                
                If Not IsMissing(vObjetivo) And Not IsNull(vObjetivo) Then
                    txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
                Else
                    txtObj = ""
                End If
                
        End Select
    
    End If

End Sub
Private Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

If node Is Nothing Then Exit Function

Select Case Left(node.Tag, 5)

Case "PRES1"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 5)
    
Case "PRES2"
    
        DevolverCod = Right(node.Tag, Len(node.Tag) - 5)
    
Case "PRES3"
    
        DevolverCod = Right(node.Tag, Len(node.Tag) - 5)

Case "PRES4"
    
        DevolverCod = Right(node.Tag, Len(node.Tag) - 5)

End Select

End Function



