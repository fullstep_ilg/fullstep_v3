VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDistUON 
   Caption         =   "Distribuci�n en Unidades Organizativas"
   ClientHeight    =   4950
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6780
   Icon            =   "frmDistUON.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4950
   ScaleWidth      =   6780
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picPorcen 
      Height          =   495
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   6720
      TabIndex        =   3
      Top             =   3960
      Width           =   6780
      Begin VB.TextBox txtPorcentaje 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1500
         TabIndex        =   5
         Top             =   90
         Width           =   1200
      End
      Begin MSComctlLib.Slider Slider1 
         Height          =   315
         Left            =   3000
         TabIndex        =   4
         Top             =   60
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         Max             =   100
      End
      Begin VB.Label lblPorcen 
         Caption         =   "Porcentaje:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   6
         Top             =   120
         Width           =   1300
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   3330
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   4560
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2220
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   4560
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwestrorg 
      Height          =   3500
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   6800
      _ExtentX        =   11986
      _ExtentY        =   6165
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   4400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDistUON.frx":014A
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDistUON.frx":01E9
            Key             =   "UON3A"
            Object.Tag             =   "UON3A"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDistUON.frx":02A7
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDistUON.frx":0357
            Key             =   "UON2A"
            Object.Tag             =   "UON2A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDistUON.frx":0418
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDistUON.frx":04C8
            Key             =   "UON1A"
            Object.Tag             =   "UON1A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDistUON.frx":0588
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblSinDistPorcentaje 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4800
      TabIndex        =   10
      Top             =   30
      Width           =   1200
   End
   Begin VB.Label lblSinDist 
      Caption         =   "Sin distribuir:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3000
      TabIndex        =   9
      Top             =   60
      Width           =   1600
   End
   Begin VB.Label lblPorcentaje 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1500
      TabIndex        =   8
      Top             =   30
      Width           =   1200
   End
   Begin VB.Label lblDist 
      Caption         =   "Distribu�do:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   60
      TabIndex        =   7
      Top             =   60
      Width           =   1300
   End
End
Attribute VB_Name = "frmDistUON"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Unidades organizativas
Private m_oUnidadesOrgN1 As CUnidadesOrgNivel1
Private m_oUnidadesOrgN2 As CUnidadesOrgNivel2
Private m_oUnidadesOrgN3 As CUnidadesOrgNivel3

'Restricciones
Private m_bRuo As Boolean
'permiso distribuci�n compras
Private m_bRestrDistrUsu As Boolean
Private m_bRestrDistrPerf As Boolean

Private m_bRespetarPorcen As Boolean
Private m_bAceptar As Boolean

Private m_stexto As String

Public g_iCol As Integer
Public g_iLstGrupoItem As Integer
Public g_sOrigen As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>


Private Sub ConfigurarSeguridad()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUOPers)) Is Nothing) Then
        m_bRuo = True
    End If
    
        
    'Restriccion a la distribuci�n de la compra
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUOPers)) Is Nothing) Then
        m_bRestrDistrUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDisCompraUONPerf)) Is Nothing) Then
        m_bRestrDistrPerf = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3
Dim nodx As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next

    Screen.MousePointer = vbHourglass

    tvwestrorg.Nodes.clear
    Dim lIdPerfil As Integer
    If Not oUsuarioSummit.Perfil Is Nothing Then
        lIdPerfil = oUsuarioSummit.Perfil.Id
    End If
    Select Case gParametrosGenerales.giNEO

        Case 1
                m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, , , basOptimizacion.gCodDepUsuario, m_bRestrDistrUsu, , , , , bOrdenadoPorDen ', , m_bRestrDistrPerf, lIdPerfil
        Case 2
               m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , basOptimizacion.gCodDepUsuario, m_bRuo, , , , , bOrdenadoPorDen ', , m_bRestrDistrPerf, lIdPerfil
               m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , basOptimizacion.gCodDepUsuario, m_bRuo, , , , , bOrdenadoPorDen ', , m_bRestrDistrPerf, lIdPerfil
        Case 3
               
               m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , m_bRestrDistrUsu, , , , , bOrdenadoPorDen, , , , , , , , , , , , , , , m_bRestrDistrPerf, lIdPerfil
               m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , m_bRestrDistrUsu, , , , , bOrdenadoPorDen, , , , , , , , , , , , , , , , m_bRestrDistrPerf, lIdPerfil
               m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , m_bRestrDistrUsu, , , , , bOrdenadoPorDen, , , , , , , , , , , , , , , , , m_bRestrDistrPerf, lIdPerfil
              
    End Select




    lblPorcentaje.caption = "0 %"
    lblSinDistPorcentaje.caption = "100 %"


   '************************************************************
    'Generamos la estructura arborea

    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True

    For Each oUON1 In m_oUnidadesOrgN1

        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
        If Not oUON1.activa Then
            bloquearNodo nodx
        End If
    Next

    For Each oUON2 In m_oUnidadesOrgN2

        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON1" & scod1 & "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
        If Not oUON2.activa Then
            bloquearNodo nodx
        End If
    Next

    For Each oUON3 In m_oUnidadesOrgN3

        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1 & "UON2" & scod2, tvwChild, "UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
        If Not oUON3.activa Then
            bloquearNodo nodx
        End If
    Next


    If g_sOrigen = "PROCEM" Then
        If Not frmPROCE Is Nothing Then
            If frmPROCE.m_iConfigAnterior = TipoDefinicionDatoProceso.EnProceso Then
            
                Dim dDistPorcen As Double
                dDistPorcen = frmPROCE.g_oProcesoSeleccionado.CargarDistribucion()
               
                lblPorcentaje.caption = CStr(CDec(dDistPorcen * 100)) & " %"
                lblSinDistPorcentaje.caption = CStr(100 - CDec(dDistPorcen * 100)) & " %"
               
                Dim oDistNivel1 As CDistItemNivel1
                Dim oDistNivel2 As CDistItemNivel2
                Dim oDistNivel3 As CDistItemNivel3
        
            
                'Distribuciones
                For Each oDistNivel1 In frmPROCE.g_oProcesoSeleccionado.DistsNivel1
                    Set nodx = Nothing
                    scod1 = oDistNivel1.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel1.CodUON1))
                    Set nodx = tvwestrorg.Nodes("UON1" & scod1)
                    nodx.Text = nodx.Text & " (" & CDec(oDistNivel1.Porcentaje * 100) & "%)"
                    If nodx.Image = "UON1" Then
                        nodx.Image = "UON1A"
                    Else
                        nodx.Image = "UON1B"
                    End If
                Next
                
                For Each oDistNivel2 In frmPROCE.g_oProcesoSeleccionado.DistsNivel2
                    Set nodx = Nothing
                    scod1 = oDistNivel2.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel2.CodUON1))
                    scod2 = oDistNivel2.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel2.CodUON2))
                    Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2)
                    nodx.Text = nodx.Text & " (" & CDec(oDistNivel2.Porcentaje * 100) & "%)"
                    If nodx.Image = "UON2" Then
                        nodx.Image = "UON2A"
                    Else
                        nodx.Image = "UON2B"
                    End If
                    
                    nodx.Parent.Expanded = True
                Next
                
                For Each oDistNivel3 In frmPROCE.g_oProcesoSeleccionado.DistsNivel3
                    Set nodx = Nothing
                    scod1 = oDistNivel3.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel3.CodUON1))
                    scod2 = oDistNivel3.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel3.CodUON2))
                    scod3 = oDistNivel3.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDistNivel3.CodUON3))
                    Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3)
                    nodx.Text = nodx.Text & " (" & CDec(oDistNivel3.Porcentaje * 100) & "%)"
                    If nodx.Image = "UON3" Then
                        nodx.Image = "UON3A"
                    Else
                        nodx.Image = "UON3B"
                    End If
                    nodx.Parent.Parent.Expanded = True
                    nodx.Parent.Expanded = True
                Next
        
        
            
            End If
        End If
    End If






    Set nodx = Nothing
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "GenerarEstructuraOrg", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim nod1 As MSComctlLib.node
    Dim nod2 As MSComctlLib.node
    Dim nod3 As MSComctlLib.node
    Dim nod4 As MSComctlLib.node
    Dim nodx As MSComctlLib.node
    Dim dDistPorcen As Double
    Dim oDistsNivel1 As CDistItemsNivel1
    Dim oDistsNivel2 As CDistItemsNivel2
    Dim oDistsNivel3 As CDistItemsNivel3
    Dim oItem As CItem
    Dim oGrupo As CGrupo
    Dim iDef As Integer
    Dim i As Integer
    Dim oItems As CItems
    Dim dPorcen As Double
    Dim sCad1 As Variant
    Dim sCad2 As Variant
    Dim sCad3 As Variant
    Dim NoPasarNulos As Boolean

    'Controlar la selecci�n
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If CDbl(Left(lblPorcentaje.caption, Len(lblPorcentaje.caption) - 1)) = 0 Then
        Screen.MousePointer = vbNormal
        oMensajes.ProceDistObligatoria
        Exit Sub
    End If
   
    Screen.MousePointer = vbHourglass
    
    Set oDistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
    Set oDistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
    Set oDistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
     
    dDistPorcen = 0
     
    Set nodx = tvwestrorg.Nodes(1)
    
     If nodx.Children = 0 Then
          Screen.MousePointer = vbNormal
          Exit Sub
     End If
    
     dPorcen = 0
    
     Set oItem = oFSGSRaiz.Generar_CItem
     
     Set nod1 = nodx.Child
     While Not nod1 Is Nothing
         
     ' No distribu�do
     If nod1.Image = "UON1" Then
        Set nod2 = nod1.Child
        While Not nod2 Is Nothing
            
            ' No distribu�do
            If nod2.Image = "UON2" Then
                
                    Set nod3 = nod2.Child
                    While Not nod3 Is Nothing
                        If nod3.Image = "UON3A" Then
                            If DevolverPorcentaje(nod3.Text) > dPorcen Then
                                sCad1 = Right(nod3.Parent.Parent.Tag, Len(nod3.Parent.Parent.Tag) - 4)
                                sCad2 = Right(nod3.Parent.Tag, Len(nod3.Parent.Tag) - 4)
                                sCad3 = Right(nod3.Tag, Len(nod3.Tag) - 4)
                                dPorcen = DevolverPorcentaje(nod3.Text)
                            End If
                            
                            oDistsNivel3.Add oItem, Right(nod3.Parent.Parent.Tag, Len(nod3.Parent.Parent.Tag) - 4), Right(nod3.Parent.Tag, Len(nod3.Parent.Tag) - 4), Right(nod3.Tag, Len(nod3.Tag) - 4), DevolverPorcentaje(nod3.Text)
                            dDistPorcen = dDistPorcen + DevolverPorcentaje(nod3.Text)
                        End If
                        Set nod3 = nod3.Next
                    Wend
                    
                Else
                    ' Si est� distribu�do UON2
                    If DevolverPorcentaje(nod2.Text) > dPorcen Then
                        sCad1 = Right(nod2.Parent.Tag, Len(nod2.Parent.Tag) - 4)
                        sCad2 = Right(nod2.Tag, Len(nod2.Tag) - 4)
                        sCad3 = Null
                        dPorcen = DevolverPorcentaje(nod2.Text)
                    End If
                    
                    oDistsNivel2.Add oItem, Right(nod2.Parent.Tag, Len(nod2.Parent.Tag) - 4), Right(nod2.Tag, Len(nod2.Tag) - 4), DevolverPorcentaje(nod2.Text)
                    dDistPorcen = dDistPorcen + DevolverPorcentaje(nod2.Text)
                End If
                
                Set nod2 = nod2.Next
            Wend
            
        Else
            ' Si est� distribu�do UON1
            If DevolverPorcentaje(nod1.Text) > dPorcen Then
                sCad1 = Right(nod1.Tag, Len(nod1.Tag) - 4)
                sCad2 = Null
                sCad3 = Null
                dPorcen = DevolverPorcentaje(nod1.Text)
            End If
        
            oDistsNivel1.Add oItem, Right(nod1.Tag, Len(nod1.Tag) - 4), DevolverPorcentaje(nod1.Text)
            dDistPorcen = dDistPorcen + DevolverPorcentaje(nod1.Text)
        End If
        
        Set nod1 = nod1.Next

    Wend
    
    If dDistPorcen > 1 Then
        oMensajes.NoValido lblPorcen.caption & " " & CDec(dDistPorcen * 100)
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
            
    Screen.MousePointer = vbHourglass
    
    Dim sUonMostrar As String
    Dim sIdItem As String
    Dim ErrorDistribucion As Boolean
    
    Select Case g_sOrigen
    
    
            Case "PROCEM" 'Modificaci�n de la configuraci�n de un proceso existente
    
                    If g_iCol = 2 Then 'Pasa a PROCESO
                                            
                        ''Comprobaciones de que los items del proceso son validos para la nueva distribucion
                        If Not (frmPROCE.ComprobarValidezItemsConNuevaDistribucion(oDistsNivel1, oDistsNivel2, oDistsNivel3, sUonMostrar, sIdItem)) Then
                            oMensajes.ImposibleAnyadirItemADistribucion sUonMostrar, sIdItem, TipoDefinicionDatoProceso.EnProceso, False
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                            
                        
                    
                        iDef = frmPROCE.g_oProcesoSeleccionado.DefDistribUON
                        frmPROCE.g_oProcesoSeleccionado.DefDistribUON = EnProceso
                        If dDistPorcen = 0 Then
                            frmPROCE.g_oProcesoSeleccionado.HayDistribucionUON = False
                        Else
                            frmPROCE.g_oProcesoSeleccionado.HayDistribucionUON = True
                        End If
                        Set frmPROCE.g_oProcesoSeleccionado.DistsNivel1 = oDistsNivel1
                        Set frmPROCE.g_oProcesoSeleccionado.DistsNivel2 = oDistsNivel2
                        Set frmPROCE.g_oProcesoSeleccionado.DistsNivel3 = oDistsNivel3
                                                
                        teserror = frmPROCE.g_oProcesoSeleccionado.ModificarConfiguracionProceso(Distribucion)
                        If teserror.NumError <> TESnoerror Then
                            Screen.MousePointer = vbNormal
                            basErrores.TratarError teserror
                            frmPROCE.g_oProcesoSeleccionado.DefDistribUON = iDef
                            frmPROCE.MostrarConfiguracion
                            Unload Me
                            Set oDistsNivel1 = Nothing
                            Set oDistsNivel2 = Nothing
                            Set oDistsNivel3 = Nothing
                            Set oGrupo = Nothing
                            Set oItem = Nothing
                            Exit Sub
                        End If
                        frmPROCE.sdbcGrupo.Columns(0).Value = ""
                        For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                            If oGrupo.DefDistribUON Then
                                oGrupo.DefDistribUON = False
                                oGrupo.HayDistribucionUON = False
                                '�Modificar en BD los grupos?
                            End If
                        Next
                        frmPROCE.g_bAceptarUON = True
                    End If
                    If g_iCol = 3 Then 'Pasa a GRUPO
                                    
                        ''Comprobaciones de que los items del proceso son validos para la nueva distribucion
                        Set frmPROCE.g_oGrupoSeleccionado = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems(g_iLstGrupoItem).Tag))
                        If Not (frmPROCE.ComprobarValidezItemsConNuevaDistribucion(oDistsNivel1, oDistsNivel2, oDistsNivel3, sUonMostrar, sIdItem, frmPROCEGrupos.lstGrupos.ListItems(g_iLstGrupoItem).Tag)) Then
                            oMensajes.ImposibleAnyadirItemADistribucion sUonMostrar, sIdItem, TipoDefinicionDatoProceso.EnGrupo, False
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                            
                        

                        frmPROCE.g_oProcesoSeleccionado.DefDistribUON = EnGrupo
                        Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems(g_iLstGrupoItem).Tag))
                        If oGrupo Is Nothing Then
                            Set oDistsNivel1 = Nothing
                            Set oDistsNivel2 = Nothing
                            Set oDistsNivel3 = Nothing
                            Set oGrupo = Nothing
                            Set oItem = Nothing
                            Exit Sub
                        End If
                        
                        oGrupo.DefDistribUON = True
                        If dDistPorcen = 0 Then
                            oGrupo.HayDistribucionUON = False
                        Else
                            oGrupo.HayDistribucionUON = True
                        End If
                        Set oGrupo.DistsNivel1 = oDistsNivel1
                        Set oGrupo.DistsNivel2 = oDistsNivel2
                        Set oGrupo.DistsNivel3 = oDistsNivel3
                        frmPROCE.g_bAceptarUON = True
                    End If
    
            Case "PROCEN_UONP" 'Nuevo proceso, UON definido en proceso
                    Set frmPROCE.g_oProcesoSeleccionado.DistsNivel1 = oDistsNivel1
                    Set frmPROCE.g_oProcesoSeleccionado.DistsNivel2 = oDistsNivel2
                    Set frmPROCE.g_oProcesoSeleccionado.DistsNivel3 = oDistsNivel3
                    frmPROCE.g_oProcesoSeleccionado.HayDistribucionUON = True
            
            Case "PROCEN_UONG" 'Nuevo proceso, UON definido en grupo
                    Set frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).DistsNivel1 = oDistsNivel1
                    Set frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).DistsNivel2 = oDistsNivel2
                    Set frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).DistsNivel3 = oDistsNivel3
                    frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).HayDistribucionUON = True
                    
             Case "PROCEN_UONG2"
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefDistribUON Then
                            Set oGrupo.DistsNivel1 = oDistsNivel1
                            Set oGrupo.DistsNivel2 = oDistsNivel2
                            Set oGrupo.DistsNivel3 = oDistsNivel3
                            oGrupo.HayDistribucionUON = True
                        End If
                    Next
                    
            Case "GRUPON_UONG" 'Nuevo grupo, UON definido en grupo
                    Set frmPROCE.g_oGrupoSeleccionado.DistsNivel1 = oDistsNivel1
                    Set frmPROCE.g_oGrupoSeleccionado.DistsNivel2 = oDistsNivel2
                    Set frmPROCE.g_oGrupoSeleccionado.DistsNivel3 = oDistsNivel3
                    frmPROCE.g_oGrupoSeleccionado.HayDistribucionUON = True
                    
            Case "PROCE_MODIFITEMS" 'Modificaci�n de m�ltiples �tems
            
                    'Antes de Actualizar comprobamos que es valida la distribucion en caso de tener estos parametros activados
                        
                     

                    Dim adoRecordset As ADODB.Recordset
                    Dim sUON As String
                    'Dim sUonMostrar As String
                    Dim UON1tmp As String
                    Dim UON2tmp As String
                    Dim UON3tmp As String
                    Dim bEsValido As Boolean
                    bEsValido = False
                    
                    Dim iValidoN1 As Integer
                    iValidoN1 = 0
                    Dim iValidoN2 As Integer
                    iValidoN2 = 0
                    Dim iNoValidoN2 As Integer
                    iNoValidoN2 = 0
                       
                    Dim iValidoN3 As Integer
                    iValidoN3 = 0
                    Dim iNumDist3 As Integer
                       
                    Dim bTodosValidos As Boolean
                    Dim m_sUONMostrar As String
                    Dim m_sCodsItemsNoDistribuibles As String
    
                    bTodosValidos = True
                    m_sCodsItemsNoDistribuibles = ""

                    Dim oDistN1 As CDistItemNivel1
                    Dim oDistN2 As CDistItemNivel2
                    Dim oDistN3 As CDistItemNivel3
                    
                    If frmPROCE.g_oProcesoSeleccionado Is Nothing Then Exit Sub
                    
                        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                                      
                            Dim oUon As IUOn
                            Dim oArticulo As CArticulo
                            Set oArticulo = oFSGSRaiz.Generar_CArticulo
                            oArticulo.Cod = frmPROCE.sdbgItems.Columns("COD").Value
                            Dim o As IUOn
                            Dim dist1 As CDistItemNivel1
                            Dim dist2 As CDistItemNivel2
                            Dim dist3 As CDistItemNivel3
                            Dim bItemvalido As Boolean
                            bItemvalido = True
                            Dim oUons As CUnidadesOrganizativas
                            Set oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
    
            
                            If Not oDistsNivel1 Is Nothing Then
                                For Each dist1 In oDistsNivel1
                                    Set oUon = oUons.createUon(dist1.CodUON1, "", "", "")
                                    bItemvalido = False
                                    For Each o In oArticulo.uons
                                        If o.incluye(oUon) Or oUon.incluye(o) Then
                                            bItemvalido = True
                                            Exit For
                                        End If
                                    Next
                                Next
                            End If
                            
                            If bItemvalido Then
                                If Not oDistsNivel2 Is Nothing Then
                                    For Each dist2 In oDistsNivel2
                                        Set oUon = oUons.createUon(dist2.CodUON1, dist2.CodUON2, "", "")
                                                        
                                        bItemvalido = False
                                        For Each o In oArticulo.uons
                                            If o.incluye(oUon) Or oUon.incluye(o) Then
                                                bItemvalido = True
                                                Exit For
                                            End If
                                        Next
                                    Next
                                End If
                            End If
                            
                            If bItemvalido Then
                                If Not oDistsNivel3 Is Nothing Then
                                    For Each dist3 In oDistsNivel3
                                        Set oUon = oUons.createUon(dist3.CodUON1, dist3.CodUON2, dist3.CodUON3, "")
                                        
                                        bItemvalido = False
                                        For Each o In oArticulo.uons
                                            If o.incluye(oUon) Or oUon.incluye(o) Then
                                                bItemvalido = True
                                                Exit For
                                            End If
                                        Next
                                    Next
    
                                End If
                            End If
                            If Not bItemvalido Then
                                oMensajes.ImposibleAnyadirItemIndividualADistribucion oArticulo.uons.toString, oArticulo.uons.Count
                                Exit Sub
                            End If
    
                        Next

                    Screen.MousePointer = vbHourglass
                    Set oItems = oFSGSRaiz.Generar_CItems
                    teserror = oItems.ActualizarDistribucion(frmPROCE.g_oProcesoSeleccionado.Anyo, frmPROCE.g_oProcesoSeleccionado.GMN1Cod, frmPROCE.g_oProcesoSeleccionado.Cod, frmPROCE.g_arItems, frmPROCE.g_oGrupoSeleccionado.Codigo, oDistsNivel1, oDistsNivel2, oDistsNivel3)
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Set oItems = Nothing
                        Set oDistsNivel1 = Nothing
                        Set oDistsNivel2 = Nothing
                        Set oDistsNivel3 = Nothing
                        Unload Me
                        Exit Sub
                    End If
        
                    frmPROCE.g_bUpdate = False 'Evitar el BeforeUpdate
                    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                        frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                        If NullToStr(sCad3) <> "" Then
                            frmPROCE.sdbgItems.Columns("DIST").Value = sCad1 & "-" & sCad2 & "-" & sCad3
                        ElseIf NullToStr(sCad2) <> "" Then
                            frmPROCE.sdbgItems.Columns("DIST").Value = sCad1 & "-" & sCad2
                        Else
                            frmPROCE.sdbgItems.Columns("DIST").Value = NullToStr(sCad1)
                        End If
                    Next
                    frmPROCE.sdbgItems.SelBookmarks.RemoveAll
                    frmPROCE.sdbgItems.Update
                    frmPROCE.g_bUpdate = True
                    Screen.MousePointer = vbNormal
                    Set oItems = Nothing
            
            Case "XLS_ITEMS" 'Carga de items desde Excel
                    Set frmPROCE.g_oDistsNivel1Item = oDistsNivel1
                    Set frmPROCE.g_oDistsNivel2Item = oDistsNivel2
                    Set frmPROCE.g_oDistsNivel3Item = oDistsNivel3
    
    End Select
        
    m_bAceptar = True
    Unload Me
    Set oDistsNivel1 = Nothing
    Set oDistsNivel2 = Nothing
    Set oDistsNivel3 = Nothing
    If g_sOrigen <> "PROCEN_UONG2" Then
        Set oGrupo = Nothing
    End If
    Set oItem = Nothing
                
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Function DevolverPorcentaje(ByVal sTexto As String) As Double
Dim i As Integer
Dim sPorcen As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sTexto = "" Then Exit Function
    i = Len(sTexto)
    
    If i = 1 Then Exit Function
    
    sTexto = Left(sTexto, Len(sTexto) - 1)
    
    While i > 1
        
        If Mid(sTexto, Len(sTexto), 1) = "%" Then
            sTexto = Left(sTexto, Len(sTexto) - 1)
        i = i - 1
        Else
            If Mid(sTexto, i, 1) = "(" Then
                sPorcen = Mid(sTexto, i + 1, Len(sTexto))
                i = 0
            Else
                i = i - 1
            End If
        End If
        
    Wend
    If sPorcen = "" Or Not IsNumeric(sPorcen) Then sPorcen = "0"
    DevolverPorcentaje = CDec(sPorcen / 100)
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "DevolverPorcentaje", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    m_bActivado = False
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    Me.Height = 5355
    Me.Width = 6900

    CargarRecursos
    
    ConfigurarSeguridad
    
    m_bAceptar = False
    
    Set m_oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set m_oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set m_oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3

    GenerarEstructuraOrg (False)
    
    tvwestrorg_NodeClick tvwestrorg.Nodes(1)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub ConfigurarInterfaz(ByVal node As MSComctlLib.node)
Dim iDistrib As Integer
Dim dDistrib As Double
Dim nodoHermano As MSComctlLib.node
Dim dPorcentajeHermano As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    If node.Parent Is Nothing Then
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    m_stexto = node.Text
    
    'Configuramos seg�n nivel de de distribuci�n m�nimo
    Select Case gParametrosGenerales.giNIVDIST
        Case 2
            If Mid(node.Image, 4, 1) = "1" Then
                picPorcen.Enabled = False
                Exit Sub
            Else
                picPorcen.Enabled = True
            End If
            
        Case 3
            If Mid(node.Image, 4, 1) = "1" Or Mid(node.Image, 4, 1) = "2" Then
                picPorcen.Enabled = False
                Exit Sub
            Else
                picPorcen.Enabled = True
            End If
        Case Else
            picPorcen.Enabled = True
    End Select
    
    If isBloqueado(node) Then
        picPorcen.Enabled = False
        Exit Sub
    End If
    'Configuramos dependiendo de si est� restringido a su unidad organizativa
    If frmPROCE.g_bRUO Then
        If node.Parent.Parent Is Nothing Then
            'Ha seleccionado la de nivel1
            'Tenemos que ver si la del usuario esta por encima de la seleccionada
            If Trim(basOptimizacion.gUON2Usuario) = "" Then
                ' El usuario tiene permisos sobre la UON1 seleccionada
                picPorcen.Enabled = True
            Else
                picPorcen.Enabled = False
                Exit Sub
            End If
        Else
            If node.Parent.Parent.Parent Is Nothing Then
                'Ha seleccionado la de nivel2
                'Tenemos que ver si la del usuario esta por encima de la seleccionada
                If Trim(basOptimizacion.gUON3Usuario) = "" Then
                    ' El usuario tiene permisos sobre la UON2 seleccionada
                    picPorcen.Enabled = True
                Else
                    picPorcen.Enabled = False
                    Exit Sub
                End If
            End If
        End If
    End If
    
    Slider1.Max = 100
    If node.Image = "UON1A" Or node.Image = "UON2A" Or node.Image = "UON3A" Then
        m_bRespetarPorcen = True
        txtPorcentaje.Text = CDec(DevolverPorcentaje(node.Text) * 100)
        If txtPorcentaje.Text < 32767 Then
            m_bRespetarPorcen = True
            Slider1.Value = Int(txtPorcentaje.Text)
        End If
        If Slider1.Value = 0 Then
            If CDbl(Left(lblSinDistPorcentaje.caption, Len(lblSinDistPorcentaje.caption) - 1)) > 0 Then
                Slider1.Max = Int(Left(lblSinDistPorcentaje.caption, Len(lblSinDistPorcentaje.caption) - 1))
            Else
                Slider1.Max = 1
            End If
        Else
            Slider1.Max = Slider1.Value + Int(Left(lblSinDistPorcentaje.caption, Len(lblSinDistPorcentaje.caption) - 1))
        End If
        m_bRespetarPorcen = False
        
    Else
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        
        
        If picPorcen.Enabled = True Then
            'Si las ramas hijas o padres tienen porcentajes los elimina
            dDistrib = DevolverPorcentajePadreHijos(node)
            If dDistrib <> 0 Then
                LimpiarRama node
                lblSinDistPorcentaje.caption = CStr(CDbl(Left(lblSinDistPorcentaje.caption, Len(lblSinDistPorcentaje.caption) - 1)) + (dDistrib * 100)) & " %"
                'Y pone ese porcentaje en el nuevo nodo
                txtPorcentaje.Text = CDec(dDistrib * 100)
                If txtPorcentaje.Text < 32767 Then
                '    Slider1.Value = txtPorcentaje
                     Slider1.Max = IIf(Int(txtPorcentaje.Text) = 0, 1, Int(txtPorcentaje.Text))
                End If
                    
            Else
            
                If CDbl(Left(lblSinDistPorcentaje.caption, Len(lblSinDistPorcentaje.caption) - 1)) > 0 Then
                    'Si no llega al 100% asigna lo que falta
                    txtPorcentaje.Text = CDbl(Left(lblSinDistPorcentaje.caption, Len(lblSinDistPorcentaje.caption) - 1))
                    If txtPorcentaje.Text < 32767 Then
                    '    Slider1.Value = txtPorcentaje
                        Slider1.Max = IIf(Int(txtPorcentaje.Text) = 0, 1, Int(txtPorcentaje.Text))
                    End If
                    
                
                Else
                    'Si hay m�s de un nodo asignado no hace nada.Si no lo asigna el 100% a este nodo en el que nos posicionamos.
                    iDistrib = DevolverDistUON
                    If iDistrib >= 2 Then   'Hay mas de un nodo asignado:no hace nada
                         'Si solo hay un nodo hermano asignado cogeremos su porcentaje y lo pondremos en el seleccionado
                        Set nodoHermano = Nothing
                        Set nodoHermano = SoloUnHermanoAsignado(node)
                        If Not nodoHermano Is Nothing Then
                            dPorcentajeHermano = CDec(DevolverPorcentaje(nodoHermano))
                            Slider1.Max = Int(CDec(dPorcentajeHermano * 100))
                            nodoHermano.Image = Mid(nodoHermano.Image, 1, 4)
                            nodoHermano.Text = QuitarPorcentaje(nodoHermano.Text)
                            lblSinDistPorcentaje.caption = CDbl(Left(lblSinDistPorcentaje.caption, Len(lblSinDistPorcentaje.caption) - 1)) + (dPorcentajeHermano * 100) & " %"
                            'dDistPorcen = dDistPorcen - dPOrcentajeHermano
                            
                            Slider1.Value = Slider1.Max
                        Else
                            Slider1.Max = 1
                        End If
                    Else  'Hay un nodo asignado al 100% o todav�a no hay nada distribuido: asigna el 100%
                        lblSinDistPorcentaje.caption = "0 %"
                        LimpiarTodasLasRamas
                        Slider1.Max = 100
                        txtPorcentaje.Text = Slider1.Max
                    End If
                End If
            End If
        Else
            If Int(Left(lblSinDistPorcentaje.caption, Len(lblSinDistPorcentaje.caption) - 1)) > 1 Then
                Slider1.Max = Int(Left(lblSinDistPorcentaje.caption, Len(lblSinDistPorcentaje.caption) - 1))
            Else
                Slider1.Max = 1
            End If
        End If
        
        m_bRespetarPorcen = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "ConfigurarInterfaz", err, Erl, , m_bActivado)
      Resume 0
      Exit Sub
   End If
    
End Sub

Private Sub CargarRecursos()
Dim Adores As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_DISTUON, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Adores Is Nothing Then
        Me.caption = Adores(0).Value
        Adores.MoveNext
        cmdAceptar.caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value
        Adores.MoveNext
        lblPorcen.caption = Adores(0).Value
        Adores.MoveNext
        lblDist.caption = Adores(0).Value
        Adores.MoveNext
        lblSinDist.caption = Adores(0).Value
        
        Adores.Close
        
    End If
    
    Set Adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
 If Me.Height < 2000 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    tvwestrorg.Height = Me.Height - 1740
    picPorcen.Top = Me.Height - 1350
    tvwestrorg.Width = Me.Width - 200
    picPorcen.Width = tvwestrorg.Width
    cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 50
    cmdCancelar.Left = Me.Width / 2 + 50
    cmdAceptar.Top = Me.Height - 800
    cmdCancelar.Top = Me.Height - 800
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim irespuesta As Integer
    
    'Desde la configuraci�n del proceso puede cancelarse (aunque sea obligatorio)
    'y la distribuci�n volver�a al ambito anterior.17/06/2002
    'Al modificar un �tem o varios puede cancelarse la distribuci�n aunque sea oblig
    'porque si el �tem est� en BD se supone que ya tiene todos los datos obligatorios
    'No se contempla el caso de que la distrib no fuese obligatoria en un principio y ahora s� lo sea.19/09/2002

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, False
    If (g_sOrigen <> "PROCEM" And g_sOrigen <> "PROCE_MODIFITEMS") Then
        If Not m_bAceptar Then
            If g_sOrigen = "GRUPON_UONG" Then
                irespuesta = oMensajes.PreguntaCancelarAnyadirGrupoDist
                If irespuesta = vbNo Then
                    Cancel = True
                    Exit Sub
                End If
            ElseIf g_sOrigen = "PROCEN_UONP" Or g_sOrigen = "PROCEN_UONG" Then
                irespuesta = oMensajes.PreguntaCancelarAnyadirProcesoDist
                If irespuesta = vbNo Then
                    Cancel = True
                    Exit Sub
                End If
            Else
                oMensajes.PresupuestoDistribOblig 0, basParametros.gParametrosGenerales.gsSingPres1, basParametros.gParametrosGenerales.gsSingPres2, _
                            basParametros.gParametrosGenerales.gsSingPres3, basParametros.gParametrosGenerales.gsSingPres4
                Cancel = True
                Exit Sub
            End If
        Else
            If CDbl(Left(lblPorcentaje.caption, Len(lblPorcentaje.caption) - 1)) = 0 Then
                Screen.MousePointer = vbNormal
                oMensajes.ProceDistObligatoria
                Cancel = True
                Exit Sub
            End If
        End If
    End If
        
    m_bAceptar = False
    Set m_oUnidadesOrgN1 = Nothing
    Set m_oUnidadesOrgN2 = Nothing
    Set m_oUnidadesOrgN3 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub Slider1_Change()
Dim sPorcen As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If tvwestrorg.selectedItem Is Nothing Then 'ninguno
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If tvwestrorg.selectedItem.Parent Is Nothing Then 'Raiz
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
'    m_bRespetarPorcen = True
    If Slider1.Value >= Slider1.Max Then
        sPorcen = CDec(DevolverPorcentaje(tvwestrorg.selectedItem.Text) * 100)
        txtPorcentaje.Text = CDbl(sPorcen) + CDbl(Left(lblSinDistPorcentaje.caption, Len(lblSinDistPorcentaje.caption) - 1))
    Else
        txtPorcentaje.Text = Slider1.Value
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "Slider1_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub



Private Sub tvwestrorg_NodeClick(ByVal node As MSComctlLib.node)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarInterfaz node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "tvwestrorg_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub LimpiarRama(ByVal node As MSComctlLib.node)
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node

   'Primero hacia arriba
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nod1 = node.Parent
    
    While Not nod1 Is Nothing
        
        If Mid(nod1.Image, 5, 1) = "A" Then
            nod1.Image = Mid(nod1.Image, 1, 4)
            nod1.Text = QuitarPorcentaje(nod1.Text)
            Exit Sub
        Else
            Set nod1 = nod1.Parent
        End If
    
    Wend
    
    'Ahora hacia abajo
    
    Set nod1 = node.Child
    
    While Not nod1 Is Nothing
        
        If Mid(nod1.Image, 5, 1) = "A" Then
            nod1.Image = Mid(nod1.Image, 1, 4)
            nod1.Text = QuitarPorcentaje(nod1.Text)

        Else
            Set nod2 = nod1.Child
                
            While Not nod2 Is Nothing
                
                If Mid(nod2.Image, 5, 1) = "A" Then
                    nod2.Image = Mid(nod2.Image, 1, 4)
                    nod2.Text = QuitarPorcentaje(nod2.Text)
                    
                Else
                    Set nod3 = nod2.Child
                        
                    While Not nod3 Is Nothing
                        If Mid(nod3.Image, 5, 1) = "A" Then
                            nod3.Image = Mid(nod3.Image, 1, 4)
                            nod3.Text = QuitarPorcentaje(nod3.Text)
                        End If
                        Set nod3 = nod3.Next
                    Wend
                End If
                Set nod2 = nod2.Next
            Wend
        
        End If
        
        Set nod1 = nod1.Next
        
    Wend
        
    Set nod1 = Nothing
    Set nod2 = Nothing
    Set nod3 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "LimpiarRama", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


Private Function QuitarPorcentaje(ByVal sTexto As String) As String
Dim i As Integer
Dim sAux As String
    

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sAux = sTexto
    
    If sTexto = "" Then Exit Function
    
    i = Len(sTexto)
    
    If i = 1 Then Exit Function
    
    sTexto = Left(sTexto, Len(sTexto) - 1)
    
    While i > 1
        
        If Mid(sTexto, Len(sTexto), 1) <> "(" Then
            sTexto = Left(sTexto, Len(sTexto) - 1)
            i = i - 1
        Else
            sTexto = Left(sTexto, Len(sTexto) - 2)
            i = -1
        End If
    
    Wend
    
    If i = 0 Or i = 1 Then
        sTexto = sAux
    End If
    
    QuitarPorcentaje = sTexto
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "QuitarPorcentaje", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function


Private Sub txtPorcentaje_Change()
Dim nodx As MSComctlLib.node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    Set nodx = tvwestrorg.selectedItem
    
    If nodx Is Nothing Then 'ninguno
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    If nodx.Parent Is Nothing Then 'raiz
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If Trim(txtPorcentaje.Text) = "" Then
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_stexto = QuitarPorcentaje(m_stexto)
        tvwestrorg.selectedItem.Text = m_stexto
        Select Case Left(tvwestrorg.selectedItem.Tag, 4)
            Case "UON1"
                tvwestrorg.selectedItem.Image = "UON1"
            Case "UON2"
                tvwestrorg.selectedItem.Image = "UON2"
            Case "UON3"
                tvwestrorg.selectedItem.Image = "UON3"
        End Select
        m_bRespetarPorcen = False
        
        lblSinDistPorcentaje.caption = 100 - CDec(RecalcularPorcen * 100) & " %"
        lblPorcentaje.caption = CDec(RecalcularPorcen * 100) & " %"
        Exit Sub
    End If
        
    If Not IsNumeric(txtPorcentaje.Text) Then
        m_bRespetarPorcen = True
        txtPorcentaje.Text = Left(txtPorcentaje.Text, Len(txtPorcentaje.Text) - 1)
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    m_stexto = nodx.Text
    
    If Mid(nodx.Image, 5, 1) = "A" Then
        m_stexto = QuitarPorcentaje(m_stexto)
    End If
    
    If CDbl(txtPorcentaje.Text) <= 0 Then
        If CDbl(txtPorcentaje.Text) <> 0 Then
            m_bRespetarPorcen = True
            txtPorcentaje.Text = ""
        End If
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        tvwestrorg.selectedItem.Text = m_stexto
        Select Case Left(tvwestrorg.selectedItem.Tag, 4)
            Case "UON1"
                tvwestrorg.selectedItem.Image = "UON1"
            Case "UON2"
                tvwestrorg.selectedItem.Image = "UON2"
            Case "UON3"
                tvwestrorg.selectedItem.Image = "UON3"
        End Select
        
        lblSinDistPorcentaje.caption = 100 - CDec(RecalcularPorcen * 100) & " %"
        lblPorcentaje.caption = CDec(RecalcularPorcen * 100) & " %"
        
    Else
        
        m_bRespetarPorcen = True
        If txtPorcentaje.Text < 32767 Then
            Slider1.Value = txtPorcentaje
        End If
        m_bRespetarPorcen = False
        
        tvwestrorg.selectedItem.Text = m_stexto & " (" & CDbl(txtPorcentaje.Text) & "%" & ")"
        
        Select Case Left(tvwestrorg.selectedItem.Tag, 4)
            Case "UON1"
                tvwestrorg.selectedItem.Image = "UON1A"
            Case "UON2"
                tvwestrorg.selectedItem.Image = "UON2A"
            Case "UON3"
                tvwestrorg.selectedItem.Image = "UON3A"
        End Select
        
        LimpiarRama tvwestrorg.selectedItem
        
        lblSinDistPorcentaje.caption = 100 - CDec(RecalcularPorcen * 100) & " %"
        lblPorcentaje.caption = CDec(RecalcularPorcen * 100) & " %"
        If CDec(RecalcularPorcen * 100) > 100 Then
            txtPorcentaje.Text = Left(txtPorcentaje.Text, Len(txtPorcentaje.Text) - 1)
        End If
                
    End If

    Set nodx = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "txtPorcentaje_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Function RecalcularPorcen() As Double
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nodx As MSComctlLib.node
Dim dDistPorcen As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dDistPorcen = 0
    
   Set nodx = tvwestrorg.Nodes(1)
   If nodx.Children = 0 Then Exit Function
    
   Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            ' No distribu�do
            If nod1.Image = "UON1" Then
                
                Set nod2 = nod1.Child
                
                While Not nod2 Is Nothing
                    ' No distribu�do
                    If nod2.Image = "UON2" Then
                    
                        Set nod3 = nod2.Child
                        
                        While Not nod3 Is Nothing
                            
                            If nod3.Image = "UON3A" Then
                                dDistPorcen = dDistPorcen + DevolverPorcentaje(nod3.Text)
                            End If
                            
                            Set nod3 = nod3.Next
                        Wend
                    Else
                        ' Si est� distribu�do UON2
                        dDistPorcen = dDistPorcen + DevolverPorcentaje(nod2.Text)
                    End If
                    
                    Set nod2 = nod2.Next
                Wend
                
            Else
            ' Si est� distribu�do UON1
                dDistPorcen = dDistPorcen + DevolverPorcentaje(nod1.Text)
            End If
            
            Set nod1 = nod1.Next
        
        Wend

    Set nod1 = Nothing
    Set nod2 = Nothing
    Set nod3 = Nothing
    Set nodx = Nothing
    
    RecalcularPorcen = dDistPorcen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "RecalcularPorcen", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function DevolverPorcentajePadreHijos(ByVal nod As MSComctlLib.node) As Double
Dim i As Integer
Dim dPorcen As Double
Dim sCod As String
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nod Is Nothing Then Exit Function
    
    dPorcen = 0
    
    'Primero hacia arriba
    Set nod1 = nod.Parent
    
    While Not nod1 Is Nothing
        If Mid(nod1.Image, 5, 1) = "A" Then
            dPorcen = dPorcen + DevolverPorcentaje(nod1)
            DevolverPorcentajePadreHijos = dPorcen
            Exit Function
        Else
            Set nod1 = nod1.Parent
        End If
    Wend
    
    'Ahora hacia abajo
    
    Set nod1 = nod.Child
    
    While Not nod1 Is Nothing
        
        If Mid(nod1.Image, 5, 1) = "A" Then
            dPorcen = dPorcen + DevolverPorcentaje(nod1)
        Else
            Set nod2 = nod1.Child
                
            While Not nod2 Is Nothing
                
                If Mid(nod2.Image, 5, 1) = "A" Then
                    dPorcen = dPorcen + DevolverPorcentaje(nod2)
                Else
                    Set nod3 = nod2.Child
                        
                    While Not nod3 Is Nothing
                        If Mid(nod3.Image, 5, 1) = "A" Then
                            dPorcen = dPorcen + DevolverPorcentaje(nod3)
                        End If
                        Set nod3 = nod3.Next
                    Wend
                End If
                Set nod2 = nod2.Next
            Wend
        
        End If
        
        Set nod1 = nod1.Next
        
    Wend
    
    Set nod1 = Nothing
    Set nod2 = Nothing
    Set nod3 = Nothing
    
    DevolverPorcentajePadreHijos = dPorcen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "DevolverPorcentajePadreHijos", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function


Private Function DevolverDistUON() As Integer
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node
Dim nodx As MSComctlLib.node
Dim iDistrib  As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iDistrib = 0
        
    Set nodx = tvwestrorg.Nodes(1)
   
    If nodx.Children = 0 Then
        DevolverDistUON = 0
        Exit Function
    End If
   
    Set nod1 = nodx.Child
    While Not nod1 Is Nothing
        ' No distribu�do
        If nod1.Image = "UON1" Then
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                ' No distribu�do
                If nod2.Image = "UON2" Then
                    Set nod3 = nod2.Child
                    
                    While Not nod3 Is Nothing
                        If nod3.Image = "UON3A" Then
                            iDistrib = iDistrib + 1
                        End If
                        Set nod3 = nod3.Next
                    Wend
                    
                Else
                    ' Si est� distribu�do UON2
                    iDistrib = iDistrib + 1
                End If
                Set nod2 = nod2.Next
            Wend
                
        Else
            ' Si est� distribu�do UON1
            iDistrib = iDistrib + 1
        End If
            
        Set nod1 = nod1.Next
    Wend
    
   DevolverDistUON = iDistrib
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "DevolverDistUON", err, Erl, , m_bActivado)
      Exit Function
   End If
   
End Function

''' <summary>Limpia los porcentajes de asignaci�n de los nombres de todas las ramas</summary>
''' <remarks>Llamada desde: ConfigurarInterfaz</remarks>
''' <revision>LTG 19/09/2013</revision>

Private Sub LimpiarTodasLasRamas()
    Dim nod1 As MSComctlLib.node
    Dim nod2 As MSComctlLib.node
    Dim nod3 As MSComctlLib.node
    Dim nodx As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwestrorg.Nodes(1)
   
    If nodx.Children = 0 Then
        Exit Sub
    End If
   
    Set nod1 = nodx.Child
    While Not nod1 Is Nothing
        ' No distribu�do
        If nod1.Image = "UON1" Then
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                ' No distribu�do
                If nod2.Image = "UON2" Then
                    Set nod3 = nod2.Child
                    
                    While Not nod3 Is Nothing
                        If nod3.Image = "UON3A" Then
                            'LimpiarRama nodx
                            nod3.Image = Mid(nod3.Image, 1, 4)
                            nod3.Text = QuitarPorcentaje(nod3.Text)
                        End If
                        Set nod3 = nod3.Next
                    Wend
                    
                Else
                    ' Si est� distribu�do UON2
                    nod2.Image = Mid(nod2.Image, 1, 4)
                    nod2.Text = QuitarPorcentaje(nod2.Text)
                End If
                Set nod2 = nod2.Next
            Wend
                
        Else
            ' Si est� distribu�do UON1
            nod1.Image = Mid(nod1.Image, 1, 4)
            nod1.Text = QuitarPorcentaje(nod1.Text)
        End If
            
        Set nod1 = nod1.Next
    Wend
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "LimpiarTodasLasRamas", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function SoloUnHermanoAsignado(ByVal node As MSComctlLib.node) As Object
Dim lCount As Long
Dim nod As MSComctlLib.node
Dim nodoHermano As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
lCount = 0


Set nod = node.FirstSibling
While Not nod Is Nothing
    If Mid(nod.Image, 5, 1) = "A" And nod.key <> node.key Then
        Set nodoHermano = Nothing
        Set nodoHermano = nod
        lCount = lCount + 1
    End If
    Set nod = nod.Next
Wend

    If lCount = 1 Then
        Set SoloUnHermanoAsignado = nodoHermano
    Else
        Set SoloUnHermanoAsignado = Nothing
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "SoloUnHermanoAsignado", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Sub bloquearNodo(ByRef nodo As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    nodo.Forecolor = &HC0C0C0
    nodo.Bold = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "bloquearNodo", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function isBloqueado(ByRef nodo As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    isBloqueado = nodo.Bold
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDistUON", "isBloqueado", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

