VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDetAtribProce 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DDetalle de atributo"
   ClientHeight    =   5880
   ClientLeft      =   1335
   ClientTop       =   3630
   ClientWidth     =   9105
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDetAtribProce.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5880
   ScaleWidth      =   9105
   Begin TabDlg.SSTab sstabgeneral 
      Height          =   5775
      Left            =   90
      TabIndex        =   0
      Top             =   60
      Width           =   8955
      _ExtentX        =   15796
      _ExtentY        =   10186
      _Version        =   393216
      TabHeight       =   520
      BackColor       =   8421376
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DDatos generales"
      TabPicture(0)   =   "frmDetAtribProce.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblDefNivel"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "picSino"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "picLibre"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "picNivel"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "picSelec"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "DPonderaci�n"
      TabPicture(1)   =   "frmDetAtribProce.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lblTipoPond"
      Tab(1).Control(1)=   "Shape1"
      Tab(1).Control(2)=   "picSelNumerico"
      Tab(1).Control(3)=   "picSelBoolean"
      Tab(1).Control(4)=   "picNumerico"
      Tab(1).Control(5)=   "picBoolean"
      Tab(1).ControlCount=   6
      TabCaption(2)   =   "DValores del proveedor"
      TabPicture(2)   =   "frmDetAtribProce.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "sdbgValores"
      Tab(2).Control(1)=   "picConsulta"
      Tab(2).Control(2)=   "picEdicion"
      Tab(2).Control(3)=   "sdbgValoresItem"
      Tab(2).Control(4)=   "sdbcGrupo"
      Tab(2).ControlCount=   5
      Begin VB.PictureBox picSelec 
         BorderStyle     =   0  'None
         Height          =   4095
         Left            =   210
         ScaleHeight     =   4095
         ScaleWidth      =   8505
         TabIndex        =   50
         Top             =   960
         Width           =   8505
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   525
            Left            =   1680
            TabIndex        =   51
            Top             =   2310
            Width           =   6585
            Begin VB.OptionButton OptValorBajo 
               Caption         =   "DValores bajos/recientes"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   60
               TabIndex        =   53
               Top             =   180
               Value           =   -1  'True
               Width           =   2295
            End
            Begin VB.OptionButton OptValorAlto 
               Caption         =   "DValores altos/lejanos"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   3000
               TabIndex        =   52
               ToolTipText     =   "Lejanos para el caso de fechas"
               Top             =   180
               Width           =   1935
            End
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcLista 
            Height          =   285
            Left            =   4680
            TabIndex        =   54
            Top             =   1440
            Width           =   3585
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            Row.Count       =   4
            Row(0)          =   "10.000"
            Row(1)          =   "25.000"
            Row(2)          =   "40.000"
            Row(3)          =   "55.000"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   6694
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   14811135
            _ExtentX        =   6324
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483624
         End
         Begin VB.Label lblAmbitotxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1710
            TabIndex        =   73
            Top             =   1830
            Width           =   1335
         End
         Begin VB.Label lblAmbito 
            Caption         =   "DAmbito"
            Height          =   195
            Left            =   90
            TabIndex        =   72
            Top             =   1905
            Width           =   1335
         End
         Begin VB.Label lblObliga 
            Caption         =   "(DObligatorio )"
            Height          =   195
            Left            =   3390
            TabIndex        =   71
            Top             =   1890
            Width           =   1635
         End
         Begin VB.Label lblIntrotxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1710
            TabIndex        =   70
            Top             =   1440
            Width           =   1335
         End
         Begin VB.Label lblLista 
            Caption         =   "DLista"
            Height          =   240
            Left            =   3420
            TabIndex        =   69
            Top             =   1500
            Width           =   1005
         End
         Begin VB.Label lblIntro 
            Caption         =   "DIntroducci�n"
            Height          =   195
            Left            =   90
            TabIndex        =   68
            Top             =   1440
            Width           =   1335
         End
         Begin VB.Label lblDentxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1710
            TabIndex        =   67
            Top             =   405
            Width           =   6520
         End
         Begin VB.Label lblTipotxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1710
            TabIndex        =   66
            Top             =   810
            Width           =   1335
         End
         Begin VB.Label lblCodtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1710
            TabIndex        =   65
            Top             =   0
            Width           =   1335
         End
         Begin VB.Label lblDen 
            Caption         =   "DDenominaci�n"
            Height          =   255
            Left            =   60
            TabIndex        =   64
            Top             =   450
            Width           =   1215
         End
         Begin VB.Label lblCod 
            Caption         =   "DC�digo"
            Height          =   195
            Left            =   60
            TabIndex        =   63
            Top             =   30
            Width           =   1455
         End
         Begin VB.Label lblTipo 
            Caption         =   "DTipo de dato"
            Height          =   255
            Left            =   90
            TabIndex        =   62
            Top             =   840
            Width           =   1395
         End
         Begin VB.Line Line3 
            Index           =   1
            X1              =   0
            X2              =   8520
            Y1              =   1230
            Y2              =   1230
         End
         Begin VB.Label lblExter 
            Caption         =   "(D Externo )"
            Height          =   195
            Left            =   3390
            TabIndex        =   61
            Top             =   870
            Width           =   1635
         End
         Begin VB.Label lblAplicarDefec 
            Caption         =   "(DAplicar por defecto)"
            Height          =   195
            Left            =   3375
            TabIndex        =   60
            Top             =   3060
            Width           =   1635
         End
         Begin VB.Label lblAplicartxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1710
            TabIndex        =   59
            Top             =   3480
            Width           =   3135
         End
         Begin VB.Label lblRelaciontxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1710
            TabIndex        =   58
            Top             =   3000
            Width           =   1335
         End
         Begin VB.Label lblAplicarPrecio 
            Caption         =   "DAplicar a precio"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   75
            TabIndex        =   57
            Top             =   3540
            Width           =   1455
         End
         Begin VB.Label lblRelacionPrecio 
            Caption         =   "DRelaci�n con el precio"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   90
            TabIndex        =   56
            Top             =   3060
            Width           =   1575
         End
         Begin VB.Label lblPrefe 
            Caption         =   "DPreferencia"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   90
            TabIndex        =   55
            Top             =   2460
            Width           =   1395
         End
         Begin VB.Line Line3 
            Index           =   2
            X1              =   60
            X2              =   8580
            Y1              =   2880
            Y2              =   2880
         End
         Begin VB.Line Line3 
            Index           =   3
            X1              =   0
            X2              =   8520
            Y1              =   2280
            Y2              =   2280
         End
      End
      Begin VB.PictureBox picNivel 
         BorderStyle     =   0  'None
         Height          =   300
         Left            =   1920
         ScaleHeight     =   300
         ScaleWidth      =   4995
         TabIndex        =   108
         Top             =   480
         Width           =   5000
         Begin SSDataWidgets_B.SSDBCombo sdbcGrupos 
            Height          =   285
            Left            =   0
            TabIndex        =   109
            Top             =   0
            Width           =   4005
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Row.Count       =   4
            Col.Count       =   3
            Row(0).Col(0)   =   "AP"
            Row(0).Col(1)   =   "Apisonadoras"
            Row(1).Col(0)   =   "EXC"
            Row(1).Col(1)   =   "Excabadoras"
            Row(2).Col(0)   =   "HOR"
            Row(2).Col(1)   =   "Hormigoneras"
            Row(3).Col(0)   =   "ALL"
            Row(3).Col(1)   =   "ALL"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   14811135
            Columns(1).Width=   6482
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   14811135
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "POND"
            Columns(2).Name =   "POND"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   7064
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483624
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgValores 
         Height          =   4095
         Left            =   -74760
         TabIndex        =   107
         Top             =   960
         Width           =   8535
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   4
         stylesets.count =   1
         stylesets(0).Name=   "Cabecera"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmDetAtribProce.frx":0D06
         AllowUpdate     =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   -2147483630
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   79
         Columns.Count   =   4
         Columns(0).Width=   7355
         Columns(0).Caption=   "PROV"
         Columns(0).Name =   "PROV"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777151
         Columns(0).HeadStyleSet=   "Cabecera"
         Columns(1).Width=   3519
         Columns(1).Caption=   "VAL"
         Columns(1).Name =   "VAL"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HeadStyleSet=   "Cabecera"
         Columns(2).Width=   3519
         Columns(2).Caption=   "POND"
         Columns(2).Name =   "POND"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HeadStyleSet=   "Cabecera"
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "CodProv"
         Columns(3).Name =   "CodProv"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         _ExtentX        =   15055
         _ExtentY        =   7223
         _StockProps     =   79
         BackColor       =   -2147483633
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox picConsulta 
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   -74760
         ScaleHeight     =   495
         ScaleWidth      =   2535
         TabIndex        =   103
         Top             =   5160
         Width           =   2535
         Begin VB.CommandButton cmdModificar 
            Caption         =   "&Modificar"
            Height          =   315
            Left            =   0
            TabIndex        =   104
            Top             =   0
            Width           =   1155
         End
      End
      Begin VB.PictureBox picEdicion 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   -71900
         ScaleHeight     =   375
         ScaleWidth      =   3420
         TabIndex        =   102
         Top             =   5160
         Visible         =   0   'False
         Width           =   3415
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "Cancelar"
            Height          =   315
            Left            =   1260
            TabIndex        =   106
            Top             =   0
            Width           =   1215
         End
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Aceptar"
            Default         =   -1  'True
            Height          =   315
            Left            =   0
            TabIndex        =   105
            Top             =   0
            Width           =   1155
         End
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgValoresItem 
         Height          =   4095
         Left            =   -74760
         TabIndex        =   100
         Top             =   960
         Width           =   8535
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   2
         stylesets.count =   2
         stylesets(0).Name=   "Amarillo"
         stylesets(0).BackColor=   13169908
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmDetAtribProce.frx":0D22
         stylesets(1).Name=   "Cabecera"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmDetAtribProce.frx":0D3E
         UseGroups       =   -1  'True
         AllowUpdate     =   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   79
         Groups(0).Width =   5662
         Groups(0).Caption=   "DItem"
         Groups(0).HeadStyleSet=   "Cabecera"
         Groups(0).Columns.Count=   2
         Groups(0).Columns(0).Width=   5662
         Groups(0).Columns(0).Name=   "COD"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(0).Locked=   -1  'True
         Groups(0).Columns(0).HasBackColor=   -1  'True
         Groups(0).Columns(0).BackColor=   16777151
         Groups(0).Columns(1).Width=   2858
         Groups(0).Columns(1).Visible=   0   'False
         Groups(0).Columns(1).Caption=   "ID"
         Groups(0).Columns(1).Name=   "ID"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         _ExtentX        =   15055
         _ExtentY        =   7223
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   -2147483633
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox picBoolean 
         BorderStyle     =   0  'None
         Height          =   2775
         Left            =   -73620
         ScaleHeight     =   2775
         ScaleWidth      =   5805
         TabIndex        =   88
         Top             =   1860
         Visible         =   0   'False
         Width           =   5805
         Begin VB.Frame fraManualBoolean 
            Height          =   975
            Left            =   465
            TabIndex        =   98
            Top             =   750
            Visible         =   0   'False
            Width           =   4935
            Begin VB.Label lblTextoManual 
               Caption         =   "DLos valores de ponderaci�n deber�n introducirse manualmente por el usuario"
               ForeColor       =   &H00000000&
               Height          =   435
               Left            =   300
               TabIndex        =   99
               Top             =   360
               Width           =   4275
            End
         End
         Begin VB.Frame fraAutomatico 
            Height          =   1875
            Left            =   465
            TabIndex        =   91
            Top             =   360
            Visible         =   0   'False
            Width           =   4935
            Begin VB.TextBox txtPuntuacion1 
               Alignment       =   1  'Right Justify
               Height          =   315
               Left            =   1860
               Locked          =   -1  'True
               TabIndex        =   93
               Text            =   "3"
               Top             =   600
               Width           =   855
            End
            Begin VB.TextBox txtPuntuacion2 
               Alignment       =   1  'Right Justify
               Height          =   315
               Left            =   1860
               Locked          =   -1  'True
               TabIndex        =   92
               Text            =   "0"
               Top             =   1020
               Width           =   855
            End
            Begin VB.Label lblPuntos2 
               Caption         =   "DPuntos"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   2820
               TabIndex        =   97
               Top             =   1080
               Width           =   615
            End
            Begin VB.Label lblNo 
               Caption         =   "DValor = No"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   750
               TabIndex        =   96
               Top             =   1080
               Width           =   945
            End
            Begin VB.Label lblSi 
               Caption         =   "DValor = S�"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   750
               TabIndex        =   95
               Top             =   660
               Width           =   960
            End
            Begin VB.Label lblPuntos1 
               Caption         =   "DPuntos"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   2820
               TabIndex        =   94
               Top             =   660
               Width           =   615
            End
         End
         Begin VB.Frame fraPond0b 
            Height          =   1095
            Left            =   465
            TabIndex        =   89
            Top             =   750
            Visible         =   0   'False
            Width           =   4935
            Begin VB.Label lblPond0b 
               Caption         =   "DAtributo sin ponderaci�n"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   600
               TabIndex        =   90
               Top             =   480
               Width           =   3795
            End
         End
      End
      Begin VB.PictureBox picNumerico 
         BorderStyle     =   0  'None
         Height          =   2955
         Left            =   -73770
         ScaleHeight     =   2955
         ScaleWidth      =   6300
         TabIndex        =   74
         Top             =   1770
         Visible         =   0   'False
         Width           =   6300
         Begin VB.Frame fraPond0 
            Height          =   1035
            Left            =   675
            TabIndex        =   75
            Top             =   660
            Visible         =   0   'False
            Width           =   4935
            Begin VB.Label lblPond0 
               Caption         =   "DAtributo sin ponderaci�n"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   675
               TabIndex        =   76
               Top             =   450
               Width           =   3795
            End
         End
         Begin VB.Frame fraFormula 
            Height          =   1035
            Left            =   675
            TabIndex        =   77
            Top             =   630
            Visible         =   0   'False
            Width           =   4935
            Begin VB.TextBox txtSigno 
               Alignment       =   1  'Right Justify
               Height          =   315
               Left            =   825
               Locked          =   -1  'True
               TabIndex        =   79
               Top             =   420
               Width           =   765
            End
            Begin VB.TextBox txtNumero 
               Alignment       =   1  'Right Justify
               Height          =   315
               Left            =   1725
               Locked          =   -1  'True
               TabIndex        =   78
               Top             =   420
               Width           =   1215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcSigno 
               Height          =   315
               Left            =   810
               TabIndex        =   80
               Top             =   420
               Width           =   765
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   8361
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "COD"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1349
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblValor 
               Caption         =   "DValor"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   300
               TabIndex        =   81
               Top             =   480
               Width           =   495
            End
         End
         Begin VB.Frame fraManual 
            Height          =   975
            Left            =   675
            TabIndex        =   86
            Top             =   570
            Visible         =   0   'False
            Width           =   4935
            Begin VB.Label lblManualBoo 
               Caption         =   "DLos valores de ponderaci�n deber�n introducirse manualmente por el usuario"
               ForeColor       =   &H00000000&
               Height          =   435
               Left            =   300
               TabIndex        =   87
               Top             =   360
               Width           =   4275
            End
         End
         Begin VB.Frame fraDiscreta 
            Height          =   2595
            Left            =   675
            TabIndex        =   84
            Top             =   60
            Width           =   4935
            Begin SSDataWidgets_B.SSDBGrid sdbgDiscreta 
               Height          =   2085
               Left            =   150
               TabIndex        =   85
               Top             =   300
               Width           =   4605
               ScrollBars      =   2
               _Version        =   196617
               DataMode        =   2
               Row.Count       =   3
               Col.Count       =   3
               Row(0).Col(0)   =   "1/1/2002"
               Row(0).Col(1)   =   "1"
               Row(1).Col(0)   =   "1/1/2003"
               Row(1).Col(1)   =   "2"
               Row(2).Col(0)   =   "1/1/2004"
               Row(2).Col(1)   =   "3"
               stylesets.count =   1
               stylesets(0).Name=   "Normal"
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmDetAtribProce.frx":0D5A
               MultiLine       =   0   'False
               AllowColumnSwapping=   0
               SelectTypeCol   =   0
               HeadStyleSet    =   "Normal"
               StyleSet        =   "Normal"
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               ExtraHeight     =   26
               Columns.Count   =   3
               Columns(0).Width=   3942
               Columns(0).Caption=   "Valor"
               Columns(0).Name =   "VALOR"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(0).Locked=   -1  'True
               Columns(1).Width=   3200
               Columns(1).Caption=   " => Puntos"
               Columns(1).Name =   "PUNTOS"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(1).Locked=   -1  'True
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "INDI"
               Columns(2).Name =   "INDI"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(2).Locked=   -1  'True
               _ExtentX        =   8123
               _ExtentY        =   3678
               _StockProps     =   79
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraContinua 
            Height          =   2595
            Left            =   600
            TabIndex        =   82
            Top             =   30
            Width           =   5085
            Begin SSDataWidgets_B.SSDBGrid sdbgContinua 
               Height          =   2055
               Left            =   225
               TabIndex        =   83
               Top             =   300
               Width           =   4635
               ScrollBars      =   2
               _Version        =   196617
               DataMode        =   2
               Row.Count       =   3
               Col.Count       =   4
               Row(0).Col(0)   =   "0"
               Row(0).Col(1)   =   "10"
               Row(0).Col(2)   =   "1"
               Row(1).Col(0)   =   "11"
               Row(1).Col(1)   =   "20"
               Row(1).Col(2)   =   "2"
               Row(2).Col(0)   =   "20"
               Row(2).Col(1)   =   "999999"
               Row(2).Col(2)   =   "10"
               stylesets.count =   1
               stylesets(0).Name=   "Normal"
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmDetAtribProce.frx":0D76
               MultiLine       =   0   'False
               AllowColumnSwapping=   0
               HeadStyleSet    =   "Normal"
               StyleSet        =   "Normal"
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               ExtraHeight     =   26
               Columns.Count   =   4
               Columns(0).Width=   2752
               Columns(0).Caption=   "Desde"
               Columns(0).Name =   "DESDE"
               Columns(0).AllowSizing=   0   'False
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(0).Locked=   -1  'True
               Columns(1).Width=   2143
               Columns(1).Caption=   "Hasta"
               Columns(1).Name =   "HASTA"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(1).Locked=   -1  'True
               Columns(2).Width=   2196
               Columns(2).Caption=   "=> Puntos"
               Columns(2).Name =   "PUNTOS"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(2).Locked=   -1  'True
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "INDI"
               Columns(3).Name =   "INDI"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(3).Locked=   -1  'True
               _ExtentX        =   8176
               _ExtentY        =   3625
               _StockProps     =   79
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
      Begin VB.PictureBox picSelBoolean 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   255
         Left            =   -74700
         ScaleHeight     =   255
         ScaleWidth      =   8040
         TabIndex        =   8
         Top             =   660
         Width           =   8040
         Begin VB.OptionButton optAutomatico 
            Caption         =   "Autom�tico"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   1875
            TabIndex        =   10
            Top             =   60
            Value           =   -1  'True
            Width           =   1515
         End
         Begin VB.OptionButton optManual2 
            Caption         =   "DManual"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   3825
            TabIndex        =   9
            Top             =   60
            Width           =   1515
         End
      End
      Begin VB.PictureBox picSelNumerico 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -74175
         ScaleHeight     =   375
         ScaleWidth      =   7140
         TabIndex        =   3
         Top             =   600
         Width           =   7140
         Begin VB.OptionButton optContinua 
            Caption         =   "DEscala continua"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   375
            TabIndex        =   7
            Top             =   120
            Value           =   -1  'True
            Width           =   1515
         End
         Begin VB.OptionButton optDiscreta 
            Caption         =   "DEscala discreta"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   2100
            TabIndex        =   6
            Top             =   120
            Width           =   1515
         End
         Begin VB.OptionButton optFormula 
            Caption         =   "DF�rmula"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   3855
            TabIndex        =   5
            Top             =   120
            Width           =   1155
         End
         Begin VB.OptionButton optManual 
            Caption         =   "DManual"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   5055
            TabIndex        =   4
            Top             =   120
            Width           =   1155
         End
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGrupo 
         Height          =   285
         Left            =   -74760
         TabIndex        =   101
         Top             =   600
         Width           =   4485
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   47031
         Columns(1).Width=   6482
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   47031
         _ExtentX        =   7911
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   49344
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.PictureBox picLibre 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   4305
         Left            =   180
         ScaleHeight     =   4305
         ScaleWidth      =   8655
         TabIndex        =   25
         Top             =   960
         Visible         =   0   'False
         Width           =   8655
         Begin VB.OptionButton optValorAltoL 
            Caption         =   "DValores altos/lejanos"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   4020
            TabIndex        =   27
            ToolTipText     =   "Lejanos para el caso de fechas"
            Top             =   2820
            Width           =   1935
         End
         Begin VB.OptionButton optValorBajoL 
            Caption         =   "DValores bajos/recientes"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   1740
            TabIndex        =   26
            Top             =   2820
            Value           =   -1  'True
            Width           =   2295
         End
         Begin VB.Label lblIntroL 
            Caption         =   "DIntroducci�n"
            Height          =   195
            Left            =   120
            TabIndex        =   49
            Top             =   1440
            Width           =   1335
         End
         Begin VB.Label lblMin 
            Caption         =   "DM�nimo"
            Height          =   240
            Left            =   3450
            TabIndex        =   48
            Top             =   1440
            Width           =   1005
         End
         Begin VB.Label lblMax 
            Caption         =   "DM�ximo"
            Height          =   255
            Left            =   3450
            TabIndex        =   47
            Top             =   1740
            Width           =   1275
         End
         Begin VB.Label lblMaxtxt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   5010
            TabIndex        =   46
            Top             =   1740
            Width           =   1335
         End
         Begin VB.Label lblMintxt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   5010
            TabIndex        =   45
            Top             =   1380
            Width           =   1335
         End
         Begin VB.Label lblIntroLtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1740
            TabIndex        =   44
            Top             =   1395
            Width           =   1335
         End
         Begin VB.Label lblObligaL 
            Caption         =   "(DObligatorio )"
            Height          =   195
            Left            =   3375
            TabIndex        =   43
            Top             =   2250
            Width           =   1635
         End
         Begin VB.Label lblAmbitoL 
            Caption         =   "DAmbito"
            Height          =   195
            Left            =   120
            TabIndex        =   42
            Top             =   2175
            Width           =   1335
         End
         Begin VB.Label lblAmbitoLtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1740
            TabIndex        =   41
            Top             =   2160
            Width           =   1335
         End
         Begin VB.Label lblExterL 
            Caption         =   "( Externo )"
            Height          =   195
            Left            =   3390
            TabIndex        =   40
            Top             =   870
            Width           =   1635
         End
         Begin VB.Line Line3 
            Index           =   0
            X1              =   30
            X2              =   8550
            Y1              =   1200
            Y2              =   1200
         End
         Begin VB.Label lblPrefeL 
            Caption         =   "DPreferencia"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   90
            TabIndex        =   39
            Top             =   2820
            Width           =   1395
         End
         Begin VB.Label lblRelacionL 
            Caption         =   "DRelaci�n con el precio"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   90
            TabIndex        =   38
            Top             =   3420
            Width           =   1575
         End
         Begin VB.Label lblAplicarPrecioL 
            Caption         =   "DAplicar a precio"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   120
            TabIndex        =   37
            Top             =   3900
            Width           =   1455
         End
         Begin VB.Label lblTipoL 
            Caption         =   "Tipo de dato"
            Height          =   255
            Left            =   120
            TabIndex        =   36
            Top             =   840
            Width           =   1395
         End
         Begin VB.Label lblCodL 
            Caption         =   "DC�digo"
            Height          =   195
            Left            =   90
            TabIndex        =   35
            Top             =   30
            Width           =   1455
         End
         Begin VB.Label lblDenL 
            Caption         =   "DDenominaci�n"
            Height          =   255
            Left            =   90
            TabIndex        =   34
            Top             =   450
            Width           =   1215
         End
         Begin VB.Label lblCodLtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1740
            TabIndex        =   33
            Top             =   0
            Width           =   1335
         End
         Begin VB.Label lblTipoLtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1740
            TabIndex        =   32
            Top             =   840
            Width           =   1335
         End
         Begin VB.Label lblDenLtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1740
            TabIndex        =   31
            Top             =   405
            Width           =   6795
         End
         Begin VB.Line Line4 
            X1              =   0
            X2              =   8520
            Y1              =   2550
            Y2              =   2550
         End
         Begin VB.Line Line5 
            X1              =   0
            X2              =   8520
            Y1              =   3240
            Y2              =   3240
         End
         Begin VB.Label lblRelacionLtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1770
            TabIndex        =   30
            Top             =   3390
            Width           =   1335
         End
         Begin VB.Label lblAplicarLtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1800
            TabIndex        =   29
            Top             =   3840
            Width           =   3135
         End
         Begin VB.Label lblAplicarDefecL 
            Caption         =   "(DAplicar por defecto)"
            Height          =   195
            Left            =   3420
            TabIndex        =   28
            Top             =   3450
            Width           =   1635
         End
      End
      Begin VB.PictureBox picSino 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   2685
         Left            =   180
         ScaleHeight     =   2685
         ScaleWidth      =   8655
         TabIndex        =   11
         Top             =   960
         Visible         =   0   'False
         Width           =   8655
         Begin VB.OptionButton optValorBajoSN 
            Caption         =   "DValores bajos/recientes"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   1680
            TabIndex        =   13
            Top             =   1620
            Value           =   -1  'True
            Width           =   2295
         End
         Begin VB.OptionButton optValorAltoSN 
            Caption         =   "DValores altos/lejanos"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   3960
            TabIndex        =   12
            ToolTipText     =   "Lejanos para el caso de fechas"
            Top             =   1620
            Width           =   1935
         End
         Begin VB.Line Line7 
            X1              =   30
            X2              =   8550
            Y1              =   1470
            Y2              =   1470
         End
         Begin VB.Label lblAmbitoSNtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1740
            TabIndex        =   24
            Top             =   1020
            Width           =   1335
         End
         Begin VB.Label lblAmbitoSN 
            Caption         =   "DAmbito"
            Height          =   195
            Left            =   120
            TabIndex        =   23
            Top             =   1035
            Width           =   1335
         End
         Begin VB.Label lblObliSN 
            Caption         =   "(DObligatorio )"
            Height          =   195
            Left            =   3390
            TabIndex        =   22
            Top             =   1080
            Width           =   1635
         End
         Begin VB.Label lblDenSNtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   5010
            TabIndex        =   21
            Top             =   60
            Width           =   3495
         End
         Begin VB.Label lblTipoSNtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1740
            TabIndex        =   20
            Top             =   420
            Width           =   1335
         End
         Begin VB.Label lblCodSNtxt 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1740
            TabIndex        =   19
            Top             =   0
            Width           =   1335
         End
         Begin VB.Label lblDenSN 
            Caption         =   "DDenominaci�n"
            Height          =   255
            Left            =   3420
            TabIndex        =   18
            Top             =   60
            Width           =   1215
         End
         Begin VB.Label lblCodSN 
            Caption         =   "DC�digo"
            Height          =   195
            Left            =   90
            TabIndex        =   17
            Top             =   30
            Width           =   1455
         End
         Begin VB.Label lblTipoSN 
            Caption         =   "DTipo de dato"
            Height          =   255
            Left            =   120
            TabIndex        =   16
            Top             =   450
            Width           =   1395
         End
         Begin VB.Label lblPrefeSN 
            Caption         =   "DPreferencia"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   30
            TabIndex        =   15
            Top             =   1650
            Width           =   1395
         End
         Begin VB.Line Line6 
            X1              =   0
            X2              =   8520
            Y1              =   810
            Y2              =   810
         End
         Begin VB.Label lblExterSN 
            Caption         =   "( DExterno )"
            Height          =   195
            Left            =   3390
            TabIndex        =   14
            Top             =   480
            Width           =   1635
         End
      End
      Begin VB.Shape Shape1 
         Height          =   3735
         Left            =   -74700
         Top             =   1350
         Width           =   8355
      End
      Begin VB.Label lblTipoPond 
         Caption         =   "DTipo:"
         Height          =   315
         Left            =   -74700
         TabIndex        =   2
         Top             =   420
         Width           =   2535
      End
      Begin VB.Label lblDefNivel 
         Caption         =   "DDefinido a nivel de"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   510
         Width           =   1695
      End
   End
End
Attribute VB_Name = "frmDetAtribProce"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variable de control de flujo de proceso
Public g_oProceso As CProceso
Public g_oAtributo As Catributo
Public g_adoRes As Ador.Recordset
Public g_oGrupoSeleccionado As CGrupo
Public g_sOrigen As String
Public g_oProveedores As CProveedores
Public g_oOrigen As Form
Private m_picActual As vb.PictureBox
Private m_idAtribProce As Long
Private m_sAmbito(3) As String
Private m_sTipo(4) As String
Private m_sAtrib(4) As String
Private m_sIntro(2) As String
Private m_sValor As String
Private m_sPond As String
Private m_sDefAmbito(3) As String

Private m_iPond As Integer
Private m_iIndiCole As Integer

Private m_sIdiExterno As String
Private m_sIdiInterno As String

Public m_bDescargarFrm As Boolean
Dim m_bActivado As Boolean

Private m_sMsgError As String
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DETATRIBPROCE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        sstabgeneral.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabgeneral.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        lblDefNivel.caption = Ador(0).Value
        Ador.MoveNext
        lblCod.caption = Ador(0).Value
        lblCodL.caption = Ador(0).Value
        lblCodSN.caption = Ador(0).Value
        Ador.MoveNext
        lblDen.caption = Ador(0).Value
        lblDenL.caption = Ador(0).Value
        lblDenSN.caption = Ador(0).Value
        Ador.MoveNext
        lblTipo.caption = Ador(0).Value
        lblTipoL.caption = Ador(0).Value
        lblTipoSN.caption = Ador(0).Value
        Ador.MoveNext
        lblIntro.caption = Ador(0).Value
        lblIntroL.caption = Ador(0).Value
        Ador.MoveNext
        lblAmbito.caption = Ador(0).Value
        lblAmbitoL.caption = Ador(0).Value
        lblAmbitoSN.caption = Ador(0).Value
        Ador.MoveNext
        lblPrefe.caption = Ador(0).Value
        lblPrefeL.caption = Ador(0).Value
        lblPrefeSN.caption = Ador(0).Value
        Ador.MoveNext
        lblRelacionPrecio.caption = Ador(0).Value
        lblRelacionL.caption = Ador(0).Value
        Ador.MoveNext
        lblAplicarPrecio.caption = Ador(0).Value
        lblAplicarPrecioL.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiExterno = Ador(0).Value
        lblExter.caption = Ador(0).Value
        lblExterL.caption = Ador(0).Value
        lblExterSN.caption = Ador(0).Value
        Ador.MoveNext
        lblObliga.caption = Ador(0).Value
        lblObligaL.caption = Ador(0).Value
        lblObliSN.caption = Ador(0).Value
        Ador.MoveNext
        lblAplicarDefec.caption = Ador(0).Value
        lblAplicarDefecL.caption = Ador(0).Value
        Ador.MoveNext
        OptValorBajo.caption = Ador(0).Value
        optValorBajoL.caption = Ador(0).Value
        optValorBajoSN.caption = Ador(0).Value
        Ador.MoveNext
        OptValorAlto.caption = Ador(0).Value
        optValorAltoL.caption = Ador(0).Value
        optValorAltoSN.caption = Ador(0).Value
        Ador.MoveNext
        lblMin.caption = Ador(0).Value
        Ador.MoveNext
        lblMax.caption = Ador(0).Value
        Ador.MoveNext
        lblLista.caption = Ador(0).Value
        Ador.MoveNext
        m_sIntro(0) = Ador(0).Value
        Ador.MoveNext
        m_sIntro(1) = Ador(0).Value
        Ador.MoveNext
        m_sAmbito(1) = Ador(0).Value
        m_sDefAmbito(1) = Ador(0).Value
        Ador.MoveNext
        m_sAmbito(2) = Ador(0).Value
        Ador.MoveNext
        m_sAmbito(3) = Ador(0).Value
        Ador.MoveNext
        m_sTipo(1) = Ador(0).Value
        Ador.MoveNext
        m_sTipo(2) = Ador(0).Value
        Ador.MoveNext
        m_sTipo(3) = Ador(0).Value
        Ador.MoveNext
        m_sTipo(4) = Ador(0).Value
        Ador.MoveNext
        optContinua.caption = Ador(0).Value
        Ador.MoveNext
        optDiscreta.caption = Ador(0).Value
        Ador.MoveNext
        optFormula.caption = Ador(0).Value
        Ador.MoveNext
        optManual.caption = Ador(0).Value
        optManual2.caption = Ador(0).Value
        Ador.MoveNext
        sdbgContinua.Columns("DESDE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgContinua.Columns("HASTA").caption = Ador(0).Value
        Ador.MoveNext
        sdbgContinua.Columns("PUNTOS").caption = Ador(0).Value
        sdbgDiscreta.Columns("PUNTOS").caption = Ador(0).Value
        Ador.MoveNext
        sdbgDiscreta.Columns("VALOR").caption = Ador(0).Value
        lblValor.caption = Ador(0).Value
        Ador.MoveNext
        optAutomatico.caption = Ador(0).Value
        Ador.MoveNext
        lblSi.caption = Ador(0).Value
        Ador.MoveNext
        lblNo.caption = Ador(0).Value
        Ador.MoveNext
        lblPuntos1.caption = Ador(0).Value
        lblPuntos2.caption = Ador(0).Value
        Ador.MoveNext
        lblPond0.caption = Ador(0).Value
        lblPond0b.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        lblTextoManual.caption = Ador(0).Value
        lblManualBoo.caption = Ador(0).Value
        Ador.MoveNext
        lblTipoPond.caption = Ador(0).Value
        Ador.MoveNext
        m_sAtrib(0) = Ador(0).Value
        Ador.MoveNext
        m_sAtrib(1) = Ador(0).Value
        Ador.MoveNext
        m_sAtrib(2) = Ador(0).Value
        Ador.MoveNext
        m_sAtrib(3) = Ador(0).Value
        
        Ador.MoveNext
        sstabgeneral.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        m_sDefAmbito(3) = Ador(0).Value
        Ador.MoveNext
        m_sValor = Ador(0).Value
        sdbgValores.Columns("VAL").caption = Ador(0).Value
        Ador.MoveNext
        m_sPond = Ador(0).Value
        sdbgValores.Columns("POND").caption = Ador(0).Value
        Ador.MoveNext
        m_sDefAmbito(2) = Ador(0).Value
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        sdbgValores.Columns("PROV").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiInterno = Ador(0).Value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

''' <summary>
''' Guarda los valores de la ponderaci�n manual en la base de datos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    'Guarda los valores de la ponderaci�n manual en la base de datos
    Dim i As Integer
    Dim oValoresPond As CAtributosOfertados
    Dim j As Integer
    Dim teserror As TipoErrorSummit
    Dim sCodGrupo As String
    Dim scod1 As String
    
    'Rellena la colecci�n
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oValoresPond = oFSGSRaiz.Generar_CAtributosOfertados
    
    Select Case g_oAtributo.ambito
    
        Case AmbProceso   '�mbito proceso
            For i = 0 To sdbgValores.Rows - 1  'Se recorre los proveedores
                If Not g_oProceso.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)) Is Nothing Then
                    If Not g_oProceso.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)).AtribProcOfertados Is Nothing Then
                        oValoresPond.Add g_oAtributo.idAtribProce, g_oProceso.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)), , , , , , sdbgValores.Columns("POND").CellValue(i), , , , , True
                    End If
                End If
            Next i
            
            teserror = oValoresPond.GuardarPonderacion(AmbProceso)
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            Else
                'Guarda los datos en la colecci�n de frmADJ
                For i = 0 To sdbgValores.Rows - 1
                    If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)) Is Nothing Then
                        If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)).AtribProcOfertados Is Nothing Then
                            g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)).AtribProcOfertados.Item(CStr(g_oAtributo.idAtribProce)).ValorPond = sdbgValores.Columns("POND").CellValue(i)
                        End If
                    End If
                Next i
            End If
        
        
        Case AmbGrupo
            sCodGrupo = sdbcGrupo.Columns(0).Value
            For i = 0 To sdbgValores.Rows - 1
                If Not g_oProceso.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)) Is Nothing Then
                    If Not g_oProceso.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)).AtribGrOfertados Is Nothing Then
                        oValoresPond.Add g_oAtributo.idAtribProce, g_oProceso.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)), , sCodGrupo, , , , sdbgValores.Columns("POND").CellValue(i), , , , , True
                    End If
                End If
            Next i

            teserror = oValoresPond.GuardarPonderacion(AmbGrupo)
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            Else
                'Guarda los datos en la colecci�n de frmADJ
                For i = 0 To sdbgValores.Rows - 1
                    If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)) Is Nothing Then
                        If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)).AtribGrOfertados Is Nothing Then
                            scod1 = sCodGrupo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(sCodGrupo))
                            scod1 = scod1 & CStr(g_oAtributo.idAtribProce)
                            g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sdbgValores.Columns("CodProv").CellValue(i)).AtribGrOfertados.Item(scod1).ValorPond = sdbgValores.Columns("POND").CellValue(i)
                        End If
                    End If
                Next i
            End If
            
        Case AmbItem
            sCodGrupo = sdbcGrupo.Columns(0).Value
            For i = 0 To sdbgValoresItem.Rows - 1
                For j = 1 To sdbgValoresItem.Groups.Count - 1
                    If Not g_oProceso.Grupos.Item(sCodGrupo).UltimasOfertas.Item(Trim(sdbgValoresItem.Groups(j).TagVariant)) Is Nothing Then
                        If Not g_oProceso.Grupos.Item(sCodGrupo).UltimasOfertas.Item(Trim(sdbgValoresItem.Groups(j).TagVariant)).AtribItemOfertados Is Nothing Then
                            oValoresPond.Add g_oAtributo.idAtribProce, g_oProceso.Ofertas.Item(sdbgValoresItem.Groups(j).TagVariant), sdbgValoresItem.Columns("ID").CellValue(i), , , , , sdbgValoresItem.Columns("Pond" & sdbgValoresItem.Groups(j).TagVariant).CellValue(i), , , , , True
                        End If
                    End If
                Next j
            Next i

            teserror = oValoresPond.GuardarPonderacion(AmbItem)
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            Else
                'Guarda los datos en la colecci�n de frmADJ
                For i = 0 To sdbgValoresItem.Rows - 1
                    For j = 1 To sdbgValoresItem.Groups.Count - 1
                        If Not g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(sCodGrupo).UltimasOfertas.Item(Trim(sdbgValoresItem.Groups(j).TagVariant)) Is Nothing Then
                            If Not g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(sCodGrupo).UltimasOfertas.Item(Trim(sdbgValoresItem.Groups(j).TagVariant)).AtribItemOfertados Is Nothing Then
                                If Not g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(sCodGrupo).UltimasOfertas.Item(Trim(sdbgValoresItem.Groups(j).TagVariant)).AtribItemOfertados.Item(CStr(sdbgValoresItem.Columns("ID").CellValue(i) & "$" & g_oAtributo.idAtribProce)) Is Nothing Then
                                    g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(sCodGrupo).UltimasOfertas.Item(Trim(sdbgValoresItem.Groups(j).TagVariant)).AtribItemOfertados.Item(CStr(sdbgValoresItem.Columns("ID").CellValue(i) & "$" & g_oAtributo.idAtribProce)).ValorPond = sdbgValoresItem.Columns("Pond" & sdbgValoresItem.Groups(j).TagVariant).CellValue(i)
                                End If
                            End If
                        End If
                    Next j
                Next i
            End If
    End Select
        
        
    Set oValoresPond = Nothing
    
    
    'Modo consulta
    picConsulta.Visible = True
    picEdicion.Visible = False
    
    If g_oAtributo.ambito = AmbItem Then
        sdbgValoresItem.AllowUpdate = False
    Else
        sdbgValores.AllowUpdate = False
    End If
    
    'Desbloquea los tabs
    sstabgeneral.TabEnabled(0) = True
    sstabgeneral.TabEnabled(1) = True
    sdbcGrupo.Enabled = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCancelar_Click()
    'Visualiza el bot�n de consulta
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picConsulta.Visible = True
    picEdicion.Visible = False
    
    'Vuelve a cargar la grid con los datos originales
    Select Case g_oAtributo.ambito
        Case AmbProceso
            CargarGridValoresProceso
            sdbgValores.AllowUpdate = False
            
        Case AmbGrupo
            CargarGridValoresGrupo
            sdbgValores.AllowUpdate = False
            
        Case AmbItem
            RellenarValoresItem
            sdbgValoresItem.AllowUpdate = False
    End Select
    
    'Desbloquea los tabs
    sstabgeneral.TabEnabled(0) = True
    sstabgeneral.TabEnabled(1) = True
    sdbcGrupo.Enabled = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdModificar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picEdicion.Visible = True
    picConsulta.Visible = False
    
    'Deja actualizar la grid correspondiente
    If g_oAtributo.ambito = AmbItem Then
        sdbgValoresItem.AllowUpdate = True
    Else
        sdbgValores.AllowUpdate = True
    End If

    'Si est� en edici�n bloquea el tab
    sstabgeneral.TabEnabled(0) = False
    sstabgeneral.TabEnabled(1) = False
    sdbcGrupo.Enabled = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "cmdModificar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bActivado Then
        m_bActivado = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    
    Me.Height = 6315
    Me.Width = 9240
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    DoEvents
    
    PonerFieldSeparator Me
    
    
    If g_sOrigen = "frmOFEREC" Or g_sOrigen = "frmDatExtModif" Then
        lblDefNivel.Visible = False
        sdbcGrupos.Visible = False
    Else
        lblDefNivel.Visible = True
        sdbcGrupos.Visible = True
        CargarComboGruposAtri
    End If
    
    
    CargarAtributoDeGrupo
    
    If g_sOrigen <> "frmDatExtModif" Then
        m_idAtribProce = g_oAtributo.idAtribProce
    
    
        'Dependiendo del valor del parametro que indica si se va a usar la ponderaci�n
        'y de si el proceso la tiene habilitada
        'en los atributos se muestra o no el tab de ponderaci�n
        If gParametrosGenerales.gbUsarPonderacion = True And g_oProceso.UsarPonderacion = True Then
            sstabgeneral.TabVisible(1) = True
            
             'Si se le llama desde el formulario de la comparativa y es un atributo con
            'ponderaci�n manual muestra el tab de los valores del proveedor
            If g_sOrigen = "frmADJ" And g_oAtributo.TipoPonderacion = Manual Then
                sstabgeneral.TabVisible(2) = True
                
                VisualizarValoresManual
            Else
                sstabgeneral.TabVisible(2) = False
            End If
            
        Else
            sstabgeneral.TabVisible(1) = False
            sstabgeneral.TabVisible(2) = False
        End If
    Else
        sstabgeneral.TabVisible(1) = False
        sstabgeneral.TabVisible(2) = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub Form_Unload(Cancel As Integer)
 ''' * Objetivo: Descargar el formulario
 
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
    m_bDescargarFrm = False
    oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set g_oProceso = Nothing
    Set g_oAtributo = Nothing
    Set g_adoRes = Nothing
    Set g_oGrupoSeleccionado = Nothing
    Set g_oProveedores = Nothing
    Set g_oOrigen = Nothing
    g_sOrigen = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGrupo_CloseUp()
    Dim oAtributo As Catributo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGrupo.Text = "" Then Exit Sub
    
    sdbcGrupo.Value = sdbcGrupo.Columns(0).Value
    If sdbcGrupo.Columns(1).Value <> "" Then
        sdbcGrupo.Value = sdbcGrupo.Value & " - " & sdbcGrupo.Columns(1).Value
    End If
    Set g_oGrupoSeleccionado = g_oProceso.Grupos.Item(sdbcGrupo.Columns(0).Value)
    
    
    
    'Carga las grids
    If g_oAtributo.ambito = AmbGrupo Then
        If Not IsNull(g_oAtributo.codgrupo) Then
            If g_oAtributo.codgrupo <> g_oGrupoSeleccionado.Codigo Then
               ' If g_oGrupoSeleccionado.ATRIBUTOS Is Nothing Then
                '    g_oGrupoSeleccionado.
                If Not g_oProceso.AtributosGrupo Is Nothing Then
                    For Each oAtributo In g_oProceso.AtributosGrupo
                        If oAtributo.codgrupo = g_oGrupoSeleccionado.Codigo Then
                            If oAtributo.Cod = g_oAtributo.Cod Then
                                Set g_oAtributo = oAtributo
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If
        End If
        CargarGridValoresGrupo
    ElseIf g_oAtributo.ambito = AmbItem Then
        If Not IsNull(g_oAtributo.codgrupo) Then
            If g_oAtributo.codgrupo <> g_oGrupoSeleccionado.Codigo Then
               ' If g_oGrupoSeleccionado.ATRIBUTOS Is Nothing Then
                '    g_oGrupoSeleccionado.
                If Not g_oProceso.AtributosItem Is Nothing Then
                    For Each oAtributo In g_oProceso.AtributosItem
                        If oAtributo.codgrupo = g_oGrupoSeleccionado.Codigo Then
                            If oAtributo.Cod = g_oAtributo.Cod Then
                                Set g_oAtributo = oAtributo
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If
        End If
        RellenarValoresItem
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "sdbcGrupo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGrupo_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGrupo.DataFieldList = "Column 0"
    sdbcGrupo.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "sdbcGrupo_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGrupo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    sdbcGrupo.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbcGrupo.Rows - 1
            bm = sdbcGrupo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGrupo.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGrupo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "sdbcGrupo_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcGrupos_CloseUp()
Dim oAtributo As Catributo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGrupos.Text = "" Then Exit Sub

    If sdbcGrupos.Value <> m_sAmbito(1) Then
        sdbcGrupos.Value = sdbcGrupos.Columns(0).Value
        If sdbcGrupos.Columns(1).Value <> "" Then
            sdbcGrupos.Value = sdbcGrupos.Value & " - " & sdbcGrupos.Columns(1).Value
        End If
        Set g_oGrupoSeleccionado = g_oProceso.Grupos.Item(sdbcGrupos.Columns(0).Value)
        Set g_oGrupoSeleccionado.proceso = g_oProceso
        g_oGrupoSeleccionado.CargarTodosLosAtributos
        For Each oAtributo In g_oGrupoSeleccionado.ATRIBUTOS
            If oAtributo.Id = g_oAtributo.Id Then
               Set g_oAtributo = oAtributo
               Exit For
            End If
        Next

        CargarAtributoDeGrupo

        If g_sOrigen = "frmADJ" And CInt(sdbcGrupos.Columns(2).Value) = Manual And gParametrosGenerales.gbUsarPonderacion = True And g_oProceso.UsarPonderacion = True Then
            sstabgeneral.TabVisible(2) = True
            VisualizarValoresManual
        Else
            sstabgeneral.TabVisible(2) = False
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "sdbcGrupos_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGrupos_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGrupos.DataFieldList = "Column 0"
    sdbcGrupos.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "sdbcGrupos_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGrupoS_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcGrupos.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbcGrupos.Rows - 1
            bm = sdbcGrupos.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGrupos.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGrupos.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub CargarComboGruposAtri()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oProceso Is Nothing Then Exit Sub
    
    
    sdbcGrupos.RemoveAll
    
    Set g_adoRes = g_oAtributo.CargarDeGrupos
    
    While Not g_adoRes.EOF
        If IsNull(g_adoRes("GRUPO").Value) Then
            sdbcGrupos.AddItem m_sAmbito(1)
        Else
            sdbcGrupos.AddItem NullToStr(g_adoRes("GRUPO").Value) & Chr(m_lSeparador) & NullToStr(g_adoRes("DEN").Value) & Chr(m_lSeparador) & NullToStr(g_adoRes("POND").Value)
        End If
        g_adoRes.MoveNext
    Wend

    'Cierra el recordset
    g_adoRes.Close
    Set g_adoRes = Nothing
    
    If sdbcGrupos.Rows <= 1 Then
        picNivel.Enabled = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "CargarComboGruposAtri", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
Private Sub CargarAtributoDeGrupo()
Dim oLista As CValorPond

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With g_oAtributo
          If .Tipo = TiposDeAtributos.TipoFecha Or .Tipo = TiposDeAtributos.TipoNumerico Or .Tipo = TiposDeAtributos.TipoString Then
                If .TipoIntroduccion = IntroLibre Then
                   picLibre.Visible = True
                   picSelec.Visible = False
                   picBoolean.Visible = False
                   lblCodLtxt.caption = .Cod
                   lblDenLtxt.caption = .Den
                   lblDenLtxt.ToolTipText = .Den
                   If .Tipo = TiposDeAtributos.TipoFecha Then
                        lblTipoLtxt.caption = m_sTipo(3)
                   Else
                        If .Tipo = TiposDeAtributos.TipoNumerico Then
                            lblTipoLtxt.caption = m_sTipo(2)
                        Else
                            lblTipoLtxt.caption = m_sTipo(1)
                        End If
                   End If
                   If .TipoIntroduccion = IntroLibre Then
                        lblIntroLtxt.caption = m_sIntro(0)
                   Else
                        lblIntroLtxt.caption = m_sIntro(1)
                   End If
                   If g_sOrigen = "frmOFEREC" And (g_oAtributo.PrecioFormula = "+" Or g_oAtributo.PrecioFormula = "-") Then
                      If frmOFERec.g_oOfertaSeleccionada.CodMon <> g_oProceso.MonCod Then
                           lblMintxt.caption = NullToStr(.Minimo * frmOFERec.g_oOfertaSeleccionada.Cambio)
                           lblMaxtxt.caption = NullToStr(.Maximo * frmOFERec.g_oOfertaSeleccionada.Cambio)
                      Else
                           lblMintxt.caption = NullToStr(.Minimo)
                           lblMaxtxt.caption = NullToStr(.Maximo)
                      End If
                   Else
                        lblMintxt.caption = NullToStr(.Minimo)
                        lblMaxtxt.caption = NullToStr(.Maximo)
                   End If
                   If .PreferenciaValorBajo Then
                        optValorBajoL.Value = True
                   Else
                        optValorAltoL.Value = True
                   End If
                   If g_sOrigen <> "frmDatExtModif" Then
                        lblAmbitoL.Visible = True
                        lblAmbitoLtxt.Visible = True
                        lblRelacionL.Visible = True
                        lblRelacionLtxt.Visible = True
                        lblAplicarPrecioL.Visible = True
                        lblAplicarLtxt.Visible = True
                        lblExterL.Visible = True
                        If .ambito = AmbGrupo Then
                             lblAmbitoLtxt.caption = m_sAmbito(2)
                        Else
                             If .ambito = AmbProceso Then
                                 lblAmbitoLtxt.caption = m_sAmbito(1)
                             Else
                                 lblAmbitoLtxt.caption = m_sAmbito(3)
                             End If
                        End If
                        If .interno Then
                            lblExterL.caption = m_sIdiInterno
                        Else
                            lblExterL.caption = m_sIdiExterno
                        End If
                        If .Obligatorio Then
                            lblObligaL.Visible = True
                        Else
                            lblObligaL.Visible = False
                        End If
                        lblRelacionLtxt.caption = NullToStr(.PrecioFormula)
                        Select Case .PrecioAplicarA
                             Case 0: lblAplicarLtxt.caption = m_sAtrib(0) 'Total de la oferta Precio unitario del item
                             Case 1: lblAplicarLtxt.caption = m_sAtrib(1) 'Total del grupo
                             Case 2: lblAplicarLtxt.caption = m_sAtrib(2) 'Total del item
                             Case 3: lblAplicarLtxt.caption = m_sAtrib(3) ' Precio unitario del item
                             Case Null: lblAplicarLtxt.caption = ""
                        End Select
                        If .PrecioDefecto = 1 Then
                            lblAplicarDefecL.Visible = True
                        Else
                            lblAplicarDefecL.Visible = False
                        End If
                   Else
                        lblAmbitoL.Visible = False
                        lblAmbitoLtxt.Visible = False
                        lblObligaL.Visible = False
                        lblRelacionL.Visible = False
                        lblRelacionLtxt.Visible = False
                        lblAplicarDefecL.Visible = False
                        lblAplicarPrecioL.Visible = False
                        lblAplicarLtxt.Visible = False
                        lblExterL.Visible = False
                   End If
                Else
                   picLibre.Visible = False
                   picSelec.Visible = True
                   picBoolean.Visible = False
                   lblCodtxt.caption = .Cod
                   lblDentxt.caption = .Den
                   lblDentxt.ToolTipText = .Den
                   sdbcLista.RemoveAll
                    If g_oAtributo.ListaPonderacion Is Nothing Then
                        g_oAtributo.CargarListaDeValores
                    ElseIf g_oAtributo.ListaPonderacion.Count = 0 Then
                        g_oAtributo.CargarListaDeValores
                    End If
                   For Each oLista In g_oAtributo.ListaPonderacion
                        If g_sOrigen = "frmOFEREC" And (g_oAtributo.PrecioFormula = "+" Or g_oAtributo.PrecioFormula = "-") Then
                            If frmOFERec.g_oOfertaSeleccionada.CodMon <> g_oProceso.MonCod Then
                                sdbcLista.AddItem NullToStr(oLista.ValorLista * frmOFERec.g_oOfertaSeleccionada.Cambio)
                            Else
                                sdbcLista.AddItem NullToStr(oLista.ValorLista)
                            End If
                        Else
                            sdbcLista.AddItem NullToStr(oLista.ValorLista)
                        End If
                   Next
                   If .Tipo = TiposDeAtributos.TipoFecha Then
                        lblTipotxt.caption = m_sTipo(3)
                   Else
                        If .Tipo = TiposDeAtributos.TipoNumerico Then
                            lblTipotxt.caption = m_sTipo(2)
                        Else
                            lblTipotxt.caption = m_sTipo(1)
                        End If
                   End If
                   If .TipoIntroduccion = IntroLibre Then
                        lblIntrotxt.caption = m_sIntro(0)
                   Else
                        lblIntrotxt.caption = m_sIntro(1)
                   End If
                   If .PreferenciaValorBajo Then
                        OptValorBajo.Value = True
                        OptValorAlto.Value = False
                   Else
                        OptValorBajo.Value = False
                        OptValorAlto.Value = True
                   End If
                   If g_sOrigen <> "frmDatExtModif" Then
                        lblAmbito.Visible = True
                        lblAmbitotxt.Visible = True
                        lblRelacionPrecio.Visible = True
                        lblRelaciontxt.Visible = True
                        lblAplicarPrecio.Visible = True
                        lblAplicartxt.Visible = True
                        lblExter.Visible = True
                        If .ambito = AmbGrupo Then
                             lblAmbitotxt.caption = m_sAmbito(2)
                        Else
                             If .ambito = AmbProceso Then
                                 lblAmbitotxt.caption = m_sAmbito(1)
                             Else
                                 lblAmbitotxt.caption = m_sAmbito(3)
                             End If
                        End If
                        If .interno Then
                            lblExter.caption = m_sIdiInterno
                        Else
                            lblExter.caption = m_sIdiExterno
                        End If
                        If .Obligatorio Then
                            lblObliga.Visible = True
                        Else
                            lblObliga.Visible = False
                        End If
                        If .PrecioDefecto = 1 Then
                            lblAplicarDefec.Visible = True
                        Else
                            lblAplicarDefec.Visible = False
                        End If
                         lblRelaciontxt.caption = NullToStr(.PrecioFormula)
                         If .PrecioAplicarA >= 0 And .PrecioAplicarA <= 3 Then
                             lblAplicartxt.caption = m_sAtrib(.PrecioAplicarA)
                         Else
                             lblAplicartxt.caption = ""
                         End If
                   Else
                        lblAmbito.Visible = False
                        lblAmbitotxt.Visible = False
                        lblObliga.Visible = False
                        lblRelacionPrecio.Visible = False
                        lblRelaciontxt.Visible = False
                        lblAplicarDefec.Visible = False
                        lblAplicarPrecio.Visible = False
                        lblAplicartxt.Visible = False
                        lblExter.Visible = False
                   End If
                End If
            Else
                picLibre.Visible = False
                picSelec.Visible = False
                picSino.Visible = True
                lblCodSNtxt.caption = .Cod
                lblDenSNtxt.caption = .Den
                lblTipoSNtxt.caption = m_sTipo(4)
                If g_sOrigen <> "frmDatExtModif" Then
                    lblAmbitoSN.Visible = True
                    lblAmbitoSNtxt.Visible = True
                    lblExterSN.Visible = True
                    Line6.Visible = True
                    If .ambito = AmbGrupo Then
                         lblAmbitoSNtxt.caption = m_sAmbito(2)
                    Else
                         If .ambito = AmbProceso Then
                             lblAmbitoSNtxt.caption = m_sAmbito(1)
                         Else
                             lblAmbitoSNtxt.caption = m_sAmbito(3)
                         End If
                    End If
                    If .interno Then
                        lblExterSN.caption = m_sIdiInterno
                    Else
                        lblExterSN.caption = m_sIdiExterno
                    End If
                    If .Obligatorio Then
                        lblObliSN.Visible = True
                    Else
                        lblObliSN.Visible = False
                    End If
                Else
                    lblAmbitoSN.Visible = False
                    lblAmbitoSNtxt.Visible = False
                    lblObliSN.Visible = False
                    Line6.Visible = False
                    lblExterSN.Visible = False
                End If
                If .PreferenciaValorBajo Then
                     optValorBajoSN.Value = True
                     optValorAltoSN.Value = False
                Else
                     optValorBajoSN.Value = False
                     optValorAltoSN.Value = True
                End If
                   
            End If
        End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "CargarAtributoDeGrupo", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbgValores_AfterColUpdate(ByVal ColIndex As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgValores.Update
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "sdbgValores_AfterColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbgValores_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    'si no hay valor para el atributo no se puede introducir la ponderaci�n manual
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgValores.Columns("VAL").Value = "" Then
        sdbgValores.Columns("POND").Locked = True
    Else
        sdbgValores.Columns("POND").Locked = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "sdbgValores_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgValoresItem_AfterColUpdate(ByVal ColIndex As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgValoresItem.Update
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "sdbgValoresItem_AfterColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgValoresItem_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim sProve
    
    'si no hay valor para el atributo no se puede introducir la ponderaci�n manual
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgValoresItem.Grp <= 0 Then Exit Sub
    
    sProve = sdbgValoresItem.Groups.Item(sdbgValoresItem.Grp).TagVariant
    
    If sdbgValoresItem.Columns("Valor" & sProve).Value = "" Then
        sdbgValoresItem.Columns("Pond" & sProve).Locked = True
    Else
        sdbgValoresItem.Columns("Pond" & sProve).Locked = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "sdbgValoresItem_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sstabGeneral_Click(PreviousTab As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If PreviousTab = 0 And sstabgeneral.Tab = 1 Then
        Screen.MousePointer = vbHourglass
        CargarAtributoDeGrupo
        Screen.MousePointer = vbNormal
    End If
    If sstabgeneral.Tab = 1 Then
        CargarPonderacion
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "sstabGeneral_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub CargarPonderacion()
Dim oLista As CValorPond
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    optContinua.Value = False
    optDiscreta.Value = False
    optFormula.Value = False
    optManual.Value = False
    
    fraFormula.Visible = False
    fraDiscreta.Visible = False
    fraContinua.Visible = False
    fraManual.Visible = False
    fraPond0.Visible = False
    fraPond0b.Visible = False
    
    sdbgDiscreta.RemoveAll
    sdbgContinua.RemoveAll
    
    g_oAtributo.CargarPonderacion
    Select Case g_oAtributo.TipoPonderacion
        Case 0

            Select Case g_oAtributo.Tipo
                Case 4
                    fraPond0b.Visible = True
                    optAutomatico.Value = False
                    optManual2.Value = False
                    txtPuntuacion1.Text = ""
                    txtPuntuacion2.Text = ""
                    picNumerico.Visible = False
                    picBoolean.Visible = True
                    picSelNumerico.Visible = False
                    picSelBoolean.Visible = True
                
                Case Else
                    fraPond0.Visible = True
                    picNumerico.Visible = True
                    picBoolean.Visible = False
                    picSelNumerico.Visible = True
                    picSelBoolean.Visible = False
                End Select
            
        Case 1
                        
                If g_oAtributo.Tipo <> 2 And g_oAtributo.Tipo <> 3 Then
                    'mensaje de error, no coincide ponderacion con tipo de dato de atributo
                    oMensajes.ErrorPonderacionTipoDato
                    GoTo ERROR
                End If
                optContinua.Value = True
                sdbgContinua.RemoveAll

                m_iIndiCole = 1
                If g_oAtributo.ListaPonderacion Is Nothing Then
                    g_oAtributo.CargarListaDeValores
                ElseIf g_oAtributo.ListaPonderacion.Count = 0 Then
                    g_oAtributo.CargarListaDeValores
                End If
                
                For Each oLista In g_oAtributo.ListaPonderacion
                    If oLista.IdOrden = 0 Then
                        sdbgContinua.AddItem oLista.ValorDesde & Chr(m_lSeparador) & oLista.ValorHasta & Chr(m_lSeparador) & oLista.ValorPond & Chr(m_lSeparador) & m_iIndiCole
                    End If
                Next

                picNumerico.Visible = True
                picBoolean.Visible = False
                picSelNumerico.Visible = True
                picSelBoolean.Visible = False
                fraContinua.Visible = True
                fraContinua.Enabled = False
                Set m_picActual = picNumerico
                    
                
        Case 2
                If g_oAtributo.TipoIntroduccion = IntroLibre Then
                    'mensaje de error, la ponderacion no se correspondecon el tipo de introducci�n
                    oMensajes.ErrorPonderacionTipoIntroduccion
                    GoTo ERROR_Frm
                End If
                If g_oAtributo.Tipo = 4 Then
                    'mensaje de error, no coincide ponderacion con tipo de dato de atributo
                    oMensajes.ErrorPonderacionTipoDato
                    GoTo ERROR_Frm
                End If
                sdbgDiscreta.RemoveAll
                m_iIndiCole = 1
                If g_oAtributo.ListaPonderacion Is Nothing Then
                    g_oAtributo.CargarListaDeValores
                ElseIf g_oAtributo.ListaPonderacion.Count = 0 Then
                    g_oAtributo.CargarListaDeValores
                End If
                For Each oLista In g_oAtributo.ListaPonderacion
                    If oLista.IdOrden <> 0 Then
                        sdbgDiscreta.AddItem oLista.ValorLista & Chr(m_lSeparador) & oLista.ValorPond & Chr(m_lSeparador) & m_iIndiCole
                        m_iIndiCole = m_iIndiCole + 1
                    End If
                Next

                optDiscreta.Value = True
                fraDiscreta.Visible = True
                fraDiscreta.Enabled = False
                picNumerico.Visible = True
                picBoolean.Visible = False
                picSelNumerico.Visible = True
                picSelBoolean.Visible = False
                    
                Set m_picActual = picNumerico
        
        Case 3
                If g_oAtributo.Tipo <> 2 Then
                    'mensaje de error, no coincide ponderacion con tipo de atributo
                    oMensajes.ErrorPonderacionTipoDato
                    GoTo ERROR_Frm
                End If

                sdbcSigno.Visible = False
                txtSigno.Text = NullToStr(g_oAtributo.Formula)
                txtNumero.Text = NullToStr(g_oAtributo.Numero)
                optFormula.Value = True
                fraFormula.Visible = True
                fraFormula.Enabled = False
                picNumerico.Visible = True
                picBoolean.Visible = False
                picSelNumerico.Visible = True
                picSelBoolean.Visible = False
                    
                Set m_picActual = picNumerico
                    
        Case 4
                If g_oAtributo.Tipo <> 4 Then
                    optManual.Value = True
                    fraManual.Visible = True
                    picNumerico.Visible = True
                    picBoolean.Visible = False
                    picSelNumerico.Visible = True
                    picSelBoolean.Visible = False
                    
                    Set m_picActual = picNumerico
                Else
                    picNumerico.Visible = False
                    picBoolean.Visible = True
                    picSelNumerico.Visible = False
                    picSelBoolean.Visible = True
                    optAutomatico.Value = False
                    optManual2.Value = True
                    fraAutomatico.Visible = False
                    fraManualBoolean.Visible = True
                    Set m_picActual = picBoolean
                End If
                        
          Case 5
                picNumerico.Visible = False
                picBoolean.Visible = True
                picSelNumerico.Visible = False
                picSelBoolean.Visible = True
                fraAutomatico.Visible = True
                fraAutomatico.Enabled = False
                optAutomatico.Value = True
                optManual2.Value = False
                fraManualBoolean.Visible = False
                txtPuntuacion1.Text = NullToStr(g_oAtributo.ValorPondSi)
                txtPuntuacion2.Text = NullToStr(g_oAtributo.ValorPondNo)
                Set m_picActual = picBoolean
    
          
    End Select
    
    m_iPond = g_oAtributo.TipoPonderacion
    Exit Sub
    
ERROR_Frm:
    Exit Sub
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "CargarPonderacion", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Sub
   End If
End Sub

Private Sub CargarComboGruposAtribManual()
    Dim oGrupo As CGrupo
    Dim Adores As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oProceso Is Nothing Then Exit Sub
    
    sdbcGrupo.RemoveAll
    
    If IsNull(g_oAtributo.codgrupo) Then
        For Each oGrupo In g_oProceso.Grupos
            sdbcGrupo.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den
        Next
       
    Else
        Set Adores = g_oAtributo.CargarDeGrupos
        While Not Adores.EOF
            If IsNull(Adores("GRUPO").Value) Then
                If g_oAtributo.ambito = AmbGrupo Then
                    sdbcGrupo.AddItem m_sAmbito(1)
                ElseIf g_oAtributo.ambito = AmbItem Then
                    sdbcGrupo.AddItem m_sAmbito(2)
                End If
            Else
                If Adores("POND").Value = TAtributoPonderacion.Manual Then
                    sdbcGrupo.AddItem NullToStr(Adores("GRUPO").Value) & Chr(m_lSeparador) & NullToStr(Adores("DEN").Value)
                End If
            End If
            Adores.MoveNext
        Wend
    End If
    
    If sdbcGrupos.Text = m_sAmbito(1) Then
        sdbcGrupo.Value = g_oGrupoSeleccionado.Codigo & " - " & g_oGrupoSeleccionado.Den
    End If
    
    'Cierra el recordset
    If Not Adores Is Nothing Then
        Adores.Close
        Set Adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "CargarComboGruposAtribManual", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub CargarGridValoresItem()
    Dim oProve As CProveedor
    Dim iGroupIndex As Integer
    Dim ogroup As SSDataWidgets_B.Group
    Dim oColumn As SSDataWidgets_B.Column
    Dim i As Integer
    Dim bCargar As Boolean
    
    'Rellena la grid de los valores manuales de los atributos de �mbito item
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oProveedores Is Nothing Then Exit Sub
    
    'Cargamos los proveedores en los grupos de la grid.
    iGroupIndex = 1
    
    i = sdbgValoresItem.Groups.Count
    While i > 1
        sdbgValoresItem.Groups.Remove (1)
        i = sdbgValoresItem.Groups.Count
    Wend
    
    For Each oProve In g_oProveedores
        If g_oOrigen.Name = "frmADJItem" Then
            bCargar = g_oOrigen.g_oVistaSeleccionada.ConfVistasItemProv.Item(oProve.Cod).Visible
        Else
            Select Case g_oOrigen.sstabComparativa.selectedItem.Tag
                Case "General"
                    If g_oOrigen.m_oVistaSeleccionada.Vista = 0 Then
                        bCargar = True  'vista inicial
                    Else
                        bCargar = g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible
                    End If
                Case "ALL"
                    If g_oOrigen.m_oVistaSeleccionadaAll.Vista = 0 Then
                        bCargar = True 'vista inicial
                    Else
                        bCargar = g_oOrigen.m_oVistaSeleccionadaAll.ConfVistasAllProv.Item(oProve.Cod).Visible
                    End If
                Case Else
                    If g_oOrigen.m_oVistaSeleccionadaGr.Vista = 0 Then
                        bCargar = True 'es la vista inicial
                    Else
                        bCargar = g_oOrigen.m_oVistaSeleccionadaGr.ConfVistasGrProv.Item(oProve.Cod).Visible
                    End If
            End Select
        End If
    
        'A�adimos el proveedor a la grid
        If bCargar Then
            sdbgValoresItem.Groups.Add iGroupIndex
            Set ogroup = sdbgValoresItem.Groups(iGroupIndex)
            ogroup.caption = oProve.Cod
            ogroup.Width = 1900
            ogroup.AllowSizing = True
            ogroup.CaptionAlignment = ssCaptionAlignmentCenter
            ogroup.HeadStyleSet = "Cabecera"
            
            ogroup.Columns.Add 0
            Set oColumn = ogroup.Columns(0)
            oColumn.Name = "Valor" & oProve.Cod
            oColumn.caption = m_sValor
            oColumn.Alignment = ssCaptionAlignmentLeft
            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
            oColumn.Visible = True
            oColumn.StyleSet = "Amarillo"
            oColumn.Locked = True
            
            ogroup.Columns.Add 1
            Set oColumn = ogroup.Columns(1)
            oColumn.Name = "Pond" & oProve.Cod
            oColumn.caption = m_sPond
            oColumn.Alignment = ssCaptionAlignmentLeft
            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
            If g_oProceso.UsarPonderacion = False Then
                oColumn.Visible = False
            Else
                oColumn.Visible = True
            End If
            
            ogroup.Columns.Add 2
            Set oColumn = ogroup.Columns(2)
            oColumn.Name = "Cod" & oProve.Cod
            oColumn.Visible = False
            
            ogroup.TagVariant = oProve.Cod
            iGroupIndex = iGroupIndex + 1
        End If
    Next
    
    'Ahora a�ade las filas a la grid
    RellenarValoresItem
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "CargarGridValoresItem", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub VisualizarValoresManual()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case g_oAtributo.ambito
        Case AmbProceso
            sdbgValores.Visible = True
            sdbgValoresItem.Visible = False
            sdbcGrupo.Visible = False
            sdbgValores.Top = 600
            sdbgValores.Height = sdbgValores.Height + 300
            CargarGridValoresProceso
            If g_oProceso.UsarPonderacion = False Then
                sdbgValores.Columns("VAL").Width = sdbgValores.Columns("VAL").Width + sdbgValores.Columns("POND").Width - 200
                sdbgValores.Columns("POND").Visible = False
            Else
                sdbgValores.Columns("POND").Visible = True
            End If
            
        Case AmbGrupo
            sdbgValores.Visible = True
            sdbgValoresItem.Visible = False
            sdbcGrupo.Visible = True
            sdbgValores.Top = 960
            sdbgValores.Height = 4095
            CargarComboGruposAtribManual
            CargarGridValoresGrupo
            
            'Carga la grid con el grupo correspondiente
            sdbcGrupo.Value = sdbcGrupo.Columns(0).Value
            If g_oGrupoSeleccionado Is Nothing Then
                sdbcGrupo.Value = sdbcGrupo.Columns(0).Value & " - " & sdbcGrupo.Columns(1).Value
                Set g_oGrupoSeleccionado = g_oProceso.Grupos.Item(sdbcGrupo.Columns(0).Value)
            Else
                sdbcGrupo.Value = g_oGrupoSeleccionado.Codigo & " - " & g_oGrupoSeleccionado.Den
                sdbcGrupo.Columns(0).Value = g_oGrupoSeleccionado.Codigo
                sdbcGrupo.Columns(1).Value = g_oGrupoSeleccionado.Den
            End If
            
            If g_oProceso.UsarPonderacion = False Then
                sdbgValores.Columns("VAL").Width = sdbgValores.Columns("VAL").Width + sdbgValores.Columns("POND").Width - 200
                sdbgValores.Columns("POND").Visible = False
            Else
                sdbgValores.Columns("POND").Visible = True
            End If
            
        Case AmbItem
            sdbgValores.Visible = False
            sdbgValoresItem.Visible = True
            sdbcGrupo.Visible = True
            CargarComboGruposAtribManual
            CargarGridValoresItem
            
            'Carga la grid con el grupo correspondiente
            If g_oGrupoSeleccionado Is Nothing Then
                sdbcGrupo.Value = sdbcGrupo.Columns(0).Value
                If sdbcGrupo.Columns(1).Value <> "" Then
                    sdbcGrupo.Value = sdbcGrupo.Value & " - " & sdbcGrupo.Columns(1).Value
                End If
                
                Set g_oGrupoSeleccionado = g_oProceso.Grupos.Item(sdbcGrupo.Columns(0).Value)
            Else
                sdbcGrupo.Columns(0).Value = g_oGrupoSeleccionado.Codigo
                sdbcGrupo.Columns(1).Value = g_oGrupoSeleccionado.Den
                sdbcGrupo.Value = g_oGrupoSeleccionado.Codigo & " - " & g_oGrupoSeleccionado.Den
            End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "VisualizarValoresManual", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub CargarGridValoresGrupo()
    Dim oProve As CProveedor
    Dim sGrupo As String
    Dim sFila As String
    Dim bCargar As Boolean
    Dim scod1 As String
    
    'Rellena la grid de los valores manuales de los atributos de �mbito item
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oProveedores Is Nothing Then Exit Sub
    
    sdbgValores.RemoveAll
    
    If g_oGrupoSeleccionado Is Nothing Then
        sGrupo = sdbcGrupo.Columns(0).Value
    Else
        sGrupo = g_oGrupoSeleccionado.Codigo
    End If

    For Each oProve In g_oProveedores
        If g_oOrigen.Name = "frmADJItem" Then
            bCargar = g_oOrigen.g_oVistaSeleccionada.ConfVistasItemProv.Item(oProve.Cod).Visible
        Else
            Select Case g_oOrigen.sstabComparativa.selectedItem.Tag
                Case "General"
                    If g_oOrigen.m_oVistaSeleccionada.Vista = 0 Then
                        bCargar = True 'es la vista inicial
                    Else
                        bCargar = g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible
                    End If
                Case "ALL"
                    If g_oOrigen.m_oVistaSeleccionadaAll.Vista = 0 Then
                        bCargar = True 'es la vista inicial
                    Else
                        bCargar = g_oOrigen.m_oVistaSeleccionadaAll.ConfVistasAllProv.Item(oProve.Cod).Visible
                    End If
                Case Else
                    If g_oOrigen.m_oVistaSeleccionadaGr.Vista = 0 Then
                        bCargar = True 'es la vista inicial
                    Else
                        bCargar = g_oOrigen.m_oVistaSeleccionadaGr.ConfVistasGrProv.Item(oProve.Cod).Visible
                    End If
            End Select
        End If
    
        If bCargar Then
         sFila = oProve.Cod & " - " & oProve.Den
         
         If Not g_oProceso.Ofertas.Item(oProve.Cod) Is Nothing Then
             If Not g_oProceso.Ofertas.Item(oProve.Cod).AtribGrOfertados Is Nothing Then
                scod1 = sGrupo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(sGrupo))
                scod1 = scod1 & CStr(g_oAtributo.idAtribProce)
                 If Not g_oProceso.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(scod1) Is Nothing Then
                     Select Case g_oAtributo.Tipo
                         Case TiposDeAtributos.TipoBoolean
                             sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(scod1).valorBool)
                         Case TiposDeAtributos.TipoFecha
                             sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(scod1).valorFec)
                         Case TiposDeAtributos.TipoNumerico
                             sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(scod1).valorNum)
                         Case TiposDeAtributos.TipoString
                             sFila = sFila & Chr(m_lSeparador) & EliminarTab(NullToStr(g_oProceso.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(scod1).valorText))
                     End Select
                     sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(scod1).ValorPond)
                     sFila = sFila & Chr(m_lSeparador) & oProve.Cod
                 End If
             End If
        
         End If
         'A�ade la fila a la grid
         sdbgValores.AddItem sFila
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "CargarGridValoresGrupo", err, Erl, , m_bActivado)
      Exit Sub
   End If
                
End Sub


Private Sub CargarGridValoresProceso()
    Dim oProve As CProveedor
    Dim sFila As String
    Dim bCargar As Boolean
    
    'Rellena la grid de los valores manuales de los atributos de �mbito item
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oProveedores Is Nothing Then Exit Sub

    sdbgValores.RemoveAll
    
    For Each oProve In g_oProveedores
        'Solo se cargan los visibles
        If g_oOrigen.Name = "frmADJItem" Then
            bCargar = g_oOrigen.g_oVistaSeleccionada.ConfVistasItemProv.Item(oProve.Cod).Visible
        Else
            Select Case g_oOrigen.sstabComparativa.selectedItem.Tag
                Case "General"
                    If g_oOrigen.m_oVistaSeleccionada.Vista = 0 Then
                        bCargar = True 'es la vista inicial
                    Else
                        bCargar = g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible
                    End If
                Case "ALL"
                    If g_oOrigen.m_oVistaSeleccionadaAll.Vista = 0 Then
                        bCargar = True 'es la vista inicial
                    Else
                        bCargar = g_oOrigen.m_oVistaSeleccionadaAll.ConfVistasAllProv.Item(oProve.Cod).Visible
                    End If
                Case Else
                    If g_oOrigen.m_oVistaSeleccionadaGr.Vista = 0 Then
                        bCargar = True 'es la vista inicial
                    Else
                        bCargar = g_oOrigen.m_oVistaSeleccionadaGr.ConfVistasGrProv.Item(oProve.Cod).Visible
                    End If
            End Select
        End If
        If bCargar Then
            sFila = oProve.Cod & " - " & oProve.Den
            
            If Not g_oProceso.Ofertas.Item(oProve.Cod) Is Nothing Then
                If Not g_oProceso.Ofertas.Item(oProve.Cod).AtribProcOfertados Is Nothing Then
                    If Not g_oProceso.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(g_oAtributo.idAtribProce)) Is Nothing Then
                        Select Case g_oAtributo.Tipo
                            Case TiposDeAtributos.TipoBoolean
                                sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(g_oAtributo.idAtribProce)).valorBool)
                            Case TiposDeAtributos.TipoFecha
                                sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(g_oAtributo.idAtribProce)).valorFec)
                            Case TiposDeAtributos.TipoNumerico
                                sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(g_oAtributo.idAtribProce)).valorNum)
                            Case TiposDeAtributos.TipoString
                                sFila = sFila & Chr(m_lSeparador) & EliminarTab(NullToStr(g_oProceso.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(g_oAtributo.idAtribProce)).valorText))
                        End Select
                        sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(g_oAtributo.idAtribProce)).ValorPond)
                        sFila = sFila & Chr(m_lSeparador) & oProve.Cod
                    End If
                End If
            End If
            
            'A�ade la fila a la grid
            sdbgValores.AddItem sFila
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "CargarGridValoresProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' A�ade las filas a la grid para el atributo manual de �mbito item
''' </summary>
''' <remarks>Llamada desde: cmdCancelar_Click       sdbcGrupo_CloseUp       CargarGridValoresItem; Tiempo m�ximo: 0,2</remarks>
Private Sub RellenarValoresItem()
    Dim sFila As String
    Dim sGrupo As String
    Dim oItem As CItem
    Dim oProve As CProveedor
    Dim bCargar As Boolean
    'A�ade las filas a la grid para el atributo manual de �mbito item
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgValoresItem.RemoveAll
    
    sdbgValoresItem.Groups(0).caption = m_sDefAmbito(3)
    
    If g_oGrupoSeleccionado Is Nothing Then
        sGrupo = sdbcGrupo.Columns(0).Value
    Else
        sGrupo = g_oGrupoSeleccionado.Codigo
    End If
    
    For Each oItem In g_oProceso.Grupos.Item(sGrupo).Items
        sFila = ""
        If Not IsNull(oItem.ArticuloCod) Then
            sFila = oItem.ArticuloCod & " - " & oItem.Descr & Chr(m_lSeparador) & oItem.Id
        Else
            sFila = oItem.Descr & Chr(m_lSeparador) & oItem.Id
        End If
        For Each oProve In g_oProveedores
            If g_oOrigen.Name = "frmADJItem" Then
                bCargar = g_oOrigen.g_oVistaSeleccionada.ConfVistasItemProv.Item(oProve.Cod).Visible
            Else
                Select Case g_oOrigen.sstabComparativa.selectedItem.Tag
                    Case "General"
                        If g_oOrigen.m_oVistaSeleccionada.Vista = 0 Then
                            bCargar = True
                        Else
                            bCargar = g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible
                        End If
                    Case "ALL"
                        If g_oOrigen.m_oVistaSeleccionadaAll.Vista = 0 Then
                            bCargar = True
                        Else
                            bCargar = g_oOrigen.m_oVistaSeleccionadaAll.ConfVistasAllProv.Item(oProve.Cod).Visible
                        End If
                    Case Else
                        If g_oOrigen.m_oVistaSeleccionadaGr.Vista = 0 Then
                            bCargar = True
                        Else
                            bCargar = g_oOrigen.m_oVistaSeleccionadaGr.ConfVistasGrProv.Item(oProve.Cod).Visible
                        End If
                End Select
            End If
            If bCargar Then
                If Not g_oProceso.Grupos.Item(sGrupo).UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                    If Not g_oProceso.Grupos.Item(sGrupo).UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados Is Nothing Then
                        If Not g_oProceso.Grupos.Item(sGrupo).UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id & "$" & g_oAtributo.idAtribProce)) Is Nothing Then
                            Select Case g_oAtributo.Tipo
                                Case TiposDeAtributos.TipoBoolean
                                    sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Grupos.Item(sGrupo).UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id & "$" & g_oAtributo.idAtribProce)).valorBool)
                                Case TiposDeAtributos.TipoFecha
                                    sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Grupos.Item(sGrupo).UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id & "$" & g_oAtributo.idAtribProce)).valorFec)
                                Case TiposDeAtributos.TipoNumerico
                                    sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Grupos.Item(sGrupo).UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id & "$" & g_oAtributo.idAtribProce)).valorNum)
                                Case TiposDeAtributos.TipoString
                                    sFila = sFila & Chr(m_lSeparador) & EliminarTab(NullToStr(g_oProceso.Grupos.Item(sGrupo).UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id & "$" & g_oAtributo.idAtribProce)).valorText))
                            End Select
                            sFila = sFila & Chr(m_lSeparador) & NullToStr(g_oProceso.Grupos.Item(sGrupo).UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id & "$" & g_oAtributo.idAtribProce)).ValorPond) & Chr(m_lSeparador) & oProve.Cod
                        Else
                            sFila = sFila & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oProve.Cod
                        End If
                    Else
                        sFila = sFila & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oProve.Cod
                    End If
                Else
                    sFila = sFila & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oProve.Cod
                End If
            End If
        Next
        sdbgValoresItem.AddItem sFila
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetAtribProce", "RellenarValoresItem", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
