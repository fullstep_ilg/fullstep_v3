VERSION 5.00
Begin VB.Form frmADJGuardarVista 
   BackColor       =   &H00808000&
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Guardar en vista nueva"
   ClientHeight    =   1800
   ClientLeft      =   210
   ClientTop       =   1650
   ClientWidth     =   5565
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmADJGuardarVista.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1800
   ScaleWidth      =   5565
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtDecimales 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3600
      MaxLength       =   200
      TabIndex        =   5
      Top             =   960
      Width           =   405
   End
   Begin VB.TextBox txtNombreVista 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   120
      MaxLength       =   200
      TabIndex        =   2
      Top             =   480
      Width           =   5205
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2805
      TabIndex        =   1
      Top             =   1350
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1680
      TabIndex        =   0
      Top             =   1350
      Width           =   1005
   End
   Begin VB.Label lblLabelDecimal 
      BackColor       =   &H00808000&
      Caption         =   "DN�mero de decimales para las puntuaciones:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000014&
      Height          =   285
      Left            =   120
      TabIndex        =   4
      Top             =   960
      Width           =   3450
   End
   Begin VB.Label lblLabel1 
      BackColor       =   &H00808000&
      Caption         =   "Nombre de la vista:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000014&
      Height          =   240
      Left            =   120
      TabIndex        =   3
      Top             =   135
      Width           =   5370
   End
End
Attribute VB_Name = "frmADJGuardarVista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Public g_sOrigen As String

Private m_sNombre As String
Private m_iDecimales As Integer

Public m_ofrmADJItem As frmADJItem
Public m_ofrmConfiguracionVista As frmConfiguracionVista
Public m_sNombreVistaAnterior As String
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean

Private m_sMsgError As String

''' <summary>
''' Cancel los cambios y cierra
''' </summary>
''' <remarks>Llamada desde: Sistema; Tiempo m�ximo: 0</remarks>
Private Sub cmdCancelar_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_sNombre = ""
    m_sNombreVistaAnterior = ""
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGuardarVista", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>
''' Valida los datos dados y cierra si estan bien
''' </summary>
''' <remarks>Llamada desde: Sistema; Tiempo m�ximo: 0</remarks>
Private Sub cmdAceptar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtNombreVista.Text = "" Then
        oMensajes.NoValido Left(lblLabel1.caption, Len(lblLabel1.caption))
        Exit Sub
    End If
    m_sNombre = txtNombreVista.Text
    
    If Me.txtDecimales.Visible Then
        If Me.txtDecimales.Text = "" Then
            oMensajes.NoValido Left(lblLabelDecimal.caption, Len(lblLabelDecimal.caption))
            Exit Sub
        End If
        If Not IsNumeric(Me.txtDecimales.Text) Then
            oMensajes.NoValido Left(lblLabelDecimal.caption, Len(lblLabelDecimal.caption))
            Exit Sub
        End If
        If CInt(Me.txtDecimales.Text) < 0 Then
            oMensajes.NoValido Left(lblLabelDecimal.caption, Len(lblLabelDecimal.caption))
            Exit Sub
        End If
        
        m_iDecimales = CInt(Me.txtDecimales.Text)
    End If
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGuardarVista", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>
''' Nada mas entrar q lo q este activo sea la caja de texto
''' </summary>
''' <remarks>Llamada desde: Sistema; Tiempo m�ximo:0</remarks>
Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
        Unload Me
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
    If Me.Visible Then txtNombreVista.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGuardarVista", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Carga la pantalla. En caso de venir de Comparativa Qa se visualiza un campo para decimales.
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub Form_Load()
    'Posiciona el formulario en la mitad del control de la �ptima,de forma que cuando se
    'cierre este formulario no se oculte el picture de la �ptima.
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_sNombre = ""
    Screen.MousePointer = vbHourglass
    m_bActivado = False
    m_bDescargarFrm = False
    CargarRecursos
    
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    If (MDI.ScaleHeight / 2 - Me.Height / 2) > 200 Then
        Me.Top = (MDI.ScaleHeight / 2 - Me.Height / 2) - 500
    End If
    
    txtNombreVista.Text = m_sNombreVistaAnterior
    
    If g_sOrigen = "frmComparativaQA" Then
        Me.lblLabelDecimal.Visible = True
        Me.txtDecimales.Visible = True
        
        m_iDecimales = frmComparativaQA.m_oVistaCalSeleccionada.decimales
        Me.txtDecimales.Text = frmComparativaQA.m_oVistaCalSeleccionada.decimales
    Else
        Me.lblLabelDecimal.Visible = False
        Me.txtDecimales.Visible = False
        
        Me.cmdAceptar.Top = Me.txtDecimales.Top
        Me.cmdCancelar.Top = Me.cmdAceptar.Top
        
        Me.Height = Me.Height - Me.txtDecimales.Height - 12
    End If
    
    If g_sOrigen = "frmSeguimiento" Then
        txtNombreVista.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodDENART
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGuardarVista", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
  
End Sub

''' <summary>
''' Carga los textos de pantalla
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo: 0,1</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADJ_GUARDARVISTA, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblLabel1.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblLabelDecimal.caption = Ador(0).Value
        If g_sOrigen = "frmSeguimiento" Then
            Ador.MoveNext
            Me.caption = Ador(0).Value
            Ador.MoveNext
            lblLabel1.caption = Ador(0).Value
        End If
        Ador.Close
    End If

    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

''' <summary>
''' Tras dar un nuevo nombre valido, este nombre se le pasa a la pantalla q llamo para q actualice.
''' En el caso de la compartiva qa tambien pasa el numero de decimales.
''' </summary>
''' <param name="Cancel">Si se desea deshacer el cierre de pantalla</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case g_sOrigen
        Case "frmADJ"
            frmADJ.g_sNombreVista = m_sNombre
        Case "frmRESREU"
            frmRESREU.g_sNombreVista = m_sNombre
        Case "frmADJItem"
            m_ofrmADJItem.g_sNombreVista = m_sNombre
            Set frmADJItem = m_ofrmADJItem
        Case "frmConfiguracionVista"
            m_ofrmConfiguracionVista.g_sNombreVista = m_sNombre
            Set frmConfiguracionVista = m_ofrmConfiguracionVista
        Case "frmComparativaQA"
            frmComparativaQA.g_sNombreVista = m_sNombre
            frmComparativaQA.m_oVistaCalSeleccionada.decimales = m_iDecimales
        Case "frmSeguimiento"
            If m_sNombre <> "" Then
                frmSeguimiento.sdbgItemPorProve.Columns("ARTI").Value = frmSeguimiento.sdbgItemPorProve.Columns("ARTCOD").Value & " - " & m_sNombre
            End If
    End Select
    
    m_sNombreVistaAnterior = ""
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGuardarVista", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub



