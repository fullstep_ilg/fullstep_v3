VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmVARCalidad 
   Caption         =   "DVariables de calidad"
   ClientHeight    =   6330
   ClientLeft      =   75
   ClientTop       =   1725
   ClientWidth     =   20145
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmVARCalidad.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6330
   ScaleWidth      =   20145
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   10800
      Top             =   480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalidad.frx":014A
            Key             =   "BAJA"
            Object.Tag             =   "BAJA"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalidad.frx":049C
            Key             =   "NOBAJA"
            Object.Tag             =   "NOBAJA"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   11415
      TabIndex        =   27
      Top             =   120
      Width           =   11415
      Begin VB.CheckBox chkVerBaja 
         Caption         =   "DVer bajas l�gicas"
         Height          =   192
         Left            =   8160
         TabIndex        =   31
         Top             =   50
         Width           =   3105
      End
      Begin VB.CheckBox chkMultiidioma 
         Caption         =   "dMostrar denominaciones en todos los idiomas"
         Height          =   192
         Left            =   4320
         TabIndex        =   30
         Top             =   50
         Width           =   3825
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPYME 
         Height          =   285
         Left            =   720
         TabIndex        =   28
         Top             =   0
         Width           =   3435
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1773
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   4260
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6068
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblPYME 
         AutoSize        =   -1  'True
         Caption         =   "PYME:"
         Height          =   195
         Left            =   120
         TabIndex        =   29
         Top             =   0
         Width           =   450
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddSubTipo 
      Height          =   915
      Left            =   4440
      TabIndex        =   22
      Top             =   3120
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmVARCalidad.frx":07EE
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3916
      Columns(0).Name =   "NOMBRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTipo 
      Height          =   915
      Left            =   2400
      TabIndex        =   21
      Top             =   3120
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmVARCalidad.frx":080A
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3545
      Columns(0).Name =   "NOMBRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddOrigen 
      Height          =   915
      Left            =   360
      TabIndex        =   23
      Top             =   3120
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmVARCalidad.frx":0826
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6165
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID"
      Columns(2).Name =   "ID"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "FORM"
      Columns(3).Name =   "FORM"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
   End
   Begin VB.PictureBox picDatos 
      BorderStyle     =   0  'None
      Height          =   4980
      Left            =   180
      ScaleHeight     =   4980
      ScaleWidth      =   10620
      TabIndex        =   11
      Top             =   540
      Width           =   10620
      Begin VB.Frame fraConfiguracion 
         Caption         =   "DSelecci�n del modo de c�lculo para unidades de negocio de nivel superior"
         ForeColor       =   &H00FF0000&
         Height          =   855
         Left            =   5000
         TabIndex        =   36
         Top             =   4000
         Width           =   8000
         Begin VB.OptionButton optOpcionConf 
            Caption         =   "DEvaluar f�rmula"
            Height          =   300
            Index           =   0
            Left            =   100
            TabIndex        =   39
            Top             =   360
            Value           =   -1  'True
            Width           =   1800
         End
         Begin VB.OptionButton optOpcionConf 
            Caption         =   "DMedia ponderada seg�n:"
            Height          =   255
            Index           =   1
            Left            =   2000
            TabIndex        =   38
            Top             =   360
            Width           =   2500
         End
         Begin VB.OptionButton optOpcionConf 
            Caption         =   "DNo calcular"
            Height          =   255
            Index           =   2
            Left            =   6400
            TabIndex        =   37
            Top             =   360
            Width           =   2000
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcVarPond 
            Height          =   285
            Left            =   4500
            TabIndex        =   40
            Top             =   360
            Width           =   1785
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "COD"
            Columns(0).Alignment=   1
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6800
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3149
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.Frame fraSubVariables 
         Caption         =   "DSubvariables"
         ForeColor       =   &H00FF0000&
         Height          =   3960
         Left            =   40
         TabIndex        =   12
         Top             =   0
         Width           =   10500
         Begin VB.PictureBox PicBajaLogica 
            BorderStyle     =   0  'None
            Height          =   325
            Left            =   9000
            ScaleHeight     =   330
            ScaleWidth      =   1335
            TabIndex        =   34
            Top             =   120
            Width           =   1335
            Begin VB.Label lblBajaLog 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "d(!) Baja l�gica"
               Height          =   195
               Left            =   240
               TabIndex        =   35
               Top             =   120
               Width           =   1080
            End
         End
         Begin VB.PictureBox picBotonesSubvar 
            BorderStyle     =   0  'None
            Height          =   325
            Left            =   120
            ScaleHeight     =   330
            ScaleWidth      =   1335
            TabIndex        =   13
            Top             =   240
            Width           =   1335
            Begin VB.CommandButton cmdBajaSubvar 
               Height          =   300
               Left            =   840
               Picture         =   "frmVARCalidad.frx":0842
               Style           =   1  'Graphical
               TabIndex        =   32
               Top             =   0
               Width           =   350
            End
            Begin VB.CommandButton cmdElimSubvar 
               Height          =   300
               Left            =   420
               Picture         =   "frmVARCalidad.frx":0B84
               Style           =   1  'Graphical
               TabIndex        =   7
               Top             =   0
               Width           =   350
            End
            Begin VB.CommandButton cmdAnyaSubvar 
               Height          =   300
               Left            =   0
               Picture         =   "frmVARCalidad.frx":0C16
               Style           =   1  'Graphical
               TabIndex        =   6
               Top             =   0
               Width           =   350
            End
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgSubVariables 
            Height          =   3210
            Left            =   120
            TabIndex        =   5
            Top             =   600
            Width           =   10245
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   16
            stylesets.count =   3
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmVARCalidad.frx":0C98
            stylesets(1).Name=   "StringTachado"
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   -1  'True
            EndProperty
            stylesets(1).Picture=   "frmVARCalidad.frx":0CB4
            stylesets(2).Name=   "Bloqueado"
            stylesets(2).BackColor=   12632256
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmVARCalidad.frx":0CD0
            AllowAddNew     =   -1  'True
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            SelectByCell    =   -1  'True
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   16
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1773
            Columns(1).Caption=   "COD"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   5
            Columns(2).Width=   3200
            Columns(2).Caption=   "DEN"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   150
            Columns(3).Width=   3200
            Columns(3).Caption=   "TIPO"
            Columns(3).Name =   "TIPO"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(4).Width=   1535
            Columns(4).Name =   "SUB"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Style=   4
            Columns(4).ButtonsAlways=   -1  'True
            Columns(5).Width=   3122
            Columns(5).Caption=   "SUBTIPO"
            Columns(5).Name =   "SUBTIPO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Locked=   -1  'True
            Columns(6).Width=   2593
            Columns(6).Caption=   "ORIGEN"
            Columns(6).Name =   "ORIGEN"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).Locked=   -1  'True
            Columns(7).Width=   3200
            Columns(7).Caption=   "VALOR_DEF"
            Columns(7).Name =   "VALOR_DEF"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Caption=   "DVal. cert. sin cumpl."
            Columns(8).Name =   "CERT_VALOR_SINCUMPL"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3201
            Columns(9).Caption=   "Puntuaci�n"
            Columns(9).Name =   "PUNT"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(9).Style=   4
            Columns(9).ButtonsAlways=   -1  'True
            Columns(10).Width=   1693
            Columns(10).Caption=   "Materiales"
            Columns(10).Name=   "MAT"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(10).Style=   4
            Columns(10).ButtonsAlways=   -1  'True
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "ID_TIPO"
            Columns(11).Name=   "ID_TIPO"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "ID_SUBTIPO"
            Columns(12).Name=   "ID_SUBTIPO"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(13).Width=   3200
            Columns(13).Visible=   0   'False
            Columns(13).Caption=   "ID_ORIGEN"
            Columns(13).Name=   "ID_ORIGEN"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(14).Width=   3200
            Columns(14).Visible=   0   'False
            Columns(14).Caption=   "ID_FORM"
            Columns(14).Name=   "ID_FORM"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(15).Width=   3200
            Columns(15).Visible=   0   'False
            Columns(15).Caption=   "BAJALOG"
            Columns(15).Name=   "BAJALOG"
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            _ExtentX        =   18071
            _ExtentY        =   5662
            _StockProps     =   79
            BackColor       =   16777215
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFormula 
         Caption         =   "DF�rmula para calcular la puntuaci�n de la variable"
         ForeColor       =   &H00FF0000&
         Height          =   900
         Left            =   40
         TabIndex        =   14
         Top             =   4005
         Width           =   5000
         Begin VB.CommandButton cmdAyuda 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   7.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   9045
            Picture         =   "frmVARCalidad.frx":0CEC
            Style           =   1  'Graphical
            TabIndex        =   9
            Top             =   360
            Width           =   350
         End
         Begin VB.TextBox txtFormula 
            Height          =   300
            Left            =   120
            TabIndex        =   8
            Top             =   360
            Width           =   8805
         End
      End
   End
   Begin VB.PictureBox picEdicion 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   450
      Left            =   0
      ScaleHeight     =   450
      ScaleWidth      =   20145
      TabIndex        =   10
      Top             =   5880
      Width           =   20145
      Begin VB.CommandButton cmdBajaVar 
         Caption         =   "dDeshacer Baja l�gica"
         Height          =   345
         Left            =   2760
         TabIndex        =   33
         Top             =   45
         Width           =   1725
      End
      Begin VB.CommandButton cmdCalcular 
         Caption         =   "Actualizar puntuaci�n de proveedores"
         Height          =   360
         Left            =   8760
         TabIndex        =   26
         Top             =   30
         Width           =   3165
      End
      Begin VB.CommandButton cmdModificarVar 
         Caption         =   "DModificar"
         Height          =   345
         Left            =   4560
         TabIndex        =   24
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer cambios"
         Height          =   345
         Left            =   7200
         TabIndex        =   3
         Top             =   45
         Visible         =   0   'False
         Width           =   1500
      End
      Begin VB.CommandButton cmdGuardar 
         Caption         =   "&Guardar cambios"
         Height          =   345
         Left            =   5640
         TabIndex        =   2
         Top             =   45
         Visible         =   0   'False
         Width           =   1500
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "DEliminar"
         Height          =   345
         Left            =   1650
         TabIndex        =   1
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdAyadirVar 
         Caption         =   "&Anyadir variable"
         Height          =   345
         Left            =   90
         TabIndex        =   0
         Top             =   45
         Width           =   1500
      End
   End
   Begin VB.PictureBox picProveedor 
      BorderStyle     =   0  'None
      Height          =   4880
      Left            =   300
      ScaleHeight     =   4875
      ScaleWidth      =   10395
      TabIndex        =   15
      Top             =   600
      Width           =   10400
      Begin VB.PictureBox picFormula 
         BorderStyle     =   0  'None
         Height          =   800
         Left            =   40
         ScaleHeight     =   795
         ScaleWidth      =   10095
         TabIndex        =   19
         Top             =   4000
         Width           =   10100
         Begin VB.CommandButton cmdAyuda 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   7.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   9045
            Picture         =   "frmVARCalidad.frx":0F1D
            Style           =   1  'Graphical
            TabIndex        =   18
            Top             =   300
            Width           =   350
         End
         Begin VB.TextBox txtFormulaProv 
            Height          =   300
            Left            =   0
            TabIndex        =   17
            Top             =   300
            Width           =   8805
         End
         Begin VB.Label lblFormulaProv 
            Caption         =   "DF�rmula para calcular el total por proveedor:"
            ForeColor       =   &H00FF0000&
            Height          =   285
            Left            =   0
            TabIndex        =   20
            Top             =   0
            Width           =   5000
         End
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgTotProveedor 
         Height          =   3795
         Left            =   45
         TabIndex        =   16
         Top             =   45
         Width           =   10215
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         Col.Count       =   5
         stylesets.count =   1
         stylesets(0).Name=   "Valor_def"
         stylesets(0).BackColor=   16382457
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmVARCalidad.frx":114E
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   0
         BackColorEven   =   -2147483633
         BackColorOdd    =   -2147483633
         RowHeight       =   423
         ExtraHeight     =   106
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).StyleSet=   "Valor_def"
         Columns(1).Width=   3200
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   10583
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "DEN_IDIOMAS"
         Columns(3).Name =   "DEN_IDIOMAS"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Caption=   "VALOR_DEF"
         Columns(4).Name =   "VALOR_DEF"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   18018
         _ExtentY        =   6694
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.TabStrip ssTabVar 
      Height          =   5505
      Left            =   30
      TabIndex        =   4
      Top             =   120
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   9710
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Label lblCambios 
      Caption         =   "Para guardar los cambios efectuados pulse Guadar cambios."
      ForeColor       =   &H000000FF&
      Height          =   240
      Left            =   90
      TabIndex        =   25
      Top             =   5670
      Visible         =   0   'False
      Width           =   10860
   End
End
Attribute VB_Name = "frmVARCalidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnValCertNoCumplDefecto As Integer = 0

'Variables de coleccion
Public g_oVarsCalidad1 As CVariablesCalidad
Public g_oVarsCalidad1Mod As CVariablesCalidad
Public m_bSaltar As Boolean 'Variable que permite saltar la seleccion del tab cuando a�ade una nueva fila o no (si se pinche en multiIdioma)
Public m_lIdCal1Ant As Long 'Variable que sirve para llevar correctamente el ID del tab
Public m_bDescargarFrm As Boolean

Private m_bActivado As Boolean
Private m_ilIDTabAnt As Long
'Variables de gesti�n
Private m_bRespetarColor As Boolean
Private m_bError As Boolean

'Variables de idiomas
Private m_sTotProveedor As String
Private m_sTotProveedorIdiomas As String
Private m_sTipo(0 To 1) As String
Private m_sSubTipo(0 To 8) As String
Private m_sIdiErrorFormula(12) As String
Private m_sMsgBox As String
Private m_sNombreDen As String
Private m_sIdiomaSPA As String
Private m_oIdiomas As CIdiomas

Private m_bGuardarTabAnt As Boolean
Private m_bCambioTabs As Boolean
Private m_sDeshacerBaja As String
Private m_sBaja As String
Private m_iUltimaFila As Integer 'Variable que sirve para llevar el control de cambio de fila en Total de Proveedores
Private m_sMensajeErrorNumerico As String
Private m_bCambioTotProveedores As Boolean
Private m_sMsgError As String

Private Sub cmdAnyaSubvar_Click()
    'A�ade una fila para una nueva variable de nivel 2:
    sdbgSubVariables.AllowAddNew = True
    
    sdbgSubVariables.Scroll 0, sdbgSubVariables.Rows - sdbgSubVariables.Row
    
    If sdbgSubVariables.VisibleRows > 0 Then
        If sdbgSubVariables.VisibleRows >= sdbgSubVariables.Rows Then
            If sdbgSubVariables.VisibleRows = sdbgSubVariables.Rows Then
                sdbgSubVariables.Row = sdbgSubVariables.Rows - 1
            Else
                sdbgSubVariables.Row = sdbgSubVariables.Rows
            End If
        Else
            sdbgSubVariables.Row = sdbgSubVariables.Rows - (sdbgSubVariables.Rows - sdbgSubVariables.VisibleRows) - 1
        End If
    End If
    
    If Me.Visible Then sdbgSubVariables.SetFocus
    
End Sub

Private Sub cmdAyadirVar_Click()
Dim lMaxID As Long
Dim b As Boolean
Dim lVarCalID As Long
Dim sCadena As String
'si hay cambios los guarda
If sdbgSubVariables.DataChanged = True Then
    m_bError = False
    sdbgSubVariables.Update
    If m_bError = True Then Exit Sub
End If
lMaxID = frmVARCalidad.g_oVarsCalidad1Mod.MaxID1

b = FSGSForm.MostrarFormVARCalAnya(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, True, True, m_oIdiomas, lMaxID, g_oVarsCalidad1Mod, lVarCalID, sCadena, gLongitudesDeCodigos.giLongCodVarCal, Nothing)
If b Then
    g_oVarsCalidad1Mod.MaxID1 = lMaxID
    ssTabVar.Tabs.Add ssTabVar.Tabs.Count, "V" & lVarCalID, sCadena
    ssTabVar.Tabs("V" & lVarCalID).Selected = True
    m_lIdCal1Ant = lVarCalID
    VisualizarGuardar
    chkMultiidioma.Value = vbChecked
End If
    
End Sub

Private Sub cmdAyuda_Click(Index As Integer)
    'si hay cambios los guarda
    If sdbgSubVariables.DataChanged = True Then
        sdbgSubVariables.Update
        If m_bError = True Then Exit Sub
    End If
    
    MostrarFormSOLAyudaCalculos oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub

Private Sub cmdCalcular_Click()
    Dim oWebSvc As FSGSLibrary.CWebService
    Set oWebSvc = New FSGSLibrary.CWebService
    oWebSvc.LlamarWebServiceQA gParametrosGenerales, oUsuarioSummit.Cod, oUsuarioSummit.Pwd
    Set oWebSvc = Nothing
    RegistrarAccion AccionesSummit.ACCVarCalRecalculoPunt, ""
    oMensajes.MostrarMensajeActualizacionPuntuaciones
End Sub

Private Sub cmdEliminar_Click()
''' <summary>
''' Elimina una variable de calidad de nivel 1 de la coleccion. Comprueba si dispone de puntuaciones en el historico (esta variable o sus hijas) y en este caso se da de baja logica.
''' Y si dispone de puntuaci�n en curso lo indicara mediante un mensaje y le permitira eliminarlo
''' Si se puede ver las variables dadas de baja logica se dejara en la grid con un estilo tachado y sino lo eliminara
''' </summary>

''' <returns>no devuelve nada</returns>
''' <remarks>Llamada desde = Propio formulario; Tiempo ejecucion= 0,3seg.</remarks>

Dim irespuesta As Integer
Dim rs As ADODB.Recordset
Dim bPuntCurso, bPuntHist As Boolean
Dim bVarConhijos As Boolean
Dim oVarCal1 As CVariableCalidad
Dim oVarCal2 As CVariableCalidad
Dim oVarCal3 As CVariableCalidad
Dim oVarCal4 As CVariableCalidad
Dim oVarCal5 As CVariableCalidad
Dim i As Integer
Dim vbm As Variant

    Set oVarCal1 = g_oVarsCalidad1Mod.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2)))
    
    If Not oVarCal1 Is Nothing Then
        Set rs = oVarCal1.ExistenPuntuaciones
        If Not rs.EOF Then
            bPuntCurso = (rs("CONT").Value > 0)
            rs.MoveNext
            bPuntHist = (rs("CONT").Value > 0)
        End If
        If Not oVarCal1.VariblesCal Is Nothing Then
            If oVarCal1.VariblesCal.Count > 0 Then
                bVarConhijos = True
            End If
        End If
    End If
    
    
    irespuesta = oMensajes.PreguntaEliminarVariableCalidad(ssTabVar.selectedItem.caption, bPuntHist, bPuntCurso, bVarConhijos)
    If irespuesta = vbNo Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If bPuntHist Then

        oVarCal1.BajaLog = True
        oVarCal1.modificado = True
        If Not oVarCal1.VariblesCal Is Nothing Then
            For Each oVarCal2 In oVarCal1.VariblesCal
                oVarCal2.BajaLog = True
                oVarCal2.modificado = True
                For i = 0 To sdbgSubVariables.Rows
                    vbm = sdbgSubVariables.AddItemBookmark(i)
                    If sdbgSubVariables.Columns("COD").CellValue(vbm) = oVarCal2.Cod Then
                        sdbgSubVariables.Row = i
                        If chkVerBaja.Value = vbChecked Then
                            sdbgSubVariables.Columns("BAJALOG").Value = 1
                        Else
                            sdbgSubVariables.DeleteSelected
                        End If
                        Exit For
                    End If
                Next i
                If Not oVarCal2.VariblesCal Is Nothing Then
                    For Each oVarCal3 In oVarCal2.VariblesCal
                        oVarCal3.BajaLog = True
                        oVarCal3.modificado = True
                        If Not oVarCal3.VariblesCal Is Nothing Then
                            For Each oVarCal4 In oVarCal3.VariblesCal
                                oVarCal4.BajaLog = True
                                oVarCal4.modificado = True
                                If Not oVarCal4.VariblesCal Is Nothing Then
                                    For Each oVarCal5 In oVarCal4.VariblesCal
                                        oVarCal5.BajaLog = True
                                        oVarCal5.modificado = True
                                    Next
                                End If
                            Next
                        End If
                    Next
                End If
            Next
        End If
        
        If chkVerBaja.Value = vbChecked Then
            ssTabVar.selectedItem.caption = "(!)" & ssTabVar.selectedItem.caption
        Else
            ssTabVar.Tabs.Remove (ssTabVar.selectedItem.Index)
        End If
    Else

        'Elimina la variable de calidad del tab y de la colecci�n de modificaciones
        g_oVarsCalidad1Mod.Remove "1" & CStr(Mid(ssTabVar.selectedItem.key, 2))
        ssTabVar.Tabs.Remove (Me.ssTabVar.selectedItem.Index)
    End If
    
    If ssTabVar.selectedItem.key = "TOT_PROV" Then
        txtFormulaProv.Forecolor = vbRed
    ElseIf InStr(1, NullToStr(g_oVarsCalidad1Mod.Formula), g_oVarsCalidad1Mod.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))).Cod) <> 0 Then
        txtFormulaProv.Forecolor = vbRed
    End If
    
    If ssTabVar.selectedItem.key = "TOT_PROV" Then
        picDatos.Visible = False
        picProveedor.Visible = True
        cmdEliminar.Enabled = False
        cmdModificarVar.Enabled = False
        CargarTotalProveedores
    Else
        CargarVariablesNivel2 Mid(ssTabVar.selectedItem.key, 2)
    End If
    
    VisualizarGuardar
    
    Set oVarCal1 = Nothing
    Set oVarCal2 = Nothing
    Set oVarCal3 = Nothing
    Set oVarCal4 = Nothing
    Set oVarCal5 = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Public Sub VisualizarGuardar()

    cmdGuardar.Visible = True
    cmdDeshacer.Visible = True
    cmdCalcular.Visible = False
    If lblCambios.Visible = False Then
        lblCambios.Visible = True
        Arrange
    End If

End Sub
    
Private Sub cmdElimSubvar_Click()
'mpg:=03/04/2009
''' <summary>
'''                 Evento que salta al pulsar el boton de "Eliminar Variable"
'''                 Comprueba si la variable de calidad tiene algun tipo de puntuacion (En curso o en el historico)
'''                 Si tiene en el historico --> Da de baja logica la variable
'''                 Sino si tiene en curso muestra mensaje de que tiene puntuaciones en curso y de si quieres eliminarlos tb.

''' </summary>
''' <returns>Nothing</returns>
''' <remarks>Llamada desde=Propio formulario; Tiempo m�ximo=0,3</remarks>

Dim irespuesta As Integer
Dim oVarCal1 As CVariableCalidad
Dim sCod As String
Dim bPuntCurso, bPuntHist As Boolean
Dim oVarCal2 As CVariableCalidad
Dim oVarCal3 As CVariableCalidad
Dim oVarCal4 As CVariableCalidad
Dim oVarCal5 As CVariableCalidad

Dim rs As ADODB.Recordset


    'si hay cambios los guarda
    If sdbgSubVariables.DataChanged = True Then
        sdbgSubVariables.Update
        If m_bError = True Then Exit Sub
    End If
    
    If sdbgSubVariables.Rows = 0 Then Exit Sub
    If sdbgSubVariables.SelBookmarks.Count = 0 Then sdbgSubVariables.SelBookmarks.Add sdbgSubVariables.Bookmark
    
    Set oVarCal1 = g_oVarsCalidad1Mod.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2)))
    If Not oVarCal1 Is Nothing Then
        If Not oVarCal1.VariblesCal.Item("2" & CStr(sdbgSubVariables.Columns("ID").Value)) Is Nothing Then
            Set rs = oVarCal1.VariblesCal.Item("2" & CStr(sdbgSubVariables.Columns("ID").Value)).ExistenPuntuaciones
            If Not rs.EOF Then
                bPuntCurso = (rs("CONT").Value > 0)
                rs.MoveNext
                bPuntHist = (rs("CONT").Value > 0)
            End If
        End If
    End If

    irespuesta = oMensajes.PreguntaEliminarVariableCalidad(sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).Value, bPuntHist, bPuntCurso)
    If irespuesta = vbNo Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bPuntHist Then
    '***********************************************
    'Realizar la baja logica de la variable y todas sus hijas.
        If Not oVarCal1.VariblesCal.Item("2" & CStr(sdbgSubVariables.Columns("ID").Value)) Is Nothing Then
            Set oVarCal2 = oVarCal1.VariblesCal.Item("2" & CStr(sdbgSubVariables.Columns("ID").Value))
            oVarCal2.BajaLog = True
            oVarCal2.modificado = True
            If Not oVarCal2.VariblesCal Is Nothing Then
                For Each oVarCal3 In oVarCal2.VariblesCal
                    oVarCal3.BajaLog = True
                    oVarCal3.modificado = True
                    If Not oVarCal3.VariblesCal Is Nothing Then
                        For Each oVarCal4 In oVarCal3.VariblesCal
                            oVarCal4.BajaLog = True
                            oVarCal4.modificado = True
                            If Not oVarCal4.VariblesCal Is Nothing Then
                                For Each oVarCal5 In oVarCal4.VariblesCal
                                    oVarCal5.BajaLog = True
                                    oVarCal5.modificado = True
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        End If
        sCod = sdbgSubVariables.Columns("COD").Value
        If chkVerBaja.Value = vbChecked Then
            sdbgSubVariables.Columns("BAJALOG").Value = BooleanToSQLBinary(oVarCal2.BajaLog)
            sdbgSubVariables.Refresh
        Else
            sdbgSubVariables.DeleteSelected
        End If

    Else

        If Not oVarCal1.VariblesCal.Item("2" & CStr(sdbgSubVariables.Columns("ID").Value)) Is Nothing Then
            oVarCal1.VariblesCal.Remove "2" & CStr(sdbgSubVariables.Columns("ID").Value)
        End If
        sCod = sdbgSubVariables.Columns("COD").Value
        sdbgSubVariables.DeleteSelected
    
    End If
    
   

        
    VisualizarGuardar

        
    Screen.MousePointer = vbNormal
    
    If InStr(1, txtFormula.Text, sCod) <> 0 Then
        txtFormula.Forecolor = vbRed
    End If
    
    sdbgSubVariables.SelBookmarks.RemoveAll
    If Me.Visible Then sdbgSubVariables.SetFocus
    Set rs = Nothing
    Set oVarCal1 = Nothing
    Set oVarCal2 = Nothing
    Set oVarCal3 = Nothing
    Set oVarCal4 = Nothing
    Set oVarCal5 = Nothing
    
    Screen.MousePointer = vbNormal
End Sub
''' <summary>Evento que salta al pulsar el boton de "Guardar cambios"    Guarda los cambios que ha habido en TODAS las variables de calidad</summary>
''' <remarks>Llamada desde: propio formulario; Tiempo m�ximo: 2,4seg.</remarks>
Private Sub cmdGuardar_Click()
    Dim teserror As TipoErrorSummit
    Dim oVarCal1 As CVariableCalidad
    Dim oVarCal2 As CVariableCalidad
    Dim cont As Byte

    On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    'si hay cambios los guarda
    If sdbgSubVariables.DataChanged Then
        sdbgSubVariables.Update
        If m_bError Then Exit Sub
    End If
    'Comprueba exiten todas las f�rmulas y son correctas
    For Each oVarCal1 In g_oVarsCalidad1Mod
        If Not oVarCal1.BajaLog Then
            If IsNull(oVarCal1.Formula) Then
                If Not oVarCal1.VariblesCal Is Nothing Then
                    For Each oVarCal2 In oVarCal1.VariblesCal
                        If Not oVarCal2.BajaLog Then cont = cont + 1
                    Next
                    If cont > 0 Then
                        oMensajes.NoValido fraFormula.caption & " " & oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                        Exit Sub
                    End If
                End If
            Else
                If Not ValidarFormula(oVarCal1.Id, False, oVarCal1.Formula, 0) Then
                    oMensajes.NoValido fraFormula.caption & " " & oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                    Exit Sub
                End If
                If Not oVarCal1.VariblesCal Is Nothing Then
                    For Each oVarCal2 In oVarCal1.VariblesCal
                        If oVarCal2.Subtipo = CalidadSubtipo.CalCertificado Or oVarCal2.Subtipo = CalidadSubtipo.CalEncuesta Then  'Resto tienen formula por defecto Ej NC: X1- NoConfs abiertas en periodo 6 meses
                            If NullToStr(oVarCal2.Formula) = "" Then
                                oMensajes.NoValido fraFormula.caption & " " & oVarCal2.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                Exit Sub
                            End If
                        End If
                    Next
                End If
            End If
        End If
    Next
    If IsNull(g_oVarsCalidad1Mod.Formula) Then
        If g_oVarsCalidad1Mod.Count > 0 Then
            oMensajes.NoValido Left(lblFormulaProv.caption, Len(lblFormulaProv.caption) - 1)
            Exit Sub
        End If
    Else
        If Not ValidarFormula(0, True, g_oVarsCalidad1Mod.Formula, False) Then
            oMensajes.NoValido Left(lblFormulaProv.caption, Len(lblFormulaProv.caption) - 1)
            Exit Sub
        End If
    End If
        
    If optOpcionConf(VarCalOpcionConf.MediaPonderadaSegunVarHermana).Value Then
        If sdbcVarPond.Text = "" Then
            oMensajes.NoValido Left(fraConfiguracion.caption, Len(fraConfiguracion.caption) - 1)
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    Dim lIdPyme As Long
    
    lIdPyme = 0
    If Me.sdbcPYME.Visible Then
        If sdbcPYME.Value <> "" Then lIdPyme = sdbcPYME.Columns("ID").Value
    End If
    
    teserror = g_oVarsCalidad1Mod.GuardarVariablesCalidad(lIdPyme, m_oIdiomas, g_oVarsCalidad1)
    
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
    Else
        cmdGuardar.Visible = False
        cmdDeshacer.Visible = False
        lblCambios.Visible = False
        cmdCalcular.Visible = True
        Arrange
        CargarVariablesCalidad lIdPyme, (chkVerBaja.Value = vbChecked)
    End If

    Set oVarCal1 = Nothing
    Set oVarCal2 = Nothing
    
    Screen.MousePointer = vbNormal
    Exit Sub
Error:
    Screen.MousePointer = vbNormal
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmVARCalidad", "cmdGuardar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub
Private Sub cmdDeshacer_Click()
    Dim lIdPyme As Long
    
    lIdPyme = 0
    If Me.sdbcPYME.Visible Then
        If sdbcPYME.Value <> "" Then
            lIdPyme = sdbcPYME.Columns("ID").Value
        End If
    End If
    
    CargarVariablesCalidad lIdPyme, (chkVerBaja.Value = vbChecked)
    cmdGuardar.Visible = False
    cmdDeshacer.Visible = False
    lblCambios.Visible = False
    cmdCalcular.Visible = True
    Arrange
End Sub

Private Sub cmdModificarVar_Click()
Dim sCadena As String
Dim b As Boolean
    'si hay cambios los guarda
    If sdbgSubVariables.DataChanged = True Then
        sdbgSubVariables.Update
        If m_bError = True Then Exit Sub
    End If

    'A�ade una variable de nivel 1:
    b = FSGSForm.MostrarFormVARCalAnya(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, False, IIf(Me.chkMultiidioma.Value = vbChecked, True, False), m_oIdiomas, 0, g_oVarsCalidad1Mod, 0, sCadena, gLongitudesDeCodigos.giLongCodVarCal, g_oVarsCalidad1Mod.Item("1" & Mid(ssTabVar.selectedItem.key, 2)))
    
    If b Then ssTabVar.selectedItem.caption = sCadena
End Sub

Private Sub Form_Activate()
    On Error GoTo Error
    
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
    
    If Not m_bActivado Then m_bActivado = True

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmVARCalidad", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    Dim oIdioma As CIdioma

    On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub

    CargarRecursos
    PonerFieldSeparator Me
    
    If Not gParametrosGenerales.gbQAVariableMaterialAsig Then
        sdbgSubVariables.Columns("MAT").Visible = False
    End If
    m_bCambioTotProveedores = False
    sdbddTipo.AddItem ""
    sdbgSubVariables.Columns("TIPO").DropDownHwnd = sdbddTipo.hWnd
    sdbddSubTipo.AddItem ""
    sdbgSubVariables.Columns("SUBTIPO").DropDownHwnd = sdbddSubTipo.hWnd
    sdbddOrigen.AddItem ""
    sdbgSubVariables.Columns("ORIGEN").DropDownHwnd = sdbddOrigen.hWnd
    sdbgSubVariables.Columns("COD").FieldLen = gLongitudesDeCodigos.giLongCodVarCal

    'Carga las columnas de los diferentes idiomas de la grid (la 1� ser� la del idioma de la aplicaci�n):
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(, , False)
       
    Dim i, iPosition As Byte
    
    i = sdbgSubVariables.Columns.Count
    iPosition = 2
    
    sdbgSubVariables.Columns.Add i
    sdbgSubVariables.Columns(i).Name = gParametrosInstalacion.gIdioma
    sdbgSubVariables.Columns(i).caption = m_sNombreDen
    sdbgSubVariables.Columns(i).Position = iPosition

     For Each oIdioma In m_oIdiomas
        If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
            i = i + 1
            iPosition = iPosition + 1
            sdbgSubVariables.Columns.Add i
            sdbgSubVariables.Columns(i).Name = oIdioma.Cod
            sdbgSubVariables.Columns(i).caption = oIdioma.Den
            sdbgSubVariables.Columns(i).Position = iPosition
            sdbgSubVariables.Columns(i).Visible = False
        Else
            m_sIdiomaSPA = oIdioma.Den
        End If
    Next
    
    If Not (gParametrosGenerales.gbPymes) Then
        CargarVariablesCalidad
        ssTabVar.Tabs(1).Selected = True
    Else
        bloquearDesbloquearPantalla (True)
    End If
    

    Me.Width = 12055
    Me.Height = 6735
        
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
        
    Set oIdioma = Nothing
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmVARCalidad", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Redimensiona la grid de variables y total proveedor
''' </summary>
''' <remarks>Llamada desde=Evento que salta al redimensionar el formulario; Tiempo m�ximo=0</remarks>
Private Sub Form_Resize()

    Arrange
    sdbgSubVariables.Columns("DEN").Visible = False
    If sdbgSubVariables.Columns("MAT").Visible = True Then
        sdbgSubVariables.Columns("COD").Width = sdbgSubVariables.Width * 0.07
        sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).Width = (sdbgSubVariables.Width * 0.38) - 580
        sdbgSubVariables.Columns("TIPO").Width = sdbgSubVariables.Width * 0.09
        sdbgSubVariables.Columns("SUB").Width = sdbgSubVariables.Width * 0.05
        sdbgSubVariables.Columns("SUBTIPO").Width = sdbgSubVariables.Width * 0.11
        sdbgSubVariables.Columns("ORIGEN").Width = sdbgSubVariables.Width * 0.1
        sdbgSubVariables.Columns("VALOR_DEF").Width = sdbgSubVariables.Width * 0.06
        sdbgSubVariables.Columns("PUNT").Width = sdbgSubVariables.Width * 0.08
        sdbgSubVariables.Columns("MAT").Width = sdbgSubVariables.Width * 0.076
    Else
        sdbgSubVariables.Columns("COD").Width = sdbgSubVariables.Width * 0.07
        sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).Width = (sdbgSubVariables.Width * 0.315) - 580
        sdbgSubVariables.Columns("TIPO").Width = sdbgSubVariables.Width * 0.09
        sdbgSubVariables.Columns("SUB").Width = sdbgSubVariables.Width * 0.05
        sdbgSubVariables.Columns("SUBTIPO").Width = sdbgSubVariables.Width * 0.11
        sdbgSubVariables.Columns("ORIGEN").Width = sdbgSubVariables.Width * 0.155
        sdbgSubVariables.Columns("VALOR_DEF").Width = sdbgSubVariables.Width * 0.06
        sdbgSubVariables.Columns("PUNT").Width = sdbgSubVariables.Width * 0.08
    End If
        
    sdbgTotProveedor.Columns("COD").Width = sdbgTotProveedor.Width * 0.15
    sdbgTotProveedor.Columns("DEN").Width = sdbgTotProveedor.Width * 0.65
    sdbgTotProveedor.Columns("DEN_IDIOMAS").Width = sdbgTotProveedor.Width * 0.65
    sdbgTotProveedor.Columns("VALOR_DEF").Width = sdbgTotProveedor.Width * 0.15
    
   If Me.WindowState = 0 Then
    If Me.Height <= 7770 Then
        Me.Height = 7770
        If Me.Width <= 12165 Then
            Me.Width = 12165
        End If
        Exit Sub
    End If
    If Me.Width <= 12165 Then
        Me.Width = 12165
        If Me.Height <= 7770 Then
            Me.Height = 7770
        End If
        Exit Sub
    End If
End If
    
End Sub
Private Sub Arrange()

On Error Resume Next

    'Redimensiona el formulario
    If Me.Width < 2000 Then Exit Sub
    If Me.Height < 4000 Then Exit Sub
    
    Me.Picture1.Visible = True 'Combo pymes
    
    If gParametrosGenerales.gbPymes Then
        Me.sdbcPYME.Visible = True
        Me.chkMultiidioma.Left = 4500
    Else
        Me.lblPYME.Visible = False
        Me.sdbcPYME.Visible = False
        Me.chkMultiidioma.Left = 50
    End If
    chkVerBaja.Left = Me.chkMultiidioma.Left + Me.chkMultiidioma.Width
   
    If lblCambios.Visible = False Then
        ssTabVar.Height = Me.Height - 1300
    Else
        ssTabVar.Height = Me.Height - 1800
    End If

    ssTabVar.Top = 550
    lblCambios.Top = ssTabVar.Top + ssTabVar.Height '+ 100
    ssTabVar.Width = Me.Width - 330
    lblCambios.Width = ssTabVar.Width
    picDatos.Width = ssTabVar.Width - 195
    picDatos.Height = ssTabVar.Height - 720
    picDatos.Top = ssTabVar.Top + 350
    fraSubVariables.Width = picDatos.Width - 120
    fraSubVariables.Height = picDatos.Height - 1000
    sdbgSubVariables.Width = fraSubVariables.Width - 255
    sdbgSubVariables.Height = fraSubVariables.Height - 790 - PicBajaLogica.Height
    fraFormula.Width = fraSubVariables.Width / 2
    fraFormula.Top = fraSubVariables.Top + fraSubVariables.Height
    
    fraConfiguracion.Width = fraSubVariables.Width / 2
    fraConfiguracion.Left = fraFormula.Width + 50
    fraConfiguracion.Top = fraFormula.Top
    fraConfiguracion.Height = fraFormula.Height
    sdbcVarPond.Top = optOpcionConf(1).Top + 50
    sdbcVarPond.Width = fraConfiguracion.Width / 4 - 200
    optOpcionConf(0).Width = fraConfiguracion.Width / 4 - 200
    optOpcionConf(1).Width = fraConfiguracion.Width / 4 - 200
    optOpcionConf(2).Width = fraConfiguracion.Width / 4 - 600
    optOpcionConf(0).Height = 400
    optOpcionConf(1).Height = 400
    optOpcionConf(2).Height = 400
    optOpcionConf(0).Left = 100
    optOpcionConf(1).Left = optOpcionConf(0).Left + optOpcionConf(0).Width + 100
    sdbcVarPond.Left = optOpcionConf(1).Left + optOpcionConf(1).Width + 100
    optOpcionConf(2).Left = sdbcVarPond.Left + sdbcVarPond.Width + 600
        
    picProveedor.Width = ssTabVar.Width - 415
    picProveedor.Height = ssTabVar.Height - 620
    picProveedor.Top = picDatos.Top
    sdbgTotProveedor.Width = picProveedor.Width - 200
    sdbgTotProveedor.Height = picProveedor.Height - 1080
    picFormula.Top = sdbgTotProveedor.Top + sdbgTotProveedor.Height + 120
    PicBajaLogica.Top = sdbgSubVariables.Top + sdbgSubVariables.Height + 100
    PicBajaLogica.Left = (sdbgSubVariables.Width - picBotonesSubvar.Width)
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_VAR_CALIDAD, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        With Ador
            Me.caption = Ador(0).Value  '1 Variables de calidad
            .MoveNext
            cmdAyadirVar.caption = Ador(0).Value '2 &A�adir variable
            .MoveNext
            cmdEliminar.caption = Ador(0).Value '3 &Eliminar
            .MoveNext
            fraSubVariables.caption = Ador(0).Value '4 Subvariables
            .MoveNext
            fraFormula.caption = Ador(0).Value '5 F�rmula para calcular la puntuaci�n de la variable
            .MoveNext
            cmdGuardar.caption = Ador(0).Value '6 &Guardar cambios
            .MoveNext
            cmdDeshacer.caption = Ador(0).Value '7 &Deshacer cambios
            .MoveNext
            sdbgSubVariables.Columns("COD").caption = Ador(0).Value '8 Identificador
            sdbgTotProveedor.Columns("COD").caption = Ador(0).Value '8 Identificador
            .MoveNext
            m_sNombreDen = Ador(0).Value '9 Nombre
            sdbgSubVariables.Columns("DEN").caption = m_sNombreDen
            sdbgTotProveedor.Columns("DEN").caption = m_sNombreDen
            sdbgTotProveedor.Columns("DEN_IDIOMAS").caption = m_sNombreDen
            .MoveNext
            sdbgSubVariables.Columns("TIPO").caption = Ador(0).Value '10 Tipo
            .MoveNext
            sdbgSubVariables.Columns("SUBTIPO").caption = Ador(0).Value '11 SubTipo
            .MoveNext
            sdbgSubVariables.Columns("ORIGEN").caption = Ador(0).Value '12 Origen de datos
            .MoveNext
            sdbgSubVariables.Columns("PUNT").caption = Ador(0).Value '13 Puntuaci�n
            .MoveNext
            sdbgSubVariables.Columns("MAT").caption = Ador(0).Value '14 Materiales
            .MoveNext
            m_sTotProveedor = Ador(0).Value '15 Total proveedor
            .MoveNext
            lblFormulaProv.caption = Ador(0).Value '16 F�rmula para calcular el total por proveedor:
            .MoveNext
            m_sTipo(0) = Ador(0).Value '17 Simple
            .MoveNext
            m_sTipo(1) = Ador(0).Value '18 Compuesta
            .MoveNext
            m_sSubTipo(1) = Ador(0).Value '19 Integraci�n
            .MoveNext
            m_sSubTipo(2) = Ador(0).Value '20 Manual
            .MoveNext
            m_sSubTipo(3) = Ador(0).Value '21 Certificado
            .MoveNext
            m_sSubTipo(4) = Ador(0).Value '22 No conformidad
            For i = 1 To 11
                .MoveNext
                m_sIdiErrorFormula(i) = Ador(0).Value
            Next i
            .MoveNext
            .MoveNext
            .MoveNext
            .MoveNext
            .MoveNext
            'lblCambios.Caption = Ador(0).Value '38
            .MoveNext
            cmdModificarVar.caption = Ador(0).Value '39 &Modificar
            .MoveNext
            lblCambios.caption = Ador(0).Value '40
            .MoveNext
            m_sMsgBox = Ador(0).Value
            .MoveNext
            .MoveNext
            sdbgSubVariables.Columns("VALOR_DEF").caption = Ador(0).Value 'Val.defecto
            sdbgTotProveedor.Columns("VALOR_DEF").caption = Ador(0).Value 'Val.defecto
            .MoveNext
            .MoveNext
            cmdCalcular.caption = Ador(0).Value
            .MoveNext
            'Nuevos Textos
            Me.lblPYME.caption = Ador(0).Value
            .MoveNext
            Me.chkMultiidioma.caption = Ador(0).Value
            .MoveNext
            m_sSubTipo(5) = Ador(0).Value '48 PPM
            .MoveNext
            m_sSubTipo(6) = Ador(0).Value  '49 Cargo a proveedores
            .MoveNext
            m_sSubTipo(7) = Ador(0).Value '50 Tasa de Servicio
            .MoveNext
            chkVerBaja.caption = Ador(0).Value '51 Ver bajas l�gicas
            .MoveNext
            m_sBaja = Ador(0).Value '52 dBaja l�gica"
            Me.cmdBajaVar.caption = m_sBaja
            lblBajaLog.caption = "(!)" & m_sBaja
            .MoveNext
            m_sDeshacerBaja = Ador(0).Value '53 dDeshacer baja l�gica"
            .MoveNext
            m_sIdiErrorFormula(12) = Ador(0).Value '54 Existen variables de calidad dados de baja con el mismo c�digo
            .MoveNext
            m_sMensajeErrorNumerico = Ador(0).Value 'debe ser n�merico"
            .MoveNext
            .MoveNext
            sdbgSubVariables.Columns("CERT_VALOR_SINCUMPL").caption = Ador(0).Value 'Val. cert. sin cumpl.
            .MoveNext
            m_sSubTipo(8) = Ador(0).Value 'Encuesta
                        
            Ador.MoveNext
            fraConfiguracion.caption = Ador(0).Value ' Selecci�n del modo de c�lculo para unidades de negocio de nivel superior:
            Ador.MoveNext
            optOpcionConf(0).caption = Ador(0).Value ' Evaluar f�rmula
            Ador.MoveNext
            optOpcionConf(1).caption = Ador(0).Value ' Media ponderada seg�n:
            Ador.MoveNext
            optOpcionConf(2).caption = Ador(0).Value ' No calcular
            
            .Close
        End With
    End If

    Set Ador = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo Error
    
    If m_bDescargarFrm Then
         m_bDescargarFrm = False
         oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
   
    m_bCambioTotProveedores = False
    Set g_oVarsCalidad1 = Nothing
    Set g_oVarsCalidad1Mod = Nothing

    m_bRespetarColor = False
    m_bError = False
    
    Exit Sub
Error:
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmVARCalidad", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub

Private Sub sdbddOrigen_CloseUp()
    sdbgSubVariables.Columns("ORIGEN").Value = sdbddOrigen.Columns("COD").Value
    sdbgSubVariables.Columns("ID_ORIGEN").Value = sdbddOrigen.Columns("ID").Value
    sdbgSubVariables.Columns("ID_FORM").Value = sdbddOrigen.Columns("FORM").Value
End Sub

Private Sub sdbddOrigen_DropDown()
    Dim oSolicitudes As CSolicitudes
    Dim Ador As ADODB.Recordset

    Screen.MousePointer = vbHourglass
    
    sdbddOrigen.RemoveAll
    
    'Carga las solicitudes de tipo certificado o no conformidad,seg�n lo que se haya seleccionado en subtipo
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    Select Case sdbgSubVariables.Columns("ID_SUBTIPO").Value
        Case CalidadSubtipo.CalCertificado
            Set Ador = oSolicitudes.DevolverSolicitudesDeTipo(TipoSolicitud.Certificados)
        Case CalidadSubtipo.CalNoConformidad
            Set Ador = oSolicitudes.DevolverSolicitudesDeTipo(TipoSolicitud.NoConformidades)
        Case CalidadSubtipo.CalEncuesta
            Set Ador = oSolicitudes.DevolverSolicitudesDeTipo(TipoSolicitud.Encuesta)
    End Select
    
    While Not Ador.EOF
        sdbddOrigen.AddItem Ador.Fields("COD").Value & Chr(m_lSeparador) & Ador.Fields("COD").Value & "-" & Ador.Fields("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & Ador.Fields("ID").Value & Chr(m_lSeparador) & Ador.Fields("FORMULARIO").Value
        Ador.MoveNext
    Wend
    
    If sdbddOrigen.Rows = 0 Then sdbddOrigen.AddItem ""

    Ador.Close
    Set Ador = Nothing
    Set oSolicitudes = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddOrigen_InitColumnProps()
    sdbddOrigen.DataFieldList = "Column 1"
    sdbddOrigen.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddOrigen_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddOrigen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOrigen.Rows - 1
            bm = sdbddOrigen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOrigen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgSubVariables.Columns("ORIGEN").Value = Mid(sdbddOrigen.Columns(0).CellText(bm), 1, Len(Text))
                sdbddOrigen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddSubTipo_CloseUp()
    With sdbgSubVariables
        If .Columns("ID_TIPO").Value = "0" And sdbddSubTipo.Columns("ID").Value = CStr(CalCertificado) Then
            If .Columns("ID_SUBTIPO").Value <> sdbddSubTipo.Columns("ID").Value Then .Columns("CERT_VALOR_SINCUMPL").Value = cnValCertNoCumplDefecto
            .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "", .Row
        Else
            .Columns("CERT_VALOR_SINCUMPL").Value = ""
            .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "Bloqueado", .Row
        End If
        
        .Columns("ID_SUBTIPO").Value = sdbddSubTipo.Columns("ID").Value
        .Columns("SUBTIPO").Value = sdbddSubTipo.Columns("NOMBRE").Value
        .Columns("ORIGEN").Value = ""
        .Columns("ID_ORIGEN").Value = ""
        .Columns("ID_FORM").Value = ""
        
        If .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalCertificado Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalNoConformidad Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalEncuesta Then
            .Columns("ORIGEN").CellStyleSet "", sdbgSubVariables.Row
        Else
            .Columns("ORIGEN").CellStyleSet "Bloqueado", .Row
        End If
    End With
End Sub

Private Sub sdbddSubTipo_DropDown()
    Screen.MousePointer = vbHourglass
    
    With sdbddSubTipo
        .RemoveAll
        
        .AddItem m_sSubTipo(1) & Chr(m_lSeparador) & CalidadSubtipo.CalIntegracion
        .AddItem m_sSubTipo(2) & Chr(m_lSeparador) & CalidadSubtipo.CalManual
        If basParametros.gParametrosGenerales.gbAccesoQACertificados Then .AddItem m_sSubTipo(3) & Chr(m_lSeparador) & CalidadSubtipo.CalCertificado
        If basParametros.gParametrosGenerales.gbAccesoQANoConformidad Then .AddItem m_sSubTipo(4) & Chr(m_lSeparador) & CalidadSubtipo.CalNoConformidad
        .AddItem m_sSubTipo(5) & Chr(m_lSeparador) & CalidadSubtipo.CalPPM
        .AddItem m_sSubTipo(6) & Chr(m_lSeparador) & CalidadSubtipo.CalCargoProveedores
        .AddItem m_sSubTipo(7) & Chr(m_lSeparador) & CalidadSubtipo.CalTasaServicios
        .AddItem m_sSubTipo(8) & Chr(m_lSeparador) & CalidadSubtipo.CalEncuesta
    End With
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddSubTipo_InitColumnProps()
    sdbddSubTipo.DataFieldList = "Column 0"
    sdbddSubTipo.DataFieldToDisplay = "Column 0"
End Sub

''' * Objetivo: Posicionarse en el combo segun la seleccion
Private Sub sdbddSubTipo_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddSubTipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddSubTipo.Rows - 1
            bm = sdbddSubTipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddSubTipo.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgSubVariables.Columns("SUBTIPO").Value = Mid(sdbddSubTipo.Columns(0).CellText(bm), 1, Len(Text))
                sdbddSubTipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddTipo_CloseUp()
    With sdbgSubVariables
        .Columns("ID_TIPO").Value = sdbddTipo.Columns("ID").Value
        .Columns("TIPO").Value = sdbddTipo.Columns("NOMBRE").Value
        
        If sdbddTipo.Columns("ID").Value = 0 Then  'Es simple
            .Columns("SUB").Value = ""
            .Columns("SUBTIPO").CellStyleSet "", .Row
            
            If .Columns("ID_SUBTIPO").Value = CStr(CalCertificado) Then
                If .Columns("ID_TIPO").Value <> sdbddTipo.Columns("ID").Value Then .Columns("CERT_VALOR_SINCUMPL").Value = cnValCertNoCumplDefecto
                .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "", .Row
            Else
                .Columns("CERT_VALOR_SINCUMPL").Value = ""
                .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "Bloqueado", .Row
            End If
        Else
            .Columns("SUB").Value = "..."  'Si es compuesta
            .Columns("ID_SUBTIPO").Value = ""
            .Columns("SUBTIPO").Value = ""
            .Columns("ORIGEN").Value = ""
            .Columns("ID_ORIGEN").Value = ""
            .Columns("ID_FORM").Value = ""
            .Columns("CERT_VALOR_SINCUMPL").Value = ""
            .Columns("SUBTIPO").CellStyleSet "Bloqueado", sdbgSubVariables.Row
            .Columns("ORIGEN").CellStyleSet "Bloqueado", sdbgSubVariables.Row
            .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "Bloqueado", .Row
        End If
    End With
End Sub

Private Sub sdbddTipo_DropDown()
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    sdbddTipo.RemoveAll
    
    For i = 0 To 1
        sdbddTipo.AddItem m_sTipo(i) & Chr(m_lSeparador) & i
    Next
            
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddTipo_InitColumnProps()
    sdbddTipo.DataFieldList = "Column 0"
    sdbddTipo.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddTipo_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddTipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddTipo.Rows - 1
            bm = sdbddTipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddTipo.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgSubVariables.Columns("TIPO").Value = Mid(sdbddTipo.Columns(0).CellText(bm), 1, Len(Text))
                sdbddTipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbgSubVariables_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = 0
End Sub

''' <summary>Evento que salta cuando se modifica una fila de la grid y pierde el foco. Modifica la variable de nivel 2. Si no existia antes lo a�ade y sino lo modifica.</summary>
''' <returns>No devuelve nada</returns>
''' <remarks>Llamada desde=propio formulario; Tiempo m�ximo=0,2seg</remarks>

Private Sub sdbgSubVariables_BeforeUpdate(Cancel As Integer)
    Dim oVarCal1 As CVariableCalidad
    Dim oVarCal2 As CVariableCalidad
    Dim i As Byte
    Dim sCod As String
    Dim sDen As String
    
    m_bError = False
    
    With sdbgSubVariables
        If .col < 0 Then Exit Sub
            
        If .Columns("COD").Value = "" Then
            m_bError = True
            oMensajes.NoValido .Columns("COD").caption
            Cancel = True
            Exit Sub
        End If
        'Solo con letras y n�meros
        If Not NombreDeVariableValido(.Columns("COD").Value) Then
            m_bError = True
            oMensajes.NoValido 176
            Cancel = True
            Exit Sub
        End If
        
        If Me.chkMultiidioma.Value = vbChecked Then
            For i = 1 To m_oIdiomas.Count
                If .Columns(m_oIdiomas.Item(i).Cod).Value = "" Then
                    m_bError = True
                    oMensajes.NoValido .Columns(m_oIdiomas.Item(i).Cod).caption
                    Cancel = True
                    Exit Sub
                End If
            Next
        Else
            If .Columns(gParametrosInstalacion.gIdioma).Value = "" Then
                m_bError = True
                oMensajes.NoValido .Columns(gParametrosInstalacion.gIdioma).caption
                Cancel = True
                Exit Sub
            End If
        End If
        
        If .Columns("TIPO").Value = "" Then
            m_bError = True
            oMensajes.NoValido .Columns("TIPO").caption
            Cancel = True
            Exit Sub
        End If
        If .Columns("ID_TIPO").Value = 0 Then
            If .Columns("SUBTIPO").Value = "" Then
                m_bError = True
                oMensajes.NoValido .Columns("SUBTIPO").caption
                Cancel = True
                Exit Sub
            End If
            If .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalCertificado Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalNoConformidad Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalEncuesta Then
                If .Columns("ORIGEN").Value = "" Then
                    m_bError = True
                    oMensajes.NoValido .Columns("ORIGEN").caption
                    Cancel = True
                    Exit Sub
                End If
            End If
        End If
        
        If .IsAddRow Then
            If m_bGuardarTabAnt Then
                Set oVarCal1 = g_oVarsCalidad1Mod.Item("1" & CStr(m_lIdCal1Ant))
            Else
                Set oVarCal1 = g_oVarsCalidad1Mod.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2)))
            End If
                
            Set oVarCal2 = oFSGSRaiz.Generar_CVariableCalidad
            oVarCal2.Id = g_oVarsCalidad1Mod.MaxID2
            g_oVarsCalidad1Mod.MaxID2 = g_oVarsCalidad1Mod.MaxID2 + 1
            oVarCal2.IdVarCal1 = oVarCal1.Id
            oVarCal2.Nivel = 2
            oVarCal2.Cod = .Columns("COD").Value
        
        '    'Denominaciones multiIdioma
            Set oVarCal2.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas
            For i = 1 To m_oIdiomas.Count
                sCod = m_oIdiomas.Item(i).Cod
                sDen = .Columns(sCod).Value
                oVarCal2.Denominaciones.Add sCod, sDen
            Next
            
            oVarCal2.Tipo = .Columns("ID_TIPO").Value
            If oVarCal2.Tipo = 1 Then
                oVarCal2.Subtipo = Null
                oVarCal2.Origen = Null
                oVarCal2.OrigenCod = Null
                oVarCal2.FormularioCOD = Null
                oVarCal2.FormularioID = Null
                oVarCal2.TipoPonderacion = PondNoPonderacion
                oVarCal2.NCPeriodo = Null
            Else
                oVarCal2.Subtipo = StrToNull(.Columns("ID_SUBTIPO").Value)
                If oVarCal2.Subtipo = CalidadSubtipo.CalCertificado Or oVarCal2.Subtipo = CalidadSubtipo.CalNoConformidad Or oVarCal2.Subtipo = CalidadSubtipo.CalEncuesta Then
                    oVarCal2.Origen = StrToNull(.Columns("ID_ORIGEN").Value)
                    oVarCal2.OrigenCod = StrToNull(.Columns("ORIGEN").Value)
                    oVarCal2.FormularioID = StrToNull(.Columns("ID_FORM").Value)
                    oVarCal2.FormularioCOD = Null
                Else
                    oVarCal2.Origen = Null
                    oVarCal2.OrigenCod = Null
                    oVarCal2.FormularioCOD = Null
                    oVarCal2.FormularioID = Null
                End If
                Select Case oVarCal2.Subtipo
                    Case CalidadSubtipo.CalEncuesta
                        oVarCal2.TipoPonderacion = PondEncuesta
                            
                    Case CalidadSubtipo.CalCertificado
                        oVarCal2.TipoPonderacion = PondCertificado
                    Case CalidadSubtipo.CalNoConformidad
                        oVarCal2.TipoPonderacion = PondNCMediaPesos
                        oVarCal2.NCPeriodo = 6
                        oVarCal2.Formula = "X1"
                        CargaListaVariablesFormula oVarCal2
                    
                    Case CalidadSubtipo.CalIntegracion
                        oVarCal2.TipoPonderacion = PondIntFormula
                        oVarCal2.Formula = "X"
                        
                    Case CalidadSubtipo.CalManual
                        oVarCal2.TipoPonderacion = PondManual
                        
                    Case CalidadSubtipo.CalPPM
                        oVarCal2.TipoPonderacion = PondPPM
                        oVarCal2.NCPeriodo = 6
                        oVarCal2.Formula = "X1"
                        CargaListaVariablesFormula oVarCal2
                        
                    Case CalidadSubtipo.CalCargoProveedores
                        oVarCal2.TipoPonderacion = PondCargoProveedores
                        oVarCal2.NCPeriodo = 6
                        oVarCal2.Formula = "X1"
                        CargaListaVariablesFormula oVarCal2
                    
                    Case CalidadSubtipo.CalTasaServicios
                        oVarCal2.TipoPonderacion = PondTasaServicios
                        oVarCal2.NCPeriodo = 6
                        oVarCal2.Formula = "X1"
                        CargaListaVariablesFormula oVarCal2
                End Select
            End If
        
            If Not IsNumeric(.Columns("VALOR_DEF").Value) Then .Columns("VALOR_DEF").Value = 0
            oVarCal2.ValorDefecto = StrToDblOrNull(.Columns("VALOR_DEF").Value)
            
            If Not IsNumeric(.Columns("CERT_VALOR_SINCUMPL").Value) Then .Columns("CERT_VALOR_SINCUMPL").Value = 0
            oVarCal2.ValorCertSinCumpl = StrToDblOrNull(.Columns("CERT_VALOR_SINCUMPL").Value)
            
            oVarCal2.modificado = True
            oVarCal1.VariblesCal.Add oVarCal2.Id, 2, oVarCal2.Cod, oVarCal2.Denominaciones, oVarCal2.Tipo, oVarCal2.Subtipo, oVarCal2.Formula, oVarCal2.Origen, , , oVarCal2.IdVarCal1, , , , oVarCal2.OrigenCod, True, , oVarCal2.FormularioID, , , , oVarCal2.TipoPonderacion, oVarCal2.NCPeriodo, , , , , , , oVarCal2.UnidadNegQA, oVarCal2.UnidadNegQADen, oVarCal2.ValorDefecto, , , , , , , , , , , oVarCal2.VariablesXi, oVarCal2.ValorCertSinCumpl
            .Columns("ID").Value = oVarCal2.Id
            Set oVarCal1 = Nothing
            Set oVarCal2 = Nothing
            
            VisualizarGuardar
        Else
            If m_bGuardarTabAnt Then
                Set oVarCal2 = g_oVarsCalidad1Mod.Item("1" & CStr(m_lIdCal1Ant)).VariblesCal.Item("2" & CStr(.Columns("ID").Value))
            Else
                Set oVarCal2 = g_oVarsCalidad1Mod.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))).VariblesCal.Item("2" & CStr(.Columns("ID").Value))
            End If
            
            oVarCal2.Cod = .Columns("COD").Value
            
            'Denominaciones multiIdioma
            For i = 1 To oVarCal2.Denominaciones.Count
                sCod = oVarCal2.Denominaciones.Item(i).Cod
                oVarCal2.Denominaciones.Item(sCod).Den = .Columns(sCod).Value
            Next
            
            oVarCal2.Tipo = CInt(.Columns("ID_TIPO").Value)
            If oVarCal2.Tipo = 1 Then
                oVarCal2.Subtipo = Null
                oVarCal2.Origen = Null
                oVarCal2.OrigenCod = Null
                oVarCal2.FormularioCOD = Null
                oVarCal2.FormularioID = Null
                oVarCal2.TipoPonderacion = PondNoPonderacion
                oVarCal2.NCPeriodo = Null
            Else
                If NullToStr(oVarCal2.Subtipo) <> .Columns("ID_SUBTIPO").Value Then
                    Select Case oVarCal2.Subtipo
                        Case CalidadSubtipo.CalCertificado, CalidadSubtipo.CalEncuesta
                            Set oVarCal2.CamposPond = Nothing
                            oVarCal2.Formula = Null
                        Case CalidadSubtipo.CalNoConformidad, CalidadSubtipo.CalPPM, CalidadSubtipo.CalCargoProveedores, CalidadSubtipo.CalTasaServicios
                            oVarCal2.NCPeriodo = Null
                            oVarCal2.Formula = Null
                            Set oVarCal2.VariablesXi = Nothing
                        Case CalidadSubtipo.CalIntegracion
                            oVarCal2.Formula = Null
                    End Select
                    
                    oVarCal2.Subtipo = StrToNull(.Columns("ID_SUBTIPO").Value)
                    
                    Select Case oVarCal2.Subtipo
                        Case CalidadSubtipo.CalEncuesta
                            oVarCal2.TipoPonderacion = PondEncuesta
                            
                        Case CalidadSubtipo.CalCertificado
                            oVarCal2.TipoPonderacion = PondCertificado
        
                        Case CalidadSubtipo.CalNoConformidad
                            oVarCal2.TipoPonderacion = PondNCMediaPesos
                            oVarCal2.NCPeriodo = 6
                            oVarCal2.Formula = "X1"
                            
                            CargaListaVariablesFormula oVarCal2
                        
                        Case CalidadSubtipo.CalIntegracion
                            oVarCal2.TipoPonderacion = PondIntFormula
                            oVarCal2.Formula = "X"
                            
                        Case CalidadSubtipo.CalManual
                            oVarCal2.TipoPonderacion = PondManual
                        
                        Case CalidadSubtipo.CalPPM
                            oVarCal2.TipoPonderacion = PondPPM
                            oVarCal2.NCPeriodo = 6
                            oVarCal2.Formula = "X1"
                            
                            CargaListaVariablesFormula oVarCal2
        
                        Case CalidadSubtipo.CalCargoProveedores
                            oVarCal2.TipoPonderacion = PondCargoProveedores
                            oVarCal2.NCPeriodo = 6
                            oVarCal2.Formula = "X1"
        
                            CargaListaVariablesFormula oVarCal2
        
                        Case CalidadSubtipo.CalTasaServicios
                            oVarCal2.TipoPonderacion = PondTasaServicios
                            oVarCal2.NCPeriodo = 6
                            oVarCal2.Formula = "X1"
                    
                            CargaListaVariablesFormula oVarCal2
                    End Select
                End If
                
                If oVarCal2.Subtipo = CalidadSubtipo.CalCertificado Or oVarCal2.Subtipo = CalidadSubtipo.CalNoConformidad Or oVarCal2.Subtipo = CalidadSubtipo.CalEncuesta Then
                    oVarCal2.Origen = CDbl(.Columns("ID_ORIGEN").Value)
                    oVarCal2.OrigenCod = StrToNull(.Columns("ORIGEN").Value)
                    If VarToDec0(oVarCal2.FormularioID) <> VarToDec0(.Columns("ID_FORM").Value) Then
                        oVarCal2.FormularioID = StrToNull(.Columns("ID_FORM").Value)
                        oVarCal2.FormularioCOD = Null
                        Set oVarCal2.CamposPond = Nothing
                    End If
                Else
                    oVarCal2.Origen = Null
                    oVarCal2.OrigenCod = Null
                    oVarCal2.FormularioCOD = Null
                    oVarCal2.FormularioID = Null
                End If
            End If
        
            If Not IsNumeric(.Columns("VALOR_DEF").Value) Then .Columns("VALOR_DEF").Value = 0
            oVarCal2.ValorDefecto = StrToDblOrNull(.Columns("VALOR_DEF").Value)
            
            If Not IsNumeric(.Columns("CERT_VALOR_SINCUMPL").Value) Then .Columns("CERT_VALOR_SINCUMPL").Value = 0
            oVarCal2.ValorCertSinCumpl = StrToDblOrNull(.Columns("CERT_VALOR_SINCUMPL").Value)
            
            If m_bGuardarTabAnt Then
                If ComprobarModificadoVar2(CStr(m_lIdCal1Ant), oVarCal2.Id) Then
                    VisualizarGuardar
                End If
            Else
                If ComprobarModificadoVar2(Mid(ssTabVar.selectedItem.key, 2), oVarCal2.Id) Then
                    VisualizarGuardar
                End If
            End If
        End If
    End With
End Sub

Private Sub sdbgSubVariables_BtnClick()
    Dim oVarCal2 As CVariableCalidad
    Dim oVarCal2Orig As CVariableCalidad
    Dim bCargarMultiIdioma As Boolean
    
    With sdbgSubVariables
        If .Rows = 0 Then Exit Sub
        
        If .DataChanged Then
            .Update
            If m_bError Then Exit Sub
        End If
        
        If .Columns(.col).Name = "SUB" Then
            If .Columns("ID_TIPO").Value = 1 Then
                CargarVariablesNivel3 Mid(ssTabVar.selectedItem.key, 2), .Columns("ID").Value
            End If
            Exit Sub
        End If
        
        Select Case .Columns(.col).Name
            Case "MAT"
                Set oVarCal2 = g_oVarsCalidad1Mod.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))).VariblesCal.Item("2" & CStr(.Columns("ID").Value))
                If Not oVarCal2 Is Nothing Then
                    If oVarCal2.Tipo = 0 Then
                        Dim bExiste As Boolean
                        If Not g_oVarsCalidad1.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))) Is Nothing Then
                            If Not g_oVarsCalidad1.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))).VariblesCal Is Nothing Then
                                If Not g_oVarsCalidad1.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))).VariblesCal.Item("2" & CStr(.Columns("ID").Value)) Is Nothing Then
                                    bExiste = True
                                End If
                            End If
                        End If
                                        
                        If bExiste Then
                            'Comprobar si ha habido cambios en la f�rmula antes de abrir la pantalla de asignaci�n de materiales
                            If Not VarCalHayCambiosFormula(oVarCal2, g_oVarsCalidad1.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))).VariblesCal.Item("2" & CStr(.Columns("ID").Value))) Then
                                MostrarFormVARCalMaterial oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, basParametros.gLongitudesDeCodigos, oVarCal2, bExiste, _
                                    oMensajes, gParametrosGenerales, oGestorSeguridad, oUsuarioSummit.Cod
                            Else
                                oMensajes.VarCalHayCambiosObjetivosSuelosEnFormula
                            End If
                        Else
                            MostrarFormVARCalMaterial oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, basParametros.gLongitudesDeCodigos, oVarCal2, bExiste, _
                                    oMensajes, gParametrosGenerales, oGestorSeguridad, oUsuarioSummit.Cod
                        End If
                    End If
                End If
                Set oVarCal2 = Nothing
        
            Case "PUNT"
                Set oVarCal2 = g_oVarsCalidad1Mod.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))).VariblesCal.Item("2" & CStr(.Columns("ID").Value))
                If Not oVarCal2 Is Nothing Then
                    If oVarCal2.Tipo = 0 Then
                        Set oVarCal2Orig = Nothing
                        If Not g_oVarsCalidad1.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))) Is Nothing Then
                            If Not g_oVarsCalidad1.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))).VariblesCal Is Nothing Then
                                Set oVarCal2Orig = g_oVarsCalidad1.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))).VariblesCal.Item("2" & CStr(.Columns("ID").Value))
                            End If
                        End If
                        
                        If oVarCal2.Subtipo = CalidadSubtipo.CalCertificado Then
                            MostrarFormVARCalPondCert oGestorIdiomas, oGestorParametros, gParametrosInstalacion.gIdioma, oMensajes, oFSGSRaiz, oGestorSeguridad, gParametrosGenerales, oUsuarioSummit.Cod, oVarCal2, oVarCal2Orig, _
                                Me, Nothing, Nothing, Nothing
                        ElseIf oVarCal2.Subtipo = CalidadSubtipo.CalEncuesta Then
                            MostrarFormVARCalPondEnc oGestorIdiomas, gParametrosInstalacion.gIdioma, oMensajes, oFSGSRaiz, oGestorSeguridad, gParametrosGenerales, oUsuarioSummit.Cod, oVarCal2, oVarCal2Orig, _
                                Me, Nothing, Nothing, Nothing
                        ElseIf oVarCal2.Subtipo = CalidadSubtipo.CalIntegracion Then
                            MostrarFormVARCalPondInt oGestorIdiomas, gParametrosInstalacion.gIdioma, oMensajes, oFSGSRaiz, oVarCal2, oVarCal2Orig, Me, Nothing, Nothing, Nothing
                        ElseIf oVarCal2.Subtipo = CalidadSubtipo.CalNoConformidad Then
                            MostrarFormVARCalPondNC oGestorIdiomas, gParametrosInstalacion.gIdioma, oMensajes, oVarCal2, oVarCal2Orig, Me, Nothing, Nothing, Nothing
                            bCargarMultiIdioma = True
                        ElseIf (oVarCal2.Subtipo = CalidadSubtipo.CalPPM) Or (oVarCal2.Subtipo = CalidadSubtipo.CalCargoProveedores) Or (oVarCal2.Subtipo = CalidadSubtipo.CalTasaServicios) Then
                            MostrarFormVARCalSubtipo oGestorIdiomas, gParametrosInstalacion.gIdioma, oMensajes, oVarCal2, Me, Nothing, Nothing, Nothing, oVarCal2.Subtipo
                            bCargarMultiIdioma = True
                        End If
                    End If
                    
                    If oVarCal2.VariablesXi Is Nothing And bCargarMultiIdioma Then CargaListaVariablesFormula oVarCal2
                End If
                Set oVarCal2 = Nothing
        End Select
    End With
End Sub

Private Sub sdbgSubVariables_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim i As Byte

    With sdbgSubVariables
        If .Columns("BAJALOG").Value = "1" Then
            cmdBajaSubvar.Picture = ImageList1.ListImages("NOBAJA").Picture
            cmdBajaSubvar.Tag = "BAJA"
            cmdElimSubvar.Enabled = False
        Else
            cmdBajaSubvar.Picture = ImageList1.ListImages("BAJA").Picture
            cmdBajaSubvar.Tag = "NOBAJA"
            cmdElimSubvar.Enabled = True
        End If
    
        If .col < 0 Then Exit Sub
        
        If gParametrosGenerales.gbPymes = True And Me.sdbcPYME.Value = "" Then Exit Sub
        
        If ((.IsAddRow) And (chkMultiidioma.Value = vbUnchecked)) Then
            m_bSaltar = True
            chkMultiidioma.Value = vbChecked
            m_bSaltar = False
        End If
        
        Select Case .Columns(.col).Name
            Case "TIPO"
                If .Columns("BAJALOG").Value = "1" Then
                    .Columns("TIPO").DropDownHwnd = 0
                Else
                    .Columns("TIPO").DropDownHwnd = sdbddTipo.hWnd
                End If
                
            Case "SUBTIPO"
                If .Columns("ID_TIPO").Value = 0 Then
                    If .Columns("BAJALOG").Value = "1" Then
                        .Columns("SUBTIPO").DropDownHwnd = 0
                    Else
                        .Columns("SUBTIPO").DropDownHwnd = sdbddSubTipo.hWnd
                    End If
                Else
                    .Columns("SUBTIPO").DropDownHwnd = 0
                End If
                
            Case "ORIGEN"
                If .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalCertificado Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalNoConformidad Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalEncuesta Then
                    If .Columns("BAJALOG").Value = "1" Then
                        .Columns("ORIGEN").DropDownHwnd = 0
                    Else
                        .Columns("ORIGEN").DropDownHwnd = sdbddOrigen.hWnd
                    End If
                Else
                    .Columns("ORIGEN").DropDownHwnd = 0
                End If
        End Select
        
        .Columns("SUBTIPO").Locked = (.Columns("BAJALOG").Value = "1")
        .Columns("ORIGEN").Locked = (.Columns("BAJALOG").Value = "1")
        .Columns("COD").Locked = (.Columns("BAJALOG").Value = "1")
        .Columns("TIPO").Locked = (.Columns("BAJALOG").Value = "1")
        .Columns("VALOR_DEF").Locked = (.Columns("BAJALOG").Value = "1")
        .Columns("CERT_VALOR_SINCUMPL").Locked = Not (.Columns("ID_TIPO").Value = "0" And .Columns("ID_SUBTIPO").Value = CStr(CalCertificado))
        For i = 1 To m_oIdiomas.Count
            If Not .Columns(m_oIdiomas.Item(i).Cod) Is Nothing Then
                .Columns(m_oIdiomas.Item(i).Cod).Locked = (.Columns("BAJALOG").Value = "1")
            End If
        Next i
    End With
End Sub

Private Sub sdbgSubVariables_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
        
    With sdbgSubVariables
        If .Columns("ID_TIPO").Value = 1 Then
            .Columns("SUBTIPO").CellStyleSet "Bloqueado"
            .Columns("SUB").Value = "..."
            .Columns("MAT").Value = ""
        Else
            If .Columns("BAJALOG").Value = 1 Then
                .Columns("SUBTIPO").CellStyleSet "StringTachado"
                .Columns("ORIGEN").CellStyleSet "StringTachado"
            Else
                .Columns("SUBTIPO").CellStyleSet ""
                .Columns("ORIGEN").CellStyleSet ""
            End If
            .Columns("SUB").Value = ""
            .Columns("MAT").Value = "..."
        End If
        
        If .Columns("ID_TIPO").Value = "0" And .Columns("ID_SUBTIPO").Value = CStr(CalCertificado) Then
            .Columns("CERT_VALOR_SINCUMPL").CellStyleSet ""
        Else
            .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "Bloqueado"
        End If
        
        'Si selecciona una no conformidad o certificado
        If .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalCertificado Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalNoConformidad Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalEncuesta Then
           If .Columns("BAJALOG").Value = 1 Then
                .Columns("ORIGEN").CellStyleSet "StringTachado"
            Else
                .Columns("ORIGEN").CellStyleSet ""
            End If
        Else
            .Columns("ORIGEN").CellStyleSet "Bloqueado"
        End If
        
        If .Columns("ID_TIPO").Value = 1 Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalManual Or .Columns("ID_SUBTIPO").Value = 0 Then
            .Columns("PUNT").Value = ""
        Else
            .Columns("PUNT").Value = "..."
        End If
        
        If .Columns("BAJALOG").Value = 1 Then
            .Columns("COD").CellStyleSet "StringTachado"
            .Columns("TIPO").CellStyleSet "StringTachado"
            .Columns("VALOR_DEF").CellStyleSet "StringTachado"
    
            For i = 1 To m_oIdiomas.Count
                If Not .Columns(m_oIdiomas.Item(i).Cod) Is Nothing Then
                    .Columns(m_oIdiomas.Item(i).Cod).CellStyleSet "StringTachado"
                End If
            Next i
        End If
    End With
End Sub


''' <summary>
''' Muestra que ha habido cambios y guarda en la coleccion el valor por defecto
''' </summary>
''' <remarks>Llamada desde=Evento que salta al modificar un valor de la fila; Tiempo m�ximo=0</remarks>
Private Sub sdbgTotProveedor_BeforeUpdate(Cancel As Integer)
m_bCambioTotProveedores = False
Dim oVarCal1 As CVariableCalidad
    Set oVarCal1 = g_oVarsCalidad1Mod.Item("1" & CStr(sdbgTotProveedor.Columns("ID").Value))
    sdbgTotProveedor.Columns("VALOR_DEF").Value = Replace(sdbgTotProveedor.Columns("VALOR_DEF").Value, ".", ",")
    sdbgTotProveedor.Columns("VALOR_DEF").Value = FormateoNumerico(sdbgTotProveedor.Columns("VALOR_DEF").Value, "#,##0.00")
    If Not oVarCal1 Is Nothing Then
        If IsNumeric(sdbgTotProveedor.Columns("VALOR_DEF").Value) Then
            oVarCal1.ValorDefecto = sdbgTotProveedor.Columns("VALOR_DEF").Value
            VisualizarGuardar
        Else
            oMensajes.NoValido sdbgTotProveedor.Columns("VALOR_DEF").caption & " " & m_sMensajeErrorNumerico
            Cancel = True
            Exit Sub
                              
        End If
    End If
End Sub

''' <summary>
''' Cambia el valor de la variable a true para que al cambiar de tab, si hay una unica variable
''' guarde tb los cambios
''' </summary>
''' <remarks>Llamada desde=Evento que salta al haber cambios en la grid; Tiempo m�ximo=0</remarks>
Private Sub sdbgTotProveedor_Change()
m_bCambioTotProveedores = True
End Sub

''' <summary>
''' Si ha habido cambios llamar al metodo de update
''' </summary>
''' <remarks>Llamada desde=Evento que salta al perder el foco; Tiempo m�ximo=0</remarks>
Private Sub sdbgTotProveedor_LostFocus()
    If m_bCambioTotProveedores Then
        sdbgTotProveedor.Update
    End If
End Sub


''' <summary>
''' Inicializa la columna valor por defecto como editable y estilo a blanco.
''' </summary>
''' <remarks>Llamada desde=Evento que salta al cargarse la grid; Tiempo m�ximo=0</remarks>
Private Sub sdbgTotProveedor_RowLoaded(ByVal Bookmark As Variant)
    sdbgTotProveedor.Columns("COD").Locked = True
    sdbgTotProveedor.Columns("DEN").Locked = True
    sdbgTotProveedor.Columns("DEN_IDIOMAS").Locked = True
    sdbgTotProveedor.Columns("VALOR_DEF").Locked = False
    
    sdbgTotProveedor.Columns("VALOR_DEF").StyleSet = "VALOR_DEF"
End Sub

Private Sub ssTabVar_BeforeClick(Cancel As Integer)

If ssTabVar.selectedItem.key <> "TOT_PROV" Then
    m_lIdCal1Ant = Mid(ssTabVar.selectedItem.key, 2)
    m_ilIDTabAnt = ssTabVar.selectedItem.Index
End If
    
m_bCambioTabs = True
End Sub

Private Sub ssTabVar_Click()
Dim lIdCal1 As Long
Dim oVarCal1 As CVariableCalidad
    If m_bError Then Exit Sub
    If Not m_bCambioTabs Then Exit Sub
    
    If Me.sdbgSubVariables.DataChanged Then
        m_bGuardarTabAnt = True
        m_bError = False
        sdbgSubVariables.Update
        If m_bError Then
            m_bCambioTabs = False
            ssTabVar.Tabs(m_ilIDTabAnt).Selected = True
            m_bCambioTabs = True
            m_bError = False
            Exit Sub
        End If
        m_bGuardarTabAnt = False
    End If
    
    If Me.sdbgTotProveedor.DataChanged Then
        sdbgTotProveedor.Update
    End If

    If ssTabVar.selectedItem.key = "TOT_PROV" Then
        'Es el total por proveedor
        picDatos.Visible = False
        picProveedor.Visible = True
        cmdEliminar.Enabled = False
        cmdModificarVar.Enabled = False
        cmdBajaVar.Enabled = False
        CargarTotalProveedores
        
    Else
       'Es una variable de seguridad de nivel 1:
        picDatos.Visible = True
        picProveedor.Visible = False
        cmdEliminar.Enabled = True
        cmdModificarVar.Enabled = True
        cmdBajaVar.Enabled = True
        lIdCal1 = Mid(ssTabVar.selectedItem.key, 2)
        Set oVarCal1 = g_oVarsCalidad1Mod.Item("1" & CStr(lIdCal1))
        If oVarCal1.BajaLog = True Then
            cmdBajaVar.caption = m_sDeshacerBaja
            cmdBajaVar.Tag = "BAJA"
            cmdEliminar.Enabled = False
            picBotonesSubvar.Visible = False
        Else
            cmdBajaVar.caption = m_sBaja
            cmdBajaVar.Tag = "NOBAJA"
            cmdEliminar.Enabled = True
            picBotonesSubvar.Visible = True
        End If
        CargarVariablesNivel2 lIdCal1
    End If
    m_bCambioTabs = False
    
End Sub


Private Sub CargarTotalProveedores()
''' <summary>
''' Cargar la grid de la pesta�a de total de proveedores con las variables de calidad de nivel 1 que no estan dados de baja
''' </summary>
''' <returns>Nothing</returns>
''' <remarks>Llamada desde=Propio formulario; Tiempo m�ximo=0,1seg</remarks>
    Dim oVarCal As CVariableCalidad
    Dim oIdioma As CIdioma
    Dim sIdioma As String
    Dim sTodosIdiomas As String
    
    m_iUltimaFila = -1
    sdbgTotProveedor.RemoveAll
    sdbgTotProveedor.Columns("VALOR_DEF").Locked = False
    For Each oVarCal In g_oVarsCalidad1Mod
        If Not oVarCal.BajaLog Then
            sIdioma = oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            sTodosIdiomas = sIdioma
            For Each oIdioma In m_oIdiomas
                If Not oVarCal.Denominaciones.Item(oIdioma.Cod) Is Nothing Then
                    If oVarCal.Denominaciones.Item(oIdioma.Cod).Cod <> gParametrosInstalacion.gIdioma And oVarCal.Denominaciones.Item(oIdioma.Cod).Den <> "" Then
                        sTodosIdiomas = sTodosIdiomas & " / " & oVarCal.Denominaciones.Item(oIdioma.Cod).Den
                    End If
                End If
            Next
            sdbgTotProveedor.AddItem oVarCal.Id & Chr(m_lSeparador) & oVarCal.Cod & Chr(m_lSeparador) & sIdioma & Chr(m_lSeparador) & sTodosIdiomas & Chr(m_lSeparador) & oVarCal.ValorDefecto
        End If
    Next
    sdbgTotProveedor.Columns("DEN").Visible = Not (Me.chkMultiidioma.Value = vbChecked)
    sdbgTotProveedor.Columns("DEN_IDIOMAS").Visible = (chkMultiidioma.Value = vbChecked)
        
    m_bRespetarColor = True
    txtFormulaProv.Text = NullToStr(g_oVarsCalidad1Mod.Formula)
    m_bRespetarColor = False
    If Not ValidarFormula(0, True, NullToStr(g_oVarsCalidad1Mod.Formula), 1) Then
        txtFormulaProv.Forecolor = vbRed
    Else
        txtFormulaProv.Forecolor = vbBlack
    End If
    Set oVarCal = Nothing
    Set oIdioma = Nothing
End Sub

Private Sub txtFormula_Change()
If m_bRespetarColor Then Exit Sub
txtFormula.Forecolor = vbRed
VisualizarGuardar
End Sub

Private Sub txtFormula_Click()
    'si hay cambios los guarda
    If sdbgSubVariables.DataChanged = True Then
        m_bError = False
        sdbgSubVariables.Update
        If m_bError = True Then
            If Me.Visible Then sdbgSubVariables.SetFocus
        End If
    End If

End Sub

Private Sub txtFormula_Validate(Cancel As Boolean)
    
'Se comprueban al guardar todas
g_oVarsCalidad1Mod.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))).Formula = StrToNull(txtFormula.Text)

If ComprobarModificadoVar1(g_oVarsCalidad1Mod.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2))).Id) Then
    VisualizarGuardar
End If

End Sub

Private Sub txtFormulaProv_Change()
If m_bRespetarColor Then Exit Sub
txtFormulaProv.Forecolor = vbRed
VisualizarGuardar
End Sub

Private Sub txtFormulaProv_Validate(Cancel As Boolean)

If Me.sdbcPYME.Visible = True Then
    If Me.sdbcPYME.Value = "" Then Exit Sub
End If

g_oVarsCalidad1Mod.Formula = StrToNull(txtFormulaProv.Text)
If NullToStr(g_oVarsCalidad1.Formula) <> NullToStr(g_oVarsCalidad1Mod.Formula) Then
    VisualizarGuardar
End If

End Sub

'MPG (01/04/2009)
'************************************************************
'*** Descripci�n:   Carga todas las variable de calidad en la coleccion. Carga tambien la formula del proveedor (Variable Calidad 0)
'                   Se realizan 2 lecturas para obtener una copiar para comprobar si ha habido cambios
'                   Carga los idiomas de la Variable de calidad de nivel 0 para cuando no exista introducir las denominaciones en la tabla de idiomas
'*** Par�metros de entrada: idPyme = Id de la PYME si se esta trabajando en modo PYME
'                           bBajaLog= true->Muestra tb las variables de calidad que estan dados de baja
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 1,40seg
'************************************************************

Private Sub CargarVariablesCalidad(Optional ByVal idPyme As Long = 0, Optional ByVal bBajaLog As Boolean = False)
    Dim ADORs As ADODB.Recordset
    Dim oIdiomasProv As CMultiidiomas
    Set oIdiomasProv = oFSGSRaiz.Generar_CMultiidiomas
    Dim i As Byte
    Dim oIdioma As CIdioma
    Dim stextoIdioma As String
    Set ADORs = oGestorIdiomas.DevolverTextosTotalProveedores(FRM_VAR_CALIDAD)
    If Not ADORs.EOF Then
        For Each oIdioma In m_oIdiomas
            stextoIdioma = "TEXT_" & oIdioma.Cod
            oIdiomasProv.Add oIdioma.Cod, NullToStr(ADORs.Fields(stextoIdioma).Value)
        Next
        m_sTotProveedor = oIdiomasProv.Item(gParametrosInstalacion.gIdioma).Den
        m_sTotProveedorIdiomas = m_sTotProveedor
        For i = 1 To oIdiomasProv.Count
            If oIdiomasProv.Item(i).Cod <> gParametrosInstalacion.gIdioma And oIdiomasProv.Item(i).Den <> "" Then
                m_sTotProveedorIdiomas = m_sTotProveedorIdiomas & " / " & oIdiomasProv.Item(i).Den
            End If
        Next
    End If
    
    Dim oVarCal As CVariableCalidad
    Dim iNumVar As Integer
    Dim sCadena As String
    
    iNumVar = 0
    
    ssTabVar.Tabs.clear
    sdbgSubVariables.RemoveAll
    fraSubVariables.Visible = False
    sdbgSubVariables.Visible = False
    m_bRespetarColor = True
    txtFormula.Text = ""
    m_bRespetarColor = False
    'Cargo dos veces lo mismo porque en una voy a tener el original y en la otra las modificaciones
    'para poder comparar en todo momento con lo que hay en BD sin tener que leer de BD.
    Set g_oVarsCalidad1 = oFSGSRaiz.Generar_CVariablesCalidad
    g_oVarsCalidad1.CargarFormulaProveedor idPyme, oIdiomasProv
    g_oVarsCalidad1.CargarVariables , , , , , , , idPyme, True
    
    Set g_oVarsCalidad1Mod = oFSGSRaiz.Generar_CVariablesCalidad
    
    g_oVarsCalidad1Mod.Formula = g_oVarsCalidad1.Formula
    g_oVarsCalidad1Mod.FormulaID = g_oVarsCalidad1.FormulaID
    g_oVarsCalidad1Mod.CargarVariables , , , , , , , idPyme, True
    sCadena = ""
    For Each oVarCal In g_oVarsCalidad1
        If ((oVarCal.BajaLog = True And (chkVerBaja.Value = vbChecked)) Or (Not oVarCal.BajaLog)) Then
            iNumVar = iNumVar + 1
            If Me.chkMultiidioma.Value = vbChecked Then
                sCadena = oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                For Each oIdioma In m_oIdiomas
                    If oVarCal.Denominaciones.Item(oIdioma.Cod).Cod <> gParametrosInstalacion.gIdioma And oVarCal.Denominaciones.Item(oIdioma.Cod).Den <> "" Then
                        sCadena = sCadena & " / " & oVarCal.Denominaciones.Item(oIdioma.Cod).Den
                    End If
                Next
                
            Else
                sCadena = oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            End If
            If oVarCal.BajaLog Then
                sCadena = "(!)" & sCadena ' Indicador que muestra que esta dado de baja logica
            End If
            ssTabVar.Tabs.Add iNumVar, "V" & oVarCal.Id, sCadena
        End If
    Next
    If Me.chkMultiidioma.Value = vbChecked Then
        ssTabVar.Tabs.Add iNumVar + 1, "TOT_PROV", m_sTotProveedorIdiomas
    Else
        ssTabVar.Tabs.Add iNumVar + 1, "TOT_PROV", oIdiomasProv.Item(gParametrosInstalacion.gIdioma).Den
    End If
    If ssTabVar.selectedItem.key = "TOT_PROV" Then
        picDatos.Visible = False
        picProveedor.Visible = True
        CargarTotalProveedores
    Else
        picDatos.Visible = True
        picProveedor.Visible = False
        CargarVariablesNivel2 Mid(ssTabVar.selectedItem.key, 2)
    End If

    Set oIdiomasProv = Nothing
    ADORs.Close
    Set ADORs = Nothing
End Sub

'<summary>Cargar las Variables de calidad de Nivel 2. Comprueba si Muestra o no las variables que estan dados de baja</summary>
'<param name="lIDCal1">Id del tab en el que esta</param>
'<remarks>Llamada desde: propio formulario</remarks>
'<revision>LTG 26/06/2013</revision>

Private Sub CargarVariablesNivel2(ByVal lIdCal1 As Long)
    Dim oVarCal As CVariableCalidad
    Dim sCadena As String
    Dim sw As Boolean
    Dim oIdioma As CIdioma

    Screen.MousePointer = vbHourglass
    sCadena = ""
    sdbgSubVariables.RemoveAll
    fraSubVariables.Visible = True
    sdbgSubVariables.Visible = True
    If Not g_oVarsCalidad1Mod.Item("1" & CStr(lIdCal1)).VariblesCal Is Nothing Then
        For Each oVarCal In g_oVarsCalidad1Mod.Item("1" & CStr(lIdCal1)).VariblesCal
            If (oVarCal.BajaLog And chkVerBaja.Value = vbChecked) Or (Not oVarCal.BajaLog) Then
                sCadena = sCadena & Chr(m_lSeparador) & oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                
                For Each oIdioma In m_oIdiomas
                    If oVarCal.Denominaciones.Item(oIdioma.Cod).Cod <> gParametrosInstalacion.gIdioma Then
                        sCadena = sCadena & Chr(m_lSeparador) & oVarCal.Denominaciones.Item(oIdioma.Cod).Den
                    End If
                    If m_oIdiomas.Item(oIdioma.Cod).Cod <> gParametrosInstalacion.gIdioma Then
                        sdbgSubVariables.Columns(m_oIdiomas.Item(oIdioma.Cod).Cod).Visible = Me.chkMultiidioma.Value
                    Else
                        sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).Visible = True
                    End If
                Next
                                        
                sdbgSubVariables.AddItem oVarCal.Id & Chr(m_lSeparador) & oVarCal.Cod & Chr(m_lSeparador) & Chr(m_lSeparador) & m_sTipo(oVarCal.Tipo) & _
                    Chr(m_lSeparador) & IIf(oVarCal.Tipo = 0, "", "...") & Chr(m_lSeparador) & _
                    IIf(oVarCal.Subtipo = 0, "", m_sSubTipo(NullToDbl0(oVarCal.Subtipo))) & Chr(m_lSeparador) & NullToStr(oVarCal.OrigenCod) & _
                    Chr(m_lSeparador) & NullToStr(oVarCal.ValorDefecto) & Chr(m_lSeparador) & _
                    IIf((oVarCal.Tipo = 0 And oVarCal.Subtipo = CalCertificado), NullToStr(oVarCal.ValorCertSinCumpl), "") & Chr(m_lSeparador) & _
                    IIf((oVarCal.Subtipo = 0 Or oVarCal.Subtipo = CalManual), "", "...") & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & oVarCal.Tipo & _
                    Chr(m_lSeparador) & oVarCal.Subtipo & Chr(m_lSeparador) & NullToStr(oVarCal.Origen) & Chr(m_lSeparador) & _
                    oVarCal.FormularioID & Chr(m_lSeparador) & IIf(oVarCal.BajaLog, 1, 0) & sCadena
                sCadena = ""
                If Not sw Then
                    If oVarCal.BajaLog = True Then
                        cmdBajaSubvar.Picture = ImageList1.ListImages("NOBAJA").Picture
                        cmdBajaSubvar.Tag = "BAJA"
                        cmdElimSubvar.Enabled = False
                    Else
                        cmdBajaSubvar.Picture = ImageList1.ListImages("BAJA").Picture
                        cmdBajaSubvar.Tag = "NOBAJA"
                        cmdElimSubvar.Enabled = True
                    End If
                    sw = True
                End If
            End If
            
        Next
    End If
    sdbgSubVariables.Visible = True
    m_bRespetarColor = True
    txtFormula.Text = NullToStr(g_oVarsCalidad1Mod.Item("1" & CStr(lIdCal1)).Formula)
        CargarVariablesHermanas
        optOpcionConf(g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).Opcion_Conf) = True
        If g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).Opcion_Conf = VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
            sdbcVarPond.Columns("ID").Value = g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).IdVar_Pond
        End If
    
    m_bRespetarColor = False
    
    If Not g_oVarsCalidad1.Item("1" & CStr(lIdCal1)) Is Nothing Then
        If NullToStr(g_oVarsCalidad1Mod.Item("1" & CStr(lIdCal1)).Formula) <> NullToStr(g_oVarsCalidad1.Item("1" & CStr(lIdCal1)).Formula) Then
            txtFormula.Forecolor = vbRed
        Else
            txtFormula.Forecolor = vbBlack
        End If
    End If
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarVariablesNivel3(ByVal lIdCal1 As Long, ByVal lIdCal2 As Long)

    Screen.MousePointer = vbHourglass
        
    If Not g_oVarsCalidad1.Item("1" & CStr(lIdCal1)) Is Nothing Then
        Set frmVARCalCompuestaNivel3.g_oVarCal2 = g_oVarsCalidad1.Item("1" & CStr(lIdCal1)).VariblesCal.Item("2" & CStr(lIdCal2))
    End If
    Set frmVARCalCompuestaNivel3.g_oVarCal2Mod = g_oVarsCalidad1Mod.Item("1" & CStr(lIdCal1)).VariblesCal.Item("2" & CStr(lIdCal2))
    frmVARCalCompuestaNivel3.g_bMultiIdioma = (chkMultiidioma.Value = vbChecked)
    frmVARCalCompuestaNivel3.g_bVerBaja = (chkVerBaja.Value = vbChecked)
    
    Set frmVARCalCompuestaNivel3.g_oIdiomas = m_oIdiomas
    frmVARCalCompuestaNivel3.Show 1
            
    Screen.MousePointer = vbNormal
    
End Sub

Private Function ComprobarModificadoVar1(ByVal lID As Long) As Boolean
'mpg30/03/2009
'************************************************************************************
'*** Descripci�n: Comprueba si ha variado el codigo y formula                   *****
'*** Par�metros de entrada: (Para acceder a la collecion de la variable de calidad de nivel 1
'                           lID1 Variable Calidad Nivel 1                       *****
'*** Par�metros de salida: True si ha habido cambios /false si no               *****
'***                     devuelve                                               *****
'*** Llamada desde: propio formulario                                           *****
'*** Tiempo m�ximo: 0,03                                                        *****
'************************************************************************************
Dim oVarCal1 As CVariableCalidad
Dim oVarCal1Mod As CVariableCalidad

Set oVarCal1 = g_oVarsCalidad1.Item("1" & CStr(lID))
Set oVarCal1Mod = g_oVarsCalidad1Mod.Item("1" & CStr(lID))

If oVarCal1 Is Nothing Then
    oVarCal1Mod.modificado = True
Else
    If (oVarCal1.Cod <> oVarCal1Mod.Cod) Or (NullToStr(oVarCal1.Formula) <> NullToStr(oVarCal1Mod.Formula) Or _
        NullToStr(oVarCal1.ValorDefecto) <> NullToStr(oVarCal1Mod.ValorDefecto)) Or _
            (oVarCal1Mod.Opcion_Conf <> oVarCal1.Opcion_Conf) Or (oVarCal1Mod.IdVar_Pond <> oVarCal1.IdVar_Pond) Then
        oVarCal1Mod.modificado = True
        'Las denominaciones se controla si se ha modificado en la pantalla frmVarCalAnya.frm
    End If
End If

ComprobarModificadoVar1 = oVarCal1Mod.modificado

Set oVarCal1Mod = Nothing
Set oVarCal1 = Nothing

End Function

'<summary>Comprueba si ha variado el codigo,origen,tipo,subtipo,formula,valor por defecto de la Variable de calidad nivel 2
'                   Recorre las denominaciones por si ha habido variacion en ellas</summary>
'<param name="lID1">Variable Calidad Nivel 1</param>
'<param name="lID2">Variable Calidad Nivel 2</param>
'<returns>True si ha habido cambios /false si no devuelve</returns>
'<remarks>Llamada desde: propio formulario</remarks>
'<revision>LTG 26/06/2013</revision>

Public Function ComprobarModificadoVar2(ByVal lID1 As Long, ByVal lID2 As Long) As Boolean
    Dim oVarCal2 As CVariableCalidad
    Dim oVarCal2Mod As CVariableCalidad
    Dim oVarCal3 As CVariableCalidad
    Dim oVarCal4 As CVariableCalidad
    Dim oVarCal5 As CVariableCalidad
    Dim bEncontrado As Boolean
    Dim i As Byte
    Dim rs As Recordset
    Dim bPuntHist As Boolean
    
    If Not g_oVarsCalidad1.Item("1" & CStr(lID1)) Is Nothing Then
        Set oVarCal2 = g_oVarsCalidad1.Item("1" & CStr(lID1)).VariblesCal.Item("2" & CStr(lID2))
    End If
    Set oVarCal2Mod = g_oVarsCalidad1Mod.Item("1" & CStr(lID1)).VariblesCal.Item("2" & CStr(lID2))
    
    If oVarCal2 Is Nothing Then
        oVarCal2Mod.modificado = True
    Else
        If (oVarCal2Mod.Cod <> oVarCal2.Cod) Or (NullToDbl0(oVarCal2Mod.Origen) <> NullToDbl0(oVarCal2.Origen)) Or _
           (NullToDbl0(oVarCal2Mod.Subtipo) <> NullToDbl0(oVarCal2.Subtipo)) Or (oVarCal2Mod.Tipo <> oVarCal2.Tipo) Or _
           (NullToStr(oVarCal2Mod.Formula) <> NullToStr(oVarCal2.Formula)) Or (NullToDbl0(oVarCal2Mod.ValorDefecto) <> NullToDbl0(oVarCal2.ValorDefecto)) Or _
           (oVarCal2Mod.BajaLog <> oVarCal2.BajaLog) Or (NullToDbl0(oVarCal2Mod.ValorCertSinCumpl) <> NullToDbl0(oVarCal2.ValorCertSinCumpl)) Or _
           (oVarCal2Mod.Opcion_Conf <> oVarCal2.Opcion_Conf) Or (oVarCal2Mod.IdVar_Pond <> oVarCal2.IdVar_Pond) Then
            
            oVarCal2Mod.modificado = True
            If oVarCal2.Tipo = 1 And oVarCal2Mod.Tipo = 0 Then
                'Si hay puntuaciones en el historico poner las hijas de baja logica
                Set rs = oVarCal2Mod.ExistenPuntuaciones
                If Not rs.EOF Then
                    rs.MoveNext
                    bPuntHist = (rs("CONT").Value > 0)
                End If
                rs.Close
                Set rs = Nothing
                
                If bPuntHist Then
                    For Each oVarCal3 In oVarCal2Mod.VariblesCal
                        If oVarCal3.Tipo = 1 Then
                            For Each oVarCal4 In oVarCal3.VariblesCal
                                If oVarCal4.Tipo = 1 Then
                                    For Each oVarCal5 In oVarCal4.VariblesCal
                                        If Not oVarCal5.BajaLog Then
                                            oVarCal5.BajaLog = True
                                            oVarCal5.modificado = True
                                        End If
                                    Next
                                
                                End If
                                If Not oVarCal4.BajaLog Then
                                    oVarCal4.BajaLog = True
                                    oVarCal4.modificado = True
                                End If
                            Next
                        End If
                            
                        If Not oVarCal3.BajaLog Then
                            oVarCal3.BajaLog = True
                            oVarCal3.modificado = True
                        End If
                    Next
                Else
                    Set oVarCal2Mod.VariblesCal = Nothing
                    oVarCal2Mod.Formula = Null
                End If
            End If
        Else
            bEncontrado = False
            For i = 1 To oVarCal2Mod.Denominaciones.Count
                If oVarCal2Mod.Denominaciones.Item(i).Den <> oVarCal2.Denominaciones.Item(i).Den Then
                    bEncontrado = True
                    Exit For
                End If
            Next
            If bEncontrado Then
                oVarCal2Mod.modificado = True
            End If
        End If
    End If
    
    ComprobarModificadoVar2 = oVarCal2Mod.modificado
    
    Set oVarCal2Mod = Nothing
    Set oVarCal2 = Nothing
End Function

Private Function ValidarFormula(ByVal lID1 As Long, ByVal bProve As Boolean, ByVal sFormula As String, ByVal bNoMens As Boolean) As Boolean
''' <summary>
''' Comprueba si la formula introducida es correcta o no (tiene correcto el identificador,operador matematico correcto...
''' </summary>
''' <param name="lID1">ID de la variable</param>
''' <param name="bProve">True--> Comprobar las variables de nivel 1 // false --> Comprueba la formula en las variables hijas de la Variable de calidad de nivel 1 indicado por lID1</param>
''' <param name="sFormula">formula introducida</param>
''' <param name="bNoMens">True--> No muestra mensaje de error</param>
''' <returns>True/false si la formula introducida es correcta o no</returns>
''' <remarks>Llamada desde=Form_unload; Tiempo m�ximo=0,1seg</remarks>

'mpg (08/04/2009)

Dim sVariables() As String
Dim sVariablesBaja() As String
Dim lErrCode As Integer
Dim lIndex As Integer
Dim iEq As USPExpression
Dim sCaracter As String
Dim i, cont As Integer
Dim oVarCal As CVariableCalidad
Dim bMensajeMostrado, bVariablesBaja As Boolean
    
If sFormula <> "" Then
    Set iEq = New USPExpression
    
    If bProve Then
        If g_oVarsCalidad1Mod.Count > 0 Then
            ReDim sVariables(g_oVarsCalidad1Mod.Count)
            ReDim sVariablesBaja(g_oVarsCalidad1Mod.Count - 1)
            cont = 0
            i = 0
            If Not g_oVarsCalidad1Mod Is Nothing Then
                For Each oVarCal In g_oVarsCalidad1Mod
                    If oVarCal.BajaLog = False Then
                        sVariables(cont) = oVarCal.Cod
                        cont = cont + 1
                    Else
                        bVariablesBaja = True
                    End If
                    sVariablesBaja(i) = oVarCal.Cod
                    i = i + 1
                Next
            End If
        Else
            ReDim sVariables(0)
        End If
    Else
        Set oVarCal = g_oVarsCalidad1Mod.Item("1" & CStr(lID1))
        If Not oVarCal Is Nothing Then
            If oVarCal.VariblesCal.Count > 0 Then
                ReDim sVariables(oVarCal.VariblesCal.Count)
                ReDim sVariablesBaja(oVarCal.VariblesCal.Count)
                i = 0
                cont = 0
                Set oVarCal = Nothing
                For Each oVarCal In g_oVarsCalidad1Mod.Item("1" & CStr(lID1)).VariblesCal
                    If oVarCal.BajaLog = False Then
                        sVariables(cont) = oVarCal.Cod
                        cont = cont + 1
                    Else
                        bVariablesBaja = True
                    End If
                    sVariablesBaja(i) = oVarCal.Cod
                    i = i + 1
                Next
                
            Else
                ReDim sVariables(0)
            End If
        Else
            ReDim sVariables(0)
        End If
        
    End If
    lIndex = iEq.Parse(sFormula, sVariables, lErrCode)

    If lErrCode <> USPEX_NO_ERROR Then
        ' Parsing error handler
        If bNoMens = False Then
            Select Case lErrCode
                Case USPEX_DIVISION_BY_ZERO
                    oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(1))
                Case USPEX_EMPTY_EXPRESSION
                    oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(2))
                Case USPEX_MISSING_OPERATOR
                    oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(3))
                Case USPEX_SYNTAX_ERROR
                    oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(4))
                Case USPEX_UNKNOWN_FUNCTION
                    oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(5))
                Case USPEX_UNKNOWN_OPERATOR
                    oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(6))
                Case USPEX_WRONG_PARAMS_NUMBER
                    oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(7))
                Case USPEX_UNKNOWN_IDENTIFIER
                    oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(8))
                Case USPEX_UNKNOWN_VAR
                    oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(9))
                Case USPEX_VARIABLES_NOTUNIQUE
                    oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(10))
                    bMensajeMostrado = True
                Case Else
                    sCaracter = Mid(sFormula, lIndex)
                    oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(11) & vbCrLf & sCaracter)
            End Select
        End If
        Set iEq = Nothing
        ValidarFormula = False
        Exit Function
    End If
    
    If bVariablesBaja Then
        If Not bMensajeMostrado Then
            lIndex = iEq.Parse(sFormula, sVariablesBaja, lErrCode)
            If lErrCode <> USPEX_NO_ERROR Then
                Select Case lErrCode
                    Case USPEX_VARIABLES_NOTUNIQUE
                        oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(10) & Chr(10) & m_sIdiErrorFormula(12))
                End Select

                Set iEq = Nothing
                ValidarFormula = False
                Exit Function
            End If
        End If
    End If
    
    Set iEq = Nothing

End If

If bProve Then
    g_oVarsCalidad1Mod.Formula = StrToNull(sFormula)
    If NullToStr(g_oVarsCalidad1.Formula) <> NullToStr(g_oVarsCalidad1Mod.Formula) Then
        VisualizarGuardar
    End If
Else
    g_oVarsCalidad1Mod.Item("1" & CStr(lID1)).Formula = StrToNull(sFormula)
    
    If ComprobarModificadoVar1(g_oVarsCalidad1Mod.Item("1" & CStr(lID1)).Id) Then
        VisualizarGuardar
    End If
End If
ValidarFormula = True

End Function

Public Function ComprobarModificadoVar3(ByVal lID1 As Long, ByVal lID2 As Long, ByVal lID3 As Long) As Boolean
'mpg30/03/2009
'************************************************************************************
'*** Descripci�n: Comprueba si ha variado el codigo, origen, tipo, subtipo      *****
'***              , formula, valor por defecto de la Variable de calidad nivel 3*****
'                   Recorre las denominaciones por si ha habido variacion en ellas
'*** Par�metros de entrada: lID1 Variable Calidad Nivel 1                       *****
'                           lID2 Variable Calidad Nivel 2                       *****
'                           lID3 Variable Calidad Nivel 3                       *****
'*** Par�metros de salida: True si ha habido cambios /false si no               *****
'***                     devuelve                                               *****
'*** Llamada desde: propio formulario                                           *****
'*** Tiempo m�ximo: 0,03                                                        *****
'************************************************************************************

Dim oVarCal3 As CVariableCalidad
Dim oVarCal3Mod As CVariableCalidad
Dim i As Byte
Dim bEncontrado As Boolean

If Not g_oVarsCalidad1.Item("1" & CStr(lID1)) Is Nothing Then
    If Not g_oVarsCalidad1.Item("1" & CStr(lID1)).VariblesCal Is Nothing Then
        If Not g_oVarsCalidad1.Item("1" & CStr(lID1)).VariblesCal.Item("2" & CStr(lID2)) Is Nothing Then
            Set oVarCal3 = g_oVarsCalidad1.Item("1" & CStr(lID1)).VariblesCal.Item("2" & CStr(lID2)).VariblesCal.Item("3" & CStr(lID3))
        End If
    End If
End If

Set oVarCal3Mod = g_oVarsCalidad1Mod.Item("1" & CStr(lID1)).VariblesCal.Item("2" & CStr(lID2)).VariblesCal.Item("3" & CStr(lID3))

If oVarCal3 Is Nothing Then
    oVarCal3Mod.modificado = True
Else
    If (oVarCal3Mod.Cod <> oVarCal3.Cod) Or (NullToDbl0(oVarCal3Mod.Origen) <> NullToDbl0(oVarCal3.Origen)) Or (NullToDbl0(oVarCal3Mod.Subtipo) <> NullToDbl0(oVarCal3.Subtipo)) Or _
    (oVarCal3Mod.Tipo <> oVarCal3.Tipo) Or (NullToStr(oVarCal3Mod.Formula) <> NullToStr(oVarCal3.Formula)) Or _
    (oVarCal3Mod.Opcion_Conf <> oVarCal3.Opcion_Conf) Or (oVarCal3Mod.IdVar_Pond <> oVarCal3.IdVar_Pond) Then
        oVarCal3Mod.modificado = True
        If oVarCal3.Tipo = 1 And oVarCal3Mod.Tipo = 0 Then
            oVarCal3Mod.Formula = Null
        End If
    Else
        bEncontrado = False
        For i = 1 To oVarCal3Mod.Denominaciones.Count
            If oVarCal3Mod.Denominaciones.Item(i).Den <> oVarCal3.Denominaciones.Item(i).Den Then
                bEncontrado = True
                Exit For
            End If
        Next
        If bEncontrado Then
            oVarCal3Mod.modificado = True
        End If
    End If
End If

ComprobarModificadoVar3 = oVarCal3Mod.modificado

Set oVarCal3Mod = Nothing
Set oVarCal3 = Nothing

End Function

Public Function ComprobarModificadoVar4(ByVal lID1 As Long, ByVal lID2 As Long, ByVal lID3 As Long, ByVal lID4 As Long) As Boolean
'mpg30/03/2009
'************************************************************************************
'*** Descripci�n: Comprueba si ha variado el codigo, origen, tipo, subtipo      *****
'***              , formula, valor por defecto de la Variable de calidad nivel 4*****
'                   Recorre las denominaciones por si ha habido variacion en ellas
'*** Par�metros de entrada: (Para acceder a la collecion de la variable de calidad de nivel 4
'                           lID1 Variable Calidad Nivel 1                       *****
'                           lID2 Variable Calidad Nivel 2                       *****
'                           lID3 Variable Calidad Nivel 3                       *****
'                           lID4 Variable Calidad Nivel 4                       *****
'*** Par�metros de salida: True si ha habido cambios /false si no               *****
'***                     devuelve                                               *****
'*** Llamada desde: propio formulario                                           *****
'*** Tiempo m�ximo: 0,03                                                        *****
'************************************************************************************

Dim oVarCal4 As CVariableCalidad
Dim oVarCal4Mod As CVariableCalidad
Dim bEncontrado As Boolean
Dim i As Byte

If Not g_oVarsCalidad1.Item("1" & CStr(lID1)) Is Nothing Then
    If Not g_oVarsCalidad1.Item("1" & CStr(lID1)).VariblesCal.Item("2" & CStr(lID2)) Is Nothing Then
        If Not g_oVarsCalidad1.Item("1" & CStr(lID1)).VariblesCal.Item("2" & CStr(lID2)).VariblesCal.Item("3" & CStr(lID3)) Is Nothing Then
            Set oVarCal4 = g_oVarsCalidad1.Item("1" & CStr(lID1)).VariblesCal.Item("2" & CStr(lID2)).VariblesCal.Item("3" & CStr(lID3)).VariblesCal.Item("4" & CStr(lID4))
        End If
    End If
End If
Set oVarCal4Mod = g_oVarsCalidad1Mod.Item("1" & CStr(lID1)).VariblesCal.Item("2" & CStr(lID2)).VariblesCal.Item("3" & CStr(lID3)).VariblesCal.Item("4" & CStr(lID4))

If oVarCal4 Is Nothing Then
    oVarCal4Mod.modificado = True
Else
    If (oVarCal4Mod.Cod <> oVarCal4.Cod) Or (NullToDbl0(oVarCal4Mod.Origen) <> NullToDbl0(oVarCal4.Origen)) Or (NullToDbl0(oVarCal4Mod.Subtipo) <> NullToDbl0(oVarCal4.Subtipo)) Or _
    (oVarCal4Mod.Tipo <> oVarCal4.Tipo) Or (NullToStr(oVarCal4Mod.Formula) <> NullToStr(oVarCal4.Formula)) Or _
    (oVarCal4Mod.Opcion_Conf <> oVarCal4.Opcion_Conf) Or (oVarCal4Mod.IdVar_Pond <> oVarCal4.IdVar_Pond) Then
        oVarCal4Mod.modificado = True
        If oVarCal4.Tipo = 1 And oVarCal4Mod.Tipo = 0 Then
            oVarCal4Mod.Formula = Null
        End If
    Else
        bEncontrado = False
        For i = 1 To oVarCal4Mod.Denominaciones.Count
            If oVarCal4Mod.Denominaciones.Item(i).Den <> oVarCal4.Denominaciones.Item(i).Den Then
                bEncontrado = True
                Exit For
            End If
        Next
        If bEncontrado Then
            oVarCal4Mod.modificado = True
        End If
    End If
End If

ComprobarModificadoVar4 = oVarCal4Mod.modificado

Set oVarCal4Mod = Nothing
Set oVarCal4 = Nothing

End Function

Private Sub sdbcPYME_CloseUp()
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Evento que salta al seleccionar una PYME de la combo.
'                   Selecciona las variables de calidad de la pyme seleccionada.//Desbloquea el contenido de la pantalla para poder realizar operaciones con la Variable
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,8seg
'************************************************************
    If sdbcPYME.Value = "" Then Exit Sub
    lblCambios.Visible = False
    CargarVariablesCalidad sdbcPYME.Columns("ID").Value
    bloquearDesbloquearPantalla (False)
    Arrange
End Sub

Private Sub sdbcPYME_DropDown()
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Carga la combo con todas las pymes que hay en la aplicacion
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,1seg
'************************************************************
Dim oPymes As cPymes
Dim oPyme As cPyme

    Screen.MousePointer = vbHourglass
    
    sdbcPYME.RemoveAll
    
    Set oPymes = oFSGSRaiz.Generar_CPymes
    oPymes.CargarTodasLasPymes
    
    For Each oPyme In oPymes
        sdbcPYME.AddItem oPyme.Id & Chr(m_lSeparador) & oPyme.Cod & Chr(m_lSeparador) & oPyme.Den
    Next
    
    Set oPymes = Nothing
    Set oPyme = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPYME_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcPYME.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPYME.Rows - 1
            bm = sdbcPYME.GetBookmark(i)
            If UCase(Text) = UCase(sdbcPYME.Columns("DEN").CellText(bm)) Then
                sdbcPYME.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcPYME_InitColumnProps()
    
    sdbcPYME.DataFieldList = "Column 0"
    sdbcPYME.DataFieldToDisplay = "Column 2"
    
End Sub

Private Sub sdbcPYME_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyBack Then
        sdbcPYME.Value = ""
        sdbgSubVariables.RemoveAll
        Me.ssTabVar.Tabs.clear
        Me.ssTabVar.Tabs.Add
        If Me.picProveedor.Visible Then
            Me.picProveedor.Visible = False
            Me.picDatos.Visible = True
        
        End If
        bloquearDesbloquearPantalla (True)
    End If
End Sub

Private Sub bloquearDesbloquearPantalla(ByVal bBloqueo As Boolean)
'Descripcion:=  Procedure que bloquea/desbloquea el contenido de la pantalla cuando se ha entrado en modo PYMEs y no tiene
'               nada que elegir
'Parametros:=   bBloqueo --> SI bloquea o No el control
Me.chkMultiidioma.Enabled = Not bBloqueo
Me.chkVerBaja.Enabled = Not bBloqueo
Me.picEdicion.Enabled = Not bBloqueo
Me.ssTabVar.Enabled = Not bBloqueo
Me.picDatos.Enabled = Not bBloqueo

End Sub

Private Sub chkMultiidioma_Click()
'************************************************************
'*** Descripci�n:   Muestra/Oculta los idiomas que hay en la aplicacion en la grid de Variables de calidad
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,1seg
'************************************************************

  Dim sCadena As String
  Dim i, iNumVar As Byte
  Dim oVarCal As CVariableCalidad
  Dim Indice As Byte
  
  
    Indice = ssTabVar.selectedItem.Index
    If chkMultiidioma.Value = vbChecked Then
        sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).caption = m_sIdiomaSPA
    Else
        sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).caption = m_sNombreDen
    End If
    
    ssTabVar.Tabs.clear
    
    sCadena = ""
    For Each oVarCal In g_oVarsCalidad1Mod
        If ((oVarCal.BajaLog = True And (chkVerBaja.Value = vbChecked)) Or (Not oVarCal.BajaLog)) Then
            iNumVar = iNumVar + 1
            If Me.chkMultiidioma.Value = vbChecked Then
                sCadena = oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                For i = 1 To m_oIdiomas.Count
                    If Not oVarCal.Denominaciones.Item(m_oIdiomas.Item(i).Cod) Is Nothing Then
                        If oVarCal.Denominaciones.Item(m_oIdiomas.Item(i).Cod).Cod <> gParametrosInstalacion.gIdioma And oVarCal.Denominaciones.Item(m_oIdiomas.Item(i).Cod).Den <> "" Then
                            sCadena = sCadena & " / " & oVarCal.Denominaciones.Item(m_oIdiomas.Item(i).Cod).Den
                        End If
                    End If
                Next
            Else
                sCadena = oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            End If
            If oVarCal.BajaLog Then
                sCadena = "(!)" & sCadena
            End If
            ssTabVar.Tabs.Add iNumVar, "V" & oVarCal.Id, sCadena
        End If
    Next
    
    If Me.chkMultiidioma.Value = vbChecked Then
        ssTabVar.Tabs.Add iNumVar + 1, "TOT_PROV", m_sTotProveedorIdiomas
    Else
        ssTabVar.Tabs.Add iNumVar + 1, "TOT_PROV", m_sTotProveedor
    End If

    If Indice <> iNumVar + 1 Then
        For i = 1 To m_oIdiomas.Count
            If m_oIdiomas.Item(i).Cod <> gParametrosInstalacion.gIdioma Then
                sdbgSubVariables.Columns(m_oIdiomas.Item(i).Cod).Visible = Me.chkMultiidioma.Value
                
            Else
                sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).Visible = True
            End If
        Next
'    Else
'        sdbgTotProveedor.Columns("DEN").Visible = Not (Me.chkMultiidioma.Value)
'        sdbgTotProveedor.Columns("DEN_IDIOMAS").Visible = Me.chkMultiidioma.Value
        
    End If
    If Not m_bSaltar Then
        ssTabVar.Tabs(Indice).Selected = True
    End If
    CargarVariablesHermanas
    Set oVarCal = Nothing
            
End Sub

Private Sub cmdBajaVar_Click()
''' <summary>
''' Proceso que realiza la baja logica o deshace la baja logica de de una variable de calidad de nivel 1
''' </summary>
''' <returns>Ninguno</returns>
''' <remarks>Llamada desde=Evento que salta cuando se pincha en el boton de Baja/deshacer baja logica; Tiempo m�ximo=1seg.</remarks>
Dim oVarCal1 As CVariableCalidad
Dim oVarCal2 As CVariableCalidad
Dim oVarCal3 As CVariableCalidad
Dim oVarCal4 As CVariableCalidad
Dim oVarCal5 As CVariableCalidad
Dim bHijasSinBaja As Boolean
Dim irespuesta As Integer
Dim vbm As Variant
Dim i As Integer

    'si hay cambios los guarda
    If sdbgSubVariables.DataChanged = True Then
        sdbgSubVariables.Update
        If m_bError = True Then Exit Sub
    End If
    
    
    Set oVarCal1 = g_oVarsCalidad1Mod.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2)))
    
    If oVarCal1 Is Nothing Then Exit Sub
    
    If cmdBajaVar.Tag = "NOBAJA" Then

        If Not oVarCal1.VariblesCal Is Nothing Then
            For Each oVarCal2 In oVarCal1.VariblesCal
                If oVarCal2.BajaLog = False Then
                    bHijasSinBaja = True
                    Exit For
                End If
    
            Next
        End If
            
        irespuesta = oMensajes.PreguntaBajaLogVariableCalidad(oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den, bHijasSinBaja)
        If irespuesta = vbNo Then Exit Sub
    
        Screen.MousePointer = vbHourglass
        If chkVerBaja.Value = vbChecked Then
            ssTabVar.selectedItem.caption = "(!)" & ssTabVar.selectedItem.caption
            cmdBajaVar.caption = m_sBaja
            cmdBajaVar.Tag = "BAJA"
        Else
            ssTabVar.Tabs.Remove (ssTabVar.selectedItem.Index)
        End If
       
        oVarCal1.BajaLog = True
        oVarCal1.modificado = True
        If bHijasSinBaja Then
            For Each oVarCal2 In oVarCal1.VariblesCal
                oVarCal2.BajaLog = True
                oVarCal2.modificado = True
                For i = 0 To sdbgSubVariables.Rows
                    vbm = sdbgSubVariables.AddItemBookmark(i)
                    If sdbgSubVariables.Columns("COD").CellValue(vbm) = oVarCal2.Cod Then
                        sdbgSubVariables.Row = i
                        If chkVerBaja.Value = vbChecked Then
                            sdbgSubVariables.Columns("BAJALOG").Value = 1
                        Else
                            sdbgSubVariables.DeleteSelected
                        End If
                        Exit For
                    End If
                Next i
                If Not oVarCal2.VariblesCal Is Nothing Then
                    For Each oVarCal3 In oVarCal2.VariblesCal
                        oVarCal3.BajaLog = True
                        oVarCal3.modificado = True
                        If Not oVarCal3.VariblesCal Is Nothing Then
                            For Each oVarCal4 In oVarCal3.VariblesCal
                                oVarCal4.BajaLog = True
                                oVarCal4.modificado = True
                                If Not oVarCal4.VariblesCal Is Nothing Then
                                    For Each oVarCal5 In oVarCal4.VariblesCal
                                        oVarCal5.BajaLog = True
                                        oVarCal5.modificado = True
                                    Next
                                End If
                            Next
                        End If
                    Next
                End If
            Next
        End If
    Else
        'Deshacer la baja logica de nivel 1
        oVarCal1.BajaLog = False
        oVarCal1.modificado = True
        
        ssTabVar.selectedItem.caption = Replace(ssTabVar.selectedItem.caption, "(!)", "")
        cmdBajaVar.caption = m_sBaja
        cmdBajaVar.Tag = "NOBAJA"
    End If
    
    VisualizarGuardar
        
    ssTabVar.Tabs(ssTabVar.selectedItem.Index).Selected = True

    If ssTabVar.selectedItem.key = "TOT_PROV" Then
        CargarTotalProveedores
    Else
        CargarVariablesNivel2 Mid(ssTabVar.Tabs(ssTabVar.selectedItem.Index).key, 2)
    End If
    
    Set oVarCal1 = Nothing
    Set oVarCal2 = Nothing
    Set oVarCal3 = Nothing
    Set oVarCal4 = Nothing
    Set oVarCal5 = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdBajaSubvar_Click()
''' <summary>
''' Proceso que realiza la baja logica o deshace la baja logica de de una variable de calidad de nivel 2
''' </summary>
''' <returns>Ninguno</returns>
''' <remarks>Llamada desde=Evento que salta cuando se pincha en el boton de Baja/deshacer baja logica; Tiempo m�ximo=0,5seg.</remarks>
Dim irespuesta As Integer
Dim oVarCal1 As CVariableCalidad
Dim oVarCal2 As CVariableCalidad
Dim oVarCal3 As CVariableCalidad
Dim oVarCal4 As CVariableCalidad
Dim oVarCal5 As CVariableCalidad

Dim sCod As String
Dim bHijasSinBaja As Boolean

    'si hay cambios los guarda
    If sdbgSubVariables.DataChanged = True Then
        sdbgSubVariables.Update
        If m_bError = True Then Exit Sub
    End If

    If sdbgSubVariables.Rows = 0 Then Exit Sub
    If sdbgSubVariables.SelBookmarks.Count = 0 Then sdbgSubVariables.SelBookmarks.Add sdbgSubVariables.Bookmark

    Set oVarCal1 = g_oVarsCalidad1Mod.Item("1" & CStr(Mid(ssTabVar.selectedItem.key, 2)))
    If oVarCal1 Is Nothing Then Exit Sub
    
    Set oVarCal2 = oVarCal1.VariblesCal.Item("2" & CStr(sdbgSubVariables.Columns("ID").Value))
    
    If cmdBajaSubvar.Tag = "NOBAJA" Then
        If Not oVarCal2.VariblesCal Is Nothing Then
            For Each oVarCal3 In oVarCal2.VariblesCal
                If Not oVarCal3.BajaLog Then
                    bHijasSinBaja = True
                    Exit For
                End If
            Next
        End If
    
        irespuesta = oMensajes.PreguntaBajaLogVariableCalidad(sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).Value, bHijasSinBaja)
        If irespuesta = vbNo Then Exit Sub
        Screen.MousePointer = vbHourglass
        oVarCal2.BajaLog = True
        If bHijasSinBaja Then
            For Each oVarCal3 In oVarCal2.VariblesCal
                oVarCal3.BajaLog = True
                oVarCal3.modificado = True
                If Not oVarCal3.VariblesCal Is Nothing Then
                    For Each oVarCal4 In oVarCal3.VariblesCal
                        oVarCal4.BajaLog = True
                        oVarCal4.modificado = True
                        If Not oVarCal4.VariblesCal Is Nothing Then
                            For Each oVarCal5 In oVarCal4.VariblesCal
                                oVarCal5.BajaLog = True
                                oVarCal5.modificado = True
                            Next
                        End If
                    Next
                End If
            Next
        End If
        
        sCod = sdbgSubVariables.Columns("COD").Value
        If chkVerBaja.Value = vbChecked Then
            sdbgSubVariables.Columns("BAJALOG").Value = BooleanToSQLBinary(oVarCal2.BajaLog)
        Else
            sdbgSubVariables.DeleteSelected
        End If
        cmdBajaSubvar.Picture = ImageList1.ListImages("BAJA").Picture
        cmdBajaSubvar.Tag = "BAJA"
        cmdElimSubvar.Enabled = False
    Else
        Screen.MousePointer = vbHourglass
        oVarCal2.BajaLog = False
        cmdBajaSubvar.Tag = "NOBAJA"
         cmdBajaSubvar.Picture = ImageList1.ListImages("NOBAJA").Picture
        cmdElimSubvar.Enabled = True
        sdbgSubVariables.Columns("BAJALOG").Value = 0
    End If
    oVarCal2.modificado = True
    sdbgSubVariables.col = 1
    sdbgSubVariables.Update

    VisualizarGuardar

    If InStr(1, txtFormula.Text, sCod) <> 0 Then
        txtFormula.Forecolor = vbRed
    End If

    sdbgSubVariables.SelBookmarks.RemoveAll
    If Me.Visible Then sdbgSubVariables.SetFocus
    Set oVarCal1 = Nothing
    Set oVarCal2 = Nothing
    Set oVarCal3 = Nothing
    Set oVarCal4 = Nothing
    Set oVarCal5 = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub chkVerBaja_Click()
''' <summary>
''' Proceso que carga  en los tabs y en la grid las variables que estan dados de baja/o no.
''' </summary>
''' <returns>Ninguno</returns>
''' <remarks>Llamada desde=Evento que salta cuando se pincha en el Check de "Ver bajas logicas"; Tiempo m�ximo=0,03seg.</remarks>

Dim oVarCal As CVariableCalidad
Dim iNumVar As Integer
Dim sCadena As String
Dim i As Byte
Dim Indice As Integer
Dim indiceSeleccionado

    iNumVar = 0
    If ssTabVar.selectedItem.key <> "TOT_PROV" Then
        Indice = Mid(ssTabVar.selectedItem.key, 2)
    End If
    indiceSeleccionado = 0
    ssTabVar.Tabs.clear
    
    For Each oVarCal In g_oVarsCalidad1Mod
        If ((oVarCal.BajaLog = True And (chkVerBaja.Value = vbChecked)) Or (Not oVarCal.BajaLog)) Then
            iNumVar = iNumVar + 1
            If Me.chkMultiidioma.Value = vbChecked Then
                sCadena = oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                For i = 1 To oVarCal.Denominaciones.Count
                    If oVarCal.Denominaciones.Item(i).Cod <> gParametrosInstalacion.gIdioma And oVarCal.Denominaciones.Item(i).Den <> "" Then
                        sCadena = sCadena & " / " & oVarCal.Denominaciones.Item(i).Den
                    End If
                    
                Next
            Else
                sCadena = oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            End If

            If oVarCal.BajaLog Then
                sCadena = "(!)" & sCadena ' Indicador que muestra que esta dado de baja logica
            End If
            If oVarCal.Id = Indice Then
                indiceSeleccionado = iNumVar
            End If
            ssTabVar.Tabs.Add iNumVar, "V" & oVarCal.Id, sCadena
            ssTabVar.Tabs(iNumVar).Tag = IIf(oVarCal.BajaLog, "BAJA", "NOBAJA")
        End If
    Next
    
    ssTabVar.Tabs.Add iNumVar + 1, "TOT_PROV", m_sTotProveedor
    If indiceSeleccionado = 0 Then
        indiceSeleccionado = iNumVar + 1
    End If
    ssTabVar.Tabs(indiceSeleccionado).Selected = True

    If ssTabVar.selectedItem.key = "TOT_PROV" Then
        CargarTotalProveedores
    Else
        CargarVariablesNivel2 Mid(ssTabVar.Tabs(ssTabVar.selectedItem.Index).key, 2)
    End If
    
    If ssTabVar.Tabs((ssTabVar.selectedItem.Index)).Tag = "BAJA" Then
        Me.cmdBajaVar.caption = m_sDeshacerBaja
    Else
        Me.cmdBajaVar.caption = m_sBaja
    End If
End Sub

Private Sub optOpcionConf_Click(Index As Integer)
    
     If Index <> VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
        sdbcVarPond.Text = ""
    End If
        
    If Index <> g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).Opcion_Conf Then
        HayCambios
        If Index = VarCalOpcionConf.EvaluarFormula Then
            g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).Opcion_Conf = VarCalOpcionConf.EvaluarFormula
            g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).IdVar_Pond = ""
        ElseIf Index = VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
            g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).Opcion_Conf = VarCalOpcionConf.MediaPonderadaSegunVarHermana
            g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).IdVar_Pond = sdbcVarPond.Columns(0).Value
        Else
            g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).Opcion_Conf = VarCalOpcionConf.NoCalcular
            g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).IdVar_Pond = ""
        End If
    End If
        
End Sub

Private Sub sdbcVarPond_Click()
    HayCambios
    optOpcionConf(VarCalOpcionConf.MediaPonderadaSegunVarHermana).Value = True
    g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).Opcion_Conf = VarCalOpcionConf.MediaPonderadaSegunVarHermana
    g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).IdVar_Pond = sdbcVarPond.Columns(0).Value
End Sub

Private Sub sdbcVarPond_InitColumnProps()
    sdbcVarPond.DataFieldList = "Column 0"
    sdbcVarPond.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcVarPond_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcVarPond.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcVarPond.Rows - 1
            bm = sdbcVarPond.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcVarPond.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcVarPond.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub HayCambios()
    If Not lblCambios.Visible Then
        VisualizarGuardar
    End If
End Sub

Private Sub CargarVariablesHermanas()
    Screen.MousePointer = vbHourglass
    Dim oVarCal As CVariableCalidad
    Dim g_oVariablesCalidadHermanas As CVariablesCalidad
    
    sdbcVarPond.RemoveAll
    Set g_oVariablesCalidadHermanas = g_oVarsCalidad1Mod
    For Each oVarCal In g_oVariablesCalidadHermanas
        If oVarCal.Id <> g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).Id And oVarCal.BajaLog = False Then
            sdbcVarPond.AddItem oVarCal.Id & Chr(m_lSeparador) & oVarCal.Cod & " - " & oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            If oVarCal.Id = g_oVarsCalidad1Mod.Item(ssTabVar.selectedItem.Index).IdVar_Pond Then
                sdbcVarPond.Text = oVarCal.Cod
            End If
        End If
    Next
    Screen.MousePointer = vbNormal
End Sub

