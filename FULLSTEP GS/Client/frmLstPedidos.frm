VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPedidos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dlistado Seguimiento de pedidos"
   ClientHeight    =   10530
   ClientLeft      =   405
   ClientTop       =   2010
   ClientWidth     =   13890
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstPedidos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10530
   ScaleWidth      =   13890
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   2000
      Left            =   0
      Top             =   5880
   End
   Begin VB.CommandButton cmdObtener 
      Caption         =   "Obtener"
      Height          =   375
      Left            =   10500
      TabIndex        =   81
      Top             =   9780
      Width           =   1335
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   10335
      Left            =   60
      TabIndex        =   80
      Top             =   60
      Width           =   13755
      _ExtentX        =   24262
      _ExtentY        =   18230
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstPedidos.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFiltro"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstPedidos.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraOpciones"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraFiltro 
         Height          =   9855
         Left            =   60
         TabIndex        =   90
         Top             =   360
         Width           =   13575
         Begin VB.Frame frmSegui 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1215
            Left            =   90
            TabIndex        =   119
            Top             =   120
            Width           =   13440
            Begin VB.CheckBox chkVerPedBajaLogica 
               Caption         =   "DVer pedidos borrados (baja l�gica)"
               Height          =   195
               Left            =   1320
               TabIndex        =   144
               Top             =   960
               Width           =   5310
            End
            Begin VB.TextBox txtFecEmiD 
               Height          =   285
               Left            =   8805
               TabIndex        =   3
               Top             =   105
               Width           =   1035
            End
            Begin VB.CommandButton cmdFecEmiD 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   9870
               Picture         =   "frmLstPedidos.frx":0CEA
               Style           =   1  'Graphical
               TabIndex        =   4
               Top             =   105
               Width           =   315
            End
            Begin VB.TextBox txtFecEmiH 
               Height          =   285
               Left            =   8805
               TabIndex        =   7
               Top             =   495
               Width           =   1035
            End
            Begin VB.CommandButton cmdFecEmiH 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   9870
               Picture         =   "frmLstPedidos.frx":0FFC
               Style           =   1  'Graphical
               TabIndex        =   8
               Top             =   495
               Width           =   315
            End
            Begin VB.TextBox txtPedido 
               Height          =   285
               Left            =   3390
               TabIndex        =   2
               Top             =   105
               Width           =   1275
            End
            Begin VB.TextBox txtReferencia 
               Height          =   285
               Left            =   11595
               TabIndex        =   5
               Top             =   105
               Width           =   1725
            End
            Begin VB.CommandButton cmdAvanzado 
               Caption         =   "&Avanzado>>"
               Height          =   315
               Left            =   12030
               TabIndex        =   82
               Top             =   825
               Width           =   1290
            End
            Begin VB.TextBox txtNumExt 
               Height          =   285
               Left            =   11595
               TabIndex        =   9
               Top             =   495
               Width           =   1725
            End
            Begin VB.TextBox txtNumCesta 
               Height          =   285
               Left            =   2445
               TabIndex        =   1
               Top             =   105
               Width           =   885
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
               Height          =   285
               Left            =   1305
               TabIndex        =   0
               Top             =   105
               Width           =   1065
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns(0).Width=   1693
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               _ExtentX        =   1879
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEst 
               Height          =   285
               Left            =   1305
               TabIndex        =   6
               Top             =   495
               Width           =   5355
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   8361
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "COD"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   9446
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "A�o:"
               Height          =   195
               Left            =   60
               TabIndex        =   125
               Top             =   150
               Width           =   1095
            End
            Begin VB.Label Label2 
               Caption         =   "Emision desde:"
               Height          =   195
               Left            =   7500
               TabIndex        =   124
               Top             =   120
               Width           =   1125
            End
            Begin VB.Label Label3 
               Caption         =   "Estado"
               Height          =   195
               Left            =   45
               TabIndex        =   123
               Top             =   525
               Width           =   1215
            End
            Begin VB.Label Label13 
               Caption         =   "hasta:"
               Height          =   195
               Left            =   8100
               TabIndex        =   122
               Top             =   540
               Width           =   540
            End
            Begin VB.Label Label17 
               Caption         =   "DRef en factura:"
               Height          =   195
               Left            =   10290
               TabIndex        =   121
               Top             =   120
               Width           =   1260
            End
            Begin VB.Label Label18 
               Caption         =   "N�m. externo:"
               Height          =   195
               Left            =   10290
               TabIndex        =   120
               Top             =   525
               Width           =   1095
            End
         End
         Begin VB.Frame FrameCateg 
            BorderStyle     =   0  'None
            Caption         =   "Frame6"
            Height          =   525
            Left            =   90
            TabIndex        =   131
            Top             =   4530
            Width           =   13440
            Begin VB.CommandButton cmdCatego 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   7140
               Picture         =   "frmLstPedidos.frx":130E
               Style           =   1  'Graphical
               TabIndex        =   37
               Top             =   120
               Width           =   315
            End
            Begin VB.CommandButton cmdBorraCat 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6795
               Picture         =   "frmLstPedidos.frx":137A
               Style           =   1  'Graphical
               TabIndex        =   36
               Top             =   120
               Width           =   315
            End
            Begin VB.TextBox txtDato2 
               Height          =   285
               Left            =   11550
               TabIndex        =   39
               Top             =   120
               Visible         =   0   'False
               Width           =   1635
            End
            Begin VB.TextBox txtDato1 
               Height          =   285
               Left            =   8805
               TabIndex        =   38
               Top             =   120
               Visible         =   0   'False
               Width           =   1635
            End
            Begin VB.Line Line8 
               X1              =   0
               X2              =   13340
               Y1              =   0
               Y2              =   0
            End
            Begin VB.Label lblCategoria 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1305
               TabIndex        =   35
               Top             =   150
               Width           =   5430
            End
            Begin VB.Label Label12 
               Caption         =   "Categor�a:"
               Height          =   225
               Left            =   45
               TabIndex        =   134
               Top             =   180
               Width           =   1650
            End
            Begin VB.Label lblDato2 
               Caption         =   "Dato2:"
               Height          =   195
               Left            =   10740
               TabIndex        =   133
               Tag             =   "0"
               Top             =   180
               Visible         =   0   'False
               Width           =   720
            End
            Begin VB.Label lblDato1 
               Caption         =   "Dato1:"
               Height          =   195
               Left            =   7500
               TabIndex        =   132
               Tag             =   "0"
               Top             =   180
               Visible         =   0   'False
               Width           =   1005
            End
         End
         Begin VB.Frame FrameProve 
            BorderStyle     =   0  'None
            Height          =   1335
            Left            =   90
            TabIndex        =   111
            Top             =   2250
            Width           =   13440
            Begin VB.CommandButton cmdFecEntH 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   11730
               Picture         =   "frmLstPedidos.frx":141F
               Style           =   1  'Graphical
               TabIndex        =   30
               Top             =   930
               Width           =   315
            End
            Begin VB.TextBox txtFecEntH 
               Height          =   285
               Left            =   10830
               TabIndex        =   29
               Top             =   930
               Width           =   915
            End
            Begin VB.CommandButton cmdFecEntD 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   9750
               Picture         =   "frmLstPedidos.frx":1731
               Style           =   1  'Graphical
               TabIndex        =   28
               Top             =   930
               Width           =   315
            End
            Begin VB.TextBox txtFecEntD 
               Height          =   285
               Left            =   8805
               TabIndex        =   27
               Top             =   930
               Width           =   915
            End
            Begin VB.CommandButton cmdBuscarProve 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6800
               Picture         =   "frmLstPedidos.frx":1A43
               Style           =   1  'Graphical
               TabIndex        =   18
               Top             =   180
               Width           =   315
            End
            Begin VB.CommandButton cmdBorraMat 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6795
               Picture         =   "frmLstPedidos.frx":1AD0
               Style           =   1  'Graphical
               TabIndex        =   21
               Top             =   540
               Width           =   315
            End
            Begin VB.CommandButton cmdSelMat 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   7140
               Picture         =   "frmLstPedidos.frx":1B75
               Style           =   1  'Graphical
               TabIndex        =   22
               Top             =   540
               Width           =   315
            End
            Begin VB.TextBox txtCodERP 
               Height          =   285
               Left            =   8805
               TabIndex        =   19
               Top             =   180
               Width           =   2985
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
               Height          =   285
               Left            =   2925
               TabIndex        =   17
               Top             =   180
               Width           =   3855
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6809
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcArtiCod 
               Height          =   285
               Left            =   8805
               TabIndex        =   23
               Top             =   570
               Width           =   1500
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2408
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   5900
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2646
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcArtiDen 
               Height          =   285
               Left            =   10335
               TabIndex        =   24
               Top             =   570
               Width           =   3030
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   5900
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2408
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5345
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
               Height          =   285
               Left            =   1305
               TabIndex        =   16
               Top             =   180
               Width           =   1515
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2672
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcDestCod 
               Height          =   285
               Left            =   1305
               TabIndex        =   25
               Top             =   930
               Width           =   1515
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2672
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcDestDen 
               Height          =   285
               Left            =   2925
               TabIndex        =   26
               Top             =   930
               Width           =   3840
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6773
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label Label15 
               Caption         =   "hasta:"
               Height          =   195
               Left            =   10230
               TabIndex        =   136
               Top             =   960
               Width           =   540
            End
            Begin VB.Label Label14 
               Caption         =   "Entrega desde:"
               Height          =   195
               Left            =   7500
               TabIndex        =   135
               Top             =   960
               Width           =   1215
            End
            Begin VB.Label Label8 
               Caption         =   "Proveedor:"
               Height          =   195
               Left            =   45
               TabIndex        =   116
               Top             =   210
               Width           =   840
            End
            Begin VB.Label Label9 
               Caption         =   "Material:"
               Height          =   195
               Left            =   45
               TabIndex        =   115
               Top             =   570
               Width           =   840
            End
            Begin VB.Label Label10 
               Caption         =   "Art�culo"
               Height          =   195
               Left            =   7500
               TabIndex        =   114
               Top             =   570
               Width           =   690
            End
            Begin VB.Label Label11 
               Caption         =   "Destino:"
               Height          =   195
               Left            =   45
               TabIndex        =   113
               Top             =   960
               Width           =   840
            End
            Begin VB.Label lblMaterial 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1305
               TabIndex        =   20
               Top             =   540
               Width           =   5430
            End
            Begin VB.Label lblCodERP 
               Caption         =   "DProveedor ERP:"
               Height          =   195
               Left            =   7500
               TabIndex        =   112
               Top             =   210
               Width           =   1245
            End
            Begin VB.Line Line2 
               X1              =   0
               X2              =   13340
               Y1              =   0
               Y2              =   0
            End
         End
         Begin VB.Frame frameEmpresa 
            BorderStyle     =   0  'None
            Caption         =   "Frame5"
            Height          =   1020
            Left            =   90
            TabIndex        =   107
            Top             =   3600
            Width           =   13440
            Begin SSDataWidgets_B.SSDBCombo sdbcReceptor 
               Height          =   285
               Left            =   1305
               TabIndex        =   33
               Top             =   540
               Width           =   5385
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   -2147483630
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   8414
               Columns(1).Caption=   "Denominacion"
               Columns(1).Name =   "Denominacion"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Per"
               Columns(2).Name =   "Per"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   9499
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcUsuCod 
               Height          =   285
               Left            =   8805
               TabIndex        =   32
               Top             =   150
               Width           =   4425
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   -2147483640
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   2937
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   8758
               Columns(1).Caption=   "Denominacion"
               Columns(1).Name =   "Denominacion"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Per"
               Columns(2).Name =   "Per"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   7805
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEmpresa 
               Height          =   285
               Left            =   1305
               TabIndex        =   31
               Top             =   120
               Width           =   5385
               DataFieldList   =   "Column 0"
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3360
               Columns(1).Caption=   "CIF"
               Columns(1).Name =   "CIF"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   8308
               Columns(2).Caption=   "RAZON"
               Columns(2).Name =   "RAZON"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   9499
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGestor 
               Height          =   285
               Left            =   8805
               TabIndex        =   34
               Top             =   570
               Width           =   4425
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   -2147483630
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   8414
               Columns(1).Caption=   "Denominacion"
               Columns(1).Name =   "Denominacion"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Per"
               Columns(2).Name =   "Per"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   7805
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblGestor 
               Caption         =   "DGestor:"
               Height          =   195
               Left            =   7500
               TabIndex        =   128
               Top             =   570
               Width           =   1035
            End
            Begin VB.Label lblEmpresa 
               Caption         =   "Empresa:"
               Height          =   195
               Left            =   45
               TabIndex        =   110
               Top             =   150
               Width           =   1095
            End
            Begin VB.Label lblAprov 
               Caption         =   "Aprovisionador:"
               Height          =   195
               Left            =   7500
               TabIndex        =   109
               Top             =   150
               Width           =   1155
            End
            Begin VB.Label lblReceptor 
               Caption         =   "Receptor:"
               Height          =   195
               Left            =   45
               TabIndex        =   108
               Top             =   570
               Width           =   1335
            End
            Begin VB.Line Line5 
               X1              =   0
               X2              =   13340
               Y1              =   0
               Y2              =   0
            End
         End
         Begin VB.Frame FrameFactura 
            BorderStyle     =   0  'None
            Height          =   2055
            Left            =   90
            TabIndex        =   99
            Top             =   6060
            Width           =   13440
            Begin VB.CheckBox chkFacPedido 
               Caption         =   "dPor pedido"
               Height          =   315
               Left            =   6150
               TabIndex        =   55
               Top             =   750
               Width           =   1305
            End
            Begin VB.CheckBox chkAutom 
               Caption         =   "dPedido con planes de entrega automaticos"
               Height          =   315
               Left            =   3120
               TabIndex        =   48
               Top             =   360
               Width           =   3585
            End
            Begin VB.CheckBox chkAutoFactura 
               Caption         =   "dPedidos de Autofactura"
               Height          =   315
               Left            =   60
               TabIndex        =   47
               Top             =   360
               Width           =   3015
            End
            Begin VB.CommandButton cmdFecHastaPlanEntrega 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   11910
               Picture         =   "frmLstPedidos.frx":1BE1
               Style           =   1  'Graphical
               TabIndex        =   52
               Top             =   390
               Width           =   315
            End
            Begin VB.TextBox txtFecHastaPlanEntrega 
               Height          =   285
               Left            =   10830
               TabIndex        =   51
               Top             =   390
               Width           =   1065
            End
            Begin VB.CommandButton cmdFecDesdePlanEntrega 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   9870
               Picture         =   "frmLstPedidos.frx":1EF3
               Style           =   1  'Graphical
               TabIndex        =   50
               Top             =   390
               Width           =   315
            End
            Begin VB.TextBox txtFecDesdePlanEntrega 
               Height          =   285
               Left            =   8805
               TabIndex        =   49
               Top             =   390
               Width           =   1035
            End
            Begin VB.TextBox txtNumPago 
               Height          =   285
               Left            =   2370
               TabIndex        =   63
               Top             =   1680
               Width           =   915
            End
            Begin VB.TextBox txtFecDesdeFactura 
               Height          =   285
               Left            =   8805
               TabIndex        =   59
               Top             =   1320
               Width           =   1035
            End
            Begin VB.CommandButton cmdFecDesdeFactura 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   9870
               Picture         =   "frmLstPedidos.frx":2205
               Style           =   1  'Graphical
               TabIndex        =   60
               Top             =   1320
               Width           =   315
            End
            Begin VB.TextBox txtFecHastaFactura 
               Height          =   285
               Left            =   10830
               TabIndex        =   61
               Top             =   1320
               Width           =   1065
            End
            Begin VB.CommandButton cmdFecHastaFactura 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   11910
               Picture         =   "frmLstPedidos.frx":2517
               Style           =   1  'Graphical
               TabIndex        =   62
               Top             =   1320
               Width           =   315
            End
            Begin VB.TextBox txtNumFactura 
               Height          =   285
               Left            =   2370
               TabIndex        =   57
               Top             =   1305
               Width           =   915
            End
            Begin VB.CommandButton cmdFecHastaPago 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   11910
               Picture         =   "frmLstPedidos.frx":2829
               Style           =   1  'Graphical
               TabIndex        =   68
               Top             =   1710
               Width           =   315
            End
            Begin VB.TextBox txtFecHastaPago 
               Height          =   285
               Left            =   10830
               TabIndex        =   67
               Top             =   1710
               Width           =   1065
            End
            Begin VB.CommandButton cmdFecDesdePago 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   9870
               Picture         =   "frmLstPedidos.frx":2B3B
               Style           =   1  'Graphical
               TabIndex        =   66
               Top             =   1710
               Width           =   315
            End
            Begin VB.TextBox txtFecDesdePago 
               Height          =   285
               Left            =   8805
               TabIndex        =   65
               Top             =   1710
               Width           =   1035
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcAnyoFacturas 
               Height          =   285
               Left            =   1305
               TabIndex        =   56
               Top             =   1305
               Width           =   1035
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns(0).Width=   1693
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEstadoFactura 
               Height          =   285
               Left            =   3330
               TabIndex        =   58
               Top             =   1305
               Width           =   3975
               DataFieldList   =   "Column 0"
               _Version        =   196617
               DataMode        =   2
               HeadLines       =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   7011
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   7011
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEstadoPago 
               Height          =   285
               Left            =   3330
               TabIndex        =   64
               Top             =   1680
               Width           =   3975
               DataFieldList   =   "Column 0"
               _Version        =   196617
               DataMode        =   2
               HeadLines       =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   7011
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   7011
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.CheckBox chkFacAlbaran 
               Caption         =   "dPor albar�n"
               Height          =   315
               Left            =   4710
               TabIndex        =   54
               Top             =   750
               Width           =   1335
            End
            Begin VB.CheckBox chkBloqueos 
               Caption         =   "dPedidos con bloqueos de factura"
               Height          =   315
               Left            =   60
               TabIndex        =   53
               Top             =   750
               Width           =   2955
            End
            Begin VB.Label lblTipoFacturacion 
               AutoSize        =   -1  'True
               Caption         =   "DTipo de facturaci�n:"
               Height          =   195
               Left            =   3120
               TabIndex        =   137
               Top             =   780
               Width           =   1545
            End
            Begin VB.Label lblEstadoFacturaPago 
               AutoSize        =   -1  'True
               Caption         =   "Estado de Factura / Pago"
               Height          =   195
               Left            =   3360
               TabIndex        =   103
               Top             =   1080
               Width           =   1830
            End
            Begin VB.Label lblPlanEntregaDesde 
               AutoSize        =   -1  'True
               Caption         =   "DPlanes de entrega desde"
               Height          =   195
               Left            =   8805
               TabIndex        =   127
               Top             =   180
               Width           =   1890
            End
            Begin VB.Label lblPlanEntregaHasta 
               Caption         =   "hasta"
               Height          =   195
               Left            =   10830
               TabIndex        =   126
               Top             =   180
               Width           =   540
            End
            Begin VB.Label lblNumPago 
               Caption         =   "Pagos (n�m.)"
               Height          =   195
               Left            =   1320
               TabIndex        =   106
               Top             =   1740
               Width           =   975
            End
            Begin VB.Label lblHasta 
               Caption         =   "hasta"
               Height          =   195
               Left            =   10830
               TabIndex        =   105
               Top             =   1110
               Width           =   540
            End
            Begin VB.Label lblFechaDesdeFacturaPago 
               AutoSize        =   -1  'True
               Caption         =   "Fecha factura/pago desde"
               Height          =   195
               Left            =   8805
               TabIndex        =   104
               Top             =   1110
               Width           =   1905
            End
            Begin VB.Label lblNumFactura 
               AutoSize        =   -1  'True
               Caption         =   "N�m factura"
               Height          =   195
               Left            =   2370
               TabIndex        =   102
               Top             =   1080
               Width           =   885
            End
            Begin VB.Label lblAnyoFactura 
               AutoSize        =   -1  'True
               Caption         =   "A�o factura"
               Height          =   195
               Left            =   1305
               TabIndex        =   101
               Top             =   1080
               Width           =   855
            End
            Begin VB.Label lblFactura 
               Caption         =   "Factura:"
               Height          =   195
               Left            =   30
               TabIndex        =   100
               Top             =   1350
               Width           =   795
            End
            Begin VB.Line Line6 
               X1              =   0
               X2              =   13340
               Y1              =   120
               Y2              =   120
            End
         End
         Begin VB.Frame fraProce 
            BorderStyle     =   0  'None
            Caption         =   "Frame5"
            Height          =   1605
            Left            =   90
            TabIndex        =   95
            Top             =   8160
            Width           =   13440
            Begin VB.Frame Frame3 
               BorderStyle     =   0  'None
               Height          =   735
               Left            =   60
               TabIndex        =   98
               Top             =   120
               Width           =   10980
               Begin VB.CommandButton cmdBuscarProAdj 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   9420
                  Picture         =   "frmLstPedidos.frx":2E4D
                  Style           =   1  'Graphical
                  TabIndex        =   76
                  Top             =   390
                  Width           =   315
               End
               Begin VB.CommandButton cmdBuscarProce 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   9420
                  Picture         =   "frmLstPedidos.frx":2EDA
                  Style           =   1  'Graphical
                  TabIndex        =   73
                  Top             =   30
                  Width           =   315
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcAnyo2 
                  Height          =   285
                  Left            =   1305
                  TabIndex        =   69
                  Top             =   30
                  Width           =   1050
                  ScrollBars      =   2
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns(0).Width=   1693
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  _ExtentX        =   1852
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   16777215
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcCM 
                  Height          =   285
                  Left            =   2340
                  TabIndex        =   70
                  Top             =   30
                  Width           =   1170
                  ScrollBars      =   2
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns(0).Width=   1693
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  _ExtentX        =   2064
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   16777215
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
                  Height          =   285
                  Left            =   3525
                  TabIndex        =   71
                  Top             =   30
                  Width           =   1200
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   2117
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
                  Height          =   285
                  Left            =   4740
                  TabIndex        =   72
                  Top             =   30
                  Width           =   4590
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   6165
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 1"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   2117
                  Columns(1).Caption=   "Cod"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 0"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   8096
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcProAdjDen 
                  Height          =   285
                  Left            =   2835
                  TabIndex        =   75
                  Top             =   390
                  Width           =   6510
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   6165
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 1"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   2117
                  Columns(1).Caption=   "Cod"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 0"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   11483
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcProAdjCod 
                  Height          =   285
                  Left            =   1305
                  TabIndex        =   74
                  Top             =   390
                  Width           =   1515
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   2672
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblProAdj 
                  Caption         =   "DProv adjudic:"
                  Height          =   195
                  Left            =   0
                  TabIndex        =   130
                  Top             =   450
                  Width           =   1185
               End
               Begin VB.Label Label4 
                  Caption         =   "DProceso (A�o, Comm, Cod):"
                  Height          =   595
                  Left            =   0
                  TabIndex        =   129
                  Top             =   0
                  Width           =   1190
               End
            End
            Begin VB.Frame Frame4 
               BorderStyle     =   0  'None
               Caption         =   "Frame4"
               Height          =   320
               Left            =   0
               TabIndex        =   96
               Top             =   900
               Width           =   11730
               Begin VB.CommandButton cmdBuscarSolicitud 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   6420
                  Picture         =   "frmLstPedidos.frx":2F67
                  Style           =   1  'Graphical
                  TabIndex        =   79
                  Top             =   0
                  Width           =   315
               End
               Begin VB.CommandButton cmdBorraSolicitud 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   6090
                  Picture         =   "frmLstPedidos.frx":2FF4
                  Style           =   1  'Graphical
                  TabIndex        =   78
                  Top             =   0
                  Width           =   315
               End
               Begin VB.CommandButton cmdOcultar 
                  Caption         =   "<< Ocultar"
                  Height          =   315
                  Left            =   10335
                  TabIndex        =   83
                  Top             =   0
                  Width           =   1065
               End
               Begin VB.Label lblSolicitud 
                  BackColor       =   &H00C0FFFF&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   1365
                  TabIndex        =   77
                  Top             =   0
                  Width           =   4695
               End
               Begin VB.Label lblSolicitudTitle 
                  Caption         =   "Solicitud:"
                  Height          =   195
                  Left            =   45
                  TabIndex        =   97
                  Top             =   0
                  Width           =   975
               End
            End
            Begin VB.Line Line3 
               X1              =   0
               X2              =   13340
               Y1              =   0
               Y2              =   0
            End
         End
         Begin VB.Frame FrameImp 
            BorderStyle     =   0  'None
            Height          =   1095
            Left            =   90
            TabIndex        =   91
            Top             =   4950
            Width           =   13440
            Begin VB.ListBox lstCentroCoste 
               Height          =   420
               IntegralHeight  =   0   'False
               Left            =   1305
               TabIndex        =   92
               Top             =   600
               Visible         =   0   'False
               Width           =   5385
            End
            Begin VB.TextBox txtCentroCoste 
               Height          =   285
               Left            =   1305
               TabIndex        =   40
               Top             =   240
               Width           =   5430
            End
            Begin VB.TextBox txtPartida 
               Height          =   285
               Left            =   8805
               TabIndex        =   43
               Top             =   240
               Width           =   3855
            End
            Begin VB.CommandButton cmdLimpiarCentroCoste 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6795
               Picture         =   "frmLstPedidos.frx":3099
               Style           =   1  'Graphical
               TabIndex        =   41
               Top             =   240
               Width           =   315
            End
            Begin VB.CommandButton cmdBuscarCentroCoste 
               Height          =   285
               Left            =   7140
               Picture         =   "frmLstPedidos.frx":313E
               Style           =   1  'Graphical
               TabIndex        =   42
               Top             =   240
               Width           =   315
            End
            Begin VB.CommandButton cmdBuscarPartida 
               Height          =   285
               Left            =   13020
               Picture         =   "frmLstPedidos.frx":34C0
               Style           =   1  'Graphical
               TabIndex        =   45
               Top             =   240
               Width           =   315
            End
            Begin VB.CommandButton cmdLimpiarPartida 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   12660
               Picture         =   "frmLstPedidos.frx":3842
               Style           =   1  'Graphical
               TabIndex        =   44
               Top             =   240
               Width           =   315
            End
            Begin SSDataWidgets_B.SSDBGrid sdbgPartidas 
               Height          =   855
               Left            =   7500
               TabIndex        =   46
               Top             =   240
               Visible         =   0   'False
               Width           =   5850
               _Version        =   196617
               DataMode        =   2
               RecordSelectors =   0   'False
               GroupHeaders    =   0   'False
               ColumnHeaders   =   0   'False
               Col.Count       =   5
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowColumnSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   5
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(0).Locked=   -1  'True
               Columns(1).Width=   2381
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(1).Locked=   -1  'True
               Columns(1).HasBackColor=   -1  'True
               Columns(1).BackColor=   12632256
               Columns(2).Width=   4921
               Columns(2).Caption=   "VALOR"
               Columns(2).Name =   "VALOR"
               Columns(2).AllowSizing=   0   'False
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(2).Style=   1
               Columns(2).ButtonsAlways=   -1  'True
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "POSICION"
               Columns(3).Name =   "POSICION"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   3200
               Columns(4).Visible=   0   'False
               Columns(4).Caption=   "NIVEL_IMPUTACION"
               Columns(4).Name =   "NIVEL_IMPUTACION"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               _ExtentX        =   10319
               _ExtentY        =   1508
               _StockProps     =   79
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Line Line7 
               X1              =   0
               X2              =   13355
               Y1              =   120
               Y2              =   120
            End
            Begin VB.Label lblCentroCoste 
               AutoSize        =   -1  'True
               Caption         =   "Centro de coste:"
               Height          =   195
               Left            =   0
               TabIndex        =   94
               Top             =   270
               Width           =   1215
            End
            Begin VB.Label lblContrato 
               AutoSize        =   -1  'True
               Caption         =   "DContrato:"
               Height          =   195
               Left            =   7500
               TabIndex        =   93
               Top             =   270
               Width           =   1260
            End
         End
         Begin VB.Frame FrameTipoPedido 
            BorderStyle     =   0  'None
            Height          =   780
            Left            =   90
            TabIndex        =   117
            Top             =   1440
            Width           =   13440
            Begin VB.CheckBox chkSoloAbono 
               Caption         =   "dSolo abono"
               Height          =   255
               Left            =   12000
               TabIndex        =   15
               Top             =   270
               Width           =   1455
            End
            Begin VB.CheckBox chkPedErp 
               Caption         =   "Erp"
               Height          =   195
               Left            =   9600
               TabIndex        =   14
               Top             =   465
               Width           =   2190
            End
            Begin VB.CheckBox chkPedDirec 
               Caption         =   "dNegociados"
               Height          =   195
               Left            =   7530
               TabIndex        =   11
               Top             =   120
               Width           =   1725
            End
            Begin VB.CheckBox chkPedAprov 
               Caption         =   "DCatalogo"
               Height          =   195
               Left            =   9600
               TabIndex        =   12
               Top             =   120
               Width           =   2115
            End
            Begin VB.CheckBox chkPedPM 
               Caption         =   "Directos"
               Height          =   195
               Left            =   7530
               TabIndex        =   13
               Top             =   465
               Width           =   1785
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcTipoPedido 
               Height          =   285
               Left            =   1305
               TabIndex        =   10
               Top             =   225
               Width           =   5385
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   9
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "C�digo"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(1).HasForeColor=   -1  'True
               Columns(1).HasBackColor=   -1  'True
               Columns(1).BackColor=   16777215
               Columns(2).Width=   7594
               Columns(2).Caption=   "Denominaci�n"
               Columns(2).Name =   "DEN"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(2).HasBackColor=   -1  'True
               Columns(2).BackColor=   16777215
               Columns(3).Width=   1931
               Columns(3).Caption=   "Concepto"
               Columns(3).Name =   "INV"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   2302
               Columns(4).Caption=   "Almacen"
               Columns(4).Name =   "ALMAC"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               Columns(5).Width=   2302
               Columns(5).Caption=   "Recepci�n"
               Columns(5).Name =   "RECEP"
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).FieldLen=   256
               Columns(6).Width=   3200
               Columns(6).Visible=   0   'False
               Columns(6).Caption=   "INV_"
               Columns(6).Name =   "INV_"
               Columns(6).DataField=   "Column 6"
               Columns(6).DataType=   8
               Columns(6).FieldLen=   256
               Columns(7).Width=   3200
               Columns(7).Visible=   0   'False
               Columns(7).Caption=   "ALMAC_"
               Columns(7).Name =   "ALMAC_"
               Columns(7).DataField=   "Column 7"
               Columns(7).DataType=   8
               Columns(7).FieldLen=   256
               Columns(8).Width=   3200
               Columns(8).Visible=   0   'False
               Columns(8).Caption=   "RECEP_"
               Columns(8).Name =   "RECEP_"
               Columns(8).DataField=   "Column 8"
               Columns(8).DataType=   8
               Columns(8).FieldLen=   256
               _ExtentX        =   9499
               _ExtentY        =   503
               _StockProps     =   93
               ForeColor       =   -2147483642
               BackColor       =   -2147483643
               DataFieldToDisplay=   "Column 0"
            End
            Begin VB.Line Line1 
               X1              =   0
               X2              =   13250
               Y1              =   0
               Y2              =   0
            End
            Begin VB.Label lblTipoPedido 
               AutoSize        =   -1  'True
               Caption         =   "Tipo de pedido:"
               Height          =   195
               Left            =   45
               TabIndex        =   118
               Top             =   270
               Width           =   1110
            End
         End
      End
      Begin VB.Frame fraOpciones 
         Height          =   3495
         Left            =   -74820
         TabIndex        =   89
         Top             =   495
         Width           =   9870
         Begin VB.CheckBox chkIncluir 
            Caption         =   "DIncluir detalle de los costes y descuentos de los pedidos"
            Height          =   285
            Index           =   5
            Left            =   1395
            TabIndex        =   138
            Top             =   480
            Width           =   8160
         End
         Begin VB.CheckBox chkIncluir 
            Caption         =   "Incluir observaciones generales de los pedidos"
            Height          =   285
            Index           =   0
            Left            =   1395
            TabIndex        =   139
            Top             =   930
            Width           =   8160
         End
         Begin VB.CheckBox chkIncluir 
            Caption         =   "Incluir observaciones de las lineas de pedido"
            Height          =   285
            Index           =   1
            Left            =   1395
            TabIndex        =   140
            Top             =   1395
            Width           =   8025
         End
         Begin VB.CheckBox chkIncluir 
            Caption         =   "Incluir informaci�n presupuestaria"
            Height          =   285
            Index           =   2
            Left            =   1395
            TabIndex        =   141
            Top             =   1860
            Width           =   8160
         End
         Begin VB.CheckBox chkIncluir 
            Caption         =   "Incluir datos de imputaci�n a partidas de control presupuestario."
            Height          =   285
            Index           =   3
            Left            =   1395
            TabIndex        =   142
            Top             =   2310
            Width           =   8025
         End
         Begin VB.CheckBox chkIncluir 
            Caption         =   "DIncluir planes de entrega"
            Height          =   285
            Index           =   4
            Left            =   1395
            TabIndex        =   143
            Top             =   2760
            Width           =   8025
         End
      End
      Begin VB.Frame fraOrden 
         Caption         =   "Orden"
         Height          =   675
         Left            =   -74865
         TabIndex        =   86
         Top             =   915
         Width           =   5775
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   195
            Left            =   2970
            TabIndex        =   88
            Top             =   270
            Width           =   1515
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            Height          =   195
            Left            =   615
            TabIndex        =   87
            Top             =   270
            Value           =   -1  'True
            Width           =   1515
         End
      End
      Begin VB.Frame Frame2 
         Height          =   615
         Left            =   -74880
         TabIndex        =   84
         Top             =   315
         Width           =   5775
         Begin VB.CheckBox chkAcc 
            Caption         =   "Incluir acciones de seguridad disponibles"
            Height          =   240
            Left            =   615
            TabIndex        =   85
            Top             =   225
            Value           =   1  'Checked
            Width           =   4440
         End
      End
   End
End
Attribute VB_Name = "frmLstPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_ador As Ador.Recordset
Public g_ador1 As Ador.Recordset
Public g_ador2 As Ador.Recordset
Public g_ador3 As Ador.Recordset
Public g_ador4 As Ador.Recordset
Public g_ador5 As Ador.Recordset
Public g_ador6 As Ador.Recordset
Public g_ador7 As Ador.Recordset
Public g_ador8 As Ador.Recordset
 
Private m_bRMat As Boolean
Private m_bRAsig As Boolean
Private m_bRCompResp As Boolean
Private m_bREqpAsig As Boolean
Private m_bRUsuAper As Boolean
Private m_bRUsuUON As Boolean
Private m_bRUsuPerfUON As Boolean
Private m_bRUsuDep As Boolean
Private m_bRUsuAprov As Boolean
Private m_bPedAprov As Boolean
Private m_bPedDirec As Boolean
Private m_bPedErp As Boolean

Private m_bRDest As Boolean
Private m_bVerSolic As Boolean
Private m_bRuo As Boolean
Private m_bRPerfUO As Boolean
Private m_bRDep As Boolean
Private m_bREmpresa As Boolean

Public g_sOrigen As String

Public g_bPedidosAbiertos As Boolean

Public bRespetarComboEst As Boolean
Public bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
Public bRespetarComboProve As Boolean
Public bRespetarComboDest As Boolean
Private bRespetarComboEmpresa As Boolean
Private bRespetarComboRecep As Boolean
Private bRespetarComboGestor As Boolean

Private m_lNumPedido As Long
Private m_iAnyoPed As Integer
Private m_lNumCesta As Long
Private m_iNumProceCod As Long
Private m_iNumAnyo2 As Integer
Public m_lSolicit As Long
Private m_sCodPer As String
Private m_sCodReceptor As String
Private m_sCodGestor As String

Private sEspera(1 To 3) As String
Private arCaptionsInforme(0 To 64) As String

Private txtViaPago As String
Private sEstados(1 To 22) As String
Private sEstadosInt(0 To 4) As String
Private sSeleccion As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private txtCentro As String
Private txtProveAdj As String

Private oGrupoMN4seleccionado As CGrupoMatNivel4
Private oGMN1Seleccionado As CGrupoMatNivel1

'Proceso seleccionado
Public oProcesoSeleccionado As CProceso
' Coleccion de procesos a cargar
Private oProcesos As CProcesos
Private oProveSeleccionado As CProveedor
Private oProves As CProveedores
Private oDestinos As CDestinos
Private oEmpresas As CEmpresas

Private sIdiEst(1 To 10) As String
Private sIdiEstLin(1 To 7) As String
Private sIdioma() As String
Private sEmpresa As String
Private vNumEst As Variant
Public sGMN1Cod As String
Public sGMN2Cod As String
Public sGMN3Cod As String
Public sGMN4Cod As String

Public ACN1Seleccionada As Variant
Public ACN2Seleccionada As Variant
Public ACN3Seleccionada As Variant
Public ACN4Seleccionada As Variant
Public ACN5Seleccionada As Variant

Public iCampo1 As Integer
Public iCampo2 As Integer

Private iAnyoSeleccionado As Integer
Private CargarComboDesde As Boolean

Private m_lTipoPedido As Long  'Seleccion del filtro de Tipo de Pedido
Private m_arrConcep(2) As String
Private m_arrRecep(2) As String
Private m_arrAlmac(2) As String

Private m_lEmpresa As Long
Public g_oCenCos As CCentroCoste
Private m_arrCentrosCoste() As String 'Contiene un array con los centros de coste (uon1-uon2-uon3-uon4)
Private m_bRespetar As Boolean
Private m_sUON_CC As String ' Cadena que contiene la estructura del Centro de coste para el filtro (UON1-UON2-UON3-UON4)
Public m_oCentroCosteSeleccionadoFiltro As CCentroCoste

Private m_bControlPartidaFiltro As Boolean
Private m_iNivelImputacion As Integer
Private m_arrPartidas() As String  'array de cadenas que contiene la estructura de las partida(s) (PRES0-PRES1-PRES2-PRES3-PRES4-PRES5)
Private m_bControlCC As Boolean 'Variable que lleva el control de si se ha modificado la celda de CC
                                'Cuando se modifica la celda de CC se pone a true y una vez validada
Public oContratoSeleccionado As CContratopres
Public oCentroCosteSeleccionado As CCentroCoste

'Filtrado
Private arfiltrosFac(5) As Variant
Private arfiltrosPago(4) As Variant
Private arFiltrosImp() As Variant

'Variables Combos Proveedores adjudicados
Public bRespetarComboProAdj As Boolean
Private bCargarComboProAdjDesde As Boolean

'Planes
Private sPlanes(0 To 3) As String
Private g_ador9 As Ador.Recordset

'Prov Adjudic
Private m_bProvMat As Boolean

''' <summary>Carga de los literales multiidioma</summary>
''' <remarks>Llamada desde:frmLstPedidos.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTPEDIDOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 7)
        Ador.MoveNext
        label2.caption = Ador(0).Value '2
        Ador.MoveNext
        Label3.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Label13.caption = Ador(0).Value
        Label15.caption = Ador(0).Value
        Ador.MoveNext
        Label14.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        chkPedDirec.caption = Ador(0).Value
        Ador.MoveNext
        chkPedAprov.caption = Ador(0).Value
        Ador.MoveNext
        'chkLinPedidos.Caption = Ador(0).Value '10
        Ador.MoveNext
        frmLstPedidos.caption = Ador(0).Value
        arCaptionsInforme(0) = Ador(0).Value   'txtTitulo
        Ador.MoveNext
        Label12.caption = Ador(0).Value
        Ador.MoveNext
        Label8.caption = Ador(0).Value
        Ador.MoveNext
        Label9.caption = Ador(0).Value
        Ador.MoveNext
        label10.caption = Ador(0).Value
        Ador.MoveNext
        Label11.caption = Ador(0).Value
        Ador.MoveNext
        Label4.caption = Ador(0).Value
        sIdioma(7) = Left(Ador(0).Value, Len(Ador(0).Value) - 1) 'A�o proceso
        Ador.MoveNext
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(16) = Ador(0).Value '20  'txtSeleccion
        Ador.MoveNext
        For i = 1 To 10
            sIdiEst(i) = Ador(0).Value
            Ador.MoveNext
        Next
        sIdioma(2) = Ador(0).Value 'Numero de Pedido 31
        Ador.MoveNext
        sIdioma(3) = Ador(0).Value 'Proceso
'        Label7.caption = ador(0).Value & ":"
        Ador.MoveNext
        sIdioma(6) = Ador(0).Value '33 Material
        Ador.MoveNext
        sIdioma(4) = Ador(0).Value 'Fecha
        sPlanes(1) = Ador(0).Value
        Ador.MoveNext
        sIdioma(5) = Ador(0).Value 'Proveedor 35
        arCaptionsInforme(20) = Ador(0).Value  'txtProveedor
        Ador.MoveNext
        For i = 1 To 6
            sIdiEstLin(i) = Ador(0).Value
            Ador.MoveNext
        Next 'hasta 41
        arCaptionsInforme(3) = Ador(0).Value '46    'txtAnyo
        Ador.MoveNext
        arCaptionsInforme(19) = Ador(0).Value  '47 Cesta   'txtCesta
        Ador.MoveNext
        arCaptionsInforme(15) = Ador(0).Value  '48 Orden entrega   'txtPed
        Ador.MoveNext
        arCaptionsInforme(10) = Ador(0).Value '50  'txtFecEmision
        Ador.MoveNext
        arCaptionsInforme(8) = Ador(0).Value   'txtEstadoAct
        Ador.MoveNext
        arCaptionsInforme(13) = Ador(0).Value  'txtFecCambEst
        Ador.MoveNext
        arCaptionsInforme(4) = Ador(0).Value   'txtArt
        Ador.MoveNext
        arCaptionsInforme(7) = Ador(0).Value   'txtDest
        Ador.MoveNext
        arCaptionsInforme(21) = Ador(0).Value  'txtUP
        Ador.MoveNext
        arCaptionsInforme(17) = Ador(0).Value  'txtPrecioUP
        Ador.MoveNext
        arCaptionsInforme(5) = Ador(0).Value   'txtCantidad
        sPlanes(2) = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(12) = Ador(0).Value  'txtFecEntrega
        Ador.MoveNext
        arCaptionsInforme(9) = Ador(0).Value   'txtEstado
        Ador.MoveNext
        arCaptionsInforme(11) = Ador(0).Value '60  'txtFecEntProv
        Ador.MoveNext
        arCaptionsInforme(18) = Ador(0).Value   '61    'txtPag
        Ador.MoveNext
        arCaptionsInforme(6) = Ador(0).Value   '62 'txtDe
        Ador.MoveNext
        arCaptionsInforme(14) = Ador(0).Value  '63  Num.Ext    'txtNumExterno
        Label18.caption = Ador(0).Value
        Ador.MoveNext
        For i = 1 To 16
            sEstados(i) = Ador(0).Value
            Ador.MoveNext
        Next
        'txtPrecioUP = Ador(0).Value '80
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sdbcArtiCod.Columns(0).caption = Ador(0).Value
        sdbcArtiDen.Columns(1).caption = Ador(0).Value
        sdbcCM.Columns(0).caption = Ador(0).Value
        sdbcDestCod.Columns(0).caption = Ador(0).Value
        sdbcDestDen.Columns(1).caption = Ador(0).Value
        sdbcProceCod.Columns(0).caption = Ador(0).Value
        sdbcProceDen.Columns(1).caption = Ador(0).Value
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        sdbcUsuCod.Columns(0).caption = Ador(0).Value
        sdbcReceptor.Columns(0).caption = Ador(0).Value
        Me.sdbcGestor.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbcArtiCod.Columns(1).caption = Ador(0).Value
        sdbcArtiDen.Columns(0).caption = Ador(0).Value
        sdbcCM.Columns(1).caption = Ador(0).Value
        sdbcDestCod.Columns(1).caption = Ador(0).Value
        sdbcDestDen.Columns(0).caption = Ador(0).Value
        sdbcEst.Columns(0).caption = Ador(0).Value
        sdbcProceCod.Columns(1).caption = Ador(0).Value
        sdbcProceDen.Columns(0).caption = Ador(0).Value
        sdbcProveCod.Columns(1).caption = Ador(0).Value
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        sdbcUsuCod.Columns(1).caption = Ador(0).Value
        sdbcReceptor.Columns(1).caption = Ador(0).Value
        Me.sdbcGestor.Columns(1).caption = Ador(0).Value
        
        Ador.MoveNext
        arCaptionsInforme(2) = Ador(0).Value   'txtMoneda

        Ador.MoveNext
        arCaptionsInforme(22) = Ador(0).Value  'stxtSolicit
        lblSolicitudTitle.caption = Ador(0).Value & ":"
        Ador.MoveNext
        arCaptionsInforme(23) = Ador(0).Value  'txtImporte
        Ador.MoveNext
        sEspera(1) = Ador(0).Value
        Ador.MoveNext
        sEspera(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(3) = Ador(0).Value '89
        
        Ador.MoveNext
'        lblNumCesta.caption = ador(0).Value & ":" '90 N�m. cesta
        sIdioma(1) = Ador(0).Value
        Ador.MoveNext
        chkPedPM.caption = Ador(0).Value  '91 Pedidos libres
        Ador.MoveNext
        arCaptionsInforme(19) = Ador(0).Value '92 Cesta    'txtCesta
        
        Ador.MoveNext
        sEstados(17) = Ador(0).Value  '93 Emitida
        
        Ador.MoveNext
        lblEmpresa.caption = Ador(0).Value & ":"  'Empresa
        sEmpresa = Ador(0).Value
        arCaptionsInforme(30) = Ador(0).Value & ":" 'txtEmpresa
        
        Ador.MoveNext
        sdbcEmpresa.Columns(1).caption = Ador(0).Value '99 CIF
'        sdbcEmpresaDen.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbcEmpresa.Columns(2).caption = Ador(0).Value '100 Raz�n social
'        sdbcEmpresaDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        lblCodERP.caption = Ador(0).Value   '101 C�digo en ERP:
        Ador.MoveNext
        lblAprov.caption = Ador(0).Value  '102 Aprovisionador
        arCaptionsInforme(31) = Ador(0).Value  'txtIdiAprov
        Ador.MoveNext
        lblReceptor.caption = Ador(0).Value   '103 Receptor
        arCaptionsInforme(28) = Ador(0).Value  'txtIdiReceptor
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        chkIncluir(0).caption = Ador(0).Value
        Ador.MoveNext
        chkIncluir(1).caption = Ador(0).Value
        Ador.MoveNext
        chkIncluir(2).caption = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(32) = Ador(0).Value  'txtObservaciones
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value
        arCaptionsInforme(49) = Ador(0).Value
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value
        arCaptionsInforme(50) = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(1) = Ador(0).Value   'txtPago
        Ador.MoveNext
        txtViaPago = Ador(0).Value
        Ador.MoveNext
        chkIncluir(3).caption = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(33) = Ador(0).Value  'txtTipoPedido
        Ador.MoveNext
        txtCentro = Ador(0).Value
        Ador.MoveNext
        If Not gParametrosGenerales.gbUsarOrgCompras Then
            arCaptionsInforme(34) = Ador(0).Value  'txtCampo3
            Ador.MoveNext
            If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
                arCaptionsInforme(35) = Ador(0).Value  'txtCampo4
            End If
        Else
            Ador.MoveNext
            If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
                arCaptionsInforme(34) = Ador(0).Value   'txtCampo3
            End If
        End If
        Ador.MoveNext
        chkSoloAbono.caption = Ador(0).Value 'S�lo abonos
        Ador.MoveNext
        chkPedErp.caption = Ador(0).Value  ' Pedidos de erp
        Ador.MoveNext
        arCaptionsInforme(38) = Ador(0).Value  'txtDirEnvioFactura
        Ador.MoveNext
        arCaptionsInforme(36) = Ador(0).Value  'txtVisitadoProve
        Ador.MoveNext
        txtProveAdj = Ador(0).Value
       
        Ador.MoveNext
        Me.chkAutoFactura.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkBloqueos.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkFacAlbaran.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkAutom.caption = Ador(0).Value
        Ador.MoveNext
        lblPlanEntregaDesde.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblGestor.caption = Ador(0).Value
        arCaptionsInforme(39) = Ador(0).Value  'txtGestor
        Ador.MoveNext
        sPlanes(0) = Ador(0).Value
        Ador.MoveNext
        sPlanes(3) = Ador(0).Value
        Ador.MoveNext
        Me.chkIncluir(4).caption = Ador(0).Value
        Ador.MoveNext
        Me.lblProAdj.caption = Ador(0).Value
        arCaptionsInforme(37) = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(40) = Ador(0).Value  'txtAutofactura
        Ador.MoveNext
        arCaptionsInforme(41) = Ador(0).Value  'txtImporteBruto
        Ador.MoveNext
        arCaptionsInforme(42) = Ador(0).Value  'txtCostes
        Ador.MoveNext
        arCaptionsInforme(43) = Ador(0).Value  'txtDescuentos
        Ador.MoveNext
        arCaptionsInforme(44) = Ador(0).Value  'txtImporteLinea
        Ador.MoveNext
        arCaptionsInforme(45) = Ador(0).Value  'txtAlmacen
        Ador.MoveNext
        arCaptionsInforme(46) = Ador(0).Value  'txtActivo
        Ador.MoveNext
        arCaptionsInforme(29) = Ador(0).Value  'txtIdiCodERP
        Ador.MoveNext
        arCaptionsInforme(47) = Ador(0).Value  'txtFacTrasRecepAlbaran
        Ador.MoveNext
        lblTipoFacturacion.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkFacPedido.caption = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(48) = Ador(0).Value  'txtFacTrasRecepPedido
        Ador.MoveNext
        Me.chkIncluir(5).caption = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(27) = Ador(0).Value  'txtFacOriginal
        Ador.MoveNext
        arCaptionsInforme(24) = Ador(0).Value  'txtPeriodo
        Ador.MoveNext
        arCaptionsInforme(26) = Ador(0).Value  'txtAl
        Ador.MoveNext
        arCaptionsInforme(25) = Ador(0).Value  'txtAbono
        Ador.MoveNext
        arCaptionsInforme(52) = Ador(0).Value & ":"    'Costes y descuentos
        Ador.MoveNext
        arCaptionsInforme(53) = Ador(0).Value  'por cantidad
        Ador.MoveNext
        arCaptionsInforme(54) = Ador(0).Value  'por importe
        Ador.MoveNext
        For i = 18 To 21
            sEstados(i) = Ador(0).Value
            Ador.MoveNext
        Next
        arCaptionsInforme(55) = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(56) = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(57) = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(58) = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(59) = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(60) = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(61) = Ador(0).Value
        Ador.MoveNext
        chkVerPedBajaLogica.caption = Ador(0).Value
        Ador.MoveNext
        sEstados(22) = Ador(0).Value
        Ador.MoveNext
        sEstadosInt(0) = Ador(0).Value
        Ador.MoveNext
        sEstadosInt(1) = Ador(0).Value
        sEstadosInt(2) = ""
        Ador.MoveNext
        sEstadosInt(3) = Ador(0).Value
        Ador.MoveNext
        sEstadosInt(4) = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(63) = Ador(0).Value
        Ador.MoveNext
        arCaptionsInforme(64) = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
    
    Label17.caption = basParametros.gParametrosGenerales.gsNomCodPersonalizPedido & ":"
End Sub

''' <summary>
''' Solo puede estas marcado uno de entre "Facturaci�n por albar�n" y "Facturaci�n por pedido"
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub chkFacAlbaran_Click()
    If chkFacAlbaran.Value = vbChecked Then
        chkFacPedido.Value = vbUnchecked
    End If
End Sub

''' <summary>
''' Solo puede estas marcado uno de entre "Facturaci�n por albar�n" y "Facturaci�n por pedido"
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub chkFacPedido_Click()
    If chkFacPedido.Value = vbChecked Then
        chkFacAlbaran.Value = vbUnchecked
    End If
End Sub

''' <summary>
''' Marcar/desmascar si es pedido negociaddo.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub chkPedDirec_Click()
    If chkPedDirec.Value = vbChecked And sdbcEst.Text <> "" Then
        If sdbcEst.Text = sIdiEst(1) Or sdbcEst.Text = sIdiEst(2) Or sdbcEst.Text = sIdiEst(10) Then
            sdbcEst.Text = ""
        End If
    End If
End Sub

Private Sub cmdBorraCat_Click()
    lblCategoria = ""
    ACN1Seleccionada = 0
    ACN2Seleccionada = 0
    ACN3Seleccionada = 0
    ACN4Seleccionada = 0
    ACN5Seleccionada = 0
    
    'Oculta los campos personalizados:
    lblDato1.Visible = False
    lblDato2.Visible = False
    txtDato1.Visible = False
    txtDato2.Visible = False
    iCampo1 = 0
    iCampo2 = 0
    txtDato1.Text = ""
    txtDato2.Text = ""
End Sub

Private Sub cmdBorraMat_Click()
       lblMaterial = ""
       sGMN1Cod = ""
       sGMN2Cod = ""
       sGMN3Cod = ""
       sGMN4Cod = ""
       Set oGMN1Seleccionado = Nothing
       Set oGrupoMN4seleccionado = Nothing
       sdbcArtiCod.Text = ""
       sdbcArtiDen.Text = ""
       sdbcDestCod.Text = ""
       sdbcDestDen.Text = ""
       sdbcDestCod.RemoveAll
       sdbcDestDen.RemoveAll
       sdbcArtiCod.RemoveAll
       sdbcArtiDen.RemoveAll
End Sub


''' <summary>
''' Muestra las opciones avanzadas. Visualiza los demas criterios de busqueda.
''' </summary>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub cmdAvanzado_Click()
    
    If Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS Then Me.frmSegui.Height = 855
    
    Frame2.Visible = True
    FrameProve.Visible = True
    Frame4.Visible = True
    fraProce.Visible = True
    frameEmpresa.Visible = True
    FrameImp.Visible = gParametrosGenerales.gsAccesoFSSM = AccesoFSSM
    
    FrameFactura.Visible = gParametrosGenerales.gsAccesoFSIM = AccesoFSIM

    Line5.Visible = True
    cmdAvanzado.Visible = False
        
    If FSEPConf Then
        Frame3.Visible = False
        Frame4.Top = Frame3.Top
        FrameCateg.Visible = True
        frmSegui.Top = IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 220, 120)
        Line1.Visible = True
        FrameTipoPedido.Top = 1440 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
        Line2.Visible = True
        FrameProve.Top = 2250 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
        fraProce.Top = 8160 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
        frameEmpresa.Top = 3600 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
        FrameCateg.Top = 4530 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
        FrameImp.Top = 4950 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
        FrameFactura.Top = 6060 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
        Line3.Y1 = 3900
        Line3.Y2 = Line3.Y1
        Line3.Visible = False
    Else
        Frame3.Visible = True
        If m_bPedAprov And gParametrosGenerales.gbPedidosAprov Then
            FrameCateg.Visible = True
            frmSegui.Top = IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 220, 120)
            Line1.Visible = True
            FrameTipoPedido.Top = 1440 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            Line2.Visible = True
            FrameProve.Top = 2250 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            fraProce.Top = 8160 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            frameEmpresa.Top = 3600 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            FrameCateg.Top = 4530 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            FrameImp.Top = 4950 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            FrameFactura.Top = 6060 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            Line3.Visible = True
        Else
            FrameCateg.Visible = False
            frmSegui.Top = IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 220, 120)

            Line1.Visible = True
            FrameTipoPedido.Top = 1440 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            Line2.Visible = True
            FrameProve.Top = 2250 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            fraProce.Top = 8160 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            frameEmpresa.Top = 3600 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            FrameCateg.Top = 4530 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            FrameImp.Top = 4950 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            FrameFactura.Top = 6060 - IIf(Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS, 300, 0)
            Line3.Visible = True
        End If
    End If
        
    If FrameCateg.Visible = False Then
        If FrameImp.Visible And FrameFactura.Visible Then
            FrameImp.Top = FrameCateg.Top
            FrameFactura.Top = FrameImp.Top + FrameImp.Height + 30
            fraProce.Top = FrameFactura.Top + FrameFactura.Height + 30
        ElseIf (Not FrameImp.Visible) And FrameFactura.Visible Then
            FrameFactura.Top = FrameCateg.Top
            fraProce.Top = FrameFactura.Top + FrameFactura.Height + 30
        ElseIf FrameImp.Visible And (Not FrameFactura.Visible) Then
            FrameImp.Top = FrameCateg.Top
            fraProce.Top = FrameImp.Top + FrameImp.Height + 30
        Else
            fraProce.Top = frameEmpresa.Top + frameEmpresa.Height + 30
        End If
    ElseIf (FrameImp.Visible) And (Not FrameFactura.Visible) Then
        fraProce.Top = FrameFactura.Top
    ElseIf (Not FrameImp.Visible) And (FrameFactura.Visible) Then
        FrameFactura.Top = FrameImp.Top
        fraProce.Top = FrameFactura.Top + FrameFactura.Height + 30
    ElseIf (Not FrameImp.Visible) And (Not FrameFactura.Visible) Then
        fraProce.Top = FrameImp.Top
    End If
    Me.cmdObtener.Top = fraProce.Top + fraProce.Height + 30
End Sub


Private Sub cmdBorraSolicitud_Click()
    lblSolicitud.caption = ""
    m_lSolicit = 0
End Sub


''' <summary>
''' Realiza la llamada al buscador de Centros de Coste, cuando se pulsa el boton en el filtro.
''' </summary>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub cmdBuscarCentroCoste_Click()
Dim sCod As String

    Set g_oCenCos = oFSGSRaiz.Generar_CCentroCoste

    frmSelCenCoste.g_sOrigen = "frmLstPedidos"
    frmSelCenCoste.g_bCentrosSM = False
    frmSelCenCoste.g_lIdEmpresa = m_lEmpresa
    frmSelCenCoste.g_bSaltarComprobacionArbol = True
    Set frmSelCenCoste.g_oOrigen = Me
    frmSelCenCoste.Show 1

    m_bRespetar = True


    If Not g_oCenCos Is Nothing Then
        If Not NoHayParametro(g_oCenCos.UON4) Then
            sCod = g_oCenCos.UON4
        ElseIf Not NoHayParametro(g_oCenCos.UON3) Then
            sCod = g_oCenCos.UON3
        ElseIf Not NoHayParametro(g_oCenCos.UON2) Then
            sCod = g_oCenCos.UON2
        ElseIf Not NoHayParametro(g_oCenCos.UON1) Then
            sCod = g_oCenCos.UON1
        End If
        If sCod <> "" Then
            If g_oCenCos.CodConcat <> "" Then
                txtCentroCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & "(" & g_oCenCos.CodConcat & ")"
            Else
                txtCentroCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
            End If
            m_sUON_CC = NullToStr(g_oCenCos.UON1) & "-" & NullToStr(g_oCenCos.UON2) & "-" & NullToStr(g_oCenCos.UON3) & "-" & NullToStr(g_oCenCos.UON4)
            Set m_oCentroCosteSeleccionadoFiltro = g_oCenCos
        End If
        
    Else
        m_sUON_CC = ""
        txtCentroCoste.Text = ""
        Set m_oCentroCosteSeleccionadoFiltro = Nothing
    End If

    If txtPartida.Text <> "" Then
        m_bControlPartidaFiltro = True
        txtPartida_Validate False
    End If

    Set g_oCenCos = Nothing
End Sub


Private Sub cmdBuscarPartida_Click()
Dim sPres0 As String

    sPres0 = cmdBuscarPartida.Tag
    
    
    Set frmSelCContables.g_oPartida = Nothing
    Set frmSelCContables.g_oCenCos = Nothing
    frmSelCContables.g_sPres0 = sPres0
    frmSelCContables.g_iNivel = g_oParametrosSM.Item(sPres0).impnivel
    
    If Not m_oCentroCosteSeleccionadoFiltro Is Nothing Then
        If txtCentroCoste.Text <> "" Then
            Set frmSelCContables.g_oCenCos = m_oCentroCosteSeleccionadoFiltro
        End If
    End If
    
    Set frmSelCContables.g_oOrigen = Me
    frmSelCContables.g_sOrigen = "frmLstPedidos"
    frmSelCContables.Show vbModal

    If Not m_oCentroCosteSeleccionadoFiltro Is Nothing Then
        If m_oCentroCosteSeleccionadoFiltro.UON1 <> "" And m_oCentroCosteSeleccionadoFiltro.Cod <> "" Then
            
            m_sUON_CC = m_oCentroCosteSeleccionadoFiltro.UON1 & "-" & m_oCentroCosteSeleccionadoFiltro.UON2 & "-" & m_oCentroCosteSeleccionadoFiltro.UON3 & "-" & m_oCentroCosteSeleccionadoFiltro.UON4
            
            If m_oCentroCosteSeleccionadoFiltro.UON4 <> "" Then
                m_oCentroCosteSeleccionadoFiltro.CodConcat = m_oCentroCosteSeleccionadoFiltro.UON1 & "-" & m_oCentroCosteSeleccionadoFiltro.UON2 & "-" & m_oCentroCosteSeleccionadoFiltro.UON3
            ElseIf m_oCentroCosteSeleccionadoFiltro.UON3 <> "" Then
                m_oCentroCosteSeleccionadoFiltro.CodConcat = m_oCentroCosteSeleccionadoFiltro.UON1 & "-" & m_oCentroCosteSeleccionadoFiltro.UON2
            ElseIf m_oCentroCosteSeleccionadoFiltro.UON2 <> "" Then
                m_oCentroCosteSeleccionadoFiltro.CodConcat = m_oCentroCosteSeleccionadoFiltro.UON1
            End If
            m_bControlCC = False
            If m_oCentroCosteSeleccionadoFiltro.CodConcat <> "" Then
                txtCentroCoste.Text = m_oCentroCosteSeleccionadoFiltro.Cod & " - " & m_oCentroCosteSeleccionadoFiltro.Den & "(" & m_oCentroCosteSeleccionadoFiltro.CodConcat & ")"
            Else
                txtCentroCoste.Text = m_oCentroCosteSeleccionadoFiltro.Cod & " - " & m_oCentroCosteSeleccionadoFiltro.Den
            End If
            m_bRespetar = True
        End If
        Set frmSelCContables.g_oCenCos = Nothing
    End If
    If Not oContratoSeleccionado Is Nothing Then
        If oContratoSeleccionado.Cod <> "" Then
            txtPartida.Text = oContratoSeleccionado.Cod & " - " & oContratoSeleccionado.Den
            m_bControlPartidaFiltro = True
            txtPartida_Validate True
        End If
    End If
End Sub

''' <summary>
''' Bot�n para mostrar el formulario de b�squeda
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdBuscarProAdj_Click()
    frmPROVEBuscar.sOrigen = "lstSeguimientoProAdj"
    frmPROVEBuscar.bRMat = m_bProvMat
    frmPROVEBuscar.Show 1
End Sub


Private Sub cmdBuscarSolicitud_Click()
    frmSolicitudBuscar.g_sOrigen = "frmLstSeguimiento"
    frmSolicitudBuscar.Show 1
End Sub
Public Sub PonerSolicitudSeleccionada(ByVal oSolic As CInstancia)
    lblSolicitud.caption = oSolic.Id & " " & oSolic.DescrBreve
    m_lSolicit = oSolic.Id
    
    Set oSolic = Nothing
End Sub

''' <summary>
''' Bot�n para mostrar el formulario de b�squeda
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdBuscarProve_Click()
    frmPROVEBuscar.sOrigen = "frmLstPedidos"
    frmPROVEBuscar.bRMat = m_bProvMat
    
    frmPROVEBuscar.Show 1
    
End Sub

Private Sub cmdBuscarProce_Click()
   
        frmPROCEBuscar.bRDest = False
        frmPROCEBuscar.m_bProveAsigComp = False
        frmPROCEBuscar.m_bProveAsigEqp = False
        
        frmPROCEBuscar.bRMat = m_bRMat
        frmPROCEBuscar.bRAsig = m_bRAsig
        frmPROCEBuscar.bRCompResponsable = m_bRCompResp
        frmPROCEBuscar.bREqpAsig = m_bREqpAsig
        frmPROCEBuscar.bRUsuAper = m_bRUsuAper
        frmPROCEBuscar.bRUsuUON = m_bRUsuUON
        frmPROCEBuscar.bRPerfUON = m_bRUsuPerfUON
        frmPROCEBuscar.bRUsuDep = m_bRUsuDep
        frmPROCEBuscar.bRespetarComboGMN2 = True
        frmPROCEBuscar.bRespetarComboGMN3 = True
        frmPROCEBuscar.bRespetarComboGMN4 = True
        frmPROCEBuscar.bRespetarComboArt = True
        frmPROCEBuscar.sOrigen = "frmLstPedidos"
        frmPROCEBuscar.sdbcAnyo.Text = sdbcAnyo2.Text
        frmPROCEBuscar.sdbcGMN1Proce_Cod.Text = sdbcCM
        frmPROCEBuscar.sdbcGMN1Proce_Cod_Validate False
        
        frmPROCEBuscar.sdbcGMN1_4Cod.Text = sGMN1Cod 'sdbcCM
        frmPROCEBuscar.sdbcGMN1_4Cod_Validate False
        frmPROCEBuscar.sdbcGMN2_4Cod.Text = sGMN2Cod
        frmPROCEBuscar.sdbcGMN2_4Cod_Validate False
        frmPROCEBuscar.sdbcGMN3_4Cod.Text = sGMN3Cod
        frmPROCEBuscar.sdbcGMN3_4Cod_Validate False
        frmPROCEBuscar.sdbcGMN4_4Cod.Text = sGMN4Cod
        frmPROCEBuscar.sdbcGMN4_4Cod_Validate False
        If sdbcArtiCod.Text <> "" Then
           frmPROCEBuscar.sdbcCodArticulo.Text = sdbcArtiCod.Text
           frmPROCEBuscar.txtDenArticulo.Text = sdbcArtiDen.Text
        End If
    
        frmPROCEBuscar.Show 1
        
    
End Sub

''' <summary>
''' Bot�n para mostrar el formulario de b�squeda
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdCatego_Click()
    Dim m_oCampos As CCampos
    Dim oCat1 As CCategoriaN1
    Dim oCat2 As CCategoriaN2
    Dim oCat3 As CCategoriaN3
    Dim oCat4 As CCategoriaN4
    Dim oCat5 As CCategoriaN5
    Dim bOcultarCampos As Boolean
    
    'Llama al formulario que muestra las categor�as:
    frmSELCat.sOrigen = "frmLstPedidos"
    frmSELCat.bRUsuAprov = m_bRUsuAprov
    frmSELCat.bProvMat = m_bProvMat
    frmSELCat.Show 1
    
    
    'Comprueba si el material seleccionado tiene campos personalizados y en este caso los muestra:
    bOcultarCampos = False
    
    If ACN5Seleccionada <> 0 Then
        Set oCat5 = oFSGSRaiz.Generar_CCategoriaN5
        oCat5.Id = ACN5Seleccionada
        Set oCat5.CamposPersonalizados = oFSGSRaiz.Generar_CCampos
        Set m_oCampos = oCat5.CamposPersonalizados.CargarDatos(oCat5.Id, 5)
        
    ElseIf ACN4Seleccionada <> 0 Then
        Set oCat4 = oFSGSRaiz.Generar_CCategoriaN4
        oCat4.Id = ACN4Seleccionada
        Set oCat4.CamposPersonalizados = oFSGSRaiz.Generar_CCampos
        Set m_oCampos = oCat4.CamposPersonalizados.CargarDatos(oCat4.Id, 4)
        
    ElseIf ACN3Seleccionada <> 0 Then
        Set oCat3 = oFSGSRaiz.Generar_CCategoriaN3
        oCat3.Id = ACN3Seleccionada
        Set oCat3.CamposPersonalizados = oFSGSRaiz.Generar_CCampos
        Set m_oCampos = oCat3.CamposPersonalizados.CargarDatos(oCat3.Id, 3)
        
    ElseIf ACN2Seleccionada <> 0 Then
        Set oCat2 = oFSGSRaiz.Generar_CCategoriaN2
        oCat2.Id = ACN2Seleccionada
        Set oCat2.CamposPersonalizados = oFSGSRaiz.Generar_CCampos
        Set m_oCampos = oCat2.CamposPersonalizados.CargarDatos(oCat2.Id, 2)
        
    ElseIf ACN1Seleccionada <> 0 Then
        Set oCat1 = oFSGSRaiz.Generar_CCategoriaN1
        oCat1.Id = ACN1Seleccionada
        Set oCat1.CamposPersonalizados = oFSGSRaiz.Generar_CCampos
        Set m_oCampos = oCat1.CamposPersonalizados.CargarDatos(oCat1.Id, 1)
    End If

    If Not m_oCampos Is Nothing Then
        If m_oCampos.Count = 0 Then
            bOcultarCampos = True
        Else
            bOcultarCampos = False
        End If
    Else
        bOcultarCampos = True
    End If
    
    txtDato1.Text = ""
    txtDato2.Text = ""
    If bOcultarCampos = True Then
        lblDato1.Visible = False
        lblDato2.Visible = False
        txtDato1.Visible = False
        txtDato2.Visible = False
        iCampo1 = 0
        iCampo2 = 0
    Else
        lblDato1.caption = m_oCampos.Item(basPublic.gParametrosInstalacion.gIdioma & "1").Den & ":"
        lblDato1.ToolTipText = m_oCampos.Item(basPublic.gParametrosInstalacion.gIdioma & "1").Den
        lblDato2.caption = m_oCampos.Item(basPublic.gParametrosInstalacion.gIdioma & "2").Den & ":"
        lblDato2.ToolTipText = m_oCampos.Item(basPublic.gParametrosInstalacion.gIdioma & "2").Den
        lblDato1.Visible = True
        lblDato2.Visible = True
        txtDato1.Visible = True
        txtDato2.Visible = True
        iCampo1 = m_oCampos.Item(basPublic.gParametrosInstalacion.gIdioma & "1").Id
        iCampo2 = m_oCampos.Item(basPublic.gParametrosInstalacion.gIdioma & "2").Id
    End If
    
    Set oCat1 = Nothing
    Set oCat2 = Nothing
    Set oCat3 = Nothing
    Set oCat4 = Nothing
    Set oCat5 = Nothing
    Set m_oCampos = Nothing
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdFecEmiD_Click()
    AbrirFormCalendar Me, txtFecEmiD
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdFecEmiH_Click()
    AbrirFormCalendar Me, txtFecEmiH
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdFecEntD_Click()
    AbrirFormCalendar Me, txtFecEntD
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdFecEntH_Click()
    AbrirFormCalendar Me, txtFecEntH
End Sub

''' <summary>
''' Comprueba q los filtros proporcionados sean correctos
''' </summary>
''' <remarks>Llamada desde: cmdObtener_Click ; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 04/04/2012</revision>

Private Function ComprobarFiltro() As Boolean
   
    m_iAnyoPed = 0
    If sdbcAnyo.Text <> "" Then
        If Not IsNumeric(sdbcAnyo.Text) Then
            oMensajes.NoValido 18
            If Me.Visible Then sdbcAnyo.SetFocus
            ComprobarFiltro = False
            Exit Function
        Else
            m_iAnyoPed = CInt(sdbcAnyo.Text)
        End If
    End If
    
    m_lNumCesta = 0
    If Trim(txtNumCesta.Text) <> "" Then
        If Not IsNumeric(txtNumCesta.Text) Then
            oMensajes.NoValido sIdioma(1)
            If Me.Visible Then txtNumCesta.SetFocus
            ComprobarFiltro = False
            Exit Function
        Else
            m_lNumCesta = CLng(txtNumCesta.Text)
        End If
    End If
    
    m_lNumPedido = 0
    If Trim(txtPedido.Text) <> "" Then
        If Not IsNumeric(txtPedido.Text) Then
            oMensajes.NoValido sIdioma(2)
            If Me.Visible Then txtPedido.SetFocus
            ComprobarFiltro = False
            Exit Function
        Else
            m_lNumPedido = CLng(txtPedido.Text)
        End If
    End If
    
    If txtFecEmiD.Text <> "" Then
        If Not IsDate(txtFecEmiD.Text) Then
            oMensajes.NoValida Left(label2.caption, Len(label2.caption) - 1)
            If Me.Visible Then txtFecEmiD.SetFocus
            ComprobarFiltro = False
            Exit Function
        End If
    End If
    
    If txtFecEmiH.Text <> "" Then
        If Not IsDate(txtFecEmiH.Text) Then
            oMensajes.NoValida " " & sIdioma(4) & " " & Left(Label13.caption, Len(Label13.caption) - 1)
            If Me.Visible Then txtFecEmiH.SetFocus
            ComprobarFiltro = False
            Exit Function
        End If
        If txtFecEmiD.Text <> "" Then
            If CDate(txtFecEmiD.Text) > CDate(txtFecEmiH.Text) Then
                oMensajes.FechaDesdeMayorFechaHasta
                If Me.Visible Then txtFecEmiD.SetFocus
                ComprobarFiltro = False
                Exit Function
            End If
        End If
    End If
    
    If txtFecEntD.Text <> "" Then
        If Not IsDate(txtFecEntD.Text) Then
            oMensajes.NoValida Left(Label14.caption, Len(Label14.caption) - 1)
            If Me.Visible Then txtFecEntD.SetFocus
            ComprobarFiltro = False
            Exit Function
        End If
    End If
    
    If txtFecEntH.Text <> "" Then
        If Not IsDate(txtFecEntH.Text) Then
            oMensajes.NoValida " " & sIdioma(4) & " " & Left(Label15.caption, Len(Label15.caption) - 1)
            If Me.Visible Then txtFecEntH.SetFocus
            ComprobarFiltro = False
            Exit Function
        End If
        If txtFecEntD.Text <> "" Then
            If CDate(txtFecEntD.Text) > CDate(txtFecEntH.Text) Then
                oMensajes.FechaDesdeMayorFechaHasta
                If Me.Visible Then txtFecEntD.SetFocus
                ComprobarFiltro = False
                Exit Function
            End If
        End If
    End If
    
    If Trim(sdbcTipoPedido.Text) <> "" Then
        m_lTipoPedido = sdbcTipoPedido.Columns("ID").Value
    End If
    
      
    m_iNumProceCod = 0
    If sdbcProceCod.Text <> "" Then
        If Not IsNumeric(sdbcProceCod.Text) Then
            oMensajes.NoValido 44
            If Me.Visible Then sdbcProceCod.SetFocus
            ComprobarFiltro = False
            Exit Function
        Else
            m_iNumProceCod = CLng(sdbcProceCod.Text)
        End If
    End If
    
    m_iNumAnyo2 = 0
    If sdbcAnyo2.Text <> "" Then
        If Not IsNumeric(sdbcAnyo2.Text) Then
            oMensajes.NoValido sIdioma(7)
            If Me.Visible Then sdbcAnyo2.SetFocus
            ComprobarFiltro = False
            Exit Function
        Else
            m_iNumAnyo2 = CInt(sdbcAnyo2.Text)
        End If
    End If
    
    If m_bREmpresa = True And Trim(sdbcEmpresa.Text) = "" Then
        oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        If oEmpresas.Count = 0 Then
            oMensajes.UnidadUOSinEmpresa
        Else
            oMensajes.SeleccionarEmpresa
        End If
        If Me.Visible Then sdbcEmpresa.SetFocus
        ComprobarFiltro = False
        Exit Function
    End If
    
    m_sCodPer = ""
    If sdbcUsuCod.Text <> "" Then
        m_sCodPer = sdbcUsuCod.Columns(0).Value
    End If
    
    m_sCodReceptor = ""
    If sdbcReceptor.Text <> "" Then
        m_sCodReceptor = sdbcReceptor.Columns(0).Value
    End If
    
    If IsEmpty(vNumEst) Then
       vNumEst = 30
    End If

    If gParametrosGenerales.gsAccesoFSIM = AccesoFSIM Then
        If Me.txtFecDesdeFactura.Text <> "" Then
            arfiltrosFac(1) = txtFecDesdeFactura.Text
        End If
        If Me.txtFecHastaFactura.Text <> "" Then
            arfiltrosFac(2) = txtFecHastaFactura.Text
        End If
        If Me.sdbcAnyoFacturas.Value <> "" Then
            arfiltrosFac(3) = sdbcAnyoFacturas.Value
        End If
        If Me.txtNumFactura.Text <> "" Then
            arfiltrosFac(4) = txtNumFactura.Text
        End If
        If Me.sdbcEstadoFactura.Value <> "" Then
            arfiltrosFac(5) = sdbcEstadoFactura.Value
        End If
    
        If Me.txtFecDesdePago.Text <> "" Then
            arfiltrosPago(1) = txtFecDesdePago.Text
        End If
        If Me.txtFecHastaPago.Text <> "" Then
            arfiltrosPago(2) = txtFecHastaPago.Text
        End If
        If Me.txtNumPago.Text <> "" Then
            arfiltrosPago(3) = txtNumPago.Text
        End If
        If Me.sdbcEstadoPago.Value <> "" Then
            arfiltrosPago(4) = sdbcEstadoPago.Value
        End If
    End If
    Dim i As Integer
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        If Not g_oParametrosSM Is Nothing Then
            ReDim arFiltrosImp(UBound(m_arrPartidas))
            For i = 0 To UBound(m_arrPartidas) - 1
                arFiltrosImp(i) = m_arrPartidas(i)
            Next i
        End If
    End If
    
    m_sCodGestor = ""
    If sdbcGestor.Text <> "" Then
        m_sCodGestor = sdbcGestor.Columns(0).Value
    End If
    
    If Me.txtFecDesdePlanEntrega.Text <> "" Then
        If Not IsDate(txtFecDesdePlanEntrega.Text) Then
            oMensajes.NoValida Left(Me.lblPlanEntregaDesde.caption, Len(lblPlanEntregaDesde.caption) - 1)
            If Me.Visible Then txtFecDesdePlanEntrega.SetFocus
            ComprobarFiltro = False
            Exit Function
        End If
    End If
    
    If Me.txtFecHastaPlanEntrega.Text <> "" Then
        If Not IsDate(txtFecHastaPlanEntrega.Text) Then
            oMensajes.NoValida " " & sIdioma(6) & " " & Left(lblPlanEntregaHasta.caption, Len(lblPlanEntregaHasta.caption) - 1)
            If Me.Visible Then txtFecHastaPlanEntrega.SetFocus
            ComprobarFiltro = False
            Exit Function
        End If
        If txtFecDesdePlanEntrega.Text <> "" Then
            If CDate(txtFecDesdePlanEntrega.Text) > CDate(txtFecHastaPlanEntrega.Text) Then
                oMensajes.FechaDesdeMayorFechaHasta
                If Me.Visible Then txtFecDesdePlanEntrega.SetFocus
                ComprobarFiltro = False
                Exit Function
            End If
        End If
    End If

    ComprobarFiltro = True
End Function

''' <summary>
''' Limpia el contenido del Centro de Coste
''' </summary>
''' <remarks>Tiempo m�ximo= 0seg.</remarks>
Private Sub cmdLimpiarCentroCoste_Click()
    txtCentroCoste.Text = ""
    lstCentroCoste.Visible = False
    m_bRespetar = True
    m_sUON_CC = ""
    Set m_oCentroCosteSeleccionadoFiltro = Nothing
End Sub


''' <summary>
''' Limpia el contenido de la partida
''' </summary>
''' <remarks>Tiempo m�ximo= 0seg.</remarks>
Private Sub cmdLimpiarPartida_Click()
    txtPartida.Text = ""
    m_arrPartidas(0) = ""
End Sub

''' <summary>Obtener listado de �rdenes de entrega</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
''' <revision>JVS 07/09/2011</revision>

Public Sub cmdObtener_Click()
    Dim arFiltrosGestorPlanesFacturas As Variant
    Dim rsCDCab As Ador.Recordset
    Dim rsCDLin As Ador.Recordset
    Dim sCodComp As String
    Dim sCodEqp As String
    Dim sPerSoli As String
    Dim oParametroSM As CParametroSM
    Dim sPres0 As String
    Dim oGmn As EstrucGMN
    Dim lIdPerfil As Long
    Dim bAbrirInforme As Boolean
    Dim RepPath As String
    Dim oCRPedidos As CRPedidos
    Dim oFos As FileSystemObject
    Dim arFiltrosPedido(5) As Variant
    
    If Not ComprobarFiltro Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    sSeleccion = GenerarTextoSeleccion
    
    arFiltrosPedido(0) = m_iAnyoPed
    arFiltrosPedido(1) = m_lNumCesta
    arFiltrosPedido(2) = m_lNumPedido
    arFiltrosPedido(3) = Trim(txtNumExt.Text)
    arFiltrosPedido(4) = Trim(txtFecEmiD.Text)
    arFiltrosPedido(5) = Trim(txtFecEmiH.Text)
    
    ' PARAMETROS REPORT, llamada STORED PROCEDURE
    sCodComp = ""
    sCodEqp = ""
    If m_bRMat Then
        sCodComp = oUsuarioSummit.comprador.Cod
        sCodEqp = oUsuarioSummit.comprador.codEqp
    End If
    If m_bRUsuAprov Then
        m_sCodPer = basOptimizacion.gCodPersonaUsuario
    End If
    sPerSoli = ""
    If m_bVerSolic Then
        sPerSoli = basOptimizacion.gCodPersonaUsuario
    End If

    ' PARAMETROS REPORT, llamada STORED PROCEDURE
    'Array con el gestor, planes de entrega desde-hasta, pedido de autofactura-con plan autom-con bloqueos-conn facturacion tras recep
    ReDim arFiltrosGestorPlanesFacturas(12)
    arFiltrosGestorPlanesFacturas(0) = m_sCodGestor
    arFiltrosGestorPlanesFacturas(1) = chkAutoFactura.Value
    arFiltrosGestorPlanesFacturas(2) = chkAutom.Value
    arFiltrosGestorPlanesFacturas(3) = chkBloqueos.Value
    arFiltrosGestorPlanesFacturas(4) = chkFacAlbaran.Value
    arFiltrosGestorPlanesFacturas(5) = txtFecDesdePlanEntrega.Text
    arFiltrosGestorPlanesFacturas(6) = txtFecHastaPlanEntrega.Text
    arFiltrosGestorPlanesFacturas(7) = sdbcProAdjCod.Text
    arFiltrosGestorPlanesFacturas(8) = chkFacPedido.Value
    If (oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador) Then arFiltrosGestorPlanesFacturas(9) = oUsuarioSummit.Persona.Cod
    arFiltrosGestorPlanesFacturas(10) = oUsuarioSummit.Cod
    arFiltrosGestorPlanesFacturas(11) = oUsuarioSummit.VerPedidosCCImputables
    arFiltrosGestorPlanesFacturas(12) = (oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador)
    
    oGmn.GMN1 = sGMN1Cod
    oGmn.GMN2 = sGMN2Cod
    oGmn.GMN3 = sGMN3Cod
    oGmn.GMN4 = sGMN4Cod
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        If Not g_oParametrosSM Is Nothing Then
                        
            Set g_ador = oGestorInformes.ListadoPedidos(arFiltrosPedido, Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), oGmn, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, , , Trim(txtReferencia.Text), chkPedPM.Value, iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), basPublic.gParametrosInstalacion.gIdioma, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), _
                        m_sCodReceptor, Trim(txtCodERP.Text), sdbcCM.Value, , , m_lTipoPedido, arfiltrosFac, arfiltrosPago, arFiltrosImp, chkPedErp.Value, chkSoloAbono.Value, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, g_bPedidosAbiertos, VerPedBajaLogica:=chkVerPedBajaLogica.Value)

        Else
            Set g_ador = oGestorInformes.ListadoPedidos(arFiltrosPedido, Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), oGmn, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, , , Trim(txtReferencia.Text), chkPedPM.Value, iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), basPublic.gParametrosInstalacion.gIdioma, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), _
                    m_sCodReceptor, Trim(txtCodERP.Text), sdbcCM.Value, , , m_lTipoPedido, arfiltrosFac, arfiltrosPago, , chkPedErp.Value, chkSoloAbono.Value, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, g_bPedidosAbiertos, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
        End If
    Else
        Set g_ador = oGestorInformes.ListadoPedidos(arFiltrosPedido, Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), oGmn, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, , , Trim(txtReferencia.Text), chkPedPM.Value, iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), basPublic.gParametrosInstalacion.gIdioma, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), _
                m_sCodReceptor, Trim(txtCodERP.Text), sdbcCM.Value, , , m_lTipoPedido, arfiltrosFac, arfiltrosPago, , chkPedErp.Value, chkSoloAbono.Value, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, g_bPedidosAbiertos, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
    End If
    
    If g_ador Is Nothing Then
        Screen.MousePointer = vbNormal
        oMensajes.NoHayDatos
        Exit Sub
    End If
    
    'Atributos de la orden
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        If Not g_oParametrosSM Is Nothing Then
            Set g_ador5 = oGestorInformes.ListadoPedidos(arFiltrosPedido, Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), oGmn, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, True, 8, Trim(txtReferencia.Text), chkPedPM.Value, iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), basPublic.gParametrosInstalacion.gIdioma, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), _
                    m_sCodReceptor, Trim(txtCodERP.Text), sdbcCM.Value, , , m_lTipoPedido, arfiltrosFac, arfiltrosPago, arFiltrosImp, chkPedErp.Value, chkSoloAbono.Value, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, g_bPedidosAbiertos, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
        Else
            Set g_ador5 = oGestorInformes.ListadoPedidos(arFiltrosPedido, Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), oGmn, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, True, 8, Trim(txtReferencia.Text), chkPedPM.Value, iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), basPublic.gParametrosInstalacion.gIdioma, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), _
                    m_sCodReceptor, Trim(txtCodERP.Text), sdbcCM.Value, , , m_lTipoPedido, arfiltrosFac, arfiltrosPago, , chkPedErp.Value, chkSoloAbono.Value, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, g_bPedidosAbiertos, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
        End If
    Else
        Set g_ador5 = oGestorInformes.ListadoPedidos(arFiltrosPedido, Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), oGmn, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, True, 8, Trim(txtReferencia.Text), chkPedPM.Value, iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), basPublic.gParametrosInstalacion.gIdioma, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), _
                    m_sCodReceptor, Trim(txtCodERP.Text), sdbcCM.Value, , , m_lTipoPedido, arfiltrosFac, arfiltrosPago, , chkPedErp.Value, chkSoloAbono.Value, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, g_bPedidosAbiertos, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
    End If
    
    'Imputaciones
    If chkIncluir(3).Value = vbChecked Then
        If Not g_oParametrosSM Is Nothing Then
            For Each oParametroSM In g_oParametrosSM
                If oParametroSM.ImpModo = TipoModoImputacionSM.NivelCabecera Then
                    sPres0 = oParametroSM.PRES5
                    Exit For
                End If
           Next
        End If
        If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
            If Not g_oParametrosSM Is Nothing Then
                Set g_ador7 = oGestorInformes.ListadoPedidosImputaciones(m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), False, sPres0, chkPedErp.Value, chkSoloAbono.Value, m_lTipoPedido, arfiltrosFac, arfiltrosPago _
                , arFiltrosImp, chkPedPM.Value, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
            Else
                Set g_ador7 = oGestorInformes.ListadoPedidosImputaciones(m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), False, sPres0, chkPedErp.Value, chkSoloAbono.Value, m_lTipoPedido, arfiltrosFac, arfiltrosPago _
                , , chkPedPM.Value, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
            End If
        Else
            Set g_ador7 = oGestorInformes.ListadoPedidosImputaciones(m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), False, sPres0, chkPedErp.Value, chkSoloAbono.Value, m_lTipoPedido, arfiltrosFac, arfiltrosPago _
            , , chkPedPM.Value, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
        End If
        
        If sPres0 <> "" Then
            If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
                If Not g_oParametrosSM Is Nothing Then
                    Set g_ador8 = oGestorInformes.ListadoPedidosImputaciones(m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), True, sPres0, chkPedErp.Value, chkSoloAbono.Value, m_lTipoPedido, arfiltrosFac, arfiltrosPago _
                    , arFiltrosImp, chkPedPM.Value, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
                Else
                    Set g_ador8 = oGestorInformes.ListadoPedidosImputaciones(m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), True, sPres0, chkPedErp.Value, chkSoloAbono.Value, m_lTipoPedido, arfiltrosFac, arfiltrosPago _
                    , , chkPedPM.Value, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
                End If
            Else
                Set g_ador8 = oGestorInformes.ListadoPedidosImputaciones(m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), True, sPres0, chkPedErp.Value, chkSoloAbono.Value, m_lTipoPedido, arfiltrosFac, arfiltrosPago _
                , , chkPedPM.Value, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
            End If
        End If
    End If
    
    'Pres
    If chkIncluir(2).Value = vbChecked Then
        If gParametrosGenerales.gbUsarPres1 And gParametrosGenerales.gbUsarPedPres1 Then
            Set g_ador1 = oGestorInformes.ListadoPedidosPresupuestos(1, m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), chkPedErp.Value, chkSoloAbono.Value, chkPedPM.Value, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas _
            , m_bRPerfUO, lIdPerfil, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
        End If
        If gParametrosGenerales.gbUsarPres2 And gParametrosGenerales.gbUsarPedPres2 Then
            Set g_ador2 = oGestorInformes.ListadoPedidosPresupuestos(2, m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), chkPedErp.Value, chkSoloAbono.Value, chkPedPM.Value, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas _
            , m_bRPerfUO, lIdPerfil, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
        End If
        If gParametrosGenerales.gbUsarPres3 And gParametrosGenerales.gbUsarPedPres3 Then
            Set g_ador3 = oGestorInformes.ListadoPedidosPresupuestos(3, m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), chkPedErp.Value, chkSoloAbono.Value, chkPedPM.Value, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas _
            , m_bRPerfUO, lIdPerfil, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
        End If
        If gParametrosGenerales.gbUsarPres4 And gParametrosGenerales.gbUsarPedPres4 Then
            Set g_ador4 = oGestorInformes.ListadoPedidosPresupuestos(4, m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), chkPedErp.Value, chkSoloAbono.Value, chkPedPM.Value, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas _
            , m_bRPerfUO, lIdPerfil, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
        End If
    End If
    
    'Atributos de la LINEA
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        If Not g_oParametrosSM Is Nothing Then
            Set g_ador6 = oGestorInformes.ListadoPedidos(arFiltrosPedido, Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), oGmn, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, True, 9, Trim(txtReferencia.Text), chkPedPM.Value, iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), basPublic.gParametrosInstalacion.gIdioma, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), _
                m_sCodReceptor, Trim(txtCodERP.Text), sdbcCM.Value, , , m_lTipoPedido, arfiltrosFac, arfiltrosPago, arFiltrosImp, chkPedErp.Value, chkSoloAbono.Value, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, g_bPedidosAbiertos, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
        Else
            Set g_ador6 = oGestorInformes.ListadoPedidos(arFiltrosPedido, Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), oGmn, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, True, 9, Trim(txtReferencia.Text), chkPedPM.Value, iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), basPublic.gParametrosInstalacion.gIdioma, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), _
                m_sCodReceptor, Trim(txtCodERP.Text), sdbcCM.Value, , , m_lTipoPedido, arfiltrosFac, arfiltrosPago, , chkPedErp.Value, chkSoloAbono.Value, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, g_bPedidosAbiertos, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
        End If
    Else
        Set g_ador6 = oGestorInformes.ListadoPedidos(arFiltrosPedido, Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), oGmn, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, True, 9, Trim(txtReferencia.Text), chkPedPM.Value, iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), basPublic.gParametrosInstalacion.gIdioma, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), _
            m_sCodReceptor, Trim(txtCodERP.Text), sdbcCM.Value, , , m_lTipoPedido, arfiltrosFac, arfiltrosPago, , chkPedErp.Value, chkSoloAbono.Value, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, g_bPedidosAbiertos, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
    End If
    
    'Planes
    If chkIncluir(4).Value = vbChecked Then
        Set g_ador9 = oGestorInformes.ListadoPedidosPlanes(m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), chkPedErp.Value, chkPedPM.Value, chkSoloAbono.Value, m_lTipoPedido, arfiltrosFac, arfiltrosPago _
        , Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas, m_bRPerfUO, lIdPerfil, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
    End If
    
    'Costes/Descuentos
    If chkIncluir(5).Value = vbChecked Then
        'Costes/descuentos
        Set rsCDCab = oGestorInformes.ListadoPedidosCDs(True, m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), chkPedErp.Value, chkPedPM.Value, _
            chkSoloAbono.Value, m_lTipoPedido, arfiltrosFac, arfiltrosPago, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
        Set rsCDLin = oGestorInformes.ListadoPedidosCDs(False, m_iAnyoPed, m_lNumCesta, m_lNumPedido, Trim(txtNumExt.Text), Trim(txtFecEmiD.Text), Trim(txtFecEmiH.Text), Trim(txtFecEntD.Text), Trim(txtFecEntH.Text), vNumEst, chkPedDirec.Value, chkPedAprov.Value, m_bRMat, sCodEqp, sCodComp, ACN1Seleccionada, ACN2Seleccionada, ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, Trim(sdbcProveCod.Text), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcArtiCod.Text), Trim(sdbcDestCod.Text), m_iNumAnyo2, m_iNumProceCod, m_sCodPer, m_lSolicit, sPerSoli, Trim(txtReferencia.Text), iCampo1, iCampo2, Trim(txtDato1.Text), Trim(txtDato2.Text), m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, IIf(sdbcEmpresa.Text <> "", sdbcEmpresa.Columns(0).Value, 0), m_sCodReceptor, Trim(txtCodERP.Text), chkPedErp.Value, chkPedPM.Value, _
            chkSoloAbono.Value, m_lTipoPedido, arfiltrosFac, arfiltrosPago, Me.sdbcProAdjCod.Text, arFiltrosGestorPlanesFacturas, VerPedBajaLogica:=chkVerPedBajaLogica.Value)
    End If
    
    Me.Hide
    
    bAbrirInforme = True
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
            bAbrirInforme = False
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If

    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPEDIDOS.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        bAbrirInforme = False
    End If
    Set oFos = Nothing
    
    If bAbrirInforme Then
        Set oCRPedidos = GenerarCRPedidos(RepPath)
        Dim oReport As CRAXDRT.Report
        Set oReport = oCRPedidos.InformePedidos(Me, sSeleccion, arCaptionsInforme, sEstados, sEspera, Not (chkIncluir(0).Value = vbUnchecked), Not (chkIncluir(1).Value = vbUnchecked), _
            (chkIncluir(3).Value = vbChecked), g_ador, g_ador5, g_ador7, g_ador8, chkIncluir(2).Value = vbChecked, g_ador1, g_ador2, g_ador3, g_ador4, _
            g_ador6, (chkIncluir(4).Value = vbChecked), sPlanes, g_ador9, (chkIncluir(5).Value = vbChecked), rsCDCab, rsCDLin, sEstadosInt)
            
        DoEvents
        frmESPERA.Show
        frmESPERA.lblGeneral.caption = sEspera(1)
        frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
        frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
        frmESPERA.Frame2.caption = ""
        frmESPERA.Frame3.caption = ""
        frmESPERA.lblContacto = sEspera(2)
        frmESPERA.lblDetalle = sEspera(3)
        
        Dim pv As Preview
        Set pv = New Preview
        pv.Hide
        pv.g_sOrigen = "frmSeguimiento"
        pv.caption = arCaptionsInforme(0)
        Set pv.g_oReport = oReport
        pv.crViewer.ReportSource = oReport
        
        Timer1.Enabled = True
        
        pv.crViewer.ViewReport
        pv.crViewer.Visible = True
        DoEvents
        pv.Show
        
        Timer1.Enabled = False
        Unload frmESPERA
        Set oReport = Nothing
    End If
    
    Screen.MousePointer = vbNormal
    
    Unload Me
    
    Set rsCDCab = Nothing
    Set rsCDLin = Nothing
End Sub


''' <summary>
''' Oculta los criterios de busqueda avanzados...
''' </summary>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub cmdOcultar_Click()
    Me.frmSegui.Height = 1215

    FrameProve.Visible = False
    FrameCateg.Visible = False
    Frame2.Visible = False
    Frame3.Visible = False
    Line1.Visible = False
    Line2.Visible = False
    Line3.Visible = False
    Line5.Visible = False
    Frame4.Visible = False
    frameEmpresa.Visible = False
    FrameFactura.Visible = False
    fraProce.Visible = False
    FrameImp.Visible = False
    cmdAvanzado.Visible = True
    frmSegui.Top = 1180
    cmdObtener.Top = cmdObtener.Top - 90
    
     If Me.Visible Then Me.sdbcAnyo.SetFocus
End Sub

''' <summary>
''' Almacena el valor del estado de una factura seleccionado
''' </summary>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub sdbcEstadoFactura_CloseUp()
    If sdbcEstadoFactura.Value = "0" Or Trim(sdbcEstadoFactura.Value) = "" Then
        sdbcTipoPedido.Text = ""
        Exit Sub
    End If
    sdbcEstadoFactura.Text = sdbcEstadoFactura.Columns(1).Value
End Sub

''' <summary>
''' Carga el combo de estados de pago con los posibles estados que puede tener un pago de una factura
''' </summary>
''' <remarks>Tiempo m�ximo=0,1seg.</remarks>
Private Sub sdbcEstadoFactura_DropDown()
    Dim oFacturas As CFacturas
    Dim rs As Recordset
    Screen.MousePointer = vbHourglass
    
    sdbcEstadoFactura.RemoveAll
    Set oFacturas = oFSGSRaiz.Generar_CFacturas
    
    Set rs = oFacturas.devolverTipoEstados(gParametrosInstalacion.gIdioma)
    sdbcEstadoFactura.AddItem "" & Chr(m_lSeparador) & ""
    While Not rs.EOF
        sdbcEstadoFactura.AddItem rs("ID").Value & Chr(m_lSeparador) & rs("DEN").Value
        rs.MoveNext
    Wend
    Set oFacturas = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcEstadoFactura_InitColumnProps()
    sdbcEstadoFactura.DataFieldList = "Column 0"
    sdbcEstadoFactura.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Valida que el Estado de una factura introducida sea el correcto
''' </summary>
''' <param name="Cancel">Propio del evento</param>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub sdbcEstadoFactura_Validate(Cancel As Boolean)
Dim i As Integer
Dim bm As Variant
    
    If sdbcEstadoFactura.Text <> "" Then
        sdbcEstadoFactura.MoveFirst
        For i = 0 To sdbcEstadoFactura.Rows - 1
         bm = sdbcEstadoFactura.GetBookmark(i)
         If UCase(sdbcEstadoFactura.Text) = UCase(Mid(sdbcEstadoFactura.Columns(1).CellText(bm), 1, Len(sdbcEstadoFactura.Text))) Then
             sdbcEstadoFactura.Bookmark = bm
             Exit Sub
         End If
        Next i
        sdbcEstadoFactura.Text = ""
        End If
End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">Valor de la celda</param>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub sdbcEstadoFactura_PositionList(ByVal Text As String)
PositionList sdbcEstadoFactura, Text, 1
End Sub

''' <summary>
''' Carga el combo de estados de pago con los posibles estados que puede tener un pago de una factura
''' </summary>
''' <remarks>Tiempo m�ximo=0,1seg.</remarks>
Private Sub sdbcEstadoPago_DropDown()
    Dim oPagosFactura As cPagosFactura
    Dim rs As Recordset
    Screen.MousePointer = vbHourglass
    
    sdbcEstadoPago.RemoveAll
    Set oPagosFactura = oFSGSRaiz.Generar_CPagosFactura
    
    Set rs = oPagosFactura.devolverTipoEstados(gParametrosInstalacion.gIdioma)
    sdbcEstadoPago.AddItem "" & Chr(m_lSeparador) & ""
    While Not rs.EOF
        sdbcEstadoPago.AddItem rs("ID").Value & Chr(m_lSeparador) & rs("DEN").Value
        rs.MoveNext
    Wend
    Set oPagosFactura = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

''' <summary>
''' Almacena el valor del estado de un pago seleccionado
''' </summary>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub sdbcEstadoPago_CloseUp()
    If sdbcEstadoPago.Value = "0" Or Trim(sdbcEstadoPago.Value) = "" Then
        sdbcEstadoPago.Text = ""
        sdbcEstadoPago.Value = ""
        Exit Sub
    End If
    sdbcEstadoPago.Text = sdbcEstadoPago.Columns(1).Value
End Sub

Private Sub sdbcEstadoPago_InitColumnProps()
    sdbcEstadoPago.DataFieldList = "Column 0"
    sdbcEstadoPago.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Valida que el Estado de un pago introducido sea el correcto
''' </summary>
''' <param name="Cancel">Propio del evento</param>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub sdbcEstadoPago_Validate(Cancel As Boolean)
Dim i As Integer
Dim bm As Variant
    
    If sdbcEstadoPago.Text <> "" Then
        sdbcEstadoPago.MoveFirst
        For i = 0 To sdbcEstadoPago.Rows - 1
         bm = sdbcEstadoPago.GetBookmark(i)
         If UCase(sdbcEstadoPago.Text) = UCase(Mid(sdbcEstadoPago.Columns(1).CellText(bm), 1, Len(sdbcEstadoPago.Text))) Then
             sdbcEstadoPago.Bookmark = bm
             Exit Sub
         End If
        Next i
        sdbcEstadoPago.Text = ""
        End If
End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">Valor de la celda</param>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub sdbcEstadoPago_PositionList(ByVal Text As String)
PositionList sdbcEstadoPago, Text, 1
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub cmdFecDesdeFactura_Click()
    AbrirFormCalendar Me, txtFecDesdeFactura
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub cmdFecDesdePago_Click()
    AbrirFormCalendar Me, txtFecDesdePago
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub cmdFecHastaFactura_Click()
    AbrirFormCalendar Me, txtFecHastaFactura
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego alamcenar el valor.
''' </summary>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub cmdFecHastaPago_Click()
    AbrirFormCalendar Me, txtFecHastaPago
End Sub

''' <summary>
''' Valida que el codigo introducido sea un codigo de Centro de coste.
''' Valida que el codigo este en alguna de las ramas de imputaciones del sistema.
''' Carga la informacion de un centro de coste (Codigo UON.. + Denominacion) Igual que se muestra en la grid de seguimiento
''' m_sUON_CC --> se almacena internamente la estructura del centro de coste elegido (UON1-UON2-UON3-UON4)
''' </summary>
''' <remarks>Tiempo m�ximo= < 1seg.</remarks>
Private Sub txtCentroCoste_Validate(Cancel As Boolean)
Dim bExiste As Boolean
Dim oParametroSM As CParametroSM
Dim sAux() As String
Dim sValor As String

    If m_bRespetar Then Exit Sub
    
    If txtCentroCoste.Text = "" Then Exit Sub
    Dim oCentrosCoste As CCentrosCoste
    Set oCentrosCoste = oFSGSRaiz.Generar_CCentrosCoste
    
    sAux = Split(txtCentroCoste.Text, " - ")
    If UBound(sAux) > 0 Then
        sValor = sAux(0)
    Else
        sValor = txtCentroCoste.Text
    End If
    
    bExiste = False
    For Each oParametroSM In g_oParametrosSM
        oCentrosCoste.CargarTodosLosCentrosCoste oParametroSM.PRES5, oParametroSM.impnivel, basOptimizacion.gTipoDeUsuario, basOptimizacion.gvarCodUsuario, gParametrosInstalacion.gIdioma, , , , , , Trim(txtCentroCoste.Text)
        If oCentrosCoste.Count > 0 Then
            bExiste = True
            Exit For
        End If
    Next
  
    
    If bExiste Then
        Dim sCod As String
        
        If oCentrosCoste.Count > 0 Then
            If oCentrosCoste.Count > 1 Then
                lstCentroCoste.clear
                Dim sItemCentro As String
                Dim i As Integer
                ReDim m_arrCentrosCoste(oCentrosCoste.Count)
                
                For i = 1 To oCentrosCoste.Count
                    If Not NoHayParametro(oCentrosCoste.Item(i).UON4) Then
                        sCod = oCentrosCoste.Item(i).UON4
                    ElseIf Not NoHayParametro(oCentrosCoste.Item(i).UON3) Then
                        sCod = oCentrosCoste.Item(i).UON3
                    ElseIf Not NoHayParametro(oCentrosCoste.Item(i).UON2) Then
                        sCod = oCentrosCoste.Item(i).UON2
                    Else
                        sCod = oCentrosCoste.Item(i).UON1
                    End If
                    m_arrCentrosCoste(i - 1) = NullToStr(oCentrosCoste.Item(i).UON1) & "-" & NullToStr(oCentrosCoste.Item(i).UON2) & "-" & NullToStr(oCentrosCoste.Item(i).UON3) & "-" & NullToStr(oCentrosCoste.Item(i).UON4)
                    sItemCentro = sCod & " - " & DevolverDenCC(oCentrosCoste.Item(i).UON1, oCentrosCoste.Item(i).UON2, oCentrosCoste.Item(i).UON3, oCentrosCoste.Item(i).UON4, oCentrosCoste.Item(i).Den, oCentrosCoste.Item(i).Den, oCentrosCoste.Item(i).Den, oCentrosCoste.Item(i).Den)
                    lstCentroCoste.AddItem sItemCentro
                Next
                lstCentroCoste.Height = 480
                lstCentroCoste.Visible = True
                If Me.Visible Then lstCentroCoste.SetFocus
            ElseIf oCentrosCoste.Count = 1 Then
                If Not NoHayParametro(oCentrosCoste.Item(1).UON4) Then
                    sCod = oCentrosCoste.Item(1).UON4
                ElseIf Not NoHayParametro(oCentrosCoste.Item(1).UON3) Then
                    sCod = oCentrosCoste.Item(1).UON3
                ElseIf Not NoHayParametro(oCentrosCoste.Item(1).UON2) Then
                    sCod = oCentrosCoste.Item(1).UON2
                Else
                    sCod = oCentrosCoste.Item(1).UON1
                End If
                Set m_oCentroCosteSeleccionadoFiltro = oCentrosCoste.Item(1)
                txtCentroCoste.Text = sCod & " - " & DevolverDenCC(oCentrosCoste.Item(1).UON1, oCentrosCoste.Item(1).UON2, oCentrosCoste.Item(1).UON3, oCentrosCoste.Item(1).UON4, oCentrosCoste.Item(1).Den, oCentrosCoste.Item(1).Den, oCentrosCoste.Item(1).Den, oCentrosCoste.Item(1).Den)
                m_sUON_CC = NullToStr(oCentrosCoste.Item(1).UON1) & "-" & NullToStr(oCentrosCoste.Item(1).UON2) & "-" & NullToStr(oCentrosCoste.Item(1).UON3) & "-" & NullToStr(oCentrosCoste.Item(1).UON4)

            End If
        Else
            m_sUON_CC = ""
        End If

    Else

        txtCentroCoste.Text = ""
        m_sUON_CC = ""

    
    End If
    If txtPartida.Text <> "" Then
        m_bControlPartidaFiltro = True
        txtPartida_Validate False
    End If
    Set oCentrosCoste = Nothing
End Sub


''' <summary>
''' Valida que la fecha introducida sea el correcto
''' </summary>
''' <param name="Cancel">Propio del evento</param>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub txtFecDesdeFactura_Validate(Cancel As Boolean)
    If txtFecDesdeFactura.Text <> "" Then
        If Not IsDate(txtFecDesdeFactura.Text) Then
            oMensajes.NoValida " " & lblFechaDesdeFacturaPago.caption
            txtFecDesdeFactura.Text = ""
            Cancel = True
        End If
    End If
End Sub

''' <summary>
''' Valida que la fecha introducida sea el correcto
''' </summary>
''' <param name="Cancel">Propio del evento</param>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub txtFecHastaFactura_Validate(Cancel As Boolean)
    If txtFecHastaFactura.Text <> "" Then
        If Not IsDate(txtFecHastaFactura.Text) Then
            oMensajes.NoValida " " & sIdioma(6) & " " & lblHasta.caption
            txtFecHastaFactura.Text = ""
            Cancel = True
        End If
    End If
End Sub
''' <summary>
''' Valida que la fecha introducida sea el correcto
''' </summary>
''' <param name="Cancel">Propio del evento</param>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub txtFecDesdePago_Validate(Cancel As Boolean)
    If txtFecDesdePago.Text <> "" Then
        If Not IsDate(txtFecDesdePago.Text) Then
            oMensajes.NoValida " " & lblFechaDesdeFacturaPago.caption
            txtFecDesdePago.Text = ""
            Cancel = True
            Exit Sub
        End If
    End If
End Sub

''' <summary>
''' Valida que la fecha introducida sea el correcto
''' </summary>
''' <param name="Cancel">Propio del evento</param>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub txtFecHastaPago_Validate(Cancel As Boolean)
    If txtFecHastaPago.Text <> "" Then
        If Not IsDate(txtFecHastaPago.Text) Then
            oMensajes.NoValida " " & sIdioma(6) & " " & lblHasta.caption
            txtFecHastaPago.Text = ""
            Cancel = True
        End If
    End If
End Sub


''' <summary>
''' Evento que salta modificar un valor de la grid de partidas y cambiar de fila
''' Valida que la partida introducida sea correcta.
''' Internamente se almacena un array con el valor de los distintas partidas que tenemos en la grid (estructura PRES0-PRES1-PRES2-PRES3-PRES4)
''' </summary>
''' <remarks>Tiempo m�ximo= 0,2seg.</remarks>
Private Sub sdbgPartidas_BeforeRowColChange(Cancel As Integer)
    Dim sValor As String
    Dim sPres0 As String
    Dim oContratosPres As CContratosPres
    Dim bEncontrado As Boolean
    Dim arrCC() As String
    Dim i As Byte
    Dim sUsu As String
    Dim sCod As String
    Dim iNivelImp As Integer
    Dim sAux() As String
    If Not m_bControlPartidaFiltro Then Exit Sub
    
    sPres0 = sdbgPartidas.Columns(0).Value
    sValor = ""
    If sdbgPartidas.Columns(2).Value <> "" Then
        sAux = Split(sdbgPartidas.Columns(2).Value, " - ")
        If UBound(sAux) > 0 Then
            sValor = sAux(0)
        Else
            sValor = sdbgPartidas.Columns(2).Value
        End If
    End If
    iNivelImp = sdbgPartidas.Columns("NIVEL_IMPUTACION").Value
    If basOptimizacion.gTipoDeUsuario <> Administrador Then
        sUsu = basOptimizacion.gvarCodUsuario
    End If

    Set oContratosPres = oFSGSRaiz.Generar_CContratosPres
    
    If m_sUON_CC <> "" Then
        arrCC = Split(m_sUON_CC, "-")
    Else
        ReDim arrCC(4)
    End If
    

    oContratosPres.CargarTodosLosContratosCentros sPres0, iNivelImp, sUsu, gParametrosInstalacion.gIdioma, arrCC(0), arrCC(1), arrCC(2), arrCC(3), sValor, , True
    If oContratosPres.Count > 0 Then
        bEncontrado = True
    End If
    
    If bEncontrado Then
        If m_sUON_CC = "" Then
            If oContratosPres.Count = 1 Then
                m_sUON_CC = NullToStr(oContratosPres.Item(1).UON1) & "-" & NullToStr(oContratosPres.Item(1).UON2) & "-" & NullToStr(oContratosPres.Item(1).UON3) & "-" & NullToStr(oContratosPres.Item(1).UON4)
                If Not NoHayParametro(oContratosPres.Item(1).UON4) Then
                    sCod = oContratosPres.Item(1).UON4
                ElseIf Not NoHayParametro(oContratosPres.Item(1).UON3) Then
                    sCod = oContratosPres.Item(1).UON3
                ElseIf Not NoHayParametro(oContratosPres.Item(1).UON2) Then
                    sCod = oContratosPres.Item(1).UON2
                Else
                    sCod = oContratosPres.Item(1).UON1
                End If
                txtCentroCoste.Text = sCod & " - " & NullToStr(oContratosPres.Item(1).DenUON)
            Else
                ReDim m_arrCentrosCoste(oContratosPres.Count)
                lstCentroCoste.clear
                Dim sItemCentro As String
                txtCentroCoste.Text = ""
                For i = 1 To oContratosPres.Count
                    If Not NoHayParametro(oContratosPres.Item(i).UON4) Then
                        sCod = oContratosPres.Item(i).UON4
                    ElseIf Not NoHayParametro(oContratosPres.Item(i).UON3) Then
                        sCod = oContratosPres.Item(i).UON3
                    ElseIf Not NoHayParametro(oContratosPres.Item(i).UON2) Then
                        sCod = oContratosPres.Item(i).UON2
                    Else
                        sCod = oContratosPres.Item(i).UON1
                    End If
                    sItemCentro = sCod & " - " & NullToStr(oContratosPres.Item(i).DenUON)
                    lstCentroCoste.AddItem sItemCentro
                    m_arrCentrosCoste(i - 1) = NullToStr(oContratosPres.Item(i).UON1) & "-" & NullToStr(oContratosPres.Item(i).UON2) & "-" & NullToStr(oContratosPres.Item(i).UON3) & "-" & NullToStr(oContratosPres.Item(i).UON4)
                Next
                lstCentroCoste.Height = 480
                lstCentroCoste.Visible = True
                If Me.Visible Then lstCentroCoste.SetFocus
        
            End If
        End If
        sdbgPartidas.Columns(2).Value = oContratosPres.Item(1).Cod & " - " & oContratosPres.Item(1).Den
        m_arrPartidas(sdbgPartidas.Columns("POSICION").Value) = sPres0 & "-" & NullToStr(oContratosPres.Item(1).Pres1) & "-" & NullToStr(oContratosPres.Item(1).Pres2) & "-" & NullToStr(oContratosPres.Item(1).Pres3) & "-" & NullToStr(oContratosPres.Item(1).Pres4)
        
    Else
        sdbgPartidas.Columns(2).Value = ""
        m_arrPartidas(sdbgPartidas.Row) = ""
    End If
    Set oContratosPres = Nothing
    m_bControlPartidaFiltro = False
    m_bRespetar = True
End Sub

''' <summary>
''' A traves de la variable se lleva el control de si se ha modificado algun valor el la grid de partidas
''' </summary>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub sdbgPartidas_Change()
    m_bControlPartidaFiltro = True
End Sub

''' <summary>
''' Cuando en la grid de partidas se pulse una tecla y sea la de eliminar caracter se elimina el contenido
''' y el contenido de la variable interna
''' </summary>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub sdbgPartidas_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyBack Then
        sdbgPartidas.Columns(2).Value = ""
        m_arrPartidas(sdbgPartidas.Columns("POSICION").Value) = ""
    End If
End Sub


Private Sub Timer1_Timer()
frmESPERA.ZOrder
   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub


Private Sub Form_Unload(Cancel As Integer)

    m_bRMat = False
    m_bRAsig = False
    m_bRCompResp = False
    m_bREqpAsig = False
    m_bRUsuAper = False
    m_bRUsuUON = False
    m_bRUsuPerfUON = False
    m_bRUsuDep = False
    m_bRUsuAprov = False
    m_bPedAprov = False
    m_bPedDirec = False
    m_bRDest = False
    m_bRuo = False
    m_bRDep = False
    
    Set oGrupoMN4seleccionado = Nothing
    
    Set oGMN1Seleccionado = Nothing

    Set oProcesoSeleccionado = Nothing
    Set oProcesos = Nothing
    Set oProveSeleccionado = Nothing
    Set oProves = Nothing
    Set oDestinos = Nothing

    vNumEst = Empty
     sGMN1Cod = ""
     sGMN2Cod = ""
     sGMN3Cod = ""
     sGMN4Cod = ""
    
     ACN1Seleccionada = 0
     ACN2Seleccionada = 0
     ACN3Seleccionada = 0
     ACN4Seleccionada = 0
     ACN5Seleccionada = 0

     iAnyoSeleccionado = 0

    g_sOrigen = ""
    
End Sub



Private Sub sdbcAnyo2_Change()
    
    If sdbcAnyo2.Text = "" Then
        sdbcProceCod = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        sdbcProceDen.RemoveAll
        sdbcCM = ""
    End If
End Sub

Private Sub sdbcEst_DropDown()
     sdbcEst.RemoveAll
        
    If chkPedDirec.Value = vbChecked Then
        sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 2
        sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 3
        sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 4
        sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 5
        sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 6
        sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 20
        sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 21
    Else
        sdbcEst.AddItem sIdiEst(1) & Chr(m_lSeparador) & 0
        sdbcEst.AddItem sIdiEst(2) & Chr(m_lSeparador) & 1
        sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 2
        sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 3
        sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 4
        sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 5
        sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 6
        sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 20
        sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 21
        sdbcEst.AddItem sIdiEst(10) & Chr(m_lSeparador) & 22
    End If
End Sub



''' <summary>
''' Evento que salta al modificar el valor del centro de coste
''' </summary>
''' <remarks>Tiempo m�ximo= 0,1seg.</remarks>
Private Sub txtCentroCoste_Change()
    lstCentroCoste.Visible = False
    m_bRespetar = False
    If txtCentroCoste.Text = "" Then
        m_sUON_CC = ""
    End If
End Sub

''' <summary>
''' Evento que salta seleccionar un centro de coste de la lista
''' Internamente se almacena la estructura de los UONs del centro de coste
''' </summary>
''' <remarks>Tiempo m�ximo= 0,1seg.</remarks>
Private Sub lstCentroCoste_Click()
Dim sAux() As String
Dim iPos1 As Integer
Dim iPos2 As Integer
    txtCentroCoste.Text = lstCentroCoste.List(lstCentroCoste.ListIndex)
    lstCentroCoste.Visible = False
    m_sUON_CC = NullToStr(m_arrCentrosCoste(lstCentroCoste.ListIndex))
    sAux = Split(m_sUON_CC, "-")
    Set m_oCentroCosteSeleccionadoFiltro = Nothing
    Set m_oCentroCosteSeleccionadoFiltro = oFSGSRaiz.Generar_CCentroCoste
    m_oCentroCosteSeleccionadoFiltro.UON1 = sAux(0)
    m_oCentroCosteSeleccionadoFiltro.UON2 = sAux(1)
    m_oCentroCosteSeleccionadoFiltro.UON3 = sAux(2)
    m_oCentroCosteSeleccionadoFiltro.UON4 = sAux(3)
    
    iPos1 = InStr(1, txtCentroCoste.Text, " - ")
    m_oCentroCosteSeleccionadoFiltro.Cod = Mid(txtCentroCoste.Text, 1, iPos1 - 1)
    iPos2 = InStr(iPos1, txtCentroCoste.Text, "(")
    If iPos2 = 0 Then
        m_oCentroCosteSeleccionadoFiltro.Den = Mid(txtCentroCoste.Text, iPos1 + 3, Len(txtCentroCoste.Text))
    Else
        m_oCentroCosteSeleccionadoFiltro.Den = Mid(txtCentroCoste.Text, iPos1 + 3, Len(txtCentroCoste.Text) - (iPos2))
        m_oCentroCosteSeleccionadoFiltro.CodConcat = Mid(txtCentroCoste.Text, iPos2 + 1, Len(txtCentroCoste.Text) - (iPos2) - 1)
    End If
    
    
       
End Sub


Private Sub txtFecEmiD_Validate(Cancel As Boolean)
    
    If txtFecEmiD <> "" Then
        If Not IsDate(txtFecEmiD) Then
            oMensajes.NoValida " " & sIdioma(4) & " " & label2.caption
            txtFecEmiD = ""
            If Me.Visible Then txtFecEmiD.SetFocus
            Exit Sub
        End If
    End If

End Sub
Private Sub txtFecEmiH_Validate(Cancel As Boolean)
    
    If txtFecEmiH <> "" Then
        If Not IsDate(txtFecEmiH) Then
            oMensajes.NoValida " " & sIdioma(4) & " " & Label13.caption
            txtFecEmiH = ""
            If Me.Visible Then txtFecEmiH.SetFocus
            Exit Sub
        End If
    End If

End Sub
Private Sub txtFecEntD_Validate(Cancel As Boolean)
    
    If txtFecEntD <> "" Then
        If Not IsDate(txtFecEntD) Then
            oMensajes.NoValida " " & sIdioma(4) & " " & Label14.caption
            txtFecEntD = ""
            If Me.Visible Then txtFecEntD.SetFocus
            Exit Sub
        End If
    End If

End Sub
Private Sub txtFecEntH_Validate(Cancel As Boolean)
    
    If txtFecEntH <> "" Then
        If Not IsDate(txtFecEntH) Then
            oMensajes.NoValida " " & sIdioma(4) & " " & Label14.caption
            txtFecEntD = ""
            If Me.Visible Then txtFecEntH.SetFocus
            Exit Sub
        End If
    End If

End Sub

Private Sub cmdSelMat_Click()
    frmSELMAT.sOrigen = "frmLstPedidos"
    frmSELMAT.bRComprador = m_bRMat
    frmSELMAT.bRCompResponsable = m_bRCompResp
    frmSELMAT.bRUsuAper = m_bRUsuAper
    frmSELMAT.Show 1
End Sub

''' <summary>
''' Evento que salta al llamar al formulario. Configura la pantalla y carga los combos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Form_Load()
    
    'Ocultamos los frames 1,2 y 3 y los check�s 1,2 y 3  porque el report no puede con todo.
    'Todo lo comentado en este evento debe de descomentarse una vez se mejore el report

    Me.Height = 10750 '8475
    Me.Width = 14050 '10350
        
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    m_lNumPedido = 0
    m_iAnyoPed = 0
    m_lNumCesta = 0
    m_iNumProceCod = 0
    m_iNumAnyo2 = 0
    m_lSolicit = 0
    m_sCodPer = ""
    m_sCodPer = ""
        
    ConfigurarSeguridad
    
    CargarRecursos
    
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        chkIncluir(3).Visible = True
    Else
        chkIncluir(3).Visible = False
    End If
    
    PonerFieldSeparator Me
    
    CargarAnyos

    iAnyoSeleccionado = Year(Date)
    
    
    FrameProve.Visible = False
    FrameCateg.Visible = False
    Frame2.Visible = False
    Frame3.Visible = False
    Line1.Visible = False
    Line2.Visible = False
    Line3.Visible = False
    Line5.Visible = False
    Frame4.Visible = False
    frameEmpresa.Visible = False
    FrameFactura.Visible = False
    fraProce.Visible = False
    FrameImp.Visible = False
    cmdAvanzado.Visible = True
    frmSegui.Top = 1180


    chkPedDirec.Value = vbUnchecked
    chkPedAprov.Value = vbUnchecked
    chkPedErp.Value = vbUnchecked
    chkPedPM.Value = vbUnchecked
    chkPedDirec.Visible = True
    chkPedAprov.Visible = True
    chkPedErp.Visible = True
    chkPedPM.Visible = True
    
    If FSEPConf Then
        chkPedDirec.Value = vbUnchecked
        chkPedAprov.Value = vbChecked
        chkPedErp.Value = vbUnchecked
        chkPedDirec.Visible = False
        chkPedAprov.Visible = False
        chkPedErp.Visible = False
        chkPedPM.Top = chkSoloAbono.Top
        fraProce.Visible = False
    Else
        'Si solo hay un tipo de pedido configurado o el usuario s�lo tiene permiso para uno de los tipos no se ver�n los checks.
        If (gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP) Or (m_bPedDirec And Not m_bPedAprov And Not m_bPedErp) Then
            chkPedAprov.Value = vbUnchecked
            chkPedDirec.Value = vbChecked
            chkPedErp.Value = vbUnchecked
            chkPedDirec.Visible = False
            chkPedAprov.Visible = False
            chkPedErp.Visible = False
            chkPedPM.Visible = False
        'Si solo hay un tipo de pedido configurado o el usuario s�lo tiene permiso para uno de los tipos no se ver�n los checks.
        ElseIf (gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosERP) Or (m_bPedAprov And Not m_bPedDirec And Not m_bPedErp) Then
            chkPedAprov.Value = vbChecked
            chkPedDirec.Value = vbUnchecked
            chkPedErp.Value = vbUnchecked
            chkPedDirec.Visible = False
            chkPedAprov.Visible = False
            chkPedErp.Visible = False
            chkPedPM.Visible = False
        'Si solo hay un tipo de pedido configurado o el usuario s�lo tiene permiso para uno de los tipos no se ver�n los checks.
        ElseIf (gParametrosGenerales.gbPedidosERP And Not gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov) Or (m_bPedErp And Not m_bPedDirec And Not m_bPedAprov) Then
            chkPedAprov.Value = vbUnchecked
            chkPedErp.Value = vbChecked
            chkPedDirec.Value = vbUnchecked
            chkPedDirec.Visible = False
            chkPedAprov.Visible = False
            chkPedErp.Visible = False
            chkPedPM.Visible = False
        'Si hay alg�n tipo de pedido no configurado o el usuario no tiene permiso para �l, no se ver�.
        ElseIf (gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP) Or (m_bPedDirec And m_bPedAprov And Not m_bPedErp) Then
            chkPedErp.Visible = False
        'Si hay alg�n tipo de pedido no configurado o el usuario no tiene permiso para �l, no se ver�.
        ElseIf (gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP) Or (m_bPedDirec And Not m_bPedAprov And m_bPedErp) Then
            chkPedAprov.Visible = False
            chkPedErp.Top = chkSoloAbono.Top
        'Si hay alg�n tipo de pedido no configurado o el usuario no tiene permiso para �l, no se ver�.
        ElseIf (Not gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP) Or (Not m_bPedDirec And m_bPedAprov And m_bPedErp) Then
            chkPedDirec.Visible = False
            chkSoloAbono.Left = chkPedErp.Left
            chkSoloAbono.Top = chkPedErp.Top
            chkPedErp.Left = chkPedDirec.Left
        End If
    End If
    
    If m_bRUsuAprov Then
        sdbcUsuCod.Visible = False
        lblAprov.Visible = False
        lblReceptor.Top = lblAprov.Top
        sdbcReceptor.Top = sdbcUsuCod.Top
    End If
    
    If Not gParametrosGenerales.gbSolicitudesCompras Or FSEPConf Then
        lblSolicitudTitle.Visible = False
        lblSolicitud.Visible = False
        cmdBorraSolicitud.Visible = False
        cmdBuscarSolicitud.Visible = False
    End If
    
    If basParametros.gParametrosGenerales.gbCodPersonalizPedido = False Then
        Label17.Visible = False
        Label18.Left = Label17.Left
        txtReferencia.Visible = False
        txtNumExt.Left = txtReferencia.Left
    End If
    
    sdbcEst.RemoveAll
    If FSEPConf Then
        sdbcEst.AddItem sIdiEst(1) & Chr(m_lSeparador) & 0
        sdbcEst.AddItem sIdiEst(2) & Chr(m_lSeparador) & 1
        sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 2
        sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 3
        sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 4
        sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 5
        sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 6
        sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 20
        sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 21
        sdbcEst.AddItem sIdiEst(10) & Chr(m_lSeparador) & 22
    Else
        If (Not gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosDirectos) Or m_bPedDirec Then
            sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 2
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 3
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 20
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 21
        Else
            sdbcEst.AddItem sIdiEst(1) & Chr(m_lSeparador) & 0
            sdbcEst.AddItem sIdiEst(2) & Chr(m_lSeparador) & 1
            sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 2
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 3
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 20
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 21
            sdbcEst.AddItem sIdiEst(10) & Chr(m_lSeparador) & 22
        End If
    End If
    
    
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        CargarPartidas
        lstCentroCoste.Left = txtCentroCoste.Left
        lstCentroCoste.Top = txtCentroCoste.Top + txtCentroCoste.Height
        lstCentroCoste.Height = 480
    End If
    
    Set oProcesos = oFSGSRaiz.generar_CProcesos
    Set oProves = oFSGSRaiz.generar_CProveedores
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
    
    If Not g_sOrigen = "frmSeguimiento" Then
        sdbcAnyo.Text = Year(Date)
        sdbcAnyo2.Text = ""
    End If
    
    'Si no est�n activados ninguno de los 2 par�metros
    If gParametrosGenerales.gbActivarCodProveErp = False Then
        lblCodERP.Visible = False
        txtCodERP.Visible = False
    End If
    
End Sub
Public Sub PonerMatSeleccionado()
    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    
    If Not oGMN1Seleccionado Is Nothing Then
        sGMN1Cod = oGMN1Seleccionado.Cod
        lblMaterial = sGMN1Cod & " - " & oGMN1Seleccionado.Den
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
        lblMaterial = sGMN1Cod & " - " & sGMN2Cod & " - " & oGMN2Seleccionado.Den
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
        lblMaterial = sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod & " - " & oGMN3Seleccionado.Den
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
        lblMaterial = sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod & " - " & sGMN4Cod & " - " & oGMN4Seleccionado.Den
    End If
    
    sdbcProceCod.Text = ""
    sdbcProceDen.Text = ""
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
End Sub
 
Private Sub CargarAnyos()
    
    Dim iAnyoActual As Integer
    Dim iInd As Integer

    iAnyoActual = Year(Date)
    sdbcAnyoFacturas.AddItem ""
    
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        sdbcAnyo.AddItem iInd
        sdbcAnyo2.AddItem iInd
        sdbcAnyoFacturas.AddItem iInd
    Next

    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
End Sub


Private Sub sdbcAnyo2_Click()
    
    sdbcProceCod = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    sdbcProceDen.RemoveAll

End Sub

Private Sub sdbcDestCod_Change()
    If Not bRespetarComboDest Then
    
        bRespetarComboDest = True
        sdbcDestDen.Text = ""
        
        bRespetarComboDest = False
        
        bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcDestCod_Click()
    If Not sdbcDestCod.DroppedDown Then
        sdbcDestCod = ""
        sdbcDestDen = ""
    End If
End Sub

Private Sub sdbcDestCod_CloseUp()

    If sdbcDestCod.Value = "..." Then
        sdbcDestCod.Text = ""
        Exit Sub
    End If
    
    If sdbcDestCod.Value = "" Then Exit Sub
    
    bRespetarComboDest = True
    sdbcDestDen.Text = sdbcDestCod.Columns(1).Text
    sdbcDestCod.Text = sdbcDestCod.Columns(0).Text
    bRespetarComboDest = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcDestCod_DropDown()
Dim ADORs As Recordset
    
    Screen.MousePointer = vbHourglass
    
    sdbcDestCod.RemoveAll
        
    If basOptimizacion.gTipoDeUsuario <> Administrador Then
    
        If oUsuarioSummit.Persona Is Nothing Then
            If bCargarComboDesde Then
                Set ADORs = oDestinos.DevolverTodosLosDestinos(Trim(sdbcDestCod.Text), , , m_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
            Else
                Set ADORs = oDestinos.DevolverTodosLosDestinos(, , , m_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
            End If
        Else
'            Screen.MousePointer = vbNormal
'            Exit Sub
            If bCargarComboDesde Then
                Set ADORs = oDestinos.DevolverTodosLosDestinos(Trim(sdbcDestCod.Text), , , m_bRDest)
            Else
                Set ADORs = oDestinos.DevolverTodosLosDestinos(, , , m_bRDest)
            End If
        End If
    Else
        If bCargarComboDesde Then
            Set ADORs = oDestinos.DevolverTodosLosDestinos(Trim(sdbcDestCod.Columns(0).Text))
        Else
            Set ADORs = oDestinos.DevolverTodosLosDestinos
        End If
    End If
    
    If ADORs Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestCod.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    ADORs.Close
    Set ADORs = Nothing
    sdbcDestCod.SelStart = 0
    sdbcDestCod.SelLength = Len(sdbcDestCod.Text)
    sdbcDestCod.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcDestCod_InitColumnProps()
    sdbcDestCod.DataFieldList = "Column 0"
    sdbcDestCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcDestCod_PositionList(ByVal Text As String)
PositionList sdbcDestCod, Text
End Sub

Private Sub sdbcDestCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
Dim ADORs As Ador.Recordset

    If sdbcDestCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el destino
    If basOptimizacion.gTipoDeUsuario <> Administrador Then
        If oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = oDestinos.DevolverTodosLosDestinos(Trim(sdbcDestCod.Columns(0).Text), , True, m_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
        Else
            sdbcDestCod.Text = ""
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    Else
        Set ADORs = oDestinos.DevolverTodosLosDestinos(Trim(sdbcDestCod.Columns(0).Text), , True)
    End If

    If ADORs Is Nothing Then
        bExiste = False
    Else
        bExiste = Not (ADORs.RecordCount = 0)
    End If
    
    If bExiste Then bExiste = (UCase(ADORs(0).Value) = UCase(Trim(sdbcDestCod.Text)))
    
    If Not bExiste Then
        sdbcDestCod.Text = ""
    Else
        bRespetarComboDest = True
        sdbcDestDen.Text = ADORs(1).Value
        
        sdbcDestCod.Columns(0).Value = sdbcDestCod.Text
        sdbcDestCod.Columns(1).Value = sdbcDestDen.Text
        
        bRespetarComboDest = False
        
    End If

    If Not ADORs Is Nothing Then
        ADORs.Close
        Set ADORs = Nothing
    End If
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcDestDen_Change()
    If Not bRespetarComboDest Then
    
        bRespetarComboDest = True
        sdbcDestCod.Text = ""
        bRespetarComboDest = False
        
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcDestDen_Click()
    If Not sdbcDestDen.DroppedDown Then
        sdbcDestCod = ""
        sdbcDestDen = ""
    End If
End Sub

Private Sub sdbcDestDen_CloseUp()

    If sdbcDestDen.Value = "..." Then
        sdbcDestDen.Text = ""
        Exit Sub
    End If
    
    If sdbcDestDen.Value = "" Then Exit Sub
    
    bRespetarComboDest = True
    sdbcDestDen.Text = sdbcDestDen.Columns(0).Text
    sdbcDestCod.Text = sdbcDestDen.Columns(1).Text
    bRespetarComboDest = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcDestDen_DropDown()
    Dim ADORs As Recordset

    Screen.MousePointer = vbHourglass
    
    sdbcDestDen.RemoveAll
    
    If basOptimizacion.gTipoDeUsuario <> Administrador Then
        If oUsuarioSummit.Persona Is Nothing Then
            If bCargarComboDesde Then
                Set ADORs = oDestinos.DevolverTodosLosDestinos(, Trim(sdbcDestDen.Text), , m_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , True)
            Else
                Set ADORs = oDestinos.DevolverTodosLosDestinos(, , , m_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , True)
            End If
        Else
            If bCargarComboDesde Then
                Set ADORs = oDestinos.DevolverTodosLosDestinos(, Trim(sdbcDestDen.Text), , m_bRDest, , , , , True)
            Else
                Set ADORs = oDestinos.DevolverTodosLosDestinos(, , , m_bRDest, , , , , True)
            End If
        End If
    Else
        If bCargarComboDesde Then
            Set ADORs = oDestinos.DevolverTodosLosDestinos(sCaracteresInicialesDen:=Trim(sdbcDestDen.Text), bOrdenadoPorDen:=True)
        Else
            Set ADORs = oDestinos.DevolverTodosLosDestinos(bOrdenadoPorDen:=True)
        End If
    End If
    
    If ADORs Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestDen.AddItem ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    ADORs.Close
    Set ADORs = Nothing



    sdbcDestDen.SelStart = 0
    sdbcDestDen.SelLength = Len(sdbcDestDen.Text)
    sdbcDestDen.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcDestDen_InitColumnProps()
    sdbcDestDen.DataFieldList = "Column 0"
    sdbcDestDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcDestDen_PositionList(ByVal Text As String)
PositionList sdbcDestDen, Text
End Sub

Private Sub sdbcEst_Click()
    If Not sdbcEst.DroppedDown Then
        sdbcEst.Text = ""
    End If
End Sub

Private Sub sdbcEst_CloseUp()
    
    If sdbcEst.Value = "..." Or Trim(sdbcEst.Value) = "" Then
        sdbcEst.Text = ""
        vNumEst = 30
        Exit Sub
    End If
    
    bRespetarComboEst = True
    sdbcEst.Text = sdbcEst.Columns(0).Text
    vNumEst = sdbcEst.Columns(1).Text
    bRespetarComboEst = False
    
End Sub

Private Sub sdbcEst_InitColumnProps()
  
    sdbcEst.DataFieldList = "Column 0"
    sdbcEst.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcEst_PositionList(ByVal Text As String)
PositionList sdbcEst, Text
End Sub

Public Sub sdbcEst_Validate(Cancel As Boolean)
Dim i As Integer
Dim bm As Variant
    
    vNumEst = 30
    If sdbcEst.Text <> "" Then

           For i = 0 To sdbcEst.Rows - 1
                bm = sdbcEst.GetBookmark(i)
                If UCase(sdbcEst.Text) = UCase(Mid(sdbcEst.Columns(0).CellText(bm), 1, Len(sdbcEst.Text))) Then
                    sdbcUsuCod.Bookmark = bm
                    vNumEst = sdbcEst.Columns(1).CellText(bm)
                    
                    Exit Sub
                End If
           Next i
           oMensajes.NoValida " " & Label3.caption
           sdbcEst.Text = ""
    End If
   
End Sub


Public Sub ProcesoSeleccionado()
Dim sCod As String
    
    Screen.MousePointer = vbHourglass
    
    If sdbcAnyo2 = "" Then Exit Sub
    
    sCod = sdbcCM & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcCM))
    sCod = CStr(sdbcAnyo2) & sCod & sdbcProceCod
       
    oProcesos.CargarDatosGeneralesProceso sdbcAnyo2, sdbcCM, val(sdbcProceCod)
    
    Set oProcesoSeleccionado = Nothing
    
    Set oProcesoSeleccionado = oProcesos.Item(sCod)
    
    If oProcesoSeleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCM_Change()
    
    If Not bRespetarCombo Then
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        sdbcProceCod = ""
        
    End If
    
End Sub
Private Sub sdbcCM_Click()
    If Not sdbcCM.DroppedDown Then
        sdbcCM = ""
    End If
End Sub

Private Sub sdbcCM_CloseUp()
    
    If sdbcCM.Value = "..." Then
        sdbcCM.Text = ""
        Exit Sub
    End If
    
    GMN1Seleccionado
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    bCargarComboDesde = False
    sdbcProceCod.Text = ""
    
      
End Sub

Private Sub sdbcCM_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
Dim oGruposMN1 As CGruposMatNivel1
       
    Screen.MousePointer = vbHourglass
    
    sdbcCM.RemoveAll
    
    
    If m_bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        If bCargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcCM.Text)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos)
        End If
    Else
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        If bCargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcCM.Text
        Else
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcCM.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not oGruposMN1.EOF Then
        sdbcCM.AddItem "..."
    End If

    sdbcCM.SelStart = 0
    sdbcCM.SelLength = Len(sdbcCM.Text)
    sdbcCM.Refresh
    
    Set oGruposMN1 = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCM_InitColumnProps()

    sdbcCM.DataFieldList = "Column 0"
    sdbcCM.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcCM_PositionList(ByVal Text As String)
PositionList sdbcCM, Text
End Sub
Private Sub sdbcCM_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    Dim oGruposMN1 As CGruposMatNivel1
    
    
    If sdbcCM.Text = "" Then Exit Sub
    
    If sdbcCM.Text = sdbcCM.Columns(0).Text Then
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    
    If m_bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcCM), , True)
        
        scod1 = sdbcCM.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcCM.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcCM.Text = ""
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdioma(6)
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)

            bCargarComboDesde = False
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
      
        oGMN1.Cod = sdbcCM
        Set oIBaseDatos = oGMN1
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcCM.Text = ""
            Screen.MousePointer = vbNormal
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
          
            bCargarComboDesde = False
        End If
    
    End If
    
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Public Sub GMN1Seleccionado()
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    
    
    oGMN1Seleccionado.Cod = sdbcCM
    
End Sub

Private Sub sdbcProceCod_Change()
    
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcProceDen.Text = ""
        Set oProcesoSeleccionado = Nothing
        bRespetarCombo = False
        
        bCargarComboDesde = True
    
    End If
    
End Sub

Private Sub sdbcProceCod_Click()
    If Not sdbcProceCod.DroppedDown Then
        sdbcProceCod = ""
    End If
End Sub

Private Sub sdbcProceCod_CloseUp()
    
    If sdbcProceCod.Value = "..." Or Trim(sdbcProceCod.Value) = "" Then
        sdbcProceCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
    sdbcProceCod.Text = sdbcProceCod.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False
        
End Sub

''' <summary>
''' Evento que se lanza cuando se despliega la sdbcProceCod. Carga el combo.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProceCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim lIdPerfil As Long
    
    sdbcProceCod.RemoveAll
     
    If sdbcAnyo2 = "" Then
        oMensajes.FaltanDatos (sIdioma(7))
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If sdbcCM.Text = "" Then
        oMensajes.FaltanDatos (gParametrosGenerales.gsABR_GMN1 & ":")
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, ParcialmenteCerrado, ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo2.Text, sdbcCM.Text, , , , val(sdbcProceCod), , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , m_bRUsuPerfUON, lIdPerfil
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, ParcialmenteCerrado, ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo2.Text, sdbcCM.Text, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , m_bRUsuPerfUON, lIdPerfil
    End If
   
    
    Codigos = oProcesos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    'If bCargarComboDesde And Not oProcesos.EOF Then
    If Not oProcesos.EOF Then
        sdbcProceCod.AddItem "..."
    End If

    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Text)
    sdbcProceCod.Refresh
    
    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcProceCod_InitColumnProps()

    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProceCod_PositionList(ByVal Text As String)
PositionList sdbcProceCod, Text
End Sub
Public Sub sdbcProceCod_Validate(Cancel As Boolean)
    Dim lIdPerfil As Long
    
    If Trim(sdbcProceCod.Text = "") Then Exit Sub
    
    If Not IsNumeric(sdbcProceCod) Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdioma(3)
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcCM.Text = "" Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If

    If sdbcProceCod.Value > giMaxProceCod Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdioma(3)
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
        
    If sdbcProceCod.Text = sdbcProceCod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
        bRespetarCombo = False
        If oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceDen.Columns(1).Text Then
        bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
        bRespetarCombo = False
        If oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proceso
    Screen.MousePointer = vbHourglass
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    oProcesos.CargarTodosLosProcesosDesde 1, ParcialmenteCerrado, ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo2, sdbcCM.Value, , , , val(sdbcProceCod), , True, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , m_bRUsuPerfUON, lIdPerfil
        
    If oProcesos.Count = 0 Then
        sdbcProceCod.Text = ""
        oMensajes.NoValido sIdioma(3)
        Set oProcesoSeleccionado = Nothing
    Else
        bRespetarCombo = True
        sdbcProceDen.Text = oProcesos.Item(1).Den
        
        sdbcProceCod.Columns(0).Text = sdbcProceCod.Text
        sdbcProceCod.Columns(1).Text = sdbcProceDen.Text
        
        bRespetarCombo = False
        Set oProcesoSeleccionado = Nothing
        Set oProcesoSeleccionado = oProcesos.Item(1)
      
        bCargarComboDesde = False
        ProcesoSeleccionado
    End If
    bCargarComboDesde = False
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcProceDen_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcProceCod.Text = ""
        Set oProcesoSeleccionado = Nothing
        bRespetarCombo = False
        bCargarComboDesde = True
    End If
    
End Sub

Private Sub sdbcProceDen_Click()
    If Not sdbcProceDen.DroppedDown Then
        sdbcProceCod = ""
        sdbcProceDen = ""
    End If
End Sub

Private Sub sdbcProceDen_CloseUp()
    
    If sdbcProceDen.Value = "....." Or Trim(sdbcProceDen.Value) = "" Then
        sdbcProceDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProceDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcProceCod.Text = sdbcProceDen.Columns(1).Text
    sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False
        
End Sub

''' <summary>
''' Evento que se lanza cuando se despliega la sdbcProceDen. Carga el combo.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProceDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim lIdPerfil As Long
    
    sdbcProceDen.RemoveAll
    
    If sdbcAnyo2 = "" Then
        oMensajes.FaltanDatos (sIdioma(7))
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    If sdbcCM.Text = "" Then
        oMensajes.FaltanDatos (gParametrosGenerales.gsABR_GMN1 & ":")
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, ParcialmenteCerrado, ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo2.Text, sdbcCM.Text, , , , sdbcProceDen.Text, , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , m_bRUsuPerfUON, lIdPerfil
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, ParcialmenteCerrado, ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo2.Text, sdbcCM.Text, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , m_bRUsuPerfUON, lIdPerfil
    End If
            
    Codigos = oProcesos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    'If bCargarComboDesde And Not oProcesos.EOF Then
    If Not oProcesos.EOF Then
        sdbcProceDen.AddItem "....."
    End If

    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Text)
    sdbcProceDen.Refresh
    
    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcProceDen_InitColumnProps()

    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProceDen_PositionList(ByVal Text As String)
PositionList sdbcProceDen, Text
End Sub
Private Sub sdbcProveCod_Change()
    
    If Not bRespetarComboProve Then
    
        bRespetarComboProve = True
        sdbcProveDen.Text = ""
        bRespetarComboProve = False
        
        bCargarComboDesde = True
    End If
    
End Sub

Private Sub sdbcProveCod_Click()
     
     If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
     End If
End Sub
Private Sub sdbcProveCod_CloseUp()
    
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    bRespetarComboProve = False
    
    DoEvents
    
    ProveedorSeleccionado
    
    bCargarComboDesde = False
End Sub

''' <summary>
''' cargar el combo de codigos proveedor
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcProveCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcProveCod.RemoveAll
    
    
    If bCargarComboDesde Then
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProveCod.Text), , , , , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , , , , , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If

    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub



Private Sub sdbcProveDen_CloseUp()

    
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    bRespetarComboProve = False
    
    DoEvents
    
    ProveedorSeleccionado
    
    bCargarComboDesde = False
End Sub
Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub

''' <summary>
''' Validar el codigo proveedor
''' </summary>
''' <param name="Cancel">cancelar la edicion</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcProveCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el proveedor
    
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    

    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        bRespetarComboProve = True
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Value = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Value = sdbcProveDen.Text
        
        bRespetarComboProve = False
        ProveedorSeleccionado
    End If

    bCargarComboDesde = False
End Sub

Private Sub sdbcProveDen_Change()
    
    If Not bRespetarComboProve Then
    
        bRespetarComboProve = True
        sdbcProveCod.Text = ""
        bRespetarComboProve = False
        
        bCargarComboDesde = True
    End If
    
End Sub

Private Sub sdbcProveDen_Click()
    
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

''' <summary>
''' Cargar el combo de denominaciones proveedor
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcProveDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcProveDen.RemoveAll
    If bCargarComboDesde Then
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveDen.Text), , True, , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , , , True, , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    End If

    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next
    
    If Not oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveCod.Refresh
    
    bCargarComboDesde = False
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub

Private Sub ProveedorSeleccionado()
    
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    
End Sub
Public Sub CargarProveedorConBusqueda()

    Set oProves = frmPROVEBuscar.oProveEncontrados
   
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveCod_Validate False
    ProveedorSeleccionado
    
End Sub

Public Sub CargarProcesoConBusqueda()

    Set oProcesos = frmPROCEBuscar.oProceEncontrados
    Set frmPROCEBuscar.oProceEncontrados = Nothing
    sdbcAnyo2 = oProcesos.Item(1).Anyo
    sdbcCM.Text = oProcesos.Item(1).GMN1Cod
    sGMN1Cod = oProcesos.Item(1).GMN1Cod
    GMN1Seleccionado
    bRespetarCombo = True
    sdbcProceCod = oProcesos.Item(1).Cod
    sdbcProceDen = oProcesos.Item(1).Den
    bRespetarCombo = False
    ProcesoSeleccionado
            
End Sub
Private Sub sdbcArtiCod_Click()
    If Not sdbcArtiCod.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiCod_CloseUp()
 
    If sdbcArtiCod.Value = "..." Then
        sdbcArtiCod.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiCod.Value = "" Then Exit Sub
       
    bRespetarCombo = True
    sdbcArtiDen.Text = sdbcArtiCod.Columns(1).Text
    sdbcArtiCod.Text = sdbcArtiCod.Columns(0).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcArtiCod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcArtiDen.Text = ""
        bRespetarCombo = False

        bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcArtiCod_DropDown()
Dim oArt As CArticulo
    
    sdbcArtiCod.RemoveAll

    Screen.MousePointer = vbHourglass
    GMN4Seleccionado
    
    If oGrupoMN4seleccionado Is Nothing Then Exit Sub
    
    If bCargarComboDesde Then
        oGrupoMN4seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcArtiCod.Text)
    Else
        oGrupoMN4seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos
    End If
    
    
    For Each oArt In oGrupoMN4seleccionado.ARTICULOS
   
        sdbcArtiCod.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den
   
    Next

    sdbcArtiCod.SelStart = 0
    sdbcArtiCod.SelLength = Len(sdbcArtiCod.Text)
    sdbcArtiCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiCod_InitColumnProps()
    
    sdbcArtiCod.DataFieldList = "Column 0"
    sdbcArtiCod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcArtiCod_PositionList(ByVal Text As String)
PositionList sdbcArtiCod, Text
End Sub

Private Sub sdbcArtiCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
    If sdbcArtiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    
    If Not oGrupoMN4seleccionado Is Nothing Then
        oGrupoMN4seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, sdbcArtiCod, , True
        bExiste = Not (oGrupoMN4seleccionado.ARTICULOS.Count = 0)
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcArtiCod.Text = ""
    Else
        bRespetarCombo = True
        sdbcArtiDen.Text = oGrupoMN4seleccionado.ARTICULOS.Item(1).Den
        
        sdbcArtiCod.Columns(0).Value = sdbcArtiCod.Text
        sdbcArtiCod.Columns(1).Value = sdbcArtiDen.Text
        
        bRespetarCombo = False

    End If
    If Not oGrupoMN4seleccionado Is Nothing Then
        Set oGrupoMN4seleccionado.ARTICULOS = Nothing
    End If
    
    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
            
End Sub

Private Sub sdbcArtiDen_Click()
    
    If Not sdbcArtiDen.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiDen_CloseUp()
    
    If sdbcArtiDen.Value = "..." Then
        sdbcArtiDen.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcArtiCod.Text = sdbcArtiDen.Columns(1).Text
    sdbcArtiDen.Text = sdbcArtiDen.Columns(0).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcArtiDen_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcArtiCod.Text = ""
        bRespetarCombo = False

        bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcArtiDen_DropDown()
    Dim oArt As CArticulo
    
    sdbcArtiDen.RemoveAll
        
    Screen.MousePointer = vbHourglass
    GMN4Seleccionado

    If oGrupoMN4seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
        oGrupoMN4seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , sdbcArtiDen, , True
    Else
        oGrupoMN4seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , True
    End If
    
    
    For Each oArt In oGrupoMN4seleccionado.ARTICULOS
   
        sdbcArtiDen.AddItem oArt.Den & Chr(m_lSeparador) & oArt.Cod
   
    Next

    sdbcArtiDen.SelStart = 0
    sdbcArtiDen.SelLength = Len(sdbcArtiDen.Text)
    sdbcArtiDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiDen_InitColumnProps()
    
    sdbcArtiDen.DataFieldList = "Column 0"
    sdbcArtiDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcArtiDen_PositionList(ByVal Text As String)
PositionList sdbcArtiDen, Text
End Sub

Private Sub GMN4Seleccionado()
    Set oGrupoMN4seleccionado = Nothing
    Set oGrupoMN4seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    oGrupoMN4seleccionado.GMN1Cod = sGMN1Cod
    oGrupoMN4seleccionado.GMN2Cod = sGMN2Cod
    oGrupoMN4seleccionado.GMN3Cod = sGMN3Cod
    oGrupoMN4seleccionado.Cod = sGMN4Cod
End Sub

Private Sub sdbcUsuCod_Change()

    If Not bRespetarCombo Then
        CargarComboDesde = True
    End If
    
End Sub
Private Sub sdbcUsucod_CloseUp()

    If sdbcUsuCod.Value = "..." Then
        sdbcUsuCod.Text = ""
        Exit Sub
    End If
    
    If sdbcUsuCod.Text = "" Then
        CargarComboDesde = False
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcUsuCod.Text = sdbcUsuCod.Columns(1).Text
    bRespetarCombo = False
    
    CargarComboDesde = False
    
End Sub

Private Sub sdbcUsuCod_DropDown()
    Dim oRes As Ador.Recordset
            
    sdbcUsuCod.RemoveAll
    
    'Cargamos los codigos
    Screen.MousePointer = vbHourglass
 
    If CargarComboDesde Then
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(Trim(sdbcUsuCod.Text), False, TipoOrdenacionPersonas.OrdPorCod, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario)
    Else
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(, False, TipoOrdenacionPersonas.OrdPorCod, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario)
    End If
    While Not oRes.EOF
        sdbcUsuCod.AddItem oRes("CODPER").Value & Chr(m_lSeparador) & oRes("NOM").Value & " " & oRes("APE").Value & Chr(m_lSeparador) & oRes("USUCOD").Value
        oRes.MoveNext
    Wend

    oRes.Close
    Set oRes = Nothing
    sdbcUsuCod.SelStart = 0
    sdbcUsuCod.SelLength = Len(sdbcUsuCod.Text)
    sdbcUsuCod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcusucod_InitColumnProps()

    sdbcUsuCod.DataFieldList = "Column 1"
    sdbcUsuCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcusucod_PositionList(ByVal Text As String)
PositionList sdbcUsuCod, Text
End Sub

Public Sub sdbcusucod_Validate(Cancel As Boolean)

    Dim oRes As Ador.Recordset
    Dim bExiste As Boolean
        
    If sdbcUsuCod.Text = "" Then
        CargarComboDesde = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el usuario
    
    Screen.MousePointer = vbHourglass
    Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(sdbcUsuCod.Columns(0).Value, True, , m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario)
    
    bExiste = (Not (oRes.EOF))
    
    If Not bExiste Then
        sdbcUsuCod.Text = ""
    Else
        bRespetarCombo = True
        sdbcUsuCod.Text = oRes("NOM").Value & " " & oRes("APE").Value
        sdbcUsuCod.Columns(0).Value = oRes("CODPER").Value
        sdbcUsuCod.Columns(1).Value = oRes("NOM").Value & " " & oRes("APE").Value
        sdbcUsuCod.Columns(2).Value = oRes("USUCOD").Value
        bRespetarCombo = False
        CargarComboDesde = False
    End If
    
    oRes.Close
    Set oRes = Nothing
    Screen.MousePointer = vbNormal

End Sub


Private Sub txtNumCesta_Change()
    If txtNumCesta.Text <> "" Then
        If Not IsNumeric(txtNumCesta) Then
            oMensajes.NoValido sIdioma(1)
            If Me.Visible Then txtNumCesta.SetFocus
            Exit Sub
        End If
    End If
End Sub

''' <summary>
''' Evento que salta al modificar el valor la partida
''' </summary>
''' <remarks>Tiempo m�ximo= 0,1seg.</remarks>
Private Sub txtPartida_Change()
    m_bControlPartidaFiltro = True
End Sub

''' <summary>
''' Evento que salta modificar un valor de la caja de texto partidas
''' Valida que la partida introducida sea correcta.
''' Internamente se almacena un array con el valor de la unica partida que tenemos  (estructura PRES0-PRES1-PRES2-PRES3-PRES4)
''' </summary>
''' <param name="Cancel">parametro del evento</param>
''' <remarks>Tiempo m�ximo= 0,2seg.</remarks>
Private Sub txtPartida_Validate(Cancel As Boolean)
    Dim sPres0 As String
    Dim oContratosPres As CContratosPres
    Dim bEncontrado As Boolean
    Dim arrCC() As String
    Dim i As Byte
    Dim sUsu As String
    Dim sCod As String
    Dim sAux() As String
    
    If Not m_bControlPartidaFiltro Then Exit Sub
    If txtPartida.Text = "" Then Exit Sub
    
    sPres0 = cmdBuscarPartida.Tag
    
    sAux = Split(txtPartida.Text, " - ")
    If UBound(sAux) > 0 Then
        sCod = sAux(0)
    Else
        sCod = txtPartida.Text
    End If
    If basOptimizacion.gTipoDeUsuario <> Administrador Then
        sUsu = basOptimizacion.gvarCodUsuario
    End If
    

    Set oContratosPres = oFSGSRaiz.Generar_CContratosPres
    
    If m_sUON_CC <> "" Then
        arrCC = Split(m_sUON_CC, "-")
    Else
        ReDim arrCC(4)
    End If


    oContratosPres.CargarTodosLosContratosCentros sPres0, m_iNivelImputacion, sUsu, gParametrosInstalacion.gIdioma, arrCC(0), arrCC(1), arrCC(2), arrCC(3), sCod, , True
    If oContratosPres.Count > 0 Then
        bEncontrado = True
    End If
    
    
    
    If bEncontrado Then
        ReDim m_arrCentrosCoste(oContratosPres.Count)
        If oContratosPres.Count = 1 Then
            m_sUON_CC = NullToStr(oContratosPres.Item(1).UON1) & "-" & NullToStr(oContratosPres.Item(1).UON2) & "-" & NullToStr(oContratosPres.Item(1).UON3) & "-" & NullToStr(oContratosPres.Item(1).UON4)
            If Not NoHayParametro(oContratosPres.Item(1).UON4) Then
                sCod = oContratosPres.Item(1).UON4
            ElseIf Not NoHayParametro(oContratosPres.Item(1).UON3) Then
                sCod = oContratosPres.Item(1).UON3
            ElseIf Not NoHayParametro(oContratosPres.Item(1).UON2) Then
                sCod = oContratosPres.Item(1).UON2
            Else
                sCod = oContratosPres.Item(1).UON1
            End If
            m_arrCentrosCoste(0) = NullToStr(oContratosPres.Item(1).UON1) & "-" & NullToStr(oContratosPres.Item(1).UON2) & "-" & NullToStr(oContratosPres.Item(1).UON3) & "-" & NullToStr(oContratosPres.Item(1).UON4)
'            txtCentroCoste.Text = sCod & " - " & DevolverDenCC(oContratosPres.Item(1).UON1, oContratosPres.Item(1).UON2, oContratosPres.Item(1).UON3, oContratosPres.Item(1).UON4, oContratosPres.Item(1).DenUON, oContratosPres.Item(1).DenUON, oContratosPres.Item(1).DenUON, oContratosPres.Item(1).DenUON)
            txtCentroCoste.Text = sCod & " - " & oContratosPres.Item(1).DenUON
        Else
            lstCentroCoste.clear
            Dim sItemCentro As String
            txtCentroCoste.Text = ""
            For i = 1 To oContratosPres.Count
                If Not NoHayParametro(oContratosPres.Item(i).UON4) Then
                    sCod = oContratosPres.Item(i).UON4
                ElseIf Not NoHayParametro(oContratosPres.Item(i).UON3) Then
                    sCod = oContratosPres.Item(i).UON3
                ElseIf Not NoHayParametro(oContratosPres.Item(i).UON2) Then
                    sCod = oContratosPres.Item(i).UON2
                Else
                    sCod = oContratosPres.Item(i).UON1
                End If
                m_arrCentrosCoste(i - 1) = NullToStr(oContratosPres.Item(i).UON1) & "-" & NullToStr(oContratosPres.Item(i).UON2) & "-" & NullToStr(oContratosPres.Item(i).UON3) & "-" & NullToStr(oContratosPres.Item(i).UON4)
                sItemCentro = sCod & " - " & DevolverDenCC(oContratosPres.Item(i).UON1, oContratosPres.Item(i).UON2, oContratosPres.Item(i).UON3, oContratosPres.Item(i).UON4, oContratosPres.Item(i).DenUON, oContratosPres.Item(i).DenUON, oContratosPres.Item(i).DenUON, oContratosPres.Item(i).DenUON)
                lstCentroCoste.AddItem sItemCentro
            Next
            lstCentroCoste.Height = 480
            lstCentroCoste.Visible = True
            If Me.Visible Then lstCentroCoste.SetFocus
    
        End If
        txtPartida.Text = oContratosPres.Item(1).Cod & " - " & oContratosPres.Item(1).Den
        m_arrPartidas(0) = sPres0 & "-" & NullToStr(oContratosPres.Item(1).Pres1) & "-" & NullToStr(oContratosPres.Item(1).Pres2) & "-" & NullToStr(oContratosPres.Item(1).Pres3) & "-" & NullToStr(oContratosPres.Item(1).Pres4)
    
    
    Else
        txtPartida.Text = ""
        m_arrPartidas(0) = ""
    End If
    Set oContratosPres = Nothing
    m_bControlPartidaFiltro = False
End Sub


Private Sub txtPedido_Change()
    
    If txtPedido.Text <> "" Then
        If Not IsNumeric(txtPedido) Then
            oMensajes.NoValido sIdioma(2)
            If Me.Visible Then txtPedido.SetFocus
            Exit Sub
        End If
    End If
End Sub

''' <summary>
''' Crea un Texto con los filtros activos
''' </summary>
''' <returns>Texto con los filtros activos</returns>
''' <remarks>Llamada desde: cmdObtener_Click ; Tiempo m�ximo: 0</remarks>
Private Function GenerarTextoSeleccion() As String

    Dim sUnion As String

    sSeleccion = ""
    sUnion = ""
    
    If sdbcAnyo.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & Label1.caption & " " & sdbcAnyo.Text
    End If
    
    If txtNumCesta.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & sIdioma(5) & " " & txtNumCesta.Text
    End If
    
    If txtPedido.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & sIdioma(4) & " " & txtPedido.Text
    End If
    
    If txtReferencia.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & Label17.caption & " " & txtReferencia.Text
    End If
    
    If txtNumExt.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & Label18.caption & " " & txtNumExt.Text
    End If
    
    If txtFecEmiD.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & label2.caption & " " & txtFecEmiD.Text
    End If
    
    If txtFecEmiH.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & Label13.caption & " " & txtFecEmiH.Text
    End If
    
    If txtFecEntD.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & Label14.caption & " " & txtFecEntD.Text
    End If
    
    If txtFecEntH.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & Label15.caption & " " & txtFecEntH.Text
    End If
    
    If chkPedDirec.Value = vbChecked Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & chkPedDirec.caption
    ElseIf chkPedAprov.Value = vbChecked Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & chkPedAprov.caption
    End If
    If chkPedPM.Value = vbChecked Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & chkPedPM.caption
    End If
    
    If sdbcEst.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & Label3 & " " & sdbcEst.Text
    End If
    
    If lblCategoria.caption <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & Label12.caption & " " & lblCategoria.caption
    End If
        
    If sdbcProveCod.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & Label8.caption & " " & sdbcProveCod.Text & "-" & sdbcProveDen.Text
    End If
    
    If lblMaterial.caption <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & Label9.caption & " " & lblMaterial.caption
    End If
    
    If sdbcArtiCod.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & label10.caption & " " & sdbcArtiCod.Text & "-" & sdbcArtiDen.Text
    End If
    
    If sdbcDestCod.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & Label11.caption & " " & sdbcDestCod.Text & "-" & sdbcDestDen.Text
    End If
    
    If sdbcAnyo2.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & sIdioma(7) & ": " & sdbcAnyo2.Text
    End If
    
    If sdbcCM.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & gParametrosGenerales.gsABR_GMN1 & ":" & " " & sdbcCM.Text
    End If

    If sdbcProceCod.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & sIdioma(3) & ": " & sdbcProceCod.Text & "-" & sdbcProceDen.Text
    End If
     
    If txtCodERP.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblCodERP.caption & " " & txtCodERP.Text
    End If
    
    If sdbcEmpresa.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblEmpresa.caption & " " & sdbcEmpresa.Text
    End If
    
    If sdbcUsuCod.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblAprov.caption & " " & sdbcUsuCod.Text
    End If
        
    If sdbcReceptor.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblReceptor.caption & " " & sdbcReceptor.Text
    End If
    
    If lblSolicitud.caption <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblSolicitudTitle.caption & " " & lblSolicitud.caption
    End If
    
    GenerarTextoSeleccion = sSeleccion

End Function

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde=form_load; Tiempo m�ximo=0seg.</remarks>

Private Function ConfigurarSeguridad()
    m_bVerSolic = False
    m_bProvMat = False
        
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGRestMatComp)) Is Nothing) Then
        m_bRMat = True
    End If
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestAsignado)) Is Nothing) Then
        m_bRAsig = True
    End If
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestResponsable)) Is Nothing) Then
        m_bRCompResp = True
    End If
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestEqpAsignado)) Is Nothing) Then
        m_bREqpAsig = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestUsuAper)) Is Nothing) Then
        m_bRUsuAper = True
    End If
    
    m_bRUsuUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestUsuUO)) Is Nothing)
    m_bRUsuPerfUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestUsuPerfUO)) Is Nothing)
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestUsuDep)) Is Nothing) Then
        m_bRUsuDep = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGRestUsuAprov)) Is Nothing) Then
        m_bRUsuAprov = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGSoloAprov)) Is Nothing) Then
        m_bPedAprov = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGSoloDirectos)) Is Nothing) Then
        m_bPedDirec = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGErp)) Is Nothing) Then
        m_bPedErp = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegSolicMostrar)) Is Nothing) Then
        m_bVerSolic = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGRestDest)) Is Nothing) Then
        m_bRDest = True
    End If
    
    m_bRuo = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestUO)) Is Nothing)
    m_bRPerfUO = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestPerfUO)) Is Nothing)
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestDep)) Is Nothing) Then
        m_bRDep = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegVerEmpresaUsu)) Is Nothing) Then
        m_bREmpresa = True
    End If
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestProvMatComp)) Is Nothing) Then
        m_bProvMat = True
    End If
End Function

Private Sub sdbcEmpresa_Change()
    If Not bRespetarComboEmpresa Then
        bRespetarComboEmpresa = True
        bRespetarComboEmpresa = False
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcEmpresa_Click()
    If Not sdbcEmpresa.DroppedDown Then
        sdbcEmpresa = ""
    End If
End Sub


Private Sub sdbcEmpresa_CloseUp()
    If sdbcEmpresa.Value = "..." Or sdbcEmpresa.Text = "" Then
        sdbcEmpresa.Text = ""
        Exit Sub
    End If
    
    bRespetarComboEmpresa = True
    sdbcEmpresa.Text = sdbcEmpresa.Columns(1).Value
    bRespetarComboEmpresa = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcEmpresa_DropDown()
    Dim oEmpresa As CEmpresa
    
    Screen.MousePointer = vbHourglass
    
    sdbcEmpresa.RemoveAll
    
    If bCargarComboDesde Then
        If m_bREmpresa = True Then
            oEmpresas.CargarTodasLasEmpresasDesde sdbcEmpresa.Text, , , , , True, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        Else
            oEmpresas.CargarTodasLasEmpresasDesde sdbcEmpresa.Text, , , , , True
        End If
    Else
        If m_bREmpresa = True Then
            oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        Else
            oEmpresas.CargarTodasLasEmpresasDesde
        End If
    End If
    
    For Each oEmpresa In oEmpresas
        sdbcEmpresa.AddItem oEmpresa.Id & Chr(m_lSeparador) & oEmpresa.NIF & Chr(m_lSeparador) & oEmpresa.Den
    Next
        
    sdbcEmpresa.SelStart = 0
    sdbcEmpresa.SelLength = Len(sdbcEmpresa.Text)
    sdbcEmpresa.Refresh
    
    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcEmpresa_InitColumnProps()
    sdbcEmpresa.DataFieldList = "Column 0"
    sdbcEmpresa.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcEmpresa_PositionList(ByVal Text As String)
PositionList sdbcEmpresa, Text
End Sub

Public Sub sdbcEmpresa_Validate(Cancel As Boolean)
    
    If sdbcEmpresa.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    
    oEmpresas.CargarTodasLasEmpresasDesde Trim(sdbcEmpresa.Text)

    If oEmpresas.Count = 0 Then
        sdbcEmpresa.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sEmpresa
        
    Else
        bRespetarComboEmpresa = True

'        sdbcEmpresaDen.Text = oEmpresas.Item(1).Den
        sdbcEmpresa.Columns(0).Value = oEmpresas.Item(1).Id
        sdbcEmpresa.Columns(1).Value = oEmpresas.Item(1).NIF
        sdbcEmpresa.Columns(2).Value = oEmpresas.Item(1).Den

        bRespetarComboEmpresa = False

        bCargarComboDesde = False
    End If

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcReceptor_Change()
    If Not bRespetarComboRecep Then
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcReceptor_CloseUp()
       
    If sdbcReceptor.Value = "..." Then
        sdbcReceptor.Text = ""
        Exit Sub
    End If
    
    If sdbcReceptor.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If
    
    bRespetarComboRecep = True
    sdbcReceptor.Text = sdbcReceptor.Columns(1).Text
    bRespetarComboRecep = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcReceptor_DropDown()
    Dim oRes As Ador.Recordset
            
    sdbcReceptor.RemoveAll
    
    'Cargamos los codigos
    Screen.MousePointer = vbHourglass

    If bCargarComboDesde Then
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(Trim(sdbcReceptor.Text), False, TipoOrdenacionPersonas.OrdPorCod, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , True)
    Else
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(, False, TipoOrdenacionPersonas.OrdPorCod, m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , True)
    End If

    While Not oRes.EOF
        sdbcReceptor.AddItem oRes("CODPER").Value & Chr(m_lSeparador) & oRes("NOM").Value & " " & oRes("APE").Value & Chr(m_lSeparador) & oRes("USUCOD").Value
        oRes.MoveNext
    Wend

    oRes.Close
    Set oRes = Nothing
    
    sdbcReceptor.SelStart = 0
    sdbcReceptor.SelLength = Len(sdbcReceptor.Text)
    sdbcReceptor.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcReceptor_InitColumnProps()
    sdbcReceptor.DataFieldList = "Column 1"
    sdbcReceptor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcReceptor_PositionList(ByVal Text As String)
PositionList sdbcReceptor, Text
End Sub

Public Sub sdbcReceptor_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oRes As Ador.Recordset
    
    
    If sdbcReceptor.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el usuario
    
    Screen.MousePointer = vbHourglass
    Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(sdbcReceptor.Columns(0).Value, True, , m_bRuo, m_bRDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , True)
    
    bExiste = Not (oRes.EOF)
    
    If Not bExiste Then
        sdbcReceptor.Text = ""
    Else
        bRespetarComboRecep = True
        sdbcReceptor.Text = oRes("NOM").Value & " " & oRes("APE").Value
        sdbcReceptor.Columns(0).Value = oRes("CODPER").Value
        sdbcReceptor.Columns(1).Value = oRes("NOM").Value & " " & oRes("APE").Value
        sdbcReceptor.Columns(2).Value = oRes("USUCOD").Value
        bRespetarComboRecep = False
        bCargarComboDesde = False
    End If
    
    oRes.Close
    Set oRes = Nothing
    Screen.MousePointer = vbNormal
End Sub




''' <summary>
''' Carga el combo de tipo de pedidos // Muestra el codigo, denominacion y distintos opciones del pedido (Concepto, almacenable, recepcionable)
''' </summary>
''' <remarks>Tiempo m�ximo=0,1seg.</remarks>
Private Sub sdbcTipoPedido_DropDown()
    Dim oTipoPedido As CTipoPedido
    Dim oTiposPedido As CTiposPedido
    
    Screen.MousePointer = vbHourglass
    
    sdbcTipoPedido.RemoveAll
    Set oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
    oTiposPedido.CargarTodosLosTiposPedidos (gParametrosInstalacion.gIdioma)

    sdbcTipoPedido.AddItem 0 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""


    For Each oTipoPedido In oTiposPedido
        sdbcTipoPedido.AddItem oTipoPedido.Id & Chr(m_lSeparador) & oTipoPedido.Cod & Chr(m_lSeparador) & oTipoPedido.Den & _
        Chr(m_lSeparador) & m_arrConcep(oTipoPedido.CodConcep) & Chr(m_lSeparador) & m_arrAlmac(oTipoPedido.CodAlmac) & Chr(m_lSeparador) & m_arrRecep(oTipoPedido.CodRecep) & _
        Chr(m_lSeparador) & oTipoPedido.CodConcep & Chr(m_lSeparador) & oTipoPedido.CodAlmac & Chr(m_lSeparador) & oTipoPedido.CodRecep
    Next

    sdbcTipoPedido.SelStart = 0
    sdbcTipoPedido.SelLength = Len(sdbcTipoPedido.Text)
    sdbcTipoPedido.Refresh
    
    Set oTipoPedido = Nothing
    Set oTiposPedido = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

''' <summary>
''' Almacena el valor de tipo de pedido seleccionado
''' </summary>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub sdbcTipoPedido_CloseUp()
    If sdbcTipoPedido.Value = "0" Or Trim(sdbcTipoPedido.Value) = "" Then
        sdbcTipoPedido.Text = ""
        m_lTipoPedido = 0
        Exit Sub
    End If
    m_lTipoPedido = sdbcTipoPedido.Columns("ID").Value
    sdbcTipoPedido.Text = sdbcTipoPedido.Columns(1).Value & " - " & sdbcTipoPedido.Columns(2).Value
End Sub

Private Sub sdbcTipoPedido_InitColumnProps()
    sdbcTipoPedido.DataFieldList = "Column 0"
    sdbcTipoPedido.DataFieldToDisplay = "Column 2"
End Sub

''' <summary>
''' Valida que el Tipo de pedido introducida sea el correcto
''' </summary>
''' <param name="Cancel">Propio del evento</param>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Public Sub sdbcTipoPedido_Validate(Cancel As Boolean)
Dim i As Integer
Dim bm As Variant
Dim sAux() As String
Dim sCod As String
Dim oTipoPedido As CTipoPedido
Dim oTiposPedido As CTiposPedido

sAux = Split(sdbcTipoPedido.Text, " - ")
If UBound(sAux) > 0 Then
    sCod = sAux(0)
Else
    sCod = sdbcTipoPedido.Text
End If
    
    m_lTipoPedido = 0
    If sdbcTipoPedido.Text <> "" Then
    
        If oTiposPedido Is Nothing Then
            Set oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
            oTiposPedido.CargarTodosLosTiposPedidos (gParametrosInstalacion.gIdioma)
        
            sdbcTipoPedido.AddItem 0 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        
            For Each oTipoPedido In oTiposPedido
                sdbcTipoPedido.AddItem oTipoPedido.Id & Chr(m_lSeparador) & oTipoPedido.Cod & Chr(m_lSeparador) & oTipoPedido.Den & _
                Chr(m_lSeparador) & m_arrConcep(oTipoPedido.CodConcep) & Chr(m_lSeparador) & m_arrAlmac(oTipoPedido.CodAlmac) & Chr(m_lSeparador) & m_arrRecep(oTipoPedido.CodRecep) & _
                Chr(m_lSeparador) & oTipoPedido.CodConcep & Chr(m_lSeparador) & oTipoPedido.CodAlmac & Chr(m_lSeparador) & oTipoPedido.CodRecep
            Next
        
            sdbcTipoPedido.SelStart = 0
            sdbcTipoPedido.SelLength = Len(sdbcTipoPedido.Text)
            sdbcTipoPedido.Refresh
            
            Set oTipoPedido = Nothing
            Set oTiposPedido = Nothing
        End If
    
        sdbcTipoPedido.MoveFirst
        For i = 0 To sdbcTipoPedido.Rows - 1
         bm = sdbcTipoPedido.GetBookmark(i)
         If UCase(sCod) = UCase(Mid(sdbcTipoPedido.Columns(1).CellText(bm), 1, Len(sdbcTipoPedido.Text))) Then
             sdbcTipoPedido.Bookmark = bm
             sdbcTipoPedido.Text = sdbcTipoPedido.Columns(1).CellText(bm) & " - " & sdbcTipoPedido.Columns(2).CellText(bm)
             m_lTipoPedido = sdbcTipoPedido.Columns(0).CellText(bm)
             
             Exit Sub
         End If
        Next i
        sdbcTipoPedido.Text = ""
    End If
End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">Valor de la celda</param>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub sdbcTipoPedido_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcTipoPedido.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipoPedido.Rows - 1
            bm = sdbcTipoPedido.GetBookmark(i)
            If UCase(Text) = UCase(sdbcTipoPedido.Columns(1).CellText(bm) & " - " & sdbcTipoPedido.Columns(2).CellText(bm)) Then
                sdbcTipoPedido.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

''' <summary>
''' Carga las partidas que hay en el sistema.
''' Si hay una unica partida, se visualiza un text sino se muestra una grid con los distintos contratos que hay
''' </summary>
''' <remarks>Llamada desde=Form_load; Tiempo m�ximo= 0,3seg.</remarks>
Private Sub CargarPartidas()
    Dim opres5 As cPresConceptos5Nivel0
    Dim sUsu As String
    
    Set opres5 = oFSGSRaiz.Generar_CPresConceptos5Nivel0

    If basOptimizacion.gTipoDeUsuario <> Administrador Then
        sUsu = basOptimizacion.gvarCodUsuario
    End If
    
    If g_oParametrosSM.Count > 0 Then
        ReDim m_arrPartidas(g_oParametrosSM.Count)
        cmdBuscarPartida.Visible = False
        cmdLimpiarPartida.Visible = False
        If g_oParametrosSM.Count > 1 Then
            Dim iPosicion As Integer
            cmdBuscarPartida.Tag = ""
            Dim oParamSM As CParametroSM
            For Each oParamSM In g_oParametrosSM
                 sdbgPartidas.AddItem oParamSM.PRES5 & Chr(m_lSeparador) & oParamSM.NomNivelImputacion & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & iPosicion & Chr(m_lSeparador) & oParamSM.impnivel

                iPosicion = iPosicion + 1
            Next
            ConfiguracionMasDe1Partida
        Else
            lblContrato.caption = g_oParametrosSM.Item(1).NomNivelImputacion & ":"
            m_iNivelImputacion = g_oParametrosSM.Item(1).impnivel
            cmdBuscarPartida.Visible = True
            cmdLimpiarPartida.Visible = True
            sdbgPartidas.Visible = False
            cmdBuscarPartida.Tag = g_oParametrosSM.Item(1).PRES5
        End If
    End If
    
    Set opres5 = Nothing
End Sub


''' <summary>
''' Si es el caso que hay varias partidas, configura la grid y oculta el caso de que sea un unico contrato
''' </summary>
''' <remarks>Llamada desde=CargarPartidas; Tiempo m�ximo= 0,1seg.</remarks>
Private Sub ConfiguracionMasDe1Partida()
    lblContrato.Visible = False
    txtPartida.Visible = False
    cmdBuscarPartida.Visible = False
    cmdLimpiarPartida.Visible = False
    sdbgPartidas.Visible = True
    
    sdbgPartidas.Top = lblContrato.Top
    sdbgPartidas.Left = lblContrato.Left
    sdbgPartidas.Height = 3 * (sdbgPartidas.RowHeight + 10)
        
    sdbgPartidas.Columns(1).Width = sdbgPartidas.Width * 0.25
    sdbgPartidas.Columns(2).Width = sdbgPartidas.Width * 0.7 - 100
    
End Sub

''' <summary>
''' No se desea q se recarge todos los valores solo los q empiezan por lo ya escrito
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcGestor_Change()
    If Not bRespetarComboGestor Then
        bCargarComboDesde = True
    End If
End Sub

''' <summary>
''' Al cerrar el combo, selecciona.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcGestor_CloseUp()
        
    If sdbcGestor.Value = "..." Then
        sdbcGestor.Text = ""
        Exit Sub
    End If
    
    If sdbcGestor.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If
    
    bRespetarComboGestor = True
    sdbcGestor.Text = sdbcGestor.Columns(1).Text
    bRespetarComboGestor = False
    
    bCargarComboDesde = False
End Sub

''' <summary>
''' Carga el combo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcGestor_DropDown()
    Dim oRes As Ador.Recordset
    Dim oOrden As COrdenEntrega
    
    sdbcGestor.RemoveAll
    
    'Cargamos los codigos
    Screen.MousePointer = vbHourglass

    Set oOrden = oFSGSRaiz.Generar_COrdenEntrega

    If bCargarComboDesde Then
        Set oRes = oOrden.LeerGestores(oUsuarioSummit, Trim(sdbcGestor.Text))
    Else
        Set oRes = oOrden.LeerGestores(oUsuarioSummit)
    End If

    While Not oRes.EOF
        sdbcGestor.AddItem oRes("PER").Value & Chr(m_lSeparador) & oRes("NOM").Value & " " & oRes("APE").Value
        oRes.MoveNext
    Wend

    oRes.Close
    Set oRes = Nothing
    Set oOrden = Nothing
    
    sdbcGestor.SelStart = 0
    sdbcGestor.SelLength = Len(sdbcGestor.Text)
    sdbcGestor.Refresh
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Inicializa el combo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcGestor_InitColumnProps()
    sdbcGestor.DataFieldList = "Column 1"
    sdbcGestor.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Si pulsas del o supr limpia el combo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcGestor_KeyDown(KeyCode As Integer, Shift As Integer)
    If sdbcGestor.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
        sdbcGestor.Text = ""
    End If
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcGestor_PositionList(ByVal Text As String)
PositionList sdbcGestor, Text
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub cmdFecDesdePlanEntrega_Click()
    AbrirFormCalendar Me, txtFecDesdePlanEntrega
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub cmdFecHastaPlanEntrega_Click()
    AbrirFormCalendar Me, txtFecHastaPlanEntrega
End Sub

''' <summary>Evento de cambio de c�digo de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjCod_Change()
    
   If Not bRespetarComboProAdj Then
    
        bRespetarComboProAdj = True
        sdbcProAdjDen.Text = ""
        bRespetarComboProAdj = False
        
        bCargarComboProAdjDesde = True
    End If
    
End Sub

''' <summary>Evento de click sobre el c�digo de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjCod_Click()
     
     If Not sdbcProAdjCod.DroppedDown Then
        sdbcProAdjCod = ""
        sdbcProAdjDen = ""
        bCargarComboProAdjDesde = False
    End If
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">c�digo de proveedor seleccionado</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjCod_PositionList(ByVal Text As String)
PositionList sdbcProAdjCod, Text
End Sub

''' <summary>
''' Validacion del codigo del proveedor
''' </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oProves As CProveedores
    
    If sdbcProAdjCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProAdjCod.Text), , True, , False
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProAdjCod.Text))
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        sdbcProAdjCod.Text = ""
    Else
        bRespetarComboProAdj = True
        sdbcProAdjCod.Text = oProves.Item(1).Cod
        sdbcProAdjDen.Text = oProves.Item(1).Den
        
        sdbcProAdjCod.Columns(0).Text = sdbcProAdjCod.Text
        sdbcProAdjCod.Columns(1).Text = sdbcProAdjDen.Text
            
        bRespetarComboProAdj = False
    End If
    bCargarComboProAdjDesde = True
End Sub

''' <summary>Evento closeup del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjCod_CloseUp()
    
    If sdbcProAdjCod.Value = "..." Then
        sdbcProAdjCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProAdjCod.Value = "" Then Exit Sub
    
    bRespetarComboProAdj = True
    sdbcProAdjDen.Text = sdbcProAdjCod.Columns(1).Text
    sdbcProAdjCod.Text = sdbcProAdjCod.Columns(0).Text
    bRespetarComboProAdj = False
    bCargarComboProAdjDesde = False
    DoEvents
        
End Sub

''' <summary>Evento dropdown del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oProves As CProveedores
        
    sdbcProAdjCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If bCargarComboProAdjDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProAdjCod.Text), , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        End If
    Else
    
        If bCargarComboProAdjDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProAdjCod.Text)
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp
        End If
        
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProAdjCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProAdjCod.AddItem "..."
    End If

    sdbcProAdjCod.SelStart = 0
    sdbcProAdjCod.SelLength = Len(sdbcProAdjCod.Text)
    sdbcProAdjCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboProAdjDesde = False
    
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjCod_InitColumnProps()
    sdbcProAdjCod.DataFieldList = "Column 0"
    sdbcProAdjCod.DataFieldToDisplay = "Column 0"
End Sub

'' <summary>Evento de cambio de denominaci�n de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjDen_Change()
    
    If Not bRespetarComboProAdj Then
    
        bRespetarComboProAdj = True
        sdbcProAdjCod.Text = ""
        bRespetarComboProAdj = False
        bCargarComboProAdjDesde = True
    End If
    
End Sub

''' <summary>Evento de click sobre la denominaci�n de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjDen_Click()
    
    If Not sdbcProAdjDen.DroppedDown Then
        sdbcProAdjCod = ""
        sdbcProAdjDen = ""
        bCargarComboProAdjDesde = False
    End If
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">denominaci�n de proveedor seleccionada</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjDen_PositionList(ByVal Text As String)
PositionList sdbcProAdjDen, Text
End Sub

''' <summary>Evento de validaci�n en el combo</summary>
''' <param name="Cancel">Cancela el cambio de columna o fila</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oProves As CProveedores
    
    If sdbcProAdjDen.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProAdjDen.Text), True, , False
                    
    bExiste = Not (oProves.Count = 0)
    
    If Not bExiste Then
        sdbcProAdjDen.Text = ""
    Else
        bRespetarComboProAdj = True
        sdbcProAdjCod.Text = oProves.Item(1).Cod
        sdbcProAdjDen.Text = oProves.Item(1).Den
        sdbcProAdjDen.Columns(1).Text = sdbcProAdjCod.Text
        sdbcProAdjDen.Columns(0).Text = sdbcProAdjDen.Text
        
        bRespetarComboProAdj = False
    End If
    Screen.MousePointer = vbNormal
    bCargarComboProAdjDesde = False
End Sub

''' <summary>Evento closeup en el combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjDen_CloseUp()

    
    If sdbcProAdjDen.Value = "..." Then
        sdbcProAdjDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProAdjDen.Value = "" Then Exit Sub
    
    bRespetarComboProAdj = True
    sdbcProAdjCod.Text = sdbcProAdjDen.Columns(1).Text
    sdbcProAdjDen.Text = sdbcProAdjDen.Columns(0).Text
    bRespetarComboProAdj = False
    bCargarComboProAdjDesde = False
    DoEvents
            
End Sub

''' <summary>
''' DropDown del combo de denominacion de proveedores, carga la lista
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en el combo de denominacion de proveedor; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProAdjDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oProves As CProveedores
    
    sdbcProAdjDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
         
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If bCargarComboProAdjDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProAdjDen.Text), , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        End If
    Else
    
        If bCargarComboProAdjDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProAdjDen.Text), , , , , , , , , , , True
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True
        End If
        
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProAdjDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not oProves.EOF Then
        sdbcProAdjDen.AddItem "..."
    End If

    sdbcProAdjDen.SelStart = 0
    sdbcProAdjDen.SelLength = Len(sdbcProAdjDen.Text)
    sdbcProAdjCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboProAdjDesde = False
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProAdjDen_InitColumnProps()
    sdbcProAdjDen.DataFieldList = "Column 0"
    sdbcProAdjDen.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>Proveedor adjudicado seleccionado en el combo</summary>
''' <remarks>Llamada desde:CargarProveedorAdjConBusqueda; Tiempo m�ximo:0,1</remarks>
Private Sub ProveedorAdjSeleccionado()
    
    oProves.CargarDatosProveedor (sdbcProAdjCod.Text)
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProAdjCod.Text)
    
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Carga del proveedor adjudicado desde la pantalla de b�squeda de proveedores</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
Public Sub CargarProveedorAdjConBusqueda()

    Set oProves = frmPROVEBuscar.oProveEncontrados
   
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    sdbcProAdjCod = oProves.Item(1).Cod
    sdbcProAdjCod_Validate False
    ProveedorAdjSeleccionado
    
End Sub
