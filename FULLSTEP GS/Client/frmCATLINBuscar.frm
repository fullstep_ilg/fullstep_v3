VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCATLINBuscar 
   BackColor       =   &H00808000&
   Caption         =   "Buscar l�neas de cat�logo"
   ClientHeight    =   4005
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9675
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATLINBuscar.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4005
   ScaleWidth      =   9675
   StartUpPosition =   3  'Windows Default
   Begin TabDlg.SSTab sstabLineas 
      Height          =   3840
      Left            =   30
      TabIndex        =   0
      Top             =   120
      Width           =   9585
      _ExtentX        =   16907
      _ExtentY        =   6773
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BackColor       =   8421376
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Datos de busqueda"
      TabPicture(0)   =   "frmCATLINBuscar.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Adjudicaciones"
      TabPicture(1)   =   "frmCATLINBuscar.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdAceptar"
      Tab(1).Control(1)=   "cmdCancelar"
      Tab(1).Control(2)=   "sdbgAdjudicaciones"
      Tab(1).ControlCount=   3
      Begin SSDataWidgets_B.SSDBGrid sdbgAdjudicaciones 
         Height          =   2805
         Left            =   -74880
         TabIndex        =   11
         Top             =   480
         Width           =   9300
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   16
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   16
         Columns(0).Width=   1984
         Columns(0).Caption=   "Proceso"
         Columns(0).Name =   "PROCE"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3969
         Columns(1).Caption=   "Art�culo"
         Columns(1).Name =   "ART"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1588
         Columns(2).Caption=   "Proveedor"
         Columns(2).Name =   "PROVE"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   847
         Columns(3).Caption=   "UC"
         Columns(3).Name =   "UC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1852
         Columns(4).Caption=   "Cantidad"
         Columns(4).Name =   "CANTADJ"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   5
         Columns(4).NumberFormat=   "Standard"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "LINEA"
         Columns(5).Name =   "LINEA"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "CAT1"
         Columns(6).Name =   "CAT1"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "CAT2"
         Columns(7).Name =   "CAT2"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "CAT3"
         Columns(8).Name =   "CAT3"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "CAT4"
         Columns(9).Name =   "CAT4"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "CAT5"
         Columns(10).Name=   "CAT5"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   1111
         Columns(11).Caption=   "CAT1"
         Columns(11).Name=   "CAT1COD"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(12).Width=   1111
         Columns(12).Caption=   "CAT2"
         Columns(12).Name=   "CAT2COD"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(13).Width=   1111
         Columns(13).Caption=   "CAT3"
         Columns(13).Name=   "CAT3COD"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(14).Width=   1111
         Columns(14).Caption=   "CAT4"
         Columns(14).Name=   "CAT4COD"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   1111
         Columns(15).Caption=   "CAT5"
         Columns(15).Name=   "CAT5COD"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         _ExtentX        =   16404
         _ExtentY        =   4948
         _StockProps     =   79
         BackColor       =   -2147483633
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cerrar"
         Height          =   345
         Left            =   -70200
         TabIndex        =   13
         Top             =   3390
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Seleccionar"
         Height          =   345
         Left            =   -71280
         TabIndex        =   12
         Top             =   3390
         Width           =   1005
      End
      Begin VB.Frame Frame1 
         Height          =   3225
         Left            =   105
         TabIndex        =   14
         Top             =   420
         Width           =   9300
         Begin VB.PictureBox picSolicitud 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   165
            ScaleHeight     =   285
            ScaleWidth      =   3660
            TabIndex        =   19
            Top             =   2655
            Width           =   3660
            Begin VB.CommandButton cmdBuscarSolic 
               Height          =   285
               Left            =   3345
               Picture         =   "frmCATLINBuscar.frx":0182
               Style           =   1  'Graphical
               TabIndex        =   21
               Top             =   0
               Width           =   315
            End
            Begin VB.CommandButton cmdBorrarSolic 
               Height          =   285
               Left            =   2985
               Picture         =   "frmCATLINBuscar.frx":020F
               Style           =   1  'Graphical
               TabIndex        =   20
               Top             =   0
               Width           =   315
            End
            Begin VB.Label lblSolicitud 
               Caption         =   "DSolicitud:"
               Height          =   255
               Left            =   0
               TabIndex        =   24
               Top             =   30
               Width           =   870
            End
            Begin VB.Label lblSolicCompras 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   960
               TabIndex        =   23
               Top             =   0
               Width           =   2025
            End
            Begin VB.Label lblIdSolic 
               Height          =   255
               Left            =   2355
               TabIndex        =   22
               Top             =   315
               Visible         =   0   'False
               Width           =   735
            End
         End
         Begin VB.TextBox txtEstCat 
            BackColor       =   &H80000018&
            Enabled         =   0   'False
            Height          =   285
            Left            =   1125
            Locked          =   -1  'True
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   2085
            Width           =   4350
         End
         Begin VB.TextBox txtEstMat 
            BackColor       =   &H80000018&
            Enabled         =   0   'False
            Height          =   285
            Left            =   1125
            Locked          =   -1  'True
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   1050
            Width           =   4350
         End
         Begin VB.CommandButton cmdBorrarCat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5490
            Picture         =   "frmCATLINBuscar.frx":02B4
            Style           =   1  'Graphical
            TabIndex        =   9
            Top             =   2070
            Width           =   285
         End
         Begin VB.CommandButton cmdSelCat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5805
            Picture         =   "frmCATLINBuscar.frx":035A
            Style           =   1  'Graphical
            TabIndex        =   10
            Top             =   2070
            Width           =   285
         End
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5805
            Picture         =   "frmCATLINBuscar.frx":03C6
            Style           =   1  'Graphical
            TabIndex        =   5
            Top             =   1035
            Width           =   285
         End
         Begin VB.CommandButton cmdBorrarMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5490
            Picture         =   "frmCATLINBuscar.frx":0432
            Style           =   1  'Graphical
            TabIndex        =   4
            Top             =   1035
            Width           =   285
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
            Height          =   285
            Left            =   1125
            TabIndex        =   1
            Top             =   390
            Width           =   1515
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2672
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
            Height          =   285
            Left            =   2655
            TabIndex        =   2
            Top             =   390
            Width           =   3465
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6112
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiCod 
            Height          =   285
            Left            =   1125
            TabIndex        =   6
            Top             =   1440
            Width           =   1515
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2672
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiDen 
            Height          =   285
            Left            =   2640
            TabIndex        =   7
            Top             =   1440
            Width           =   3465
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6112
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblProve 
            Caption         =   "Proveedor:"
            Height          =   225
            Left            =   165
            TabIndex        =   18
            Top             =   450
            Width           =   1035
         End
         Begin VB.Label lblArt 
            Caption         =   "Art�culo:"
            Height          =   225
            Left            =   165
            TabIndex        =   17
            Top             =   1485
            Width           =   1020
         End
         Begin VB.Label lblCat 
            Caption         =   "Categor�a:"
            Height          =   225
            Left            =   165
            TabIndex        =   16
            Top             =   2085
            Width           =   870
         End
         Begin VB.Label lblMat 
            Caption         =   "Material:"
            Height          =   225
            Left            =   165
            TabIndex        =   15
            Top             =   1050
            Width           =   870
         End
      End
   End
End
Attribute VB_Name = "frmCATLINBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables de seguridad
Private bRMat As Boolean
Private bRUsuAprov As Boolean

'Variables de combos
Private CargarComboDesdeProve As Boolean
Private CargarComboDesdeArti As Boolean
Private RespetarComboProve As Boolean
Private RespetarComboArti As Boolean

Private oProves As CProveedores
Private oProveSeleccionado As CProveedor
Private oArticulos As CArticulos

Public sGMN1Cod As String
Public sGMN2Cod As String
Public sGMN3Cod As String
Public sGMN4Cod As String
Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4
Public iCAT1Seleccionada As Integer
Public iCAT2Seleccionada As Integer
Public iCAT3Seleccionada As Integer
Public iCAT4Seleccionada As Integer
Public iCAT5Seleccionada As Integer

'Idiomas
Private sSelecci�n As String
Private sIdiProve As String

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATLIN_BUSCAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        sstabLineas.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabLineas.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        lblProve.caption = Ador(0).Value & ":"
        sdbgAdjudicaciones.Columns("PROVE").caption = Ador(0).Value
        sIdiProve = Ador(0).Value
        Ador.MoveNext
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        sdbcArtiCod.Columns(0).caption = Ador(0).Value
        sdbcArtiDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcProveCod.Columns(1).caption = Ador(0).Value
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        sdbcArtiCod.Columns(1).caption = Ador(0).Value
        sdbcArtiDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        lblMat.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblArt.caption = Ador(0).Value & ":"
        sdbgAdjudicaciones.Columns("ART").caption = Ador(0).Value
        Ador.MoveNext
        lblCat.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("PROCE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("UC").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("CANTADJ").caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sSelecci�n = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("CAT1COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("CAT2COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("CAT3COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("CAT4COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("CAT5COD").caption = Ador(0).Value
        Ador.MoveNext
        lblSolicitud.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing

End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Private Sub ConfigurarSeguridad()
    If FSEPConf Then
        picSolicitud.Visible = False
    Else
        If Not basParametros.gParametrosGenerales.gbSolicitudesCompras Then
            picSolicitud.Visible = False
        Else
            'Si no tiene permisos de consulta de las solicitudes no podr� visualizarlas
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSolicAsignar)) Is Nothing) Then
                picSolicitud.Visible = False
            Else
                picSolicitud.Visible = True
            End If
        End If
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestMatComp)) Is Nothing) Then
        bRMat = True
    End If

    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestUsuAprov)) Is Nothing) Then
        bRUsuAprov = True
    End If
End Sub

Public Sub PonerMatSeleccionado()

    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
        
    If Not oGMN1Seleccionado Is Nothing Then
        sGMN1Cod = oGMN1Seleccionado.Cod
        txtEstMat = sGMN1Cod
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    Else
        sGMN2Cod = ""
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    Else
        sGMN3Cod = ""
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    Else
        sGMN4Cod = ""
    End If
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
End Sub

Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim iNivel As Integer
Dim oLineaSeleccionada As CLineaCatalogo
Dim i As Integer

    If sdbgAdjudicaciones.Rows = 0 Then Exit Sub
    
    On Error GoTo NoSeEncuentra
    
    If Trim(sdbgAdjudicaciones.Columns("CAT5COD").Value) <> "" Then
        iNivel = 5
    Else
        If Trim(sdbgAdjudicaciones.Columns("CAT4COD").Value) <> "" Then
            iNivel = 4
        Else
            If Trim(sdbgAdjudicaciones.Columns("CAT3COD").Value) <> "" Then
                iNivel = 3
            Else
                If Trim(sdbgAdjudicaciones.Columns("CAT2COD").Value) <> "" Then
                    iNivel = 2
                Else
                    If Trim(sdbgAdjudicaciones.Columns("CAT1COD").Value) <> "" Then
                        iNivel = 1
                    End If
                End If
            End If
        End If
    End If
    

    Select Case iNivel
    
    Case 1
        
        scod1 = sdbgAdjudicaciones.Columns("CAT1COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(sdbgAdjudicaciones.Columns("CAT1COD").Value))
        
        Set nodx = frmCatalogo.tvwCategorias.Nodes("CAT1" & scod1)
        nodx.Selected = True
        nodx.EnsureVisible
        frmCatalogo.ConfigurarInterfazSeguridad nodx
    
    Case 2
        
        scod1 = sdbgAdjudicaciones.Columns("CAT1COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(sdbgAdjudicaciones.Columns("CAT1COD").Value))
        scod2 = sdbgAdjudicaciones.Columns("CAT2COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(sdbgAdjudicaciones.Columns("CAT2COD").Value))
        
        Set nodx = frmCatalogo.tvwCategorias.Nodes("CAT2" & scod1 & scod2)
        nodx.Selected = True
        nodx.EnsureVisible
        frmCatalogo.ConfigurarInterfazSeguridad nodx
    
    Case 3
        
        scod1 = sdbgAdjudicaciones.Columns("CAT1COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(sdbgAdjudicaciones.Columns("CAT1COD").Value))
        scod2 = sdbgAdjudicaciones.Columns("CAT2COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(sdbgAdjudicaciones.Columns("CAT2COD").Value))
        scod3 = sdbgAdjudicaciones.Columns("CAT3COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(sdbgAdjudicaciones.Columns("CAT3COD").Value))
        
        Set nodx = frmCatalogo.tvwCategorias.Nodes("CAT3" & scod1 & scod2 & scod3)
        nodx.Selected = True
        nodx.EnsureVisible
        frmCatalogo.ConfigurarInterfazSeguridad nodx
    
    Case 4
        
        scod1 = sdbgAdjudicaciones.Columns("CAT1COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(sdbgAdjudicaciones.Columns("CAT1COD").Value))
        scod2 = sdbgAdjudicaciones.Columns("CAT2COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(sdbgAdjudicaciones.Columns("CAT2COD").Value))
        scod3 = sdbgAdjudicaciones.Columns("CAT3COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(sdbgAdjudicaciones.Columns("CAT3COD").Value))
        scod4 = sdbgAdjudicaciones.Columns("CAT4COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(sdbgAdjudicaciones.Columns("CAT4COD").Value))
        
        Set nodx = frmCatalogo.tvwCategorias.Nodes("CAT4" & scod1 & scod2 & scod3 & scod4)
        nodx.Selected = True
        nodx.EnsureVisible
        frmCatalogo.ConfigurarInterfazSeguridad nodx
        
    Case 5
        
        scod1 = sdbgAdjudicaciones.Columns("CAT1COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(sdbgAdjudicaciones.Columns("CAT1COD").Value))
        scod2 = sdbgAdjudicaciones.Columns("CAT2COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(sdbgAdjudicaciones.Columns("CAT2COD").Value))
        scod3 = sdbgAdjudicaciones.Columns("CAT3COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(sdbgAdjudicaciones.Columns("CAT3COD").Value))
        scod4 = sdbgAdjudicaciones.Columns("CAT4COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(sdbgAdjudicaciones.Columns("CAT4COD").Value))
        scod5 = sdbgAdjudicaciones.Columns("CAT5COD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(sdbgAdjudicaciones.Columns("CAT5COD").Value))
        
        Set nodx = frmCatalogo.tvwCategorias.Nodes("CAT5" & scod1 & scod2 & scod3 & scod4 & scod5)
        nodx.Selected = True
        nodx.EnsureVisible
        frmCatalogo.ConfigurarInterfazSeguridad nodx
        
    End Select

    frmCatalogo.tabCatalogo_Click 0
    frmCatalogo.tabCatalogo.TabVisible(1) = True
    frmCatalogo.tabCatalogo.Tab = 1

    frmCatalogo.sdbgAdjudicaciones.MoveFirst
    For i = 1 To frmCatalogo.oLineasCatalogo.Count
        If frmCatalogo.oLineasCatalogo.Item(i).Id = sdbgAdjudicaciones.Columns("LINEA").Value Then
            Exit For
        End If
        frmCatalogo.sdbgAdjudicaciones.MoveNext
    Next
        
    Unload Me
    Exit Sub

NoSeEncuentra:
    oMensajes.CategoriaNueva
    Unload Me

End Sub

Private Sub cmdBorrarCat_Click()
    txtEstCat.Text = ""
    iCAT1Seleccionada = 0
    iCAT2Seleccionada = 0
    iCAT3Seleccionada = 0
    iCAT4Seleccionada = 0
    iCAT5Seleccionada = 0
End Sub

Private Sub cmdBorrarMat_Click()
    txtEstMat.Text = ""
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    sdbcArtiCod.Value = ""
End Sub


Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdSelCat_Click()
    frmSELCat.sOrigen = "frmCATLINBuscar"
    frmSELCat.bVerBajas = False
    frmSELCat.Show 1
End Sub

Private Sub cmdSelMat_Click()
    If sdbcProveCod.Text <> "" Then
        Set frmSELMATProve.oProveedor = oProveSeleccionado
        frmSELMATProve.sOrigen = "frmCATLINBuscar"
        frmSELMATProve.bRMat = bRMat
        frmSELMATProve.Show 1
    Else
        frmSELMAT.sOrigen = "frmCATLINBuscar"
        frmSELMAT.bRComprador = bRMat
        frmSELMAT.Show 1
    End If

End Sub

Public Sub PonerMatProveSeleccionado()
         
    If sGMN1Cod <> "" Then
        txtEstMat = sGMN1Cod
    End If
    
    If sGMN2Cod <> "" Then
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    End If
    
    If sGMN3Cod <> "" Then
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    End If
    
    If sGMN4Cod <> "" Then
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    End If

End Sub

Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    Arrange
    
    CargarRecursos

    ConfigurarSeguridad

    Set oProves = oFSGSRaiz.generar_CProveedores
    
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
    PonerFieldSeparator Me

End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Arrange()

    On Error Resume Next
    
    If sstabLineas.Tab = 0 Then
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
    Else
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
    End If
        
    sstabLineas.Width = Me.Width - 210
    sstabLineas.Height = Me.Height - 570

    Frame1.Width = sstabLineas.Width - 285
    Frame1.Height = sstabLineas.Height - 615
    
    sdbgAdjudicaciones.Width = Me.Width - 495
    sdbgAdjudicaciones.Height = Frame1.Height - 420
    
    If Me.Width < 10000 Then
        sdbgAdjudicaciones.Columns(0).Width = sdbgAdjudicaciones.Width * 0.11
        sdbgAdjudicaciones.Columns(1).Width = sdbgAdjudicaciones.Width * 0.2
        sdbgAdjudicaciones.Columns(2).Width = sdbgAdjudicaciones.Width * 0.15
        sdbgAdjudicaciones.Columns(3).Width = sdbgAdjudicaciones.Width * 0.07
        sdbgAdjudicaciones.Columns(4).Width = sdbgAdjudicaciones.Width * 0.1
        sdbgAdjudicaciones.Columns(5).Width = sdbgAdjudicaciones.Width * 0.1
        sdbgAdjudicaciones.Columns(6).Width = sdbgAdjudicaciones.Width * 0.1
        sdbgAdjudicaciones.Columns(7).Width = sdbgAdjudicaciones.Width * 0.1
        sdbgAdjudicaciones.Columns(8).Width = sdbgAdjudicaciones.Width * 0.1
        sdbgAdjudicaciones.Columns(9).Width = sdbgAdjudicaciones.Width * 0.1
    Else
        sdbgAdjudicaciones.Columns(0).Width = sdbgAdjudicaciones.Width * 0.11
        sdbgAdjudicaciones.Columns(1).Width = sdbgAdjudicaciones.Width * 0.25
        sdbgAdjudicaciones.Columns(2).Width = sdbgAdjudicaciones.Width * 0.1
        sdbgAdjudicaciones.Columns(3).Width = sdbgAdjudicaciones.Width * 0.07
        sdbgAdjudicaciones.Columns(4).Width = sdbgAdjudicaciones.Width * 0.18
        sdbgAdjudicaciones.Columns(5).Width = sdbgAdjudicaciones.Width * 0.18
        sdbgAdjudicaciones.Columns(6).Width = sdbgAdjudicaciones.Width * 0.18
        sdbgAdjudicaciones.Columns(7).Width = sdbgAdjudicaciones.Width * 0.18
        sdbgAdjudicaciones.Columns(8).Width = sdbgAdjudicaciones.Width * 0.18
        sdbgAdjudicaciones.Columns(9).Width = sdbgAdjudicaciones.Width * 0.18
    End If
    
    cmdAceptar.Left = (sdbgAdjudicaciones.Width / 2) - (cmdAceptar.Width / 2) - 300
    cmdCancelar.Left = (sdbgAdjudicaciones.Width / 2) + 300
    cmdAceptar.Top = sdbgAdjudicaciones.Top + sdbgAdjudicaciones.Height + 100
    cmdCancelar.Top = cmdAceptar.Top

End Sub

 Sub Form_Unload(Cancel As Integer)
    Set oProves = Nothing
    Set oArticulos = Nothing
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    CargarComboDesdeProve = False
    CargarComboDesdeArti = False
    RespetarComboProve = False
    RespetarComboArti = False

    Set oProveSeleccionado = Nothing


    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    iCAT1Seleccionada = 0
    iCAT2Seleccionada = 0
    iCAT3Seleccionada = 0
    iCAT4Seleccionada = 0
    iCAT5Seleccionada = 0

End Sub




Private Sub sdbcArtiCod_Change()
    If Not RespetarComboArti Then
        RespetarComboArti = True
        sdbcArtiDen.Text = ""
        RespetarComboArti = False
        CargarComboDesdeArti = True
    End If
End Sub

Private Sub sdbcArtiCod_Click()
    If Not sdbcArtiCod.DroppedDown Then
        sdbcArtiCod.Text = ""
        sdbcArtiDen.Text = ""
    End If
End Sub


Private Sub sdbcArtiCod_CloseUp()
    If sdbcArtiCod.Value = "..." Then
        sdbcArtiCod.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiCod.Value = "" Then Exit Sub
    
    RespetarComboArti = True
    sdbcArtiDen.Text = sdbcArtiCod.Columns(1).Text
    sdbcArtiCod.Text = sdbcArtiCod.Columns(0).Text
    RespetarComboArti = False
    
    CargarComboDesdeArti = False
End Sub


Private Sub sdbcArtiCod_DropDown()
Dim oArt As CArticulo

    sdbcArtiCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If CargarComboDesdeArti Then
        If bRMat Then
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, True, Trim(sdbcArtiCod.Text), , , 0, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
            
        Else
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, True, Trim(sdbcArtiCod.Text), , , 0
        End If
    Else
        If bRMat Then
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, True, , , , 0, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
            
        Else
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, True, , , , 0
        End If

    End If


    For Each oArt In oArticulos
        sdbcArtiCod.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den
    Next

    sdbcArtiCod.SelStart = 0
    sdbcArtiCod.SelLength = Len(sdbcArtiCod.Text)
    sdbcArtiCod.Refresh

    Set oArt = Nothing
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcArtiCod_InitColumnProps()
    sdbcArtiCod.DataFieldList = "Column 0"
    sdbcArtiCod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcArtiCod_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcArtiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcArtiCod.Rows - 1
            bm = sdbcArtiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcArtiCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcArtiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbcArtiCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean

    If sdbcArtiCod.Text = "" Then Exit Sub

    ''' Solo continuamos si existe el art�culo

    Screen.MousePointer = vbHourglass
    If bRMat Then
        oArticulos.DevolverArticulosDesde 1, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, True, Trim(sdbcArtiCod.Text), , True, 0, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
        
    Else
        oArticulos.DevolverArticulosDesde 1, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, True, Trim(sdbcArtiCod.Text), , True, 0
    End If


    bExiste = Not (oArticulos.Count = 0)
    
    If Not bExiste Then
        sdbcArtiCod.Text = ""
    Else
        RespetarComboArti = True
        sdbcArtiDen.Text = oArticulos.Item(1).Den
        RespetarComboArti = False
    End If
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcArtiDen_Change()
    If Not RespetarComboArti Then
    
        RespetarComboArti = True
        sdbcArtiCod.Text = ""
        RespetarComboArti = False
'        Set oArtiSeleccionado = Nothing
        CargarComboDesdeArti = True
        
    End If
End Sub

Private Sub sdbcArtiDen_Click()
    If Not sdbcArtiDen.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub


Private Sub sdbcArtiDen_CloseUp()
    If sdbcArtiDen.Value = "..." Then
        sdbcArtiDen.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiDen.Value = "" Then Exit Sub
    
    RespetarComboArti = True
    sdbcArtiCod.Text = sdbcArtiDen.Columns(1).Text
    sdbcArtiDen.Text = sdbcArtiDen.Columns(0).Text
    RespetarComboArti = False
    CargarComboDesdeArti = False
End Sub


Private Sub sdbcArtiDen_DropDown()
Dim oArt As CArticulo

    sdbcArtiDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If CargarComboDesdeArti Then
        If bRMat Then
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, True, , Trim(sdbcArtiDen.Text), , 1, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
            
        Else
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, True, , Trim(sdbcArtiDen.Text), , 1
        End If
    Else
        If bRMat Then
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, True, , , , 1, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
            
        Else
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, True, , , , 1
        End If

    End If


    For Each oArt In oArticulos
        sdbcArtiDen.AddItem oArt.Den & Chr(m_lSeparador) & oArt.Cod
    Next

    sdbcArtiDen.SelStart = 0
    sdbcArtiDen.SelLength = Len(sdbcArtiDen.Text)
    sdbcArtiDen.Refresh
    
    Set oArt = Nothing
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcArtiDen_InitColumnProps()
    sdbcArtiDen.DataFieldList = "Column 0"
    sdbcArtiDen.DataFieldToDisplay = "Column 0"

End Sub


Private Sub sdbcArtiDen_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcArtiDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcArtiDen.Rows - 1
            bm = sdbcArtiDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcArtiDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcArtiDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub




Private Sub sdbcProveCod_Change()
   If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveDen.Text = ""
        RespetarComboProve = False
        RespetarComboArti = True
        sdbcArtiCod.Text = ""
        RespetarComboArti = False
    End If
End Sub

Private Sub sdbcProveCod_Click()
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod.Text = ""
        sdbcProveDen.Text = ""
    End If
End Sub


Private Sub sdbcProveCod_CloseUp()
    If sdbcProveCod.Value = "" Or sdbcProveCod.Value = "..." Then
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Value
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Value
    RespetarComboProve = False
    sdbcArtiCod.Text = ""
    ProveedorSeleccionado

End Sub

Private Sub ProveedorSeleccionado()
    
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    
    If oProveSeleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    txtEstMat = ""
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    
'    If gParametrosGenerales.giINSTWEB = ConPortal Then
'        oProves.CargarDatosProveedor sdbcProveCod.Text
'        If ((IsNull(oProves.Item(CStr(sdbcProveCod.Text)).CodPortal) Or (oProves.Item(CStr(sdbcProveCod.Text)).CodPortal = ""))) Then
'            oMensajes.ProveCatalogProvePortal
'            Screen.MousePointer = vbNormal
'            sdbcProveCod.Text = ""
'            Set oProveSeleccionado = Nothing
'            Exit Sub
'        End If
'    End If
                
    If gParametrosGenerales.giINSTWEB = conweb Then
        oProves.CargarDatosProveedor sdbcProveCod.Text, True
        If oProves.Item(CStr(sdbcProveCod.Text)).UsuarioWeb Is Nothing Then
            oMensajes.ProveCatalogProveWeb
            Screen.MousePointer = vbNormal
            sdbcProveCod.Text = ""
            Set oProveSeleccionado = Nothing
            Exit Sub
        End If
    End If
                
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcProveCod.RemoveAll
    
    If oProves Is Nothing Then
        Set oProves = oFSGSRaiz.generar_CProveedores
    End If
    
    'Restricci�n de material en proveedores
    If bRMat Then
        oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text
    End If
    
    'Sin restricciones
    If Not bRMat Then
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveCod.Text)
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not oProves.EOF Then
        sdbcProveCod.AddItem "..." & Chr(m_lSeparador) & "....."
    End If
    
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcProveCod_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcProveCod.DroppedDown = False
        sdbcProveCod.Text = ""
        sdbcProveCod.RemoveAll
        sdbcProveDen.DroppedDown = False
        sdbcProveDen.Text = ""
        sdbcProveDen.RemoveAll
    End If
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveCod.Rows - 1
            bm = sdbcProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


Private Sub sdbcProveCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean

    If Trim(sdbcProveCod.Text) = "" Then Exit Sub

    ''' Solo continuamos si existe el proveedor

    Screen.MousePointer = vbHourglass

    'Restricci�n de material en proveedores
    If bRMat Then
        oProves.BuscarProveedoresConMatDelCompDesde 1, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text
    End If

    'Sin restricciones
    If Not bRMat Then
        oProves.BuscarTodosLosProveedoresDesde 1, , Trim(sdbcProveCod.Text)
    End If

    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))

    Screen.MousePointer = vbNormal

    If Not bExiste Then
        sdbcProveCod.Text = ""
        oMensajes.NoValido sIdiProve
    Else
        RespetarComboProve = True
        sdbcProveDen = oProves.Item(1).Den
        RespetarComboProve = False
        ProveedorSeleccionado
    End If

End Sub


Private Sub sdbcProveDen_Change()
     If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveCod.Text = ""
        RespetarComboProve = False
        RespetarComboArti = True
        sdbcArtiCod.Text = ""
        RespetarComboArti = False
    End If
End Sub

Private Sub sdbcProveDen_Click()
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod.Text = ""
        sdbcProveDen.Text = ""
    End If
End Sub


Private Sub sdbcProveDen_CloseUp()
    If sdbcProveDen.Value = "" Or sdbcProveDen.Value = "....." Then
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Value
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Value
    RespetarComboProve = False
    sdbcArtiCod.Text = ""
    ProveedorSeleccionado

End Sub


Private Sub sdbcProveDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcProveDen.RemoveAll
    
    If oProves Is Nothing Then
        Set oProves = oFSGSRaiz.generar_CProveedores
    End If
    
    'Restricci�n de material en proveedores
    If bRMat Then
        oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , Trim(sdbcProveDen), , , , , , , True
    End If
    
    'Sin restricciones
    If Not bRMat Then
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , Trim(sdbcProveDen), , , , , , , , , , , True
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not oProves.EOF Then
        sdbcProveDen.AddItem "..." & Chr(m_lSeparador) & "....."
    End If
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub




Private Sub sdbcProveDen_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveDen.Rows - 1
            bm = sdbcProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub





Private Sub sstabLineas_Click(PreviousTab As Integer)
    
    If sstabLineas.Tab = 1 Then
        If sdbcProveCod.Text = "" And txtEstMat.Text = "" And sdbcArtiCod.Text = "" And txtEstCat.Text = "" And lblIdSolic.caption = "" Then
            sstabLineas.Tab = 0
            oMensajes.FaltanDatos sSelecci�n
            Exit Sub
        End If
        sdbgAdjudicaciones.RemoveAll
        Screen.MousePointer = vbHourglass
        Cargar
        Screen.MousePointer = vbNormal
    End If

    If sstabLineas.Tab = 1 Then
        cmdCancelar.Visible = True
        cmdAceptar.Visible = True
    Else
        cmdCancelar.Visible = False
        cmdAceptar.Visible = False
    End If

End Sub

Private Sub Cargar()
Dim oLineasCat As CLineasCatalogo
Dim ADORs As Ador.Recordset
Dim sArticulo As String

    Set oLineasCat = Nothing
    Set oLineasCat = oFSGSRaiz.Generar_CLineasCatalogo

    Set ADORs = oLineasCat.DevolverLineasDeCatalogo(Trim(sdbcProveCod.Text), Trim(sGMN1Cod), Trim(sGMN2Cod), Trim(sGMN3Cod), Trim(sGMN4Cod), Trim(sdbcArtiCod.Text), iCAT1Seleccionada, iCAT2Seleccionada, iCAT3Seleccionada, iCAT4Seleccionada, iCAT5Seleccionada, bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRUsuAprov, basOptimizacion.gCodPersonaUsuario, frmCatalogo.chkVerBajas.Value, Me.lblIdSolic.caption)

    If Not ADORs Is Nothing Then
        While Not ADORs.EOF
            If IsNull(ADORs("ARTCOD").Value) Then
                sArticulo = ADORs("ARTDEN").Value
            Else
                sArticulo = ADORs("ARTCOD").Value & " - " & ADORs("ARTDEN").Value
            End If
            If (IsNull(ADORs("ANYO").Value) And IsNull(ADORs("PROCE").Value) And IsNull(ADORs("ITEM").Value)) Then
                sdbgAdjudicaciones.AddItem "" & Chr(m_lSeparador) & sArticulo & Chr(m_lSeparador) & ADORs("PROVE").Value & Chr(m_lSeparador) & ADORs("UC").Value & Chr(m_lSeparador) & ADORs("CANT_ADJ").Value & Chr(m_lSeparador) & ADORs("ID").Value & Chr(m_lSeparador) & NullToStr(ADORs("CAT1").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT2").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT3").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT4").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT5").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT1COD").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT2COD").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT3COD").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT4COD").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT5COD").Value)
            Else
                sdbgAdjudicaciones.AddItem ADORs("ANYO").Value & "/" & ADORs("GMN1").Value & "/" & ADORs("PROCE").Value & Chr(m_lSeparador) & sArticulo & Chr(m_lSeparador) & ADORs("PROVE").Value & Chr(m_lSeparador) & ADORs("UC").Value & Chr(m_lSeparador) & ADORs("CANT_ADJ").Value & Chr(m_lSeparador) & ADORs("ID").Value & Chr(m_lSeparador) & NullToStr(ADORs("CAT1").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT2").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT3").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT4").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT5").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT1COD").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT2COD").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT3COD").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT4COD").Value) & Chr(m_lSeparador) & NullToStr(ADORs("CAT5COD").Value)
            End If
            ADORs.MoveNext
        Wend
    
        ADORs.Close
        Set ADORs = Nothing
    End If

End Sub



Private Sub cmdBorrarSolic_Click()
    lblSolicCompras.caption = ""
    lblIdSolic.caption = ""
End Sub

Private Sub cmdBuscarSolic_Click()
    frmSolicitudBuscar.g_sOrigen = "frmCATLINBuscar"
    frmSolicitudBuscar.Show vbModal

End Sub

Public Sub PonerSolicitudSeleccionada(ByVal oSolic As CInstancia)
    lblSolicCompras.caption = oSolic.Id & " " & oSolic.DescrBreve
    lblIdSolic.caption = oSolic.Id
    
    Set oSolic = Nothing
End Sub

