VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmImpuestos 
   Caption         =   "DImpuestos para el material: xxx"
   ClientHeight    =   5355
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   9780
   Icon            =   "frmImpuestos.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5355
   ScaleWidth      =   9780
   StartUpPosition =   3  'Windows Default
   Begin SSDataWidgets_B.SSDBDropDown sdbddImpuestos 
      Height          =   915
      Left            =   1830
      TabIndex        =   8
      Top             =   2910
      Width           =   6615
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmImpuestos.frx":0CB2
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   185
      Columns.Count   =   10
      Columns(0).Width=   1773
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5292
      Columns(1).Caption=   "DESCR"
      Columns(1).Name =   "DESCR"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1773
      Columns(2).Caption=   "VALOR"
      Columns(2).Name =   "VALOR"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1773
      Columns(3).Caption=   "RETENIDO"
      Columns(3).Name =   "RETENIDO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "VIGENTE"
      Columns(4).Name =   "VIGENTE"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "IMPUESTO_PAI"
      Columns(5).Name =   "IMPUESTO_PAIPROVI"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "IMPUESTO_ID"
      Columns(6).Name =   "IMPUESTO_ID"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "IMPUESTO_VALOR"
      Columns(7).Name =   "IMPUESTO_VALOR"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "CONCEPTO"
      Columns(8).Name =   "CONCEPTO"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "GRUPO_COMP"
      Columns(9).Name =   "GRUPO_COMP"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      _ExtentX        =   11668
      _ExtentY        =   1614
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   9780
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   4800
      Width           =   9780
      Begin VB.CommandButton cmdEdicionConsulta 
         Caption         =   "Edicion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   8640
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   90
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3450
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2340
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProvincias 
      Height          =   915
      Left            =   5190
      TabIndex        =   2
      Top             =   1200
      Width           =   3255
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmImpuestos.frx":0CCE
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   79
      Columns.Count   =   3
      Columns(0).Width=   2646
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4419
      Columns(1).Caption=   "NOMBRE"
      Columns(1).Name =   "NOMBRE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "COD_DEN"
      Columns(2).Name =   "COD_DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   1614
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddPaises 
      Height          =   915
      Left            =   1680
      TabIndex        =   1
      Top             =   1350
      Width           =   3255
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmImpuestos.frx":0CEA
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   79
      Columns.Count   =   3
      Columns(0).Width=   2646
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4419
      Columns(1).Caption=   "NOMBRE"
      Columns(1).Name =   "NOMBRE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "COD_DEN"
      Columns(2).Name =   "COD_DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   1614
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgImpuestos 
      Height          =   4785
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   9690
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   17
      stylesets.count =   4
      stylesets(0).Name=   "Heredado"
      stylesets(0).BackColor=   16776960
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmImpuestos.frx":0D06
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmImpuestos.frx":0D22
      stylesets(2).Name=   "NoHeredado"
      stylesets(2).BackColor=   8454143
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmImpuestos.frx":0D3E
      stylesets(3).Name=   "NoVigente"
      stylesets(3).BackColor=   14671839
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmImpuestos.frx":0D5A
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      BalloonHelp     =   0   'False
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   17
      Columns(0).Width=   3200
      Columns(0).Caption=   "PAIS"
      Columns(0).Name =   "PAIS"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "PROVI"
      Columns(1).Name =   "PROVI"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "IMPUESTO"
      Columns(2).Name =   "IMPUESTO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "DESCR"
      Columns(3).Name =   "DESCR"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   3200
      Columns(4).Caption=   "VALOR"
      Columns(4).Name =   "VALOR"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   0
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ID"
      Columns(5).Name =   "ID"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "HEREDADO"
      Columns(6).Name =   "HEREDADO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "PAIS_ID"
      Columns(7).Name =   "PAIS_ID"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "PROVI_ID"
      Columns(8).Name =   "PROVI_ID"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "VIGENTE"
      Columns(9).Name =   "VIGENTE"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "IMPUESTO_PAI"
      Columns(10).Name=   "IMPUESTO_PAIPROVI"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "RETENIDO"
      Columns(11).Name=   "RETENIDO"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "IMPUESTO_ID"
      Columns(12).Name=   "IMPUESTO_ID"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "IMPUESTO_ALTA"
      Columns(13).Name=   "IMPUESTO_ALTA"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "IMPUESTO_VALOR"
      Columns(14).Name=   "IMPUESTO_VALOR"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "Concepto"
      Columns(15).Name=   "CONCEPTO"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "Grupo Compatibilidad"
      Columns(16).Name=   "GRUPO_COMP"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      _ExtentX        =   17092
      _ExtentY        =   8440
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmImpuestos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GMN1Cod As String
Public GMN2Cod As String
Public GMN3Cod As String
Public GMN4Cod As String

Public ArticuloCod As String

Public PermiteModif As Boolean

Public g_sOrigen As String
Public g_sCaptionFormulario As String
Public g_lAtribId As Long
Public g_iNivelCategoria As Integer
Public g_ConceptoLineaCatalogo As Variant

Private m_iTipo As tipoimpuesto
'Impuestos en grid
Private oImpuestos As CImpuestos

''' Coleccion de paises
Private m_oPaises As CPaises
Private m_sPais As String
Private m_bCargarComboDesdePai As Boolean

''' Coleccion de provincias
Private m_oProvincias As CProvincias
Private m_sProvi As String
Private m_bCargarComboDesdeProvi As Boolean

''' Coleccion de Impuestos para el combo por pais o pais + provi
Private m_oComboImpuestos As CImpuestos
Private m_sImpuesto As String

''' Control de errores
Private bValError As Boolean

Private oIBaseDatos As IBaseDatos

''
Private m_oImpuestosExistentes As CImpuestos

Private m_bCambioSelCloseUpTab As Boolean
Private m_bCambioSelCloseUpLocked As Boolean

Private m_sCaptionEdicion As String
Private m_sCaptionConsulta As String
Private m_sCaptionFormularioCoste As String
Private m_sCaptionFormularioLineaCatalogo As String
Private m_lGrupoCompatibilidad As Long

'Concepto
Private ms_ImpuestoArticuloGasto As String
Private ms_ImpuestoArticuloInversi�n As String

''' <summary>
''' Situarnos en la fila de adicion
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdA�adir_Click()
    If Me.sdbgImpuestos.DataChanged = True Then
        bValError = False
    
        sdbgImpuestos.Update
        If bValError = True Then
            Exit Sub
        End If
    End If

    sdbgImpuestos.Scroll 0, sdbgImpuestos.Rows - sdbgImpuestos.Row
    
    If sdbgImpuestos.VisibleRows > 0 Then
        
        If sdbgImpuestos.VisibleRows > sdbgImpuestos.Rows Then
            sdbgImpuestos.Row = sdbgImpuestos.Rows
        Else
            sdbgImpuestos.Row = sdbgImpuestos.Rows - (sdbgImpuestos.Rows - sdbgImpuestos.VisibleRows) - 1
        End If
        
    End If
    
    sdbgImpuestos.Col = 0
    sdbgImpuestos.Columns("PAIS_ID").Value = gParametrosInstalacion.gsPais
    
    m_oPaises.CargarTodosLosPaises , , , , , False
    CargarGridConPaises
    
    sdbgImpuestos.Columns("PAIS").Text = m_oPaises.Item(gParametrosInstalacion.gsPais).Cod & " - " & m_oPaises.Item(gParametrosInstalacion.gsPais).Den
    sdbgImpuestos.Col = 0
    If Me.Visible Then sdbgImpuestos.SetFocus
    
    Me.cmdDeshacer.Enabled = True
End Sub

''' <summary>
''' Deshacer la edicion en la grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdDeshacer_Click()
    sdbgImpuestos.CancelUpdate
    sdbgImpuestos.DataChanged = False
    
    Me.cmdDeshacer.Enabled = False
    
    m_bCargarComboDesdePai = False
    m_bCargarComboDesdeProvi = False
End Sub

Private Sub cmdEdicionConsulta_Click()
    Dim iRowActual As Long
    bValError = False
    If Me.cmdEdicionConsulta.caption = m_sCaptionEdicion Then
        Me.cmdA�adir.Visible = True
        Me.cmdEliminar.Visible = True
        Me.cmdDeshacer.Visible = True
           
        Me.sdbgImpuestos.AllowAddNew = True
        Me.cmdEdicionConsulta.caption = m_sCaptionConsulta
        sdbgImpuestos.Columns("PAIS").DropDownHwnd = sdbddPaises.hWnd
        sdbgImpuestos.Columns("PROVI").DropDownHwnd = sdbddProvincias.hWnd
    ElseIf Me.cmdEdicionConsulta.caption = m_sCaptionConsulta Then
        'Les quito el manejador a las 2 columnas ya que cuando grabo un nuevo impuesto y habia ya mas impuestos grabados
        'al dar al boton de consulta para que el nuevo impuesto se grabe en BD, al resto de impuestos les desaparecen los textos de estas 2 columnas
        sdbgImpuestos.Columns("PAIS").DropDownHwnd = 0
        sdbgImpuestos.Columns("PROVI").DropDownHwnd = 0
        Me.sdbgImpuestos.Update
        If Not bValError Then
            'Si al actualizar da error no oculto los botones
            Me.cmdA�adir.Visible = False
            Me.cmdEliminar.Visible = False
            Me.cmdDeshacer.Visible = False
            Me.cmdRestaurar.Visible = False
            Me.sdbgImpuestos.AllowAddNew = False
            Me.cmdEdicionConsulta.caption = m_sCaptionEdicion
        End If
    End If
End Sub

''' <summary>
''' Elimina el/los impuesto/s seleccionado/s
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdEliminar_Click()
    Dim aIdentificadores As Variant
    Dim aHeredados As Variant
    Dim udtTeserror As TipoErrorSummit
    Dim i As Integer
    
    If sdbgImpuestos.Rows = 0 Then Exit Sub
    
    If sdbgImpuestos.SelBookmarks.Count = 0 Then sdbgImpuestos.SelBookmarks.Add sdbgImpuestos.Bookmark
    
    Screen.MousePointer = vbHourglass
    
    ReDim aIdentificadores(0)
    ReDim aHeredados(0)
    
    i = 0
    While i < sdbgImpuestos.SelBookmarks.Count
        If sdbgImpuestos.Columns("ID").CellValue(sdbgImpuestos.SelBookmarks(i)) = "" Then
            'No en bbdd, luego quitar del grid
        Else
            If sdbgImpuestos.Columns("HEREDADO").CellValue(sdbgImpuestos.SelBookmarks(i)) Then
                'A tabla IMPUESTO_NO_MAT
                ReDim Preserve aHeredados(UBound(aHeredados) + 1)
                
                aHeredados(UBound(aHeredados)) = sdbgImpuestos.Columns("IMPUESTO_VALOR").CellValue(sdbgImpuestos.SelBookmarks(i))
            Else
                'Eliminar
                ReDim Preserve aIdentificadores(UBound(aIdentificadores) + 1)
                
                aIdentificadores(UBound(aIdentificadores)) = sdbgImpuestos.Columns("ID").CellValue(sdbgImpuestos.SelBookmarks(i))
            End If
            
            oImpuestos.Remove sdbgImpuestos.Columns("ID").CellValue(sdbgImpuestos.SelBookmarks(i))
        End If
        
        i = i + 1
    Wend
    
    udtTeserror = oImpuestos.EliminarImpuestos(aIdentificadores, aHeredados, GMN1Cod, GMN2Cod, GMN3Cod, GMN4Cod, ArticuloCod, m_iTipo, g_iNivelCategoria)
    If udtTeserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError udtTeserror
        Exit Sub
    End If
    
    sdbgImpuestos.DeleteSelected
    DoEvents
    
    sdbgImpuestos.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Restaurar el contenido de la grid desde la base de datos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdRestaurar_Click()
    cmdDeshacer_Click

    Me.sdbgImpuestos.RemoveAll

    CargarGrid
End Sub

''' <summary>
''' Cargar la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    PonerFieldSeparator Me
           
    CargarGrid
    
    sdbddPaises.AddItem ""
    sdbddProvincias.AddItem ""
    sdbddImpuestos.AddItem ""
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
    
    m_oPaises.CargarTodosLosPaises , , , , , False
    
    CargarGridConPaises
    
    sdbgImpuestos.Columns("PAIS").DropDownHwnd = sdbddPaises.hWnd
    sdbgImpuestos.Columns("PROVI").DropDownHwnd = sdbddProvincias.hWnd
    sdbgImpuestos.Columns("IMPUESTO").DropDownHwnd = sdbddImpuestos.hWnd
    
    sdbgImpuestos.Columns("VALOR").NumberFormat = FormateoNumericoComp(2, True)
    sdbddImpuestos.Columns("VALOR").NumberFormat = FormateoNumericoComp(2, True)
    
    Set m_oImpuestosExistentes = oFSGSRaiz.Generar_CImpuestos
    m_oImpuestosExistentes.CargarTodosLosImpuestos gParametrosInstalacion.gIdioma
    
    If g_sOrigen = "frmCatalogo_ImpuestosCategorias" Or g_sOrigen = "frmCatalogo_ImpuestosCostesLineas" Or g_sOrigen = "frmCatalogo_ImpuestosLineas" Then
        cmdEdicionConsulta.Visible = True
        
        Me.cmdA�adir.Visible = False
        Me.cmdEliminar.Visible = False
        Me.cmdDeshacer.Visible = False
        
        Me.cmdRestaurar.Visible = False
           
        Me.sdbgImpuestos.AllowAddNew = False
    Else
        cmdEdicionConsulta.Visible = False
        If Not PermiteModif Then
            Me.cmdA�adir.Visible = False
            Me.cmdEliminar.Visible = False
            Me.cmdDeshacer.Visible = False
            
            Me.cmdRestaurar.Left = Me.cmdA�adir.Left
               
            Me.sdbgImpuestos.AllowAddNew = False
        End If
    End If
End Sub

''' <summary>
''' Redimensionar el grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Form_Resize()
    If Me.Width < 500 Then Exit Sub
    If Me.Height < 1700 Then Exit Sub

    Me.sdbgImpuestos.Width = Me.Width - 400
    Me.sdbgImpuestos.Height = Me.Height - picNavigate.Height - 620
    
    Me.sdbgImpuestos.ScrollBars = ssScrollBarsAutomatic
    
    Me.sdbgImpuestos.Columns("PAIS").Width = Me.sdbgImpuestos.Width * 0.16
    Me.sdbgImpuestos.Columns("PROVI").Width = Me.sdbgImpuestos.Width * 0.16
    Me.sdbgImpuestos.Columns("IMPUESTO").Width = Me.sdbgImpuestos.Width * 0.16
    Me.sdbgImpuestos.Columns("DESCR").Width = Me.sdbgImpuestos.Width * 0.36
    Me.sdbgImpuestos.Columns("VALOR").Width = Me.sdbgImpuestos.Width * 0.1
End Sub

''' <summary>
''' Descargar las variables de la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Form_Unload(Cancel As Integer)
   
    If Me.sdbgImpuestos.DataChanged = True Then
        bValError = False
    
        sdbgImpuestos.Update
        
        If bValError = True Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    GMN1Cod = ""
    GMN2Cod = ""
    GMN3Cod = ""
    GMN4Cod = ""
    
    Select Case m_iTipo
    Case tipoimpuesto.CosteCategoria
        If Me.sdbgImpuestos.Rows > 0 Then
            frmCatalogo.sdbgCostesCategoria.Columns("IMPUESTOS").Value = True
        Else
            frmCatalogo.sdbgCostesCategoria.Columns("IMPUESTOS").Value = False
        End If
        frmCatalogo.sdbgCostesCategoria.Refresh
    Case tipoimpuesto.CosteLineaCatalogo
        If Me.sdbgImpuestos.Rows > 0 Then
            frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo.Columns("IMPUESTOS").Value = True
        Else
            frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo.Columns("IMPUESTOS").Value = False
        End If
        frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo.Refresh
    Case tipoimpuesto.LineaCatalogo
        If Me.sdbgImpuestos.Rows > 0 Then
            frmCatalogo.sdbgAdjudicaciones.Columns("IMPUESTOS_HIDDEN").Value = True
        Else
            frmCatalogo.sdbgAdjudicaciones.Columns("IMPUESTOS_HIDDEN").Value = False
        End If
        frmCatalogo.sdbgAdjudicaciones.Refresh
    End Select
    
    ArticuloCod = IIf(ArticuloCod <> "", CStr(oImpuestos.Count), "")
    
    Set oIBaseDatos = Nothing
    Set m_oPaises = Nothing
    Set m_oProvincias = Nothing
    Set m_oComboImpuestos = Nothing
    Set m_oImpuestosExistentes = Nothing
End Sub

''' <summary>
''' Procedimiento que carga los textos que aparecer�n en el formulario en el idioma que corresponda
''' </summary>
''' <remarks>Llamada desde: form_load ; Tiempo m�ximo: 0</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_IMPUESTO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        sdbgImpuestos.Columns("PAIS").caption = Ador(0).Value
        Ador.MoveNext
        sdbgImpuestos.Columns("PROVI").caption = Ador(0).Value
        Ador.MoveNext
        sdbgImpuestos.Columns("IMPUESTO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgImpuestos.Columns("DESCR").caption = Ador(0).Value
        Ador.MoveNext
        sdbgImpuestos.Columns("VALOR").caption = Ador(0).Value
        sdbddImpuestos.Columns("VALOR").caption = Ador(0).Value
        
        Ador.MoveNext
        Me.cmdA�adir.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        
        Ador.MoveNext
        sdbddPaises.Columns(0).caption = Ador(0).Value
        sdbddProvincias.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPaises.Columns(1).caption = Ador(0).Value
        sdbddProvincias.Columns(1).caption = Ador(0).Value
        sdbddImpuestos.Columns("DESCR").caption = Ador(0).Value
                
        Ador.MoveNext
        Me.sdbddImpuestos.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbddImpuestos.Columns("RETENIDO").caption = Ador(0).Value
        Ador.MoveNext
        m_sCaptionEdicion = Ador(0).Value
        Me.cmdEdicionConsulta.caption = m_sCaptionEdicion
        Ador.MoveNext
        m_sCaptionConsulta = Ador(0).Value
        Ador.MoveNext
        m_sCaptionFormularioCoste = Ador(0).Value
        Ador.MoveNext
        m_sCaptionFormularioLineaCatalogo = Ador(0).Value
        
        Select Case g_sOrigen
        Case "frmCatalogo_ImpuestosCategorias", "frmCatalogo_ImpuestosCostesLineas"
            Me.caption = m_sCaptionFormularioCoste & " " & g_sCaptionFormulario
        Case "frmCatalogo_ImpuestosLineas"
            Me.caption = m_sCaptionFormularioLineaCatalogo & " " & g_sCaptionFormulario
        End Select
        
        Ador.MoveNext
        ms_ImpuestoArticuloGasto = Ador(0).Value 'del impuesto es Gasto. Concepto del art�culo es Inversi�n.
        Ador.MoveNext
        ms_ImpuestoArticuloInversi�n = Ador(0).Value 'del impuesto es Inversi�n. Concepto del art�culo es Gasto.
        
        Ador.Close
    End If
    
    Set Ador = Nothing
    
End Sub
''' <summary>
''' Al cerrar el combo, si ha habido cambio de impuesto, carga descripci�n y valor
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddImpuestos_CloseUp()
    If sdbgImpuestos.Columns("IMPUESTO").Value <> "" Then
        If sdbddImpuestos.Columns(0).Value = m_sImpuesto Then
            Exit Sub
        Else
            sdbgImpuestos.Columns("DESCR").Value = sdbddImpuestos.Columns(1).Value
            sdbgImpuestos.Columns("VALOR").Value = sdbddImpuestos.Columns(2).Value

            sdbgImpuestos.Columns("RETENIDO").Value = sdbddImpuestos.Columns(3).Value
            sdbgImpuestos.Columns("VIGENTE").Value = sdbddImpuestos.Columns(4).Value

            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = sdbddImpuestos.Columns(5).Value

            sdbgImpuestos.Columns("IMPUESTO_ID").Value = sdbddImpuestos.Columns(6).Value
            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = sdbddImpuestos.Columns(7).Value

            sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "0"
            
            Me.sdbgImpuestos.Columns("CONCEPTO").Value = sdbddImpuestos.Columns(8).Value
            Me.sdbgImpuestos.Columns("GRUPO_COMP").Value = sdbddImpuestos.Columns(9).Value
        End If
    Else
        sdbgImpuestos.Columns("DESCR").Value = ""
        sdbgImpuestos.Columns("VALOR").Value = ""
        
        sdbgImpuestos.Columns("IMPUESTO_ID").Value = ""
        sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
        sdbgImpuestos.Columns("CONCEPTO").Value = ""
    End If
    m_sImpuesto = ""
End Sub

''' <summary>
''' Abrir el combo de Impuestos de la forma adecuada
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddImpuestos_DropDown()
    Dim oImpuestos As CImpuestos
    Dim oImpuesto As CImpuesto
    
    If Me.sdbgImpuestos.Columns("IMPUESTO").Locked = True Then
        sdbddImpuestos.DroppedDown = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    sdbddImpuestos.RemoveAll
    
    Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
    m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, sdbgImpuestos.Columns("PROVI_ID").Value, gParametrosInstalacion.gIdioma
    
    If m_oComboImpuestos.Count > 0 Then
        For Each oImpuesto In m_oComboImpuestos
            sdbddImpuestos.AddItem oImpuesto.CodImpuesto & Chr(m_lSeparador) & oImpuesto.Descripcion & Chr(m_lSeparador) & oImpuesto.valor _
            & Chr(m_lSeparador) & oImpuesto.Retenido & Chr(m_lSeparador) & oImpuesto.Vigencia & Chr(m_lSeparador) & oImpuesto.ImpuestoPaisProvincia _
            & Chr(m_lSeparador) & oImpuesto.IdImpuesto & Chr(m_lSeparador) & oImpuesto.IDValor & Chr(m_lSeparador) & oImpuesto.Concepto & Chr(m_lSeparador) & oImpuesto.GrupoCompatibilidad
        Next
    Else
        For Each oImpuesto In m_oImpuestosExistentes
            sdbddImpuestos.AddItem oImpuesto.CodImpuesto & Chr(m_lSeparador) & oImpuesto.Descripcion & Chr(m_lSeparador) & oImpuesto.valor _
            & Chr(m_lSeparador) & oImpuesto.Retenido & Chr(m_lSeparador) & oImpuesto.Vigencia & Chr(m_lSeparador) & oImpuesto.ImpuestoPaisProvincia _
            & Chr(m_lSeparador) & oImpuesto.IdImpuesto & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oImpuesto.Concepto & Chr(m_lSeparador) & oImpuesto.GrupoCompatibilidad
        Next
        
        Me.sdbgImpuestos.Columns("VALOR").Locked = False
    End If
    
    If sdbddImpuestos.Rows = 0 Then
        sdbddImpuestos.AddItem ""
    End If
    
    If m_sImpuesto = "@@@" Then
        m_sImpuesto = ""
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    sdbgImpuestos.ActiveCell.SelStart = 0
    sdbgImpuestos.ActiveCell.SelLength = Len(sdbgImpuestos.ActiveCell.Text)
    
    Set oImpuestos = Nothing
    
    Screen.MousePointer = vbNormal
    m_sImpuesto = sdbgImpuestos.Columns("IMPUESTO_VALOR").Value
End Sub
''' <summary>
''' Definir que columna es la de busqueda y seleccion, y cual llevaremos a la grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddImpuestos_InitColumnProps()
    sdbddImpuestos.DataFieldList = "Column 0"
    sdbddImpuestos.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">seleccion</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddImpuestos_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddImpuestos.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddImpuestos.Rows - 1
            bm = sdbddImpuestos.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddImpuestos.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddImpuestos.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
''' <summary>
''' Validar la seleccion (nulo o existente)
''' </summary>
''' <param name="Text">seleccion</param>
''' <param name="RtnPassed">correcto o no</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddImpuestos_ValidateList(Text As String, RtnPassed As Integer)
    Dim oImpuesto As CImpuesto
    Dim oImpuestoCloseUp As CImpuesto
    Dim bEncontrado As Boolean
    Dim bm As Variant
    
    ''' Si es nulo, correcto
    
    If Trim(Text) = "" Then
        RtnPassed = True
        
        sdbgImpuestos.Columns("IMPUESTO").Value = ""
        
        sdbgImpuestos.Columns("DESCR").Value = ""
        sdbgImpuestos.Columns("VALOR").Value = ""
        
        sdbgImpuestos.Columns("VIGENTE").Value = ""
        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
        
        sdbgImpuestos.Columns("IMPUESTO_ID").Value = ""
        sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
        
        sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "0"
    Else
        
        ''' Comprobar la existencia en la lista
        Text = UCase(Text)
        
        'Existencia en bd
        bEncontrado = False
        For Each oImpuesto In m_oImpuestosExistentes
            If oImpuesto.CodImpuesto = sdbgImpuestos.ActiveCell.Value Then
                bEncontrado = True
                Exit For
            End If
        Next
        
        If bEncontrado Then
            RtnPassed = True
                               
            If sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "1" Then Exit Sub
            
            bEncontrado = False

            For Each oImpuestoCloseUp In m_oComboImpuestos
                If NullToStr(oImpuestoCloseUp.CodPais) = Me.sdbgImpuestos.Columns("PAIS_ID").Value Then
                    If NullToStr(oImpuestoCloseUp.CodProvincia) = Me.sdbgImpuestos.Columns("PROVI_ID").Value Then
                        If NullToStr(oImpuestoCloseUp.CodImpuesto) = sdbgImpuestos.ActiveCell.Value Then
                            If NullToStr(oImpuestoCloseUp.IdImpuesto) = Me.sdbgImpuestos.Columns("IMPUESTO_ID").Value Then
                                bEncontrado = True
                            End If
                            Exit For
                        End If
                    End If
                End If
            Next
                        
            If Not bEncontrado Then
                sdbgImpuestos.Columns("DESCR").Value = oImpuesto.Descripcion
                
                sdbgImpuestos.Columns("VIGENTE").Value = oImpuesto.Vigencia
                
                sdbgImpuestos.Columns("IMPUESTO_ID").Value = oImpuesto.IdImpuesto
                
                sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "0"
                                
                sdbgImpuestos.Columns("VALOR").Value = ""
                sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
                sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
                sdbgImpuestos.Columns("CONCEPTO").Value = oImpuesto.Concepto
                sdbgImpuestos.Columns("GRUPO_COMP").Value = oImpuesto.GrupoCompatibilidad
            End If
        Else
            sdbgImpuestos.Columns("DESCR").Value = ""
            sdbgImpuestos.Columns("VALOR").Value = ""
            
            sdbgImpuestos.Columns("IMPUESTO_ID").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
                
            If oMensajes.PreguntaNuevoImpuesto = vbYes Then
                frmNuevoImpuesto.m_sCodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value
                frmNuevoImpuesto.g_lGrupoCompatibilidad = m_lGrupoCompatibilidad
                frmNuevoImpuesto.g_ConceptoLineaCatalogo = g_ConceptoLineaCatalogo
                frmNuevoImpuesto.Show vbModal
                RtnPassed = False
            Else
                sdbgImpuestos.Columns("IMPUESTO").Value = ""
                sdbgImpuestos.Col = 2
                RtnPassed = False
            End If
        End If
    
    End If
End Sub
''' <summary>
''' Al cerrar el combo, si ha habido cambio de provincia, limpia el impuesto
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddProvincias_CloseUp()
    Dim oImpuesto As CImpuesto
    
    If sdbgImpuestos.Columns("PROVI").Value <> "" Then
        If sdbddProvincias.Columns(0).Value = m_sProvi Then
            Exit Sub
        Else
            sdbgImpuestos.Columns("PROVI_ID").Value = sdbddProvincias.Columns(0).Value
            
            If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
            
            Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
            m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, sdbgImpuestos.Columns("PROVI_ID").Value, gParametrosInstalacion.gIdioma
            
            For Each oImpuesto In m_oComboImpuestos
                If oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value Then
                    sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia
                    
                    If sdbgImpuestos.Columns("VALOR").Value = "" Then
                        Exit For
                    Else
                        If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor
                            
                            Exit For
                        End If
                    End If
                End If
            Next
        End If
        
    Else
        sdbgImpuestos.Columns("PROVI_ID").Value = ""
    End If
    m_sProvi = ""
End Sub

''' <summary>
''' Abrir el combo de Paises de la forma adecuada
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddProvincias_DropDown()

    Dim oPais As CPais
        
    If Me.sdbgImpuestos.Columns("PROVI").Locked = True Then
        sdbddProvincias.DroppedDown = False
        Exit Sub
    End If
    
    If sdbgImpuestos.Columns("PAIS").Text = "" Then
        sdbddProvincias.RemoveAll
        sdbddProvincias.AddItem ""
        sdbddProvincias.Refresh
        
        sdbgImpuestos.ActiveCell.SelStart = 0
        sdbgImpuestos.ActiveCell.SelLength = Len(sdbgImpuestos.ActiveCell.Text)
        Screen.MousePointer = vbNormal
    
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
        
    If m_oPaises.Item(sdbgImpuestos.Columns("PAIS_ID").Value) Is Nothing Then
        If m_bCargarComboDesdePai Then
            m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbgImpuestos.ActiveCell.Value), , , False
        Else
            m_oPaises.CargarTodosLosPaises , , , , , False
        End If
    End If
    
    Set oPais = m_oPaises.Item(sdbgImpuestos.Columns("PAIS_ID").Value)
      
    If m_bCargarComboDesdeProvi And sdbgImpuestos.ActiveCell.Value <> "" Then
        oPais.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbgImpuestos.ActiveCell.Value), , , False
    Else
        oPais.CargarTodasLasProvincias
    End If
    
    Set m_oProvincias = oPais.Provincias
        
    Set oPais = Nothing
    
    CargarGridConProvincias
    
    sdbgImpuestos.ActiveCell.SelStart = 0
    sdbgImpuestos.ActiveCell.SelLength = Len(sdbgImpuestos.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
    m_sProvi = sdbgImpuestos.Columns("PROVI").Value
End Sub

''' <summary>
''' Definir que columna es la de busqueda y seleccion, y cual llevaremos a la grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddProvincias_InitColumnProps()
    
    sdbddProvincias.DataFieldList = "Column 0"
    sdbddProvincias.DataFieldToDisplay = "Column 2"
    
End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">seleccion</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddProvincias_PositionList(ByVal Text As String)
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProvincias.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProvincias.Rows - 1
            bm = sdbddProvincias.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProvincias.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProvincias.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

''' <summary>
''' Validar la seleccion (nulo o existente)
''' </summary>
''' <param name="Text">seleccion</param>
''' <param name="RtnPassed">correcto o no</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddProvincias_ValidateList(Text As String, RtnPassed As Integer)
    Dim oPais As CPais
    Dim oImpuesto As CImpuesto
    Dim sProvincia As String
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
        
        sdbgImpuestos.Columns("PROVI").Value = ""
        sdbgImpuestos.Columns("PROVI_ID").Value = ""
        
        If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
        
        Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
        m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, "", gParametrosInstalacion.gIdioma
                    
        For Each oImpuesto In m_oComboImpuestos
            If oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value Then
                sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia
                
                If sdbgImpuestos.Columns("VALOR").Value = "" Then
                    Exit For
                Else
                    If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                        sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor
                        
                        Exit For
                    End If
                End If
            End If
        Next

    Else
        
        ''' Comprobar la existencia en la lista
                        
        Set oPais = m_oPaises.Item(sdbgImpuestos.Columns("PAIS_ID").Value)
        
        If oPais Is Nothing Then
            sdbgImpuestos.Columns("PAIS").Value = ""
            sdbgImpuestos.Columns("PAIS_ID").Value = ""
        
            sdbgImpuestos.Columns("PROVI").Value = ""
            sdbgImpuestos.Columns("PROVI_ID").Value = ""
            
            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
            
            Exit Sub
        End If
        
        If oPais.Provincias Is Nothing Then
            oPais.CargarTodasLasProvincias
            If oPais.Provincias Is Nothing Then
                sdbgImpuestos.Columns("PROVI").Value = ""
                sdbgImpuestos.Columns("PROVI_ID").Value = ""

                If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
                sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
                
                Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
                m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, "", gParametrosInstalacion.gIdioma
                            
                For Each oImpuesto In m_oComboImpuestos
                    If oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value Then
                        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia

                        If sdbgImpuestos.Columns("VALOR").Value = "" Then
                            Exit For
                        Else
                            If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                                sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor
                                
                                Exit For
                            End If
                        End If
                    End If
                Next
                
                Exit Sub
            End If
        End If
        
        Set m_oProvincias = oPais.Provincias
        CargarGridConProvincias
        
        sProvincia = Text
        If Not (sdbgImpuestos.Columns("PROVI").Value = "") Then sProvincia = sdbgImpuestos.Columns("PROVI_ID").Value
        
        If Not oPais.Provincias.Item(sProvincia) Is Nothing Then
            RtnPassed = True
            sdbgImpuestos.Columns("PROVI_ID").Value = oPais.Provincias.Item(sProvincia).Cod
            sdbgImpuestos.Columns("PROVI").Value = oPais.Provincias.Item(sProvincia).Cod & " - " & oPais.Provincias.Item(sProvincia).Den
            
            If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
            
            Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
            m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, sdbgImpuestos.Columns("PROVI_ID").Value, gParametrosInstalacion.gIdioma
            
            For Each oImpuesto In m_oComboImpuestos
                If oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value Then
                    sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia
                    
                    If sdbgImpuestos.Columns("VALOR").Value = "" Then
                        Exit For
                    Else
                        If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor
                            
                            Exit For
                        End If
                    End If
                End If
            Next
        Else
            sdbgImpuestos.Columns("PROVI").Value = ""
            sdbgImpuestos.Columns("PROVI_ID").Value = ""

            If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
            
            Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
            m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, "", gParametrosInstalacion.gIdioma
                        
            For Each oImpuesto In m_oComboImpuestos
                If oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value Then
                    sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia

                    If sdbgImpuestos.Columns("VALOR").Value = "" Then
                        Exit For
                    Else
                        If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor
                            
                            Exit For
                        End If
                    End If
                End If
            Next
        End If
    
    End If
    
End Sub


''' <summary>
''' Al cerrar el combo, si ha habido cambio de pais, limpia la provincia e impuesto
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddPaises_CloseUp()
    Dim oImpuesto As CImpuesto
    
    If sdbgImpuestos.Columns("PAIS").Value <> "" Then
        If sdbddPaises.Columns(0).Value = m_sPais Then
            Exit Sub
        Else
            sdbgImpuestos.Columns("PROVI").Value = ""
            sdbgImpuestos.Columns("PROVI_ID").Value = ""
            
            If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
            
            Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
            m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS").Value, "", gParametrosInstalacion.gIdioma
                        
            For Each oImpuesto In m_oComboImpuestos
                If oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value Then
                    sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia

                    If sdbgImpuestos.Columns("VALOR").Value = "" Then
                        Exit For
                    Else
                        If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor
                            
                            Exit For
                        End If
                    End If
                End If
            Next
        End If
        
        sdbgImpuestos.Columns("PAIS_ID").Value = sdbddPaises.Columns(0).Value
    Else
        sdbgImpuestos.Columns("PAIS_ID").Value = ""
    End If
    m_sPais = ""
End Sub


''' <summary>
''' Abrir el combo de Paises de la forma adecuada
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddPaises_DropDown()

    Screen.MousePointer = vbHourglass
    
    If Me.sdbgImpuestos.Columns("PAIS").Locked = True Then
        sdbddPaises.DroppedDown = False
        Exit Sub
    End If
    
    If m_bCargarComboDesdePai Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbgImpuestos.ActiveCell.Value), , , False
    Else
        m_oPaises.CargarTodosLosPaises , , , , , False
    End If
        
    CargarGridConPaises
    
    sdbgImpuestos.ActiveCell.SelStart = 0
    sdbgImpuestos.ActiveCell.SelLength = Len(sdbgImpuestos.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
    m_sPais = sdbgImpuestos.Columns("PAIS").Value
End Sub

''' <summary>
''' Definir que columna es la de busqueda y seleccion, y cual llevaremos a la grid
''' </summary>
''' <param name="ParametroEntrada1">Explicaci�npar�metro1</param>
''' <param name="ParametroEntrada2">Explicaci�npar�metro2</param>
''' <returns>Explicaci�nretornodelafunci�n</returns>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddPaises_InitColumnProps()
    
    sdbddPaises.DataFieldList = "Column 0"
    sdbddPaises.DataFieldToDisplay = "Column 2"
    
End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">seleccion</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddPaises_PositionList(ByVal Text As String)
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddPaises.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddPaises.Rows - 1
            bm = sdbddPaises.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddPaises.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddPaises.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

''' <summary>
''' Validar la selecci�n (nulo o existente)
''' </summary>
''' <param name="Text">seleccion</param>
''' <param name="RtnPassed">correcto o no</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddPaises_ValidateList(Text As String, RtnPassed As Integer)
    Dim oImpuesto As CImpuesto
    
    If Text = "..." Then RtnPassed = False
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

        sdbgImpuestos.Columns("PAIS").Value = ""
        sdbgImpuestos.Columns("PAIS_ID").Value = ""
        
        sdbgImpuestos.Columns("PROVI").Value = ""
        sdbgImpuestos.Columns("PROVI_ID").Value = ""

        sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
    Else
        
        ''' Comprobar la existencia en la lista
        
        If InStr(Text, " -") > 0 Then Text = Left(Text, InStr(Text, " -") - 1)
        
        m_oPaises.CargarTodosLosPaises Text, , True, , , False
        If Not m_oPaises.Item(Text) Is Nothing Then
            RtnPassed = True
            sdbgImpuestos.Columns("PAIS_ID").Value = m_oPaises.Item(Text).Cod
            sdbgImpuestos.Columns("PAIS").Value = m_oPaises.Item(Text).Cod & " - " & m_oPaises.Item(Text).Den
            
            sdbgImpuestos.Columns("PROVI").Value = ""
            sdbgImpuestos.Columns("PROVI_ID").Value = ""
            
            If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
            
            Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
            m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, "", gParametrosInstalacion.gIdioma
                        
            For Each oImpuesto In m_oComboImpuestos
                If oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value Then
                    sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia

                    If sdbgImpuestos.Columns("VALOR").Value = "" Then
                        Exit For
                    Else
                        If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor
                            
                            Exit For
                        End If
                    End If
                End If
            Next
            
        Else
            sdbgImpuestos.Columns("PAIS").Value = ""
            sdbgImpuestos.Columns("PAIS_ID").Value = ""
            
            sdbgImpuestos.Columns("PROVI").Value = ""
            sdbgImpuestos.Columns("PROVI_ID").Value = ""

            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
        End If
    
    End If
    
End Sub

''' <summary>
''' Cargar combo con la coleccion de Paises
''' </summary>
''' <remarks>Llamada desde: sdbddProvincias_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridConProvincias()
    
    Dim oProvi As CProvincia
    
    sdbddProvincias.RemoveAll
    
    For Each oProvi In m_oProvincias
        sdbddProvincias.AddItem oProvi.Cod & Chr(m_lSeparador) & oProvi.Den & Chr(m_lSeparador) & oProvi.Cod & " - " & oProvi.Den
    Next
    
    If sdbddProvincias.Rows = 0 Then
        sdbddProvincias.AddItem ""
    Else
        If m_bCargarComboDesdeProvi And Not m_oProvincias.EOF Then
            sdbddProvincias.AddItem "..."
        End If
    End If
    
End Sub

''' <summary>
''' Cargar combo con la coleccion de Paises
''' </summary>
''' <remarks>Llamada desde: sdbddPaises_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridConPaises()
  
    Dim oPai As CPais
    
    sdbddPaises.RemoveAll
    
    For Each oPai In m_oPaises
        sdbddPaises.AddItem oPai.Cod & Chr(m_lSeparador) & oPai.Den & Chr(m_lSeparador) & oPai.Cod & " - " & oPai.Den
    Next
    
    If sdbddPaises.Rows = 0 Then
        sdbddPaises.AddItem ""
    Else
        If m_bCargarComboDesdePai And Not m_oPaises.EOF Then
            sdbddPaises.AddItem "..."
        End If
    End If
End Sub
''' <summary>
''' Cargar los impuestos
''' </summary>
''' <remarks>Llamada desde: Form_load   cmdRestaurar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGrid()
    Dim Impuesto As CImpuesto
    Dim sLinea As String
    
    Screen.MousePointer = vbHourglass
    
    Set oImpuestos = Nothing
    
    Set oImpuestos = oFSGSRaiz.Generar_CImpuestos
    
    sdbgImpuestos.Columns("PAIS").DropDownHwnd = 0
    sdbgImpuestos.Columns("PROVI").DropDownHwnd = 0
    sdbgImpuestos.Columns("IMPUESTO").DropDownHwnd = 0
    
    Select Case g_sOrigen
    Case "frmCatalogo_ImpuestosCategorias"
        m_iTipo = CosteCategoria
        oImpuestos.CargarTodosLosImpuestosDeCosteDeCategoria g_lAtribId, gParametrosInstalacion.gIdioma, g_iNivelCategoria
    Case "frmCatalogo_ImpuestosCostesLineas"
        m_iTipo = CosteLineaCatalogo
        oImpuestos.CargarTodosLosImpuestosDeCosteDeLineaDeCatalogo g_lAtribId, gParametrosInstalacion.gIdioma
    Case "frmCatalogo_ImpuestosLineas"
        m_iTipo = LineaCatalogo
        oImpuestos.CargarTodosLosImpuestosDeLineaDeCatalogo g_lAtribId, gParametrosInstalacion.gIdioma
    Case Else
        m_iTipo = ArticuloMaterial
        oImpuestos.CargarTodosLosImpuestosDeMatArt GMN1Cod, GMN2Cod, GMN3Cod, GMN4Cod, ArticuloCod, gParametrosInstalacion.gIdioma
    End Select
    
    
    For Each Impuesto In oImpuestos
        Select Case g_sOrigen
        Case "frmCatalogo_ImpuestosLineas"
            'Recojo el grupo de compatibilidad de los impuestos de la linea de catalogo, que debera ser el mismo para todos los impuestos
            m_lGrupoCompatibilidad = Impuesto.GrupoCompatibilidad
        End Select
        sLinea = Impuesto.DenPais & Chr(m_lSeparador) & Impuesto.DenProvincia & Chr(m_lSeparador) & Impuesto.CodImpuesto & Chr(m_lSeparador)
        sLinea = sLinea & Impuesto.Descripcion & Chr(m_lSeparador) & Impuesto.valor & Chr(m_lSeparador) & Impuesto.IDMat & Chr(m_lSeparador)
        sLinea = sLinea & Impuesto.Heredado & Chr(m_lSeparador) & Impuesto.CodPais & Chr(m_lSeparador) & Impuesto.CodProvincia
        sLinea = sLinea & Chr(m_lSeparador) & Impuesto.Vigencia & Chr(m_lSeparador) & Impuesto.ImpuestoPaisProvincia & Chr(m_lSeparador)
        sLinea = sLinea & Impuesto.Retenido & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & Impuesto.IDValor & Chr(m_lSeparador) & Impuesto.Concepto & Chr(m_lSeparador) & Impuesto.GrupoCompatibilidad
        
        Me.sdbgImpuestos.AddItem sLinea
    Next
            
    Screen.MousePointer = vbNormal
End Sub
''' <summary>
''' Posicionar el bookmark en la fila actual despues de eliminar
''' </summary>
''' <param name="RtnDispErrMsg">Si hay error o no</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgImpuestos.Rows = 0) Then
        Exit Sub
    End If
    If Me.Visible Then sdbgImpuestos.SetFocus
    sdbgImpuestos.Bookmark = sdbgImpuestos.RowBookmark(sdbgImpuestos.Row)
End Sub
''' <summary>
''' Si no hay error, volver a la situacion normal
''' </summary>
''' <param name="RtnDispErrMsg">Si hay error o no</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_AfterInsert(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
End Sub
''' <summary>
''' Si no hay error, volver a la situacion normal
''' </summary>
''' <param name="RtnDispErrMsg">Si hay error o no</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0

    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
End Sub
''' <summary>
''' Evita q se meta texto en la columna valor
''' </summary>
''' <param name="ColIndex">Indice de la columna cambiada</param>
''' <param name="OldValue">Valor anterior</param>
''' <param name="Cancel">Para deshacer el cambio</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgImpuestos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
   If sdbgImpuestos.Columns(ColIndex).Name = "VALOR" Then
        If m_bCambioSelCloseUpLocked Then
            m_bCambioSelCloseUpTab = False
            m_bCambioSelCloseUpLocked = False
            Exit Sub
        End If
        
        If Not IsNumeric(sdbgImpuestos.Columns(ColIndex).Value) Then
            If Not m_bCambioSelCloseUpTab Then
                oMensajes.NoValido sdbgImpuestos.Columns(ColIndex).caption
            End If
            Cancel = True
        End If
        
        m_bCambioSelCloseUpTab = False
    End If
End Sub

''' <summary>
''' No Confirmacion antes de eliminar
''' </summary>
''' <param name="Cancel">Si se cancela o no el borrado</param>
''' <param name="DispPromptMsg">si se`pregunat antes de eliminar</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub
''' <summary>
''' Guarda los cambios producidos el grid
''' </summary>
''' <param name="Cancel">Si se cancela o no el update</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim oImpuesto As CImpuesto
    Dim sMensaje As String
    
    bValError = False
    
    Cancel = False
        
    If m_bCambioSelCloseUpTab Then
        Me.sdbgImpuestos.Col = 4
        
        Cancel = True
        
        m_bCambioSelCloseUpTab = False
        
        Exit Sub
    End If
    
    If (Me.sdbgImpuestos.Columns("PAIS_ID").Value = "") Then
        oMensajes.NoValido Me.sdbgImpuestos.Columns("PAIS").caption
        Cancel = True
        bValError = Cancel
        If Me.Visible Then sdbgImpuestos.SetFocus
        cmdDeshacer.Enabled = True
        Exit Sub
    End If
        
    If (Me.sdbgImpuestos.Columns("IMPUESTO").Value = "") Then
        oMensajes.NoValido Me.sdbgImpuestos.Columns("IMPUESTO").caption
        Cancel = True
        bValError = Cancel
        If Me.Visible Then sdbgImpuestos.SetFocus
        cmdDeshacer.Enabled = True
        Exit Sub
    End If
    
    If (Me.sdbgImpuestos.Columns("VALOR").Value = "") Then
        oMensajes.NoValido Me.sdbgImpuestos.Columns("VALOR").caption
        Cancel = True
        bValError = Cancel
        If Me.Visible Then sdbgImpuestos.SetFocus
        cmdDeshacer.Enabled = True
        Exit Sub
    End If
    
    If Me.sdbgImpuestos.IsAddRow Then
        ''Selecc en el combo IVA 16% pero escribes a mano 20% ->IMPUESTO_VALOR es nulo si no exist�a o X si ya exist�a
        sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
        For Each oImpuesto In m_oComboImpuestos
            If NullToStr(oImpuesto.CodPais) = Me.sdbgImpuestos.Columns("PAIS_ID").Value Then
                If NullToStr(oImpuesto.CodProvincia) = Me.sdbgImpuestos.Columns("PROVI_ID").Value Then
                    If NullToStr(oImpuesto.IdImpuesto) = Me.sdbgImpuestos.Columns("IMPUESTO_ID").Value Then
                        If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor
                            Exit For
                        End If
                    End If
                End If
            End If
        Next
        
        ''No repetidos.
        For Each oImpuesto In oImpuestos
            If Me.sdbgImpuestos.Columns("IMPUESTO_VALOR").Value <> "" Then 'Si has seleccionado pais/provincia y luego impuesto. Es q ya esta en bd.
                If NullToStr(oImpuesto.IDValor) = Me.sdbgImpuestos.Columns("IMPUESTO_VALOR").Value Then
                    oMensajes.DatoDuplicado 200
                    Cancel = True
                    bValError = Cancel
                    If Me.Visible Then sdbgImpuestos.SetFocus
                    cmdDeshacer.Enabled = True
                    Exit Sub
                End If
            End If
        Next
        
        If Not IsNull(Me.g_ConceptoLineaCatalogo) And Not IsEmpty(Me.g_ConceptoLineaCatalogo) Then
            If Me.sdbgImpuestos.Columns("CONCEPTO").Value <> CStr(Me.g_ConceptoLineaCatalogo) Then
                sMensaje = ""
                '0-> Gasto
                '1-> Inversi�n
                '2 -> Gasto/Inversi�n
                Select Case Me.sdbgImpuestos.Columns("CONCEPTO").caption
                Case 0
                    If CInt(Me.g_ConceptoLineaCatalogo) < 2 Then
                        sMensaje = Me.sdbgImpuestos.Columns("CONCEPTO").caption & " " & ms_ImpuestoArticuloGasto
                    End If
                Case 1
                    If CInt(Me.g_ConceptoLineaCatalogo) < 2 Then
                        sMensaje = Me.sdbgImpuestos.Columns("CONCEPTO").caption & " " & ms_ImpuestoArticuloInversi�n
                    End If
                Case 2
                    'El impuesto es de Gasto/Inversi�n, el articulo sea cual sea lo admito.
                End Select
                
                If sMensaje <> "" Then
                    oMensajes.NoValido Me.sdbgImpuestos.Columns("CONCEPTO").caption
                    Cancel = True
                    bValError = Cancel
                    If Me.Visible Then sdbgImpuestos.SetFocus
                    cmdDeshacer.Enabled = True
                    Exit Sub
                End If
            End If
        End If
        
        If m_lGrupoCompatibilidad > 0 Then
            If Me.sdbgImpuestos.Columns("GRUPO_COMP").Value <> m_lGrupoCompatibilidad Then
                oMensajes.NoValido Me.sdbgImpuestos.Columns("GRUPO_COMP").caption
                Cancel = True
                bValError = Cancel
                If Me.Visible Then sdbgImpuestos.SetFocus
                cmdDeshacer.Enabled = True
                Exit Sub
            End If
        Else
            Select Case g_sOrigen
            Case "frmCatalogo_ImpuestosLineas"
                'Recojo el grupo de compatibilidad del primer impuesto que se mete para esa linea, por si luego se mete un segundo, que tenga que tener el mismo grupo
                m_lGrupoCompatibilidad = Me.sdbgImpuestos.Columns("GRUPO_COMP").Value
            End Select
        End If
        
        Set oImpuesto = oFSGSRaiz.Generar_CImpuesto
        
        oImpuesto.Art = ArticuloCod
        
        oImpuesto.GMN1Cod = GMN1Cod
        oImpuesto.GMN2Cod = GMN2Cod
        oImpuesto.GMN3Cod = GMN3Cod
        oImpuesto.GMN4Cod = GMN4Cod
           
        ''Los impuestos puedes haberlos cogido de pais � pais+provincia � todos.
        ''  En caso de todos, estas dando de alta en impuestos_mat y impuestos_pais/impuestos_provi
        ''  Eoc, estas dando de alta en impuestos_mat
        oImpuesto.ImpuestoPaisProvincia = Me.sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value
        oImpuesto.IdImpuesto = Me.sdbgImpuestos.Columns("IMPUESTO_ID").Value
        oImpuesto.IDValor = Me.sdbgImpuestos.Columns("IMPUESTO_VALOR").Value
        
        oImpuesto.CodPais = Me.sdbgImpuestos.Columns("PAIS_ID").Value
        oImpuesto.CodProvincia = Me.sdbgImpuestos.Columns("PROVI_ID").Value
        
        oImpuesto.DenPais = Me.sdbgImpuestos.Columns("PAIS").Text
        oImpuesto.DenProvincia = Me.sdbgImpuestos.Columns("PROVI").Text
                               
        oImpuesto.CodImpuesto = Me.sdbgImpuestos.Columns("IMPUESTO").Value
        
        oImpuesto.Descripcion = Me.sdbgImpuestos.Columns("DESCR").Value
        oImpuesto.Heredado = 0
        oImpuesto.valor = Me.sdbgImpuestos.Columns("VALOR").Value
        oImpuesto.Vigencia = Me.sdbgImpuestos.Columns("VIGENTE").Value
        oImpuesto.Retenido = Me.sdbgImpuestos.Columns("RETENIDO").Value
        oImpuesto.Tipo = m_iTipo
        oImpuesto.NivelCategoria = Me.g_iNivelCategoria
        oImpuesto.AtribID = Me.g_lAtribId
        
        Set oIBaseDatos = oImpuesto
    
        teserror = oIBaseDatos.AnyadirABaseDatos
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            Cancel = True
            bValError = Cancel
            If Me.Visible Then sdbgImpuestos.SetFocus
            Set oImpuesto = Nothing
            Exit Sub
        End If
                
        Me.sdbgImpuestos.Columns("ID").Value = oImpuesto.IDMat
        Me.sdbgImpuestos.Columns("HEREDADO").Value = 0
        
        Me.sdbgImpuestos.Columns("IMPUESTO_ID").Value = oImpuesto.IdImpuesto
        Me.sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor
        
        Me.sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "0"
        
        Me.sdbgImpuestos.Columns("HEREDADO").Value = oImpuesto.Heredado
        Me.sdbgImpuestos.Columns("VIGENTE").Value = oImpuesto.Vigencia
        
        Me.sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia
        
        If (oImpuestos.Item(oImpuesto.IDMat) Is Nothing) Then
            oImpuestos.Add oImpuesto.IDMat, oImpuesto.CodImpuesto, oImpuesto.Descripcion, oImpuesto.IdImpuesto, oImpuesto.IDValor, oImpuesto.GMN1Cod _
            , oImpuesto.GMN2Cod, oImpuesto.GMN3Cod, oImpuesto.GMN4Cod, oImpuesto.Art, oImpuesto.valor, oImpuesto.Vigencia, oImpuesto.ImpuestoPaisProvincia _
            , oImpuesto.Heredado, oImpuesto.Retenido, oImpuesto.CodPais, oImpuesto.CodProvincia, , , oImpuesto.FECACT
        End If
        
        Set oImpuesto = Nothing
    End If
End Sub
''' <summary>
''' Evento que salta al producirse un cambio en el grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_Change()
    Dim teserror As TipoErrorSummit
    Dim oImpuesto As CImpuesto
        
    If cmdDeshacer.Enabled = False Then
        cmdDeshacer.Enabled = True
    End If
    
    If Not (Me.sdbgImpuestos.Columns("ID").Value = "") Then
        Set oImpuesto = oImpuestos.Item(Me.sdbgImpuestos.Columns("ID").Value)
    End If
    
    If oImpuesto Is Nothing Then Exit Sub
    
    ''mirar en bd q no lo hayan tocado
    Set oIBaseDatos = oImpuesto

    teserror = oIBaseDatos.IniciarEdicion

    If (teserror.NumError = TESDatoEliminado) Or (teserror.NumError = TESInfModificada) Then
        TratarError teserror
        sdbgImpuestos.DataChanged = False
        Exit Sub
    End If
End Sub

''' <summary>
''' Captura la tecla Supr (Delete) y cuando �sta es presionada se lanza el evento Click asociado al bot�n eliminar para borrar de la BD y de la grid las lineas seleccionadas.
''' </summary>
''' <param name="KeyCode">Contiene el c�digo interno de la tecla pulsada.</param>
''' <param name="Shift">Es la m�scara, sin uso en esta subrutina.</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        If sdbgImpuestos.SelBookmarks.Count > 0 Then
            cmdEliminar_Click
        End If
    End If
    
    Dim sImpuesto As String
    Dim bm As Variant
    Dim i As Integer

    If (KeyCode < 112 Or KeyCode > 123) And (KeyCode < 37 Or KeyCode > 40) And KeyCode <> 27 Then
        With sdbgImpuestos
            If .Col >= 0 Then
                Select Case .Columns(.Col).Name
                    Case "IMPUESTO"
                        If Not sdbddImpuestos.DroppedDown Then
                            m_sImpuesto = "@@@"
                            SendKeys "{F4}"
                        End If
                        sImpuesto = .Columns("IMPUESTO").Text
                        sImpuesto = EliminarCaracteresEspeciales(sImpuesto)
                                                
                        sdbddImpuestos.MoveFirst
                        If sImpuesto <> "" Then
                            For i = 0 To sdbddImpuestos.Rows - 1
                                bm = sdbddImpuestos.GetBookmark(i)
                                If InStr(1, UCase(EliminarCaracteresEspeciales(sdbddImpuestos.Columns("COD").CellText(bm))), UCase(sImpuesto)) > 0 Then
                                    sdbddImpuestos.Bookmark = bm
                                    Exit For
                                End If
                            Next i
                        End If
                        
                        If KeyCode = vbKeyTab Then
                            m_bCambioSelCloseUpTab = True
                        End If
                End Select
            End If
        End With
    End If
End Sub

''' <summary>
''' Evento al moverse de columna o de fila en la grid
''' </summary>
''' <param name="lastRow">Fila desde la que se mueve</param>
''' <param name="LastCol">Columna desde la que se mueve</param>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub sdbgImpuestos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    If Me.sdbgImpuestos.Columns("ID").Value <> "" Then
        Me.sdbgImpuestos.Columns("PAIS").Locked = True
        Me.sdbgImpuestos.Columns("PROVI").Locked = True
        Me.sdbgImpuestos.Columns("IMPUESTO").Locked = True
        
        sdbgImpuestos.Columns("PAIS").DropDownHwnd = 0
        sdbgImpuestos.Columns("PROVI").DropDownHwnd = 0
        sdbgImpuestos.Columns("IMPUESTO").DropDownHwnd = 0
    Else
        Me.sdbgImpuestos.Columns("PAIS").Locked = False
        Me.sdbgImpuestos.Columns("PROVI").Locked = False
        Me.sdbgImpuestos.Columns("IMPUESTO").Locked = False
        
        sdbgImpuestos.Columns("PAIS").DropDownHwnd = sdbddPaises.hWnd
        sdbgImpuestos.Columns("PROVI").DropDownHwnd = sdbddProvincias.hWnd
        sdbgImpuestos.Columns("IMPUESTO").DropDownHwnd = sdbddImpuestos.hWnd
    End If
    
    m_bCambioSelCloseUpLocked = Me.sdbgImpuestos.Columns("VALOR").Locked

    If sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = "" And sdbgImpuestos.Columns("IMPUESTO_ID").Value <> "" Then
        Me.sdbgImpuestos.Columns("VALOR").Locked = False
        If Me.sdbgImpuestos.Col = 4 Then Me.sdbgImpuestos.Col = 4
    ElseIf Me.sdbgImpuestos.IsAddRow Then
        Me.sdbgImpuestos.Columns("VALOR").Locked = False
        If Me.sdbgImpuestos.Col = 4 Then Me.sdbgImpuestos.Col = 4
    Else
        Me.sdbgImpuestos.Columns("VALOR").Locked = True
    End If
        
    If Me.sdbgImpuestos.Columns("HEREDADO").Value = "1" And ArticuloCod = "" Then
        Me.cmdEliminar.Enabled = False
    Else
        Me.cmdEliminar.Enabled = True
    End If
End Sub
''' <summary>
'''     Configura la linea de la grid con el estilo
''' </summary>
''' <param name="Bookmark">del evento</param>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub sdbgImpuestos_RowLoaded(ByVal Bookmark As Variant)
    Dim j As Integer
        
    If sdbgImpuestos.Columns("ID").CellValue(Bookmark) = "" Then Exit Sub
            
    If CStr(sdbgImpuestos.Columns("VIGENTE").CellValue(Bookmark)) = "1" Then
        If CStr(sdbgImpuestos.Columns("HEREDADO").CellValue(Bookmark)) = "1" Then
            For j = 0 To sdbgImpuestos.Columns.Count - 1
                sdbgImpuestos.Columns(j).CellStyleSet "NoHeredado"
            Next j
        Else
            For j = 0 To sdbgImpuestos.Columns.Count - 1
                sdbgImpuestos.Columns(j).CellStyleSet "Heredado"
            Next j
        End If
    Else
        For j = 0 To sdbgImpuestos.Columns.Count - 1
            sdbgImpuestos.Columns(j).CellStyleSet "NoVigente"
        Next j
    End If
End Sub

''' <summary>
''' Tras dar de alta un nuevo impuesto, cargo los datos proporcionadoso en la grid
''' </summary>
''' <param name="Id">ID del nuevo impuesto</param>
''' <param name="Tipo">Codigo del nuevo impuesto</param>
''' <param name="Descripcion">Descripcion del nuevo impuesto</param>
''' <param name="Retenido">Si es Retenido el nuevo impuesto</param>
''' <remarks>Llamada desde: frmNuevoImpuesto/cmdAceptar_Click ; Tiempo m�ximo: 0</remarks>
Public Sub CargaIdImpuesto(ByVal Id As Variant, ByVal Tipo As Variant, ByVal Descripcion As Variant, ByVal Retenido As Variant, ByVal Concepto As Variant, ByVal GrupoCompatibilidad As Long)
    Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos ' Nothing
    
    sdbgImpuestos.Columns("IMPUESTO_ID").Value = Id
        
    sdbgImpuestos.Columns("IMPUESTO").Value = Tipo
    
    sdbgImpuestos.Columns("DESCR").Value = Descripcion
    
    sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "1"
    
    sdbgImpuestos.Columns("RETENIDO").Value = Retenido
    
    sdbgImpuestos.Columns("CONCEPTO").Value = Concepto
    
    sdbgImpuestos.Columns("GRUPO_COMP").Value = GrupoCompatibilidad
    
    m_oImpuestosExistentes.Add Id, Tipo, Descripcion, Id, , , , , , , , , , , , , , , , , , , , Concepto, GrupoCompatibilidad
    
    m_oComboImpuestos.CargarTodosLosImpuestos gParametrosInstalacion.gIdioma
    
    Me.sdbgImpuestos.Columns("VALOR").Locked = False
End Sub


