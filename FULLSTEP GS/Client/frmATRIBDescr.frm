VERSION 5.00
Begin VB.Form frmATRIBDescr 
   BackColor       =   &H00808000&
   Caption         =   "Descripci�n del atributo"
   ClientHeight    =   3420
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8835
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmATRIBDescr.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3420
   ScaleWidth      =   8835
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtDescr 
      Height          =   2835
      Left            =   75
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   45
      Width           =   8640
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   315
      Left            =   3300
      TabIndex        =   1
      Top             =   3000
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   4500
      TabIndex        =   2
      Top             =   3000
      Width           =   1005
   End
End
Attribute VB_Name = "frmATRIBDescr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_bEdicion As Boolean
Public g_sOrigen As String
Private sIdiCerrar As String
Private sIdiAceptar As String
'Almacenamos valor para poderlo leer desde el origen
Private m_vValor As String
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean

Private m_sMsgError As String
''' <summary>
''' Boton Aceptar_click de la descripcion del catributo
''' </summary>
''' <remarks>Llamada desde: </remarks>
Private Sub cmdAceptar_Click()
Dim oCtlCostes As Object
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bEdicion Then
        Select Case g_sOrigen
            Case "MDIfrmATRIB"
                frmAtrib.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmAtrib.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmAtrib.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If
            Case "frmCatalogoCostesDescuentos_Costes"
                'Pesta�a costes
                frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo.Columns("DESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo.Columns("DESCR_BTN").Text = ""
                Else
                    frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo.Columns("DESCR_BTN").Text = "..."
                End If
            Case "frmCatalogoCostesDescuentos_Descuentos"
                'Pesta�a descuentos
                frmCatalogoCostesDescuentos.sdbgDescuentosLineaCatalogo.Columns("DESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmCatalogoCostesDescuentos.sdbgDescuentosLineaCatalogo.Columns("DESCR_BTN").Text = ""
                Else
                    frmCatalogoCostesDescuentos.sdbgDescuentosLineaCatalogo.Columns("DESCR_BTN").Text = "..."
                End If
            Case "MDIfrmATRIB2"
                    frmAtrib.sdbgAtributosGrupo.Columns("VDEFECTO").Value = Me.txtDescr
            Case "ATRIB_ESP"
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("Valor").Value = Me.txtDescr
            Case "ITEM_ATRIBESP"
                frmPROCE.sdbgItems.Columns(frmPROCE.sdbgItems.col).Value = Me.txtDescr
            Case "PLANTILLASfrmATRIB", "PLANTILLASESPfrmATRIB"
                frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If

            Case "frmPROCEfrmATRIB"
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If
            
            Case "frmFormulariosfrmATRIB"
                frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If
                
            Case "frmDesglosefrmATRIB"
                frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If
                
            Case "frmPARTipoSolicitfrmATRIB"
                frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If
                
            Case "frmPARTipoSolicitDesglosefrmATRIB"
                frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If
            
            Case "frmPEDIDOSLPfrmATRIB"
                frmPEDIDOS.g_ofrmATRIB.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmPEDIDOS.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmPEDIDOS.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If
                
            Case "ITEM_ATRIBESPfrmATRIB"
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If
            
            Case "ATRIB_ESP_GRUPOfrmATRIB"
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If
                
            Case "ATRIB_ESPfrmATRIB"
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If
                
            Case "PLANT_ATRIB_ESP_DESC"
                frmPlantillasProce.sdbgAtributosEsp.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmPlantillasProce.sdbgAtributosEsp.Columns("DESCR").Text = ""
                Else
                    frmPlantillasProce.sdbgAtributosEsp.Columns("DESCR").Text = "..."
                End If
            
            Case "PLANT_ATRIB_ESP_VAL"
                frmPlantillasProce.sdbgAtributosEsp.Columns("VALOR").Value = Me.txtDescr
                        
            Case "ESTRMAT_ATRIB"
                If frmESTRMATAtrib.sdbgAtributos.col <> -1 Then
                    Select Case frmESTRMATAtrib.sdbgAtributos.Columns(frmESTRMATAtrib.sdbgAtributos.col).Name
                        Case "DESCR"
                            frmESTRMATAtrib.sdbgAtributos.Columns("HIDENDESCR").Value = Me.txtDescr
                            If Me.txtDescr.Text = "" Then
                                frmESTRMATAtrib.sdbgAtributos.Columns("DESCR").Text = ""
                            Else
                                frmESTRMATAtrib.sdbgAtributos.Columns("DESCR").Text = "..."
                            End If
                        Case "VALOR"
                            frmESTRMATAtrib.sdbgAtributos.Columns("VALOR").Value = Me.txtDescr
                    End Select
                End If
            Case "ESTRMAT_ATRIB_UON"
                If frmESTRMATAtrib.AtributoUonSeleccionado.Tipo = TiposDeAtributos.TipoNumerico Then
                    frmESTRMATAtrib.setCurrentCellValue (Me.getTexto)
                End If
                frmESTRMATAtrib.setCurrentCellValue txtDescr.Text
            Case "frmCostesDescuentos"
                If frmCostesDescuentos.ctlCostesDescuentos.oSstabCD.Tab = 0 Then
                    frmCostesDescuentos.ctlCostesDescuentos.oSdbgCostes.Columns("DESCINT").Value = Me.txtDescr
                Else
                    frmCostesDescuentos.ctlCostesDescuentos.oSdbgDescuentos.Columns("DESCINT").Value = Me.txtDescr
                End If
                    
                If Me.txtDescr.Text = "" Then
                    If frmCostesDescuentos.ctlCostesDescuentos.oSstabCD.Tab = 0 Then
                        frmCostesDescuentos.ctlCostesDescuentos.oSdbgCostes.Columns("DESC").Text = ""
                    Else
                        frmCostesDescuentos.ctlCostesDescuentos.oSdbgDescuentos.Columns("DESC").Text = ""
                    End If
                Else
                    If frmCostesDescuentos.ctlCostesDescuentos.oSstabCD.Tab = 0 Then
                        frmCostesDescuentos.ctlCostesDescuentos.oSdbgCostes.Columns("DESC").Text = "..."
                    Else
                        frmCostesDescuentos.ctlCostesDescuentos.oSdbgDescuentos.Columns("DESC").Text = ""
                    End If
                End If
            Case "frmPEDIDOS"
                If frmPEDIDOS.ctlCostesDescuentos.oSstabCD.Tab = 0 Then
                    frmPEDIDOS.ctlCostesDescuentos.oSdbgCostes.Columns("DESCINT").Value = Me.txtDescr
                Else
                    frmPEDIDOS.ctlCostesDescuentos.oSdbgDescuentos.Columns("DESCINT").Value = Me.txtDescr
                End If
                    
                If Me.txtDescr.Text = "" Then
                    If frmPEDIDOS.ctlCostesDescuentos.oSstabCD.Tab = 0 Then
                        frmPEDIDOS.ctlCostesDescuentos.oSdbgCostes.Columns("DESC").Text = ""
                    Else
                        frmPEDIDOS.ctlCostesDescuentos.oSdbgDescuentos.Columns("DESC").Text = ""
                    End If
                Else
                    If frmPEDIDOS.ctlCostesDescuentos.oSstabCD.Tab = 0 Then
                        frmPEDIDOS.ctlCostesDescuentos.oSdbgCostes.Columns("DESC").Text = "..."
                    Else
                        frmPEDIDOS.ctlCostesDescuentos.oSdbgDescuentos.Columns("DESC").Text = ""
                    End If
                End If
            Case "frmSeguimiento", "frmSeguimientoLinea"
                Select Case g_sOrigen
                    Case "frmSeguimiento"
                        Set oCtlCostes = frmSeguimiento.ctlCostesDescuentos
                    Case "frmSeguimientoLinea"
                        Set oCtlCostes = frmSeguimiento.ctlCostesDescuentosLinea
                End Select
                If oCtlCostes.oSstabCD.Tab = 0 Then
                    oCtlCostes.oSdbgCostes.Columns("DESCINT").Value = Me.txtDescr
                Else
                    oCtlCostes.oSdbgDescuentos.Columns("DESCINT").Value = Me.txtDescr
                End If
                    
                If Me.txtDescr.Text = "" Then
                    If oCtlCostes.oSstabCD.Tab = 0 Then
                        oCtlCostes.oSdbgCostes.Columns("DESC").Text = ""
                    Else
                        oCtlCostes.oSdbgDescuentos.Columns("DESC").Text = ""
                    End If
                Else
                    If oCtlCostes.oSstabCD.Tab = 0 Then
                        oCtlCostes.oSdbgCostes.Columns("DESC").Text = "..."
                    Else
                        oCtlCostes.oSdbgDescuentos.Columns("DESC").Text = ""
                    End If
                End If
                Set oCtlCostes = Nothing
            Case "frmCatalogo"
                frmCatalogo.sdbgCamposPedido.Columns("VAL").Value = Me.txtDescr
            Case "frmCatalogo_Recep"
                frmCatalogo.sdbgCamposRecepcion.Columns("VAL").Value = Me.txtDescr
            Case "frmCatalogoAtrib"
                frmCatalogoAtrib.sdbgCamposEsp(3).Columns("VAL").Value = Me.txtDescr
            Case "frmCatalogoAtribEspYAdj"
                frmCatalogoAtribEspYAdj.sdbgCamposEsp.Columns("VAL").Value = Me.txtDescr
            Case "frmOFERec"
                frmOFERec.sdbgAtributos.Columns("VALor").Value = Me.txtDescr.Text
            Case "frmOFERec_Item"
                frmOFERec.sdbgPrecios.Columns(frmOFERec.sdbgPrecios.col).Value = Me.txtDescr.Text
            Case "frmPEDIDOSCabecera"
                frmPEDIDOS.PonerAtributo 1, Me.txtDescr.Text
            Case "frmPEDIDOSLinea"
                frmPEDIDOS.PonerAtributo 2, Me.txtDescr.Text
            Case "frmSeguimientoATCabecera"
                frmSeguimiento.sdbgAtributos.Columns(frmSeguimiento.sdbgAtributos.col).Value = Me.txtDescr.Text
            Case "frmSeguimientoATLinea"
                frmSeguimiento.sdbgAtributosLinea.Columns(frmSeguimiento.sdbgAtributosLinea.col).Value = Me.txtDescr.Text
            Case "frmCONFINST"
                frmCONFINST.sdbgCamposPedido.Columns(frmCONFINST.sdbgCamposPedido.col).Value = Me.txtDescr.Text
            Case Else
                frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("HIDENDESCR").Value = Me.txtDescr
                If Me.txtDescr.Text = "" Then
                    frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = ""
                Else
                    frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("DESCR").Text = "..."
                End If
        End Select
        m_vValor = Me.txtDescr
    End If
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmATRIBDescr", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
 
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmATRIBDescr", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
txtDescr.SelStart = Len(txtDescr.Text)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmATRIBDescr", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Load()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.Height = 4820
    Me.Width = 7480
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarRecursos
    txtDescr.Text = ""
    If Me.g_bEdicion Then
        txtDescr.Locked = False
        Me.cmdAceptar.caption = sIdiAceptar
        Me.cmdCancelar.Visible = True
        cmdCancelar.Enabled = True
    Else
        txtDescr.Locked = True
        Me.cmdAceptar.caption = sIdiCerrar
        Me.cmdCancelar.Visible = False
        cmdCancelar.Enabled = False
    End If
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmATRIBDescr", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ATRIBDESCR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        sIdiCerrar = Ador(0).Value
        Ador.MoveNext
        sIdiAceptar = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        
        Select Case g_sOrigen
            Case "MDIfrmATRIB2"
                Me.caption = Ador(0).Value
        End Select
        
               
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
End Sub

Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height > 2900 And Me.Width > 500 Then
        txtDescr.Height = Me.Height - 945
        txtDescr.Width = Me.Width - 320
        cmdAceptar.Top = txtDescr.Height + 150
        cmdCancelar.Top = cmdAceptar.Top
        If Me.g_bEdicion Then
            cmdAceptar.Left = txtDescr.Width / 2 - cmdAceptar.Width - 50
        Else
            cmdAceptar.Left = txtDescr.Width / 2 - cmdAceptar.Width / 2
        End If
        cmdCancelar.Left = txtDescr.Width / 2 + 50
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub
'''Obtiene el valor introducido en el formulario
Public Function getTexto() As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    getTexto = m_vValor
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmATRIBDescr", "getTexto", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Sub setTexto(ByVal sTexto As String)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_vValor = sTexto
    Me.txtDescr.Text = sTexto
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmATRIBDescr", "setTexto", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
        Exit Sub

   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmATRIBDescr", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


