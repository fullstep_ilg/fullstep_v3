VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstLISPERUsu 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listados por usuarios"
   ClientHeight    =   2640
   ClientLeft      =   1905
   ClientTop       =   795
   ClientWidth     =   6090
   Icon            =   "frmLstLISPERUsu.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2640
   ScaleWidth      =   6090
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   6090
      TabIndex        =   0
      Top             =   2265
      Width           =   6090
      Begin VB.CommandButton cmdObtener 
         Caption         =   "&Obtener+"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4710
         TabIndex        =   1
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2205
      Left            =   15
      TabIndex        =   2
      Top             =   0
      Width           =   6045
      _ExtentX        =   10663
      _ExtentY        =   3889
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n+"
      TabPicture(0)   =   "frmLstLISPERUsu.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame3"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Timer1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Opciones+"
      TabPicture(1)   =   "frmLstLISPERUsu.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).ControlCount=   1
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4080
         Top             =   150
      End
      Begin VB.Frame Frame3 
         Height          =   1785
         Left            =   150
         TabIndex        =   6
         Top             =   315
         Width           =   5730
         Begin VB.OptionButton opTodos 
            Caption         =   "Todos los usuarios+"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   300
            TabIndex        =   9
            Top             =   375
            Value           =   -1  'True
            Width           =   2250
         End
         Begin VB.OptionButton opTipoUsu 
            Caption         =   "Usuarios de tipo:+"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   300
            TabIndex        =   8
            Top             =   850
            Width           =   1905
         End
         Begin VB.OptionButton opUsuario 
            Caption         =   "Usuario:+"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   300
            TabIndex        =   7
            Top             =   1335
            Width           =   1860
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcUsuCod 
            Height          =   285
            Left            =   2205
            TabIndex        =   10
            Top             =   1305
            Width           =   2925
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   2434
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7726
            Columns(1).Caption=   "Denominacion"
            Columns(1).Name =   "Denominacion"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "Tipo"
            Columns(2).Name =   "Tipo"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   5159
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Orden+"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1425
         Left            =   -74850
         TabIndex        =   3
         Top             =   500
         Width           =   5730
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n+"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2910
            TabIndex        =   5
            Top             =   630
            Width           =   2040
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo+"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   705
            TabIndex        =   4
            Top             =   630
            Value           =   -1  'True
            Width           =   1575
         End
      End
   End
End
Attribute VB_Name = "frmLstLISPERUsu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Si true-> listados por usuarios.Sino Usuarios por listados
Public bListUsu As Boolean

Private RespetarCombo As Boolean
Private CargarComboDesde As Boolean

'MULTILENGUAJE
Private sEspera(3) As String         'textos para formulario de espera
Private sTitulo As String
Private sTipos(2) As String   ' cargar combo

Private FormulaRpt(1 To 2, 1 To 8) As String      'formulas textos RPT
Private sListadoSel(4) As String

Private sDe As String
Private sPag As String
Private sListados As String
Private sSel As String
Private sUsuario As String
Private sPersonalizado As String

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LST_LISPER_USU, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        If bListUsu Then
            Me.caption = Ador(0).Value
            Ador.MoveNext
        Else
            Ador.MoveNext
            Me.caption = Ador(0).Value
        End If
        Ador.MoveNext
        
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        Frame1.caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        opTipoUsu.caption = Ador(0).Value
        Ador.MoveNext
        opTodos.caption = Ador(0).Value
        Ador.MoveNext
        opUsuario.caption = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        
        If bListUsu Then
            sTitulo = Ador(0).Value
            Ador.MoveNext
        Else
            Ador.MoveNext
            sTitulo = Ador(0).Value
        End If
        Ador.MoveNext
        
        FormulaRpt(1, 1) = sTitulo
        
        sEspera(1) = Ador(0).Value
        Ador.MoveNext
        sEspera(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(3) = Ador(0).Value
        
        Ador.MoveNext
        
        Ador.MoveNext
        sdbcUsuCod.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcUsuCod.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        
        sPag = Ador(0).Value
        Ador.MoveNext
        sDe = Ador(0).Value
        Ador.MoveNext
        sSel = Ador(0).Value
        Ador.MoveNext
        sUsuario = Ador(0).Value
        Ador.MoveNext
        sListados = Ador(0).Value
        Ador.MoveNext
        
        sListadoSel(1) = Ador(0).Value
        Ador.MoveNext
        sListadoSel(2) = Ador(0).Value
        Ador.MoveNext
        sListadoSel(3) = Ador(0).Value
        Ador.MoveNext
        sListadoSel(4) = Ador(0).Value
        
        Ador.MoveNext
        sPersonalizado = Ador(0).Value
        Ador.MoveNext
        opTipoUsu.caption = Ador(0).Value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub


Private Sub Timer1_Timer()

   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub

Private Sub sdbcUsuCod_Change()

    If Not RespetarCombo Then
        CargarComboDesde = True
    End If
    
End Sub
Private Sub sdbcUsucod_CloseUp()

    Dim i As Integer
    
    If sdbcUsuCod.Value = "..." Then
        sdbcUsuCod.Text = ""
        Exit Sub
    End If
    
    If sdbcUsuCod.Text = "" Then
        CargarComboDesde = False
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcUsuCod.Text = sdbcUsuCod.Columns(0).Text
    RespetarCombo = False
    
    opUsuario.Value = True
    CargarComboDesde = False
    
End Sub

Private Sub sdbcUsuCod_DropDown()
    Dim Codigos() As TipoDatosUsu
    Dim i As Integer
        
    sdbcUsuCod.RemoveAll
    
    'Cargamos los codigos
    Screen.MousePointer = vbHourglass
    Codigos = oGestorSeguridad.BuscarTodosLosUsuarios
    ' se cargan todos los tipos
    For i = 0 To UBound(Codigos)
        sdbcUsuCod.AddItem Codigos(i).USU & Chr(m_lSeparador) & Codigos(i).Nom & Chr(m_lSeparador) & Codigos(i).Tipo
    Next
    
    opUsuario.Value = True
    sdbcUsuCod.SelStart = 0
    sdbcUsuCod.SelLength = Len(sdbcUsuCod.Text)
    sdbcUsuCod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcusucod_InitColumnProps()

    sdbcUsuCod.DataFieldList = "Column 0"
    sdbcUsuCod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcusucod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcUsuCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcUsuCod.Rows - 1
            bm = sdbcUsuCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcUsuCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcUsuCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Public Sub sdbcusucod_Validate(Cancel As Boolean)

    Dim oUsuarios As CUsuarios
    Dim bExiste As Boolean
    
    Set oUsuarios = oFSGSRaiz.Generar_CUsuarios
    
    If sdbcUsuCod.Text = "" Then
        CargarComboDesde = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el usuario
    
    Screen.MousePointer = vbHourglass
    Set oUsuarios = oGestorSeguridad.DevolverTodosLosUsuarios(sdbcUsuCod.Text, , True)
    
    bExiste = Not (oUsuarios.Count = 0)
    
    If Not bExiste Then
        sdbcUsuCod.Text = ""
    Else
        RespetarCombo = True
        sdbcUsuCod.Text = oUsuarios.Item(sdbcUsuCod).Cod
        RespetarCombo = False
        CargarComboDesde = False
    End If
    
    Set oUsuarios = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub Form_Load()

    Me.Height = 3045
    Me.Width = 6210
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    opTodos.Value = True
    
End Sub

Private Sub cmdObtener_Click()
   Dim oReport As Object
    Dim SubListado As CRAXDRT.Report
    Dim oCRSeguridad As CRSeguridad
    Dim pv As Preview
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim i As Integer
    Dim iTipo As Integer   ' tipo de usuario para listado de USUARIOS, para pasar a clase
    Dim sSeleccion As String
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Set oCRSeguridad = GenerarCRSeguridad
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    If bListUsu Then
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptLisperLisUsu.rpt"
    Else
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptLisperUsuLis.rpt"
    End If
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTITULO")).Text = """" & sTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & sPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & sDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSEL")).Text = """" & sSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLISTADOS")).Text = """" & sListados & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUSUARIOS")).Text = """" & sUsuario & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPersonalizado")).Text = """" & sPersonalizado & """"
    
    iTipo = 0
    If opTipoUsu.Value = True Then
        sSeleccion = opTipoUsu.caption
        iTipo = 2
    ElseIf sdbcUsuCod <> "" Then
        sSeleccion = sListadoSel(2) & " " & sdbcUsuCod
    Else
        sSeleccion = ""
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSEL")).Text = ""
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sSeleccion & """"
    
    oCRSeguridad.ListadoLISPERUsu oReport, sdbcUsuCod.Text, iTipo, opOrdDen.Value, bListUsu
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo   'Listado de Usuarios
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
        
    frmESPERA.lblGeneral.caption = sEspera(1) & " " & pv.caption      'Generando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)  'Seleccionando registros ...
    frmESPERA.lblDetalle = sEspera(3)  'Visualizando listado ...
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

End Sub


