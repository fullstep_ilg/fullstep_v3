VERSION 5.00
Begin VB.Form frmPlanesEntrega 
   Caption         =   "DRecepciones automaticas"
   ClientHeight    =   4665
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   10380
   Icon            =   "frmPlanesEntrega.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4665
   ScaleWidth      =   10380
   StartUpPosition =   2  'CenterScreen
   Begin FSGSClient.ctlPlanesEntrega ctlPlanesEntrega 
      Height          =   4605
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   10365
      _ExtentX        =   18283
      _ExtentY        =   8123
   End
End
Attribute VB_Name = "frmPlanesEntrega"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public m_bDescargarFrm As Boolean
Dim m_bActivado As Boolean
Private m_sMsgError As String
Sub CargarRecursos()
Dim Ador As Ador.Recordset


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PlanesEntrega, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
    ctlPlanesEntrega.Initialize
End If
Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlanesEntrega", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
CargarRecursos
 
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlanesEntrega", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlanesEntrega", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

