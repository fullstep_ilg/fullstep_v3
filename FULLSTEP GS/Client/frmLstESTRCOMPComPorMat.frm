VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstESTRCOMPComPorMat 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DListado de Compradores por material (Opciones)"
   ClientHeight    =   2730
   ClientLeft      =   105
   ClientTop       =   2430
   ClientWidth     =   6480
   Icon            =   "frmLstESTRCOMPComPorMat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2730
   ScaleWidth      =   6480
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   6480
      TabIndex        =   7
      Top             =   2355
      Width           =   6480
      Begin VB.CommandButton cmdObtener 
         Caption         =   "D&Obtener"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5115
         TabIndex        =   6
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2310
      Left            =   0
      TabIndex        =   8
      Top             =   15
      Width           =   6450
      _ExtentX        =   11377
      _ExtentY        =   4075
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DSelecci�n"
      TabPicture(0)   =   "frmLstESTRCOMPComPorMat.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame4"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Timer1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdBorrar"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "DOrden"
      TabPicture(1)   =   "frmLstESTRCOMPComPorMat.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.CommandButton cmdBorrar 
         Height          =   315
         Left            =   5450
         Picture         =   "frmLstESTRCOMPComPorMat.frx":0CEA
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   1065
         Width           =   315
      End
      Begin VB.Timer Timer1 
         Interval        =   2000
         Left            =   5625
         Top             =   120
      End
      Begin VB.Frame Frame1 
         Height          =   1755
         Left            =   -74865
         TabIndex        =   12
         Top             =   375
         Width           =   6195
         Begin VB.OptionButton opOrdCod 
            Caption         =   "DC�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   300
            TabIndex        =   4
            Top             =   420
            Value           =   -1  'True
            Width           =   4650
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "DDenominaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   300
            TabIndex        =   5
            Top             =   1035
            Width           =   5190
         End
      End
      Begin VB.Frame Frame4 
         Height          =   660
         Left            =   135
         TabIndex        =   11
         Top             =   1560
         Width           =   6195
         Begin VB.CheckBox chkDetalles 
            Caption         =   "DIncluir detalles"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3150
            TabIndex        =   3
            Top             =   270
            Width           =   2850
         End
         Begin VB.CheckBox chkComprador 
            Caption         =   "DMostrar compradores"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   300
            TabIndex        =   2
            Top             =   270
            Width           =   2565
         End
      End
      Begin VB.Frame Frame2 
         Height          =   1185
         Left            =   135
         TabIndex        =   9
         Top             =   390
         Width           =   6195
         Begin VB.TextBox txtEstMat 
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1590
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   690
            Width           =   3660
         End
         Begin VB.OptionButton opSelMat 
            Caption         =   "DSolo material:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   45
            TabIndex        =   1
            Top             =   720
            Width           =   1605
         End
         Begin VB.CommandButton cmdSelMat 
            Enabled         =   0   'False
            Height          =   315
            Left            =   5685
            Picture         =   "frmLstESTRCOMPComPorMat.frx":0D8F
            Style           =   1  'Graphical
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   675
            Width           =   345
         End
         Begin VB.OptionButton opTodos 
            Caption         =   "DListado Completo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   45
            TabIndex        =   0
            Top             =   315
            Value           =   -1  'True
            Width           =   5640
         End
      End
   End
End
Attribute VB_Name = "frmLstESTRCOMPComPorMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables de restricciones
Public bRMat As Boolean
Public bREqp As Boolean
Public sGMN1Cod As String
Public sGMN2Cod As String
Public sGMN3Cod As String
Public sGMN4Cod As String
Public g_oADORes As Ador.Recordset
Public g_oADORes1 As Ador.Recordset
Public g_oADORes2 As Ador.Recordset
Public g_oADORes3 As Ador.Recordset
Public g_oADORes4 As Ador.Recordset

'Variables de idioma
Private sEspera(1 To 3) As String
Private sIdiSel As String
Private txtTitulo As String
Private txtPag As String
Private txtDe As String
Private txtSeleccion As String
Private txtEqdeCom As String
Private txtEqpyCom As String
Private txtEmail As String
Private txtFax As String
Private txtTfno As String

Private Sub chkComprador_Click()
 If chkComprador Then
    chkDetalles.Enabled = True
 Else
    chkDetalles.Enabled = False
 End If
End Sub


Private Sub cmdBorrar_Click()
    txtEstMat.Text = ""
End Sub

Private Sub cmdSelMat_Click()
    frmSELMAT.sOrigen = "frmLstESTRCOMPComPorMat"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Show 1

End Sub
Public Sub PonerMatSeleccionado()
        
    If Not frmSELMAT.oGMN1Seleccionado Is Nothing Then
         sGMN1Cod = frmSELMAT.oGMN1Seleccionado.Cod
         txtEstMat = sGMN1Cod
    Else
         sGMN1Cod = ""
    End If
        
    If Not frmSELMAT.oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = frmSELMAT.oGMN2Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    Else
        sGMN2Cod = ""
    End If
        
    If Not frmSELMAT.oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = frmSELMAT.oGMN3Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    Else
        sGMN3Cod = ""
    End If
        
    If Not frmSELMAT.oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = frmSELMAT.oGMN4Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    Else
        sGMN4Cod = ""
    End If

End Sub

Private Sub cmdObtener_Click()

    ObtenerListado
End Sub

Private Sub ObtenerListado()
    Dim SelectionText As String
    Dim i As Integer
    Dim Parametros(1 To 3) As String
    Dim oReport As CRAXDRT.Report
    Dim Srpt As CRAXDRT.Report
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim pv As Preview
    Dim sEqp As String
    Dim sCom As String

    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If txtEstMat = "" Then
        opTodos.Value = True
        opSelMat.Value = False
    Else
        opSelMat.Value = True
        opTodos.Value = False
    End If
    
    If Not opTodos Then
        SelectionText = sIdiSel & " " & txtEstMat
    Else
        SelectionText = ""
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptCOMPorMat.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
        
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN1NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN1 & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN2NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN2 & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN3NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN3 & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN4NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN4 & ":" & """"
    'Pasamos los textos del idioma
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & txtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & txtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & txtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & txtSeleccion & """"
    
    
    For i = 1 To 4
        Set Srpt = Nothing
        Set Srpt = oReport.OpenSubreport("COM_NIVEL" & i)
        
        If chkComprador Then
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "Compradores")).Text = "'S'"
            If chkDetalles Then
                Srpt.FormulaFields(crs_FormulaIndex(Srpt, "Detalles")).Text = "'S'"
            Else
                Srpt.FormulaFields(crs_FormulaIndex(Srpt, "Detalles")).Text = "'N'"
            End If
        Else
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "Compradores")).Text = "'N'"
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "Detalles")).Text = "'N'"
        End If
        'Pasamos los textos del idioma
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtEqdeCom")).Text = """" & txtEqdeCom & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtEqpyCom")).Text = """" & txtEqpyCom & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtEmail")).Text = """" & txtEmail & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtFax")).Text = """" & txtFax & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtTfno")).Text = """" & txtTfno & """"
    Next i

        
    If bRMat Then
        sEqp = basOptimizacion.gCodEqpUsuario
        sCom = basOptimizacion.gCodCompradorUsuario
    End If

    Set g_oADORes = oGestorInformes.ListadoMATERIALES(sEqp, sCom, opOrdDen, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod)
    
    If g_oADORes Is Nothing Then
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    ElseIf g_oADORes.EOF Then
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        oReport.Database.SetDataSource g_oADORes
    End If
            
    sEqp = ""
    If bREqp Then
        sEqp = basOptimizacion.gCodEqpUsuario
    End If
        
    Set Srpt = Nothing
    Set Srpt = oReport.OpenSubreport("COM_NIVEL" & 1)
    
    Set g_oADORes1 = oGestorInformes.ListadoCompPorMatSubrpt(1, opOrdDen, sEqp)
    Srpt.Database.SetDataSource g_oADORes1
    
    Set Srpt = Nothing
    Set Srpt = oReport.OpenSubreport("COM_NIVEL" & 2)
    
    Set g_oADORes2 = oGestorInformes.ListadoCompPorMatSubrpt(2, opOrdDen, sEqp)
    Srpt.Database.SetDataSource g_oADORes2
          
    Set Srpt = Nothing
    Set Srpt = oReport.OpenSubreport("COM_NIVEL" & 3)
    
    Set g_oADORes3 = oGestorInformes.ListadoCompPorMatSubrpt(3, opOrdDen, sEqp)
    Srpt.Database.SetDataSource g_oADORes3
          
    Set Srpt = Nothing
    Set Srpt = oReport.OpenSubreport("COM_NIVEL" & 4)
    
    Set g_oADORes4 = oGestorInformes.ListadoCompPorMatSubrpt(4, opOrdDen, sEqp)
    Srpt.Database.SetDataSource g_oADORes4
          
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sEspera(1)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = txtTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.g_sOrigen = "frmLstESTRCOMPComPorMat"
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Screen.MousePointer = vbNormal
    Unload frmESPERA
    Unload Me
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTESTRCOMP_COMPORMAT, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    
        SSTab1.TabCaption(0) = Ador(0).Value      '1
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value     '2
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        chkComprador.caption = Ador(0).Value
        Ador.MoveNext
        chkDetalles.caption = Ador(0).Value '5
        Ador.MoveNext
        frmLstESTRCOMPComPorMat.caption = Ador(0).Value
        Ador.MoveNext
        opSelMat.caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value '8 c�digo
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value '9 Denominaci�n
        Ador.MoveNext
        opTodos.caption = Ador(0).Value
        Ador.MoveNext
        sEspera(1) = Ador(0).Value
        Ador.MoveNext
        sEspera(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(3) = Ador(0).Value
        
        'Idiomas del RPT
        Ador.MoveNext
        sIdiSel = Ador(0).Value '200
        Ador.MoveNext
        txtTitulo = Ador(0).Value
        Ador.MoveNext
        txtPag = Ador(0).Value
        Ador.MoveNext
        txtDe = Ador(0).Value
        Ador.MoveNext
        txtSeleccion = Ador(0).Value
        Ador.MoveNext
        txtEqdeCom = Ador(0).Value '205
        Ador.MoveNext
        txtEqpyCom = Ador(0).Value
        Ador.MoveNext
        txtEmail = Ador(0).Value
        Ador.MoveNext
        txtFax = Ador(0).Value
        Ador.MoveNext
        txtTfno = Ador(0).Value
    
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

Private Sub Form_Load()

    Me.Width = 6600
    Me.Height = 3135
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    opTodos = True
    Timer1.Enabled = False
    txtEstMat.Enabled = False
    cmdSelMat.Enabled = False
    cmdBorrar.Enabled = False
    ' Configurar la seguridad
    ConfigurarSeguridad
 
 
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    bRMat = False
    bREqp = False
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPPorGRUPRestMatComp)) Is Nothing) Then
        bRMat = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPPorGRUPRestEquipo)) Is Nothing) Then
        bREqp = True
    End If
End Sub



Private Sub Form_Unload(Cancel As Integer)
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""

End Sub

Private Sub opSelMat_Click()
    If opSelMat.Value = True Then
        cmdSelMat.Enabled = True
        cmdBorrar.Enabled = True
    Else
        cmdSelMat.Enabled = False
        cmdBorrar.Enabled = False
    End If

End Sub

Private Sub opTodos_Click()
    If opTodos Then
        txtEstMat.Text = ""
        txtEstMat.Refresh
        sGMN1Cod = ""
        sGMN2Cod = ""
        sGMN3Cod = ""
        sGMN4Cod = ""
        cmdSelMat.Enabled = False
        cmdBorrar.Enabled = False
    Else
        cmdSelMat.Enabled = True
        cmdBorrar.Enabled = True
    End If
End Sub

Private Sub Timer1_Timer()
   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If
End Sub

Private Sub txtestmat_Change()
    If txtEstMat.Text <> "" Then
        opSelMat.Value = True
    End If
End Sub


