VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmUSUARIOSSelGestores 
   BackColor       =   &H00808000&
   Caption         =   "frmUSUARIOSSelGestores"
   ClientHeight    =   8730
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8655
   Icon            =   "frmUSUARIOSSelGestores.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8730
   ScaleMode       =   0  'User
   ScaleWidth      =   8878.195
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer 
      Enabled         =   0   'False
      Interval        =   5
      Left            =   1680
      Top             =   8280
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "CModificar"
      Height          =   315
      Left            =   120
      TabIndex        =   10
      Top             =   8370
      Width           =   1005
   End
   Begin VB.Frame fraSelCContables 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7695
      Left            =   120
      TabIndex        =   14
      Top             =   600
      Width           =   8415
      Begin VB.CommandButton cmdCargar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8010
         Picture         =   "frmUSUARIOSSelGestores.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Cargar"
         Top             =   420
         Width           =   336
      End
      Begin VB.TextBox txtFecHasta 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4740
         TabIndex        =   5
         Top             =   420
         Width           =   1170
      End
      Begin VB.CommandButton cmdCalHasta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5970
         Picture         =   "frmUSUARIOSSelGestores.frx":01D6
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Mantenimiento"
         Top             =   420
         Width           =   336
      End
      Begin VB.TextBox txtFecDesde 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1860
         TabIndex        =   3
         Top             =   420
         Width           =   1170
      End
      Begin VB.CommandButton cmdCalDesde 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3090
         Picture         =   "frmUSUARIOSSelGestores.frx":0760
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   420
         Width           =   336
      End
      Begin VB.CheckBox chkNoVigentes 
         BackColor       =   &H00808000&
         Caption         =   "Ver no vigentes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   6540
         TabIndex        =   7
         Top             =   420
         Value           =   1  'Checked
         Width           =   1800
      End
      Begin VB.TextBox txtSeleccion 
         Height          =   285
         Left            =   1860
         TabIndex        =   1
         Top             =   0
         Width           =   6075
      End
      Begin VB.CommandButton cmdSeleccion 
         Height          =   315
         Left            =   8010
         Picture         =   "frmUSUARIOSSelGestores.frx":0CEA
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   0
         Width           =   336
      End
      Begin MSComctlLib.TreeView tvwestrCC 
         Height          =   6765
         Left            =   0
         TabIndex        =   9
         Top             =   870
         Width           =   8385
         _ExtentX        =   14790
         _ExtentY        =   11933
         _Version        =   393217
         Style           =   7
         Checkboxes      =   -1  'True
         HotTracking     =   -1  'True
         ImageList       =   "ImageList2"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   1290
         Top             =   2100
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   7
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmUSUARIOSSelGestores.frx":0D3B
               Key             =   "PP"
               Object.Tag             =   "PP"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmUSUARIOSSelGestores.frx":118D
               Key             =   "CCC"
               Object.Tag             =   "CCC"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmUSUARIOSSelGestores.frx":17E6
               Key             =   "CC3"
               Object.Tag             =   "CC3"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmUSUARIOSSelGestores.frx":1878
               Key             =   "CC4"
               Object.Tag             =   "CC4"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmUSUARIOSSelGestores.frx":190B
               Key             =   "CC2"
               Object.Tag             =   "CC2"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmUSUARIOSSelGestores.frx":19BB
               Key             =   "CC1"
               Object.Tag             =   "CC1"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmUSUARIOSSelGestores.frx":1A6B
               Key             =   "RAIZ"
               Object.Tag             =   "RAIZ"
            EndProperty
         EndProperty
      End
      Begin VB.Label lblFecHasta 
         BackStyle       =   0  'Transparent
         Caption         =   "DFecha fin hasta :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   3480
         TabIndex        =   17
         Top             =   450
         Width           =   1575
      End
      Begin VB.Label lblFecDesde 
         BackStyle       =   0  'Transparent
         Caption         =   "DFecha inicio desde :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   16
         Top             =   450
         Width           =   1785
      End
      Begin VB.Label lblSeleccion 
         BackStyle       =   0  'Transparent
         Caption         =   "DSeleccion"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   15
         Top             =   30
         Width           =   1185
      End
   End
   Begin VB.ComboBox cboPartPres 
      Height          =   315
      ItemData        =   "frmUSUARIOSSelGestores.frx":1B09
      Left            =   1979
      List            =   "frmUSUARIOSSelGestores.frx":1B0B
      TabIndex        =   0
      Top             =   180
      Width           =   6454
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "CCancelar"
      Height          =   315
      Left            =   4470
      TabIndex        =   12
      Top             =   8370
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "CAceptar"
      Height          =   315
      Left            =   3420
      TabIndex        =   11
      Top             =   8370
      Width           =   1005
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   7920
      Top             =   8280
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   -2147483643
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOSSelGestores.frx":1B0D
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOSSelGestores.frx":1F5F
            Key             =   "PRES1"
            Object.Tag             =   "PRES1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOSSelGestores.frx":23B1
            Key             =   "PRES2"
            Object.Tag             =   "PRES2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOSSelGestores.frx":2803
            Key             =   "PRES3"
            Object.Tag             =   "PRES3"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOSSelGestores.frx":2C55
            Key             =   "PRES4"
            Object.Tag             =   "PRES4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOSSelGestores.frx":30A7
            Key             =   "PRESBAJALOGICA"
            Object.Tag             =   "PRESBAJALOGICA"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOSSelGestores.frx":3AB9
            Key             =   "PRESSEL"
            Object.Tag             =   "PRESSEL"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblPartida 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "DPartida Presupuestaria:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   13
      Top             =   180
      Width           =   1785
   End
End
Attribute VB_Name = "frmUSUARIOSSelGestores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public m_oPresupuestosNiv0 As cPresConceptos5Nivel0 'Contendra toda la estructura de presupuestos
Private mNode As node
Private m_bSoloLectura As Boolean
Private bOrdenadoPorDen As Boolean
Private m_lNodo As Long
Private m_oPartidas As cPresConceptos5Nivel0
Private oPres0 As cPresConcep5Nivel0
Private m_sPartida As String
Private oIBaseDatos As IBaseDatos
Private m_sEstructuraArbolPresupuestario As String '"C o M"    Modo del arbol de partida presupuestaria
Private m_SelNode As MSComctlLib.node
Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String
Private m_sUON4 As String
Private m_sPres0Inicial As String
Private arArboles() As String 'C�digo de los �rboles presentes en el combo
Private m_bCambiosEnNodosSel As Boolean 'Boolean que nos dir� si se han modificado los nodos seleciconados
Private m_sSeleccionePartida As String
Private m_sAvisoGuardarCambios As String
Private m_bYaAvisado As Boolean

'Variables publicas
Public m_oPresupuestos As CPresConceptos5Nivel1 'Contendra toda la estructura de presupuestos
Private m_vFecDesde As Variant
Private m_vFecHasta As Variant
Private m_bVerNoVigentes As Boolean

Private m_bPres0PluriAnual As Boolean

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Bot�n que muestra el calendario para seleccionar la fecha Desde.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdCalDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub


''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Bot�n que muestra el calendario para seleccionar la fecha Hasta.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdCalHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Funci�n que carga los recursos de la ventana, etiquetas...
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_USUARIOSSELCCONTABLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.caption = Ador(0).Value & " " & g_oParametrosSM.Item(m_sPres0Inicial).NomNivelImputacion '1 Asignar Gestor a las partidas del centro de coste:
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value '2 &Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '3 &Cancelar
        Ador.MoveNext
        Ador.MoveNext
        lblSeleccion.caption = Ador(0).Value '5 Selecci�n:
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value '6 Fecha inicio desde :
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value '7 Fecha fin hasta :
        Ador.MoveNext
        chkNoVigentes.caption = Ador(0).Value '8 Ver no vigentes
        Ador.MoveNext
        cmdSeleccion.ToolTipText = Ador(0).Value '9 Siguiente
        Ador.MoveNext
        lblPartida.caption = Ador(0).Value & ":" '10 Partida presupuestaria
        m_sPartida = Ador(0).Value & ":"
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value '11 Modificar
        Ador.MoveNext
        Ador.MoveNext
        m_sSeleccionePartida = Ador(0).Value '13 Por favor, seleccione una partida presupuestaria.
        Ador.MoveNext
        Ador.MoveNext
        m_sAvisoGuardarCambios = Ador(0).Value '15 Guarde o Cancele los cambios antes de continuar
        Ador.MoveNext
        
        Me.caption = Ador(0).Value 'Gestionar Partida Presupuestaria
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub


''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Bot�n que efectua una nueva carga del �rbol de partidas teniendo en cuenta los criterios de busqueda seleccionados.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdCargar_Click()
Dim sFechaActual As String

    Screen.MousePointer = vbHourglass
    
    sFechaActual = Format(Now, "DD\/MM\/YYYY")
    
    If txtFecDesde.Text <> "" And txtFecHasta.Text <> "" Then
        If CDate(txtFecDesde.Text) < CDate(sFechaActual) And CDate(txtFecHasta.Text) < CDate(sFechaActual) Then
            chkNoVigentes.Value = 1
        ElseIf CDate(txtFecDesde.Text) > CDate(sFechaActual) And CDate(txtFecHasta.Text) > CDate(sFechaActual) Then
            chkNoVigentes.Value = 1
        End If
    End If
    
    CargarFiltros
    Set m_oPresupuestos = oFSGSRaiz.Generar_CPresConceptos5Nivel1
    GenerarEstructuraPresupuestos m_sPres0Inicial, bOrdenadoPorDen
    Screen.MousePointer = vbNormal

End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Bot�n te va moviendo a trav�s del �rbol de partidas por las diferentes partidas de cuyo nombre forme parte el texto introducido por el usuario en la caja de texto de la partida.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdSeleccion_Click()
Dim nodx As MSComctlLib.node
Dim i As Integer
Dim bParar As Boolean

If m_lNodo >= tvwestrCC.Nodes.Count Then m_lNodo = 0
bParar = False
For i = m_lNodo + 1 To tvwestrCC.Nodes.Count
        Set nodx = tvwestrCC.Nodes.Item(i)
        If InStr(1, nodx.Text, txtSeleccion.Text, vbTextCompare) > 0 Then
            nodx.Selected = True
            Set tvwestrCC.selectedItem = nodx
            m_lNodo = nodx.Index
            If Me.Visible Then tvwestrCC.SetFocus
            Exit For
        End If
        If i = tvwestrCC.Nodes.Count And Not bParar Then
            i = 2
            m_lNodo = 0
            bParar = True
        End If
Next
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Carga del formulario.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub Form_Load()

    Screen.MousePointer = vbHourglass

    CargarRecursos
    
    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
    cmdModificar.Visible = True

    DoEvents
    
    chkNoVigentes.Value = False
    
    Dim bPartidaYaSeleccionada As Boolean
    Dim i As Integer
    
    'COMPROBAMOS SI EL NODO SELECCIONADO TIENE ALGUNA PARTIDA PRESUPUESTARIA CON NODOS SELECCIONADOS.
    'SI ES AS�, TOMAMOS EL C�DIGO DE LA PRIMERA QUE HAYA PARA MOSTRARLA
    If NoHayParametro(m_sEstructuraArbolPresupuestario) Then
        m_sEstructuraArbolPresupuestario = "C"
    End If
    
    'Con el nodo  de la unidad organizativa seleccionado en la pantalla de origen, recuperamos sus datos presupuestarios
    Set m_SelNode = frmUSUARIOS.g_SelNode
    
    Set m_oPartidas = Nothing
    Set m_oPartidas = oFSGSRaiz.Generar_CPresConceptos5Nivel0
    
    If Not m_SelNode Is Nothing Then
        
        Dim ILongCodUON1 As Long
        Dim ILongCodUON2 As Long
        Dim ILongCodUON3 As Long
        Dim ILongCodUON4 As Long
        
        ILongCodUON1 = basParametros.gLongitudesDeCodigos.giLongCodUON1
        ILongCodUON2 = basParametros.gLongitudesDeCodigos.giLongCodUON2
        ILongCodUON3 = basParametros.gLongitudesDeCodigos.giLongCodUON3
        ILongCodUON4 = basParametros.gLongitudesDeCodigos.giLongCodUON4
        
        m_sUON1 = Trim(Mid(m_SelNode.key, 5, ILongCodUON1))
        m_sUON2 = Trim(Mid(m_SelNode.key, 5 + ILongCodUON1, ILongCodUON2))
        m_sUON3 = Trim(Mid(m_SelNode.key, 5 + ILongCodUON1 + ILongCodUON2, ILongCodUON3))
        m_sUON4 = Trim(Mid(m_SelNode.key, 5 + ILongCodUON1 + ILongCodUON2 + ILongCodUON3, ILongCodUON4))
    
        Dim sDen As String
        Dim aPartidaPresup() As String
    
        'Al cargar el form, comprobamos si la UON tiene ya alguna partida presup asociada
        '(cogemos la primera)
        aPartidaPresup = m_oPartidas.obtenerPartidaPresup(m_sUON1, m_sUON2, m_sUON3, m_sUON4)
        
        m_sPres0Inicial = aPartidaPresup(0)
        sDen = aPartidaPresup(1)
        
        m_bPres0PluriAnual = IIf(aPartidaPresup(2) = "1", True, False)
        
    Else
        'ERROR ERROR ERROR
        'Si m_SelNode es nothing, significa que no vienes desde el men� contextual de un nodo del �rbol presupuestario
        'Entonces, �de d�nde vienes?
        m_sUON1 = ""
        m_sUON2 = ""
        m_sUON3 = ""
        m_sUON4 = ""
    End If
    
    'CARGAR COMBO CON LAS PARTIDAS PRESUPUESTARIAS
    Dim oPartida As cPresConcep5Nivel0
    
    cboPartPres.clear
    
    'EL usuario y el perfil son necesarios para determinar permisos de acceso a mantenimiento.
    'aqu� no los vamos a necesitar (en principio).
    'm_oPartidas.CargarPresupuestosConceptos5 , , , , , , , , m_sUsu, m_sPerfilCod
    'Pero le pasamos un TRUE porque s�lo queremos las partidas presup con nodos activos (o sea, con al menos 1 nodo de nivel 1 que no sea baja l�gica)
    m_oPartidas.CargarPresupuestosConceptos5 , , , , , , , , , , False
    
    ReDim arArboles(m_oPartidas.Count)
    
    For Each oPartida In m_oPartidas
        cboPartPres.AddItem oPartida.Cod & " - " & oPartida.Den
        arArboles(cboPartPres.ListCount - 1) = oPartida.Cod
    Next
    
    
    'Si s�lo hay un partida
    If m_oPartidas.Count = 1 Then
        'Si s�lo hay una partida presupuestaria, ocultamos el combo
        fraSelCContables.Top = cboPartPres.Top
        tvwestrCC.Height = tvwestrCC.Height + cboPartPres.Height
        cboPartPres.Visible = False
    End If
    
    If m_bPres0PluriAnual Then
        lblFecDesde.Visible = False
        lblFecHasta.Visible = False
        txtFecDesde.Visible = False
        txtFecHasta.Visible = False
        cmdCalDesde.Visible = False
        cmdCalHasta.Visible = False
        chkNoVigentes.Visible = False
        cmdCargar.Visible = False
    End If
    
    'Si hay partidas presupuestarias
    If m_oPartidas.Count > 0 Then
        
        bPartidaYaSeleccionada = False
        
        'Seleccionamos en el combo la partida que hemos obtenido antes
        If m_sPres0Inicial <> "" Then
            For i = 0 To cboPartPres.ListCount - 1
                If cboPartPres.List(i) = m_sPres0Inicial & " - " & sDen Or arArboles(i) = m_sPres0Inicial Then
                    'AL ASIGNAR EL INDEX, SE CARGA EL ARBOL DESDE EL cboPartPres_Click
                    cboPartPres.ListIndex = i
                    bPartidaYaSeleccionada = True
                End If
            Next
        End If
            
        If Not bPartidaYaSeleccionada Then
            'Si no hay ninguna partida presup seleccionada (porque la unidad organizativa tiene nodos seleccionados de la misma)
            'seleccionamos la primera partida
            'Al dar valor al ListIndex, ya directamente se va a cargar el Treeview
            'pues se activa el evento cboPartPres_click
            cboPartPres.ListIndex = 0
            
        End If
        
    End If
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    

    ModoConsulta
    
    Screen.MousePointer = vbNormal
End Sub

''' Revisado por: blp. Fecha: 03/05/2012
''' <summary>
''' Procedimiento que lanza la carga del �rbol de nodos presupuestarios a partir de la partida presup. seleccionada en el combo.
''' Controla as� mismo si, estando en modo edici�n, se han realizado cambios en los nodos seleccionados. Si es as�, solicita confirmaci�n para guardar los cambios.
''' </summary>
''' <remarks>Llamada desde la selecci�n de cualquiera de los elementos del combobox cboPartPres; Tiempo m�ximo < 1 seg.</remarks>
Private Sub cboPartPres_Click()
    
    CargarFiltros
    
    If cboPartPres.List(cboPartPres.ListIndex) = "" Then
        tvwestrCC.Nodes.clear
        Exit Sub
    End If
    
    m_bYaAvisado = False
    
    m_sPres0Inicial = arArboles(cboPartPres.ListIndex)
    
    If cboPartPres = "" Then Exit Sub
    
    'Al de arbol presupuestario en el combo; volvemos a solo lectura
    m_sEstructuraArbolPresupuestario = "C"
    ModoConsulta
End Sub

''' Revisado por: blp. Fecha:03/05/2012
''' <summary>
''' Guarda los nodos presupuestarios seleccionados en el arbol asociados a la unidad organizativa correspondiente
''' </summary>
''' <remarks>Llamada desde Boton Aceptar (cmdAceptar_Click) y desde la selecci�n de una partida presup en el combo (cboPartPres_Click); Tiempo m�ximo < 1 seg. </remarks>
Private Sub AceptarCambiosNodosPresup()
If NoHayParametro(m_sUON1) And NoHayParametro(m_sUON2) And NoHayParametro(m_sUON3) And NoHayParametro(m_sUON4) Then
    'ERROR NO HAY DATOS DE LA UNIDAD ORGANIZATIVA
    Exit Sub
End If

Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node
Dim nodx As MSComctlLib.node
Dim sPres0 As String
Dim sPres1 As String
Dim sPRES2 As String
Dim sPRES3 As String
Dim sPRES4 As String
Dim lLongPresNiv1 As Long
Dim lLongPresNiv2 As Long
Dim lLongPresNiv3 As Long
Dim lLongPresNiv4 As Long
Dim adoresNodosSeleccionados As New ADODB.Recordset
Dim adoresNodosNoSeleccionados As New ADODB.Recordset

    adoresNodosSeleccionados.Fields.Append "PRES0", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv0
    adoresNodosSeleccionados.Fields.Append "PRES1", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv1
    adoresNodosSeleccionados.Fields.Append "PRES2", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv2
    adoresNodosSeleccionados.Fields.Append "PRES3", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv3
    adoresNodosSeleccionados.Fields.Append "PRES4", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv4
    adoresNodosSeleccionados.Open

    adoresNodosNoSeleccionados.Fields.Append "PRES0", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv0
    adoresNodosNoSeleccionados.Fields.Append "PRES1", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv1
    adoresNodosNoSeleccionados.Fields.Append "PRES2", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv2
    adoresNodosNoSeleccionados.Fields.Append "PRES3", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv3
    adoresNodosNoSeleccionados.Fields.Append "PRES4", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv4
    adoresNodosNoSeleccionados.Open

    lLongPresNiv1 = basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1
    lLongPresNiv2 = basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2
    lLongPresNiv3 = basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3
    lLongPresNiv4 = basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4

    Set nodx = tvwestrCC.Nodes(1)

    If nodx.key = "Raiz " Then
        If Not oPres0 Is Nothing Then
            sPres0 = oPres0.Cod
        Else
            sPres0 = arArboles(cboPartPres.ListIndex)
        End If

        If nodx.Checked Then
            adoresNodosSeleccionados.AddNew
            adoresNodosSeleccionados("PRES0") = sPres0
            adoresNodosSeleccionados("PRES1") = ""
            adoresNodosSeleccionados("PRES2") = ""
            adoresNodosSeleccionados("PRES3") = ""
            adoresNodosSeleccionados("PRES4") = ""
            adoresNodosSeleccionados.Update
        Else
            adoresNodosNoSeleccionados.AddNew
            adoresNodosNoSeleccionados("PRES0") = sPres0
            adoresNodosNoSeleccionados("PRES1") = ""
            adoresNodosNoSeleccionados("PRES2") = ""
            adoresNodosNoSeleccionados("PRES3") = ""
            adoresNodosNoSeleccionados("PRES4") = ""
            adoresNodosNoSeleccionados.Update
        End If
    Else
        Exit Sub
    End If

    If nodx.Children > 0 Then
        Set nod1 = nodx.Child
        Do While Not nod1 Is Nothing

            sPres1 = Trim(Mid(nod1.key, 6, lLongPresNiv1))

            If nod1.Checked Then
                adoresNodosSeleccionados.AddNew
                adoresNodosSeleccionados("PRES0") = sPres0
                adoresNodosSeleccionados("PRES1") = sPres1
                adoresNodosSeleccionados("PRES2") = ""
                adoresNodosSeleccionados("PRES3") = ""
                adoresNodosSeleccionados("PRES4") = ""
                adoresNodosSeleccionados.Update
            Else
                adoresNodosNoSeleccionados.AddNew
                adoresNodosNoSeleccionados("PRES0") = sPres0
                adoresNodosNoSeleccionados("PRES1") = sPres1
                adoresNodosNoSeleccionados("PRES2") = ""
                adoresNodosNoSeleccionados("PRES3") = ""
                adoresNodosNoSeleccionados("PRES4") = ""
                adoresNodosNoSeleccionados.Update
            End If

            If nod1.Children > 0 Then
                Set nod2 = nod1.Child
                Do While Not nod2 Is Nothing

                    sPRES2 = Trim(Mid(nod2.key, (6 + lLongPresNiv1) - 1, lLongPresNiv2))

                    If nod2.Checked Then
                        adoresNodosSeleccionados.AddNew
                        adoresNodosSeleccionados("PRES0") = sPres0
                        adoresNodosSeleccionados("PRES1") = sPres1
                        adoresNodosSeleccionados("PRES2") = sPRES2
                        adoresNodosSeleccionados("PRES3") = ""
                        adoresNodosSeleccionados("PRES4") = ""
                        adoresNodosSeleccionados.Update
                    Else
                        adoresNodosNoSeleccionados.AddNew
                        adoresNodosNoSeleccionados("PRES0") = sPres0
                        adoresNodosNoSeleccionados("PRES1") = sPres1
                        adoresNodosNoSeleccionados("PRES2") = sPRES2
                        adoresNodosNoSeleccionados("PRES3") = ""
                        adoresNodosNoSeleccionados("PRES4") = ""
                        adoresNodosNoSeleccionados.Update
                    End If

                    If nod2.Children > 0 Then
                        Set nod3 = nod2.Child
                        Do While Not nod3 Is Nothing

                            sPRES3 = Trim(Mid(nod3.key, (6 + lLongPresNiv1 + lLongPresNiv2) - 1, lLongPresNiv3))

                            If nod3.Checked Then
                                adoresNodosSeleccionados.AddNew
                                adoresNodosSeleccionados("PRES0") = sPres0
                                adoresNodosSeleccionados("PRES1") = sPres1
                                adoresNodosSeleccionados("PRES2") = sPRES2
                                adoresNodosSeleccionados("PRES3") = sPRES3
                                adoresNodosSeleccionados("PRES4") = ""
                                adoresNodosSeleccionados.Update
                            Else
                                adoresNodosNoSeleccionados.AddNew
                                adoresNodosNoSeleccionados("PRES0") = sPres0
                                adoresNodosNoSeleccionados("PRES1") = sPres1
                                adoresNodosNoSeleccionados("PRES2") = sPRES2
                                adoresNodosNoSeleccionados("PRES3") = sPRES3
                                adoresNodosNoSeleccionados("PRES4") = ""
                                adoresNodosNoSeleccionados.Update
                            End If


                            If nod3.Children > 0 Then
                                Set nod4 = nod3.Child
                                Do While Not nod4 Is Nothing

                                    sPRES4 = Trim(Mid(nod4.key, (6 + lLongPresNiv1 + lLongPresNiv2 + lLongPresNiv3) - 1, lLongPresNiv4))

                                    If nod4.Checked Then
                                        adoresNodosSeleccionados.AddNew
                                        adoresNodosSeleccionados("PRES0") = sPres0
                                        adoresNodosSeleccionados("PRES1") = sPres1
                                        adoresNodosSeleccionados("PRES2") = sPRES2
                                        adoresNodosSeleccionados("PRES3") = sPRES3
                                        adoresNodosSeleccionados("PRES4") = sPRES4
                                        adoresNodosSeleccionados.Update
                                    Else
                                        adoresNodosNoSeleccionados.AddNew
                                        adoresNodosNoSeleccionados("PRES0") = sPres0
                                        adoresNodosNoSeleccionados("PRES1") = sPres1
                                        adoresNodosNoSeleccionados("PRES2") = sPRES2
                                        adoresNodosNoSeleccionados("PRES3") = sPRES3
                                        adoresNodosNoSeleccionados("PRES4") = sPRES4
                                        adoresNodosNoSeleccionados.Update
                                    End If

                                    Set nod4 = nod4.Next
                                Loop
                            End If
                            Set nod3 = nod3.Next
                        Loop
                    End If
                    Set nod2 = nod2.Next
                Loop
            End If
            Set nod1 = nod1.Next
        Loop
    End If

    If m_oPresupuestosNiv0 Is Nothing Then
        Set m_oPresupuestosNiv0 = oFSGSRaiz.Generar_CPresConceptos5Nivel0
    End If

    m_oPresupuestosNiv0.ModificarGestor frmUSUARIOS.g_oUsuario.Persona.Cod, adoresNodosSeleccionados, adoresNodosNoSeleccionados

    Set m_oPresupuestos = Nothing

End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que muestra la informaci�n del formulario en modo de s�lo lectura
''' </summary>
''' <remarks>Llamada desde cmdAceptar_Click, cmdCancelar_Click, cboPartPres_Click. Tiempo m�ximo 0,3 seg.</remarks>
Private Sub ModoConsulta()
    m_sEstructuraArbolPresupuestario = "C"
    
    m_bSoloLectura = True
    
    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
    cmdModificar.Visible = True
    cmdCargar.Enabled = False
    chkNoVigentes.Enabled = True
    txtFecDesde.Enabled = True
    cmdCalDesde.Enabled = True
    txtFecHasta.Enabled = True
    cmdCalHasta.Enabled = True
    cmdCargar.Enabled = True
                
    m_bCambiosEnNodosSel = False 'En modo consulta no hay cambios en nodos. por si acaso pasamos a false la vble
    
    If NoHayParametro(cboPartPres.ListIndex) Then
        iniciarCargaArbolPresup m_sPres0Inicial
    Else
        iniciarCargaArbolPresup arArboles(cboPartPres.ListIndex)
    End If
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que muestra la informaci�n del formulario en modo modificable
''' </summary>
''' <remarks>Llamada desde cmdModificar_Click. Tiempo m�ximo 0,3 seg.</remarks>
Private Sub ModoEdicion()
    m_sEstructuraArbolPresupuestario = "M"

    m_bSoloLectura = False

    cmdAceptar.Visible = True
    cmdCancelar.Visible = True
    cmdModificar.Visible = False
    cmdCargar.Enabled = True
    chkNoVigentes.Enabled = True
    txtFecDesde.Enabled = True
    cmdCalDesde.Enabled = True
    txtFecHasta.Enabled = True
    cmdCalHasta.Enabled = True
    
    If NoHayParametro(cboPartPres.ListIndex) Then
        iniciarCargaArbolPresup m_sPres0Inicial
    Else
        iniciarCargaArbolPresup arArboles(cboPartPres.ListIndex)
    End If
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que inicia la carga de �rbol presupuestario. Llama a un procedimento que genera una clase CPresConcep5Nivel0 con el c�digo de Partida presupuestaria recibido y luego a otro que genera el �rbol
''' </summary>
''' <param name="sPres0">C�digo de la partida presupuestaria de la que se desea construir el �rbol</param>
''' <remarks>Llamada desde ModoConsulta, ModoEdicion. Tiempo m�ximo 0,02 seg.</remarks>
Private Sub iniciarCargaArbolPresup(ByVal sPres0 As String)
    PartidaSeleccionada sPres0
    
    GenerarEstructuraPresupuestos sPres0, False
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que genera el �rbol presupuestario a partir de un c�digo de partida presupuestaria determinado
''' </summary>
''' <param name="sPres0">C�digo de la partida presupuestaria de la que se desea construir el �rbol</param>
''' <param name="bOrdenadoPorDen">Boolean que indica si se desea ordenar la estructura de presupuestos por descripci�n. False implica ordenar por c�digo</param>
''' <remarks>Llamada desde iniciarCargaArbolPresup. Tiempo m�ximo 0,03 seg.</remarks>
Private Sub GenerarEstructuraPresupuestos(ByVal sPres0 As String, ByVal bOrdenadoPorDen As Boolean)
    Dim nodo As MSComctlLib.node
    
    'SOLO LECTURA -> FORMULARIO DE SOLO LECTURA
    If m_sEstructuraArbolPresupuestario = "C" Or m_sEstructuraArbolPresupuestario = "" Then
        tvwestrCC.Checkboxes = False
    Else 'M, MODO EDICION
        tvwestrCC.Checkboxes = False
    End If
    
    tvwestrCC.Nodes.clear
    
    Set m_oPresupuestos = oFSGSRaiz.Generar_CPresConceptos5Nivel1
    m_oPresupuestos.GenerarEstructuraPresupSeleccionadosGestor m_sUON1, m_sUON2, m_sUON3, m_sUON4, basPublic.gParametrosInstalacion.gIdioma, m_bSoloLectura, sPres0, bOrdenadoPorDen, frmUSUARIOS.g_oUsuario.Persona.Cod, m_vFecDesde, m_vFecHasta, m_bVerNoVigentes, m_bPres0PluriAnual
    
    'Generamos el arbol de presupuestos que se ve en el formulario
    GenerarArbolPresupuestos
    
    Exit Sub

ERROR:
    Set nodo = Nothing
    Resume Next
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que, a partir de unos datos ya generados, construye un �rbol presupuestario con sus nodos
''' en un control treeview
''' </summary>
''' <remarks>Llamada desde GenerarEstructuraPresupuestos; Tiempo m�ximo 0,02 seg.</remarks>
Private Sub GenerarArbolPresupuestos()
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim oPRES1 As CPresConcep5Nivel1
    Dim oPRES2 As CPresConcep5Nivel2
    Dim oPRES3 As CPresConcep5Nivel3
    Dim oPRES4 As CPresConcep5Nivel4
    Dim nodx As MSComctlLib.node
    Dim nodxPadrePres1 As MSComctlLib.node
    Dim nodxPadrePres2 As MSComctlLib.node
    Dim nodxPadrePres3 As MSComctlLib.node
    Dim i As Integer
    Dim todosSubnodosPRES1Seleccionados As Boolean
    Dim todosSubnodosPRES2Seleccionados As Boolean
    Dim todosSubnodosPRES3Seleccionados As Boolean
    Dim todosSubnodosPRES4Seleccionados As Boolean
    todosSubnodosPRES1Seleccionados = False
    todosSubnodosPRES2Seleccionados = False
    todosSubnodosPRES3Seleccionados = False
    todosSubnodosPRES4Seleccionados = False

    If m_sEstructuraArbolPresupuestario = "C" Or m_sEstructuraArbolPresupuestario = "" Then 'SOLO LECTURA -> FORMULARIO DE SOLO LECTURA
        
        tvwestrCC.Checkboxes = False
    Else 'M, MODO EDICION
        
        tvwestrCC.Checkboxes = True
    End If

    tvwestrCC.Nodes.clear

    Set nodx = tvwestrCC.Nodes.Add(, , "Raiz ", oPres0.Den, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True
    
    If oPres0.Seleccionado Then
        If tvwestrCC.Checkboxes Then
            nodx.Checked = True
        Else
            nodx.Image = "PRESSEL"
        End If
    End If
    

    For Each oPRES1 In m_oPresupuestos
        todosSubnodosPRES1Seleccionados = True
        If Not (oPRES1.BajaLog) Then
            scod1 = oPRES1.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(oPRES1.Cod))
            Set nodx = tvwestrCC.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
            nodx.Tag = "PRES1" & oPRES1.Cod
            If oPRES1.Seleccionado Then
                If tvwestrCC.Checkboxes Then
                    nodx.Checked = True
                Else
                    nodx.Image = "PRESSEL"
                End If
            Else
                todosSubnodosPRES1Seleccionados = False
            End If
        End If

        For Each oPRES2 In oPRES1.PresConceptos5Nivel2
            todosSubnodosPRES2Seleccionados = True
            If Not (oPRES2.BajaLog) Then
                scod2 = oPRES2.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(oPRES2.Cod))
                Set nodxPadrePres1 = nodx
                Set nodx = tvwestrCC.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                nodx.Tag = "PRES2" & oPRES2.Cod
                If oPRES2.Seleccionado Then
                    If tvwestrCC.Checkboxes Then
                        nodx.Checked = True
                    Else
                        nodx.Image = "PRESSEL"
                    End If
                Else
                    todosSubnodosPRES2Seleccionados = False
                End If
            End If
            For Each oPRES3 In oPRES2.PresConceptos5Nivel3
                todosSubnodosPRES3Seleccionados = True
                If Not (oPRES3.BajaLog) Then
                    scod3 = oPRES3.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(oPRES3.Cod))
                    Set nodxPadrePres2 = nodx
                    Set nodx = tvwestrCC.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                    nodx.Tag = "PRES3" & oPRES3.Cod
                    If oPRES3.Seleccionado Then
                        If tvwestrCC.Checkboxes Then
                            nodx.Checked = True
                        Else
                            nodx.Image = "PRESSEL"
                        End If
                    Else
                        todosSubnodosPRES3Seleccionados = False
                    End If
                End If
                For Each oPRES4 In oPRES3.PresConceptos5Nivel4
                    todosSubnodosPRES4Seleccionados = True
                    If Not (oPRES4.BajaLog) Then
                        scod4 = oPRES4.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(oPRES4.Cod))
                        Set nodxPadrePres3 = nodx
                        Set nodx = tvwestrCC.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                        nodx.Tag = "PRES4" & oPRES4.Cod
                        If oPRES4.Seleccionado Then
                            If tvwestrCC.Checkboxes Then
                                nodx.Checked = True
                            Else
                                nodx.Image = "PRESSEL"
                            End If
                        Else
                            todosSubnodosPRES4Seleccionados = False
                        End If
                    End If
                Next
                If todosSubnodosPRES4Seleccionados Then
                    If tvwestrCC.Checkboxes Then
                        nodxPadrePres3.Checked = True
                    Else
                        nodxPadrePres3.Image = "PRESSEL"
                    End If
                    todosSubnodosPRES3Seleccionados = True
                End If
                Set oPRES4 = Nothing
            Next
            If todosSubnodosPRES3Seleccionados Then
                If tvwestrCC.Checkboxes Then
                    nodxPadrePres2.Checked = True
                Else
                    nodxPadrePres2.Image = "PRESSEL"
                End If
                todosSubnodosPRES2Seleccionados = True
            End If
            Set oPRES3 = Nothing
        Next
        If todosSubnodosPRES2Seleccionados Then
            If tvwestrCC.Checkboxes Then
                nodxPadrePres1.Checked = True
            Else
                nodxPadrePres1.Image = "PRESSEL"
            End If
            todosSubnodosPRES1Seleccionados = True
        End If
        Set oPRES2 = Nothing
    Next
    If todosSubnodosPRES1Seleccionados Then
        If tvwestrCC.Checkboxes Then
            tvwestrCC.Nodes(1).Checked = True
        Else
            tvwestrCC.Nodes(1).Image = "PRESSEL"
        End If
    End If
    
    'ChequearPadres tvwestrCC.Nodes
    
    With tvwestrCC
        For i = 1 To .Nodes.Count
                If .Checkboxes Then 'treeview de escritura
                    If .Nodes(i).Checked And .Nodes(i).Tag <> "Raiz " Then
                        .Nodes(i).EnsureVisible
                        .Nodes(i).Expanded = False
                    End If
                Else 'treeview de solo lectura
                    If .Nodes(i).Image = "PRESSEL" And .Nodes(i).Tag <> "Raiz " Then
                        .Nodes(i).EnsureVisible
                        .Nodes(i).Expanded = False
                    End If
                End If
        Next
    End With
    
    Set oPRES1 = Nothing
        
    Set m_oPresupuestos = Nothing
    
    Exit Sub
    
ERROR:
    Set nodx = Nothing
    Set m_oPresupuestos = Nothing
    Resume Next
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que guarda los cambios realizados en los nodos presupuestarios vinculados a la Unidad Organizativa sobre la que estemos trabajando
''' y pasa la visualizaci�n del formulario a s�lo lectura
''' </summary>
''' <remarks>Llamada desde Bot�n Aceptar del presente formulario: frmESTRORGCCoste; Tiempo m�ximo < 1 seg</remarks>
Private Sub cmdAceptar_Click()
    
    Screen.MousePointer = vbHourglass
    
    AceptarCambiosNodosPresup
    
    ModoConsulta
        
    Screen.MousePointer = vbNormal
    
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que pasa la visualizaci�n del formulario a modo s�lo lectura y descarta los cambios que se hayan podido hacer y no hayan sido guardados previamente
''' </summary>
''' <remarks>Llamada desde Boton Cancelar del formulario frmESTRORGCCoste; Tiempo m�ximo < 1 seg</remarks>
Private Sub cmdCancelar_Click()
    
    ModoConsulta
    
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que pasa la visualizaci�n del formulario a modo modificaci�n
''' </summary>
''' <remarks>Llamada desde Boton Modificar del formulario frmESTRORGCCoste; Tiempo m�ximo < 1 seg</remarks>
Private Sub cmdModificar_Click()
    
    If cboPartPres.List(cboPartPres.ListIndex) = "" Then
        MsgBox (m_sSeleccionePartida)
        Exit Sub
    End If
    
    ModoEdicion
    
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que crea una clase de partida presupuestaria (CPresConcep5Nivel0) al que, mediante el c�digo de la partida seleccionada
''' asigna todos los valores de dicha partida para poder trabajar con ella en el formulario.
''' </summary>
''' <param name="sPres0">C�digo de la partida presupuestaria seleccionada</param>
''' <remarks>Llamada desde iniciarCargaArbolPresup; Tiempo m�ximo < 1 seg.</remarks>
Private Sub PartidaSeleccionada(ByVal sPres0 As String)
    
    Set oPres0 = oFSGSRaiz.Generar_CPresConcep5Nivel0
    oPres0.Cod = sPres0
    oPres0.UON1 = m_sUON1
    oPres0.UON2 = m_sUON2
    oPres0.UON3 = m_sUON3
    oPres0.UON4 = m_sUON4
    Set oIBaseDatos = oPres0
    oIBaseDatos.IniciarEdicion
    Set oIBaseDatos = Nothing
    
End Sub


''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que marca como checkeados los nodos hijos de un nodo checkeado
''' </summary>
''' <param name="Nodx">Nodo checkeado</param>
''' <returns></returns>
''' <remarks>Llamada desde el chequeo un nodo en el arbol de nodos presupuestarios; Tiempo m�ximo < 1 seg.</remarks>
Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.node)
    
    Dim nod1 As MSComctlLib.node
    Dim nod2 As MSComctlLib.node
    Dim nod3 As MSComctlLib.node
    Dim nod4 As MSComctlLib.node

    Set nod1 = nodx.Child
    
    If Not nod1 Is Nothing Then
    If NodeNestingLevel(nod1) <= g_oParametrosSM.Item(m_sPres0Inicial).ImpNivel Then
        While Not nod1 Is Nothing
             nod1.Checked = True
             nod1.EnsureVisible
             nod1.Expanded = False
             Set nod2 = nod1.Child
             If Not nod2 Is Nothing Then
             If NodeNestingLevel(nod2) <= g_oParametrosSM.Item(m_sPres0Inicial).ImpNivel Then
                While Not nod2 Is Nothing
                    nod2.Checked = True
                    nod2.EnsureVisible
                    nod2.Expanded = False
                    Set nod3 = nod2.Child
                    If Not nod3 Is Nothing Then
                    If NodeNestingLevel(nod3) <= g_oParametrosSM.Item(m_sPres0Inicial).ImpNivel Then
                        While Not nod3 Is Nothing
                            nod3.Checked = True
                            nod3.EnsureVisible
                            nod3.Expanded = False
                            Set nod4 = nod3.Child
                            If Not nod4 Is Nothing Then
                            If NodeNestingLevel(nod4) <= g_oParametrosSM.Item(m_sPres0Inicial).ImpNivel Then
                                While Not nod4 Is Nothing
                                    nod4.Checked = True
                                    nod4.EnsureVisible
                                    nod4.Expanded = False
                                    Set nod4 = nod4.Next
                                Wend
                            End If
                            End If
                            Set nod3 = nod3.Next
                        Wend
                    End If
                    End If
                    Set nod2 = nod2.Next
                Wend
             End If
             End If
             Set nod1 = nod1.Next
        Wend
    DoEvents
    End If
    End If

End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que quita la marca de checkeados a los nodos hijos de un nodo checkeado
''' </summary>
''' <param name="Nodx">Nodo descheckeado</param>
''' <returns></returns>
''' <remarks>Llamada desde el deschequeo de un nodo en el arbol de nodos presupuestarios; Tiempo m�ximo < 1 seg.</remarks>

Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.node)
    
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node

    Set nod1 = nodx.Child

    While Not nod1 Is Nothing
         
         nod1.Checked = False
         Set nod2 = nod1.Child
         
         While Not nod2 Is Nothing
             nod2.Checked = False
             Set nod3 = nod2.Child
             
             While Not nod3 Is Nothing
                 nod3.Checked = False
                 Set nod4 = nod3.Child
                 
                 While Not nod4 Is Nothing
                     nod4.Checked = False
                     Set nod4 = nod4.Next
                 Wend
                 Set nod3 = nod3.Next
             Wend
             Set nod2 = nod2.Next
         Wend
         Set nod1 = nod1.Next
    Wend
            
    DoEvents

End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que deschequea a los nodos padres del nodo deschequeado
''' </summary>
''' <param name="Nodx">Nodo deschequeado</param>
''' <returns></returns>
''' <remarks>Llamada desde el deschequeo de un nodo en el arbol de nodos presupuestarios; Tiempo m�ximo < 1 seg.</remarks>
Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.node)
    
    If nodx.Index = 1 Then
        'Es el nodo raiz
        nodx.Checked = False
    Else
        Set nodx = nodx.Parent
        While Not nodx Is Nothing And nodx.Index <> 1
            nodx.Checked = False
            Set nodx = nodx.Parent
        Wend
    End If
    DoEvents
    
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que redimensiona los elementos del formulario cuando se cambian las dimensiones del mismo
''' </summary>
''' <remarks>Llamada desde la modificaci�n del tama�o del formulario; Tiempo m�ximo < 1 seg.</remarks>
Private Sub Form_Resize()

    Arrange

End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que vac�a de contenido una serie de variables y objetos usados en el formulario
''' </summary>
''' <remarks>Llamada desde el evento Unload del formulario; Tiempo m�ximo < 1 seg.</remarks>
Private Sub Form_Unload(Cancel As Integer)
Set oPres0 = Nothing
m_bCambiosEnNodosSel = False
m_sEstructuraArbolPresupuestario = ""
m_vFecDesde = ""
m_vFecHasta = ""
m_bVerNoVigentes = False
m_bPres0PluriAnual = False
End Sub


''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que al chequear un nodo, chequea �ste y sus hijos.
''' Si se deschequea, se deschequea el nodo y sus padres.
''' </summary>
''' <remarks>Llamada desde el chequeo de un nodo en el arbol de nodos presupuestarios; Tiempo m�ximo < 1 seg.</remarks>
Private Sub tvwestrCC_NodeCheck(ByVal node As MSComctlLib.node)
    
    'El nodo s�lo puede marcarse si el nivel del nodo es menor o igual al nivel de imputaci�n
    '(no es muy probable que haya partidas a un nivel al que no se pueda imputar, dado que ser�a absurdo,
    'pero se controla por precauci�n
    'Por otro lado, si s�lo hay un nodo ra�z, sin subnodos, no se puede seleccionar el check
    If NodeNestingLevel(node) <= g_oParametrosSM.Item(m_sPres0Inicial).ImpNivel _
        And _
       (node.Children > 0 Or Not node.Parent Is Nothing) _
    Then
        m_bCambiosEnNodosSel = True 'Cmabia el check de un nodo, con lo que ya suponemos que hay cambios
        
        If node.Checked Then
                MarcarTodosLosHijos node
                ChequearPadre node
        Else
                QuitarMarcaTodosSusPadres node
                QuitarMarcaTodosLosHijos node
        End If
    Else
        Set mNode = node
        Timer.Enabled = True
    End If
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Procedimiento que el tamanyo y posicion de los controles del formulario cuando se cambian las dimensiones del mismo
''' </summary>
''' <remarks>Llamada desde el evento Resize del formulario: form_resize; Tiempo m�ximo < 1 seg.</remarks>
Private Sub Arrange()
    Dim iAncho As Integer
    iAncho = 8770
    If Me.WindowState = 0 Then          'Limitamos la reducci�n
        If Me.Width <= iAncho - 50 Then 'de tama�o de la ventana
            Me.Width = iAncho           'para que no se superpongan
            Exit Sub                    'unos controles sobre
        End If                          'otros. S�lo lo hacemos
        If Me.Height <= 4500 Then       'cuando no se maximiza ni
            Me.Height = 4570            'minimiza la ventana,
            Exit Sub                    'WindowState=0, pues cuando esto
        End If                          'ocurre no se pueden cambiar las
    End If                              'propiedades Form.Height y Form.Width
    
    
    If Height >= 1755 Then
        If Not cboPartPres.Visible Then
            tvwestrCC.Height = Height - 600
            fraSelCContables.Height = Height - (1550 - 470)
        Else
            tvwestrCC.Height = Height - 2800
            fraSelCContables.Height = Height - 1500
        End If
        cmdModificar.Top = Height - 870
        cmdAceptar.Top = Height - 870
        cmdCancelar.Top = Height - 870
    End If
    If Width >= iAncho - 50 Then
        If (m_bPres0PluriAnual) Then
            tvwestrCC.Height = tvwestrCC.Height + txtFecDesde.Height
            tvwestrCC.Top = txtFecDesde.Top
        End If
        
        tvwestrCC.Width = Width - 470
        
        fraSelCContables.Width = Width
        cboPartPres.Width = Width - 2120
        txtSeleccion.Width = Width - 2730
                    
        cmdSeleccion.Left = txtSeleccion.Left + txtSeleccion.Width + 100
        
    End If
    
End Sub

''' Revisado por: blp. Fecha: 21/06/2012
''' <summary>
''' Procedimiento que carga en las variables los valores de los filtros del formulario para efectuar la carga de las partidas
''' </summary>
''' <remarks>Llamada desde cboPartPres_click y cmdCargar_click. Tiempo m�ximo < 1 seg.</remarks>
Private Sub CargarFiltros()
    bOrdenadoPorDen = False
    m_vFecDesde = txtFecDesde.Text
    m_vFecHasta = txtFecHasta.Text
    m_bVerNoVigentes = chkNoVigentes.Value
    m_sPres0Inicial = arArboles(cboPartPres.ListIndex)
End Sub

''' Revisado por: blp. Fecha: 22/06/2012
''' <summary>Returns the nesting level of a TreeView's Node object<summary>
''' <returns>(returns zero for root nodes.)</returns>
''' <remarks>Llamada desde frmUsuariosSelContables. M�ximo < 0,1 seg.</remarks>
Function NodeNestingLevel(ByVal node As node) As Integer
Do Until (node.Parent Is Nothing)
NodeNestingLevel = NodeNestingLevel + 1
Set node = node.Parent
Loop
End Function

''' Revisado por: blp. Fecha: 26/06/2012
''' <summary>
''' Marcamos los checks de los nodos padre si los hijos est�n marcados
''' </summary>
''' <remarks>Llamada desde GenerarArbolPresupuestos; Tiempo m�ximo < 1 seg.</remarks>
Private Sub ChequearPadre(ByVal oNode As node)
    Dim oNodoParent As node
    Dim oNodoChild As node
    Dim bTodosNodosMarcados As Boolean
    bTodosNodosMarcados = True
    Set oNodoParent = oNode.Parent
    If Not oNodoParent Is Nothing Then
        If oNodoParent.Children > 0 Then
            Set oNodoChild = oNodoParent.Child
            Do While Not oNodoChild Is Nothing
                If Not oNodoChild.Checked Then
                    bTodosNodosMarcados = False
                End If
                Set oNodoChild = oNodoChild.Next
            Loop
            If bTodosNodosMarcados Then
                oNodoParent.Checked = True
            End If
        End If
    End If
End Sub



''' Revisado por: blp. Fecha: 26/06/2012
''' <summary>
''' Usamos un timer para solucionar un bug del treeview que impide controlar el evento NodeCheck del treeview
''' de modo que permita cancelar el check de un nodo
''' En este caso queremos controlar que ciertos nodos NO est�n chequeados nunca pero es posible controlar que no se pueda cambiar
''' </summary>
''' <remarks>Llamada desde ; Tiempo m�ximo < 1 seg.</remarks>
Private Sub timer_Timer()
    Timer.Enabled = False
    mNode.Checked = False
    Set mNode = Nothing
End Sub

