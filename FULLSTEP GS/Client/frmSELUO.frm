VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSELUO 
   BackColor       =   &H00808000&
   Caption         =   "Selecci�n de unidad organizativa"
   ClientHeight    =   4470
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6465
   Icon            =   "frmSELUO.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4470
   ScaleWidth      =   6465
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1950
      TabIndex        =   2
      Top             =   4110
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cerrar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3135
      TabIndex        =   1
      Top             =   4110
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwestrorg 
      Height          =   3975
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   6330
      _ExtentX        =   11165
      _ExtentY        =   7011
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   13
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":0CB2
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":107A
            Key             =   "UON3A"
            Object.Tag             =   "UON3A"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":13CC
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":1720
            Key             =   "UON2A"
            Object.Tag             =   "UON2A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":1A72
            Key             =   "UON1A"
            Object.Tag             =   "UON1A"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":1DC4
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":2118
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":24AC
            Key             =   "UON1B"
            Object.Tag             =   "UON1B"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":27FE
            Key             =   "UON2B"
            Object.Tag             =   "UON2B"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":2B50
            Key             =   "UON3B"
            Object.Tag             =   "UON3B"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":2EA2
            Key             =   "UON1ERP"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":321A
            Key             =   "UON2ERP"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELUO.frx":358C
            Key             =   "UON3ERP"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSELUO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables para interactuar con otros forms
Public sUON1 As String
Public sUON2 As String
Public sUON3 As String
Public sDen As String
Public bRUO As Boolean
Public sOrigen As String
Public m_bModif As Boolean
Public bRPerfUON As Boolean
Public bMostrarBajas As Boolean
Private m_bCheckChildren As Boolean
Private m_bCheckParents As Boolean
Private m_bUncheckParents As Boolean
Public Aceptar As Boolean
Public Cancelar As Boolean
Public ismultiselect As Boolean
Private m_sSubtitulo As String
Private mrstUsuPerfUONs As ADODB.Recordset

Private m_oUonsSeleccionadas As CUnidadesOrganizativas
Private m_oUonSeleccionada As Variant
Private m_oUons As CUnidadesOrganizativas ' guarda la lista de uons cargadas en el treeview
Private m_bGuardarUon0 As Boolean
Private m_bModoEdicion As Boolean
'nodos de interes
Private m_oNodoActual As node
Private m_bMouseDown As Boolean
Public m_bDescargarFrm As Boolean
Dim m_bActivado As Boolean
Private m_bUnload As Boolean
Private m_sMsgError As String

Public Property Let enableCancelar(ByVal Value As Boolean)
    Me.cmdCancelar.Enabled = Value
End Property

Public Property Get multiselect() As Boolean
    multiselect = ismultiselect
End Property

Public Property Let multiselect(ByVal Var As Boolean)
    ismultiselect = Var
End Property
'''<summary>Si activado al seleccionar un nodo selecciona todos sus descendientes, por defecto activado</summary>
Public Property Let CheckChildren(ByVal Value As Boolean)
    m_bCheckChildren = Value
End Property

'''<summary>Si activado al seleccionar un nodo selecciona todos sus descendientes, por defecto activado</summary>
Public Property Get CheckChildren() As Boolean
    CheckChildren = m_bCheckChildren
End Property
'''<summary>Si activado al deshacer la selecci�n de un nodo deshace la de todos sus ascendientes, por defecto activado</summary>
Public Property Let UncheckParents(ByVal Value As Boolean)
    m_bCheckParents = Value
End Property

Public Property Let EnableCheck(ByVal Value As Boolean)
    m_bModoEdicion = Value
End Property

Public Property Let ModoEdicion(ByVal Value As Boolean)
    m_bModoEdicion = Value
    'Me.tvwestrorg.Checkboxes = value
    If Not Value Then
        Me.cmdAceptar.Visible = False
        Me.cmdCancelar.Visible = False
    End If
End Property
'''<summary>Establece si marcar la raiz del arbol supone guardar la uon0, en caso de falso se guardaran todas las uons de nivel 1</summary>
Public Property Let GuardarUon0(ByVal Value As Boolean)
    m_bGuardarUon0 = Value
End Property

'''<summary>Obtiene un listado con las unidades organizativas seleccionadas de nivel superior</summary>
'''<summary>esto es, excluye las hijas de las seleccionadas</summary>
Public Property Get UonsSeleccionadas() As CUnidadesOrganizativas
    If Not m_oUonsSeleccionadas Is Nothing Then
        Set UonsSeleccionadas = m_oUonsSeleccionadas
    Else
        Set UonsSeleccionadas = oFSGSRaiz.Generar_CUnidadesOrganizativas
    End If
End Property

'''<summary>Obtiene un listado con las unidades organizativas seleccionadas de nivel superior</summary>
'''<summary>esto es, excluye las hijas de las seleccionadas</summary>
Public Property Set UonsSeleccionadas(ByRef oUons As CUnidadesOrganizativas)
    Set m_oUonsSeleccionadas = oUons
    dibujarNodosChequeados
End Property

Public Property Get UonSeleccionada() As Variant
    Set UonSeleccionada = m_oUonSeleccionada
End Property

Public Property Let subtitulo(ByVal Value As String)
    m_sSubtitulo = Value
End Property

Public Property Get subtitulo() As String
    subtitulo = m_sSubtitulo
End Property

'''<summary>guarda en el atributo uonsSeleccionadas las las unidades organizativas seleccionadas de nivel superior</summary>
Private Sub guardarUonsSeleccionadas()
    Dim nodosChequeados As Collection
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodosChequeados = New Collection
    Set m_oUonsSeleccionadas = oFSGSRaiz.Generar_CUnidadesOrganizativas
    'Obtenemos los nodos seleccionados de nivel superior
    buscarChequeados Me.tvwestrorg.Nodes(1), nodosChequeados
    'insertamos los nodos (uon) en la colecci�n de uons seleccionadas
    Dim nodo As node
    For Each nodo In nodosChequeados
        m_oUonsSeleccionadas.Add m_oUons.Item(nodo.key), m_oUons.Item(nodo.key).key
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "guardarUonsSeleccionadas", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub cmdAceptar_Click()
    Dim nodx As MSComctlLib.node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.Aceptar = True
    Set nodx = tvwestrorg.selectedItem
    '''si el arbol es checkeable guardamos la colecci�n de uons seleccionadas para usar despu�s
    If Me.multiselect Then
        guardarUonsSeleccionadas
        Unload Me
        Exit Sub
    Else
        Set m_oUonSeleccionada = m_oUons.Item(nodx.key)
        If Not isBloqueado(nodx) Then
            Set m_oUonsSeleccionadas = oFSGSRaiz.Generar_CUnidadesOrganizativas
            m_oUonsSeleccionadas.Add m_oUons.Item(nodx.key), m_oUons.Item(nodx.key).key
        Else
            Me.Aceptar = False
            Exit Sub
        End If
    End If
    If Not nodx Is Nothing Then
        Select Case Left(nodx.Tag, 4)
            Case "UON1"
                sUON1 = DevolverCod(nodx)
                sUON2 = ""
                sUON3 = ""
            Case "UON2"
                sUON1 = DevolverCod(nodx.Parent)
                sUON2 = DevolverCod(nodx)
                sUON3 = ""
            Case "UON3"
                sUON1 = DevolverCod(nodx.Parent.Parent)
                sUON2 = DevolverCod(nodx.Parent)
                sUON3 = DevolverCod(nodx)
            Case Else
                sUON1 = ""
                sUON2 = ""
                sUON3 = ""
        End Select
        
        If UODestinoNoValida Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        If nodx.Parent Is Nothing Then
            sDen = nodx.Text
        Else
            sDen = Right(nodx.Text, Len(nodx.Text) - Len(Right(nodx.Tag, Len(nodx.Tag) - 4)) - 3)
        End If
                
        Select Case sOrigen
            Case "InfAhorroApliUO"
                frmInfAhorroApliUO.MostrarUOSeleccionada
            Case "LstESTRORG"
                frmLstESTRORG.MostrarUOSeleccionada
            Case "frmLstINFAhorroAplA4B2C5"
                frmListados.ofrmLstApliUO.MostrarUOSeleccionada
            Case "frmLstINFAhorroAplfrmInfAhorroApliUO"
                frmInfAhorroApliUO.ofrmLstApliUO.MostrarUOSeleccionada
            Case "frmLstDEST"
                frmLstDEST.MostrarUOSeleccionada
            Case "frmLstPRESPorProy"
                frmLstPRESPorProy.m_sUON1Sel = sUON1
                frmLstPRESPorProy.m_sUON2Sel = sUON2
                frmLstPRESPorProy.m_sUON3Sel = sUON3
                frmLstPRESPorProy.m_sUODescrip = sDen
                frmLstPRESPorProy.lblProy = ""
                frmLstPRESPorProy.MostrarUOSeleccionada
            Case "frmLstPRESPorParCon"
                frmLstPRESPorParCon.m_sUON1Sel = sUON1
                frmLstPRESPorParCon.m_sUON2Sel = sUON2
                frmLstPRESPorParCon.m_sUON3Sel = sUON3
                frmLstPRESPorParCon.m_sUODescrip = sDen
                frmLstPRESPorParCon.lblParCon = ""
                frmLstPRESPorParCon.MostrarUOSeleccionada
            Case "frmLstPRESPorCon3"
                frmLstPRESPorCon3.m_sUON1Sel = sUON1
                frmLstPRESPorCon3.m_sUON2Sel = sUON2
                frmLstPRESPorCon3.m_sUON3Sel = sUON3
                frmLstPRESPorCon3.m_sUODescrip = sDen
                frmLstPRESPorCon3.lblParCon = ""
                frmLstPRESPorCon3.MostrarUOSeleccionada
            Case "frmLstPRESPorCon4"
                frmLstPRESPorCon4.m_sUON1Sel = sUON1
                frmLstPRESPorCon4.m_sUON2Sel = sUON2
                frmLstPRESPorCon4.m_sUON3Sel = sUON3
                frmLstPRESPorCon4.m_sUODescrip = sDen
                frmLstPRESPorCon4.lblParCon = ""
                frmLstPRESPorCon4.MostrarUOSeleccionada
                
            Case "frmEST"
                frmEST.MostrarUOSeleccionada
                
            Case "frmSolicitudes"
                frmSolicitudes.MostrarUOSeleccionada
               
            Case "frmLstSolicitud"
                frmLstSolicitud.MostrarUOSeleccionada
                
            Case "frmSOLConfiguracion"
                frmSOLConfiguracion.MostrarUOSeleccionada
                
            Case "frmFlujosRoles"
                Me.Hide 'Lo descargo en el Origen
                Screen.MousePointer = vbNormal
                Exit Sub
                
            Case "frmFormularios"
                frmFormularios.MostrarUOSeleccionada
                
            Case "frmDesgloseValores"
                frmDesgloseValores.MostrarUOSeleccionada
                
            Case "frmSolicitudDetalle"
                frmSolicitudDetalle.m_sUON1Sel = sUON1
                frmSolicitudDetalle.m_sUON2Sel = sUON2
                frmSolicitudDetalle.m_sUON3Sel = sUON3
                frmSolicitudDetalle.m_sUODescrip = sDen
                
            Case "frmSolicitudDesglose"
                frmSolicitudDesglose.m_sUON1Sel = sUON1
                frmSolicitudDesglose.m_sUON2Sel = sUON2
                frmSolicitudDesglose.m_sUON3Sel = sUON3
                frmSolicitudDesglose.m_sUODescrip = sDen
                
            Case "frmSOLAbrirFaltan"
                frmSOLAbrirFaltan.UOSeleccionada sUON1, sUON2, sUON3, sDen
            
        End Select
    
        Screen.MousePointer = vbNormal
        
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCancelar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.Cancelar = True
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()

   If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bActivado Then
        PonerHook Me
        m_bActivado = True
    End If
    'checkear los nodos si los hubiera
    dibujarNodosChequeados
    
    caption = caption & " : " & m_sSubtitulo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bUnload = True
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELUO", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
   End If
End Sub

Private Sub Form_Initialize()
    m_bCheckChildren = True
    m_bUncheckParents = True
    m_bGuardarUon0 = False
    m_bModoEdicion = True
    Set m_oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    m_bDescargarFrm = False
    m_bActivado = False
    m_bUnload = False
    
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    sDen = ""
    Screen.MousePointer = vbHourglass
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    ConfigurarSeguridad
    'JIM:Para arboles chequeables selecciono la estructura arborea desde los propios forms en funci�n de lo que en ese momento necesite
    'mediante el m�todo cargarArbol
    If m_oUons.Count = 0 Then
        GenerarEstructuraOrg False
    End If
    Me.tvwestrorg.Checkboxes = Me.ismultiselect
    Screen.MousePointer = vbNormal
    
    caption = caption & m_sSubtitulo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Resize()
    
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height > 1000 Then
        If sOrigen = "frmFlujosRoles" And Not m_bModif Then
            tvwestrorg.Height = Me.Height - 643
        Else
            tvwestrorg.Height = Me.Height - 958
        End If
    End If
    If Me.Width > 500 Then tvwestrorg.Width = Me.Width - 300
    
    cmdAceptar.Top = tvwestrorg.Top + tvwestrorg.Height + 40
    cmdCancelar.Top = cmdAceptar.Top
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub

Private Sub ConfigurarSeguridad()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sOrigen = "frmFlujosRoles" And Not m_bModif Then
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub cargarArbol(ByRef oUnidadesOrgN1 As CUnidadesOrgNivel1, ByRef oUnidadesOrgN2 As CUnidadesOrgNivel2, ByRef oUnidadesOrgN3 As CUnidadesOrgNivel3)
Dim j As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    '************************************************************
    'Generamos la estructura arborea
    ' Unidades organizativas
    'previamente limpiamos el arbol por si hubiera anteriormente datos
    Me.clear
    Dim nodx As node
    
    Dim oUON0 As CUnidadOrgNivel0
    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3
    Dim scod1, scod2, scod3 As String
    
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
    Set oUON0 = oFSGSRaiz.Generar_CUnidadOrgNivel0
    m_oUons.Add oUON0, "UON0"
    
    For Each oUON1 In oUnidadesOrgN1
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        m_oUons.Add oUON1, "UON1" & scod1
        If Not oUON1.activa Then
            bloquearNodo nodx
            bloquearNodo Me.tvwestrorg.Nodes(1)
        End If
        If oUON1.ConPresup Then
            nodx.Image = "UON1A"
        End If
        
        If oUON1.BajaLog Then
            nodx.Image = "UON1B"
        End If
        
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON1" & scod1 & "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        m_oUons.Add oUON2, "UON1" & scod1 & "UON2" & scod2
        If oUON2.ConPresup Then
            nodx.Image = "UON2A"
        End If
        
        If oUON2.BajaLog Then
            nodx.Image = "UON2B"
        End If
        If Not oUON2.activa Then
            bloquearNodo nodx
        End If
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
    Next
    
    j = 0
    For Each oUON3 In oUnidadesOrgN3
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1 & "UON2" & scod2, tvwChild, "UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        m_oUons.Add oUON3, "UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3
        If Not oUON3.activa Then
            bloquearNodo nodx
        End If
        If oUON3.ConPresup Then
            nodx.Image = "UON3A"
        End If
        
        If oUON3.BajaLog Then
            nodx.Image = "UON3B"
        End If
        
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "cargarArbol", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>Genera la estructura organizativa</summary>
''' <param name="bOrdenadoPorDen">Ordenado por denominaci�n</param>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
    Dim oDistsNivel1 As CDistItemsNivel1
    Dim oDistsNivel2 As CDistItemsNivel2
    Dim oDistsNivel3 As CDistItemsNivel3
    ' Unidades organizativas
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    ' Otras
    Dim dDistPorcen As Double
    Dim bytTipoPres As Byte
    Dim lIdPerfil As Long
    Dim oPerf As CPerfil
    
      
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    'Si alguien ha distribu�do la compra sin tener la restricci�n de UO,
    ' y despu�s entra alguien con la restricci�n de UO, no hay quemostrar esas distribuciones
    
    dDistPorcen = 0
    
    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    Set oDistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
    Set oDistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
    Set oDistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
    
    Select Case sOrigen
        Case "frmLstPRESPorProy"
            bytTipoPres = 1
        Case "frmLstPRESPorParCon"
            bytTipoPres = 2
        Case "frmLstPRESPorCon3"
            bytTipoPres = 3
        Case "frmLstPRESPorCon4"
            bytTipoPres = 4
    End Select

    If (oUsuarioSummit.Tipo <> TIpoDeUsuario.proveedor) And (bRUO Or bRPerfUON) Then
        If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
        
        If bRPerfUON Then
            Set oPerf = oFSGSRaiz.Generar_CPerfil
            oPerf.Id = lIdPerfil
            Set mrstUsuPerfUONs = oPerf.DevolverUONsPerfil
            Set oPerf = Nothing
        End If
        
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, bytTipoPres, , bMostrarBajas, , bRPerfUON, lIdPerfil
            Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, bytTipoPres, , bMostrarBajas, , bRPerfUON, lIdPerfil
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPres basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, bytTipoPres, bMostrarBajas, , , bRPerfUON, lIdPerfil
            Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, bytTipoPres, , bMostrarBajas, , bRPerfUON, lIdPerfil
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPres basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, bytTipoPres, bMostrarBajas, , , bRPerfUON, lIdPerfil
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPres basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, bytTipoPres, bMostrarBajas, , , bRPerfUON, lIdPerfil
        End Select
    Else
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres , , , , , , , bOrdenadoPorDen, False, bytTipoPres, , bMostrarBajas
            Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres , , , , , , , bOrdenadoPorDen, False, bytTipoPres, , bMostrarBajas
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPres , , , , , , , bOrdenadoPorDen, False, bytTipoPres, bMostrarBajas
            Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres , , , , , , , bOrdenadoPorDen, False, bytTipoPres, , bMostrarBajas
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPres , , , , , , , bOrdenadoPorDen, False, bytTipoPres, bMostrarBajas
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPres , , , , , , , bOrdenadoPorDen, False, bytTipoPres, bMostrarBajas
        End Select
    End If
   '************************************************************
    'Generamos la estructura de arbol
    ' Unidades organizativas
    cargarArbol oUnidadesOrgN1, oUnidadesOrgN2, oUnidadesOrgN3
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    If sOrigen = "frmFlujosRoles" Then
        If sUON1 <> "" Then
            If sUON2 <> "" Then
                If sUON3 <> "" Then
                    tvwestrorg.Nodes("UON1" & sUON1 & Space(basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sUON1)) & "UON2" & sUON2 & Space(basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sUON2)) & "UON3" & sUON3 & Space(basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sUON3))).Selected = True
                Else
                    tvwestrorg.Nodes("UON1" & sUON1 & Space(basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sUON1)) & "UON2" & sUON2 & Space(basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sUON2))).Selected = True
                End If
            Else
                tvwestrorg.Nodes("UON1" & sUON1 & Space(basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sUON1))).Selected = True
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "GenerarEstructuraOrg", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case Left(node.Tag, 4)
    Case "UON1", "UON2", "UON3"
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "DevolverCod", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub Form_Terminate()
    Set m_oUonsSeleccionadas = Nothing
    Set m_oUonSeleccionada = Nothing
    Set m_oUons = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
QuitarHook
    If Me.cmdAceptar.Enabled Or Me.cmdCancelar.Enabled Or Not m_bModoEdicion Then
        bRUO = False
        bRPerfUON = False
        sOrigen = ""
        bMostrarBajas = False
        Set mrstUsuPerfUONs = Nothing
        m_oUons.clear
    Else
        Cancel = True
        'Estamos en un alta... no podemos cerrar el form sin seleccionar al menos una uon
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub tvwestrorg_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarInterfaz tvwestrorg.selectedItem
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "tvwestrorg_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub tvwestrorg_Collapse(ByVal node As MSComctlLib.node)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarInterfaz node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "tvwestrorg_Collapse", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub tvwestrorg_Expand(ByVal node As MSComctlLib.node)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarInterfaz node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "tvwestrorg_Expand", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub tvwestrorg_KeyUp(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bMouseDown Then
        
        m_bMouseDown = False
        
        If tvwestrorg.Checkboxes Then
            If isBloqueado(m_oNodoActual) Then
                m_oNodoActual.Checked = Not m_oNodoActual.Checked
                DoEvents
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "tvwestrorg_KeyUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub tvwestrorg_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim nod As MSComctlLib.node
    Dim tvhti As TVHITTESTINFO
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwestrorg.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwestrorg.hWnd, TVM_HITTEST, 0, tvhti)) Then
            
                If (tvhti.FLAGS And TVHT_ONITEMSTATEICON) Then
                     'El estado del nodo cambiar� al finalizar este procedimiento
                    Set m_oNodoActual = nod
                    m_bMouseDown = True
                End If
            End If
        Else
            m_bMouseDown = False
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "tvwestrorg_MouseDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub tvwestrorg_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim nodo As node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If ismultiselect Then
        If Not m_oUons Is Nothing Then
            Set nodo = tvwestrorg.HitTest(X, Y)
            If Not nodo Is Nothing Then
                If nodo <> Me.tvwestrorg.Nodes(1) Then
                    tvwestrorg.ToolTipText = getSentidoIntegracionText(nodo)
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "tvwestrorg_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub tvwEstrOrg_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

    'si nodo bloqueado y venimos del evento nodechek !! cancelamos el check
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    tvwestrorg_KeyUp 0, Shift
    If m_bMouseDown Then
        Dim temp As node
        Set temp = tvwestrorg.HitTest(X, Y)
        If Not temp Is Nothing Then
            If temp.key = m_oNodoActual.key Then
                If m_bMouseDown Then
                    If Not m_oNodoActual Is Nothing Then
                        If isBloqueado(m_oNodoActual) Then
                            m_oNodoActual.Checked = Not m_oNodoActual.Checked
    
                            DoEvents
                        End If
                    End If
                End If
                
            End If
        Else
            m_oNodoActual.Checked = Not m_oNodoActual.Checked
        End If
        m_bMouseDown = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "tvwEstrOrg_MouseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub tvwestrorg_NodeCheck(ByVal node As MSComctlLib.node)
    Dim Value As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Value = node.Checked
    'comprobamos que el nodo no est� bloqueado (bloqueado si node.bold=true)
    If Not isBloqueado(node) Then
        'si estamos deseleccionando comprobamos que no tenga un padre bloqueado y seleccionado
        If Not node.Checked And tieneBloqueoEnPadre(node) Then
            node.Bold = True
        Else
            If Me.CheckChildren Then
                checkNodeDescendente node, Value
                
                If Not Value Then
                    checkNodeAscendente node, False
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "tvwestrorg_NodeCheck", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub ConfigurarInterfaz(ByRef node As MSComctlLib.node)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Sub
    
    cmdAceptar.Enabled = True
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "ConfigurarInterfaz", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELUO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
End Sub

''' <summary>Determinar si se est� intentando copiar los presupuestos en alguna UO predecesora para un usuario con restricci�n de UO.</summary>
''' <returns>Un valor booleano que ser� TRUE cuando la UO de destino seleccionada es padre o predecesor dela UO a la que se ve restringido
''' un usuario con esta restricci�n.</returns>
''' <remarks>Llamada desde: cmdAceptar_Click</remarks>

Private Function UODestinoNoValida() As Boolean
    Dim iUOBase As Integer
    Dim nodx As MSComctlLib.node
    Dim bNoValida As Boolean
    Dim sFiltro As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bNoValida = False
        
    If bRUO Then
        iUOBase = 0
        
        If (CStr(basOptimizacion.gUON3Usuario) <> "") Then
            iUOBase = 3
        ElseIf (CStr(basOptimizacion.gUON2Usuario) <> "") Then
            iUOBase = 2
        ElseIf (CStr(basOptimizacion.gUON1Usuario) <> "") Then
            iUOBase = 1
        End If
        
        Set nodx = tvwestrorg.selectedItem
        If val(Right(Left(nodx.Tag, 4), 1)) < iUOBase Then
            bNoValida = True
        Else
            oMensajes.RestriccionAUO
            bNoValida = False
        End If
    End If

    If Not bNoValida And bRPerfUON And Not Me.multiselect And bRUO Then
        'Comprobar que la UON seleccionada est� dentro de las UONs del perfil
        If sUON1 <> "" Then
            sFiltro = "(UON1='" & DblQuote(sUON1) & "' AND UON2=NULL AND UON3=NULL)"
        Else
            sFiltro = "(UON1=NULL AND UON2=NULL AND UON3=NULL)"
        End If
        If sUON2 <> "" Then
            sFiltro = sFiltro & " OR (UON1='" & DblQuote(sUON1) & "' AND UON2='" & DblQuote(sUON2) & "' AND UON3 =NULL)"
        End If
        If sUON3 <> "" Then
            sFiltro = sFiltro & " OR (UON1='" & DblQuote(sUON1) & "' AND UON2='" & DblQuote(sUON2) & "' AND UON3='" & DblQuote(sUON3) & "')"
        End If

        mrstUsuPerfUONs.Filter = sFiltro

        If mrstUsuPerfUONs.EOF Then
            oMensajes.RestriccionPerfUON
            bNoValida = True
        End If
    End If

    UODestinoNoValida = bNoValida
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "UODestinoNoValida", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Public Sub chequearTodo()
    Dim nodo As node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oUonsSeleccionadas = oFSGSRaiz.Generar_CUnidadesOrganizativas
    For Each nodo In Me.tvwestrorg.Nodes
        If Not isBloqueado(nodo) Then
            m_oUonsSeleccionadas.Add m_oUons.Item(nodo.key), nodo.key
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "ChequearTodo", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub bloquearTodo()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    deshabilitarNodoDescendente Me.tvwestrorg.Nodes(1)
    DoEvents
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "bloquearTodo", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
Public Function isBloqueadoNodo(ByVal sUON1 As String, ByVal sUON2 As String, ByVal sUON3 As String)
    Dim nodo As node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodo = buscarNodoUonCod(sUON1, sUON2, sUON3)
    isBloqueadoNodo = nodo.Bold
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "isBloqueadoNodo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
'''<summary>Chequea el nodo correspondiente a una UON y todos sus descendientes</summary>
'''<param name="oUon">UON a chequear (representada por nivel 3, pero puede ser de cualquier nivel)</param>
Public Sub chequearNodoUons(ByRef oUON As IUon)
    Dim nodo As node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodo = buscarNodoUon(oUON)
    If Not nodo Is Nothing Then
        nodo.Checked = True
        Expandir nodo
        If m_bCheckChildren Then
            checkNodeDescendente nodo, True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "chequearNodoUons", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub chequearNudoUonsCod(ByVal sUON1 As String, ByVal sUON2 As String, ByVal sUON3 As String)
    Dim nodo As node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodo = buscarNodoUonCod(sUON1, sUON2, sUON3)
    If Not nodo Is Nothing Then
        checkNodeDescendente nodo, True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "chequearNudoUonsCod", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub chequearNodosUons(ByRef oUons As CUnidadesOrganizativas)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If Not oUons Is Nothing Then
        Dim oUON As IUon
        For Each oUON In oUons
            chequearNodoUons oUON
        Next
    Else
        frmSELUO.chequearTodo
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "chequearNodosUons", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub bloquearNodo(ByRef nodo As node)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    nodo.ForeColor = &HC0C0C0
    nodo.Bold = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "bloquearNodo", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function isBloqueado(ByRef nodo As node) As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not nodo Is Nothing Then
        isBloqueado = nodo.Bold
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "isBloqueado", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
'''<summary>bloquea el nodo de una uon</summary>
Public Sub bloquearNodoUon(ByRef oUON As IUon)
    Dim nodo As node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodo = buscarNodoUon(oUON)
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "bloquearNodoUon", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

'<summary>Marca los nodos con Erp y bloquea sus ascendientes y descendientes en caso de
'que el par�metro bloquear sea true y tenga erp con sentido entrada </summary>
'<param name="oUons">Unidades organizativas (representando a cualquier tipo de Uon, de nivel 1, 2 o 3)</param>
'<param name="bloquear">true si queremos bloquear aquellas unidades con erp de entrada </param>
Public Sub marcarErpUons(ByRef oUons As CUnidadesOrganizativas, Optional ByVal bloquear As Boolean = True)
    Dim nodo As node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oUons.Count > 0 Then
        Dim oUON As IUon
        
        For Each oUON In oUons
            Set nodo = buscarNodoUon(oUON)
            'si la uon es de nivel 4, no aparece en el �rbol, sin embargo si la integraci�n es de sentido entrada
            'hay que bloquear los nodos ascendientes
            If oUON.Nivel < 4 Then
                
                If nodo.key <> "UON0" Then
                    m_oUons.Item(nodo.key).SentidoIntegracion = oUON.SentidoIntegracion
                    setImageErp nodo
                    If oUON.SentidoIntegracion = SentidoIntegracion.Entrada Then
                        bloquearNodoUon oUON
                        If bloquear Then
                            deshabilitarNodoDescendente nodo
                            deshabilitarNodoAscendente nodo
                        End If
                        
                    End If
                    
                End If
            Else
                'buscamos el nodo padre de la uon4 y lo bloqueamos
                Set nodo = buscarNodoUonCod(oUON.CodUnidadOrgNivel1, oUON.CodUnidadOrgNivel2, oUON.CodUnidadOrgNivel3)
                If oUON.SentidoIntegracion = SentidoIntegracion.Entrada Then
                    bloquearNodo nodo
                    deshabilitarNodoAscendente nodo
                End If
            End If
        Next
    End If
    Set nodo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "marcarErpUons", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

'''<summary>busca el nodo de una uon<summary>
Private Function buscarNodoUon(ByRef oUON As IUon) As node
    Dim sUON1, sUON2, sUON3 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sUON1 = oUON.CodUnidadOrgNivel1
    sUON2 = oUON.CodUnidadOrgNivel2
    sUON3 = oUON.CodUnidadOrgNivel3
    Set buscarNodoUon = buscarNodoUonCod(sUON1, NullToStr(sUON2), NullToStr(sUON3))
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "buscarNodoUon", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function buscarNodoUonCod(ByVal sUON1 As String, ByVal sUON2 As String, ByVal sUON3 As String)
    Dim nodo As node
    Dim hijo As node
    Dim bSalir As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodo = Me.tvwestrorg.Nodes(1)
    If sUON1 <> "" Then
        Set hijo = nodo.Child
        While Not hijo Is Nothing And Not bSalir
            If Replace(hijo.key, " ", "") = "UON1" & sUON1 Then
                Set nodo = hijo
                bSalir = True
            Else
                ' siguiente nodo
                Set hijo = hijo.Next
            End If
        Wend
    End If
    
    If sUON2 <> "" And Not nodo Is Nothing Then
        Set hijo = nodo.Child
        bSalir = False
        While Not hijo Is Nothing And Not bSalir
            If Replace(hijo.key, " ", "") = "UON1" & sUON1 & "UON2" & sUON2 Then
                Set nodo = hijo
                bSalir = True
            Else
                ' siguiente nodo
                Set hijo = hijo.Next
            End If
        Wend
    End If
    
    If sUON3 <> "" And Not nodo Is Nothing Then
        bSalir = False
        Set hijo = nodo.Child
        While Not hijo Is Nothing And Not bSalir
            If Replace(hijo.key, " ", "") = "UON1" & sUON1 & "UON2" & sUON2 & "UON3" & sUON3 Then
                Set nodo = hijo
                bSalir = True
            Else
                ' siguiente nodo
                Set hijo = hijo.Next
            End If
        Wend
    End If
    Set buscarNodoUonCod = nodo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "buscarNodoUonCod", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

'''<summary>Cambia el estado chequeado al nodo y sus descendientes, si chequeados expande la rama</summary>
'''<param name="node">nodo</param>
'''<param name="value">estado del check</param>
Private Sub checkNodeDescendente(ByRef nodo As node, Value As Boolean)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    nodo.Checked = Value
    If nodo.Children = 0 Then
        Exit Sub
    Else
        Dim hijo As node
        Set hijo = nodo.Child
        While Not hijo Is Nothing
            hijo.Checked = Value
            checkNodeDescendente hijo, Value
            ' siguiente nodo
            Set hijo = hijo.Next
        Wend
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "checkNodeDescendente", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
'''
'''<summary>Cambia el estado chequeado al nodo y a sus ascendientes</summary>
'''<param name="node">nodo</param>
'''<param name="value">estado del check</param>
Private Sub checkNodeAscendente(ByRef nodo As MSComctlLib.node, Value As Boolean)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    nodo.Checked = Value
    If nodo.Parent Is Nothing Or isBloqueado(nodo.Parent) Then
        Exit Sub
    Else
        checkNodeAscendente nodo.Parent, Value
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "checkNodeAscendente", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub deshabilitarNodoAscendente(ByRef nodo As node)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bloquearNodo nodo
    If Not nodo.Parent Is Nothing Then
        deshabilitarNodoAscendente nodo.Parent
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "deshabilitarNodoAscendente", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub deshabilitarNodoDescendente(ByRef nodo As node)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bloquearNodo nodo
    
    If nodo.Children = 0 Then
        Exit Sub
    Else
        Dim hijo As node
        Set hijo = nodo.Child
        While Not hijo Is Nothing
            deshabilitarNodoDescendente hijo
            ' siguiente nodo
            Set hijo = hijo.Next
        Wend
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "deshabilitarNodoDescendente", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function buscarChequeados(ByRef nodo As node, ByRef chequeados As Collection)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    'Si hemos activado guardar nodo 0 si el nodo chequeado es raiz, tomamos la uon0 como seleccionada
    'en caso contrario buscamos las uon (de mayor nivel, no sus hijas) chequeadas

    If nodo.key = "UON0" And nodo.Checked And m_bGuardarUon0 Then
        chequeados.Add nodo
        Exit Function
    Else
        If nodo.Checked And nodo.key <> "UON0" Then
            chequeados.Add nodo
        Else
            If nodo.Children > 0 Then
                Dim hijo As node
                Set hijo = nodo.Child
                While Not hijo Is Nothing
                    buscarChequeados hijo, chequeados
                    ' siguiente nodo
                    Set hijo = hijo.Next
                Wend
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "buscarChequeados", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub setImageErp(ByRef nodo As node)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case Left(nodo.Tag, 4)
        Case "UON1"
               nodo.Image = "UON1ERP"
        Case "UON2"
                nodo.Image = "UON2ERP"
        Case "UON3"
                nodo.Image = "UON3ERP"
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "setImageErp", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub clear()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.tvwestrorg.Nodes.clear
    Set m_oNodoActual = Nothing
    Set m_oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "clear", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Function tieneBloqueoEnPadre(ByVal nodo As node) As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nodo.Parent Is Nothing Then
        tieneBloqueoEnPadre = False
    Else
        If nodo.Parent.Checked And isBloqueado(nodo.Parent) Then
            tieneBloqueoEnPadre = True
        Else
            tieneBloqueoEnPadre = tieneBloqueoEnPadre(nodo.Parent)
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "tieneBloqueoEnPadre", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Function getSentidoIntegracionText(nodo As node) As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If ismultiselect Then
        Select Case m_oUons.Item(nodo.key).SentidoIntegracion
            Case 1:
                getSentidoIntegracionText = "FULLSTEP->ERP"
            Case 2:
                getSentidoIntegracionText = "FULLSTEP<-ERP"
            Case 3:
                getSentidoIntegracionText = "FULLSTEP<->ERP"
            Case 0:
                getSentidoIntegracionText = ""
        End Select
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "getSentidoIntegracionText", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub dibujarNodosChequeados()

    'checkear los nodos si los hubiera
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    LockWindowUpdate Me.hWnd
    If ismultiselect Then
        If Not m_oUonsSeleccionadas Is Nothing Then
            If Me.UonsSeleccionadas.Count > 0 Then
                Me.chequearNodosUons Me.UonsSeleccionadas
            End If
        End If
    End If
    If Not m_bModoEdicion Then
        Me.bloquearTodo
        Me.cmdAceptar.Visible = False
        Me.cmdCancelar.Visible = False
    End If
    LockWindowUpdate 0&
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "dibujarNodosChequeados", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub Expandir(nodo As node)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not nodo Is Nothing Then
        nodo.Expanded = True
        Expandir nodo.Parent
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "Expandir", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
