VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSolicitudBuscarSeguimiento 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Busqueda de solicitudes"
   ClientHeight    =   6150
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11640
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSolicitudBuscarSeguimiento.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6150
   ScaleWidth      =   11640
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtFecHasta 
      Height          =   285
      Left            =   7490
      TabIndex        =   6
      Top             =   1010
      Width           =   1110
   End
   Begin VB.CommandButton cmdCalFecHasta 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8620
      Picture         =   "frmSolicitudBuscarSeguimiento.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1010
      Width           =   315
   End
   Begin VB.CommandButton cmdBuscar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10500
      Picture         =   "frmSolicitudBuscarSeguimiento.frx":06D4
      Style           =   1  'Graphical
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Mantenimiento"
      Top             =   120
      Width           =   315
   End
   Begin VB.PictureBox picSeleccion 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   11640
      TabIndex        =   14
      Top             =   5775
      Width           =   11640
      Begin VB.CommandButton cmdSeleccionar 
         Caption         =   "&Seleccionar"
         Height          =   315
         Left            =   4080
         TabIndex        =   12
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   5400
         TabIndex        =   13
         Top             =   0
         Width           =   1215
      End
   End
   Begin VB.TextBox txtFecDesde 
      Height          =   285
      Left            =   5200
      TabIndex        =   4
      Top             =   1010
      Width           =   1110
   End
   Begin VB.CommandButton cmdCalFecDesde 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6340
      Picture         =   "frmSolicitudBuscarSeguimiento.frx":0A16
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1010
      Width           =   315
   End
   Begin VB.TextBox txtId 
      Height          =   285
      Left            =   1455
      TabIndex        =   1
      Top             =   625
      Width           =   1300
   End
   Begin VB.TextBox txtDescr 
      Height          =   285
      Left            =   3940
      TabIndex        =   2
      Top             =   625
      Width           =   4975
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcPeticionario 
      Height          =   285
      Left            =   1455
      TabIndex        =   3
      Top             =   1010
      Width           =   2535
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      HeadLines       =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4895
      Columns(1).Caption=   "PET"
      Columns(1).Name =   "PET"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4471
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgSolicitudes 
      Height          =   4245
      Left            =   60
      TabIndex        =   11
      Top             =   1500
      Width           =   11520
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   12
      stylesets.count =   8
      stylesets(0).Name=   "Normal"
      stylesets(0).BackColor=   16777215
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSolicitudBuscarSeguimiento.frx":0FA0
      stylesets(1).Name=   "Seleccion"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   8388608
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSolicitudBuscarSeguimiento.frx":0FBC
      stylesets(2).Name=   "Anulada"
      stylesets(2).ForeColor=   0
      stylesets(2).BackColor=   4744445
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSolicitudBuscarSeguimiento.frx":0FD8
      stylesets(3).Name=   "Cerrada"
      stylesets(3).ForeColor=   0
      stylesets(3).BackColor=   10079487
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmSolicitudBuscarSeguimiento.frx":0FF4
      stylesets(4).Name=   "Pendiente"
      stylesets(4).ForeColor=   0
      stylesets(4).BackColor=   16777215
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmSolicitudBuscarSeguimiento.frx":1010
      stylesets(5).Name=   "Header"
      stylesets(5).ForeColor=   0
      stylesets(5).BackColor=   -2147483633
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmSolicitudBuscarSeguimiento.frx":102C
      stylesets(6).Name=   "Aprobada"
      stylesets(6).ForeColor=   0
      stylesets(6).BackColor=   10409635
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmSolicitudBuscarSeguimiento.frx":1048
      stylesets(7).Name=   "Rechazada"
      stylesets(7).ForeColor=   0
      stylesets(7).BackColor=   12632256
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmSolicitudBuscarSeguimiento.frx":1064
      BeveColorScheme =   0
      BevelColorFrame =   12632256
      BevelColorHighlight=   8421504
      BevelColorShadow=   128
      BevelColorFace  =   12632256
      CheckBox3D      =   0   'False
      AllowDelete     =   -1  'True
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      CellNavigation  =   1
      MaxSelectedRows =   1
      HeadStyleSet    =   "Header"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   12
      Columns(0).Width=   2937
      Columns(0).Caption=   "TIPO"
      Columns(0).Name =   "TIPO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   1
      Columns(0).ButtonsAlways=   -1  'True
      Columns(1).Width=   1667
      Columns(1).Caption=   "Alta"
      Columns(1).Name =   "ALTA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1667
      Columns(2).Caption=   "Necesidad"
      Columns(2).Name =   "NECESIDAD"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   1058
      Columns(3).Caption=   "Ident."
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   2910
      Columns(4).Caption=   "Descripci�n breve"
      Columns(4).Name =   "DESCR"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1773
      Columns(5).Caption=   "Importe"
      Columns(5).Name =   "IMPORTE"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "standard"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   2699
      Columns(6).Caption=   "Peticionario"
      Columns(6).Name =   "PET"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).Style=   1
      Columns(6).ButtonsAlways=   -1  'True
      Columns(7).Width=   1852
      Columns(7).Caption=   "Estado"
      Columns(7).Name =   "ESTADO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   2725
      Columns(8).Caption=   "Comprador"
      Columns(8).Name =   "COMP"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(8).Style=   1
      Columns(8).ButtonsAlways=   -1  'True
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "ID_ESTADO"
      Columns(9).Name =   "ID_ESTADO"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "COD_PER"
      Columns(10).Name=   "COD_PER"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "COD_COMP"
      Columns(11).Name=   "COD_COMP"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      _ExtentX        =   20320
      _ExtentY        =   7488
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picEstado 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   9100
      ScaleHeight     =   900
      ScaleWidth      =   1305
      TabIndex        =   19
      Top             =   120
      Width           =   1300
      Begin VB.CheckBox chkEstado 
         BackColor       =   &H00808000&
         Caption         =   "DAprobadas"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   0
         Left            =   0
         TabIndex        =   8
         Top             =   -15
         Value           =   1  'Checked
         Width           =   1335
      End
      Begin VB.CheckBox chkEstado 
         BackColor       =   &H00808000&
         Caption         =   "DCerradas"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   1
         Left            =   0
         TabIndex        =   9
         Top             =   210
         Width           =   1215
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcTipoSolicit 
      Height          =   285
      Left            =   1455
      TabIndex        =   0
      Top             =   240
      Width           =   7480
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2249
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   9419
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID"
      Columns(2).Name =   "ID"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   13194
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblTipoSolicit 
      BackColor       =   &H00808000&
      Caption         =   "Dtipo de solicitud:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   21
      Top             =   270
      Width           =   1300
   End
   Begin VB.Label lblFecHasta 
      BackColor       =   &H00808000&
      Caption         =   "DHasta:"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   6850
      TabIndex        =   20
      Top             =   1040
      Width           =   645
   End
   Begin VB.Label lblFecDesde 
      BackColor       =   &H00808000&
      Caption         =   "DDesde:"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   4500
      TabIndex        =   18
      Top             =   1040
      Width           =   660
   End
   Begin VB.Label lblIdentificador 
      BackColor       =   &H00808000&
      Caption         =   "DIdentificador: : "
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   120
      TabIndex        =   17
      Top             =   655
      Width           =   1300
   End
   Begin VB.Label lblPeticionario 
      BackColor       =   &H00808000&
      Caption         =   "DPeticionario : "
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   120
      TabIndex        =   16
      Top             =   1040
      Width           =   1300
   End
   Begin VB.Label lblDescripcion 
      BackColor       =   &H00808000&
      Caption         =   "DDescripci�n: "
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   2900
      TabIndex        =   15
      Top             =   655
      Width           =   900
   End
   Begin VB.Shape ShapeSelecc 
      Height          =   1395
      Left            =   60
      Top             =   60
      Width           =   10400
   End
End
Attribute VB_Name = "frmSolicitudBuscarSeguimiento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public g_sOrigen As String

'Variables privadas
Private m_oInstancias As CInstancias

'Seguridad
Private m_bRuo As Boolean
Private m_bREquipo As Boolean
Private m_bRAsig As Boolean

'Variables de idiomas:
Private m_sPendiente As String
Private m_sAprobada As String
Private m_sRechazada As String
Private m_sAnulada As String
Private m_sCerrada As String
Private sId As String
Private sFecDesde As String
Private sFecHasta As String
Private sOrdenando As String

Private m_bRespetarCombo As Boolean

Private Sub chkEstado_Click(Index As Integer)
 Dim i As Integer
    Dim bCancelar As Boolean
    
    'cuando se deschequea se comprueba si existe alg�n check m�s marcado.En caso contrario
    'no permite quitar el check
    If chkEstado(Index).Value = vbUnchecked Then
        bCancelar = True
        For i = 0 To chkEstado.Count - 1
            If chkEstado(i).Value = vbChecked Then
                bCancelar = False
                Exit For
            End If
        Next i
        If bCancelar = True Then
            chkEstado(Index).Value = vbChecked
        End If
    End If
End Sub

Private Sub cmdBuscar_Click()
    CargarGridSolicitudes 1
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecDesde_Click()
    Set frmCalendar.frmDestination = frmSolicitudBuscar
    Set frmCalendar.ctrDestination = txtFecDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecDesde.Text <> "" Then
        frmCalendar.Calendar.Value = txtFecDesde.Text
    Else
        frmCalendar.Calendar.Value = Date
    End If
    
    frmCalendar.Show 1
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecHasta_Click()
    Set frmCalendar.frmDestination = frmSolicitudBuscar
    Set frmCalendar.ctrDestination = txtFecHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecHasta.Text <> "" Then
        frmCalendar.Calendar.Value = txtFecHasta.Text
    Else
        frmCalendar.Calendar.Value = Date
    End If
    
    frmCalendar.Show 1
    
   
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub


Private Sub cmdSeleccionar_Click()
    Dim oSolic As CInstancia
    
    'Selecciona una solicitud y la carga en el formulario correspondiente
    
    If sdbgSolicitudes.Rows = 0 Then
        Unload Me
        Exit Sub
    End If
    
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))) Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oSolic = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
                         
    Select Case g_sOrigen
    
        Case "frmSeguimiento"
            frmSeguimiento.PonerSolicitudSeleccionada oSolic
            
        Case "frmLstSeguimiento"
            frmLstPedidos.PonerSolicitudSeleccionada oSolic
            
    End Select
    
    Set oSolic = Nothing
    
    Screen.MousePointer = vbNormal
    
    Unload Me
End Sub

''' <summary>
''' Carga el formulario de busqueda solicitudes, con la configuraci�n de los campos, el orden , ...
''' </summary>
''' <remarks>Llamada desde; Al cargarse el formulario</remarks>
Private Sub Form_Load()
    'Carga el formulario
    Me.Width = 11730
    Me.Height = 6630
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    PonerFieldSeparator Me
    
    MDI.mnuPopUpSolicitudes.Item(1).Visible = False 'detalle de desglose
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_BUSCAR_SOLICITUDES_SEGUIMIENTO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        Me.caption = Ador(0).Value       '1 b�squeda de solicitudes de compra
        Ador.MoveNext
        sId = Ador(0).Value  'Identificador
        lblIdentificador.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblDescripcion.caption = Ador(0).Value   'descripci�n
        Ador.MoveNext
        lblPeticionario.caption = Ador(0).Value  'peticionario
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value & ":"    'desde
        sFecDesde = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value & ":"  'hasta
        sFecHasta = Ador(0).Value
        Ador.MoveNext
        
        sdbgSolicitudes.Columns("ALTA").caption = Ador(0).Value  'alta
        Ador.MoveNext
        sdbgSolicitudes.Columns("NECESIDAD").caption = Ador(0).Value  'necesidad
        Ador.MoveNext
        sdbgSolicitudes.Columns("ID").caption = Ador(0).Value  'identificador
        Ador.MoveNext
        sdbgSolicitudes.Columns("DESCR").caption = Ador(0).Value  'descripci�n breve
        Ador.MoveNext
        sdbgSolicitudes.Columns("IMPORTE").caption = Ador(0).Value  'importe
        Ador.MoveNext
        sdbgSolicitudes.Columns("PET").caption = Ador(0).Value  'peticionario
        Ador.MoveNext
        sdbgSolicitudes.Columns("COMP").caption = Ador(0).Value  'comprador
        
        Ador.MoveNext
        sOrdenando = Ador(0).Value 'Ordenando...
        
        Ador.MoveNext
        cmdSeleccionar.caption = Ador(0).Value 'Seleccionar
        
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value 'Cancelar
        
        Ador.MoveNext
        
        m_sPendiente = Ador(0).Value   'pendiente
        Ador.MoveNext
        m_sAprobada = Ador(0).Value  'aprobada
        Ador.MoveNext
        m_sRechazada = Ador(0).Value                             'rechazada
        Ador.MoveNext
        m_sAnulada = Ador(0).Value            'anulada
        Ador.MoveNext
        m_sCerrada = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        chkEstado(0).caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        chkEstado(1).caption = Ador(0).Value 'Pendientes
        Ador.MoveNext
        sdbgSolicitudes.Columns("UO").caption = Ador(0).Value  'UNIDAD ORG
        Ador.MoveNext
        sdbgSolicitudes.Columns("ESTADO").caption = Ador(0).Value  'ESTADO
        
        Ador.MoveNext
        lblTipoSolicit.caption = Ador(0).Value '29 tipo de solicitud
        Ador.MoveNext
        sdbcTipoSolicit.Columns("COD").caption = Ador(0).Value '30 C�digo
        Ador.MoveNext
        sdbcTipoSolicit.Columns("DEN").caption = Ador(0).Value '31 Denominaci�n
        Ador.MoveNext
        sdbgSolicitudes.Columns("TIPO").caption = Ador(0).Value '32 Tipo
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oInstancias = Nothing
End Sub



Private Sub sdbcPeticionario_Click()
    If Not sdbcPeticionario.DroppedDown Then
        sdbcPeticionario = ""
    End If
End Sub

Private Sub sdbcPeticionario_CloseUp()
     If sdbcPeticionario.Value = "..." Or sdbcPeticionario = "" Then
        sdbcPeticionario.Text = ""
        Exit Sub
    End If
    
    
    m_bRespetarCombo = True
    sdbcPeticionario.Text = sdbcPeticionario.Columns(1).Text
    m_bRespetarCombo = False
    
End Sub


Private Sub sdbcPeticionario_DropDown()
    'Cargar la combo con todos los usuarios con acceso al EP
    Dim oPersona As CPersona
    Dim oPersonas As CPersonas
    
    Screen.MousePointer = vbHourglass
    
    sdbcPeticionario.RemoveAll
    
    Set oPersonas = oFSGSRaiz.Generar_CPersonas
    If m_bRuo Then
        oPersonas.CargarPeticionariosSolCompra True, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
    Else
        oPersonas.CargarPeticionariosSolCompra (True)
    End If
    

    For Each oPersona In oPersonas
        sdbcPeticionario.AddItem oPersona.Cod & Chr(m_lSeparador) & oPersona.Cod & " - " & NullToStr(oPersona.nombre) & " " & oPersona.Apellidos
    Next
        
    Set oPersonas = Nothing
    
    sdbcPeticionario.SelStart = 0
    sdbcPeticionario.SelLength = Len(sdbcPeticionario.Text)
    sdbcPeticionario.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPeticionario_InitColumnProps()
    sdbcPeticionario.DataFieldList = "Column 1"
    sdbcPeticionario.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcPeticionario_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPeticionario.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPeticionario.Rows - 1
            bm = sdbcPeticionario.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPeticionario.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcPeticionario.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgSolicitudes_BtnClick()
    If sdbgSolicitudes.Col = -1 Then Exit Sub
    
    Select Case sdbgSolicitudes.Columns(sdbgSolicitudes.Col).Name
        Case "TIPO"
            If Not m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))).Solicitud Is Nothing Then
                Set frmDetTipoSolicitud.g_oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))).Solicitud
                frmDetTipoSolicitud.Show vbModal
            End If
            
        Case "PET"
            frmDetallePersona.g_sPersona = sdbgSolicitudes.Columns("COD_PER").Value
            frmDetallePersona.Show vbModal
            
        Case "COMP"
            frmDetallePersona.g_sPersona = sdbgSolicitudes.Columns("COD_COMP").Value
            frmDetallePersona.Show vbModal
    End Select
    
End Sub

Private Sub sdbgSolicitudes_HeadClick(ByVal ColIndex As Integer)
    Dim sHeadCaption As String

    ''' * Objetivo: Ordenar el grid segun la columna

    If ColIndex > 8 Then Exit Sub

    Screen.MousePointer = vbHourglass

    sHeadCaption = sdbgSolicitudes.Columns(ColIndex).caption
    sdbgSolicitudes.Columns(ColIndex).caption = sOrdenando
    
    CargarGridSolicitudes ColIndex
    
    sdbgSolicitudes.Columns(ColIndex).caption = sHeadCaption

    Screen.MousePointer = vbNormal

End Sub

''' <summary>
''' evento que visualiza o no las opciones del men� de solicitudes.
''' </summary>
''' <param name="Button">Boton izquierdo o derecho del raton</param>
''' <param name="Shift">Si esta la tecla shift presionada</param>
''' <param name="X">Posicion x</param>
''' <param name="Y">Posicion y</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgSolicitudes_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If sdbgSolicitudes.Rows = 0 Then Exit Sub
    
    If Button = 2 Then
        mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
        mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
        DoEvents
          
        'Habilita los men�s seg�n el estado de la solicitud seleccionada
        Select Case sdbgSolicitudes.Columns("ID_ESTADO").CellValue(sdbgSolicitudes.Bookmark)
            Case EstadoSolicitud.Anulada
                MDI.mnuPopUpSolicitudes.Item(8).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(4).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(5).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(6).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(2).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(3).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(7).Enabled = False
                
            Case EstadoSolicitud.Aprobada
                MDI.mnuPopUpSolicitudes.Item(8).Enabled = True
                MDI.mnuPopUpSolicitudes.Item(4).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(5).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(6).Enabled = True
                MDI.mnuPopUpSolicitudes.Item(2).Enabled = True
                MDI.mnuPopUpSolicitudes.Item(3).Enabled = True
                MDI.mnuPopUpSolicitudes.Item(7).Enabled = False
                
            Case EstadoSolicitud.Cerrada
                MDI.mnuPopUpSolicitudes.Item(8).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(4).Enabled = True
                MDI.mnuPopUpSolicitudes.Item(5).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(6).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(2).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(3).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(7).Enabled = True
                
            Case EstadoSolicitud.Pendiente
                MDI.mnuPopUpSolicitudes.Item(8).Enabled = True  'anular
                MDI.mnuPopUpSolicitudes.Item(4).Enabled = True  'aprobar
                MDI.mnuPopUpSolicitudes.Item(5).Enabled = True  'rechazar
                MDI.mnuPopUpSolicitudes.Item(6).Enabled = False 'cerrada
                MDI.mnuPopUpSolicitudes.Item(2).Enabled = True  'modificar
                MDI.mnuPopUpSolicitudes.Item(3).Enabled = True  'asignar comprador
                MDI.mnuPopUpSolicitudes.Item(7).Enabled = False 'reabrir
                
            Case EstadoSolicitud.Rechazada
                MDI.mnuPopUpSolicitudes.Item(8).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(4).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(5).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(6).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(2).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(3).Enabled = False
                MDI.mnuPopUpSolicitudes.Item(7).Enabled = False
                
        End Select
        
        PopupMenu MDI.POPUPSolicitudes
    End If
End Sub

Private Sub sdbgSolicitudes_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    
    If sdbgSolicitudes.Columns("ID_ESTADO").CellValue(Bookmark) = EstadoSolicitud.Pendiente Then
        For i = 0 To sdbgSolicitudes.Columns.Count - 1
            sdbgSolicitudes.Columns(i).CellStyleSet "Pendiente"
        Next i
    End If
    
    If sdbgSolicitudes.Columns("ID_ESTADO").CellValue(Bookmark) = EstadoSolicitud.Aprobada Then
        For i = 0 To sdbgSolicitudes.Columns.Count - 1
            sdbgSolicitudes.Columns(i).CellStyleSet "Aprobada"
        Next i
    End If
        
    If sdbgSolicitudes.Columns("ID_ESTADO").CellValue(Bookmark) = EstadoSolicitud.Rechazada Then
        For i = 0 To sdbgSolicitudes.Columns.Count - 1
            sdbgSolicitudes.Columns(i).CellStyleSet "Rechazada"
        Next i

    End If
    
    If sdbgSolicitudes.Columns("ID_ESTADO").CellValue(Bookmark) = EstadoSolicitud.Anulada Then
        For i = 0 To sdbgSolicitudes.Columns.Count - 1
            sdbgSolicitudes.Columns(i).CellStyleSet "Anulada"
        Next i
    End If
    
    If sdbgSolicitudes.Columns("ID_ESTADO").CellValue(Bookmark) = EstadoSolicitud.Cerrada Then
        For i = 0 To sdbgSolicitudes.Columns.Count - 1
            sdbgSolicitudes.Columns(i).CellStyleSet "Cerrada"
        Next i
    End If
End Sub

Private Sub CargarGridSolicitudes(iCol As Integer)
    Dim sPet As String
    Dim vEstado() As Variant
    Dim iOrden As TipoOrdenacionSolicitudes
    Dim vFecNec As Variant
    Dim i As Integer
    Dim sestado As String
    Dim oInstancia As CInstancia
    Dim lTipo As Long
    Dim sCompradorCod As String
    Dim sCompradorDatos As String
    
    sdbgSolicitudes.RemoveAll
    
    'Hace las comprobaciones necesarias:
    If txtId.Text <> "" Then
        If Not IsNumeric(txtId.Text) Then
            oMensajes.NoValido sId
            If Me.Visible Then txtId.SetFocus
            Exit Sub
        End If
    End If
    
    If txtFecDesde.Text <> "" Then
        If Not IsDate(txtFecDesde.Text) Then
            oMensajes.NoValido sFecDesde
            If Me.Visible Then txtFecDesde.SetFocus
            Exit Sub
        End If
    End If
    
    If txtFecHasta.Text <> "" Then
        If Not IsDate(txtFecHasta.Text) Then
            oMensajes.NoValido sFecHasta
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
    End If
    
    
    Screen.MousePointer = vbHourglass
        
    Set m_oInstancias = Nothing
    Set m_oInstancias = oFSGSRaiz.Generar_CInstancias
    
    If sdbcPeticionario.Text <> "" Then
        sPet = sdbcPeticionario.Columns(0).Value
    End If
    
    'Obtiene los estados
    i = 1
    ReDim vEstado(1)
    
    If chkEstado(0).Value = vbChecked Then
        ReDim Preserve vEstado(i)
        vEstado(i) = EstadoSolicitud.Aprobada
        i = i + 1
    End If
    If chkEstado(1).Value = vbChecked Then
        ReDim Preserve vEstado(i)
        vEstado(i) = EstadoSolicitud.Cerrada
        i = i + 1
    End If
    
    Select Case sdbgSolicitudes.Columns(iCol).Name
        Case "TIPO"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorTipo
        Case "ALTA"
            iOrden = OrdSolicPorfecalta
        Case "NECESIDAD"
            iOrden = OrdSolicPorFecNec
        Case "ID"
            iOrden = OrdSolicPorId
        Case "DESCR"
            iOrden = OrdSolicPorDescr
        Case "IMPORTE"
            iOrden = OrdSolicPorImporte
        Case "PET"
            iOrden = OrdSolicPorPet
        Case "ESTADO"
            iOrden = OrdSolicPorEstado
        Case "COMP"
            iOrden = OrdSolicPorComp
    End Select
    
    If sdbcTipoSolicit.Text <> "" Then
        lTipo = sdbcTipoSolicit.Columns("ID").Value
    End If
    
    If m_bRuo Then
        m_oInstancias.BuscarSolicitudes iOrden, lTipo, txtId.Text, txtDescr.Text, sPet, vEstado, txtFecDesde.Text, txtFecHasta.Text, m_bRAsig, m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , , basOptimizacion.gCodPersonaUsuario
    Else
        m_oInstancias.BuscarSolicitudes iOrden, lTipo, txtId.Text, txtDescr.Text, sPet, vEstado, txtFecDesde.Text, txtFecHasta.Text, m_bRAsig, m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, , , , , , basOptimizacion.gCodPersonaUsuario
    End If
    
    For Each oInstancia In m_oInstancias
        If IsNull(oInstancia.FecNecesidad) Then
            vFecNec = ""
        Else
            vFecNec = Format(oInstancia.FecNecesidad, "Short date")
        End If
        
        Select Case oInstancia.Estado
            Case EstadoSolicitud.Anulada
                sestado = m_sAnulada
            Case EstadoSolicitud.Aprobada
                sestado = m_sAprobada
            Case EstadoSolicitud.Cerrada
                sestado = m_sCerrada
            Case EstadoSolicitud.GenEnviada, EstadoSolicitud.GenAprobada
                sestado = m_sPendiente
            Case EstadoSolicitud.Rechazada
                sestado = m_sRechazada
        End Select
        
        sCompradorCod = ""
        sCompradorDatos = ""
        If Not oInstancia.comprador Is Nothing Then
            sCompradorCod = oInstancia.comprador.Cod
            sCompradorDatos = oInstancia.comprador.Cod & " - " & NullToStr(oInstancia.comprador.nombre) & " " & NullToStr(oInstancia.comprador.Apel)
        ElseIf Not oInstancia.Solicitud.Gestor Is Nothing Then  'si no tiene ning�n comprador asignado saca el gestor de la solicitud
            sCompradorDatos = oInstancia.Solicitud.Gestor.Cod & " - " & NullToStr(oInstancia.Solicitud.Gestor.nombre) & " " & NullToStr(oInstancia.Solicitud.Gestor.Apellidos)
            sCompradorCod = oInstancia.Solicitud.Gestor.Cod
        End If
        
        sdbgSolicitudes.AddItem NullToStr(oInstancia.Solicitud.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & Chr(m_lSeparador) & Format(oInstancia.FecAlta, "Short date") & Chr(m_lSeparador) & vFecNec & Chr(m_lSeparador) & oInstancia.Id & Chr(m_lSeparador) & oInstancia.DescrBreve & Chr(m_lSeparador) & oInstancia.importe / oInstancia.Cambio & Chr(m_lSeparador) & oInstancia.Peticionario.Cod & " - " & NullToStr(oInstancia.Peticionario.nombre) & " " & NullToStr(oInstancia.Peticionario.Apellidos) & Chr(m_lSeparador) & sestado & Chr(m_lSeparador) & sCompradorDatos & Chr(m_lSeparador) & oInstancia.Estado & Chr(m_lSeparador) & oInstancia.Peticionario.Cod & Chr(m_lSeparador) & sCompradorCod
    Next
    
    sdbgSolicitudes.MoveFirst
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub ConfigurarSeguridad()
    'Si no es el usuario administrador
    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
        
        'Restricci�n de equipo
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegSolicRestEquipo)) Is Nothing) Then
            If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
                'S�lo le afectar� la restricci�n si el usuario es un comprador.
                m_bREquipo = True
            End If
        End If
        
        'Restricci�n de asignaci�n
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegSolicRestAsig)) Is Nothing) Then
            m_bRAsig = True
        End If
        
        'Restricci�n de unidad organizativa
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegSolicRestUO)) Is Nothing) Then
            m_bRuo = True
        End If
        
    Else
        m_bRuo = False
        m_bREquipo = False
        m_bRAsig = False
    End If
   
End Sub


Private Sub sdbcPeticionario_Validate(Cancel As Boolean)
    Dim oPersonas As CPersonas
    Dim bExiste As Boolean
    Dim sbuscar As String
    
    If sdbcPeticionario.Text = "" Then
        sdbcPeticionario.Columns(0).Value = ""
        Exit Sub
    End If
    
    If sdbcPeticionario.Text = sdbcPeticionario.Columns(0).Text Then Exit Sub

    Set oPersonas = oFSGSRaiz.Generar_CPersonas
    
    If sdbcPeticionario.Columns(0).Value = "" Then
        sdbcPeticionario.Text = ""
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el peticionario
    
    Screen.MousePointer = vbHourglass
    
    If sdbcPeticionario.Columns(0).Text = "" Then
        sbuscar = "NULL"
    Else
        sbuscar = sdbcPeticionario.Columns(0).Text
    End If
    
    If m_bRuo Then
        oPersonas.CargarPeticionariosSolCompra True, sbuscar, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
    Else
        oPersonas.CargarPeticionariosSolCompra True, sbuscar
    End If
    
    bExiste = Not (oPersonas.Count = 0)
    
    If Not bExiste Then
        sdbcPeticionario.Text = ""
    Else
        m_bRespetarCombo = True
        sdbcPeticionario.Text = oPersonas.Item(1).Cod & " - " & NullToStr(oPersonas.Item(1).nombre) & " " & NullToStr(oPersonas.Item(1).Apellidos)
        sdbcPeticionario.Columns(0).Value = oPersonas.Item(1).Cod
        sdbcPeticionario.Columns(1).Value = oPersonas.Item(1).Cod & "-" & oPersonas.Item(1).nombre & " " & NullToStr(oPersonas.Item(1).Apellidos)
        
        m_bRespetarCombo = False
        
    End If
    
    Set oPersonas = Nothing

    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcTipoSolicit_Click()
    If Not sdbcTipoSolicit.DroppedDown Then
        sdbcTipoSolicit = ""
    End If
End Sub

Private Sub sdbcTipoSolicit_CloseUp()
    If sdbcTipoSolicit.Value = "..." Or sdbcTipoSolicit.Value = "" Then
        sdbcTipoSolicit.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcTipoSolicit.Text = sdbcTipoSolicit.Columns(1).Text
    m_bRespetarCombo = False
End Sub


Private Sub sdbcTipoSolicit_DropDown()
    'Cargar la combo con todos los usuarios con acceso al EP
    Dim oSolic As CSolicitud
    Dim oSolicitudes As CSolicitudes
    
    Screen.MousePointer = vbHourglass
    
    sdbcTipoSolicit.RemoveAll
    
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    If m_bRuo Then
        oSolicitudes.CargarTodasSolicitudesDeCompras , , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
    Else
        oSolicitudes.CargarTodasSolicitudesDeCompras
    End If
    

    For Each oSolic In oSolicitudes
        sdbcTipoSolicit.AddItem oSolic.Codigo & Chr(m_lSeparador) & NullToStr(oSolic.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & Chr(m_lSeparador) & oSolic.Id
    Next
        
    Set oSolicitudes = Nothing
    
    sdbcTipoSolicit.SelStart = 0
    sdbcTipoSolicit.SelLength = Len(sdbcTipoSolicit.Text)
    sdbcTipoSolicit.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcTipoSolicit_InitColumnProps()
    sdbcTipoSolicit.DataFieldList = "Column 1"
    sdbcTipoSolicit.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcTipoSolicit_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcTipoSolicit.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipoSolicit.Rows - 1
            bm = sdbcTipoSolicit.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcTipoSolicit.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcTipoSolicit.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


