
Le comunicamos que el siguiente pedido ha sido aceptado por el proveedor.
----------------------------------------------------------------------------------

Proveedor: @DENPROVE 
Pedido : @ANYO / @PEDIDO / @ORDEN
Fecha de emisi�n: @FECHA
Importe: @IMPORTE
Aprovisionador: @APROV_NOM @APROV_APE	
Receptor: @RECEP_NOM @RECEP_APE	
Empresa: @CIF @RAZON
Referencia: @REFERENCIA
Observaciones: @OBSPEDIDO

----------------------------------------------------------------------------------
Art�culos:
@LINEAS
----------------------------------------------------------------------------------


Un saludo

(Si desea recibir este e-mail en HTML modifique sus preferencias en el apartado de formatos)
