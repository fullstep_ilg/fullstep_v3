﻿Imports GSClient
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class DatosPausarSubasta
    Public ModReinicioSub As ModoReinicioSubasta
    Public MinReinicioSub As Nullable(Of Integer)
    Public FecReinicioSub As Nullable(Of Date)
    Public ModIncTSub As Nullable(Of ModoIncTiempoSubasta)
    Public MinIncTSub As Nullable(Of Integer)
End Class

Public Class OpcionesPausaSubasta
    Inherits ControlBase

    Public Event OkClick(ByVal e As DatosPausarSubasta)
    Public Event CloseClick()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        MyBase.CargarTextos(ModuleName.FRM_SUBASTAMONITOR, Me.GridPpal)

        'Valores iniciales  
        rdbManual.IsChecked = True
        rdbNo.IsChecked = True
        rdbTiempo.IsChecked = False
        rdbFecha.IsChecked = False
    End Sub

#Region " Botones "

    Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnCerrar.Click
        RaiseEvent CloseClick()
    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdCancelar.Click
        RaiseEvent CloseClick()
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdAceptar.Click
        If ComprobarDatos() Then
            Dim oDatos As New DatosPausarSubasta
            If rdbManual.IsChecked Then
                oDatos.ModReinicioSub = ModoReinicioSubasta.Manual
            Else
                oDatos.ModReinicioSub = ModoReinicioSubasta.Automatico
                If rdbTiempo.IsChecked Then
                    oDatos.MinReinicioSub = CType(xamneAutMin.Value, Integer)
                Else
                    oDatos.FecReinicioSub = xamdteAutFecha.Value
                End If
            End If
            If rdbTPausa.IsChecked Or rdbMinutos.IsChecked Then
                If rdbTPausa.IsChecked Then
                    oDatos.ModIncTSub = ModoIncTiempoSubasta.TiempoPausa
                Else
                    oDatos.ModIncTSub = ModoIncTiempoSubasta.TiempoIndicado
                    oDatos.MinIncTSub = CType(xamneIncMin.Value, Integer)
                End If                
            End If

            RaiseEvent OkClick(oDatos)
        End If
    End Sub

    Private Function ComprobarDatos() As Boolean
        ComprobarDatos = True

        If rdbAuto.IsChecked Then
            If Not rdbTiempo.IsChecked And Not rdbFecha.IsChecked Then
                ComprobarDatos = False
                Mensajes.MensajeSeleccionarTiempoFecha()
            Else
                If rdbTiempo.IsChecked Then
                    If xamneAutMin.Value Is Nothing Then
                        ComprobarDatos = False
                        Mensajes.MensajeIndicarMinutosEspera()
                    End If
                Else
                    If xamdteAutFecha.Value Is Nothing Then
                        ComprobarDatos = False
                        Mensajes.MensajeIndicarFecha()
                    End If                    
                End If
            End If
        End If

        If rdbMinutos.IsChecked AndAlso xamneIncMin.Value Is Nothing Then
            ComprobarDatos = False
            Mensajes.MensajeSeleccionarMinutosIncSubasta()
        End If        
    End Function

#End Region

#Region " Eventos controles "

    Private Sub rdbAuto_Unchecked(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles rdbAuto.Unchecked
        rdbTiempo.IsChecked = False
        rdbFecha.IsChecked = False
    End Sub

    Private Sub rdbFecha_Unchecked(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles rdbFecha.Unchecked
        xamdteAutFecha.Value = Nothing
        xamdteAutFecha.Text = Nothing
    End Sub

    Private Sub xamtbManual_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbManual.MouseUp
        rdbManual.IsChecked = True
    End Sub

    Private Sub xamtbAuto_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbAuto.MouseUp
        rdbAuto.IsChecked = True
    End Sub

    Private Sub xamtbTiempo_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbTiempo.MouseUp
        rdbTiempo.IsChecked = True
    End Sub

    Private Sub xamtbFecha_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbFecha.MouseUp
        rdbFecha.IsChecked = True
    End Sub

    Private Sub xamtbTPausa_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbTPausa.MouseUp
        rdbTPausa.IsChecked = True
    End Sub

    Private Sub xamtbNo_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbNo.MouseUp
        rdbNo.IsChecked = True
    End Sub

#End Region

End Class
