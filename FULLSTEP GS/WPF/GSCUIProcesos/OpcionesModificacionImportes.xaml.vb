﻿Imports GSClient

Public Class DatosModifImporte
    Public PrecioSalida As Boolean
    Public BajadaMinGanadora As Boolean
    Public BajadaMinProveedor As Boolean
    Public Incrementar As Boolean
    Public Porcentaje As Boolean
    Public Cantidad As Double
End Class

Public Class OpcionesModificacionImportes
    Inherits ControlBase

    Public Event OkClick(ByVal e As DatosModifImporte)
    Public Event CloseClick()

    Private _bBajMinProveVisible As Boolean
    Private _bBajMinGanVisible As Boolean

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        MyBase.CargarTextos(ModuleName.FRM_SUBASTACONF, Me.GridPpal)

        'Valores iniciales
        xamchkPrecioSalida.IsChecked = True
        xamchkBajMinGanadora.IsChecked = False
        xamchkBajMinProveedor.IsChecked = False

        rdbIncrementar.IsChecked = True
        rdbPorcentaje.IsChecked = True

        slPorcentaje.Value = 0
    End Sub

#Region " Propiedades "

    Public Property BajMinProveVisible As Boolean
        Get
            Return _bBajMinProveVisible
        End Get
        Set(ByVal value As Boolean)
            _bBajMinProveVisible = value
        End Set
    End Property

    Public Property BajMinGanVisible As Boolean
        Get
            Return _bBajMinGanVisible
        End Get
        Set(ByVal value As Boolean)
            _bBajMinGanVisible = value
        End Set
    End Property

#End Region

#Region " Eventos control "

    Private Sub OpcionesModificacionImportes_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        If Not _bBajMinProveVisible Then spBajMinProve.Visibility = Visibility.Collapsed
        If Not _bBajMinGanVisible Then spBajMinGanadora.Visibility = Visibility.Collapsed
    End Sub

#End Region

#Region " Botones "

    Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnCerrar.Click
        RaiseEvent CloseClick()
    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdCancelar.Click
        RaiseEvent CloseClick()
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdAceptar.Click
        If ComprobarDatos() Then
            Dim oDatos As New DatosModifImporte
            If Not xamchkPrecioSalida.Value Is Nothing Then oDatos.PrecioSalida = xamchkPrecioSalida.IsChecked
            If Not xamchkBajMinGanadora.Value Is Nothing Then oDatos.BajadaMinGanadora = xamchkBajMinGanadora.IsChecked
            If Not xamchkBajMinProveedor.Value Is Nothing Then oDatos.BajadaMinProveedor = xamchkBajMinProveedor.IsChecked
            oDatos.Incrementar = rdbIncrementar.IsChecked
            oDatos.Porcentaje = rdbPorcentaje.IsChecked
            If rdbPorcentaje.IsChecked Then
                oDatos.Cantidad = Math.Round(xamnePorcentaje.Value, 0)
            Else
                oDatos.Cantidad = xamneImporte.Value
            End If

            RaiseEvent OkClick(oDatos)
        End If
    End Sub

    Private Function ComprobarDatos() As Boolean
        ComprobarDatos = True

        'Comprobar que se ah elegido al menos un tipo de valor a modificar
        If ((xamchkPrecioSalida.Value Is Nothing OrElse Not xamchkPrecioSalida.IsChecked) And _
            (xamchkBajMinGanadora.Value Is Nothing OrElse Not xamchkBajMinGanadora.IsChecked) And _
            (xamchkBajMinProveedor.Value Is Nothing OrElse Not xamchkBajMinProveedor.IsChecked)) Then
            ComprobarDatos = False
            Mensajes.MensajeSeleccionarValorModificar()
        ElseIf Not (rdbIncrementar.IsChecked Or rdbDecrementar.IsChecked) Then
            ComprobarDatos = False
            Mensajes.MensajeSeleccionarIncrementarDecrementar()
        ElseIf Not (rdbPorcentaje.IsChecked Or rdbImorte.IsChecked) Then
            ComprobarDatos = False
            Mensajes.MensajeSeleccionarPorcentajeImporte()
        ElseIf rdbPorcentaje.IsChecked And xamnePorcentaje.Text.Length = 0 Then
            ComprobarDatos = False
            Mensajes.MensajeIndicarPorcentaje()
        ElseIf rdbImorte.IsChecked And xamneImporte.Text.Length = 0 Then
            ComprobarDatos = False
            Mensajes.MensajeIndicarImporte()
        End If

    End Function

#End Region

#Region " Eventos controles "

    Private Sub xamtbIncrementar_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbIncrementar.MouseDown
        rdbIncrementar.IsChecked = (e.LeftButton = System.Windows.Input.MouseButtonState.Pressed)        
    End Sub

    Private Sub xamtbDecrementar_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbDecrementar.MouseDown
        rdbDecrementar.IsChecked = (e.LeftButton = System.Windows.Input.MouseButtonState.Pressed)        
    End Sub

    Private Sub xamtbEnPorcentaje_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbEnPorcentaje.MouseDown
        rdbPorcentaje.IsChecked = (e.LeftButton = System.Windows.Input.MouseButtonState.Pressed)        
    End Sub

    Private Sub xamtbEnImporteFijo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbEnImporteFijo.MouseDown
        rdbImorte.IsChecked = (e.LeftButton = System.Windows.Input.MouseButtonState.Pressed)        
    End Sub

    Private Sub txtbPrecioSalida_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles txtbPrecioSalida.MouseDown
        If e.LeftButton = System.Windows.Input.MouseButtonState.Pressed Then
            xamchkPrecioSalida.IsChecked = Not xamchkPrecioSalida.IsChecked
        End If
    End Sub

    Private Sub txtbBajMinGanadora_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles txtbBajMinGanadora.MouseDown
        If e.LeftButton = System.Windows.Input.MouseButtonState.Pressed Then
            xamchkBajMinGanadora.IsChecked = Not xamchkBajMinGanadora.IsChecked
        End If
    End Sub

    Private Sub txtbBajMinProveedor_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles txtbBajMinProveedor.MouseDown
        If e.LeftButton = System.Windows.Input.MouseButtonState.Pressed Then
            xamchkBajMinProveedor.IsChecked = Not xamchkBajMinProveedor.IsChecked
        End If
    End Sub

#End Region
        
End Class
