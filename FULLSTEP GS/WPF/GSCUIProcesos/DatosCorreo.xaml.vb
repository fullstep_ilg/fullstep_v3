﻿Imports GSClient
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel

Public Class DatosCorreo
    Inherits ControlBase

    Private _oEMail As GSServerModel.EMail

    Public Sub New(ByVal oEmail As GSServerModel.EMail)
        _oEMail = oEmail

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        MyBase.CargarTextos(ModuleName.FRM_MENSAJECORREO, Me.GridPpal)
    End Sub

#Region " Propiedades "

    Public Property EMail As GSServerModel.EMail
        Get
            Return _oEMail
        End Get
        Set(ByVal value As GSServerModel.EMail)
            _oEMail = value
        End Set
    End Property

#End Region

    Private Sub DatosCorreo_Initialized(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Initialized
        'Construir el Para
        Dim sPara As String = String.Empty
        If Not _oEMail.Para Is Nothing AndAlso _oEMail.Para.Count > 0 Then
            For Each oContacto As Contacto In _oEMail.Para
                If NothingToStr(oContacto.Nombre) <> String.Empty Then sPara &= oContacto.Nombre & " "
                If NothingToStr(oContacto.Apellidos) <> String.Empty Then sPara &= oContacto.Apellidos & " "
                If sPara <> String.Empty Then
                    sPara &= "(" & oContacto.EMail & ");"
                Else
                    sPara = oContacto.EMail & ";"
                End If
            Next

            sPara = sPara.Substring(0, sPara.Length - 1)
        End If
        xamteTo.Text = sPara

        xamteSubject.Text = _oEMail.Asunto

        'Construir Adjuntos
        Dim sAdjuntos As String = String.Empty
        If Not _oEMail.Adjuntos Is Nothing AndAlso _oEMail.Adjuntos.Count > 0 Then
            For Each oAdj As File In _oEMail.Adjuntos
                If NothingToStr(oAdj.NombreArchivo) <> String.Empty Then sAdjuntos &= oAdj.NombreArchivo
                If oAdj.Tamaño <> 0 Then sAdjuntos &= " (" & oAdj.Tamaño.ToString & ")"
                sAdjuntos &= ", "
            Next

            sAdjuntos = sAdjuntos.Substring(0, sAdjuntos.Length - 2)
        End If
        xamteAdjuntos.Text = sAdjuntos

        If _oEMail.TipoEmail = TipoEmail.Texto Then
            ''rtbContenido.Visibility = Visibility.Visible
            ''bwbContenido.Visibility = Visibility.Collapsed

            ' ''Create a FlowDocument
            ''Dim oFlowDoc As New FlowDocument()

            ' ''Create a paragraph with text
            ''Dim oPara As New Paragraph
            ''oPara.Inlines.Add(New Run(_oEMail.Contenido))

            ' ''Add the paragraph to blocks of paragraph
            ''oFlowDoc.Blocks.Add(oPara)

            ''rtbContenido.Document = oFlowDoc
            ' ''rtbContenido.IsReadOnly = True

            xamtbContenido.Visibility = Visibility.Visible
            bwbContenido.Visibility = Visibility.Collapsed

            xamtbContenido.Text = _oEMail.Contenido            

            'xamtbContenido.isreadonly = True
        Else
            bwbContenido.Visibility = Visibility.Visible
            ''rtbContenido.Visibility = Visibility.Collapsed
            xamtbContenido.Visibility = Visibility.Collapsed

            Dim ms As New System.IO.MemoryStream(System.Text.ASCIIEncoding.Default.GetBytes(_oEMail.Contenido))
            wbContenido.NavigateToStream(ms)
        End If
    End Sub

    '<summary>Actualiza los datos del email</summary>    

    Public Sub ActualizarEMail()
        _oEMail.Asunto = xamteSubject.Text
        If _oEMail.TipoEmail = TipoEmail.Texto Then
            ''_oEMail.Contenido = CType(CType(CType(rtbContenido.Document, FlowDocument).Blocks(0), Paragraph).Inlines(0), Run).Text
            _oEMail.Contenido = xamtbcontenido.Text
        Else
            'Todavía no se ha puesto un editor HTML
        End If

        'Actualizar destinartarios
        Dim oContactos As New Contactos
        Dim sPara As String = xamteTo.Text
        If sPara.Length > 0 Then
            Dim arPara() As String = sPara.Split(";")
            For i As Integer = 0 To arPara.Length - 1
                Dim sEMail As String = arPara(i)
                If sEMail.Contains("(") Then
                    sEMail = sEMail.Substring(sEMail.IndexOf("(") + 1)
                End If
                If sEMail.Contains(")") Then
                    sEMail = sEMail.Substring(0, sEMail.IndexOf(")"))
                End If

                Dim oContacto As New Contacto
                oContacto.EMail = sEMail

                oContactos.Add(oContacto)
            Next
        End If
        _oEMail.Para = oContactos
    End Sub

End Class
