﻿Imports System.Windows.Interop
Imports System.Configuration

Class Application
    Inherits System.Windows.Application
    ' Application events, such as Startup(), Exit(), and DispatcherUnhandledException
    ' can be handled in this file

    Private Sub Application_Startup(ByVal sender As Object, ByVal e As System.Windows.StartupEventArgs) Handles Me.Startup
        Dim theme As ResourceDictionary
        Dim stheme As String
        stheme = "themes\"
        Dim sVersion As String
        sVersion = ConfigurationManager.AppSettings.Get("Version")
        stheme = "themes\" & sVersion & ".xaml"
        Dim themeUri As Uri
        themeUri = New Uri(stheme, UriKind.Relative)
        Try
            theme = CType(Application.LoadComponent(themeUri), ResourceDictionary)
        Catch Ex As Exception
            stheme = "themes\Generic.xaml"
            themeUri = New Uri(stheme, UriKind.Relative)
            theme = CType(Application.LoadComponent(themeUri), ResourceDictionary)

        End Try

        Resources.MergedDictionaries.Add(theme)
    End Sub
End Class
