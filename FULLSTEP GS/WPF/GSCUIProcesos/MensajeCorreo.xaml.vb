﻿Imports GSClient
Imports Fullstep.FSNLibrary

Public Class MensajeCorreo
    Inherits ControlBase

    Public Event EnviarCorreoClick(ByVal oEmails As GSServerModel.EMails)
    Public Event CancelarEnvioCorreoClick()

    Private _oEMails As GSServerModel.EMails
    Private _colCorreos As Collection

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.       
        MyBase.CargarTextos(ModuleName.FRM_MENSAJECORREO, Me.GridPpal)
        _colCorreos = New Collection
    End Sub

#Region "Propiedades"

    Public Property EMails As GSServerModel.EMails
        Get
            Return _oEMails
        End Get
        Set(ByVal value As GSServerModel.EMails)
            _oEMails = value
        End Set
    End Property

#End Region

    Private Sub btnEnviar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnEnviar.Click
        'Actualizar datos correos
        For Each oDatosCorreo As DatosCorreo In _colCorreos
            oDatosCorreo.ActualizarEMail()
        Next

        RaiseEvent EnviarCorreoClick(_oEMails)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnCancelar.Click
        RaiseEvent CancelarEnvioCorreoClick()
    End Sub

    Private Sub MensajeCorreo_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        If Not _oEMails Is Nothing Then
            CrearTabsCorreos()
        End If
    End Sub

    '<summary>Crea una pestaña para cada uno de los correos</summary>       
    '<remarks>Llamada desde: MensajeCorreo_Loaded  </remarks>

    Private Sub CrearTabsCorreos()        
        If Not _oEMails Is Nothing AndAlso _oEMails.Count > 0 Then
            xamtcCorreos.Items.Clear()

            For Each oEMail As GSServerModel.EMail In _oEMails
                Dim oTabItem As New Infragistics.Windows.Controls.TabItemEx
                Dim oDatosCorreo As New DatosCorreo(oEMail)
                _colCorreos.Add(oDatosCorreo)

                Dim oPanel As New DockPanel                
                oPanel.LastChildFill = True
                oPanel.Children.Add(oDatosCorreo)
                oTabItem.Content = oPanel
                oTabItem.Header = oEMail.NombreEMail
                xamtcCorreos.Items.Add(oTabItem)
            Next

            xamtcCorreos.SelectedItem = xamtcCorreos.Items(0)
        End If
    End Sub

    Protected Overrides Sub Finalize()
        _colCorreos = Nothing
        _oEMails = Nothing

        MyBase.Finalize()
    End Sub
End Class
