﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Media
Imports System.Windows.Media.Animation
Imports System.Windows.Navigation
Imports GSClient
Imports System.Windows.Threading

Partial Public Class EstadoProveedor
    Public Event CloseClick(ByVal oEstadoProve As EstadoProveedor)

    Private _oCloseAnimation As Storyboard
    Private WithEvents _oTimer As DispatcherTimer

#Region " Constructores "

    Public Sub New()
        MyBase.New()

        Me.InitializeComponent()

        ' Insert code required on object creation below this point.
        MyBase.CargarTextos(ModuleName.FRM_SUBASTAMONITOR, Me.LayoutRoot)

        Dim oAnim As Storyboard = Me.FindResource("StoryboardSalidaVentanaNotificacion")
        _oCloseAnimation = oAnim.Clone
        Storyboard.SetTarget(_oCloseAnimation, Me)
        AddHandler _oCloseAnimation.Completed, AddressOf StoryboardSalidaVentana_Completed

        'Set timer
        _oTimer = New DispatcherTimer
        _oTimer.Interval = New TimeSpan(0, 0, 8)
        _oTimer.Start()
    End Sub

    Public Sub New(ByVal sProveedor As String, ByVal sDenominacion As String, ByVal bConectado As Boolean)
        Me.New()

        tbProveDen.Text = sDenominacion
        Dim oTextos As New GSClient.CTextos
        If bConectado Then
            Flecha.Style = Me.FindResource("EstiloFlechaVerde")
            tbMsg.Text = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 67)
        Else
            Flecha.Style = Me.FindResource("EstiloFlechaRoja")
            tbMsg.Text = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 66)
        End If
    End Sub

#End Region

    Public Sub StoryboardSalidaVentana_Completed()
        RaiseEvent CloseClick(Me)
    End Sub

    Private Sub Cerrar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Cerrar.Click
        _oCloseAnimation.Begin()
    End Sub

    Private Sub _oTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _oTimer.Tick
        _oCloseAnimation.Begin()
    End Sub

End Class
