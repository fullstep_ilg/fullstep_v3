﻿Imports System.Windows.Interop

Partial Public Class PInicio
    Inherits GSClient.PageBase

    Public Sub New()
        MyBase.New(BrowserInteropHelper.Source.Query.ToString())

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        Dim sQueryString As String = BrowserInteropHelper.Source.Query.ToString()
        sQueryString = sQueryString.Trim("?")
        Dim arParam() As String = sQueryString.Split("&")

        Select Case m_sUCToOpen

            Case "SubastaConf"                
                If arParam.Length = 10 Then
                    Dim iAnyo As Integer = CType(arParam(3).Substring(arParam(3).IndexOf("=") + 1, arParam(3).Length - arParam(3).IndexOf("=") - 1), Integer)
                    Dim iCod As Integer = CType(arParam(4).Substring(arParam(4).IndexOf("=") + 1, arParam(4).Length - arParam(4).IndexOf("=") - 1), Integer)
                    Dim sGMN1 As String = arParam(5).Substring(arParam(5).IndexOf("=") + 1, arParam(5).Length - arParam(5).IndexOf("=") - 1)
                    Dim dtFecApe As Date = Date.MinValue
                    Dim strFecApe As String = arParam(6).Substring(arParam(6).IndexOf("=") + 1, arParam(6).Length - arParam(6).IndexOf("=") - 1)
                    If strFecApe.Length > 0 Then dtFecApe = CType(strFecApe, Date)
                    Dim dtFecPres As Date = Date.MinValue
                    Dim strFecPres As String = arParam(7).Substring(arParam(7).IndexOf("=") + 1, arParam(7).Length - arParam(7).IndexOf("=") - 1)
                    If strFecPres.Length > 0 Then dtFecPres = CType(strFecPres, Date)
                    Dim bPermAdjDir As Boolean = CType(arParam(8).Substring(arParam(8).IndexOf("=") + 1, arParam(8).Length - arParam(8).IndexOf("=") - 1), Boolean)
                    Dim bSoloLectura As Boolean = CType(arParam(9).Substring(arParam(9).IndexOf("=") + 1, arParam(9).Length - arParam(9).IndexOf("=") - 1), Boolean)
                    Dim ucSubastaConf As New SubastaConf(iAnyo, iCod, sGMN1, dtFecApe, dtFecPres, bPermAdjDir, bSoloLectura)
                    ucSubastaConf.ParentPage = Me
                    Me.GridAplicacion.Children.Add(ucSubastaConf)
                End If
            Case "SubastaMonitor"
                If arParam.Length = 6 Then
                    Dim iAnyo As Integer = CType(arParam(3).Substring(arParam(3).IndexOf("=") + 1, arParam(3).Length - arParam(3).IndexOf("=") - 1), Integer)
                    Dim iCod As Integer = CType(arParam(4).Substring(arParam(4).IndexOf("=") + 1, arParam(4).Length - arParam(4).IndexOf("=") - 1), Integer)
                    Dim sGMN1 As String = arParam(5).Substring(arParam(5).IndexOf("=") + 1, arParam(5).Length - arParam(5).IndexOf("=") - 1)
                    Dim ucSubastaConf As New SubastaMonitor(iAnyo, iCod, sGMN1)
                    ucSubastaConf.ParentPage = Me
                    Me.GridAplicacion.Children.Add(ucSubastaConf)
                End If
            Case Else
                Dim ucErrorLogin As New GSClient.ErrorLogin
                Me.GridAplicacion.Children.Add(ucErrorLogin)
        End Select
    End Sub

    Public Overrides Sub RemoveChild(ByVal oChild As System.Windows.Controls.UserControl)
        Me.GridAplicacion.Children.Remove(oChild)
    End Sub

End Class
