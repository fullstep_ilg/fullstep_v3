﻿Imports GSClient
Imports Infragistics.Windows.Editors
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.Collections.ObjectModel
Imports Infragistics.Windows.DockManager
Imports System.Windows.Threading
Imports Infragistics.Windows.DataPresenter
Imports Fullstep.FSNLibrary
Imports Infragistics.Windows.Reporting
Imports Infragistics.Windows.DataPresenter.ExcelExporter
Imports Infragistics.Excel
Imports System.ComponentModel
Imports System.Data

Public Class SubastaMonitor
    Inherits ControlBase

    Private _iAnyo As Integer
    Private _iCod As Integer
    Private _sGMN1 As String
    Private _oEMails As GSServerModel.EMails
    Private WithEvents _oProveResView As ProceProvesResumenSubastaView    

    Private WithEvents oProveExporter As DataPresenterExcelExporter
    Private WithEvents oEveExporter As DataPresenterExcelExporter
    Private WithEvents _oTimer As DispatcherTimer
    Private _iNumSeg As Integer

    Private _FechaServidor As Date
    Private _FechaCliente As Date

#Region " Constructor "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.  
        _iNumSeg = 0

        _FechaServidor = ModulePublic.UTCdeBD
        _FechaCliente = Now.UtcNow
    End Sub

    Public Sub New(ByVal iAnyo As Integer, ByVal iCod As Integer, ByVal sGMN1 As String)
        Me.New()

        _iAnyo = iAnyo
        _iCod = iCod
        _sGMN1 = sGMN1

        'Cargar textos
        MyBase.CargarTextos(ModuleName.FRM_SUBASTAMONITOR, Me.Grid)
        Dim oTextos As New GSClient.CTextos
        cpProceso.Header = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 13)
        cpEventos.Header = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 62)

        GetData()
        MakeDataBinding()
        CargarGraficoProceso()
        CrearPestañasGrupos()
        CargarCombos()
        SetTimers()
        ConfigurarBotonesAcciones()

        'Asignar vista por defecto
        xamceTipoVista.Value = 0
    End Sub

#End Region

#Region " Eventos SubastaMonitor "

    'Private Sub SubastaMonitor_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded
    '    'Asignar vista por defecto
    '    xamceTipoVista.Value = 0
    'End Sub

#End Region

#Region " Métodos privados "

    Private Sub GetData(Optional ByVal bRefresh As Boolean = False, Optional ByVal bTimer As Boolean = False)
        Dim iGrupo As Nullable(Of Integer) = Nothing
        If Not xamceTipoVista.Value Is Nothing AndAlso xamceTipoVista.Value > 0 Then iGrupo = xamceTipoVista.Value

        'Obtener idioma
        Dim sIdioma As String = ModulePublic.ParametrosInstalacion.gsIdioma
        If Not ModulePublic.oUsu.Idioma Is Nothing Then sIdioma = ModulePublic.oUsu.Idioma

        'Obtener objetos de datos        
        Dim oProcRule As New GSClient.ProcesosRule
        Dim oData As GSServerModel.SubastaMonitorData = oProcRule.DevolverDatosSubastaMonitor(_iAnyo, _iCod, _sGMN1, sIdioma, iGrupo, xamceElementos.Value)

        Me.AddBindingObject(oData.Proceso, "Proceso")
        Me.AddBindingObject(oData.Subasta, "Subasta")
        If Not bRefresh Then
            _oProveResView = New ProceProvesResumenSubastaView(oData.ProceProves)
            Me.AddBindingObject(_oProveResView, "Proveedores")

            Me.AddBindingObject(New EventosView(oData.Eventos), "Eventos")
        Else
            Dim oProceProveRes As ProceProvesResumenSubastaView = Me.GetBindingObject("Proveedores")
            oProceProveRes.Refresh(oData.ProceProves, bTimer)
            If Not xamdgProveedores.Records Is Nothing Then xamdgProveedores.Records.RefreshSort()

            Dim oEventosView As EventosView = Me.GetBindingObject("Eventos")
            oEventosView.Refresh(oData.Eventos)
        End If
        Me.AddBindingObject(oData.SubastaPujas, "SubastaPujas")
        Me.AddBindingObject(oData.Moneda, "Moneda")
    End Sub

    '' <summary>Realiza el binding de los controles a los objetos de datos necesarios</summary>    
    '' <remarks>Llamada desde: New  </remarks>

    Private Sub MakeDataBinding()
        Me.MakeBinding(Me.Grid)

        'Controles sin binding automático         
        'TimeZone        
        Dim sTimeZone As String = TimeZoneInfo.FindSystemTimeZoneById(ModulePublic.oUsu.TimeZone).DisplayName
        xamteTimeZone.Text = sTimeZone.Substring(0, sTimeZone.IndexOf(")") + 1)
        'Datos proceso
        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        txtbDescProceso.Text = _iAnyo & "/" & _sGMN1 & "/" & _iCod & " " & oProceso.Denominacion        
        'Modo Subasta
        Dim oSubasta As GSServerModel.ProcesoSubasta = Me.GetBindingObject("Subasta")
        'Tiempo restante  
        SetRemainingTime(oProceso, oSubasta)

        Dim oTextos As New GSClient.CTextos
        Select Case oSubasta.Modo
            Case ModoSubasta.ProcesoCompleto
                txtbDescModoSubasta.Text = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 24)
            Case ModoSubasta.Grupo
                txtbDescModoSubasta.Text = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 25)
            Case ModoSubasta.Item
                txtbDescModoSubasta.Text = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 26)
        End Select
        'Estado de subasta
        Select Case oSubasta.Estado
            Case EstadoSubasta.EnProceso
                txtbDescEstadoSubasta.Text = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 29)
            Case EstadoSubasta.Finalizada
                txtbDescEstadoSubasta.Text = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 30)
            Case EstadoSubasta.NoComenzada
                txtbDescEstadoSubasta.Text = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 31)
            Case EstadoSubasta.Parada
                txtbDescEstadoSubasta.Text = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 32)
            Case EstadoSubasta.Pausada
                txtbDescEstadoSubasta.Text = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 33)
        End Select
    End Sub

    '' <summary>Establece el tiempo restante de subasta a mostrar en pantalla</summary>    
    '' <remarks>Llamada desde: _oTimer_Tick y MakeDataBinding </remarks>

    Private Sub SetRemainingTime(ByVal oProceso As GSServerModel.Proceso, ByVal oSubasta As GSServerModel.ProcesoSubasta)
        Dim ts As TimeSpan

        Select Case oSubasta.Estado
            Case EstadoSubasta.Parada
                ts = TimeSpan.FromTicks(0)
            Case Else
                'Dim oTZInfo As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(ModulePublic.oUsu.TimeZone)
                'Dim dtTZNow As Date = TimeZoneInfo.ConvertTimeFromUtc(Date.UtcNow, oTZInfo)

                'Obtener la fecha y hora actual del servidor de bbdd
                Dim dtTZNow As Date = Now_FechaHoraServidor()

                Dim dtFechaLimite As Date = oProceso.FechaLimiteOfertas

                If oSubasta.Estado = EstadoSubasta.Pausada Then
                    'Obtener los datos de la pausa
                    Dim oDatosPausa As GSServerModel.SubastaEvento = ObtenerDatosPausa()
                    If Not oDatosPausa.ModoIncTiempoSubasta Is Nothing Then
                        If CType(oDatosPausa.ModoIncTiempoSubasta, ModoIncTiempoSubasta) = ModoIncTiempoSubasta.TiempoIndicado Then
                            dtFechaLimite = CType(oProceso.FechaLimiteOfertas, Date).AddMinutes(oDatosPausa.MinIncTiempoSubasta)
                        Else
                            'Parar el reloj (sacar siempre el mismo tiempo)
                            dtTZNow = oDatosPausa.Fecha
                        End If
                    End If
                End If

                'Comprobar si la subasta ha empezado
                If Date.Compare(dtTZNow, oProceso.FechaInicioSubasta) > 0 Then
                    ts = dtFechaLimite - dtTZNow
                Else
                    ts = dtFechaLimite - oProceso.FechaInicioSubasta
                End If
        End Select

        If ts.Ticks < 0 Then ts = TimeSpan.FromTicks(0)
        xamteTiempoRestante.Text = ts.Days & " " & ts.Hours.ToString("00") & ":" & ts.Minutes.ToString("00") & ":" & ts.Seconds.ToString("00")
    End Sub

    '' <summary>Devuelve los datos de la última pausa de la subasta</summary>    
    '' <returns>Objeto de tipo DatosPausarSubasta con los datos de la última pausa</returns>
    '' <remarks>Llamada desde: SetRemainingTime </remarks>

    Private Function ObtenerDatosPausa() As GSServerModel.SubastaEvento
        Dim oEventos As EventosView = Me.GetBindingObject("Eventos")
        Dim oColView As New ListCollectionView(oEventos)
        oColView.Filter = New Predicate(Of Object)(AddressOf FiltroEventosPausa)
        oColView.SortDescriptions.Add(New SortDescription("Fecha", ListSortDirection.Descending))
        If oColView.Count > 0 Then
            Dim oDatosPausa As New GSServerModel.SubastaEvento
            oDatosPausa.Fecha = CType(oColView.GetItemAt(0), EventoView).Fecha
            oDatosPausa.FechaReinicioSubasta = CType(oColView.GetItemAt(0), EventoView).FechaReinicioSubasta
            oDatosPausa.MinIncTiempoSubasta = CType(oColView.GetItemAt(0), EventoView).MinIncTiempoSubasta
            oDatosPausa.MinReinicioSubasta = CType(oColView.GetItemAt(0), EventoView).MinReinicioSubasta
            oDatosPausa.ModoIncTiempoSubasta = CType(oColView.GetItemAt(0), EventoView).ModoIncTiempoSubasta
            oDatosPausa.ModoReinicioSubasta = CType(oColView.GetItemAt(0), EventoView).ModoReinicioSubasta

            Return oDatosPausa
        End If
    End Function

    Private Function FiltroEventosPausa(ByVal t As Object) As Boolean
        Dim oEventoView As EventoView = CType(t, EventoView)
        Return oEventoView.Accion = Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Pausar
    End Function

    '' <summary>Configura las columnas visibles del grid en función de los datos a mostrar</summary>    
    '' <remarks>Llamada desde: Refresh </remarks>

    Private Sub ConfigurarGrid()
        Dim bColItemsVis As Visibility = Windows.Visibility.Collapsed
        If Not xamceElementos.Value Is Nothing Then bColItemsVis = Windows.Visibility.Visible

        xamdgProveedores.FieldLayouts(0).Fields("Precio").Visibility = bColItemsVis
        xamdgProveedores.FieldLayouts(0).Fields("Cantidad").Visibility = bColItemsVis
    End Sub

    '' <summary>Carga el gráfico del proceso</summary>    
    '' <remarks>Llamada desde: New y _oTimer_Tick </remarks>

    Private Sub CargarGraficoProceso()
        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        Dim oPujas As GSServerModel.ProcesoSubastaPujas = Me.GetBindingObject("SubastaPujas")

        'Pujas proceso  
        GrafProceso.BorrarGrafico()
        GrafProceso.FechaMin = oProceso.FechaInicioSubasta
        GrafProceso.FechaMax = oProceso.FechaLimiteOfertas
        GrafProceso.DataSource = oPujas
        GrafProceso.CargarGrafico(CType(Me.GetBindingObject("Proveedores"), ProceProvesResumenSubastaView).CheckedProves)
    End Sub

    '' <summary>Carga el gráfico del item</summary>    
    '' <remarks>Llamada desde: New y _oTimer_Tick </remarks>

    Private Sub CargarGraficoItem(ByVal ID As Integer)
        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        Dim oSubastaPujas As GSServerModel.ProcesoSubastaPujas = Me.GetBindingObject("SubastaPujas")

        Dim oItem As GSServerModel.Item = oProceso.Grupos.Find(oProceso.Anyo, oProceso.GMN1, oProceso.Codigo, xamceTipoVista.Value).Items.Find(oProceso.Anyo, oProceso.GMN1, oProceso.Codigo, ID)
        cpItem.Header = oItem.Articulo & " - " & oItem.Descripcion
        cpItem.Visibility = Visibility.Visible

        GrafItem.BorrarGrafico()
        GrafItem.Header = oItem.Articulo & " - " & oItem.Descripcion
        GrafItem.FechaMin = oProceso.FechaInicioSubasta
        GrafItem.FechaMax = oProceso.FechaLimiteOfertas
        If Not oSubastaPujas.GruposPujas Is Nothing AndAlso oSubastaPujas.GruposPujas.Count > 0 AndAlso _
            Not oSubastaPujas.GruposPujas.Item(xamceTipoVista.Value) Is Nothing AndAlso _
            Not oSubastaPujas.GruposPujas.Item(xamceTipoVista.Value).ItemsPujas Is Nothing AndAlso oSubastaPujas.GruposPujas.Item(xamceTipoVista.Value).ItemsPujas.Count > 0 Then
            GrafItem.DataSource = oSubastaPujas.GruposPujas.Item(xamceTipoVista.Value).ItemsPujas.Item(ID)
        Else
            GrafItem.DataSource = Nothing
        End If
        GrafItem.CargarGrafico(CType(Me.GetBindingObject("Proveedores"), ProceProvesResumenSubastaView).CheckedProves)
    End Sub

    '' <summary>Crea las pestañas con los gráficos para cada grupo</summary>    
    '' <remarks>Llamada desde: New  </remarks>

    Private Sub CrearPestañasGrupos()
        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        If Not oProceso.Grupos Is Nothing AndAlso oProceso.Grupos.Count > 0 Then
            Dim oSubastaPujas As GSServerModel.ProcesoSubastaPujas = Me.GetBindingObject("SubastaPujas")

            Dim i As Integer = 0
            For Each oGrupo As GSServerModel.ProcesoGrupo In oProceso.Grupos
                Dim oCP As New ContentPane
                oCP.AllowClose = False
                oCP.IsPinned = True
                oCP.Tag = oGrupo.ID
                oCP.Header = oGrupo.Cod & " - " & oGrupo.Denominacion

                Dim oGraf As New GraficoSubastaMonitor
                'Pujas de grupos
                oGraf.Header = oGrupo.Cod & " - " & oGrupo.Denominacion
                oGraf.FechaMin = oProceso.FechaInicioSubasta
                oGraf.FechaMax = oProceso.FechaLimiteOfertas
                If Not oSubastaPujas.GruposPujas Is Nothing AndAlso oSubastaPujas.GruposPujas.Count > 0 Then
                    oGraf.DataSource = oSubastaPujas.GruposPujas.Item(oGrupo.ID)
                Else
                    oGraf.DataSource = Nothing
                End If
                oGraf.CargarGrafico(CType(Me.GetBindingObject("Proveedores"), ProceProvesResumenSubastaView).CheckedProves)                
                oCP.Content = oGraf

                tgpGrupoItem.Items.Insert(i, oCP)
                i += 1
            Next
        End If
    End Sub

    '' <summary>Refresca los gráficos de las pestañas existentes</summary>     
    '' <remarks>Llamada desde: New  </remarks>

    Private Sub RefrescarPestañasGrupos()
        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        Dim oSubastaPujas As GSServerModel.ProcesoSubastaPujas = Me.GetBindingObject("SubastaPujas")

        For Each oCP As ContentPane In tgpGrupoItem.Items
            Dim oGraf As GraficoSubastaMonitor = oCP.Content

            If Not oCP.Name Is Nothing AndAlso oCP.Name = "cpItem" Then
                'Pujas de items
                If xamceTipoVista.Value > 0 AndAlso Not xamceElementos.Value Is Nothing Then
                    CargarGraficoItem(xamceElementos.Value)
                End If
            Else
                'Pujas de grupos                
                If Not oSubastaPujas.GruposPujas Is Nothing AndAlso oSubastaPujas.GruposPujas.Count > 0 Then
                    oGraf.BorrarGrafico()
                    oGraf.DataSource = oSubastaPujas.GruposPujas.Item(oCP.Tag)
                    oGraf.FechaMin = oProceso.FechaInicioSubasta
                    oGraf.FechaMax = oProceso.FechaLimiteOfertas
                    oGraf.CargarGrafico(CType(Me.GetBindingObject("Proveedores"), ProceProvesResumenSubastaView).CheckedProves)
                End If
            End If
        Next
    End Sub

    '' <summary>Carga los combos que muestran datos de enumerados</summary>     
    '' <remarks>Llamada desde: New  </remarks>

    Private Sub CargarCombos()
        Dim oTextos As New GSClient.CTextos

        'Tipo vista
        Dim oItemProv As New ComboBoxItemsProvider
        oItemProv = New ComboBoxItemsProvider
        oItemProv.Items.Add(New ComboBoxDataItem(0, oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 9)))

        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        If Not oProceso.Grupos Is Nothing AndAlso oProceso.Grupos.Count > 0 Then
            For Each oGrupo As GSServerModel.ProcesoGrupo In oProceso.Grupos
                oItemProv.Items.Add(New ComboBoxDataItem(oGrupo.ID, oGrupo.Cod & " - " & oGrupo.Denominacion))
            Next
        End If

        xamceTipoVista.ItemsProvider = oItemProv

        'Combos grid eventos
        Dim oAccion As New ComboBoxItemsProvider
        oAccion.Items.Add(New ComboBoxDataItem(Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Extender, oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 5)))
        oAccion.Items.Add(New ComboBoxDataItem(Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Parar, oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 4)))
        oAccion.Items.Add(New ComboBoxDataItem(Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Pausar, oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 2)))
        oAccion.Items.Add(New ComboBoxDataItem(Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Reiniciar, oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 3)))
        Me.Resources.Add("AccionItemsProvider", oAccion)

        Dim oModReiniSub As New ComboBoxItemsProvider
        oModReiniSub.Items.Add(New ComboBoxDataItem(Fullstep.FSNLibrary.TiposDeDatos.ModoReinicioSubasta.Automatico, oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 38)))
        oModReiniSub.Items.Add(New ComboBoxDataItem(Fullstep.FSNLibrary.TiposDeDatos.ModoReinicioSubasta.Manual, oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 37)))
        Me.Resources.Add("ModReiniSubItemsProvider", oModReiniSub)

        Dim oModIncTSub As New ComboBoxItemsProvider
        oModIncTSub.Items.Add(New ComboBoxDataItem(Fullstep.FSNLibrary.TiposDeDatos.ModoIncTiempoSubasta.TiempoIndicado, oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 61)))
        oModIncTSub.Items.Add(New ComboBoxDataItem(Fullstep.FSNLibrary.TiposDeDatos.ModoIncTiempoSubasta.TiempoPausa, oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 42)))
        Me.Resources.Add("ModIncTSubItemsProvider", oModIncTSub)
    End Sub

    '' <summary>Establece el timer según el cual se refrescarán los datos de la pantalla y el timer de reloj</summary>     
    '' <remarks>Llamada desde: New  </remarks>

    Private Sub SetTimers()
        _oTimer = New DispatcherTimer
        _oTimer.Interval = New TimeSpan(0, 0, 1)
        _oTimer.Start()
    End Sub

    '<summary>Presenta en pantalla el email a proveedores</summary>   
    '<param name="oEmails">Objeto de tipo GSServerModel.EMails con los datos de los correos a mostrar</param>    
    '<remarks>Llamada desde: cmdAceptar_Click  </remarks>

    Private Sub MostrarCorreos(ByVal oEmails As GSServerModel.EMails)
        'deshabilitar el resto de controles            
        'For Each item As UIElement In Grid.Children
        '    item.IsEnabled = False
        'Next
        PantallaDeshabilitada.Visibility = Visibility.Visible

        'Añadir el nuevo control y controladores de eventos
        Dim oCorreo As New MensajeCorreo
        oCorreo.EMails = oEmails
        AddHandler oCorreo.EnviarCorreoClick, AddressOf EnviarCorreo_Click
        AddHandler oCorreo.CancelarEnvioCorreoClick, AddressOf CancelarEnvioCorreo_Click

        pnCorreos.Children.Add(oCorreo)
        pnCorreos.Visibility = Visibility.Visible
        pnCorreos.IsEnabled = True
    End Sub

    '<summary>Acciones al aceptar el envío de correos</summary>    

    Private Sub EnviarCorreo_Click(ByVal oEmails As GSServerModel.EMails)
        Me.Cursor = Cursors.Wait

        ComunicarProveedores(oEmails)
        OcultarPanelEMail()

        Me.Cursor = Cursors.Arrow
    End Sub

    '<summary>Acciones al cancelar el evío de correos</summary>    

    Private Sub CancelarEnvioCorreo_Click()
        OcultarPanelEMail()

        Me.Cursor = Cursors.Arrow
    End Sub

    '<summary>Oculta el panel de los correos</summary> 

    Private Sub OcultarPanelEMail()
        pnCorreos.Children.RemoveAt(pnCorreos.Children.Count - 1)
        'For Each item As UIElement In Grid.Children
        '    item.IsEnabled = True
        'Next
        PantallaDeshabilitada.Visibility = Visibility.Hidden
    End Sub


    Private Function Now_FechaHoraServidor() As Date
        Dim Diferencia As TimeSpan = Now.Subtract(_FechaCliente)

        Dim DiferenciaMilisec As Integer = Diferencia.Milliseconds
        Dim DiferenciaSeconds As Integer = Diferencia.Seconds
        Dim DiferenciaMinutes As Integer = Diferencia.Minutes
        Dim DiferenciaHours As Integer = Diferencia.Hours
        Dim DiferenciaDays As Integer = Diferencia.Days

        If (_FechaServidor.Millisecond + DiferenciaMilisec) > 999 Then
            DiferenciaMilisec = _FechaServidor.Millisecond + DiferenciaMilisec - 1000
            DiferenciaSeconds = DiferenciaSeconds + 1
        Else
            DiferenciaMilisec = _FechaServidor.Millisecond + DiferenciaMilisec
        End If
        If (_FechaServidor.Second + DiferenciaSeconds) > 59 Then
            DiferenciaSeconds = _FechaServidor.Second + DiferenciaSeconds - 60
            DiferenciaMinutes = DiferenciaMinutes + 1
        Else
            DiferenciaSeconds = _FechaServidor.Second + DiferenciaSeconds
        End If
        If (_FechaServidor.Minute + DiferenciaMinutes) > 59 Then
            DiferenciaMinutes = _FechaServidor.Minute + DiferenciaMinutes - 60
            DiferenciaHours = DiferenciaHours + 1
        Else
            DiferenciaMinutes = _FechaServidor.Minute + DiferenciaMinutes
        End If
        If (_FechaServidor.Hour + DiferenciaHours) > 23 Then
            DiferenciaHours = _FechaServidor.Hour + DiferenciaHours - 24
            DiferenciaDays = DiferenciaDays + 1
        Else
            DiferenciaHours = _FechaServidor.Hour + DiferenciaHours
        End If
        Dim dtTZNow As Date
        Try
            dtTZNow = New Date(_FechaServidor.Year, _FechaServidor.Month, _FechaServidor.Day + DiferenciaDays, DiferenciaHours, DiferenciaMinutes, DiferenciaSeconds, DiferenciaMilisec)
        Catch ex1 As Exception
            Try
                dtTZNow = New Date(_FechaServidor.Year, _FechaServidor.Month + 1, 1, DiferenciaHours, DiferenciaMinutes, DiferenciaSeconds, DiferenciaMilisec)
            Catch ex2 As Exception
                dtTZNow = New Date(_FechaServidor.Year + 1, 1, 1, DiferenciaHours, DiferenciaMinutes, DiferenciaSeconds, DiferenciaMilisec)
            End Try
        End Try

        Return dtTZNow
    End Function

#End Region

#Region " Botones "

    Private Sub cmdCerrar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdCerrar.Click
        Me.CloseParentWindow()
    End Sub

    Private Sub cmdPausa_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdPausa.Click
        MostrarPantallaDatosPausa()
    End Sub

    Private Sub cmdReiniciar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdReiniciar.Click
        AccionSubasta(Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Reiniciar)
        ConfigurarBotonesAcciones()
    End Sub

    Private Sub cmdParar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdParar.Click
        AccionSubasta(Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Parar)
        ConfigurarBotonesAcciones()
    End Sub

    Private Sub cmdExtender_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdExtender.Click
        MostrarPantallaDatosExtender()
    End Sub

    'Private Sub cmdImprimir_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdImprimir.Click
    '    Dim oSaveDlg As New System.Windows.Forms.SaveFileDialog

    '    oSaveDlg.Filter = "XPS documents|*.xps"
    '    If Not oSaveDlg.ShowDialog = System.Windows.Forms.DialogResult.Yes Then
    '        Dim oGraficosGrupos As New List(Of GraficoSubastaMonitor)
    '        Dim oGraficoItem As GraficoSubastaMonitor
    '        For Each oCP As ContentPane In tgpGrupoItem.Items
    '            If Not oCP.Name Is Nothing AndAlso oCP.Name = "cpItem" AndAlso oCP.Visibility = Visibility.Visible Then
    '                oGraficoItem = oCP.Content
    '            Else
    '                oGraficosGrupos.Add(oCP.Content)
    '            End If
    '        Next

    '        Dim oReport As New Report1(pnGridDatosProceso, xamdgProveedores, xamdgEventos, GrafProceso, oGraficosGrupos, oGraficoItem)
    '        oReport.Export(OutputFormat.XPS, oSaveDlg.FileName)
    '    End If
    'End Sub

    Private Sub cmdExportar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdExportar.Click
        'Exportar el contenido de los grids a Excel
        Dim sFileName As String = System.IO.Path.GetTempPath() + "\SubastaMonitor.xls"
        Try
            'Textos para las worksheet
            Dim oTextos As New GSClient.CTextos
            Dim sProve As String = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 64)
            Dim sEventos As String = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 62)

            'Creación del Excel              
            'Dim oExporter As DataPresenterExcelExporter = CType(Me.Resources("Exporter"), DataPresenterExcelExporter)
            oProveExporter = New DataPresenterExcelExporter '= CType(Me.Resources("Exporter"), DataPresenterExcelExporter)
            Dim oExcel As Infragistics.Excel.Workbook = oProveExporter.Export(Me.xamdgProveedores, sFileName, WorkbookFormat.Excel97To2003, New ExportOptions)
            oExcel.Worksheets(0).Name = sProve

            oEveExporter = New DataPresenterExcelExporter
            Dim oWS As Infragistics.Excel.Worksheet = oExcel.Worksheets.Add(sEventos)
            oEveExporter.Export(Me.xamdgEventos, oWS, New ExportOptions)
            oExcel.Save(sFileName)
        Catch ex As Exception

        End Try

        ' Execute Excel to display the exported workbook.
        Dim p As New Process()
        p.StartInfo.FileName = sFileName
        p.Start()
    End Sub

    '<summary>Muestra la pantalla de datos de pausa de subasta</summary>        
    '<remarks>Llamada desde: cmdPausa_Click  </remarks>

    Private Sub MostrarPantallaDatosPausa()
        PantallaDeshabilitada.Visibility = Visibility.Visible

        'Añadir el nuevo control y controladores de eventos
        Dim oDatosPausaCtrl As New OpcionesPausaSubasta

        AddHandler oDatosPausaCtrl.OkClick, AddressOf DatosPausa_OkClick
        AddHandler oDatosPausaCtrl.CloseClick, AddressOf DatosPausa_CloseClick

        pnCorreos.Children.Add(oDatosPausaCtrl)
        pnCorreos.Visibility = Visibility.Visible
        pnCorreos.IsEnabled = True
    End Sub

    '<summary>Muestra la pantalla de datos de extensión de subasta</summary>        
    '<remarks>Llamada desde: cmdExtender_Click  </remarks>

    Private Sub MostrarPantallaDatosExtender()
        PantallaDeshabilitada.Visibility = Visibility.Visible

        'Añadir el nuevo control y controladores de eventos
        Dim oDatosExtCtrl As New OpcionesExtender

        AddHandler oDatosExtCtrl.OkClick, AddressOf DatosExt_OkClick
        AddHandler oDatosExtCtrl.CloseClick, AddressOf DatosExt_CloseClick

        pnCorreos.Children.Add(oDatosExtCtrl)
        pnCorreos.Visibility = Visibility.Visible
        pnCorreos.IsEnabled = True
    End Sub

    '<summary>Lleva a cabo la pausa de la subasta</summary>        
    '<remarks>Llamada desde: cmdPausa_Click  </remarks>

    Private Sub AccionSubasta(ByVal Accion As AccionSubasta, Optional ByVal oDatosPausa As DatosPausarSubasta = Nothing, Optional ByVal iMinExtender As Integer = 0)
        Dim oSubasta As GSServerModel.ProcesoSubasta = Me.GetBindingObject("Subasta")

        Dim oEvento As New GSServerModel.ProcesoSubastaEvento
        oEvento.Anyo = oSubasta.Anyo
        oEvento.GMN1 = oSubasta.GMN1
        oEvento.Proceso = oSubasta.Codigo
        oEvento.Accion = Accion
        'Dim oTZInfo As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(ModulePublic.oUsu.TimeZone)
        'Dim dtTZNow As Date = TimeZoneInfo.ConvertTimeFromUtc(Date.UtcNow, oTZInfo)

        'Obtener la fecha y hora actual del servidor de bbdd
        Dim dtTZNow As Date = Now_FechaHoraServidor()

        oEvento.Fecha = dtTZNow
        Select Case Accion
            Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Pausar
                If Not oDatosPausa Is Nothing Then
                    oEvento.ModoReinicioSub = oDatosPausa.ModReinicioSub
                    oEvento.MinReinicioSub = oDatosPausa.MinReinicioSub
                    oEvento.FechaReinicioSub = oDatosPausa.FecReinicioSub
                    oEvento.ModoIncTSub = oDatosPausa.ModIncTSub
                    oEvento.MinIncTSub = oDatosPausa.MinIncTSub
                End If
            Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Extender
                oEvento.Extension = iMinExtender
        End Select

        Me.Cursor = Cursors.Wait

        Dim oProcRule As New GSClient.ProcesosRule
        Dim oError As GSServerModel.GSException = oProcRule.AccionSubasta(oEvento)

        If oError.Number = ErroresGS.TESnoerror Then
            If oSubasta.NotificarEventos Then
                Me.Cursor = Cursors.Wait

                _oEMails = ComponerCorreos(oEvento)
                If ParametrosInstalacion.gbMostrarMail Then
                    MostrarCorreos(_oEMails)
                Else
                    ComunicarProveedores(_oEMails)
                    Me.CloseParentWindow()
                End If

                Me.Cursor = Cursors.Arrow
            End If

            Refresh()
        Else
            Me.Cursor = Cursors.Arrow
            GSClient.TratarError(oError)
        End If
    End Sub

    '<summary>
    'Devuelve los datos de los correos que se enviarán a los proveedores.
    'Se compone el mail con los datos contacto de subasta (idioma, tipo de correo, formato de fechas y de números)
    '</summary>   
    '<returns>Objeto de tipo EMail con los datos del correo a mostrar</returns>    
    '<remarks>Llamada desde: AccionSubasta  </remarks>

    Private Function ComponerCorreos(ByVal oEvento As GSServerModel.ProcesoSubastaEvento) As GSServerModel.EMails
        Dim sDirPlantilla As String = ParametrosInstalacion.gsPetOfeSubWeb

        Dim sIdioma As String = ModulePublic.ParametrosInstalacion.gsIdioma
        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        Dim oProceRules As New GSClient.ProcesosRule
        'Calcular el From
        Dim sFrom As String

        If GSClient.ParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS.Length > 0 Then
            Dim sCarpeta As String = ""
            Dim sMarcaPlantillas As String = ""
            If InStr(ParametrosInstalacion.gsPetOfeSubWeb, GSClient.ParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
                'Buscamos la empresa CARPETA_PLANTILLAS
                sCarpeta = oProceRules.DevolverCarpetaPlantillaProce(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
            End If
            If sCarpeta = "" Then
                'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
                sMarcaPlantillas = "\" & GSClient.ParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
            Else
                sMarcaPlantillas = GSClient.ParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
            End If
            sDirPlantilla = Replace(ParametrosInstalacion.gsPetOfeSubWeb, sMarcaPlantillas, sCarpeta)
        Else
            sDirPlantilla = ParametrosInstalacion.gsPetOfeSubWeb
        End If

        If GSClient.ParametrosGenerales.gbUsarRemitenteEmpresa And oProceso.Codigo > 0 Then
            sFrom = oProceRules.SacarRemitenteEmpresa(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
        ElseIf GSClient.ParametrosGenerales.gbSMTPUsarCuenta Then
            sFrom = GSClient.ParametrosGenerales.gsSMTPCuentaMail
        Else
            If Not ModulePublic.oUsu Is Nothing Then
                If NothingToStr(ModulePublic.oUsu.Persona) <> String.Empty Then
                    Dim oUsuRule As New GSClient.UsuarioRule
                    Dim oPer As GSServerModel.Persona = oUsuRule.DevolverPersona(ModulePublic.oUsu.Persona)
                    If NothingToStr(oPer.EMail) <> String.Empty Then
                        sFrom = oPer.EMail
                    End If
                ElseIf ModulePublic.oUsu.Tipo = TipoDeUsuario.Administrador Then
                    If NothingToStr(ModulePublic.oUsu.EMail) <> String.Empty Then
                        sFrom = ModulePublic.oUsu.EMail
                    End If
                End If
            End If
        End If

        'Descripciones del evento
        Dim iDescID As Integer
        Select Case oEvento.Accion
            Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Extender
                iDescID = 47
            Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Parar
                iDescID = 46
            Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Pausar
                iDescID = 49
            Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Reiniciar
                iDescID = 48
        End Select
        Dim oTextos As New CTextos
        Dim oEveDesc As Dictionary(Of String, String) = oTextos.DevolverTextos(ModuleName.FRM_SUBASTAMONITOR, iDescID)


        Return oProceRules.ComponerCorreosNotificacionEventoSubasta(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, GSClient.ParametrosGenerales.giINSTWEB, GSClient.ParametrosGenerales.giFSP_CIA, _
                                     GSClient.ParametrosGenerales.gsFSP_SRV, GSClient.ParametrosGenerales.gsFSP_BD, sDirPlantilla, sFrom, oEvento, oEveDesc)
    End Function

    '<summary>Realiza el envío de los emails a los proveedores</summary>   
    '<param name="oEmails">Objeto de tipo GSServerModel.EMails con los datos de los correos a enviar</param>         
    '<remarks>Llamada desde: AccionSubasta  </remarks>

    Private Function ComunicarProveedores(ByVal oEMails As GSServerModel.EMails) As Boolean
        Dim sIdioma As String = ModulePublic.ParametrosInstalacion.gsIdioma

        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")

        'Notificar
        Dim oProcRule As New GSClient.ProcesosRule
        Dim oError As GSServerModel.GSException = oProcRule.NotificacionEventoSubasta(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, oEMails)
        If oError.Number <> ErroresGS.TESnoerror Then
            GSClient.TratarError(oError)
        End If

        'Refrescar datos
        Refresh()
    End Function

    '<summary>Habilita o deshabilita los botones de las acciones en función del estado de la subasta</summary>        
    '<remarks>Llamada desde: cmdPausa_Click  </remarks>

    Private Sub ConfigurarBotonesAcciones()
        Dim oSubasta As GSServerModel.ProcesoSubasta = Me.GetBindingObject("Subasta")
        Select Case oSubasta.Estado
            Case EstadoSubasta.EnProceso
                cmdExtender.IsEnabled = True
                cmdReiniciar.IsEnabled = True
                cmdPausa.IsEnabled = True
                cmdParar.IsEnabled = True
            Case EstadoSubasta.Finalizada, EstadoSubasta.NoComenzada, EstadoSubasta.Parada
                cmdExtender.IsEnabled = False
                cmdReiniciar.IsEnabled = False
                cmdPausa.IsEnabled = False
                cmdParar.IsEnabled = False
            Case EstadoSubasta.Pausada
                cmdExtender.IsEnabled = False
                cmdReiniciar.IsEnabled = True
                cmdPausa.IsEnabled = False
                cmdParar.IsEnabled = True
        End Select
    End Sub

    '<summary>Controlador de evento. Simula el cierre de una ventana modal para el control de datos de subasta</summary> 

    Private Sub DatosPausa_CloseClick()
        pnCorreos.Children.RemoveAt(pnCorreos.Children.Count - 1)
        'For Each item As UIElement In Grid.Children
        '    item.IsEnabled = True
        'Next
        PantallaDeshabilitada.Visibility = Visibility.Hidden
    End Sub

    '<summary>Controlador de evento. Obtiene los datos de pausa de subasta</summary>           
    '<param name="e">Objeto de tipo DatosModifImporte que indica los citerios de modificación de importes</param>   

    Private Sub DatosPausa_OkClick(ByVal e As DatosPausarSubasta)
        DatosPausa_CloseClick()

        AccionSubasta(Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Pausar, e)
        ConfigurarBotonesAcciones()

        Dim oTextos As New GSClient.CTextos
        txtbDescEstadoSubasta.Text = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 33)
    End Sub

    '<summary>Controlador de evento. Simula el cierre de una ventana modal para el control de datos de subasta</summary> 

    Private Sub DatosExt_CloseClick()
        pnCorreos.Children.RemoveAt(pnCorreos.Children.Count - 1)
        PantallaDeshabilitada.Visibility = Visibility.Hidden
    End Sub

    '<summary>Controlador de evento. Obtiene los datos de extensión de subasta</summary>           
    '<param name="e">Integer que indica los minutos de extensión de la subasta</param>   

    Private Sub DatosExt_OkClick(ByVal e As Integer)
        DatosExt_CloseClick()

        AccionSubasta(Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Extender, , e)
        ConfigurarBotonesAcciones()
    End Sub

#End Region

#Region " Eventos controles "

    Private Sub xamceTipoVista_ValueChanged(ByVal sender As Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of Object)) Handles xamceTipoVista.ValueChanged
        Select Case e.NewValue
            Case 0
                xamceElementos.Value = Nothing
                VaciarComboElementos()
                cpItem.Visibility = Visibility.Collapsed
            Case Else
                CargarComboElementosItems(e.NewValue)
        End Select

        If Me.IsLoaded Then Refresh()
    End Sub

    Private Sub xamceElementos_ValueChanged(ByVal sender As Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of Object)) Handles xamceElementos.ValueChanged
        If Not e.NewValue Is Nothing Then            
            CargarGraficoItem(CType(e.NewValue, Integer))
            'Mostrar el gráfico del item
            tgpGrupoItem.SelectedItem = cpItem
        Else
            cpItem.Visibility = Visibility.Collapsed
        End If

        If Me.IsLoaded Then Refresh()
    End Sub

    '' <summary>Vacía el combo de elementos</summary>     
    '' <remarks>Llamada desde: xamceTipoVista_ValueChanged  </remarks>

    Private Sub VaciarComboElementos()
        xamceElementos.ItemsProvider = Nothing
    End Sub

    '' <summary>Carga el combo de elmentos con los items del proceso</summary>     
    '' <remarks>Llamada desde: xamceTipoVista_ValueChanged  </remarks>

    Private Sub CargarComboElementosItems(ByVal ID As Integer)
        Dim oItemProv As New ComboBoxItemsProvider
        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        Dim oItems As GSServerModel.Items = oProceso.Grupos.Find(oProceso.Anyo, oProceso.GMN1, oProceso.Codigo, ID).Items

        oItemProv.Items.Add(New ComboBoxDataItem(Nothing, String.Empty))    'Elemento nulo
        If Not oItems Is Nothing AndAlso oItems.Count > 0 Then
            For Each oItem As GSServerModel.Item In oItems
                oItemProv.Items.Add(New ComboBoxDataItem(oItem.ID, oItem.Articulo & " - " & oItem.Descripcion))
            Next
        End If
        xamceElementos.ItemsProvider = oItemProv
        xamceElementos.Value = Nothing
    End Sub

    Private Sub ProveCheckedChanged(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Dim editor As XamCheckEditor = CType(sender, XamCheckEditor)
        editor.EndEditMode(True, True)
    End Sub

    Private Sub _oProveResView_ItemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs, ByVal bNewData As Boolean) Handles _oProveResView.ItemPropertyChanged
        Dim oProveView As ProceProveResumenSubastaView = CType(sender, ProceProveResumenSubastaView)

        Select Case e.PropertyName
            Case "IsChecked"
                If oProveView.IsChecked Then
                    GrafProceso.AñadirSerie(oProveView.Proveedor)
                    For Each oCP As ContentPane In tgpGrupoItem.Items
                        Dim oGraf As GraficoSubastaMonitor = oCP.Content
                        If Not oGraf.DataSource Is Nothing Then oGraf.AñadirSerie(oProveView.Proveedor)
                    Next
                Else
                    GrafProceso.EliminarSerie(oProveView.Proveedor)
                    For Each oCP As ContentPane In tgpGrupoItem.Items
                        Dim oGraf As GraficoSubastaMonitor = oCP.Content
                        If Not oGraf.DataSource Is Nothing Then oGraf.EliminarSerie(oProveView.Proveedor)
                    Next
                End If
            Case "Conectado"
                Dim oEstadoProveCtrl As New EstadoProveedor(oProveView.Proveedor, oProveView.Denominacion, oProveView.Conectado)
                AddHandler oEstadoProveCtrl.CloseClick, AddressOf EstadoProveedor_CloseClick

                pnNotificaciones.Children.Add(oEstadoProveCtrl)
                pnNotificaciones.Visibility = Visibility.Visible
                pnNotificaciones.IsEnabled = True
            Case "PosicionPuja"
                If bNewData And oProveView.PosicionPuja = 1 Then
                    Dim oGanadorCtrl As New GanadorSubasta(oProveView.Proveedor, oProveView.Denominacion)
                    AddHandler oGanadorCtrl.CloseClick, AddressOf Ganador_CloseClick

                    pnNotificaciones.Children.Add(oGanadorCtrl)
                    pnNotificaciones.Visibility = Visibility.Visible
                    pnNotificaciones.IsEnabled = True
                End If
        End Select
    End Sub

    '<summary>Controlador de evento. Simula el cierre de una ventana modal para el control de estado de proveedor</summary> 

    Private Sub EstadoProveedor_CloseClick(ByVal oEstadoProve As EstadoProveedor)
        'pnNotificaciones.Children.RemoveAt(pnNotificaciones.Children.Count - 1)
        pnNotificaciones.Children.Remove(oEstadoProve)
    End Sub

    '<summary>Controlador de evento. Simula el cierre de una ventana modal para el control de ganador de subasta</summary>

    Private Sub Ganador_CloseClick(ByVal oGanador As GanadorSubasta)
        pnNotificaciones.Children.Remove(oGanador)
    End Sub

    Private Sub _oTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _oTimer.Tick
        _iNumSeg += 1
        If _iNumSeg >= 10 Then
            _iNumSeg = 0
            Refresh(True)
        End If

        SetRemainingTime(Me.GetBindingObject("Proceso"), Me.GetBindingObject("Subasta"))
    End Sub

    '' <summary>Refresca los datos de pantalla</summary>     
    '' <remarks>Llamada desde: _oTimer_Tick, xamceTipoVista_ValueChanged  </remarks>

    Private Sub Refresh(Optional ByVal bTimer As Boolean = False)
        Me.Cursor = Cursors.Wait

        'Dim oSubasta As GSServerModel.ProcesoSubasta = Me.GetBindingObject("Subasta")
        'If oSubasta.Estado = EstadoSubasta.EnProceso Then
        GetData(True, bTimer)
        MakeDataBinding()
        ConfigurarGrid()
        CargarGraficoProceso()
        RefrescarPestañasGrupos()
        ConfigurarBotonesAcciones()
        'End If

        Me.Cursor = Cursors.Arrow
    End Sub

#End Region

#Region " Excel Export Events"

    Private Sub oProveExporter_CellExporting(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.ExcelExporter.CellExportingEventArgs) Handles oProveExporter.CellExporting
        Select Case e.Field.Name
            Case "IsChecked", "Conectado"
                e.Cancel = True
        End Select
    End Sub

    Private Sub oProveExporter_HeaderLabelExporting(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.ExcelExporter.HeaderLabelExportingEventArgs) Handles oProveExporter.HeaderLabelExporting
        Select Case e.Field.Name
            Case "IsChecked", "Conectado"
                e.CurrentWorksheet.Columns(e.CurrentColumnIndex).Hidden = True
        End Select
    End Sub

#End Region

#Region " ProveResView Class "

    Private Class ProceProvesResumenSubastaView
        Inherits ObservableCollection(Of ProceProveResumenSubastaView)

        Public Event ItemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs, ByVal bNewData As Boolean)

        Private _bNewData As Boolean

        Public Sub New(ByVal oProceProves As GSServerModel.ProceProvesResumenSubasta)            
            For Each oProveRes As GSServerModel.ProceProveResumenSubasta In oProceProves
                Dim oProceProveView As ProceProveResumenSubastaView = New ProceProveResumenSubastaView(oProveRes, True)
                AddHandler oProceProveView.PropertyChanged, AddressOf Item_PropertyChanged
                Me.Add(oProceProveView)
            Next
        End Sub

        Public Sub Refresh(ByVal oProceProves As GSServerModel.ProceProvesResumenSubasta, ByVal bNewData As Boolean)
            _bNewData = bNewData

            'Nuevos y actualizaciones
            For Each oProceProve As GSServerModel.ProceProveResumenSubasta In oProceProves
                Dim oProceProveView As ProceProveResumenSubastaView = Me.Item(oProceProve.Proveedor)
                If Not oProceProveView Is Nothing Then
                    'Si ya existe el proveedor actualizar los datos                    
                    oProceProveView.ProveRes = oProceProve
                Else
                    'Si es un proveedor nuevo darlo de alta
                    Dim oNewProceProveView As ProceProveResumenSubastaView = New ProceProveResumenSubastaView(oProceProve, True)
                    AddHandler oNewProceProveView.PropertyChanged, AddressOf Item_PropertyChanged
                    Me.Add(oNewProceProveView)
                End If
            Next

            'Eliminados
            For i As Integer = Me.Count - 1 To 0 Step -1
                Dim oProceProve As GSServerModel.ProceProveResumenSubasta = oProceProves.Item(Me.Item(i).Proveedor)
                If oProceProve Is Nothing Then Me.Remove(Me.Item(i))
            Next
        End Sub

        Public Function CheckedProves() As List(Of String)
            Dim oCheckedProves As New List(Of String)

            For Each oProveView As ProceProveResumenSubastaView In Me
                If oProveView.IsChecked Then oCheckedProves.Add(oProveView.Proveedor)
            Next

            Return oCheckedProves
        End Function

        Public Overloads ReadOnly Property Item(ByVal Prove As String) As ProceProveResumenSubastaView
            Get
                'Return Me.Item(Function(elemento As ProceProveResumenSubastaView) elemento.Proveedor = Prove)
                For Each o As ProceProveResumenSubastaView In Me
                    If o.Proveedor = Prove Then Return o
                Next
            End Get
        End Property

        Private Sub Item_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            RaiseEvent ItemPropertyChanged(sender, e, _bNewData)
        End Sub

    End Class

    Private Class ProceProveResumenSubastaView
        Implements System.ComponentModel.INotifyPropertyChanged

        Private _oProveRes As GSServerModel.ProceProveResumenSubasta
        Private _bIsChecked As Boolean

        Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

        Public Sub New(ByVal oProveRes As GSServerModel.ProceProveResumenSubasta, ByVal bIsChecked As Boolean)
            _oProveRes = oProveRes
            _bIsChecked = bIsChecked
        End Sub

#Region " Propiedades "

        Public WriteOnly Property ProveRes As GSServerModel.ProceProveResumenSubasta
            Set(ByVal value As GSServerModel.ProceProveResumenSubasta)
                Me.NumPujas = value.NumPujas
                Me.FechaUltimaPuja = value.FechaUltimaPuja
                Me.Precio = value.Precio
                Me.Cantidad = value.Cantidad
                Me.ValorUltimaPuja = value.ValorUltimaPuja
                Me.Ahorro = value.Ahorro
                Me.PorcentAhorro = value.PorcentAhorro
                Me.PosicionPuja = value.PosicionPuja
                Me.Conectado = value.Conectado
                Me.FechaInicio = value.FechaInicio
                Me.FechaFin = value.FechaFin
            End Set
        End Property

        Public Property IsChecked As Boolean
            Get
                Return _bIsChecked
            End Get
            Set(ByVal value As Boolean)
                _bIsChecked = value
                NotifyPropertyChanged("IsChecked")
            End Set
        End Property

        Public ReadOnly Property Anyo() As Integer
            Get
                Return _oProveRes.Anyo
            End Get
        End Property

        Public ReadOnly Property GMN1() As String
            Get
                GMN1 = _oProveRes.GMN1
            End Get
        End Property

        Public ReadOnly Property Proceso() As Integer
            Get
                Proceso = _oProveRes.Proceso
            End Get
        End Property

        Public ReadOnly Property Proveedor() As String
            Get
                Return _oProveRes.Proveedor
            End Get
        End Property

        Public ReadOnly Property Denominacion As String
            Get
                Return _oProveRes.Denominacion
            End Get
        End Property

        Public Property NumPujas() As Integer
            Get
                Return _oProveRes.NumPujas
            End Get
            Set(ByVal value As Integer)
                If _oProveRes.NumPujas <> value Then
                    _oProveRes.NumPujas = value
                    NotifyPropertyChanged("NumPujas")
                End If
            End Set
        End Property

        Public Property FechaUltimaPuja() As Nullable(Of Date)
            Get
                Return _oProveRes.FechaUltimaPuja
            End Get
            Set(ByVal value As Nullable(Of Date))
                If Not Date.Equals(_oProveRes.FechaUltimaPuja, value) Then
                    _oProveRes.FechaUltimaPuja = value
                    NotifyPropertyChanged("FechaUltimaPuja")
                End If
            End Set
        End Property

        Public Property Precio() As Nullable(Of Double)
            Get
                Return _oProveRes.Precio
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oProveRes.Precio) Then
                    _oProveRes.Precio = value
                    NotifyPropertyChanged("Precio")
                End If
            End Set
        End Property

        Public Property Cantidad() As Nullable(Of Double)
            Get
                Return _oProveRes.Cantidad
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oProveRes.Cantidad) Then
                    _oProveRes.Cantidad = value
                    NotifyPropertyChanged("Cantidad")
                End If
            End Set
        End Property

        Public Property ValorUltimaPuja() As Nullable(Of Double)
            Get
                Return _oProveRes.ValorUltimaPuja
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oProveRes.ValorUltimaPuja) Then
                    _oProveRes.ValorUltimaPuja = value
                    NotifyPropertyChanged("ValorUltimaPuja")
                End If
            End Set
        End Property

        Public Property Ahorro() As Nullable(Of Double)
            Get
                Return _oProveRes.Ahorro
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oProveRes.Ahorro) Then
                    _oProveRes.Ahorro = value
                    NotifyPropertyChanged("Ahorro")
                End If
            End Set
        End Property

        Public Property PorcentAhorro() As Nullable(Of Double)
            Get
                Return _oProveRes.PorcentAhorro
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oProveRes.PorcentAhorro) Then
                    _oProveRes.PorcentAhorro = value
                    NotifyPropertyChanged("PorcentAhorro")
                End If
            End Set
        End Property

        Public Property PosicionPuja() As Nullable(Of Integer)
            Get
                Return _oProveRes.PosicionPuja
            End Get
            Set(ByVal value As Nullable(Of Integer))
                If Not value.Equals(_oProveRes.PosicionPuja) Then
                    _oProveRes.PosicionPuja = value
                    NotifyPropertyChanged("PosicionPuja")
                End If
            End Set
        End Property

        Public Property Conectado() As Boolean
            Get
                Return _oProveRes.Conectado
            End Get
            Set(ByVal value As Boolean)
                If Not value = _oProveRes.Conectado Then
                    _oProveRes.Conectado = value
                    NotifyPropertyChanged("Conectado")
                End If
            End Set
        End Property

        Public Property FechaInicio() As Nullable(Of Date)
            Get
                Return _oProveRes.FechaInicio
            End Get
            Set(ByVal value As Nullable(Of Date))
                If Not value.Equals(_oProveRes.FechaInicio) Then
                    _oProveRes.FechaInicio = value
                    NotifyPropertyChanged("FechaInicio")
                End If
            End Set
        End Property

        Public Property FechaFin() As Nullable(Of Date)
            Get
                Return _oProveRes.FechaFin
            End Get
            Set(ByVal value As Nullable(Of Date))
                If Not value.Equals(_oProveRes.FechaFin) Then
                    _oProveRes.FechaFin = value
                    NotifyPropertyChanged("FechaFin")
                End If
            End Set
        End Property

        Public ReadOnly Property OrdenSubasta() As Nullable(Of Integer)
            Get
                If Not _oProveRes.PosicionPuja Is Nothing Then
                    Return _oProveRes.PosicionPuja
                Else
                    Return 10000
                End If
            End Get
        End Property

#End Region

        Private Sub NotifyPropertyChanged(ByVal info As String)
            RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(info))
        End Sub

    End Class

#End Region

#Region " EventosView Class "

    Private Class EventosView
        Inherits ObservableCollection(Of EventoView)

        Public Sub New(ByVal oEventos As GSServerModel.SubastaEventos)
            Dim oTextos As New GSClient.CTextos
            Dim ds As DataSet = oTextos.DevolverTextos(ModuleName.FRM_SUBASTAMONITOR, ModulePublic.ParametrosInstalacion.gsIdioma)
            Dim dvTextos As New DataView(ds.Tables(0))
            dvTextos.Sort = "ID"

            For Each oEvento As GSServerModel.SubastaEvento In oEventos                
                Dim oEventoView As EventoView = New EventoView(oEvento, dvTextos)
                Me.Add(oEventoView)
            Next
        End Sub

        Public Sub Refresh(ByVal oEventos As GSServerModel.SubastaEventos)
            Dim oTextos As New GSClient.CTextos
            Dim ds As DataSet = oTextos.DevolverTextos(ModuleName.FRM_SUBASTAMONITOR, ModulePublic.ParametrosInstalacion.gsIdioma)
            Dim dvTextos As New DataView(ds.Tables(0))
            dvTextos.Sort = "ID"

            'Nuevos y actualizaciones
            For Each oEvento As GSServerModel.SubastaEvento In oEventos
                Dim oEventoView As EventoView = Me.Item(oEvento.ID)
                If oEventoView Is Nothing Then
                    'Si es un evento nuevo darlo de alta
                    Dim oNewEventoView As New EventoView(oEvento, dvTextos)
                    Me.Add(oNewEventoView)
                End If
            Next
        End Sub

        Public Shadows Function Item(ByVal ID As Integer) As EventoView
            For Each o As EventoView In Me
                If o.ID = ID Then Return o
            Next
        End Function

    End Class

    Private Class EventoView

        Private _oEvento As GSServerModel.SubastaEvento
        Private _sDescEvento As String

        Public Sub New(ByVal oEvento As GSServerModel.SubastaEvento, ByVal dvTextos As DataView)
            _oEvento = oEvento

            Select Case _oEvento.Accion
                Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Arrancar
                    _sDescEvento = DevolverTexto(dvTextos, 70)
                Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Extender
                    _sDescEvento = CType(DevolverTexto(dvTextos, 71), String).Replace("@MIN", IIf(_oEvento.Extension Is Nothing, String.Empty, _oEvento.Extension))
                Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Finalizar
                    _sDescEvento = DevolverTexto(dvTextos, 72)
                Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.FinalizarSesionProve
                    _sDescEvento = CType(DevolverTexto(dvTextos, 73), String).Replace("@PROVE", _oEvento.Proveedor)
                Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.IniciarSesionProve
                    _sDescEvento = CType(DevolverTexto(dvTextos, 74), String).Replace("@PROVE", _oEvento.Proveedor)
                Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Parar
                    _sDescEvento = DevolverTexto(dvTextos, 75)
                Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Pausar                    
                    Select Case _oEvento.ModoReinicioSubasta
                        Case Fullstep.FSNLibrary.TiposDeDatos.ModoReinicioSubasta.Manual
                            If _oEvento.ModoIncTiempoSubasta Is Nothing Then
                                _sDescEvento = DevolverTexto(dvTextos, 76)
                            Else                                
                                Select Case _oEvento.ModoIncTiempoSubasta
                                    Case Fullstep.FSNLibrary.TiposDeDatos.ModoIncTiempoSubasta.TiempoPausa
                                        _sDescEvento = DevolverTexto(dvTextos, 79)
                                    Case Fullstep.FSNLibrary.TiposDeDatos.ModoIncTiempoSubasta.TiempoIndicado
                                        _sDescEvento = CType(DevolverTexto(dvTextos, 85), String).Replace("@MININC", IIf(_oEvento.MinIncTiempoSubasta Is Nothing, String.Empty, _oEvento.MinIncTiempoSubasta))
                                End Select
                            End If
                        Case Fullstep.FSNLibrary.TiposDeDatos.ModoReinicioSubasta.Automatico
                            If _oEvento.FechaReinicioSubasta Is Nothing Then
                                If _oEvento.ModoIncTiempoSubasta Is Nothing Then
                                    _sDescEvento = CType(DevolverTexto(dvTextos, 77), String).Replace("@MIN", IIf(_oEvento.MinReinicioSubasta Is Nothing, String.Empty, _oEvento.MinReinicioSubasta))
                                Else                                    
                                    Select Case _oEvento.ModoIncTiempoSubasta
                                        Case Fullstep.FSNLibrary.TiposDeDatos.ModoIncTiempoSubasta.TiempoPausa
                                            _sDescEvento = CType(DevolverTexto(dvTextos, 80), String).Replace("@MIN", IIf(_oEvento.MinReinicioSubasta Is Nothing, String.Empty, _oEvento.MinReinicioSubasta))
                                        Case Fullstep.FSNLibrary.TiposDeDatos.ModoIncTiempoSubasta.TiempoIndicado
                                            _sDescEvento = CType(DevolverTexto(dvTextos, 86), String).Replace("@MININC", IIf(_oEvento.MinIncTiempoSubasta Is Nothing, String.Empty, _oEvento.MinIncTiempoSubasta))
                                            _sDescEvento = _sDescEvento.Replace("@MIN", _oEvento.MinReinicioSubasta)
                                    End Select
                                End If
                            Else
                                If _oEvento.ModoIncTiempoSubasta Is Nothing Then
                                    _sDescEvento = CType(DevolverTexto(dvTextos, 78), String).Replace("@FEC", IIf(_oEvento.FechaReinicioSubasta Is Nothing, String.Empty, _oEvento.FechaReinicioSubasta))
                                Else                                    
                                    Select Case _oEvento.ModoIncTiempoSubasta
                                        Case Fullstep.FSNLibrary.TiposDeDatos.ModoIncTiempoSubasta.TiempoPausa
                                            _sDescEvento = CType(DevolverTexto(dvTextos, 81), String).Replace("@FEC", IIf(_oEvento.FechaReinicioSubasta Is Nothing, String.Empty, _oEvento.FechaReinicioSubasta))
                                        Case Fullstep.FSNLibrary.TiposDeDatos.ModoIncTiempoSubasta.TiempoIndicado
                                            _sDescEvento = CType(DevolverTexto(dvTextos, 87), String).Replace("@FEC", IIf(_oEvento.FechaReinicioSubasta Is Nothing, String.Empty, _oEvento.FechaReinicioSubasta))
                                            _sDescEvento = _sDescEvento.Replace("@MININC", _oEvento.MinIncTiempoSubasta)
                                    End Select
                                End If
                            End If
                    End Select                    
                Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Pujar
                    _sDescEvento = CType(DevolverTexto(dvTextos, 82), String).Replace("@PROVE", _oEvento.Proveedor)
                Case Fullstep.FSNLibrary.TiposDeDatos.AccionSubasta.Reiniciar
                    _sDescEvento = DevolverTexto(dvTextos, 83)
            End Select
        End Sub

        Private Function DevolverTexto(ByVal dvTextos As DataView, ByVal iID As Integer) As String
            Dim sTexto As String = String.Empty
            Dim iIndex As Integer = dvTextos.Find(iID)
            If iIndex >= 0 Then sTexto = dvTextos(iIndex).Item(0).ToString
            Return sTexto
        End Function

#Region " Propiedades "

        Public ReadOnly Property ID As Integer
            Get
                Return _oEvento.ID
            End Get
        End Property

        Public ReadOnly Property Anyo() As Integer
            Get
                Return _oEvento.Anyo
            End Get
        End Property

        Public ReadOnly Property Proceso() As Integer
            Get
                Return _oEvento.Proceso
            End Get
        End Property

        Public ReadOnly Property GMN1() As String
            Get
                Return _oEvento.GMN1
            End Get
        End Property

        Public ReadOnly Property Accion() As AccionSubasta
            Get
                Return _oEvento.Accion
            End Get
        End Property

        Public ReadOnly Property Fecha() As Date
            Get
                Return _oEvento.Fecha
            End Get
        End Property

        Public ReadOnly Property ModoReinicioSubasta() As Nullable(Of ModoReinicioSubasta)
            Get
                Return _oEvento.ModoReinicioSubasta
            End Get            
        End Property

        Public ReadOnly Property MinReinicioSubasta() As Nullable(Of Integer)
            Get
                Return _oEvento.MinReinicioSubasta
            End Get            
        End Property

        Public ReadOnly Property FechaReinicioSubasta() As Nullable(Of Date)
            Get
                Return _oEvento.FechaReinicioSubasta
            End Get
        End Property

        Public ReadOnly Property ModoIncTiempoSubasta() As Nullable(Of ModoIncTiempoSubasta)
            Get
                Return _oEvento.ModoIncTiempoSubasta
            End Get
        End Property

        Public ReadOnly Property MinIncTiempoSubasta() As Nullable(Of Integer)
            Get
                Return _oEvento.MinIncTiempoSubasta
            End Get
        End Property

        Public ReadOnly Property DescEvento() As String
            Get
                Return _sDescEvento
            End Get
        End Property

#End Region

    End Class

#End Region

End Class

#Region " Converters "

Public Class BooleanToStyleConverter
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        'Dim image As BitmapImage = New BitmapImage()
        'image.BeginInit()
        Dim oStyle As Style
        If CType(value, Boolean) Then
            'image.UriSource = New Uri("/GSCUIProcesos;component/Images/connected.ico", System.UriKind.Relative)            
            oStyle = Application.Current.FindResource("EstiloProveedorDesconectadoConectado")
        Else
            'image.UriSource = New Uri("/GSCUIProcesos;component/Images/unconnected.ico", System.UriKind.Relative)
            oStyle = Application.Current.FindResource("EstiloProveedorConectadoDesconectado")
        End If
        'image.EndInit()
        'Return image
        Return oStyle
    End Function

    Public Function ConvertBack1(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

Public Class BooleanToStringConverter
    Implements IValueConverter

    Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Dim oTextos As New GSClient.CTextos
        If CType(value, Boolean) Then
            Return oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 21)
        Else
            Return oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTAMONITOR, 22)
        End If
    End Function

    Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

Public Class TipoVistaToBooleanConverter
    Implements IValueConverter

    Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements IValueConverter.Convert        
        If value = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

#End Region


