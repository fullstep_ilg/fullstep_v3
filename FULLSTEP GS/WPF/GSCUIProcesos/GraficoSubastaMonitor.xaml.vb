﻿Imports Infragistics.Windows.Chart
Imports GSClient

Public Class GraficoSubastaMonitor
    Inherits ControlBase

    Private _bChartVacio As Boolean
    Private _oDataSource As GSServerModel.ProceProvesPujas
    Private _dtFechaMin As Date
    Private _dtFechaMax As Date
    Private _sHeader As String

#Region " Constructor "

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.          
        _bChartVacio = True
        xamchGrafico.Legend.Visible = False
    End Sub

#End Region

#Region " Propiedades "

    Public Property DataSource As GSServerModel.ProceProvesPujas
        Get
            Return _oDataSource
        End Get
        Set(ByVal value As GSServerModel.ProceProvesPujas)
            _oDataSource = value            
        End Set
    End Property

    Public Property FechaMin As Date
        Get
            Return (_dtFechaMin)
        End Get
        Set(ByVal value As Date)
            _dtFechaMin = value

            Dim oDateToDoubleConv As New GSClient.DateToDoubleConverter
            xamchGrafico.Axes(0).Minimum = oDateToDoubleConv.Convert(Nothing, Nothing, _dtFechaMin, Nothing)
            'Se comparan los valores consigo mismos para verificar que se les ha asigndo un valor (si no tienen el valor -1.#IND, NaN)
            If xamchGrafico.Axes(0).Minimum = xamchGrafico.Axes(0).Minimum And xamchGrafico.Axes(0).Maximum = xamchGrafico.Axes(0).Maximum Then
                If xamchGrafico.Axes(0).Maximum > xamchGrafico.Axes(0).Minimum Then
                    xamchGrafico.Axes(0).Unit = (xamchGrafico.Axes(0).Maximum - xamchGrafico.Axes(0).Minimum) / 5
                End If
            End If
        End Set
    End Property

    Public Property FechaMax As Date
        Get
            Return _dtFechaMax
        End Get
        Set(ByVal value As Date)
            _dtFechaMax = value

            Dim oDateToDoubleConv As New GSClient.DateToDoubleConverter            
            xamchGrafico.Axes(0).Maximum = oDateToDoubleConv.Convert(Nothing, Nothing, _dtFechaMax, Nothing)
            'Se comparan los valores consigo mismos para verificar que se les ha asigndo un valor (si no tienen el valor -1.#IND, NaN)
            If xamchGrafico.Axes(0).Minimum = xamchGrafico.Axes(0).Minimum And xamchGrafico.Axes(0).Maximum = xamchGrafico.Axes(0).Maximum Then
                If xamchGrafico.Axes(0).Maximum > xamchGrafico.Axes(0).Minimum Then
                    xamchGrafico.Axes(0).Unit = (xamchGrafico.Axes(0).Maximum - xamchGrafico.Axes(0).Minimum) / 5                    
                End If
            End If
        End Set
    End Property

    Public Property Header As String
        Get
            Return _sHeader
        End Get
        Set(ByVal value As String)
            _sHeader = value
        End Set
    End Property

#End Region

#Region " Eventos GraficoSubastaMonitor "

    Private Sub GraficoSubastaMonitor_Initialized(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Initialized
        If Not System.ComponentModel.DesignerProperties.GetIsInDesignMode(Me) Then
            MyBase.CargarTextos(ModuleName.FRM_SUBASTAMONITOR, Me.GridPpal)
        End If
        xamchMostrarMarcas.Value = True
    End Sub

#End Region

#Region " Eventos gráfico "

    Private Sub xamchGrafico_DataBind(ByVal sender As Object, ByVal e As Infragistics.Windows.Chart.DataBindEventArgs) Handles xamchGrafico.DataBind
        For Each oSeries As Series In xamchGrafico.Series
            AddMarker(oSeries)
        Next
    End Sub

#End Region

#Region " Eventos controles "

    Private Sub xamchMostrarMarcas_ValueChanged(ByVal sender As Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of Object)) Handles xamchMostrarMarcas.ValueChanged
        MostrarMarcas(e.NewValue)
    End Sub

#End Region

#Region " Métodos públicos "

    '' <summary>Vacía el gráfico</summary>     
    '' <remarks>Llamada desde:  </remarks>

    Public Sub BorrarGrafico()
        xamchGrafico.Series.Clear()        
    End Sub

    '' <summary>Vacía el gráfico</summary>       
    '' <remarks>Llamada desde:  </remarks>

    Public Sub CargarGrafico(ByVal oProves As List(Of String))
        If Not _oDataSource Is Nothing AndAlso _oDataSource.Count > 0 Then
            'xamchGrafico.Axes(0).Label.Format = "{0:T}"
            'xamchGrafico.Axes(0).Label.Angle = -30

            If Not oProves Is Nothing AndAlso oProves.Count > 0 Then
                For Each oProvePujas As GSServerModel.ProceProvePujas In _oDataSource
                    If oProves.Contains(oProvePujas.Prove) Then AñadirSerie(oProvePujas.Prove)
                Next

                xamchGrafico.Legend.Visible = True
            End If
        Else
            'Si el control xamchart no tiene series y el valor de la unidad es muy pequeño genera el warning: "The Unit value for this axis is too small ..."
            'Para eviatr esto se el pone el valor de la unidad a 1 en el caso de una gráfica sin datos
            xamchGrafico.Axes(0).Unit = 1
        End If
    End Sub

    '' <summary>Busca la serie de un proveedor en las series del gráfico</summary> 
    '' <param name="sProve">Código del proveedor</param>  
    '' <remarks>Llamada desde:  </remarks>

    Public Function BuscarSerie(ByVal sProve As String) As Integer
        Dim iIndex As Integer = -1
        If Not _bChartVacio Then
            For Each oSeries As Series In xamchGrafico.Series
                If CType(oSeries.DataSource, GSServerModel.ProceProvePujas).Prove = sProve Then
                    iIndex = xamchGrafico.Series.IndexOf(oSeries)
                    Exit For
                End If
            Next
        End If

        Return iIndex
    End Function

    '' <summary>Añade la serie de un proveedor al gráfico</summary> 
    '' <param name="Prove">Código del proveedor</param>  
    '' <remarks>Llamada desde:  </remarks>

    Public Sub AñadirSerie(ByVal Prove As String)
        If BuscarSerie(Prove) = -1 AndAlso Not _oDataSource Is Nothing AndAlso _oDataSource.Count > 0 Then
            Dim oProvePujas As GSServerModel.ProceProvePujas = _oDataSource.Item(Prove)
            AñadirSerie(oProvePujas)
            'Mostrar la leyenda
            xamchGrafico.Legend.Visible = True
            _bChartVacio = False
        End If
    End Sub

    '' <summary>Añade los datos de de pujas pasados como parámetros al gráfico</summary> 
    '' <param name="oProvePujas">Datos de pujas</param>  
    '' <remarks>Llamada desde:  </remarks>

    Private Sub AñadirSerie(ByVal oProvePujas As GSServerModel.ProceProvePujas)
        If Not oProvePujas Is Nothing Then
            'Si estaba vacío (con una serie vacía) la quito
            If _bChartVacio And xamchGrafico.Series.Count > 0 Then xamchGrafico.Series.Clear()

            Dim oSeries As New Series
            oSeries.Label = oProvePujas.Denominacion
            oSeries.DataMapping = "ValueX=FechaPuja; ValueY=Valor"
            oSeries.DataSource = oProvePujas
            oSeries.ChartType = ChartType.ScatterLine
            oSeries.Fill = StringToBrush(oProvePujas.Prove)

            'Marker
            'AddMarker(oSeries)

            xamchGrafico.Series.Add(oSeries)
        End If
    End Sub

    '' <summary>Obtiene un color a partir de una cadena de caracteres. Se intenta conseguir un código hexadecimal para el color a partir del
    '' código del proveedor. El código hexadecimal está formado por 9 caracteres: #TTRRGGBB</summary> 
    '' <param name="sCadena">cadena de caracteres</param>  
    '' <remarks>Llamada desde:  </remarks>
    '' <returns>Objeto de tipo brush</returns>

    Private Function StringToBrush(ByVal sCadena As String) As Brush
        Dim iCadena As Integer

        '1º se convierte la cadena a un entero
        Dim cCad As Char() = sCadena.ToCharArray
        For i = 0 To cCad.Length - 1
            'Se da más peso en el nº final a los últimos caracteres
            'En los NIFs, que suelen ser el código de los proveedores, hay más variación en los últimos caracteres
            'En el nº resultante habrá más variación en los últimos dígitos, en los que influirán más los últimos caracteres
            iCadena += ((cCad.Length - 1 - i) * Convert.ToInt16(cCad(i)))
        Next
        While iCadena.ToString("X").Length < 6
            iCadena = iCadena + (iCadena * sCadena.Length)
        End While

        '2º se pasa el entero a hexadecimal y se cogen 6 caracteres que representarán el color
        Dim s As String = iCadena.ToString("X")
        'Me quedo con los últimos dígitos que son los que más variación tendrán
        s = s.Substring(s.Length - 6, 6)
        'Se intercambian los últimos caracteres, que serán los que más variación tengan
        Dim a As Char() = s.ToArray
        Dim c As Char
        c = a(0)
        a(0) = a(5)
        a(5) = c
        c = a(2)
        a(2) = a(4)
        a(4) = c        

        s = "#FF" & s

        Dim o As New BrushConverter
        Return o.ConvertFromString(s)
    End Function

    Private Sub AddMarker(ByVal oSeries As Series)
        'Dim oMarker As New Marker
        'If xamchMostrarMarcas.Value Then
        '    oMarker.Type = MarkerType.Circle
        'Else
        '    oMarker.Type = MarkerType.None
        'End If
        'oMarker.Fill = oSeries.Fill
        'oMarker.Format = " "
        'oSeries.Marker = oMarker

        'AddHandler oMarker.MouseEnter, AddressOf OnMarkerMouseEnter
        'AddHandler oMarker.MouseLeave, AddressOf OnMarkerMouseLeave

        For Each oDP As DataPoint In oSeries.DataPoints
            oDP.Marker = New Marker
            If xamchMostrarMarcas.Value Then
                oDP.Marker.Type = MarkerType.Circle
            Else
                oDP.Marker.Type = MarkerType.None
            End If
            oDP.Marker.Fill = oSeries.Fill
            oDP.Marker.Format = " "            

            AddHandler oDP.Marker.MouseEnter, AddressOf OnMarkerMouseEnter
            AddHandler oDP.Marker.MouseLeave, AddressOf OnMarkerMouseLeave
        Next
    End Sub

    Private Sub OnMarkerMouseEnter(ByVal sender As Object, ByVal e As System.Windows.Input.MouseEventArgs)        
        CType(sender, Marker).Format = "{ValueX:T}" & Environment.NewLine & "{ValueY:N}"        
    End Sub

    Private Sub OnMarkerMouseLeave(ByVal sender As Object, ByVal e As System.Windows.Input.MouseEventArgs)
        CType(sender, Marker).Format = " "        
    End Sub

    Public Sub EliminarSerie(ByVal Prove As String)
        Dim iIndex As Integer = BuscarSerie(Prove)
        If iIndex > -1 Then
            xamchGrafico.Series.RemoveAt(iIndex)

            If xamchGrafico.Series.Count = 0 Then
                '    'Para que no se vea el chart por defecto
                '    AñadirSerie(New GSServerModel.ProceProvePujas)
                '    'Ocultar la leyenda
                xamchGrafico.Legend.Visible = False
                '    _bChartVacio = True
            End If
        End If
    End Sub

    Public Sub MostrarMarcas(ByVal bMostrar As Boolean)
        If Not _bChartVacio AndAlso Not xamchGrafico.Series Is Nothing AndAlso xamchGrafico.Series.Count > 0 Then
            For Each oSeries As Series In xamchGrafico.Series
                For Each oDP As DataPoint In oSeries.DataPoints
                    If Not bMostrar Then
                        oDP.Marker.Type = MarkerType.None
                    Else
                        oDP.Marker.Type = MarkerType.Circle
                    End If
                Next                
            Next
        End If
    End Sub

#End Region

End Class
