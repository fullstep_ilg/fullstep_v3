﻿Imports GSClient
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class OpcionesExtender
    Inherits ControlBase

    Public Event OkClick(ByVal e As Integer)
    Public Event CloseClick()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        MyBase.CargarTextos(ModuleName.FRM_SUBASTAMONITOR, Me.GridPpal)

        'Valores iniciales  
        xamneIncMin.Value = 0
    End Sub

#Region " Botones "

    Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnCerrar.Click
        RaiseEvent CloseClick()
    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdCancelar.Click
        RaiseEvent CloseClick()
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdAceptar.Click
        If ComprobarDatos() Then            
            RaiseEvent OkClick(CType(xamneIncMin.Value, Integer))
        End If
    End Sub

    Private Function ComprobarDatos() As Boolean
        ComprobarDatos = True

        If xamneIncMin.Value Is Nothing OrElse xamneIncMin.Value = 0 Then
            ComprobarDatos = False
            Mensajes.MensajeSeleccionarMinutosIncSubasta()
        End If
    End Function

#End Region

End Class
