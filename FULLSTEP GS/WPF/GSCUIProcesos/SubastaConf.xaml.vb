﻿Imports GSClient
Imports Infragistics.Windows.Editors
Imports Infragistics.Windows.DataPresenter
Imports System.Collections.ObjectModel
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Fullstep.FSNLibrary
Imports System.ComponentModel

Partial Public Class SubastaConf
    Inherits ControlBase

    Private _iAnyo As Integer
    Private _iCod As Integer
    Private _sGMN1 As String
    Private _dtFecApe As Date
    Private _dtFecPres As Date
    Private _bPermAdjDir As Boolean    
    Private _bPanelImportePinned As Boolean
    Private _DownloadManager As DownloadManager
    Private _oDatos As GSServerModel.DatosNotifSubasta
    Private _bSoloLectura As Boolean
    Private _oProvesDespublicar As GSServerModel.ProcesoProveedores
    Private _oProvesPublicar As GSServerModel.ProcesoProveedores

#Region " Constructor "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.  
        pnModifImportes.Visibility = Visibility.Collapsed
    End Sub

    '' <summary>Constructor</summary>
    '' <param name="iAnyo">Año del proceso</param>   
    '' <param name="iCod">Código del proceso</param>   
    '' <param name="sGMN1">GMN1 del proceso</param>   
    '' <remarks>Llamada desde: PInicio.New  </remarks>

    Public Sub New(ByVal iAnyo As Integer, ByVal iCod As Integer, ByVal sGMN1 As String, ByVal dtFecApe As Date, ByVal dtFecPres As Date, ByVal bPermAdjDir As Boolean, Optional ByVal bSoloLectura As Boolean = False)        
        Me.New()

        _iAnyo = iAnyo
        _iCod = iCod
        _sGMN1 = sGMN1
        _dtFecApe = dtFecApe
        _dtFecPres = dtFecPres
        _bPermAdjDir = bPermAdjDir
        _bSoloLectura = bSoloLectura

        _DownloadManager = New DownloadManager(System.IO.Path.GetTempPath)
        _oProvesDespublicar = New GSServerModel.ProcesoProveedores
        _oProvesPublicar = New GSServerModel.ProcesoProveedores

        If _bSoloLectura Then cmdAceptar.Visibility = Visibility.Collapsed

        RegisterEventsManagers()

        'Cargar textos
        MyBase.CargarTextos(ModuleName.FRM_SUBASTACONF, Me.Grid)
        Dim oTextos As New GSClient.CTextos
        cpDatosPpales.Header = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 85)
        cpDatosOpcionales.Header = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 86)
        cpImportes.Header = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 87)
        If _bSoloLectura Then cmdCancelar.Content = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 88)

        CargarEnumerados()        
        MakeDataBinding()
    End Sub

#End Region

#Region " Eventos SubastaConf "

    Private Sub SubastaConf_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        TextosFin.SoloLectura = _bSoloLectura
        If _bSoloLectura Then BloquearControles()
        MostrarDatosProceso()
        'ChequearProveedoresPortal()
        HabilitarComunicar()
    End Sub

#End Region

#Region " Métodos privados "

    '' <summary>Realiza el binding de los controles a los objetos de datos necesarios</summary>    
    '' <remarks>Llamada desde: New  </remarks>

    Private Sub MakeDataBinding()
        'Obtener idioma
        Dim sIdioma As String = ModulePublic.ParametrosInstalacion.gsIdioma
        If Not ModulePublic.oUsu.Idioma Is Nothing Then sIdioma = ModulePublic.oUsu.Idioma

        'Obtener objetos de datos        
        Dim oProcRule As New GSClient.ProcesosRule
        Dim oData As GSServerModel.SubastaConfData = oProcRule.DevolverDatosSubastaConf(_iAnyo, _iCod, _sGMN1, sIdioma)

        Me.AddBindingObject(oData.Proceso, "Proceso")
        Me.AddBindingObject(oData.ProcesoDef, "ProcesoDef")
        Me.AddBindingObject(oData.Moneda, "Moneda")
        Me.AddBindingObject(oData.Subasta, "Subasta")
        Me.AddBindingObject(oData.ProceProves, "Proveedores")
        Me.AddBindingObject(oData.SubastaReglas, "SubastaReglas")
        Me.AddBindingObject(oData.Idiomas, "Idiomas")
        Me.AddBindingObject(ModulePublic.oUsu, "Usuario")
        Me.AddBindingObject(oData.ProvesComunicados, "ProvesComunicados")

        'Me.MakeBinding(Me.Grid)
        Me.MakeBinding(Me.pnDatosProceso)
        Me.MakeBinding(Me.pnDatosSubasta)

        ''El binding del combo de las zonas horarias se hace sólo en un sentido porque sólo es para mostrar el dato, y no para actualizar el objeto de usuario
        ''ControlBase hace el binding en los 2 sentidos
        'Dim oBinding As New System.Windows.Data.Binding("TimeZone")
        'oBinding.Source = ModulePublic.oUsu
        'oBinding.Mode = BindingMode.OneWay
        'xamceTimeZone.SetBinding(FSNComboEditor.ValueProperty, oBinding)

        MakeBindingDatosOpcionales()

        MakeBindingImportes()
        ConfigurarGridImportes()

        'Zona horaria        
        Dim colTimeZones As ReadOnlyCollection(Of TimeZoneInfo) = TimeZoneInfo.GetSystemTimeZones
        For Each oTZInfo As TimeZoneInfo In colTimeZones
            If oTZInfo.Id = ModulePublic.oUsu.TimeZone Then
                txteTimeZone.text = oTZInfo.DisplayName
                Exit For
            End If
        Next        
    End Sub

    '<summary>Hace el binding del panel de datos opcionales</summary>          
    '<remarks>Llamada desde: xamdmDatosOp_ActivePaneChanged  </remarks>

    Private Sub MakeBindingDatosOpcionales()
        Dim oProcRule As New GSClient.ProcesosRule

        'Reglas
        Dim oSubastaReglas As GSServerModel.ProcesoSubastaReglas = Me.GetBindingObject("SubastaReglas")
        If Not oSubastaReglas Is Nothing Then
            Dim oFiles As GSServerModel.Files = SubastaReglaToFile(oSubastaReglas)
            fsnfsReglas.Archivos = oFiles
        End If
        TextosFin.Idiomas = Me.GetBindingObject("Idiomas")
        If Not CType(Me.GetBindingObject("Subasta"), GSServerModel.ProcesoSubasta).TextoFin Is Nothing Then
            TextosFin.IdTextos = CType(Me.GetBindingObject("Subasta"), GSServerModel.ProcesoSubasta).TextoFin
        Else
            TextosFin.IdTextos = 0
        End If
        TextosFin.Textos = CType(Me.GetBindingObject("Subasta"), GSServerModel.ProcesoSubasta).TextosFin

        Me.MakeBinding(Me.pnDatosOpc)
    End Sub

    '<summary>Hace el binding del panel de importes</summary>          
    '<remarks>Llamada desde: xamdmDatosOp_ActivePaneChanged  </remarks>

    Private Sub MakeBindingImportes()
        'Grupos
        If Not Me.ExistsBindingObject("Procesos") Then
            'Dim oProcesos As New GSServerModel.ProcesosSubasta
            'oProcesos.Add(Me.GetBindingObject("Subasta"))
            'Me.AddBindingObject(oProcesos, "Procesos")

            Dim oSubastasView As New SubastasView(Me.GetBindingObject("Subasta"), _bSoloLectura)
            Me.AddBindingObject(oSubastasView, "Procesos")
        End If

        Me.MakeBinding(Me.pnImportesPrecios)
    End Sub

    '' <summary>Registra handlers para eventos</summary>   
    '' <remarks>Llamada desde: New  </remarks>

    Private Sub RegisterEventsManagers()
        'Evento para el click del ratón en el header del grid de precios        
        'EventManager.RegisterClassHandler(GetType(HeaderPresenter), HeaderPresenter.MouseRightButtonUpEvent, New RoutedEventHandler(AddressOf OnHeaderRightClick))
        EventManager.RegisterClassHandler(GetType(LabelPresenter), LabelPresenter.MouseRightButtonUpEvent, New RoutedEventHandler(AddressOf OnHeaderRightClick))
    End Sub

    '' <summary>Carga los combos que muestran datos de enumerados</summary>     
    '' <remarks>Llamada desde: New  </remarks>

    Private Sub CargarEnumerados()
        Dim oTextos As New GSClient.CTextos

        'Tipo de subasta
        Dim oItemProv As New ComboBoxItemsProvider
        oItemProv.Items.Add(New ComboBoxDataItem(CType(TipoSubasta.Inglesa, Integer), oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 50)))
        oItemProv.Items.Add(New ComboBoxDataItem(CType(TipoSubasta.SobreCerrado, Integer), oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 51)))
        'oItemProv.Items.Add(New ComboBoxDataItem(CType(TipoSubasta.Japonesa, Integer), oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 52)))
        xamceTipoSub.ItemsProvider = oItemProv

        'Modo de subasta
        oItemProv = New ComboBoxItemsProvider
        oItemProv.Items.Add(New ComboBoxDataItem(CType(ModoSubasta.ProcesoCompleto, Integer), oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 53)))
        oItemProv.Items.Add(New ComboBoxDataItem(CType(ModoSubasta.Grupo, Integer), oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 54)))
        oItemProv.Items.Add(New ComboBoxDataItem(CType(ModoSubasta.Item, Integer), oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 55)))
        xamceModoSub.ItemsProvider = oItemProv
    End Sub

    Private Sub MostrarDatosProceso()
        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        txtbProceso.Text &= " " & oProceso.Anyo & "/" & oProceso.GMN1 & "/" & oProceso.Codigo & " - " & oProceso.Denominacion
    End Sub

    '<summary>Marca para subasta lo proveedores que tienen un código de portal</summary>      
    '<remarks>Llamada desde: SubastaConf_Loaded  </remarks>

    Private Sub ChequearProveedoresPortal()
        Dim oCheckField As Field = xamdgProveedores.FieldLayouts(0).Fields("Subasta")
        For Each oRecord As DataRecord In xamdgProveedores.Records
            If oRecord.Cells("CodigoPortal").Value = String.Empty Then
                oRecord.Cells(oCheckField).Value = False
            Else
                oRecord.Cells(oCheckField).Value = True
            End If
        Next
    End Sub

    '<summary>Devuelve el nº de proveedores seleccionados</summary>   
    '<returns>Nº de proveedores seleccionados para subasta</returns>    
    '<remarks>Llamada desde: cmdAceptar_Click  </remarks>

    Private Function NumProvesSelec() As Integer
        Dim iChecked As Integer = 0

        For Each oRecord As DataRecord In xamdgProveedores.Records
            If oRecord.Cells("Subasta").Value Then iChecked += 1
        Next

        Return iChecked
    End Function

    '<summary>Habilita el check de publicación a proveedores</summary>    
    '<remarks>Llamada desde: SubastaConf_Loaded  </remarks>

    Private Sub HabilitarComunicar(Optional ByVal strProveActual As String = Nothing, Optional ByVal bChecked As Boolean = False)
        'Si hay algún proveedor seleccionado habilitar check comunicación
        Dim bCom As Boolean = False
        Dim bSubasta As Boolean = False

        Dim oProves As GSServerModel.ProcesoProveedores = Me.GetBindingObject("Proveedores")
        For Each oProve As GSServerModel.ProcesoProveedor In oProves
            If Not strProveActual Is Nothing AndAlso oProve.Proveedor = strProveActual Then
                bSubasta = bChecked
            Else
                bSubasta = oProve.Subasta
            End If

            If bSubasta Then
                bCom = True
                Exit For
            End If
        Next

        xamchkPubProv.IsEnabled = bCom
        If Not bCom Then xamchkPubProv.IsChecked = False
    End Sub

    '<summary>Configura la pantalla para que no se puedan modificar los datos</summary>    
    '<remarks>Llamada desde: SubastaConf_Loaded  </remarks>

    Private Sub BloquearControles()
        xamceTipoSub.IsReadOnly = True
        xamceTipoSub.IsHitTestVisible = False
        xamceModoSub.IsReadOnly = True
        xamceModoSub.IsHitTestVisible = False
        xamdteInicio.IsReadOnly = True
        xamdteCierre.IsReadOnly = True        
        xamchkPubProv.IsReadOnly = True
        xamchkPubProv.IsHitTestVisible = False
        xamchkNotifProv.IsReadOnly = True
        xamchkNotifProv.IsHitTestVisible = False
        xamtxtMinEspera.IsReadOnly = True
        xamtxtMinEspera.IsReadOnly = True
        xamdteHoraSobre.IsReadOnly = True
        xamdgProveedores.FieldSettings.AllowEdit = False
        xamdgProveedores.FieldLayouts(0).Fields("Subasta").Settings.AllowEdit = False
        fsnfsReglas.SoloLectura = _bSoloLectura
        xamceMostrarProveGan.IsReadOnly = True
        xamceMostrarProveGan.IsHitTestVisible = False
        xamceNoMostrarInfoPuja.IsReadOnly = True
        xamceNoMostrarInfoPuja.IsHitTestVisible = False
        xamceEstBajadaMin.IsReadOnly = True
        xamceEstBajadaMin.IsHitTestVisible = False
        xamceMostrarPrecioPujaGan.IsReadOnly = True
        xamceMostrarPrecioPujaGan.IsHitTestVisible = False
        xamceMostrarDetPujas.IsReadOnly = True
        xamceMostrarDetPujas.IsHitTestVisible = False
        xamceEstBajMinProve.IsReadOnly = True
        xamceEstBajMinProve.IsHitTestVisible = False
        btnCargarPresPrecSal.IsEnabled = False
        btnModifValor.IsEnabled = False
        xamdgEstrucProce.FieldSettings.AllowEdit = False
        xamdgEstrucProce.FieldLayouts("FL1").Fields("PrecioSalida").Settings.AllowEdit = False
        xamdgEstrucProce.FieldLayouts("FL1").Fields("MinPujaGanadora").Settings.AllowEdit = False
        xamdgEstrucProce.FieldLayouts("FL1").Fields("MinPujaProve").Settings.AllowEdit = False
        xamdgEstrucProce.FieldLayouts("FL2").Fields("PrecioSalida").Settings.AllowEdit = False
        xamdgEstrucProce.FieldLayouts("FL2").Fields("MinPujaGanadora").Settings.AllowEdit = False
        xamdgEstrucProce.FieldLayouts("FL2").Fields("MinPujaProve").Settings.AllowEdit = False
        xamdgEstrucProce.FieldLayouts("FL3").Fields("PrecioSalida").Settings.AllowEdit = False
        xamdgEstrucProce.FieldLayouts("FL3").Fields("MinPujaGanadora").Settings.AllowEdit = False
        xamdgEstrucProce.FieldLayouts("FL3").Fields("MinPujaProve").Settings.AllowEdit = False
    End Sub

    '' <summary>Convierte un objeto ProcesoSubastaRegla a un objeto Files</summary>
    '' <param name="oSubastaRegla">objeto ProcesoSubastaReglas</param>   
    '' <returns>Objeto Files</returns>
    '' <remarks>Llamada desde: New  </remarks>

    Private Function SubastaReglaToFile(ByVal oSubastaReglas As GSServerModel.ProcesoSubastaReglas) As GSServerModel.Files
        Dim oFiles As New GSServerModel.Files

        If Not oSubastaReglas Is Nothing AndAlso oSubastaReglas.Count > 0 Then
            For Each oRegla As GSServerModel.ProcesoSubastaRegla In oSubastaReglas
                Dim oFile As New GSServerModel.File(oRegla.Nombre, oRegla.Nombre, oRegla.Comentario, oRegla.FecAlta, oRegla.TamañoArchivo)
                oFiles.Add(oFile)
            Next
        End If

        Return oFiles
    End Function

    '' <summary>Convierte un objeto Files a un objeto ProcesoSubastaReglas</summary>
    '' <param name="oFiles">objeto Files</param>   
    '' <returns>objeto ProcesoSubastaReglas</returns>
    '' <remarks>Llamada desde: cmdAceptar_Click  </remarks>

    Private Function FileToSubastaRegla(ByVal oFiles As GSServerModel.Files) As GSServerModel.ProcesoSubastaReglas
        Dim oProce As GSServerModel.Proceso = CType(Me.GetBindingObject("Proceso"), GSServerModel.Proceso)
        Dim oSubastaReglas As GSServerModel.ProcesoSubastaReglas = Me.GetBindingObject("SubastaReglas")

        'Borrados
        If Not oSubastaReglas Is Nothing AndAlso oSubastaReglas.Count > 0 Then
            Dim k As Integer = oSubastaReglas.Count - 1
            Dim i As Integer = 0
            While i <= k
                Dim oFile As GSServerModel.File = oFiles.Item(oSubastaReglas(i).Nombre)

                If oFile Is Nothing Then
                    oSubastaReglas.Remove(oSubastaReglas(i))
                    k -= 1
                Else
                    i += 1
                End If
            End While
        End If

        'Actualizaciones e inserciones
        For Each oFile As GSServerModel.File In oFiles
            'Buscarlo en el array de reglas que se ha obtenido
            Dim oRegla As GSServerModel.ProcesoSubastaRegla
            If Not oSubastaReglas Is Nothing AndAlso oSubastaReglas.Count > 0 Then
                oRegla = oSubastaReglas.Item(oFile.NombreArchivo)
            End If

            If Not oRegla Is Nothing Then
                'Actualizar
                oRegla.Comentario = oFile.Comentario
            Else
                'Nuevo                    
                oRegla = New GSServerModel.ProcesoSubastaRegla
                oRegla.Anyo = oProce.Anyo
                oRegla.Proce = oProce.Codigo
                oRegla.GMN1 = oProce.GMN1
                oRegla.ID = -1
                oRegla.Nombre = oFile.NombreArchivo
                oRegla.Comentario = oFile.Comentario
                oRegla.FecAlta = oFile.Fecha

                oSubastaReglas.Add(oRegla)
            End If
        Next

        Return oSubastaReglas
    End Function

#End Region

#Region " Botones "

    Private Sub cmdAceptar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdAceptar.Click
        'Finalizar posibles ediciones
        xamdgProveedores.EndEdit()
        xamdgEstrucProce.EndEdit()
        fsnfsReglas.EndEdit()

        If ComprobarDatos() Then            
            If PublicacionProveedores(xamchkPubProv.IsChecked) Then
                'ActualizarDatos
                Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
                Dim oProcesoDef As GSServerModel.ProcesoDef = Me.GetBindingObject("ProcesoDef")
                Dim oSubasta As GSServerModel.ProcesoSubasta = Me.GetBindingObject("Subasta")

                oProcesoDef.Subasta = True

                Dim oProceProves As GSServerModel.ProcesoProveedores = Me.GetBindingObject("Proveedores")
                Dim oReglas As GSServerModel.Files = fsnfsReglas.Archivos
                Dim oSubastaReglas As GSServerModel.ProcesoSubastaReglas = FileToSubastaRegla(oReglas)
                TextosFin.EndEdit()
                oSubasta.TextosFin = TextosFin.Textos

                'Actualización de datos                
                Me.Cursor = Cursors.Wait

                Dim oError As New GSServerModel.GSException(False, "", "", True)
                Dim oProcRule As New GSClient.ProcesosRule

                oError = oProcRule.ActualizarProcesoSubasta(Me.GetBindingObject("Proceso"), Me.GetBindingObject("ProcesoDef"), Me.GetBindingObject("Subasta"), _
                                                            oProceProves, oSubastaReglas, oReglas)

                If oError.Number <> ErroresGS.TESnoerror Then
                    Me.Cursor = Cursors.Arrow

                    GSClient.TratarError(oError)
                Else
                    'Refresh()

                    Me.Cursor = Cursors.Arrow
                    Mensajes.DatosActulizados()

                    Dim bComunica As Boolean = (oProceso.Estado >= TipoEstadoProceso.conasignacionvalida And xamchkPubProv.IsChecked And NumProvesSelec() > 0)
                    If bComunica Then
                        Mensajes.EnvioCorreosProveedores()

                        Me.Cursor = Cursors.Wait

                        _oDatos = ComponerCorreos()
                        If ParametrosInstalacion.gbMostrarMail Then
                            MostrarCorreos(_oDatos.EMailsSubasta)
                        Else
                            ComunicarProveedores(_oDatos)
                            Me.CloseParentWindow()
                        End If

                        Me.Cursor = Cursors.Arrow
                    Else
                        Me.CloseParentWindow()
                    End If
                End If
            End If
        End If
    End Sub

    '<summary>Refresca los datos de pantalla trayéndolos de nuevo</summary>          
    '<remarks>Llamada desde: cmdAceptar_Click  </remarks>

    Private Sub Refresh()
        Me.ClearBindingObjects()

        MakeDataBinding()

        MakeBindingDatosOpcionales()

        MakeBindingImportes()
    End Sub

    '<summary>
    'Devuelve una muestra de los correos que se enviarán a los proveedores.
    'Se compone el mail con los datos contacto de subasta (idioma, tipo de correo, formato de fechas y de números)
    '</summary>   
    '<returns>Objeto de tipo GSServerModel.EMail con los datos del correo a mostrar</returns>    
    '<remarks>Llamada desde: cmdAceptar_Click  </remarks>

    Private Function ComponerCorreos() As GSServerModel.DatosNotifSubasta
        Dim sDirPlantilla As String = ParametrosInstalacion.gsPetOfeSubWeb

        Dim sIdioma As String = ModulePublic.ParametrosInstalacion.gsIdioma

        'Calcular el From
        Dim sFrom As String
        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        Dim oProceRules As New GSClient.ProcesosRule

        If GSClient.ParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS.Length > 0 Then
            Dim sCarpeta As String = ""
            Dim sMarcaPlantillas As String = ""
            If InStr(ParametrosInstalacion.gsPetOfeSubWeb, GSClient.ParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
                'Buscamos la empresa CARPETA_PLANTILLAS
                sCarpeta = oProceRules.DevolverCarpetaPlantillaProce(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
            End If
            If sCarpeta = "" Then
                'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
                sMarcaPlantillas = "\" & GSClient.ParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
            Else
                sMarcaPlantillas = GSClient.ParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
            End If
            sDirPlantilla = Replace(ParametrosInstalacion.gsPetOfeSubWeb, sMarcaPlantillas, sCarpeta)
        Else
            sDirPlantilla = ParametrosInstalacion.gsPetOfeSubWeb
        End If

        If GSClient.ParametrosGenerales.gbUsarRemitenteEmpresa And oProceso.Codigo > 0 Then
            sFrom = oProceRules.SacarRemitenteEmpresa(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
        ElseIf GSClient.ParametrosGenerales.gbSMTPUsarCuenta Then
            sFrom = GSClient.ParametrosGenerales.gsSMTPCuentaMail
        Else
            If Not ModulePublic.oUsu Is Nothing Then
                If NothingToStr(ModulePublic.oUsu.Persona) <> String.Empty Then
                    Dim oUsuRule As New GSClient.UsuarioRule
                    Dim oPer As GSServerModel.Persona = oUsuRule.DevolverPersona(ModulePublic.oUsu.Persona)
                    If NothingToStr(oPer.EMail) <> String.Empty Then
                        sFrom = oPer.EMail
                    End If
                ElseIf ModulePublic.oUsu.Tipo = TipoDeUsuario.Administrador Then
                    If NothingToStr(ModulePublic.oUsu.EMail) <> String.Empty Then
                        sFrom = ModulePublic.oUsu.EMail
                    End If
                End If
            End If
        End If

        Return oProceRules.ComponerCorreosNotificacionSubasta(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, GSClient.ParametrosGenerales.giINSTWEB, GSClient.ParametrosGenerales.giFSP_CIA, _
                                     GSClient.ParametrosGenerales.gsFSP_SRV, GSClient.ParametrosGenerales.gsFSP_BD, sDirPlantilla, sFrom, ParametrosInstalacion.gsPetOfeSubMailSubject, _
                                     GSClient.ParametrosGenerales.gsURLSUMMIT)
    End Function

    '<summary>Presenta en pantalla los emails a proveedores</summary>   
    '<param name="oEmails">Objeto de tipo GSServerModel.EMails con los datos de los correos a mostrar</param>    
    '<remarks>Llamada desde: cmdAceptar_Click  </remarks>

    Private Sub MostrarCorreos(ByVal oEmails As GSServerModel.EMails)
        'deshabilitar el resto de controles                    
        PantallaDeshabilitada.Visibility = Visibility.Visible

        'Añadir el nuevo control y controladores de eventos
        Dim oCorreo As New MensajeCorreo
        oCorreo.EMails = oEmails
        AddHandler oCorreo.EnviarCorreoClick, AddressOf EnviarCorreo_Click
        AddHandler oCorreo.CancelarEnvioCorreoClick, AddressOf CancelarEnvioCorreo_Click

        pnModifImportes.Children.Add(oCorreo)
        pnModifImportes.Visibility = Visibility.Visible
        pnModifImportes.IsEnabled = True
    End Sub

    '<summary>Realiza la comunicación con los proveedores</summary>   
    '<param name="oEmail">Objeto de tipo GSServerModel.EMail con los datos del correo a mostrar</param>         
    '<remarks>Llamada desde: cmdAceptar_Click  </remarks>

    Private Function ComunicarProveedores(ByVal oDatos As GSServerModel.DatosNotifSubasta) As Boolean
        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        If GSClient.ParametrosGenerales.giINSTWEB <> TipoInstWeb.SinWeb Then
            If CDate(oProceso.FechaLimiteOfertas) < Date.Today Then
                Mensajes.ImposiblePublicarCaducado()
                Exit Function
            End If
        End If

        Dim sIdioma As String = ModulePublic.ParametrosInstalacion.gsIdioma

        'Notificar
        Dim oProcRule As New GSClient.ProcesosRule
        Dim oError As GSServerModel.GSException = oProcRule.NotificacionSubasta(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, GSClient.ParametrosGenerales.giINSTWEB,
                                                             GSClient.ParametrosGenerales.giFSP_CIA, GSClient.ParametrosGenerales.gsFSP_SRV, GSClient.ParametrosGenerales.gsFSP_BD, oDatos, GSClient.ParametrosGenerales.gbUsarRemitenteEmpresa)
        If oError.Number <> ErroresGS.TESnoerror Then
            If oError.Number = ErroresGS.TESAtribEspObl Then
                Dim oTextos As New GSClient.CTextos
                oError.ErrorStr.Replace("@PROCESO", oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 27))
                oError.ErrorStr.Replace("@GRUPO", oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 28))
                oError.ErrorStr.Replace("@ITEM", oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 29))
            End If

            GSClient.TratarError(oError)
        End If

        'Refrescar datos
        Refresh()
    End Function

    '<summary>Comprueba que los datos que se han introducido son correctos</summary>   
    '<returns>Boolean indicando si los datos son correctos</returns>    
    '<remarks>Llamada desde: cmdAceptar_Click  </remarks>

    Private Function ComprobarDatos() As Boolean
        Dim bComprobarDatos As Boolean = True        

        If xamtxtMinEspera.Text.Length = 0 Then
            bComprobarDatos = False
            Mensajes.CampoVacio(txtbMinEspera.Text)
            Exit Function
        End If

        If fsnfsReglas.PendingUploads.Count > 0 Then
            bComprobarDatos = False
            Mensajes.ReglasPendientes()
            Exit Function
        End If

        bComprobarDatos = ComprobarFechasSubasta()

        'Comprobar importes de bajada mínima ganadora y de proveedor
        If bComprobarDatos Then
            Dim oSubastaView = CType(Me.GetBindingObject("Procesos"), SubastasView).Item(0)
            Dim oSubasta As GSServerModel.ProcesoSubasta = Me.GetBindingObject("Subasta")

            Dim bBajMinGanOK As Boolean
            Dim bBajMinProve As Boolean
            oSubastaView.ComprobarImportes(oSubasta.BajadaMinGanador, oSubasta.BajadaMinProve, bBajMinGanOK, bBajMinProve)
            If Not (bBajMinGanOK And bBajMinProve) Then
                bComprobarDatos = False
                If Not bBajMinGanOK Then
                    Mensajes.ImportesBajMinGanNoEstablecidos()
                Else
                    If Not bBajMinProve Then Mensajes.ImportesBajMinProveNoEstablecidos()
                End If
            End If
        End If

        Return bComprobarDatos
    End Function

    '<summary>Comprueba que los datos de las fechas sean correctos</summary>   
    '<returns>Boolean indicando si los datos son correctos</returns>    
    '<remarks>Llamada desde: ComprobarDatos  </remarks>

    Private Function ComprobarFechasSubasta() As Boolean
        ComprobarFechasSubasta = False

        'Comprobar fechas inicio y fin subasta
        If xamdteInicio.Text.Length = 0 Then
            Mensajes.NoValido(txtbInicioSub.Text)
            xamdteInicio.Focus()
            Exit Function
        End If
        If xamdteCierre.Text.Length = 0 Then
            Mensajes.NoValido(txtbCierreSub.Text)
            xamdteCierre.Focus()
            Exit Function
        End If
        If Date.Compare(xamdteInicio.Value, xamdteCierre.Value) > 0 Then
            Mensajes.MensajeFechaCierreSubastaIncorrecta()
            Exit Function
        End If

        'Comprobar fecha apertura proceso        
        If Not _dtFecApe.Equals(Date.MinValue) Then
            If Date.Compare(xamdteInicio.Value, _dtFecApe) < 0 Then
                Mensajes.FechaIniSubMenorQueApertura()
                Exit Function
            End If
            If Date.Compare(xamdteCierre.Value, _dtFecApe) < 0 Then
                Mensajes.FechaCierreSubMenorQueApertura()
                Exit Function
            End If
        End If

        'Comprobar Fecha presentación
        If Not _dtFecPres.Equals(Date.MinValue) Then
            If Date.Compare(_dtFecPres, xamdteCierre.Value) < 0 Then
                If _bPermAdjDir Then
                    Mensajes.FechaMayorQuePresentacion(False)
                Else
                    Mensajes.FechaMayorQuePresentacion(True)
                End If
                Exit Function
            End If
        End If

        'Comprobar fecha apertura sobre         
        If xamceTipoSub.Value = TipoSubasta.SobreCerrado Then
            If xamdteHoraSobre.Text.Length = 0 Then
                Mensajes.NoValido(txtbHoraSobre.Text.Substring(0, txtbHoraSobre.Text.Length - 1))
                xamdteHoraSobre.Focus()
                Exit Function
            Else
                If xamdteHoraSobre.Text.Length > 0 Then
                    If Date.Compare(xamdteHoraSobre.Value, xamdteCierre.Value) > 0 Then
                        Mensajes.MensajeHoraAperturaSobreIncorrecta()
                        xamdteHoraSobre.Focus()
                        Exit Function
                    ElseIf Date.Compare(xamdteInicio.Value, xamdteHoraSobre.Value) > 0 Then
                        Mensajes.MensajeHoraAperturaSobreIncorrecta()
                        xamdteHoraSobre.Focus()
                        Exit Function
                    End If
                End If
            End If
        End If

        ComprobarFechasSubasta = True
    End Function

    Private Function PublicacionProveedores(ByVal bPublicar As Boolean) As Boolean
        PublicacionProveedores = False

        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
        If bPublicar And Date.Compare(xamdteCierre.Value, Date.Today) > 0 Then
            If (GSClient.ParametrosGenerales.giINSTWEB = TipoInstWeb.ConPortal Or GSClient.ParametrosGenerales.giINSTWEB = TipoInstWeb.conweb) And oProceso.Estado >= TipoEstadoProceso.conasignacionvalida Then
                oProceso.FechaLimiteOfertas = xamdteCierre.Value

                Dim oProveedores As GSServerModel.Proveedores
                Dim oProceRule As New GSClient.ProcesosRule
                If ModulePublic.oUsu.Tipo = TipoDeUsuario.Comprador Then
                    Dim oUsuRule As New UsuarioRule
                    Dim oPersona As GSServerModel.Persona = oUsuRule.DevolverPersona(ModulePublic.oUsu.Persona)
                    If Not (ModulePublic.oUsu.Acciones.Item(CType(Fullstep.FSNLibrary.TiposDeDatos.AccionesDeSeguridad.COMUNIPROVERestAsigEqp, Long)) Is Nothing) Then
                        'Si tiene restricción de comunicar solo los proves de su equipo sólo se publican esos
                        oProveedores = oProceRule.DevolverProveedoresAPublicar(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, oPersona.Equipo)
                    ElseIf Not (ModulePublic.oUsu.Acciones.Item(CType(Fullstep.FSNLibrary.TiposDeDatos.AccionesDeSeguridad.COMUNIPROVERestComprador, Long)) Is Nothing) Then
                        oProveedores = oProceRule.DevolverProveedoresAPublicar(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, oPersona.Equipo, oPersona.Codigo)
                    Else
                        oProveedores = oProceRule.DevolverProveedoresAPublicar(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
                    End If
                Else
                    oProveedores = oProceRule.DevolverProveedoresAPublicar(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
                End If

                'Quitar lo proveedores que no están en la subasta
                Dim oProves As GSServerModel.ProcesoProveedores = Me.GetBindingObject("Proveedores")
                Dim oProvesPublicar As New GSServerModel.ProcesoProveedores
                For Each oProve As GSServerModel.Proveedor In oProveedores
                    If Not oProves.Item(oProve.Codigo) Is Nothing AndAlso oProves.Item(oProve.Codigo).Subasta Then
                        oProvesPublicar.Add(oProves.Item(oProve.Codigo))
                    End If
                Next

                If oProvesPublicar.Count > 0 Then                                        
                    Dim oProcesoDef As GSServerModel.ProcesoDef = Me.GetBindingObject("ProcesoDef")
                    If Not oProcesoDef.ValAtrEsp Then
                        'Comprobacion de atributo de epecificacion obligatorios con validacion en la publicacion                            
                        Dim oVal As GSServerModel.ValidacionAtributos = oProceRule.ValidarAtributosEspObligatorios(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, TValidacionAtrib.Publicacion)
                        If Not oVal.AtributosCorrectos Then
                            Dim oTextos As New GSClient.CTextos
                            Dim sProceso As String = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 27)
                            Dim sGrupo As String = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 28)
                            Dim sItem As String = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 29)

                            Dim sCadena As String = String.Empty
                            For Each oAtribEsp As GSServerModel.ProcesoAtributoEspecificacion In oVal.AtributosProceso
                                sCadena &= oAtribEsp.Denominacion
                                sCadena &= " " & sProceso & Environment.NewLine
                            Next
                            For Each oAtribEsp As GSServerModel.ProcesoGrupoAtributoEspecificacion In oVal.AtributosPorcesoGrupo
                                sCadena &= " " & sGrupo & Environment.NewLine
                            Next
                            For Each oAtribEsp As GSServerModel.ItemAtributoEspecificacion In oVal.AtributosItem
                                sCadena &= " " & sItem & Environment.NewLine
                            Next

                            Mensajes.AtributosEspObl(sCadena, TValidacionAtrib.Publicacion)
                        Else
                            oProcesoDef.ValAtrEsp = True

                            'Activa la publicación
                            Dim oProcRules As New GSClient.ProcesosRule
                            Dim oError As GSServerModel.GSException = oProcRules.ActivarPublicacionProveedores(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, oProvesPublicar, GSClient.ParametrosGenerales.giINSTWEB, _
                                                                                                               GSClient.ParametrosGenerales.giFSP_CIA, GSClient.ParametrosGenerales.gsFSP_SRV, GSClient.ParametrosGenerales.gsFSP_BD)
                            If oError.Number <> ErroresGS.TESnoerror Then
                                GSClient.TratarError(oError)
                                Exit Function
                            End If
                        End If
                    Else
                        'Activa la publicación
                        Dim oProcRules As New GSClient.ProcesosRule
                        Dim oError As GSServerModel.GSException = oProcRules.ActivarPublicacionProveedores(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, oProvesPublicar, GSClient.ParametrosGenerales.giINSTWEB, _
                                                                                                           GSClient.ParametrosGenerales.giFSP_CIA, GSClient.ParametrosGenerales.gsFSP_SRV, GSClient.ParametrosGenerales.gsFSP_BD)
                        If oError.Number <> ErroresGS.TESnoerror Then
                            GSClient.TratarError(oError)
                            Exit Function
                        End If
                    End If                    
                End If
            End If
        Else
            'Proveedores a publicar
            If (GSClient.ParametrosGenerales.giINSTWEB = TipoInstWeb.ConPortal Or GSClient.ParametrosGenerales.giINSTWEB = TipoInstWeb.conweb) And oProceso.Estado >= TipoEstadoProceso.conasignacionvalida Then
                If (Not _oProvesPublicar Is Nothing AndAlso _oProvesPublicar.Count > 0) Then
                    'Activa la publicación
                    Dim oProcRules As New GSClient.ProcesosRule
                    Dim oError As GSServerModel.GSException = oProcRules.ActivarPublicacionProveedores(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, _oProvesPublicar, GSClient.ParametrosGenerales.giINSTWEB, _
                                                                                                       GSClient.ParametrosGenerales.giFSP_CIA, GSClient.ParametrosGenerales.gsFSP_SRV, GSClient.ParametrosGenerales.gsFSP_BD)
                    If oError.Number <> ErroresGS.TESnoerror Then
                        GSClient.TratarError(oError)
                        Exit Function
                    End If
                End If

                'Proveedores a despublicar
                If Not _oProvesDespublicar Is Nothing AndAlso _oProvesDespublicar.Count > 0 Then
                    Dim oProcRules As New GSClient.ProcesosRule
                    Dim oError As GSServerModel.GSException = oProcRules.DesactivarPublicacionProveedores(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, _oProvesDespublicar, GSClient.ParametrosGenerales.giINSTWEB, _
                                                                                                          GSClient.ParametrosGenerales.gbPremium, GSClient.ParametrosGenerales.giFSP_CIA, GSClient.ParametrosGenerales.gsFSP_SRV, GSClient.ParametrosGenerales.gsFSP_BD)
                    If oError.Number <> ErroresGS.TESnoerror Then
                        GSClient.TratarError(oError)
                        Exit Function
                    End If
                End If
            End If
        End If

        Return True
    End Function

    Private Sub cmdCancelar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdCancelar.Click
        'Me.ParentPage.RemoveChild(Me)
        'Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown
        'Application.Current.Shutdown()

        Me.CloseParentWindow()
    End Sub

    '<summary>Se carga el presupuesto unitario de los items como precio de salida</summary>             

    Private Sub btnCargarPresPrecSal_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnCargarPresPrecSal.Click        
        Dim oSubasta As SubastaView = CType(Me.GetBindingObject("Procesos"), SubastasView).Item(0)
        If Not oSubasta Is Nothing Then            
            For Each oGrupo As GrupoView In oSubasta.Grupos                
                For Each oItem As ItemView In oGrupo.Items
                    oItem.PrecioSalida = oItem.Presupuesto
                Next
            Next
        End If
    End Sub

    '<summary>Abre una ventana para introducir modificaciones sobre varios registros a la vez</summary>    

    Private Sub btnModifValor_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnModifValor.Click
        cpImportes.IsPinned = True

        If Not xamdgEstrucProce.SelectedItems Is Nothing AndAlso xamdgEstrucProce.SelectedItems.Count > 0 Then
            'Fijar el panel de importes si no lo está
            _bPanelImportePinned = cpImportes.IsPinned

            'deshabilitar el resto de controles                        
            PantallaDeshabilitada.Visibility = Visibility.Visible

            'Añadir el nuevo control y controladores de eventos
            Dim oModifImportesCtrl As New OpcionesModificacionImportes
            oModifImportesCtrl.BajMinProveVisible = xamceEstBajMinProve.IsChecked
            oModifImportesCtrl.BajMinGanVisible = xamceEstBajadaMin.IsChecked
            AddHandler oModifImportesCtrl.OkClick, AddressOf ModifImporte_OkClick
            AddHandler oModifImportesCtrl.CloseClick, AddressOf ModifImporte_CloseClick

            pnModifImportes.Children.Add(oModifImportesCtrl)
            pnModifImportes.Visibility = Visibility.Visible
            pnModifImportes.IsEnabled = True
        Else
            Mensajes.SeleccionarFilaGrid()
        End If
    End Sub

    '<summary>Controlador de evento. Simula el cierre de una ventana modal para el control de modificación de importes</summary> 

    Private Sub ModifImporte_CloseClick()
        pnModifImportes.Children.RemoveAt(pnModifImportes.Children.Count - 1)
        'For Each item As UIElement In Grid.Children
        '    item.IsEnabled = True
        'Next
        PantallaDeshabilitada.Visibility = Visibility.Hidden

        'Dejar el panel de importes como estaba
        cpImportes.IsPinned = _bPanelImportePinned
    End Sub

    '<summary>Controlador de evento. Modifica los importes de los registros seleccionados según los criterios indicados en el parámetro e</summary>           
    '<param name="e">Objeto de tipo DatosModifImporte que indica los citerios de modificación de importes</param>   

    Private Sub ModifImporte_OkClick(ByVal e As DatosModifImporte)
        ModifImporte_CloseClick()

        Dim oSubasta As SubastaView = CType(Me.GetBindingObject("Procesos"), SubastasView).Item(0)        

        For Each oRecord As DataRecord In xamdgEstrucProce.SelectedItems
            Select Case oRecord.FieldLayout.Key
                Case "FL1"
                    If e.PrecioSalida Then
                        oSubasta.PrecioSalida = CambioCantidad(oSubasta.PrecioSalida, e.Incrementar, e.Porcentaje, e.Cantidad)
                    End If
                    If e.BajadaMinGanadora Then
                        oSubasta.MinPujaGanadora = CambioCantidad(oSubasta.MinPujaGanadora, e.Incrementar, e.Porcentaje, e.Cantidad)
                    End If
                    If e.BajadaMinProveedor Then
                        oSubasta.MinPujaProve = CambioCantidad(oSubasta.MinPujaProve, e.Incrementar, e.Porcentaje, e.Cantidad)
                    End If
                Case "FL2"
                    Dim oGrupo As GrupoView = oSubasta.Grupos.Find(oRecord.Cells("Anyo").Value, oRecord.Cells("GMN1").Value, oRecord.Cells("Proce").Value, oRecord.Cells("ID").Value)                    
                    If Not oGrupo Is Nothing Then
                        If e.PrecioSalida Then
                            oGrupo.PrecioSalida = CambioCantidad(oGrupo.PrecioSalida, e.Incrementar, e.Porcentaje, e.Cantidad)
                        End If
                        If e.BajadaMinGanadora Then
                            oGrupo.MinPujaGanadora = CambioCantidad(oGrupo.MinPujaGanadora, e.Incrementar, e.Porcentaje, e.Cantidad)
                        End If
                        If e.BajadaMinProveedor Then
                            oGrupo.MinPujaProve = CambioCantidad(oGrupo.MinPujaProve, e.Incrementar, e.Porcentaje, e.Cantidad)
                        End If
                    End If
                Case "FL3"
                    Dim oGrupo As GrupoView = oSubasta.Grupos.Find(oRecord.Cells("Anyo").Value, oRecord.Cells("GMN1_PROCE").Value, oRecord.Cells("Proce").Value, oRecord.Cells("Grupo").Value)                    
                    If Not oGrupo Is Nothing Then
                        Dim oItem As ItemView = oGrupo.Items.Find(oRecord.Cells("Anyo").Value, oRecord.Cells("GMN1_PROCE").Value, oRecord.Cells("Proce").Value, oRecord.Cells("ID").Value)                        
                        If Not oItem Is Nothing Then
                            If e.PrecioSalida Then
                                oItem.PrecioSalida = CambioCantidad(oItem.PrecioSalida, e.Incrementar, e.Porcentaje, e.Cantidad)
                            End If
                            If e.BajadaMinGanadora Then
                                oItem.MinPujaGanadora = CambioCantidad(oItem.MinPujaGanadora, e.Incrementar, e.Porcentaje, e.Cantidad)
                            End If
                            If e.BajadaMinProveedor Then
                                oItem.MinPujaProve = CambioCantidad(oItem.MinPujaProve, e.Incrementar, e.Porcentaje, e.Cantidad)
                            End If
                        End If
                    End If
            End Select
        Next

        'Dejar el panel de importes como estaba
        cpImportes.IsPinned = _bPanelImportePinned
    End Sub

    '<summary>Acciones al aceptar el envío de correos</summary>    

    Private Sub EnviarCorreo_Click(ByVal oEmails As GSServerModel.EMails)
        Me.Cursor = Cursors.Wait

        _oDatos.EMailsSubasta = oEmails
        ComunicarProveedores(_oDatos)
        OcultarPanelEMail()

        Me.Cursor = Cursors.Arrow

        Me.CloseParentWindow()
    End Sub

    '<summary>Acciones al cancelar el evío de correos</summary>    

    Private Sub CancelarEnvioCorreo_Click()
        OcultarPanelEMail()

        Me.Cursor = Cursors.Arrow

        Me.CloseParentWindow()
    End Sub

    '<summary>Oculta el panel de los correos</summary> 

    Private Sub OcultarPanelEMail()
        pnModifImportes.Children.RemoveAt(pnModifImportes.Children.Count - 1)
        'For Each item As UIElement In Grid.Children
        '    item.IsEnabled = True
        'Next
        PantallaDeshabilitada.Visibility = Visibility.Hidden
    End Sub

    Private Function CambioCantidad(ByVal CantidadModificar As Nullable(Of Double), ByVal Incremento As Boolean, ByVal Porcentaje As Boolean, ByVal Cantidad As Double) As Nullable(Of Double)
        Dim dblCantidadModificar As Nullable(Of Double) = CantidadModificar
        If CantidadModificar Is Nothing And Not Porcentaje Then dblCantidadModificar = 0
        If Not dblCantidadModificar Is Nothing Then
            Dim dblCambio As Double
            If Porcentaje Then
                dblCambio = dblCantidadModificar * Cantidad / 100
            Else
                dblCambio = Cantidad
            End If
            If Not Incremento Then dblCambio = -dblCambio
            dblCantidadModificar = dblCantidadModificar + dblCambio

            If dblCantidadModificar < 0 Then dblCantidadModificar = 0
        End If

        Return dblCantidadModificar
    End Function

#End Region

#Region " Eventos Grid Proveedores "

    Private Sub xamdgProveedores_CellChanged(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellChangedEventArgs) Handles xamdgProveedores.CellChanged
        Select Case e.Cell.Field.Name
            Case "Subasta"
                HabilitarComunicar(e.Cell.Record.Cells("Proveedor").Value, e.Editor.Value)

                Dim oProves As GSServerModel.ProcesoProveedores = Me.GetBindingObject("Proveedores")
                If e.Editor.Value Then
                    If Not _oProvesDespublicar.Item(e.Cell.Record.Cells("Proveedor").Value) Is Nothing Then
                        _oProvesDespublicar.Remove(oProves.Item(e.Cell.Record.Cells("Proveedor").Value))
                    Else
                        _oProvesPublicar.Add(oProves.Item(e.Cell.Record.Cells("Proveedor").Value))
                    End If
                Else
                    If Not _oProvesPublicar.Item(e.Cell.Record.Cells("Proveedor").Value) Is Nothing Then
                        _oProvesPublicar.Remove(oProves.Item(e.Cell.Record.Cells("Proveedor").Value))
                    Else
                        _oProvesDespublicar.Add(oProves.Item(e.Cell.Record.Cells("Proveedor").Value))
                    End If
                End If
        End Select
    End Sub

    Private Sub xamdgProveedores_RecordActivated(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordActivatedEventArgs) Handles xamdgProveedores.RecordActivated
        If TypeOf (e.Record) Is DataRecord Then
            Dim oRecord As DataRecord = CType(e.Record, DataRecord)
            If oRecord.Cells("CodigoPortal").Value = String.Empty Then
                oRecord.IsActive = False
            End If
        End If
    End Sub

#End Region

#Region " Eventos Grid Estructura "

    'Private Sub OnLabelPresenterSizeChanged(ByVal sender As Object, ByVal e As SizeChangedEventArgs)
    '    Dim oPres As LabelPresenter = CType(sender, LabelPresenter)

    '    If oPres Is Nothing OrElse oPres.Field.Owner.Key <> _oFL1.Key Then Return

    '    'Ignore tiny changes because they can lead to infinite layout loops.
    '    If oPres.Field.Visibility = Visibility.Visible Then
    '        'NaN --> Not a Number: Todavía no se ha definido
    '        'If Not Double.IsNaN(oPres.Field.Settings.LabelWidth) Then
    '        Dim dblDiff As Double = Math.Abs(oPres.Field.Settings.LabelWidth - oPres.ActualWidth)
    '        If dblDiff <= 1 Then Return

    '        'Set the LabelWidth property so that the LabelWidthResolved is recalculated.
    '        'That forces the binding to update the width of the corresponding field in the detail layout.
    '        oPres.Field.Settings.LabelWidth = oPres.ActualWidth
    '        'End If
    '    End If
    'End Sub

    'Private Sub xamdgEstrucProce_FieldLayoutInitialized(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.FieldLayoutInitializedEventArgs) Handles xamdgEstrucProce.FieldLayoutInitialized
    'If e.FieldLayout.Key = "FL1" AndAlso _oFL1 Is Nothing Then
    '    _oFL1 = e.FieldLayout
    'ElseIf e.FieldLayout.Key = "FL2" Or e.FieldLayout.Key = "FL3" Then
    '        '        '        ''Get all of the visible fields in the master layout.
    '        '        '        'Dim masterFields As List(Of Field) = (From f In _oFL1.Fields Where f.VisibilityResolved = Visibility.Visible Select f).ToList()

    '        '        '        ''Get all of the visible fields in the detail layout.
    '        '        '        'Dim detailFields As List(Of Field) = (From f In e.FieldLayout.Fields Where f.VisibilityResolved = Visibility.Visible Select f).ToList()

    '        '        '        'Dim iterations As Integer = Math.Min(masterFields.Count, detailFields.Count)

    '        '        '        ''Bind the width of each field in the detail layout to the resolved/actual width of the corresponding field in the master layout.
    '        '        '        'For n As Integer = 0 To iterations - 1
    '        '        '        '    Dim oBinding As New Binding()
    '        '        '        '    oBinding.Path = New PropertyPath("LabelWidthResolved")
    '        '        '        '    oBinding.Source = masterFields(n)

    '        '        '        '    BindingOperations.SetBinding(detailFields(n).Settings, FieldSettings.LabelWidthProperty, oBinding)
    '        '        '        'Next

    'Dim oBinding As New Binding("LabelWidthResolved")
    'oBinding.Source = _oFL1.Fields("MinPujaProve")
    'BindingOperations.SetBinding(e.FieldLayout.Fields("MinPujaProve").Settings, FieldSettings.LabelWidthProperty, oBinding)            
    '    End If
    'End Sub

    Private Sub xamdgEstrucProce_CellUpdated(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellUpdatedEventArgs) Handles xamdgEstrucProce.CellUpdated
        If TypeOf e.Record Is DataRecord Then
            If e.Field.Name = "PrecioSalida" Then
                Dim oRecord As DataRecord = CType(e.Record, DataRecord)
                Dim oSubastasView As SubastasView = Me.GetBindingObject("Procesos")
                Dim oSubastaView As SubastaView = oSubastasView(0)

                'Actualizar el Presupuesto del grupo y del proceso al que pertenece el item
                'Select Case e.Record.FieldLayout.Key
                '    Case "FL3"
                '        Dim oGrupo As GrupoView = oSubastaView.Grupos.Find(oRecord.Cells("Anyo").Value, oRecord.Cells("GMN1_PROCE").Value, oRecord.Cells("Proce").Value, oRecord.Cells("Grupo").Value)
                '        Dim dblPrecio As Nullable(Of Double)
                '        For Each oItem As ItemView In oGrupo.Items
                '            If Not oItem.PrecioSalida Is Nothing Then
                '                If dblPrecio Is Nothing Then dblPrecio = 0
                '                dblPrecio += (oItem.PrecioSalida * oItem.Cantidad)
                '            End If
                '        Next
                '        oGrupo.PrecioSalida = dblPrecio

                '        dblPrecio = Nothing
                '        For Each oGrupoSub As GrupoView In oSubastaView.Grupos
                '            If Not oGrupoSub.PrecioSalida Is Nothing Then
                '                If dblPrecio Is Nothing Then dblPrecio = 0
                '                dblPrecio += oGrupoSub.PrecioSalida
                '            End If                            
                '        Next
                '        oSubastaView.PrecioSalida = dblPrecio
                '    Case ("FL2")
                '        Dim dblPrecio As Nullable(Of Double)
                '        For Each oGrupoSub As GrupoView In oSubastaView.Grupos
                '            If Not oGrupoSub.PrecioSalida Is Nothing Then
                '                If dblPrecio Is Nothing Then dblPrecio = 0
                '                dblPrecio += oGrupoSub.PrecioSalida
                '            End If                            
                '        Next
                '        oSubastaView.PrecioSalida = dblPrecio
                '        CType(xamdgEstrucProce.Records(0), DataRecord).Cells("PrecioSalida").Value = dblPrecio
                'End Select

                oSubastaView.CalcularHabilitacion()
            End If
        End If
    End Sub

    Private Sub xamdgEstrucProce_InitializeRecord(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.InitializeRecordEventArgs) Handles xamdgEstrucProce.InitializeRecord
        If Not e.Record Is Nothing Then
            Select Case e.Record.FieldLayout.Key
                Case "FL1"
                    If TypeOf e.Record Is DataRecord Then
                        'Se obtienen los datos del proceso porque si se trata de un proceso ya existente que se convierte a subasta todavía no hay datos en PROCE_SUBASTA
                        Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")

                        Dim oRecord As DataRecord = CType(e.Record, DataRecord)
                        oRecord.Cells("Proceso").Value = oProceso.Anyo & "/" & oProceso.GMN1 & "/" & oProceso.Codigo & " " & oProceso.Denominacion
                    End If
                Case "FL2"
                    If TypeOf e.Record Is DataRecord Then
                        Dim oRecord As DataRecord = CType(e.Record, DataRecord)
                        oRecord.Cells("Grupo").Value = oRecord.Cells("Cod").Value & " - " & oRecord.Cells("Denominacion").Value
                    End If
                Case "FL3"
                    If TypeOf e.Record Is DataRecord Then
                        Dim oRecord As DataRecord = CType(e.Record, DataRecord)
                        oRecord.Cells("Art").Value = oRecord.Cells("Articulo").Value & " - " & oRecord.Cells("Descripcion").Value
                    End If
            End Select
        End If
    End Sub

    Private Sub OnHeaderRightClick(ByVal sender As Object, ByVal e As MouseEventArgs)
        Dim oLabelPresenter As LabelPresenter = CType(sender, LabelPresenter)
        'El handler se ha registrado para la calse LabelPresenter, luego saltará para todos los grids
        'Hay que discriminar para el grid de importes
        If oLabelPresenter.DataPresenter.Name = "xamdgEstrucProce" Then
            Select Case oLabelPresenter.Field.Name
                Case "MinPujaGanadora", "MinPujaProve"
                    xamdgEstrucProce.ContextMenu = New MenuPrecios(CType(Me.GetBindingObject("Subasta"), GSServerModel.ProcesoSubasta), xamdgEstrucProce, oLabelPresenter.Field.Name)
                Case Else
                    xamdgEstrucProce.ContextMenu = Nothing
            End Select
        End If
    End Sub

    Private Sub CopiarImportesButton_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs)
        If Not _bSoloLectura Then
            'Obtener el valor
            xamdgEstrucProce.ActiveCell.EndEditMode()
            If Not xamdgEstrucProce.ActiveCell.Value Is Nothing Then
                Dim dblImporte As Double = xamdgEstrucProce.ActiveCell.Value

                Dim oSubasta As SubastaView = CType(Me.GetBindingObject("Procesos"), SubastasView).Item(0)
                Select Case xamdgEstrucProce.ActiveRecord.FieldLayout.Key
                    Case "FL2"
                        For Each oGrupoView As GrupoView In oSubasta.Grupos
                            Select Case xamdgEstrucProce.ActiveCell.Field.Name
                                Case "MinPujaGanadora"
                                    oGrupoView.MinPujaGanadora = dblImporte
                                Case "MinPujaProve"
                                    oGrupoView.MinPujaProve = dblImporte
                            End Select
                        Next
                    Case "FL3"
                        For Each oGrupoview As GrupoView In oSubasta.Grupos
                            For Each oItemView As ItemView In oGrupoview.Items
                                Select Case xamdgEstrucProce.ActiveCell.Field.Name
                                    Case "MinPujaGanadora"
                                        oItemView.MinPujaGanadora = dblImporte
                                    Case "MinPujaProve"
                                        oItemView.MinPujaProve = dblImporte
                                End Select
                            Next
                        Next
                End Select
            End If
        End If
    End Sub

#End Region

#Region " Eventos controles "

    Private Sub xamceTipoSub_ValueChanged(ByVal sender As Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of Object)) Handles xamceTipoSub.ValueChanged
        If e.NewValue = TipoSubasta.SobreCerrado Then
            pnHoraSobre.Visibility = Windows.Visibility.Visible

            'Establecer la fecha de apertura de sobre por defecto
            Dim oSubasta As GSServerModel.ProcesoSubasta = Me.GetBindingObject("Subasta")
            If oSubasta.FechaAperturaSobre Is Nothing Then
                Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")
                oSubasta.FechaAperturaSobre = oProceso.FechaLimiteOfertas
            End If
        Else
            pnHoraSobre.Visibility = Windows.Visibility.Collapsed
        End If
    End Sub

    '<summary>Configura las columnas de bajadas del grid de importes</summary>          
    '<remarks>Llamada desde: xamdmDatosOp_ActivePaneChanged  </remarks>

    Private Sub ConfigurarGridImportes()
        Dim oSubasta As GSServerModel.ProcesoSubasta = CType(Me.GetBindingObject("Subasta"), GSServerModel.ProcesoSubasta)

        'Si se ha marcado establecer bajada mínima para pujas ganadoras
        If oSubasta.BajadaMinGanador Then
            VisualizacionColumna("MinPujaGanadora", Windows.Visibility.Visible)

            Select Case oSubasta.BajadaMinGanadorTipo
                Case TipoBajadaSubasta.PorPorcentaje
                    AsignarEstiloColumna("MinPujaGanadora", "FieldEstiloPorcentaje")
                Case TipoBajadaSubasta.PorImporte
                    AsignarEstiloColumna("MinPujaGanadora", "FieldEstiloImporte")
            End Select
        Else
            VisualizacionColumna("MinPujaGanadora", Windows.Visibility.Collapsed)
        End If

        'Si se ha marcado establecer bajada mínima para pujas por proveedor
        If oSubasta.BajadaMinProve Then
            VisualizacionColumna("MinPujaProve", Windows.Visibility.Visible)

            Select Case oSubasta.BajadaMinProveTipo
                Case TipoBajadaSubasta.PorPorcentaje
                    AsignarEstiloColumna("MinPujaProve", "FieldEstiloPorcentaje")
                Case TipoBajadaSubasta.PorImporte
                    AsignarEstiloColumna("MinPujaProve", "FieldEstiloImporte")
            End Select
        Else
            VisualizacionColumna("MinPujaProve", Windows.Visibility.Collapsed)
        End If
    End Sub

    '<summary>Asigna la máscar indicada a la columna indicada</summary>
    '<param name="sFieldName">Nombre del campo</param>   
    '<param name="sStyleName">Estilo a asignar</param>   
    '<remarks>Llamada desde: xamdmDatosOp_ActivePaneChanged  </remarks>

    Private Sub AsignarEstiloColumna(ByVal sFieldName As String, ByVal sStyleName As String)
        Dim oStyle As Style = Me.FindResource(sStyleName)

        'Dim oStyle As New Style(GetType(XamNumericEditor))
        ''oStyle.Setters.Add(New Setter(XamNumericEditor.PromptCharProperty, " "))
        'oStyle.Setters.Add(New Setter(XamNumericEditor.MaskProperty, sMask))
        'oStyle.Setters.Add(New Setter(XamNumericEditor.FormatProperty, sFormat))

        xamdgEstrucProce.FieldLayouts("FL1").Fields(sFieldName).Settings.EditorType = GetType(XamNumericEditor)
        xamdgEstrucProce.FieldLayouts("FL1").Fields(sFieldName).Settings.EditorStyle = oStyle
        xamdgEstrucProce.FieldLayouts("FL2").Fields(sFieldName).Settings.EditorType = GetType(XamNumericEditor)
        xamdgEstrucProce.FieldLayouts("FL2").Fields(sFieldName).Settings.EditorStyle = oStyle
        xamdgEstrucProce.FieldLayouts("FL3").Fields(sFieldName).Settings.EditorType = GetType(XamNumericEditor)
        xamdgEstrucProce.FieldLayouts("FL3").Fields(sFieldName).Settings.EditorStyle = oStyle
    End Sub

    Private Sub VisualizacionColumna(ByVal sFieldName As String, ByVal oVis As Visibility)
        xamdgEstrucProce.FieldLayouts("FL1").Fields(sFieldName).Visibility = oVis
        xamdgEstrucProce.FieldLayouts("FL2").Fields(sFieldName).Visibility = oVis
        xamdgEstrucProce.FieldLayouts("FL3").Fields(sFieldName).Visibility = oVis
    End Sub

    Private Sub xamceEstBajadaMin_ValueChanged(ByVal sender As Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of Object)) Handles xamceEstBajadaMin.ValueChanged
        ConfigurarGridImportes()
    End Sub

    Private Sub xamceEstBajMinProve_ValueChanged(ByVal sender As Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of Object)) Handles xamceEstBajMinProve.ValueChanged
        ConfigurarGridImportes()
    End Sub

    Private Sub txtbPubProv_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles txtbPubProv.MouseDown
        If Not xamchkPubProv.IsReadOnly And xamchkPubProv.IsEnabled Then
            If e.LeftButton = System.Windows.Input.MouseButtonState.Pressed Then
                xamchkPubProv.IsChecked = Not xamchkPubProv.IsChecked
            End If
        End If
    End Sub

    Private Sub txtbNotifProv_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles txtbNotifProv.MouseDown
        If Not xamchkNotifProv.IsReadOnly Then
            If e.LeftButton = System.Windows.Input.MouseButtonState.Pressed Then
                xamchkNotifProv.IsChecked = Not xamchkNotifProv.IsChecked
            End If
        End If
    End Sub

    Private Sub xamtbEstBajMinProve_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbEstBajMinProve.MouseDown
        If Not xamceEstBajMinProve.IsReadOnly Then
            If e.LeftButton = System.Windows.Input.MouseButtonState.Pressed Then
                xamceEstBajMinProve.IsChecked = Not xamceEstBajMinProve.IsChecked
            End If
        End If
    End Sub

    Private Sub xamtbEstBajadaMin_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbEstBajadaMin.MouseDown
        If Not xamceEstBajadaMin.IsReadOnly Then
            If e.LeftButton = System.Windows.Input.MouseButtonState.Pressed Then
                xamceEstBajadaMin.IsChecked = Not xamceEstBajadaMin.IsChecked
            End If
        End If
    End Sub

    Private Sub xamtbMostrarDetPujas_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbMostrarDetPujas.MouseDown
        If Not xamceMostrarDetPujas.IsReadOnly Then
            If e.LeftButton = System.Windows.Input.MouseButtonState.Pressed Then
                xamceMostrarDetPujas.IsChecked = Not xamceMostrarDetPujas.IsChecked
            End If
        End If
    End Sub

    Private Sub txtbNoMostrarInfoPuja_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles txtbNoMostrarInfoPuja.MouseDown
        If Not xamceNoMostrarInfoPuja.IsReadOnly Then
            If e.LeftButton = System.Windows.Input.MouseButtonState.Pressed Then
                xamceNoMostrarInfoPuja.IsChecked = Not xamceNoMostrarInfoPuja.IsChecked
            End If
        End If
    End Sub

    Private Sub xamtbMostrarPrecioPujaGan_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbMostrarPrecioPujaGan.MouseDown
        If Not xamceMostrarPrecioPujaGan.IsReadOnly Then
            If e.LeftButton = System.Windows.Input.MouseButtonState.Pressed Then
                xamceMostrarPrecioPujaGan.IsChecked = Not xamceMostrarPrecioPujaGan.IsChecked
            End If
        End If
    End Sub

    Private Sub xamtbMostrarProveGan_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles xamtbMostrarProveGan.MouseDown
        If Not xamceMostrarProveGan.IsReadOnly Then
            If e.LeftButton = System.Windows.Input.MouseButtonState.Pressed Then
                xamceMostrarProveGan.IsChecked = Not xamceMostrarProveGan.IsChecked
            End If
        End If
    End Sub

    Private Sub xamchkPubProv_ValueChanged(ByVal sender As Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of Object)) Handles xamchkPubProv.ValueChanged
        If e.NewValue Then
            Dim oProceso As GSServerModel.Proceso = Me.GetBindingObject("Proceso")

            'Comprobar que ya se ha validado la selección de proveedores
            If oProceso.Estado < TipoEstadoProceso.conasignacionvalida Then
                Mensajes.ImposibleComunicarAsignacionNoValidada()
                xamchkPubProv.Value = False
                Exit Sub
            End If

            'Comprobar que no ha pasado ya la fecha de fin de subasta
            Dim oTZInfo As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(ModulePublic.oUsu.TimeZone)
            Dim dtTZNow As Date = TimeZoneInfo.ConvertTimeFromUtc(Date.UtcNow, oTZInfo)
            If Date.Compare(oProceso.FechaLimiteOfertas, dtTZNow) < 0 Then
                Mensajes.ImposibleComunicarSubasta()
                xamchkPubProv.Value = False
                Exit Sub
            End If

            'Comprobar si ya se ha hecho la comunicación a los proveedores
            Dim oSubasta As GSServerModel.ProcesoSubasta = Me.GetBindingObject("Subasta")
            If Not oSubasta.FechaUltPub Is Nothing Then
                If Mensajes.ProveedoresYaComunicados(oSubasta.FechaUltPub, Me.GetBindingObject("ProvesComunicados")) = MessageBoxResult.No Then xamchkPubProv.Value = False
            End If
        End If
    End Sub

    Private Sub xamceMostrarPrecioPujaGan_ValueChanged(ByVal sender As Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of Object)) Handles xamceMostrarPrecioPujaGan.ValueChanged
        If Not xamceMostrarPrecioPujaGan.IsChecked Then xamceMostrarDetPujas.IsChecked = False
    End Sub

    'Private Sub cpDatosOpcionales_RequestBringIntoView(ByVal sender As Object, ByVal e As System.Windows.RequestBringIntoViewEventArgs) Handles cpDatosOpcionales.RequestBringIntoView
    '    'pnDatosOpc.Width = cpDatosPpales.ActualWidth - 25
    '    pnDatosOpc.SetValue(WidthProperty, cpDatosPpales.ActualWidth - 25)
    'End Sub

    'Private Sub cpDatosOpcionales_SizeChanged(ByVal sender As Object, ByVal e As System.Windows.SizeChangedEventArgs) Handles cpDatosOpcionales.SizeChanged
    '    'cpDatosOpcionales.ClearValue(WidthProperty)
    'End Sub

    'Private Sub cpImportes_RequestBringIntoView(ByVal sender As Object, ByVal e As System.Windows.RequestBringIntoViewEventArgs) Handles cpImportes.RequestBringIntoView
    '    pnImportesPrecios.Width = cpDatosPpales.ActualWidth - 65
    'End Sub

    'Private Sub cpImportes_SizeChanged(ByVal sender As Object, ByVal e As System.Windows.SizeChangedEventArgs) Handles cpImportes.SizeChanged
    '    'cpImportes.ClearValue(WidthProperty)
    'End Sub

#End Region

#Region " Control Reglas "

    Private Sub fsnfsReglas_ButtonPressed() Handles fsnfsReglas.Adding, fsnfsReglas.Removing, fsnfsReglas.Opening, fsnfsReglas.SavingToDisk
        If Not cpDatosOpcionales.IsPinned Then cpDatosOpcionales.IsPinned = True
    End Sub

    Private Sub fsnfsReglas_OpenFileClick(ByVal Archivo As GSServerModel.File) Handles fsnfsReglas.OpenFileClick
        If Not Archivo Is Nothing Then
            Dim oSubastaReglas As GSServerModel.ProcesoSubastaReglas = Me.GetBindingObject("SubastaReglas")

            If Not oSubastaReglas Is Nothing AndAlso oSubastaReglas.Count > 0 Then
                Dim oRegla As GSServerModel.ProcesoSubastaRegla = oSubastaReglas.Item(Archivo.NombreArchivo)

                If Not oRegla Is Nothing Then
                    Me.Cursor = Cursors.Wait

                    If Not _DownloadManager.FileExists(oRegla.Nombre) Then
                        Dim oProceRule As New GSClient.ProcesosRule
                        Dim bOk As Boolean = oProceRule.DevolverArchivoSubastaRegla(_DownloadManager.DownloadFolder, oRegla.Nombre, oRegla.AdjunProce)
                        If bOk Then _DownloadManager.AddFile(oRegla.Nombre)
                    End If

                    _DownloadManager.OpenFile(oRegla.Nombre)

                    Me.Cursor = Cursors.Arrow
                End If
            End If
        End If
    End Sub

    Private Sub fsnfsReglas_SaveToDiskClick(ByVal Archivo As GSServerModel.File, ByVal Ruta As String) Handles fsnfsReglas.SaveToDiskClick
        If Not Archivo Is Nothing Then
            Dim oSubastaReglas As GSServerModel.ProcesoSubastaReglas = Me.GetBindingObject("SubastaReglas")

            If Not oSubastaReglas Is Nothing AndAlso oSubastaReglas.Count > 0 Then
                Dim oRegla As GSServerModel.ProcesoSubastaRegla = oSubastaReglas.Item(Archivo.NombreArchivo)

                If Not oRegla Is Nothing Then
                    Me.Cursor = Cursors.Wait

                    If Not _DownloadManager.FileExists(oRegla.Nombre) Then
                        Dim oProceRule As New GSClient.ProcesosRule
                        Dim bOk As Boolean = oProceRule.DevolverArchivoSubastaRegla(_DownloadManager.DownloadFolder, oRegla.Nombre, oRegla.AdjunProce)
                        If bOk Then _DownloadManager.AddFile(oRegla.Nombre)
                    End If

                    _DownloadManager.CopyFile(oRegla.Nombre, Ruta)

                    Me.Cursor = Cursors.Arrow
                End If
            End If
        End If
    End Sub

#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

#Region " Clase MenuPrecios "

    Private Class MenuPrecios
        Inherits ContextMenu

        Private WithEvents _oMenuItem1 As MenuItem
        Private WithEvents _oMenuItem2 As MenuItem
        Private _oSubasta As GSServerModel.ProcesoSubasta
        Private _oGrid As FSNDataGrid
        Private _FieldName As String

        Public Sub New(ByRef Subasta As GSServerModel.ProcesoSubasta, ByRef Grid As FSNDataGrid, ByVal FieldName As String)
            MyBase.New()

            _oSubasta = Subasta
            _oGrid = Grid
            _FieldName = FieldName

            LoadMenuItems()
        End Sub

        Private Sub LoadMenuItems()
            Dim oTextos As New CTextos

            _oMenuItem1 = New MenuItem
            _oMenuItem1.Name = "mnuPorcentaje"
            _oMenuItem1.Header = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 73)
            Me.Items.Add(_oMenuItem1)

            _oMenuItem2 = New MenuItem
            _oMenuItem2.Name = "mnuImporte"
            _oMenuItem2.Header = oTextos.DevolverTextosMensaje(ModuleName.FRM_SUBASTACONF, 74)
            Me.Items.Add(_oMenuItem2)
        End Sub

        Private Sub _oMenuItem1_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles _oMenuItem1.Click
            CambiarTipoBajada(TipoBajadaSubasta.PorPorcentaje)
            ChangeStyle("FieldEstiloPorcentaje")
        End Sub

        Private Sub _oMenuItem2_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles _oMenuItem2.Click
            CambiarTipoBajada(TipoBajadaSubasta.PorImporte)
            ChangeStyle("FieldEstiloImporte")
        End Sub

        Private Sub CambiarTipoBajada(ByVal TipoBajada As TipoBajadaSubasta)
            Select Case _FieldName
                Case "MinPujaGanadora"
                    _oSubasta.BajadaMinGanadorTipo = TipoBajada
                Case "MinPujaProve"
                    _oSubasta.BajadaMinProveTipo = TipoBajada
            End Select
        End Sub

        Private Sub ChangeStyle(ByVal sStyleName As String)
            Dim oStyle As Style = Me.FindResource(sStyleName)

            'Dim oStyle As New Style(GetType(XamNumericEditor))
            ''oStyle.Setters.Add(New Setter(XamNumericEditor.PromptCharProperty, " ")) 
            'oStyle.Setters.Add(New Setter(XamNumericEditor.MaskProperty, sMask))
            'oStyle.Setters.Add(New Setter(XamNumericEditor.FormatProperty, sFormat))

            _oGrid.FieldLayouts("FL1").Fields(_FieldName).Settings.EditorType = GetType(XamNumericEditor)
            _oGrid.FieldLayouts("FL1").Fields(_FieldName).Settings.EditorStyle = oStyle
            _oGrid.FieldLayouts("FL2").Fields(_FieldName).Settings.EditorType = GetType(XamNumericEditor)
            _oGrid.FieldLayouts("FL2").Fields(_FieldName).Settings.EditorStyle = oStyle
            _oGrid.FieldLayouts("FL3").Fields(_FieldName).Settings.EditorType = GetType(XamNumericEditor)
            _oGrid.FieldLayouts("FL3").Fields(_FieldName).Settings.EditorStyle = oStyle
        End Sub

    End Class

#End Region

#Region " Clase DownloadManager "

    Private Class DownloadManager

        Private _sDownloadFolder As String
        Private _arDownloadedFiles(-1) As String

        Public Sub New(ByVal DownloadFolder As String)
            If Not DownloadFolder.EndsWith("\") Then DownloadFolder &= "\"
            _sDownloadFolder = DownloadFolder
        End Sub

        Public Property DownloadFolder As String
            Get
                Return _sDownloadFolder
            End Get
            Set(ByVal value As String)
                _sDownloadFolder = value
            End Set
        End Property

        Public Sub AddFile(ByVal sFile As String)
            ReDim Preserve _arDownloadedFiles(_arDownloadedFiles.Length)
            _arDownloadedFiles(_arDownloadedFiles.Length - 1) = sFile
        End Sub

        Public Function FileExists(ByVal sFile As String) As Boolean
            FileExists = False

            For i As Integer = 0 To _arDownloadedFiles.Length - 1
                If _arDownloadedFiles(i).Equals(sFile) Then
                    FileExists = True
                    Exit For
                End If
            Next
        End Function

        Public Sub OpenFile(ByVal sFile As String)
            If System.IO.File.Exists(_sDownloadFolder & sFile) Then System.Diagnostics.Process.Start(_sDownloadFolder & sFile)
        End Sub

        Public Sub CopyFile(ByVal sFile As String, ByVal sDestFile As String)
            If System.IO.File.Exists(_sDownloadFolder & sFile) Then System.IO.File.Copy(_sDownloadFolder & sFile, sDestFile)
        End Sub

        Protected Overrides Sub Finalize()
            If Not _arDownloadedFiles Is Nothing AndAlso _arDownloadedFiles.Length > 0 Then
                For i As Integer = 0 To _arDownloadedFiles.Length - 1
                    Dim sFile As String = _sDownloadFolder & _arDownloadedFiles(i)
                    If System.IO.File.Exists(sFile) Then System.IO.File.Delete(sFile)
                Next

                _arDownloadedFiles = Nothing
            End If

            MyBase.Finalize()
        End Sub

    End Class

#End Region

#Region " ProcesosView classes "

    Private Class SubastasView
        Inherits GSServerModel.Lista(Of SubastaView)

        Public Sub New(ByVal oSubasta As GSServerModel.ProcesoSubasta, ByVal bSoloLectura As Boolean)
            MyBase.New(True, String.Empty, String.Empty, False)

            Dim oSubastaView As New SubastaView(oSubasta, bSoloLectura)
            Me.Add(oSubastaView)
        End Sub
    End Class

    Private Class SubastaView
        Implements INotifyPropertyChanged

        Private _oSubasta As GSServerModel.ProcesoSubasta
        Private _oGruposView As GruposView
        Private _bHabilitarPrecioSalida As Boolean
        Private _bSoloLectura As Boolean

        Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

        Public Sub New(ByVal oSubasta As GSServerModel.ProcesoSubasta, ByVal bSoloLectura As Boolean)
            _oSubasta = oSubasta
            _oGruposView = New GruposView(oSubasta.Grupos)
            _bHabilitarPrecioSalida = True
            _bSoloLectura = bSoloLectura

            CalcularHabilitacion()
        End Sub

        '<summary>Calcula la habilitación de los precios de salida en cada nivel</summary>        
        '<remarks>Llamada desde: New y xamdgEstrucProce_CellUpdated  </remarks>

        Public Sub CalcularHabilitacion()            
            '1º - Mirar a qué niveles hay precios
            Dim bPrecioNivelSubasta As Boolean = False
            Dim bPrecioNivelGrupo As Boolean = False
            Dim bPrecioNivelItem As Boolean = False

            bPrecioNivelSubasta = Not Me.PrecioSalida Is Nothing
            For Each oGrupoView As GrupoView In _oGruposView
                For Each oItemView As ItemView In oGrupoView.Items
                    If Not oItemView.PrecioSalida Is Nothing Then
                        bPrecioNivelItem = True
                        Exit For
                    End If
                Next

                If Not oGrupoView.PrecioSalida Is Nothing Then
                    bPrecioNivelGrupo = True
                    'Si ya hemos visto que hay precios a nivel de item se puede salir
                    If bPrecioNivelItem Then Exit For
                End If
            Next
            '2º - Calcular la habilitación
            Dim bHabilitarNivelSubasta As Boolean = True
            Dim bHabilitarNivelGrupo As Boolean = True
            Dim bHabilitarNivelItem As Boolean = True
            If bPrecioNivelItem Then
                bHabilitarNivelSubasta = False
                bHabilitarNivelGrupo = False
            Else
                If bPrecioNivelGrupo Then
                    bHabilitarNivelItem = False
                    bHabilitarNivelSubasta = False
                Else
                    If bPrecioNivelSubasta Then
                        bHabilitarNivelItem = False
                        bHabilitarNivelGrupo = False
                    End If
                End If
            End If
            '3º - Asignar la habilitación
            Me.HabilitarPrecioSalida = bHabilitarNivelSubasta And (Not _bSoloLectura)
            For Each oGrupoView As GrupoView In _oGruposView
                oGrupoView.HabilitarPrecioSalida = bHabilitarNivelGrupo And (Not _bSoloLectura)
                For Each oItemView As ItemView In oGrupoView.Items
                    oItemView.HabilitarPrecioSalida = bHabilitarNivelItem And (Not _bSoloLectura)
                Next
            Next
        End Sub

        '<summary>Comprueba la introducción de importes de bajada mínima ganadora y por proveedor</summary> 
        '<param name="bBajMinGan">Indica si se ha establecido bajada mínima ganadora</param>   
        '<param name="bBajMinProve">Indica si se ha establecido bajada mínima por proveedor</param>  
        '<param name="bImportesBajMinGanOK">Indica si los importes de bajada mínima ganadora se han establecido correctamente</param>   
        '<param name="bImportesBajMinProveOK">Indica si los importes de bajada mínima por proveedor se han establecido correctamente</param>
        '<remarks>Llamada desde: SubastaConf.ComprobarDatos  </remarks>

        Public Sub ComprobarImportes(ByVal bBajMinGan As Boolean, ByVal bBajMinProve As Boolean, ByRef bImportesBajMinGanOK As Boolean, ByRef bImportesBajMinProveOK As Boolean)
            bImportesBajMinGanOK = Not bBajMinGan
            bImportesBajMinProveOK = Not bBajMinProve

            If bBajMinGan And Not Me.MinPujaGanadora Is Nothing Then bImportesBajMinGanOK = True
            If bBajMinProve And Not Me.MinPujaProve Is Nothing Then bImportesBajMinProveOK = True

            If Not (bImportesBajMinGanOK And bImportesBajMinProveOK) Then
                For Each oGrupoView As GrupoView In _oGruposView
                    If bBajMinGan And Not oGrupoView.MinPujaGanadora Is Nothing Then bImportesBajMinGanOK = True
                    If bBajMinProve And Not oGrupoView.MinPujaProve Is Nothing Then bImportesBajMinProveOK = True

                    If Not (bImportesBajMinGanOK And bImportesBajMinProveOK) Then
                        For Each oItemView As ItemView In oGrupoView.Items
                            If bBajMinGan And Not oItemView.MinPujaGanadora Is Nothing Then bImportesBajMinGanOK = True
                            If bBajMinProve And Not oItemView.MinPujaProve Is Nothing Then bImportesBajMinProveOK = True

                            If (bImportesBajMinGanOK And bImportesBajMinProveOK) Then Exit For
                        Next
                    Else
                        Exit For
                    End If
                Next
            End If
        End Sub

        Private Sub NotifyPropertyChanged(ByVal info As String)
            RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(info))
        End Sub

#Region " Propiedades "

        Public ReadOnly Property Anyo() As Integer
            Get
                Anyo = _oSubasta.Anyo
            End Get
        End Property

        Public ReadOnly Property Codigo() As Integer
            Get
                Codigo = _oSubasta.Codigo
            End Get
        End Property

        Public ReadOnly Property GMN1() As String
            Get
                GMN1 = _oSubasta.GMN1
            End Get
        End Property

        Public ReadOnly Property Denominacion() As String
            Get
                Return _oSubasta.Denominacion
            End Get
        End Property

        Public ReadOnly Property Presupuesto As Double
            Get
                Return _oSubasta.Presupuesto
            End Get
        End Property

        Public Property PrecioSalida As Nullable(Of Double)
            Get
                Return _oSubasta.PrecioSalida
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oSubasta.PrecioSalida) Then
                    _oSubasta.PrecioSalida = value
                    NotifyPropertyChanged("PrecioSalida")
                End If
            End Set
        End Property

        Public Property MinPujaGanadora As Nullable(Of Double)
            Get
                Return _oSubasta.MinPujaGanadora
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oSubasta.MinPujaGanadora) Then
                    _oSubasta.MinPujaGanadora = value
                    NotifyPropertyChanged("MinPujaGanadora")
                End If
            End Set
        End Property

        Public Property MinPujaProve As Nullable(Of Double)
            Get
                Return _oSubasta.MinPujaProve
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oSubasta.MinPujaProve) Then
                    _oSubasta.MinPujaProve = value
                    NotifyPropertyChanged("MinPujaProve")
                End If
            End Set
        End Property

        Public Property Grupos As GruposView
            Get
                Return _oGruposView
            End Get
            Set(ByVal value As GruposView)
                _oGruposView = value
                '_oSubasta.Grupos = value.Grupos
            End Set
        End Property

        Public Property HabilitarPrecioSalida As Boolean
            Get
                Return _bHabilitarPrecioSalida
            End Get
            Set(ByVal value As Boolean)
                If value <> _bHabilitarPrecioSalida Then
                    _bHabilitarPrecioSalida = value
                    NotifyPropertyChanged("HabilitarPrecioSalida")
                End If
            End Set
        End Property

#End Region

    End Class

    Private Class GruposView
        Inherits GSServerModel.Lista(Of GrupoView)

        Private _oGrupos As GSServerModel.ProcesoGrupos

        Public Sub New(ByVal oGrupos As GSServerModel.ProcesoGrupos)
            MyBase.New(True, String.Empty, String.Empty, False)

            If Not oGrupos Is Nothing And oGrupos.Count > 0 Then
                For Each oGrupo As GSServerModel.ProcesoGrupo In oGrupos
                    Me.Add(New GrupoView(oGrupo))
                Next
            End If
        End Sub

        Public ReadOnly Property Grupos
            Get
                Return _oGrupos
            End Get
        End Property

        Public Overloads Function Find(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Integer, ByVal ID As Integer) As GrupoView
            For Each oGrupo As GrupoView In Me
                If oGrupo.Anyo = Anyo And oGrupo.GMN1 = GMN1 And oGrupo.Proce = Proce And oGrupo.ID = ID Then
                    Return oGrupo
                End If
            Next
        End Function

    End Class

    Private Class GrupoView
        Implements INotifyPropertyChanged

        Private _oGrupo As GSServerModel.ProcesoGrupo
        Private _bHabilitarPrecioSalida As Boolean
        Private _oItemsView As ItemsView

        Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

        Public Sub New(ByVal oGrupo As GSServerModel.ProcesoGrupo)
            _oGrupo = oGrupo
            _oItemsView = New ItemsView(oGrupo.Items)
            _bHabilitarPrecioSalida = True
        End Sub

        public Sub NotifyPropertyChanged(ByVal info As String)
            RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(info))
        End Sub

#Region " Propiedades "

        Public ReadOnly Property Anyo() As Integer
            Get
                Anyo = _oGrupo.Anyo
            End Get
        End Property

        Public ReadOnly Property GMN1() As String
            Get
                GMN1 = _oGrupo.GMN1
            End Get
        End Property

        Public ReadOnly Property Proce As Integer
            Get
                Return _oGrupo.Proce
            End Get
        End Property

        Public ReadOnly Property ID As Integer
            Get
                Return _oGrupo.ID
            End Get
        End Property

        Public ReadOnly Property Cod As String
            Get
                Return _oGrupo.Cod
            End Get
        End Property

        Public ReadOnly Property Denominacion As String
            Get
                Return _oGrupo.Denominacion
            End Get
        End Property

        Public ReadOnly Property Presupuesto As Double
            Get
                Return _oGrupo.Presupuesto
            End Get
        End Property

        Public Property PrecioSalida As Nullable(Of Double)
            Get
                Return _oGrupo.PrecioSalida
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oGrupo.PrecioSalida) Then
                    _oGrupo.PrecioSalida = value
                    NotifyPropertyChanged("PrecioSalida")
                End If
            End Set
        End Property

        Public Property MinPujaGanadora As Nullable(Of Double)
            Get
                Return _oGrupo.MinPujaGanadora
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oGrupo.MinPujaGanadora) Then
                    _oGrupo.MinPujaGanadora = value
                    NotifyPropertyChanged("MinPujaGanadora")
                End If
            End Set
        End Property

        Public Property MinPujaProve As Nullable(Of Double)
            Get
                Return _oGrupo.MinPujaProve
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oGrupo.MinPujaProve) Then
                    _oGrupo.MinPujaProve = value
                    NotifyPropertyChanged("MinPujaProve")
                End If
            End Set
        End Property

        Public Property Items As ItemsView
            Get
                Return _oItemsView
            End Get
            Set(ByVal value As ItemsView)
                _oItemsView = value
            End Set
        End Property

        Public Property HabilitarPrecioSalida As Boolean
            Get
                Return _bHabilitarPrecioSalida
            End Get
            Set(ByVal value As Boolean)
                If value <> _bHabilitarPrecioSalida Then
                    _bHabilitarPrecioSalida = value
                    NotifyPropertyChanged("HabilitarPrecioSalida")
                End If
            End Set
        End Property

#End Region

    End Class

    Private Class ItemsView
        Inherits GSServerModel.Lista(Of ItemView)

        Private _oItems As GSServerModel.Items

        Public Sub New(ByVal oItems As GSServerModel.Items)
            MyBase.New(True, String.Empty, String.Empty, False)

            _oItems = oItems
            If Not oItems Is Nothing And oItems.Count > 0 Then
                For Each oItem As GSServerModel.Item In oItems
                    Me.Add(New ItemView(oItem))
                Next
            End If
        End Sub

        Public Overloads Function Find(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Integer, ByVal ID As Integer) As ItemView
            For Each oItem As ItemView In Me
                If oItem.Anyo = Anyo And oItem.GMN1_PROCE = GMN1 And oItem.Proce = Proce And oItem.ID = ID Then
                    Return oItem
                End If
            Next
        End Function

        Public ReadOnly Property Items As GSServerModel.Items
            Get
                Return _oItems
            End Get            
        End Property
    End Class

    Private Class ItemView
        Implements INotifyPropertyChanged

        Private _oItem As GSServerModel.Item
        Private _bHabilitarPrecioSalida As Boolean

        Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

        Public Sub New(ByVal oItem As GSServerModel.Item)
            _oItem = oItem
            _bHabilitarPrecioSalida = True
        End Sub

#Region " Propiedades "

        Public ReadOnly Property Anyo As Integer
            Get
                Return _oItem.Anyo
            End Get            
        End Property

        Public ReadOnly Property Proce As Integer
            Get
                Return _oItem.Proce
            End Get            
        End Property

        Public ReadOnly Property GMN1_PROCE As String
            Get
                Return _oItem.GMN1_PROCE
            End Get
        End Property

        Public ReadOnly Property ID As Integer
            Get
                Return _oItem.ID
            End Get
        End Property

        Public ReadOnly Property Articulo As String
            Get
                Return _oItem.Articulo
            End Get
        End Property

        Public ReadOnly Property Descripcion As String
            Get
                Return _oItem.Descripcion
            End Get
        End Property

        Public ReadOnly Property Dest As String
            Get
                Return _oItem.Dest
            End Get
        End Property

        Public ReadOnly Property Unidad As String
            Get
                Return _oItem.Unidad
            End Get
        End Property

        Public ReadOnly Property Cantidad As Double
            Get
                Return _oItem.Cantidad
            End Get
        End Property

        Public Property Presupuesto As Nullable(Of Double)
            Get
                Return _oItem.Presupuesto
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oItem.Presupuesto) Then
                    _oItem.Presupuesto = value
                    NotifyPropertyChanged("Presupuesto")
                End If
            End Set
        End Property

        Public ReadOnly Property Grupo As Integer
            Get
                Return _oItem.Grupo
            End Get
        End Property

        Public Property MinPujaGanadora As Nullable(Of Double)
            Get
                Return _oItem.MinPujaGanadora
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oItem.MinPujaGanadora) Then
                    _oItem.MinPujaGanadora = value
                    NotifyPropertyChanged("MinPujaGanadora")
                End If
            End Set
        End Property

        Public Property PrecioSalida As Nullable(Of Double)
            Get
                Return _oItem.PrecioSalida
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oItem.PrecioSalida) Then
                    _oItem.PrecioSalida = value
                    NotifyPropertyChanged("PrecioSalida")
                End If
            End Set
        End Property

        Public Property MinPujaProve As Nullable(Of Double)
            Get
                Return _oItem.MinPujaProve
            End Get
            Set(ByVal value As Nullable(Of Double))
                If Not value.Equals(_oItem.MinPujaProve) Then
                    _oItem.MinPujaProve = value
                    NotifyPropertyChanged("MinPujaProve")
                End If
            End Set
        End Property

        Public Property HabilitarPrecioSalida As Boolean
            Get
                Return _bHabilitarPrecioSalida
            End Get
            Set(ByVal value As Boolean)
                If value <> _bHabilitarPrecioSalida Then
                    _bHabilitarPrecioSalida = value
                    NotifyPropertyChanged("HabilitarPrecioSalida")
                End If
            End Set
        End Property

#End Region

        Private Sub NotifyPropertyChanged(ByVal info As String)
            RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(info))
        End Sub
    End Class

#End Region

End Class

#Region " Converters "

Public Class HabilitarPrecioSalidaToBackgroundConverter
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        Dim oBrush As Brush
        If Not value Then
            oBrush = New BrushConverter().ConvertFromString("Gray")
        Else
            Dim oNE As New XamNumericEditor            
            oBrush = oNE.Background
        End If
        Return oBrush
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

Public Class HabilitarPrecioSalidaToForegroundConverter
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        Dim oBrush As Brush
        If Not value Then
            oBrush = New BrushConverter().ConvertFromString("Black")
        Else
            Dim oNE As New XamNumericEditor            
            oBrush = oNE.Foreground
        End If
        Return oBrush
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

#End Region
