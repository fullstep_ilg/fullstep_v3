Le notificamos que ha tenido lugar el siguiente evento de subasta: @DESCEVENTO	 
A continuaci�n le presentamos los datos de la subasta.

	Datos del evento
	------------------------------------------------------
	Fecha evento:		@FECEVE
	Fecha UTC evento:	@FECUTCEVE

	Datos del proveedor
	------------------------------------------------------
  	C�digo:			@CODPROVE
  	Denominaci�n:		@DENPROVE

	Datos del contacto
	------------------------------------------------------
	Nombre:			@NOM_CON
  	Apellidos:		@APE_CON  	
  	Tel�fono:		@TFNO1_CON		
	E-mail:			@EMAIL_CON	

	Datos del proceso
	------------------------------------------------------
	A�o:			@ANYO_PROCE
	GMN1:			@GMN1_PROCE
	C�digo:			@COD_PROCE
	Denominaci�n:		@DEN_PROCE	

	Datos de la subasta
	------------------------------------------------------
	Fecha de apertura: 	@FEC_APE	
	Fecha UTC de apertura: 	@FEC_UTC_APE	
	Fecha de cierre:     	@FEC_CIERRE	
	Fecha UTC de cierre: 	@FEC_UTC_CIERRE	

	Datos del comprador responsable 
	------------------------------------------------------
	Nombre: 		@NOM_COM
	Apellidos: 		@APE_COM
	E-mail: 		@EMAIL_COM
	Tel�fono: 		@TFNO1_COM

Un saludo