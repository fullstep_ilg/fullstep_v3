﻿Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Diagnostics

Public Class CTextos

    Public Function DevolverTextos(ByVal Modulo As Integer, ByVal Idioma As String) As DataSet

        Dim ds As New DataSet

        Dim OledbConnection As New OleDbConnection
        Dim OledbCommand As New OleDbCommand
        Dim dir As DirectoryInfo
        Dim dbPath As String

        Dim appPath As String
        appPath = System.Reflection.Assembly.GetExecutingAssembly().CodeBase
        appPath = appPath.Substring(8)
        appPath = System.IO.Path.GetDirectoryName(appPath)
        dir = New DirectoryInfo(appPath)
        appPath = dir.FullName
        dbPath = appPath + "\\fsintcap.mdb"

        Dim oleConnection = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + dbPath)

        Dim oleCustomersCommand = New OleDbCommand("SELECT TEXT_" & Idioma & ",ID FROM TEXTOS WHERE Modulo=" & Modulo & " ORDER BY ID", oleConnection)

        Dim oleAdapter = New OleDbDataAdapter()

        oleAdapter.SelectCommand = oleCustomersCommand        
        oleAdapter.Fill(ds, "Textos")        

        Return ds

    End Function

    Public Function DevolverTextos(ByVal Modulo As Integer, ByVal Id As Integer) As System.Collections.Generic.Dictionary(Of String, String)
        Dim oTextos As New System.Collections.Generic.Dictionary(Of String, String)
        Dim OledbConnection As New OleDbConnection
        Dim OledbCommand As New OleDbCommand
        Dim dir As DirectoryInfo
        Dim dbPath As String

        Dim appPath As String
        appPath = System.Reflection.Assembly.GetExecutingAssembly().CodeBase
        appPath = appPath.Substring(8)
        appPath = System.IO.Path.GetDirectoryName(appPath)
        dir = New DirectoryInfo(appPath)
        appPath = dir.FullName
        dbPath = appPath + "\\fsintcap.mdb"

        Dim oleConnection = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + dbPath)

        Dim oleCustomersCommand = New OleDbCommand("SELECT * FROM TEXTOS WHERE Modulo=" & Modulo & " AND ID=" & Id, oleConnection)

        Dim oleAdapter = New OleDbDataAdapter()

        oleAdapter.SelectCommand = oleCustomersCommand
        Dim ds As New DataSet
        oleAdapter.Fill(ds, "Textos")
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            For Each oCol As DataColumn In ds.Tables(0).Columns
                If oCol.ColumnName.StartsWith("TEXT_") Then
                    oTextos.Add(oCol.ColumnName.Substring(5), ds.Tables(0).Rows(0)(oCol.ColumnName))
                End If
            Next
        End If

        Return oTextos
    End Function

    Public Function DevolverTextosMensaje(ByVal Modulo As Integer, ByVal Id As Integer, ByVal Idioma As String) As String


        Dim ds As New DataSet

        Dim OledbConnection As New OleDbConnection
        Dim OledbCommand As New OleDbCommand
        Dim dir As DirectoryInfo
        Dim dbPath As String

        Dim appPath As String
        appPath = System.Reflection.Assembly.GetExecutingAssembly().CodeBase
        appPath = appPath.Substring(8)
        appPath = System.IO.Path.GetDirectoryName(appPath)
        dir = New DirectoryInfo(appPath)
        appPath = dir.FullName
        dbPath = appPath + "\\fsintcap.mdb"

        Dim oleConnection = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + dbPath)

        Dim oleCustomersCommand = New OleDbCommand("SELECT TEXT_" & Idioma & " FROM TEXTOS WHERE Modulo=" & Modulo & " AND ID= " & Id, oleConnection)

        Dim oleAdapter = New OleDbDataAdapter()

        oleAdapter.SelectCommand = oleCustomersCommand
        oleAdapter.Fill(ds, "Textos")

        Return ds.Tables(0).Rows(0).Item(0)


    End Function

    '' <summary>Devuelve el texto indicado en el idioma indicado en parámetros</summary>
    '' <param name="Modulo">Módulo del texto</param>   
    '' <param name="Id">Id del texto</param>           

    Public Function DevolverTextosMensaje(ByVal Modulo As Integer, ByVal Id As Integer) As String
        Return DevolverTextosMensaje(Modulo, Id, ModulePublic.ParametrosInstalacion.gsIdioma)
    End Function

End Class
