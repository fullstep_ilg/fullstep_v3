﻿Imports System.ServiceModel

Public Class FileTransfer
    Public Event FileChunkSent(ByVal FileName As String, ByVal FileSizeCompleted As Integer)

    Private _Service As GSClientServices.GSWCFReferenceProcesses.GSFileTransferServerClient

    '' <summary>Crea un dir. temporal en el que dejar los archivos transferidos</summary>
    '' <param name="RootFolder">Direcotorio raíz para la transferencia de archivos</param>       
    '' <remarks>Llamada desde: New  </remarks>

    Public Function BeginFileTransfer() As String
        _Service = GSClientServices.ServiceFactory.Instance.GetFullGSFileTransferService
        Return _Service.BeginFileTransfer()
    End Function

    '' <summary>Elimina el dir. temporal de descargas y su contenido</summary>
    '' <param name="RootFolder">Direcotorio raíz para la transferencia de archivos</param>       
    '' <remarks>Llamada desde: New  </remarks>

    Public Function EndFileTransfer(ByVal sFolder As String) As Boolean
        Return _Service.EndFileTransfer(sFolder)
    End Function

    '' <summary>Recibe un array de bytes y crea un archivo con ellos si no existe o los añade al final si existe</summary>
    '' <param name="FileNme">Nombre del archivo</param>   
    '' <param name="Buffer">Array de bytes</param>       
    '' <remarks>Llamada desde: New  </remarks>

    Public Function SendFile(ByVal sFilePath As String, ByVal sFolder As String, ByVal FileName As String) As Boolean
        'Obtener el tamaño del buffer del máximo tamaño permitido para los mensajes
        Dim iChunkSize As Integer
        If TypeOf _Service.ChannelFactory.Endpoint.Binding Is System.ServiceModel.BasicHttpBinding Then
            Dim oHTTPBinding As System.ServiceModel.BasicHttpBinding = _Service.ChannelFactory.Endpoint.Binding
            iChunkSize = oHTTPBinding.ReaderQuotas.MaxArrayLength
        ElseIf TypeOf _Service.ChannelFactory.Endpoint.Binding Is System.ServiceModel.WSHttpBinding Then
            Dim oHTTPBinding As System.ServiceModel.WSHttpBinding = _Service.ChannelFactory.Endpoint.Binding
            iChunkSize = oHTTPBinding.ReaderQuotas.MaxArrayLength
        End If

        Dim Buffer(iChunkSize) As Byte
        Dim oFileStream As System.IO.FileStream = New System.IO.FileStream(sFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
        Dim iOffset As Integer = 0
        oFileStream.Seek(iOffset, System.IO.SeekOrigin.Begin)

        Dim bOk As Boolean = True
        Dim iNumBytes As Integer = oFileStream.Read(Buffer, 0, iChunkSize)
        If iNumBytes < Buffer.Length Then ReDim Preserve Buffer(iNumBytes - 1)
        While iNumBytes > 0 And bOk
            bOk = _Service.SendFileChunk(FileName, sFolder, Buffer)

            iOffset += iNumBytes
            oFileStream.Seek(iOffset, System.IO.SeekOrigin.Begin)
            iNumBytes = oFileStream.Read(Buffer, 0, Buffer.Length)
            If iNumBytes < Buffer.Length Then ReDim Preserve Buffer(iNumBytes - 1)

            RaiseEvent FileChunkSent(FileName, iOffset)
        End While

        SendFile = bOk
    End Function

End Class
