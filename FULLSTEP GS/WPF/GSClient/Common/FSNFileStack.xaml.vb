﻿Imports System
Imports System.Collections.Generic
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Media.Animation
Imports System.Windows.Media.Imaging
Imports System.Windows.Navigation
Imports System.Windows.Shapes
Imports Infragistics.Windows.DataPresenter
Imports System.Diagnostics.Process
Imports System.ComponentModel
Imports System.IO
Imports System.Threading
Imports System.Runtime.InteropServices

Partial Public Class FSNFileStack
    Inherits ControlBase

    Public Event OpenFileClick(ByVal Archivo As GSServerModel.File)
    Public Event SaveToDiskClick(ByVal Archivo As GSServerModel.File, ByVal Ruta As String)
    Public Event Adding()
    Public Event Added()
    Public Event Removing()
    Public Event Removed()    
    Public Event Opening()
    Public Event Opened()    
    Public Event SavingToDisk()
    Public Event SavedToDisk()

    Private Delegate Function AsyncUploadFileCaller(ByVal oFile As GSServerModel.File, <Out()> ByRef bUploadFileResult As Boolean) As GSServerModel.File
    Private Shared _bUploadFileResult As Boolean
    Private Shared _PendingUploads As Collection

    Private WithEvents _oFT As GSClient.FileTransfer
    Private Shared WithEvents _oFiles As BindingList(Of GSServerModel.File)

    Private _bSoloLectura As Boolean

#Region " Constructor "

    Public Sub New()
        MyBase.New()

        Me.InitializeComponent()

        ' Insert code required on object creation below this point.
        _oFiles = New BindingList(Of GSServerModel.File)        
        _PendingUploads = New Collection
        _oFT = New GSClient.FileTransfer
    End Sub

#End Region

#Region " Propiedades "

    Public Property Archivos As GSServerModel.Files
        Get
            Return New GSServerModel.Files(_oFiles.ToList)            
        End Get
        Set(ByVal value As GSServerModel.Files)
            _oFiles = New BindingList(Of GSServerModel.File)(value)            
            MakeDataBinding()
        End Set
    End Property

    Public ReadOnly Property PendingUploads As Collection
        Get
            Return _PendingUploads
        End Get        
    End Property

    Public Property SoloLectura As Boolean
        Get
            Return _bSoloLectura
        End Get
        Set(ByVal value As Boolean)
            _bSoloLectura = value
        End Set
    End Property

#End Region

#Region " Eventos control "

    Private Sub FSNFileStack_Initialized(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Initialized
        If Not System.ComponentModel.DesignerProperties.GetIsInDesignMode(Me) Then            
            MyBase.CargarTextos(ModuleName.FRM_FILESTACK, Me.LayoutRoot)
            MakeDataBinding()            
        End If
    End Sub

    Private Sub FSNFileStack_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        If _bSoloLectura Then BloquearControles()
    End Sub

    '' <summary>Realiza el binding de los controles a los objetos de datos necesarios</summary>    
    '' <remarks>Llamada desde: FSNFileStack_Initialized  </remarks>

    Private Sub MakeDataBinding()
        Me.RemoveBindingObject("Archivos")
        Me.AddBindingObject(_oFiles, "Archivos")        
        Me.MakeBinding(Me.LayoutRoot)
    End Sub

    '' <summary>Bloquea los controles para que sean de sólo lectura</summary>    
    '' <remarks>Llamada desde: FSNFileStack_Initialized  </remarks>

    Private Sub BloquearControles()
        xamdgArchivos.FieldLayouts(0).Settings.AllowDelete = False

        btnAdd.IsEnabled = False        
        btnOpen.IsEnabled = False
        btnSaveToDisk.IsEnabled = False
        btnRemove.IsEnabled = False
    End Sub

#End Region

#Region " Eventos Grid "

    'Private Sub xamdgArchivos_CellDeactivating(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellDeactivatingEventArgs) Handles xamdgArchivos.CellDeactivating
    '    If e.Cell.Field.Name = "Comenario" Then
    '        e.Cell.Field.Settings.AllowEdit = False
    '    End If
    'End Sub

    'Private Sub xamdgArchivos_CellUpdated(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellUpdatedEventArgs) Handles xamdgArchivos.CellUpdated
    '    e.Field.Settings.AllowEdit = False
    'End Sub

#End Region

#Region " Botones "

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnAdd.Click
        RaiseEvent Adding()

        Dim oDialog As New Windows.Forms.OpenFileDialog
        If oDialog.ShowDialog = Forms.DialogResult.OK Then
            Dim oListaFiles As New GSServerModel.Files(_oFiles.ToList)
            If oListaFiles.Item(oDialog.SafeFileName) Is Nothing Then
                Dim oContexto As GSServerModel.Contexto = System.Threading.Thread.GetData(System.Threading.Thread.GetNamedDataSlot("Contexto"))
                Dim oTZInfo As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(oContexto.UserTZ)
                Dim dtFecha As Date = TimeZoneInfo.ConvertTimeFromUtc(Date.UtcNow, oTZInfo)

                Dim oFile As New GSServerModel.File(oDialog.FileName, oDialog.SafeFileName, String.Empty, dtFecha, 0, True, 0)
                _oFiles.Add(oFile)

                'Enviar el archivo al servidor                
                UploadFileAsync(oFile)
            End If
        End If

        RaiseEvent Added()
    End Sub

    ' <summary>Realiza la copia del archivo seleccionado en una carpeta temporal del servidor de forma asíncrona</summary>
    ' <param name="oFile">Año del proceso</param>  
    ' <returns>Booleano indicando si se ha producido el envío</returns>
    ' <remarks>Llamada desde: btnAdd_Click  </remarks>

    Private Sub UploadFileAsync(ByRef oFile As GSServerModel.File)
        'Create the delegate
        Dim oCaller As New AsyncUploadFileCaller(AddressOf Me.UploadFile)

        ' Initiate the asynchronous call.
        Dim oResult As IAsyncResult = oCaller.BeginInvoke(oFile, _bUploadFileResult, AddressOf CallbackMethod, oCaller)

        _PendingUploads.Add(oFile, oFile.NombreArchivo)
    End Sub

    ' <summary>Método al que se llama cuando termina la subida asíncrona de un archivo. Decrementa la cuenta de archivos pendientes</summary>
    ' <param name="oAR">Año del proceso</param>      

    Shared Sub CallbackMethod(ByVal oAR As IAsyncResult)
        ' Retrieve the delegate.
        Dim oCaller As AsyncUploadFileCaller = CType(oAR.AsyncState, AsyncUploadFileCaller)

        ' Call EndInvoke to retrieve the results.
        Dim oFile As GSServerModel.File = oCaller.EndInvoke(_bUploadFileResult, oAR)

        'Decrementar la cuenta de archivos pendientes        
        If _bUploadFileResult Then
            _PendingUploads.Remove(oFile.NombreArchivo)            
        End If
    End Sub

    ' <summary>Realiza la copia del archivo seleccionado en una carpeta temporal del servidor</summary>
    ' <param name="oFile">Año del proceso</param>  
    ' <returns>Booleano indicando si se ha producido el envío</returns>
    ' <remarks>Llamada desde: btnAdd_Click  </remarks>

    'Private Function UploadFile(<Out()> ByRef oFile As GSServerModel.File) As Boolean
    Private Function UploadFile(ByVal oFile As GSServerModel.File, <Out()> ByRef bUploadFileResult As Boolean) As GSServerModel.File
        Dim sFileDirectory As String
        'Dim oFT As GSClient.FileTransfer

        If Not oFile Is Nothing Then
            _oFT = New GSClient.FileTransfer
            sFileDirectory = _oFT.BeginFileTransfer()
            oFile.ServerFileDirectory = sFileDirectory

            'UploadFile = oFT.SendFile(oFile.Archivo, oFile.NombreArchivo)
            bUploadFileResult = _oFT.SendFile(oFile.Archivo, sFileDirectory, oFile.NombreArchivo)
            UploadFile = oFile
        End If
    End Function

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnRemove.Click
        RaiseEvent Removing()

        xamdgArchivos.ActiveRecord.IsSelected = True
        xamdgArchivos.ExecuteCommand(DataPresenterCommands.DeleteSelectedDataRecords)

        RaiseEvent Removed()
    End Sub

    Private Sub btnOpen_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnOpen.Click
        RaiseEvent Opening()

        'If Not xamdgArchivos.SelectedItems Is Nothing AndAlso xamdgArchivos.SelectedItems.Count = 1 Then
        If Not xamdgArchivos.ActiveRecord Is Nothing Then
            'Dim oRecord As DataRecord = xamdgArchivos.SelectedItems(0)

            Dim oRecord As DataRecord = xamdgArchivos.ActiveRecord
            If oRecord.Cells("Nuevo").Value Then
                Me.Cursor = Cursors.Wait
                Start(oRecord.Cells("Archivo").Value)
                Me.Cursor = Cursors.Arrow
            Else
                Dim oFile As New GSServerModel.File(oRecord.Cells("Archivo").Value, oRecord.Cells("NombreArchivo").Value, oRecord.Cells("Comentario").Value, oRecord.Cells("Fecha").Value, oRecord.Cells("Tamaño").Value)
                RaiseEvent OpenFileClick(oFile)
            End If
        End If

        RaiseEvent Opened()
    End Sub

    Private Sub btnSaveToDisk_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnSaveToDisk.Click
        RaiseEvent SavingToDisk()

        If Not xamdgArchivos.ActiveRecord Is Nothing Then
            Dim oRecord As DataRecord = xamdgArchivos.ActiveRecord

            Dim oDialog As New Windows.Forms.SaveFileDialog
            oDialog.FileName = oRecord.Cells("NombreArchivo").Value
            If oDialog.ShowDialog = Forms.DialogResult.OK Then
                If oRecord.Cells("Nuevo").Value Then
                    Me.Cursor = Cursors.Wait
                    System.IO.File.Copy(oRecord.Cells("Archivo").Value, oDialog.FileName)
                    Me.Cursor = Cursors.Arrow
                Else
                    Dim oFile As New GSServerModel.File(oRecord.Cells("Archivo").Value, oRecord.Cells("NombreArchivo").Value, oRecord.Cells("Comentario").Value, oRecord.Cells("Fecha").Value, oRecord.Cells("Tamaño").Value)
                    RaiseEvent SaveToDiskClick(oFile, oDialog.FileName)
                End If
            End If
        End If

        RaiseEvent SavedToDisk()
    End Sub

#End Region

#Region " Métodos públicos "

    Public Sub EndEdit()
        xamdgArchivos.EndEdit()
    End Sub

#End Region

    Private Sub _oFT_FileChunkSent(ByVal FileName As String, ByVal FileSizeCompleted As Integer) Handles _oFT.FileChunkSent
        For Each oArch As GSServerModel.File In _oFiles
            If oArch.NombreArchivo = FileName Then
                oArch.Uploaded = (FileSizeCompleted / oArch.Tamaño) * 100
                _oFiles.ResetItem(_oFiles.IndexOf(oArch))                
                Exit For
            End If
        Next
    End Sub

End Class

Public Class BytesToKBConverter
    Implements IValueConverter

    Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements IValueConverter.Convert        
        Return value / 1024
    End Function

    Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New Exception("Not implemented yet (and won't be) :)")
    End Function
End Class

