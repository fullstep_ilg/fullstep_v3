﻿Public Class Usuario
    Public Function ComprobarSession(ByVal sUsu As String, ByVal sIdSession As Long) As Boolean       
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.ComprobarSession(sUsu, sIdSession)        
    End Function

    Public Function CargarDatosUsu(ByVal sUsu As String) As GSServerModel.Usuario
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()

        Return service.CargarDatosUsu(sUsu, IIf(ParametrosInstalacion.gsIdioma Is Nothing, ParametrosGenerales.gsIdioma, ParametrosInstalacion.gsIdioma))
    End Function

End Class
