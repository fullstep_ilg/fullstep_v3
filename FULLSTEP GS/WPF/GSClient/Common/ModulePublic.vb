﻿Public Module ModulePublic
    Public oUsu As New GSServerModel.Usuario(False, "", "", True)
    Public ParametrosIntegracion As New GSServerModel.ParametrosIntegracion
    Public ParametrosGenerales As New GSServerModel.ParametrosGenerales
    Public ParametrosInstalacion As New GSServerModel.ParametrosInstalacion
    Public LongitudesDeCodigo As New GSServerModel.LongitudesCodigo
    Public sFSP As String
    Public Const giLongDenMoneda As Integer = 100


    Public Sub CargarUsu(ByVal sUsu As String)
        Dim oUsuario As New GSClient.Usuario
        oUsu = oUsuario.CargarDatosUsu(sUsu)
    End Sub

    Public Sub CargarParametrosGenerales()
        Dim oPar As New GSClient.Parametros
        oPar.CargarParametrosGenerales(ParametrosGenerales, LongitudesDeCodigo)
        sFSP = ModulePublic.Parametrosgenerales.gsFSP_SRV & "." & ModulePublic.Parametrosgenerales.gsFSP_BD & ".dbo."
    End Sub

    Public Sub CargarParametrosInstalacion()
        Dim oPar As New GSClient.Parametros
        oPar.CargarParametrosInstalacion(ParametrosGenerales, ParametrosInstalacion, oUsu.Cod)
    End Sub

    Public Sub CargarParametrosIntegracion()
        Dim oPar As New GSClient.Parametros
        oPar.CargarParametrosIntegracion(ParametrosIntegracion, ParametrosGenerales)
    End Sub

    Public Function UTCdeBD() As Date
        Dim oPar As New GSClient.Parametros
        Return oPar.UTCdeBD()
    End Function

End Module
