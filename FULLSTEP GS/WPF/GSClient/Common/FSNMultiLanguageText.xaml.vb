﻿Imports System.Windows.Media.Animation
Imports System.Windows.Data
Imports System.Windows.Media.Imaging
Imports System.Windows
Imports System.Collections.ObjectModel
Imports System.ComponentModel

Public Class FSNMultiLanguageText
    Inherits ControlBase

    Private Const cnTipoTexto As String = "FIN SUBASTA PROCE"

    Private _IdTextos As Integer
    Private _oIdiomas As GSServerModel.Idiomas
    Private _Textos As GSServerModel.TextosMultiidioma
    Private _bSoloLectura As Boolean

#Region " Constructor "

    Public Sub New()
        MyBase.New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

#End Region

#Region " Propiedades "

    Public Property Idiomas As GSServerModel.Idiomas
        Get
            Return _oIdiomas
        End Get
        Set(ByVal value As GSServerModel.Idiomas)
            _oIdiomas = value
        End Set
    End Property

    Public Property IdTextos As Nullable(Of Integer)
        Get
            Return _IdTextos
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If value Is Nothing Then
                _IdTextos = 0
            Else
                _IdTextos = value
            End If
        End Set
    End Property

    Public Property Textos As GSServerModel.TextosMultiidioma
        Get
            Return _Textos
        End Get
        Set(ByVal value As GSServerModel.TextosMultiidioma)
            _Textos = value
        End Set
    End Property

    Public Property SoloLectura As Boolean
        Get
            Return _bSoloLectura
        End Get
        Set(ByVal value As Boolean)
            _bSoloLectura = value
        End Set
    End Property

#End Region

#Region " Eventos control "

    Private Sub FSNMultiLanguageText_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        textBox.IsReadOnly = _bSoloLectura

        If Not System.ComponentModel.DesignerProperties.GetIsInDesignMode(Me) Then
            'Establecer el idioma seleccionado por defecto
            Dim sIdioma As String = ModulePublic.oUsu.Idioma
            If sIdioma Is Nothing OrElse sIdioma = String.Empty Then sIdioma = ParametrosInstalacion.gsIdioma
            For Each oItem As Object In CarouselIdiomas.Items
                If CType(oItem, IdiomaView).Cod = sIdioma Then
                    CarouselIdiomas.SelectedItem = oItem
                    Exit For
                End If
            Next
        End If
    End Sub

#End Region

#Region " Métodos públicos "

    Public Sub MakeDataBinding()
        Me.AddBindingObject(New IdiomasView(_IdTextos, _oIdiomas, _Textos), "Idiomas")

        Me.MakeBinding(TextoPersonalizado)
    End Sub

    Public Function EndEdit()
        Dim oBinding As BindingExpression = textBox.GetBindingExpression(System.Windows.Controls.TextBox.TextProperty)
        If Not oBinding Is Nothing Then oBinding.UpdateSource()

        For i As Integer = _Textos.Count - 1 To 0 Step -1
            If _Textos(i).Texto Is Nothing OrElse _Textos(i).Texto = String.Empty Then _Textos.Remove(_Textos(i))
        Next
    End Function

#End Region

#Region " Eventos CarouselIdiomas "

    Private Sub CarouselIdiomas_MouseEnter(ByVal sender As Object, ByVal e As System.Windows.Input.MouseEventArgs) Handles CarouselIdiomas.MouseEnter
        If Not textBox.IsFocused Then
            Dim oAnim As Storyboard = Me.FindResource("StoryboardCarouselOver")
            oAnim.Begin()
        End If
    End Sub

    Private Sub CarouselIdiomas_MouseLeave(ByVal sender As Object, ByVal e As System.Windows.Input.MouseEventArgs) Handles CarouselIdiomas.MouseLeave
        If Not textBox.IsFocused Then
            Dim oAnim As Storyboard = Me.FindResource("StoryboardCarouselOut")
            oAnim.Begin()
        End If
    End Sub

    Private Sub CarouselIdiomas_SelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Controls.SelectionChangedEventArgs) Handles CarouselIdiomas.SelectionChanged
        If Not CarouselIdiomas.SelectedItem Is Nothing Then
            ''Actualizar el item anterior
            'If Not e.RemovedItems Is Nothing AndAlso e.RemovedItems.Count > 0 Then
            '    _Textos.Item(_IdTextos, CType(e.RemovedItems(0), GSServerModel.Idioma).Cod).Texto = textBox.Text
            'End If

            ''Mostrar el texto en el idioma seleccionado
            'If Not _Textos Is Nothing Then textBox.Text = _Textos.Item(_IdTextos, CType(CarouselIdiomas.SelectedItem, GSServerModel.Idioma).Cod).Texto

            If _Textos Is Nothing OrElse _Textos.Count = 0 OrElse _Textos.Item(_IdTextos, CType(CarouselIdiomas.SelectedItem, IdiomaView).Cod) Is Nothing Then
                'Caso de sin textos
                If _Textos Is Nothing Then _Textos = New GSServerModel.TextosMultiidioma
                Dim oTexto As New GSServerModel.TextoMultiidioma
                oTexto.ID = _IdTextos
                oTexto.Idioma = CType(CarouselIdiomas.SelectedItem, IdiomaView).Cod
                oTexto.Tipo = cnTipoTexto                
                _Textos.Add(oTexto)

                CType(Me.GetBindingObject("Idiomas"), IdiomasView).AddTexto(oTexto)
            End If

            MakeTextBinding(_Textos.Item(_IdTextos, CType(CarouselIdiomas.SelectedItem, IdiomaView).Cod))
            btnIdiomaActual.Tag = CType(CarouselIdiomas.SelectedItem, IdiomaView).Cod
        End If
    End Sub

    '<summary>Función que maneja el MouseUp de los elementos del carrusel</summary>
    '<param name="sender">sender</param>         
    '<param name="e">Datos del evento</param>     

    Private Sub ItemMouseUp(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs)
        'Ocultar el carrusel
        Dim oAnim As Storyboard = Me.FindResource("StoryboardTextBoxOut")
        oAnim.Begin()
        textBox.Focus()
    End Sub

    Private Sub MakeTextBinding(ByVal oTexto As GSServerModel.TextoMultiidioma)
        Dim oBinding As New System.Windows.Data.Binding("Texto")
        oBinding.Source = oTexto
        oBinding.Mode = BindingMode.TwoWay

        textBox.SetBinding(System.Windows.Controls.TextBox.TextProperty, oBinding)
    End Sub

#End Region

#Region " Eventos textBox "

    Private Sub textBox_GotFocus(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles textBox.GotFocus
        Dim oAnim As Storyboard = Me.FindResource("StoryboardTextBoxFocus")
        oAnim.Begin()
    End Sub

    Private Sub ActualizarItemActual()
        If Not CarouselIdiomas.SelectedItem Is Nothing Then
            _Textos.Item(_IdTextos, CType(CarouselIdiomas.SelectedItem, GSServerModel.Idioma).Cod).Texto = textBox.Text
        End If
    End Sub

#End Region

    Public Class IdiomasView
        Inherits ObservableCollection(Of IdiomaView)

        Public Sub New(ByVal IdTextos As Integer, ByVal Idiomas As GSServerModel.Idiomas, ByVal Textos As GSServerModel.TextosMultiidioma)
            For Each oIdioma As GSServerModel.Idioma In Idiomas
                Dim oTexto As GSServerModel.TextoMultiidioma
                If Not Textos Is Nothing AndAlso Textos.Count > 0 AndAlso Not Textos.Item(IdTextos, oIdioma.Cod) Is Nothing Then
                    oTexto = Textos.Item(IdTextos, oIdioma.Cod)
                Else
                    oTexto = Nothing
                End If
                Dim oIdiomaView As New IdiomaView(oIdioma, oTexto)
                Me.Add(oIdiomaView)
            Next
        End Sub

        Public Sub AddTexto(ByVal oTexto As GSServerModel.TextoMultiidioma)
            For Each oIdioma As IdiomaView In Me
                If oIdioma.Cod = oTexto.Idioma Then oIdioma.Texto = oTexto
            Next
        End Sub

    End Class

    Public Class IdiomaView
        Implements INotifyPropertyChanged

        Private _oIdioma As GSServerModel.Idioma
        Private WithEvents _oTexto As GSServerModel.TextoMultiidioma

        Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

        Public Sub New(ByVal oIdioma As GSServerModel.Idioma, ByVal oTexto As GSServerModel.TextoMultiidioma)
            _oIdioma = oIdioma
            _oTexto = oTexto
        End Sub

#Region " Propiedades "

        Public ReadOnly Property Cod() As String
            Get
                Return _oIdioma.Cod
            End Get
        End Property

        Public Property Texto As GSServerModel.TextoMultiidioma
            Get
                Return _oTexto
            End Get
            Set(ByVal value As GSServerModel.TextoMultiidioma)
                _oTexto = value
            End Set
        End Property

        Public ReadOnly Property TieneTexto() As Boolean
            Get
                If Not _oTexto Is Nothing AndAlso Not _oTexto.Texto Is Nothing AndAlso _oTexto.Texto.Length > 0 Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

#End Region

        Private Sub _oTexto_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _oTexto.PropertyChanged
            If e.PropertyName = "TEXTO" Then
                NotifyPropertyChanged("TieneTexto")
            End If
        End Sub

        Private Sub NotifyPropertyChanged(ByVal info As String)
            RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(info))
        End Sub

    End Class

End Class

#Region " Converters "

Public Class CodeToImageConverter
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        Dim image As BitmapImage = New BitmapImage()
        image.BeginInit()

        Select Case CType(value, String)
            Case "GER"
                image.UriSource = New Uri("/GSClient;component/Images/aleman.png", System.UriKind.Relative)
            Case "SPA"
                image.UriSource = New Uri("/GSClient;component/Images/español.png", System.UriKind.Relative)
            Case "ENG"
                image.UriSource = New Uri("/GSClient;component/Images/inglesUK.png", System.UriKind.Relative)
            Case "FRA"
                image.UriSource = New Uri("/GSClient;component/Images/frances.png", System.UriKind.Relative)
                'Case ""
                '    Return "/GSClient;component/Images/inglesUSA.png"
                'Case ""
                '    Return "/GSClient;component/Images/portugues.png"
            Case Else
                image.UriSource = Nothing
        End Select

        If Not image.UriSource Is Nothing Then image.EndInit()
        Return image
    End Function

    Public Function ConvertBack1(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

Public Class CodeToImageConverter2
    Implements IMultiValueConverter

    Public Function Convert(ByVal values() As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IMultiValueConverter.Convert
        Dim image As BitmapImage = New BitmapImage()
        image.BeginInit()

        If Not values(0) Is DependencyProperty.UnsetValue AndAlso Not values(1) Is DependencyProperty.UnsetValue Then
            Dim sCod As String = values(0)
            Dim bHayTexto As Boolean = values(1)

            Select Case CType(sCod, String)
                Case "GER"
                    If bHayTexto Then
                        image.UriSource = New Uri("/GSClient;component/Images/alemanSeleccionado.png", System.UriKind.Relative)
                    Else
                        image.UriSource = New Uri("/GSClient;component/Images/aleman.png", System.UriKind.Relative)
                    End If
                Case "SPA"
                    If bHayTexto Then
                        image.UriSource = New Uri("/GSClient;component/Images/españolSeleccionado.png", System.UriKind.Relative)
                    Else
                        image.UriSource = New Uri("/GSClient;component/Images/español.png", System.UriKind.Relative)
                    End If
                Case "ENG"
                    If bHayTexto Then
                        image.UriSource = New Uri("/GSClient;component/Images/inglesUKSeleccionado.png", System.UriKind.Relative)
                    Else
                        image.UriSource = New Uri("/GSClient;component/Images/inglesUK.png", System.UriKind.Relative)
                    End If
                Case "FRA"
                    If bHayTexto Then
                        image.UriSource = New Uri("/GSClient;component/Images/francesSeleccionado.png", System.UriKind.Relative)
                    Else
                        image.UriSource = New Uri("/GSClient;component/Images/frances.png", System.UriKind.Relative)
                    End If
                    'Case ""
                    '    Return "/GSClient;component/Images/inglesUSA.png"
                    'Case ""
                    '    Return "/GSClient;component/Images/portugues.png"
                Case Else
                    image.UriSource = Nothing
            End Select
        End If

        If Not image.UriSource Is Nothing Then image.EndInit()
        Return image
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetTypes() As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object() Implements System.Windows.Data.IMultiValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

#End Region



