﻿Imports System.Data
Imports System.Windows
Imports System.Windows.Controls
Imports Infragistics.Windows.DataPresenter
Imports System.Windows.Media
Imports System.Globalization
Imports System.Windows.Markup
Imports System.Windows.Data
Imports System.Windows.Interop
Imports System.Runtime.InteropServices
Imports System.Windows.Media.Media3D
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class ControlBase
    Inherits Windows.Controls.UserControl

    Private _sCadenaEntrada As String
    Private _sCadenaSalida As String
    Private _iAccionEntrada As AccionesGS
    Private _iAccionSalida As AccionesGS

    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> Public Shared Function GetAncestor(ByVal hWnd As IntPtr, ByVal flags As Integer) As IntPtr
    End Function
    <DllImport("user32", CharSet:=CharSet.Auto)> Public Shared Function PostMessage(ByVal hwnd As IntPtr, ByVal msg As Integer, ByVal wparam As IntPtr, ByVal lparam As IntPtr) As Boolean
    End Function

    Private _ParentPage As PageBase
    Private _colBindingObjects As Collection

    Public Sub New()
        If Not System.ComponentModel.DesignerProperties.GetIsInDesignMode(Me) Then
            Me.Language = XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)

            If Not ModulePublic.oUsu Is Nothing Then
                'Crear un nuevo contexto e insertarlo en la memoria del thread
                Dim oContexto As New GSServerModel.Contexto
                oContexto.UsuCod = ModulePublic.oUsu.Cod
                oContexto.UserTZ = ModulePublic.oUsu.TimeZone
                Dim oSlot As System.LocalDataStoreSlot = System.Threading.Thread.GetNamedDataSlot("Contexto")
                System.Threading.Thread.SetData(oSlot, oContexto)

                'Localización

                'Establecer el lenguaje del control para que lo hereden todos los controles
                If Not ModulePublic.oUsu.LanguageTag Is Nothing AndAlso ModulePublic.oUsu.LanguageTag.Length > 0 Then
                    Me.Language = XmlLanguage.GetLanguage(ModulePublic.oUsu.LanguageTag)
                End If
            End If

            'Customize assembly resource strings
            Dim oTextos As New CTextos
            Dim ds As DataSet = oTextos.DevolverTextos(TiposDeDatos.ModuleName.INFRAGISTICS, ModulePublic.ParametrosInstalacion.gsIdioma)
            If ds.Tables.Count > 0 And ds.Tables(0).Rows.Count > 0 Then
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("GroupByArea_Prompt1", ds.Tables(0).Rows(0).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("GroupByArea_Prompt2", ds.Tables(0).Rows(1).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("ConditionGroup_Description", ds.Tables(0).Rows(2).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SpecialFilterOperand_Blanks_DisplayContent", ds.Tables(0).Rows(3).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SpecialFilterOperand_NonBlanks_DisplayContent", ds.Tables(0).Rows(4).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SR_FilterDropDownItem_Custom", ds.Tables(0).Rows(2).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SR_FilterDropDownItem_Blanks", ds.Tables(0).Rows(3).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SpecialFilterOperand_Blanks_DisplayContent", ds.Tables(0).Rows(3).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SpecialFilterOperand_NonBlanks_DisplayContent", ds.Tables(0).Rows(4).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_TitleDescriptionProperty", ds.Tables(0).Rows(5).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_FieldDescriptionProperty", ds.Tables(0).Rows(6).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_AddConditionLabelProperty", ds.Tables(0).Rows(7).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_RemoveSelectedConditionsLabelProperty", ds.Tables(0).Rows(8).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_AndGroupLegendDescriptionProperty", ds.Tables(0).Rows(9).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_GroupSelectedConditionsAsAndGroupLabelProperty", ds.Tables(0).Rows(10).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_OrGroupLegendDescriptionProperty", ds.Tables(0).Rows(11).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_GroupSelectedConditionsAsOrGroupLabelProperty", ds.Tables(0).Rows(12).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_ToggleOperatorOfSelectedConditionsLabelProperty", ds.Tables(0).Rows(13).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_UngroupSelectedConditionsLabelProperty", ds.Tables(0).Rows(14).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_OperandFieldLabel", ds.Tables(0).Rows(15).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_OperatorFieldLabel", ds.Tables(0).Rows(16).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_OkButtonLabelProperty", ds.Tables(0).Rows(17).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_CancelButtonLabelProperty", ds.Tables(0).Rows(18).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("DeleteSingleRecordPrompt", ds.Tables(0).Rows(20).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("DeleteMultipleRecordsPrompt", ds.Tables(0).Rows(21).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_GroupSelectedLabelProperty", ds.Tables(0).Rows(23).Item(0).ToString())
                Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("DataError_UpdateCellValue", ds.Tables(0).Rows(22).Item(0).ToString())

                Infragistics.Windows.Editors.Resources.Customizer.SetCustomizedString("TodayButtonCaption", ds.Tables(0).Rows(19).Item(0).ToString() & ": {0:d}")
                Infragistics.Windows.Editors.Resources.Customizer.SetCustomizedString("LE_ArgumentException_5", ds.Tables(0).Rows(22).Item(0).ToString())
            End If
        End If
    End Sub

#Region " Propiedades "

    Public Property ParentPage As PageBase
        Get
            Return _ParentPage
        End Get
        Set(ByVal value As PageBase)
            _ParentPage = value
        End Set
    End Property

    Public Property CadenaEntrada As String
        Get
            Return _sCadenaEntrada
        End Get
        Set(ByVal value As String)
            _sCadenaEntrada = value
        End Set
    End Property

    Public Property CadenaSalida As String
        Get
            Return _sCadenaSalida
        End Get
        Set(ByVal value As String)
            _sCadenaSalida = value
        End Set
    End Property

    Public Property AccionEntrada As AccionesGS
        Get
            Return _iAccionEntrada
        End Get
        Set(ByVal value As AccionesGS)
            _iAccionEntrada = value
        End Set
    End Property

    Public Property AccionSalida As AccionesGS
        Get
            Return _iAccionSalida
        End Get
        Set(ByVal value As AccionesGS)
            _iAccionSalida = value
        End Set
    End Property

#End Region

#Region " Binding "

    '' <summary>Añade un objeto de datos a la colección de objetos para Binding. Si ya existe lo sustituye (refresco)</summary>
    '' <param name="oBindingObj">Objeto de datos a añadir</param>
    '' <param name="sKey">Clave con la que se añade el objeto</param>
    '' <remarks>Llamada desde: New  </remarks>

    Public Sub AddBindingObject(ByVal oBindingObj As Object, ByVal sKey As String)
        If _colBindingObjects Is Nothing Then _colBindingObjects = New Collection
        If _colBindingObjects.Contains(sKey) Then _colBindingObjects.Remove(sKey)
        _colBindingObjects.Add(oBindingObj, sKey)
    End Sub

    '' <summary>Quita un objeto de datos de la colección de objetos de Binding</summary>
    '' <param name="sKey">Clave del objeto a eliminar</param>
    '' <remarks>Llamada desde: New  </remarks>

    Public Sub RemoveBindingObject(ByVal sKey As String)
        If Not _colBindingObjects Is Nothing Then
            _colBindingObjects.Remove(sKey)
        End If
    End Sub

    '' <summary>Quita un objeto de datos de la colección de objetos de Binding</summary>
    '' <param name="sKey">Clave del objeto a eliminar</param>
    '' <remarks>Llamada desde: New  </remarks>

    Public Function GetBindingObject(ByVal sKey As String) As Object
        If Not _colBindingObjects Is Nothing AndAlso _colBindingObjects.Contains(sKey) Then
            Return _colBindingObjects(sKey)
        End If
    End Function

    '' <summary>Comprueba la existencia de un objeto de datos en la colección de objetos de Binding</summary>
    '' <param name="sKey">Clave del objeto a comprobar</param>
    '' <remarks>Llamada desde: New  </remarks>

    Public Function ExistsBindingObject(ByVal sKey As String) As Boolean
        ExistsBindingObject = False
        If Not _colBindingObjects Is Nothing Then Return _colBindingObjects.Contains(sKey)
    End Function

    Public Sub ClearBindingObjects()
        _colBindingObjects.Clear()
    End Sub

#End Region

#Region " Métodos Públicos "

    '' <summary>Obtiene los textos correspondientes a la pantalla y llama a la función que los carga</summary>
    '' <param name="iModulo">Número de módulo en la BD de textos</param>
    '' <param name="Container">Contenedor de los controles que se van a traducir</param>
    '' <remarks>Llamada desde: New  </remarks>

    Public Sub CargarTextos(ByVal iModulo As Integer, ByRef Container As Panel)
        Mensajes.Idioma = ModulePublic.ParametrosInstalacion.gsIdioma

        Dim oTextos As New GSClient.CTextos
        Dim ds As DataSet = oTextos.DevolverTextos(iModulo, ModulePublic.ParametrosInstalacion.gsIdioma)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            Dim dvTextos As New DataView(ds.Tables(0))
            dvTextos.Sort = "ID"

            CargarTextosControl(Container, dvTextos)
        End If
    End Sub

    '' <summary>Carga los textos de la pantalla</summary>
    '' <param name="Control">Control que se va a traducir</param>
    '' <param name="dvTextos">DataView con los textos</param>
    '' <remarks>Llamada desde: CargarTextos  </remarks>

    Private Sub CargarTextosControl(ByRef Control As DependencyObject, ByVal dvTextos As DataView)
        If Not Control Is Nothing Then
            Dim iChildNum As Integer = 0
            If TypeOf Control Is Visual Or TypeOf Control Is Visual3D Then iChildNum = VisualTreeHelper.GetChildrenCount(Control)

            If iChildNum > 0 Then
                For i = 0 To iChildNum - 1
                    Dim oControl As DependencyObject = VisualTreeHelper.GetChild(Control, i)
                    CargarTextosControl(oControl, dvTextos)
                Next
            Else
                If TypeOf Control Is Infragistics.Windows.Controls.XamTabControl Then
                    For Each oTabItem As TabItem In CType(Control, Infragistics.Windows.Controls.XamTabControl).Items
                        If Not oTabItem.Content Is Nothing Then CargarTextosControl(oTabItem.Content, dvTextos)
                    Next
                ElseIf TypeOf Control Is Infragistics.Windows.Tiles.XamTilesControl Then
                    For Each oTile As Infragistics.Windows.Tiles.Tile In CType(Control, Infragistics.Windows.Tiles.XamTilesControl).Items
                        If Not oTile.Content Is Nothing Then CargarTextosControl(oTile.Content, dvTextos)
                    Next
                ElseIf TypeOf Control Is Infragistics.Windows.DockManager.XamDockManager Then
                    For Each oSplitPane As Infragistics.Windows.DockManager.SplitPane In CType(Control, Infragistics.Windows.DockManager.XamDockManager).Panes
                        CargarTextosControl(oSplitPane, dvTextos)
                    Next
                    If Not CType(Control, Infragistics.Windows.DockManager.XamDockManager).Content Is Nothing Then
                        CargarTextosControl(CType(Control, Infragistics.Windows.DockManager.XamDockManager).Content, dvTextos)
                    End If
                ElseIf TypeOf Control Is Infragistics.Windows.DockManager.SplitPane Then
                    For Each oPane As Infragistics.Windows.DockManager.ContentPane In CType(Control, Infragistics.Windows.DockManager.SplitPane).Panes
                        CargarTextosControl(oPane, dvTextos)
                    Next
                ElseIf TypeOf Control Is Infragistics.Windows.DockManager.TabGroupPane Then
                    For Each oTabItem As Control In CType(Control, Infragistics.Windows.DockManager.TabGroupPane).Items
                        CargarTextosControl(oTabItem, dvTextos)
                    Next
                ElseIf TypeOf Control Is Infragistics.Windows.DockManager.ContentPane Then
                    CargarTextosControl(CType(Control, Infragistics.Windows.DockManager.ContentPane).Content, dvTextos)
                ElseIf TypeOf Control Is Infragistics.Windows.DockManager.DocumentContentHost Then
                    For Each oPane As Infragistics.Windows.DockManager.SplitPane In CType(Control, Infragistics.Windows.DockManager.DocumentContentHost).Panes
                        CargarTextosControl(oPane, dvTextos)
                    Next
                ElseIf TypeOf Control Is FSNDataGrid Then
                    If Not CType(Control, FSNDataGrid).FieldLayouts Is Nothing AndAlso CType(Control, FSNDataGrid).FieldLayouts.Count > 0 Then
                        For Each oFL As FieldLayout In CType(Control, FSNDataGrid).FieldLayouts
                            If Not oFL.Fields Is Nothing AndAlso oFL.Fields.Count > 0 Then
                                For Each oFld As Field In oFL.Fields
                                    If TypeOf (oFld) Is FSNField Or TypeOf (oFld) Is FSNUnboundField Then
                                        CargarTextosControl(oFld, dvTextos)
                                    End If
                                Next
                            End If
                        Next
                    End If
                Else
                    'Tooltips
                    If TypeOf Control Is IFSNToolTipInterface Then
                        Dim iIndex As Integer = dvTextos.Find(CType(Control, IFSNToolTipInterface).ToolTipID)
                        If iIndex >= 0 Then
                            If TypeOf (Control) Is FSNButton Then
                                CType(Control, FSNButton).ToolTip = dvTextos(iIndex).Item(0).ToString
                            End If
                        End If
                    End If

                    'Textos
                    If TypeOf Control Is IFSNLabelInterface Then
                        If CType(Control, IFSNLabelInterface).TextID >= 0 Then
                            Dim sTexto As String = String.Empty
                            If CType(Control, IFSNLabelInterface).TextID > 0 Then
                                Dim iIndex As Integer = dvTextos.Find(CType(Control, IFSNLabelInterface).TextID)
                                If iIndex >= 0 Then sTexto = dvTextos(iIndex).Item(0).ToString
                            End If

                            If TypeOf (Control) Is FSNTextBlock Then
                                CType(Control, FSNTextBlock).Text = sTexto
                            ElseIf TypeOf (Control) Is FSNButton Then
                                CType(Control, FSNButton).Content = sTexto
                            ElseIf TypeOf (Control) Is FSNRadioButton Then
                                CType(Control, FSNRadioButton).Content = sTexto
                            ElseIf TypeOf (Control) Is FSNField Then
                                CType(Control, FSNField).Label = sTexto
                            ElseIf TypeOf (Control) Is FSNUnboundField Then
                                CType(Control, FSNUnboundField).Label = sTexto
                            End If
                        End If
                    ElseIf TypeOf Control Is IFSNMultiLabelInterface Then
                        Dim sTextID As String = CType(Control, IFSNMultiLabelInterface).TextID
                        If Not sTextID Is Nothing AndAlso sTextID.Length > 0 Then
                            'If TypeOf (Control) Is FSNDataGrid Then
                            '    'Layouts
                            '    Dim arFLTextIDs() As String = sTextID.Split("#")
                            '    If arFLTextIDs.Length > 0 Then
                            '        For k As Integer = 0 To arFLTextIDs.Length - 1
                            '            'Fields 
                            '            If Not arFLTextIDs(k) Is Nothing AndAlso arFLTextIDs(k).Length > 0 Then
                            '                Dim arTextIDs() As String = arFLTextIDs(k).Split("|")
                            '                If arTextIDs.Length > 0 Then
                            '                    If TypeOf (Control) Is FSNDataGrid Then
                            '                        'Caso del grid, un par Nombre:ID para cada columna separados por |

                            '                        For i As Integer = 0 To arTextIDs.Length - 1
                            '                            Dim sID As String = arTextIDs(i)
                            '                            If sID.Length > 0 Then
                            '                                If sID.IndexOf(":") > 0 And sID.IndexOf(":") < sID.Length - 1 Then
                            '                                    Dim sColName As String = sID.Substring(0, sID.IndexOf(":"))
                            '                                    Dim iID As Integer = CType(sID.Substring(sID.IndexOf(":") + 1, sID.Length - sID.IndexOf(":") - 1), Integer)

                            '                                    Dim iIndex As Integer = dvTextos.Find(iID)
                            '                                    If CType(Control, FSNDataGrid).FieldLayouts(k).Fields.IndexOf(sColName) > -1 Then
                            '                                        CType(Control, FSNDataGrid).FieldLayouts(k).Fields(sColName).Label = dvTextos(iIndex).Item(0).ToString
                            '                                    End If
                            '                                ElseIf sID.IndexOf(":") > 0 Then
                            '                                    Dim sColName As String = sID.Substring(0, sID.IndexOf(":"))
                            '                                    If CType(Control, FSNDataGrid).FieldLayouts(k).Fields.IndexOf(sColName) > -1 Then
                            '                                        CType(Control, FSNDataGrid).FieldLayouts(k).Fields(sColName).Label = String.Empty
                            '                                    End If
                            '                                End If
                            '                            End If
                            '                        Next

                            '                    End If
                            '                End If
                            '            End If
                            '        Next
                            '    End If
                            'ElseIf TypeOf (Control) Is FSNTabControl AndAlso CType(Control, FSNTabControl).HasItems Then
                            '    ''Caso del Tab, ID para cada columna separados por |
                            '    'For i As Integer = 0 To arTextIDs.Length - 1
                            '    '    If TypeOf (CType(Control, FSNTabControl).Items(i)) Is TabItem Then
                            '    '        Dim iID As Integer = CType(arTextIDs(i), Integer)

                            '    '        Dim iIndex As Integer = dvTextos.Find(iID)
                            '    '        CType(CType(Control, FSNTabControl).Items(i), TabItem).Header = dvTextos(iIndex).Item(0).ToString
                            '    '    End If
                            '    'Next
                            'End If                                                    
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    '' <summary>Realiza el binding de los objetos de datos</summary>
    '' <param name="Container">Contenedor de los controles a los que se va a hacer el binding</param>    
    '' <remarks>Llamada desde: New  </remarks>

    Public Sub MakeBinding(ByRef Container As DependencyObject)
        If Not Container Is Nothing AndAlso TypeOf (Container) Is FrameworkElement Then
            MakeControlBinding(Container)

            Dim iChildNum As Integer = VisualTreeHelper.GetChildrenCount(Container)
            If iChildNum > 0 Then
                For i = 0 To iChildNum - 1
                    Dim oControl As DependencyObject = VisualTreeHelper.GetChild(Container, i)
                    MakeBinding(oControl)
                Next
            Else
                If TypeOf Container Is Infragistics.Windows.Tiles.XamTilesControl Then
                    For Each oTile As Infragistics.Windows.Tiles.Tile In CType(Container, Infragistics.Windows.Tiles.XamTilesControl).Items
                        MakeBinding(oTile.Content)
                    Next
                ElseIf TypeOf Container Is Infragistics.Windows.Controls.XamTabControl Then
                    For Each oTabItem As TabItem In CType(Container, Infragistics.Windows.Controls.XamTabControl).Items
                        MakeBinding(oTabItem.Content)
                    Next
                ElseIf TypeOf Container Is Infragistics.Windows.DockManager.XamDockManager Then
                    MakeBinding(CType(Container, Infragistics.Windows.DockManager.XamDockManager).Content)

                    For Each oSplitPane As Infragistics.Windows.DockManager.SplitPane In CType(Container, Infragistics.Windows.DockManager.XamDockManager).Panes
                        MakeBinding(oSplitPane)
                    Next
                ElseIf TypeOf Container Is Infragistics.Windows.DockManager.ContentPane Then
                    MakeBinding(CType(Container, Infragistics.Windows.DockManager.ContentPane).Content)
                ElseIf TypeOf Container Is FSNMultiLanguageText Then
                    CType(Container, FSNMultiLanguageText).MakeDataBinding()
                    'Else
                    '    MakeControlBinding(Container)
                End If
            End If
        End If
    End Sub

    '' <summary>Realiza el binding de un control con el objeto y la propiedad indicados en sus propiedades</summary>
    '' <param name="Control">Control al que se va a hacer el binding</param>   
    '' <remarks>Llamada desde: MakeBinding  </remarks>

    Private Sub MakeControlBinding(ByRef Control As FrameworkElement)
        If TypeOf Control Is IFSNControlInterface Then
            Dim sBOKey As String = CType(Control, IFSNControlInterface).BindingObject
            If sBOKey <> String.Empty AndAlso _colBindingObjects.Contains(sBOKey) Then
                Dim oBindingObj As Object = _colBindingObjects(sBOKey)
                If Not oBindingObj Is Nothing Then
                    If TypeOf Control Is FSNDataGrid Then
                        CType(Control, FSNDataGrid).DataSource = oBindingObj

                        'Asignar el tipo de columna
                        Dim oType As Type = oBindingObj.GetType
                        If oType.IsArray Then oType = oType.GetElementType()

                        If Not CType(Control, FSNDataGrid).FieldLayouts Is Nothing AndAlso CType(Control, FSNDataGrid).FieldLayouts.Count > 0 Then
                            If Not CType(Control, FSNDataGrid).FieldLayouts(0).Fields Is Nothing AndAlso CType(Control, FSNDataGrid).FieldLayouts(0).Fields.Count > 0 Then
                                For Each oField As Field In CType(Control, FSNDataGrid).FieldLayouts(0).Fields
                                    Dim oPropInfo As System.Reflection.PropertyInfo = oType.GetProperty(oField.Name)
                                    If Not oPropInfo Is Nothing Then
                                        oField.DataType = oPropInfo.PropertyType
                                    End If
                                Next
                            End If
                        End If
                    ElseIf TypeOf Control Is FSNCarouselListBox Then
                        CType(Control, FSNCarouselListBox).ItemsSource = Nothing
                        CType(Control, FSNCarouselListBox).Items.Clear()
                        CType(Control, FSNCarouselListBox).ItemsSource = oBindingObj
                    Else
                        Dim oBinding As New System.Windows.Data.Binding(CType(Control, IFSNControlInterface).BindingProperty)
                        oBinding.Source = oBindingObj
                        oBinding.Mode = BindingMode.TwoWay

                        If CType(Control, IFSNControlInterface).BindingProperty <> String.Empty Then
                            If TypeOf Control Is FSNTextBlock Then
                                Control.SetBinding(FSNTextBlock.TextProperty, oBinding)
                            ElseIf TypeOf Control Is FSNTextEditor Then
                                Control.SetBinding(FSNTextEditor.TextProperty, oBinding)
                            ElseIf TypeOf Control Is FSNNumericEditor Then
                                Control.SetBinding(FSNNumericEditor.TextProperty, oBinding)
                            ElseIf TypeOf Control Is FSNComboEditor Then
                                Control.SetBinding(FSNComboEditor.ValueProperty, oBinding)
                            ElseIf TypeOf Control Is FSNDateTimeEditor Then
                                Control.SetBinding(FSNDateTimeEditor.ValueProperty, oBinding)
                            ElseIf TypeOf Control Is FSNCheckEditor Then
                                Control.SetBinding(FSNCheckEditor.ValueProperty, oBinding)
                            ElseIf TypeOf Control Is FSNRadioButton Then
                                Control.SetBinding(FSNRadioButton.IsCheckedProperty, oBinding)
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    '' <summary>Cierra la ventana que contiene el control</summary>       

    Public Sub CloseParentWindow()
        Dim wih As New WindowInteropHelper(Application.Current.MainWindow)
        Dim ieHwnd As IntPtr = GetAncestor(wih.Handle, 2)   '2=GA_ROOT        
        PostMessage(ieHwnd, &H10, IntPtr.Zero, IntPtr.Zero)     '&H10=WM_CLOSE 
    End Sub

#End Region

    Private Sub ControlBase_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        If _iAccionEntrada <> 0 And Not _sCadenaEntrada Is Nothing Then
            Dim oRegistro As New GSClient.RegistrosRule()
            oRegistro.RegistrarEntradaOSalida(oUsu.Cod, _iAccionEntrada, _sCadenaEntrada)
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If _iAccionSalida <> 0 And Not _sCadenaSalida Is Nothing Then
            Dim oRegistro As New GSClient.RegistrosRule()
            oRegistro.RegistrarEntradaOSalida(oUsu.Cod, _iAccionSalida, _sCadenaSalida)
            MyBase.Finalize()
        End If
    End Sub

End Class
