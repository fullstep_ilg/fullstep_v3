﻿
Public MustInherit Class PageBase
    Inherits Windows.Controls.Page

    Protected m_sUCToOpen As String

    Public MustOverride Sub RemoveChild(ByVal oChild As Windows.Controls.UserControl)

    Public Sub New(ByVal sQueryString As String)
        Dim sUsu As String
        Dim IdSession As Long
        Dim sPantalla As String
        Dim p As String()
        Dim q As String()
        Dim oUsu As New Usuario

        m_sUCToOpen = String.Empty

        sQueryString = sQueryString.Trim("?")
        If (IsNothing(sQueryString) Or sQueryString.Length = 0) Then
            Exit Sub
        End If

        sUsu = ""
        IdSession = 0
        sPantalla = ""

        p = sQueryString.Split("&")

        If p.Length < 3 Then
            Exit Sub
        Else
            For i = 0 To p.Count - 1
                q = p(i).Split("=")

                If q(0).ToString = "session" Then
                    IdSession = q(1)
                End If

                If q(0).ToString = "usu" Then
                    sUsu = q(1)
                End If

                If q(0).ToString = "pantalla" Then
                    sPantalla = q(1)
                End If
            Next i

            If sUsu <> "" And IdSession <> 0 And sPantalla <> "" Then
                If oUsu.ComprobarSession(sUsu, IdSession) Then
                    ModulePublic.CargarParametrosGenerales()
                    ModulePublic.CargarUsu(sUsu)
                    ModulePublic.CargarParametrosInstalacion()
                    ModulePublic.CargarParametrosIntegracion()

                    m_sUCToOpen = sPantalla
                End If
            End If
        End If
    End Sub

End Class
