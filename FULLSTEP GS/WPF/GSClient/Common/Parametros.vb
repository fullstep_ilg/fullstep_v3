﻿Public Class Parametros

    Public Function CargarParametrosGenerales(ByRef ParametrosGenerales As GSServerModel.ParametrosGenerales, ByRef LongitudesDeCodigo As GSServerModel.LongitudesCodigo) As Object
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.CargarParametrosGenerales(ParametrosGenerales, LongitudesDeCodigo)
    End Function

    Public Function CargarParametrosInstalacion(ByVal ParametrosGenerales As GSServerModel.ParametrosGenerales, ByRef ParametrosInstalacion As GSServerModel.ParametrosInstalacion, ByVal sUsu As String) As Object
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.CargarParametrosInstalacion(ParametrosGenerales, ParametrosInstalacion, sUsu)
    End Function

    Public Function CargarParametrosIntegracion(ByRef ParametrosIntegracion As GSServerModel.ParametrosIntegracion, ByRef ParametrosGenerales As GSServerModel.ParametrosGenerales) As Object
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.CargarParametrosIntegracion(ParametrosIntegracion, ParametrosGenerales)
    End Function

    Function UTCdeBD() As Date
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.UTCdeBD
    End Function

End Class
