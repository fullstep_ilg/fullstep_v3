﻿Public Class CIdioma
    Private _msCod As String
    Private _msDen As String

    Property Den() As String
        Get
            Den = _msDen
        End Get
        Set(ByVal Value As String)
            _msDen = Value
        End Set
    End Property

    Property Cod() As String
        Get
            Cod = _msCod
        End Get
        Set(ByVal Value As String)
            _msCod = Value
        End Set
    End Property


    Public Sub New()

    End Sub

    Public Function DevolverIdiomas() As GSServerModel.Idiomas
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.DevolverTodosLosIdiomas()
    End Function

End Class
