﻿Imports Infragistics.Windows.Controls

Public Interface IFSNControlInterface
    Property BindingObject As String
    Property BindingProperty As String
End Interface

Public Interface IFSNLabelInterface
    Property TextID As Integer
End Interface

Public Interface IFSNMultiLabelInterface
    Property TextID As String
End Interface

Public Interface IFSNToolTipInterface
    Property ToolTipID As Integer
End Interface


Public Class CFSNTextEditor
    Inherits Infragistics.Windows.Editors.XamTextEditor
    Implements IFSNControlInterface

    Private _sBindingProperty As String
    Private _sBindingObject As String

    Public Property BindingProperty As String Implements IFSNControlInterface.BindingProperty
        Get
            BindingProperty = _sBindingProperty
        End Get
        Set(ByVal value As String)
            _sBindingProperty = value
        End Set
    End Property

    Public Property BindingObject As String Implements IFSNControlInterface.BindingObject
        Get
            Return _sBindingObject
        End Get
        Set(ByVal value As String)
            _sBindingObject = value
        End Set
    End Property

End Class

Public Class CFSNNumericEditor
    Inherits Infragistics.Windows.Editors.XamNumericEditor
    Implements IFSNControlInterface

    Private _sBindingProperty As String
    Private _sBindingObject As String

    Public Property BindingProperty As String Implements IFSNControlInterface.BindingProperty
        Get
            BindingProperty = _sBindingProperty
        End Get
        Set(ByVal value As String)
            _sBindingProperty = value
        End Set
    End Property

    Public Property BindingObject As String Implements IFSNControlInterface.BindingObject
        Get
            Return _sBindingObject
        End Get
        Set(ByVal value As String)
            _sBindingObject = value
        End Set
    End Property

End Class

Public Class CFSNComboEditor
    Inherits Infragistics.Windows.Editors.XamComboEditor
    Implements IFSNControlInterface

    Private _sBindingProperty As String
    Private _sBindingObject As String

    Public Property BindingProperty As String Implements IFSNControlInterface.BindingProperty
        Get
            BindingProperty = _sBindingProperty
        End Get
        Set(ByVal value As String)
            _sBindingProperty = value
        End Set
    End Property

    Public Property BindingObject As String Implements IFSNControlInterface.BindingObject
        Get
            Return _sBindingObject
        End Get
        Set(ByVal value As String)
            _sBindingObject = value
        End Set
    End Property
End Class

Public Class CFSNTextBlock
    Inherits System.Windows.Controls.TextBlock
    Implements IFSNLabelInterface
    Implements IFSNControlInterface

    Private _iTextID As Integer = -1
    Private _sBindingProperty As String
    Private _sBindingObject As String

    Public Property TextID As Integer Implements IFSNLabelInterface.TextID
        Get
            TextID = _iTextID
        End Get
        Set(ByVal value As Integer)
            _iTextID = value
        End Set
    End Property

    Public Property BindingProperty As String Implements IFSNControlInterface.BindingProperty
        Get
            BindingProperty = _sBindingProperty
        End Get
        Set(ByVal value As String)
            _sBindingProperty = value
        End Set
    End Property

    Public Property BindingObject As String Implements IFSNControlInterface.BindingObject
        Get
            Return _sBindingObject
        End Get
        Set(ByVal value As String)
            _sBindingObject = value
        End Set
    End Property

End Class

Public Class CFSNButton
    Inherits System.Windows.Controls.Button
    Implements IFSNLabelInterface
    Implements IFSNToolTipInterface

    Private _iTextID As Integer = -1
    Private _iTooltipID As Integer

    Public Property TextID As Integer Implements IFSNLabelInterface.TextID
        Get
            TextID = _iTextID
        End Get
        Set(ByVal value As Integer)
            _iTextID = value
        End Set
    End Property

    Public Property ToolTipID As Integer Implements IFSNToolTipInterface.ToolTipID
        Get
            ToolTipID = _iTooltipID
        End Get
        Set(ByVal value As Integer)
            _iTooltipID = value
        End Set
    End Property
End Class

Public Class CFSNDateTimeEditor
    Inherits Infragistics.Windows.Editors.XamDateTimeEditor
    Implements IFSNControlInterface

    Private _sBindingProperty As String
    Private _sBindingObject As String

    Public Property BindingProperty As String Implements IFSNControlInterface.BindingProperty
        Get
            Return _sBindingProperty
        End Get
        Set(ByVal value As String)
            _sBindingProperty = value
        End Set
    End Property

    Public Property BindingObject As String Implements IFSNControlInterface.BindingObject
        Get
            Return _sBindingObject
        End Get
        Set(ByVal value As String)
            _sBindingObject = value
        End Set
    End Property
End Class

Public Class CFSNCheckEditor
    Inherits Infragistics.Windows.Editors.XamCheckEditor
    Implements IFSNControlInterface

    Private _sBindingProperty As String
    Private _sBindingObject As String

    Public Property BindingProperty As String Implements IFSNControlInterface.BindingProperty
        Get
            Return _sBindingProperty
        End Get
        Set(ByVal value As String)
            _sBindingProperty = value
        End Set
    End Property

    Public Property BindingObject As String Implements IFSNControlInterface.BindingObject
        Get
            Return _sBindingObject
        End Get
        Set(ByVal value As String)
            _sBindingObject = value
        End Set
    End Property
End Class

Public Class CFSNDataGrid
    Inherits Infragistics.Windows.DataPresenter.XamDataGrid
    Implements IFSNControlInterface

    Private _sBindingProperty As String
    Private _sBindingObject As String

    Public Property BindingObject As String Implements IFSNControlInterface.BindingObject
        Get
            Return _sBindingProperty
        End Get
        Set(ByVal value As String)
            _sBindingProperty = value
        End Set
    End Property

    Public Property BindingProperty As String Implements IFSNControlInterface.BindingProperty
        Get
            Return _sBindingObject
        End Get
        Set(ByVal value As String)
            _sBindingObject = value
        End Set
    End Property

    Public Sub EndEdit()
        'Force end edit if we're still editing
        If Not Me.ActiveCell Is Nothing AndAlso Me.ActiveCell.IsDataChanged Then
            Me.ActiveCell.EndEditMode()
            Me.ActiveCell.Record.Update()
        End If
    End Sub

End Class

Public Class CFSNTabControl
    Inherits XamTabControl
    Implements IFSNMultiLabelInterface

    Private _sTextID As String

    Public Property TextID As String Implements IFSNMultiLabelInterface.TextID
        Get
            Return _sTextID
        End Get
        Set(ByVal value As String)
            _sTextID = value
        End Set
    End Property
End Class

Public Class CFSNRadioButton
    Inherits System.Windows.Controls.RadioButton
    Implements IFSNLabelInterface
    Implements IFSNControlInterface

    Private _iTextID As Integer = -1
    Private _sBindingProperty As String
    Private _sBindingObject As String

    Public Property TextID As Integer Implements IFSNLabelInterface.TextID
        Get
            TextID = _iTextID
        End Get
        Set(ByVal value As Integer)
            _iTextID = value
        End Set
    End Property

    Public Property BindingProperty As String Implements IFSNControlInterface.BindingProperty
        Get
            BindingProperty = _sBindingProperty
        End Get
        Set(ByVal value As String)
            _sBindingProperty = value
        End Set
    End Property

    Public Property BindingObject As String Implements IFSNControlInterface.BindingObject
        Get
            Return _sBindingObject
        End Get
        Set(ByVal value As String)
            _sBindingObject = value
        End Set
    End Property

End Class

Public Class CFSNField
    Inherits Infragistics.Windows.DataPresenter.Field
    Implements IFSNLabelInterface

    Private _iTextID As Integer = -1

    Public Property TextID As Integer Implements IFSNLabelInterface.TextID
        Get
            Return _iTextID
        End Get
        Set(ByVal value As Integer)
            _iTextID = value
        End Set
    End Property
End Class

Public Class CFSNUnboundField
    Inherits Infragistics.Windows.DataPresenter.UnboundField
    Implements IFSNLabelInterface

    Private _iTextID As Integer = -1

    Public Property TextID As Integer Implements IFSNLabelInterface.TextID
        Get
            Return _iTextID
        End Get
        Set(ByVal value As Integer)
            _iTextID = value
        End Set
    End Property
End Class

Public Class CFSNCarouselListBox
    Inherits Infragistics.Windows.Controls.XamCarouselListBox
    Implements IFSNControlInterface

    Private _sBindingProperty As String
    Private _sBindingObject As String

    Public Property BindingObject As String Implements IFSNControlInterface.BindingObject
        Get
            Return _sBindingProperty
        End Get
        Set(ByVal value As String)
            _sBindingProperty = value
        End Set
    End Property

    Public Property BindingProperty As String Implements IFSNControlInterface.BindingProperty
        Get
            Return _sBindingObject
        End Get
        Set(ByVal value As String)
            _sBindingObject = value
        End Set
    End Property

End Class

Public Class CFSNTabItem
    Inherits TabItemEx
    Implements IFSNLabelInterface

    Private _iTextID As Integer = -1

    Public Property TextID As Integer Implements IFSNLabelInterface.TextID
        Get
            Return _iTextID
        End Get
        Set(ByVal value As Integer)
            _iTextID = value
        End Set
    End Property
End Class



