﻿Imports System.ServiceModel
Public Class ViasPagoRule
    Public Function DevolverViasPago(ByVal bMostrarIntengracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.DevolverTodasLasViasPago(bMostrarIntengracion, sIdioma)

    End Function

    Public Function ActualizarViaPago(ByRef oViaPago As GSServerModel.ViaPago, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.ActualizarViaPago(oViaPago, bRealizarCambioCodigo, bActivLog, bIntegracionViasPago, FSP, sUsuario)
    End Function


    Public Function AnyadirViaPago(ByRef oViaPago As GSServerModel.ViaPago, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.AnyadirViaPago(oViaPago, bActivLog, bIntegracionViasPago, FSP, sUsuario)
    End Function

    Public Function EliminarViasPago(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.EliminarViasPago(Codigos,  bActivLog, bIntegracionViasPago, FSP, sUsuario)
    End Function


    Public Function CambioCodigoViaPago(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.CambioCodigoViaPago(Codigo, CodigoNuevo, Usuario)

    End Function


End Class
