﻿Imports System.ServiceModel
Public Class FormasPagoRule
    Public Function DevolverFormasPago(ByVal bMostrarIntengracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.DevolverTodasLasFormasPago(bMostrarIntengracion, sIdioma)

    End Function

    Public Function ActualizarFormaPago(ByRef oFormaPago As GSServerModel.FormaPago, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.ActualizarFormaPago(oFormaPago, bRealizarCambioCodigo, bActivLog, bIntegracionFormasPago, FSP, sUsuario)
    End Function


    Public Function AnyadirFormaPago(ByRef oFormaPago As GSServerModel.FormaPago, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.AnyadirFormaPago(oFormaPago, bActivLog, bIntegracionFormasPago, FSP, sUsuario)
    End Function

    Public Function EliminarFormasPago(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.EliminarFormasPago(Codigos, bActivLog, bIntegracionFormasPago, FSP, sUsuario)
    End Function


    Public Function CambioCodigoFormaPago(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.CambioCodigoFormaPago(Codigo, CodigoNuevo, Usuario)

    End Function


End Class
