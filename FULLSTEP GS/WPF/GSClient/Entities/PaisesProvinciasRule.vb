﻿Imports System.ServiceModel
Public Class PaisesProvinciasRule

    Public Function DevolverPaisesProvincias(ByVal bMostrarIntengracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.DevolverTodosLosPaisesProvincias(bMostrarIntengracion, sIdioma)
    End Function

    Public Function DevolverMonedasPais(ByVal sIdioma As String) As System.Data.DataSet
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.DevolverMonedasPais(sIdioma)
    End Function

    Public Function AnyadirPais(ByRef oPais As GSServerModel.Pais, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.AnyadirPais(oPais, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario)

    End Function

    Public Function ActualizarPais(ByRef oPais As GSServerModel.Pais, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.ActualizarPais(oPais, bRealizarCambioCodigo, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario)
    End Function


    Public Function CambioCodigoPais(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.CambioCodigoPais(Codigo, CodigoNuevo, Usuario)

    End Function


    Public Function AnyadirProvincia(ByRef oProvincia As GSServerModel.Provincia, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.AnyadirProvincia(oProvincia, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario)

    End Function

    Public Function ActualizarProvincia(ByRef oProvincia As GSServerModel.Provincia, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.ActualizarProvincia(oProvincia, bRealizarCambioCodigo, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario)
    End Function

    Public Function CambioCodigoProvincia(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String, ByVal sPais As String) As Integer
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.CambioCodigoProvincia(Codigo, CodigoNuevo, Usuario, sPais)

    End Function

    Public Function EliminarProvincias(ByVal CodigosProvincias() As String, ByVal CodigosPaisProvi() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String, ByVal sIdioma As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.EliminarProvincias(CodigosProvincias, CodigosPaisProvi, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario, sIdioma)
    End Function

    Public Function EliminarPaises(ByVal CodigosPaises() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String, ByVal sIdioma As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.EliminarPaises(CodigosPaises, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario, sIdioma)
    End Function

End Class
