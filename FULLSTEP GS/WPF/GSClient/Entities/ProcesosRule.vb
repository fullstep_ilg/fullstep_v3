﻿Imports System.ServiceModel
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.ComponentModel

Public Class ProcesosRule    

#Region " Proceso "

    '<summary>Devuelve los datos de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Objeto Proceso</returns> 

    Public Function ObtenerProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal DevolverEstructura As Boolean = False) As GSServerModel.Proceso
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return (service.DevolverProceso(Anyo, Cod, GMN1, DevolverEstructura))
    End Function

    '<summary>Actualiza los datos de un proceso</summary>
    '<param name="oProceso">Datos del proceso</param>    
    '<returns>Objeto GSException con el error si lo ha habido</returns>

    Public Function ActualizarProceso(ByVal oProceso As GSServerModel.Proceso) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.ActualizarProceso(CType(oProceso, GSServerModel.Proceso))
    End Function

    '<summary>Validación de los atributos obligatorios</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param> 
    '<param name="udtTipoVal">udtTipoVal</param> 
    '<returns>Objeto de tipo ValidacionAtributos con la validación</returns>

    Public Function ValidarAtributosEspObligatorios(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal udtTipoVal As Fullstep.FSNLibrary.TiposDeDatos.TValidacionAtrib) As GSServerModel.ValidacionAtributos
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.ValidarAtributosEspObligatorios(Anyo, Cod, GMN1, udtTipoVal)
    End Function

    Public Function DevolverAsignacionesProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal CriterioOrdenacion As Fullstep.FSNLibrary.TiposDeDatos.TipoOrdenacionAsignaciones = -1, Optional ByVal CodEqp As String = Nothing, Optional ByVal CodComp As String = Nothing, Optional ByVal CodProve As String = Nothing, Optional ByVal DevolverEstructura As Boolean = False) As GSServerModel.Asignaciones
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverAsignacionesProceso(Anyo, Cod, GMN1, CriterioOrdenacion, CodEqp, CodComp, CodProve, DevolverEstructura)
    End Function

    Public Function DevolverCompradoresEquipo(ByVal Equipo As String, Optional ByVal NumMax As Integer = -1, Optional ByVal CarIniCod As String = Nothing, Optional ByVal CarIniApel As String = Nothing, Optional ByVal NoBajaLog As Boolean = False, Optional ByVal OrderByApe As Boolean = False) As GSServerModel.Compradores
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverCompradoresEquipo(Equipo, NumMax, CarIniCod, CarIniApel, NoBajaLog, OrderByApe)
    End Function

    Public Function DevolverPeticionesProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProceProvePetOfertas
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverPeticionesProceso(Anyo, Cod, GMN1)
    End Function

    Public Function InsertarPeticionesProceso(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal TipoInstalacionWeb As TipoInstWeb, ByVal CIA As Integer, ByVal oPets As GSServerModel.ProceProvePetOfertas) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.InsertarPeticionesProceso(Anyo, Proce, GMN1, TipoInstalacionWeb, CIA, oPets)
    End Function

    '<summary>Devuelve los proveedores a publicar</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param> 
    '<param name="Equipo">Equipo</param> 
    '<param name="Com">Com</param> 
    '<returns>Objeto Proveedores con los datos de los proveedores</returns>

    Public Function DevolverProveedoresAPublicar(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, Optional ByVal Equipo As String = Nothing, Optional ByVal Com As String = Nothing) As GSServerModel.Proveedores
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverProveedoresAPublicar(Anyo, Proce, GMN1, Equipo, Com)
    End Function

    Public Function ActivarPublicacionProveedores(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal oProves As GSServerModel.ProcesoProveedores, ByVal InsWeb As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, ByVal sFSPSRV As String, ByVal sFSPBD As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.ActivarPublicacionProveedores(Anyo, Proce, GMN1, oProves, InsWeb, FSP_CIA, sFSPSRV, sFSPBD)
    End Function

    Public Function DesactivarPublicacionProveedores(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal oProves As GSServerModel.ProcesoProveedores, ByVal InsWeb As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal bPremium As Boolean, ByVal FSP_CIA As Integer, ByVal sFSPSRV As String, ByVal sFSPBD As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DesactivarPublicacionProveedores(Anyo, Proce, GMN1, oProves, InsWeb, bPremium, FSP_CIA, sFSPSRV, sFSPBD)
    End Function

#End Region

#Region " ProcesoDef "

    '<summary>Devuelve los datos de un proceso de la tabla Proceso_Def</summary>
    '<param name="Anyo">Año</param>
    '<param name="Proce">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Objeto ProcesoDef</returns> 

    Public Function ObtenerProcesoDef(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoDef
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return (service.DevolverProcesoDef(Anyo, Proce, GMN1))
    End Function

    '<summary>Actualiza los datos de un proceso de la tabla Proce_Def</summary>
    '<param name="oProcesodef">Datos del proceso</param>    
    '<returns>Objeto GSException con el error si lo ha habido</returns>

    Public Function ActualizarProcesoDef(ByVal oProcesoDef As GSServerModel.ProcesoDef) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.ActualizarProcesoDef(oProcesoDef)
    End Function

#End Region

#Region " Proceso Subasta "

    '<summary>Devuelve el objeto de datos necesario para SubastaMonitor</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Objeto SubastaMonitorData</returns> 

    Public Function DevolverDatosSubastaMonitor(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal Idioma As String, Optional ByVal Grupo As Nullable(Of Integer) = Nothing, Optional ByVal Item As Nullable(Of Integer) = Nothing) As GSServerModel.SubastaMonitorData
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverDatosSubastaMonitor(Anyo, Cod, GMN1, Idioma, Grupo, Item)
    End Function

    '<summary>Devuelve el objeto de datos necesario para SubastaConf</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="Idioma">Idioma</param>
    '<returns>Objeto SubastaMonitorData</returns> 

    Public Function DevolverDatosSubastaConf(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal Idioma As String) As GSServerModel.SubastaConfData
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverDatosSubastaConf(Anyo, Cod, GMN1, Idioma)
    End Function
    ''' <summary>
    ''' Devuelve el remitente
    ''' </summary>
    ''' <param name="Anyo"></param>
    ''' <param name="Cod"></param>
    ''' <param name="GMN1"></param>
    ''' <returns></returns>
    Public Function SacarRemitenteEmpresa(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As String
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.SacarRemitenteEmpresa(Anyo, Cod, GMN1)
    End Function

    Public Function DevolverCarpetaPlantillaProce(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As String
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverCarpetaPlantillaProce(Anyo, Cod, GMN1)
    End Function
    '<summary>Devuelve los datos de subasta de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Objeto ProcesoSubasta</returns> 

    Public Function ObtenerProcesoSubasta(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal DevolverEstructura As Boolean = False) As GSServerModel.ProcesoSubasta
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverProcesoSubasta(Anyo, Cod, GMN1, DevolverEstructura)
    End Function

    '<summary>Actualiza los datos de subasta de un proceso</summary>
    '<param name="oProceso">Datos del proceso</param>
    '<param name="oProcesoSubasta">Datos de subasta del proceso</param>
    '<param name="arProcesoProves">Proveedores del proceso</param>
    '<param name="arSubastaReglas">Reglas de la subasta</param>
    '<returns>Objeto GSException con el error si lo ha habido</returns>

    Public Function ActualizarProcesoSubasta(ByVal oProceso As GSServerModel.Proceso, ByVal oProcesoDef As GSServerModel.ProcesoDef, ByVal oProcesoSubasta As GSServerModel.ProcesoSubasta, _
                                             ByVal oProcesoProves As GSServerModel.ProcesoProveedores, ByVal oSubastaReglas As GSServerModel.ProcesoSubastaReglas, ByVal oReglas As GSServerModel.Files) As GSServerModel.GSException
        Dim bActualizarDatos As Boolean = True
        Dim oError As GSServerModel.GSException

        '***** Cambio: Los archivos se suben al ser seleccionados y ya están en el servidor al actualizar el proceso
        ''Antes de realizar la actualización mirar si hay reglas nuevas. Si las hay, enviar los archivos, y una vez enviados
        ''realizar la actualización
        ''Los archivos se guardarán temporalmente en una carpeta y se almacenarán en BD de forma transaccional con la actualización
        ''del resto de datos  
        'Dim bHayReglasNuevas As Boolean = HayReglasNuevas(oSubastaReglas)
        'Dim sFileDirectory As String
        'Dim oFT As GSClient.FileTransfer
        'If bHayReglasNuevas AndAlso Not oReglas Is Nothing AndAlso oReglas.Count > 0 Then
        '    oFT = New GSClient.FileTransfer
        '    sFileDirectory = oFT.BeginFileTransfer()

        '    For Each oRegla As File In oReglas
        '        If oRegla.Nuevo Then
        '            bActualizarDatos = oFT.SendFile(oRegla.Archivo, oRegla.NombreArchivo)
        '        End If
        '    Next
        'End If

        If bActualizarDatos Then
            Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
            'Debug.Write(Date.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") & ": GSClient -> Llamada a ActualizarProcesoSubasta" & Environment.NewLine)
            oError = service.ActualizarProcesoSubasta(oProceso, oProcesoDef, oProcesoSubasta, oProcesoProves, oSubastaReglas, oReglas)
            'Debug.Write(Date.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") & ": GSClient -> Retorno ActualizarProcesoSubasta" & Environment.NewLine)

            'If bHayReglasNuevas AndAlso Not oReglas Is Nothing AndAlso oReglas.Count > 0 Then oFT.EndFileTransfer()
        End If

        Return oError
    End Function

    '<summary>Devuelve los proveedores de una subasta</summary>
    '<param name="Anyo">Anyo</param>
    '<param name="Codigo">Codigo</param>
    '<param name="GMN1">GMN1</param>    
    '<returns>Objeto ProcesoProveedores con los datos de los proveedores</returns>

    Public Function DevolverSubastaProveedores(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoProveedores
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverSubastaProveedores(Anyo, Codigo, GMN1)
    End Function

    '<summary>Comprueba si se han introducido reglas nuevas en la colección de reglas</summary>
    '<param name="oSubastaReglas">Anyo</param>    
    '<returns>Boolean indiando si hay reglas nuevas</returns>
    '<remarks>Llamada desde: ActualizarProcesoSubasta  </remarks>

    Private Function HayReglasNuevas(ByVal oSubastaReglas As GSServerModel.ProcesoSubastaReglas) As Boolean
        HayReglasNuevas = False

        If Not oSubastaReglas Is Nothing AndAlso oSubastaReglas.Count > 0 Then
            For Each oRegla As GSServerModel.ProcesoSubastaRegla In oSubastaReglas
                If oRegla.ID = -1 Then
                    HayReglasNuevas = True
                    Exit Function
                End If
            Next
        End If
    End Function

    '<summary>Compone los correos de notificación de subasta</summary>
    '<param name="Anyo">Anyo</param>             
    '<param name="Codigo">Codigo</param> 
    '<param name="GMN1">GMN1</param> 
    '<param name="InsWEB">InsWEB</param> 
    '<param name="FSP_CIA">FSP_CIA</param> 
    '<param name="FSP_SRV">FSP_SRV</param> 
    '<param name="FSP_BD">FSP_BD</param> 
    '<param name="sFrom">sFrom</param> 
    '<param name="MailSubject">MailSubject</param> 
    '<param name="URLWeb">URLWeb</param> 
    '<returns>Objeto de tipo DatosNotifSubasta con los datos de las correos</returns>

    Public Function ComponerCorreosNotificacionSubasta(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String, ByVal InsWEB As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, _
                                                       ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal DirPlantilla As String, ByVal sFrom As String, ByVal MailSubject As String, ByVal URLWeb As String) As GSServerModel.DatosNotifSubasta
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.ComponerCorreosNotificacionSubasta(Anyo, Codigo, GMN1, InsWEB, FSP_CIA, FSP_SRV, FSP_BD, DirPlantilla, sFrom, MailSubject, URLWeb)
    End Function

    '<summary>Realiza la notificación a los proveedores de la subasta</summary>
    '<param name="Anyo">Anyo</param>             
    '<param name="Codigo">Codigo</param> 
    '<param name="GMN1">GMN1</param> 
    '<param name="InsWEB">InsWEB</param> 
    '<param name="FSP_CIA">FSP_CIA</param> 
    '<param name="FSP_SRV">FSP_SRV</param> 
    '<param name="FSP_BD">FSP_BD</param> 
    '<param name="oDatos">Objeto de tipo DatosNotifSubasta con los datos de los correos</param>     
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns>

    Public Function NotificacionSubasta(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String, ByVal InsWEB As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer,
                                        ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal oDatos As GSServerModel.DatosNotifSubasta, ByVal bUsarRemitenteEmpresa As Boolean) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.NotificacionSubasta(Anyo, Codigo, GMN1, InsWEB, FSP_CIA, FSP_SRV, FSP_BD, oDatos, bUsarRemitenteEmpresa)
    End Function

    '<summary>Compone los correos de notificación de evento de subasta</summary>
    '<param name="Anyo">Anyo</param>             
    '<param name="Codigo">Codigo</param> 
    '<param name="GMN1">GMN1</param> 
    '<param name="InsWEB">InsWEB</param> 
    '<param name="FSP_CIA">FSP_CIA</param> 
    '<param name="FSP_SRV">FSP_SRV</param> 
    '<param name="FSP_BD">FSP_BD</param> 
    '<param name="sFrom">sFrom</param> 
    '<param name="oEvento">Datos del evento</param> 
    '<param name="oEveDesc">Descripciones del evento en los idiomas del GS</param> 
    '<returns>Objeto de tipo DatosNotifSubasta con los datos de las correos</returns>

    Public Function ComponerCorreosNotificacionEventoSubasta(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String, ByVal InsWEB As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal DirPlantilla As String, ByVal sFrom As String, ByVal oEvento As GSServerModel.ProcesoSubastaEvento, ByVal oEveDesc As System.Collections.Generic.Dictionary(Of String, String)) As GSServerModel.EMails
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.ComponerCorreosNotificacionEventoSubasta(Anyo, Codigo, GMN1, InsWEB, FSP_CIA, FSP_SRV, FSP_BD, DirPlantilla, sFrom, oEvento, oEveDesc)
    End Function

    '<summary>Realiza la notificación a los proveedores de la subasta</summary>
    '<param name="Anyo">Anyo</param>             
    '<param name="Codigo">Codigo</param> 
    '<param name="GMN1">GMN1</param> 
    '<param name="InsWEB">InsWEB</param> 
    '<param name="FSP_CIA">FSP_CIA</param> 
    '<param name="FSP_SRV">FSP_SRV</param> 
    '<param name="FSP_BD">FSP_BD</param> 
    '<param name="oDatos">Objeto de tipo DatosNotifSubasta con los datos de los correos</param>     
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns>

    Public Function NotificacionEventoSubasta(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String, ByVal oEMails As GSServerModel.EMails) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.NotificacionEventoSubasta(Anyo, Codigo, GMN1, oEMails)
    End Function

    '<summary>Devuelve los datos de pujas de un proceso en modo subasta</summary>
    '<param name="Anyo">Anyo</param>             
    '<param name="Codigo">Codigo</param> 
    '<param name="GMN1">GMN1</param> 
    '<returns>Objeto de tipo ProcesoSubastaPujas con los datos de las pujas</returns>

    Public Function DevolverProcesoSubastaPujas(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoSubastaPujas
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverProcesoSubastaPujas(Anyo, Codigo, GMN1)
    End Function

    '<summary>Inserta un registro de evento de subasta</summary>
    '<param name="oEvento">Datos del evento</param>             
    '<returns>Objeto de tipo GSException con el error si se ha producido</returns>

    Public Function AccionSubasta(ByVal Evento As GSServerModel.ProcesoSubastaEvento) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.AccionSubasta(Evento)
    End Function

#End Region

#Region " Proceso Proveedores "

    '<summary>Devuelve los proveedores de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Array de objetos ProcesoProveedor</returns> 

    Public Function ObtenerProcesoProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoProveedores
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverProcesoProveedores(Anyo, Cod, GMN1)
    End Function

    '<summary>Actualiza los proveedores de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="arProcesoProves">Array de objetos ProcesoProveedor</param>
    '<returns>Objeto GSException con el error si lo ha habido</returns>

    Public Function ActualizarProcesoProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oProcesoProves As GSServerModel.ProcesoProveedores) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.ActualizarProcesoProveedores(Anyo, Cod, GMN1, oProcesoProves)
    End Function

#End Region

#Region " Proceso Subasta Reglas "

    '<summary>Devuelve las reglas de subasta de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Array de objetos ProcesoSubastaRegla</returns> 

    Public Function ObtenerProcesoSubastaReglas(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoSubastaReglas
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverProcesoSubastaReglas(Anyo, Cod, GMN1)
    End Function

    '<summary>Actualiza las reglas de subasta de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="arSubastaReglas">Array de objetos ProcesoSubastaRegla</param>
    '<returns>Objeto GSException con el error si lo ha habido</returns>

    Public Function ActualizarProcesoSubastaReglas(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oSubastaReglas As GSServerModel.ProcesoSubastaReglas, ByVal oArchivos As GSServerModel.Files) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSFileStreamService
        Return service.ActualizarProcesoSubastaReglas(Anyo, Cod, GMN1, oSubastaReglas, oArchivos)
    End Function

    '<summary>Guarda el archivo de regla indicado en el directorio indicado</summary>
    '<param name="DownloadFolder">Carpeta destino</param>    
    '<param name="FileName">Nombre del archivo</param>    
    '<param name="ID">ID del archivo</param>        
    '<returns>Boolean indicando si el proceso ha finalizado correctamente</returns>

    Public Function DevolverArchivoSubastaRegla(ByVal DownloadFolder As String, ByVal FileName As String, ByVal ID As Integer) As Boolean
        DevolverArchivoSubastaRegla = False

        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSFileStreamService

        'Obtener el tamaño del buffer del máximo tamaño permitido para los mensajes
        Dim iChunkSize As Integer
        If TypeOf service.ChannelFactory.Endpoint.Binding Is System.ServiceModel.BasicHttpBinding Then
            Dim oHTTPBinding As System.ServiceModel.BasicHttpBinding = service.ChannelFactory.Endpoint.Binding
            iChunkSize = oHTTPBinding.ReaderQuotas.MaxArrayLength
        ElseIf TypeOf service.ChannelFactory.Endpoint.Binding Is System.ServiceModel.WSHttpBinding Then
            Dim oHTTPBinding As System.ServiceModel.WSHttpBinding = service.ChannelFactory.Endpoint.Binding
            iChunkSize = oHTTPBinding.ReaderQuotas.MaxArrayLength
        End If

        Dim oFS As New System.IO.FileStream(DownloadFolder & FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write)

        Dim iOffset As Integer = 0
        Dim Buffer() As Byte = service.DevolverArchivoSubastaRegla(iOffset, iChunkSize, ID)
        While Buffer.Length > 0
            oFS.Write(Buffer, 0, Buffer.Length)

            iOffset += Buffer.Length
            Buffer = service.DevolverArchivoSubastaRegla(iOffset, iChunkSize, ID)
        End While

        oFS.Close()

        DevolverArchivoSubastaRegla = True
    End Function

#End Region

#Region " ProcesoGrupos "

    '<summary>Devuelve los grupos de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Array de objetos ProcesoGrupo</returns> 

    Public Function ObtenerProcesoGrupos(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal DevolverEstructura As Boolean = False) As GSServerModel.ProcesoGrupos
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverProcesoGrupos(Anyo, Cod, GMN1, DevolverEstructura)
    End Function

#End Region

#Region " ProcesoGrupoItem "

    '<summary>Devuelve los items del grupo de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="ID">ID</param>
    '<returns>Array de objetos Item</returns> 

    Public Function ObtenerProcesoGrupoItems(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal ID As Integer) As GSServerModel.Items
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverProcesoGrupoItems(Anyo, Cod, GMN1, ID)
    End Function

#End Region

End Class

