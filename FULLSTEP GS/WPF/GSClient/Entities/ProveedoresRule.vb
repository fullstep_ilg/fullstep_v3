﻿Imports System.ServiceModel
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.ComponentModel

Public Class ProveedoresRule    

    '<summary>Devuelve los contactos de un proveedor</summary>
    '<param name="Prove">Proveedor</param>    
    '<returns>Array de objetos Contacto</returns> 

    Public Function DevolverProveedorContactos(ByVal Prove As String) As GSServerModel.Contactos
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverProveedorContactos(Prove)
    End Function

    Public Function DevolverEstadoProveedorPortal(ByVal Prove As String, ByVal Cia As Integer) As GSServerModel.EstadoProveedorPortal
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.DevolverEstadoProveedorPortal(Prove, Cia)
    End Function

    Public Function ComprobarEmailEnPortal(ByVal CodProvePortal As String, ByVal EMail As String) As Boolean
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSProcessService
        Return service.ComprobarEmailEnPortal(CodProvePortal, EMail, ParametrosGenerales.gsFSP_SRV, ParametrosGenerales.gsFSP_BD)
    End Function

End Class
