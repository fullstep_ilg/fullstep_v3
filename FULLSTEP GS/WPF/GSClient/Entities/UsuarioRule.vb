﻿Imports System.ServiceModel
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class UsuarioRule

    Public Function DevolverDatosUsuario(ByVal sUsu As String, ByVal sIdi As String) As GSServerModel.Usuario
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService
        Return service.CargarDatosUsu(sUsu, sIdi)
    End Function

    Public Function DevolverPersona(ByVal Codigo As String) As GSServerModel.Persona
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()

        Return service.DevolverPersona(Codigo)
    End Function

End Class
