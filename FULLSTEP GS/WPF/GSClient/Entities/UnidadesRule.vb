﻿
Imports System.ServiceModel

Public Class UnidadesRule
    Public Function DevolverUnidades(ByVal bMostrarIntengracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.DevolverTodasLasUnidades(bMostrarIntengracion, sIdioma)

    End Function


    Public Function ActualizarUnidad(ByRef oUnidad As GSServerModel.Unidad, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.ActualizarUnidad(oUnidad, bRealizarCambioCodigo, bActivLog, bIntegracionUnidades, FSP, sUsuario)
    End Function


    Public Function AnyadirUnidad(ByRef oUnidad As GSServerModel.Unidad, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.AnyadirUnidad(oUnidad, bActivLog, bIntegracionUnidades, FSP, sUsuario)
    End Function

    Public Function EliminarUnidades(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.EliminarUnidades(Codigos, bActivLog, bIntegracionUnidades, FSP, sUsuario)
    End Function


    Public Function CambioCodigoUnidad(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.CambioCodigoUnidad(Codigo, CodigoNuevo, Usuario)

    End Function


End Class
