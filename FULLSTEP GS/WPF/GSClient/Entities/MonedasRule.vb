﻿Imports System.ServiceModel
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class MonedasRule
    Public Function DevolverMonedas(ByVal bMostrarIntengracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()

        Return service.DevolverTodasLasMonedas(bMostrarIntengracion, sIdioma)
    End Function


    Public Function ActualizarMoneda(ByRef oMon As GSServerModel.Moneda, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.ActualizarMoneda(oMon, bRealizarCambioCodigo, iInstWeb, bActivLog, bIntegracionMonedas, FSP, sUsuario)
    End Function


    Public Function AnyadirMoneda(ByRef oMon As GSServerModel.Moneda, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.AnyadirMoneda(oMon, iInstWeb, bActivLog, bIntegracionMonedas, FSP, sUsuario)

    End Function

    Public Function EliminarMonedas(ByVal Codigos() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sMonedaCentral As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.EliminarMonedas(Codigos, iInstWeb, bActivLog, bIntegracionMonedas, FSP, sMonedaCentral, sUsuario)
    End Function


    Public Function CambioCodigoMoneda(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.CambioCodigoMoneda(Codigo, CodigoNuevo, Usuario)

    End Function

    Public Function DevolverMoneda(ByVal Moneda As String, ByVal Idioma As String) As GSServerModel.Moneda
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.DevolverMoneda(Moneda, Idioma)
    End Function

End Class
