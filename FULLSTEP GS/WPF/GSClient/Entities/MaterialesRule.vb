﻿Imports System.ServiceModel
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel

Public Class MaterialesRule

    Public Function DevolverEstructuraMaterialesImpuestos(ByVal iIdImpuestoValor As Integer, ByVal sIdioma As String, Optional ByVal sCodEqpComprador As String = Nothing, _
            Optional ByVal sCodComprador As String = Nothing, Optional ByVal bOrdPorCod As Boolean = False) As DataSet
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.DevolverEstructuraMaterialesImpuestos(iIdImpuestoValor, sIdioma, sCodEqpComprador, sCodComprador, bOrdPorCod)
    End Function

    'Public Function ActualizarEstructuraMaterialesImpuestos(ByVal _iIdImpuestoValor As Integer, ByVal oAsignar As GruposMaterial, ByVal oDesasignar As GruposMaterial, _
    '        ByVal oIncluir As GruposMaterial, ByVal oExcluir As GruposMaterial, ByVal oArtAsignar As Articulos, ByVal oArtDesasignar As Articulos, _
    '        ByVal oArtIncluir As Articulos, ByVal oArtExcluir As Articulos) As GSServerModel.GSException
    Public Function ActualizarEstructuraMaterialesImpuestos(ByVal _iIdImpuestoValor As Integer, ByVal oAsignar As GruposMaterial, ByVal oDesasignar As GruposMaterial, _
            ByVal oArtAsignar As Articulos, ByVal oArtDesasignar As Articulos, ByVal oArtIncluir As Articulos, ByVal oArtExcluir As Articulos) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.ActualizarEstructuraMaterialesImpuestos(_iIdImpuestoValor, oAsignar, oDesasignar, oArtAsignar, oArtDesasignar, oArtIncluir, oArtExcluir)
    End Function

    Public Function DevolverArticulosGMNImpuestos(ByVal IdImpuestoValor As Integer, ByVal oGMN As GSServerModel.GrupoMaterial) As GSServerModel.Articulos
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.DevolverArticulosGMNImpuestos(IdImpuestoValor, oGMN)
    End Function

End Class
