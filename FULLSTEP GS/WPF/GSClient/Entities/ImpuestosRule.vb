﻿Imports System.ServiceModel
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class ImpuestosRule

    Public Function DevolverDatosImpuestos(ByVal sIdioma As String) As GSServerModel.ImpuestosData
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.DevolverDatosImpuestos(sIdioma)
    End Function

    Public Function DevolverImpuestos(ByVal sIdioma As String) As GSServerModel.Impuestos
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.DevolverImpuestos(sIdioma)
    End Function

    Public Function InsertarImpuesto(ByRef oImpuesto As GSServerModel.Impuesto) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.InsertarImpuesto(oImpuesto)
    End Function

    Public Function EliminarImpuestos(ByVal ID() As Integer) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.EliminarImpuestos(ID)
    End Function

    Public Function ActualizarImpuesto(ByRef oImpuesto As GSServerModel.Impuesto) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.ActualizarImpuesto(oImpuesto)
    End Function

    Public Function InsertarImpuestoValor(ByVal IdImpuesto As Integer, ByRef oImpuestoValor As GSServerModel.ImpuestoValor) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.InsertarImpuestoValor(IdImpuesto, oImpuestoValor)
    End Function

    Public Function ActualizarImpuestoValor(ByRef oImpuestoValor As GSServerModel.ImpuestoValor) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.ActualizarImpuestoValor(oImpuestoValor)
    End Function

    Public Function EliminarImpuestoValores(ByVal ID() As Integer) As GSServerModel.GSException
        Dim service = GSClientServices.ServiceFactory.Instance.GetFullGSService()
        Return service.EliminarImpuestoValores(ID)
    End Function
End Class
