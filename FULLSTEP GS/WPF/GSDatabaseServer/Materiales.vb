﻿Imports Fullstep.FSNLibrary
Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel

<Serializable()> _
Public Class Materiales
    Inherits DBServerBase

    '<summary>Devuelve los impuestos existentes</summary>
    '<param name="iIdImpuestoValor">Id. impuesto-valor</param>
    '<param name="sIdioma">Idioma</param>
    '<param name="sCodEqpComprador">Cod. equipo comprador</param>
    '<param name="sCodComprador">Cod. comprador</param>
    '<param name="bOrdPorCod">Orden por código</param>
    '<returns>Dataset con los datos de los materiales</returns>  
    '<revision>LTG 10/09/2012</revision>

    Public Function DevolverEstructuraMaterialesImpuestos(ByVal iIdImpuestoValor As Integer, ByVal sIdioma As String, Optional ByVal sCodEqpComprador As String = Nothing, _
            Optional ByVal sCodComprador As String = Nothing, Optional ByVal bOrdPorCod As Boolean = False) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_ESTRUCTURA_MATERIALES_IMPUESTOS"
        With cm.Parameters
            .Add("@IMPUESTO_PP_VALOR", SqlDbType.Int)
            cm.Parameters("@IMPUESTO_PP_VALOR").Value = iIdImpuestoValor
            .Add("@IDI", SqlDbType.VarChar)
            cm.Parameters("@IDI").Value = sIdioma
            .Add("@CODEQP", SqlDbType.VarChar)
            cm.Parameters("@CODEQP").Value = sCodEqpComprador
            .Add("@CODCOMP", SqlDbType.VarChar)
            cm.Parameters("@CODCOMP").Value = sCodComprador
            .Add("@ORDCOD", SqlDbType.TinyInt)
            cm.Parameters("@ORDCOD").Value = bOrdPorCod
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Actualiza los cambios en la asignacion de impuestos a materiales</summary>
    '<param name="iIdImpuestoValor">Id. impuesto valor</param>
    '<param name="oAsignar">Materiales a asignar</param>
    '<param name="oDesasignar">Materiales a desasignar</param>
    '<param name="oIncluir">Materiales a incluir</param>
    '<param name="oExcluir">Materiales a excluir</param>        
    '<param name="oArtAsignar">Artículos a asignar</param>
    '<param name="oArtDesasignar">Artículos a desasignar</param>
    '<param name="oArtIncluir">Artículos a incluir</param>    
    '<param name="oArtExcluir">Artículos a excluir</param>    
    '<returns>Objeto error con el error si se ha producido</returns>  
    '<revision>LTG 13/09/2012</revision>

    Public Function ActualizarEstructuraMaterialesImpuestos(ByVal _iIdImpuestoValor As Integer, ByVal oAsignar As GSServerModel.GruposMaterial, _
            ByVal oDesasignar As GSServerModel.GruposMaterial, ByVal oArtAsignar As GSServerModel.Articulos, ByVal oArtDesasignar As GSServerModel.Articulos, _
            ByVal oArtIncluir As GSServerModel.Articulos, ByVal oArtExcluir As GSServerModel.Articulos) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            'Desasignaciones
            If oDesasignar.Count > 0 Then
                For Each oGMN As GrupoMaterial In oDesasignar
                    Dim Cm As SqlCommand = NuevoStoredActualizarEstructuraMaterialesImpuestos("FSGS_DESASIGNAR_IMPUESTO_MATERIAL_ARTICULO", cn, _iIdImpuestoValor, oGMN)
                    Cm.ExecuteNonQuery()
                Next
            End If

            'Asignaciones
            If oAsignar.Count > 0 Then
                For Each oGMN As GrupoMaterial In oAsignar
                    Dim cm As SqlCommand = NuevoStoredActualizarEstructuraMaterialesImpuestos("FSGS_ASIGNAR_IMPUESTO_MATERIAL_ARTICULO", cn, _iIdImpuestoValor, oGMN)
                    cm.ExecuteNonQuery()
                Next
            End If

            ''Inclusiones
            'If oIncluir.Count > 0 Then
            '    For Each oGMN As GrupoMaterial In oIncluir
            '        Dim Cm As SqlCommand = NuevoStoredActualizarEstructuraMaterialesImpuestos("FSGS_INCLUIR_IMPUESTO_MATERIAL_ARTICULO", cn, _iIdImpuestoValor, oGMN)
            '        Cm.ExecuteNonQuery()
            '    Next
            'End If

            ''Exclusiones
            'If oExcluir.Count > 0 Then
            '    For Each oGMN As GrupoMaterial In oExcluir
            '        Dim Cm As SqlCommand = NuevoStoredActualizarEstructuraMaterialesImpuestos("FSGS_EXCLUIR_IMPUESTO_MATERIAL_ARTICULO", cn, _iIdImpuestoValor, oGMN)
            '        Cm.ExecuteNonQuery()
            '    Next
            'End If

            'Desasignaciones artículos
            If oArtAsignar.Count > 0 Then
                For Each oArt As Articulo In oArtAsignar
                    Dim Cm As SqlCommand = NuevoStoredActualizarArticulosImpuestos("FSGS_ASIGNAR_IMPUESTO_MATERIAL_ARTICULO", cn, _iIdImpuestoValor, oArt)
                    Cm.ExecuteNonQuery()
                Next
            End If

            'Asignaciones artículos
            If oArtDesasignar.Count > 0 Then
                For Each oArt As Articulo In oArtDesasignar
                    Dim Cm As SqlCommand = NuevoStoredActualizarArticulosImpuestos("FSGS_DESASIGNAR_IMPUESTO_MATERIAL_ARTICULO", cn, _iIdImpuestoValor, oArt)
                    Cm.ExecuteNonQuery()
                Next
            End If

            'Inclusiones artículos
            If oArtIncluir.Count > 0 Then
                For Each oArt As Articulo In oArtIncluir
                    Dim Cm As SqlCommand = NuevoStoredActualizarArticulosImpuestos("FSGS_INCLUIR_IMPUESTO_MATERIAL_ARTICULO", cn, _iIdImpuestoValor, oArt)
                    Cm.ExecuteNonQuery()
                Next
            End If

            'Exclusiones artículos
            If oArtExcluir.Count > 0 Then
                For Each oArt As Articulo In oArtExcluir
                    Dim Cm As SqlCommand = NuevoStoredActualizarArticulosImpuestos("FSGS_EXCLUIR_IMPUESTO_MATERIAL_ARTICULO", cn, _iIdImpuestoValor, oArt)
                    Cm.ExecuteNonQuery()
                Next
            End If

            Me.CommitTransaction(cn)
            cn.Close()

        Catch ex As SqlException
            oError = TratarError(ex)
            If Not cn Is Nothing Then Me.RollbackTransaction(cn)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Crea un stored para la actualización de la asignación materiales-impuestos</summary>
    '<param name="sStoredName">Nombre del stored</param>
    '<param name="cn">Conexión</param>
    '<param name="_iIdImpuestoValor">Id. impuesto valor</param>
    '<param name="oGMN">Material</param>    
    '<returns>Objeto sqlcommand</returns>  
    '<revision>LTG 13/09/2012</revision>

    Private Function NuevoStoredActualizarEstructuraMaterialesImpuestos(ByVal sStoredName As String, ByVal cn As SqlConnection, ByVal _iIdImpuestoValor As Integer, _
            ByVal oGMN As GrupoMaterial) As SqlCommand
        Dim Cm As New SqlCommand(sStoredName, cn)
        Cm.CommandType = CommandType.StoredProcedure
        With Cm.Parameters
            .Add("@IMPUESTO_PP_VALOR", SqlDbType.Int)
            .Add("@GMN1", SqlDbType.VarChar)
            .Add("@GMN2", SqlDbType.VarChar)
            .Add("@GMN3", SqlDbType.VarChar)
            .Add("@GMN4", SqlDbType.VarChar)
        End With
        Cm.Parameters("@IMPUESTO_PP_VALOR").Value = _iIdImpuestoValor
        Cm.Parameters("@GMN1").Value = oGMN.GMN1
        If Not oGMN.GMN2 Is Nothing Then Cm.Parameters("@GMN2").Value = oGMN.GMN2
        If Not oGMN.GMN3 Is Nothing Then Cm.Parameters("@GMN3").Value = oGMN.GMN3
        If Not oGMN.GMN4 Is Nothing Then Cm.Parameters("@GMN4").Value = oGMN.GMN4

        Return Cm
    End Function

    '<summary>Crea un stored para la actualización de la asignación artículos-impuestos</summary>
    '<param name="sStoredName">Nombre del stored</param>
    '<param name="cn">Conexión</param>
    '<param name="_iIdImpuestoValor">Id. impuesto valor</param>
    '<param name="oArt">Artículo</param>    
    '<returns>Objeto sqlcommand</returns>  
    '<revision>LTG 13/09/2012</revision>

    Private Function NuevoStoredActualizarArticulosImpuestos(ByVal sStoredName As String, ByVal cn As SqlConnection, ByVal _iIdImpuestoValor As Integer, _
            ByVal oArt As Articulo) As SqlCommand
        Dim Cm As New SqlCommand(sStoredName, cn)
        Cm.CommandType = CommandType.StoredProcedure
        With Cm.Parameters
            .Add("@IMPUESTO_PP_VALOR", SqlDbType.Int)
            .Add("@GMN1", SqlDbType.VarChar)
            .Add("@GMN2", SqlDbType.VarChar)
            .Add("@GMN3", SqlDbType.VarChar)
            .Add("@GMN4", SqlDbType.VarChar)
            .Add("@ART", SqlDbType.VarChar)
        End With
        Cm.Parameters("@IMPUESTO_PP_VALOR").Value = _iIdImpuestoValor
        Cm.Parameters("@GMN1").Value = oArt.GMN1
        Cm.Parameters("@GMN2").Value = oArt.GMN2
        Cm.Parameters("@GMN3").Value = oArt.GMN3
        Cm.Parameters("@GMN4").Value = oArt.GMN4
        Cm.Parameters("@ART").Value = oArt.Codigo

        Return Cm
    End Function

    '<summary>Devuelve las asignaciones a impuestos de los artículos de un material</summary>
    '<param name="IdImpuestoValor">Id. impuesto-valor</param>  
    '<param name="oGMN">Material</param>    
    '<returns>Datatable con los datos de los artículos</returns>  
    '<revision>LTG 13/09/2012</revision>

    Public Function DevolverArticulosGMNImpuestos(ByVal IdImpuestoValor As Integer, ByVal oGMN As GSServerModel.GrupoMaterial) As DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_ARTICULOS_IMPUESTOS"
        With cm.Parameters
            .Add("@IMPUESTO_PP_VALOR", SqlDbType.Int)
            cm.Parameters("@IMPUESTO_PP_VALOR").Value = IdImpuestoValor
            .Add("@GMN1", SqlDbType.VarChar)
            cm.Parameters("@GMN1").Value = oGMN.GMN1
            .Add("@GMN2", SqlDbType.VarChar)
            cm.Parameters("@GMN2").Value = oGMN.GMN2
            .Add("@GMN3", SqlDbType.VarChar)
            cm.Parameters("@GMN3").Value = oGMN.GMN3
            .Add("@GMN4", SqlDbType.VarChar)
            cm.Parameters("@GMN4").Value = oGMN.GMN4
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

End Class
