﻿Imports Fullstep.FSNLibrary
Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel

<Serializable()> _
Public Class Impuestos
    Inherits DBServerBase

    '<summary>Devuelve los impuestos existentes</summary>
    '<param name="sIdioma">Idioma de las descripciones de paises y provincias</param>
    '<returns>Dataset con los datos de los impuestos</returns> 
    '<revision>LTG 17/08/2012</revision>

    Public Function DevolverImpuestos(ByVal sIdioma As String) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_IMPUESTOS"
        With cm.Parameters
            .Add("@IDIOMA", SqlDbType.VarChar)
            cm.Parameters("@IDIOMA").Value = sIdioma
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)            
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Inserta un nuevo impuesto en BD</summary>
    '<param name="oImpuesto">Nuevo impuesto</param>
    '<returns>Objeto de tipo GSException con el error si se ha producido</returns> 
    '<remarks>Llamada desde ImpuestosRule.InsertarImpuesto</remarks>
    '<revision>LTG 27/08/2012</revision>

    Public Function InsertarImpuesto(ByRef oImpuesto As GSServerModel.Impuesto) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            'Insertar impuesto
            Dim cm As New SqlCommand("FSGS_INSERTAR_IMPUESTO", cn)
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .Add("@COD", SqlDbType.NVarChar)
                .Add("@CONCEPTO", SqlDbType.TinyInt)
                .Add("@RETENIDO", SqlDbType.TinyInt)
                .Add("@GRPCOMP", SqlDbType.TinyInt)
                .Add("@COMMENT", SqlDbType.NVarChar)
                .Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
                .Add("@FECACT", SqlDbType.DateTime).Direction = ParameterDirection.Output                
            End With
            cm.Parameters("@COD").Value = oImpuesto.Codigo
            If Not oImpuesto.Concepto Is Nothing Then cm.Parameters("@CONCEPTO").Value = oImpuesto.Concepto
            cm.Parameters("@RETENIDO").Value = BooleanToSQLBinary(oImpuesto.Retenido)
            If Not oImpuesto.Comentario Is Nothing Then cm.Parameters("@COMMENT").Value = oImpuesto.Comentario
            cm.Parameters("@GRPCOMP").Value = oImpuesto.GrupoCompatibilidad
            cm.ExecuteNonQuery()

            oImpuesto.Id = cm.Parameters("@ID").Value
            oImpuesto.FechaAct = cm.Parameters("@FECACT").Value

            'Insertar denominaciones
            If Not oImpuesto.Denominaciones Is Nothing AndAlso oImpuesto.Denominaciones.Count > 0 Then
                For Each oDen As GSServerModel.Multiidioma In oImpuesto.Denominaciones
                    Dim oCm As New SqlCommand("FSGS_INSERTAR_IMPUESTO_DENOMINACION", cn)
                    oCm.CommandType = CommandType.StoredProcedure
                    With oCm.Parameters
                        .Add("@IMPUESTO", SqlDbType.VarChar)
                        .Add("@IDIOMA", SqlDbType.VarChar)
                        .Add("@DEN", SqlDbType.VarChar)
                        .Add("@FECACT", SqlDbType.DateTime).Direction = ParameterDirection.Output
                    End With
                    oCm.Parameters("@IMPUESTO").Value = oImpuesto.Id
                    oCm.Parameters("@IDIOMA").Value = oDen.Idioma
                    oCm.Parameters("@DEN").Value = oDen.Denominacion
                    oCm.ExecuteNonQuery()

                    oDen.FecAct = oCm.Parameters("@FECACT").Value
                Next
            End If

            Me.CommitTransaction(cn)
            cn.Close()

        Catch ex As SqlException
            oError = TratarError(ex)
            If Not cn Is Nothing Then Me.RollbackTransaction(cn)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Actualiza un impuesto en BD</summary>
    '<param name="oImpuesto">Objeto Impuesto con los datos a actualizar</param>
    '<returns>Objeto de tipo GSException con el error si se ha producido</returns> 
    '<remarks>Llamada desde ImpuestosRule.ActualizarImpuesto</remarks>
    '<revision>LTG 28/08/2012</revision>

    Public Function ActualizarImpuesto(ByRef oImpuesto As GSServerModel.Impuesto) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            'Actualizar impuesto
            Dim cm As New SqlCommand("FSGS_ACTUALIZAR_IMPUESTO", cn)
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .Add("@ID", SqlDbType.Int)
                .Add("@COD", SqlDbType.NVarChar)
                .Add("@CONCEPTO", SqlDbType.TinyInt)
                .Add("@RETENIDO", SqlDbType.TinyInt)
                .Add("@GRPCOMP", SqlDbType.TinyInt)
                .Add("@COMMENT", SqlDbType.NVarChar)
                .Add("@FECACT", SqlDbType.DateTime).Direction = ParameterDirection.Output
            End With
            cm.Parameters("@ID").Value = oImpuesto.Id
            cm.Parameters("@COD").Value = oImpuesto.Codigo
            If Not oImpuesto.Concepto Is Nothing Then cm.Parameters("@CONCEPTO").Value = oImpuesto.Concepto
            cm.Parameters("@RETENIDO").Value = BooleanToSQLBinary(oImpuesto.Retenido)
            cm.Parameters("@GRPCOMP").Value = oImpuesto.GrupoCompatibilidad
            If Not oImpuesto.Comentario Is Nothing Then cm.Parameters("@COMMENT").Value = oImpuesto.Comentario
            cm.ExecuteNonQuery()

            oImpuesto.FechaAct = cm.Parameters("@FECACT").Value

            'Actualizar denominaciones                       
            If Not oImpuesto.Denominaciones Is Nothing AndAlso oImpuesto.Denominaciones.Count > 0 Then
                For Each oDen As GSServerModel.Multiidioma In oImpuesto.Denominaciones
                    Dim oCm As New SqlCommand("FSGS_ACTUALIZAR_IMPUESTO_DENOMINACION", cn)
                    oCm.CommandType = CommandType.StoredProcedure
                    With oCm.Parameters
                        .Add("@IMPUESTO", SqlDbType.VarChar)
                        .Add("@IDIOMA", SqlDbType.VarChar)
                        .Add("@DEN", SqlDbType.VarChar)
                        .Add("@FECACT", SqlDbType.DateTime).Direction = ParameterDirection.Output
                    End With
                    oCm.Parameters("@IMPUESTO").Value = oImpuesto.Id
                    oCm.Parameters("@IDIOMA").Value = oDen.Idioma
                    oCm.Parameters("@DEN").Value = oDen.Denominacion
                    oCm.ExecuteNonQuery()

                    oDen.FecAct = oCm.Parameters("@FECACT").Value
                Next            
            End If

            Me.CommitTransaction(cn)
            cn.Close()

        Catch ex As SqlException
            oError = TratarError(ex)
            If Not cn Is Nothing Then Me.RollbackTransaction(cn)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Elimina los impuestos cuyos Id están en el array</summary>
    '<param name="IDs">Arrary con los Ids a eliminar</param>
    '<returns>Objeto de tipo GSException con el error si se ha producido</returns> 
    '<remarks>Llamada desde ImpuestosRule.EliminarImpuesto</remarks>
    '<revision>LTG 27/08/2012</revision>

    Public Function EliminarImpuestos(ByVal IDs() As Integer) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            Dim cm As New SqlCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "FSGS_ELIMINAR_IMPUESTO"
            cm.Connection = cn
            With cm.Parameters
                .Add("@ID", SqlDbType.Int)
            End With
            For i As Integer = 0 To IDs.Count - 1
                cm.Parameters("@ID").Value = IDs(i)
                cm.ExecuteNonQuery()
            Next

            Me.CommitTransaction(cn)
            cn.Close()

        Catch ex As SqlException
            oError = TratarError(ex)
            If Not cn Is Nothing Then Me.RollbackTransaction(cn)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Inserta un nuevo valor de un impuesto en BD</summary>
    '<param name="IdImpuesto">Id del impuesto</param>
    '<param name="oImpuestoValor">Nuevo valor del impuesto</param>
    '<returns>Objeto de tipo GSException con el error si se ha producido</returns> 
    '<remarks>Llamada desde ImpuestosRule.InsertarImpuestoValor</remarks>
    '<revision>LTG 30/08/2012</revision>

    Public Function InsertarImpuestoValor(ByVal IdImpuesto As Integer, ByRef oImpuestoValor As ImpuestoValor) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            'Insertar impuesto
            Dim cm As New SqlCommand("FSGS_INSERTAR_IMPUESTO_VALOR", cn)
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .Add("@IMPUESTO", SqlDbType.Int)
                .Add("@PAIS", SqlDbType.VarChar)
                .Add("@PROVINCIA", SqlDbType.VarChar)
                .Add("@VALOR", SqlDbType.Float)
                .Add("@VIGENTE", SqlDbType.TinyInt)
                .Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
                .Add("@FECACT", SqlDbType.DateTime).Direction = ParameterDirection.Output
            End With
            cm.Parameters("@IMPUESTO").Value = IdImpuesto
            cm.Parameters("@PAIS").Value = oImpuestoValor.CodigoPais
            cm.Parameters("@PROVINCIA").Value = oImpuestoValor.CodigoProvincia
            cm.Parameters("@VALOR").Value = oImpuestoValor.Valor
            cm.Parameters("@VIGENTE").Value = BooleanToSQLBinary(oImpuestoValor.Vigente)
            cm.ExecuteNonQuery()

            oImpuestoValor.Id = cm.Parameters("@ID").Value
            oImpuestoValor.FechaAct = cm.Parameters("@FECACT").Value

            Me.CommitTransaction(cn)
            cn.Close()

        Catch ex As SqlException
            oError = TratarError(ex)
            If Not cn Is Nothing Then Me.RollbackTransaction(cn)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Actualiza el valor de un impuesto en BD</summary>
    '<param name="oImpuestoValor">Objeto ImpuestoValor con los datos a actualizar</param>
    '<returns>Objeto de tipo GSException con el error si se ha producido</returns> 
    '<remarks>Llamada desde ImpuestosRule.ActualizarImpuestoValor</remarks>
    '<revision>LTG 28/08/2012</revision>

    Public Function ActualizarImpuestoValor(ByRef oImpuestoValor As GSServerModel.ImpuestoValor) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            'Actualizar valor
            Dim cm As New SqlCommand("FSGS_ACTUALIZAR_IMPUESTO_VALOR", cn)
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .Add("@ID", SqlDbType.Int)
                .Add("@VALOR", SqlDbType.Float)
                .Add("@VIGENTE", SqlDbType.TinyInt)
                .Add("@FECACT", SqlDbType.DateTime).Direction = ParameterDirection.Output
            End With
            cm.Parameters("@ID").Value = oImpuestoValor.Id
            cm.Parameters("@VALOR").Value = oImpuestoValor.Valor
            cm.Parameters("@VIGENTE").Value = BooleanToSQLBinary(oImpuestoValor.Vigente)
            cm.ExecuteNonQuery()

            oImpuestoValor.FechaAct = cm.Parameters("@FECACT").Value

            Me.CommitTransaction(cn)
            cn.Close()

        Catch ex As SqlException
            oError = TratarError(ex)
            If Not cn Is Nothing Then Me.RollbackTransaction(cn)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Elimina los avlores cuyos Id están en el array</summary>
    '<param name="IDs">Arrary con los Ids a eliminar</param>
    '<returns>Objeto de tipo GSException con el error si se ha producido</returns> 
    '<remarks>Llamada desde ImpuestosRule.EliminarImpuestoValores</remarks>
    '<revision>LTG 30/08/2012</revision>

    Public Function EliminarImpuestoValores(ByVal IDs() As Integer) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            Dim cm As New SqlCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "FSGS_ELIMINAR_IMPUESTO_VALORES"
            cm.Connection = cn
            With cm.Parameters
                .Add("@ID", SqlDbType.Int)
            End With
            For i As Integer = 0 To IDs.Count - 1
                cm.Parameters("@ID").Value = IDs(i)
                cm.ExecuteNonQuery()
            Next

            Me.CommitTransaction(cn)
            cn.Close()

        Catch ex As SqlException
            oError = TratarError(ex)
            If Not cn Is Nothing Then Me.RollbackTransaction(cn)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

End Class
