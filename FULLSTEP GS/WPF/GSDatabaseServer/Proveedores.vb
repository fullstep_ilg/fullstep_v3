﻿Imports Fullstep.FSNLibrary
Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel

<Serializable()> _
Public Class Proveedores
    Inherits DBServerBase

#Region " Proveedor "

    '<summary>Devuelve los datos de un proveedor</summary>
    '<param name="Prove">CÃ³digo del proveedor</param>    
    '<returns>Dataset con los datos del provedor</returns> 

    Public Function DevolverProveedor(ByVal Prove As String) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_PROVEEDOR"
        cm.Parameters.Add("@PROVE", SqlDbType.VarChar, 50)
        cm.Parameters("@PROVE").Value = Prove

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Devuelve contactos</summary>
    '<param name="Prove">Prove</param>
    '<param name="Cia">Compañía</param>    
    '<returns>Dataset con los datos de autorización del proveedor en el portal</returns> 

    Public Function DevolverEstadoProveedorPortal(ByVal Prove As String, ByVal Cia As Integer) As DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_ESTADO_PROVEEDOR_PORTAL"
        cm.Parameters.Add("@PROVE", SqlDbType.VarChar, 50)
        cm.Parameters("@PROVE").Value = Prove
        cm.Parameters.Add("@CIA", SqlDbType.Int)
        cm.Parameters("@CIA").Value = Cia

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Comprueba el email del proveedor en el portal</summary>
    '<param name="CodProvePortal">Proveedor</param>
    '<param name="EMail">Email</param>    
    '<returns>Objeto AutorizacionPortal con los datos de la autorización</returns>    

    Public Function ComprobarEmailEnPortal(ByVal CodProvePortal As String, ByVal EMail As String, ByVal FSP_SRV As String, ByVal FSP_BD As String) As Boolean
        ComprobarEmailEnPortal = False

        Dim sFSP As String = FSP_SRV & "." & FSP_BD & ".dbo."

        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = sFSP & "SP_COMPROBAR_EMAIL"
        cm.Parameters.Add("@FSP_CIA", SqlDbType.VarChar, 20)
        cm.Parameters("@FSP_CIA").Value = CodProvePortal
        cm.Parameters.Add("@EMAIL", SqlDbType.VarChar, 100)
        cm.Parameters("@EMAIL").Value = EMail
        Dim oParam As New SqlParameter("@EXISTE", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, Nothing, DataRowVersion.Current, Nothing)
        cm.Parameters.Add(oParam)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            EjecutarComando(cm, cn)
            Return (oParam.Value > 0)

            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

#End Region

#Region " Contactos "

    '<summary>Devuelve contactos</summary>
    '<param name="Prove">Prove</param>
    '<param name="ID">ID</param>    
    '<returns>Dataset con los datos de los contactos</returns> 

    Public Function DevolverContactos(Optional ByVal Prove As String = Nothing, Optional ByVal ID As Integer = -1, Optional ByVal Subasta As Boolean = False, _
                                      Optional ByVal RecibePeticiones As Boolean = False) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_CONTACTOS"
        If Not Prove Is Nothing Then
            cm.Parameters.Add("@PROVE", SqlDbType.VarChar, 50)
            cm.Parameters("@PROVE").Value = Prove
        End If
        If ID <> -1 Then
            cm.Parameters.Add("@ID", SqlDbType.Int)
            cm.Parameters("@ID").Value = ID
        End If
        If Subasta Then
            cm.Parameters.Add("@SUBASTA", SqlDbType.TinyInt)
            cm.Parameters("@SUBASTA").Value = BooleanToSQLBinary(Subasta)
        End If
        If RecibePeticiones Then
            cm.Parameters.Add("@RECPET", SqlDbType.TinyInt)
            cm.Parameters("@RECPET").Value = BooleanToSQLBinary(RecibePeticiones)
        End If

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

#End Region

End Class
