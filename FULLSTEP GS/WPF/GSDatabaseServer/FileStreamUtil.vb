﻿Imports System.Data.SqlClient

Public Class FileStreamUtil

    Private _sSP As String

    Public Sub New(ByVal sSP As String)
        _sSP = sSP
    End Sub

    '<summary>Obtiene un sqlFileStream para guardar el archivo de la regla</summary>
    '<param name="cn">Conexión</param>
    '<returns>Objeto de tipo SqlFileStream para guardar el archivo</returns>    

    Public Function GetSqlFileStreamForWriting(ByRef cn As SqlConnection, ByVal ID As Integer) As SqlTypes.SqlFileStream
        Dim sPath As String
        Dim arTransactionContext() As Byte
        GetPathNameAndTxContext(cn, sPath, arTransactionContext, ID)

        If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
            GetSqlFileStreamForWriting = New SqlTypes.SqlFileStream(sPath, arTransactionContext, System.IO.FileAccess.Write)
        End If
        'Dim handle As SafeFileHandle = OpenSqlFilestream(Path, DESIRED_ACCESS_WRITE, SQL_FILESTREAM_OPEN_NO_FLAGS, txCtx, CType(txCtx, UInt32).Length, 0)
    End Function

    '<summary>Obtiene un sqlFileStream para leer el archivo de la regla</summary>
    '<param name="cn">Conexión</param>
    '<returns>Objeto de tipo SqlFileStream para guardar el archivo</returns>   

    Public Function GetSqlFileStreamForReading(ByVal cn As SqlConnection, ByVal ID As Integer) As SqlTypes.SqlFileStream
        Dim sPath As String
        Dim arTransactionContext() As Byte
        GetPathNameAndTxContext(cn, sPath, arTransactionContext, ID)

        'Create the SqlFileStream            
        If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
            GetSqlFileStreamForReading = New SqlTypes.SqlFileStream(sPath, arTransactionContext, System.IO.FileAccess.Read, System.IO.FileOptions.SequentialScan, 0)
        End If
    End Function

    '<summary>Obtiene un Path y un conexto de transacción para poder guardar un archivo en SQL</summary>
    '<param name="Path">Path</param>
    '<param name="TxContext">Contexto de transacción</param>
    '<param name="cn">Conexión</param>

    Private Sub GetPathNameAndTxContext(ByVal cn As SqlConnection, ByRef Path As String, ByRef TxContext As Byte(), ByVal ID As Integer)
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = _sSP
        With cm.Parameters
            .Add("@ID", SqlDbType.Int)
            cm.Parameters("@ID").Value = ID
        End With
        cm.Connection = cn
        'cm.Transaction = tx

        Dim da As New SqlDataAdapter
        da.SelectCommand = cm
        Dim ds As New System.Data.DataSet
        da.Fill(ds)

        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = ds.Tables(0).Rows(0)

            If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
            If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte()) 'reader.GetSqlBytes(1).Buffer;            
        End If
    End Sub

End Class
