﻿Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary.TiposDeDatos
Module modUtilidades

    '***************** DICCIONARIO DE DATOS ***************
    Function tableName(ByVal TableCode As String) As Integer

        Select Case TableCode

            Case "ACC"
                tableName = 1
            Case "ACC_DEPEN"
                tableName = 2
            Case "ADM"
                tableName = 3
            Case "AR_ITEM"
                tableName = 4
            Case "AR_ITEM_MES"
                tableName = 5
            Case "AR_ITEM_MES_PARCON1"
                tableName = 6
            Case "AR_ITEM_MES_PARCON2"
                tableName = 7
            Case "AR_ITEM_MES_PARCON3"
                tableName = 8
            Case "AR_ITEM_MES_PARCON4"
                tableName = 9
            Case "AR_ITEM_MES_PARPROY1"
                tableName = 10
            Case "AR_ITEM_MES_PARPROY2"
                tableName = 11
            Case "AR_ITEM_MES_PARPROY3"
                tableName = 12
            Case "AR_ITEM_MES_PARPROY4"
                tableName = 13
            Case "AR_ITEM_MES_UON1"
                tableName = 14
            Case "AR_ITEM_MES_UON2"
                tableName = 15
            Case "AR_ITEM_MES_UON3"
                tableName = 16
            Case "AR_ITEM_PROVE"
                tableName = 17
            Case "AR_PROCE"
                tableName = 18
            Case "ART1"
                tableName = 19
            Case "ART1_ADJ"
                tableName = 20
            Case "ART2"
                tableName = 21
            Case "ART2_ADJ"
                tableName = 22
            Case "ART3"
                tableName = 23
            Case "ART3_ADJ"
                tableName = 24
            Case "ART4"
                tableName = 25
            Case "ART4_ADJ"
                tableName = 26
            Case "BLOQUEO_PROCE"
                tableName = 27
            Case "CAL"
                tableName = 28
            Case "COM"
                tableName = 29
            Case "COM_GMN1"
                tableName = 30
            Case "COM_GMN2"
                tableName = 31
            Case "COM_GMN3"
                tableName = 32
            Case "COM_GMN4"
                tableName = 33
            Case "CON"
                tableName = 34
            Case "DEP"
                tableName = 35
            Case "DEST", "UON1_DEST", "UON2_DEST", "UON3_DEST"
                tableName = 36
            Case "DIC"
                tableName = 37
            Case "EQP"
                tableName = 38
            Case "FIJ"
                tableName = 39
            Case "FIRMAS"
                tableName = 40
            Case "GMN1"
                tableName = 41
            Case "GMN2"
                tableName = 42
            Case "GMN3"
                tableName = 43
            Case "GMN4"
                tableName = 44
            Case "IDIOMAS"
                tableName = 45
            Case "ITEM"
                tableName = 46
            Case "ITEM_ADJ"
                tableName = 47
            Case "ITEM_ADJREU"
                tableName = 48
            Case "ITEM_ESP"
                tableName = 49
            Case "ITEM_OBJ"
                tableName = 50
            Case "ITEM_OFE"
                tableName = 51
            Case "ITEM_OFE_WEB"
                tableName = 52
            Case "ITEM_PRES"
                tableName = 53
            Case "ITEM_PRESCON"
                tableName = 54
            Case "ITEM_PRESPROY"
                tableName = 55
            Case "ITEM_UON1"
                tableName = 56
            Case "ITEM_UON2"
                tableName = 57
            Case "ITEM_UON3"
                tableName = 58
            Case "LOG"
                tableName = 59
            Case "MENU"
                tableName = 60
            Case "MON"
                tableName = 61
            Case "OBJSADM"
                tableName = 62
            Case "OFEEST"
                tableName = 63
            Case "PAG"
                tableName = 64
            Case "PAI"
                tableName = 65
            Case "PARGEN_INTERNO", "PARGEN_LIT", "PARGEN_DEF", "PARGEN_GEST", "PARGEN_PORT", "PARGEN_DOT"
                tableName = 66
            Case "PER"
                tableName = 67
            Case "PERF"
                tableName = 68
            Case "PERF_ACC"
                tableName = 69
            Case "PRES_CON1"
                tableName = 70
            Case "PRES_CON2"
                tableName = 71
            Case "PRES_CON3"
                tableName = 72
            Case "PRES_CON4"
                tableName = 73
            Case "PRES_MAT1"
                tableName = 74
            Case "PRES_MAT2"
                tableName = 75
            Case "PRES_MAT3"
                tableName = 76
            Case "PRES_MAT4"
                tableName = 77
            Case "PRES_PROY1"
                tableName = 78
            Case "PRES_PROY2"
                tableName = 79
            Case "PRES_PROY3"
                tableName = 80
            Case "PRES_PROY4"
                tableName = 81
            Case "PROCE"
                tableName = 82
            Case "PROCE_ESP"
                tableName = 83
            Case "PROCE_OFE"
                tableName = 84
            Case "PROCE_OFE_WEB"
                tableName = 85
            Case "PROCE_PER"
                tableName = 86
            Case "PROCE_PROVE"
                tableName = 87
            Case "PROCE_PROVE_PET"
                tableName = 88
            Case "PROCE_PROVE_POT"
                tableName = 89
            Case "PROVE"
                tableName = 90
            Case "PROVE_ART1"
                tableName = 91
            Case "PROVE_ART2"
                tableName = 92
            Case "PROVE_ART3"
                tableName = 93
            Case "PROVE_ART4"
                tableName = 94
            Case "PROVE_EQP"
                tableName = 95
            Case "PROVE_GMN1"
                tableName = 96
            Case "PROVE_GMN2"
                tableName = 97
            Case "PROVE_GMN3"
                tableName = 98
            Case "PROVE_GMN4"
                tableName = 99
            Case "PROVI"
                tableName = 100
            Case "REU"
                tableName = 101
            Case "REU_PROCE"
                tableName = 102
            Case "ROL"
                tableName = 103
            Case "SES"
                tableName = 104
            Case "UNI"
                tableName = 105
            Case "UON0_DEP"
                tableName = 106
            Case "UON1"
                tableName = 107
            Case "UON1_DEP"
                tableName = 108
            Case "UON2"
                tableName = 109
            Case "UON2_DEP"
                tableName = 110
            Case "UON3"
                tableName = 111
            Case "UON3_DEP"
                tableName = 112
            Case "USU"
                tableName = 113
            Case "USU_ACC"
                tableName = 114
            Case "VERSION"
                tableName = 115
            Case "ITEM_PRESCON3"
                tableName = 116
            Case "ITEM_PRESCON4"
                tableName = 117
            Case "LINEAS_RECEP"
                tableName = 118
            Case "PEDIDO_RECEP"
                tableName = 119
            Case "SEGURIDAD"
                tableName = 120
            Case "APROB_LIM"
                tableName = 121
            Case "NOTIF_TIPO1"
                tableName = 122
            Case "NOTIF_TIPO2"
                tableName = 122
            Case "CATN1"
                tableName = 123
            Case "CATN2"
                tableName = 124
            Case "CATN3"
                tableName = 125
            Case "CATN4"
                tableName = 126
            Case "CATN5"
                tableName = 127
            Case "DEF_ATRIB"
                tableName = 128
            Case "PEDIDO"
                tableName = 129
            Case "ORDEN_ENTREGA"
                tableName = 130
            Case "CAT_LIN_MIN"
                tableName = 131
            Case "PROVE_ART_UP"
                tableName = 131
            Case "ART4_ATRIB"
                tableName = 132
            Case "GMN4_ATRIB"
                tableName = 132
            Case "PROVE_ART4_ATRIB"
                tableName = 132
            Case "LINEAS_PEDIDO"
                tableName = 133
            Case "CATALOG_LIN"
                tableName = 134
            Case "PROCE_GRUPO"
                tableName = 135
            Case "PLANTILLA_ATRIB_LISTA"
                tableName = 136
            Case "PLANTILLA_ATRIB_POND"
                tableName = 137
            Case "PLANTILLA_ATRIB"
                tableName = 138
            Case "DEF_ATRIB_LISTA"
                tableName = 139
            Case "DEF_ATRIB_POND"
                tableName = 140
            Case "PLANTILLA"
                tableName = 141
            Case "PLANTILLA_GR"
                tableName = 142
            Case "PLANTILLA_GR_DEF"
                tableName = 143
            Case "PROCE_ATRIB"
                tableName = 144
            Case "DEF_ATRIB"
                tableName = 145
            Case "PROCE_ATRIB_LISTA"
                tableName = 146
            Case "PROCE_ATRIB_POND"
                tableName = 147
            Case "CONTR"
                tableName = 148
            Case "PROCE_CONTR"
                tableName = 149
            Case "SOLICIT"
                tableName = 150
            Case "SOLICIT_ADJUN"
                tableName = 151
            Case "FAVORITOS_LINEAS_PEDIDO", "FAVORITOS_LINEAS_PEDIDO_ADJUN", "FAVORITOS_ORDEN_ENTREGA", "FAVORITOS_ORDEN_ENTREGA_ADJUN"
                tableName = 152
            Case "PRES3_NIV1"
                tableName = 153
            Case "PRES3_NIV2"
                tableName = 154
            Case "PRES3_NIV3"
                tableName = 155
            Case "PRES3_NIV4"
                tableName = 156
            Case "PRES4_NIV1"
                tableName = 157
            Case "PRES4_NIV2"
                tableName = 158
            Case "PRES4_NIV3"
                tableName = 159
            Case "PRES4_NIV4"
                tableName = 160
            Case "FORMULARIO"
                tableName = 161
            Case "FORM_GRUPO"
                tableName = 162
            Case "FORM_CAMPO"
                tableName = 163
            Case "CAMPO_VALOR_LISTA"
                tableName = 164
            Case "CAMPO_ADJUN"
                tableName = 165
            Case "LINEA_DESGLOSE"
                tableName = 166
            Case "LINEA_DESGLOSE_ADJUN"
                tableName = 167
            Case "INSTANCIA"
                tableName = 168
            Case "COPIA_CAMPO"
                tableName = 169
            Case "COPIA_LINEA_DESGLOSE"
                tableName = 170
            Case "TIPO_SOLICITUDES"
                tableName = 171
            Case "CAMPO_PREDEF_VALOR_LISTA"
                tableName = 172
            Case "RECEPTOR"
                tableName = 173
            Case "COPIA_CAMPO_DEF"
                tableName = 177
            Case "PASO_NOTIF"
                tableName = 178
            Case "CAMPOS_PREDEF"
                tableName = 179
            Case "PROCE_UON1"
                tableName = 180
            Case "PROCE_UON2"
                tableName = 181
            Case "PROCE_UON3"
                tableName = 182
            Case "CERTIFICADO"
                tableName = 183
            Case "COPIA_CAMPO_ADJUN"
                tableName = 184
            Case "COPIA_LINEA_DESGLOSE_ADJUN"
                tableName = 185
            Case "COPIA_PASO_NOTIF"
                tableName = 186
            Case "VERSION_INSTANCIA"
                tableName = 187
            Case "INSTANCIA_EST"
                tableName = 188
            Case "SOLICITUD_ADJUN"
                tableName = 189
            Case "SOLICITUD_ESTADOS"
                tableName = 190
            Case "SOLICITUD"
                tableName = 191
            Case "CARPETAS_SOLICIT_N1", "CARPETAS_SOLICIT_N2", "CARPETAS_SOLICIT_N3", "CARPETAS_SOLICIT_N4"
                tableName = 192
            Case "PM_ENLACE_CONDICIONES"
                tableName = 193
            Case "PM_NOTIFICADO_ACCION"
                tableName = 194
            Case "PM_NOTIFICADO_ENLACE"
                tableName = 195
            Case "PM_ROL"
                tableName = 196
            Case "PM_COPIA_ROL", "PM_COPIA_PARTICIPANTES", "PM_COPIA_NOTIFICADO_ACCION", "PM_COPIA_NOTIFICADO_ENLACE"
                tableName = 197
            Case "PM_PARTICIPANTES"
                tableName = 198
            Case "PM_ACCION_CONDICIONES"
                tableName = 199
            Case "PROCE_PRES1"
                tableName = 200
            Case "PROCE_PRES2"
                tableName = 201
            Case "PROCE_PRES3"
                tableName = 202
            Case "PROCE_PRES4"
                tableName = 203
            Case "PRES5_CONTROL"
                tableName = 205

            Case "PRES5_NIV1"
                tableName = 207
            Case "PRES5_NIV2"
                tableName = 208
            Case "PRES5_NIV3"
                tableName = 209
            Case "PRES5_NIV4"
                tableName = 210
            Case "UON4"
                tableName = 211
                'Case "USU_CC_IMPUTACION"
                'Case "USU_CC_CONTROL"
            Case "NOCONFORMIDAD"
                tableName = 206
        End Select


    End Function
    Function FieldName(ByVal FieldCode As String) As Integer

        Select Case FieldCode

            Case "ID"
                FieldName = 1
            Case "COD"
                FieldName = 2
            Case "DEN"
                FieldName = 3
            Case "CAMBIO"
                FieldName = 4
            Case "FECACT"
                FieldName = 5
            Case "MON"
                FieldName = 6
            Case "PAI"
                FieldName = 7
            Case "EQP"
                FieldName = 8
            Case "COM"
                FieldName = 9
            Case "DIR"
                FieldName = 10
            Case "CP"
                FieldName = 11
            Case "POB"
                FieldName = 12
            Case "PROVI"
                FieldName = 13
            Case "OBS"
                FieldName = 14
            Case "PROVE"
                FieldName = 15
            Case "PAG"
                FieldName = 16
            Case "DEST"
                FieldName = 17
            Case "ANYO"
                FieldName = 18
            Case "DESCR"
                FieldName = 19
            Case "DEP"
                FieldName = 20
            Case "ART"
                FieldName = 21
            Case "DEN"
                FieldName = 22
            Case "ESP"
                FieldName = 23
            Case "UNI"
                FieldName = 24
            Case "FECNEC"
                FieldName = 25
            Case "FECINI"
                FieldName = 26
            Case "FECFIN"
                FieldName = 27
            Case "PROVE"
                FieldName = 28
            Case "FECNEC"
                FieldName = 29
            Case "FECAPE"
                FieldName = 30
            Case "FECVALSELPROVE"
                FieldName = 31
            Case "FECVAL"
                FieldName = 32
            Case "FECPRES"
                FieldName = 33
            Case "FECENVPET"
                FieldName = 34
            Case "FECLIMOFE"
                FieldName = 35
            Case "FECPROXREU"
                FieldName = 36
            Case "FECULTREU"
                FieldName = 37
            Case "FECCIERRE"
                FieldName = 38
            Case "FECREAPE"
                FieldName = 39
            Case "EST"
                FieldName = 40
            Case "USU"
                FieldName = 41
            Case "FECINS"
                FieldName = 42
            Case "FECACT"
                FieldName = 43
            Case "PROCE"
                FieldName = 44
            Case "DEST"
                FieldName = 45
            Case "OFE"
                FieldName = 46
            Case "ITEM"
                FieldName = 47
            Case "SOLICITUD"
                FieldName = 48
            Case "FECREU"
                FieldName = 49
            Case "REF"
                FieldName = 50
            Case "HORA"
                FieldName = 51
            Case "COME"
                FieldName = 52
            Case "REUDEC"
                FieldName = 53
            Case "PROCE_PROVE"
                FieldName = 54
            Case "NUMOFE"
                FieldName = 55
            Case "ANYORES"
                FieldName = 56
            Case "NUMPROCE"
                FieldName = 57

            Case "ADMPWD"
                FieldName = 58
            Case "ACC"
                FieldName = 59
            Case "PERF"
                FieldName = 60
            Case "PER"
                FieldName = 61
            Case "TS"
                FieldName = 62
            Case "USU"
                FieldName = 63
            Case "LINEAS_RECEP"
                FieldName = 98
            Case "PEDIDO_RECEP"
                FieldName = 99
            Case "APROB_LIM"
                FieldName = 100
            Case "NOTIF_TIPO1"
                FieldName = 101
            Case "NOTIF_TIPO2"
                FieldName = 101
            Case "CATN1"
                FieldName = 102
            Case "CATN2"
                FieldName = 103
            Case "CATN3"
                FieldName = 104
            Case "CATN4"
                FieldName = 105
            Case "CATN5"
                FieldName = 106
            Case "DEF_ATRIB"
                FieldName = 107
            Case "PEDIDO"
                FieldName = 108
            Case "ORDEN_ENTREGA"
                FieldName = 109
            Case "CAT_LIN_MIN"
                FieldName = 110
            Case "PROVE_ART_UP"
                FieldName = 110
            Case "GMN4_ATRIB"
                FieldName = 111
            Case "ART4_ATRIB"
                FieldName = 111
            Case "PROVE_ART4_ATRIB"
                FieldName = 111
            Case "CATALOG_LIN"
                FieldName = 112
            Case "LINEAS_PEDIDO"
                FieldName = 113
            Case "PREC"
                FieldName = 117
            Case "GRUPO", "PROCE_GRUPO"
                FieldName = 120
            Case "PLANTILLA_ATRIB"
                FieldName = 107
            Case "SOLICIT"
                FieldName = 125
            Case "PROVE_ART4"
                FieldName = 136
            Case "VAR_CAL1"
                FieldName = 171
            Case "VAR_CAL2"
                FieldName = 172
            Case "VAR_CAL3"
                FieldName = 173
            Case "VAR_CAL0"
                FieldName = 174
            Case "VAR_CAL4"
                FieldName = 175
            Case "VAR_CAL5"
                FieldName = 176
            Case "VIA_PAG"
                FieldName = 177
            Case Else
                FieldName = 64
        End Select

    End Function
    'Public Function TratarErrorAdo(ByVal adoerrors As ADODB.Errors) As TipoErrorSummit
    '
    'Dim Error As TipoErrorSummit
    '
    '''' Rutina general de respuesta a errores.
    '
    'Dim ID As Long
    'Dim Den As String
    'Dim ItemError1 As String, ItemError2 As String
    'Dim pos1 As Integer, pos2 As Integer
    '
    'If adoerrors.Count <> 0 Then
    '    ID = adoerrors(0).NativeError
    '    Den = adoerrors(0).Description
    '    'ID = adoerrors(adoerrors.Count - 1).Number
    '    'Den = adoerrors(adoerrors.Count - 1).Description
    'End If
    '
    'Select Case ID
    '
    'Case 0
    '
    '    Error.Number = TESInfModificada
    '    Error.Arg1 = ""
    '    Error.Arg2 = ""
    '
    'Case 515
    '
    '    Beep
    '    pos1 = InStr(1, Den, "'")
    '    pos2 = InStr(pos1 + 1, Den, "'")
    '    ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
    '    Error.Number = TESFaltanDatos
    '    Error.Arg1 = FieldName(ItemError1)
    '
    'Case 547
    '
    '        pos1 = InStr(1, Den, "table") + 6
    '        pos2 = InStr(pos1 + 1, Den, "'")
    '        ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
    '        pos1 = InStr(1, Den, "column") + 7
    '        pos2 = InStr(pos1 + 1, Den, "'")
    '        ItemError2 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
    '
    '
    '        If Mid(Den, 55, 3) = "INS" Or Mid(Den, 55, 3) = "UPD" Then
    '            Error.Number = TESDatoEliminado
    '            Error.Arg1 = FieldName(ItemError1)
    '            'Error.Arg2 = "Eliminado"
    '
    '        Else
    '            Error.Number = TESImposibleEliminar
    '            Error.Arg1 = TableName(ItemError1)
    '            Error.Arg2 = "DatRel"
    '        End If
    '
    '
    'Case 2627
    '
    '    If InStr(1, Den, "PRIMARY") <> 0 Then
    '
    '        Error.Number = TESDatoDuplicado
    '        Error.Arg1 = 2 '' 2="Codigo"
    '
    '        If InStr(1, Den, "PK_USU_COM") Then
    '            Error.Number = TESDatoDuplicado
    '            Error.Arg1 = 9 ''9="Comprador"
    '        End If
    '
    '
    '    Else
    '
    '        pos1 = InStr(1, Den, "UC_") + 2
    '        pos2 = InStr(pos1 + 1, Den, "'")
    '        ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
    '        Error.Number = TESDatoDuplicado
    '        Error.Arg1 = FieldName(ItemError1)
    '
    '    End If
    '
    'Case 50000
    '
    '    If Mid(Den, 1, 5) = "37000" Then
    '
    '            If InStr(1, Den, "Data in table ITEM has been deleted by other user.") <> 0 Then
    '                Error.Number = TESDatoEliminado
    '                Error.Arg1 = 47 ''47=Item
    '            Else
    '                If InStr(1, Den, "Sum of distribution is greater than Cant.") <> 0 Then
    '                    Error.Number = TESCantDistMayorQueItem
    '                Else
    '                    If InStr(1, Den, "Distribution is greater than 100.") <> 0 Then
    '                        Error.Number = TESCantDistMayorQueItem
    '                    Else
    '                        If InStr(1, Den, "The offer has been procesed") <> 0 Then
    '                            Error.Number = TESOfertaAdjudicada
    '                        Else
    '                            If InStr(1, Den, "Incorrect password") <> 0 Then
    '                                Error.Number = TESCambioContrasenya
    '                            End If
    '                        End If
    '                    End If
    '                End If
    '            End If
    '    Else
    '
    '        Error.Number = TESImposibleEliminar
    '        Error.Arg1 = TableName(Mid(Den, 55, Len(Den) - 55 + 1))
    '    End If
    '
    'Case Else
    '
    '    Error.Number = TESOtroerror
    '    Error.Arg1 = ID
    '    Error.Arg2 = Den
    '
    'End Select
    '
    '
    'TratarErrorAdo = Error
    '
    'End Function

    Public Function TratarError(ByVal adoerrors As SqlException) As GSServerModel.GSException


        Dim eError As New GSServerModel.GSException(False, "", "", True)
        eError.Number = ErroresGS.TESnoerror
        eError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        ''' Rutina general de respuesta a errores.

        Dim ID As Long
        Dim Den As String
        Dim ItemError1 As String, ItemError2 As String
        Dim pos1 As Integer, pos2 As Integer

        If adoerrors.Errors.Count <> 0 Then
            ID = adoerrors.Errors(0).Number
            'Forzamos a un error desconocido para los casos
            'que devuelven denominaciones de error vacías
            If adoerrors.Errors(0).Message = "" Then
                ID = 8
            End If
            Den = adoerrors.Errors(0).Message
        End If

        If Den = "Invalid date format" Then
            eError.Number = ErroresGS.TESDatoNoValido
            eError.ErrorId = 96
            eError.ErrorStr = ""

        End If

        Select Case ID

            Case 0

                eError.Number = ErroresGS.TESInfModificada
                eError.ErrorId = 0
                eError.ErrorStr = ""

            Case 7

            Case 242 'The conversion of a data type to a datetime data type resulted in an out of range datetime
                eError.Number = ErroresGS.TESDatoNoValido
                eError.ErrorId = 96
                eError.ErrorStr = ""


            Case 515

                Beep()
                pos1 = InStr(1, Den, "'")
                pos2 = InStr(pos1 + 1, Den, "'")
                ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
                eError.Number = ErroresGS.TESFaltanDatos
                eError.ErrorId = FieldName(ItemError1)


            Case 547
                Dim bConDBO As Boolean
                bConDBO = True
                pos1 = InStr(1, Den, "table " & """" & "dbo.") + 6
                If pos1 = 6 Then
                    bConDBO = False
                    pos1 = InStr(1, Den, "table") + 6
                End If

                pos2 = InStr(pos1 + 1, Den, "'")

                If pos2 = 0 Or bConDBO Then
                    pos1 = pos1 + 4
                    pos2 = InStr(pos1 + 1, Den, """")
                End If

                ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
                pos1 = InStr(1, Den, "column") + 7
                pos2 = InStr(pos1 + 1, Den, "'")
                If pos2 = 0 Then
                    pos1 = pos1 + 4
                    pos2 = InStr(pos1 + 1, Den, """")
                End If
                ItemError2 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)


                If Mid(Den, 55, 3) = "INS" Or Mid(Den, 55, 3) = "UPD" Or Left(Den, 3) = "INS" Or Left(Den, 3) = "UPD" Then
                    eError.Number = ErroresGS.TESDatoEliminado
                    eError.ErrorId = FieldName(ItemError1)

                Else
                    eError.Number = ErroresGS.TESImposibleEliminar
                    If ItemError2 = "RECEPTOR" And ItemError1 = "FAVORITOS_ORDEN_ENTREGA" Then
                        eError.ErrorId = tableName(ItemError2)
                        eError.ErrorStr = "DatDel"
                    Else
                        If ItemError2 = "PROVE" And Left(ItemError1, 12) = "CONF_VISTAS_" Then
                            eError.ErrorId = tableName("PROCE_OFE")
                            eError.ErrorStr = "DatDel"

                        Else
                            eError.ErrorId = tableName(ItemError1)
                            eError.ErrorStr = "DatDel"

                        End If
                    End If
                End If


            Case 2627

                If InStr(1, Den, "PRIMARY") <> 0 Then

                    eError.Number = ErroresGS.TESDatoDuplicado
                    eError.ErrorId = 2

                ElseIf InStr(1, Den, "Violation") <> 0 Then 'RAISERROR
                    eError.Number = ErroresGS.TESDatoDuplicado
                    eError.ErrorId = 2
                Else

                    pos1 = InStr(1, Den, "UC_") + 2
                    pos2 = InStr(pos1 + 1, Den, "'")
                    ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
                    eError.Number = ErroresGS.TESDatoDuplicado
                    eError.ErrorId = FieldName(ItemError1)

                End If

            Case 16934
                eError.Number = ErroresGS.TESInfActualModificada
                eError.ErrorId = 0
                eError.ErrorStr = ""

            Case 50000

                If Mid(Den, 1, 5) = "37000" Then

                    If InStr(1, Den, "Sum of distribution is greater than Cant.") <> 0 Then
                        eError.Number = ErroresGS.TESCantDistMayorQueItem
                    Else
                        If InStr(1, Den, "Distribution is greater than 100.") <> 0 Then
                            eError.Number = ErroresGS.TESCantDistMayorQueItem
                        Else
                            If InStr(1, Den, "The offer has been procesed") <> 0 Then
                                eError.Number = ErroresGS.TESOfertaAdjudicada
                            Else
                                If InStr(1, Den, "Incorrect password") <> 0 Then
                                    eError.Number = ErroresGS.TESCambioContrasenya
                                End If
                            End If
                        End If
                    End If
                Else
                    If InStr(1, Den, "Incorrect password") <> 0 Then
                        eError.Number = ErroresGS.TESCambioContrasenya
                    Else
                        If InStr(1, Den, "Atributo existente") <> 0 Then
                            'Código duplicado en PROCE_ATRIB o PLANTILLA_ATRIB
                            eError.Number = ErroresGS.TESDatoDuplicado
                            eError.ErrorId = 2

                        Else
                            eError.Number = ErroresGS.TESImposibleEliminar

                            pos1 = 55
                            If InStr(1, Den, "dbo.") > 0 Then
                                pos1 = pos1 + 4
                            End If

                            If Len(Den) > 55 Then 'Revisar las den
                                eError.ErrorId = tableName(Mid(Den, pos1, Len(Den) - pos1 + 1))
                            End If
                        End If
                    End If

                End If

            Case 7133
                eError.Number = ErroresGS.TESAdjuntoEliminadoPortal
                eError.ErrorId = ID
                eError.ErrorStr = Den
            Case Else

                eError.Number = ErroresGS.TESOtroerror
                eError.ErrorId = ID
                eError.ErrorStr = Den

        End Select


        TratarError = eError

    End Function



End Module
