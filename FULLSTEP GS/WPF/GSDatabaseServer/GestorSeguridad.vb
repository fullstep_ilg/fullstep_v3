﻿Imports Fullstep.FSNLibrary
Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel

<Serializable()> _
Public Class GestorSeguridad
    Inherits DBServerBase

    Public Sub RegistrarAccion(ByVal sUsu As String, ByVal iAcc As Integer, ByVal sDat As String)
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_REGISTRAR_ACCION"
        With cm.Parameters
            .Add("@USU", SqlDbType.VarChar, 50)
            cm.Parameters("@USU").Value = sUsu
            .Add("@ACC", SqlDbType.Int)
            cm.Parameters("@ACC").Value = iAcc
            .Add("@DAT", SqlDbType.VarChar, 512)
            cm.Parameters("@DAT").Value = sDat
            .Add("@FEC", SqlDbType.DateTime)
            cm.Parameters("@FEC").Value = Date.Now
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            EjecutarComando(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Sub

End Class
