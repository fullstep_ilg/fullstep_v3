﻿Imports Fullstep.FSNLibrary
Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel

<Serializable()> _
Public Class Procesos
    Inherits DBServerBase

#Region " Proceso "

    '<summary>Devuelve los datos de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos del proceso</returns>    

    Public Function DevolverProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_PROCESO"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@COD", SqlDbType.Int)
            cm.Parameters("@COD").Value = Cod
            .Add("@GMN1", SqlDbType.NVarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1            
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Devuelve los datos de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos del proceso</returns>    

    Public Function DevolverCarpetaPlantillaProce(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As String
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_EMPDISTPROCE"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Cod
            .Add("@GMN1", SqlDbType.NVarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Dim ds As DataSet = EjecutarComandoLectura(cm, cn)
            Return ds.Tables(0).Rows(0)("CARPETA_PLANTILLAS").ToString
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Devuelve los datos de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos del proceso</returns>    

    Public Function SacarRemitenteEmpresa(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As String
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_REMITENTE_EMPRESA"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Cod
            .Add("@GMN1", SqlDbType.NVarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Dim ds As DataSet = EjecutarComandoLectura(cm, cn)
            Return ds.Tables(0).Rows(0)("REMITENTE").ToString
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function
    '<summary>Actualiza los datos de un proceso</summary>
    '<param name="oProceso">Datos de Proceso</param>
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns> 

    Public Function ActualizarProceso(ByVal oProceso As GSServerModel.Proceso) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Dim oActCom As SqlCommand = ComandoActualizarProceso(oProceso)
            EjecutarComandoActualizacion(oActCom, cn)            

            cn.Close()
        Catch ex As SqlException
            oError = TratarError(ex)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Actualiza los datos de un proceso</summary>
    '<param name="oProceso">Datos de Proceso</param>
    '<param name="cn">Conexión</param>
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns> 

    Private Sub ActualizarProceso(ByVal oProceso As GSServerModel.Proceso, ByVal cn As SqlConnection)
        Dim oActCom As SqlCommand = ComandoActualizarProceso(oProceso)
        EjecutarComandoActualizacion(oActCom, cn)
    End Sub

    '<summary>Crea un comando SQL para actualizar el proceso</summary>
    '<param name="oProceso">Datos de Proceso</param>    
    '<returns>Objeto de tipo SqlCommand</returns>    

    Private Function ComandoActualizarProceso(ByVal oProceso As GSServerModel.Proceso) As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_ACTUALIZAR_PROCESO"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.SmallInt)
            .Add("@COD", SqlDbType.Int)
            .Add("@GMN1", SqlDbType.VarChar, 50)
            '.Add("@DEN", SqlDbType.VarChar, 100)            
            '.Add("@ADJDIR", SqlDbType.TinyInt)
            '.Add("@PRES", SqlDbType.Float)
            '.Add("@FECNEC", SqlDbType.DateTime)
            '.Add("@FECAPE", SqlDbType.DateTime)
            '.Add("@FECVALSELPROVE", SqlDbType.DateTime)
            '.Add("@FECVAL", SqlDbType.DateTime)
            '.Add("@FECPRES", SqlDbType.DateTime)
            '.Add("@FECENVPET", SqlDbType.DateTime)
            .Add("@FECLIMOFE", SqlDbType.DateTime)
            '.Add("@FECPROXREU", SqlDbType.DateTime)
            '.Add("@FECULTREU", SqlDbType.DateTime)
            '.Add("@FECCIERRE", SqlDbType.DateTime)
            '.Add("@FECREAPE", SqlDbType.DateTime)
            '.Add("@REUDEC", SqlDbType.TinyInt)
            '.Add("@USUVAL", SqlDbType.VarChar, 50)
            '.Add("@USUVALSELPROVE", SqlDbType.VarChar, 50)
            '.Add("@USUVALCIERRE", SqlDbType.VarChar, 50)
            '.Add("@MON", SqlDbType.NVarChar, 50)
            '.Add("@CAMBIO", SqlDbType.Float)
            '.Add("@ESP", SqlDbType.VarChar, 800)
            '.Add("@EST", SqlDbType.TinyInt)
            '.Add("@ESTULT", SqlDbType.TinyInt)
            '.Add("@EQP", SqlDbType.VarChar, 50)
            '.Add("@COM", SqlDbType.VarChar, 50)
            '.Add("@SOLICITUD", SqlDbType.VarChar, 20)
            '.Add("@FECNECMANUAL", SqlDbType.TinyInt)
            '.Add("@PED_APROV", SqlDbType.TinyInt)
            '.Add("@DEST", SqlDbType.VarChar, 50)
            '.Add("@PAG", SqlDbType.NVarChar, 50)
            '.Add("@FECINI", SqlDbType.DateTime)
            '.Add("@FECFIN", SqlDbType.DateTime)
            '.Add("@PROVEACT", SqlDbType.VarChar, 50)
            '.Add("@USUAPER", SqlDbType.VarChar, 50)
            .Add("@FECINISUB", SqlDbType.DateTime)
            '.Add("@SOLICIT", SqlDbType.Int)
            '.Add("@CALCPEND", SqlDbType.TinyInt)
            '.Add("@CALCFEC", SqlDbType.DateTime)
            '.Add("@ADJUDICADO", SqlDbType.Float)
            '.Add("@PLANTILLA", SqlDbType.Int)
            '.Add("@PLANTILLA_VISTAS", SqlDbType.TinyInt)
            '.Add("@ADJ_ERP", SqlDbType.TinyInt)
            '.Add("@COMENT_ADJ", SqlDbType.VarChar, 4000, "COMENT_ADJ")
            '.Add("@P", SqlDbType.Int)
            '.Add("@W", SqlDbType.Int)
            '.Add("@O", SqlDbType.Int)
            '.Add("@PRESTOTAL", SqlDbType.Float)
            '.Add("@CONSUMIDO", SqlDbType.Float)
            '.Add("@DESDE_SOLICITUD", SqlDbType.TinyInt)
            '.Add("@PER_APER", SqlDbType.VarChar, 50)
            '.Add("@UON1_APER", SqlDbType.VarChar, 50)
            '.Add("@UON2_APER", SqlDbType.VarChar, 50)
            '.Add("@UON3_APER", SqlDbType.VarChar, 50)
            '.Add("@DEP_APER", SqlDbType.VarChar, 50)
            '.Add("@PUBMATPROVE", SqlDbType.TinyInt)
            '.Add("@GSTOERP", SqlDbType.VarChar, 50)
            '.Add("@FECADJ", SqlDbType.DateTime)
        End With

        cm.Parameters("@ANYO").Value = oProceso.Anyo
        cm.Parameters("@COD").Value = oProceso.Codigo
        cm.Parameters("@GMN1").Value = oProceso.GMN1
        'cm.Parameters("@DEN").Value = oProceso.Denominacion
        'cm.Parameters("@ADJDIR").Value = oProceso.AdjDir
        'cm.Parameters("@PRES").Value = DoubleToSQLDoubleDBNull(oProceso.Presupuesto)           
        'cm.Parameters("@FECNEC").Value = DateToSQLDateDBNull(oProceso.FechaNecesidad)
        'cm.Parameters("@FECAPE").Value = oProceso.FechaApe
        'cm.Parameters("@FECVALSELPROVE").Value = DateToSQLDateDBNull(oProceso.FechaValSelProve)
        'cm.Parameters("@FECVAL").Value = DateToSQLDateDBNull(oProceso.FechaValidacion)
        'cm.Parameters("@FECPRES").Value = DateToSQLDateDBNull(oProceso.FechaPresentacion)
        'cm.Parameters("@FECENVPET").Value = DateToSQLDateDBNull(oProceso.FechaEnvPet)        
        cm.Parameters("@FECLIMOFE").Value = DateToSQLDateDBNull(oProceso.FechaLimiteOfertas)        
        'cm.Parameters("@FECPROXREU").Value = DateToSQLDateDBNull(oProceso.FechaProximaReunion)
        'cm.Parameters("@FECULTREU").Value = DateToSQLDateDBNull(oProceso.FechaUltimaReunion)
        'cm.Parameters("@FECCIERRE").Value = DateToSQLDateDBNull(oProceso.FechaCierre)
        'cm.Parameters("@FECREAPE").Value = DateToSQLDateDBNull(oProceso.FechaReapertura)
        'cm.Parameters("@REUDEC").Value = oProceso.Reudec
        'cm.Parameters("@USUVAL").Value = strToDBNull(oProceso.UsuVal)
        'cm.Parameters("@USUVALSELPROVE").Value = strToDBNull(oProceso.UsuValSelProve)
        'cm.Parameters("@USUVALCIERRE").Value = strToDBNull(oProceso.UsuValCierre)
        'cm.Parameters("@MON").Value = oProceso.Moneda
        'cm.Parameters("@CAMBIO").Value = oProceso.Cambio
        'cm.Parameters("@ESP").Value = strToDBNull(oProceso.Esp)
        'cm.Parameters("@EST").Value = oProceso.Estado
        'cm.Parameters("@ESTULT").Value = IntToSQLIntegerDBNull(oProceso.EstUlt)
        'cm.Parameters("@EQP").Value = strToDBNull(oProceso.Equipo)
        'cm.Parameters("@COM").Value = strToDBNull(oProceso.Comprador)
        'cm.Parameters("@SOLICITUD").Value = strToDBNull(oProceso.Solicitud)
        'cm.Parameters("@FECNECMANUAL").Value = BooleanToSQLBinaryDBNull(oProceso.FechaNecManual)
        'cm.Parameters("@PED_APROV").Value = oProceso.PedAprov
        'cm.Parameters("@DEST").Value = strToDBNull(oProceso.Dest)
        'cm.Parameters("@PAG").Value = strToDBNull(oProceso.Pag)
        'cm.Parameters("@FECINI").Value = DateToSQLDateDBNull(oProceso.FechaIni)
        'cm.Parameters("@FECFIN").Value = DateToSQLDateDBNull(oProceso.FechaFin)
        'cm.Parameters("@PROVEACT").Value = strToDBNull(oProceso.ProveAct)
        'cm.Parameters("@USUAPER").Value = oProceso.UsuarioApertura        
        cm.Parameters("@FECINISUB").Value = DateToSQLDateDBNull(oProceso.FechaInicioSubasta)        
        'cm.Parameters("@SOLICIT").Value = IntToSQLIntegerDBNull(oProceso.Solicit)
        'cm.Parameters("@CALCPEND").Value = oProceso.CalcPend
        'cm.Parameters("@CALCFEC").Value = DateToSQLDateDBNull(oProceso.FechaCalc)
        'cm.Parameters("@ADJUDICADO").Value = DoubleToSQLDoubleDBNull(oProceso.Adjudicado)
        'cm.Parameters("@PLANTILLA").Value = IntToSQLIntegerDBNull(oProceso.Plantilla)
        'cm.Parameters("@PLANTILLA_VISTAS").Value = oProceso.PlantillaVistas
        'cm.Parameters("@ADJ_ERP").Value = oProceso.AdjERP
        'cm.Parameters("@COMENT_ADJ").Value = strToDBNull(oProceso.ComentAdj)
        'cm.Parameters("@P").Value = IntToSQLIntegerDBNull(oProceso.P)
        'cm.Parameters("@W").Value = IntToSQLIntegerDBNull(oProceso.W)
        'cm.Parameters("@O").Value = IntToSQLIntegerDBNull(oProceso.O)
        'cm.Parameters("@PRESTOTAL").Value = DoubleToSQLDoubleDBNull(oProceso.PresTotal)
        'cm.Parameters("@CONSUMIDO").Value = DoubleToSQLDoubleDBNull(oProceso.Consumido)
        'cm.Parameters("@DESDE_SOLICITUD").Value = oProceso.DesdeSolicitud
        'cm.Parameters("@PER_APER").Value = strToDBNull(oProceso.PersonaApertura)
        'cm.Parameters("@UON1_APER").Value = strToDBNull(oProceso.UON1Apertura)
        'cm.Parameters("@UON2_APER").Value = strToDBNull(oProceso.UON2Apertura)
        'cm.Parameters("@UON3_APER").Value = strToDBNull(oProceso.UON3Apertura)
        'cm.Parameters("@DEP_APER").Value = strToDBNull(oProceso.DepApertura)
        'cm.Parameters("@PUBMATPROVE").Value = oProceso.PubMatProve
        'cm.Parameters("@GSTOERP").Value = strToDBNull(oProceso.GstoERP)
        'cm.Parameters("@FECADJ").Value = DateToSQLDateDBNull(oProceso.FechaAdj)

        Return cm
    End Function

    '<summary>Devuelve las asignaciones de un proceso</summary>
    '<param name="CriterioOrdenacion">Datos de Proceso</param>
    '<param name="CodEqp">Conexión con la que se lleva a cabo la actualización</param>
    '<param name="CodComp">Conexión con la que se lleva a cabo la actualización</param>    
    '<param name="CodProve">Conexión con la que se lleva a cabo la actualización</param>        
    '<returns>Dataset con las asignaciones</returns>    

    Public Function DevolverAsignaciones(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, _
                                         Optional ByVal CriterioOrdenacion As Fullstep.FSNLibrary.TiposDeDatos.TipoOrdenacionAsignaciones = -1, Optional ByVal CodEqp As String = Nothing, Optional ByVal CodComp As String = Nothing, Optional ByVal CodProve As String = Nothing) As DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_ASIGNACIONES_PROCESO"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Proce
            .Add("@GMN1", SqlDbType.NVarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
            If Not CodProve Is Nothing Then
                .Add("@PROVE", SqlDbType.VarChar, 50)
                cm.Parameters("@PROVE").Value = Anyo
            End If
            If Not CodComp Is Nothing Then
                .Add("@COm", SqlDbType.VarChar, 50)
                cm.Parameters("@COm").Value = Anyo
            End If
            If Not CodEqp Is Nothing Then
                .Add("@EQP", SqlDbType.VarChar, 50)
                cm.Parameters("@EQP").Value = Anyo
            End If            
            .Add("@ORD", SqlDbType.Int)
            cm.Parameters("@ORD").Value = Anyo           
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Devuelve los datos de peticiones de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos de peticiones del proceso</returns>   

    Public Function DevolverProcesoPeticiones(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_PETICIONES_PROCESO"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Cod
            .Add("@GMN1", SqlDbType.VarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Inserta peticiones de un proceso</summary>
    '<param name="dsPeticiones">Datos de Peticiones</param>    
    '<returns>Objeto GsException con el error si lo hubiese</returns>    

    Public Function InsertarProcesoPeticiones(ByVal CIA As Integer, ByVal dsPeticiones As DataSet, ByVal TipoInstalacionWeb As TipoInstWeb) As GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Dim oInsCom As SqlCommand = ComandoInsertarPeticion(CIA, TipoInstalacionWeb)
            EjecutarActualizacion(dsPeticiones, Nothing, oInsCom, Nothing, cn)

            cn.Close()
        Catch ex As SqlException
            oError = TratarError(ex)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Genera un comando de inserción de peticiones</summary>
    '<param name="dsPeticiones">Datos de Peticiones</param>
    '<param name="oCon">Conexión con la que se lleva a cabo la actualización</param>
    '<returns>Objeto SqlCommand</returns> 

    Private Function ComandoInsertarPeticion(ByVal CIA As Integer, ByVal TipoInstalacionWeb As TipoInstWeb) As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_INSERTAR_PROCESO_PETICION"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.SmallInt, 0, "ANYO")
            .Add("@PROCE", SqlDbType.Int, 0, "PROCE")
            .Add("@GMN1", SqlDbType.VarChar, 50, "GMN1")
            .Add("@PROVE", SqlDbType.VarChar, 50, "PROVE")
            .Add("@FECHA", SqlDbType.DateTime, 0, "FECHA")
            .Add("@WEB", SqlDbType.TinyInt, 0, "WEB")
            .Add("@EMAIL", SqlDbType.TinyInt, 0, "EMAIL")
            .Add("@IMP", SqlDbType.TinyInt, 0, "IMP")
            .Add("@APECON", SqlDbType.VarChar, 100, "APECON")
            .Add("@NOMCON", SqlDbType.VarChar, 20, "NOMCON")
            .Add("@TIPOPET", SqlDbType.TinyInt, 0, "TIPOPET")
            .Add("@CODPROVEPORTAL", SqlDbType.VarChar, 50, "CODIGOPORTAL")
            .Add("@CIA", SqlDbType.Int, 0)
            .Add("@TIPOINSTALACIONWEB", SqlDbType.Int, 0)
        End With
        cm.Parameters("@CIA").Value = CIA
        cm.Parameters("@TIPOINSTALACIONWEB").Value = TipoInstalacionWeb

        Return cm
    End Function

    '<summary>Devuelve los datos de un comprador</summary>
    '<param name="Equipo">Equipo</param>
    '<param name="Codigo">Codigo</param>
    '<returns>Objeto Dataset con los datos del comprador</returns> 

    Public Function DevolverComprador(ByVal Equipo As String, ByVal Codigo As String) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_COMPRADOR"
        With cm.Parameters
            .Add("@EQP", SqlDbType.VarChar, 50)
            cm.Parameters("@EQP").Value = Equipo
            .Add("@COD", SqlDbType.VarChar, 50)
            cm.Parameters("@COD").Value = Codigo
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Devuelve los proveedores del proceso a publicar</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="Equipo">Equipo</param>
    '<param name="Codigo">Código</param>    
    '<returns>Objeto Dataset con los datos de los proveedores</returns>    

    Public Function DevolverProveedoresAPublicar(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal Equipo As String = Nothing, Optional ByVal Com As String = Nothing) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "SP_DEVOLVER_PROVEEDORES_A_PUBLICAR"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Cod
            .Add("@GMN1", SqlDbType.VarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
            .Add("@EQP", SqlDbType.VarChar, 50)
            If Not Equipo Is Nothing Then
                cm.Parameters("@EQP").Value = Equipo
            Else
                cm.Parameters("@EQP").Value = DBNull.Value
            End If
            .Add("@COM", SqlDbType.VarChar, 50)
            If Not Com Is Nothing Then
                cm.Parameters("@COM").Value = Com
            Else
                cm.Parameters("@COM").Value = DBNull.Value
            End If
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Activa la publicación de provedores</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="oProves">Proveedores</param>
    '<param name="InsWeb">Tipo instalación web</param>    
    '<returns>Objeto GSException con el error si se ha producido</returns>   

    Public Function ActivarPublicacionProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oProves As GSServerModel.ProcesoProveedores, ByVal InsWeb As TiposDeDatos.TipoInstWeb, _
                                                  ByVal sFSPSRV As String, ByVal sFSPBD As String, ByVal iFSPCIA As Integer) As GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Dim oASR As New ArchivoSubastaReglas
        Try
            Dim sConnString As String = mDBConnection
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            For Each oProve As GSServerModel.ProcesoProveedor In oProves
                If InsWeb = TiposDeDatos.TipoInstWeb.ConPortal Then
                    'Actualizamos la publicación.

                    Dim oDBProce As New GSDatabaseServer.Procesos
                    Dim dsProve As DataSet = oDBProce.DevolverProveedores(Anyo, Cod, GMN1, , oProve.Proveedor)
                    dsProve.Tables(0).Rows(0)("PUB") = 1
                    dsProve.Tables(0).Rows(0)("FECPUB") = Date.Today
                    oDBProce.ActualizarProcesoProveedores(dsProve)

                    Dim oDatos As DatosProcesosPublicados = DevolverProcesosPublicados(oProve.Proveedor)

                    oError = ActualizarProcesoPublicacion(oProve, oDatos, sFSPSRV, sFSPBD, iFSPCIA)
                Else
                    Dim oDBProce As New GSDatabaseServer.Procesos
                    Dim dsProve As DataSet = oDBProce.DevolverProveedores(Anyo, Cod, GMN1, , oProve.Proveedor)
                    dsProve.Tables(0).Rows(0)("PUB") = 1
                    dsProve.Tables(0).Rows(0)("FECPUB") = Date.Today
                    oDBProce.ActualizarProcesoProveedores(dsProve)
                End If
            Next

            Me.CommitTransaction(cn)
            cn.Close()

        Catch ex As SqlException
            oError = TratarError(ex)
            If Not cn Is Nothing Then Me.RollbackTransaction(cn)
        Finally
            If Not cn Is Nothing Then cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Desactiva la publicación de provedores</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="oProves">Proveedores</param>
    '<param name="InsWeb">Tipo instalación web</param>    
    '<returns>Objeto GSException con el error si se ha producido</returns>   

    Public Function DesactivarPublicacionProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oProves As GSServerModel.ProcesoProveedores, ByVal InsWeb As TiposDeDatos.TipoInstWeb, _
                                                     ByVal sFSPSRV As String, ByVal sFSPBD As String, ByVal iFSPCIA As Integer) As GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Dim oASR As New ArchivoSubastaReglas
        Try
            Dim sConnString As String = mDBConnection
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            For Each oProve As GSServerModel.ProcesoProveedor In oProves
                If InsWeb = TiposDeDatos.TipoInstWeb.ConPortal Then                    
                    Dim oDatos As DatosProcesosPublicados = DevolverProcesosPublicados(oProve.Proveedor)

                    oError = ActualizarProcesoPublicacion(oProve, oDatos, sFSPSRV, sFSPBD, iFSPCIA)
                End If

                'Actualizamos la publicación.
                Dim oDBProce As New GSDatabaseServer.Procesos
                Dim dsProve As DataSet = oDBProce.DevolverProveedores(Anyo, Cod, GMN1, , oProve.Proveedor)
                dsProve.Tables(0).Rows(0)("PUB") = 0
                dsProve.Tables(0).Rows(0)("FECPUB") = System.DBNull.Value
                oDBProce.ActualizarProcesoProveedores(dsProve)
            Next

            Me.CommitTransaction(cn)
            cn.Close()

        Catch ex As SqlException
            oError = TratarError(ex)
            If Not cn Is Nothing Then Me.RollbackTransaction(cn)
        Finally
            If Not cn Is Nothing Then cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Devuelve datos de procesos publicados para un proveedor</summary>
    '<param name="Prove">Código del proveedor</param>    
    '<returns>Objeto DatosProcesosPublicados con los datos de los procesos publicados</returns>   

    Public Function DevolverProcesosPublicados(ByVal Prove As String) As DatosProcesosPublicados
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandTimeout = 1800
        cm.CommandText = "NUM_PROCE_PUB"
        With cm.Parameters
            .Add("@PROVE", SqlDbType.VarChar, 50)
            cm.Parameters("@PROVE").Value = Prove
            .Add("@NUM", SqlDbType.Int).Direction = ParameterDirection.Output
            .Add("@NUMNUE", SqlDbType.Int).Direction = ParameterDirection.Output
            .Add("@NUMAREV", SqlDbType.Int).Direction = ParameterDirection.Output
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            EjecutarComando(cm, cn)

            Dim oDatos As New DatosProcesosPublicados
            oDatos.ProcesosPublicados = cm.Parameters("@NUM").Value
            oDatos.ProcesosPublicadosSinOfertas = cm.Parameters("@NUMNUE").Value
            oDatos.ProcesosPublicadosARevisar = cm.Parameters("@NUMAREV").Value

            Return oDatos

            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Devuelve datos de procesos publicados para un proveedor</summary>
    '<param name="Prove">Código del proveedor</param>    
    '<returns>Objeto DatosProcesosPublicados con los datos de los procesos publicados</returns>   

    Public Function ActualizarProcesoPublicacion(ByVal oProve As GSServerModel.ProcesoProveedor, ByVal oDatos As DatosProcesosPublicados, ByVal sFSPSRV As String, ByVal sFSPBD As String, _
                                                 ByVal iFSPCIA As Integer) As GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandTimeout = 1800
        cm.CommandText = sFSPSRV & "." & sFSPBD & ".dbo." & "SP_ACT_PROCE_PUB"
        With cm.Parameters
            .Add("@CIA_COMP", SqlDbType.SmallInt)
            cm.Parameters("@CIA_COMP").Value = iFSPCIA
            .Add("@CIA_PROVE", SqlDbType.VarChar, 50)
            cm.Parameters("@CIA_PROVE").Value = oProve.CodigoPortal
            .Add("@NUM_PUB", SqlDbType.Int)
            cm.Parameters("@NUM_PUB").Value = oDatos.ProcesosPublicados
            .Add("@NUM_PUB_SINOFE", SqlDbType.Int)
            cm.Parameters("@NUM_PUB_SINOFE").Value = oDatos.ProcesosPublicadosSinOfertas
            .Add("@NUM_PUB_AREV", SqlDbType.Int)
            cm.Parameters("@NUM_PUB_AREV").Value = oDatos.ProcesosPublicadosARevisar
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            EjecutarComando(cm, cn)
            cn.Close()
        Catch ex As SqlException
            oError = TratarError(ex)
            If Not cn Is Nothing Then Me.RollbackTransaction(cn)
        Finally
            If Not cn Is Nothing Then cn.Dispose()
        End Try

        Return oError
    End Function

#End Region

#Region " Proceso_Def "

    '<summary>Devuelve los datos de un proceso de la tabla Proce_Def</summary>
    '<param name="Anyo">Año</param>
    '<param name="Proce">Proce</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos del proceso de la tabla Proce_Def</returns>    

    Public Function DevolverProcesoDef(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_PROCESO_DEF"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Proce
            .Add("@GMN1", SqlDbType.VarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Actualiza los datos de un proceso de la tabla Proce_Def</summary>
    '<param name="dsProcesoDef">Datos de Proceso de la tabla Proce_Def</param>
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns> 

    Public Function ActualizarProcesoDef(ByVal dsProcesoDef As DataSet) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            ActualizarProcesoDef(dsProcesoDef, cn)

            cn.Close()
        Catch ex As SqlException
            oError = TratarError(ex)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    Private Sub ActualizarProcesoDef(ByVal dsProcesoDef As DataSet, ByVal cn As SqlConnection)
        Dim oActCom As SqlCommand = ComandoActualizarProcesoDef()
        EjecutarActualizacion(dsProcesoDef, oActCom, Nothing, Nothing, cn)
    End Sub

    '<summary>Actualiza los datos de un proceso de la tabla Proce_Def</summary>
    '<param name="dsProcesoDef">Datos de Proceso</param>
    '<param name="oCon">Conexión con la que se lleva a cabo la actualización</param>
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns>    

    Private Function ComandoActualizarProcesoDef() As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_ACTUALIZAR_PROCESO_DEF"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.SmallInt, 0, "ANYO")
            .Add("@PROCE", SqlDbType.Int, 0, "PROCE")
            .Add("@GMN1", SqlDbType.VarChar, 50, "GMN1")
            '.Add("@DEST", SqlDbType.TinyInt, 0, "DEST")
            '.Add("@PAG", SqlDbType.TinyInt, 0, "PAG")
            '.Add("@FECSUM", SqlDbType.TinyInt, 0, "FECSUM")
            '.Add("@PROVE", SqlDbType.TinyInt, 0, "PROVE")
            '.Add("@DIST", SqlDbType.TinyInt, 0, "DIST")
            '.Add("@PRESANU1", SqlDbType.TinyInt, 0, "PRESANU1")
            '.Add("@PRESANU2", SqlDbType.TinyInt, 0, "PRESANU2")
            '.Add("@PRES1", SqlDbType.TinyInt, 0, "PRES1")
            '.Add("@PRES2", SqlDbType.TinyInt, 0, "PRES2")
            '.Add("@PROCE_ESP", SqlDbType.TinyInt, 0, "PROCE_ESP")
            '.Add("@GRUPO_ESP", SqlDbType.TinyInt, 0, "GRUPO_ESP")
            '.Add("@ITEM_ESP", SqlDbType.TinyInt, 0, "ITEM_ESP")
            '.Add("@OFE_ADJUN", SqlDbType.TinyInt, 0, "OFE_ADJUN")
            '.Add("@GRUPO_ADJUN", SqlDbType.TinyInt, 0, "GRUPO_ADJUN")
            '.Add("@ITEM_ADJUN", SqlDbType.TinyInt, 0, "ITEM_ADJUN")
            '.Add("@SOLCANTMAX", SqlDbType.TinyInt, 0, "SOLCANTMAX")
            '.Add("@PONDERAR", SqlDbType.TinyInt, 0, "PONDERAR")
            '.Add("@PRECALTER", SqlDbType.TinyInt, 0, "PRECALTER")
            .Add("@SUBASTA", SqlDbType.TinyInt, 0, "SUBASTA")
            '.Add("@CAMBIARMON", SqlDbType.TinyInt, 0, "CAMBIARMON")
            '.Add("@SOLICIT", SqlDbType.TinyInt, 0, "SOLICIT")
            '.Add("@ADMIN_PUB", SqlDbType.TinyInt, 0, "ADMIN_PUB")
            '.Add("@MAX_OFE", SqlDbType.TinyInt, 0, "MAX_OFE")
            '.Add("@PUBLICARFINSUM", SqlDbType.TinyInt, 0, "PUBLICARFINSUM")
            '.Add("@AVISARDESPUB", SqlDbType.TinyInt, 0, "AVISARDESPUB")
            '.Add("@UNSOLOPEDIDO", SqlDbType.TinyInt, 0, "UNSOLOPEDIDO")
            '.Add("@OBL_ATRIB_OFE", SqlDbType.TinyInt, 0, "OBL_ATRIB_OFE")
            .Add("@VAL_ATR_ESP", SqlDbType.TinyInt, 0, "VAL_ATR_ESP")
            '.Add("@SUBASTATIPO", SqlDbType.TinyInt, 0, "SUBASTATIPO")
            '.Add("@SUBASTACOMUNICA", SqlDbType.TinyInt, 0, "SUBASTACOMUNICA")
        End With

        Return cm
    End Function

#End Region

#Region " ProcesoSubasta "

    '<summary>Devuelve los datos de subasta de un proceso en modo subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos de la subasta</returns> 

    Public Function DevolverProcesoSubasta(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_PROCESO_SUBASTA"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Proce
            .Add("@GMN1", SqlDbType.NVarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    Public Function ActualizarProcesoSubasta(ByVal oProceso As GSServerModel.Proceso, ByVal dsProcDef As DataSet, ByVal dsSubasta As DataSet, ByVal dsGrupos As DataSet, ByVal arItems() As DataSet, _
                                             ByVal dsProves As DataSet, ByVal dsReglas As DataSet, ByVal oArchivos As GSServerModel.Files, ByVal dsTextosFin As DataSet) As GSServerModel.GSException        
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Dim oErrorReglas As GSServerModel.GSException
        Dim oASR As New ArchivoSubastaReglas
        Try
            Dim sConnString As String = mDBConnection

            'Se hace la actualización de las reglas en primer lugar porque la inserción en PROCESO_SUBASTA_REGLAS necesita bloquear
            'la tabla PROCE. Si se hiciera al revés, se bloquearía la tabla PROCE para su actualización, con lo que al ir a bloquearla 
            'otra vez para la inserción en PROCESO_SUBASTA_REGLAS se produciría un timeout               
            If Not dsReglas Is Nothing AndAlso dsReglas.Tables.Count > 0 Then
                'Comprobar si ha habido cambios en el datatable
                Dim dv As New DataView(dsReglas.Tables(0), Nothing, Nothing, DataViewRowState.Added Or DataViewRowState.Deleted Or DataViewRowState.ModifiedCurrent)
                If dv.Count > 0 Then
                    oErrorReglas = oASR.ComenzarActualizarProcesoSubastaReglas(dsReglas, oArchivos)
                End If
            End If

            'Si no había reglas o si había y no se han producido errores en la actualización de las reglas se puede hacer el commit de estos cambios
            If oErrorReglas Is Nothing OrElse oErrorReglas.Number = ErroresGS.TESnoerror Then
                cn = New SqlConnection(mDBConnection)
                cn.Open()

                Me.BeginTransaction(cn)

                ActualizarProceso(oProceso, cn)
                ActualizarProcesoDef(dsProcDef, cn)
                ActualizarProcesoSubasta(dsSubasta, cn)
                If Not dsGrupos Is Nothing Then ActualizarProcesoGrupos(dsGrupos, cn)
                If Not arItems Is Nothing AndAlso arItems.Length > 0 Then
                    For Each dsItem As DataSet In arItems
                        ActualizarProcesoGrupoItems(dsItem, cn)
                    Next
                End If
                If Not dsTextosFin Is Nothing Then
                    Dim drSubasta As DataRow = dsSubasta.Tables(0).Rows(0)
                    ActualizarTextosMultiidioma(drSubasta("Anyo"), drSubasta("GMN1"), drSubasta("Proce"), dsTextosFin, cn)
                End If
                ActualizarProcesoProveedores(dsProves, cn)                

                Me.CommitTransaction(cn)
                cn.Close()

                oASR.TerminarActualizarProcesoSubastaReglas(True)
            Else
                oError = oErrorReglas
                oASR.TerminarActualizarProcesoSubastaReglas(False)
            End If

        Catch ex As SqlException
            oError = TratarError(ex)
            If Not cn Is Nothing Then Me.RollbackTransaction(cn)
            If Not oASR Is Nothing Then oASR.TerminarActualizarProcesoSubastaReglas(False)
        Finally
            If Not cn Is Nothing Then cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Actualiza los datos de subasta de un proceso en modo subasta</summary>
    '<param name="dsSubasta">Datos de la subasta</param>   
    '<param name="oCon">Conexión que se utiliza para llevar a cabo la actualización</param> 
    '<returns>Objeto de tipo GSExpection con el error si lo ha habido</returns> 

    Private Sub ActualizarProcesoSubasta(ByVal dsSubasta As DataSet, ByVal oCon As SqlConnection)
        Dim cmAct As New SqlCommand
        cmAct.CommandType = CommandType.StoredProcedure
        cmAct.CommandText = "FSGS_ACTUALIZAR_PROCESO_SUBASTA"
        With cmAct.Parameters
            .Add("@ANYO", SqlDbType.SmallInt, 0, "ANYO")
            .Add("@PROCE", SqlDbType.Int, 0, "PROCE")
            .Add("@GMN1", SqlDbType.VarChar, 50, "GMN1")
            .Add("@TIPO", SqlDbType.TinyInt, 0, "TIPO")
            .Add("@MODO", SqlDbType.TinyInt, 0, "MODO")
            .Add("@PUBLICARFEC", SqlDbType.DateTime, 0, "PUBLICARFEC")
            .Add("@MINESPERACIERRE", SqlDbType.SmallInt, 0, "MINESPERACIERRE")
            .Add("@NOTIFICAREVENTOS", SqlDbType.TinyInt, 0, "NOTIFICAREVENTOS")
            .Add("@FECAPESOBRE", SqlDbType.DateTime, 0, "FECAPESOBRE")
            .Add("@MINJAPONESA", SqlDbType.SmallInt, 0, "MINJAPONESA")
            .Add("@VERPROVEGANADOR", SqlDbType.TinyInt, 0, "VERPROVEGANADOR")
            .Add("@VERPRECIOGANADOR", SqlDbType.TinyInt, 0, "VERPRECIOGANADOR")
            .Add("@VERDETALLEPUJAS", SqlDbType.TinyInt, 0, "VERDETALLEPUJAS")
            .Add("@VERDESDEPRIMERAPUJA", SqlDbType.TinyInt, 0, "VERDESDEPRIMERAPUJA")
            .Add("@BAJMINGANADOR", SqlDbType.TinyInt, 0, "BAJMINGANADOR")
            .Add("@BAJMINGANADORTIPO", SqlDbType.TinyInt, 0, "BAJMINGANADORTIPO")
            .Add("@BAJMINPROVE", SqlDbType.TinyInt, 0, "BAJMINPROVE")
            .Add("@BAJMINPROVETIPO", SqlDbType.TinyInt, 0, "BAJMINPROVETIPO")
            .Add("@NOTIFAUTOM", SqlDbType.TinyInt, 0, "NOTIFAUTOM")
            .Add("@TEXTOFIN", SqlDbType.NVarChar, 0, "TEXTOFIN")
            .Add("@PRECSALIDA", SqlDbType.Float, 0, "PRECSALIDA")
            .Add("@MINPUJGANADOR", SqlDbType.Float, 0, "MINPUJGANADOR")
            .Add("@MINPUJPROVE", SqlDbType.Float, 0, "MINPUJPROVE")
            .Add("@EST", SqlDbType.TinyInt, 0, "EST")
        End With

        Dim cmIns As SqlCommand = cmAct.Clone
        cmIns.CommandText = "FSGS_INSERTAR_PROCESO_SUBASTA"

        EjecutarActualizacion(dsSubasta, cmAct, cmIns, Nothing, oCon)
    End Sub

    '<summary>Actualiza los datos de subasta de un proceso en modo subasta</summary>
    '<param name="oSubasta">Datos de la subasta</param>   
    '<param name="oCon">Conexión que se utiliza para llevar a cabo la actualización</param> 
    '<returns>Objeto de tipo GSExpection con el error si lo ha habido</returns> 

    Private Sub ActualizarProcesoSubasta(ByVal oSubasta As GSServerModel.ProcesoSubasta, ByVal oCon As SqlConnection)
        Dim cmAct As New SqlCommand
        cmAct.CommandType = CommandType.StoredProcedure
        cmAct.CommandText = "FSGS_ACTUALIZAR_PROCESO_SUBASTA"
        With cmAct.Parameters
            .Add("@ANYO", SqlDbType.SmallInt)
            .Add("@PROCE", SqlDbType.Int)
            .Add("@GMN1", SqlDbType.VarChar, 50)
            .Add("@TIPO", SqlDbType.TinyInt)
            .Add("@MODO", SqlDbType.TinyInt)
            .Add("@PUBLICARFEC", SqlDbType.DateTime)
            .Add("@MINESPERACIERRE", SqlDbType.SmallInt)
            .Add("@NOTIFICAREVENTOS", SqlDbType.TinyInt)
            .Add("@FECAPESOBRE", SqlDbType.DateTime)
            .Add("@MINJAPONESA", SqlDbType.SmallInt)
            .Add("@VERPROVEGANADOR", SqlDbType.TinyInt)
            .Add("@VERPRECIOGANADOR", SqlDbType.TinyInt)
            .Add("@VERDETALLEPUJAS", SqlDbType.TinyInt)
            .Add("@VERDESDEPRIMERAPUJA", SqlDbType.TinyInt)
            .Add("@BAJMINGANADOR", SqlDbType.TinyInt)
            .Add("@BAJMINGANADORTIPO", SqlDbType.TinyInt)
            .Add("@BAJMINPROVE", SqlDbType.TinyInt)
            .Add("@BAJMINPROVETIPO", SqlDbType.TinyInt)
            .Add("@NOTIFAUTOM", SqlDbType.TinyInt)
            .Add("@TEXTOFIN", SqlDbType.NVarChar)
            .Add("@PRECSALIDA", SqlDbType.Float)
            .Add("@MINPUJGANADOR", SqlDbType.Float)
            .Add("@MINPUJPROVE", SqlDbType.Float)
            .Add("@EST", SqlDbType.TinyInt)
        End With

        cmAct.Parameters("@ANYO").Value = oSubasta.Anyo
        cmAct.Parameters("@PROCE").Value = oSubasta.Codigo
        cmAct.Parameters("@GMN1").Value = oSubasta.GMN1
        cmAct.Parameters("@TIPO").Value = oSubasta.Tipo
        cmAct.Parameters("@MODO").Value = oSubasta.Modo
        cmAct.Parameters("@PUBLICARFEC").Value = DateToSQLDateDBNull(oSubasta.FechaUltPub)
        cmAct.Parameters("@MINESPERACIERRE").Value = oSubasta.MinEsperaCierre
        cmAct.Parameters("@NOTIFICAREVENTOS").Value = BooleanToSQLBinaryDBNull(oSubasta.NotificarEventos)
        cmAct.Parameters("@FECAPESOBRE").Value = DateToSQLDateDBNull(oSubasta.FechaAperturaSobre)
        cmAct.Parameters("@MINJAPONESA").Value = IntToSQLIntegerDBNull(oSubasta.MinutosJaponesa)
        cmAct.Parameters("@VERPROVEGANADOR").Value = BooleanToSQLBinaryDBNull(oSubasta.VerProveGanador)
        cmAct.Parameters("@VERPRECIOGANADOR").Value = BooleanToSQLBinaryDBNull(oSubasta.VerPrecioGanador)
        cmAct.Parameters("@VERDETALLEPUJAS").Value = BooleanToSQLBinaryDBNull(oSubasta.VerDetallePujas)
        cmAct.Parameters("@VERDESDEPRIMERAPUJA").Value = BooleanToSQLBinaryDBNull(oSubasta.VerDesdePrimeraPuja)
        cmAct.Parameters("@BAJMINGANADOR").Value = BooleanToSQLBinaryDBNull(oSubasta.BajadaMinGanador)
        cmAct.Parameters("@BAJMINGANADORTIPO").Value = oSubasta.BajadaMinGanadorTipo
        cmAct.Parameters("@BAJMINPROVE").Value = BooleanToSQLBinaryDBNull(oSubasta.BajadaMinProve)
        cmAct.Parameters("@BAJMINPROVETIPO").Value = oSubasta.BajadaMinProveTipo
        cmAct.Parameters("@NOTIFAUTOM").Value = oSubasta.NotifAutom
        cmAct.Parameters("@TEXTOFIN").Value = IntToSQLIntegerDBNull(oSubasta.TextoFin)
        cmAct.Parameters("@PRECSALIDA").Value = DoubleToSQLDoubleDBNull(oSubasta.PrecioSalida)
        cmAct.Parameters("@MINPUJGANADOR").Value = DoubleToSQLDoubleDBNull(oSubasta.MinPujaGanadora)
        cmAct.Parameters("@MINPUJPROVE").Value = DoubleToSQLDoubleDBNull(oSubasta.MinPujaProve)
        cmAct.Parameters("@EST").Value = oSubasta.Estado

        EjecutarComandoActualizacion(cmAct, oCon)
    End Sub

    '<summary>Devuelve los datos de subasta un proceso en modo subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="Grupo">Grupo</param>
    '<param name="Item">Item</param>
    '<returns>Dataset con los datos</returns>    

    Public Function DevolverSubastaProveedoresResumen(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, Optional ByVal Grupo As Nullable(Of Integer) = Nothing, Optional ByVal Item As Nullable(Of Integer) = Nothing) As DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_SUBASTA_PROVEEDORES_RESUMEN"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Proce
            .Add("@GMN1", SqlDbType.VarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
            .Add("@GRUPO", SqlDbType.SmallInt)
            cm.Parameters("@GRUPO").Value = IntToSQLIntegerDBNull(Grupo)
            .Add("@ITEM", SqlDbType.Int)
            cm.Parameters("@ITEM").Value = IntToSQLIntegerDBNull(Item)
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Devuelve los eventos de una subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>    
    '<returns>Dataset con los datos</returns>   

    Public Function DevolverSubastaEventos(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String) As DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_SUBASTA_EVENTOS"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Proce
            .Add("@GMN1", SqlDbType.VarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Devuelve los datos de pujas de un proceso en modo subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="Grupo">Grupo</param>
    '<param name="Item">Item</param>
    '<returns>Dataset con los datos</returns>    

    Public Function DevolverProcesoSubastaPujas(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, Optional ByVal Grupo As Nullable(Of Integer) = Nothing, Optional ByVal Item As Nullable(Of Integer) = Nothing) As DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_SUBASTA_PUJAS"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Proce
            .Add("@GMN1", SqlDbType.VarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
            .Add("@GRUPO", SqlDbType.SmallInt)
            cm.Parameters("@GRUPO").Value = IntToSQLIntegerDBNull(Grupo)
            .Add("@ITEM", SqlDbType.Int)
            cm.Parameters("@ITEM").Value = IntToSQLIntegerDBNull(Item)
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Lleva a cabo los cambios necesarios para ejecutar una acción de subasta</summary>      
    '<param name="oEvento">Datos del evento</param>             
    '<returns>Objeto de tipo GSException con el error si se ha producido</returns>

    Public Function AccionSubasta(ByVal oEvento As GSServerModel.ProcesoSubastaEvento) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            'Insertar el evento
            Dim oActCom As SqlCommand = ComandoInsertarEvento(oEvento)
            EjecutarComandoActualizacion(oActCom, cn)

            Me.CommitTransaction(cn)

            cn.Close()
        Catch ex As SqlException
            oError = TratarError(ex)
            Me.RollbackTransaction(cn)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    '<summary>Crea un comando SQL para actualizar el proceso</summary>
    '<param name="oProceso">Datos de Proceso</param>    
    '<returns>Objeto de tipo SqlCommand</returns>    

    Private Function ComandoInsertarEvento(ByVal oEvento As GSServerModel.ProcesoSubastaEvento) As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_INSERTAR_SUBASTA_EVENTO"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.SmallInt)
            .Add("@PROCE", SqlDbType.Int)
            .Add("@GMN1", SqlDbType.VarChar, 50)
            .Add("@ACCION", SqlDbType.TinyInt)
            .Add("@FECHA", SqlDbType.DateTime)
            .Add("@EXTEN", SqlDbType.Int)
            .Add("@FECFINHIST", SqlDbType.DateTime)
            .Add("@MODREINISUB", SqlDbType.TinyInt)
            .Add("@MINREINISUB", SqlDbType.SmallInt)
            .Add("@FECREINISUB", SqlDbType.DateTime)
            .Add("@MODINCTSUB", SqlDbType.TinyInt)
            .Add("@MININCTSUB", SqlDbType.SmallInt)
        End With

        cm.Parameters("@ANYO").Value = oEvento.Anyo
        cm.Parameters("@PROCE").Value = oEvento.Proceso
        cm.Parameters("@GMN1").Value = oEvento.GMN1
        cm.Parameters("@ACCION").Value = oEvento.Accion
        cm.Parameters("@FECHA").Value = oEvento.Fecha
        cm.Parameters("@EXTEN").Value = IntToSQLIntegerDBNull(oEvento.Extension)
        cm.Parameters("@FECFINHIST").Value = DateToSQLDateDBNull(oEvento.FechaFinHist)
        cm.Parameters("@MODREINISUB").Value = IntToSQLIntegerDBNull(oEvento.ModoReinicioSub)
        cm.Parameters("@MINREINISUB").Value = IntToSQLIntegerDBNull(oEvento.MinReinicioSub)
        cm.Parameters("@FECREINISUB").Value = DateToSQLDateDBNull(oEvento.FechaReinicioSub)
        cm.Parameters("@MODINCTSUB").Value = IntToSQLIntegerDBNull(oEvento.ModoIncTSub)
        cm.Parameters("@MININCTSUB").Value = IntToSQLIntegerDBNull(oEvento.MinIncTSub)

        Return cm
    End Function

#End Region

#Region " ProcesoSubastaReglas "

    '<summary>Devuelve reglas de subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos de la subasta</returns> 

    Public Function DevolverSubastaReglas(Optional ByVal Anyo As Integer = -1, Optional ByVal Proce As Integer = -1, Optional ByVal GMN1 As String = Nothing) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_SUBASTA_REGLAS"
        With cm.Parameters
            If Anyo <> -1 Then
                .Add("@ANYO", SqlDbType.Int)
                cm.Parameters("@ANYO").Value = Anyo
            End If
            If Proce <> -1 Then
                .Add("@PROCE", SqlDbType.Int)
                cm.Parameters("@PROCE").Value = Proce
            End If
            If Not GMN1 Is Nothing Then
                .Add("@GMN1", SqlDbType.NVarChar, 50)
                cm.Parameters("@GMN1").Value = GMN1
            End If
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Actualiza las reglas de subasta de un proceso</summary>
    '<param name="dsReglas">Reglas del proceso</param>
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns>    

    Public Function ActualizarProcesoSubastaReglas(ByVal dsReglas As DataSet, ByVal oArchivos As GSServerModel.Files) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim oASR As New ArchivoSubastaReglas
        oError = oASR.ComenzarActualizarProcesoSubastaReglas(dsReglas, oArchivos)
        oASR.TerminarActualizarProcesoSubastaReglas(oError.Number = ErroresGS.TESnoerror)

        Return oError
    End Function

#End Region

#Region " TextosMultiidioma "

    '<summary>Devuelve los textos de fin de subasta</summary>
    '<param name="ID">ID</param>
    '<returns>Dataset con los textos de fin de la subasta</returns> 

    Public Function DevolverTextosMultiidioma(ByVal ID As Integer) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_TEXTOSMULTIIDIOMA"
        With cm.Parameters
            .Add("@ID", SqlDbType.Int)
            cm.Parameters("@ID").Value = ID
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Actualiza los textos de fin de subasta de un proceso</summary>
    '<param name="dsReglas">Reglas del proceso</param>
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns>    

    Public Function ActualizarTextosMultiidioma(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Integer, ByVal dsTextos As DataSet) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            ActualizarTextosMultiidioma(Anyo, GMN1, Proce, dsTextos, cn)

            Me.CommitTransaction(cn)

            cn.Close()
        Catch ex As SqlException
            oError = TratarError(ex)
            Me.RollbackTransaction(cn)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    Private Sub ActualizarTextosMultiidioma(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Integer, ByVal dsTextos As DataSet, ByVal cn As SqlConnection)
        Dim oActCom As SqlCommand = ComandoActualizarTextosMultiidioma()
        Dim oInsCom As SqlCommand = ComandoInsertarTextosMultiidioma(Anyo, GMN1, Proce)
        Dim oDelCom As SqlCommand = ComandoBorrarTextosMultiidioma()
        EjecutarActualizacion(dsTextos, oActCom, oInsCom, oDelCom, cn)
    End Sub

    '<summary>Devuelve un comnado para actualizar los textos de fin de una subasta</summary>
    '<param name="dsTextos">Datos de los textos</param>
    '<param name="oCon">Conexión con la que se lleva a cabo la actualización</param>
    '<returns>Comando de actualización</returns>    

    Private Function ComandoActualizarTextosMultiidioma() As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_ACTUALIZAR_TEXTOSMULTIIDIOMA"
        With cm.Parameters
            .Add("@ID", SqlDbType.Int, 0, "ID")
            .Add("@IDI", SqlDbType.VarChar, 50, "IDI")
            .Add("@TEXTO", SqlDbType.NVarChar, 0, "TEXTO")
            .Add("@TIPO", SqlDbType.NVarChar, 25, "TIPO")
        End With

        Return cm
    End Function

    '<summary>Devuelve un comnado para insertar los textos de fin de una subasta</summary>
    '<param name="dsTextos">Datos de los textos</param>
    '<param name="oCon">Conexión con la que se lleva a cabo la inserción</param>
    '<returns>Comando de inserción</returns>   

    Private Function ComandoInsertarTextosMultiidioma(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Integer) As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_INSERTAR_TEXTOSMULTIIDIOMA"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Proce
            .Add("@GMN1", SqlDbType.VarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
            .Add("@ID", SqlDbType.Int, 0, "ID")
            .Add("@IDI", SqlDbType.VarChar, 50, "IDI")
            .Add("@TEXTO", SqlDbType.NVarChar, 0, "TEXTO")
            .Add("@TIPO", SqlDbType.NVarChar, 25, "TIPO")
        End With

        Return cm
    End Function

    '<summary>Devuelve un comnado para borrar los textos de fin de una subasta</summary>
    '<param name="dsTextos">Datos de los textos</param>
    '<param name="oCon">Conexión con la que se lleva a cabo el borrado</param>
    '<returns>Comando de borrado</returns>  

    Private Function ComandoBorrarTextosMultiidioma() As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_BORRAR_TEXTOSMULTIIDIOMA"
        With cm.Parameters
            .Add("@ID", SqlDbType.Int, 0, "ID")
            .Add("@IDI", SqlDbType.VarChar, 50, "IDI")
        End With

        Return cm
    End Function

#End Region

#Region " ProcesoProveedor "

    '<summary>Devuelve proveedores</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos de los provedores</returns> 

    Public Function DevolverProveedores(Optional ByVal Anyo As Integer = -1, Optional ByVal Proce As Integer = -1, Optional ByVal GMN1 As String = Nothing, _
                                        Optional ByVal Subasta As Boolean = False, Optional ByVal Prove As String = Nothing) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_PROVEEDORES"
        If Anyo <> -1 Then
            cm.Parameters.Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
        End If
        If Proce <> -1 Then
            cm.Parameters.Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Proce
        End If
        If Not GMN1 Is Nothing Then
            cm.Parameters.Add("@GMN1", SqlDbType.NVarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
        End If
        If Subasta Then
            cm.Parameters.Add("@SUBASTA", SqlDbType.TinyInt)
            cm.Parameters("@SUBASTA").Value = 1
        End If
        If Not Prove Is Nothing Then
            cm.Parameters.Add("@PROVE", SqlDbType.VarChar, 50)
            cm.Parameters("@PROVE").Value = Prove
        End If

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Devuelve proveedores</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos de los provedores</returns>

    Public Function DevolverProveedoresComunicados(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal dtFechaCom As Date) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_PROVEEDORES_COMUNICADOS"

        cm.Parameters.Add("@ANYO", SqlDbType.Int)
        cm.Parameters("@ANYO").Value = Anyo
        cm.Parameters.Add("@PROCE", SqlDbType.Int)
        cm.Parameters("@PROCE").Value = Proce
        cm.Parameters.Add("@GMN1", SqlDbType.NVarChar, 50)
        cm.Parameters("@GMN1").Value = GMN1
        cm.Parameters.Add("@FECHACOM", SqlDbType.DateTime)
        cm.Parameters("@FECHACOM").Value = dtFechaCom

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Actualiza los datos de proveedores un proceso</summary>
    '<param name="dsProves">Datos de proveedores del proceso</param>
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns>    

    Public Function ActualizarProcesoProveedores(ByVal dsProves As DataSet) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            ActualizarProcesoProveedores(dsProves, cn)

            cn.Close()
        Catch ex As SqlException
            oError = TratarError(ex)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    Private Sub ActualizarProcesoProveedores(ByVal dsProves As DataSet, ByVal cn As SqlConnection)
        Dim oActCom As SqlCommand = ComandoActualizarProcesoProveedores(dsProves)
        EjecutarActualizacion(dsProves, oActCom, Nothing, Nothing, cn)
    End Sub

    '<summary>Devuelve un comnado para actualizar los datos de proveedores de un proceso</summary>
    '<param name="dsProves">Datos de proveedores</param>
    '<param name="oCon">Conexión con la que se lleva a cabo la actualización</param>
    '<returns>Comando de actualización</returns>    

    Private Function ComandoActualizarProcesoProveedores(ByVal dsProves As DataSet) As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_ACTUALIZAR_PROCESO_PROVEEDORES"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.SmallInt, 0, "ANYO")
            .Add("@PROCE", SqlDbType.Int, 0, "PROCE")
            .Add("@GMN1", SqlDbType.VarChar, 50, "GMN1")
            .Add("@PROVE", SqlDbType.VarChar, 50, "PROVE")
            .Add("@EQP", SqlDbType.VarChar, 50, "EQP")
            .Add("@COM", SqlDbType.VarChar, 50, "COM")
            .Add("@PUB", SqlDbType.TinyInt, 0, "PUB")
            .Add("@FECPUB", SqlDbType.DateTime, 0, "FECPUB")
            .Add("@OFE", SqlDbType.Bit, 0, "OFE")
            .Add("@SUPERADA", SqlDbType.TinyInt, 0, "SUPERADA")
            .Add("@NUMOBJ", SqlDbType.Int, 0, "NUMOBJ")
            .Add("@OBJNUEVOS", SqlDbType.TinyInt, 0, "OBJNUEVOS")
            .Add("@CERRADO", SqlDbType.TinyInt, 0, "CERRADO")
            .Add("@NO_OFE", SqlDbType.TinyInt, 0, "NO_OFE")
            .Add("@FEC_NO_OFE", SqlDbType.DateTime, 0, "FEC_NO_OFE")
            .Add("@MOTIVO_NO_OFE", SqlDbType.VarChar, 4000, "MOTIVO_NO_OFE")
            .Add("@AVISADODESPUB", SqlDbType.TinyInt, 0, "AVISADODESPUB")
            .Add("@PORTAL_NO_OFE", SqlDbType.TinyInt, 0, "PORTAL_NO_OFE")
            .Add("@USU_NO_OFE", SqlDbType.VarChar, 50, "USU_NO_OFE")
            .Add("@NOMUSU_NO_OFE", SqlDbType.VarChar, 200, "NOMUSU_NO_OFE")
            .Add("@SUBASTA", SqlDbType.TinyInt, 0, "SUBASTA")
        End With

        Return cm
    End Function    

#End Region

#Region " ProcesoGrupos "

    '<summary>Devuelve grupos</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos de los grupos</returns> 

    Public Function DevolverGrupos(Optional ByVal Anyo As Integer = -1, Optional ByVal Proce As Integer = -1, Optional ByVal GMN1 As String = Nothing) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_GRUPOS"
        If Anyo <> -1 Then
            cm.Parameters.Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
        End If
        If Proce <> -1 Then
            cm.Parameters.Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Proce
        End If
        If Not GMN1 Is Nothing Then
            cm.Parameters.Add("@GMN1", SqlDbType.NVarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
        End If

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Actualiza los datos de grupos un proceso</summary>
    '<param name="dsGrupos">Datos de grupos del proceso</param>
    '<param name="arItems">Array de datasets con los datos de los items a actualizar</param>
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns>    

    Public Function ActualizarProcesoGrupos(ByVal dsGrupos As DataSet, ByVal arItems As DataSet()) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            Me.BeginTransaction(cn)

            ActualizarProcesoGrupos(dsGrupos, cn)
            If Not arItems Is Nothing AndAlso arItems.Length > 0 Then
                For Each ds As DataSet In arItems
                    ActualizarProcesoGrupos(ds, cn)
                Next
            End If

            Me.CommitTransaction(cn)

            cn.Close()
        Catch ex As SqlException
            oError = TratarError(ex)
            Me.RollbackTransaction(cn)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    Private Sub ActualizarProcesoGrupos(ByVal dsGrupos As DataSet, ByVal cn As SqlConnection)
        Dim oActCom As SqlCommand = ComandoActualizarProcesoGrupos(dsGrupos)
        EjecutarActualizacion(dsGrupos, oActCom, Nothing, Nothing, cn)
    End Sub

    '<summary>Devuelve un comnado para actualizar los datos de grupos de un proceso</summary>
    '<param name="dsGrupos">Datos de grupos</param>
    '<param name="oCon">Conexión con la que se lleva a cabo la actualización</param>
    '<returns>Comando de actualización</returns>    

    Private Function ComandoActualizarProcesoGrupos(ByVal dsProves As DataSet) As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_ACTUALIZAR_PROCESO_GRUPOS"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.SmallInt, 0, "ANYO")
            .Add("@PROCE", SqlDbType.Int, 0, "PROCE")
            .Add("@GMN1", SqlDbType.VarChar, 50, "GMN1")
            .Add("@ID", SqlDbType.SmallInt, 0, "ID")
            '.Add("@COD", SqlDbType.VarChar, 50, "COD")
            '.Add("@DEN", SqlDbType.VarChar, 100, "DEN")
            '.Add("@DESCR", SqlDbType.VarChar, 500, "DESCR")
            '.Add("@DEST", SqlDbType.VarChar, 50, "DEST")
            '.Add("@PAG", SqlDbType.NVarChar, 50, "PAG")
            '.Add("@FECINI", SqlDbType.DateTime, 0, "FECINI")
            '.Add("@FECFIN", SqlDbType.DateTime, 0, "FECFIN")
            '.Add("@PROVEACT", SqlDbType.VarChar, 50, "PROVEACT")
            '.Add("@ESP", SqlDbType.VarChar, 800, "ESP")
            '.Add("@CERRADO", SqlDbType.TinyInt, 0, "CERRADO")
            '.Add("@SOLICIT", SqlDbType.Int, 0, "SOLICIT")
            '.Add("@SOBRE", SqlDbType.SmallInt, 0, "SOBRE")
            '.Add("@ADJUDICADO", SqlDbType.Float, 0, "ADJUDICADO")
            .Add("@PRECSALIDA", SqlDbType.Float, 0, "PRECSALIDA")
            .Add("@MINPUJGANADOR", SqlDbType.Float, 0, "MINPUJGANADOR")
            .Add("@MINPUJPROVE", SqlDbType.Float, 0, "MINPUJPROVE")
        End With

        Return cm
    End Function

#End Region

#Region " Item "

    '<summary>Devuelve los items asociados al grupo de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos de los items</returns> 

    Public Function DevolverItems(Optional ByVal Anyo As Integer = -1, Optional ByVal Proce As Integer = -1, Optional ByVal GMN1 As String = Nothing, _
                                  Optional ByVal GRUPO As Integer = -1) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_ITEMS"
        If Anyo <> -1 Then
            cm.Parameters.Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
        End If
        If Proce <> -1 Then
            cm.Parameters.Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Proce
        End If
        If Not GMN1 Is Nothing Then
            cm.Parameters.Add("@GMN1", SqlDbType.NVarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
        End If
        If GRUPO <> -1 Then
            cm.Parameters.Add("@GRUPO", SqlDbType.Int)
            cm.Parameters("@GRUPO").Value = GRUPO
        End If

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Actualiza los datos de items de un grupo</summary>
    '<param name="dsItems">Datos de items de un grupo</param>
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns>    

    Public Function ActualizarProcesoGrupoItems(ByVal dsItems As DataSet) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            ActualizarProcesoGrupoItems(dsItems, cn)

            cn.Close()
        Catch ex As SqlException
            oError = TratarError(ex)
        Finally
            cn.Dispose()
        End Try

        Return oError
    End Function

    Private Sub ActualizarProcesoGrupoItems(ByVal dsItems As DataSet, ByVal cn As SqlConnection)
        Dim oActCom As SqlCommand = ComandoActualizarProcesoGrupoItems()
        EjecutarActualizacion(dsItems, oActCom, Nothing, Nothing, cn)
    End Sub

    '<summary>Devuelve un comnado para actualizar los datos de items de un grupo</summary>
    '<param name="dsItems">Datos de items</param>
    '<param name="oCon">Conexión con la que se lleva a cabo la actualización</param>
    '<returns>Comando de actualización</returns>    

    Private Function ComandoActualizarProcesoGrupoItems() As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_ACTUALIZAR_PROCESO_GRUPO_ITEMS"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.SmallInt, 0, "ANYO")
            .Add("@PROCE", SqlDbType.Int, 0, "PROCE")
            .Add("@GMN1_PROCE", SqlDbType.VarChar, 50, "GMN1_PROCE")
            .Add("@ID", SqlDbType.SmallInt, 0, "ID")
            '.Add("@ART", SqlDbType.VarChar, 50, "ART")
            '.Add("@DESCR", SqlDbType.VarChar, 200, "DESCR")
            '.Add("@GMN1", SqlDbType.VarChar, 50, "GMN1")
            '.Add("@GMN2", SqlDbType.VarChar, 50, "GMN2")
            '.Add("@GMN3", SqlDbType.VarChar, 50, "GMN3")
            '.Add("@GMN4", SqlDbType.VarChar, 50, "GMN4")
            '.Add("@DEST", SqlDbType.VarChar, 50, "DEST")
            '.Add("@UNI", SqlDbType.NVarChar, 50, "UNI")
            '.Add("@PAG", SqlDbType.NVarChar, 50, "PAG")
            '.Add("@CANT", SqlDbType.Float, 0, "CANT")
            '.Add("@PREC", SqlDbType.Float, 0, "PREC")
            '.Add("@PRES", SqlDbType.Float, 0, "PRES")
            '.Add("@FECINI", SqlDbType.DateTime, 0, "FECINI")
            '.Add("@FECFIN", SqlDbType.DateTime, 0, "FECFIN")
            '.Add("@OBJ", SqlDbType.Float, 0, "OBJ")
            '.Add("@ESP", SqlDbType.VarChar, 800, "ESP")
            '.Add("@CONF", SqlDbType.TinyInt, 0, "CONF")
            '.Add("@FECHAOBJ", SqlDbType.DateTime, 0, "FECHAOBJ")
            '.Add("@PROVEACT", SqlDbType.VarChar, 50, "PROVEACT")
            '.Add("@EST", SqlDbType.TinyInt, 0, "EST")
            '.Add("@GRUPO", SqlDbType.SmallInt, 0, "GRUPO")
            '.Add("@SOLICIT", SqlDbType.Int, 0, "SOLICIT")
            '.Add("@LINEA_SOLICIT", SqlDbType.Int, 0, "LINEA_SOLICIT")
            '.Add("@FECULTREU", SqlDbType.DateTime, 0, "FECULTREU")
            '.Add("@FECCIERRE", SqlDbType.DateTime, 0, "FECCIERRE")
            '.Add("@ADJCOM", SqlDbType.TinyInt, 0, "ADJCOM")
            '.Add("@CAMPO_SOLICIT", SqlDbType.Int, 0, "CAMPO_SOLICIT")
            .Add("@MINPUJGANADOR", SqlDbType.Float, 0, "MINPUJGANADOR")
            .Add("@PRECSALIDA", SqlDbType.Float, 0, "PRECSALIDA")
            .Add("@MINPUJPROVE", SqlDbType.Float, 0, "MINPUJPROVE")
        End With

        Return cm
    End Function

    '<summary>Actualiza los datos de un proceso</summary>
    '<param name="oProceso">Datos de Proceso</param>
    '<param name="cn">Conexión</param>
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns> 

    Private Sub ActualizarUsuario(ByVal oUsuario As GSServerModel.Usuario, ByVal cn As SqlConnection)
        Dim oActCom As SqlCommand = ComandoActualizarUsuario(oUsuario)
        EjecutarComandoActualizacion(oActCom, cn)
    End Sub

    '<summary>Crea un comando SQL para actualizar el proceso</summary>
    '<param name="oProceso">Datos de Proceso</param>    
    '<returns>Objeto de tipo SqlCommand</returns>    

    Private Function ComandoActualizarUsuario(ByVal oUsuario As GSServerModel.Usuario) As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_ACTUALIZAR_USUARIO"
        With cm.Parameters
            .Add("@USU", SqlDbType.NVarChar, 50)
            .Add("@TIMEZONE", SqlDbType.VarChar, 100)
        End With

        cm.Parameters("@USU").Value = oUsuario.Cod
        cm.Parameters("@TIMEZONE").Value = oUsuario.TimeZone

        Return cm
    End Function

#End Region

#Region " Envío EMails "

    '<summary>Devuelve los items asociados al grupo de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos de los items</returns> 

    Public Function DevolverParamConfigServicioEMail() As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_PARAM_CONFIG_SERVICIOEMAIL"

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Función que graba en la tabla REGISTRO_EMAIL toda la información del email enviado.</summary>
    '<param name="From">El from del mensaje</param>
    '<param name="Subject">El asunto</param>
    '<param name="Para">El destinatario</param>
    ' <param name="Message">El mensaje</param>
    '<param name="CC">Copia a</param>
    '<param name="CCO">Copia oculta</param>
    '<param name="bEsPm">Si estamos en PM o no.</param>
    '<param name="bHTML">Si el email se ha enviado con formato HTML o no.</param>
    '<param name="bKO">Si ha ido bien el envío.</param>
    '<param name="sTextoError">El texto de error (si hay)</param>
    '<param name="sAttachments">Los adjuntos atachados en el email</param>
    '<param name="Ruta">La ruta de los ficheros adjuntos</param>
    '<param name="CodProve">Proveedor al q se le envia el mail</param>
    '<param name="IdMail">Tipo de Notificacion. Es un valor de FSNLibrary.TipoNotificacion</param>
    '<param name="lIdInstancia">Id de la instancia a la que asociaremos la notificación</param>
    '<remarks>Llamada desde: PMNotificador --> Notificar.vb --> Enviarmail. Tiempo máximo: 0 sg.</remarks>

    Public Sub GrabarEnviarMensaje(ByVal oEMail As EMail, ByVal bKO As Boolean, ByVal strError As String, _
                                   ByVal Ruta As String, ByVal CodProve As String, ByVal IdMail As Integer, Optional ByVal lIdInstancia As Long = 0)
        Dim lRegMailId As Long
        Dim lRegMailAdjunId As Long

        Dim sPath As String
        Dim oFileStream As System.IO.FileStream
        Dim byteBuffer() As Byte
        Dim lDataSize As Long

        Dim cn As SqlConnection
        Dim cm As SqlCommand
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()

            cm = New SqlCommand("BEGIN TRANSACTION", cn)
            cm.ExecuteNonQuery()

            cm = New SqlCommand("FSGS_INSERT_REGISTRO_MAIL", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@SUBJECT", SqlDbType.NVarChar, 100)
            cm.Parameters("@SUBJECT").Value = oEMail.Asunto
            If IsNothing(cm.Parameters("@SUBJECT").Value) _
            OrElse (cm.Parameters("@SUBJECT").Value = "") Then
                cm.Parameters("@SUBJECT").Value = "Error antes de crear subject"
            End If
            cm.Parameters.Add("@PARA", SqlDbType.NVarChar, 100)
            Dim sPara As String
            For Each oContacto As Contacto In oEMail.Para
                sPara &= oContacto.EMail & ";"
            Next
            cm.Parameters("@PARA").Value = sPara
            If IsNothing(cm.Parameters("@PARA").Value) _
            OrElse (cm.Parameters("@PARA").Value = "") Then
                cm.Parameters("@PARA").Value = "Error antes de crear para"
            End If
            Dim sCC As String = String.Empty
            If Not oEMail.CC Is Nothing AndAlso oEMail.CC.Count > 0 Then
                For Each oContacto As Contacto In oEMail.CC
                    sCC &= oContacto.EMail & ";"
                Next
            End If
            cm.Parameters.Add("@CC", SqlDbType.NVarChar, 100)
            cm.Parameters("@CC").Value = sCC
            Dim sBCC As String = String.Empty
            If Not oEMail.BCC Is Nothing AndAlso oEMail.BCC.Count > 0 Then
                For Each oContacto As Contacto In oEMail.BCC
                    sBCC &= oContacto.EMail & ";"
                Next
            End If
            cm.Parameters.Add("@CCO", SqlDbType.NVarChar, 100)
            cm.Parameters("@CCO").Value = sBCC
            cm.Parameters.Add("@DIR_RESPUESTA", SqlDbType.NVarChar, 100)
            cm.Parameters("@DIR_RESPUESTA").Value = oEMail.De.EMail
            cm.Parameters.Add("@CUERPO", SqlDbType.NText)
            cm.Parameters("@CUERPO").Value = oEMail.Contenido
            If IsNothing(cm.Parameters("@CUERPO").Value) _
            OrElse (cm.Parameters("@CUERPO").Value = "") Then
                'Error antes de crear cuerpo
                cm.Parameters("@CUERPO").Value = "Error antes de crear cuerpo"
            End If
            cm.Parameters.Add("@ACUSE_RECIBO", SqlDbType.TinyInt)
            cm.Parameters("@ACUSE_RECIBO").Value = 0
            cm.Parameters.Add("@TIPO", SqlDbType.TinyInt)
            cm.Parameters("@TIPO").Value = oEMail.TipoEmail
            cm.Parameters.Add("@PRODUCTO", SqlDbType.Int)
            cm.Parameters("@PRODUCTO").Value = 3        'IIf(bEsPm, 2, 3)
            cm.Parameters.Add("@USU", SqlDbType.NVarChar, 20)
            cm.Parameters("@USU").Value = mUserCod
            cm.Parameters.Add("@KO", SqlDbType.TinyInt)
            cm.Parameters("@KO").Value = IIf(bKO, 1, 0)
            cm.Parameters.Add("@TEXTO_ERROR", SqlDbType.NVarChar, 200)
            cm.Parameters("@TEXTO_ERROR").Value = strError

            cm.Parameters.Add("@IDENTIFICADOR", SqlDbType.Int).Direction = ParameterDirection.Output

            cm.Parameters.Add("@PROVE", SqlDbType.VarChar, 50)
            cm.Parameters("@PROVE").Value = CodProve
            cm.Parameters.Add("@ID_EMAIL", SqlDbType.TinyInt)
            cm.Parameters("@ID_EMAIL").Value = IdMail
            If lIdInstancia <> 0 Then
                cm.Parameters.Add("@INSTANCIA", SqlDbType.Int)
                cm.Parameters("@INSTANCIA").Value = lIdInstancia
            End If

            cm.ExecuteNonQuery()

            lRegMailId = cm.Parameters("@IDENTIFICADOR").Value


            If Ruta <> "" Then
                If Not oEMail.Adjuntos Is Nothing AndAlso oEMail.Adjuntos.Count > 0 Then
                    For Each oAdj As File In oEMail.Adjuntos
                        sPath = Ruta & oAdj.NombreArchivo
                        Try
                            cm = New SqlCommand("FSGS_INSERT_REGISTRO_MAIL_ADJUN", cn)
                            cm.CommandType = CommandType.StoredProcedure

                            cm.Parameters.Add("@REG_ID", SqlDbType.Int)
                            cm.Parameters("@REG_ID").Value = lRegMailId
                            cm.Parameters.Add("@NOM", SqlDbType.NVarChar, 300)
                            cm.Parameters("@NOM").Value = sPath

                            cm.Parameters.Add("@IDENTIFICADOR", SqlDbType.Int).Direction = ParameterDirection.Output

                            cm.ExecuteNonQuery()

                            lRegMailAdjunId = cm.Parameters("@IDENTIFICADOR").Value

                            ''Updatar DATA
                            oFileStream = New System.IO.FileStream(sPath, IO.FileMode.Open)

                            ReDim byteBuffer(oFileStream.Length)
                            lDataSize = oFileStream.Length

                            Dim sAux() As String = Split(oFileStream.Name, "\")

                            Dim sNom As String = sAux(UBound(sAux))

                            Call oFileStream.Read(byteBuffer, 0, Int32.Parse(oFileStream.Length.ToString()))

                            cm = New SqlCommand("FSGS_UPDATE_REGISTRO_MAIL_ADJUN", cn)
                            cm.CommandType = CommandType.StoredProcedure

                            cm.Parameters.Add("@REG_ID", SqlDbType.Int)
                            cm.Parameters("@REG_ID").Value = lRegMailId
                            cm.Parameters.Add("@IDENTIFICADOR", SqlDbType.Int)
                            cm.Parameters("@IDENTIFICADOR").Value = lRegMailAdjunId
                            cm.Parameters.Add("@INIT", SqlDbType.Int)
                            cm.Parameters("@INIT").Value = 0
                            cm.Parameters.Add("@DATA", SqlDbType.Image)
                            cm.Parameters("@DATA").Value = byteBuffer

                            cm.ExecuteNonQuery()

                            oFileStream.Close()
                        Catch ex As Exception

                        End Try
                    Next
                End If
            End If

            cm = New SqlCommand("COMMIT TRANSACTION", cn)
            cm.ExecuteNonQuery()

        Catch ex As Exception
            Try
                cm = New SqlCommand("FSGS_UPDATE_REGISTRO_MAIL", cn)
                cm.CommandType = CommandType.StoredProcedure

                cm.Parameters.Add("@IDENTIFICADOR", SqlDbType.Int)
                cm.Parameters("@IDENTIFICADOR").Value = lRegMailId
                cm.Parameters.Add("@TEXTO_ERROR", SqlDbType.NVarChar, 200)
                cm.Parameters("@TEXTO_ERROR").Value = ex.Message

                cm.ExecuteNonQuery()

                cm = New SqlCommand("COMMIT TRANSACTION", cn)
                cm.ExecuteNonQuery()

            Catch ex2 As Exception
                cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
                cm.ExecuteNonQuery()
            End Try
        Finally
            cn.Close()
            cn = Nothing

            cm = Nothing

            oFileStream = Nothing
        End Try
    End Sub

#End Region

End Class
