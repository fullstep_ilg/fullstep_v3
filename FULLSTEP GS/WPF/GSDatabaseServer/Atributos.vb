﻿Imports Fullstep.FSNLibrary
Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel

Public Class Atributos
    Inherits DBServerBase

    '<summary>Devuelve los datos de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="Oblig">Oblig</param>
    '<param name="Validacion">Validacion</param>
    '<param name="Vacios">Vacios</param>
    '<returns>Dataset con los datos del proceso</returns>    

    Public Function DevolverAtributosProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, _
                                             Optional ByVal Oblig As Object = Nothing, Optional ByVal Validacion As Object = Nothing, _
                                             Optional ByVal Vacios As Object = Nothing) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_ATRIBUTOS_PROCESO"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Cod
            .Add("@GMN1", SqlDbType.NVarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
            If Not Oblig Is Nothing Then
                .Add("@OBLIG", SqlDbType.TinyInt)
                cm.Parameters("@OBLIG").Value = BooleanToSQLBinary(Oblig)
            End If
            If Not Validacion Is Nothing Then
                .Add("@VALIDACION", SqlDbType.TinyInt)
                cm.Parameters("@VALIDACION").Value = Validacion
            End If
            If Not Vacios Is Nothing Then
                .Add("@VACIOS", SqlDbType.TinyInt)
                cm.Parameters("@VACIOS").Value = BooleanToSQLBinary(Vacios)
            End If
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Devuelve los datos de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos del proceso</returns>    

    Public Function DevolverAtributosProcesoGrupo(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal Grupo As Integer = -1, _
                                                  Optional ByVal Oblig As Object = Nothing, Optional ByVal Validacion As Object = Nothing, _
                                                  Optional ByVal Vacios As Object = Nothing) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_ATRIBUTOS_PROCESO_GRUPO"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Cod
            .Add("@GMN1", SqlDbType.NVarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
            If Grupo <> -1 Then
                .Add("@GRUPO", SqlDbType.SmallInt)
                cm.Parameters("@GRUPO").Value = Grupo
            End If
            If Not Oblig Is Nothing Then
                .Add("@OBLIG", SqlDbType.TinyInt)
                cm.Parameters("@OBLIG").Value = BooleanToSQLBinary(Oblig)
            End If
            If Not Validacion Is Nothing Then
                .Add("@VALIDACION", SqlDbType.TinyInt)
                cm.Parameters("@VALIDACION").Value = Validacion
            End If
            If Not Vacios Is Nothing Then
                .Add("@VACIOS", SqlDbType.TinyInt)
                cm.Parameters("@VACIOS").Value = BooleanToSQLBinary(Vacios)
            End If
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    '<summary>Devuelve los datos de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos del proceso</returns>    

    Public Function DevolverAtributosItem(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal Item As Integer = -1, _
                                                  Optional ByVal Oblig As Object = Nothing, Optional ByVal Validacion As Object = Nothing, _
                                                  Optional ByVal Vacios As Object = Nothing) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_ATRIBUTOS_ITEM"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.Int)
            cm.Parameters("@ANYO").Value = Anyo
            .Add("@PROCE", SqlDbType.Int)
            cm.Parameters("@PROCE").Value = Cod
            .Add("@GMN1", SqlDbType.VarChar, 50)
            cm.Parameters("@GMN1").Value = GMN1
            If Item <> -1 Then
                .Add("@ITEM", SqlDbType.Int)
                cm.Parameters("@ITEM").Value = Item
            End If
            If Not Oblig Is Nothing Then
                .Add("@OBLIG", SqlDbType.TinyInt)
                cm.Parameters("@OBLIG").Value = BooleanToSQLBinary(Oblig)
            End If
            If Not Validacion Is Nothing Then
                .Add("@VALIDACION", SqlDbType.TinyInt)
                cm.Parameters("@VALIDACION").Value = Validacion
            End If
            If Not Vacios Is Nothing Then
                .Add("@VACIOS", SqlDbType.TinyInt)
                cm.Parameters("@VACIOS").Value = BooleanToSQLBinary(Vacios)
            End If
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

End Class
