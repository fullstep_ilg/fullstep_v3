﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports System.Text
Imports System.Security.Cryptography
Imports System.Security.Permissions
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos

<Serializable()> _
Public Class Root
    Inherits DBServerBase

#Region " UserIdentity data access methods "

    Public Function UserIdentity_GetActions(Optional ByVal usercode As String = "", Optional ByVal userpassword As String = "") As UserIdentity_Actions

        Authenticate(usercode, userpassword)

        Dim Act As New UserIdentity_Actions
        ReDim Act.ID(1)
        Act.ID(0) = 1
        Act.ID(1) = 2
        Return Act
    End Function
#End Region

#Region "Data Access Methods"

#Region " cXML data access methods "

    ''' <summary>
    ''' Función que ejectuta un Procedimiento almacenado en la Base de Datos, dado su nombre y sus parÃƒÂ¡metros en una lista de strings
    ''' </summary>
    ''' <param name="NombreStored">Nombre del Procedimiento almacenado que se quiere ejecutar</param>
    ''' <param name="Parametros">ParÃƒÂ¡metros de entrada del procedimiento almacenado</param>
    ''' <returns>Un objeto de tipo DataSet con el resultado de la consulta del Procedimiento almacenado</returns>
    ''' <remarks>
    ''' Llamada desde: La clase cXML mÃƒÂ©todo EjecutaStored
    ''' Tiempo mÃƒÂ¡ximo: Variable, depende completamente del Procedimiento al que se llame.</remarks>
    <PrincipalPermission(SecurityAction.Demand, Authenticated:=True)> _
Public Function cXML_EjecutaStored(ByVal NombreStored As String, ByVal Parametros() As TiposDeDatos.ParametrosOrigenDeDatos) As DataSet
        Dim SqlCon As New SqlConnection(mDBConnection)
        Dim DsetResul As New DataSet
        Dim SqlCmd As New SqlCommand
        Dim sqlParam As New SqlParameter
        Dim SqlDa As New SqlDataAdapter
        SqlCmd.CommandText = NombreStored
        SqlCmd.CommandType = CommandType.StoredProcedure
        If Not Parametros Is Nothing AndAlso Parametros.Count > 0 Then
            Dim ContParametros As Integer = 0
            For Each param As TiposDeDatos.ParametrosOrigenDeDatos In Parametros
                If Not param.ValorDato.ToUpper = "SYSTEM.DBNULL.VALUE" Then
                    sqlParam = SqlCmd.Parameters.AddWithValue(param.Nombre, param.ValorDato)
                Else
                    sqlParam = SqlCmd.Parameters.AddWithValue(param.Nombre, System.DBNull.Value)
                End If
                Select Case param.TipoDeDatos
                    Case TiposDeDatos.TipoDeDatoXML.Booleano
                        SqlCmd.Parameters(ContParametros).DbType = DbType.Boolean
                    Case TiposDeDatos.TipoDeDatoXML.Entero
                        SqlCmd.Parameters(ContParametros).DbType = DbType.Int32
                    Case TiposDeDatos.TipoDeDatoXML.Fecha
                        SqlCmd.Parameters(ContParametros).DbType = DbType.DateTime
                    Case TiposDeDatos.TipoDeDatoXML.Real
                        SqlCmd.Parameters(ContParametros).DbType = DbType.Double
                    Case TiposDeDatos.TipoDeDatoXML.Texto
                        SqlCmd.Parameters(ContParametros).DbType = DbType.String
                    Case Else
                        SqlCmd.Parameters(ContParametros).DbType = DbType.String
                End Select
                ContParametros = ContParametros + 1
            Next
            SqlCon.Open()
            SqlCmd.Connection = SqlCon
            SqlDa.SelectCommand = SqlCmd
            SqlDa.Fill(DsetResul)
        Else
            SqlCon.Open()
            SqlCmd.Connection = SqlCon
            SqlDa.SelectCommand = SqlCmd
            SqlDa.Fill(DsetResul)
        End If
        SqlCon.Close()
        SqlCmd.Dispose()
        SqlDa.Dispose()
        Return DsetResul
        DsetResul.Clear()
    End Function


#End Region
#Region " Parametros data access methods"
    ''' <summary>
    ''' Función que devuelve un texto concreto de la tabla PARGEN_LIT para el idioma requerido
    ''' </summary>
    ''' <param name="lId">Id del literal</param>
    ''' <param name="sIdi">Códgio de idioma</param>
    ''' <returns>Devuelve el texto concreto de la tabla PARGEN_LIT para el idioma requerido</returns>
    ''' <remarks>
    ''' Llamada desde: CParametros.CargarLiteralParametros
    ''' Tiempo máximo: 0 sec.
    ''' </remarks>
    <PrincipalPermission(SecurityAction.Demand, Authenticated:=True)> _
    Public Function Parametros_CargarLiteralParametros(ByVal lId As Long, ByVal sIdi As String) As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New DataSet

        Using cn
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "SELECT DEN FROM PARGEN_LIT WITH(NOLOCK) WHERE ID = " & lId & " AND IDI = '" & DblQuote(sIdi) & "'"
            cm.CommandType = CommandType.Text

            Dim da As New SqlDataAdapter
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Tables(0).Rows(0).Item(0).ToString
        End Using
    End Function

    ''' <summary>
    ''' Devuelve los valores de conexión al portal, así como el idioma y moneda por defecto
    ''' </summary>
    ''' <returns>Un DataRow con los valores indicados</returns>
    Public Function Parametros_Portal() As DataRow
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Using cn
            cm = New SqlCommand("FSWS_CONEXION_PORTAL", cn)
            cm.CommandType = CommandType.StoredProcedure
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter(cm)
            cn.Open()
            da.Fill(ds)
            Return ds.Tables(0).Rows(0)
        End Using
    End Function

    ' ''' <summary>
    ' ''' Cargar los parametros generales de la aplicacion
    ' ''' </summary>
    ' ''' <returns>Un Dataset con los parametros generales</returns>
    ' ''' <remarks>
    ' ''' Llamada desde: GSServer.ParametrosRule.CargarParametros
    ' ''' Tiempo mÃ¡ximo:< 1 sg</remarks>
    Public Function CargarParametrosGenerales() As System.Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_PARAMETROS_GENERALES"
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)
            cn.Close()
            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Cargar los parametros de integracion
    ''' </summary>
    ''' <param name="ParametrosIntegracion">Clase de parametros de integracion donde se devuelven</param>
    ''' <param name="ParametrosGenerales">Parametros Generales</param>
    ''' <returns>Object si se ha producido error</returns>
    ''' <remarks>
    ''' Llamada desde: GSServer.ParametrosRule.CargarParametros
    ''' Tiempo máximo:< 1 sg</remarks>
    ''' 
    Public Function CargarParametrosIntegracion(ByRef ParametrosIntegracion As GSServerModel.ParametrosIntegracion, ByVal ParametrosGenerales As GSServerModel.ParametrosGenerales) As Object

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet
        Dim sConsulta As String
        Dim iTablas As Integer
        Dim i As Integer
        Dim bActiva, bExportar, bSoloImportar, bSentidoEsNull As Boolean

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            sConsulta = "SELECT MAX(TABLA) AS NUM_TABLAS FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK)"
            cm = New SqlCommand(sConsulta, cn)
            'cm.ExecuteNonQuery()
            da = New SqlDataAdapter
            ds = New System.Data.DataSet
            cm.CommandType = System.Data.CommandType.Text
            da.SelectCommand = cm
            da.Fill(ds)

            If ds.Tables(0).Rows.Count > 0 Then
                iTablas = ds.Tables(0).Rows(0).Item(0)
            End If



            ReDim ParametrosIntegracion.gaExportar(0 To iTablas)
            ReDim ParametrosIntegracion.gaSoloImportar(0 To iTablas)
            For i = 1 To iTablas
                ParametrosIntegracion.gaExportar(i) = False
                ParametrosIntegracion.gaSoloImportar(i) = False
            Next
            ParametrosIntegracion.gbImportarPedAprov = False



            If ParametrosGenerales.gbIntegracion Then

                sConsulta = "SELECT DISTINCT TABLA_INTERMEDIA FROM TABLAS_INTEGRACION_ERP WITH (NOLOCK) WHERE TABLA=8 AND TABLA_INTERMEDIA=1"

                cm = New SqlCommand(sConsulta, cn)
                'cm.ExecuteNonQuery()
                da = New SqlDataAdapter
                ds = New System.Data.DataSet
                cm.CommandType = System.Data.CommandType.Text
                da.SelectCommand = cm
                da.Fill(ds)

                If ds.Tables(0).Rows.Count > 0 Then
                    ParametrosGenerales.gbActivarCodProveErp = ds.Tables(0).Rows(0).Item("TABLA_INTERMEDIA")
                End If

                For i = 1 To iTablas
                    bActiva = False
                    bExportar = False
                    bSoloImportar = True
                    bSentidoEsNull = True

                    sConsulta = "SELECT TABLA, ACTIVA, SENTIDO FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA =" & i & " ORDER BY ERP"

                    cm = New SqlCommand(sConsulta, cn)
                    'cm.ExecuteNonQuery()
                    da = New SqlDataAdapter
                    ds = New System.Data.DataSet
                    cm.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(ds)

                    If ds.Tables(0).Rows.Count > 0 Then

                        For j = 0 To ds.Tables(0).Rows.Count - 1

                            If Not IsDBNull(ds.Tables(0).Rows(0).Item("ACTIVA")) Then
                                bActiva = True
                            End If

                            If Not IsDBNull(ds.Tables(0).Rows(0).Item("SENTIDO")) Then

                                bSentidoEsNull = False
                                If (ds.Tables(0).Rows(0).Item("SENTIDO") <> Fullstep.FSNLibrary.TiposDeDatos.SentidoIntegracion.Entrada) Then
                                    bSoloImportar = False
                                    bExportar = True
                                End If


                                If ds.Tables(0).Rows(0).Item("TABLA") = EntidadIntegracion.PED_Aprov Then
                                    If ds.Tables(0).Rows(0).Item("SENTIDO") = SentidoIntegracion.Entrada Or ds.Tables(0).Rows(0).Item("SENTIDO") = SentidoIntegracion.EntradaSalida Then
                                        ParametrosIntegracion.gbImportarPedAprov = True
                                    End If
                                End If

                            End If

                        Next j
                    End If

                    If bActiva Then
                        If bSentidoEsNull Then
                            bSoloImportar = False
                            bExportar = False
                        End If
                        ParametrosIntegracion.gaSoloImportar(i) = bSoloImportar
                        ParametrosIntegracion.gaExportar(i) = bExportar
                    End If

                Next

            End If

            ds = Nothing
            Return 0
        Catch e As Exception
            Return 0
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    '<summary>
    'Cargar los parametros de instalacion para el usuario
    '</summary>
    '<param name="ParametrosInstalacion">Clase de parametros de instalacion donde se devuelven</param>
    '<param name="sUsu">usuario</param>
    '<returns>Object si se ha producido error</returns>
    '<remarks>
    'Llamada desde: GSServer.ParametrosRule.CargarParametros
    'Tiempo máximo:< 1 sg</remarks>

    Public Function CargarParametrosInstalacion(ByRef ParametrosInstalacion As GSServerModel.ParametrosInstalacion, ByVal sUsu As String) As DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_PARAMETROS_INSTALACION"
            cm.Parameters.Add("@USU", SqlDbType.NVarChar, 50)
            cm.Parameters("@USU").Value = sUsu
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)

            cn.Close()

            Return ds

        Catch e As Exception            
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    '<summary>Devuelve el literral indicado en el idioma indicado</summary>
    '<param name="ID">ID</param>
    '<param name="Idioma">Idioma</param>    
    '<returns>Dataset con el literal</returns>
    '<remarks>Llamada desde: GSServer.ParametrosRule.DevolverLiteral, Tiempo máximo: 0,3 seg</remarks>

    Public Function DevolverLiteral(ByVal ID As Integer, ByVal Idioma As String) As DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_PARGEN_LIT"
            cm.Parameters.Add("@ID", SqlDbType.TinyInt)
            cm.Parameters("@ID").Value = ID
            cm.Parameters.Add("@IDI", SqlDbType.VarChar, 50)
            cm.Parameters("@IDI").Value = Idioma
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)

            cn.Close()

            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    '<summary>Devuelve el literral indicado en el idioma indicado</summary>
    '<param name="ID">ID</param>
    '<param name="Idioma">Idioma</param>    
    '<returns>Dataset con el literal</returns>
    '<remarks>Llamada desde: GSServer.ParametrosRule.DevolverLiteral, Tiempo máximo: 0,3 seg</remarks>

    Public Function DevolverTextoWEBTEXT(ByVal Modulo As Integer, ByVal ID As Integer, ByVal Idioma As String) As DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_TEXTO_WEBTEXT"
            cm.Parameters.Add("@MODULO", SqlDbType.Int)
            cm.Parameters("@MODULO").Value = Modulo
            cm.Parameters.Add("@ID", SqlDbType.Int)
            cm.Parameters("@ID").Value = ID
            cm.Parameters.Add("@IDIOMA", SqlDbType.VarChar, 3)
            cm.Parameters("@IDIOMA").Value = Idioma
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)

            cn.Close()

            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    '<summary>
    'Cargar las longitudes de los codigos de la aplicacion
    '</summary>
    '<param name="LongitudesdeCodigo">Clase de longitudes de codigo donde se devuelven</param>
    '<returns>Object si se ha producido error</returns>
    '<remarks>
    'Llamada desde: GSServer.ParametrosRule.CargarParametros
    'Tiempo máximo:< 1 sg</remarks>

    Public Function CargarLongitudesDeCodigo(ByRef LongitudesDeCodigo As GSServerModel.LongitudesCodigo) As Object

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet
        Dim j As Integer

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_LONGITUDES_CODIGO"
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)

            If ds.Tables(0).Rows.Count > 0 Then
                For j = 0 To ds.Tables(0).Rows.Count - 1

                    Select Case ds.Tables(0).Rows(j).Item("NOMBRE")
                        'Case "ART"
                        '    LongitudesDeCodigo.giLongCodART = rs(0).Value
                        'Case "CAL"
                        '    LongitudesDeCodigo.giLongCodCAL = rs(0).Value
                        'Case "COM"
                        '    LongitudesDeCodigo.giLongCodCOM = rs(0).Value
                        '    'Case "CON"
                        '    '    LongitudesDeCodigo.giLongCodCON = rs(0).Value
                        'Case "DEP"
                        '    LongitudesDeCodigo.giLongCodDEP = rs(0).Value
                        'Case "DEST"
                        '    LongitudesDeCodigo.giLongCodDEST = rs(0).Value
                        'Case "EQP"
                        '    LongitudesDeCodigo.giLongCodEQP = rs(0).Value
                        'Case "GMN1"
                        '    LongitudesDeCodigo.giLongCodGMN1 = rs(0).Value
                        'Case "GMN2"
                        '    LongitudesDeCodigo.giLongCodGMN2 = rs(0).Value
                        'Case "GMN3"
                        '    LongitudesDeCodigo.giLongCodGMN3 = rs(0).Value
                        'Case "GMN4"
                        '    LongitudesDeCodigo.giLongCodGMN4 = rs(0).Value
                        Case "MON"
                            LongitudesDeCodigo.giLongCodMON = ds.Tables(0).Rows(j).Item("LONGITUD")
                            'Case "OFEEST"
                            '    LongitudesDeCodigo.giLongCodOFEEST = rs(0).Value
                        Case "PAG"
                            LongitudesDeCodigo.giLongCodPAG = ds.Tables(0).Rows(j).Item("LONGITUD")
                        Case "VIA_PAG"
                            LongitudesDeCodigo.giLongCodVIAPAG = ds.Tables(0).Rows(j).Item("LONGITUD")
                        Case "PAI"
                            LongitudesDeCodigo.giLongCodPAI = ds.Tables(0).Rows(j).Item("LONGITUD")
                            'Case "PER"
                            '    LongitudesDeCodigo.giLongCodPER = rs(0).Value
                            'Case "PERF"
                            '    LongitudesDeCodigo.giLongCodPERF = rs(0).Value
                            'Case "PRESCON1"
                            '    LongitudesDeCodigo.giLongCodPRESCON1 = rs(0).Value
                            'Case "PRESCON2"
                            '    LongitudesDeCodigo.giLongCodPRESCON2 = rs(0).Value
                            'Case "PRESCON3"
                            '    LongitudesDeCodigo.giLongCodPRESCON3 = rs(0).Value
                            'Case "PRESCON4"
                            '    LongitudesDeCodigo.giLongCodPRESCON4 = rs(0).Value
                            'Case "PRESPROY1"
                            '    LongitudesDeCodigo.giLongCodPRESPROY1 = rs(0).Value
                            'Case "PRESPROY2"
                            '    LongitudesDeCodigo.giLongCodPRESPROY2 = rs(0).Value
                            'Case "PRESPROY3"
                            '    LongitudesDeCodigo.giLongCodPRESPROY3 = rs(0).Value
                            'Case "PRESPROY4"
                            '    LongitudesDeCodigo.giLongCodPRESPROY4 = rs(0).Value
                            'Case "PROVE"
                            '    LongitudesDeCodigo.giLongCodPROVE = rs(0).Value
                        Case "PROVI"
                            LongitudesDeCodigo.giLongCodPROVI = ds.Tables(0).Rows(j).Item("LONGITUD")
                            'Case "ROL"
                            '    LongitudesDeCodigo.giLongCodROL = rs(0).Value
                        Case "UNI"
                            LongitudesDeCodigo.giLongCodUNI = ds.Tables(0).Rows(j).Item("LONGITUD")
                            'Case "UON1"
                            '    LongitudesDeCodigo.giLongCodUON1 = rs(0).Value
                            'Case "UON2"
                            '    LongitudesDeCodigo.giLongCodUON2 = rs(0).Value
                            'Case "UON3"
                            '    LongitudesDeCodigo.giLongCodUON3 = rs(0).Value
                            'Case "UON4"
                            '    LongitudesDeCodigo.giLongCodUON4 = rs(0).Value
                            'Case "USU"
                            '    LongitudesDeCodigo.giLongCodUSU = rs(0).Value
                            'Case "ACT"
                            '    LongitudesDeCodigo.giLongCodACT1 = rs(0).Value
                            'Case "SUBACT1"
                            '    LongitudesDeCodigo.giLongCodACT2 = rs(0).Value
                            'Case "SUBACT2"
                            '    LongitudesDeCodigo.giLongCodACT3 = rs(0).Value
                            'Case "SUBACT3"
                            '    LongitudesDeCodigo.giLongCodACT4 = rs(0).Value
                            'Case "SUBACT4"
                            '    LongitudesDeCodigo.giLongCodACT5 = rs(0).Value
                            'Case "PRESCONCEP31"
                            '    LongitudesDeCodigo.giLongCodPRESCONCEP31 = rs(0).Value
                            'Case "PRESCONCEP32"
                            '    LongitudesDeCodigo.giLongCodPRESCONCEP32 = rs(0).Value
                            'Case "PRESCONCEP33"
                            '    LongitudesDeCodigo.giLongCodPRESCONCEP33 = rs(0).Value
                            'Case "PRESCONCEP34"
                            '    LongitudesDeCodigo.giLongCodPRESCONCEP34 = rs(0).Value
                            'Case "PRESCONCEP41"
                            '    LongitudesDeCodigo.giLongCodPRESCONCEP41 = rs(0).Value
                            'Case "PRESCONCEP42"
                            '    LongitudesDeCodigo.giLongCodPRESCONCEP42 = rs(0).Value
                            'Case "PRESCONCEP43"
                            '    LongitudesDeCodigo.giLongCodPRESCONCEP43 = rs(0).Value
                            'Case "PRESCONCEP44"
                            '    LongitudesDeCodigo.giLongCodPRESCONCEP44 = rs(0).Value
                            'Case "CAT1"
                            '    LongitudesDeCodigo.giLongCodCAT1 = rs(0).Value
                            'Case "CAT2"
                            '    LongitudesDeCodigo.giLongCodCAT2 = rs(0).Value
                            'Case "CAT3"
                            '    LongitudesDeCodigo.giLongCodCAT3 = rs(0).Value
                            'Case "CAT4"
                            '    LongitudesDeCodigo.giLongCodCAT4 = rs(0).Value
                            'Case "CAT5"
                            '    LongitudesDeCodigo.giLongCodCAT5 = rs(0).Value
                            'Case "GRUPO"
                            '    LongitudesDeCodigo.giLongCodGRUPOPROCE = rs(0).Value
                            'Case "DIC"

                            'Case "CONTR"

                            'Case "VARCAL"
                            '    LongitudesDeCodigo.giLongCodVarCal = rs(0).Value
                            'Case "DENART"
                            '    LongitudesDeCodigo.giLongCodDENART = rs(0).Value
                            'Case "PRES5_0"
                            '    LongitudesDeCodigo.giLongCodPres5Niv0 = rs(0).Value
                            'Case "PRES5_1"
                            '    LongitudesDeCodigo.giLongCodPres5Niv1 = rs(0).Value
                            'Case "PRES5_2"
                            '    LongitudesDeCodigo.giLongCodPres5Niv2 = rs(0).Value
                            'Case "PRES5_3"
                            '    LongitudesDeCodigo.giLongCodPres5Niv3 = rs(0).Value
                            'Case "PRES5_4"
                            '    LongitudesDeCodigo.giLongCodPres5Niv4 = rs(0).Value


                    End Select



                Next
            End If

            cn.Close()
            ds = Nothing
            Return 0
        Catch e As Exception
            Return 0
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region

#Region " Raiz Data access methods "
    Public Function Root_DevolverCentrosCoste() As DataSet
        Dim adoRs As New DataSet
        Dim SQL As String
        Dim Cn As New SqlConnection(mDBConnection)
        Dim sqlCmd As New SqlCommand
        Dim da As New SqlDataAdapter
        'On Error Resume Next

        SQL = "SELECT * FROM CENTROS_COSTE"
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = SQL
        sqlCmd.Connection = Cn
        Cn.Open()
        da.SelectCommand = sqlCmd
        da.Fill(adoRs)
        Cn.Close()
        da.Dispose()
        sqlCmd.Dispose()
        Return adoRs
        adoRs.Clear()

    End Function
#End Region

#Region " Monedas data access methods "

    ''' <summary>
    ''' Cargar las monedas de la aplicacion
    ''' </summary>
    ''' <param name="bMostrarIntegracion">Si se muestra el estado de la integracion</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <returns>Un Dataset con las monedas </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.MonedasRule.CargarTodasLasMonedas
    ''' Tiempo máximo<1 sg</remarks>
    Public Function CargarTodasLasMonedas(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_MONEDAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@MOSTRARINTEGRACION", SqlDbType.TinyInt)
            cm.Parameters("@MOSTRARINTEGRACION").Value = IIf(bMostrarIntegracion, 1, 0)
            cm.Parameters.Add("@ENTIDADINTEGRACION", SqlDbType.Int)
            cm.Parameters("@ENTIDADINTEGRACION").Value = EntidadIntegracion.Mon
            cm.Parameters.Add("@ACCION", SqlDbType.NVarChar, 1)
            cm.Parameters("@ACCION").Value = Accion_CambioCodigo
            cm.Parameters.Add("@IDIOMA", SqlDbType.NVarChar, 3)
            cm.Parameters("@IDIOMA").Value = sIdioma
            da.SelectCommand = cm
            da.Fill(ds)

            cn.Close()

            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    '' <summary>
    '' Cargar las monedas hasta un numero maximo  desde un codigo o una denominacion determinadas
    '' </summary>
    '' <param name="sIdioma">Idioma de la aplicación</param>
    '' <param name="NumMaximo">Número máximo de monedas a cargar</param>
    '' <param name="CaracteresInicialesCod">Código de los caracteres iniciales de la moneda</param>
    '' <param name="CaracteresInicialesDen">Denominación de los carácteres iniciales de la moneda</param>
    '' <param name="OrdenadosPorDen">Ordenación de la moneda</param>
    '' <param name="IncluirEqNoAct">Indica si se debe indicar la equivalencia de monedas</param>
    '' <param name="CoincidenciaTotal">Inidica si debe haber coincidencia total en los códigos de moneda</param>
    '' <returns>Un Dataset con las monedas posibles del articulo y sus atributos</returns>
    '' <remarks>
    '' Llamada desde: La clase CMoneda método CargarTodasLasMonedasDesde
    '' Tiempo máximo:0,36 sg</remarks>
    '    <PrincipalPermission(SecurityAction.Demand, Authenticated:=True)> _
    'Public Function Monedas_CargarTodasLasMonedasDesde(ByVal sIdioma As String, ByVal NumMaximo As Short, ByVal CaracteresInicialesCod As String, ByVal CaracteresInicialesDen As String, ByVal OrdenadosPorDen As Boolean, ByVal IncluirEqNoAct As Boolean, ByVal CoincidenciaTotal As Boolean) As DataSet
    '        Dim ador As New DataSet
    '        Dim sConsulta As String
    '        Dim sPrimeraCond As String
    '        Dim sPrimerOrden As String
    '        Dim cn As New SqlConnection(mDBConnection)
    '        ador = New DataSet

    '        If IncluirEqNoAct = False Then
    '            sPrimeraCond = " EQ_ACT=1 AND "
    '            sPrimerOrden = "EQ_ACT,"
    '        Else
    '            sPrimeraCond = ""
    '            sPrimerOrden = "EQ_ACT,"
    '        End If

    '        If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then

    '            If IncluirEqNoAct = False Then
    '                sConsulta = "SELECT COD, DEN_" & sIdioma & " AS DEN, EQUIV, EQ_ACT, FECACT, FSP_COD FROM MON WHERE EQ_ACT=1"
    '            Else
    '                sConsulta = "SELECT COD, DEN_" & sIdioma & " AS DEN, EQUIV, EQ_ACT, FECACT, FSP_COD FROM MON AS MON"
    '            End If


    '        Else

    '            If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
    '                If CoincidenciaTotal Then
    '                    sConsulta = "SELECT COD, DEN_" & sIdioma & " AS DEN, EQUIV, EQ_ACT, FECACT, FSP_COD FROM MON WHERE" & sPrimeraCond
    '                    sConsulta = sConsulta & " COD ='" & DblQuote(CaracteresInicialesCod) & "'"
    '                    sConsulta = sConsulta & " AND DEN_" & sIdioma & " ='" & DblQuote(CaracteresInicialesDen) & "'"
    '                Else
    '                    sConsulta = "SELECT COD, DEN_" & sIdioma & " AS DEN, EQUIV, EQ_ACT, FECACT, FSP_COD FROM MON WHERE" & sPrimeraCond
    '                    sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
    '                    sConsulta = sConsulta & " AND DEN_" & sIdioma & " >='" & DblQuote(CaracteresInicialesDen) & "'"
    '                End If
    '            Else

    '                If Not (CaracteresInicialesCod = "") Then
    '                    If CoincidenciaTotal Then
    '                        sConsulta = "SELECT COD, DEN_" & sIdioma & " AS DEN, EQUIV, EQ_ACT, FECACT, FSP_COD FROM MON WHERE" & sPrimeraCond
    '                        sConsulta = sConsulta & " COD ='" & DblQuote(CaracteresInicialesCod) & "'"
    '                    Else
    '                        sConsulta = "SELECT COD, DEN_" & sIdioma & " AS DEN, EQUIV, EQ_ACT, FECACT, FSP_COD FROM MON WHERE" & sPrimeraCond
    '                        sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
    '                    End If
    '                Else
    '                    If CoincidenciaTotal Then
    '                        sConsulta = "SELECT COD, DEN_" & sIdioma & " AS DEN, EQUIV, EQ_ACT, FECACT, FSP_COD FROM MON WHERE" & sPrimeraCond
    '                        sConsulta = sConsulta & " DEN_" & sIdioma & " ='" & DblQuote(CaracteresInicialesDen) & "'"
    '                    Else
    '                        sConsulta = "SELECT COD, DEN_" & sIdioma & " AS DEN, EQUIV, EQ_ACT, FECACT, FSP_COD FROM MON WHERE" & sPrimeraCond
    '                        sConsulta = sConsulta & " DEN_" & sIdioma & " >='" & DblQuote(CaracteresInicialesDen) & "'"
    '                    End If
    '                End If

    '            End If

    '        End If

    '        If OrdenadosPorDen Then
    '            sConsulta = sConsulta & " ORDER BY " & sPrimerOrden & "DEN_" & sIdioma
    '        Else
    '            sConsulta = sConsulta & " ORDER BY " & sPrimerOrden & "COD"
    '        End If

    '        ' Crear el Resultset
    '        Dim sqlcmd As New SqlCommand
    '        sqlcmd.Connection = cn
    '        cn.Open()
    '        sqlcmd.CommandText = sConsulta
    '        sqlcmd.CommandType = CommandType.Text
    '        Dim da As New SqlDataAdapter
    '        da.SelectCommand = sqlcmd
    '        da.Fill(ador)

    '        If ador.Tables.Count = 0 Then
    '            sqlcmd.Dispose()
    '            da.Dispose()
    '            cn.Close()
    '            Return ador
    '            Exit Function

    '        Else
    '            cn.Close()
    '            sqlcmd.Dispose()
    '            da.Dispose()
    '            Return ador
    '            ador.Clear()
    '        End If

    '    End Function

    '<summary>Devuelve los datos de una moneda</summary>
    '<param name="Moneda">Código de la moneda</param>
    '<param name="Idioma">Código del idioma de la denominación</param>
    '<returns>Dataset con los datos del proceso</returns>    

    Public Function DevolverMoneda(ByVal Moneda As String, ByVal Idioma As String) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_MONEDA"
        With cm.Parameters
            .Add("@COD", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD").Value = Moneda
            .Add("@IDIOMA", SqlDbType.VarChar, 50)
            cm.Parameters("@IDIOMA").Value = Idioma
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Actualiza los cambios realizados en alguna moneda
    ''' </summary>
    ''' <param name="Moneda">Moneda en la que se ha producido el cambio</param>
    ''' <param name="bRealizarCambioCodigo">Si hay que hacer cambio de codigo</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.MonedasRule.ActualizarMoneda
    ''' Tiempo máximo<1 sg</remarks>
    Public Function ActualizarMoneda(ByRef Moneda As GSServerModel.Moneda, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException

        ''Nota: Cuando se accede a BD de portal no se usa with (nolock) pq da problemas
        Dim sCodigo As String
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""
        Dim lIdLogMON As Long

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN DISTRIBUTED TRAN"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()
        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandText = "SET XACT_ABORT ON"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()
        'Exc = New Exception
        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandType = System.Data.CommandType.Text

        Dim monden As New GSServerModel.Multiidioma(True, "", "", True)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        sCodigo = DblQuote(Moneda.Codigo)
        sConsulta = "SELECT FECACT FROM MON WITH(NOLOCK) WHERE COD=N'" & sCodigo & "'"
        adoCom.CommandText = sConsulta

        da = New SqlDataAdapter
        dr = New System.Data.DataSet
        da.SelectCommand = adoCom
        da.Fill(dr)

        If dr.Tables(0).Rows.Count > 0 Then

            If Moneda.FecAct <> dr.Tables(0).Rows(0).Item("FECACT") Then
                cn.Close()
                oError.Number = ErroresGS.TESInfActualModificada
                Return oError
                Exit Function
            End If
        Else
            oError.Number = ErroresGS.TESDatoEliminado
            oError.ErrorId = 6

        End If



        Try
            If Not bRealizarCambioCodigo Then

                sConsulta = ""
                For Each monden In Moneda.Denominaciones
                    sConsulta = sConsulta & " UPDATE MON_DEN SET DEN=N'" & DblQuote(monden.Denominacion) & "' WHERE MON =N'" & sCodigo & "' AND IDIOMA='" & monden.Idioma & "' "
                Next
                sConsulta = sConsulta & " UPDATE MON SET EQUIV=@EQUIV WHERE COD=N'" & Moneda.Codigo & "' "

                'cm = New SqlCommand(sConsulta, cn)
                adoCom.CommandText = sConsulta
                Dim sqlPar As New SqlParameter("@EQUIV", SqlDbType.Float)
                sqlPar.Value = Moneda.Equivalencia
                adoCom.Parameters.Add(sqlPar)
                adoCom.ExecuteNonQuery()


                If iInstWeb = Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb.ConPortal Then

                    sConsulta = "DECLARE @IDMON INTEGER "
                    sConsulta = sConsulta & " DECLARE @ID INTEGER "
                    sConsulta = sConsulta & " SELECT @IDMON=ID FROM " & FSP & "MON WHERE COD= N'" & sCodigo & "' "

                    For Each monden In Moneda.Denominaciones
                        sConsulta = sConsulta & " SELECT @ID=ID FROM " & FSP & "IDI WHERE COD=N'" & monden.Idioma & "'"
                        sConsulta = sConsulta & " UPDATE " & FSP & "MON_DEN SET "
                        sConsulta = sConsulta & " DEN=N'" & DblQuote(monden.Denominacion) & "' WHERE MON = @IDMON AND IDIOMA=@ID"
                    Next


                    sConsulta = sConsulta & " UPDATE " & FSP & "MON SET EQUIV=@EQUIV WHERE COD=N'" & sCodigo & "' "

                    adoCom = New SqlCommand
                    adoCom.Connection = cn
                    adoCom.CommandType = System.Data.CommandType.Text
                    adoCom.CommandText = sConsulta
                    Dim sqlPar2 As New SqlParameter("@EQUIV", SqlDbType.Float)
                    sqlPar2.Value = Moneda.Equivalencia
                    adoCom.Parameters.Add(sqlPar2)
                    adoCom.ExecuteNonQuery()

                End If


                If bGrabarLog Then

                    sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                    adoCom = New SqlCommand(sConsulta, cn)
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    adoCom.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = adoCom
                    da.Fill(dr)


                    Dim da2 = New SqlDataAdapter
                    Dim dr2 = New System.Data.DataSet

                    If dr.Tables(0).Rows.Count > 0 Then
                        For i = 0 To dr.Tables(0).Rows.Count - 1
                            sConsulta = "INSERT INTO LOG_MON (ACCION, COD, EQUIV, ORIGEN, USU, ERP) VALUES ('" & Accion_Modificacion & "',N'" & sCodigo & "'"
                            sConsulta = sConsulta & "," & DblToSQLFloat(Moneda.Equivalencia) & "," & Moneda.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "')"

                            adoCom.CommandText = sConsulta
                            adoCom.CommandType = System.Data.CommandType.Text
                            adoCom.ExecuteNonQuery()


                            da2 = New SqlDataAdapter
                            dr2 = New System.Data.DataSet

                            adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_MON ", cn)
                            adoCom.CommandType = System.Data.CommandType.Text
                            da2.SelectCommand = adoCom
                            da2.Fill(dr2)

                            If dr2.Tables(0).Rows.Count > 0 Then

                                lIdLogMON = dr2.Tables(0).Rows(0).Item(0)

                                For Each monden In Moneda.Denominaciones

                                    sConsulta = "INSERT INTO LOG_MON_DEN (ID_LOG_MON,DEN,IDI) VALUES (" & lIdLogMON & ",N'" & DblQuote(monden.Denominacion) & " ','" & DblQuote(monden.Idioma) & "')"
                                    adoCom = New SqlCommand(sConsulta, cn)
                                    adoCom.Connection = cn
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    adoCom.ExecuteNonQuery()

                                Next


                            End If


                        Next

                    End If
                End If
            End If
            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.CommandText = "COMMIT TRANSACTION"
            adoCom.ExecuteNonQuery()

            sConsulta = "SELECT FECACT FROM MON WITH(NOLOCK) WHERE COD =N'" & sCodigo & "'"
            cm = New SqlCommand(sConsulta, cn)
            'cm.ExecuteNonQuery()
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            cm.CommandType = System.Data.CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                Moneda.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If

            cn.Close()

        Catch ex As SqlException

            oError = TratarError(ex)

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            cn.Close()

        End Try

        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCMonActCamb, "Cod: " & Moneda.Codigo)
        End If
        Return oError

    End Function

    ''' <summary>
    ''' Añade una moneda a la BD
    ''' </summary>
    ''' <param name="Moneda">Moneda en la que se ha producido el cambio</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.MonedasRule.AnyadirMoneda
    ''' Tiempo máximo<1 sg</remarks>
    Public Function AnyadirMoneda(ByRef Moneda As GSServerModel.Moneda, ByVal iInstWeb As Integer, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException

        ''Nota: Cuando se accede a BD de portal no se usa with (nolock) pq da problemas
        Dim sCodigo As String
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim lIdLogMON As Long

        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN DISTRIBUTED TRAN"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()
        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandText = "SET XACT_ABORT ON"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()
        'Exc = New Exception
        'adoCom = New SqlCommand
        'adoCom.Connection = cn


        Dim monden As New GSServerModel.Multiidioma(True, "", "", True)
        Dim sqlPar As New SqlParameter("@EQUIV", SqlDbType.Float)
        Dim sqlPar2 As New SqlParameter("@EQUIV", SqlDbType.Float)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        sCodigo = DblQuote(Moneda.Codigo)

        Try
            sConsulta = " INSERT INTO MON (COD,EQUIV) VALUES (N'" & sCodigo & "',@EQUIV) "

            For Each monden In Moneda.Denominaciones
                sConsulta = sConsulta & " INSERT INTO MON_DEN (MON,IDIOMA,DEN) VALUES ( N'" & sCodigo & "','" & monden.Idioma & "',N'" & DblQuote(monden.Denominacion) & "') "
            Next

            sqlPar.Value = Moneda.Equivalencia
            adoCom.Parameters.Add(sqlPar)
            adoCom.CommandText = sConsulta
            adoCom.ExecuteNonQuery()


            sConsulta = "SELECT FECACT FROM MON WITH(NOLOCK) WHERE COD =N'" & sCodigo & "'"
            adoCom = New SqlCommand(sConsulta, cn)
            'cm.ExecuteNonQuery()
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            adoCom.CommandType = System.Data.CommandType.Text
            da.SelectCommand = adoCom
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                Moneda.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If

            Dim id As Integer


            If iInstWeb = Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb.ConPortal Then

                sConsulta = "SELECT MAX(ID) FROM " & FSP & "MON "


                adoCom.CommandText = sConsulta
                da = New SqlDataAdapter
                dr = New System.Data.DataSet
                adoCom.CommandType = System.Data.CommandType.Text
                da.SelectCommand = adoCom
                da.Fill(dr)

                If dr.Tables(0).Rows.Count > 0 Then
                    id = dr.Tables(0).Rows(0).Item(0) + 1
                Else
                    id = 1
                End If

                sConsulta = " DECLARE @ID INTEGER "
                sConsulta = sConsulta & " DECLARE @IDMON INTEGER "

                sConsulta = sConsulta & " SELECT @IDMON=MAX(ID) FROM " & FSP & "MON"
                sConsulta = sConsulta & " IF @IDMON IS NULL "
                sConsulta = sConsulta & " SET @IDMON=1 "
                sConsulta = sConsulta & " Else "
                sConsulta = sConsulta & " SET @IDMON=@IDMON+1 "

                sConsulta = sConsulta & " INSERT INTO " & FSP & "MON (ID,COD,EQUIV) VALUES (@IDMON,N'" & sCodigo & "',@EQUIV) "
                'sConsulta = sConsulta & " SELECT @IDMON=SCOPE_IDENTITY() "
                For Each monden In Moneda.Denominaciones
                    sConsulta = sConsulta & " SELECT @ID=ID FROM " & FSP & "IDI WHERE COD=N'" & monden.Idioma & "'"
                    sConsulta = sConsulta & " INSERT INTO " & FSP & "MON_DEN (MON,IDIOMA,DEN) VALUES (@IDMON,@ID,N'" & DblQuote(monden.Denominacion) & "') "
                Next

                adoCom = New SqlCommand(sConsulta, cn)
                sqlPar2.Value = Moneda.Equivalencia
                adoCom.Parameters.Add(sqlPar2)
                adoCom.ExecuteNonQuery()

            End If


            If bGrabarLog Then

                sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                adoCom = New SqlCommand(sConsulta, cn)
                da = New SqlDataAdapter
                dr = New System.Data.DataSet
                adoCom.CommandType = System.Data.CommandType.Text
                da.SelectCommand = adoCom
                da.Fill(dr)


                Dim da2 = New SqlDataAdapter
                Dim dr2 = New System.Data.DataSet

                If dr.Tables(0).Rows.Count > 0 Then
                    For i = 0 To dr.Tables(0).Rows.Count - 1
                        sConsulta = "INSERT INTO LOG_MON (ACCION, COD, EQUIV, ORIGEN, USU, ERP) VALUES ('" & Accion_Alta & "',N'" & sCodigo & "'"
                        sConsulta = sConsulta & "," & DblToSQLFloat(Moneda.Equivalencia) & "," & Moneda.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "')"

                        adoCom.CommandText = sConsulta
                        adoCom.CommandType = System.Data.CommandType.Text
                        adoCom.ExecuteNonQuery()


                        da2 = New SqlDataAdapter
                        dr2 = New System.Data.DataSet

                        adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_MON ", cn)
                        adoCom.CommandType = System.Data.CommandType.Text
                        da2.SelectCommand = adoCom
                        da2.Fill(dr2)

                        If dr2.Tables(0).Rows.Count > 0 Then

                            lIdLogMON = dr2.Tables(0).Rows(0).Item(0)

                            For Each monden In Moneda.Denominaciones

                                sConsulta = "INSERT INTO LOG_MON_DEN (ID_LOG_MON,DEN,IDI) VALUES (" & lIdLogMON & ","
                                sConsulta = sConsulta & "', N" & DblQuote(monden.Denominacion) & "','" & DblQuote(monden.Idioma) & "')"
                                adoCom = New SqlCommand(sConsulta, cn)
                                adoCom.Connection = cn
                                adoCom.CommandType = System.Data.CommandType.Text
                                adoCom.ExecuteNonQuery()

                            Next


                        End If


                    Next


                End If
            End If


            adoCom = New SqlCommand()
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            adoCom = New SqlCommand("COMMIT TRANSACTION", cn)
            adoCom.ExecuteNonQuery()

            cn.Close()


        Catch ex As SqlException

            oError = TratarError(ex)
            adoCom = New SqlCommand()
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            cn.Close()

        End Try
        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCMonAnya, "Cod: " & Moneda.Codigo)
        End If

        Return oError

    End Function

    ''' <summary>
    ''' Elimina monedas de la BD
    ''' </summary>
    ''' <param name="Codigos">Codigos de las monedas a eliminar</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sMonedaCentral">Moneda central de la aplicacion</param>
    ''' <param name="Origen">Origen de integracion</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.MonedasRule.EliminarMoneda
    ''' Tiempo máximo<1 sg</remarks>
    Public Function EliminarMonedas(ByVal Codigos() As String, ByVal iInstWeb As Integer, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sMonedaCentral As String, ByVal Origen As TiposDeDatos.OrigenIntegracion, ByVal sUsuario As String) As GSServerModel.GSException
        ''Nota: Cuando se accede a BD de portal no se usa with (nolock) pq da problemas
        Dim i, j, k As Integer
        Dim bGuardar As Boolean
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim sCodMon As String
        Dim iRes As Integer
        Dim ContRegBorrados As Integer

        Dim ds As DataSet
        Dim oIdiomas As New GSServerModel.Idiomas(True, "", "", True)
        Dim oIdioma As GSServerModel.Idioma
        Dim dblEquiv As Double
        Dim lIdLogMON As Long

        Dim oDens As New GSServerModel.Multiidiomas(False, "", "", True)
        Dim oDen As New GSServerModel.Multiidioma(False, "", "", True)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror

        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        Try
            adoCom = New SqlCommand
            cn.Open()
            adoCom.Connection = cn
            adoCom.CommandText = "BEGIN DISTRIBUTED TRAN "
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()
            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT ON"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()


            For i = 0 To Codigos.Count - 1
                bGuardar = True
                sCodMon = DblQuote(Codigos(i).ToString())


                'Exc = New Exception
                adoCom = New SqlCommand
                adoCom.Connection = cn


                'Va comprobando que la moneda no se esté usando en las provincias,destinos,etc...Se hace así
                'en lugar de provocar el error porque sino no funciona la transacción distribuida:
                'RELACIONADA CON LA TABLA DE PAISES
                If bGuardar = True Then
                    sConsulta = "SELECT PAI.COD, PAI_DEN.DEN FROM PAI WITH (NOLOCK) LEFT JOIN PAI_DEN WITH (NOLOCK) ON PAI.COD=PAI_DEN.PAI WHERE PAI.MON=N'" & sCodMon & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    'cm.ExecuteNonQuery()
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    cm.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(dr)


                    If dr.Tables(0).Rows.Count > 0 Then
                        oError.Errores.Add(Codigos(i), 65, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1))
                        iRes = iRes + 1
                        bGuardar = False
                    End If

                End If

                ' ESTA RELACIONADA CON LA TABLA DE PROCESOS
                If bGuardar = True Then
                    sConsulta = "SELECT PROCE.ANYO, PROCE.COD, PROCE.DEN FROM Proce WITH (NOLOCK) WHERE PROCE.MON=N'" & sCodMon & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    'cm.ExecuteNonQuery()
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    cm.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(dr)

                    If dr.Tables(0).Rows.Count > 0 Then
                        oError.Errores.Add(Codigos(i), 82, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1) & "-" & dr.Tables(0).Rows(0).Item(2))
                        iRes = iRes + 1
                        bGuardar = False

                    End If
                End If

                ' ESTA RELACIONADA CON LA TABLA PROVE
                If bGuardar = True Then
                    sConsulta = "SELECT PROVE.COD, PROVE.DEN FROM PROVE WITH (NOLOCK) WHERE PROVE.MON=N'" & sCodMon & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    'cm.ExecuteNonQuery()
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    cm.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(dr)

                    If dr.Tables(0).Rows.Count > 0 Then
                        oError.Errores.Add(Codigos(i), 90, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1))
                        iRes = iRes + 1
                        bGuardar = False

                    End If
                End If

                ' ESTA RELACIONADA CON LA TABLA PARGEN DEF
                If bGuardar = True Then
                    sConsulta = "SELECT PARGEN_DEF.ID FROM PARGEN_DEF WITH (NOLOCK) WHERE PARGEN_DEF.MONCEN=N'" & sCodMon & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    'cm.ExecuteNonQuery()
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    cm.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(dr)
                    If dr.Tables(0).Rows.Count > 0 Then
                        oError.Errores.Add(Codigos(i), 66, i, dr.Tables(0).Rows(0).Item(0))
                        iRes = iRes + 1
                        bGuardar = False

                    End If
                End If

                ' ESTA RELACIONADA CON LA TABLA PROCE_OFE
                If bGuardar = True Then
                    sConsulta = "SELECT PROCE_OFE.PROCE, PROCE_OFE.PROVE, PROCE_OFE.OFE FROM PROCE_OFE WITH (NOLOCK) WHERE PROCE_OFE.MON=N'" & sCodMon & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    cm.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(dr)
                    If dr.Tables(0).Rows.Count > 0 Then

                        oError.Errores.Add(Codigos(i), 84, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1) & "-" & dr.Tables(0).Rows(0).Item(2))
                        iRes = iRes + 1
                        bGuardar = False

                    End If
                End If

                ' ESTA RELACIONADA CON LA TABLA ORDEN_ENTREGA
                If bGuardar = True Then
                    sConsulta = "SELECT ORDEN_ENTREGA.ANYO,ORDEN_ENTREGA.NUM, ORDEN_ENTREGA.PROVE FROM ORDEN_ENTREGA WITH (NOLOCK) WHERE ORDEN_ENTREGA.MON=N'" & sCodMon & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    'cm.ExecuteNonQuery()
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    cm.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(dr)

                    If dr.Tables(0).Rows.Count > 0 Then

                        oError.Errores.Add(Codigos(i), 130, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1) & "-" & dr.Tables(0).Rows(0).Item(2))
                        iRes = iRes + 1
                        bGuardar = False

                    End If
                End If


                'ALMACENAMOS EN BASE DE DATOS:
                If bGuardar = True Then
                    If bGrabarLog Then ' primero recuperamos los datos de la moneda que se va a borra

                        ds = CargarTodosLosIdiomas()

                        For Each fila As DataRow In ds.Tables(0).Rows
                            'He modificado los valores que estaban vacios por sus valores por defecto para quitar la clausula Optional del metodo que lo llama
                            oIdioma = oIdiomas.Add(fila.Item("COD"), fila.Item("DEN"))
                        Next


                        sConsulta = "SELECT EQUIV FROM MON WITH (NOLOCK) WHERE COD =N'" & sCodMon & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)

                        dblEquiv = dr.Tables(0).Rows(0).Item("EQUIV")


                        sConsulta = "SELECT IDIOMA, DEN FROM MON_DEN WITH(NOLOCK) WHERE MON =N'" & sCodMon & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)
                        oDens.Clear()
                        If dr.Tables(0).Rows.Count > 0 Then
                            For j = 0 To dr.Tables(0).Rows.Count - 1
                                oDens.Add(dr.Tables(0).Rows(j).Item("IDIOMA"), dr.Tables(0).Rows(j).Item("DEN"), Now)
                            Next
                        End If


                    End If

                    'MultiERP: Elimina de MON_ERP

                    sConsulta = "DELETE FROM MON_ERP WHERE COD_GS=N'" & sCodMon & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    ContRegBorrados = cm.ExecuteNonQuery()

                    sConsulta = "DELETE FROM MON_DEN WHERE MON=N'" & sCodMon & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    ContRegBorrados = cm.ExecuteNonQuery()

                    sConsulta = "DELETE FROM MON WHERE COD=N'" & sCodMon & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    ContRegBorrados = cm.ExecuteNonQuery()


                    sConsulta = "UPDATE COPIA_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & sCodMon & "' AND COPIA_CAMPO_DEF IN (SELECT ID FROM FORM_CAMPO WHERE TIPO_CAMPO_GS=" & TipoCampoGS.Moneda & ")"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    sConsulta = "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT='" & sCodMon & "' AND CAMPO_HIJO IN (SELECT COPIA_CAMPO.ID FROM COPIA_CAMPO INNER JOIN FORM_CAMPO ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Moneda & ")"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    'elimina de los formularios:
                    sConsulta = "UPDATE FORM_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & sCodMon & "' AND TIPO_CAMPO_GS=" & TipoCampoGS.Moneda
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()

                    sConsulta = "UPDATE LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & sCodMon & "' AND CAMPO_HIJO IN (SELECT ID FROM FORM_CAMPO WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Moneda & ")"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    'Elimina de las solicitudes favoritas los campos con la Moneda
                    sConsulta = "DELETE FROM PM_CAMPO_FAV FROM PM_CAMPO_FAV CF"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = CF.CAMPO "
                    sConsulta = sConsulta & " WHERE CF.VALOR_TEXT=N'" & sCodMon & "' AND FC.TIPO_CAMPO_GS=102"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    'Elimina de las solicitudes favoritas las lineas de desglose con la Moneda
                    sConsulta = "DELETE FROM PM_LINEA_DESGLOSE_FAV FROM PM_LINEA_DESGLOSE_FAV LDF"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = LDF.CAMPO_HIJO"
                    sConsulta = sConsulta & " WHERE LDF.VALOR_TEXT=N'" & sCodMon & "' AND FC.TIPO_CAMPO_GS=102"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    'Elimina de las solicitudes favoritas los campos con la Moneda
                    sConsulta = "DELETE FROM PM_CAMPO_FAV FROM PM_CAMPO_FAV CF"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = CF.CAMPO "
                    sConsulta = sConsulta & " WHERE CF.VALOR_TEXT='" & DblQuote(sCodMon) & "' AND FC.TIPO_CAMPO_GS=102"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    'Elimina de las solicitudes favoritas las lineas de desglose con la Moneda
                    sConsulta = "DELETE FROM PM_LINEA_DESGLOSE_FAV FROM PM_LINEA_DESGLOSE_FAV LDF"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = LDF.CAMPO_HIJO"
                    sConsulta = sConsulta & " WHERE LDF.VALOR_TEXT='" & DblQuote(sCodMon) & "' AND FC.TIPO_CAMPO_GS=102"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    'Elimina la moneda correspondiente en el portal:
                    If iInstWeb = TipoInstWeb.ConPortal Then

                        cm = New SqlCommand
                        cm.Connection = cn
                        cm.CommandText = FSP & "SP_ELIM_MON"
                        cm.CommandType = CommandType.StoredProcedure

                        cm.CommandType = CommandType.StoredProcedure
                        cm.Parameters.Add("@MON_COD", SqlDbType.NVarChar, 50)
                        cm.Parameters("@MON_COD").Value = Codigos(i)
                        cm.Parameters.Add("@CODMONCENTRAL", SqlDbType.NVarChar, 50)
                        cm.Parameters("@CODMONCENTRAL").Value = sMonedaCentral


                        cm.ExecuteNonQuery()



                    End If



                    If bGrabarLog And ContRegBorrados > 0 Then

                        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                        adoCom = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        adoCom.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = adoCom
                        da.Fill(dr)


                        Dim da2 = New SqlDataAdapter
                        Dim dr2 = New System.Data.DataSet

                        If dr.Tables(0).Rows.Count > 0 Then
                            For k = 0 To dr.Tables(0).Rows.Count - 1
                                sConsulta = "INSERT INTO LOG_MON (ACCION, COD, EQUIV, ORIGEN, USU, ERP) VALUES ('" & Accion_Baja & "',N'" & sCodMon & "'"
                                sConsulta = sConsulta & "," & DblToSQLFloat(dblEquiv) & ",'','" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(k).Item("COD")) & "')"

                                adoCom.CommandText = sConsulta
                                adoCom.CommandType = System.Data.CommandType.Text
                                adoCom.ExecuteNonQuery()


                                da2 = New SqlDataAdapter
                                dr2 = New System.Data.DataSet

                                adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_MON ", cn)
                                adoCom.CommandType = System.Data.CommandType.Text
                                da2.SelectCommand = adoCom
                                da2.Fill(dr2)

                                If dr2.Tables(0).Rows.Count > 0 Then

                                    lIdLogMON = dr2.Tables(0).Rows(0).Item(0)

                                    For Each oDen In oDens

                                        sConsulta = "INSERT INTO LOG_MON_DEN (ID_LOG_MON,DEN,IDI) VALUES (" & lIdLogMON & ","
                                        sConsulta = sConsulta & "N'" & DblQuote(oDen.Denominacion) & "','" & DblQuote(oDen.Idioma) & "')"
                                        adoCom = New SqlCommand(sConsulta, cn)
                                        adoCom.Connection = cn
                                        adoCom.CommandType = System.Data.CommandType.Text
                                        adoCom.ExecuteNonQuery()

                                    Next


                                End If


                            Next

                        End If




                        RegistrarAccion(sUsuario, AccionesGS.ACCMonEli, "Cod: " & Codigos(i).ToString)


                    End If
                End If
            Next i

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            cm = New SqlCommand("COMMIT TRANSACTION", cn)
            cm.ExecuteNonQuery()
            cn.Close()
            If iRes > 0 Then
                oError.Number = Fullstep.FSNLibrary.TiposDeDatos.ErroresGS.TESImposibleEliminar
                'oError.Arg1 = vNoEliminados
            End If


        Catch ex As SqlException
            oError = TratarError(ex)

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            cn.Close()

        Finally
            cn.Dispose()
            da.Dispose()
        End Try

        Return oError

    End Function

    ''' <summary>
    ''' Guarda en la tabla UPD_COD el cambio de codigo de moneda a realizar
    ''' </summary>
    ''' <param name="Codigo">Codigo actual de la moneda</param>
    ''' <param name="CodigoNuevo">Nuevo codigo</param>
    ''' <param name="Usuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.MonedasRule.CambioCodigoMoneda
    ''' Tiempo máximo<1 sg</remarks>

    Public Function CambioCodigoMoneda(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_CHANGE_CODES"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@ACCION", SqlDbType.NVarChar, 50)
            cm.Parameters("@ACCION").Value = "Moneda"
            cm.Parameters.Add("@COD", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD").Value = Codigo
            cm.Parameters.Add("@COD_NEW", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD_NEW").Value = CodigoNuevo
            cm.Parameters.Add("@USU", SqlDbType.NVarChar, 50)
            cm.Parameters("@USU").Value = Usuario

            cm.Parameters.Add("@RES", SqlDbType.Int)
            cm.Parameters("@RES").Direction = ParameterDirection.Output

            cn.Open()
            cm.ExecuteNonQuery()
            cn.Close()

            Return cm.Parameters("@RES").Value

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try

    End Function
#End Region

#Region "Unidades data access methods"

    ''' <summary>
    ''' Cargar las monedas de la aplicacion
    ''' </summary>
    ''' <param name="bMostrarIntegracion">Si se muestra el estado de la integracion</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <returns>Un Dataset con las unidades </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.UnidadesRule.CargarTodasLasUnidades
    ''' Tiempo máximo<1 sg</remarks>
    Public Function CargarTodasLasUnidades(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_UNIDADES"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@MOSTRARINTEGRACION", SqlDbType.TinyInt)
            cm.Parameters("@MOSTRARINTEGRACION").Value = IIf(bMostrarIntegracion, 1, 0)
            cm.Parameters.Add("@ENTIDADINTEGRACION", SqlDbType.Int)
            cm.Parameters("@ENTIDADINTEGRACION").Value = EntidadIntegracion.Uni
            cm.Parameters.Add("@ACCION", SqlDbType.NVarChar, 1)
            cm.Parameters("@ACCION").Value = Accion_CambioCodigo
            cm.Parameters.Add("@IDIOMA", SqlDbType.NVarChar, 3)
            cm.Parameters("@IDIOMA").Value = sIdioma

            da.SelectCommand = cm
            da.Fill(ds)

            cn.Close()

            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Actualiza los cambios realizados en alguna unidad
    ''' </summary>
    ''' <param name="Unidad">Unidad en la que se ha producido el cambio</param>
    ''' <param name="bRealizarCambioCodigo">Si hay que hacer cambio de codigo</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.UnidadesRule.ActualizarUnidad
    ''' Tiempo máximo<1 sg</remarks>

    Public Function ActualizarUnidad(ByRef Unidad As GSServerModel.Unidad, ByVal bRealizarCambioCodigo As Boolean, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim sConsulta As String
        Dim sCodigo As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""
        Dim lIdLogUNI As Long

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN TRANSACTION"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()

        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandType = System.Data.CommandType.Text

        sCodigo = DblQuote(Unidad.Codigo)
        Dim uniden As New GSServerModel.Multiidioma(True, "", "", True)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)


        sConsulta = "SELECT FECACT FROM UNI WITH(NOLOCK) WHERE COD=N'" & DblQuote(Unidad.Codigo) & "'"
        adoCom.CommandText = sConsulta

        da = New SqlDataAdapter
        dr = New System.Data.DataSet
        da.SelectCommand = adoCom
        da.Fill(dr)

        If dr.Tables(0).Rows.Count > 0 Then

            If Unidad.FecAct <> dr.Tables(0).Rows(0).Item("FECACT") Then
                cn.Close()
                adoCom.Transaction.Rollback()
                oError.Number = ErroresGS.TESInfActualModificada
                Return oError
                Exit Function
            End If
        Else
            oError.Number = ErroresGS.TESDatoEliminado
            oError.ErrorId = 24

        End If

        Try
            If Not bRealizarCambioCodigo Then

                sConsulta = ""
                For Each uniden In Unidad.Denominaciones
                    sConsulta = sConsulta & " UPDATE UNI_DEN SET DEN=N'" & DblQuote(uniden.Denominacion) & "' WHERE UNI =N'" & sCodigo & "' AND IDIOMA='" & uniden.Idioma & "' "

                Next
                sConsulta = sConsulta & " UPDATE UNI SET "
                If IsNothing(Unidad.NumDecimales) Then
                    sConsulta = sConsulta & "NUMDEC=NULL "
                Else
                    sConsulta = sConsulta & "NUMDEC=" & Unidad.NumDecimales & " "
                End If
                sConsulta = sConsulta & " WHERE COD=N'" & sCodigo & "' "

                cm = New SqlCommand(sConsulta, cn)
                adoCom.CommandType = System.Data.CommandType.Text
                adoCom.CommandText = sConsulta
                adoCom.ExecuteNonQuery()



                If bGrabarLog Then

                    sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                    adoCom = New SqlCommand(sConsulta, cn)
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    adoCom.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = adoCom
                    da.Fill(dr)


                    Dim da2 = New SqlDataAdapter
                    Dim dr2 = New System.Data.DataSet

                    If dr.Tables(0).Rows.Count > 0 Then
                        For i = 0 To dr.Tables(0).Rows.Count - 1
                            sConsulta = "INSERT INTO LOG_UNI (ACCION, COD, ORIGEN, USU, ERP, NUMDEC) VALUES ('" & Accion_Modificacion & "',N'" & sCodigo & "'"
                            sConsulta = sConsulta & "," & Unidad.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "'"
                            If IsNothing(Unidad.NumDecimales) Then
                                sConsulta = sConsulta & ",NULL"
                            Else
                                sConsulta = sConsulta & "," & Unidad.NumDecimales
                            End If
                            sConsulta = sConsulta & ")"


                            adoCom.CommandText = sConsulta
                            adoCom.CommandType = System.Data.CommandType.Text
                            adoCom.ExecuteNonQuery()


                            da2 = New SqlDataAdapter
                            dr2 = New System.Data.DataSet

                            adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_UNI ", cn)
                            adoCom.CommandType = System.Data.CommandType.Text
                            da2.SelectCommand = adoCom
                            da2.Fill(dr2)

                            If dr2.Tables(0).Rows.Count > 0 Then

                                lIdLogUNI = dr2.Tables(0).Rows(0).Item(0)

                                For Each uniden In Unidad.Denominaciones

                                    sConsulta = "INSERT INTO LOG_UNI_DEN (ID_LOG_UNI,DEN,IDI) VALUES (" & lIdLogUNI & ",N'" & DblQuote(uniden.Denominacion) & " ','" & DblQuote(uniden.Idioma) & "')"
                                    adoCom = New SqlCommand(sConsulta, cn)
                                    adoCom.Connection = cn
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    adoCom.ExecuteNonQuery()

                                Next


                            End If


                        Next

                    End If
                End If
            End If

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.CommandText = "COMMIT TRANSACTION"
            adoCom.ExecuteNonQuery()

            sConsulta = "SELECT FECACT FROM UNI WITH(NOLOCK) WHERE COD =N'" & sCodigo & "'"
            cm = New SqlCommand(sConsulta, cn)
            'cm.ExecuteNonQuery()
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            cm.CommandType = System.Data.CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                Unidad.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If

            cn.Close()

        Catch ex As SqlException

            oError = TratarError(ex)
            adoCom.Connection = cn
            adoCom = New SqlCommand("ROLLBACK TRANSACTION", cn)
            adoCom.ExecuteNonQuery()

        End Try

        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCUniMod, "Cod: " & Unidad.Codigo)
        End If
        Return oError

    End Function

    ''' <summary>
    ''' Añade una unidad a la BD
    ''' </summary>
    ''' <param name="Unidad">Unidad en la que se ha producido el cambio</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.UnidadesRule.AnyadirUnidad
    ''' Tiempo máximo<1 sg</remarks>

    Public Function AnyadirUnidad(ByRef Unidad As GSServerModel.Unidad, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException

        Dim sConsulta As String
        Dim sCodigo As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim lIdLogUNI As Long

        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN TRANSACTION"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)
        sCodigo = DblQuote(Unidad.Codigo)
        Dim uniden As New GSServerModel.Multiidioma(True, "", "", True)
        Try
            sConsulta = " INSERT INTO UNI (COD,NUMDEC) VALUES (N'"
            sConsulta = sConsulta & sCodigo & "'"

            If IsNothing(Unidad.NumDecimales) Then
                sConsulta = sConsulta & ",NULL)"
            Else
                sConsulta = sConsulta & "," & Unidad.NumDecimales & ")"
            End If

            For Each uniden In Unidad.Denominaciones
                sConsulta = sConsulta & " INSERT INTO UNI_DEN (UNI,IDIOMA,DEN) VALUES ( N'"
                sConsulta = sConsulta & sCodigo & "','" & DblQuote(uniden.Idioma) & "',N'" & DblQuote(uniden.Denominacion) & "') "
            Next

            adoCom.CommandText = sConsulta
            adoCom.ExecuteNonQuery()


            sConsulta = "SELECT FECACT FROM UNI WITH(NOLOCK) WHERE COD =N'" & sCodigo & "'"
            adoCom = New SqlCommand(sConsulta, cn)
            'cm.ExecuteNonQuery()
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            adoCom.CommandType = System.Data.CommandType.Text
            da.SelectCommand = adoCom
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                Unidad.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If

            If bGrabarLog Then

                sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                adoCom = New SqlCommand(sConsulta, cn)
                da = New SqlDataAdapter
                dr = New System.Data.DataSet
                adoCom.CommandType = System.Data.CommandType.Text
                da.SelectCommand = adoCom
                da.Fill(dr)


                Dim da2 = New SqlDataAdapter
                Dim dr2 = New System.Data.DataSet

                If dr.Tables(0).Rows.Count > 0 Then
                    For i = 0 To dr.Tables(0).Rows.Count - 1
                        sConsulta = "INSERT INTO LOG_UNI (ACCION, COD, ORIGEN, USU, ERP,NUMDEC) VALUES ('" & Accion_Alta & "',N'" & sCodigo & "'"
                        sConsulta = sConsulta & "," & Unidad.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "'"
                        If IsNothing(Unidad.NumDecimales) Then
                            sConsulta = sConsulta & ",NULL"
                        Else
                            sConsulta = sConsulta & "," & Unidad.NumDecimales
                        End If
                        sConsulta = sConsulta & ")"

                        adoCom.CommandText = sConsulta
                        adoCom.CommandType = System.Data.CommandType.Text
                        adoCom.ExecuteNonQuery()


                        da2 = New SqlDataAdapter
                        dr2 = New System.Data.DataSet

                        adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_UNI ", cn)
                        adoCom.CommandType = System.Data.CommandType.Text
                        da2.SelectCommand = adoCom
                        da2.Fill(dr2)

                        If dr2.Tables(0).Rows.Count > 0 Then

                            lIdLogUNI = dr2.Tables(0).Rows(0).Item(0)

                            For Each uniden In Unidad.Denominaciones
                                sConsulta = "INSERT INTO LOG_UNI_DEN (ID_LOG_UNI,DEN,IDI) VALUES (" & lIdLogUNI & ","
                                sConsulta = sConsulta & "N'" & DblQuote(uniden.Denominacion) & "','" & DblQuote(uniden.Idioma) & "')"
                                adoCom = New SqlCommand(sConsulta, cn)
                                adoCom.Connection = cn
                                adoCom.CommandType = System.Data.CommandType.Text
                                adoCom.ExecuteNonQuery()

                            Next


                        End If


                    Next

                End If
            End If

            adoCom = New SqlCommand("COMMIT TRANSACTION", cn)
            adoCom.ExecuteNonQuery()

            cn.Close()

        Catch ex As SqlException

            oError = TratarError(ex)

            adoCom = New SqlCommand("ROLLBACK TRANSACTION", cn)
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()
            cn.Close()
        End Try

        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCUniAnya, "Cod: " & Unidad.Codigo)
        End If
        Return oError


    End Function

    ''' <summary>
    ''' Elimina unidades de la BD
    ''' </summary>
    ''' <param name="Codigos">Codigos de las unidades a eliminar</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sMonedaCentral">Moneda central de la aplicacion</param>
    ''' <param name="Origen">Origen de integracion</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.UnidadesRule.EliminarUnidades
    ''' Tiempo máximo<1 sg</remarks>

    Public Function EliminarUnidades(ByVal Codigos() As String, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal Origen As TiposDeDatos.OrigenIntegracion, ByVal sUsuario As String) As GSServerModel.GSException

        Dim i, j, k As Integer
        Dim bGuardar As Boolean
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim sCodUnidad As String
        Dim iRes As Integer
        Dim ContRegBorrados As Integer

        Dim oIdiomas As New GSServerModel.Idiomas(True, "", "", True)
        Dim lIdLogUNI As Long

        Dim oDens As New GSServerModel.Multiidiomas(False, "", "", True)
        Dim oDen As New GSServerModel.Multiidioma(False, "", "", True)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)


        For i = 0 To Codigos.Count - 1
            bGuardar = True
            sCodUnidad = DblQuote(Codigos(i).ToString())

            Try

                adoCom = New SqlCommand
                cn.Open()
                adoCom.Connection = cn
                adoCom.CommandText = "BEGIN TRANSACTION "
                adoCom.CommandType = System.Data.CommandType.Text
                adoCom.ExecuteNonQuery()
                'Exc = New Exception
                adoCom = New SqlCommand
                adoCom.Connection = cn

                'ALMACENAMOS EN BASE DE DATOS:
                If bGuardar = True Then
                    If bGrabarLog Then ' primero recuperamos los datos de la moneda que se va a borra


                        sConsulta = "SELECT IDIOMA, DEN FROM UNI_DEN WITH(NOLOCK) WHERE UNI =N'" & sCodUnidad & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)
                        oDens.Clear()
                        If dr.Tables(0).Rows.Count > 0 Then
                            For j = 0 To dr.Tables(0).Rows.Count - 1
                                oDens.Add(dr.Tables(0).Rows(j).Item("IDIOMA"), dr.Tables(0).Rows(j).Item("DEN"), Now)
                            Next
                        End If


                    End If


                    sConsulta = "UPDATE COPIA_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & sCodUnidad & "' AND COPIA_CAMPO_DEF IN (SELECT ID FROM FORM_CAMPO WHERE TIPO_CAMPO_GS=" & TipoCampoGS.Unidad & ")"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    sConsulta = "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & sCodUnidad & "' AND CAMPO_HIJO IN (SELECT COPIA_CAMPO.ID FROM COPIA_CAMPO INNER JOIN FORM_CAMPO ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Unidad & ")"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    'elimina de los formularios:
                    sConsulta = "UPDATE FORM_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & sCodUnidad & "' AND TIPO_CAMPO_GS=" & TipoCampoGS.Unidad
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()

                    sConsulta = "UPDATE LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & sCodUnidad & "' AND CAMPO_HIJO IN (SELECT ID FROM FORM_CAMPO WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Unidad & ")"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    'Elimina de las solicitudes favoritas los campos con la Moneda
                    sConsulta = "DELETE FROM PM_CAMPO_FAV FROM PM_CAMPO_FAV CF"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = CF.CAMPO "
                    sConsulta = sConsulta & " WHERE CF.VALOR_TEXT=N'" & sCodUnidad & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.Unidad
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    'Elimina de las solicitudes favoritas las lineas de desglose con la Moneda
                    sConsulta = "DELETE FROM PM_LINEA_DESGLOSE_FAV FROM PM_LINEA_DESGLOSE_FAV LDF"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = LDF.CAMPO_HIJO"
                    sConsulta = sConsulta & " WHERE LDF.VALOR_TEXT=N'" & sCodUnidad & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.Unidad
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    sConsulta = " DELETE FROM PROVE_ART_UP WHERE UP=N'" & sCodUnidad & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()

                    sConsulta = "DELETE FROM PROVE_ART_UP WHERE UC=N'" & sCodUnidad & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()

                    sConsulta = "DELETE FROM UNI_ERP WHERE COD_GS=N'" & sCodUnidad & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()


                    sConsulta = "DELETE FROM UNI_DEN WHERE UNI=N'" & sCodUnidad & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    cm.ExecuteNonQuery()

                    sConsulta = "DELETE FROM UNI WHERE COD=N'" & sCodUnidad & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    ContRegBorrados = cm.ExecuteNonQuery()



                    If bGrabarLog And ContRegBorrados > 0 Then

                        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                        adoCom = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        adoCom.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = adoCom
                        da.Fill(dr)


                        Dim da2 = New SqlDataAdapter
                        Dim dr2 = New System.Data.DataSet

                        If dr.Tables(0).Rows.Count > 0 Then
                            For k = 0 To dr.Tables(0).Rows.Count - 1
                                sConsulta = "INSERT INTO LOG_UNI (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Baja & "',N'" & sCodUnidad & "'"
                                sConsulta = sConsulta & ",'" & DblQuote(Origen) & "' ,'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(k).Item("COD")) & "')"

                                adoCom.CommandText = sConsulta
                                adoCom.CommandType = System.Data.CommandType.Text
                                adoCom.ExecuteNonQuery()


                                da2 = New SqlDataAdapter
                                dr2 = New System.Data.DataSet

                                adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_UNI ", cn)
                                adoCom.CommandType = System.Data.CommandType.Text
                                da2.SelectCommand = adoCom
                                da2.Fill(dr2)

                                If dr2.Tables(0).Rows.Count > 0 Then

                                    lIdLogUNI = dr2.Tables(0).Rows(0).Item(0)

                                    For Each oDen In oDens

                                        sConsulta = "INSERT INTO LOG_UNI_DEN (ID_LOG_UNI,DEN,IDI) VALUES (" & lIdLogUNI & ","
                                        sConsulta = sConsulta & "N'" & DblQuote(oDen.Denominacion) & "','" & DblQuote(oDen.Idioma) & "')"
                                        adoCom = New SqlCommand(sConsulta, cn)
                                        adoCom.Connection = cn
                                        adoCom.CommandType = System.Data.CommandType.Text
                                        adoCom.ExecuteNonQuery()

                                    Next


                                End If

                            Next

                        End If

                        RegistrarAccion(sUsuario, AccionesGS.ACCUniEli, "Cod: " & Codigos(i))

                    End If
                    cm = New SqlCommand("COMMIT TRANSACTION", cn)
                    cm.ExecuteNonQuery()
                End If

            Catch ex As SqlException

                oError = TratarError(ex)
                If ex.Errors(0).Number = 547 Then
                    Select Case oError.ErrorId


                        Case 25
                            ' ERROR PORQUE ESTA RELACIONADA CON LA TABLA ART4

                            sConsulta = "SELECT GMN1,GMN2,GMN3,GMN4,COD,DEN FROM ART4 WHERE ART4.UNI =N'" & sCodUnidad & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then

                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1) & "-" & dr.Tables(0).Rows(0).Item(2) & "-" & dr.Tables(0).Rows(0).Item(3) & "-" & dr.Tables(0).Rows(0).Item(4) & "-" & dr.Tables(0).Rows(0).Item(5))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog

                        Case 26
                            ' ERROR PORQUE ESTA RELACIONADA CON LA TABLA ART4_ADJ
                            sConsulta = "SELECT ART4.GMN1,ART4.GMN2,ART4.GMN3,ART4.GMN4,ART4_ADJ.ART FROM ART4_ADJ INNER JOIN ART4 ON ART4.COD=ART4_ADJ.ART WHERE ART4_ADJ.UNI =N'" & sCodUnidad & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then

                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1) & "-" & dr.Tables(0).Rows(0).Item(2) & "-" & dr.Tables(0).Rows(0).Item(3) & "-" & dr.Tables(0).Rows(0).Item(4))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog

                        Case 46
                            ' ERROR PORQUE ESTA RELACIONADA CON LA TABLA ITEM
                            sConsulta = "SELECT ITEM.ANYO,ITEM.PROCE,ITEM.ART,ITEM.DESCR FROM ITEM "
                            sConsulta = sConsulta & " WHERE ITEM.UNI=N'" & sCodUnidad & "'"

                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then

                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1) & "-" & dr.Tables(0).Rows(0).Item(3))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog


                        Case 66
                            ' ESTA RELACIONADA CON LA TABLA PARGEN DEF
                            sConsulta = "SELECT PARGEN_DEF.ID FROM PARGEN_DEF WHERE PARGEN_DEF.UNIDEF=N'" & sCodUnidad & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then

                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog
                        Case 133
                            ' ESTA RELACIONADA CON LA TABLA LINEAS_PEDIDO
                            sConsulta = "SELECT PEDIDO.ANYO,PEDIDO.NUM FROM LINEAS_PEDIDO "
                            sConsulta = sConsulta & " INNER JOIN PEDIDO ON PEDIDO.ID = LINEAS_PEDIDO.PEDIDO "
                            sConsulta = sConsulta & " WHERE UP =N'" & sCodUnidad & "'"

                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then
                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog


                        Case 134
                            ' ESTA RELACIONADA CON LA TABLA CATALOG_LIN
                            sConsulta = "SELECT CATN1.COD,CATN1.DEN FROM CATALOG_LIN "
                            sConsulta = sConsulta & " INNER JOIN CATN1 ON CATN1.ID = CATALOG_LIN.CAT1"
                            sConsulta = sConsulta & " WHERE UC=N'" & sCodUnidad & "' OR UP_DEF =N'" & sCodUnidad & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then

                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog

                        Case 131
                            sConsulta = " SELECT CAT_LIN_MIN.LINEA  FROM CAT_LIN_MIN WHERE CAT_LIN_MIN.UP=N'" & sCodUnidad & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then

                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog

                        Case Else
                            ' ESTA RELACIONADA una tabla desconocida

                            oError.Errores.Add(Codigos(i), oError.ErrorId, i, "")
                            iRes = iRes + 1

                            GoTo NoLog

                    End Select
                End If
Nolog:
                cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
                cm.ExecuteNonQuery()
                Return (oError)
            End Try

        Next i

        Return (oError)

        cn.Dispose()
        da.Dispose()


    End Function

    ''' <summary>
    ''' Guarda en la tabla UPD_COD el cambio de codigo de unidad a realizar
    ''' </summary>
    ''' <param name="Codigo">Codigo actual de la unidad</param>
    ''' <param name="CodigoNuevo">Nuevo codigo</param>
    ''' <param name="Usuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.UnidadesRule.CambioCodigoUnidad
    ''' Tiempo máximo<1 sg</remarks>
    Public Function CambioCodigoUnidad(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_CHANGE_CODES"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@ACCION", SqlDbType.NVarChar, 50)
            cm.Parameters("@ACCION").Value = "Unidad"
            cm.Parameters.Add("@COD", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD").Value = Codigo
            cm.Parameters.Add("@COD_NEW", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD_NEW").Value = CodigoNuevo
            cm.Parameters.Add("@USU", SqlDbType.NVarChar, 50)
            cm.Parameters("@USU").Value = Usuario

            cm.Parameters.Add("@RES", SqlDbType.Int)
            cm.Parameters("@RES").Direction = ParameterDirection.Output

            cn.Open()
            cm.ExecuteNonQuery()
            cn.Close()

            Return cm.Parameters("@RES").Value

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try

    End Function

#End Region

#Region "Vias de pago data access methods"

    ''' <summary>
    ''' Cargar las vias de pago de la aplicacion
    ''' </summary>
    ''' <param name="bMostrarIntegracion">Si se muestra el estado de la integracion</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <returns>Un Dataset con las monedas </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.ViasPagoRule.CargarTodasLasViasPago
    ''' Tiempo máximo<1 sg</remarks>

    Public Function CargarTodasLasViasPago(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_VIAS_PAGO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@MOSTRARINTEGRACION", SqlDbType.TinyInt)
            cm.Parameters("@MOSTRARINTEGRACION").Value = IIf(bMostrarIntegracion, 1, 0)
            cm.Parameters.Add("@ENTIDADINTEGRACION", SqlDbType.Int)
            cm.Parameters("@ENTIDADINTEGRACION").Value = EntidadIntegracion.ViaPag
            cm.Parameters.Add("@ACCION", SqlDbType.NVarChar, 1)
            cm.Parameters("@ACCION").Value = Accion_CambioCodigo
            cm.Parameters.Add("@IDIOMA", SqlDbType.NVarChar, 3)
            cm.Parameters("@IDIOMA").Value = sIdioma


            da.SelectCommand = cm
            da.Fill(ds)

            cn.Close()

            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Añade una via de pago a la BD
    ''' </summary>
    ''' <param name="ViaPago">ViaPago en la que se ha producido el cambio</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.ViasPagoRule.AnyadirViasPago
    ''' Tiempo máximo<1 sg</remarks>

    Public Function AnyadirViaPago(ByRef ViaPago As GSServerModel.ViaPago, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException

        Dim sConsulta As String
        Dim sCodigo As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim lIdLogVIAPAG As Long

        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN TRANSACTION"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)
        sCodigo = DblQuote(ViaPago.Codigo)
        Dim viapagden As New GSServerModel.Multiidioma(True, "", "", True)
        Try
            sConsulta = " INSERT INTO VIA_PAG (COD) VALUES (N'" & sCodigo & "')"

            For Each viapagden In ViaPago.Denominaciones
                sConsulta = sConsulta & " INSERT INTO VIA_PAG_DEN (VIA_PAG,IDIOMA,DEN) VALUES ( N'"
                sConsulta = sConsulta & sCodigo & "','" & DblQuote(viapagden.Idioma) & "',N'" & DblQuote(viapagden.Denominacion) & "') "
            Next

            adoCom.CommandText = sConsulta
            adoCom.ExecuteNonQuery()


            sConsulta = "SELECT FECACT FROM VIA_PAG WITH(NOLOCK) WHERE COD =N'" & sCodigo & "'"
            adoCom = New SqlCommand(sConsulta, cn)
            'cm.ExecuteNonQuery()
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            adoCom.CommandType = System.Data.CommandType.Text
            da.SelectCommand = adoCom
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                ViaPago.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If

            If bGrabarLog Then

                sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                adoCom = New SqlCommand(sConsulta, cn)
                da = New SqlDataAdapter
                dr = New System.Data.DataSet
                adoCom.CommandType = System.Data.CommandType.Text
                da.SelectCommand = adoCom
                da.Fill(dr)


                Dim da2 = New SqlDataAdapter
                Dim dr2 = New System.Data.DataSet

                If dr.Tables(0).Rows.Count > 0 Then
                    For i = 0 To dr.Tables(0).Rows.Count - 1
                        sConsulta = "INSERT INTO LOG_VIA_PAG (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Alta & "',N'" & sCodigo & "'"
                        sConsulta = sConsulta & "," & ViaPago.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "')"

                        adoCom.CommandText = sConsulta
                        adoCom.CommandType = System.Data.CommandType.Text
                        adoCom.ExecuteNonQuery()


                        da2 = New SqlDataAdapter
                        dr2 = New System.Data.DataSet

                        adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_VIA_PAG ", cn)
                        adoCom.CommandType = System.Data.CommandType.Text
                        da2.SelectCommand = adoCom
                        da2.Fill(dr2)

                        If dr2.Tables(0).Rows.Count > 0 Then

                            lIdLogVIAPAG = dr2.Tables(0).Rows(0).Item(0)

                            For Each viapagden In ViaPago.Denominaciones

                                sConsulta = "INSERT INTO LOG_VIA_PAG_DEN (ID_LOG_VIA_PAG,DEN,IDI) VALUES (" & lIdLogVIAPAG & ","
                                sConsulta = sConsulta & "N'" & DblQuote(viapagden.Denominacion) & "','" & DblQuote(viapagden.Idioma) & "')"
                                adoCom = New SqlCommand(sConsulta, cn)
                                adoCom.Connection = cn
                                adoCom.CommandType = System.Data.CommandType.Text
                                adoCom.ExecuteNonQuery()

                            Next


                        End If


                    Next

                End If
            End If

            adoCom = New SqlCommand("COMMIT TRANSACTION", cn)
            adoCom.ExecuteNonQuery()

            cn.Close()

        Catch ex As SqlException

            oError = TratarError(ex)
            adoCom = New SqlCommand("ROLLBACK TRANSACTION", cn)
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()
            cn.Close()
        End Try

        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCViaPagAnya, "Cod: " & ViaPago.Codigo)
        End If
        Return oError


    End Function

    ''' <summary>
    ''' Actualiza los cambios realizados en alguna via de pago
    ''' </summary>
    ''' <param name="ViaPago">ViaPago en la que se ha producido el cambio</param>
    ''' <param name="bRealizarCambioCodigo">Si hay que hacer cambio de codigo</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.ViasPagoRule.ActualizarViaPago
    ''' Tiempo máximo<1 sg</remarks>

    Public Function ActualizarViaPago(ByRef ViaPago As GSServerModel.ViaPago, ByVal bRealizarCambioCodigo As Boolean, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim sConsulta As String
        Dim sCodigo As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""
        Dim lIdLogVIAPAG As Long

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN TRANSACTION"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()
        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandType = System.Data.CommandType.Text

        Dim viapagden As New GSServerModel.Multiidioma(True, "", "", True)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        sCodigo = DblQuote(ViaPago.Codigo)
        sConsulta = "SELECT FECACT FROM VIA_PAG WITH(NOLOCK) WHERE COD=N'" & sCodigo & "'"
        adoCom.CommandText = sConsulta

        da = New SqlDataAdapter
        dr = New System.Data.DataSet
        da.SelectCommand = adoCom
        da.Fill(dr)

        If dr.Tables(0).Rows.Count > 0 Then

            If ViaPago.FecAct <> dr.Tables(0).Rows(0).Item("FECACT") Then
                cn.Close()
                adoCom.Transaction.Rollback()
                oError.Number = ErroresGS.TESInfActualModificada
                Return oError
                Exit Function
            End If
        Else
            oError.Number = ErroresGS.TESDatoEliminado
            oError.ErrorId = 24

        End If

        Try
            If Not bRealizarCambioCodigo Then

                sConsulta = ""
                For Each viapagden In ViaPago.Denominaciones
                    sConsulta = sConsulta & " UPDATE VIA_PAG_DEN SET DEN=N'" & DblQuote(viapagden.Denominacion) & "' WHERE VIA_PAG =N'" & sCodigo & "' AND IDIOMA='" & viapagden.Idioma & "' "
                Next

                cm = New SqlCommand(sConsulta, cn)
                adoCom.CommandType = System.Data.CommandType.Text
                adoCom.CommandText = sConsulta
                adoCom.ExecuteNonQuery()



                If bGrabarLog Then

                    sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                    adoCom = New SqlCommand(sConsulta, cn)
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    adoCom.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = adoCom
                    da.Fill(dr)


                    Dim da2 = New SqlDataAdapter
                    Dim dr2 = New System.Data.DataSet

                    If dr.Tables(0).Rows.Count > 0 Then
                        For i = 0 To dr.Tables(0).Rows.Count - 1
                            sConsulta = "INSERT INTO LOG_VIA_PAG (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Modificacion & "',N'" & sCodigo & "'"
                            sConsulta = sConsulta & "," & ViaPago.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "')"

                            adoCom.CommandText = sConsulta
                            adoCom.CommandType = System.Data.CommandType.Text
                            adoCom.ExecuteNonQuery()


                            da2 = New SqlDataAdapter
                            dr2 = New System.Data.DataSet

                            adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_VIA_PAG ", cn)
                            adoCom.CommandType = System.Data.CommandType.Text
                            da2.SelectCommand = adoCom
                            da2.Fill(dr2)

                            If dr2.Tables(0).Rows.Count > 0 Then

                                lIdLogVIAPAG = dr2.Tables(0).Rows(0).Item(0)

                                For Each viapagden In ViaPago.Denominaciones

                                    sConsulta = "INSERT INTO LOG_VIA_PAG_DEN (ID_LOG_VIA_PAG,DEN,IDI) VALUES (" & lIdLogVIAPAG & ",N'" & DblQuote(viapagden.Denominacion) & " ','" & DblQuote(viapagden.Idioma) & "')"
                                    adoCom = New SqlCommand(sConsulta, cn)
                                    adoCom.Connection = cn
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    adoCom.ExecuteNonQuery()

                                Next


                            End If


                        Next

                    End If
                End If
            End If

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.CommandText = "COMMIT TRANSACTION"
            adoCom.ExecuteNonQuery()

            sConsulta = "SELECT FECACT FROM VIA_PAG WITH(NOLOCK) WHERE COD =N'" & sCodigo & "'"
            cm = New SqlCommand(sConsulta, cn)
            'cm.ExecuteNonQuery()
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            cm.CommandType = System.Data.CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                ViaPago.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If

            cn.Close()

        Catch ex As SqlException

            oError = TratarError(ex)

            adoCom.Connection = cn
            adoCom = New SqlCommand("ROLLBACK TRANSACTION", cn)
            adoCom.ExecuteNonQuery()

        End Try

        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCViaPagMod, "Cod: " & ViaPago.Codigo)
        End If
        Return oError

    End Function

    ''' <summary>
    ''' Elimina vias de pago de la BD
    ''' </summary>
    ''' <param name="Codigos">Codigos de las vias de pago a eliminar</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sMonedaCentral">Moneda central de la aplicacion</param>
    ''' <param name="Origen">Origen de integracion</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.ViasPagoRule.EliminarViasPago
    ''' Tiempo máximo<1 sg</remarks>

    Public Function EliminarViasPago(ByVal Codigos() As String, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal Origen As TiposDeDatos.OrigenIntegracion, ByVal sUsuario As String) As GSServerModel.GSException

        Dim i, j, k As Integer
        Dim bGuardar As Boolean
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim sCodViaPago As String
        Dim iRes As Integer
        Dim ContRegBorrados As Integer

        Dim oIdiomas As New GSServerModel.Idiomas(True, "", "", True)
        Dim lIdLogVIAPAG As Long

        Dim oDens As New GSServerModel.Multiidiomas(False, "", "", True)
        Dim oDen As New GSServerModel.Multiidioma(False, "", "", True)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        For i = 0 To Codigos.Count - 1
            bGuardar = True
            sCodViaPago = DblQuote(Codigos(i).ToString())

            Try

                adoCom = New SqlCommand
                cn.Open()
                adoCom.Connection = cn
                adoCom.CommandText = "BEGIN TRANSACTION "
                adoCom.CommandType = System.Data.CommandType.Text
                adoCom.ExecuteNonQuery()
                'Exc = New Exception
                adoCom = New SqlCommand
                adoCom.Connection = cn

                'ALMACENAMOS EN BASE DE DATOS:
                If bGrabarLog Then ' primero recuperamos los datos de la moneda que se va a borra

                    sConsulta = "SELECT IDIOMA, DEN FROM VIA_PAG_DEN WITH(NOLOCK) WHERE VIA_PAG =N'" & sCodViaPago & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    cm.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(dr)
                    oDens.Clear()
                    If dr.Tables(0).Rows.Count > 0 Then
                        For j = 0 To dr.Tables(0).Rows.Count - 1
                            oDens.Add(dr.Tables(0).Rows(j).Item("IDIOMA"), dr.Tables(0).Rows(j).Item("DEN"), Now)
                        Next
                    End If


                End If

                ''MultiERP: Elimina de PAG_ERP
                sConsulta = "DELETE FROM VIA_PAG_ERP WHERE COD_GS=N'" & sCodViaPago & "'"
                cm = New SqlCommand(sConsulta, cn)
                cm.ExecuteNonQuery()

                sConsulta = "DELETE FROM VIA_PAG_DEN WHERE VIA_PAG=N'" & sCodViaPago & "'"
                cm = New SqlCommand(sConsulta, cn)
                cm.ExecuteNonQuery()

                sConsulta = "DELETE FROM VIA_PAG WHERE COD=N'" & sCodViaPago & "'"
                cm = New SqlCommand(sConsulta, cn)
                ContRegBorrados = cm.ExecuteNonQuery()


                If bGrabarLog And ContRegBorrados > 0 Then

                    sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                    adoCom = New SqlCommand(sConsulta, cn)
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    adoCom.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = adoCom
                    da.Fill(dr)


                    Dim da2 = New SqlDataAdapter
                    Dim dr2 = New System.Data.DataSet

                    If dr.Tables(0).Rows.Count > 0 Then
                        For k = 0 To dr.Tables(0).Rows.Count - 1
                            sConsulta = "INSERT INTO LOG_VIA_PAG (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Baja & "',N'" & sCodViaPago & "'"
                            sConsulta = sConsulta & ",'" & DblQuote(Origen) & "' ,'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(k).Item("COD")) & "')"

                            adoCom.CommandText = sConsulta
                            adoCom.CommandType = System.Data.CommandType.Text
                            adoCom.ExecuteNonQuery()


                            da2 = New SqlDataAdapter
                            dr2 = New System.Data.DataSet

                            adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_VIA_PAG ", cn)
                            adoCom.CommandType = System.Data.CommandType.Text
                            da2.SelectCommand = adoCom
                            da2.Fill(dr2)

                            If dr2.Tables(0).Rows.Count > 0 Then

                                lIdLogVIAPAG = dr2.Tables(0).Rows(0).Item(0)

                                For Each oDen In oDens

                                    sConsulta = "INSERT INTO LOG_VIA_PAG_DEN (ID_LOG_VIA_PAG,DEN,IDI) VALUES (" & lIdLogVIAPAG & ","
                                    sConsulta = sConsulta & "N'" & DblQuote(oDen.Denominacion) & "','" & DblQuote(oDen.Idioma) & "')"
                                    adoCom = New SqlCommand(sConsulta, cn)
                                    adoCom.Connection = cn
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    adoCom.ExecuteNonQuery()

                                Next


                            End If

                        Next

                    End If

                    RegistrarAccion(sUsuario, AccionesGS.ACCViaPagEli, "Cod: " & Codigos(i).ToString)

                End If
                cm = New SqlCommand("COMMIT TRANSACTION", cn)
                cm.ExecuteNonQuery()

            Catch ex As SqlException

                oError = TratarError(ex)
                If ex.Errors(0).Number = 547 Then
                    GoTo Nolog
                End If
Nolog:
                cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
                cm.ExecuteNonQuery()
                Return (oError)
            End Try

        Next i

        Return (oError)

        cn.Dispose()
        da.Dispose()


    End Function

    ''' <summary>
    ''' Guarda en la tabla UPD_COD el cambio de codigo de la via de pago a realizar
    ''' </summary>
    ''' <param name="Codigo">Codigo actual de la via de pago</param>
    ''' <param name="CodigoNuevo">Nuevo codigo</param>
    ''' <param name="Usuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.ViasPagoRule.CambioCodigoViaPago
    ''' Tiempo máximo<1 sg</remarks>

    Public Function CambioCodigoViaPago(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_CHANGE_CODES"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@ACCION", SqlDbType.NVarChar, 50)
            cm.Parameters("@ACCION").Value = "VIAPAGO"
            cm.Parameters.Add("@COD", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD").Value = Codigo
            cm.Parameters.Add("@COD_NEW", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD_NEW").Value = CodigoNuevo
            cm.Parameters.Add("@USU", SqlDbType.NVarChar, 50)
            cm.Parameters("@USU").Value = Usuario

            cm.Parameters.Add("@RES", SqlDbType.Int)
            cm.Parameters("@RES").Direction = ParameterDirection.Output

            cn.Open()
            cm.ExecuteNonQuery()
            cn.Close()

            Return cm.Parameters("@RES").Value

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try

    End Function


#End Region

#Region "Formas de pago data access methods"

    ''' <summary>
    ''' Cargar las formas de pago de la aplicacion
    ''' </summary>
    ''' <param name="bMostrarIntegracion">Si se muestra el estado de la integracion</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <returns>Un Dataset con las formas de pago </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.FormasPagoRule.CargarTodasLasFormasPago
    ''' Tiempo máximo<1 sg</remarks>

    Public Function CargarTodasLasFormasPago(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_FORMAS_PAGO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@MOSTRARINTEGRACION", SqlDbType.TinyInt)
            cm.Parameters("@MOSTRARINTEGRACION").Value = IIf(bMostrarIntegracion, 1, 0)
            cm.Parameters.Add("@ENTIDADINTEGRACION", SqlDbType.Int)
            cm.Parameters("@ENTIDADINTEGRACION").Value = EntidadIntegracion.Pag
            cm.Parameters.Add("@ACCION", SqlDbType.NVarChar, 1)
            cm.Parameters("@ACCION").Value = Accion_CambioCodigo
            cm.Parameters.Add("@IDIOMA", SqlDbType.NVarChar, 3)
            cm.Parameters("@IDIOMA").Value = sIdioma

            da.SelectCommand = cm
            da.Fill(ds)

            cn.Close()

            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Añade una froma de pago a la BD
    ''' </summary>
    ''' <param name="FormaPago">FormaPago en la que se ha producido el cambio</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.FormasPagoRule.AnyadirMoneda
    ''' Tiempo máximo<1 sg</remarks>

    Public Function AnyadirFormaPago(ByRef FormaPago As GSServerModel.FormaPago, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException

        Dim sConsulta As String
        Dim sCodigo As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim lIdLogFORMAPAG As Long

        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN TRANSACTION"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        sCodigo = DblQuote(FormaPago.Codigo)
        Dim formapagden As New GSServerModel.Multiidioma(True, "", "", True)
        Try
            sConsulta = " INSERT INTO PAG (COD) VALUES (N'" & sCodigo & "')"

            For Each formapagden In FormaPago.Denominaciones
                sConsulta = sConsulta & " INSERT INTO PAG_DEN (PAG,IDIOMA,DEN) VALUES ( N'"
                sConsulta = sConsulta & sCodigo & "','" & DblQuote(formapagden.Idioma) & "',N'" & DblQuote(formapagden.Denominacion) & "') "
            Next

            adoCom.CommandText = sConsulta
            adoCom.ExecuteNonQuery()


            sConsulta = "SELECT FECACT FROM PAG WITH(NOLOCK) WHERE COD =N'" & sCodigo & "'"
            adoCom = New SqlCommand(sConsulta, cn)
            'cm.ExecuteNonQuery()
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            adoCom.CommandType = System.Data.CommandType.Text
            da.SelectCommand = adoCom
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                FormaPago.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If

            If bGrabarLog Then

                sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                adoCom = New SqlCommand(sConsulta, cn)
                da = New SqlDataAdapter
                dr = New System.Data.DataSet
                adoCom.CommandType = System.Data.CommandType.Text
                da.SelectCommand = adoCom
                da.Fill(dr)


                Dim da2 = New SqlDataAdapter
                Dim dr2 = New System.Data.DataSet

                If dr.Tables(0).Rows.Count > 0 Then
                    For i = 0 To dr.Tables(0).Rows.Count - 1
                        sConsulta = "INSERT INTO LOG_PAG (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Alta & "',N'" & sCodigo & "'"
                        sConsulta = sConsulta & "," & FormaPago.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "')"

                        adoCom.CommandText = sConsulta
                        adoCom.CommandType = System.Data.CommandType.Text
                        adoCom.ExecuteNonQuery()


                        da2 = New SqlDataAdapter
                        dr2 = New System.Data.DataSet

                        adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_PAG ", cn)
                        adoCom.CommandType = System.Data.CommandType.Text
                        da2.SelectCommand = adoCom
                        da2.Fill(dr2)

                        If dr2.Tables(0).Rows.Count > 0 Then

                            lIdLogFORMAPAG = dr2.Tables(0).Rows(0).Item(0)

                            For Each formapagden In FormaPago.Denominaciones

                                sConsulta = "INSERT INTO LOG_PAG_DEN (ID_LOG_PAG,DEN,IDI) VALUES (" & lIdLogFORMAPAG & ","
                                sConsulta = sConsulta & "N'" & DblQuote(formapagden.Denominacion) & "','" & DblQuote(formapagden.Idioma) & "')"
                                adoCom = New SqlCommand(sConsulta, cn)
                                adoCom.Connection = cn
                                adoCom.CommandType = System.Data.CommandType.Text
                                adoCom.ExecuteNonQuery()

                            Next


                        End If


                    Next

                End If

            End If
            adoCom = New SqlCommand("COMMIT TRANSACTION", cn)
            adoCom.ExecuteNonQuery()

            cn.Close()

        Catch ex As SqlException

            oError = TratarError(ex)

            adoCom = New SqlCommand("ROLLBACK TRANSACTION", cn)
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()
            cn.Close()
        End Try

        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCPagAnya, "Cod: " & FormaPago.Codigo)
        End If
        Return oError


    End Function


    ''' <summary>
    ''' Actualiza los cambios realizados en alguna froma de pago
    ''' </summary>
    ''' <param name="FormaPago">FormaPago en la que se ha producido el cambio</param>
    ''' <param name="bRealizarCambioCodigo">Si hay que hacer cambio de codigo</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.FormasPagoRule.ActualizarFormaPago
    ''' Tiempo máximo<1 sg</remarks>
    ''' 
    Public Function ActualizarFormaPago(ByRef FormaPago As GSServerModel.FormaPago, ByVal bRealizarCambioCodigo As Boolean, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim sConsulta As String
        Dim sCodigo As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""
        Dim lIdLogFORMAPAG As Long

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN TRANSACTION"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()

        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandType = System.Data.CommandType.Text

        sCodigo = DblQuote(FormaPago.Codigo)
        Dim formapagden As New GSServerModel.Multiidioma(True, "", "", True)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)


        sConsulta = "SELECT FECACT FROM PAG WITH(NOLOCK) WHERE COD=N'" & sCodigo & "'"
        adoCom.CommandText = sConsulta

        da = New SqlDataAdapter
        dr = New System.Data.DataSet
        da.SelectCommand = adoCom
        da.Fill(dr)

        If dr.Tables(0).Rows.Count > 0 Then

            If FormaPago.FecAct <> dr.Tables(0).Rows(0).Item("FECACT") Then
                cn.Close()
                adoCom.Transaction.Rollback()
                oError.Number = ErroresGS.TESInfActualModificada
                Return oError
                Exit Function
            End If
        Else
            oError.Number = ErroresGS.TESDatoEliminado
            oError.ErrorId = 24

        End If

        Try
            If Not bRealizarCambioCodigo Then

                sConsulta = ""
                For Each formapagden In FormaPago.Denominaciones
                    sConsulta = sConsulta & " UPDATE PAG_DEN SET DEN=N'" & formapagden.Denominacion & "' WHERE PAG =N'" & sCodigo & "' AND IDIOMA='" & formapagden.Idioma & "' "
                Next

                cm = New SqlCommand(sConsulta, cn)
                adoCom.CommandType = System.Data.CommandType.Text
                adoCom.CommandText = sConsulta
                adoCom.ExecuteNonQuery()



                If bGrabarLog Then

                    sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                    adoCom = New SqlCommand(sConsulta, cn)
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    adoCom.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = adoCom
                    da.Fill(dr)


                    Dim da2 = New SqlDataAdapter
                    Dim dr2 = New System.Data.DataSet

                    If dr.Tables(0).Rows.Count > 0 Then
                        For i = 0 To dr.Tables(0).Rows.Count - 1
                            sConsulta = "INSERT INTO LOG_PAG (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Modificacion & "',N'" & sCodigo & "'"
                            sConsulta = sConsulta & "," & FormaPago.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "')"

                            adoCom.CommandText = sConsulta
                            adoCom.CommandType = System.Data.CommandType.Text
                            adoCom.ExecuteNonQuery()


                            da2 = New SqlDataAdapter
                            dr2 = New System.Data.DataSet

                            adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_PAG ", cn)
                            adoCom.CommandType = System.Data.CommandType.Text
                            da2.SelectCommand = adoCom
                            da2.Fill(dr2)

                            If dr2.Tables(0).Rows.Count > 0 Then

                                lIdLogFORMAPAG = dr2.Tables(0).Rows(0).Item(0)

                                For Each formapagden In FormaPago.Denominaciones

                                    sConsulta = "INSERT INTO LOG_PAG_DEN (ID_LOG_PAG,DEN,IDI) VALUES (" & lIdLogFORMAPAG & ",N'" & DblQuote(formapagden.Denominacion) & " ','" & DblQuote(formapagden.Idioma) & "')"
                                    adoCom = New SqlCommand(sConsulta, cn)
                                    adoCom.Connection = cn
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    adoCom.ExecuteNonQuery()

                                Next


                            End If


                        Next

                    End If
                End If
            End If

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.CommandText = "COMMIT TRANSACTION"
            adoCom.ExecuteNonQuery()

            sConsulta = "SELECT FECACT FROM PAG WITH(NOLOCK) WHERE COD =N'" & sCodigo & "'"
            cm = New SqlCommand(sConsulta, cn)
            'cm.ExecuteNonQuery()
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            cm.CommandType = System.Data.CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                FormaPago.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If

            cn.Close()

        Catch ex As SqlException

            oError = TratarError(ex)
            adoCom.Connection = cn
            adoCom = New SqlCommand("ROLLBACK TRANSACTION", cn)
            adoCom.ExecuteNonQuery()
        End Try

        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCPagMod, "Cod: " & FormaPago.Codigo)
        End If
        Return oError

    End Function



    ''' <summary>
    ''' Elimina formas de pago de la BD
    ''' </summary>
    ''' <param name="Codigos">Codigos de las formas de pago a eliminar</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sMonedaCentral">Moneda central de la aplicacion</param>
    ''' <param name="Origen">Origen de integracion</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.FormasPagoRule.EliminarFormasPago
    ''' Tiempo máximo<1 sg</remarks>

    Public Function EliminarFormasPago(ByVal Codigos() As String, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal Origen As TiposDeDatos.OrigenIntegracion, ByVal sUsuario As String) As GSServerModel.GSException

        Dim i, j, k As Integer
        Dim bGuardar As Boolean
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim sCodFormaPago As String
        Dim iRes As Integer
        Dim ContRegBorrados As Integer

        Dim oIdiomas As New GSServerModel.Idiomas(True, "", "", True)
        Dim lIdLogFORMAPAG As Long

        Dim oDens As New GSServerModel.Multiidiomas(False, "", "", True)
        Dim oDen As New GSServerModel.Multiidioma(False, "", "", True)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        For i = 0 To Codigos.Count - 1
            bGuardar = True
            sCodFormaPago = DblQuote(Codigos(i).ToString())

            Try
                adoCom = New SqlCommand
                cn.Open()
                adoCom.Connection = cn
                adoCom.CommandText = "BEGIN TRANSACTION "
                adoCom.CommandType = System.Data.CommandType.Text
                adoCom.ExecuteNonQuery()
                'Exc = New Exception
                adoCom = New SqlCommand
                adoCom.Connection = cn

                'ALMACENAMOS EN BASE DE DATOS:
                If bGrabarLog Then ' primero recuperamos los datos de la moneda que se va a borra

                    sConsulta = "SELECT IDIOMA, DEN FROM PAG_DEN WITH(NOLOCK) WHERE PAG =N'" & sCodFormaPago & "'"
                    cm = New SqlCommand(sConsulta, cn)
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    cm.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(dr)
                    oDens.Clear()
                    If dr.Tables(0).Rows.Count > 0 Then
                        For j = 0 To dr.Tables(0).Rows.Count - 1
                            oDens.Add(dr.Tables(0).Rows(j).Item("IDIOMA"), dr.Tables(0).Rows(j).Item("DEN"), Now)
                        Next
                    End If


                End If


                sConsulta = "UPDATE COPIA_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & sCodFormaPago & "' AND COPIA_CAMPO_DEF IN (SELECT ID FROM FORM_CAMPO WHERE TIPO_CAMPO_GS=" & TipoCampoGS.FormaPago & ")"
                cm = New SqlCommand(sConsulta, cn)
                cm.ExecuteNonQuery()


                sConsulta = "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & sCodFormaPago & "' AND CAMPO_HIJO IN (SELECT COPIA_CAMPO.ID FROM COPIA_CAMPO INNER JOIN FORM_CAMPO ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.FormaPago & ")"
                cm = New SqlCommand(sConsulta, cn)
                cm.ExecuteNonQuery()


                'elimina de los formularios:
                sConsulta = "UPDATE FORM_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & sCodFormaPago & "' AND TIPO_CAMPO_GS=" & TipoCampoGS.FormaPago
                cm = New SqlCommand(sConsulta, cn)
                cm.ExecuteNonQuery()

                sConsulta = "UPDATE LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & sCodFormaPago & "' AND CAMPO_HIJO IN (SELECT ID FROM FORM_CAMPO WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.FormaPago & ")"
                cm = New SqlCommand(sConsulta, cn)
                cm.ExecuteNonQuery()


                sConsulta = "UPDATE PM_CAMPO_FAV  SET VALOR_TEXT = NULL FROM PM_CAMPO_FAV CF"
                sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = CF.CAMPO"
                sConsulta = sConsulta & " WHERE CF.VALOR_TEXT=N'" & sCodFormaPago & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.FormaPago
                cm = New SqlCommand(sConsulta, cn)
                cm.ExecuteNonQuery()

                'Elimina de las solicitudes favoritas las lineas de desglose con la forma de pago
                sConsulta = "UPDATE PM_LINEA_DESGLOSE_FAV SET VALOR_TEXT = NULL FROM PM_LINEA_DESGLOSE_FAV LDF"
                sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = LDF.CAMPO_HIJO"
                sConsulta = sConsulta & " WHERE LDF.VALOR_TEXT=N'" & sCodFormaPago & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.FormaPago
                cm = New SqlCommand(sConsulta, cn)
                cm.ExecuteNonQuery()

                ''MultiERP: Elimina de PAG_ERP
                sConsulta = "DELETE FROM PAG_ERP WHERE COD_GS=N'" & sCodFormaPago & "'"
                cm = New SqlCommand(sConsulta, cn)
                cm.ExecuteNonQuery()

                sConsulta = "DELETE FROM PAG_DEN WHERE PAG=N'" & sCodFormaPago & "'"
                cm = New SqlCommand(sConsulta, cn)
                cm.ExecuteNonQuery()

                sConsulta = "DELETE FROM PAG WHERE COD=N'" & sCodFormaPago & "'"
                cm = New SqlCommand(sConsulta, cn)
                ContRegBorrados = cm.ExecuteNonQuery()


                If bGrabarLog And ContRegBorrados > 0 Then

                    sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                    adoCom = New SqlCommand(sConsulta, cn)
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    adoCom.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = adoCom
                    da.Fill(dr)


                    Dim da2 = New SqlDataAdapter
                    Dim dr2 = New System.Data.DataSet

                    If dr.Tables(0).Rows.Count > 0 Then
                        For k = 0 To dr.Tables(0).Rows.Count - 1
                            sConsulta = "INSERT INTO LOG_PAG (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Baja & "',N'" & sCodFormaPago & "'"
                            sConsulta = sConsulta & ",'" & DblQuote(Origen) & "' ,'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(k).Item("COD")) & "')"

                            adoCom.CommandText = sConsulta
                            adoCom.CommandType = System.Data.CommandType.Text
                            adoCom.ExecuteNonQuery()


                            da2 = New SqlDataAdapter
                            dr2 = New System.Data.DataSet

                            adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_PAG ", cn)
                            adoCom.CommandType = System.Data.CommandType.Text
                            da2.SelectCommand = adoCom
                            da2.Fill(dr2)

                            If dr2.Tables(0).Rows.Count > 0 Then

                                lIdLogFORMAPAG = dr2.Tables(0).Rows(0).Item(0)

                                For Each oDen In oDens

                                    sConsulta = "INSERT INTO LOG_PAG_DEN (ID_LOG_PAG,DEN,IDI) VALUES (" & lIdLogFORMAPAG & ","
                                    sConsulta = sConsulta & "N'" & DblQuote(oDen.Denominacion) & "','" & DblQuote(oDen.Idioma) & "')"
                                    adoCom = New SqlCommand(sConsulta, cn)
                                    adoCom.Connection = cn
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    adoCom.ExecuteNonQuery()

                                Next


                            End If

                        Next

                    End If

                    RegistrarAccion(sUsuario, AccionesGS.ACCPagEli, "Cod: " & Codigos(i).ToString)

                End If
                cm = New SqlCommand("COMMIT TRANSACTION", cn)
                cm.ExecuteNonQuery()

            Catch ex As SqlException

                oError = TratarError(ex)
                If ex.Errors(0).Number = 547 Then
                    Select Case oError.ErrorId

                        Case 46
                            ' ERROR PORQUE ESTA RELACIONADA CON LA TABLA ITEM
                            sConsulta = "SELECT ITEM.ANYO,ITEM.PROCE,ITEM.ART,ITEM.DESCR FROM ITEM "
                            sConsulta = sConsulta & " WHERE ITEM.PAG=N'" & sCodFormaPago & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then

                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1) & "-" & dr.Tables(0).Rows(0).Item(3))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog

                        Case 82
                            ' ERROR PORQUE ESTA RELACIONADA CON LA TABLA PROCE
                            sConsulta = "SELECT PROCE.ANYO, PROCE.COD,PROCE.GMN1 FROM PROCE WHERE PROCE.PAG=N'" & sCodFormaPago & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then

                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(2) & "-" & dr.Tables(0).Rows(0).Item(1))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog

                        Case 90
                            ' ESTA RELACIONADA CON LA TABLA PROVE
                            sConsulta = "SELECT PROVE.COD, PROVE.DEN FROM PROVE WHERE PROVE.PAG=N'" & sCodFormaPago & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then

                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog

                        Case 66
                            ' ESTA RELACIONADA CON LA TABLA PARGEN DEF
                            sConsulta = "SELECT PARGEN_DEF.ID FROM PARGEN_DEF WHERE PARGEN_DEF.UNIDEF=N'" & sCodFormaPago & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then

                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog

                        Case 135
                            ' ESTA RELACIONADA CON LA TABLA PROCE_GRUPO
                            sConsulta = "SELECT ANYO,PROCE,COD FROM PROCE_GRUPO WHERE PAG=N'" & sCodFormaPago & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            If dr.Tables(0).Rows.Count > 0 Then

                                oError.Errores.Add(Codigos(i), oError.ErrorId, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1) & "-" & dr.Tables(0).Rows(0).Item(2))
                                iRes = iRes + 1
                            End If

                            GoTo NoLog

                        Case Else
                            ' ESTA RELACIONADA una tabla desconocida
                            oError.Errores.Add(Codigos(i), oError.ErrorId, i, "")
                            iRes = iRes + 1

                            GoTo NoLog

                    End Select

                End If
Nolog:
                cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
                cm.ExecuteNonQuery()
                Return (oError)
            End Try

        Next i

        Return (oError)

        cn.Dispose()
        da.Dispose()

    End Function


    ''' <summary>
    ''' Guarda en la tabla UPD_COD el cambio de codigo de formas de pago a realizar
    ''' </summary>
    ''' <param name="Codigo">Codigo actual de la forma de pago</param>
    ''' <param name="CodigoNuevo">Nuevo codigo</param>
    ''' <param name="Usuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.FormasPagoRule.CambioCodigoFormasPago
    ''' Tiempo máximo<1 sg</remarks>
    Public Function CambioCodigoFormaPago(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_CHANGE_CODES"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@ACCION", SqlDbType.NVarChar, 50)
            cm.Parameters("@ACCION").Value = "FORMAPAGO"
            cm.Parameters.Add("@COD", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD").Value = Codigo
            cm.Parameters.Add("@COD_NEW", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD_NEW").Value = CodigoNuevo
            cm.Parameters.Add("@USU", SqlDbType.NVarChar, 50)
            cm.Parameters("@USU").Value = Usuario

            cm.Parameters.Add("@RES", SqlDbType.Int)
            cm.Parameters("@RES").Direction = ParameterDirection.Output

            cn.Open()
            cm.ExecuteNonQuery()
            cn.Close()

            Return cm.Parameters("@RES").Value

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try

    End Function


#End Region

#Region "PaisesProvincias data access methods"

    ''' <summary>
    ''' Cargar los paises de la aplicacion con su provincias
    ''' </summary>
    ''' <param name="bMostrarIntegracion">Si se muestra el estado de la integracion</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <returns>Un Dataset con los paises y las provincias </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.PaisesRule.CargarTodasLosPaisesProvincias
    ''' Tiempo máximo<1 sg</remarks>

    Public Function CargarTodasLosPaisesProvincias(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_PAISES_PROVINCIAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@MOSTRARINTEGRACION", SqlDbType.TinyInt)
            cm.Parameters("@MOSTRARINTEGRACION").Value = IIf(bMostrarIntegracion, 1, 0)
            cm.Parameters.Add("@ENTIDADINTEGRACION", SqlDbType.Int)
            cm.Parameters("@ENTIDADINTEGRACION").Value = EntidadIntegracion.Pai
            cm.Parameters.Add("@ACCION", SqlDbType.NVarChar, 1)
            cm.Parameters("@ACCION").Value = Accion_CambioCodigo
            cm.Parameters.Add("@IDIOMA", SqlDbType.NVarChar, 3)
            cm.Parameters("@IDIOMA").Value = sIdioma


            da.SelectCommand = cm
            da.Fill(ds)

            Dim tblRelation As System.Data.DataRelation
            tblRelation = New System.Data.DataRelation("Hierarchy", ds.Tables(0).Columns("PAICOD"), ds.Tables(1).Columns("PROVIPAI"))
            ds.Relations.Add(tblRelation)

            cn.Close()

            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function


    ''' <summary>
    ''' Cargar las monedas para seleccionar la moneda del pais
    ''' </summary>
    ''' <param name="Idioma">Idioma</param>
    ''' <returns>Un Dataset con las monedas </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.PaisesRule.CargarTodasLasMonedasPais
    ''' Tiempo máximo<1 sg</remarks>
    Public Function CargarTodasLasMonedasPais(ByVal sIdioma As String) As System.Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_MONEDAS_PAIS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@IDIOMA", SqlDbType.NVarChar, 3)
            cm.Parameters("@IDIOMA").Value = sIdioma


            da.SelectCommand = cm
            da.Fill(ds)

            cn.Close()

            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Añade un pais a la BD
    ''' </summary>
    ''' <param name="pais">Pais</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.PaisesRule.AnyadirPais
    ''' Tiempo máximo<1 sg</remarks>

    Public Function AnyadirPais(ByRef Pais As GSServerModel.Pais, ByVal iInstWeb As Integer, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        ''Nota: Cuando se accede a BD de portal no se usa with (nolock) pq da problemas
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim lIdLogPAI As Long

        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN DISTRIBUTED TRAN"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()
        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandText = "SET XACT_ABORT ON"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()

        Dim sCodPai As String = DblQuote(Pais.Codigo)
        Dim paiden As New GSServerModel.Multiidioma(True, "", "", True)
        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        Try
            If (strToDBNull(Pais.Moneda.Codigo) Is System.DBNull.Value) Then
                sConsulta = " INSERT INTO PAI(COD) VALUES (N'" & sCodPai & "')"
            Else
                sConsulta = " INSERT INTO PAI(COD,MON) VALUES (N'" & sCodPai & "',N'" & DblQuote(Pais.Moneda.Codigo) & "')"
            End If

            For Each paiden In Pais.Denominaciones
                sConsulta = sConsulta & " INSERT INTO PAI_DEN (PAI,IDIOMA,DEN) VALUES ( N'"
                sConsulta = sConsulta & sCodPai & "','" & DblQuote(paiden.Idioma) & "',N'" & DblQuote(paiden.Denominacion) & "') "
            Next

            adoCom.CommandText = sConsulta
            adoCom.ExecuteNonQuery()


            Dim id As Integer


            If iInstWeb = Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb.ConPortal Then

                sConsulta = "SELECT MAX(ID) FROM " & FSP & "PAI "

                adoCom.CommandText = sConsulta
                da = New SqlDataAdapter
                dr = New System.Data.DataSet
                adoCom.CommandType = System.Data.CommandType.Text
                da.SelectCommand = adoCom
                da.Fill(dr)

                If dr.Tables(0).Rows.Count > 0 Then
                    id = dr.Tables(0).Rows(0).Item(0) + 1
                Else
                    id = 1
                End If


                sConsulta = " DECLARE @ID INTEGER "
                sConsulta = sConsulta & " DECLARE @IDPAIS INTEGER "
                sConsulta = sConsulta & " DECLARE @IDMON INTEGER "

                sConsulta = sConsulta & " SELECT @IDPAIS=MAX(ID) FROM " & FSP & "PAI "
                sConsulta = sConsulta & " IF @IDPAIS IS NULL  SET @IDPAIS=1 Else SET @IDPAIS=@IDPAIS+1 "
                If (strToDBNull(Pais.Moneda.Codigo) Is System.DBNull.Value) Then
                    sConsulta = sConsulta & " INSERT INTO " & FSP & "PAI (ID,COD) VALUES (@IDPAIS, N'" & sCodPai & "') "
                Else
                    sConsulta = sConsulta & " SET @IDMON=(SELECT ID FROM " & FSP & "MON WHERE COD=N'" & DblQuote(Pais.Moneda.Codigo) & "' ) "
                    sConsulta = sConsulta & " INSERT INTO " & FSP & "PAI (ID,COD,MON) VALUES (@IDPAIS, N'" & sCodPai & "',@IDMON) "
                End If
                For Each paiden In Pais.Denominaciones
                    sConsulta = sConsulta & " IF EXISTS(SELECT ID FROM " & FSP & "IDI WHERE COD='" & DblQuote(paiden.Idioma) & "')"
                    sConsulta = sConsulta & " BEGIN"
                    sConsulta = sConsulta & " SELECT @ID=ID FROM " & FSP & "IDI WHERE COD='" & DblQuote(paiden.Idioma) & "'"
                    sConsulta = sConsulta & " INSERT INTO " & FSP & "PAI_DEN (PAI,IDIOMA,DEN) VALUES (@IDPAIS,@ID,N'" & DblQuote(paiden.Denominacion) & "') "
                    sConsulta = sConsulta & " END"
                Next

                adoCom = New SqlCommand(sConsulta, cn)
                adoCom.ExecuteNonQuery()

            End If


            sConsulta = "SELECT FECACT FROM PAI WITH(NOLOCK) WHERE COD =N'" & sCodPai & "'"
            adoCom = New SqlCommand(sConsulta, cn)
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            adoCom.CommandType = System.Data.CommandType.Text
            da.SelectCommand = adoCom
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                Pais.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If



            If bGrabarLog Then

                sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                adoCom = New SqlCommand(sConsulta, cn)
                da = New SqlDataAdapter
                dr = New System.Data.DataSet
                adoCom.CommandType = System.Data.CommandType.Text
                da.SelectCommand = adoCom
                da.Fill(dr)


                Dim da2 = New SqlDataAdapter
                Dim dr2 = New System.Data.DataSet

                If dr.Tables(0).Rows.Count > 0 Then
                    For i = 0 To dr.Tables(0).Rows.Count - 1
                        sConsulta = " INSERT INTO LOG_PAI (ACCION, COD, MON, ORIGEN, USU, ERP) VALUES ('" & Accion_Alta & "',N'" & sCodPai & "'"
                        sConsulta = sConsulta & "," & UnicodeStrToSQLNULL(Pais.Moneda.Codigo) & "," & Pais.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "')"

                        adoCom.CommandText = sConsulta
                        adoCom.CommandType = System.Data.CommandType.Text
                        adoCom.ExecuteNonQuery()


                        da2 = New SqlDataAdapter
                        dr2 = New System.Data.DataSet

                        adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_PAI WITH (NOLOCK)", cn)
                        adoCom.CommandType = System.Data.CommandType.Text
                        da2.SelectCommand = adoCom
                        da2.Fill(dr2)

                        If dr2.Tables(0).Rows.Count > 0 Then

                            lIdLogPAI = dr2.Tables(0).Rows(0).Item(0)

                            For Each paiden In Pais.Denominaciones

                                sConsulta = "INSERT INTO LOG_PAI_DEN (ID_LOG_PAI,DEN,IDI) VALUES (" & lIdLogPAI & ",N'"
                                sConsulta = sConsulta & DblQuote(paiden.Denominacion) & "','" & DblQuote(paiden.Idioma) & "')"
                                adoCom = New SqlCommand(sConsulta, cn)
                                adoCom.Connection = cn
                                adoCom.CommandType = System.Data.CommandType.Text
                                adoCom.ExecuteNonQuery()

                            Next


                        End If


                    Next
                End If
            End If

            adoCom = New SqlCommand()
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            adoCom = New SqlCommand("COMMIT TRAN", cn)
            adoCom.ExecuteNonQuery()

            cn.Close()

        Catch ex As SqlException

            oError = TratarError(ex)
            adoCom = New SqlCommand()
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT ON"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()


        End Try
        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCPaiAnya, "Cod: " & Pais.Codigo)
        End If

        Return oError

    End Function

    ''' <summary>
    ''' Actualiza los cambios realizados en algun pais
    ''' </summary>
    ''' <param name="Pais">Pais en la que se ha producido el cambio</param>
    ''' <param name="bRealizarCambioCodigo">Si hay que hacer cambio de codigo</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.PaisesRule.ActualizarPais
    ''' Tiempo máximo<1 sg</remarks>

    Public Function ActualizarPais(ByRef Pais As GSServerModel.Pais, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        ''Nota: Cuando se accede a BD de portal no se usa with (nolock) pq da problemas
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""
        Dim lIdLogPAI As Long

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN DISTRIBUTED TRAN"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()
        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandText = "SET XACT_ABORT ON"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()
        'Exc = New Exception
        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandType = System.Data.CommandType.Text

        Dim sCodPai As String = DblQuote(Pais.Codigo)
        Dim paisden As New GSServerModel.Multiidioma(True, "", "", True)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)


        sConsulta = "SELECT FECACT FROM PAI WITH(NOLOCK) WHERE COD=N'" & sCodPai & "'"
        adoCom.CommandText = sConsulta

        da = New SqlDataAdapter
        dr = New System.Data.DataSet
        da.SelectCommand = adoCom
        da.Fill(dr)

        If dr.Tables(0).Rows.Count > 0 Then

            If Pais.FecAct <> dr.Tables(0).Rows(0).Item("FECACT") Then
                cn.Close()
                oError.Number = ErroresGS.TESInfActualModificada
                Return oError
                Exit Function
            End If
        Else
            oError.Number = ErroresGS.TESDatoEliminado
            oError.ErrorId = 6

        End If



        Try
            If Not bRealizarCambioCodigo Then

                sConsulta = ""
                For Each paiden In Pais.Denominaciones
                    sConsulta = sConsulta & " UPDATE PAI_DEN SET DEN=N'" & DblQuote(paiden.Denominacion) & "' WHERE PAI =N'" & sCodPai & "' AND IDIOMA='" & paiden.Idioma & "' "

                Next
                If (strToDBNull(Pais.Moneda.Codigo) Is System.DBNull.Value) Then
                    sConsulta = sConsulta & " UPDATE PAI SET MON=NULL WHERE COD=N'" & sCodPai & "' "
                Else
                    sConsulta = sConsulta & " UPDATE PAI SET MON=N'" & DblQuote(Pais.Moneda.Codigo) & "' WHERE COD=N'" & sCodPai & "' "
                End If

                adoCom.CommandText = sConsulta
                adoCom.ExecuteNonQuery()


                If iInstWeb = Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb.ConPortal Then

                    sConsulta = "DECLARE @IDPAI INTEGER "
                    sConsulta = sConsulta & " DECLARE @IDMON INTEGER "
                    sConsulta = sConsulta & " DECLARE @ID INTEGER "
                    sConsulta = sConsulta & " SELECT @IDPAI=ID FROM " & FSP & "PAI WHERE COD= N'" & sCodPai & "' "

                    For Each paisden In Pais.Denominaciones
                        sConsulta = sConsulta & " SELECT @ID=ID FROM " & FSP & "IDI WHERE COD='" & DblQuote(paisden.Idioma) & "'"
                        sConsulta = sConsulta & " UPDATE " & FSP & "PAI_DEN SET "
                        sConsulta = sConsulta & " DEN='" & paisden.Denominacion & "' WHERE PAI = @IDPAI AND IDIOMA=@ID"
                    Next


                    If Not (strToDBNull(Pais.Moneda.Codigo) Is System.DBNull.Value) Then
                        sConsulta = sConsulta & " SELECT @IDMON=ID FROM " & FSP & "MON WHERE COD= N'" & DblQuote(Pais.Moneda.Codigo) & "' "
                        sConsulta = sConsulta & " UPDATE " & FSP & "PAI SET MON=@IDMON WHERE COD=N'" & sCodPai & "' "
                    Else
                        sConsulta = sConsulta & " UPDATE " & FSP & "PAI SET MON=NULL WHERE COD=N'" & sCodPai & "' "
                    End If

                    adoCom = New SqlCommand
                    adoCom.Connection = cn
                    adoCom.CommandType = System.Data.CommandType.Text
                    adoCom.CommandText = sConsulta
                    adoCom.ExecuteNonQuery()

                End If


                If bGrabarLog Then

                    sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                    adoCom = New SqlCommand(sConsulta, cn)
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    adoCom.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = adoCom
                    da.Fill(dr)


                    Dim da2 = New SqlDataAdapter
                    Dim dr2 = New System.Data.DataSet

                    If dr.Tables(0).Rows.Count > 0 Then
                        For i = 0 To dr.Tables(0).Rows.Count - 1
                            sConsulta = "INSERT INTO LOG_PAI (ACCION, COD, MON, ORIGEN, USU, ERP) VALUES ('" & Accion_Modificacion & "',N'" & sCodPai & "'"
                            sConsulta = sConsulta & "," & UnicodeStrToSQLNULL(Pais.Moneda.Codigo) & "," & Pais.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "')"

                            adoCom.CommandText = sConsulta
                            adoCom.CommandType = System.Data.CommandType.Text
                            adoCom.ExecuteNonQuery()


                            da2 = New SqlDataAdapter
                            dr2 = New System.Data.DataSet

                            adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_PAI ", cn)
                            adoCom.CommandType = System.Data.CommandType.Text
                            da2.SelectCommand = adoCom
                            da2.Fill(dr2)

                            If dr2.Tables(0).Rows.Count > 0 Then

                                lIdLogPAI = dr2.Tables(0).Rows(0).Item(0)

                                For Each paisden In Pais.Denominaciones

                                    sConsulta = "INSERT INTO LOG_PAI_DEN (ID_LOG_PAI,DEN,IDI) VALUES (" & lIdLogPAI & ",N'" & DblQuote(paisden.Denominacion) & " ','" & DblQuote(paisden.Idioma) & "')"
                                    adoCom = New SqlCommand(sConsulta, cn)
                                    adoCom.Connection = cn
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    adoCom.ExecuteNonQuery()

                                Next


                            End If


                        Next

                    End If
                End If
            End If

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.CommandText = "COMMIT TRAN"
            adoCom.ExecuteNonQuery()

            sConsulta = "SELECT FECACT FROM PAI WITH(NOLOCK) WHERE COD =N'" & sCodPai & "'"
            cm = New SqlCommand(sConsulta, cn)
            'cm.ExecuteNonQuery()
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            cm.CommandType = System.Data.CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                Pais.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If

            cn.Close()
        Catch ex As SqlException

            oError = TratarError(ex)

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT ON"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            Return oError

        End Try

        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCPaiMod, "Cod: " & Pais.Codigo)
        End If
        Return oError

    End Function

    ''' <summary>
    ''' Guarda en la tabla UPD_COD el cambio de codigo de pais a realizar
    ''' </summary>
    ''' <param name="Codigo">Codigo actual del pais</param>
    ''' <param name="CodigoNuevo">Nuevo codigo</param>
    ''' <param name="Usuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.PaisesRule.CambioCodigoPais
    ''' Tiempo máximo<1 sg</remarks>

    Public Function CambioCodigoPais(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_CHANGE_CODES"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@ACCION", SqlDbType.NVarChar, 50)
            cm.Parameters("@ACCION").Value = "Pais"
            cm.Parameters.Add("@COD", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD").Value = Codigo
            cm.Parameters.Add("@COD_NEW", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD_NEW").Value = CodigoNuevo
            cm.Parameters.Add("@USU", SqlDbType.NVarChar, 50)
            cm.Parameters("@USU").Value = Usuario

            cm.Parameters.Add("@RES", SqlDbType.Int)
            cm.Parameters("@RES").Direction = ParameterDirection.Output

            cn.Open()
            cm.ExecuteNonQuery()
            cn.Close()

            Return cm.Parameters("@RES").Value

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Añade una provincia a la BD
    ''' </summary>
    ''' <param name="Provincia">Provincia en la que se ha producido el cambio</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.PaisesRule.AnyadirProvincia
    ''' Tiempo máximo<1 sg</remarks>

    Public Function AnyadirProvincia(ByRef Provincia As GSServerModel.Provincia, ByVal iInstWeb As Integer, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException

        ''Nota: Cuando se accede a BD de portal no se usa with (nolock) pq da problemas
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim lIdLogPROVI As Long

        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN DISTRIBUTED TRAN"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()
        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandText = "SET XACT_ABORT ON"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()


        Dim providen As New GSServerModel.Multiidioma(True, "", "", True)
        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        Try
            sConsulta = " INSERT INTO PROVI(PAI,COD) VALUES (N'"
            sConsulta = sConsulta & DblQuote(Provincia.Pais.Codigo) & "',N'" & DblQuote(Provincia.Codigo) & "')"

            For Each providen In Provincia.Denominaciones
                sConsulta = sConsulta & " INSERT INTO PROVI_DEN (PAI,PROVI,IDIOMA,DEN) VALUES ( N'"
                sConsulta = sConsulta & DblQuote(Provincia.Pais.Codigo) & "',N'" & DblQuote(Provincia.Codigo) & "','" & DblQuote(providen.Idioma) & "',N'" & DblQuote(providen.Denominacion) & "') "
            Next

            adoCom.CommandText = sConsulta
            adoCom.ExecuteNonQuery()


            Dim id As Integer


            If iInstWeb = Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb.ConPortal Then

                sConsulta = "SELECT MAX(ID) FROM " & FSP & "PROVI "

                adoCom.CommandText = sConsulta
                da = New SqlDataAdapter
                dr = New System.Data.DataSet
                adoCom.CommandType = System.Data.CommandType.Text
                da.SelectCommand = adoCom
                da.Fill(dr)

                If dr.Tables(0).Rows.Count > 0 Then
                    id = dr.Tables(0).Rows(0).Item(0) + 1
                Else
                    id = 1
                End If



                sConsulta = " DECLARE @ID INTEGER "
                sConsulta = sConsulta & " DECLARE @IDPAIS INTEGER "
                sConsulta = sConsulta & " DECLARE @IDPROVI INTEGER "

                sConsulta = sConsulta & " SELECT @IDPAIS=ID FROM " & FSP & " PAI WHERE COD= N'" & DblQuote(Provincia.Pais.Codigo) & "' "
                sConsulta = sConsulta & " SELECT @IDPROVI=MAX(ID) FROM " & FSP & " PROVI WHERE PAI=@IDPAIS "
                sConsulta = sConsulta & " IF @IDPROVI IS NULL  SET @IDPROVI=1 Else SET @IDPROVI=@IDPROVI+1 "
                sConsulta = sConsulta & " INSERT INTO " & FSP & "PROVI (PAI,ID,COD) VALUES (@IDPAIS,@IDPROVI, N'" & DblQuote(Provincia.Codigo) & "') "
                For Each providen In Provincia.Denominaciones
                    sConsulta = sConsulta & " SELECT @ID=ID FROM " & FSP & "IDI WHERE COD='" & DblQuote(providen.Idioma) & "'"
                    sConsulta = sConsulta & " INSERT INTO " & FSP & "PROVI_DEN (PAI,PROVI,IDIOMA,DEN) VALUES (@IDPAIS,@IDPROVI,@ID,N'" & DblQuote(providen.Denominacion) & "') "
                Next

                adoCom = New SqlCommand(sConsulta, cn)
                adoCom.ExecuteNonQuery()

            End If


            sConsulta = "SELECT FECACT FROM PROVI WITH(NOLOCK) WHERE PAI = N'" & DblQuote(Provincia.Pais.Codigo) & "' AND COD =N'" & DblQuote(Provincia.Codigo) & "'"
            adoCom = New SqlCommand(sConsulta, cn)
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            adoCom.CommandType = System.Data.CommandType.Text
            da.SelectCommand = adoCom
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                Provincia.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If



            If bGrabarLog Then

                sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                adoCom = New SqlCommand(sConsulta, cn)
                da = New SqlDataAdapter
                dr = New System.Data.DataSet
                adoCom.CommandType = System.Data.CommandType.Text
                da.SelectCommand = adoCom
                da.Fill(dr)


                Dim da2 = New SqlDataAdapter
                Dim dr2 = New System.Data.DataSet

                If dr.Tables(0).Rows.Count > 0 Then
                    For i = 0 To dr.Tables(0).Rows.Count - 1
                        sConsulta = " INSERT INTO LOG_PROVI (ACCION, PAI,COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Alta & "',N'" & DblQuote(Provincia.Codigo) & "'"
                        sConsulta = sConsulta & ",N" & StrToSQLNULL(Provincia.Codigo) & "," & Provincia.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "')"

                        adoCom.CommandText = sConsulta
                        adoCom.CommandType = System.Data.CommandType.Text
                        adoCom.ExecuteNonQuery()


                        da2 = New SqlDataAdapter
                        dr2 = New System.Data.DataSet

                        adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_PROVI WITH (NOLOCK)", cn)
                        adoCom.CommandType = System.Data.CommandType.Text
                        da2.SelectCommand = adoCom
                        da2.Fill(dr2)

                        If dr2.Tables(0).Rows.Count > 0 Then

                            lIdLogPROVI = dr2.Tables(0).Rows(0).Item(0)

                            For Each providen In Provincia.Denominaciones

                                sConsulta = "INSERT INTO LOG_PROVI_DEN (ID_LOG_PROVI,DEN,IDI) VALUES (" & lIdLogPROVI & ","
                                sConsulta = sConsulta & "N'" & DblQuote(providen.Denominacion) & "','" & DblQuote(providen.Idioma) & "')"
                                adoCom = New SqlCommand(sConsulta, cn)
                                adoCom.Connection = cn
                                adoCom.CommandType = System.Data.CommandType.Text
                                adoCom.ExecuteNonQuery()

                            Next


                        End If


                    Next
                End If
            End If

            adoCom = New SqlCommand()
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            adoCom = New SqlCommand("COMMIT TRAN", cn)
            adoCom.ExecuteNonQuery()

            cn.Close()

        Catch ex As SqlException

            oError = TratarError(ex)
            adoCom = New SqlCommand()
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

        End Try
        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCProviAnya, "Cod: " & Provincia.Codigo)
        End If

        Return oError

    End Function


    ''' <summary>
    ''' Actualiza los cambios realizados en alguna provincia
    ''' </summary>
    ''' <param name="Provincia">Provincia en la que se ha producido el cambio</param>
    ''' <param name="bRealizarCambioCodigo">Si hay que hacer cambio de codigo</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.PaisesRule.Actualizarprovincia
    ''' Tiempo máximo<1 sg</remarks>

    Public Function ActualizarProvincia(ByRef Provincia As GSServerModel.Provincia, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        ''Nota: Cuando se accede a BD de portal no se usa with (nolock) pq da problemas
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""
        Dim lIdLogPROVI As Long

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        adoCom = New SqlCommand
        cn.Open()
        adoCom.Connection = cn
        adoCom.CommandText = "BEGIN DISTRIBUTED TRAN"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()
        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandText = "SET XACT_ABORT ON"
        adoCom.CommandType = System.Data.CommandType.Text
        adoCom.ExecuteNonQuery()
        adoCom = New SqlCommand
        adoCom.Connection = cn
        adoCom.CommandType = System.Data.CommandType.Text

        Dim providen As New GSServerModel.Multiidioma(True, "", "", True)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        sConsulta = "SELECT FECACT FROM PROVI WITH(NOLOCK) WHERE PAI=N'" & DblQuote(Provincia.Pais.Codigo) & "' AND COD=N'" & DblQuote(Provincia.Codigo) & "'"
        adoCom.CommandText = sConsulta

        da = New SqlDataAdapter
        dr = New System.Data.DataSet
        da.SelectCommand = adoCom
        da.Fill(dr)

        If dr.Tables(0).Rows.Count > 0 Then

            If Provincia.FecAct <> dr.Tables(0).Rows(0).Item("FECACT") Then
                cn.Close()
                oError.Number = ErroresGS.TESInfActualModificada
                Return oError
                Exit Function
            End If
        Else
            oError.Number = ErroresGS.TESDatoEliminado
            oError.ErrorId = 7

        End If



        Try
            If Not bRealizarCambioCodigo Then

                sConsulta = ""
                For Each providen In Provincia.Denominaciones
                    sConsulta = sConsulta & " UPDATE PROVI_DEN SET "
                    sConsulta = sConsulta & " IDIOMA = '" & providen.Idioma & "', DEN=N'" & providen.Denominacion & "' WHERE PAI =N'" & Provincia.Pais.Codigo & "' AND PROVI = N'" & Provincia.Codigo & "' AND IDIOMA='" & providen.Idioma & "' "
                Next

                adoCom.CommandText = sConsulta
                adoCom.ExecuteNonQuery()


                If iInstWeb = Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb.ConPortal Then

                    sConsulta = " DECLARE @ID INTEGER "
                    sConsulta = sConsulta & " DECLARE @IDPAI INTEGER "
                    sConsulta = sConsulta & " DECLARE @IDPROVI INTEGER "

                    sConsulta = sConsulta & " SELECT @IDPAI=ID FROM " & FSP & "PAI WHERE COD=N'" & DblQuote(Provincia.Pais.Codigo) & "'"
                    sConsulta = sConsulta & " SELECT @IDPROVI=ID FROM " & FSP & "PROVI WHERE COD=N'" & DblQuote(Provincia.Codigo) & "'"

                    For Each providen In Provincia.Denominaciones
                        sConsulta = sConsulta & " SELECT @ID=ID FROM " & FSP & "IDI WHERE COD='" & DblQuote(providen.Idioma) & "'"
                        sConsulta = sConsulta & " UPDATE " & FSP & "PROVI_DEN SET "
                        sConsulta = sConsulta & " DEN=N'" & providen.Denominacion & "' WHERE PAI = @IDPAI AND PROVI= @IDPROVI AND IDIOMA=@ID "
                    Next


                    'sConsulta = sConsulta & " UPDATE " & FSP & "PAI SET MON=@IDMON WHERE COD='" & Pais.Codigo & "' "

                    adoCom = New SqlCommand
                    adoCom.Connection = cn
                    adoCom.CommandType = System.Data.CommandType.Text
                    adoCom.CommandText = sConsulta
                    adoCom.ExecuteNonQuery()

                End If


                If bGrabarLog Then

                    sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                    adoCom = New SqlCommand(sConsulta, cn)
                    da = New SqlDataAdapter
                    dr = New System.Data.DataSet
                    adoCom.CommandType = System.Data.CommandType.Text
                    da.SelectCommand = adoCom
                    da.Fill(dr)


                    Dim da2 = New SqlDataAdapter
                    Dim dr2 = New System.Data.DataSet

                    If dr.Tables(0).Rows.Count > 0 Then
                        For i = 0 To dr.Tables(0).Rows.Count - 1
                            sConsulta = "INSERT INTO LOG_PROVI (ACCION, COD, PAI, ORIGEN, USU, ERP) VALUES ('" & Accion_Modificacion & "',N'" & DblQuote(Provincia.Codigo) & "'"
                            sConsulta = sConsulta & ",N'" & DblQuote(Provincia.Pais.Codigo) & "'," & Provincia.Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(i).Item("COD")) & "')"

                            adoCom.CommandText = sConsulta
                            adoCom.CommandType = System.Data.CommandType.Text
                            adoCom.ExecuteNonQuery()


                            da2 = New SqlDataAdapter
                            dr2 = New System.Data.DataSet

                            adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_PROVI ", cn)
                            adoCom.CommandType = System.Data.CommandType.Text
                            da2.SelectCommand = adoCom
                            da2.Fill(dr2)

                            If dr2.Tables(0).Rows.Count > 0 Then

                                lIdLogPROVI = dr2.Tables(0).Rows(0).Item(0)

                                For Each providen In Provincia.Denominaciones

                                    sConsulta = "INSERT INTO LOG_PROVI_DEN (ID_LOG_PROVI,DEN,IDI) VALUES (" & lIdLogPROVI & ",N'" & DblQuote(providen.Denominacion) & " ','" & DblQuote(providen.Idioma) & "')"
                                    adoCom = New SqlCommand(sConsulta, cn)
                                    adoCom.Connection = cn
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    adoCom.ExecuteNonQuery()

                                Next


                            End If


                        Next

                    End If
                End If
            End If

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.CommandText = "COMMIT TRAN"
            adoCom.ExecuteNonQuery()

            sConsulta = "SELECT FECACT FROM PROVI WITH(NOLOCK) WHERE COD =N'" & DblQuote(Provincia.Codigo) & "'"
            cm = New SqlCommand(sConsulta, cn)
            'cm.ExecuteNonQuery()
            da = New SqlDataAdapter
            dr = New System.Data.DataSet
            cm.CommandType = System.Data.CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                Provincia.FecAct = dr.Tables(0).Rows(0).Item(0)
            End If

            cn.Close()

        Catch ex As SqlException

            oError = TratarError(ex)

            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT OFF"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()

            Return oError

        End Try

        If oError.Number = ErroresGS.TESnoerror And bGrabarLog Then
            RegistrarAccion(sUsuario, AccionesGS.ACCProviMod, "Cod: " & Provincia.Codigo)
        End If
        Return oError

    End Function


    ''' <summary>
    ''' Guarda en la tabla UPD_COD el cambio de codigo de provincia a realizar
    ''' </summary>
    ''' <param name="Codigo">Codigo actual de la provincia</param>
    ''' <param name="CodigoNuevo">Nuevo codigo</param>
    ''' <param name="Usuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.PaisesRule.CambioCodigoprovincia
    ''' Tiempo máximo<1 sg</remarks>

    Public Function CambioCodigoProvincia(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String, ByVal sPais As String) As Integer

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_CHANGE_CODES"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@ACCION", SqlDbType.NVarChar, 50)
            cm.Parameters("@ACCION").Value = "Provincia"
            cm.Parameters.Add("@COD", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD").Value = Codigo
            cm.Parameters.Add("@COD_NEW", SqlDbType.NVarChar, 50)
            cm.Parameters("@COD_NEW").Value = CodigoNuevo
            cm.Parameters.Add("@USU", SqlDbType.NVarChar, 50)
            cm.Parameters("@USU").Value = Usuario

            cm.Parameters.Add("@RES", SqlDbType.Int)
            cm.Parameters("@RES").Direction = ParameterDirection.Output

            cm.Parameters.Add("@AUX", SqlDbType.NVarChar, 100)
            cm.Parameters("@AUX").Value = sPais


            cn.Open()
            cm.ExecuteNonQuery()
            cn.Close()

            Return cm.Parameters("@RES").Value

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try

    End Function




    ''' <summary>
    ''' Elimina provincias de la BD
    ''' </summary>
    ''' <param name="CodigosProvincias">Codigos de las provincias a eliminar</param>
    ''' <param name="CodigosPaisProvi">Codigos de paises de las provincias</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sMonedaCentral">Moneda central de la aplicacion</param>
    ''' <param name="Origen">Origen de integracion</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.PaisesRule.EliminarProvincias
    ''' Tiempo máximo<1 sg</remarks>

    Public Function EliminarProvincias(ByVal CodigosProvincias() As String, ByVal CodigosPaisProvi() As String, ByVal iInstWeb As Integer, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal Origen As TiposDeDatos.OrigenIntegracion, ByVal sUsuario As String, ByVal sIdioma As String) As GSServerModel.GSException
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        Try
            adoCom = New SqlCommand
            cn.Open()
            adoCom.Connection = cn
            adoCom.CommandText = "BEGIN DISTRIBUTED TRAN "
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()
            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT ON"
            adoCom.CommandType = System.Data.CommandType.Text


            oError = EliminarProvinciasBD(CodigosProvincias, CodigosPaisProvi, iInstWeb, bGrabarLog, FSP, Origen, sUsuario, sIdioma, cn)

            cm = New SqlCommand("SET XACT_ABORT OFF", cn)
            cm.ExecuteNonQuery()
            cm = New SqlCommand("COMMIT TRANSACTION", cn)
            cm.ExecuteNonQuery()
            cn.Close()


        Catch ex As SqlException
            oError = TratarError(ex)
            cm = New SqlCommand("SET XACT_ABORT OFF", cn)
            cm.ExecuteNonQuery()
        Finally
            cn.Dispose()
        End Try

        Return oError



    End Function


    ''' <summary>
    ''' Elimina provincias de la BD
    ''' </summary>
    ''' <param name="CodigosProvincias">Codigos de las provincias a eliminar</param>
    ''' <param name="CodigosPaisProvi">Codigos de paises de las provincias</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sMonedaCentral">Moneda central de la aplicacion</param>
    ''' <param name="Origen">Origen de integracion</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: EliminarProvincias
    ''' Tiempo máximo<1 sg</remarks>
    Public Function EliminarProvinciasBD(ByVal CodigosProvincias() As String, ByVal CodigosPaisProvi() As String, ByVal iInstWeb As Integer, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal Origen As TiposDeDatos.OrigenIntegracion, ByVal sUsuario As String, ByVal sIdioma As String, ByRef cn As SqlConnection) As GSServerModel.GSException
        ''Nota: Cuando se accede a BD de portal no se usa with (nolock) pq da problemas
        Dim i, j, k As Integer
        Dim bGuardar As Boolean
        Dim sConsulta As String
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim sCodProvi As String
        Dim iRes As Integer
        Dim ContRegBorrados As Integer

        Dim oIdiomas As New GSServerModel.Idiomas(True, "", "", True)
        Dim dblEquiv As Double
        Dim lIdLogPROVI As Long
        Dim lPais As Long
        Dim oDens As New GSServerModel.Multiidiomas(False, "", "", True)
        Dim oDen As New GSServerModel.Multiidioma(False, "", "", True)

        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror

        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        Try

            For i = 0 To CodigosProvincias.Count - 1
                bGuardar = True
                sCodProvi = CodigosProvincias(i).ToString()
                adoCom = New SqlCommand
                adoCom.Connection = cn


                If bGuardar = True Then

                    If iInstWeb = TipoInstWeb.ConPortal Then

                        sConsulta = "SELECT ID FROM " & FSP & "PAI WHERE COD=N'" & DblQuote(CodigosPaisProvi(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)

                        If dr.Tables(0).Rows.Count > 0 Then
                            lPais = dr.Tables(0).Rows(0).Item(0)
                            sConsulta = " SELECT * FROM " & FSP & "CIAS WHERE PAI=" & lPais
                            sConsulta = sConsulta & " AND PROVI=(SELECT ID FROM " & FSP & "PROVI WITH(NOLOCK) WHERE PAI=" & lPais & " AND COD=N'" & DblQuote(CodigosProvincias(i)) & "')"

                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)

                            If dr.Tables(0).Rows.Count > 0 Then
                                oError.Errores.Add(CodigosProvincias(i), 0, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1))
                                iRes = iRes + 1
                                bGuardar = False
                            End If
                        End If


                    End If

                    If bGuardar = True Then
                        sConsulta = "SELECT DEST.COD, DEST.DEN_" & sIdioma & " DEN FROM DEST WITH(NOLOCK) WHERE DEST.PROVI=N'" & DblQuote(CodigosProvincias(i)) & "' AND DEST.PAI =N'" & DblQuote(CodigosPaisProvi(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)

                        If dr.Tables(0).Rows.Count > 0 Then
                            oError.Errores.Add(CodigosProvincias(i), 36, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1))
                            iRes = iRes + 1
                            bGuardar = False
                        End If


                    End If

                    If bGuardar = True Then
                        sConsulta = "SELECT PROVE.COD, PROVE.DEN FROM PROVE WITH(NOLOCK) WHERE PROVE.PROVI=N'" & DblQuote(CodigosProvincias(i)) & "' AND PROVE.PAI =N'" & DblQuote(CodigosPaisProvi(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)

                        If dr.Tables(0).Rows.Count > 0 Then
                            oError.Errores.Add(CodigosProvincias(i), 90, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1))
                            iRes = iRes + 1
                            bGuardar = False
                        End If


                    End If

                    If bGuardar = True Then
                        sConsulta = "SELECT PARGEN_DEF.ID FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.PROVIDEF=N'" & DblQuote(CodigosProvincias(i)) & "' AND PARGEN_DEF.PAIDEF =N'" & DblQuote(CodigosPaisProvi(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)

                        If dr.Tables(0).Rows.Count > 0 Then
                            oError.Errores.Add(CodigosProvincias(i), 66, i, dr.Tables(0).Rows(0).Item(0))
                            iRes = iRes + 1
                            bGuardar = False
                        End If


                    End If


                    If bGuardar = True Then

                        If bGrabarLog Then ' primero recuperamos los datos de la moneda que se va a borra

                            sConsulta = "SELECT IDIOMA, DEN FROM PROVI_DEN WITH(NOLOCK) WHERE PROVI =N'" & DblQuote(CodigosPaisProvi(i)) & "' AND PROVI =N'" & DblQuote(CodigosProvincias(i)) & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            oDens.Clear()
                            If dr.Tables(0).Rows.Count > 0 Then
                                For j = 0 To dr.Tables(0).Rows.Count - 1
                                    oDens.Add(dr.Tables(0).Rows(j).Item("IDIOMA"), dr.Tables(0).Rows(j).Item("DEN"), Now)
                                Next
                            End If


                        End If


                        sConsulta = "UPDATE COPIA_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & DblQuote(CodigosProvincias(i)) & "' AND COPIA_CAMPO_DEF IN (SELECT ID FROM FORM_CAMPO WHERE TIPO_CAMPO_GS=" & TipoCampoGS.Provincia & ")"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()


                        sConsulta = "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & DblQuote(CodigosProvincias(i)) & "' AND CAMPO_HIJO IN (SELECT COPIA_CAMPO.ID FROM COPIA_CAMPO INNER JOIN FORM_CAMPO ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Provincia & ")"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()


                        'elimina de los formularios:
                        sConsulta = "UPDATE FORM_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & DblQuote(CodigosProvincias(i)) & "' AND TIPO_CAMPO_GS=" & TipoCampoGS.Provincia
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()


                        sConsulta = "UPDATE LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & DblQuote(CodigosProvincias(i)) & "' AND CAMPO_HIJO IN (SELECT ID FROM FORM_CAMPO WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Provincia & ")"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()

                        'Elimina de las solicitudes favoritas los campos con la Provincia
                        sConsulta = "UPDATE PM_CAMPO_FAV  SET VALOR_TEXT = NULL FROM PM_CAMPO_FAV CF"
                        sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = CF.CAMPO"
                        sConsulta = sConsulta & " WHERE CF.VALOR_TEXT=N'" & DblQuote(CodigosProvincias(i)) & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.Provincia
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()


                        'Elimina de las solicitudes favoritas las lineas de desglose con la Provincia
                        sConsulta = "UPDATE PM_LINEA_DESGLOSE_FAV SET VALOR_TEXT = NULL FROM PM_LINEA_DESGLOSE_FAV LDF"
                        sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = LDF.CAMPO_HIJO"
                        sConsulta = sConsulta & " WHERE LDF.VALOR_TEXT=N'" & DblQuote(CodigosProvincias(i)) & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.Provincia
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()


                        ''MultiERP: Elimina de PROVI_ERP
                        sConsulta = "DELETE FROM PROVI_ERP WHERE COD_GS=N'" & DblQuote(CodigosProvincias(i)) & "' AND PAI_GS =N'" & DblQuote(CodigosPaisProvi(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()

                        sConsulta = "DELETE FROM PROVI_DEN WHERE PROVI=N'" & DblQuote(CodigosProvincias(i)) & "' AND PAI =N'" & DblQuote(CodigosPaisProvi(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()

                        sConsulta = "DELETE FROM PROVI WHERE COD=N'" & DblQuote(CodigosProvincias(i)) & "' AND PAI =N'" & DblQuote(CodigosPaisProvi(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()


                        If iInstWeb = TipoInstWeb.ConPortal Then

                            cm = New SqlCommand
                            cm.Connection = cn
                            cm.CommandText = FSP & "SP_ELIM_PROVI"
                            cm.CommandType = CommandType.StoredProcedure

                            cm.CommandType = CommandType.StoredProcedure
                            cm.Parameters.Add("@PAI_COD", SqlDbType.NVarChar, 50)
                            cm.Parameters("@PAI_COD").Value = CodigosPaisProvi(i)
                            cm.Parameters.Add("@PROVI_COD", SqlDbType.NVarChar, 50)
                            cm.Parameters("@PROVI_COD").Value = CodigosProvincias(i)

                            cm.ExecuteNonQuery()

                        End If


                        If bGrabarLog And ContRegBorrados > 0 Then

                            sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                            adoCom = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            adoCom.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = adoCom
                            da.Fill(dr)


                            Dim da2 = New SqlDataAdapter
                            Dim dr2 = New System.Data.DataSet

                            If dr.Tables(0).Rows.Count > 0 Then
                                For k = 0 To dr.Tables(0).Rows.Count - 1
                                    sConsulta = "INSERT INTO LOG_PROVI (ACCION,PAI, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Baja & "',N'" & DblQuote(CodigosPaisProvi(i)) & "'"
                                    sConsulta = sConsulta & ",N'" & DblQuote(CodigosProvincias(i)) & "'," & Origen & ",'" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(k).Item("COD")) & "')"

                                    adoCom.CommandText = sConsulta
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    adoCom.ExecuteNonQuery()


                                    da2 = New SqlDataAdapter
                                    dr2 = New System.Data.DataSet

                                    adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_PROVI ", cn)
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    da2.SelectCommand = adoCom
                                    da2.Fill(dr2)

                                    If dr2.Tables(0).Rows.Count > 0 Then

                                        lIdLogPROVI = dr2.Tables(0).Rows(0).Item(0)

                                        For Each oDen In oDens

                                            sConsulta = "INSERT INTO LOG_PROVI_DEN (ID_LOG_PROVI,DEN,IDI) VALUES (" & lIdLogPROVI & ","
                                            sConsulta = sConsulta & "N'" & DblQuote(oDen.Denominacion) & "','" & DblQuote(oDen.Idioma) & "')"
                                            adoCom = New SqlCommand(sConsulta, cn)
                                            adoCom.Connection = cn
                                            adoCom.CommandType = System.Data.CommandType.Text
                                            adoCom.ExecuteNonQuery()

                                        Next


                                    End If


                                Next

                            End If


                            RegistrarAccion(sUsuario, AccionesGS.ACCProviEli, "Cod: " & CodigosProvincias(i) & "-" & CodigosPaisProvi(i))

                        End If


                    End If

                End If

            Next i

            If iRes > 0 Then
                oError.Number = Fullstep.FSNLibrary.TiposDeDatos.ErroresGS.TESImposibleEliminar
            End If


        Catch ex As SqlException
            oError = TratarError(ex)
        Finally
            da.Dispose()
        End Try

        Return oError

    End Function


    ''' <summary>
    ''' Elimina paises de la BD
    ''' </summary>
    ''' <param name="CodigosPaises">Codigos de paises a eliminar</param>
    ''' <param name="iInstweb">Si hay portal</param>
    ''' <param name="bGrabarLog">Si se graba en el log</param>
    ''' <param name="FSP">cadena con el path de la bd del portal</param>
    ''' <param name="sMonedaCentral">Moneda central de la aplicacion</param>
    ''' <param name="Origen">Origen de integracion</param>
    ''' <param name="sUsuario">usuario que hace el cambio</param>
    ''' <returns>Error si se produce </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.PaisesRule.Eliminarpaises
    ''' Tiempo máximo<1 sg</remarks>

    Public Function EliminarPaises(ByVal CodigosPaises() As String, ByVal iInstWeb As Integer, ByVal bGrabarLog As Boolean, ByVal FSP As String, ByVal Origen As TiposDeDatos.OrigenIntegracion, ByVal sUsuario As String, ByVal sIdioma As String) As GSServerModel.GSException
        ''Nota: Cuando se accede a BD de portal no se usa with (nolock) pq da problemas
        Dim i, j, k As Integer
        Dim bGuardar As Boolean
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New System.Data.DataSet
        Dim PwdEncriptado As String = ""

        Dim adoCom As SqlCommand
        Dim da As New SqlDataAdapter
        Dim sCodPai As String
        Dim sCodMon As String = ""
        Dim iRes As Integer
        Dim ContRegBorrados As Integer

        Dim oIdiomas As New GSServerModel.Idiomas(True, "", "", True)
        Dim lIdLogPAI As Long
        Dim oDens As New GSServerModel.Multiidiomas(False, "", "", True)
        Dim oDen As New GSServerModel.Multiidioma(False, "", "", True)

        Dim sCodigosProvi() As String
        Dim sCodigosPaisProvi() As String


        Dim oError As New GSServerModel.GSException(False, "", "", True)
        oError.Number = ErroresGS.TESnoerror

        oError.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)

        Try
            adoCom = New SqlCommand
            cn.Open()
            adoCom.Connection = cn
            adoCom.CommandText = "BEGIN DISTRIBUTED TRAN "
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()
            adoCom = New SqlCommand
            adoCom.Connection = cn
            adoCom.CommandText = "SET XACT_ABORT ON"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()


            For i = 0 To CodigosPaises.Count - 1
                bGuardar = True
                sCodPai = CodigosPaises(i).ToString()


                '        'Exc = New Exception
                adoCom = New SqlCommand
                adoCom.Connection = cn


                If bGuardar = True Then

                    If iInstWeb = TipoInstWeb.ConPortal Then

                        sConsulta = " SELECT * FROM " & FSP & "CIAS WHERE PAI= (SELECT ID FROM " & FSP & "PAI WITH (NOLOCK) WHERE COD=N'" & DblQuote(CodigosPaises(i)) & "')"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)

                        If dr.Tables(0).Rows.Count > 0 Then
                            If dr.Tables(0).Rows.Count > 0 Then
                                oError.Errores.Add(CodigosPaises(i), 0, i, "Portal")
                                iRes = iRes + 1
                                bGuardar = False
                            End If
                        End If


                    End If

                    If bGuardar = True Then
                        sConsulta = "SELECT COD, DEN_" & sIdioma & " DEN FROM DEST WITH(NOLOCK) WHERE PAI=N'" & DblQuote(CodigosPaises(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)

                        If dr.Tables(0).Rows.Count > 0 Then
                            oError.Errores.Add(CodigosPaises(i), 36, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1))
                            iRes = iRes + 1
                            bGuardar = False
                        End If


                    End If

                    If bGuardar = True Then
                        sConsulta = "SELECT COD, DEN FROM PROVE WITH(NOLOCK) WHERE PAI=N'" & DblQuote(CodigosPaises(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)

                        If dr.Tables(0).Rows.Count > 0 Then
                            oError.Errores.Add(CodigosPaises(i), 90, i, dr.Tables(0).Rows(0).Item(0) & "-" & dr.Tables(0).Rows(0).Item(1))
                            iRes = iRes + 1
                            bGuardar = False
                        End If


                    End If

                    If bGuardar = True Then
                        sConsulta = "SELECT ID, PAIDEF FROM PARGEN_DEF WITH(NOLOCK) WHERE PAIDEF=N'" & DblQuote(CodigosPaises(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)

                        If dr.Tables(0).Rows.Count > 0 Then
                            oError.Errores.Add(CodigosPaises(i), 66, i, dr.Tables(0).Rows(0).Item(0))
                            iRes = iRes + 1
                            bGuardar = False
                        End If


                    End If

                    If bGuardar Then

                        Dim oErrorProvis As New GSServerModel.GSException(False, "", "", True)
                        oErrorProvis.Number = ErroresGS.TESnoerror

                        oErrorProvis.Errores = New GSServerModel.GSExceptionObjects(False, "", "", True)


                        sConsulta = "SELECT COD FROM PROVI WITH(NOLOCK) WHERE PAI=N'" & DblQuote(CodigosPaises(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        da = New SqlDataAdapter
                        dr = New System.Data.DataSet
                        cm.CommandType = System.Data.CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)


                        If dr.Tables(0).Rows.Count > 0 Then
                            ReDim Preserve sCodigosProvi(dr.Tables(0).Rows.Count - 1)
                            ReDim Preserve sCodigosPaisProvi(dr.Tables(0).Rows.Count - 1)

                            For j = 0 To dr.Tables(0).Rows.Count - 1
                                sCodigosProvi(j) = dr.Tables(0).Rows(j).Item(0)
                                sCodigosPaisProvi(j) = sCodPai
                            Next j

                            oErrorProvis = EliminarProvincias(sCodigosProvi, sCodigosPaisProvi, iInstWeb, bGrabarLog, FSP, Origen, sUsuario, sIdioma)
                            If oErrorProvis.Number <> ErroresGS.TESnoerror Then
                                iRes = iRes + 1
                                bGuardar = False
                                For h = 0 To oErrorProvis.Errores.Count - 1
                                    oError.Errores.Add(oErrorProvis.Errores(h).Codigo, oErrorProvis.Errores(h).idError, oErrorProvis.Errores(h).Numero, oErrorProvis.Errores(h).Descripcion)
                                Next h

                            End If

                        End If



                    End If


                    If bGuardar = True Then

                        If bGrabarLog Then ' primero recuperamos los datos de la moneda que se va a borr

                            sConsulta = "SELECT COD, MON FROM PAI WITH(NOLOCK) WHERE COD =N'" & DblQuote(CodigosPaises(i)) & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            oDens.Clear()
                            If dr.Tables(0).Rows.Count > 0 Then
                                sCodPai = dr.Tables(0).Rows(0).Item(0)
                                sCodMon = dr.Tables(0).Rows(0).Item(1)
                            End If

                            sConsulta = "SELECT IDIOMA, DEN FROM PAI_DEN WITH(NOLOCK) WHERE PAI=N'" & DblQuote(CodigosPaises(i)) & "'"
                            cm = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            cm.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = cm
                            da.Fill(dr)
                            oDens.Clear()
                            If dr.Tables(0).Rows.Count > 0 Then
                                For j = 0 To dr.Tables(0).Rows.Count - 1
                                    oDens.Add(dr.Tables(0).Rows(j).Item("IDIOMA"), dr.Tables(0).Rows(j).Item("DEN"), Now)
                                Next
                            End If


                        End If


                        sConsulta = "UPDATE COPIA_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & DblQuote(CodigosPaises(i)) & "' AND COPIA_CAMPO_DEF IN (SELECT ID FROM FORM_CAMPO WHERE TIPO_CAMPO_GS=" & TipoCampoGS.Pais & ")"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()


                        sConsulta = "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & DblQuote(CodigosPaises(i)) & "' AND CAMPO_HIJO IN (SELECT COPIA_CAMPO.ID FROM COPIA_CAMPO INNER JOIN FORM_CAMPO ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Pais & ")"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()


                        '    'elimina de los formularios:
                        sConsulta = "UPDATE FORM_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & DblQuote(CodigosPaises(i)) & "' AND TIPO_CAMPO_GS=" & TipoCampoGS.Pais
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()

                        sConsulta = "UPDATE LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT=N'" & DblQuote(CodigosPaises(i)) & "' AND CAMPO_HIJO IN (SELECT ID FROM FORM_CAMPO WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Pais & ")"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()

                        sConsulta = "UPDATE PM_CAMPO_FAV  SET VALOR_TEXT = NULL FROM PM_CAMPO_FAV CF"
                        sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = CF.CAMPO"
                        sConsulta = sConsulta & " WHERE CF.VALOR_TEXT=N'" & DblQuote(CodigosPaises(i)) & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.Pais
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()


                        sConsulta = "UPDATE PM_LINEA_DESGLOSE_FAV SET VALOR_TEXT = NULL FROM PM_LINEA_DESGLOSE_FAV LDF"
                        sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = LDF.CAMPO_HIJO"
                        sConsulta = sConsulta & " WHERE LDF.VALOR_TEXT=N'" & DblQuote(CodigosPaises(i)) & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.Pais
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()


                        sConsulta = "DELETE FROM PAI_ERP WHERE COD_GS=N'" & DblQuote(CodigosPaises(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()

                        sConsulta = "DELETE FROM PAI_DEN WHERE PAI=N'" & DblQuote(CodigosPaises(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()

                        sConsulta = "DELETE FROM PAI WHERE COD=N'" & DblQuote(CodigosPaises(i)) & "'"
                        cm = New SqlCommand(sConsulta, cn)
                        ContRegBorrados = cm.ExecuteNonQuery()


                        If iInstWeb = TipoInstWeb.ConPortal Then

                            cm = New SqlCommand
                            cm.Connection = cn
                            cm.CommandText = FSP & "SP_ELIM_PAIS"
                            cm.CommandType = CommandType.StoredProcedure
                            cm.CommandType = CommandType.StoredProcedure
                            cm.Parameters.Add("@PAI_COD", SqlDbType.NVarChar, 50)
                            cm.Parameters("@PAI_COD").Value = CodigosPaises(i)
                            cm.ExecuteNonQuery()

                        End If


                        If bGrabarLog And ContRegBorrados > 0 Then

                            sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                            adoCom = New SqlCommand(sConsulta, cn)
                            da = New SqlDataAdapter
                            dr = New System.Data.DataSet
                            adoCom.CommandType = System.Data.CommandType.Text
                            da.SelectCommand = adoCom
                            da.Fill(dr)


                            Dim da2 = New SqlDataAdapter
                            Dim dr2 = New System.Data.DataSet

                            If dr.Tables(0).Rows.Count > 0 Then
                                For k = 0 To dr.Tables(0).Rows.Count - 1
                                    sConsulta = "INSERT INTO LOG_PAI (ACCION, COD, MON ORIGEN, USU, ERP) VALUES ('" & Accion_Baja & "',N'" & DblQuote(CodigosPaises(i)) & "'"
                                    sConsulta = sConsulta & "," & UnicodeStrToSQLNULL(sCodMon) & "," & Origen & ",'','" & DblQuote(sUsuario) & "','" & DblQuote(dr.Tables(0).Rows(k).Item("COD")) & "')"

                                    adoCom.CommandText = sConsulta
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    adoCom.ExecuteNonQuery()


                                    da2 = New SqlDataAdapter
                                    dr2 = New System.Data.DataSet

                                    adoCom = New SqlCommand("SELECT MAX(ID) AS ID FROM LOG_PAI ", cn)
                                    adoCom.CommandType = System.Data.CommandType.Text
                                    da2.SelectCommand = adoCom
                                    da2.Fill(dr2)

                                    If dr2.Tables(0).Rows.Count > 0 Then

                                        lIdLogPAI = dr2.Tables(0).Rows(0).Item(0)

                                        For Each oDen In oDens

                                            sConsulta = "INSERT INTO LOG_PROVI_DEN (ID_LOG_PAI,DEN,IDI) VALUES (" & lIdLogPAI & ","
                                            sConsulta = sConsulta & "N'" & DblQuote(oDen.Denominacion) & "','" & DblQuote(oDen.Idioma) & "')"
                                            adoCom = New SqlCommand(sConsulta, cn)
                                            adoCom.Connection = cn
                                            adoCom.CommandType = System.Data.CommandType.Text
                                            adoCom.ExecuteNonQuery()

                                        Next


                                    End If


                                Next

                            End If


                            RegistrarAccion(sUsuario, AccionesGS.ACCPaiEli, "Cod: " & CodigosPaises(i))

                        End If


                    End If

                End If

            Next i



            cm = New SqlCommand("SET XACT_ABORT OFF", cn)
            cm.ExecuteNonQuery()
            cm = New SqlCommand("COMMIT TRAN", cn)
            cm.ExecuteNonQuery()
            cn.Close()

            If iRes > 0 Then
                oError.Number = Fullstep.FSNLibrary.TiposDeDatos.ErroresGS.TESImposibleEliminar
            End If


        Catch ex As SqlException
            oError = TratarError(ex)
            cm = New SqlCommand("SET XACT_ABORT OFF", cn)
            cm.ExecuteNonQuery()
        Finally
            cn.Dispose()
            'cm.Dispose()
            da.Dispose()
        End Try

        Return oError

    End Function



#End Region
#Region " Idiomas data access methods "
    ''' <summary>
    ''' Cargar los idiomas de la aplicacion
    ''' </summary>
    ''' <returns>Un Dataset con los idiomas </returns>
    ''' <remarks>
    ''' Llamada desde: La clase GSServer.IdiomasRule.CargarTodosLosIdiomas
    ''' Tiempo máximo<1 sg</remarks>
    Public Function CargarTodosLosIdiomas() As System.Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_IDIOMAS"
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)
            cn.Close()
            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region

#End Region

#Region "Log"

    Public Function RegistrarEntradaOSalida(ByVal sUsuario As String, ByVal iAccion As Integer, ByVal Descripcion As String) As Boolean

        Try
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand
            Dim dr As New System.Data.DataSet

            cm = New SqlCommand
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "INSERT INTO LOG (USU,ACC,DAT,FEC) VALUES (N'" & DblQuote(sUsuario) & "'," & iAccion & ",N'" & DblQuote(Descripcion) & "'," & DateToSQLTimeDate(Now) & ")"
            cm.CommandType = System.Data.CommandType.Text
            cm.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception

        End Try

        Return (True)


    End Function

    Public Sub RegistrarAccion(ByVal sCodUsu As String, ByVal Acc As Integer, ByVal sDat As String)

        Try
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand
            Dim dr As New System.Data.DataSet

            cm = New SqlCommand
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "INSERT INTO LOG (USU,ACC,DAT,FEC) VALUES (N'" & DblQuote(sCodUsu) & "'," & Acc & ",N'" & DblQuote(sDat) & "'," & DateToSQLTimeDate(Now) & ")"
            cm.CommandType = System.Data.CommandType.Text
            cm.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception

        End Try


    End Sub


#End Region

#Region " User data access methods "

    Public Function User_LoadUserData(ByVal sCod As String, Optional ByVal UserCode As String = "", Optional ByVal UserPassword As String = "") As DataSet

        Authenticate(UserCode, UserPassword)
        Dim sConsulta As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New DataSet
        Dim gParametrosGenerales As TiposDeDatos.ParametrosGenerales
        Dim PwdEncriptado As String = ""
        sConsulta = "SELECT PWD FROM USU WHERE USU.COD='" & DblQuote(sCod) & "'"
        Try
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = sConsulta
            cm.CommandType = CommandType.Text
            Dim da0 As New SqlDataAdapter
            Dim dr0 As New DataSet
            da0.SelectCommand = cm
            da0.Fill(dr0)
            If dr0.Tables.Count > 0 Then
                PwdEncriptado = dr0.Tables(0).Rows(0).Item(0)
            Else
                Throw New Security.SecurityException("Usuario no encontrado en la base de datos")
                Return Nothing
                Exit Function
            End If
            cn.Close()
        Catch ex As Exception

        End Try
        Try
            cn.Open()
            cm = New SqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED", cn)
            cm.ExecuteNonQuery()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSEP_VALIDAR_USUARIO"
            cm.CommandType = CommandType.StoredProcedure
            'PARAMETROS DEL STORED,encriptamos el password
            cm.Parameters.AddWithValue("@USU", sCod)
            cm.Parameters.AddWithValue("@PWD", PwdEncriptado)
            cm.Parameters.AddWithValue("@BLOQUEO", gParametrosGenerales.giLOGPREBLOQ)
            'oParam = adoCom.CreateParameter("BLOQUEO", ADODB.DataTypeEnum.adInteger, ADODB.ParameterDirectionEnum.adParamInput, , gParametrosGenerales.giLOGPREBLOQ)
            'adoCom.Parameters.Append(oParam)
            'cm.Parameters.AddWithValue("@COD", sCod)
            Dim da As New SqlDataAdapter
            da.SelectCommand = cm
            da.Fill(dr)
        Catch

        End Try
        cn.Close()
        Return dr

    End Function

    ''' <summary>
    ''' Almacena las opciones de usuario.
    ''' </summary>
    ''' <param name="sCod">Código de usuario</param>
    ''' <param name="sThousanFmt">Separador de miles</param>
    ''' <param name="sDecimalFmt">Separador de decimales</param>
    ''' <param name="iPrecisionFmt">Número de decimales</param>
    ''' <param name="sDateFmt">Formato de fecha</param>
    ''' <param name="iTipoEmail">Tipo de email (texto/html)</param>
    ''' <param name="sIdioma">Código de idioma</param>
    ''' <param name="UserCode">Código de usuario</param>
    ''' <param name="UserPassword">Contraseña</param>
    ''' <returns>En caso de error devuelve la excepción SQL</returns>
    ''' <remarks></remarks>
    Public Function User_SaveUserData(ByVal sCod As String, ByVal sThousanFmt As String, ByVal sDecimalFmt As String, ByVal iPrecisionFmt As Integer, ByVal sDateFmt As String, ByVal iTipoEmail As Integer, ByVal sIdioma As String, Optional ByVal UserCode As String = "", Optional ByVal UserPassword As String = "") As SqlException

        Authenticate(UserCode, UserPassword)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Dim sql As String
        cn.Open()
        cm = New SqlCommand("BEGIN TRANSACTION", cn)
        cm.ExecuteNonQuery()

        Try
            sql = "UPDATE USU SET DECIMALFMT='" & sDecimalFmt & "'," _
                      & "   THOUSANFMT='" & sThousanFmt & "'," _
                      & "   DATEFMT='" & sDateFmt & "', " _
                      & "   PRECISIONFMT=" & iPrecisionFmt & ", " _
                      & "   TIPOEMAIL=" & iTipoEmail & ", " _
                      & "   IDIOMA='" & sIdioma & "' " _
                      & "WHERE COD ='" & sCod & "' "

            cm = New SqlCommand(sql, cn)
            cm.ExecuteNonQuery()

            cm = New SqlCommand("COMMIT TRANSACTION", cn)
            cm.ExecuteNonQuery()
            Return Nothing
            'Tratamiento de errores:
        Catch e As SqlException
            cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
            cm.ExecuteNonQuery()
            Return e
        End Try

    End Function

    Function ComprobarSession(ByVal sUsu As String, ByVal Session As Long) As Boolean

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_COMPROBAR_SESSION"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.Add("@USU", SqlDbType.NVarChar, 50)
            cm.Parameters("@USU").Value = sUsu
            cm.Parameters.Add("@SESSION", SqlDbType.BigInt)
            cm.Parameters("@SESSION").Value = Session

            cm.Parameters.Add("@RES", SqlDbType.Int)
            cm.Parameters("@RES").Direction = ParameterDirection.Output

            cn.Open()
            cm.ExecuteNonQuery()
            cn.Close()

            Return cm.Parameters("@RES").Value

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try

    End Function

    Public Function CargarDatosUsu(ByVal sUsu As String, ByVal sIdi As String) As System.Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_USUARIO"
            cm.Parameters.Add("@USU", SqlDbType.NVarChar, 50)
            cm.Parameters("@USU").Value = sUsu
            cm.Parameters.Add("@IDI", SqlDbType.VarChar, 50)
            cm.Parameters("@IDI").Value = sIdi
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)
            cn.Close()
            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    Public Function DevolverPersona(ByVal sCodigo As String) As System.Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_PERSONA"
        With cm.Parameters
            .Add("@COD", SqlDbType.VarChar, 20)
            cm.Parameters("@COD").Value = sCodigo
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

    Public Function CargarAccionesUsu(ByVal sUsu As String, Optional ByVal sIdi As String = "") As System.Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_ACCIONES_USUARIO"
            cm.Parameters.Add("@IDI", SqlDbType.NVarChar, 50)
            cm.Parameters("@IDI").Value = sIdi
            cm.Parameters.Add("@USU", SqlDbType.NVarChar, 50)
            cm.Parameters("@USU").Value = sUsu
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)

            cn.Close()
            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

    Public Function CargarAccionesPerfil(ByVal iPerfil As Integer, ByVal sIdi As String) As System.Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet

        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn

            cm.CommandText = "FSGS_DEVOLVER_ACCIONES_PERFIL"
            cm.Parameters.Add("@IDI", SqlDbType.NVarChar, 50)
            cm.Parameters("@IDI").Value = sIdi
            cm.Parameters.Add("@PERF", SqlDbType.Int)
            cm.Parameters("@PERF").Value = iPerfil
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)

            cn.Close()
            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function

#End Region

#Region " Security "
    Private Function getMd5Hash(ByVal input As String) As String
        ' Create a new instance of the MD5 object.
        Dim md5Hasher As MD5 = MD5.Create()

        ' Convert the input string to a byte array and compute the hash.
        Dim data As Byte() = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input))

        ' Create a new Stringbuilder to collect the bytes
        ' and create a string.
        Dim sBuilder As New StringBuilder()

        ' Loop through each byte of the hashed data 
        ' and format each one as a hexadecimal string.
        Dim i As Integer
        For i = 0 To data.Length - 1
            sBuilder.Append(data(i).ToString("x2"))
        Next i

        ' Return the hexadecimal string.
        Return sBuilder.ToString()

    End Function

    ' Verify a hash against a string.
    Private Function verifyMd5Hash(ByVal input As String, ByVal hash As String) As Boolean
        ' Hash the input.
        Dim hashOfInput As String = getMd5Hash(input)

        ' Create a StringComparer an comare the hashes.
        Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

        If 0 = comparer.Compare(hashOfInput, hash) Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function LoginHash(ByVal usercode As String, ByVal Hash As String) As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim pwd As String = ""
        Dim fecha As Date
        cn.Open()
        Try
            cm.Connection = cn
            cm.CommandText = "FSWS_LOGIN"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@COD", usercode)

            Dim dr As SqlDataReader = cm.ExecuteReader()
            If dr.Read() Then
                'Desencriptamos la pwd y la comparamos con la del login
                pwd = dr.Item("PWD").ToString()
                fecha = dr.Item("FECPWD")
                pwd = Encrypter.Encrypt(pwd, usercode, False, Encrypter.TipoDeUsuario.Persona, fecha)
                If verifyMd5Hash(usercode & pwd, Hash) Then
                    mUserCod = usercode
                    mUserPassword = Encrypter.Encrypt(pwd)
                    Return mUserPassword
                Else
                    Return ""
                End If
                dr.Close()
            Else
                Return ""
            End If
        Catch e As Exception

            Return ""
        Finally
            cn.Close()
        End Try

    End Function
    'Antes-->Public Function Login(Optional ByVal usercode As String = "", Optional ByVal userpassword As String = "") As Boolean
    Public Function Login(ByVal usercode As String, ByVal userpassword As String) As Boolean

        'Accedemos a bd y comparamos el login, si es correcto
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim fecha As Date
        Dim pwd As String = ""
        Dim decrypteduserpassword As String
        Dim bLoginOK = False
        Dim dr As SqlDataReader = Nothing
        Try
            cn.Open()
            decrypteduserpassword = Encrypter.Encrypt(userpassword, , False)
            'decrypteduserpassword = Encrypter.Encrypt(userpassword, usercode, True)
            cm.Connection = cn
            cm.CommandText = "FSWS_LOGIN"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@COD", usercode)

            dr = cm.ExecuteReader()
            If dr.Read() Then
                'Desencriptamos la pwd y la comparamos con la del login
                pwd = dr.Item("PWD").ToString()
                fecha = dr.Item("FECPWD")
                pwd = Encrypter.Encrypt(pwd, usercode, False, Encrypter.TipoDeUsuario.Persona, fecha)
                If pwd.Equals(decrypteduserpassword) Then
                    bLoginOK = True
                    mUserCod = usercode
                    mUserPassword = userpassword 'La guardamos encriptada con la fecha fija
                    'Asignar al usuario el POOL que le corresponde

                Else
                    bLoginOK = False
                End If
                dr.Close()
            Else
                bLoginOK = False
            End If
            Return bLoginOK
        Catch e As Exception
            Throw e
        Finally
            If Not dr Is Nothing Then
                If Not dr.IsClosed Then dr.Close()
            End If
            cn.Dispose()
            cm.Dispose()
        End Try


    End Function


    Private Sub Authenticate(ByVal usercode, ByVal userpassword)
        If usercode <> "" Or mUserCod <> "" Then
            'Single call mode. Must authenticate
            If Not Login(usercode, userpassword) And Not Login(mUserCod, mUserPassword) Then
                Throw New Security.SecurityException("User not authenticated!")
            End If
        Else
            If mUserCod = "" Then
                'No ha habido remoting y no ha pasado por el método Login
                Throw New Security.SecurityException("User not authenticated!")
            End If
        End If

    End Sub
#End Region

#Region " Constructor "
    Public Sub New()
        'Comprobar tema de licencias y cargar datos de conexiÃ³n
        Dim sDB As String = ConfigurationManager.AppSettings("DB:FS_WS")
        If sDB Is Nothing Then
            Throw New Exception("Configuration file corrupted. Missing FS_WS entry !")
        End If
        'Tenemos que extraer el login y la contraseÃ±a de conexiÃ³n.
        Dim doc As New XmlDocument
        Dim sdblogin As String = ""
        Dim sdbPassword As String = ""
        Try
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory() & "License.xml")

        Catch
            Throw New Exception("LICENSE file missing!")

        End Try

        Dim child As XmlNode
        child = doc.SelectSingleNode("/LICENSE/DB_LOGIN")
        If Not (child Is Nothing) Then
            mDBLogin = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_PASSWORD")
        If Not (child Is Nothing) Then
            mDBPassword = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        Else
            mDBPassword = ""
        End If

        child = doc.SelectSingleNode("/LICENSE/DB_NAME")
        If Not (child Is Nothing) Then
            mDBName = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_SERVER")
        If Not (child Is Nothing) Then
            mDBServer = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If

        If mDBServer = "" Or mDBName = "" Or mDBLogin = "" Then
            Throw New Exception("Corrupted LICENSE file !")
        End If

        'PENDIENTE METER ENCRIPTACIÃ“N

        sDB = sDB.Replace("DB_LOGIN", mDBLogin)
        sDB = sDB.Replace("DB_PASSWORD", mDBPassword)
        sDB = sDB.Replace("DB_NAME", mDBName)
        sDB = sDB.Replace("DB_SERVER", mDBServer)
        doc = Nothing
        mDBConnection = sDB

    End Sub
#End Region

#Region " Data structures"
    <Serializable()> _
  Public Structure UserIdentity_Actions
        Public ID() As Integer
    End Structure
#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

#Region "Subastas"
    Public Function UTCdeBD() As Date
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New System.Data.DataSet
        Dim sConsulta As String

        Try
            cn.Open()

            cm = New SqlCommand
            cm.Connection = cn

            sConsulta = "SELECT GETUTCDATE()"
            cm = New SqlCommand(sConsulta, cn)
            da = New SqlDataAdapter
            ds = New System.Data.DataSet
            cm.CommandType = System.Data.CommandType.Text
            da.SelectCommand = cm
            da.Fill(ds)

            Return CDate(ds.Tables(0).Rows(0).Item(0))

        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
End Class
