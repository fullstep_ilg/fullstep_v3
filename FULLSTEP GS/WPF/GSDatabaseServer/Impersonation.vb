﻿Imports System.Web
Imports System.Security.Principal
Imports System.Runtime.InteropServices

Class Impersonation
    Private Const LOGON32_LOGON_INTERACTIVE As Integer = 2
    Private Const LOGON32_LOGON_NETWORK As Integer = 3
    Private Const LOGON32_PROVIDER_DEFAULT As Integer = 0

    Declare Function LogonUserA Lib "advapi32.dll" (ByVal lpszUsername As String, _
                            ByVal lpszDomain As String, _
                            ByVal lpszPassword As String, _
                            ByVal dwLogonType As Integer, _
                            ByVal dwLogonProvider As Integer, _
                            ByRef phToken As IntPtr) As Integer

    Declare Auto Function DuplicateToken Lib "advapi32.dll" ( _
                            ByVal ExistingTokenHandle As IntPtr, _
                            ByVal ImpersonationLevel As Integer, _
                            ByRef DuplicateTokenHandle As IntPtr) As Integer

    Declare Auto Function RevertToSelf Lib "advapi32.dll" () As Long
    Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Long

    Private _oImpersonationContext As WindowsImpersonationContext
    Private _sUserName As String
    Private _sDomain As String
    Private _sPassword As String

    Public Sub New(ByVal sUser As String, ByVal sDomain As String, ByVal sPwd As String)
        _sUserName = sUser
        _sDomain = sDomain
        _sPassword = sPwd
    End Sub

    '<summary>Realiza la suplantación del usuario que ejecuta el proceso</summary>    
    '<returns>Booleano indicando si ha tenido éxito</returns>    

    Public Function Impersonate() As Boolean
        Dim tempWindowsIdentity As WindowsIdentity
        Dim token As IntPtr = IntPtr.Zero
        Dim tokenDuplicate As IntPtr = IntPtr.Zero
        Impersonate = False

        If RevertToSelf() Then
            If LogonUserA(_sUserName, _sDomain, _sPassword, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, token) <> 0 Then
                If DuplicateToken(token, 2, tokenDuplicate) <> 0 Then
                    tempWindowsIdentity = New WindowsIdentity(tokenDuplicate)
                    _oImpersonationContext = tempWindowsIdentity.Impersonate()
                    If Not _oImpersonationContext Is Nothing Then
                        Impersonate = True
                    End If
                End If
            End If
        End If
        If Not tokenDuplicate.Equals(IntPtr.Zero) Then
            CloseHandle(tokenDuplicate)
        End If
        If Not token.Equals(IntPtr.Zero) Then
            CloseHandle(token)
        End If
    End Function

    '<summary>Deshace la suplantación</summary>       

    Public Sub UndoImpersonation()
        _oImpersonationContext.Undo()
    End Sub

End Class
