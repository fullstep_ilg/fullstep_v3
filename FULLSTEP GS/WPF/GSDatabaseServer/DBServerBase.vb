﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports System.Text
Imports System.Security.Cryptography
Imports System.Security.Permissions
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos

<Serializable()> _
Public Class DBServerBase
    Inherits MarshalByRefObject

    Protected mDBLogin As String = ""
    Protected mDBPassword As String = ""
    Protected mDBName As String = ""
    Protected mDBServer As String = ""
    Protected mDBConnection As String = ""
    Protected mISDBConnection As String = ""
    Protected mUserCod As String = ""
    Protected mUserPassword As String = ""
    Protected mImpersonationUser As String = String.Empty
    Protected mImpersonationDomain As String = String.Empty
    Protected mImpersonationPwd As String = String.Empty

#Region " Constructor "

    Public Sub New()
        'Comprobar tema de licencias y cargar datos de conexión
        Dim sDB As String = ConfigurationManager.AppSettings("DB:FS_WS")
        If sDB Is Nothing Then
            Throw New Exception("Configuration file corrupted. Missing FS_WS entry !")
        End If
        'Tenemos que extraer el login y la contraseña de conexión.
        Dim doc As New XmlDocument
        Dim sdblogin As String = ""
        Dim sdbPassword As String = ""
        Try
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory() & "License.xml")

        Catch
            Throw New Exception("LICENSE file missing!")

        End Try

        Dim child As XmlNode
        child = doc.SelectSingleNode("/LICENSE/DB_LOGIN")
        If Not (child Is Nothing) Then
            mDBLogin = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_PASSWORD")
        If Not (child Is Nothing) Then
            mDBPassword = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        Else
            mDBPassword = ""
        End If

        child = doc.SelectSingleNode("/LICENSE/DB_NAME")
        If Not (child Is Nothing) Then
            mDBName = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_SERVER")
        If Not (child Is Nothing) Then
            mDBServer = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If

        If mDBServer = "" Or mDBName = "" Or mDBLogin = "" Then
            Throw New Exception("Corrupted LICENSE file !")
        End If

        'PENDIENTE METER ENCRIPTACIÓN

        sDB = sDB.Replace("DB_LOGIN", mDBLogin)
        sDB = sDB.Replace("DB_PASSWORD", mDBPassword)
        sDB = sDB.Replace("DB_NAME", mDBName)
        sDB = sDB.Replace("DB_SERVER", mDBServer)        
        mDBConnection = sDB

        mISDBConnection = sDB
        mISDBConnection = mISDBConnection.Replace(";user id=" & mDBLogin, String.Empty)
        mISDBConnection = mISDBConnection.Replace(";password=" & mDBPassword, String.Empty)
        mISDBConnection &= ";Integrated Security=True"

        child = doc.SelectSingleNode("/LICENSE/IMP_USER")
        If Not (child Is Nothing) Then
            mImpersonationUser = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If
        child = doc.SelectSingleNode("/LICENSE/IMP_PWD")
        If Not (child Is Nothing) Then
            mImpersonationPwd = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If
        child = doc.SelectSingleNode("/LICENSE/IMP_DOM")
        If Not (child Is Nothing) Then
            mImpersonationDomain = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If

        doc = Nothing
    End Sub

#End Region

#Region " Properties "

    ReadOnly Property DBConnection() As String
        Get
            Return mDBConnection
        End Get
    End Property

    ReadOnly Property DBLogin() As String
        Get
            Return mDBLogin
        End Get
    End Property

    ReadOnly Property DBPassword() As String
        Get
            Return mDBPassword
        End Get
    End Property

    ReadOnly Property DBName() As String
        Get
            Return mDBName
        End Get
    End Property

    ReadOnly Property DBServer() As String
        Get
            Return mDBServer
        End Get
    End Property

#End Region

#Region " Métodos "

    Protected Sub EjecutarComandoActualizacion(ByRef oComm As SqlCommand, ByVal oCon As SqlConnection)
        oComm.Connection = oCon
        oComm.ExecuteNonQuery()
    End Sub

    Protected Sub EjecutarComando(ByRef oComm As SqlCommand, ByVal oCon As SqlConnection)
        oComm.Connection = oCon       
        Dim oReader As SqlDataReader = oComm.ExecuteReader()
        'oComm.ExecuteNonQuery()
    End Sub

    Protected Function EjecutarComandoLectura(ByVal oComm As SqlCommand, ByVal oCon As SqlConnection) As DataSet
        Dim da As New SqlDataAdapter

        oComm.Connection = oCon
        da.SelectCommand = oComm

        Dim ds As New System.Data.DataSet

        da.Fill(ds)

        Dim oReader As SqlDataReader = da.SelectCommand.ExecuteReader()

        ''Obtener los tamaños de las columnas de tipo string
        'Dim dtSchema As DataTable = oReader.GetSchemaTable()
        'For Each oRow As DataRow In dtSchema.Rows
        '    If oRow("DataType") Is GetType(String) Then
        '        Try
        '            ds.Tables(0).Columns(oRow("ColumnName")).MaxLength = oRow("ColumnSize")
        '        Catch ex As Exception
        '        End Try
        '    End If
        'Next

        Return ds
    End Function

    Protected Sub EjecutarActualizacion(ByVal dsDatos As DataSet, ByVal oActComm As SqlCommand, ByVal oInsComm As SqlCommand, ByVal oDelComm As SqlCommand, ByVal oCon As SqlConnection)
        'Debug.Write(Date.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") & ": GSDatabaseServer -> Inicio EjecutarActualizacion" & Environment.NewLine)

        If Not oActComm Is Nothing Then oActComm.Connection = oCon
        If Not oInsComm Is Nothing Then oInsComm.Connection = oCon
        If Not oDelComm Is Nothing Then oDelComm.Connection = oCon

        Dim da As New SqlDataAdapter
        da.UpdateCommand = oActComm
        da.InsertCommand = oInsComm
        da.DeleteCommand = oDelComm
        da.Update(dsDatos)

        da.Dispose()

        'Debug.Write(Date.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") & ": GSDatabaseServer -> Fin EjecutarActualizacion" & Environment.NewLine)
    End Sub

    Protected Sub BeginTransaction(ByVal oCon As SqlConnection)
        If oCon.State = ConnectionState.Open Then
            Dim adoCom As SqlCommand
            adoCom = New SqlCommand
            adoCom.Connection = oCon
            adoCom.CommandText = "SET XACT_ABORT ON BEGIN TRANSACTION"
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.ExecuteNonQuery()
        End If
    End Sub

    Protected Sub CommitTransaction(ByVal oCon As SqlConnection)
        If oCon.State = ConnectionState.Open Then
            Dim adoCom As SqlCommand
            adoCom = New SqlCommand
            adoCom.Connection = oCon
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.CommandText = "SET XACT_ABORT OFF COMMIT TRANSACTION"
            adoCom.ExecuteNonQuery()
        End If
    End Sub

    Protected Sub RollbackTransaction(ByVal oCon As SqlConnection)
        If oCon.State = ConnectionState.Open Then
            Dim adoCom As SqlCommand
            adoCom = New SqlCommand
            adoCom.Connection = oCon
            adoCom.CommandType = System.Data.CommandType.Text
            adoCom.CommandText = "SET XACT_ABORT OFF IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
            adoCom.ExecuteNonQuery()
        End If
    End Sub

#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class
