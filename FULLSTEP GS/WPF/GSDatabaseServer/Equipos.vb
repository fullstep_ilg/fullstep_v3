﻿Imports Fullstep.FSNLibrary
Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel

<Serializable()> _
Public Class Equipos
    Inherits DBServerBase

    '<summary>Devuelve los compradores de un equipo</summary>
    '<param name="Equipo">Equipo</param>
    '<param name="NumMax">nº máximo de registros devueltos</param>
    '<param name="NoBajaLog"></param>    
    '<param name="OrderByApe">ordenación por apellido</param>        
    '<param name="CarIniCod">caracteres iniciales del código (filtro)</param>        
    '<param name="CarIniApel">caracteres iniciales del apellido (filtro)</param>        
    '<returns>Dataset con los compradores</returns>    

    Public Function DevolverCompradoresEquipo(ByVal Equipo As String, Optional ByVal NumMax As Integer = -1, Optional ByVal NoBajaLog As Boolean = False, _
                                              Optional ByVal CarIniCod As String = Nothing, Optional ByVal CarIniApel As String = Nothing, Optional ByVal OrderByApe As Boolean = False) As DataSet
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_DEVOLVER_COMPRADORES_EQUIPO"
        With cm.Parameters
            .Add("@EQP", SqlDbType.VarChar, 50)
            cm.Parameters("@EQP").Value = Equipo
            If NumMax <> -1 Then
                .Add("@NUMMAX", SqlDbType.Int)
                cm.Parameters("@NUMMAX").Value = NumMax
            End If
            .Add("@NOBAJALOG", SqlDbType.TinyInt)
            cm.Parameters("@NOBAJALOG").Value = BooleanToSQLBinary(NoBajaLog)
            .Add("@ORDERBYAPE", SqlDbType.TinyInt)
            cm.Parameters("@ORDERBYAPE").Value = BooleanToSQLBinary(OrderByApe)
            If Not CarIniCod Is Nothing Then
                .Add("@CARINICOD", SqlDbType.VarChar, 20)
                cm.Parameters("@CARINICOD").Value = CarIniCod
            End If
            If Not CarIniApel Is Nothing Then
                .Add("@CARINIAPEL", SqlDbType.VarChar, 100)
                cm.Parameters("@CARINIAPEL").Value = CarIniApel
            End If
        End With

        Dim cn As SqlConnection
        Try
            cn = New SqlConnection(mDBConnection)
            cn.Open()
            Return EjecutarComandoLectura(cm, cn)
            cn.Close()
        Catch ex As SqlException
            Throw ex
        Finally
            cn.Dispose()
        End Try
    End Function

End Class
