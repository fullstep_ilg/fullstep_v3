﻿Imports Fullstep.FSNLibrary
Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel

Public Class ArchivoSubastaReglas
    Inherits DBServerBase

    Private _cn As SqlConnection

    '<summary>Devuelve reglas de subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Dataset con los datos de la subasta</returns> 

    Public Function DevolverArchivoSubastaRegla(ByRef Buffer As Byte(), ByVal iOffset As Integer, ByVal iChunkSize As Integer, ByVal ID As Integer) As Integer
        Dim bImp As Boolean
        Dim oImp As Impersonation

        DevolverArchivoSubastaRegla = 0

        Try
            'Realizar la suplantación
            oImp = New Impersonation(Me.mImpersonationUser, Me.mImpersonationDomain, Me.mImpersonationPwd)
            bImp = oImp.Impersonate()

            If bImp Then
                _cn = New SqlConnection(mISDBConnection)
                _cn.Open()

                Me.BeginTransaction(_cn)

                Dim oFSUtil As New FileStreamUtil("FSGS_DEVOLVER_ARCHIVO_SUBASTA_REGLA")
                Dim oSqlFileStream As SqlTypes.SqlFileStream = oFSUtil.GetSqlFileStreamForReading(_cn, ID)
                If Not oSqlFileStream Is Nothing Then                    
                    'Obtener el chunk indicado
                    ReDim Buffer(iChunkSize - 1)
                    oSqlFileStream.Seek(iOffset, System.IO.SeekOrigin.Begin)
                    Dim NumBytes As Integer = oSqlFileStream.Read(Buffer, 0, Buffer.Length)
                    oSqlFileStream.Close()

                    DevolverArchivoSubastaRegla = NumBytes
                End If

                Me.CommitTransaction(_cn)

                _cn.Close()

                oImp.UndoImpersonation()
                bImp = False                
            End If

        Catch ex As SqlException
            If bImp Then oImp.UndoImpersonation()
            Me.RollbackTransaction(_cn)
            _cn.Close()
            Throw ex
        Finally
            If bImp Then oImp.UndoImpersonation()
            If Not _cn Is Nothing Then _cn.Dispose()
        End Try
    End Function

    '<summary>Actualiza las reglas de subasta de un proceso</summary>
    '<param name="dsReglas">Reglas del proceso</param>     
    '<param name="oArchivos">Objeto de tipo GSServerModel.Files con los datos de las reglas</param>  
    '<returns>Objeto de tipo GSException con el error si lo ha habido</returns>    

    Public Function ComenzarActualizarProcesoSubastaReglas(ByVal dsReglas As DataSet, ByVal oArchivos As GSServerModel.Files) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim oImp As Impersonation
        Dim bImp As Boolean
        Try
            'Realizar la suplantación
            oImp = New Impersonation(Me.mImpersonationUser, Me.mImpersonationDomain, Me.mImpersonationPwd)
            bImp = oImp.Impersonate()
            If bImp Then
                Dim sConnString As String = mISDBConnection
                _cn = New SqlConnection(sConnString)
                _cn.Open()

                Me.BeginTransaction(_cn)

                Dim oActCom As SqlCommand = ComandoActualizarSubastaReglas(dsReglas)
                Dim oInsCom As SqlCommand = ComandoInsertarSubastaReglas(dsReglas)
                Dim oDelCom As SqlCommand = ComandoBorrarSubastaReglas(dsReglas)

                Dim dsReglasCopia As DataSet = dsReglas.Copy()

                EjecutarActualizacion(dsReglas, oActCom, oInsCom, oDelCom, _cn)

                'Insertar archivos de reglas nuevas          
                If Not dsReglas Is Nothing AndAlso dsReglas.Tables.Count > 0 Then
                    Dim dvAddedRows As DataView = New DataView(dsReglasCopia.Tables(0), Nothing, "NOM", DataViewRowState.Added)
                    If Not dvAddedRows Is Nothing AndAlso dvAddedRows.Count > 0 Then
                        Dim sRootFolder As String = System.Configuration.ConfigurationManager.AppSettings("UploadFolder")
                        If Not sRootFolder.EndsWith("\") Then sRootFolder &= "\"

                        dsReglas.Tables(0).DefaultView.Sort = "NOM"
                        For Each oRowView As DataRowView In dvAddedRows
                            Dim iIndex As Integer = dsReglas.Tables(0).DefaultView.Find(oRowView("NOM"))

                            Dim oFSUtil As New FileStreamUtil("FSGS_DEVOLVER_ARCHIVO_SUBASTA_REGLA")
                            Dim oSqlFileStream As SqlTypes.SqlFileStream = oFSUtil.GetSqlFileStreamForWriting(_cn, dsReglas.Tables(0).DefaultView(iIndex)("ADJUN_PROCE"))
                            If Not oSqlFileStream Is Nothing Then
                                'Leer el archivo del directorio temporal
                                Dim sFilePath As String = sRootFolder & oArchivos.Item(oRowView("NOM")).ServerFileDirectory & "\" & oRowView("NOM")
                                Dim oFS As System.IO.FileStream = New System.IO.FileStream(sFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                                Dim Buffer(oFS.Length - 1) As Byte
                                oFS.Read(Buffer, 0, Buffer.Length)
                                oFS.Close()

                                oSqlFileStream.Write(Buffer, 0, Buffer.Length)
                                oSqlFileStream.Close()
                            End If
                        Next
                    End If
                End If

                'Deshacer suplantación
                oImp.UndoImpersonation()
                bImp = False
            Else
                oError.Number = ErroresGS.TESErrorSuplantacion                
            End If

        Catch ex As SqlException
            oError = TratarError(ex)
        Finally
            If bImp Then oImp.UndoImpersonation()
        End Try

        Return oError
    End Function

    Public Sub TerminarActualizarProcesoSubastaReglas(ByVal bOk As Boolean)
        If Not _cn Is Nothing Then
            If bOk Then
                Me.CommitTransaction(_cn)
            Else
                Me.RollbackTransaction(_cn)
            End If
            _cn.Close()
            _cn.Dispose()
        End If
    End Sub

    '<summary>Genera comando de actualización de las reglas de subasta de un proceso</summary>
    '<param name="dsReglas">Reglas de Proceso</param>
    '<param name="oCon">Conexión con la que se lleva a cabo la actualización</param>    
    '<returns>comando de actualización</returns>

    Private Function ComandoActualizarSubastaReglas(ByVal dsReglas As DataSet) As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_ACTUALIZAR_PROCESO_SUBASTA_REGLAS"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.SmallInt, 0, "ANYO")
            .Add("@PROCE", SqlDbType.Int, 0, "PROCE")
            .Add("@GMN1", SqlDbType.VarChar, 50, "GMN1")
            .Add("@ID", SqlDbType.SmallInt, 0, "ID")
            .Add("@NOM", SqlDbType.NVarChar, 300, "NOM")
            .Add("@COM", SqlDbType.NVarChar, 500, "COM")
            .Add("@ADJUN_PROCE", SqlDbType.Int, 0, "ADJUN_PROCE")
            .Add("@CONT", SqlDbType.Int, 0, "CONT")
            .Add("@DGUID", SqlDbType.UniqueIdentifier, 0, "DGUID")
        End With

        Return cm
    End Function

    '<summary>Genera comando de inserción de las reglas de subasta de un proceso</summary>
    '<param name="dsReglas">Reglas de Proceso</param>
    '<param name="oCon">Conexión con la que se lleva a cabo la actualización</param>      
    '<returns>comando de inserción</returns>

    Private Function ComandoInsertarSubastaReglas(ByVal dsReglas As DataSet) As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_INSERTAR_PROCESO_SUBASTA_REGLAS"
        cm.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord
        With cm.Parameters
            .Add("@ANYO", SqlDbType.SmallInt, 0, "ANYO")
            .Add("@PROCE", SqlDbType.Int, 0, "PROCE")
            .Add("@GMN1", SqlDbType.VarChar, 50, "GMN1")
            .Add("@NOM", SqlDbType.NVarChar, 300, "NOM")
            .Add("@COM", SqlDbType.NVarChar, 500, "COM")
            .Add("@FECALTA", SqlDbType.DateTime, 0, "FECALTA")
            .Add("@CONT", SqlDbType.Int, 0, "CONT")
        End With

        Return cm
    End Function

    '<summary>Genera comando de borrado de las reglas de subasta de un proceso</summary>
    '<param name="dsReglas">Reglas de Proceso</param>
    '<param name="oCon">Conexión con la que se lleva a cabo la actualización</param>      
    '<returns>comando de borrado</returns>

    Private Function ComandoBorrarSubastaReglas(ByVal dsReglas As DataSet) As SqlCommand
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_BORRAR_PROCESO_SUBASTA_REGLAS"
        With cm.Parameters
            .Add("@ANYO", SqlDbType.SmallInt, 0, "ANYO")
            .Add("@PROCE", SqlDbType.Int, 0, "PROCE")
            .Add("@GMN1", SqlDbType.VarChar, dsReglas.Tables(0).Columns("GMN1").MaxLength, "GMN1")
            .Add("@ID", SqlDbType.SmallInt, 20, "ID")
        End With

        Return cm
    End Function

    Protected Overrides Sub Finalize()
        _cn = Nothing
        MyBase.Finalize()
    End Sub

End Class
