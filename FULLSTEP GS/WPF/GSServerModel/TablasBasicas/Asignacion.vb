﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class Asignacion
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _sCodProve As String
    Private _sDenProve As String    
    Private _sCodEqp As String
    Private _sCodComp As String
    Private _bAsignado As Boolean
    Private _sCodPortal As String
    Private _bPremium As Boolean
    Private _bAntesDeCierreParcial As Boolean
    Private _bPremActivo As Boolean
    Private _sCalif1 As String
    Private _sCalif2 As String
    Private _sCalif3 As String
    Private _dblVal1 As Double
    Private _dblVal2 As Double
    Private _dblVal3 As Double
    Private _oGrupos As ProcesoGrupos

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property CodigoProveedor As String
        Get
            Return _sCodProve
        End Get
        Set(ByVal value As String)
            If Not (value = _sCodProve) Then
                _sCodProve = value
                NotifyPropertyChanged("CODPROVE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property DenominacionProveedor As String
        Get
            Return _sDenProve
        End Get
        Set(ByVal value As String)
            If Not (value = _sDenProve) Then
                _sDenProve = value
                NotifyPropertyChanged("DENPROVE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoEquipo As String
        Get
            Return _sCodEqp
        End Get
        Set(ByVal value As String)
            If Not (value = _sCodEqp) Then
                _sCodEqp = value
                NotifyPropertyChanged("CODEQP")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoComp As String
        Get
            Return _sCodComp
        End Get
        Set(ByVal value As String)
            If Not (value = _sCodComp) Then
                _sCodComp = value
                NotifyPropertyChanged("CODCOMP")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Asignado As Boolean
        Get
            Return _bAsignado
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bAsignado) Then
                _bAsignado = value
                NotifyPropertyChanged("ASIGNADO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoPortal As String
        Get
            Return _sCodPortal
        End Get
        Set(ByVal value As String)
            If Not (value = _sCodPortal) Then
                _sCodPortal = value
                NotifyPropertyChanged("CODPORTAL")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Premium As Boolean
        Get
            Return _bPremium
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPremium) Then
                _bPremium = value
                NotifyPropertyChanged("PREMIUM")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property AntesDeCierreParcial As Boolean
        Get
            Return _bAntesDeCierreParcial
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bAntesDeCierreParcial) Then
                _bAntesDeCierreParcial = value
                NotifyPropertyChanged("ANTESDECIERREPARCIAL")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PremActivo As Boolean
        Get
            Return _bPremActivo
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPremActivo) Then
                _bPremActivo = value
                NotifyPropertyChanged("PREMACTIVO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Calif1 As String
        Get
            Return _sCalif1
        End Get
        Set(ByVal value As String)
            If Not (value = _sCalif1) Then
                _sCalif1 = value
                NotifyPropertyChanged("CAL1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Calif2 As String
        Get
            Return _sCalif2
        End Get
        Set(ByVal value As String)
            If Not (value = _sCalif2) Then
                _sCalif2 = value
                NotifyPropertyChanged("CAL2")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Calif3 As String
        Get
            Return _sCalif3
        End Get
        Set(ByVal value As String)
            If Not (value = _sCalif3) Then
                _sCalif3 = value
                NotifyPropertyChanged("CAL3")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Val1 As Double
        Get
            Return _dblVal1
        End Get
        Set(ByVal value As Double)
            If Not (value = _dblVal1) Then
                _dblVal1 = value
                NotifyPropertyChanged("VAL1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Val2 As Double
        Get
            Return _dblVal2
        End Get
        Set(ByVal value As Double)
            If Not (value = _dblVal2) Then
                _dblVal2 = value
                NotifyPropertyChanged("VAL2")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Val3 As Double
        Get
            Return _dblVal3
        End Get
        Set(ByVal value As Double)
            If Not (value = _dblVal3) Then
                _dblVal3 = value
                NotifyPropertyChanged("VAL3")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Grupos() As ProcesoGrupos
        Get
            Return _oGrupos
        End Get
        Set(ByVal value As ProcesoGrupos)
            _oGrupos = value
            NotifyPropertyChanged("Grupos")
        End Set
    End Property

#End Region

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

End Class

