﻿Imports System.Runtime.Serialization

<DataContract()> _
Public Class TextosMultiidioma
    Inherits Lista(Of TextoMultiidioma)

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Overloads ReadOnly Property Item(ByVal ID As Integer) As TextoMultiidioma
        Get
            Return Me.Find(Function(elemento As TextoMultiidioma) elemento.ID = ID)
        End Get
    End Property

    Public Overloads ReadOnly Property Item(ByVal Idioma As String) As TextoMultiidioma
        Get
            Return Me.Find(Function(elemento As TextoMultiidioma) elemento.Idioma = Idioma)
        End Get
    End Property

    Public Overloads ReadOnly Property Item(ByVal ID As Integer, ByVal Idioma As String) As TextoMultiidioma
        Get
            Return Me.Find(Function(elemento As TextoMultiidioma) elemento.ID = ID And elemento.Idioma = Idioma)
        End Get
    End Property

End Class