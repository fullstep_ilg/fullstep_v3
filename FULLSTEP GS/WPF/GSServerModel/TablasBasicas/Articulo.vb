﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

Public Class Articulo
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _sGMN1 As String
    Private _sGMN2 As String
    Private _sGMN3 As String
    Private _sGMN4 As String
    Private _sCod As String
    Private _sDen As String
    Private _iAsig As Nullable(Of Integer)

#Region " Constructores "

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Sub New(ByVal sGMN1 As String, ByVal sGMN2 As String, ByVal sGMN3 As String, ByVal sGMN4 As String, ByVal sCod As String)
        MyBase.New(True, String.Empty, String.Empty, False)

        _sGMN1 = sGMN1
        _sGMN2 = sGMN2
        _sGMN3 = sGMN3
        _sGMN4 = sGMN4
        _sCod = sCod
    End Sub

#End Region

    <DataMember()> _
    Public Property GMN1 As String
        Get
            Return _sGMN1
        End Get
        Set(ByVal value As String)
            _sGMN1 = value
            NotifyPropertyChanged("GMN1")
        End Set
    End Property

    <DataMember()> _
    Public Property GMN2 As String
        Get
            Return _sGMN2
        End Get
        Set(ByVal value As String)
            _sGMN2 = value
            NotifyPropertyChanged("GMN2")
        End Set
    End Property

    <DataMember()> _
    Public Property GMN3 As String
        Get
            Return _sGMN3
        End Get
        Set(ByVal value As String)
            _sGMN3 = value
            NotifyPropertyChanged("GMN3")
        End Set
    End Property

    <DataMember()> _
    Public Property GMN4 As String
        Get
            Return _sGMN4
        End Get
        Set(ByVal value As String)
            _sGMN4 = value
            NotifyPropertyChanged("GMN4")
        End Set
    End Property

    <DataMember()> _
    Public Property Codigo As String
        Get
            Return _sCod
        End Get
        Set(ByVal value As String)
            _sCod = value
            NotifyPropertyChanged("Codigo")
        End Set
    End Property

    <DataMember()> _
    Public Property Denominacion As String
        Get
            Return _sDen
        End Get
        Set(ByVal value As String)
            _sDen = value
            NotifyPropertyChanged("Denominacion")
        End Set
    End Property

    <DataMember()> _
    Public Property Asignado As Nullable(Of Integer)
        Get
            Return _iAsig
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _iAsig = value
            NotifyPropertyChanged("Asignado")
        End Set
    End Property

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(info))
    End Sub

End Class
