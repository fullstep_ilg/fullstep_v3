﻿Imports System
Imports System.Data.SqlClient
Imports System.Exception
Imports System.Collections.ObjectModel
'Imports System.ServiceModel
Imports Fullstep.FSNLibrary


<Serializable()> _
Public Class Idiomas
    Inherits Lista(Of Idioma)
    'Implements IGSMonedasServer


    Public Overloads ReadOnly Property Item(ByVal Cod As String) As Idioma
        Get
            Return Me.Find(Function(elemento As Idioma) elemento.Cod = Cod)
        End Get
    End Property


    ''' <summary>Añade un elemento CIdioma a la lista</summary>
    ''' <param name="Cod">Código</param>
    ''' <param name="Den">Denominación</param>
    ''' <returns>Devuelve el elemento CIdioma agregado</returns>
    Public Overloads Function Add(ByVal Cod As String, ByVal Den As String) As Idioma

        ' * Objetivo: Anyadir una moneda a la coleccion
        ' * Recibe: Datos de la moneda
        ' * Devuelve: Moneda añadida

        Dim objnewmember As Idioma

        ' Creacion de objeto moneda

        objnewmember = New Idioma(mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)

        ' Paso de los parametros al nuevo objeto moneda

        objnewmember.Cod = Cod
        objnewmember.Den = Den

        MyBase.Add(objnewmember)
        Return objnewmember

    End Function

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub



End Class
