﻿Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProceGruposPujas
    Inherits Lista(Of ProceGrupoPujas)

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Overloads ReadOnly Property Item(ByVal ID As Integer) As ProceGrupoPujas
        Get
            Return Me.Find(Function(elemento As ProceGrupoPujas) elemento.ID = ID)
        End Get
    End Property

End Class
