﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class Equipo
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _sCod As String
    Private _sDen As String
    Private _dtFecAct As Nullable(Of Date)

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Codigo As String
        Get
            Return _sCod
        End Get
        Set(ByVal value As String)
            If Not (value = _sCod) Then
                _sCod = value
                NotifyPropertyChanged("COD")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Denominacion As String
        Get
            Return _sDen
        End Get
        Set(ByVal value As String)
            If Not (value = _sDen) Then
                _sDen = value
                NotifyPropertyChanged("DEN")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaAct As Nullable(Of Date)
        Get
            Return _dtFecAct
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFecAct) Then
                _dtFecAct = value
                NotifyPropertyChanged("FECACT")
            End If
        End Set
    End Property

#End Region

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

End Class
