﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class ImpuestoValor
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iId As Integer
    Private _sPaiCod As String
    Private _sPaiDen As String
    Private _sProviCod As String
    Private _sProviDen As String
    Private _iImpuesto As Integer
    Private _dValor As Double
    Private _bVigente As Boolean
    Private _dtFechaAct As Date

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
        _bVigente = True
        _dtFechaAct = Date.Now
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        _bVigente = True
        _dtFechaAct = Date.Now
    End Sub

#End Region

    <DataMember()> _
    Property Id() As Integer
        Get
            Id = _iId
        End Get
        Set(ByVal Value As Integer)
            _iId = Value
            NotifyPropertyChanged("Id")
        End Set
    End Property

    <DataMember()> _
    Property CodigoPais() As String
        Get
            CodigoPais = _sPaiCod
        End Get
        Set(ByVal Value As String)
            _sPaiCod = Value
            NotifyPropertyChanged("CodigoPais")
        End Set
    End Property

    <DataMember()> _
    Property DenominacionPais() As String
        Get
            DenominacionPais = _sPaiDen
        End Get
        Set(ByVal Value As String)
            _sPaiDen = Value
            NotifyPropertyChanged("DenominacionPais")
        End Set
    End Property

    <DataMember()> _
    Property CodigoProvincia() As String
        Get
            CodigoProvincia = _sProviCod
        End Get
        Set(ByVal Value As String)
            _sProviCod = Value
            NotifyPropertyChanged("CodigoProvincia")
        End Set
    End Property

    <DataMember()> _
    Property DenominacionProvincia() As String
        Get
            DenominacionProvincia = _sProviDen
        End Get
        Set(ByVal Value As String)
            _sProviDen = Value
            NotifyPropertyChanged("DenominacionProvincia")
        End Set
    End Property

    <DataMember()> _
    Property Impuesto() As Integer
        Get
            Impuesto = _iImpuesto
        End Get
        Set(ByVal Value As Integer)
            _iImpuesto = Value
            NotifyPropertyChanged("Impuesto")
        End Set
    End Property

    <DataMember()> _
    Property Valor() As Double
        Get
            Valor = _dValor
        End Get
        Set(ByVal Value As Double)
            _dValor = Value
            NotifyPropertyChanged("Valor")
        End Set
    End Property

    <DataMember()> _
    Property Vigente() As Boolean
        Get
            Vigente = _bVigente
        End Get
        Set(ByVal Value As Boolean)
            _bVigente = Value
            NotifyPropertyChanged("Vigente")
        End Set
    End Property

    <DataMember()> _
    Property FechaAct() As Date
        Get
            FechaAct = _dtFechaAct
        End Get
        Set(ByVal Value As Date)
            _dtFechaAct = Value
            NotifyPropertyChanged("FechaAct")
        End Set
    End Property

    Public Function Duplicate() As ImpuestoValor
        Dim oImpValor As New ImpuestoValor
        oImpValor.Id = _iId
        oImpValor.Impuesto = _iImpuesto
        oImpValor.CodigoPais = _sPaiCod
        oImpValor.CodigoProvincia = _sProviCod
        oImpValor.DenominacionPais = _sPaiDen
        oImpValor.DenominacionProvincia = _sProviDen
        oImpValor.FechaAct = _dtFechaAct
        oImpValor.Vigente = _bVigente
        oImpValor.Valor = _dValor
        Return oImpValor
    End Function

    Public Sub Copy(ByVal oImpValor As ImpuestoValor)
        Me.Id = oImpValor.Id
        Me.Impuesto = oImpValor.Impuesto
        Me.CodigoPais = oImpValor.CodigoPais
        Me.CodigoProvincia = oImpValor.CodigoProvincia
        Me.DenominacionPais = oImpValor.DenominacionPais
        Me.DenominacionProvincia = oImpValor.DenominacionProvincia
        Me.FechaAct = oImpValor.FechaAct
        Me.Vigente = oImpValor.Vigente
        Me.Valor = oImpValor.Valor
    End Sub

End Class
