﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class Proveedor
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _sCodigo As String
    Private _sDenominacion As String
    Private _sDireccion As String
    Private _sCodPostal As String
    Private _sPoblacion As String
    Private _sPais As String
    Private _sProvincia As String
    Private _sMoneda As String
    Private _dblVal1 As Nullable(Of Double)
    Private _dblVal2 As Nullable(Of Double)
    Private _dblVal3 As Nullable(Of Double)
    Private _iTipoCal1 As Nullable(Of Integer)
    Private _sCal1 As String
    Private _iTipoCal2 As Nullable(Of Integer)
    Private _sCal2 As String
    Private _iTipoCal3 As Nullable(Of Integer)
    Private _sCal3 As String
    Private _sObservaciones As String
    Private _dtFechaAct As Nullable(Of Date)
    Private _sNIF As String
    Private _sFSPCod As String
    Private _bFSPUsado As Boolean
    Private _bPremium As Boolean
    Private _bActivo As Boolean
    Private _sURLProve As String
    Private _sPag As String
    Private _bPedForm As Boolean
    Private _dtFechaAlta As Nullable(Of Date)
    Private _bCalidadPot As Boolean
    Private _bBajaCalidad As Boolean
    Private _sViaPag As String
    Private _sTipo As String
    Private _oContactos As GSServerModel.Contactos

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Sub New(ByVal Codigo As String, ByVal Denominacion As String)
        Me.New()

        _sCodigo = Codigo
        _sDenominacion = Denominacion
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Codigo As String
        Get
            Return _sCodigo
        End Get
        Set(ByVal value As String)
            If Not (value = _sCodigo) Then
                _sCodigo = value
                NotifyPropertyChanged("Codigo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Denominacion As String
        Get
            Return _sDenominacion
        End Get
        Set(ByVal value As String)
            If Not (value = _sDenominacion) Then
                _sDenominacion = value
                NotifyPropertyChanged("Denominacion")
            End If
        End Set
    End Property

    <DataMember()> _
     Public Property FSPCod As String
        Get
            Return _sDenominacion
        End Get
        Set(ByVal value As String)
            If Not (value = _sFSPCod) Then
                _sFSPCod = value
                NotifyPropertyChanged("FSPCod")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Premium As Boolean
        Get
            Return _bPremium
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPremium) Then
                _bPremium = value
                NotifyPropertyChanged("Premium")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Activo As Boolean
        Get
            Return _bActivo
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bActivo) Then
                _bActivo = value
                NotifyPropertyChanged("Activo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Contactos As GSServerModel.Contactos
        Get
            Return _oContactos
        End Get
        Set(ByVal value As GSServerModel.Contactos)            
            _oContactos = value
            NotifyPropertyChanged("CONTACTOS")
        End Set
    End Property


#End Region

End Class
