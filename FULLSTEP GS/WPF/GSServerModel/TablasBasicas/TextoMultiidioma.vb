﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class TextoMultiidioma
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iID As Integer
    Private _sIdioma As String
    Private _sTexto As String
    Private _sTipo As String

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property ID As Integer
        Get
            Return _iID
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iID) Then
                _iID = value
                NotifyPropertyChanged("ID")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Idioma As String
        Get
            Return _sIdioma
        End Get
        Set(ByVal value As String)
            If Not (value = _sIdioma) Then
                _sIdioma = value
                NotifyPropertyChanged("IDI")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Texto As String
        Get
            Return _sTexto
        End Get
        Set(ByVal value As String)
            If Not (value = _sTexto) Then
                _sTexto = value
                NotifyPropertyChanged("TEXTO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Tipo As String
        Get
            Return _sTipo
        End Get
        Set(ByVal value As String)
            If Not (value = _sTipo) Then
                _sTipo = value
                NotifyPropertyChanged("TIPO")
            End If
        End Set
    End Property

#End Region

End Class
