﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<Serializable(), DataContract()> _
Public Class Contacto
    Inherits Security
    Implements INotifyPropertyChanged    

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _sProve As String
    Private _iID As Integer
    Private _sApellidos As String
    Private _sNombre As String
    Private _sDepartamento As String
    Private _sCargo As String
    Private _sTelefono As String
    Private _sFax As String
    Private _sEMail As String
    Private _bRecPet As Boolean
    Private _dtFecAct As Nullable(Of Date)
    Private _sTelefono2 As String
    Private _bPort As Boolean
    Private _sTelefonoMovil As String
    Private _bAprov As Boolean
    Private _iIDPort As Nullable(Of Integer)
    Private _bSubasta As Boolean
    Private _sIDI As String
    Private _iTipoEMail As Nullable(Of Fullstep.FSNLibrary.TiposDeDatos.TipoEmail)
    Private _bCalidad As Boolean
    Private _sThousanFMT As String
    Private _sDecimalFMT As String
    Private _iPrecisionFMT As Integer
    Private _sDateFMT As String
    Private _sLanguageTag As String

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Proveedor As String
        Get
            Return _sProve
        End Get
        Set(ByVal value As String)
            If Not (value = _sProve) Then
                _sProve = value
                NotifyPropertyChanged("PROVE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ID As Integer
        Get
            Return _iID
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iID) Then
                _iID = value
                NotifyPropertyChanged("ID")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Apellidos As String
        Get
            Return _sApellidos
        End Get
        Set(ByVal value As String)
            If Not (value = _sApellidos) Then
                _sApellidos = value
                NotifyPropertyChanged("APE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Nombre As String
        Get
            Return _sNombre
        End Get
        Set(ByVal value As String)
            If Not (value = _sNombre) Then
                _sNombre = value
                NotifyPropertyChanged("Nombre")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Departamento As String
        Get
            Return _sDepartamento
        End Get
        Set(ByVal value As String)
            If Not (value = _sDepartamento) Then
                _sDepartamento = value
                NotifyPropertyChanged("DEP")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Cargo As String
        Get
            Return _sCargo
        End Get
        Set(ByVal value As String)
            If Not (value = _sCargo) Then
                _sCargo = value
                NotifyPropertyChanged("Cargo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Telefono As String
        Get
            Return _sTelefono
        End Get
        Set(ByVal value As String)
            If Not (value = _sTelefono) Then
                _sTelefono = value
                NotifyPropertyChanged("Telefono")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Fax As String
        Get
            Return _sFax
        End Get
        Set(ByVal value As String)
            If Not (value = _sFax) Then
                _sFax = value
                NotifyPropertyChanged("Fax")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property EMail As String
        Get
            Return _sEMail
        End Get
        Set(ByVal value As String)
            If Not (value = _sEMail) Then
                _sEMail = value
                NotifyPropertyChanged("EMail")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property RecPet As Boolean
        Get
            Return _bRecPet
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bRecPet) Then
                _bRecPet = value
                NotifyPropertyChanged("RecPet")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FecAct As Nullable(Of Date)
        Get
            Return _dtFecAct
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFecAct) Then
                _dtFecAct = value
                NotifyPropertyChanged("FecAct")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Telefono2 As String
        Get
            Return _sTelefono2
        End Get
        Set(ByVal value As String)
            If Not (value = _sTelefono2) Then
                _sTelefono2 = value
                NotifyPropertyChanged("Telefono2")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Port As Boolean
        Get
            Return _bPort
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPort) Then
                _bPort = value
                NotifyPropertyChanged("Port")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property TelefonoMovil As String
        Get
            Return _sTelefonoMovil
        End Get
        Set(ByVal value As String)
            If Not (value = _sTelefonoMovil) Then
                _sTelefonoMovil = value
                NotifyPropertyChanged("TelefonoMovil")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Aprov As Boolean
        Get
            Return _bAprov
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bAprov) Then
                _bAprov = value
                NotifyPropertyChanged("Aprov")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property IDPort As Nullable(Of Integer)
        Get
            Return _iIDPort
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not value.Equals(_iIDPort) Then
                _iIDPort = value
                NotifyPropertyChanged("IDPort")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Subasta As Boolean
        Get
            Return _bSubasta
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bSubasta) Then
                _bSubasta = value
                NotifyPropertyChanged("Subasta")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Idioma As String
        Get
            Return _sIDI
        End Get
        Set(ByVal value As String)
            If Not (value = _sIDI) Then
                _sIDI = value
                NotifyPropertyChanged("IDI")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property TipoEMail As Nullable(Of Fullstep.FSNLibrary.TiposDeDatos.TipoEmail)
        Get
            Return _iTipoEMail
        End Get
        Set(ByVal value As Nullable(Of Fullstep.FSNLibrary.TiposDeDatos.TipoEmail))
            If Not value.Equals(_iTipoEMail) Then
                _iTipoEMail = value
                NotifyPropertyChanged("TIPOEMAIL")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Calidad As Boolean
        Get
            Return _bCalidad
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bCalidad) Then
                _bCalidad = value
                NotifyPropertyChanged("Calidad")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ThousanFMT As String
        Get
            Return _sThousanFMT
        End Get
        Set(ByVal value As String)
            If Not (value = _sThousanFMT) Then
                _sThousanFMT = value
                NotifyPropertyChanged("ThousanFMT")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property DecimalFMT As String
        Get
            Return _sDecimalFMT
        End Get
        Set(ByVal value As String)
            If Not (value = _sDecimalFMT) Then
                _sDecimalFMT = value
                NotifyPropertyChanged("DecimalFMT")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PrecisionFMT As Integer
        Get
            Return _iPrecisionFMT
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iPrecisionFMT) Then
                _iPrecisionFMT = value
                NotifyPropertyChanged("PrecisionFMT")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property DateFMT As String
        Get
            Return _sDateFMT
        End Get
        Set(ByVal value As String)
            If Not (value = _sDateFMT) Then
                _sDateFMT = value
                NotifyPropertyChanged("DateFMT")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property LanguageTag As String
        Get
            Return _sLanguageTag
        End Get
        Set(ByVal value As String)
            If Not (value = _sLanguageTag) Then
                _sLanguageTag = value
                NotifyPropertyChanged("LanguageTag")
            End If
        End Set
    End Property

#End Region

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

End Class
