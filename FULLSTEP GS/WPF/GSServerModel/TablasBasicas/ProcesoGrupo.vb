﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProcesoGrupo
    Inherits Security
    Implements INotifyPropertyChanged    

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iAnyo As Integer
    Private _sGMN1 As String
    Private _iProce As Integer
    Private _iID As Integer
    Private _sCod As String
    Private _sDenominacion As String
    Private _sDescripcion As String
    Private _sDest As String
    Private _sPag As String
    Private _dtFechaInicio As Nullable(Of Date)
    Private _dtFechaFin As Nullable(Of Date)
    Private _sProveAct As String
    Private _sEsp As String
    Private _bCerrado As Nullable(Of Boolean)
    Private _iSolicit As Nullable(Of Integer)
    Private _iSobre As Nullable(Of Integer)
    Private _dblAdjudicado As Double
    Private _dtFecAct As Nullable(Of Date)
    Private _dblPrecioSalida As Nullable(Of Double)
    Private _dblMinPujaGanadora As Nullable(Of Double)
    Private _dblMinPujaProve As Nullable(Of Double)
    Private _dblPresupuesto As Double
    Private _oItems As Items

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo() As Integer
        Get
            Anyo = _iAnyo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("Anyo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1() As String
        Get
            GMN1 = _sGMN1
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proce As Integer
        Get
            Return _iProce
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iProce) Then
                _iProce = value
                NotifyPropertyChanged("Proce")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ID As Integer
        Get
            Return _iID
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iID) Then
                _iID = value
                NotifyPropertyChanged("ID")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Cod As String
        Get
            Return _sCod
        End Get
        Set(ByVal value As String)
            If Not (value = _sCod) Then
                _sCod = value
                NotifyPropertyChanged("Cod")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Denominacion As String
        Get
            Return _sDenominacion
        End Get
        Set(ByVal value As String)
            If Not (value = _sDenominacion) Then
                _sDenominacion = value
                NotifyPropertyChanged("Denominacion")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Descripcion As String
        Get
            Return _sDescripcion
        End Get
        Set(ByVal value As String)
            If Not (value = _sDenominacion) Then
                _sDescripcion = value
                NotifyPropertyChanged("Descripcion")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Dest As String
        Get
            Return _sDest
        End Get
        Set(ByVal value As String)
            If Not (value = _sDest) Then
                _sDest = value
                NotifyPropertyChanged("Dest")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Pag As String
        Get
            Return _sPag
        End Get
        Set(ByVal value As String)
            If Not (value = _sPag) Then
                _sPag = value
                NotifyPropertyChanged("Pag")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaInicio As Nullable(Of Date)
        Get
            Return _dtFechaInicio
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaInicio) Then
                _dtFechaInicio = value
                NotifyPropertyChanged("FechaInicio")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaFin As Nullable(Of Date)
        Get
            Return _dtFechaFin
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaFin) Then
                _dtFechaFin = value
                NotifyPropertyChanged("FechaFin")
            End If
        End Set
    End Property

    <DataMember()>
    Public Property ProveAct As String
        Get
            Return _sProveAct
        End Get
        Set(ByVal value As String)
            If Not (value = _sProveAct) Then
                _sProveAct = value
                NotifyPropertyChanged("ProveAct")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Esp As String
        Get
            Return _sEsp
        End Get
        Set(ByVal value As String)
            If Not (value = _sEsp) Then
                _sEsp = value
                NotifyPropertyChanged("Esp")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Cerrado As Nullable(Of Boolean)
        Get
            Return _bCerrado
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            If Not value.Equals(_bCerrado) Then
                _bCerrado = value
                NotifyPropertyChanged("Cerrado")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Solicitud As Nullable(Of Integer)
        Get
            Return _iSolicit
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not value.Equals(_iSolicit) Then
                _iSolicit = value
                NotifyPropertyChanged("Solicitud")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Sobre As Nullable(Of Integer)
        Get
            Return _iSobre
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not value.Equals(_iSobre) Then
                _iSobre = value
                NotifyPropertyChanged("Sobre")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Adjudicado As Double
        Get
            Return _dblAdjudicado
        End Get
        Set(ByVal value As Double)
            If Not (value = _dblAdjudicado) Then
                _dblAdjudicado = value
                NotifyPropertyChanged("Adjudicado")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FecAct As Nullable(Of Date)
        Get
            Return _dtFecAct
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFecAct) Then
                _dtFecAct = value
                NotifyPropertyChanged("FecAct")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PrecioSalida As Nullable(Of Double)
        Get
            Return _dblPrecioSalida
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblPrecioSalida) Then
                _dblPrecioSalida = value
                NotifyPropertyChanged("PrecioSalida")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinPujaGanadora As Nullable(Of Double)
        Get
            Return _dblMinPujaGanadora
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblMinPujaGanadora) Then
                _dblMinPujaGanadora = value
                NotifyPropertyChanged("MinPujaGanadora")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinPujaProve As Nullable(Of Double)
        Get
            Return _dblMinPujaProve
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblMinPujaProve) Then
                _dblMinPujaProve = value
                NotifyPropertyChanged("MinPujaProve")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Presupuesto As Double
        Get
            Return _dblPresupuesto
        End Get
        Set(ByVal value As Double)
            If Not (value = _dblPresupuesto) Then
                _dblPresupuesto = value
                NotifyPropertyChanged("Prespuesto")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Items() As Items
        Get
            Return _oItems
        End Get
        Set(ByVal value As Items)
            _oItems = value
            NotifyPropertyChanged("Items")
        End Set
    End Property

#End Region

End Class
