﻿Imports System.ComponentModel
Imports System.Runtime.Serialization
Imports Fullstep.FSNLibrary.TiposDeDatos

<DataContract()> _
Public Class ProcesoSubasta
    Inherits Security
    Implements INotifyPropertyChanged    

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iAnyo As Integer
    Private _iCodigo As Integer
    Private _sGMN1 As String
    Private _sDen As String
    Private _iTipo As Integer
    Private _iModo As Integer
    Private _dtPublicarFec As Nullable(Of Date)
    Private _iMinEsperaCierre As Integer
    Private _bNotificarEventos As Boolean
    Private _dtFecApSobre As Nullable(Of Date)
    Private _iMinJaponesa As Nullable(Of Integer)
    Private _bVerProveGanador As Boolean
    Private _bVerPrecioGanador As Boolean
    Private _bVerDetallePujas As Boolean
    Private _bVerDesdePrimeraPuja As Boolean
    Private _bBajMinGanador As Boolean
    Private _iBajMinGanadorTipo As Integer
    Private _bBajMinProve As Boolean
    Private _iBajMinProveTipo As Integer
    Private _iNotifAutom As Integer
    Private _iTextoFin As Nullable(Of Integer)
    Private _dtFecAct As Nullable(Of Date)
    Private _dblPrecioSalida As Nullable(Of Double)
    Private _dblMinPujaGanadora As Nullable(Of Double)
    Private _dblMinPujaProve As Nullable(Of Double)
    Private _dblPresupuestoTotal As Double
    Private _dblPresupuesto As Double
    Private _iEstado As EstadoSubasta
    Private _oGrupos As ProcesoGrupos
    Private _oTextosFin As TextosMultiidioma

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()        
        MyBase.New()
        DatosPordefecto()        
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        DatosPordefecto()
        
    End Sub

    '<summary>Datos por defecto para la subasta</summary>        

    Private Sub DatosPordefecto()
        _iTipo = TipoSubasta.Inglesa
        _iEstado = EstadoSubasta.NoComenzada
        _iModo = ModoSubasta.Item
        _iMinEsperaCierre = 5
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo() As Integer
        Get
            Anyo = _iAnyo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("Anyo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Codigo() As Integer
        Get
            Codigo = _iCodigo
        End Get
        Set(ByVal Value As Integer)
            If Not (Value = _iCodigo) Then
                _iCodigo = Value
                NotifyPropertyChanged("Codigo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1() As String
        Get
            GMN1 = _sGMN1
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Denominacion() As String
        Get
            Return _sDen
        End Get
        Set(ByVal value As String)
            If Not (value = _sDen) Then
                _sDen = value
                NotifyPropertyChanged("Denominacion")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Tipo As Integer
        Get
            Return _iTipo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iTipo) Then
                _iTipo = value
                NotifyPropertyChanged("Tipo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Modo As Integer
        Get
            Return _iModo
        End Get
        Set(ByVal value As Integer)
            If Not value = _iModo Then
                _iModo = value
                NotifyPropertyChanged("Modo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaUltPub As Nullable(Of Date)
        Get
            Return _dtPublicarFec
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtPublicarFec) Then
                _dtPublicarFec = value
                NotifyPropertyChanged("FechaUltPub")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinEsperaCierre As Integer
        Get
            Return _iMinEsperaCierre
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iMinEsperaCierre) Then
                _iMinEsperaCierre = value
                NotifyPropertyChanged("MinEsperaCierre")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property NotificarEventos As Boolean
        Get
            Return _bNotificarEventos
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bNotificarEventos) Then
                _bNotificarEventos = value
                NotifyPropertyChanged("NotificarEventos")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaAperturaSobre As Nullable(Of Date)
        Get
            Return _dtFecApSobre
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFecApSobre) Then
                _dtFecApSobre = value
                NotifyPropertyChanged("FechaAperturaSobre")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinutosJaponesa As Nullable(Of Integer)
        Get
            Return _iMinJaponesa
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not (value.Equals(_iMinJaponesa)) Then
                _iMinJaponesa = value
                NotifyPropertyChanged("MinutosJaponesa")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property VerProveGanador As Boolean
        Get
            Return _bVerProveGanador
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bVerProveGanador) Then
                _bVerProveGanador = value
                NotifyPropertyChanged("VerProveGanador")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property VerPrecioGanador As Boolean
        Get
            Return _bVerPrecioGanador
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bVerPrecioGanador) Then
                _bVerPrecioGanador = value
                NotifyPropertyChanged("VerPrecioGanador")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property VerDetallePujas As Boolean
        Get
            Return _bVerDetallePujas
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bVerDetallePujas) Then
                _bVerDetallePujas = value
                NotifyPropertyChanged("VerDetallePujas")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property VerDesdePrimeraPuja As Boolean
        Get
            Return _bVerDesdePrimeraPuja
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bVerDesdePrimeraPuja) Then
                _bVerDesdePrimeraPuja = value
                NotifyPropertyChanged("VerDesdePrimeraPuja")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property BajadaMinGanador As Boolean
        Get
            Return _bBajMinGanador
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bBajMinGanador) Then
                _bBajMinGanador = value
                NotifyPropertyChanged("BajadaMinGanador")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property BajadaMinGanadorTipo As Integer
        Get
            Return _iBajMinGanadorTipo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iBajMinGanadorTipo) Then
                _iBajMinGanadorTipo = value
                NotifyPropertyChanged("BajadaMinGanadorTipo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property BajadaMinProve As Boolean
        Get
            Return _bBajMinProve
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bBajMinProve) Then
                _bBajMinProve = value
                NotifyPropertyChanged("BajadaMinProve")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property BajadaMinProveTipo As Integer
        Get
            Return _iBajMinProveTipo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iBajMinProveTipo) Then
                _iBajMinProveTipo = value
                NotifyPropertyChanged("BajadaMinProveTipo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property NotifAutom As Integer
        Get
            Return _iNotifAutom
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iNotifAutom) Then
                _iNotifAutom = value
                NotifyPropertyChanged("NotifAutom")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property TextoFin As Nullable(Of Integer)
        Get
            Return _iTextoFin
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not (value.Equals(_iTextoFin)) Then
                _iTextoFin = value
                NotifyPropertyChanged("TextoFin")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaAct As Nullable(Of Date)
        Get
            Return _dtFecAct
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFecAct) Then
                _dtFecAct = value
                NotifyPropertyChanged("FechaAct")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PrecioSalida As Nullable(Of Double)
        Get
            Return _dblPrecioSalida
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblPrecioSalida) Then
                _dblPrecioSalida = value
                NotifyPropertyChanged("PrecioSalida")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinPujaGanadora As Nullable(Of Double)
        Get
            Return _dblMinPujaGanadora
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblMinPujaGanadora) Then
                _dblMinPujaGanadora = value
                NotifyPropertyChanged("MinPujaGanadora")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinPujaProve As Nullable(Of Double)
        Get
            Return _dblMinPujaProve
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblMinPujaProve) Then
                _dblMinPujaProve = value
                NotifyPropertyChanged("MinPujaProve")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PresupuestoTotal As Double
        Get
            Return _dblPresupuestoTotal
        End Get
        Set(ByVal value As Double)
            If Not value = _dblPresupuestoTotal Then
                _dblPresupuestoTotal = value
                NotifyPropertyChanged("PresupuestoTotal")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Presupuesto As Double
        Get
            Return _dblPresupuesto
        End Get
        Set(ByVal value As Double)
            If Not (value = _dblPresupuesto) Then
                _dblPresupuesto = value
                NotifyPropertyChanged("Prespuesto")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Estado As EstadoSubasta
        Get
            Return _iEstado
        End Get
        Set(ByVal value As EstadoSubasta)
            If Not (value = _iEstado) Then
                _iEstado = value
                NotifyPropertyChanged("Estado")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Grupos() As ProcesoGrupos
        Get
            Return _oGrupos
        End Get
        Set(ByVal value As ProcesoGrupos)
            _oGrupos = value
            NotifyPropertyChanged("Grupos")
        End Set
    End Property

    <DataMember()> _
    Public Property TextosFin() As TextosMultiidioma
        Get
            Return _oTextosFin
        End Get
        Set(ByVal value As TextosMultiidioma)
            _oTextosFin = value
            NotifyPropertyChanged("TextosFin")
        End Set
    End Property

#End Region

End Class
