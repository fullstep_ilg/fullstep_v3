﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProceProvePetOferta
    Inherits Security
    Implements INotifyPropertyChanged    

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iAnyo As Integer
    Private _sGMN1 As String
    Private _iProce As Integer
    Private _sProve As String
    Private _iID As Integer
    Private _sApeCon As String
    Private _sNomCon As String
    Private _bViaWeb As Boolean
    Private _bViaEmail As Boolean
    Private _bViaImp As Boolean
    Private _dtFecha As Date
    Private _iTipoPet As Integer
    Private _dtFechaReu As Date
    Private _bCerrado As Boolean
    'Propiedades sin campos de BD
    Private _sEMail As String
    Private _sTelefono As String
    Private _sTelefono2 As String
    Private _sTelefonoMovil As String
    Private _sFax As String
    Private _bAntesCierreParcial As Boolean
    Private _bPorContactoPortal As Boolean
    Private _bCancelMail As Boolean
    Private _sCodProvePortal As String

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Sub New(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Integer, ByVal Prove As String, ByVal bViaWeb As Boolean, ByVal bViaEMail As Boolean, _
                  ByVal bViaImpreso As Boolean, ByVal Fecha As Datetime, ByVal ID As Integer, ByVal Apellidos As String, ByVal Cerrado As Boolean, _
                  ByVal TipoComunicacion As Integer, Optional ByVal Nombre As String = Nothing, Optional ByVal FechaReu As Date = Nothing, _
                  Optional ByVal Email As String = Nothing, Optional ByVal Tfno As String = Nothing, Optional ByVal Tfno2 As String = Nothing, _
                  Optional ByVal Fax As String = Nothing, Optional ByVal TfnoMovil As String = Nothing, Optional ByVal AntesDeCierreParcial As Boolean = False, _
                  Optional ByVal PorContactoPortal As Boolean = False)
        Me.new()

        _iAnyo = Anyo
        _sGMN1 = GMN1
        _iProce = Proce
        _sProve = Prove
        _bViaEmail = bViaEMail
        _bViaWeb = bViaWeb
        _bViaImp = bViaImpreso
        _dtFecha = Fecha
        _iID = ID
        _sApeCon = Apellidos
        _bCerrado = Cerrado
        _iTipoPet = TipoComunicacion
        _sNomCon = Nombre
        _dtFechaReu = FechaReu

        _sEMail = Email
        _sTelefono = Tfno
        _sTelefono2 = Tfno2
        _sTelefonoMovil = TfnoMovil
        _sFax = Fax
        _bAntesCierreParcial = AntesCierreParcial
        _bPorContactoPortal = PorContactoPortal
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo As Integer
        Get
            Return (_iAnyo)
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("ANYO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1 As String
        Get
            Return (_sGMN1)
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proce As Integer
        Get
            Return (_iProce)
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iProce) Then
                _iProce = value
                NotifyPropertyChanged("PROCE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ID As Integer
        Get
            Return (_iID)
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iID) Then
                _iID = value
                NotifyPropertyChanged("ID")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ApellidoCon As String
        Get
            Return (_sApeCon)
        End Get
        Set(ByVal value As String)
            If Not (value = _sApeCon) Then
                _sApeCon = value
                NotifyPropertyChanged("APECON")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property NombreCon As String
        Get
            Return (_sNomCon)
        End Get
        Set(ByVal value As String)
            If Not (value = _sNomCon) Then
                _sNomCon = value
                NotifyPropertyChanged("NOMCON")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ViaWeb As Boolean
        Get
            Return (_bViaWeb)
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bViaWeb) Then
                _bViaWeb = value
                NotifyPropertyChanged("WEB")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ViaEmail As Boolean
        Get
            Return (_bViaEmail)
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bViaEmail) Then
                _bViaEmail = value
                NotifyPropertyChanged("EMAIL")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ViaImpreso As Boolean
        Get
            Return (_bViaImp)
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bViaImp) Then
                _bViaImp = value
                NotifyPropertyChanged("IMP")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Fecha As Date
        Get
            Return (_dtFecha)
        End Get
        Set(ByVal value As Date)
            If Not Date.Equals(value, _dtFecha) Then
                _dtFecha = value
                NotifyPropertyChanged("FECHA")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property TipoPet As Integer
        Get
            Return (_iTipoPet)
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iTipoPet) Then
                _iTipoPet = value
                NotifyPropertyChanged("TIPOPET")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaReu As Date
        Get
            Return (_dtFechaReu)
        End Get
        Set(ByVal value As Date)
            If Not Date.Equals(value, _dtFechaReu) Then
                _dtFechaReu = value
                NotifyPropertyChanged("FECHAREU")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Cerrado As Boolean
        Get
            Return (_bCerrado)
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bCerrado) Then
                _bCerrado = value
                NotifyPropertyChanged("CERRADO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property EMail As String
        Get
            Return (_sEMail)
        End Get
        Set(ByVal value As String)
            If Not (value = _sEMail) Then
                _sEMail = value
                NotifyPropertyChanged("EMAIL")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Telefono As String
        Get
            Return (_sTelefono)
        End Get
        Set(ByVal value As String)
            If Not (value = _sTelefono) Then
                _sTelefono = value
                NotifyPropertyChanged("TELEFONO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Telefono2 As String
        Get
            Return (_sTelefono2)
        End Get
        Set(ByVal value As String)
            If Not (value = _sTelefono2) Then
                _sTelefono2 = value
                NotifyPropertyChanged("TELEFONO2")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property TelefonoMovil As String
        Get
            Return (_sTelefonoMovil)
        End Get
        Set(ByVal value As String)
            If Not (value = _sTelefonoMovil) Then
                _sTelefonoMovil = value
                NotifyPropertyChanged("TELEFONOMOVIL")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Fax As String
        Get
            Return (_sFax)
        End Get
        Set(ByVal value As String)
            If Not (value = _sFax) Then
                _sFax = value
                NotifyPropertyChanged("FAX")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property AntesCierreParcial As Boolean
        Get
            Return (_bAntesCierreParcial)
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bAntesCierreParcial) Then
                _bAntesCierreParcial = value
                NotifyPropertyChanged("ANTESCIERREPARCIAL")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PorContactoPortal As Boolean
        Get
            Return (_bPorContactoPortal)
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPorContactoPortal) Then
                _bPorContactoPortal = value
                NotifyPropertyChanged("PORCONTACTOPORTAL")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoProveedor As String
        Get
            Return (_sProve)
        End Get
        Set(ByVal value As String)
            If Not (value = _sProve) Then
                _sProve = value
                NotifyPropertyChanged("PROVE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property CancelMail As Boolean
        Get
            Return (_bCancelMail)
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bCancelMail) Then
                _bCancelMail = value
                NotifyPropertyChanged("CANCELMAIL")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoProveedorPortal As String
        Get
            Return (_sCodProvePortal)
        End Get
        Set(ByVal value As String)
            If Not (value = _sCodProvePortal) Then
                _sCodProvePortal = value
                NotifyPropertyChanged("CODPROVEPORTAL")
            End If
        End Set
    End Property

#End Region

End Class
