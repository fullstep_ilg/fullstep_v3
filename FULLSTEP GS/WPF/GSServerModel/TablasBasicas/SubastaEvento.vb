﻿Imports System.ComponentModel
Imports System.Runtime.Serialization
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class SubastaEvento
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iID As Integer
    Private _iAnyo As Integer
    Private _sGMN1 As String
    Private _iProce As Integer
    Private _iAccion As AccionSubasta
    Private _dtFecha As Date
    Private _iExten As Nullable(Of Integer)
    Private _dtFecFinHist As Nullable(Of Date)
    Private _iModReiniSub As Nullable(Of ModoReinicioSubasta)
    Private _iMinReiniSub As Nullable(Of Integer)
    Private _dtFecReiniSub As Nullable(Of Date)
    Private _iModInTSub As Nullable(Of ModoIncTiempoSubasta)
    Private _iMinIncTSub As Nullable(Of Integer)
    Private _sProve As String

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub
#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property ID() As Integer
        Get
            ID = _iID
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iID) Then
                _iID = value
                NotifyPropertyChanged("ID")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Anyo() As Integer
        Get
            Anyo = _iAnyo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("Anyo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proceso() As Integer
        Get
            Return _iProce
        End Get
        Set(ByVal Value As Integer)
            If Not (Value = _iProce) Then
                _iProce = Value
                NotifyPropertyChanged("Proceso")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1() As String
        Get
            GMN1 = _sGMN1
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Accion() As AccionSubasta
        Get
            Return _iAccion
        End Get
        Set(ByVal Value As AccionSubasta)
            If Not (Value = _iAccion) Then
                _iAccion = Value
                NotifyPropertyChanged("Accion")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Fecha() As Date
        Get
            Return _dtFecha
        End Get
        Set(ByVal Value As Date)
            If Not Value.Equals(_dtFecha) Then
                _dtFecha = Value
                NotifyPropertyChanged("Fecha")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Extension() As Nullable(Of Integer)
        Get
            Return _iExten
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            If Not Value.Equals(_iExten) Then
                _iExten = Value
                NotifyPropertyChanged("Extension")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaFinHist() As Nullable(Of Date)
        Get
            Return _dtFecFinHist
        End Get
        Set(ByVal Value As Nullable(Of Date))
            If Not Value.Equals(_dtFecFinHist) Then
                _dtFecFinHist = Value
                NotifyPropertyChanged("FecFinHist")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ModoReinicioSubasta() As Nullable(Of ModoReinicioSubasta)
        Get
            Return _iModReiniSub
        End Get
        Set(ByVal Value As Nullable(Of ModoReinicioSubasta))
            If Not Value.Equals(_iModReiniSub) Then
                _iModReiniSub = Value
                NotifyPropertyChanged("ModoReinicioSubasta")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinReinicioSubasta() As Nullable(Of Integer)
        Get
            Return _iMinReiniSub
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            If Not Value.Equals(_iMinReiniSub) Then
                _iMinReiniSub = Value
                NotifyPropertyChanged("MinReinicioSubasta")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaReinicioSubasta() As Nullable(Of Date)
        Get
            Return _dtFecReiniSub
        End Get
        Set(ByVal Value As Nullable(Of Date))
            If Not Value.Equals(_dtFecReiniSub) Then
                _dtFecReiniSub = Value
                NotifyPropertyChanged("FechaReinicioSubasta")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ModoIncTiempoSubasta() As Nullable(Of ModoIncTiempoSubasta)
        Get
            Return _iModInTSub
        End Get
        Set(ByVal Value As Nullable(Of ModoIncTiempoSubasta))
            If Not Value.Equals(_iModInTSub) Then
                _iModInTSub = Value
                NotifyPropertyChanged("ModoIncTiempoSubasta")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinIncTiempoSubasta() As Nullable(Of Integer)
        Get
            Return _iMinIncTSub
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            If Not Value.Equals(_iMinIncTSub) Then
                _iMinIncTSub = Value
                NotifyPropertyChanged("MinIncTiempoSubasta")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proveedor() As String
        Get
            Return _sProve
        End Get
        Set(ByVal value As String)
            If Not (value = _sProve) Then
                _sProve = value
                NotifyPropertyChanged("Proveedor")
            End If
        End Set
    End Property

#End Region

End Class
