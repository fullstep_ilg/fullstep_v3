﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class Persona
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _sCodigo As String
    Private _sApellidos As String
    Private _sNombre As String
    Private _sCar As String
    Private _sTelefono As String
    Private _sFax As String
    Private _sEMail As String
    Private _sUON1 As String
    Private _sUON2 As String
    Private _sUON3 As String
    Private _sDep As String
    Private _dtFecAct As Nullable(Of Date)
    Private _sTelefono2 As String
    Private _sEqp As String
    Private _bBajaLog As Boolean

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
     Public Property Codigo As String
        Get
            Return _sCodigo
        End Get
        Set(ByVal value As String)
            If Not (value = _sCodigo) Then
                _sCodigo = value
                NotifyPropertyChanged("Codigo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Apellidos As String
        Get
            Return _sApellidos
        End Get
        Set(ByVal value As String)
            If Not (value = _sApellidos) Then
                _sApellidos = value
                NotifyPropertyChanged("Apellidos")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Nombre As String
        Get
            Return _sNombre
        End Get
        Set(ByVal value As String)
            If Not (value = _sNombre) Then
                _sNombre = value
                NotifyPropertyChanged("Nombre")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Car As String
        Get
            Return _sCar
        End Get
        Set(ByVal value As String)
            If Not (value = _sCar) Then
                _sCar = value
                NotifyPropertyChanged("Car")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Telefono As String
        Get
            Return _sTelefono
        End Get
        Set(ByVal value As String)
            If Not (value = _sTelefono) Then
                _sTelefono = value
                NotifyPropertyChanged("Telefono")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Fax As String
        Get
            Return _sFax
        End Get
        Set(ByVal value As String)
            If Not (value = _sFax) Then
                _sFax = value
                NotifyPropertyChanged("Fax")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property EMail As String
        Get
            Return _sEMail
        End Get
        Set(ByVal value As String)
            If Not (value = _sEMail) Then
                _sEMail = value
                NotifyPropertyChanged("EMail")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UON1 As String
        Get
            Return _sUON1
        End Get
        Set(ByVal value As String)
            If Not (value = _sUON1) Then
                _sUON1 = value
                NotifyPropertyChanged("UON1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UON2 As String
        Get
            Return _sUON2
        End Get
        Set(ByVal value As String)
            If Not (value = _sUON2) Then
                _sUON2 = value
                NotifyPropertyChanged("UON2")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UON3 As String
        Get
            Return _sUON3
        End Get
        Set(ByVal value As String)
            If Not (value = _sUON3) Then
                _sUON3 = value
                NotifyPropertyChanged("UON3")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Departamento As String
        Get
            Return _sDep
        End Get
        Set(ByVal value As String)
            If Not (value = _sDep) Then
                _sDep = value
                NotifyPropertyChanged("Departamento")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FecAct As Nullable(Of Date)
        Get
            Return _dtFecAct
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFecAct) Then
                _dtFecAct = value
                NotifyPropertyChanged("FecAct")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Telefono2 As String
        Get
            Return _sTelefono2
        End Get
        Set(ByVal value As String)
            If Not (value = _sTelefono2) Then
                _sTelefono2 = value
                NotifyPropertyChanged("Telefono2")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Equipo As String
        Get
            Return _sEqp
        End Get
        Set(ByVal value As String)
            If Not (value = _sEqp) Then
                _sEqp = value
                NotifyPropertyChanged("Equipo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property BajaLog As Boolean
        Get
            Return _bBajaLog
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bBajaLog) Then
                _bBajaLog = value
                NotifyPropertyChanged("BajaLog")
            End If
        End Set
    End Property

#End Region

End Class
