﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class Comprador
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _sEqp As String
    Private _sCod As String
    Private _sApe As String
    Private _sNom As String
    Private _sTfno As String
    Private _sFax As String
    Private _sEmail As String
    Private _dtFecAct As Date
    Private _sCar As String
    Private _sUON1 As String
    Private _sUON2 As String
    Private _sUON3 As String
    Private _sDep As String
    Private _sTfno2 As String

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Equipo As String
        Get
            Return _sEqp
        End Get
        Set(ByVal value As String)
            If Not (value = _sEqp) Then
                _sEqp = value
                NotifyPropertyChanged("EQP")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Codigo As String
        Get
            Return _sCod
        End Get
        Set(ByVal value As String)
            If Not (value = _sCod) Then
                _sCod = value
                NotifyPropertyChanged("COD")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Apellidos As String
        Get
            Return _sApe
        End Get
        Set(ByVal value As String)
            If Not (value = _sApe) Then
                _sApe = value
                NotifyPropertyChanged("APE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Nombre As String
        Get
            Return _sNom
        End Get
        Set(ByVal value As String)
            If Not (value = _sNom) Then
                _sNom = value
                NotifyPropertyChanged("MOM")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Telefono As String
        Get
            Return _sTfno
        End Get
        Set(ByVal value As String)
            If Not (value = _sTfno) Then
                _sTfno = value
                NotifyPropertyChanged("TFNO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Telefono2 As String
        Get
            Return _sTfno2
        End Get
        Set(ByVal value As String)
            If Not (value = _sTfno2) Then
                _sTfno2 = value
                NotifyPropertyChanged("TFNO2")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Fax As String
        Get
            Return _sFax
        End Get
        Set(ByVal value As String)
            If Not (value = _sFax) Then
                _sFax = value
                NotifyPropertyChanged("FAX")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property EMail As String
        Get
            Return _sEmail
        End Get
        Set(ByVal value As String)
            If Not (value = _sEmail) Then
                _sEmail = value
                NotifyPropertyChanged("EMAIL")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaAct As Date
        Get
            Return _dtFecAct
        End Get
        Set(ByVal value As Date)
            If Not Date.Equals(value, _dtFecAct) Then
                _dtFecAct = value
                NotifyPropertyChanged("FECACT")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Car As String
        Get
            Return _sCar
        End Get
        Set(ByVal value As String)
            If Not (value = _sCar) Then
                _sCar = value
                NotifyPropertyChanged("CAR")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UON1 As String
        Get
            Return _sUON1
        End Get
        Set(ByVal value As String)
            If Not (value = _sUON1) Then
                _sUON1 = value
                NotifyPropertyChanged("UON1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UON2 As String
        Get
            Return _sUON2
        End Get
        Set(ByVal value As String)
            If Not (value = _sUON2) Then
                _sUON2 = value
                NotifyPropertyChanged("UON2")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UON3 As String
        Get
            Return _sUON3
        End Get
        Set(ByVal value As String)
            If Not (value = _sUON3) Then
                _sUON3 = value
                NotifyPropertyChanged("UON3")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Departamento As String
        Get
            Return _sDep
        End Get
        Set(ByVal value As String)
            If Not (value = _sDep) Then
                _sDep = value
                NotifyPropertyChanged("DEP")
            End If
        End Set
    End Property

#End Region

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

End Class
