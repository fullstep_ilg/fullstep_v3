﻿Imports System.Runtime.Serialization

<DataContract()> _
Public Class Proveedores
    Inherits Lista(Of Proveedor)

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Overloads ReadOnly Property Item(ByVal Codigo As String) As Proveedor
        Get
            Return Me.Find(Function(elemento As Proveedor) elemento.Codigo = Codigo)
        End Get
    End Property

End Class
