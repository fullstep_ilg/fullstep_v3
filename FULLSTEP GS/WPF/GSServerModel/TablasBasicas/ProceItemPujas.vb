﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProceItemPujas
    Inherits ProceProvesPujas

    Private _iAnyo As Integer
    Private _sGMN1 As String
    Private _iProce As Integer
    Private _iID As Integer    

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo As Integer
        Get
            Return _iAnyo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1 As String
        Get
            Return _sGMN1
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proce As Integer
        Get
            Return _iProce
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iProce) Then
                _iProce = value
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ID As Integer
        Get
            Return _iID
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iID) Then
                _iID = value
            End If
        End Set
    End Property

#End Region

End Class
