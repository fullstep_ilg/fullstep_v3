﻿Imports System.Runtime.Serialization

<DataContract()> _
Public Class Items
    Inherits Lista(Of Item)

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Overloads Function Find(ByVal Anyo As Integer, ByVal GMN1_PROCE As String, ByVal Proce As Integer, ByVal ID As Integer) As GSServerModel.Item
        For Each oItem As GSServerModel.Item In Me
            If oItem.Anyo = Anyo And oItem.GMN1_PROCE = GMN1_PROCE And oItem.Proce = Proce And oItem.ID = ID Then
                Return oItem
            End If
        Next
    End Function

End Class
