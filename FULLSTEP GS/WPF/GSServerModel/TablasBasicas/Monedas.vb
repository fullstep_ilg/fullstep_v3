﻿Imports System
Imports System.Data.SqlClient
Imports System.Exception
Imports System.Collections.ObjectModel
'Imports System.ServiceModel
Imports Fullstep.FSNLibrary

<Serializable()> _
Public Class Monedas
    'Inherits ObservableCollection(Of Moneda)
    Inherits Lista(Of Moneda)
    'Implements IGSMonedasServer


    Public Overloads ReadOnly Property Item(ByVal Cod As String) As Moneda
        Get
            Return Me.Find(Function(elemento As Moneda) elemento.Codigo = Cod)
        End Get
    End Property

    ''' <summary>Añade un elemento CMoneda a la lista</summary>
    ''' <param name="Cod">Código</param>
    ''' <param name="Den">Denominación</param>
    ''' <param name="EQUIV">Equivalencia</param>
    ''' <param name="FECACT">Fecha última actualización</param>
    ''' <returns>Devuelve el elemento CMoneda agregado</returns>
    Public Overloads Function Add(ByVal Cod As String, ByVal Den As String, ByVal EQUIV As Double, ByVal FECACT As Object) As Moneda

        ' * Objetivo: Anyadir una moneda a la coleccion
        ' * Recibe: Datos de la moneda
        ' * Devuelve: Moneda añadida

        Dim objnewmember As Moneda

        ' Creacion de objeto moneda

        'objnewmember = New Moneda(mDBServer, mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)
        objnewmember = New Moneda(mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)

        ' Paso de los parametros al nuevo objeto moneda

        objnewmember.Codigo = Cod
        objnewmember.Denominacion = Den
        objnewmember.Equivalencia = EQUIV
        objnewmember.FecAct = FECACT
        ' objnewmember.Denominaciones = New Multiidiomas(mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)

        MyBase.Add(objnewmember)
        Return objnewmember

    End Function

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

End Class
