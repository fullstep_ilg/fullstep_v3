﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class Item
    Inherits Security
    Implements INotifyPropertyChanged    

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iAnyo As Integer
    Private _iProce As Integer
    Private _sGMN1_PROCE As String
    Private _iID As Integer
    Private _sArticulo As String
    Private _sDescripcion As String
    Private _sGMN1 As String
    Private _sGMN2 As String
    Private _sGMN3 As String
    Private _sGMN4 As String
    Private _sDest As String
    Private _sUNI As String
    Private _sPag As String
    Private _dblCantidad As Double
    Private _dblPrecio As Nullable(Of Double)
    Private _dblPresupuesto As Nullable(Of Double)
    Private _dtFechaIni As Date
    Private _dtFechaFin As Date
    Private _dblObj As Nullable(Of Double)
    Private _sEsp As String
    Private _bConf As Boolean
    Private _dtFecAct As Nullable(Of Date)
    Private _dtFechaObj As Nullable(Of Date)
    Private _sProveAct As String
    Private _bEst As Nullable(Of Boolean)
    Private _iGrupo As Integer
    Private _iSolicitud As Nullable(Of Integer)
    Private _iLineaSolicitud As Nullable(Of Integer)
    Private _dtFechaUltReu As Nullable(Of Date)
    Private _dtFechaCierre As Nullable(Of Date)
    Private _bAdjCom As Boolean
    Private _iCampoSolicitud As Nullable(Of Integer)
    Private _dblMinPujaGanadora As Nullable(Of Double)
    Private _dblPrecioSalida As Nullable(Of Double)
    Private _dblMinPujaProve As Nullable(Of Double)

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo As Integer
        Get
            Return (_iAnyo)
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("Anyo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proce As Integer
        Get
            Return _iProce
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iProce) Then
                _iProce = value
                NotifyPropertyChanged("Proce")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1_PROCE As String
        Get
            Return _sGMN1_PROCE
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1_PROCE) Then
                _sGMN1_PROCE = value
                NotifyPropertyChanged("GMN1_PROCE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ID As Integer
        Get
            Return _iID
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iID) Then
                _iID = value
                NotifyPropertyChanged("ID")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Articulo As String
        Get
            Return _sArticulo
        End Get
        Set(ByVal value As String)
            If Not (value = _sArticulo) Then
                _sArticulo = value
                NotifyPropertyChanged("Articulo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Descripcion As String
        Get
            Return _sDescripcion
        End Get
        Set(ByVal value As String)
            If Not (value = _sDescripcion) Then
                _sDescripcion = value
                NotifyPropertyChanged("Descripcion")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1 As String
        Get
            Return _sGMN1
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN2 As String
        Get
            Return _sGMN2
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN2) Then
                _sGMN2 = value
                NotifyPropertyChanged("GMN2")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN3 As String
        Get
            Return _sGMN3
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN3) Then
                _sGMN3 = value
                NotifyPropertyChanged("GMN3")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN4 As String
        Get
            Return _sGMN4
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN4) Then
                _sGMN4 = value
                NotifyPropertyChanged("GMN4")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Dest As String
        Get
            Return _sDest
        End Get
        Set(ByVal value As String)
            If Not (value = _sDest) Then
                _sDest = value
                NotifyPropertyChanged("Dest")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Unidad As String
        Get
            Return _sUNI
        End Get
        Set(ByVal value As String)
            If Not (value = _sUNI) Then
                _sUNI = value
                NotifyPropertyChanged("Unidad")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Pag As String
        Get
            Return _sPag
        End Get
        Set(ByVal value As String)
            If Not (value = _sPag) Then
                _sPag = value
                NotifyPropertyChanged("Pag")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Cantidad As Double
        Get
            Return _dblCantidad
        End Get
        Set(ByVal value As Double)
            If Not (value = _dblCantidad) Then
                _dblCantidad = value
                NotifyPropertyChanged("Cantidad")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Precio As Nullable(Of Double)
        Get
            Return _dblPrecio
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblPrecio) Then
                _dblPrecio = value
                NotifyPropertyChanged("Precio")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Presupuesto As Nullable(Of Double)
        Get
            Return _dblPresupuesto
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblPresupuesto) Then
                _dblPresupuesto = value
                NotifyPropertyChanged("Presupuesto")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaInicio As Date
        Get
            Return _dtFechaIni
        End Get
        Set(ByVal value As Date)
            If Not Date.Equals(value, _dtFechaIni) Then
                _dtFechaIni = value
                NotifyPropertyChanged("FechaInicio")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaFin As Date
        Get
            Return _dtFechaFin
        End Get
        Set(ByVal value As Date)
            If Not Date.Equals(value, _dtFechaFin) Then
                _dtFechaFin = value
                NotifyPropertyChanged("FechaFin")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Obj As Nullable(Of Double)
        Get
            Return _dblObj
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblObj) Then
                _dblObj = value
                NotifyPropertyChanged("Obj")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Esp As String
        Get
            Return _sEsp
        End Get
        Set(ByVal value As String)
            If Not (value = _sEsp) Then
                _sEsp = value
                NotifyPropertyChanged("Esp")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Conf As Boolean
        Get
            Return _bConf
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bConf) Then
                _bConf = value
                NotifyPropertyChanged("Conf")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FecAct As Nullable(Of Date)
        Get
            Return _dtFecAct
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFecAct) Then
                _dtFecAct = value
                NotifyPropertyChanged("FecAct")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaObj As Nullable(Of Date)
        Get
            Return _dtFechaObj
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaObj) Then
                _dtFechaObj = value
                NotifyPropertyChanged("FechaObj")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ProveAct As String
        Get
            Return _sProveAct
        End Get
        Set(ByVal value As String)
            If Not (value = _sProveAct) Then
                _sProveAct = value
                NotifyPropertyChanged("ProveAct")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Est As Nullable(Of Boolean)
        Get
            Return _bEst
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            If Not value.Equals(_bEst) Then
                _bEst = value
                NotifyPropertyChanged("Est")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Grupo As Integer
        Get
            Return _iGrupo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iGrupo) Then
                _iGrupo = value
                NotifyPropertyChanged("Grupo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Solicitud As Nullable(Of Integer)
        Get
            Return _iSolicitud
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not value.Equals(_iSolicitud) Then
                _iSolicitud = value
                NotifyPropertyChanged("Solicitud")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property LineaSolicitud As Nullable(Of Integer)
        Get
            Return _iLineaSolicitud
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not value.Equals(_iLineaSolicitud) Then
                _iLineaSolicitud = value
                NotifyPropertyChanged("LineaSolicitud")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaUltReu As Nullable(Of Date)
        Get
            Return _dtFechaUltReu
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaUltReu) Then
                _dtFechaUltReu = value
                NotifyPropertyChanged("FechaUltReu")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCierre As Nullable(Of Date)
        Get
            Return _dtFechaCierre
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaCierre) Then
                _dtFechaCierre = value
                NotifyPropertyChanged("FechaCierre")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property AdjCom As Boolean
        Get
            Return _bAdjCom
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bAdjCom) Then
                _bAdjCom = value
                NotifyPropertyChanged("AdjCom")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property CampoSolicitud As Nullable(Of Integer)
        Get
            Return _iCampoSolicitud
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not value.Equals(_iCampoSolicitud) Then
                _iCampoSolicitud = value
                NotifyPropertyChanged("CampoSolicitud")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinPujaGanadora As Nullable(Of Double)
        Get
            Return _dblMinPujaGanadora
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblMinPujaGanadora) Then
                _dblMinPujaGanadora = value
                NotifyPropertyChanged("MinPujaGanadora")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PrecioSalida As Nullable(Of Double)
        Get
            Return _dblPrecioSalida
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblPrecioSalida) Then
                _dblPrecioSalida = value
                NotifyPropertyChanged("PrecioSalida")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinPujaProve As Nullable(Of Double)
        Get
            Return _dblMinPujaProve
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblMinPujaProve) Then
                _dblMinPujaProve = value
                NotifyPropertyChanged("MinPujaProve")
            End If
        End Set
    End Property

#End Region

End Class
