﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProcesoSubastaRegla
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iAnyo As Integer
    Private _sGMN1 As String
    Private _iProce As Integer
    Private _iID As Integer
    Private _sNom As String
    Private _sCom As String
    Private _dtFecAct As Date
    Private _dtFecAlta As Date
    Private _iAdjunProce As Integer
    Private _iCont As Integer
    Private _guDGuid As Guid
    Private _iTamañoArchivo As Integer

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo As Integer
        Get
            Return _iAnyo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("Anyo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1 As String
        Get
            Return _sGMN1
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proce As Integer
        Get
            Return _iProce
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iProce) Then
                _iProce = value
                NotifyPropertyChanged("Proce")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ID As Integer
        Get
            Return _iID
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iID) Then
                _iID = value
                NotifyPropertyChanged("ID")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Nombre As String
        Get
            Return _sNom
        End Get
        Set(ByVal value As String)
            If Not (value = _sNom) Then
                _sNom = value
                NotifyPropertyChanged("Nom")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Comentario As String
        Get
            Return _sCom
        End Get
        Set(ByVal value As String)
            If Not (value = _sCom) Then
                _sCom = value
                NotifyPropertyChanged("Com")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FecAct As Date
        Get
            Return _dtFecAct
        End Get
        Set(ByVal value As Date)
            If Not (value = _dtFecAct) Then
                _dtFecAct = value
                NotifyPropertyChanged("FecAct")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FecAlta As Date
        Get
            Return _dtFecAlta
        End Get
        Set(ByVal value As Date)
            If Not (value = _dtFecAlta) Then
                _dtFecAlta = value
                NotifyPropertyChanged("FecAlta")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property AdjunProce As Integer
        Get
            Return _iAdjunProce
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAdjunProce) Then
                _iAdjunProce = value
                NotifyPropertyChanged("AdjunProce")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Cont As Integer
        Get
            Return _iCont
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iCont) Then
                _iCont = value
                NotifyPropertyChanged("Cont")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property DGuid As Guid
        Get
            Return _guDGuid
        End Get
        Set(ByVal value As Guid)
            If Not (value = _guDGuid) Then
                _guDGuid = value
                NotifyPropertyChanged("DGuid")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property TamañoArchivo As Integer
        Get
            Return _iTamañoArchivo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iTamañoArchivo) Then
                _iTamañoArchivo = value
                NotifyPropertyChanged("TamañoArchivo")
            End If
        End Set
    End Property

#End Region

End Class
