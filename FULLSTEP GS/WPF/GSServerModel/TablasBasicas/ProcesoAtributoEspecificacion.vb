﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProcesoAtributoEspecificacion
    Inherits Security
    Implements INotifyPropertyChanged    

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iAnyo As Integer
    Private _sGMN1 As String
    Private _iProce As Integer   
    Private _iAtrib As Integer
    Private _bInterno As Boolean
    Private _bPedido As Boolean
    Private _iOrden As Integer
    Private _dblValorNum As Double
    Private _sValorText As String
    Private _dtValorFecha As Date
    Private _bValorBool As Boolean
    Private _dtFechaAct As Date
    Private _bOblig As Boolean
    Private _iValidacion As Integer
    Private _sCodigo As String
    Private _sDenominacion As String

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo As Integer
        Get
            Return (_iAnyo)
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("ANYO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1 As String
        Get
            Return (_sGMN1)
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proce As Integer
        Get
            Return (_iProce)
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iProce) Then
                _iProce = value
                NotifyPropertyChanged("PROCE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Atrib As Integer
        Get
            Return (_iAtrib)
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAtrib) Then
                _iAtrib = value
                NotifyPropertyChanged("ATRIB")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Interno As Boolean
        Get
            Return (_bInterno)
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bInterno) Then
                _bInterno = value
                NotifyPropertyChanged("INTERNO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Pedido As Boolean
        Get
            Return (_bPedido)
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPedido) Then
                _bPedido = value
                NotifyPropertyChanged("PEDIDO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Orden As Integer
        Get
            Return (_iOrden)
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iOrden) Then
                _iOrden = value
                NotifyPropertyChanged("ORDEN")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ValorNum As Double
        Get
            Return (_dblValorNum)
        End Get
        Set(ByVal value As Double)
            If Not (value = _dblValorNum) Then
                _dblValorNum = value
                NotifyPropertyChanged("VALORNUM")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ValorText As String
        Get
            Return (_sValorText)
        End Get
        Set(ByVal value As String)
            If Not (value = _sValorText) Then
                _sValorText = value
                NotifyPropertyChanged("VALORTEXT")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ValorFecha As Date
        Get
            Return (_dtValorFecha)
        End Get
        Set(ByVal value As Date)
            If Not Date.Equals(value, _dtValorFecha) Then
                _dtValorFecha = value
                NotifyPropertyChanged("VALORFECHA")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ValorBool As Boolean
        Get
            Return (_bValorBool)
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bValorBool) Then
                _bValorBool = value
                NotifyPropertyChanged("VALORBOOL")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaAct As Date
        Get
            Return (_dtFechaAct)
        End Get
        Set(ByVal value As Date)
            If Not Date.Equals(value, _dtFechaAct) Then
                _dtFechaAct = value
                NotifyPropertyChanged("FECHAACT")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Oblig As Boolean
        Get
            Return (_bOblig)
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bOblig) Then
                _bOblig = value
                NotifyPropertyChanged("OBLIG")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Validacion As Integer
        Get
            Return (_iValidacion)
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iValidacion) Then
                _iValidacion = value
                NotifyPropertyChanged("VALIDACION")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Codigo As String
        Get
            Return (_sCodigo)
        End Get
        Set(ByVal value As String)
            If Not (value = _sCodigo) Then
                _sCodigo = value
                NotifyPropertyChanged("COD")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Denominacion As String
        Get
            Return (_sDenominacion)
        End Get
        Set(ByVal value As String)
            If Not (value = _sDenominacion) Then
                _sDenominacion = value
                NotifyPropertyChanged("DEN")
            End If
        End Set
    End Property

#End Region

End Class
