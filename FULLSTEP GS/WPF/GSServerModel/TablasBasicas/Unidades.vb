﻿Imports System
Imports System.Data.SqlClient
Imports System.Exception
Imports System.Collections.ObjectModel
'Imports System.ServiceModel
Imports Fullstep.FSNLibrary

<Serializable()> _
Public Class Unidades
    Inherits Lista(Of Unidad)


    Public Overloads ReadOnly Property Item(ByVal Cod As String) As Unidad
        Get
            Return Me.Find(Function(elemento As Unidad) elemento.Codigo = Cod)
        End Get
    End Property

    ' ''' <summary>Añade un elemento CMoneda a la lista</summary>
    ' ''' <param name="Cod">Código</param>
    ' ''' <param name="Den">Denominación</param>
    ' ''' <param name="EQUIV">Equivalencia</param>
    ' ''' <param name="FECACT">Fecha última actualización</param>
    ' ''' <returns>Devuelve el elemento CMoneda agregado</returns>
    Public Overloads Function Add(ByVal Cod As String, ByVal Den As Multiidiomas, ByVal FECACT As Object) As Unidad

        ' * Objetivo: Anyadir una moneda a la coleccion
        ' * Recibe: Datos de la moneda
        ' * Devuelve: Moneda añadida

        Dim objnewmember As Unidad

        ' Creacion de objeto moneda

        'objnewmember = New Moneda(mDBServer, mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)
        objnewmember = New Unidad(mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)

        ' Paso de los parametros al nuevo objeto moneda

        objnewmember.Codigo = Cod
        objnewmember.Denominaciones = Den
        objnewmember.FecAct = FECACT
        ' objnewmember.Denominaciones = New Multiidiomas(mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)

        MyBase.Add(objnewmember)
        Return objnewmember

    End Function

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

End Class
