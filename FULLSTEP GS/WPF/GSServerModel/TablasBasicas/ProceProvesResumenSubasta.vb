﻿Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProceProvesResumenSubasta
    Inherits Lista(Of ProceProveResumenSubasta)

    Private _sProve As String

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Overloads ReadOnly Property Item(ByVal Proveedor As String) As ProceProveResumenSubasta
        Get
            Return Me.Find(Function(elemento As ProceProveResumenSubasta) elemento.Proveedor = Proveedor)
        End Get
    End Property

End Class
