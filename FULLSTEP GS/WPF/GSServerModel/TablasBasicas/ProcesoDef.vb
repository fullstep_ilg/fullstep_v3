﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProcesoDef
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iAnyo As Integer
    Private _sGMN1 As String
    Private _iProce As Integer
    Private _iDest As Integer
    Private _iPag As Integer
    Private _iFecSum As Integer
    Private _iProve As Integer
    Private _iDist As Integer
    Private _iPresAnu1 As Integer
    Private _iPresAnu2 As Integer
    Private _iPres1 As Integer
    Private _iPres2 As Integer
    Private _bProceEsp As Boolean
    Private _bGrupoEsp As Boolean
    Private _bItemEsp As Boolean
    Private _bOfeAdjun As Boolean
    Private _bGrupoAdjun As Boolean
    Private _bItemAdjun As Boolean
    Private _bSolCantMax As Boolean
    Private _bPonderar As Boolean
    Private _bPrecAlter As Boolean
    Private _bSubasta As Boolean
    Private _dtFecAct As Nullable(Of Date)
    Private _bCambiarMon As Boolean
    Private _iSolicit As Integer
    Private _bAdminPub As Boolean
    Private _bMaxOfe As Boolean
    Private _bPublicarFinSum As Boolean
    Private _bAvisarDesPub As Nullable(Of Boolean)
    Private _bUnSoloPedido As Boolean
    Private _bOblAtribOfe As Boolean
    Private _bValAtrEsp As Boolean
    Private _iSubastaTipo As Integer
    Private _bSubastaComunica As Boolean

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo As Integer
        Get
            Return _iAnyo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("Anyo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1 As String
        Get
            Return _sGMN1
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proce As Integer
        Get
            Return _iProce
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iProce) Then
                _iProce = value
                NotifyPropertyChanged("Proce")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Dest As Integer
        Get
            Return _iDest
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iDest) Then
                _iDest = value
                NotifyPropertyChanged("Dest")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Pag As Integer
        Get
            Return _iPag
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iPag) Then
                _iPag = value
                NotifyPropertyChanged("Pag")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FecSum As Integer
        Get
            Return _iFecSum
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iFecSum) Then
                _iFecSum = value
                NotifyPropertyChanged("FecSum")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Prove As Integer
        Get
            Return _iProve
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iProve) Then
                _iProve = value
                NotifyPropertyChanged("Prove")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Dist As Integer
        Get
            Return _iDist
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iDist) Then
                _iDist = value
                NotifyPropertyChanged("Dist")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PresAnu1 As Integer
        Get
            Return _iPresanu1
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iPresAnu1) Then
                _iPresAnu1 = value
                NotifyPropertyChanged("PresAnu1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PresAnu2 As Integer
        Get
            Return _iPresanu2
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iPresAnu2) Then
                _iPresAnu2 = value
                NotifyPropertyChanged("PresAnu2")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Pres1 As Integer
        Get
            Return _iPres1
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iPres1) Then
                _iPres1 = value
                NotifyPropertyChanged("Pres1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Pres2 As Integer
        Get
            Return _iPres2
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iPres2) Then
                _iPres2 = value
                NotifyPropertyChanged("Pres2")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ProceEsp As Boolean
        Get
            Return _bProceEsp
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bProceEsp) Then
                _bProceEsp = value
                NotifyPropertyChanged("ProceEsp")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GrupoEsp As Boolean
        Get
            Return _bGrupoEsp
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bGrupoEsp) Then
                _bGrupoEsp = value
                NotifyPropertyChanged("GrupoEsp")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ItemEsp As Boolean
        Get
            Return _bItemEsp
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bItemEsp) Then
                _bItemEsp = value
                NotifyPropertyChanged("ItemEsp")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property OfeAdjun As Boolean
        Get
            Return _bOfeAdjun
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bOfeAdjun) Then
                _bOfeAdjun = value
                NotifyPropertyChanged("OfeAdjun")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GrupoAdjun As Boolean
        Get
            Return _bGrupoAdjun
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bGrupoAdjun) Then
                _bGrupoAdjun = value
                NotifyPropertyChanged("GrupoAdjun")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ItemAdjun As Boolean
        Get
            Return _bItemAdjun
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bItemAdjun) Then
                _bItemAdjun = value
                NotifyPropertyChanged("ItemAdjun")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property SolCantMax As Boolean
        Get
            Return _bSolCantMax
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bSolCantMax) Then
                _bSolCantMax = value
                NotifyPropertyChanged("SolCantMax")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Ponderar As Boolean
        Get
            Return _bPonderar
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPonderar) Then
                _bPonderar = value
                NotifyPropertyChanged("Ponderar")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PrecAlter As Boolean
        Get
            Return _bPrecAlter
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPrecAlter) Then
                _bPrecAlter = value
                NotifyPropertyChanged("PrecAlter")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Subasta As Boolean
        Get
            Return _bSubasta
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bSubasta) Then
                _bSubasta = value
                NotifyPropertyChanged("Subasta")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FecAct As Nullable(Of Date)
        Get
            Return _dtFecAct
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFecAct) Then
                _dtFecAct = value
                NotifyPropertyChanged("FecAct")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property CambiarMon As Boolean
        Get
            Return _bCambiarMon
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bCambiarMon) Then
                _bCambiarMon = value
                NotifyPropertyChanged("CambiarMon")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Solicit As Integer
        Get
            Return _iSolicit
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iSolicit) Then
                _iSolicit = value
                NotifyPropertyChanged("Solicit")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property AdminPub As Boolean
        Get
            Return _bAdminPub
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bAdminPub) Then
                _bAdminPub = value
                NotifyPropertyChanged("AdminPub")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MaxOfe As Boolean
        Get
            Return _bMaxOfe
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bMaxOfe) Then
                _bMaxOfe = value
                NotifyPropertyChanged("MaxOfe")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PublicarFinSum As Boolean
        Get
            Return _bPublicarFinSum
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPublicarFinSum) Then
                _bPublicarFinSum = value
                NotifyPropertyChanged("PublicarFinSum")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property AvisarDesPub As Nullable(Of Boolean)
        Get
            Return _bAvisarDesPub
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            If Not (value = _bAvisarDesPub) Then
                _bAvisarDesPub = value
                NotifyPropertyChanged("AvisarDesPub")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UnSoloPedido As Boolean
        Get
            Return _bUnSoloPedido
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bUnSoloPedido) Then
                _bUnSoloPedido = value
                NotifyPropertyChanged("UnSoloPedido")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property OblAtribOfe As Boolean
        Get
            Return _bOblAtribOfe
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bOblAtribOfe) Then
                _bOblAtribOfe = value
                NotifyPropertyChanged("OblAtribOfe")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ValAtrEsp As Boolean
        Get
            Return _bValAtrEsp
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bValAtrEsp) Then
                _bValAtrEsp = value
                NotifyPropertyChanged("ValAtrEesp")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property SubastaTipo As Integer
        Get
            Return _iSubastaTipo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iSubastaTipo) Then
                _iSubastaTipo = value
                NotifyPropertyChanged("SubastaTipo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property SubastaComunica As Boolean
        Get
            Return _bSubastaComunica
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bSubastaComunica) Then
                _bSubastaComunica = value
                NotifyPropertyChanged("SubastaComunica")
            End If
        End Set
    End Property

#End Region

End Class
