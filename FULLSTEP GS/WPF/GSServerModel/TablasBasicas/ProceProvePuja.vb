﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProceProvePuja
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _dtFechaPuja As Date
    Private _dblValor As Double

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property FechaPuja As Date
        Get
            Return _dtFechaPuja
        End Get
        Set(ByVal value As Date)
            If Not _dtFechaPuja.Equals(value) Then
                _dtFechaPuja = value
                NotifyPropertyChanged("FechaPuja")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Valor As Double
        Get
            Return _dblValor
        End Get
        Set(ByVal value As Double)
            If Not (value = _dblValor) Then
                _dblValor = value
                NotifyPropertyChanged("Valor")
            End If
        End Set
    End Property

#End Region

End Class
