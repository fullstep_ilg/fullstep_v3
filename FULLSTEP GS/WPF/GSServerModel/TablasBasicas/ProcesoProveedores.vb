﻿Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProcesoProveedores
    Inherits Lista(Of ProcesoProveedor)

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Overloads ReadOnly Property Item(ByVal Proveedor As String) As ProcesoProveedor
        Get
            Return Me.Find(Function(elemento As ProcesoProveedor) elemento.Proveedor = Proveedor)
        End Get
    End Property

End Class
