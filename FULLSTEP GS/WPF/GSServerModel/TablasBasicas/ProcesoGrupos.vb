﻿Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProcesoGrupos
    Inherits Lista(Of ProcesoGrupo)

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Overloads Function Find(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Integer, ByVal ID As Integer) As GSServerModel.ProcesoGrupo
        For Each oGrupo As GSServerModel.ProcesoGrupo In Me
            If oGrupo.Anyo = Anyo And oGrupo.GMN1 = GMN1 And oGrupo.Proce = Proce And oGrupo.ID = ID Then
                Return oGrupo
            End If
        Next
    End Function

End Class
