﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProcesoSubastaEvento
    Inherits Security
    Implements INotifyPropertyChanged    

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iAnyo As Integer
    Private _iProce As Integer
    Private _sGMN1 As String
    Private _iAccion As Integer
    Private _dtFecha As Date
    Private _iExten As Nullable(Of Integer)
    Private _dtFecFinHist As Nullable(Of Date)
    Private _iModReinicioSub As Nullable(Of Fullstep.FSNLibrary.TiposDeDatos.ModoReinicioSubasta)
    Private _iMinReinicioSub As Nullable(Of Integer)
    Private _dtFecReinicioSub As Nullable(Of Date)
    Private _iModIncTSub As Nullable(Of Fullstep.FSNLibrary.TiposDeDatos.ModoIncTiempoSubasta)
    Private _iMinIncTSub As Nullable(Of Integer)

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo() As Integer
        Get
            Anyo = _iAnyo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("Anyo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proceso() As Integer
        Get
            Return _iProce
        End Get
        Set(ByVal Value As Integer)
            If Not (Value = _iProce) Then
                _iProce = Value
                NotifyPropertyChanged("Proceso")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1() As String
        Get
            GMN1 = _sGMN1
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Accion() As Integer
        Get
            Return _iAccion
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAccion) Then
                _iAccion = value
                NotifyPropertyChanged("Accion")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Fecha() As Date
        Get
            Return _dtFecha
        End Get
        Set(ByVal value As Date)
            If Not (value.Equals(_dtFecha)) Then
                _dtFecha = value
                NotifyPropertyChanged("fecha")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Extension() As Nullable(Of Integer)
        Get
            Return _iExten
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not (value.Equals(_iExten)) Then
                _iExten = value
                NotifyPropertyChanged("Exten")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaFinHist() As Nullable(Of Date)
        Get
            Return _dtFecFinHist
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not (value.Equals(_dtFecFinHist)) Then
                _dtFecFinHist = value
                NotifyPropertyChanged("FecFinHist")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ModoReinicioSub() As Nullable(Of Fullstep.FSNLibrary.TiposDeDatos.ModoReinicioSubasta)
        Get
            Return _iModReinicioSub
        End Get
        Set(ByVal value As Nullable(Of Fullstep.FSNLibrary.TiposDeDatos.ModoReinicioSubasta))
            If Not (value.Equals(_iModReinicioSub)) Then
                _iModReinicioSub = value
                NotifyPropertyChanged("ModReinicioSub")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinReinicioSub() As Nullable(Of Integer)
        Get
            Return _iMinReinicioSub
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not (value.Equals(_iMinReinicioSub)) Then
                _iMinReinicioSub = value
                NotifyPropertyChanged("MinReinicioSub")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaReinicioSub() As Nullable(Of Date)
        Get
            Return _dtFecReinicioSub
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not (value.Equals(_dtFecReinicioSub)) Then
                _dtFecReinicioSub = value
                NotifyPropertyChanged("FecReinicioSub")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ModoIncTSub() As Nullable(Of Fullstep.FSNLibrary.TiposDeDatos.ModoIncTiempoSubasta)
        Get
            Return _iModIncTsub
        End Get
        Set(ByVal value As Nullable(Of Fullstep.FSNLibrary.TiposDeDatos.ModoIncTiempoSubasta))
            If Not (value.Equals(_iModIncTsub)) Then
                _iModIncTsub = value
                NotifyPropertyChanged("ModIncTsub")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property MinIncTSub() As Nullable(Of Integer)
        Get
            Return _iMinIncTSub
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not (value.Equals(_iMinIncTSub)) Then
                _iMinIncTSub = value
                NotifyPropertyChanged("MinIncTSub")
            End If
        End Set
    End Property

#End Region

End Class
