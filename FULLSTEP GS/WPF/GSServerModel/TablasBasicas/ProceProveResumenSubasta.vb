﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProceProveResumenSubasta
    Inherits Security
    Implements INotifyPropertyChanged    

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iAnyo As Integer
    Private _sGMN1 As String
    Private _iProce As Integer
    Private _bConectado As Boolean
    Private _sProveedor As String
    Private _sDenominacion As String
    Private _iNumPujas As Integer
    Private _dtFechaUltimaPuja As Nullable(Of Date)
    Private _dblPrecio As Nullable(Of Double)
    Private _dblCantidad As Nullable(Of Double)
    Private _dblValorUltimaPuja As Nullable(Of Double)
    Private _dblAhorro As Nullable(Of Double)
    Private _dblPorcentAhorro As Nullable(Of Double)
    Private _iPosicionPuja As Nullable(Of Integer)
    Private _dtFechaInicio As Nullable(Of Date)
    Private _dtFechaFin As Nullable(Of Date)

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()        
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)       
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo() As Integer
        Get
            Anyo = _iAnyo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("ANYO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1() As String
        Get
            GMN1 = _sGMN1
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proceso() As Integer
        Get
            Proceso = _iProce
        End Get
        Set(ByVal Value As Integer)
            If Not (Value = _iProce) Then
                _iProce = Value
                NotifyPropertyChanged("PROCE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proveedor() As String
        Get
            Return _sProveedor
        End Get
        Set(ByVal value As String)
            If Not (value = _sProveedor) Then
                _sProveedor = value
                NotifyPropertyChanged("PROVE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Denominacion As String
        Get
            Return _sDenominacion
        End Get
        Set(ByVal value As String)
            If Not (value = _sDenominacion) Then
                _sDenominacion = value
                NotifyPropertyChanged("PROVE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property NumPujas() As Integer
        Get
            Return _iNumPujas
        End Get
        Set(ByVal Value As Integer)
            If Not (Value = _iNumPujas) Then
                _iNumPujas = Value
                NotifyPropertyChanged("NumPujas")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaUltimaPuja() As Nullable(Of Date)
        Get
            Return _dtFechaUltimaPuja
        End Get
        Set(ByVal Value As Nullable(Of Date))
            If Not Value.Equals(_dtFechaUltimaPuja) Then
                _dtFechaUltimaPuja = Value
                NotifyPropertyChanged("FechaUltimaPuja")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Precio() As Nullable(Of Double)
        Get
            Return _dblPrecio
        End Get
        Set(ByVal Value As Nullable(Of Double))
            If Not Value.Equals(_dblPrecio) Then
                _dblPrecio = Value
                NotifyPropertyChanged("Precio")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Cantidad() As Nullable(Of Double)
        Get
            Return _dblCantidad
        End Get
        Set(ByVal Value As Nullable(Of Double))
            If Not Value.Equals(_dblCantidad) Then
                _dblCantidad = Value
                NotifyPropertyChanged("Cantidad")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ValorUltimaPuja() As Nullable(Of Double)
        Get
            Return _dblValorUltimaPuja
        End Get
        Set(ByVal Value As Nullable(Of Double))
            If Not Value.Equals(_dblValorUltimaPuja) Then
                _dblValorUltimaPuja = Value
                NotifyPropertyChanged("ValorUltimaPuja")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Ahorro() As Nullable(Of Double)
        Get
            Return _dblAhorro
        End Get
        Set(ByVal Value As Nullable(Of Double))
            If Not Value.Equals(_dblAhorro) Then
                _dblAhorro = Value
                NotifyPropertyChanged("Ahorro")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PorcentAhorro() As Nullable(Of Double)
        Get
            Return _dblPorcentAhorro
        End Get
        Set(ByVal Value As Nullable(Of Double))
            If Not Value.Equals(_dblPorcentAhorro) Then
                _dblPorcentAhorro = Value
                NotifyPropertyChanged("PorcentAhorro")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PosicionPuja() As Nullable(Of Integer)
        Get
            Return _iPosicionPuja
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If Not value.Equals(_iPosicionPuja) Then
                _iPosicionPuja = value
                NotifyPropertyChanged("POSICIONPUJA")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Conectado() As Boolean
        Get
            Return _bConectado
        End Get
        Set(ByVal value As Boolean)
            If Not value = _bConectado Then
                _bConectado = value
                NotifyPropertyChanged("CONECTADO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaInicio() As Nullable(Of Date)
        Get
            Return _dtFechaInicio
        End Get
        Set(ByVal Value As Nullable(Of Date))
            If Not Value.Equals(_dtFechaInicio) Then
                _dtFechaInicio = Value
                NotifyPropertyChanged("FechaInicio")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaFin() As Nullable(Of Date)
        Get
            Return _dtFechaFin
        End Get
        Set(ByVal Value As Nullable(Of Date))
            If Not Value.Equals(_dtFechaFin) Then
                _dtFechaFin = Value
                NotifyPropertyChanged("FechaFin")
            End If
        End Set
    End Property

#End Region

End Class
