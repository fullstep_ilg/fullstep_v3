﻿Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProceProvePujas
    Inherits Lista(Of ProceProvePuja)

    Private _sProve As String
    Private _sDenominacion As String

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#Region " Propiedades "

    <DataMember()> _
    Public Property Prove As String
        Get
            Return _sProve
        End Get
        Set(ByVal value As String)
            If Not (value = _sProve) Then
                _sProve = value                
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Denominacion As String
        Get
            Return _sDenominacion
        End Get
        Set(ByVal value As String)
            If Not (value = _sDenominacion) Then
                _sDenominacion = value
            End If
        End Set
    End Property

#End Region

End Class
