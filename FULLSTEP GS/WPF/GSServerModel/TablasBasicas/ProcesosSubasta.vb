﻿Imports System.Runtime.Serialization

<DataContract()> _
 Public Class ProcesosSubasta
    Inherits Lista(Of ProcesoSubasta)

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

End Class
