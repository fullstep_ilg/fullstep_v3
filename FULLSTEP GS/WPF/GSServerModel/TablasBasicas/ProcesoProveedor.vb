﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProcesoProveedor
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iAnyo As Integer
    Private _sGMN1 As String
    Private _iProce As Integer
    Private _sProveedor As String
    Private _sEQP As String
    Private _sCOM As String
    Private _iPUB As Integer
    Private _dtFechaPUB As Nullable(Of Date)
    Private _bOFE As Boolean
    Private _bSuperada As Boolean
    Private _iNumObj As Integer
    Private _bObjNuevos As Boolean
    Private _bCerrado As Boolean
    Private _bNoOfe As Boolean
    Private _dtFechaNoOfe As Nullable(Of Date)
    Private _sMotivoNoOfe As String
    Private _bAvisadoDespub As Boolean
    Private _bPortalNoOfe As Boolean
    Private _sUsuNoOfe As String
    Private _NombreUsuNoOfe As String
    Private _bSubasta As Boolean
    'Propiedades de Porveedor
    Private _Denominacion As String
    Private _FSPCod As String

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo() As Integer
        Get
            Anyo = _iAnyo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("ANYO")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1() As String
        Get
            GMN1 = _sGMN1
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proceso() As Integer
        Get
            Proceso = _iProce
        End Get
        Set(ByVal Value As Integer)
            If Not (Value = _iProce) Then
                _iProce = Value
                NotifyPropertyChanged("PROCE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Proveedor() As String
        Get
            Return _sProveedor
        End Get
        Set(ByVal value As String)
            If Not (value = _sProveedor) Then
                _sProveedor = value
                NotifyPropertyChanged("PROVE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Denominacion As String
        Get
            Return _Denominacion
        End Get
        Set(ByVal value As String)
            If Not (value = _sProveedor) Then
                _Denominacion = value
                NotifyPropertyChanged("PROVE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoPortal As String
        Get
            Return _FSPCod
        End Get
        Set(ByVal value As String)
            If Not (value = _FSPCod) Then
                _FSPCod = value
                NotifyPropertyChanged("FSPCod")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Subasta As Boolean
        Get
            Return _bSubasta
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bSubasta) Then
                _bSubasta = value
                NotifyPropertyChanged("SUBASTA")
            End If
        End Set
    End Property

#End Region

End Class

