﻿Imports System.Runtime.Serialization

<DataContract()> _
Public Class ProcesoSubastaReglas
    Inherits Lista(Of ProcesoSubastaRegla)

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Overloads ReadOnly Property Item(ByVal Nombre As String) As ProcesoSubastaRegla
        Get
            Return Me.Find(Function(elemento As ProcesoSubastaRegla) elemento.Nombre = Nombre)
        End Get
    End Property

End Class
