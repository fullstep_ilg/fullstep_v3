﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class Proceso
    Inherits Security
    Implements INotifyPropertyChanged    

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iAnyo As Integer
    Private _iCodigo As Integer
    Private _sGMN1 As String
    Private _sDenominacion As String
    Private _bAdjDir As Boolean
    Private _dblPres As Nullable(Of Double)
    Private _dtFechaNecesidad As Nullable(Of Date)
    Private _dtFechaApe As Date
    Private _dtFechaValSelProve As Nullable(Of Date)
    Private _dtFechaValidacion As Nullable(Of Date)
    Private _dtFechaPresentacion As Nullable(Of Date)
    Private _dtFechaEnvPet As Nullable(Of Date)
    Private _dtFechaLimOfe As Nullable(Of Date)
    Private _dtFechaProximaReunion As Nullable(Of Date)
    Private _dtFechaUltimaReunion As Nullable(Of Date)
    Private _dtFechaCierre As Nullable(Of Date)
    Private _dtFechaReapertura As Nullable(Of Date)
    Private _bReudec As Boolean
    Private _sUsuVal As String
    Private _sUsuValSelProve As String
    Private _sUsuValCierre As String
    Private _sMoneda As String
    Private _dblCambio As Double
    Private _sEsp As String
    Private _Estado As Fullstep.FSNLibrary.TiposDeDatos.TipoEstadoProceso
    Private _iEstUlt As Nullable(Of Integer)
    Private _sEqp As String
    Private _sCom As String
    Private _sSolicitud As String
    Private _bFechaNecManual As Boolean
    Private _bPedAprov As Boolean
    Private _sDest As String
    Private _sPag As String
    Private _dtFechaIni As Nullable(Of Date)
    Private _dtFechaFin As Nullable(Of Date)
    Private _sProveAct As String
    Private _dtFechaAct As Nullable(Of Date)
    Private _sUsuarioApertura As String
    Private _dtFechaIniSub As Nullable(Of Date)
    Private _iSolicitud As Nullable(Of Integer)
    Private _bCalcPend As Boolean
    Private _dtFechaCalc As Nullable(Of Date)
    Private _dblAdjudicado As Nullable(Of Double)
    Private _iPlantilla As Nullable(Of Integer)
    Private _bPlantillaVistas As Boolean
    Private _bAdjERP As Boolean
    Private _sComentAdj As String
    Private _iP As Nullable(Of Integer)
    Private _iW As Nullable(Of Integer)
    Private _iO As Nullable(Of Integer)
    Private _dblPresTotal As Nullable(Of Double)
    Private _dblConsumido As Nullable(Of Double)
    Private _bDesdeSolicitud As Boolean
    Private _sPersonaApertura As String
    Private _sUON1Apertura As String
    Private _sUON2Apertura As String
    Private _sUON3Apertura As String
    Private _sDepApertura As String
    Private _bPubMatProve As Boolean
    Private _sGstoERP As String
    Private _dtFechaAdj As Nullable(Of Date)
    Private _oGrupos As ProcesoGrupos

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    '<summary>Constructor por defecto</summary>        

    Public Sub New()
        MyBase.New()
    End Sub

    '<summary>Constructor de la clase Proceso</summary>    
    '<param name="remotting">Si es conexion remota</param>
    '<param name="UserCode">Codigo de usuario</param>
    '<param name="UserPassword">Password de usuario</param>
    '<param name="isAuthenticated">Si está autenticado</param>

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Anyo() As Integer
        Get
            Return _iAnyo
        End Get
        Set(ByVal value As Integer)
            If Not (value = _iAnyo) Then
                _iAnyo = value
                NotifyPropertyChanged("Anyo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Codigo() As Integer
        Get
            Return _iCodigo
        End Get
        Set(ByVal Value As Integer)
            If Not (Value = _iCodigo) Then
                _iCodigo = Value
                NotifyPropertyChanged("Codigo")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GMN1() As String
        Get
            Return _sGMN1
        End Get
        Set(ByVal value As String)
            If Not (value = _sGMN1) Then
                _sGMN1 = value
                NotifyPropertyChanged("GMN1")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Denominacion() As String
        Get
            Return _sDenominacion
        End Get
        Set(ByVal value As String)
            If Not (value = _sDenominacion) Then
                _sDenominacion = value
                NotifyPropertyChanged("Denominacion")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property AdjDir() As Boolean
        Get
            Return _bAdjDir
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bAdjDir) Then
                _bAdjDir = value
                NotifyPropertyChanged("AdjDir")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Presupuesto() As Nullable(Of Double)
        Get
            Return _dblPres
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblPres) Then
                _dblPres = value
                NotifyPropertyChanged("Pres")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaNecesidad As Nullable(Of Date)
        Get
            Return _dtFechaNecesidad
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaNecesidad) Then
                _dtFechaNecesidad = value
                NotifyPropertyChanged("FechaNecesidad")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaApe As Date
        Get
            Return _dtFechaApe
        End Get
        Set(ByVal value As Date)
            If Not Date.Equals(value, _dtFechaApe) Then
                _dtFechaApe = value
                NotifyPropertyChanged("FechaApe")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaValSelProve As Nullable(Of Date)
        Get
            Return _dtFechaValSelProve
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaValSelProve) Then
                _dtFechaValSelProve = value
                NotifyPropertyChanged("FechaValSelProve")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaValidacion As Nullable(Of Date)
        Get
            Return _dtFechaValidacion
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaValidacion) Then
                _dtFechaValidacion = value
                NotifyPropertyChanged("FechaValidacion")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaPresentacion As Nullable(Of Date)
        Get
            Return _dtFechaPresentacion
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaPresentacion) Then
                _dtFechaPresentacion = value
                NotifyPropertyChanged("FechaPresentacion")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaEnvPet As Nullable(Of Date)
        Get
            Return _dtFechaEnvPet
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaEnvPet) Then
                _dtFechaEnvPet = value
                NotifyPropertyChanged("FechaEnvPet")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaLimiteOfertas As Nullable(Of Date)
        Get
            Return _dtFechaLimOfe
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaLimOfe) Then
                _dtFechaLimOfe = value
                NotifyPropertyChanged("FELIMOFE")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaProximaReunion As Nullable(Of Date)
        Get
            Return _dtFechaProximaReunion
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaProximaReunion) Then
                _dtFechaProximaReunion = value
                NotifyPropertyChanged("FechaProximaReunion")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaUltimaReunion As Nullable(Of Date)
        Get
            Return _dtFechaUltimaReunion
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaUltimaReunion) Then
                _dtFechaUltimaReunion = value
                NotifyPropertyChanged("FechaUltimaReunion")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCierre As Nullable(Of Date)
        Get
            Return _dtFechaCierre
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaCierre) Then
                _dtFechaCierre = value
                NotifyPropertyChanged("FechaCierre")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaReapertura As Nullable(Of Date)
        Get
            Return _dtFechaReapertura
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaReapertura) Then
                _dtFechaReapertura = value
                NotifyPropertyChanged("FechaReapertura")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Reudec() As Boolean
        Get
            Return _bReudec
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bReudec) Then
                _bReudec = value
                NotifyPropertyChanged("Reudec")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UsuVal() As String
        Get
            Return _sUsuVal
        End Get
        Set(ByVal value As String)
            If Not (value = _sUsuVal) Then
                _sUsuVal = value
                NotifyPropertyChanged("UsuVal")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UsuValSelProve() As String
        Get
            Return _sUsuValSelProve
        End Get
        Set(ByVal value As String)
            If Not (value = _sUsuValSelProve) Then
                _sUsuValSelProve = value
                NotifyPropertyChanged("UsuValSelProve")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UsuValCierre() As String
        Get
            Return _sUsuValCierre
        End Get
        Set(ByVal value As String)
            If Not (value = _sUsuValCierre) Then
                _sUsuValCierre = value
                NotifyPropertyChanged("UsuValCierre")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Moneda() As String
        Get
            Return _sMoneda
        End Get
        Set(ByVal value As String)
            If Not (value = _sMoneda) Then
                _sMoneda = value
                NotifyPropertyChanged("MON")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Cambio() As Double
        Get
            Return _dblCambio
        End Get
        Set(ByVal value As Double)
            If Not (value = _dblCambio) Then
                _dblCambio = value
                NotifyPropertyChanged("Cambio")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Esp() As String
        Get
            Return _sEsp
        End Get
        Set(ByVal value As String)
            If Not (value = _sEsp) Then
                _sEsp = value
                NotifyPropertyChanged("Esp")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Estado() As Fullstep.FSNLibrary.TiposDeDatos.TipoEstadoProceso
        Get
            Return _Estado
        End Get
        Set(ByVal value As Fullstep.FSNLibrary.TiposDeDatos.TipoEstadoProceso)
            If Not (value = _Estado) Then
                _Estado = value
                NotifyPropertyChanged("EST")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property EstUlt() As Nullable(Of Integer)
        Get
            Return _iEstUlt
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            If Not Value.Equals(_iEstUlt) Then
                _iEstUlt = Value
                NotifyPropertyChanged("EstUlt")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Equipo() As String
        Get
            Return _sEqp
        End Get
        Set(ByVal value As String)
            If Not (value = _sEqp) Then
                _sEqp = value
                NotifyPropertyChanged("EQP")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Comprador() As String
        Get
            Return _sCom
        End Get
        Set(ByVal value As String)
            If Not (value = _sCom) Then
                _sCom = value
                NotifyPropertyChanged("COM")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Solicitud() As String
        Get
            Return _sSolicitud
        End Get
        Set(ByVal value As String)
            If Not (value = _sSolicitud) Then
                _sSolicitud = value
                NotifyPropertyChanged("Solicitud")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaNecManual() As Boolean
        Get
            Return _bFechaNecManual
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bFechaNecManual) Then
                _bFechaNecManual = value
                NotifyPropertyChanged("FechaNecManual")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PedAprov() As Boolean
        Get
            Return _bPedAprov
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPedAprov) Then
                _bPedAprov = value
                NotifyPropertyChanged("PedAprov")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Dest() As String
        Get
            Return _sDest
        End Get
        Set(ByVal value As String)
            If Not (value = _sDest) Then
                _sDest = value
                NotifyPropertyChanged("Dest")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Pag() As String
        Get
            Return _sPag
        End Get
        Set(ByVal value As String)
            If Not (value = _sPag) Then
                _sPag = value
                NotifyPropertyChanged("Pag")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaIni As Nullable(Of Date)
        Get
            Return _dtFechaIni
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaIni) Then
                _dtFechaIni = value
                NotifyPropertyChanged("FechaIni")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaFin As Nullable(Of Date)
        Get
            Return _dtFechaFin
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaFin) Then
                _dtFechaFin = value
                NotifyPropertyChanged("FechaFin")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ProveAct() As String
        Get
            Return _sProveAct
        End Get
        Set(ByVal value As String)
            If Not (value = _sProveAct) Then
                _sProveAct = value
                NotifyPropertyChanged("ProveAct")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaAct As Nullable(Of Date)
        Get
            Return _dtFechaAct
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaAct) Then
                _dtFechaAct = value
                NotifyPropertyChanged("FECACT")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UsuarioApertura() As String
        Get
            Return _sUsuarioApertura
        End Get
        Set(ByVal value As String)
            If Not (value = _sUsuarioApertura) Then
                _sUsuarioApertura = value
                NotifyPropertyChanged("UsuarioApertura")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaInicioSubasta As Nullable(Of Date)
        Get
            Return _dtFechaIniSub
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaIniSub) Then
                _dtFechaIniSub = value
                NotifyPropertyChanged("FECINISUB")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Solicit() As Nullable(Of Integer)
        Get
            Return _iSolicitud
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            If Not Value.Equals(_iSolicitud) Then
                _iSolicitud = Value
                NotifyPropertyChanged("Solicitud")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property CalcPend() As Boolean
        Get
            Return _bCalcPend
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bCalcPend) Then
                _bCalcPend = value
                NotifyPropertyChanged("CalcPend")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCalc As Nullable(Of Date)
        Get
            Return _dtFechaCalc
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaCalc) Then
                _dtFechaCalc = value
                NotifyPropertyChanged("FechaCalc")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Adjudicado() As Nullable(Of Double)
        Get
            Return _dblAdjudicado
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblAdjudicado) Then
                _dblAdjudicado = value
                NotifyPropertyChanged("Adjudicado")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Plantilla() As Nullable(Of Integer)
        Get
            Return _iPlantilla
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            If Not Value.Equals(_iPlantilla) Then
                _iPlantilla = Value
                NotifyPropertyChanged("Plantilla")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PlantillaVistas() As Boolean
        Get
            Return _bPlantillaVistas
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPlantillaVistas) Then
                _bPlantillaVistas = value
                NotifyPropertyChanged("PlantillaVistas")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property AdjERP() As Boolean
        Get
            Return _bAdjERP
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bAdjERP) Then
                _bAdjERP = value
                NotifyPropertyChanged("AdjERP")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property ComentAdj() As String
        Get
            Return _sComentAdj
        End Get
        Set(ByVal value As String)
            If Not (value = _sComentAdj) Then
                _sComentAdj = value
                NotifyPropertyChanged("ComentAdj")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property P() As Nullable(Of Integer)
        Get
            Return _iP
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            If Not Value.Equals(_iP) Then
                _iP = Value
                NotifyPropertyChanged("P")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property W() As Nullable(Of Integer)
        Get
            Return _iW
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            If Not Value.Equals(_iW) Then
                _iW = Value
                NotifyPropertyChanged("W")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property O() As Nullable(Of Integer)
        Get
            Return _iO
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            If Not Value.Equals(_iO) Then
                _iO = Value
                NotifyPropertyChanged("O")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PresTotal() As Nullable(Of Double)
        Get
            Return _dblPresTotal
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblPresTotal) Then
                _dblPresTotal = value
                NotifyPropertyChanged("PresTotal")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Consumido() As Nullable(Of Double)
        Get
            Return _dblConsumido
        End Get
        Set(ByVal value As Nullable(Of Double))
            If Not value.Equals(_dblConsumido) Then
                _dblConsumido = value
                NotifyPropertyChanged("Consumido")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property DesdeSolicitud() As Boolean
        Get
            Return _bDesdeSolicitud
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bDesdeSolicitud) Then
                _bDesdeSolicitud = value
                NotifyPropertyChanged("DesdeSolicitud")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PersonaApertura() As String
        Get
            Return _sPersonaApertura
        End Get
        Set(ByVal value As String)
            If Not (value = _sPersonaApertura) Then
                _sPersonaApertura = value
                NotifyPropertyChanged("PersonaApertura")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UON1Apertura() As String
        Get
            Return _sUON1Apertura
        End Get
        Set(ByVal value As String)
            If Not (value = _sUON1Apertura) Then
                _sUON1Apertura = value
                NotifyPropertyChanged("UON1Apertura")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UON2Apertura() As String
        Get
            Return _sUON2Apertura
        End Get
        Set(ByVal value As String)
            If Not (value = _sUON2Apertura) Then
                _sUON2Apertura = value
                NotifyPropertyChanged("UON2Apertura")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property UON3Apertura() As String
        Get
            Return _sUON3Apertura
        End Get
        Set(ByVal value As String)
            If Not (value = _sUON3Apertura) Then
                _sUON3Apertura = value
                NotifyPropertyChanged("UON3Apertura")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property DepApertura() As String
        Get
            Return _sDepApertura
        End Get
        Set(ByVal value As String)
            If Not (value = _sDepApertura) Then
                _sDepApertura = value
                NotifyPropertyChanged("DepApertura")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property PubMatProve() As Boolean
        Get
            Return _bPubMatProve
        End Get
        Set(ByVal value As Boolean)
            If Not (value = _bPubMatProve) Then
                _bPubMatProve = value
                NotifyPropertyChanged("PubMatProve")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property GstoERP() As String
        Get
            Return _sGstoERP
        End Get
        Set(ByVal value As String)
            If Not (value = _sGstoERP) Then
                _sGstoERP = value
                NotifyPropertyChanged("GstoERP")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property FechaAdj As Nullable(Of Date)
        Get
            Return _dtFechaAdj
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFechaAdj) Then
                _dtFechaAdj = value
                NotifyPropertyChanged("FechaAdj")
            End If
        End Set
    End Property

    <DataMember()> _
    Public Property Grupos() As ProcesoGrupos
        Get
            Return _oGrupos
        End Get
        Set(ByVal value As ProcesoGrupos)
            _oGrupos = value
            NotifyPropertyChanged("Grupos")
        End Set
    End Property

#End Region

End Class
