﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class Impuesto
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _iId As Integer
    Private _sCod As String
    Private _bRetenido As Boolean
    Private _iConcepto As Nullable(Of Integer)
    Private _sComentario As String
    Private _iGrupoCompatibilidad As Integer
    Private _dtFechaAct As Date    
    Private _Denominaciones As Multiidiomas
    Private _Valores As ImpuestoValores

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructores "

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

#End Region

    <DataMember()> _
    Property Id() As Integer
        Get
            Id = _iId
        End Get
        Set(ByVal Value As Integer)
            _iId = Value
            NotifyPropertyChanged("Id")
        End Set
    End Property

    <DataMember()> _
    Property Codigo() As String
        Get
            Codigo = _sCod
        End Get
        Set(ByVal Value As String)
            _sCod = Value
            NotifyPropertyChanged("Codigo")
        End Set
    End Property

    <DataMember()> _
    Property Retenido() As Boolean
        Get
            Retenido = _bRetenido
        End Get
        Set(ByVal Value As Boolean)
            _bRetenido = Value
            NotifyPropertyChanged("Retenido")
        End Set
    End Property

    <DataMember()> _
    Property Concepto() As Nullable(Of Integer)
        Get
            Concepto = _iConcepto
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            _iConcepto = Value
            NotifyPropertyChanged("Concepto")
        End Set
    End Property

    <DataMember()> _
    Property Comentario() As String
        Get
            Comentario = _sComentario
        End Get
        Set(ByVal Value As String)
            _sComentario = Value
            NotifyPropertyChanged("Comentario")
        End Set
    End Property

    <DataMember()> _
    Property GrupoCompatibilidad() As Integer
        Get
            GrupoCompatibilidad = _iGrupoCompatibilidad
        End Get
        Set(ByVal Value As Integer)
            _iGrupoCompatibilidad = Value
            NotifyPropertyChanged("GrupoCompatibilidad")
        End Set
    End Property


    <DataMember()> _
    Property FechaAct() As Date
        Get
            FechaAct = _dtFechaAct
        End Get
        Set(ByVal Value As Date)
            _dtFechaAct = Value
            NotifyPropertyChanged("FechaAct")
        End Set
    End Property

    <DataMember()> _
    Public Property Denominaciones() As Multiidiomas
        Get
            Denominaciones = _Denominaciones
        End Get
        Set(ByVal value As Multiidiomas)
            _Denominaciones = value
            NotifyPropertyChanged("Denominaciones")

        End Set
    End Property

    <DataMember()> _
    Property Valores() As Impuestovalores
        Get
            Valores = _Valores
        End Get
        Set(ByVal Value As ImpuestoValores)
            _Valores = Value
            NotifyPropertyChanged("Valores")
        End Set
    End Property

End Class

