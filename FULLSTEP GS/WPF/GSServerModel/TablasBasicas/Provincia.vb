﻿Imports System.ComponentModel
<Serializable()> _
Public Class Provincia

    Inherits Security
    Implements INotifyPropertyChanged

    Private _Codigo As String
    Private _Denominacion As String
    Private _Denominaciones As Multiidiomas
    Private _FecAct As Object
    Private _Pais As Pais
    Private _mudtOrigen As Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

    Public Property Pais() As Pais
        Get
            Pais = _Pais

        End Get
        Set(ByVal value As Pais)
            _Pais = value
        End Set
    End Property

    Public Property Origen() As Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion
        Get
            Origen = _mudtOrigen
        End Get
        Set(ByVal value As Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion)
            _mudtOrigen = value
        End Set
    End Property


    Public Property Codigo() As String
        Get

            ' * Objetivo: Devolver la variable privada COD
            ' * Recibe: Nada
            ' * Devuelve: Codigo de la moneda

            Codigo = _Codigo

        End Get
        Set(ByVal Value As String)

            ' * Objetivo: Dar valor a la variable privada COD
            ' * Recibe: Codigo de la moneda
            ' * Devuelve: Nada
            If Not (Value = _Codigo) Then
                _Codigo = Value
                NotifyPropertyChanged("Codigo")
            End If
        End Set
    End Property
    Public Property Denominacion() As String
        Get

            ' * Objetivo: Devolver la variable privada DEN
            ' * Recibe: Nada
            ' * Devuelve: Denominacion de la moneda

            Denominacion = _Denominacion

        End Get
        Set(ByVal Value As String)
            If Not (Value = _Denominacion) Then
                _Denominacion = Value
                NotifyPropertyChanged("Denominacion")
            End If

        End Set
    End Property

    Public Property Denominaciones() As Multiidiomas
        Get
            Denominaciones = _Denominaciones
        End Get
        Set(ByVal value As Multiidiomas)
            _Denominaciones = value
            NotifyPropertyChanged("Denominaciones")

        End Set
    End Property


    Public Property FecAct() As Object
        Get

            ' * Objetivo: Devolver la variable privada FECACT
            ' * Recibe: Nada
            ' * Devuelve: Fecha de ultima actualizacion de la moneda

            FecAct = _FecAct

        End Get
        Set(ByVal Value As Object)
            If Not (Value = _FecAct) Then
                _FecAct = Value
                NotifyPropertyChanged("FecAct")
            End If

        End Set
    End Property



    ''' <summary>
    ''' Constructor de la clase ViaPago
    ''' </summary>
    ''' <param name="remotting">Si es conexion remota</param>
    ''' <param name="UserCode">Codigo de usuario</param>
    ''' <param name="UserPassword">Password de usuario</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        'Public Sub New()
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        'MyBase.New()
    End Sub


End Class
