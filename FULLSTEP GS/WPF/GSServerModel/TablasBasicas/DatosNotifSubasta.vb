﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class DatosNotifSubasta
    Private _oContactosSub As Contactos
    Private _oEMails As EMails

#Region "Propiedades"

    <DataMember()> _
    Public Property ContactosSubasta As Contactos
        Get
            Return _oContactosSub
        End Get
        Set(ByVal value As Contactos)
            _oContactosSub = value
        End Set
    End Property

    <DataMember()> _
    Public Property EMailsSubasta As EMails
        Get
            Return _oEMails
        End Get
        Set(ByVal value As EMails)
            _oEMails = value
        End Set
    End Property

#End Region

End Class
