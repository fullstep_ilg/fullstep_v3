﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class File
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _sArchivo As String
    Private _sNombreArchivo As String
    Private _iTamaño As Integer
    Private _sComentario As String
    Private _dtFecha As Date
    Private _bNuevo As Boolean
    Private _iUploaded As Integer
    Private _sServerFileDirectory As String
    Private _iID As Integer

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructor "

    Public Sub New(ByVal Archivo As String, ByVal NombreArchivo As String, ByVal Comentario As String, ByVal Fecha As Date, ByVal Tamaño As Integer)
        _sArchivo = Archivo
        _sNombreArchivo = NombreArchivo
        _sComentario = Comentario
        _dtFecha = Fecha
        _bNuevo = False
        _iTamaño = Tamaño
        _iUploaded = 100

        Dim oFileInfo As New System.IO.FileInfo(Archivo)
        If Not oFileInfo Is Nothing AndAlso oFileInfo.Exists Then _iTamaño = oFileInfo.Length
    End Sub

    Public Sub New(ByVal Archivo As String, ByVal NombreArchivo As String, ByVal Comentario As String, ByVal Fecha As Date, ByVal Tamaño As Integer, ByVal Nuevo As Boolean, ByVal Uploaded As Integer)
        _sArchivo = Archivo
        _sNombreArchivo = NombreArchivo
        _sComentario = Comentario
        _dtFecha = Fecha
        _bNuevo = Nuevo
        _iTamaño = Tamaño
        _iUploaded = Uploaded

        Dim oFileInfo As New System.IO.FileInfo(Archivo)
        If Not oFileInfo Is Nothing AndAlso oFileInfo.Exists Then _iTamaño = oFileInfo.Length
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property NombreArchivo As String
        Get
            Return _sNombreArchivo
        End Get
        Set(ByVal value As String)
            _sNombreArchivo = value
        End Set
    End Property

    <DataMember()> _
    Public Property Archivo As String
        Get
            Return _sArchivo
        End Get
        Set(ByVal value As String)
            _sArchivo = value
        End Set
    End Property

    <DataMember()> _
    Public Property Tamaño As Integer
        Get
            Return _iTamaño
        End Get
        Set(ByVal value As Integer)
            _iTamaño = value
        End Set
    End Property

    <DataMember()> _
    Public Property Comentario As String
        Get
            Return _sComentario
        End Get
        Set(ByVal value As String)
            _sComentario = value
        End Set
    End Property

    <DataMember()> _
    Public Property Fecha As Date
        Get
            Return _dtFecha
        End Get
        Set(ByVal value As Date)
            _dtFecha = value
        End Set
    End Property

    <DataMember()> _
    Public Property Nuevo As Boolean
        Get
            Return _bNuevo
        End Get
        Set(ByVal value As Boolean)
            _bNuevo = value
        End Set
    End Property

    <DataMember()> _
    Public Property Uploaded As Integer
        Get
            Return _iUploaded
        End Get
        Set(ByVal value As Integer)
            _iUploaded = value
        End Set
    End Property

    <DataMember()> _
    Public Property ServerFileDirectory As String
        Get
            Return _sServerFileDirectory
        End Get
        Set(ByVal value As String)
            _sServerFileDirectory = value
        End Set
    End Property

    <DataMember()> _
    Public Property ID As Integer
        Get
            Return _iID
        End Get
        Set(ByVal value As Integer)
            _iID = value
        End Set
    End Property

#End Region

End Class
