﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class Contexto

    Private _sUsuCod As String
    Private _sUserTZ As String

    <DataMember()> _
    Public Property UsuCod As String
        Get
            Return _sUsuCod
        End Get
        Set(ByVal value As String)
            _sUsuCod = value
        End Set
    End Property

    <DataMember()> _
    Public Property UserTZ As String
        Get
            Return _sUserTZ
        End Get
        Set(ByVal value As String)
            _sUserTZ = value
        End Set
    End Property

End Class

