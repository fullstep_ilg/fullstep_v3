﻿
Public Class SubastaMonitorData    
    Public Proceso As GSServerModel.Proceso
    Public Subasta As GSServerModel.ProcesoSubasta
    Public ProceProves As GSServerModel.ProceProvesResumenSubasta
    Public Eventos As GSServerModel.SubastaEventos
    Public SubastaPujas As GSServerModel.ProcesoSubastaPujas
    Public Moneda As GSServerModel.Moneda
End Class

Public Class SubastaConfData
    Public Proceso As GSServerModel.Proceso
    Public ProcesoDef As GSServerModel.ProcesoDef
    Public ProceProves As GSServerModel.ProcesoProveedores
    Public ProvesComunicados As GSServerModel.ProcesoProveedores
    Public Subasta As GSServerModel.ProcesoSubasta    
    Public SubastaReglas As GSServerModel.ProcesoSubastaReglas
    Public Moneda As GSServerModel.Moneda
    Public Idiomas As GSServerModel.Idiomas    
End Class

Public Class ImpuestosData
    Public Impuestos As GSServerModel.Impuestos
    Public PaisesProvicias As DataSet
End Class
