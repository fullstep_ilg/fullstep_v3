﻿<Serializable()> _
Public Class Acciones
    Inherits Lista(Of Accion)


    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        'Public Sub New()
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        'MyBase.New()
    End Sub


    Public Overloads ReadOnly Property Item(ByVal Id As Long) As Accion
        Get
            Return Me.Find(Function(elemento As Accion) elemento.ID = id)
        End Get
    End Property

    ''' <summary>
    ''' Añade una accion a la lista
    ''' </summary>
    ''' <param name="Id">Id de accion</param>
    ''' <param name="Den">Denominación</param>
    ''' <param name="Orden">Orden</param>
    ''' <returns>Devuelve el elemento accion agregado</returns>
    Public Overloads Function Add(ByVal ID As String, ByVal Den As String, ByVal Orden As Long) As Accion

        ' * Objetivo: Anyadir un idioma a la coleccion
        ' * Recibe: Datos del multidioma
        ' * Devuelve: Multidioma añadido

        Dim objnewmember As Accion

        ' Creacion de objeto moneda

        'objnewmember = New Moneda(mDBServer, mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)
        objnewmember = New Accion(mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)

        ' Paso de los parametros al nuevo objeto moneda

        objnewmember.ID = ID
        objnewmember.DEN = Den
        objnewmember.Orden = Orden

        Me.Add(objnewmember)
        Return objnewmember

    End Function

End Class
