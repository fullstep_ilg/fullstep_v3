﻿Imports System.ComponentModel
<Serializable()> _
Public Class GSExceptionObject
    Inherits Security

    Private _sCodigo As String
    Private _idError As Integer
    Private _Numero As Integer
    Private _sDenominacion As String
    Private _sDescripcion As String


    Public Property Codigo() As String
        Get
            Return _sCodigo
        End Get
        Set(ByVal value As String)
            _sCodigo = value
        End Set
    End Property

    Public Property idError() As Integer
        Get
            Return _idError
        End Get
        Set(ByVal value As Integer)
            _idError = value
        End Set
    End Property

    Public Property Numero() As Integer
        Get
            Return _Numero
        End Get
        Set(ByVal value As Integer)
            _Numero = value
        End Set
    End Property

    Public Property Denominacion() As String
        Get
            Return _sDenominacion
        End Get
        Set(ByVal value As String)
            _sDenominacion = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return _sDescripcion
        End Get
        Set(ByVal value As String)
            _sDescripcion = value
        End Set
    End Property


    '' <summary>
    '' Constructor de la clase Moneda
    '' </summary>
    '' <param name="dbserver">Servidor de la bbdd</param>
    '' <param name="remotting">Si es conexion remota</param>
    '' <param name="UserCode">Codigo de usuario</param>
    '' <param name="UserPassword">Password de usuario</param>
    '' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        'Public Sub New()
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        'MyBase.New()
    End Sub
End Class
