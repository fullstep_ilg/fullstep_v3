﻿<Serializable()> _
Public Class Security
    'Friend mDBServer As FSNDatabaseServer.Root
    Friend mUserCode As String = ""
    Friend mUserPassword As String = ""
    Friend mRemottingServer As Boolean = False
    Friend mIsAuthenticated As Boolean = False

    Friend Sub New()

    End Sub

    'Friend Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
    Friend Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        'mDBServer = dbserver
        mRemottingServer = remotting
        mUserCode = UserCode
        mUserPassword = UserPassword
        mIsAuthenticated = isAuthenticated
    End Sub

    'Friend Function DBServer() As FSNDatabaseServer.Root
    '    If mDBServer Is Nothing Then
    '        mDBServer = New FSNDatabaseServer.Root
    '    End If
    '    Return mDBServer
    'End Function

    Friend Sub Authenticate()
        If Not mIsAuthenticated Then Throw New System.Security.SecurityException("User not authenticated!")
    End Sub
End Class
