﻿<Serializable()> _
Public Class ParametrosGenerales

    Public gbIntegracion As Boolean
    Public giINSTWEB As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb
    Public gbACTIVLOG As Boolean
    Public gbActivarCodProveErp As Boolean
    Public gbSincronizacionMat As Boolean
    Public gsFSP_BD As String
    Public gsFSP_SRV As String
    Public gsMONCEN As String
    Public gsIdioma As String
    Public gsPETOFEWEBDOT As String
    Public giFSP_CIA As Integer
    Public gsFSP_CIA As String
    Public gsPetOfeMailSubject As String
    Public gsURLSUMMIT As String
    Public gbSMTPUsarCuenta As Boolean
    Public gsSMTPCuentaMail As String

    Public gbPremium As Boolean
    Public gbUsarRemitenteEmpresa As Boolean
    Public gsMARCADOR_CARPETA_PLANTILLAS As String
End Class
