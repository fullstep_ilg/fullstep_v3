﻿<Serializable()> _
Public Class Multiidioma
    Inherits Security
    ' *** Clase: Moneda

    ' Variables privadas con la informacion de una moneda
    Private _Idioma As String
    Private _Denominacion As String
    Private _FecAct As Object


    Public Property FecAct() As Object
        Get

            ' * Objetivo: Devolver la variable privada FECACT
            ' * Recibe: Nada
            ' * Devuelve: Fecha de ultima actualizacion de la moneda

            FecAct = _FecAct

        End Get
        Set(ByVal Value As Object)

            ' * Objetivo: Dar valor a la variable privada FECACT
            ' * Recibe: Fecha de ultima actualizacion de la moneda
            ' * Devuelve: Nada

            _FecAct = Value

        End Set
    End Property

    Public Property Denominacion() As String
        Get

            ' * Objetivo: Devolver la variable privada DEN
            ' * Recibe: Nada
            ' * Devuelve: Denominacion de la moneda

            Denominacion = _Denominacion

        End Get
        Set(ByVal Value As String)

            ' * Objetivo: Dar valor a la variable privada DEN
            ' * Recibe: Denominacion de la moneda
            ' * Devuelve: Nada

            _Denominacion = Value

        End Set
    End Property

    Public Property Idioma() As String
        Get

            ' * Objetivo: Devolver la variable privada COD
            ' * Recibe: Nada
            ' * Devuelve: Codigo de la moneda

            Idioma = _Idioma

        End Get
        Set(ByVal Value As String)

            ' * Objetivo: Dar valor a la variable privada COD
            ' * Recibe: Codigo de la moneda
            ' * Devuelve: Nada

            _Idioma = Value

        End Set
    End Property

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    ''' <summary>
    ''' Constructor de la clase Multiidioma
    ''' </summary>
    ''' <param name="remotting">Si es conexion remota</param>
    ''' <param name="UserCode">Codigo de usuario</param>
    ''' <param name="UserPassword">Password de usuario</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        'Public Sub New()
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        'MyBase.New()
    End Sub

End Class
