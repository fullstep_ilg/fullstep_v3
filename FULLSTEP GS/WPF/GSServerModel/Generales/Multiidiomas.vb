﻿<Serializable()> _
Public Class Multiidiomas
    Inherits Lista(Of Multiidioma)

    Public Overloads ReadOnly Property Item(ByVal Cod As String) As Multiidioma
        Get
            Return Me.Find(Function(elemento As Multiidioma) elemento.Idioma = Cod)
        End Get
    End Property

    ''' <summary>
    ''' Añade un elemento Multiidioma a la lista
    ''' </summary>
    ''' <param name="Idi">Idioma</param>
    ''' <param name="Den">Denominación</param>
    ''' <param name="FECACT">Fecha última actualización</param>
    ''' <returns>Devuelve el elemento CMoneda agregado</returns>
    Public Overloads Function Add(ByVal Idi As String, ByVal Den As String, ByVal FECACT As DateTime) As Multiidioma

        ' * Objetivo: Anyadir un idioma a la coleccion
        ' * Recibe: Datos del multidioma
        ' * Devuelve: Multidioma añadido

        Dim objnewmember As Multiidioma

        ' Creacion de objeto moneda

        'objnewmember = New Moneda(mDBServer, mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)
        objnewmember = New Multiidioma(mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)

        ' Paso de los parametros al nuevo objeto moneda

        objnewmember.Idioma = Idi
        objnewmember.Denominacion = Den
        objnewmember.FecAct = FECACT


        Me.Add(objnewmember)
        Return objnewmember

    End Function

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    '' <summary>
    '' Constructor de la clase Multidiomas
    '' </summary>
    '' <param name="dbserver">Servidor de la bbdd</param>
    '' <param name="remotting">Si es conexion remota</param>
    '' <param name="UserCode">Codigo de usuario</param>
    '' <param name="UserPassword">Password de usuario</param>
    '' <param name="isAuthenticated">Si está autenticado</param>
    '' <remarks>
    '' Llamada desde: Todos los sitios donde se instancien objetos de esta clase
    '' Tiempo máximo: 0 sec. </remarks>
    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        'Public Sub New()
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        'MyBase.New()
    End Sub

End Class
