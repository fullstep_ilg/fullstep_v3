﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class EMail
    Inherits Security
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _sNombreEMail As String
    Private _iTipoMail As Fullstep.FSNLibrary.TiposDeDatos.TipoEmail
    Private _oPara As Contactos
    Private _oCC As Contactos
    Private _oBCC As Contactos
    Private _oDe As Contacto
    Private _sAsunto As String
    Private _sContenido As String
    Private _oAdjuntos As Files

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructor "

    Public Sub New()
        _oPara = New Contactos
        _oCC = New Contactos
        _oBCC = New Contactos
        _oAdjuntos = New Files
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property NombreEMail As String
        Get
            Return _sNombreEMail
        End Get
        Set(ByVal value As String)
            _sNombreEMail = value
        End Set
    End Property

    <DataMember()> _
    Public Property TipoEmail As Fullstep.FSNLibrary.TiposDeDatos.TipoEmail
        Get
            Return _iTipoMail
        End Get
        Set(ByVal value As Fullstep.FSNLibrary.TiposDeDatos.TipoEmail)
            _iTipoMail = value
        End Set
    End Property

    <DataMember()> _
    Public Property Para As Contactos
        Get
            Return _oPara
        End Get
        Set(ByVal value As Contactos)
            _oPara = value
        End Set
    End Property

    <DataMember()> _
    Public Property CC As Contactos
        Get
            Return _oCC
        End Get
        Set(ByVal value As Contactos)
            _oCC = value
        End Set
    End Property

    <DataMember()> _
    Public Property BCC As Contactos
        Get
            Return _oBCC
        End Get
        Set(ByVal value As Contactos)
            _oBCC = value
        End Set
    End Property

    <DataMember()> _
    Public Property De As Contacto
        Get
            Return _oDe
        End Get
        Set(ByVal value As Contacto)
            _oDe = value
        End Set
    End Property

    <DataMember()> _
    Public Property Asunto As String
        Get
            Return _sAsunto
        End Get
        Set(ByVal value As String)
            _sAsunto = value
        End Set
    End Property

    <DataMember()> _
    Public Property Contenido As String
        Get
            Return _sContenido
        End Get
        Set(ByVal value As String)
            _sContenido = value
        End Set
    End Property

    <DataMember()> _
    Public Property Adjuntos As Files
        Get
            Return _oAdjuntos
        End Get
        Set(ByVal value As Files)
            _oAdjuntos = value
        End Set
    End Property

#End Region

End Class
