<Serializable()> _
    Public Class Errores
    Inherits Security

    Private mlID As Long

    Public Property ID() As Long
        Get
            Return mlID
        End Get

        Set(ByVal Value As Long)
            mlID = Value
        End Set
    End Property

    'Public Sub Create(ByVal sPagina As String, ByVal sUsuario As String, ByVal sEx_FullName As String, ByVal sEx_Message As String, ByVal sEx_StackTrace As String, ByVal sSv_Query_String As String, ByVal sSv_Http_User_Agent As String)
    '    Authenticate()
    '    If mRemottingServer Then
    '        mlID = DBServer.Errores_Create(sPagina, sUsuario, sEx_FullName, sEx_Message, sEx_StackTrace, sSv_Query_String, sSv_Http_User_Agent, mUserCode, mUserPassword)
    '    Else
    '        mlID = DBServer.Errores_Create(sPagina, sUsuario, sEx_FullName, sEx_Message, sEx_StackTrace, sSv_Query_String, sSv_Http_User_Agent)
    '    End If
    'End Sub

    'Friend Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
    '    MyBase.New(dbserver, remotting, UserCode, UserPassword, isAuthenticated)
    'End Sub
    Friend Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub
End Class
