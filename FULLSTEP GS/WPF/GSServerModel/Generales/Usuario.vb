﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

<DataContract()> _
Public Class Usuario

    Inherits Security
    Implements INotifyPropertyChanged

    ' Variables privadas con la informacion de una moneda

    Private _sCod As String 'local copy
    Private _sPwd As String
    Private _iTipo As Integer 'local copy
    Private _oAcciones As Acciones 'local copy
    Private _oPerfil As Perfil
    Private _sTimeZone As String
    Private _sLanguageTag As String
    Private _sIdioma As String
    Private _iTipoEmail As Integer
    Private _sPer As String
    Private _sEMail As String
    Private _dtFecAct As Nullable(Of Date)

    'Private _Codigo As String
    'Private _Denominacion As String
    'Private _Denominaciones As Multiidiomas
    'Private _Equivalencia As Double    

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

#Region " Constructor "

    ''' <summary>
    ''' Constructor de la clase Moneda
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="remotting">Si es conexion remota</param>
    ''' <param name="UserCode">Codigo de usuario</param>
    ''' <param name="UserPassword">Password de usuario</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        'Public Sub New()
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        'MyBase.New()
    End Sub

#End Region

#Region " Propiedades "

    <DataMember()> _
    Public Property Tipo() As Integer
        Get
            Tipo = _iTipo
        End Get
        Set(ByVal value As Integer)
            _iTipo = value
        End Set
    End Property

    <DataMember()> _
    Public Property Cod() As String
        Get
            Cod = _sCod
        End Get
        Set(ByVal value As String)
            _sCod = value
        End Set
    End Property

    <DataMember()> _
    Public Property Perfil() As GSServerModel.Perfil
        Get
            Perfil = _oPerfil
        End Get
        Set(ByVal value As GSServerModel.Perfil)
            _oPerfil = value
        End Set
    End Property

    <DataMember()> _
    Public Property Acciones() As GSServerModel.Acciones
        Get
            Acciones = _oAcciones
        End Get
        Set(ByVal value As GSServerModel.Acciones)
            _oAcciones = value
        End Set
    End Property

    <DataMember()> _
    Public Property TimeZone As String
        Get
            Return _sTimeZone
        End Get
        Set(ByVal value As String)
            _sTimeZone = value
        End Set
    End Property

    <DataMember()> _
    Public Property LanguageTag As String
        Get
            Return _sLanguageTag
        End Get
        Set(ByVal value As String)
            _sLanguageTag = value
        End Set
    End Property

    <DataMember()> _
    Public Property Idioma As String
        Get
            Return _sIdioma
        End Get
        Set(ByVal value As String)
            _sIdioma = value
        End Set
    End Property

    <DataMember()> _
    Public Property TipoEmail As Integer
        Get
            Return _iTipoEmail
        End Get
        Set(ByVal value As Integer)
            _iTipoEmail = value
        End Set
    End Property

    <DataMember()> _
    Public Property Persona As String
        Get
            Return _sPer
        End Get
        Set(ByVal value As String)
            _sPer = value
        End Set
    End Property

    <DataMember()> _
    Public Property EMail As String
        Get
            Return _sEMail
        End Get
        Set(ByVal value As String)
            _sEMail = value
        End Set
    End Property

    <DataMember()> _
    Public Property FecAct As Nullable(Of Date)
        Get
            Return _dtFecAct
        End Get
        Set(ByVal value As Nullable(Of Date))
            If Not Date.Equals(value, _dtFecAct) Then
                _dtFecAct = value
                NotifyPropertyChanged("FECACT")
            End If
        End Set
    End Property

#End Region

End Class
