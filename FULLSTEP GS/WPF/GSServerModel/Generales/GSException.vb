﻿Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.ComponentModel
<Serializable()> _
Public Class GSException
    Inherits Security
    Implements INotifyPropertyChanged

    Private _Number As ErroresGS
    Private _oEstructura As GSExceptionObjects
    Private _ErrorId As Integer
    Private _ErrorStr As String



    Public Event PropertyChanged As PropertyChangedEventHandler _
    Implements INotifyPropertyChanged.PropertyChanged


    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

    Public Property Errores() As GSServerModel.GSExceptionObjects
        Get
            Errores = _oEstructura
        End Get
        Set(ByVal value As GSServerModel.GSExceptionObjects)
            _oEstructura = value
        End Set
    End Property



    Public Property Number() As ErroresGS
        Get
            Return _Number
        End Get
        Set(ByVal value As ErroresGS)
            _Number = value
            NotifyPropertyChanged("Number")
        End Set
    End Property

    Public Property ErrorId() As Integer
        Get
            Return _ErrorId
        End Get
        Set(ByVal value As Integer)
            _ErrorId = value
        End Set
    End Property

    Public Property ErrorStr() As String
        Get
            Return _ErrorStr
        End Get
        Set(ByVal value As String)
            _ErrorStr = value
        End Set
    End Property


    '' <summary>
    '' Constructor de la clase Moneda
    '' </summary>
    '' <param name="dbserver">Servidor de la bbdd</param>
    '' <param name="remotting">Si es conexion remota</param>
    '' <param name="UserCode">Codigo de usuario</param>
    '' <param name="UserPassword">Password de usuario</param>
    '' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        'Public Sub New()
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        'MyBase.New()
    End Sub

End Class
