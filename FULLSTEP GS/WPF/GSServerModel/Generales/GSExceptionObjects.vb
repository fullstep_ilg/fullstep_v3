﻿<Serializable()> _
Public Class GSExceptionObjects
    Inherits Lista(Of GSExceptionObject)

    Public Overloads ReadOnly Property Item(ByVal Cod As String) As GSExceptionObject
        Get
            Return Me.Find(Function(elemento As GSExceptionObject) elemento.Codigo = Cod)
        End Get
    End Property

    '' <summary>
    '' Añade un elemento Multiidioma a la lista
    '' </summary>
    '' <param name="Idi">Idioma</param>
    '' <param name="Den">Denominación</param>
    '' <param name="FECACT">Fecha última actualización</param>
    '' <returns>Devuelve el elemento CMoneda agregado</returns>
    Public Overloads Function Add(ByVal Codigo As String, ByVal idError As Integer, ByVal Numero As Integer, ByVal Den As String) As GSExceptionObject

        ' * Objetivo: Anyadir un idioma a la coleccion
        ' * Recibe: Datos del multidioma
        ' * Devuelve: Multidioma añadido

        Dim objnewmember As GSExceptionObject

        ' Creacion de objeto moneda

        objnewmember = New GSExceptionObject(mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)

        ' Paso de los parametros al nuevo objeto moneda

        objnewmember.Codigo = Codigo
        objnewmember.idError = idError
        objnewmember.Numero = Numero
        objnewmember.Descripcion = Den


        Me.Add(objnewmember)
        Return objnewmember

    End Function

    '' <summary>
    '' Constructor de la clase Multidiomas
    '' </summary>
    '' <param name="dbserver">Servidor de la bbdd</param>
    '' <param name="remotting">Si es conexion remota</param>
    '' <param name="UserCode">Codigo de usuario</param>
    '' <param name="UserPassword">Password de usuario</param>
    '' <param name="isAuthenticated">Si está autenticado</param>
    '' <remarks>
    '' Llamada desde: Todos los sitios donde se instancien objetos de esta clase
    '' Tiempo máximo: 0 sec. </remarks>
    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        'Public Sub New()
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        'MyBase.New()
    End Sub

End Class
