﻿<Serializable()> _
Public Class EstadoProveedorPortal
    Public CodigoGS As String
    Public CodigoPortal As String
    Public Autorizado As Boolean
    Public EstadoGS As Integer
    Public EstadoPortal As Integer

    Public Sub New(ByVal Prove As String)
        CodigoGS = Prove
        Autorizado = False
    End Sub
End Class

<Serializable()> _
Public Class ValidacionAtributos
    Public AtributosCorrectos As Boolean
    Public AtributosProceso As ProcesoAtributosEspecificacion
    Public AtributosPorcesoGrupo As ProcesoGrupoAtributosEspecificacion
    Public AtributosItem As ItemAtributosEspecificacion
End Class

<Serializable()> _
Public Class DatosProcesosPublicados
    Public ProcesosPublicados As Integer
    Public ProcesosPublicadosSinOfertas As Integer
    Public ProcesosPublicadosARevisar As Integer
End Class

