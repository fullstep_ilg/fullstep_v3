﻿<Serializable()> _
Public Class Perfil
    Inherits Security

    Private _iId As Integer
    Private _sCod As String
    Private _sDen As String 'local copy
    Private _oAcciones As Acciones 'local copy

    Public Property Id() As Integer
        Get
            Return _iId
        End Get
        Set(ByVal value As Integer)
            _iId = value
        End Set
    End Property

    Public Property Cod() As String
        Get
            Cod = _sCod
        End Get
        Set(ByVal value As String)
            _sCod = value
        End Set
    End Property

    Public Property Den() As String
        Get
            Den = _sDen
        End Get
        Set(ByVal value As String)
            _sDen = value
        End Set
    End Property

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        'Public Sub New()
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        'MyBase.New()
    End Sub


    Public Property Acciones() As GSServerModel.Acciones
        Get
            Acciones = _oAcciones
        End Get
        Set(ByVal value As GSServerModel.Acciones)
            _oAcciones = value
        End Set
    End Property
End Class
