﻿Imports System.Runtime.Serialization

<DataContract()> _
Public Class EMails
    Inherits Lista(Of EMail)

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Overloads ReadOnly Property Item(ByVal NombreEMail As String) As EMail
        Get
            Return Me.Find(Function(elemento As EMail) elemento.NombreEMail = NombreEMail)
        End Get
    End Property

End Class
