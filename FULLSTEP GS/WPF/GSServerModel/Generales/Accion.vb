﻿<Serializable()> _
Public Class Accion
    Inherits Security


    Private _lID As Long
    Private _lOrden As Long
    Private _sDen As String


    Public Property ID() As Long
        Get
            ID = _lID
        End Get
        Set(ByVal value As Long)
            _lID = value
        End Set
    End Property

    Public Property Orden() As Long
        Get
            Orden = _lOrden
        End Get
        Set(ByVal value As Long)
            _lOrden = value
        End Set
    End Property

    Public Property DEN() As String
        Get
            DEN = _sDen
        End Get
        Set(ByVal value As String)
            _sDen = value
        End Set
    End Property

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        'Public Sub New()
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
        'MyBase.New()
    End Sub


End Class
