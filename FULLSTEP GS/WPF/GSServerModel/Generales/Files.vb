﻿Imports System.Runtime.Serialization

<DataContract()> _
Public Class Files
    Inherits Lista(Of GSServerModel.File)

    Public Sub New()
        MyBase.New(True, String.Empty, String.Empty, False)
    End Sub

    Public Sub New(ByVal oFiles As List(Of GSServerModel.File))
        MyBase.New(True, String.Empty, String.Empty, False, oFiles)
    End Sub

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

    Public Overloads ReadOnly Property Item(ByVal Nombre As String) As GSServerModel.File
        Get
            Return Me.Find(Function(elemento As GSServerModel.File) elemento.NombreArchivo = Nombre)
        End Get
    End Property

End Class
