﻿Public Class TiposDeDatos

    Public Enum TipoGeneral
        SinTipo = 0
        TipoString = 1
        TipoNumerico = 2
        TipoFecha = 3
        TipoBoolean = 4
        TipoTextoCorto = 5 '25 char
        TipoTextoMedio = 6 '800 char
        TipoTextoLargo = 7 '4000 char
        TipoArchivo = 8
        TipoDesglose = 9
        TipoCheckBox = 10
        TipoPresBajaLog = 11
        TipoDepBajaLog = 12
        TipoDesgloseOblig = 13
        TipoDesgloseObligNoPopup = 14
    End Enum

    Public Enum Aplicaciones
        EP = &H7FFF
        QA = &HFFFF
        PM = &HFFFF
    End Enum

    Public Enum Producto
        GS = 1
        PM = 2
        QA = 3
        EP = 4
        OTRO = 5
        CM = 8
        IM = 9
    End Enum

    Public Enum EntidadNotificacion
        'Las denominaciones en diferentes idiomas se encuentran en el módulo 185 de WEBFSWTEXT, coincidiendo el id de cada
        'registro con el enumerador. Si se cambia algun nombre o número hay que actualizar dicho módulo.
        Calificaciones = 1
        Certificado = 2
        NoConformidad = 3
        Colaboracion = 4
        Orden = 5 'EP
        Recepcion = 6 'EP
        Solicitud = 7 'PM o GS
        Contrato = 8 'CM
        Factura = 9 'IM
        ProcesoCompra = 10 'GS
        PedDirecto = 11 'GS

    End Enum

    Public Enum TipoNotificacion
        'Notificaciones GS
        AsignacionSolicitudCompra = 1
        AprobacionSolicitudPet = 2
        RechazoSolicitudPet = 3
        AnulacionSolicitudPet = 4
        CierreSolicitudPet = 5
        ReaperturaSolicitudPet = 6

        PetOferta = 50
        PetOfertaWeb = 51
        PetOfertaSubasta = 52
        PetOfertaWebSubasta = 53
        SubastaWPF = 54
        ComObjetivos = 55
        ComObjetivosWeb = 56
        AdjudicacionYOrdenCompra = 57
        ProveNoAdjudicado = 58
        ProximaDespublicacion = 59
    End Enum

    Public Enum ModulosIdiomas
        Menu = 1
        Solicitudes = 2
        AltaSolicitudes = 3
        Solicitud = 4
        Seguimiento = 5
        DetalleSeguimiento = 6
        HistoricoEstados = 7
        AnularSolicitud = 8
        DetallePedido = 9
        DetalleProceso = 10
        ComentEstado = 11
        AccionSolicitud = 12
        ComentariosSolic = 13
        DenominacionDest = 14
        DenominacionUP = 15
        Workflow = 16
        Aprobacion = 17
        Rechazo = 18
        AprobSuperior = 19
        DetalleCampoArchivo = 20
        Traslados = 21
        AprobacionOK = 22
        Usuarios = 23
        Trasladar = 24
        TrasladoOK = 25
        DetallePersona = 26
        GestionTraslado = 27
        Devolucion = 28
        DevolucionOK = 29
        Desglose = 30
        EmisionOK = 31
        PedidoDirecto = 32
        RechazoOK = 33
        ImposibleAccion = 34
        DetalleProveedor = 35
        DesgloseControl = 36
        CamposControl = 37
        DownloadFile = 38
        AttachFile = 39
        Certificados = 40
        SolicitarCertif = 41
        AltaCertificados = 42
        CertifProveedores = 43
        ImpExp = 44
        Opciones = 45
        CertifCalendario = 46
        ImpExp_Sel = 47
        CertifPublicadoOK = 48
        CertifNoPublicado = 49
        DetalleCertificado = 50
        EnviarAvisos = 51
        PublicarCertificado = 52
        Proveedores = 53
        AttachFileProcesos = 54
        BusquedaArticulos = 55
        ValidarArticulo = 56
        PuntProveedores = 57
        FichaProveedor = 58
        Materiales = 59
        Presupuestos = 60
        EnviarSolicitud = 61
        ReemitirSolicitud = 62
        NoConformidades = 63
        AltaNoConformidad = 64
        NoConformidadEmitidaOK = 65
        NoConfonfEmitidaWorkfl = 66
        DetalleNoConformidad = 67
        GuardarInstancia = 68
        CierreNoConformidad = 69
        AprobRechazoAccion = 70
        ComprobarNoConfPortal = 71
        AttachFilePedidos = 72
        MantenimientoMat = 73
        AltaMaterialQA = 74
        ModificarDenMatQA = 75
        AsignaCertifAMatQA = 76
        AsignarMaterialesAMatQA = 77
        EliminarMaterialesQA = 78
        SelectorCamposMatQA = 79
        ParametrosGenerales = 80
        FiltroCertificados = 81
        ResultCertificados = 82
        SelectorCamposCertif = 83
        CierreNoConformidadConRevisor = 84
        CertifSolicitadosOK = 85
        NoConformidadRevisor = 86
        DetalleMatGSProv = 87
        RechazoNoConfRevisor = 88
        SelectorCamposProveQA = 89
        Listados = 90
        flowDiagram = 91
        RealizarAccion = 92
        AccionRealizadaOK = 93
        AvisosPrecondiciones = 94
        AccionInvalida = 95
        EnviarMailProveedores = 96
        MailProveedores = 97
        DetalleDenominacionPresupuestos = 98
        UnidadesOrganizativas = 99
        Espera = 100
        Otros = 101
        BusquedaNuevoCodArticulos = 102
        UsuarioNoParticipante = 103
        BusquedaSolicitudes = 104
        DetalleSeguimientoPadre = 105
        UnidadesNegocio = 106
        ModificarDenUnidadNeg = 107
        EliminarUnidadNeg = 108
        AñadirUnidadNeg = 109
        UltimasAdjudicaciones = 110
        ProvFavServer = 111
        SolicitudesFavoritas = 112
        EnviarFavoritos = 113
        DetalleSolicitudesHijas = 114
        popupDesglose = 115
        TablaExterna = 116
        AyudaCampo = 117
        PaginaError = 118
        NoConformidadEnProceso = 119
        AsignarObjetivosySuelos = 120
        Webparts = 121
        Login = 122
        Activos = 124
        CentrosCoste = 125
        Partidas = 126

        ' Módulos EP (Sumar 32768 al cod del módulo en BBDD)
        EP_CatalogoWeb = 32773
        EP_Destinos = 32775
        EP_UnidadesPedido = 32776
        EP_AprobacionPedidos = 32782
        EP_DetalleAprovisionador = 32791
        EP_RecepcionPedidos = 32797
        EP_Seguimiento = 32799
        EP_DatosContacto = 32822
        EP_PedidoLibre = 32823
        EP_PedidoLibreRellenar = 32825
        EP_PedidoLibreOkEnviarCesta = 32826
        EP_EmisionPedidos = 32828
        EP_SelecFavoritos = 32852
        EP_Favoritos = 32841
        EP_NuevoFavorito = 32853
    End Enum

    Public Enum TipoDeSolicitud
        SolicitudDeCompras = 1
        Certificado = 2
        NoConformidad = 3
        Otros = 0
    End Enum

    Public Enum TipoCampoGS
        SinTipo = 0
        'Public Enum TipoCampoSC
        DescrBreve = 1
        DescrDetallada = 2
        Importe = 3
        Cantidad = 4
        FecNecesidad = 5
        IniSuministro = 6
        FinSuministro = 7
        ArchivoEspecific = 8
        PrecioUnitario = 9
        PrecioUnitarioAdj = 10
        ProveedorAdj = 11
        CantidadAdj = 12
        'End Enum

        'Public Enum TipoCampoCertificado
        NombreCertif = 20
        FecObtencion = 21
        FecExpiracion = 22
        Certificado = 23
        FecDespublicacion = 27
        FechaLimCumplim = 28
        'End Enum

        'Public Enum TipoCampoNoConformidad
        Titulo = 30
        Motivo = 31
        CausasYConclus = 32
        Peso = 33
        Acciones = 34
        Accion = 35
        Fec_inicio = 36
        Fec_cierre = 37
        Responsable = 38
        Observaciones = 39
        Documentación = 40

        EstadoNoConf = 42
        EstadoInternoNoConf = 43
        PiezasDefectuosas = 44
        ImporteRepercutido = 45
        Subtipo = 46

        'End Enum

        Proveedor = 100
        FormaPago = 101
        Moneda = 102
        Material = 103
        CodArticulo = 104
        Unidad = 105
        Desglose = 106
        Pais = 107
        Provincia = 108
        Dest = 109
        PRES1 = 110
        Pres2 = 111
        Pres3 = 112
        Pres4 = 113
        Contacto = 114
        Persona = 115
        ProveContacto = 116
        Rol = 117
        DenArticulo = 118
        NuevoCodArticulo = 119
        NumSolicitERP = 120

        UnidadOrganizativa = 121
        Departamento = 122
        OrganizacionCompras = 123
        Centro = 124
        Almacen = 125
        ListadosPersonalizados = 126
        ImporteSolicitudesVinculadas = 127
        RefSolicitud = 128

        CentroCoste = 129
        Partida = 130
        Activo = 131


    End Enum

    <Serializable()> _
Public Enum TipoDeDatoXML
        Entero = 1
        Texto = 2
        Booleano = 3
        Real = 4
        Fecha = 5
    End Enum

    <Serializable()> _
     Public Structure ParametrosOrigenDeDatos
        Dim Nombre As String
        Dim TipoDeDatos As TipoDeDatoXML
        Dim ValorDato As String
    End Structure

    Public Enum OrigenRegistroRecepcionesPedido As Short
        Ninguno = -1
        Integracion = 0
        RegistroActividad = 1
        IntegracionyRegistroActividad = 2
    End Enum

    ' Indica el origen de un movimiento en el LOG
    Public Enum OrigenIntegracion
        FSGSInt = 0
        FSGSReg = 1
        FSGSIntReg = 2
        erp = 3
    End Enum

    Public Structure ParametrosGenerales

        Dim gbSolicitudesCompras As Boolean
        Dim gsAccesoFSPM As TipoAccesoFSPM
        Dim gsAccesoFSQA As TipoAccesoFSQA
        Dim gbAccesoFSEP As Boolean
        Dim gbAccesoFSSM As Boolean
        Dim gbAccesoQACertificados As Boolean
        Dim gbAccesoQANoConformidad As Boolean
        Dim gbOblCodPedido As Boolean
        Dim gbOblCodPedDir As Boolean
        Dim gbOblCodPedidoObl As Boolean
        Dim nomPedERP() As String
        Dim nomPedDirERP() As String
        Dim gbFSQA_Revisor As Boolean 'Figura del revisor
        Dim gbUsar_OrgCompras As Boolean
        Dim gbArticulosGenericos As Boolean ' edu 436
        Dim gbVarCal_Materiales As Boolean
        Dim gbUsarART4_UON As Boolean
        Dim gbOblAsignarPresArt As Boolean
        Dim gbPymes As Boolean
        Dim gbPedidoDirectoEnvioMail As Boolean

        ' Parámetros EP
        Dim giLOGPREBLOQ As Short
        Dim gbTrasladoAProvERP As Boolean
        Dim gbActivLog As Boolean
        Dim giRecepAprov As OrigenRegistroRecepcionesPedido

        Dim g_bAccesoFSFA As Boolean
        Dim giRecepDirecto As OrigenRegistroRecepcionesPedido
    End Structure

    Public Structure LongitudesDeCodigos

        Dim giLongCodART As Integer
        Dim giLongCodCAL As Integer
        Dim giLongCodCOM As Integer
        Dim giLongCodDEP As Integer
        Dim giLongCodDEST As Integer
        Dim giLongCodEQP As Integer
        Dim giLongCodGMN1 As Integer
        Dim giLongCodGMN2 As Integer
        Dim giLongCodGMN3 As Integer
        Dim giLongCodGMN4 As Integer
        Dim giLongCodMON As Integer
        Dim giLongCodOFEEST As Integer
        Dim giLongCodPAG As Integer
        Dim giLongCodPAI As Integer
        Dim giLongCodPER As Integer
        Dim giLongCodPERF As Integer
        Dim giLongCodPRESCON1 As Integer
        Dim giLongCodPRESCON2 As Integer
        Dim giLongCodPRESCON3 As Integer
        Dim giLongCodPRESCON4 As Integer
        Dim giLongCodPRESCONCEP31 As Integer
        Dim giLongCodPRESCONCEP32 As Integer
        Dim giLongCodPRESCONCEP33 As Integer
        Dim giLongCodPRESCONCEP34 As Integer
        Dim giLongCodPRESCONCEP41 As Integer
        Dim giLongCodPRESCONCEP42 As Integer
        Dim giLongCodPRESCONCEP43 As Integer
        Dim giLongCodPRESCONCEP44 As Integer
        Dim giLongCodPRESPROY1 As Integer
        Dim giLongCodPRESPROY2 As Integer
        Dim giLongCodPRESPROY3 As Integer
        Dim giLongCodPRESPROY4 As Integer
        Dim giLongCodPROVE As Integer
        Dim giLongCodPROVI As Integer
        Dim giLongCodROL As Integer
        Dim giLongCodUNI As Integer
        Dim giLongCodUON1 As Integer
        Dim giLongCodUON2 As Integer
        Dim giLongCodUON3 As Integer
        Dim giLongCodUSU As Integer
        Dim giLongCodACT1 As Integer
        Dim giLongCodACT2 As Integer
        Dim giLongCodACT3 As Integer
        Dim giLongCodACT4 As Integer
        Dim giLongCodACT5 As Integer
        Dim giLongCia As Integer
        Dim giLongCodCAT1 As Integer
        Dim giLongCodCAT2 As Integer
        Dim giLongCodCAT3 As Integer
        Dim giLongCodCAT4 As Integer
        Dim giLongCodCAT5 As Integer
        Dim giLongCodGRUPOPROCE As Integer
        Dim giLongCodDENART As Integer
        Dim giLongCodACTIVO As Integer
        Dim giLongCodCENTROCOSTE As Integer
    End Structure

    Public Enum TipoFecha
        FechaAlta = 1
        UnDiaDespues = 2
        DosDiasDespues = 3
        TresDiasDespues = 4
        CuatroDiasDespues = 5
        CincoDiasDespues = 6
        SeisDiasDespues = 7
        UnaSemanaDespues = 8
        DosSemanaDespues = 9
        TresSemanaDespues = 10
        UnMesDespues = 11
        DosMesDespues = 12
        TresMesDespues = 13
        CuatroMesDespues = 14
        CincoMesDespues = 15
        SeisMesDespues = 16
        UnAnyoDespues = 17
    End Enum

    Public Enum TipoAccesoFSPM
        AccesoFSPMSolicCompra = 1
        AccesoFSPMCompleto = 2
        SinAcceso = 3
    End Enum

    Public Enum TipoAccesoFSQA
        AccesoFSQABasico = 1
        SinAcceso = 2
        Modulos = 3
    End Enum

    Public Enum IdsFicticios
        EstadoActual = 1000
        Comentario = 1020
        EstadoInterno = 1010
        Grupo = 900
    End Enum

    Public Enum TipoAtributo
        CuentaContable = 886
    End Enum

    Public Enum LiteralesParametros
        PedidoERP = 31
    End Enum

    Public Enum TipoPonderacionCert
        SinPonderacion = 0
        EscalaContinua = 1
        EscalaDiscreta = 2
        Formula = 3
        Manual = 4
        SiNo = 5
        PorCaducidad = 6
        Automatico = 7
    End Enum

    Public Enum TipoPonderacionVariables
        PondNCNumero = 0
        PondNCMediaPesos = 1
        PondNCSumaPesos = 2
        PondCertificado = 3
        PondIntEscContinua = 4
        PondIntFormula = 5
        PondManual = 6
        PondPPM = 7
        PondCargoProveedores = 8
        PondTasaServicios = 9
        PondNoPonderacion = 99
    End Enum

    Public Enum TipoEstadoSolic
        Guardada = 0
        Enviada = 1
        EnCurso = 2
        Rechazada = 6
        Aprobada = 7
        Anulada = 8

        SCPendiente = 100
        SCAprobada = 101
        SCRechazada = 102
        SCAnulada = 103
        SCCerrada = 104

        CertificadoPub = 200
        NoConformidadEnviada = 300
        NoConformidadGuardada = 301

        Pendiente = 1000
    End Enum

    Public Enum TipoAccionSolicitud
        Alta = 1
        Modificacion = 2
        Traslado = 3
        Devolucion = 4
        Aprobacion = 5
        Rechazo = 6
        Anulacion = 7
        Reemision = 8
        RechazoEficazRevisor = 9 'Para Controlar  el rechazo del revisor
        RechazoNoEficazRevisor = 10
    End Enum

    Public Enum TipoEstadoOrdenEntrega
        PendienteDeAprobacion = 0
        DenegadoParcialAprob = 1
        EmitidoAlProveedor = 2
        AceptadoPorProveedor = 3
        EnCamino = 4
        EnRecepcion = 5
        RecibidoYCerrado = 6
        Anulado = 20
        RechazadoPorProveedor = 21
        DenegadoTotalAprobador = 22
    End Enum

    Public Enum TipoEstadoProceso

        sinitems = 1        'Sin validar y sin items
        ConItemsSinValidar = 2      'Con items y sin validar
        validado = 3        ' Validado y sin provedores asignados
        Conproveasignados = 4  ' Validado y con proveedores asignados pero sin validar la asignacion
        conasignacionvalida = 5 ' Con asignacion de proveedores validada
        conpeticiones = 6
        conofertas = 7
        ConObjetivosSinNotificar = 8
        ConObjetivosSinNotificarYPreadjudicado = 9
        PreadjYConObjNotificados = 10
        ParcialmenteCerrado = 11
        conadjudicaciones = 12
        ConAdjudicacionesNotificadas = 13
        Cerrado = 20

    End Enum

    Public Enum TipoCampoPredefinido
        Normal = 0
        CampoGS = 1  ' Solicitud de compras
        Atributo = 2   ' Atributo
        Calculado = 3
        Certificado = 4
        NoConformidad = 5
    End Enum

    Public Enum TipoAccesoFSEP
        Basico = 0
        SoloAprovisionador = 1
        SoloAprobador = 2
        AprobadorYAprovisionador = 3
    End Enum

    Public Enum TipoAmbitoProceso
        AmbProceso = 1
        AmbGrupo = 2
        AmbItem = 3
    End Enum

    Public Enum TipoEstadoNoConformidad
        Guardada = 0
        Abierta = 1
        CierrePosEnPlazo = 2
        CierrePosFueraPlazo = 3
        CierreNegativo = 4
        CierrePosSinRevisar = 5
        CierreNegSinRevisar = 6
    End Enum

    Public Enum TipoEstadoAcciones
        SinRevisar = 1
        Aprobada = 2
        Rechazada = 3
        FinalizadaSinRevisar = 4
        FinalizadaRevisada = 5
    End Enum

    Public Enum TipoRechazoAccion
        RechazoTemporal = 1
        RechazoDefinitivo = 2
        Anulacion = 3
    End Enum

    Public Enum TipoBloqueoEtapa
        SinBloqueo = 0
        BloqueoInicio = 1
        BloqueoSalida = 2
    End Enum

    Public Enum ErroresEMail
        Send = 0
        Adjuntos = 1
        SinError = 2
        CreacionObjeto = 3 'Casque en creacion objeto o llamada
    End Enum

    Public Enum MapperModuloMensaje
        PedidoDirecto = 1
    End Enum

    Public Enum TablasIntegracion
        PM = 18
    End Enum

    Public Enum TipoAvisoBloqueo
        Bloqueo = 0
        Aviso = 1
        SinAsignar = -1
    End Enum

    Public Enum EstadoValidacion
        SinEstado = 0
        EnCola = 1
        ProximosParticipantes = 2
        CamposObligatorios = 3
        Precondiciones = 4
        ControlImportes = 5
        Integracion = 6
        SiguienteEtapa = 7
        YaAsignadaAOtroUsuario = 8
        ErrorGeneral = 9
        Completada = 99
    End Enum

    Public Enum CalidadSubtipo
        CalIntegracion = 1
        CalManual = 2
        CalCertificado = 3
        CalNoConformidad = 4
        CalPPM = 5
        CalCargoProveedores = 6
        CalTasaServicio = 7
    End Enum

    Public Enum TipoEmail
        Texto = 0
        HTML = 1
    End Enum

    Public Enum TipoAccesoUNQAS
        EmitirNoConformidades = 1
        ConsultarNoConformidades = 2
        ReabrirNoConformidades = 3
        MantenerObjetivosSuelos = 4
        ConsultarObjetivosSuelos = 5
        AsignarProveedoresUNQA = 6
        ModificarPuntuacionesPanelCalidad = 7
        ConsultarPuntuacionesPanelCalidad = 8
    End Enum

    Public Enum AccionSubasta
        Pausar = 1
        Reiniciar = 2
        Parar = 3
        Extender = 4        
        Arrancar = 5
        Finalizar = 6
        IniciarSesionProve = 100
        FinalizarSesionProve = 101
        Pujar = 102
    End Enum

    Public Enum EstadoSubasta
        NoComenzada = 1
        EnProceso = 2
        Pausada = 3
        Parada = 4
        Finalizada = 5
    End Enum

    Public Enum ModoReinicioSubasta
        Manual = 0
        Automatico = 1
    End Enum

    Public Enum ModoIncTiempoSubasta
        TiempoPausa = 0
        TiempoIndicado = 1
    End Enum

    Public Enum ConceptoImpuesto
        Gasto = 0
        Inversion = 1
        Ambos = 2
    End Enum

    Public Sub New()

    End Sub

End Class

Public Structure Idioma
    Private _Idioma As String

    ''' <summary>
    ''' Constructor de la clase Idioma
    ''' </summary>
    ''' <param name="Idioma">Código de idioma</param>
    ''' <remarks>Comprueba que el código de idioma sea válido antes de asignarlo</remarks>
    Public Sub New(ByVal Idioma As String)
        _Idioma = ComprobarIdioma(Idioma)
        If String.IsNullOrEmpty(_Idioma) Then _
            Throw New Exception("Código de idioma no válido: " & Idioma)
    End Sub

    ''' <summary>
    ''' Función de conversión de tipos del tipo Idioma a tipo String
    ''' </summary>
    ''' <param name="value">Elemento Idioma a convertir</param>
    ''' <returns>Un elemento String con el valor del código de idioma</returns>
    Public Shared Widening Operator CType(ByVal value As Idioma) As String
        Return value._Idioma
    End Operator

    ''' <summary>
    ''' Función de conversión de tipos del tipo String a tipo Idioma
    ''' </summary>
    ''' <param name="value">Elemento String a convertir</param>
    ''' <returns>Un elemento Idioma con el código de idioma especificado si es válido</returns>
    Public Shared Narrowing Operator CType(ByVal value As String) As Idioma
        Return New Idioma(value)
    End Operator

    ''' <summary>
    ''' Sobreescribe la función ToString para devolver el código de idioma
    ''' </summary>
    ''' <returns>El código de idioma</returns>
    Public Overrides Function ToString() As String
        Return _Idioma
    End Function

    ''' <summary>
    ''' Obtiene la referencia cultural del idioma
    ''' </summary>
    ''' <returns>Una cadena con la referencia cultural del idioma</returns>
    Public Function RefCultural() As String
        Return ReferenciaCultural(Me)
    End Function

End Structure
