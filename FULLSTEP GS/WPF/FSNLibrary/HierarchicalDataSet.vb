﻿Imports System
Imports System.Data
Imports System.Web.UI
Imports System.Collections
Imports System.Web.UI.WebControls


''' <summary> 
''' A class that translates a DataSet into IHierarchicalDataSource that can be used to bind Hierarchical data to a TreeView 
''' </summary> 
Public Class HierarchicalDataSet
    Implements IHierarchicalDataSource
    ''' <summary> 
    ''' The DataView reference that the class is pointing to 
    ''' </summary> 
    ReadOnly dataView As DataView

    ''' <summary> 
    ''' The name of the primary key column 
    ''' </summary> 
    ReadOnly idColumnName As String

    ''' <summary> 
    ''' The name of the parent foreign key column 
    ''' </summary> 
    ReadOnly parentIdColumnName As String

    ''' <summary> 
    ''' evaluated once the constructor is called, true if the primary keys are strings 
    ''' </summary> 
    ReadOnly columnIsString As Boolean

    ''' <summary> 
    ''' evaluated once the constructor is called, true if the primary keys are strings 
    ''' </summary> 
    ReadOnly rootParentColumnValue As Object

    ''' <summary> 
    ''' The constructor of the class 
    ''' </summary> 
    ''' <param name="dataSet">The dataset that contains the data, the first table's view will be taken</param> 
    ''' <param name="idColumnName">The Primary key column name</param> 
    ''' <param name="parentIdColumnName">The Parent Primary key column name that identifies the Parent-Child relationship</param> 
    ''' <param name="rootParentColumnValue">By Default, the tree will load from the root, if you want to load a subtree, specify the value of the toplevel record here</param> 
    Public Sub New(ByVal dataSet As DataSet, ByVal idColumnName As String, ByVal parentIdColumnName As String, ByVal rootParentColumnValue As Object)
        dataView = dataSet.Tables(0).DefaultView
        Me.idColumnName = idColumnName
        Me.parentIdColumnName = parentIdColumnName
        Me.rootParentColumnValue = rootParentColumnValue

        If Not dataSet.Tables(0).Columns(idColumnName).DataType.Equals(dataSet.Tables(0).Columns(parentIdColumnName).DataType) Then
            Throw New Exception("The two column names passed should be of the same type")
        End If

        columnIsString = dataSet.Tables(0).Columns(idColumnName).DataType Is GetType(String)
        System.Diagnostics.Debug.Assert(dataSet.Tables(0).Columns(idColumnName).DataType.Equals(dataSet.Tables(0).Columns(parentIdColumnName).DataType), "The Column of the ID and the Column of the ParentID should be of the same type")
    End Sub

    ''' <summary> 
    ''' The constructor of the class 
    ''' </summary> 
    ''' <param name="dataSet">The dataset that contains the data, the first table's view will be taken</param> 
    ''' <param name="idColumnName">The Primary key column name</param> 
    ''' <param name="parentIdColumnName">The Parent Primary key column name that identifies the Parent-Child relationship</param> 
    Public Sub New(ByVal dataSet As DataSet, ByVal idColumnName As String, ByVal parentIdColumnName As String)
        dataView = dataSet.Tables(0).DefaultView
        Me.idColumnName = idColumnName
        Me.parentIdColumnName = parentIdColumnName
        rootParentColumnValue = Nothing

        If Not dataSet.Tables(0).Columns(idColumnName).DataType.Equals(dataSet.Tables(0).Columns(parentIdColumnName).DataType) Then
            Throw New Exception("The two column names passed should be of the same type")
        End If

        columnIsString = dataSet.Tables(0).Columns(idColumnName).DataType Is GetType(String)
        System.Diagnostics.Debug.Assert(dataSet.Tables(0).Columns(idColumnName).DataType.Equals(dataSet.Tables(0).Columns(parentIdColumnName).DataType), "The Column of the ID and the Column of the ParentID should be of the same type")
    End Sub

    ''' <summary> 
    ''' The constructor of the class 
    ''' </summary> 
    ''' <param name="dataView">The dataView that contains the data</param> 
    ''' <param name="idColumnName">The Primary key column name</param> 
    ''' <param name="parentIdColumnName">The Parent Primary key column name that identifies the Parent-Child relationship</param> 
    ''' <param name="rootParentColumnValue">By Default, the tree will load from the root, if you want to load a subtree, specify the value of the toplevel record here</param> 
    Public Sub New(ByVal dataView As DataView, ByVal idColumnName As String, ByVal parentIdColumnName As String, ByVal rootParentColumnValue As Object)
        Me.dataView = dataView
        Me.idColumnName = idColumnName
        Me.parentIdColumnName = parentIdColumnName
        Me.rootParentColumnValue = rootParentColumnValue

        If Not dataView.Table.Columns(idColumnName).DataType.Equals(dataView.Table.Columns(parentIdColumnName).DataType) Then
            Throw New Exception("The two column names passed should be of the same type")
        End If

        columnIsString = dataView.Table.Columns(idColumnName).DataType Is GetType(String)
    End Sub

    ''' <summary> 
    ''' The constructor of the class 
    ''' </summary> 
    ''' <param name="dataView">The dataView that contains the data</param> 
    ''' <param name="idColumnName">The Primary key column name</param> 
    ''' <param name="parentIdColumnName">The Parent Primary key column name that identifies the Parent-Child relationship</param> 
    Public Sub New(ByVal dataView As DataView, ByVal idColumnName As String, ByVal parentIdColumnName As String)
        Me.dataView = dataView
        Me.idColumnName = idColumnName
        Me.parentIdColumnName = parentIdColumnName
        rootParentColumnValue = Nothing

        If Not dataView.Table.Columns(idColumnName).DataType.Equals(dataView.Table.Columns(parentIdColumnName).DataType) Then
            Throw New Exception("The two column names passed should be of the same type")
        End If

        columnIsString = dataView.Table.Columns(idColumnName).DataType Is GetType(String)
    End Sub

    Public Event DataSourceChanged As EventHandler Implements IHierarchicalDataSource.DataSourceChanged
    ' never used here 
    Public Function GetHierarchicalView(ByVal viewPath As String) As HierarchicalDataSourceView Implements IHierarchicalDataSource.GetHierarchicalView
        Return New DataSourceView(Me, viewPath)
    End Function

#Region "supporting methods"
    ''' <summary> 
    ''' Gets the parent row, given a row 
    ''' </summary> 
    ''' <param name="row"></param> 
    ''' <returns></returns> 
    Private Function GetParentRow(ByVal row As DataRowView) As DataRowView
        dataView.RowFilter = GetFilter(idColumnName, row(parentIdColumnName).ToString())
        Dim parentRow As DataRowView = dataView(0)
        dataView.RowFilter = ""
        Return parentRow
    End Function

    ''' <summary> 
    ''' Prepares the filter based on the column names and type 
    ''' </summary> 
    ''' <param name="columnName"></param> 
    ''' <param name="value"></param> 
    ''' <returns></returns> 
    Private Function GetFilter(ByVal columnName As String, ByVal value As String) As String
        If columnIsString Then
            Return [String].Format("[{0}] = '{1}'", columnName, value.Replace("'", "''"))
        Else
            Return [String].Format("[{0}] = {1}", columnName, value)
        End If
    End Function

    Private Function GetChildrenViewPath(ByVal viewPath As String, ByVal row As DataRowView) As String
        Return (viewPath & "\") + row(idColumnName).ToString()
    End Function

    Private Function HasChildren(ByVal row As DataRowView) As Boolean
        dataView.RowFilter = GetFilter(parentIdColumnName, row(idColumnName).ToString())
        Dim bhasChildren As Boolean = dataView.Count > 0
        dataView.RowFilter = ""
        Return bhasChildren
    End Function

    Private Function GetParentViewPath(ByVal viewPath As String) As String
        Return viewPath.Substring(0, viewPath.LastIndexOf("\"))
    End Function
#End Region

#Region "private classes that implement further interfaces"
    Private Class DataSourceView
        Inherits HierarchicalDataSourceView
        ReadOnly hDataSet As HierarchicalDataSet
        ReadOnly viewPath As String

        Public Sub New(ByVal hDataSet As HierarchicalDataSet, ByVal viewPath As String)
            Me.hDataSet = hDataSet
            Me.viewPath = viewPath
        End Sub

        Public Overloads Overrides Function [Select]() As IHierarchicalEnumerable
            Return New HierarchicalEnumerable(hDataSet, viewPath)
        End Function
    End Class

    Private Class HierarchicalEnumerable
        Implements IHierarchicalEnumerable
        ReadOnly hDataSet As HierarchicalDataSet
        ReadOnly viewPath As String

        Public Sub New(ByVal hDataSet As HierarchicalDataSet, ByVal viewPath As String)
            Me.hDataSet = hDataSet
            Me.viewPath = viewPath
        End Sub

        Public Function GetHierarchyData(ByVal enumeratedItem As Object) As IHierarchyData Implements IHierarchicalEnumerable.GetHierarchyData
            Dim row As DataRowView = DirectCast(enumeratedItem, DataRowView)
            Return New HierarchyData(hDataSet, viewPath, row)
        End Function

        Public Function GetEnumerator() As IEnumerator Implements IHierarchicalEnumerable.GetEnumerator
            If viewPath = "" Then
                If hDataSet.rootParentColumnValue IsNot Nothing Then
                    hDataSet.dataView.RowFilter = hDataSet.GetFilter(hDataSet.parentIdColumnName, hDataSet.rootParentColumnValue.ToString())
                Else
                    If hDataSet.columnIsString Then
                        hDataSet.dataView.RowFilter = [String].Format("[{0}] is null or [{0}] = ''", hDataSet.parentIdColumnName)
                    Else
                        hDataSet.dataView.RowFilter = [String].Format("[{0}] is null or [{0}] = 0", hDataSet.parentIdColumnName)
                    End If
                End If
            Else
                Dim lastID As String = viewPath.Substring(viewPath.LastIndexOf("\") + 1)
                hDataSet.dataView.RowFilter = hDataSet.GetFilter(hDataSet.parentIdColumnName, lastID)
            End If

            Dim i As IEnumerator = hDataSet.dataView.ToTable().DefaultView.GetEnumerator()
            hDataSet.dataView.RowFilter = ""

            Return i
        End Function
    End Class

    Private Class HierarchyData
        Implements IHierarchyData
        ReadOnly hDataSet As HierarchicalDataSet
        ReadOnly row As DataRowView
        ReadOnly viewPath As String

        Public Sub New(ByVal hDataSet As HierarchicalDataSet, ByVal viewPath As String, ByVal row As DataRowView)
            Me.hDataSet = hDataSet
            Me.viewPath = viewPath
            Me.row = row
        End Sub

        Public Function GetChildren() As IHierarchicalEnumerable Implements IHierarchyData.GetChildren
            Return New HierarchicalEnumerable(hDataSet, hDataSet.GetChildrenViewPath(viewPath, row))
        End Function

        Public Function GetParent() As IHierarchyData Implements IHierarchyData.GetParent
            Return New HierarchyData(hDataSet, hDataSet.GetParentViewPath(viewPath), hDataSet.GetParentRow(row))
        End Function

        Public ReadOnly Property HasChildren() As Boolean Implements IHierarchyData.HasChildren
            Get
                Return hDataSet.HasChildren(row)
            End Get
        End Property

        Public ReadOnly Property Item() As Object Implements IHierarchyData.Item
            Get
                Return row
            End Get
        End Property

        Public ReadOnly Property Path() As String Implements IHierarchyData.Path

            Get
                Return viewPath
            End Get
        End Property

        Public ReadOnly Property Type() As String Implements IHierarchyData.Type
            Get
                Return GetType(DataRowView).ToString()
            End Get
        End Property
    End Class
#End Region
End Class


