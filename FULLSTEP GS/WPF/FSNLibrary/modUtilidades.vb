﻿Imports VB = Microsoft.VisualBasic
Public Module modUtilidades

#Region " Funciones del EPNotificador pasadas aqui "

    ''' <summary>
    ''' Funcion que devuelve la fecha en el formato pasado como parámetro
    ''' </summary>
    ''' <param name="Fecha">Fecha a transformar</param>
    ''' <param name="datFmt">Formato de fecha a visualizar</param>
    ''' <returns>Una variable de tipo fecha en el formato introducido como parámetro</returns>
    ''' <remarks>
    ''' Llamada desde: EPServer\Notificador\Notificar.vb
    ''' Tiempo máximo: 0 seg</remarks>
    Public Function VisualizacionFecha(ByVal Fecha As Object, ByVal datFmt As Object) As Object
        'Revisado por sgp a 22/12/08 para migracion
        Dim returnFecha As Object
        Dim sUrte As Object
        Dim sMes As Object
        Dim sDia As Object
        'Comentado por no usar-->Dim sepFecha As Object

        'Comentado por no usar-->sepFecha = Mid(datFmt, 3, 1)
        If IsDBNull(Fecha) Then
            VisualizacionFecha = ""
            Exit Function
        End If
        sDia = CStr(VB.Day(Fecha))
        sMes = CStr(Month(Fecha))
        sUrte = CStr(Year(Fecha))


        sDia = Left("00", 2 - Len(sDia)) & sDia
        sMes = Left("00", 2 - Len(sMes)) & sMes

        returnFecha = Replace(datFmt, "dd", sDia)
        returnFecha = Replace(returnFecha, "mm", sMes)
        returnFecha = Replace(returnFecha, "yyyy", sUrte)
        VisualizacionFecha = returnFecha

    End Function

    Public Function null2str(ByVal valor As Object) As String
        null2str = IIf(IsDBNull(valor), "", valor)
    End Function

    Public Function ReplaceV(ByVal s1 As String, ByVal s2 As String, ByVal s3 As Object) As String

        ReplaceV = Replace(s1, s2, null2str(s3))



    End Function

    Public Function VB2HTML(ByVal text As Object) As String

        Dim t As Object
        If IsDBNull(text) Then
            VB2HTML = ""
            Exit Function
        End If
        t = Replace(text, Chr(13) & Chr(10), "<br>")
        t = Replace(t, Chr(10), "<br>")
        t = Replace(t, Chr(13), "<br>")
        VB2HTML = t


    End Function

    ''' <summary>
    ''' Funcion que devuelve la fecha en el formato pasado como parámetro
    ''' </summary>
    ''' <param name="ecFmt">Formato de los decimales</param>
    ''' <param name="Num">Número a cambiar el formato</param>
    ''' <param name="PrecFmt">Precision de los decimales, en dígitos</param>
    ''' <param name="ThouFmt">Formato de miles</param>
    ''' <returns>Una variable de tipo numero en el formato introducido como parámetro</returns>
    ''' <remarks>
    ''' Llamada desde: EPServer\Notificador\Notificar.vb, EPServer\Aprobación.aspx
    ''' Tiempo máximo: 0 seg</remarks>
    Public Function VisualizacionNumero(ByVal Num As Object, ByVal ecFmt As Object, ByVal ThouFmt As Object, ByVal PrecFmt As Object) As Object
        'Revisado por sgp a fecha de 22/12/08 para la migracion a VS2008
        Dim s As Object
        Dim sNum As Object
        Dim ParteEntera As Object
        Dim numeroLocal As Object
        Dim snumeroLocal As Object
        Dim decimalloc As Object
        Dim i As Object
        Dim ParteDecimal As Object
        Dim tmpParteDecimal As Object
        Dim quitarCeros As Object
        Dim newValor As Object

        numeroLocal = 1 / 10
        If IsDBNull(Num) Or IsNothing(Num) Then
            VisualizacionNumero = ""
            Exit Function
        End If

        If Num = 0 Then
            VisualizacionNumero = "0"
            Exit Function
        End If

        snumeroLocal = CStr(numeroLocal)
        If InStr(snumeroLocal, ".") Then
            decimalloc = "."
        Else
            decimalloc = ","
        End If

        Num = System.Math.Round(Num, CType(PrecFmt, Integer))

        If IsDBNull(Num) Or Num = 0 Then
            VisualizacionNumero = ""
            Exit Function
        End If

        ParteEntera = CStr(Fix(Num))
        sNum = CStr(Num)
        tmpParteDecimal = ""

        If InStr(sNum, decimalloc) > 0 Then
            ParteDecimal = Mid(sNum, InStr(sNum, decimalloc) + 1, PrecFmt)
        Else
            ParteDecimal = ""
        End If

        quitarCeros = True

        For i = Len(ParteDecimal) To 1 Step -1
            If Mid(ParteDecimal, i, 1) = "0" And quitarCeros Then
            Else
                tmpParteDecimal = Mid(ParteDecimal, i, 1) & tmpParteDecimal
                quitarCeros = False
            End If
        Next

        ParteDecimal = tmpParteDecimal
        newValor = ""
        s = 0
        For i = Len(ParteEntera) To 1 Step -1
            If s = 3 Then
                newValor = ThouFmt & newValor
                s = 0
            End If
            newValor = Mid(ParteEntera, i, 1) & newValor
            s = s + 1
        Next
        Dim vPrec As Short
        Dim ret As String



        If ParteDecimal <> "" Then
            ret = newValor & ecFmt & ParteDecimal
        Else
            ret = newValor
        End If

        vPrec = PrecFmt
        While ret = "0" And Num <> 0
            vPrec = vPrec + 1
            ret = VisualizacionNumero(Num, ecFmt, ThouFmt, vPrec)

        End While

        VisualizacionNumero = ret
    End Function
#End Region


    Public Function DBNullToSomething(Optional ByVal value As Object = Nothing) As Object
        If IsDBNull(value) Then
            Return Nothing
        Else
            Return value
        End If

    End Function

    Public Function VacioToNothing(ByVal s As Object) As Object
        Dim o As Object = DBNullToSomething(s)
        If o Is Nothing Then Return Nothing
        If o.ToString() = "" Then
            Return Nothing
        Else
            Return o
        End If
    End Function

    Public Function NothingToDBNull(Optional ByVal value As Object = Nothing) As Object
        If IsNothing(value) Then
            Return System.DBNull.Value
        Else
            Return value
        End If
    End Function

    Public Function NothingToStr(ByVal value As String) As String
        If IsNothing(value) Then
            Return String.Empty
        Else
            Return value
        End If
    End Function

    Public Function DBNullToDbl(Optional ByVal value As Object = Nothing) As Double
        If IsDBNull(value) Then
            Return 0
        ElseIf value Is Nothing Then
            Return 0
        Else
            Return CDbl(value)
        End If
    End Function

    Public Function DBNullToDec(Optional ByVal value As Object = Nothing) As Decimal
        If IsDBNull(value) Then
            Return 0
        ElseIf value Is Nothing Then
            Return 0
        Else
            Return CDec(value)
        End If
    End Function

    Public Function GenerateRandomPath() As String
        Dim s As String = ""
        Dim i As Integer
        Dim lowerbound As Integer = 65
        Dim upperbound As Integer = 90
        Randomize()
        For i = 1 To 10
            s = s & Chr(Int((upperbound - lowerbound + 1) * Rnd() + lowerbound))
        Next
        Return s

    End Function

    Public Function igDateToDate(ByVal s As String) As DateTime

        Try
            If s = Nothing Or s = "" Then
                Return Nothing
            End If
            Dim arr As Array = s.Split("-")

            Return DateSerial(arr(0), arr(1), arr(2))


        Catch ex As Exception
            Return Nothing

        End Try

    End Function

    Public Function DateToigDate(ByVal dt As DateTime) As String
        Try
            If dt = Nothing Then
                Return ""
            End If
            '2005-7-14-12-7-14-244
            'anyo-m-di-ho-m-se-mil

            Dim s As String = Year(dt).ToString() + "-" + Month(dt).ToString() + "-" + Day(dt).ToString() + "-" + Hour(dt).ToString() + "-" + Minute(dt).ToString() + "-" + Second(dt).ToString()
            Return s

        Catch ex As Exception

            Return ""
        End Try
    End Function

    Public Function SQLBinaryToBoolean(ByVal var As Object) As Boolean
        If IsDBNull(var) OrElse var = Nothing Then
            SQLBinaryToBoolean = False
        Else
            If var = 1 Then
                SQLBinaryToBoolean = True
            Else
                SQLBinaryToBoolean = False
            End If
        End If
    End Function

    Public Function BooleanToSQLBinary(ByVal b As Object) As Object
        If IsDBNull(b) Then
            BooleanToSQLBinary = "NULL"
        Else
            If b Then
                BooleanToSQLBinary = 1
            Else
                BooleanToSQLBinary = 0
            End If
        End If

    End Function

    Public Function BooleanToSQLBinaryDBNull(ByVal b As Object) As Object
        If b Is Nothing Then
            BooleanToSQLBinaryDBNull = System.DBNull.Value
        ElseIf IsDBNull(b) Then
            BooleanToSQLBinaryDBNull = System.DBNull.Value
        Else
            If b Then
                BooleanToSQLBinaryDBNull = 1
            Else
                BooleanToSQLBinaryDBNull = 0
            End If
        End If

    End Function


    ''' <summary>
    ''' Devuelve un objeto de tipo fecha relativa a una fecha que puede ser dias, semanas, meses o años
    ''' </summary>
    ''' <param name="iTipoFecha">Tipo de fecha a devolver</param>
    ''' <param name="dtFechaFija">Fecha relativa a la que devolver la fecha</param>
    ''' <returns>Devuelve una fecha relativa a la pasada como parámetro, dependiendo del parámetro iTipoFecha</returns>
    ''' <remarks>
    ''' Llamada desde:PM\Solicitud y PM\Instancia </remarks>
    Public Function DevolverFechaRelativaA(ByVal iTipoFecha As TiposDeDatos.TipoFecha, ByVal dtFechaFija As Date) As Object

        If Not dtFechaFija = Nothing Then
            Return dtFechaFija
        End If
        Select Case iTipoFecha
            Case TiposDeDatos.TipoFecha.FechaAlta
                Return Now
            Case TiposDeDatos.TipoFecha.UnDiaDespues
                Return DateAdd(DateInterval.Day, 1, Today)
            Case TiposDeDatos.TipoFecha.DosDiasDespues
                Return DateAdd(DateInterval.Day, 2, Today)
            Case TiposDeDatos.TipoFecha.TresDiasDespues
                Return DateAdd(DateInterval.Day, 3, Today)
            Case TiposDeDatos.TipoFecha.CuatroDiasDespues
                Return DateAdd(DateInterval.Day, 4, Today)
            Case TiposDeDatos.TipoFecha.CincoDiasDespues
                Return DateAdd(DateInterval.Day, 5, Today)
            Case TiposDeDatos.TipoFecha.SeisDiasDespues
                Return DateAdd(DateInterval.Day, 6, Today)
            Case TiposDeDatos.TipoFecha.UnaSemanaDespues
                Return DateAdd(DateInterval.Day, 7, Today)
            Case TiposDeDatos.TipoFecha.DosSemanaDespues
                Return DateAdd(DateInterval.Day, 14, Today)
            Case TiposDeDatos.TipoFecha.TresSemanaDespues
                Return DateAdd(DateInterval.Day, 21, Today)
            Case TiposDeDatos.TipoFecha.UnMesDespues
                Return DateAdd(DateInterval.Month, 1, Today)
            Case TiposDeDatos.TipoFecha.DosMesDespues
                Return DateAdd(DateInterval.Month, 2, Today)
            Case TiposDeDatos.TipoFecha.TresMesDespues
                Return DateAdd(DateInterval.Month, 3, Today)
            Case TiposDeDatos.TipoFecha.CuatroMesDespues
                Return DateAdd(DateInterval.Month, 4, Today)
            Case TiposDeDatos.TipoFecha.CincoMesDespues
                Return DateAdd(DateInterval.Month, 5, Today)
            Case TiposDeDatos.TipoFecha.SeisMesDespues
                Return DateAdd(DateInterval.Month, 6, Today)
            Case TiposDeDatos.TipoFecha.UnAnyoDespues
                Return DateAdd(DateInterval.Year, 1, Today)
            Case Else
                Return DBNull.Value
        End Select

    End Function

    Public Function StrToSQLNULL(ByVal StrThatCanBeEmpty As Object) As Object
        If IsDBNull(StrThatCanBeEmpty) Then
            StrToSQLNULL = "NULL"
        ElseIf StrThatCanBeEmpty = "" Then
            StrToSQLNULL = "NULL"
        ElseIf IsNothing(StrThatCanBeEmpty) Then
            StrToSQLNULL = "NULL"
        Else
            StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
        End If
    End Function
    Public Function UnicodeStrToSQLNULL(ByVal StrThatCanBeEmpty As Object) As Object
        If IsDBNull(StrThatCanBeEmpty) Then
            UnicodeStrToSQLNULL = "NULL"
        ElseIf StrThatCanBeEmpty = "" Then
            UnicodeStrToSQLNULL = "NULL"
        ElseIf IsNothing(StrThatCanBeEmpty) Then
            UnicodeStrToSQLNULL = "NULL"
        Else
            UnicodeStrToSQLNULL = "N'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
        End If
    End Function

    Public Function DblQuote(ByVal StrToDblQuote As String) As String

        DblQuote = Replace(StrToDblQuote, "'", "''")

    End Function

    Public Function DateToSQLDate(ByVal DateThatCanBeEmpty As Object) As Object

        Try
            If IsDBNull(DateThatCanBeEmpty) Then
                Return "NULL"
            Else
                If IsDate(DateThatCanBeEmpty) Then
                    Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy") & "',110)"
                Else
                    If DateThatCanBeEmpty = "" Then
                        Return "NULL"
                    Else
                        Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy") & "',110)"
                    End If
                End If

            End If

        Catch ex As Exception

            Return "NULL"

        End Try
    End Function

    Public Function DateToSQLDateDBNull(ByVal DateThatCanBeEmpty As Object) As Object        
        If DateThatCanBeEmpty Is Nothing Then
            Return DBNull.Value
        Else
            Return DateThatCanBeEmpty
        End If
    End Function

    Public Function DoubleToSQLDoubleDBNull(ByVal DoubleThatCanBeEmpty As Object) As Object
        If DoubleThatCanBeEmpty Is Nothing Then
            Return DBNull.Value
        Else
            Return DoubleThatCanBeEmpty
        End If
    End Function

    Public Function IntToSQLIntegerDBNull(ByVal IntThatCanBeEmpty As Object) As Object
        If IntThatCanBeEmpty Is Nothing Then
            Return DBNull.Value
        Else
            Return IntThatCanBeEmpty
        End If
    End Function

    Public Function DateToSQLTimeDate(ByVal DateThatCanBeEmpty) As Object

        Try
            If IsDBNull(DateThatCanBeEmpty) Then
                Return "NULL"
            Else
                If IsDate(DateThatCanBeEmpty) Then
                    Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy HH:mm:ss") & "',110)"
                Else
                    If DateThatCanBeEmpty = "" Then
                        Return "NULL"
                    Else
                        Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy HH:mm:ss") & "',110)"
                    End If
                End If

            End If

        Catch ex As Exception

            Return "NULL"

        End Try

    End Function

    Public Function DateToBDDate(ByVal DateValue As Date) As String
        'Ponemos la fecha en formato dd/mm/yyyy pq en la BD se trata asi
        Dim DateFormat As System.Globalization.DateTimeFormatInfo
        Dim sCurrentCulture As String

        Try
            If IsDBNull(DateValue) Then
                DateToBDDate = "NULL"
            Else
                sCurrentCulture = System.Globalization.CultureInfo.CurrentCulture.Name
                DateFormat = New System.Globalization.CultureInfo(sCurrentCulture, False).DateTimeFormat
                DateFormat.ShortDatePattern = "dd/MM/yyyy"
                DateToBDDate = FormatDate(DateValue, DateFormat)
            End If

        Catch ex As Exception

            Return "NULL"

        End Try
    End Function

    Public Function DblToSQLFloat(ByVal DblToConvert As Object) As String

        Dim StrToConvert As String

        Try

            If DblToConvert Is Nothing Then
                DblToSQLFloat = "NULL"
                Exit Function
            End If
            If IsDBNull(DblToConvert) Then
                DblToSQLFloat = "NULL "
                Exit Function
            End If


            If DblToConvert = "" Then
                DblToSQLFloat = "NULL"
                Exit Function
            End If
            StrToConvert = CStr(DblToConvert)
            StrToConvert = Replace(StrToConvert, ",", ".")
            DblToSQLFloat = StrToConvert
        Catch ex As Exception
            If IsNumeric(DblToConvert) Then
                StrToConvert = CStr(DblToConvert)
                StrToConvert = Replace(StrToConvert, ",", ".")
                Return StrToConvert
            Else
                Return "NULL"
            End If

        End Try



    End Function

    Public Function DblToSQLFloatDBNull(ByVal DblToConvert As Object) As Object

        Dim StrToConvert As String

        Try

            If DblToConvert Is Nothing Then
                DblToSQLFloatDBNull = System.DBNull.Value
                Exit Function
            End If
            If IsDBNull(DblToConvert) Then
                DblToSQLFloatDBNull = System.DBNull.Value
                Exit Function
            End If


            If DblToConvert = "" Then
                DblToSQLFloatDBNull = System.DBNull.Value
                Exit Function
            End If
            StrToConvert = CStr(DblToConvert)
            StrToConvert = Replace(StrToConvert, ",", ".")
            DblToSQLFloatDBNull = StrToConvert
        Catch ex As Exception
            If IsNumeric(DblToConvert) Then
                StrToConvert = CStr(DblToConvert)
                StrToConvert = Replace(StrToConvert, ",", ".")
                Return StrToConvert
            Else
                Return System.DBNull.Value
            End If

        End Try



    End Function

    Public Function IsTime(ByVal DataAsked) As Boolean
        Dim s As String

        s = CStr(DataAsked)

        If IsDate("01/01/1990 " & DataAsked) = False Then
            IsTime = False
            Exit Function
        End If

        IsTime = True

    End Function

    Public Function DBNullToStr(Optional ByVal value As Object = Nothing) As String
        If IsDBNull(value) Then
            Return ""
        Else
            Return value
        End If
    End Function

    Public Function strToDBNull(Optional ByVal value As Object = Nothing) As Object
        If IsNothing(value) Then
            Return System.DBNull.Value
        ElseIf value = "" Then
            Return System.DBNull.Value
        Else
            Return value
        End If
    End Function

    ''' <summary>
    ''' Crea la ruta completa al path proporcionado.
    ''' </summary>
    ''' <param name="sPath">String con el path a crear</param>
    ''' <remarks></remarks>
    Public Sub CrearArbol(ByVal sPath As String)
        If Not IO.Directory.Exists(sPath) Then _
            IO.Directory.CreateDirectory(sPath)
    End Sub

    ''' <summary>
    ''' Reemplaza los caracteres especiales que puedan dar problemas en nombres de archivo por guiones bajos.
    ''' </summary>
    ''' <param name="sFileName">Nombre del archivo.</param>
    ''' <returns>Un string con el nombre del archivo con los caracteres especiales reemplazados.</returns>
    ''' <remarks></remarks>
    Public Function ValidFilename(ByVal sFileName As String) As String
        Dim s As String = sFileName

        s = Replace(s, "\", "_")
        s = Replace(s, "/", "_")
        s = Replace(s, ":", "_")
        s = Replace(s, "*", "_")
        s = Replace(s, "?", "_")
        s = Replace(s, """", "_")
        s = Replace(s, ">", "_")
        s = Replace(s, "<", "_")
        s = Replace(s, "|", "_")
        s = Replace(s, "@", "_")
        s = Replace(s, "#", "_")

        Return s

    End Function

    ''' <summary>
    ''' Devuelve un número a partir de una cadena.
    ''' </summary>
    ''' <param name="value">String con el valor a convertir.</param>
    ''' <returns>Double con el valor obtenido al convertir el String.</returns>
    Public Function Numero(ByVal value As String) As Double
        Return Numero(value, "", "")
    End Function

    ''' <summary>
    ''' Devuelve un número a partir de una cadena.
    ''' </summary>
    ''' <param name="value">String con el valor a convertir.</param>
    ''' <param name="sDecimalSeparator">Caracter que se utiliza en la cadena como separador decimal.</param>
    ''' <returns>Double con el valor obtenido al convertir el String.</returns>
    Public Function Numero(ByVal value As String, ByVal sDecimalSeparator As String) As Double
        Return Numero(value, sDecimalSeparator, "")
    End Function

    ''' <summary>
    ''' Devuelve un número a partir de una cadena.
    ''' </summary>
    ''' <param name="value">String con el valor a convertir.</param>
    ''' <param name="sDecimalSeparator">Caracter que se utiliza en la cadena como separador decimal.</param>
    ''' <param name="sThousanSeparator">Caracter que se utiliza en la cadena como</param>
    ''' <returns>Double con el valor obtenido al convertir el String.</returns>
    Public Function Numero(ByVal value As String, ByVal sDecimalSeparator As String, ByVal sThousanSeparator As String) As Double
        If value = Nothing Then
            Return Nothing
        Else
            If sDecimalSeparator <> "" Then
                Try
                    value = value.Replace(sThousanSeparator, "")
                Catch
                End Try

                value = value.Replace(sDecimalSeparator, System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)

                Try
                    Return CDbl(value)
                Catch
                    Return Nothing
                End Try

            Else
                If IsNumeric(value) Then
                    Return CDbl(value)
                Else
                    Return Nothing
                End If
            End If
        End If
    End Function

    ''' <summary>
    ''' Devuelve un string con el numero que recibe formateado con el formato que recibe
    ''' </summary>
    ''' <param name="dNumber">Numero a formatear</param>
    ''' <param name="oNumberFormat">formato</param>
    ''' <returns>String con el numero formateado</returns>
    ''' <remarks></remarks>
    Public Function FormatNumber(ByVal dNumber As Double, ByVal oNumberFormat As System.Globalization.NumberFormatInfo) As String
        Return dNumber.ToString("N", oNumberFormat)
    End Function
    ''' <summary>
    ''' Devuelve un string con la fecha pasada en dDate formateada según oDateFormat, por defecto aplica formato d:01/01/1900
    ''' </summary>
    ''' <param name="dDate">fecha a formatear</param>
    ''' <param name="oDateFormat">IFormatProvider</param>
    ''' <param name="sFormat">Format: d :08/17/2000; D :Thursday, August 17, 2000; f :Thursday, August 17, 2000 16:32; F :Thursday, August 17, 2000 16:32:32; g :08/17/2000 16:32;G :08/17/2000 16:32:32...
    '''</param>
    ''' <returns>String con la fecha</returns>
    ''' <remarks></remarks>
    Public Function FormatDate(ByVal dDate As Date, ByVal oDateFormat As System.Globalization.DateTimeFormatInfo, Optional ByVal sFormat As String = "d") As String
        Return dDate.ToString(sFormat, oDateFormat)
    End Function

    ''' <summary>
    ''' Convierte un texto para utilizarlo como cadena en javascript
    ''' </summary>
    ''' <param name="text">Texto a convertir</param>
    ''' <returns>Un String con el texto formateado para poderlo utilizar como cadena en javascript</returns>
    Function JSText(ByVal text As String) As String
        Dim t As String
        If text = Nothing Then
            Return ""
            Exit Function
        End If
        t = Replace(text, "'", "&#39;")
        t = Replace(t, """", "&#34;")
        t = Replace(t, Chr(13) & Chr(10), "\n")
        t = Replace(t, Chr(10), "\n")
        t = Replace(t, Chr(13), "\n")

        Return t
    End Function

    ''' <summary>
    ''' Convierte un texto javascript para utilizarlo como cadena 
    ''' </summary>
    ''' <param name="text">Texto a convertir</param>      
    ''' <returns>Un String sin ningun formato javascript</returns>
    ''' <remarks>Llamada desde:NoConfEnProceso.aspx; Tiempo máximo:0</remarks>
    Function DesHazJSText(ByVal text As String) As String
        Dim t As String
        If text = Nothing Then
            Return ""
            Exit Function
        End If
        t = Replace(text, "&#39;", "'")
        t = Replace(t, "&#34;", """")
        t = Replace(t, "\n", Chr(13) & Chr(10))

        'El JStext de Entry/formatos.js funciona en hexadecimal para evitar usar & en querystring
        t = Replace(text, "/X27;", "'")
        t = Replace(t, "/X22;", """")

        Return t
    End Function

    ''' <summary>
    ''' Reemplaza el separador decimal "," por "."
    ''' </summary>
    ''' <param name="Num">String con el número a formatear</param>
    ''' <returns>Un String con el número con separador decimal "."</returns>
    Function JSNum(ByVal Num As String) As String
        Dim t As String
        Try
            If IsDBNull(Num) Then
                Return "null"
            End If
            t = Replace(Num.ToString(), ",", ".")
            Return t
        Catch ex As Exception

            Return "null"
        End Try
    End Function

    ''' <summary>
    ''' Función que se encarga de dar formato al texto escrito por el usuario en la caja de texto destinada a la busqueda e artículos, encargada de ignorar acentos, mayúsculas y minúsculas...
    ''' </summary>
    ''' <param name="Cadena">Texto a buscar</param>
    ''' <param name="IgnorarAcentos">Valor booleano que indica si se deben ignorar las diferencias de acentos, diéresis, etc.</param>
    ''' <returns>Cadena para operador LIKE</returns>
    Function strToSQLLIKE(ByVal Cadena As String, Optional ByVal IgnorarAcentos As Boolean = False) As String
        Cadena = Replace(Cadena, "[", "[[]")
        Cadena = Replace(Cadena, "]", "[]]")
        Cadena = Replace(Cadena, "_", "[_]")
        Cadena = Replace(Cadena, "%", "[%]")
        Cadena = UCase(Cadena)
        If IgnorarAcentos Then
            Cadena = Replace(Cadena, "A", "[AÁÀÄÂ]")
            Cadena = Replace(Cadena, "E", "[EÉÈËÊ]")
            Cadena = Replace(Cadena, "I", "[IÍÌÏÎ]")
            Cadena = Replace(Cadena, "O", "[OÓÒÖÔ]")
            Cadena = Replace(Cadena, "U", "[UÚÙÜÛ]")
        End If
        Return Cadena
    End Function

    ''' <summary>
    ''' Evita que se produzca un error por causa de un bug del Framework.
    ''' A causa del bug al añadir un atributo de estilo con índice 20 se genera una excepción IndexOutOfRangeException.
    ''' </summary>
    ''' <param name="writer">Objeto HtmlTextWriter</param>
    ''' <param name="Atributo">Atributo a escribir</param>
    ''' <param name="Valor">Valor del atributo</param>
    Public Sub AddAtributoEstilo(ByVal writer As Web.UI.HtmlTextWriter, ByVal Atributo As Web.UI.HtmlTextWriterStyle, ByVal Valor As String)
        Try
            writer.AddStyleAttribute(Atributo, Valor)
        Catch ex As IndexOutOfRangeException
            Dim count As Reflection.FieldInfo = writer.GetType().GetField("_styleCount", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic)
            Dim i As Integer = count.GetValue(writer)
            count.SetValue(writer, i + 1)
            writer.AddStyleAttribute(Atributo, Valor)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Evita que se produzca un error por causa de un bug del Framework.
    ''' A causa del bug al añadir un atributo de estilo con índice 20 se genera una excepción IndexOutOfRangeException.
    ''' </summary>
    ''' <param name="writer">Objeto HtmlTextWriter</param>
    ''' <param name="Atributo">Atributo a escribir</param>
    ''' <param name="Valor">Valor del atributo</param>
    Public Sub AddAtributoEstilo(ByVal writer As Web.UI.HtmlTextWriter, ByVal Atributo As String, ByVal Valor As String)
        Try
            writer.AddStyleAttribute(Atributo, Valor)
        Catch ex As IndexOutOfRangeException
            Dim count As Reflection.FieldInfo = writer.GetType().GetField("_styleCount", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic)
            Dim i As Integer = count.GetValue(writer)
            count.SetValue(writer, i + 1)
            writer.AddStyleAttribute(Atributo, Valor)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Comprueba que la cadena recibida es un código de idioma válido
    ''' </summary>
    ''' <param name="Idioma">Código de idioma</param>
    ''' <returns>El código de idioma si es válido, sino una cadena vacía</returns>
    ''' <remarks>Debe utilizarse la función antes de construir consultas SQL dinámicamente 
    ''' utilizando el código de idioma para generar los nombres de los campos</remarks>
    Public Function ComprobarIdioma(ByVal Idioma As String) As String
        Select Case Idioma
            Case "SPA", "ENG", "GER"
                Return Idioma
            Case Else
                Return String.Empty
        End Select
    End Function

    ''' <summary>
    ''' Optiene la referencia cultural del idioma especificado
    ''' </summary>
    ''' <param name="Idioma">Variable de tipo Idioma</param>
    ''' <returns>Referencia cultural del idioma</returns>
    Public Function ReferenciaCultural(ByVal Idioma As FSNLibrary.Idioma) As String
        Select Case Idioma.ToString()
            Case "SPA"
                Return "es-ES_tradnl"
            Case "ENG"
                Return "en-US"
            Case "GER"
                Return "de-DE"
            Case Else
                Return Idioma.ToString()
        End Select
    End Function

End Module
