﻿Option Explicit On
Option Strict On

Imports System.Runtime.InteropServices

Public Class Encrypter
    Public Enum TipoDeUsuario
        Administrador = 1
        Persona = 2

    End Enum
    Private Const ClaveSQLtod As String = "aldjñ"

    Private Const ClaveADMpar As String = "pruiop"
    Private Const ClaveADMimp As String = "ljkdag"

    Private Const ClaveUSUpar As String = "agkag´"
    Private Const ClaveUSUimp As String = "h+hlL_"

    ''' <summary>
    ''' Se utiliza para desencriptar la password que del fichero MailPwd.txt que sirve para enviar correos autenticados.
    ''' Usa como clave "ClaveSQLtod"
    ''' </summary>
    ''' <param name="DatoUsu">"PWD"</param>
    ''' <param name="DatoPwd">Dato a encriptar/desencriptar</param>
    ''' <param name="Encriptando">True encripta/ False desencripta</param>
    ''' <param name="Fecha">Fecha para encriptar/desencriptar</param>
    ''' <returns>La password desencriptada</returns>
    ''' <remarks>Llamada desde: GSNotificador\Root.vb\New</remarks>
    Public Shared Function EncriptarSQLtod(ByVal DatoUsu As String, ByVal DatoPwd As String, ByVal Encriptando As Boolean, Optional ByVal Fecha As Date = #3/5/1974#) As String
        If DatoPwd = "" Then Return ""

        Dim Crypt As New FSNCrypt.FSNAES

        If Encriptando Then
            EncriptarSQLtod = Crypt.EncryptStringToString(Fecha, ClaveSQLtod, DatoPwd)
        Else
            EncriptarSQLtod = Crypt.DecryptStringFromString(Fecha, ClaveSQLtod, DatoPwd)
        End If

        Crypt = Nothing
    End Function

    ''' <summary>
    ''' Encripta/desencripta datos
    ''' </summary>
    ''' <param name="Data">Dato a encriptar/desencriptar</param>
    ''' <param name="Usu">Persona q encripta/desencripta</param>
    ''' <param name="Encrypting">True encripta/ False desencripta</param>
    ''' <param name="UserType">Tipo de usuario q encripta/desencript</param>
    ''' <param name="Fecha">Fecha para encriptar/desencriptar</param>
    ''' <returns>Datos Encriptados/desencriptados</returns>
    ''' <remarks>Llamada desde: GSDatabaseServer/DBServerBase.vb/New    GSDatabaseServer/Root.vb/New    GSDatabaseServer/Root.vb/Login  
    ''' GSDatabaseServer/Root.vb/LoginHash GSNotificador\Root.vb\New
    ''' ; Tiempo maximo: 0,1sg</remarks>
    Public Shared Function Encrypt(ByVal Data As String, Optional ByVal Usu As String = "", Optional ByVal Encrypting As Boolean = True, Optional ByVal UserType As TipoDeUsuario = TipoDeUsuario.Persona, Optional ByVal Fecha As Date = #3/5/1974#) As String
        Dim Crypt As New FSNCrypt.FSNAES
        Dim diacrypt As Integer
        Dim sKeyString As String

        diacrypt = Fecha.Day

        If diacrypt Mod 2 = 0 Then
            If UserType = TipoDeUsuario.Administrador Then
                sKeyString = ClaveADMpar
            Else
                sKeyString = ClaveUSUpar
            End If
        Else
            If UserType = TipoDeUsuario.Administrador Then
                sKeyString = ClaveADMimp
            Else
                sKeyString = ClaveUSUimp
            End If
        End If

        If Encrypting Then
            Encrypt = Crypt.EncryptStringToString(Fecha, Usu & sKeyString, Data)
        Else
            Encrypt = Crypt.DecryptStringFromString(Fecha, Usu & sKeyString, Data)
        End If

        Crypt = Nothing
    End Function

End Class
