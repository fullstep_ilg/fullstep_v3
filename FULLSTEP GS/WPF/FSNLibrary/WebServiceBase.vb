﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Web
Imports System.IO
Imports System.Reflection
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.UI
Imports System.Web.Script.Services

Public Class WebServiceBase
    Inherits WebService
    Implements IHttpHandlerFactory

    Private Function GetServiceType(ByVal TypeName As String) As Type
        For Each loadedAssembly As Assembly In AppDomain.CurrentDomain.GetAssemblies()
            Dim ClassType As Type = loadedAssembly.GetType(TypeName)
            If ClassType IsNot Nothing Then
                Return ClassType
            End If
        Next
        Return Nothing
    End Function

    Private _ajaxAssembly As Assembly = Nothing
    Private ReadOnly Property AjaxAssembly() As Assembly
        Get
            If _ajaxAssembly Is Nothing Then _
                _ajaxAssembly = GetType(GenerateScriptTypeAttribute).Assembly
            Return _ajaxAssembly
        End Get
    End Property

    Private UsedHandlerFactory As IHttpHandlerFactory

    Private Shared Function GetAssemblyModifiedTime(ByVal assembly As Assembly)
        Dim lastWriteTime As DateTime = File.GetLastWriteTime(New Uri(assembly.GetName().CodeBase).LocalPath)
        Return New DateTime(lastWriteTime.Year, lastWriteTime.Month, lastWriteTime.Day, lastWriteTime.Hour, lastWriteTime.Minute, 0)
    End Function

    Private Class JavascriptProxyHandler
        Implements IHttpHandler

        Private Javascript As String = ""

        Public Sub New(ByVal _javascript As String)
            Javascript = _javascript
        End Sub

        Public ReadOnly Property IsReusable() As Boolean Implements System.Web.IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property

        Public Sub ProcessRequest(ByVal context As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
            context.Response.ContentType = "application/x-javascript"
            context.Response.Write(Me.Javascript)
        End Sub

    End Class

    Public Function GetHandler(ByVal context As System.Web.HttpContext, ByVal requestType As String, ByVal url As String, ByVal pathTranslated As String) As System.Web.IHttpHandler Implements System.Web.IHttpHandlerFactory.GetHandler
        Dim HttpHandler As IHttpHandler = Nothing
        Try
            Dim WebServiceType As Type = Me.GetType()
            If WebServiceType Is Nothing Then
                Dim ScriptHandlerFactory As IHttpHandlerFactory = System.Activator.CreateInstance(AjaxAssembly.GetType("System.Web.Script.Services.ScriptHandlerFactory"))
                UsedHandlerFactory = ScriptHandlerFactory
                Return ScriptHandlerFactory.GetHandler(context, requestType, url, pathTranslated)
            End If

            Dim JavascriptHandlerFactory = System.Activator.CreateInstance(AjaxAssembly.GetType("System.Web.Script.Services.RestHandlerFactory"))
            Dim IsScriptRequestMethod As System.Reflection.MethodInfo = JavascriptHandlerFactory.GetType().GetMethod("IsRestRequest", BindingFlags.Static Or BindingFlags.NonPublic)
            If CType(IsScriptRequestMethod.Invoke(Nothing, New Object() {context}), Boolean) Then
                UsedHandlerFactory = JavascriptHandlerFactory
                Dim IsJavascriptDebug As Boolean = String.Equals(context.Request.PathInfo, "/jsdebug", StringComparison.OrdinalIgnoreCase)
                Dim IsJavascript = String.Equals(context.Request.PathInfo, "/js", StringComparison.OrdinalIgnoreCase)
                If IsJavascript Or IsJavascriptDebug Then
                    Dim WebServiceDataConstructor As ConstructorInfo = AjaxAssembly.GetType("System.Web.Script.Services.WebServiceData").GetConstructor(BindingFlags.NonPublic Or BindingFlags.Instance, Nothing, New Type() {GetType(Type), GetType(Boolean)}, Nothing)
                    Dim WebServiceClientProxyGeneratorConstructor As ConstructorInfo = AjaxAssembly.GetType("System.Web.Script.Services.WebServiceClientProxyGenerator").GetConstructor(BindingFlags.NonPublic Or BindingFlags.Instance, Nothing, New Type() {GetType(String), GetType(Boolean)}, Nothing)
                    Dim GetClientProxyScriptMethod As MethodInfo = AjaxAssembly.GetType("System.Web.Script.Services.ClientProxyGenerator").GetMethod("GetClientProxyScript", BindingFlags.NonPublic Or BindingFlags.Instance, Nothing, New Type() {AjaxAssembly.GetType("System.Web.Script.Services.WebServiceData")}, Nothing)
                    Dim Javascript As String = GetClientProxyScriptMethod.Invoke(WebServiceClientProxyGeneratorConstructor.Invoke(New Object() {url, IsJavascriptDebug}), New Object() {WebServiceDataConstructor.Invoke(New Object() {WebServiceType, False})})

                    Dim AssemblyModifiedDate As DateTime = GetAssemblyModifiedTime(WebServiceType.Assembly)
                    Dim s As String = context.Request.Headers("If-Modified-Since")
                    Dim TempDate As DateTime
                    If String.IsNullOrEmpty(s) And DateTime.TryParse(s, TempDate) And TempDate >= AssemblyModifiedDate Then
                        context.Response.StatusCode = &H130
                        Return Nothing
                    End If
                    If Not IsJavascriptDebug And AssemblyModifiedDate.ToUniversalTime() < DateTime.UtcNow Then
                        Dim cache As HttpCachePolicy = context.Response.Cache
                        cache.SetCacheability(HttpCacheability.Public)
                        cache.SetLastModified(AssemblyModifiedDate)
                    End If

                    HttpHandler = New JavascriptProxyHandler(Javascript)
                    Return HttpHandler

                Else
                    Dim JavascriptHandler As IHttpHandler = System.Activator.CreateInstance(AjaxAssembly.GetType("System.Web.Script.Services.RestHandler"))
                    Dim WebServiceDataConstructor As ConstructorInfo = AjaxAssembly.GetType("System.Web.Script.Services.WebServiceData").GetConstructor(BindingFlags.NonPublic Or BindingFlags.Instance, Nothing, New Type() {GetType(Type), GetType(Boolean)}, Nothing)
                    Dim CreateHandlerMethod As MethodInfo = JavascriptHandler.GetType().GetMethod("CreateHandler", BindingFlags.NonPublic Or BindingFlags.Static, Nothing, New Type() {AjaxAssembly.GetType("System.Web.Script.Services.WebServiceData"), GetType(String)}, Nothing)
                    HttpHandler = CreateHandlerMethod.Invoke(JavascriptHandler, New Object() {WebServiceDataConstructor.Invoke(New Object() {WebServiceType, False}), context.Request.PathInfo.Substring(1)})
                End If
                Return HttpHandler
            Else
                Dim WebServiceHandlerFactory As IHttpHandlerFactory = New WebServiceHandlerFactory()
                UsedHandlerFactory = WebServiceHandlerFactory
                Dim CoreGetHandlerMethod = UsedHandlerFactory.GetType().GetMethod("CoreGetHandler", BindingFlags.NonPublic Or BindingFlags.Instance)
                HttpHandler = CoreGetHandlerMethod.Invoke(UsedHandlerFactory, New Object() {WebServiceType, context, context.Request, context.Response})
                Return HttpHandler
            End If
        Catch ex As TargetInvocationException
            Throw ex.InnerException
        End Try
    End Function

    Public Sub ReleaseHandler(ByVal handler As System.Web.IHttpHandler) Implements System.Web.IHttpHandlerFactory.ReleaseHandler
        If UsedHandlerFactory IsNot Nothing Then UsedHandlerFactory.ReleaseHandler(handler)
    End Sub

End Class