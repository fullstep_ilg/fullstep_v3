﻿Partial Public Class TiposDeDatos
    Public Enum TipoDeUsuario
        Administrador = 1
        Comprador = 2
        persona = 3
        Proveedor = 4
        '    UsuarioExterno = 5
    End Enum

    Public Structure ParametrosIntegracion
        Dim gaExportar() As Boolean
        Dim gaSoloImportar() As Boolean
        Dim gbImportarPedAprov As Boolean
    End Structure

    Public Enum TipoInstWeb
        SinWeb = 0
        conweb = 1
        ConPortal = 2
    End Enum

    Public Enum SentidoIntegracion
        salida = 1
        Entrada = 2
        EntradaSalida = 3
    End Enum

    Public Enum EntidadIntegracion
        Mon = 1
        Pai = 2
        Provi = 3
        Pag = 4
        Dest = 5
        Uni = 6
        art4 = 7
        Prove = 8
        Adj = 9
        Recibo = 10
        con = 101
        PED_Aprov = 11
        Rec_Aprov = 12
        PED_directo = 13
        Rec_Directo = 14
        Materiales = 15
        PresArt = 16

        SolicitudPM = 18

        Presupuestos1 = 19
        Presupuestos2 = 20
        Presupuestos3 = 21
        Presupuestos4 = 22
        Proceso = 23
        TablasExternas = 24
        ViaPag = 25

        PPM = 26
        TasasServicio = 27
        CargosProveedor = 28

        UnidadesOrganizativas = 29
        Usuarios = 30

        Activos = 31
        Facturas = 32
        Pagos = 33
        PRES5 = 34
        CentroCoste = 35
        Empresa = 36
    End Enum


    Public Enum AccionesDeSeguridad

        ' *********************************************************
        ' *                 MANTENIMIENTO                         *
        ' *********************************************************

        '****************** Monedas **********************
        MONConsultar = 10100
        MONModificar = 10101
        MONActualizarCambio = 10102
        MONModificarCodigo = 10103

        '****************** Formas de Pago **********************
        PAGConsultar = 17200
        PAGModificar = 17201
        PAGModificarCodigo = 17202

        '****************** Vías de Pago **********************
        VIAPAGConsultar = 17400
        VIAPAGModificar = 17401
        VIAPAGModificarCodigo = 17402

        ' ****************** Paises *********************

        PAIConsultar = 10200
        PAIModificar = 10201
        PAIModificarCodigo = 10202

        ' ****************** Provincias *********************

        PROVIConsultar = 10300
        PROVIModificar = 10301

        ' ****************** Tipos de impuestos *********************
        TipImpConsultar = 10400
        TipImpModificar = 10401
        TipImpModifAsig = 10402
        TipImpRestMatUsu = 10403

        ' ****************** Unidades *********************

        UNIConsultar = 10500
        UNIModificar = 10501
        UNIModificarCodigo = 10502

        ' ****************** Organigrama *********************

        ORGConsultar = 10600
        ORGModificar = 10601
        ORGRestUO = 10602
        ORGRestDep = 10603
        ORGConsultarDest = 10604 'consulta de destinos en el Organigrama
        ORGModificarDest = 10605 'modificación de destinos en el Organigrama
        ORGAsignarDest = 10606 'asignación de destinos a UONs en el Organigrama
        ORGGestionarCC = 10607
        ORGConsultarCC = 10608
        ORGModificarCodigo = 10609
        ORGDESTModificarCodigo = 10610

        ' ****************** Estructura de materiales *********************

        MATConsultar = 10700
        MATModificar = 10701
        MATArtiModificar = 10702
        MATArtiAdjCon = 10703
        MATArtiAdjMod = 10704
        MATRestMatComp = 10705
        MATAtributoModif = 10706
        MATArtiModificarGen = 10707
        MATRestAsigAtrib = 10708
        MATConsultaImpuesto = 10709
        MATModificarImpuesto = 10710
        MATModificarCodigo = 10711

        '*************** ATR.MANTENIMIENTO ****************************************
        ATRIBConsultar = 20500
        ATRIBModificar = 20501
        ATRIBBajasLog = 20502
        ATRIBRestMatComp = 20503
        ATRIBModificarCodigo = 20504


        ' ****************** Estructura de compra *********************

        COMPConsultar = 10800
        COMPModificar = 10801
        COMPRestEquipo = 10802
        COMPModificarCodigo = 10803

        ' ****************** Grupos de material por comprador*********************

        GRUPPorCOMPConsultar = 10900
        GRUPPorCOMPModificar = 10901
        GRUPPorCOMPRestEquipo = 10902
        GRUPPorCOMPRestMatComp = 10903

        ' ****************** Compradores por grupos de material *********************

        COMPPorGRUPConsultar = 11100
        COMPPorGRUPModificar = 11101
        COMPPorGRUPRestEquipo = 11102
        COMPPorGRUPRestMatComp = 11103

        ' ****************** Proveedores*********************

        PROVEConsultar = 11200
        PROVEAnyadir = 11201
        PROVEEliminar = 11202
        PROVEModificarDatGen = 11203
        PROVEModifcarCalif = 11204
        PROVERestEquipo = 11205
        PROVERestMatComp = 11206
        PROVEWebConsultar = 11207
        PROVEWebmodificar = 11208
        PROVEEspPortalProveConsultar = 11209
        PROVEEspProveModificar = 11210
        PROVEModificarCodigo = 11211


        ' ****************** Equipos de compra por proveedor *********************

        EQPPorPROVEConsultar = 11300
        EQPPorPROVEModificar = 11301
        EQPPorPROVERestEquipo = 11302
        EQPPorPROVERestMatComp = 11303

        ' ****************** Proveedores por equipos de compra *********************

        PROVEPorEQPConsultar = 11400
        PROVEPorEQPModificar = 11401
        PROVEPorEQPRestEquipo = 11402
        PROVEPorEQPRestMatComp = 11403

        ' ****************** Grupos de material por proveedor*********************

        GRUPPorPROVEConsultar = 11500
        GRUPPorPROVEModificar = 11501
        GRUPPorPROVERestEquipo = 11502
        GRUPPorPROVERestMatComp = 11503

        ' ****************** Proveedores por grupo de material *********************

        PROVEPorGRUPConsultar = 11600
        ProvePorGRUPModificar = 11601
        PROVEPorGRUPRestMatComp = 11602
        ProvePorGRUPRestEquipo = 11603


        ' *************************************************************************
        ' *                 PRESUPUESTOS                                          *
        ' *************************************************************************
        ' ******************** Presupuestos por material *************************
        PRESPorMATConsultar = 11800
        PRESPorMATModificar = 11801
        PRESPorMATRestMatComp = 11802

        ' ******************** Presupuestos por proyecto *************************
        PRESPorPROYConsultar = 11700
        PRESPorProymodificar = 11701
        PRESPorPROYRestUO = 11702
        PRESPorPROYModificarCodigo = 11703

        ' ******************** Presupuestos por partida contable *************************
        PRESPorParConConsultar = 11900
        PRESPorParConModificar = 11901
        presporparconRestUO = 11902
        PRESPorParModificarCodigo = 11903

        ' ******************** Presupuestos por concepto3        *************************
        PRESConcepto3Consultar = 19200
        PRESConcepto3Modificar = 19201
        PRESConcepto3RestUO = 19202
        PRESConcepto3ModificarCodigo = 19203

        ' ******************** Presupuestos por concepto4        *************************
        PRESConcepto4Consultar = 19300
        PRESConcepto4Modificar = 19301
        PRESConcepto4RestUO = 19302
        PRESConcepto4ModificarCodigo = 19303

        ' ******************** Presupuestos por concepto5 (Partida Presupuestaria)  *************************
        PRESConcepto5CrearArboles = 19800
        PRESConcepto5ModificarArboles = 19801

        '*********************************************************************************
        '*                             PROCESOS                                          *
        '*********************************************************************************

        ' ******************** Apertura de procesos *************************

        APEConsultar = 12000
        APEAnyadir = 12001
        APEEliminar = 12002
        APEModificarDatGen = 12003
        APEModificarArti = 12004
        APEModificarPers = 12005
        APEModificarEsp = 12006
        APEValidar = 12007
        APEPermAdjDirectas = 12008
        APEPermSaltarseVolMaxAdjDir = 12009
        APERestMatComp = 12010 ' Si el comprador abre el proceso, obligaremos a que este sea asignado en la seleccion de proveedores
        APERestAsignado = 12011
        APERestResponsable = 12012
        APERestEqpAsignado = 12013
        APERestUOPers = 12014
        APEAnularProcesos = 12015
        APEModifFecLimit = 12016
        APEConsultarSubasta = 12017
        APEModifSubasta = 12018
        APERestDest = 12019 'restriccion de destinos de UON en apertura
        APERestUsuAper = 12020
        APERestMatAbrir = 12021
        APERestPerUON = 12022
        APERestPerDep = 12023
        APEGuardarPlantilla = 12024
        APEModifConfiguracion = 12025
        APEPres1RestUO = 12026
        APEPres2RestUO = 12027
        APEPres3RestUO = 12028
        APEPres4RestUO = 12029
        APEAtribCon = 12030
        APEAtribMod = 12031
        'APEAtribModDef = 12032
        APEModifProcDesdeSolic = 12032 'Permitir modificar estructura de proceso creado desde solic de compras
        APESolicAsignar = 12033
        APESolicRestAsig = 12034
        APESolicRestEquipo = 12035
        APESolicRestUO = 12036
        APERestPlantillaAbrir = 12037
        APERestUsuUON = 12038
        APERestUsuDep = 12039
        APEModificarValidado = 12040
        APEModifProcPlantilla = 12041
        APEModifResponsable = 12042
        APERestResponsableEqp = 12043
        APERestResponsableUO = 12044
        APERestResponsableDep = 12045
        APERestResponsableMat = 12046
        APEModifEquivalencia = 12047
        APEAltaArticulos = 12048
        APEImportarItems = 12049
        APEAtribInterno = 12050
        APERestUsoAtrib = 12051
        APERestProvMatComp = 12052
        APEPermProcMultimaterial = 12053 'nzg   Permitir abrir procesos multimaterial
        APERestPerfUON = 12054
        APERModifCodigo = 12055
        APERestResponsablePerfUO = 12056

        ' ******************** Comparativas y adjudicaciones *************************
        OFECOMPAdjConsulta = 12100
        OFECOMPAdjModPres = 12101
        OFECOMPADjFijarObjetivos = 12102
        OFECOMPAdjPreadjudicar = 12103
        OFECOMPAdjValidar = 12104
        OFECOMPAdjRestMat = 12105
        OFECOMPAdjRestAsignado = 12106
        OFECOMPAdjRestResponsable = 12107
        OFECOMPAdjRestEqpAsig = 12108
        OFECOMPCONFItem = 12109
        OFECOMPHojaAdj = 12110
        OFECOMPPermSoloAdjDir = 12111
        OFECOMPPermSoloenReunion = 12112
        OFECOMPHojaComp = 12113
        OFECOMPAdjRestUsuAper = 12114
        OFECOMPAdjRestOfeEqp = 12115
        OFECOMPAdjRestOfeComp = 12116
        OFECOMPAdjRestUsuUON = 12117
        OFECOMPAdjRestUsuDep = 12118
        OFECOMPAdjModifResponsable = 12119
        OFECOMPAdjRestResponsableEqp = 12120
        OFECOMPAdjRestResponsableUO = 12121
        OFECOMPAdjRestResponsableDep = 12122
        OFECOMPAdjRestResponsableMat = 12123
        OFECOMPAdjModifOfeProv = 12124
        OFECOMPAdjRestModifOfeProv = 12125
        OFECOMPAdjModifOfeUsu = 12126
        OFECOMPAdjRestModifOfeUsu = 12127
        OFECOMPAdjRestComentarResponsable = 12128
        OFECOMPAdjAdjaERP = 12129
        OFECOMPAdjPermVistasOtros = 12130
        OFECOMPAdjPermModifVistasOtros = 12131
        OFECOMPRestProvMatComp = 12132
        OFECOMPPermitirVistasCompQA = 12133

        ' ******************** Cierre de procesos *************************
        CIERRE = 12400
        CIERRERestMatComp = 12401
        CIERRERestAsignado = 12402
        CIERRERestResponsable = 12403
        CIERRERestEqpAsignado = 12404


        ' ******************** Seleccion de proveedores *************************
        SELPROVEConsultar = 12500
        SELPRoveModificar = 12501
        SELPROVEAnyadirProve = 12502
        SELPROVEAsigResp = 12503
        SELPROVEValidar = 12504
        SELPROVERestMatComp = 12505
        SELPROVERestComprador = 12506
        SELPROVERestResponsable = 12507
        SELPROVERestEquipo = 12508
        SELPROVERestUsuAper = 12509
        SELPROVERestAsigEqp = 12510
        SELPROVERestAsigComp = 12511
        SELPROVERestUsuUON = 12512
        SELPROVERestUsuDep = 12513
        SELPROVERestResponsableEqp = 12514
        SELPROVERestResponsableUO = 12515
        SELPROVERestResponsableDep = 12516
        SELPROVERestResponsableMat = 12517


        ' ******************** Peticion de ofertas *************************
        'PENDIENTE DE CAMBIAR NOMBRES
        COMUNIPROVEConsulta = 12600
        COMUNIPROVEOfeyObj = 12601
        COMUNIPROVEliminar = 12602
        COMUNIPROVEModifPublic = 12603
        COMUNIPROVERestMatComp = 12604
        COMUNIPROVERestComprador = 12605
        COMUNIPROVERestResponsable = 12606
        COMUNIPROVERestEquipo = 12607
        COMUNIPROVECierre = 12608
        COMUNIPROVEPermOrdCompra = 12609
        COMUNIPROVEModifFecLimit = 12610
        COMUNIPROVERestUsuAper = 12611
        COMUNIPROVERestAsigEqp = 12612
        COMUNIPROVERestAsigComp = 12613
        COMUNIPROVERestUsuUON = 12614
        COMUNIPROVERestUsuDep = 12615
        COMUNIPROVEModifResponsable = 12616
        COMUNIPROVERestResponsableEqp = 12617
        COMUNIPROVERestResponsableUO = 12618
        COMUNIPROVERestResponsableDep = 12619
        COMUNIPROVERestResponsableMat = 12620
        COMUNIPROVERestProvMatComp = 12621

        ' ******************** Buzón de ofertas *************************
        BUZOFEConsultar = 17300
        BUZOFERestMatComprador = 17301
        BUZOFERestComprador = 17302
        BUZOFERestResponsable = 17303
        BUZOFERestEquipo = 17304
        BUZOFEVerOfertasEquipo = 17305
        BUZOFEVerOfertasTodas = 17306
        BUZOFERestUsuAper = 17307
        BUZOFERestUsuUON = 17308
        BUZOFERestUsuDep = 17309
        BUZOFERestProvMatComp = 17310
        BUZOFERestProvEquComp = 17311

        ' ******************** Recepcion de ofertas por proceso *************************
        RECOFEConsultar = 12700
        RECOFEAlta = 12701
        RECOFERestMatComprador = 12702
        RECOFERestComprador = 12703
        RECOFERestResponsable = 12704
        RECOFERestEquipo = 12705
        RECOFERestUsuAper = 12706
        RECOFEAltaProvAsig = 12708
        RECOFEAltaProvCompAsig = 12707
        RECOFERestUsuUON = 12709
        RECOFERestUsuDep = 12710
        RECOFEModifResponsable = 12711
        RECOFERestResponsableEqp = 12712
        RECOFERestResponsableUO = 12713
        RECOFERestResponsableDep = 12714
        RECOFERestResponsableMat = 12715
        RECOFEModifEquivalencia = 12716
        RECOFEModifOfeProv = 12717
        RECOFERestModifOfeProv = 12718
        RECOFEModifOfeUsu = 12719
        RECOFERestModifOfeUsu = 12720
        RECOFEElimOfeProv = 12721
        RECOFERestElimOfeProv = 12722
        RECOFEElimOfeUsu = 12723
        RECOFERestElimOfeUsu = 12724
        RECOFERestOfeEqp = 12725
        RECOFERestOfeComp = 12726
        RECOFERestProvMatComp = 12727

        ' ******************** Contratos *************************************************
        CONTRConsultar = 15100
        CONTRCrear = 15101
        CONTRModificar = 15102
        CONTREliminar = 15103
        CONTRRestUsuContr = 15104 'Acciones a los contratos creados por el usuario
        CONTRRestContrEqp = 15105 'Acciones a los contratos del proveedor asignado al equipo
        CONTRRestContrCom = 15106 'Acciones a los contratos del proveedor asignado al comprador
        CONTRRestMatComprador = 15107 'Procesos del material
        CONTRRestComprador = 15108    'Procesos asignados al comprador
        CONTRRestResponsable = 15109  'Procesos de los que es comprador responsable
        CONTRRestEquipo = 15110       'Procesos asignados al equipo
        CONTRRestUsuAper = 15111      'Procesos abiertos por el usuario
        CONTRRestUsuUON = 15112   'Procesos abiertos dentro de la unidad organizativa del usuario
        CONTRRestUsuDep = 15113   'Procesos abiertos dentro del departamento del usuario
        CONTRModifResponsable = 15114
        CONTRRestResponsableEqp = 15115
        CONTRRestResponsableUO = 15116
        CONTRRestResponsableDep = 15117
        CONTRRestResponsableMat = 15118
        CONTRRestProvMatComp = 15119

        ' ******************** Planificacion de reuniones *************************
        PLANREUConsultar = 12800
        PLANREUModFechas = 12801
        PLANREUEstReu = 12802
        PLANREUModReu = 12803
        PLANREUConvAge = 12804
        PLANREURestMat = 12805
        PLANREURestAsignado = 12806
        PLANREURestResponsable = 12807
        PLANREURestEqpAsignado = 12808
        PLANREURestUsuAper = 12809
        PLANREURestUsuUON = 12810
        PLANREURestUsuDep = 12811
        PLANREURestProvMatComp = 12812

        ' ******************** Resultado de reuniones *************************
        RESREUConsultar = 13000
        RESREUAnotarRes = 13001
        RESREUModPres = 13002
        RESREUValidar = 13003
        RESREUObtenerHojaAdj = 13004
        RESREURestMatComp = 13005
        RESREUActaCon = 13006
        RESREUModObj = 13007
        RESREURestUsuUON = 13008
        RESREURestUsuDep = 13009
        RESREUModifResponsable = 13010
        RESREURestResponsableEqp = 13011
        RESREURestResponsableUO = 13012
        RESREURestResponsableDep = 13013
        RESREURestResponsableMat = 13014
        RESREUModifOfeProv = 13015
        RESREURestModifOfeProv = 13016
        RESREUModifOfeUsu = 13017
        RESREURestModifOfeUsu = 13018
        RESREURestComentarResponsable = 13019
        RESREUReunionesVirtuales = 13020
        RESREUAdjaErp = 13021
        RESREUPermitirVistasOtrosUsuarios = 13022
        RESREUPermitirModifVistasOtrosUsuarios = 13023
        RESREUPermitirVistasCompQA = 13024

        ' *********************************************************
        ' *               PEDIDOS                                *
        ' *********************************************************
        'Catálogo
        CATALOGConsultar = 20000
        CATALOGModificar = 20001
        CATALOGSegurConsultar = 20002
        CATALOGSegurModificar = 20003
        CATALOGAdjCategoriaCambiar = 20004
        CATALOGAdjModificar = 20005
        CATALOGAdjPrecioModificar = 20006
        CATALOGRestMatComp = 20007  'Rest a procesos de su Material y artículos de su material
        CATALOGRestUsuAprov = 20008 'Rest a categorias del usuario
        CATALOGBajaLogCon = 20009
        CATALOGBajaLogMod = 20010
        CATALOGAccesoFSEP = 20011
        CATALOGSegurRestUO = 20012  'Rest UO para la seguridad de categorias
        CATALOGSegurRestDep = 20013 'Rest Dep para la seguridad de categorias
        CATALOGRestDest = 20014 'restriccion de destinos de UON en catalogo
        CATALOGRestUsuAper = 20015 'Rest a procesos abiertos por Usuario
        CATALOGSolicAsignar = 20016
        CATALOGSolicRestAsig = 20017
        CATALOGSolicRestEquipo = 20018
        CATALOGSolicRestUO = 20019
        CATALOGRestUsuUON = 20020  'Rest a procesos de la UON del usario
        CATALOGRestUsuDep = 20021
        CATALOGRestAsignado = 20022
        CATALOGRestResponsable = 20023
        CATALOGRestEqpAsignado = 20024

        'Datos externos
        CATDatExtConsultar = 20400
        CATDatExtGestion = 20402
        CATDatExtRestMAtComp = 20401
        CATDatExtRestProvMatComp = 20403 'Restringir los proveedores a los del material del comprador


        'Emisión de pedidos directos
        PEDDIRConsultar = 20100
        PEDDIREmitir = 20101
        PEDDIRModifPrecioAdjDir = 20102
        PEDDIRSaltarCantAdj = 20103
        PEDDIRRestMatComp = 20104
        PEDDIRRestDest = 20105 'restricción de destinos de UON en pedidos directos
        PEDDIRRestUsuAper = 20106
        PEDDIRRestUsuUON = 20107
        PEDDIRRestUsuDep = 20108
        PEDDIRRestAsignado = 20109
        PEDDIRRestResponsable = 20110
        PEDDIRRestEqpAsignado = 20111
        PEDDIRRestEmpresa = 20112
        PEDDIRRestReceptorUO = 20113
        PEDDIRRestReceptorDep = 20114
        PEDDIRRestReceptorUsu = 20115
        PEDDIRRestPresup1UO = 20116
        PEDDIRRestPresup1UORama = 20117
        PEDDIRRestPresup2UO = 20118
        PEDDIRRestPresup2UORama = 20119
        PEDDIRRestPresup3UO = 20120
        PEDDIRRestPresup3UORama = 20121
        PEDDIRRestPresup4UO = 20122
        PEDDIRRestPresup4UORama = 20123
        PEDDIRModifPresupItem = 20124
        PEDDIRModifPrecioComite = 20125
        PEDDIRModifViaPago = 20126
        PEDDIRRestPersUO = 20127 'Restringir el filtro a personas de la unidad organizativa del usuario

        'Seguimiento de pedidos
        PEDSEGConsultar = 20200
        PEDSEGSoloDirectos = 20201
        PEDSEGSoloAprov = 20202
        PEDSEGModificar = 20203
        PEDSEGAnular = 20204
        PEDSEGEliminar = 20205
        PEDSEGRestMatComp = 20206
        PEDSEGRestUsuAprov = 20207
        PEDSEGComunicar = 20208
        PEDSEGRestDest = 20209 'restricción de destinos de UON en seg. de pedidos
        PEDSegSolicMostrar = 20210   'Solicitudes de compra
        PEDSegSolicRestAsig = 20211
        PEDSegSolicRestEquipo = 20212
        PEDSegSolicRestUO = 20213
        PEDSegRestUsuUO = 20214 'a procesos de la uo del usu
        PEDSegRestUsuDep = 20215 'a procesos del dep del usu
        PEDSegRestUO = 20216
        PEDSegRestDep = 20217
        PEDSegRestAsignado = 20218
        PEDSegRestResponsable = 20219
        PEDSegRestEqpAsignado = 20220
        PEDSegRestUsuAper = 20221 'a procesos abiertos por el usu
        PEDSegVerEmpresaUsu = 20222
        PEDSegModifPrecAdjDir = 20223
        PEDSegRestPresup1UO = 20224
        PEDSegRestPresup1UORama = 20225
        PEDSegRestPresup2UO = 20226
        PEDSegRestPresup2UORama = 20227
        PEDSegRestPresup3UO = 20228
        PEDSegRestPresup3UORama = 20229
        PEDSegRestPresup4UO = 20230
        PEDSegRestPresup4UORama = 20231
        PEDSegModifPresup = 20232
        PEDSegModifPresupItem = 20233
        PEDSegModifPrecComite = 20234
        PEDSegModifPrecResto = 20235
        PEDSegModifNumERP = 20236
        PEDSegRestProvMatComp = 20237 'Restringir los proveedores a los del material del comprador
        PEDSegRestPersUO = 20238 'Restringir el filtro a personas de la unidad organizativa del usuario

        'Recepción de pedidos
        PEDRECConsultar = 20300
        PEDRECModificar = 20301
        PEDRECSoloDirectos = 20302
        PEDRECRestMat = 20303
        PEDRECSoloAprov = 20304
        PEDRECRestUsuAprov = 20305
        PEDRECRestEmpresaUsu = 20306
        PEDRECRestProvMatComp = 20307 'Restringir los proveedores a los del material del comprador
        PEDRECRestPersUO = 20308 'Restringir el filtro a personas de la unidad organizativa del usuario

        ' ************** FIN PEDIDOS ****************************


        ' *********************************************************
        ' *               INFORMES                                *
        ' *********************************************************

        InfNegGenConsultar = 13700

        InfNegmatConsultar = 13800
        InfNegMatRestMat = 13801

        InfNegEqpConsultar = 13900 'Equipos negociadores
        InfNegEqpRestComp = 13901

        InfNegEqpResConsultar = 14000 'Equipos responsables
        InfNegEqpResRestComp = 14001

        InfAplGenConsultar = 14500

        InfAplMatConsultar = 14600
        InfAplMatRestMat = 14601

        InfAplProyConsultar = 14700
        InfAplProyRestUO = 14701

        InfAplPartConsultar = 14800
        InfAplPartRestUO = 14801

        InfAplUOConsultar = 14900
        InfAplUORestPer = 14901

        InfEvolucionConcepto1Consultar = 18100
        InfEvolucionConcepto1RestMat = 18101
        InfEvolucionConcepto1RestUO = 18102

        InfEvolucionConcepto2Consultar = 19100
        InfEvolucionConcepto2RestMat = 19101
        InfEvolucionConcepto2RestUO = 19102

        InfNegCon3Consultar = 19400
        InfNegCon3RestUO = 19401
        InfNegCon4Consultar = 19500
        InfNegCon4RestUO = 19501

        InfAplCon3Consultar = 19600
        InfAplCon3RestUO = 19601
        InfAplCon4Consultar = 19700
        InfAplCon4RestUO = 19701

        AhorrosActualizar = 20800
        AhorrosModifFrec = 20801

        ' ******************** Cambiar contrasenya *************************

        CAMBContrasenya = 15000

        ' ******************** Parámetros ************************ '

        PARGEN = 16100
        CONFINST = 16200
        PARGENInt = 16300

        '******************** Estado de procesos *****************
        ESTProceConsulta = 17000
        ESTProceRestComp = 17001
        ESTProceRestEqp = 17002
        ESTProceRestCompResp = 17003
        ESTProceRestMatComp = 17004
        ESTProceSoloDeAdjDir = 17005
        ESTProceSoloDeAdjEnReu = 17006
        ESTProcePendVal = 17007
        ESTProcePendAsig = 17008
        ESTProcePendPet = 17009
        ESTProceRecOfe = 17010
        ESTProceComObj = 17011
        ESTProcePendAdj = 17012
        ESTProceParCer = 17013
        ESTProcePendAdjNotif = 17014
        ESTProceAdjNotif = 17015
        ESTProceAnul = 17016
        ESTProceRestUsuAper = 17017
        ESTProceRestUsuUON = 17018
        ESTProceRestUsuDep = 17019

        '********************Administración de proveedores del portal

        PROVEPortalConsultar = 17100
        PROVEPortalIncorporar = 17101
        PROVEPortalIdentificar = 17102
        PROVEPortalActivarDesactivar = 17103

        '***************************Plantillas de procesos
        PLANTILLASConsultar = 20600
        PLANTILLASModificar = 20601
        PLANTILLASPermitirModifDefinAtrib = 20602
        ' edu Tarea 98
        PLANTILLASRestringirSoloMaterialesComprador = 20603
        PLANTILLASRestringirAsignacionAtributosMateriales = 20604


        ' *********************************************************
        ' *               SOLICITUDES                                *
        ' *********************************************************

        '************************WORKFLOWS*************
        WORKFLOWConsultar = 20900
        WORKFLOWModificar = 20901
        WORKFLOWModificarBloques = 20902

        '************************FORMULARIOS*************
        FORMULARIOConsultar = 21000
        FORMULARIOModificar = 21001
        FORMULARIORestDest = 21002
        FORMULARIORestMat = 21003
        FORMULARIORestProve = 21004

        '************************CONFIGURACION*************
        SOLCONFConsultar = 21100
        SOLCONFModificar = 21101
        SOLCONFModificarPetic = 21102
        SOLCONFRestUOPetic = 21103
        SOLCONFRestDepPetic = 21104
        SOLCONFFlujoTrabajo = 21105

        '*******************SOLICITUDES**************************
        SOLICConsultar = 20700
        SOLICModificarDatos = 20701
        SOLICModificarComent = 20702
        SOLICAprobRechazoCierre = 20703
        SOLICReabrir = 20704
        SOLICAnular = 20705
        SOLICEliminar = 20706
        SOLICAsignar = 20707
        SOLICRestAsig = 20708
        SOLICRestEquipo = 20709
        SOLICRestUO = 20710
        SOLICAbrirProc = 20711
        SOLICEnviarProc = 20712
        SOLICPedidoDirecto = 20713
        SOLICRestrDpto = 20714
        SOLICEnCurso = 20715
        SOLICRestrDest = 20716
        SOLICRestrMat = 20717
        SOLICRestrProve = 20718
        SOLICAbrirProcEnCurso = 20719

        ' ************** FIN SOLICITUDES ****************************


    End Enum

    ' ENTRADAS AL DICCIONARIO DE DATOS

    Public Enum EntradasDiccionario

        DiccART = 10
        DiccCAL = 20
        DiccCOM = 30
        'DiccCON = 40
        DiccDEP = 50
        DiccDEST = 60
        dicceqp = 70
        DiccGMN1 = 80
        DiccGMN2 = 90
        DiccGMN3 = 100
        DiccGMN4 = 110
        DiccMON = 120
        DiccOfeEst = 130
        DiccPAG = 140
        DiccPAI = 150
        DiccPER = 160
        DiccPERF = 170
        DiccPresCon1 = 180
        DiccPresCon2 = 190
        DiccPresCon3 = 200
        DiccPresCon4 = 210
        DiccPresProy1 = 220
        DiccPresProy2 = 230
        DiccPresProy3 = 240
        DiccPresProy4 = 250
        DiccPROVE = 260
        DiccPROVI = 270
        DiccROL = 280
        DiccUNI = 290
        DiccUON1 = 300
        DiccUON2 = 310
        DiccUON3 = 320
        DiccUSU = 330
        DiccAct1 = 340
        DiccAct2 = 350
        DiccAct3 = 360
        DiccAct4 = 370
        DiccAct5 = 380
        DiccPresCon31 = 390
        DiccPresCon32 = 400
        DiccPresCon33 = 410
        DiccPresCon34 = 420
        DiccPresCon41 = 430
        DiccPresCon42 = 440
        DiccPresCon43 = 450
        DiccPresCon44 = 460
        DiccCat1 = 461
        DiccCat2 = 462
        DiccCat3 = 463
        DiccCat4 = 464
        DiccCat5 = 465
        DiccGrupo = 667

    End Enum

    Public Const Accion_Alta As String = "I"
    Public Const Accion_Baja As String = "D"
    Public Const Accion_Modificacion As String = "U"
    Public Const Accion_CambioCodigo As String = "C"
    Public Const Accion_Reenvio As String = "R"
    Public Const Accion_CambioEstado As String = "E"
    Public Const Accion_Homologacion As String = "H" 'Esta acción es exclusiva para PROVEEDORES, en la adjudicacion de procesos
    Public Const Accion_CambioNivel4 As String = "4" 'Esta acción es exclusiva para ARTICULOS, implica una reubicación del artículo


    Public Enum ErroresGS

        TESnoerror = 0
        TESUsuarioNoValido = 1
        TESDatoDuplicado = 2
        TESDatoEliminado = 3
        TESDatoNoValido = 4
        TESInfModificada = 5
        TESFaltanDatos = 6
        TESImposibleEliminar = 7
        tesCodigoProcesoCambiado = 8 'No es un error en si. Indica que al dar de alta el numero mostrado de proceso ha cambiado.
        TESAdjAntDenegada = 9        'No es un error en si. Indica que no se pueden dar de alta adjudicaciones anteiores al summit, porque ya hay nuevas.
        TESCantDistMayorQueItem = 10 ' La cantidad de distribucion supera a la del item.
        TESValidarSinItems = 20      ' Intentar validar un proceso sin items
        TESValidarYaValidado = 21    ' Intentar validar un proceso ya validado. El campo Arg1 contendra la fecha de validacion, y el Arg2 el codigo de usuario.
        TESValidarSelProveSinProve = 22
        TESValidarSelProveYaValidado = 23
        TESValidarConfItemsYaValidada = 24
        TESVolumenAdjDirSuperado = 25 'Volumen de adjudicación directa superado
        TESOfertaAdjudicada = 26
        TESCierreSinPreAdjudicaciones = 27
        TESImposibleReabrir = 28
        TESProcesoCambiadoDeEstado = 29
        TESCambioContrasenya = 30
        tesprocesocerrado = 31 ' Al intentar modificar o eliminar un proceso que ha pasado a estado cerrado.
        tesimposiblecambiarfecha = 32 'Al intentar cambiar la fecha de una reunión
        TESProcesoBloqueado = 33 'Al intentar iniciar la edicion del proceso bloqueandolo
        TESImposibleEliminarAsignacion = 34 'Al intentar desasignar un proveedore al que ya se le han enviado peticiones
        TESImposibleExcluirItem = 35 ' Al intentar excluir un item que ya tiene adjudicaciones
        TESImposibleAdjudicarAProveNoHom = 36 ' Al intentar validarycerrar un proceso adjudicando a proveedores no homologados
        TESImposibleModificarReunion = 37 'Al intentar modificar una reunión que ya ha tenido algun resultado. (No al añadir procesos nuevos a esa reunión)
        TESImposibleAsignarProveedor = 38 'Al intentar asignar un proveedor que ya está asignado
        TESInfActualModificada = 39 'Cuando iniciamos la edición de una fila y vemos que los datos actuales han cambiado
        TESMail = 40 'Para tratamiento de errores de envio de mail
        TESProvePortalDesautorizado = 41
        tesciaportaldesautorizada = 42
        TESProveedorHaUsadoElPortal = 43 'El proveedor ha utilizado el portal, no se puede desenlazar o eliminar
        TESProveedorEsPremium = 44
        TESProvePortalDesautorizadoEnCia = 45
        TESSelProveFUPProveSinEnlace = 46
        TESSelProveBSProveSinEmail = 47
        TESDesPublicarPremiumActivo = 48
        TESInfBloqueadaPorOtro = 49
        TESImposibleMarcarAprob = 50
        TESImposibleEliminarApro = 51
        TESUsuarioPersonaEliminado = 52
        TESImposiblePublicarLineaCatProveNoFormaPago = 53
        TESImposiblePublicarLineaCatProveNoContactoAprov = 54
        TESImposibleAnyadirAdjACatAdjYaEnCat = 55
        TESImposibleEliminarCat = 56
        TESImposibleAnyadirLineaCatNuevaSubCategoria = 57
        TESImposibleAnyadirLineaCatPorSeguridad = 58
        TESImposiblePublicarLineaCatProveNoProvAutorizado = 59
        TESImposibleAbrirProcesoSinPermisoMaterial = 60
        TESGrupoCerrado = 61
        TESImposibleAnyadirProceFaltanDatosObl = 62
        TESImposibleAnyadirProceDesdePlantFaltanDatosObl = 63
        TESImposibleEliminarAtributoProceConOfertas = 64
        TESImposibleEliminarAtributoMantenimiento = 65
        TESPresupuestoObligatorio = 66
        TESImposibleEliminarAtributoProceNoInterno = 67
        TESNingunGrupoDef = 68
        TESErrorDeTransferenciaDeArchivo = 69
        TESImposibleAccederAlArchivo = 70
        TESAdjuntoEliminadoPortal = 71
        TESASPUser = 72
        TESMailSMTP = 73
        TESRunAsUser = 74
        TESASPUserCuentaExiste = 75
        TESASPUserAddUser = 76
        TESASPUserPropiedades = 77
        TESASPUserRemoteGrupo = 78
        TESADSIExProgramaIni = 79
        TESADSIExHomeDir = 80
        TESASPUserCambiaPwd = 81
        TESASPUserRename = 82
        TESASPUserRemove = 83
        TESCambiaThreshold = 84
        TESProcesoAhorroCalculado = 85
        TESErrorAhorrosCalculando = 86
        TESImposibleAnyadirGrupoFaltanDatosObl = 87
        TESPedidoProcesoUnoSoloEmitido = 88
        TESTraspasoAdjERP = 89
        TESNotifUsuMatQA = 90
        TESCambioUnidadMedida = 91
        TESOtroerror = 100
        TESValidarPerfilEliminar = 102
        TESArtGenericoModificado = 103
        TESProcesoSinMatNoMultimat = 104
        TESAtributoYaMarcadoAPedido = 105
        TESSolicitudAbrirMaxAdjDir = 106
        TESSolicitudAbrirMaxAdjDirPermiso = 107
        TESImposibleAccederAlArchivoVinculado = 108
        TESImposibleValidarPorAtribsOblProvePremium = 109
        TESErrorIntegracion = 110
        TESErrorEliminarRecepFacturas = 111
        TESErrorEliminarSeguimPedidos = 112
        TESErrorEliminarSeguimPedidosOK = 113
        TESProcesoNoExiste = 114
        TESAtribEspObl = 115
        TESProvePortalNoEnlazado = 117
        TESErrorSuplantacion = 126
    End Enum

    'Indica los distintos estados de un movimiento en el sistema receptor
    Public Enum EstadoIntegracion
        PendienteDeTratar = 0
        Enviado = 1
        PendienteDeReintentar = 2
        RecibidoRechazado = 3
        recibidocorrecto = 4
    End Enum

    Public Enum AccionesGS

        ' ************** Unidades ***************

        ACCUniAnya = 30
        ACCUniMod = 31
        ACCUniEli = 32
        ACCUniCon = 33

        ' ************** Monedas ****************

        ACCMonAnya = 40
        ACCMonMod = 41
        ACCMonEli = 42
        ACCMonActCamb = 43
        ACCMonCon = 44

        ' ************* Paises ******************

        ACCPaiAnya = 50
        ACCPaiMod = 51
        ACCPaiEli = 52
        ACCPaiCon = 53

        ' ************* Provincias ***************

        ACCProviAnya = 60
        ACCProviMod = 61
        ACCProviEli = 62
        ACCProviCon = 63

        ' ************************ Procesos **************

        ACCProceCon = 280
        AccProceAnya = 281
        ACCProceModDatGen = 282
        accproceeli = 283
        ACCProceVal = 284
        ACCProceItemCon = 285
        ACCProceItemAnya = 286
        ACCProceItemMod = 287
        ACCProceItemEli = 288
        ACCProcePerCon = 289
        ACCProcePerAnya = 290
        ACCProcePerMod = 291
        ACCProcePerEli = 290
        ACCProceEspCon = 291
        ACCProceEspAnya = 292
        ACCProceEspMod = 293
        ACCProceEspEli = 294
        ACCProceDistItemCon = 295
        ACCProceDistItemAnya = 296
        ACCProceDistItemMod = 297
        ACCProceDistItemEli = 298
        ACCProceDistCon = 295
        ACCProceDistAnya = 296
        ACCProceDistMod = 297
        ACCProceDistEli = 298
        ACCProceAnular = 299
        ACCProceReabrir = 300
        ACCProceGrupoAnya = 305
        ACCProceGrupoMod = 306
        ACCProceGrupoEli = 307
        ACCProceGrupoEspCon = 308
        ACCProceGrupoEspAnya = 309
        ACCProceGrupoEspMod = 310
        ACCProceGrupoEspEli = 311
        ACCProceItemsGrupoCamb = 312
        ACCProceAtribCon = 313
        ACCPROCEAtribMod = 314
        ACCPROCEAtribAnya = 315
        ACCPROCEAtribEli = 316
        ACCProceSobreAnya = 317
        ACCProceSobreMod = 318
        ACCProceSobreEli = 319
        ACCProceModConfig = 320
        ACCProceModCodigo = 321
        ACCProceGrupoModCodigo = 322
        ACCProceModPresAsig = 323
        ACCProceModifMultItems = 324
        ACCProceAnyaMultItems = 325
        ACCProceCopiaItem = 326
        ACCProceCreaDesdeSol = 327
        ACCProceAnyaItemDesdeSol = 328
        ACCProceModifDistUO = 329
        ACCModifSubasta = 330
        ACCNotifSubasta = 331
        ACCNotifEventoSubasta = 332

        ' **************  Pagos ********************

        ACCPagAnya = 500
        ACCPagMod = 501
        ACCPagEli = 502
        ACCPagCon = 503

        ' **************  Vías de pago ********************

        ACCViaPagAnya = 540
        ACCViaPagMod = 541
        ACCViaPagEli = 542
        ACCViaPagCon = 543

        ' **************  Tipos de Impuestos ********************

        ACCImpuestoAnya = 544
        ACCImpuestoMod = 545
        ACCImpuestoEli = 546
        ACCImpuestoCon = 547

    End Enum

    Public Enum TipoSubasta
        Inglesa = 0
        SobreCerrado = 1
        Japonesa = 2
    End Enum

    Public Enum ModoSubasta
        ProcesoCompleto = 1
        Grupo = 2
        Item = 3
    End Enum

    Public Enum TipoEstadoCiaEnPortal
        TESPSolicitado = 1
        TESPInactivo = 2
        TESPActivo = 3
    End Enum

    Public Enum TipoEstadoProveEnCia
        TESPCInActivo = 2
        TESPCActivo = 3
    End Enum

    Public Enum TValidacionAtrib
        Apertura = 0
        Publicacion = 1
    End Enum

    Public Enum TipoOrdenacionPeticiones
        OrdPorCodProve = 1
        OrdPorDenProve = 2
        OrdPorApe = 3
        OrdPorNom = 4
        OrdPorWeb = 5
        OrdPorEMail = 6
        OrdPorImpreso = 7
        OrdPorFecha = 8
        OrdPorTipoComu = 9
        OrdPorFechaDec = 10
    End Enum

    Public Enum TipoOrdenacionPersonas
        OrdPorCod = 1
        OrdPorApe = 2
        OrdPorNom = 3
        OrdPorUO = 4
        OrdPorDep = 5
        OrdPorCargo = 6
        OrdPortfno = 7
        OrdPortfno2 = 8
        OrdPorFax = 9
        OrdPormail = 10
        OrdPorRol = 11
        OrdPorEqp = 12
        OrdPorFSEP = 13
        OrdPorFSGS = 14
        OrdPorFSWS = 15
        OrdPorFSQA = 16
        OrdPorInvi = 17
    End Enum

    Public Enum TipoOrdenacionAsignaciones
        OrdAsigPorCodProve = 1
        OrdAsigPorDenProve = 2
        OrdAsigPorCal1 = 3
        OrdAsigPorCal2 = 4
        OrdAsigPorCal3 = 5
        OrdAsigPorval1 = 6
        OrdAsigPorval2 = 7
        OrdAsigPorval3 = 8
        OrdAsigPorComp = 9
        OrdAsigPorEqp = 10
        OrdAsigPorAsig = 11
        OrdAsigPorCodPortal = 12
    End Enum

    Public Enum TipoBajadaSubasta
        PorImporte = 0
        PorPorcentaje = 1
    End Enum

End Class

