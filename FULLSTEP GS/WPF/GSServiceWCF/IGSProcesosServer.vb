﻿Imports System.ServiceModel
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.ComponentModel

<ServiceContract()> _
Public Interface IGSProcesosServer
    'Proceso
    <OperationContract()> _
    Function DevolverProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal DevolverEstructura As Boolean) As GSServerModel.Proceso
    <OperationContract()> _
    Function ActualizarProceso(ByVal oProceso As GSServerModel.Proceso) As GSServerModel.GSException
    <OperationContract()> _
    Function DevolverProcesoProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoProveedores
    <OperationContract()> _
    Function ActualizarProcesoProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal arProcesoProves As GSServerModel.ProcesoProveedores) As GSServerModel.GSException
    <OperationContract()> _
    Function DevolverProcesoGrupos(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal DevolverEstructura As Boolean) As GSServerModel.ProcesoGrupos
    <OperationContract()> _
    Function DevolverProcesoGrupoItems(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal ID As Integer) As GSServerModel.Items
    <OperationContract()> _
    Function ActivarPublicacionProveedores(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal oProves As GSServerModel.ProcesoProveedores, ByVal InsWeb As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, ByVal sFSPSRV As String, ByVal sFSPBD As String) As GSServerModel.GSException
    <OperationContract()> _
    Function DesactivarPublicacionProveedores(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal oProves As GSServerModel.ProcesoProveedores, ByVal InsWeb As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal bPremium As Boolean, ByVal FSP_CIA As Integer, ByVal sFSPSRV As String, ByVal sFSPBD As String) As GSServerModel.GSException
    <OperationContract()>
    Function SacarRemitenteEmpresa(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As String
    <OperationContract()>
    Function DevolverCarpetaPlantillaProce(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As String

    'Proveedor
    <OperationContract()> _
    Function DevolverProveedorContactos(ByVal Prove As String) As GSServerModel.Contactos
    <OperationContract()> _
    Function DevolverProveedoresAPublicar(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, Optional ByVal Equipo As String = Nothing, Optional ByVal Com As String = Nothing) As GSServerModel.Proveedores
    <OperationContract()> _
    Function DevolverEstadoProveedorPortal(ByVal Prove As String, ByVal Cia As Integer) As GSServerModel.EstadoProveedorPortal
    <OperationContract()> _
    Function ComprobarEmailEnPortal(ByVal CodProvePortal As String, ByVal EMail As String, ByVal FSP_SRV As String, ByVal FSP_BD As String) As Boolean
    <OperationContract()> _
    Function ValidarAtributosEspObligatorios(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal udtTipoVal As TValidacionAtrib) As GSServerModel.ValidacionAtributos

    'Proce_Def
    <OperationContract()> _
    Function DevolverProcesoDef(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoDef
    <OperationContract()> _
    Function ActualizarProcesoDef(ByVal oProcesoDef As GSServerModel.ProcesoDef) As GSServerModel.GSException

    'Subasta
    <OperationContract()> _
    Function DevolverDatosSubastaMonitor(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal Idioma As String, Optional ByVal Grupo As Nullable(Of Integer) = Nothing, Optional ByVal Item As Nullable(Of Integer) = Nothing) As GSServerModel.SubastaMonitorData
    <OperationContract()> _
    Function DevolverDatosSubastaConf(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal Idioma As String) As GSServerModel.SubastaConfData
    <OperationContract()> _
    Function DevolverProcesoSubasta(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal DevolverEstructura As Boolean) As GSServerModel.ProcesoSubasta
    <OperationContract()> _
    Function ActualizarProcesoSubasta(ByVal oProceso As GSServerModel.Proceso, ByVal oProcesoDef As GSServerModel.ProcesoDef, ByVal oProcesoSubasta As GSServerModel.ProcesoSubasta, ByVal ProcesoProves As GSServerModel.ProcesoProveedores, ByVal oSubastaReglas As GSServerModel.ProcesoSubastaReglas, ByVal oArchivos As GSServerModel.Files) As GSServerModel.GSException
    <OperationContract()> _
    Function DevolverSubastaProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoProveedores
    <OperationContract()> _
    Function DevolverProcesoSubastaPujas(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoSubastaPujas
    <OperationContract()> _
    Function AccionSubasta(ByVal Evento As GSServerModel.ProcesoSubastaEvento) As GSServerModel.GSException

    'Asignaciones
    <OperationContract()> _
    Function DevolverAsignacionesProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal CriterioOrdenacion As TipoOrdenacionAsignaciones = -1, Optional ByVal CodEqp As String = Nothing, Optional ByVal CodComp As String = Nothing, Optional ByVal CodProve As String = Nothing, Optional ByVal bCargarGrupos As Boolean = False) As GSServerModel.Asignaciones

    'Equipos
    <OperationContract()> _
    Function DevolverCompradoresEquipo(ByVal Equipo As String, Optional ByVal NumMax As Integer = -1, Optional ByVal CarIniCod As String = Nothing, Optional ByVal CarIniApel As String = Nothing, Optional ByVal NoBajaLog As Boolean = False, Optional ByVal OrderByApe As Boolean = False) As GSServerModel.Compradores

    'Peticiones
    <OperationContract()> _
    Function DevolverPeticionesProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProceProvePetOfertas
    <OperationContract()> _
    Function InsertarPeticionesProceso(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal TipoInstalacionWeb As TipoInstWeb, ByVal CIA As Integer, ByVal oPets As GSServerModel.ProceProvePetOfertas) As GSServerModel.GSException

    'Reglas de subasta
    <OperationContract()> _
    Function DevolverProcesoSubastaReglas(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoSubastaReglas

    'Notificaciones
    <OperationContract()> _
    Function ComponerCorreosNotificacionSubasta(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String, ByVal InsWEB As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, _
                                                ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal DirPlantilla As String, ByVal From As String, ByVal MailSubject As String, ByVal URLWeb As String) As GSServerModel.DatosNotifSubasta
    <OperationContract()> _
    Function ComponerCorreosNotificacionEventoSubasta(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String, ByVal InsWEB As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, _
                                                      ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal DirPlantilla As String, ByVal sFrom As String, ByVal oEvento As GSServerModel.ProcesoSubastaEvento, ByVal oEveDesc As Dictionary(Of String, String)) As GSServerModel.EMails
    <OperationContract()>
    Function NotificacionSubasta(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal InsWEB As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal oDatos As GSServerModel.DatosNotifSubasta, ByVal bUsarRemitenteEmpresa As Boolean) As GSServerModel.GSException
    <OperationContract()> _
    Function NotificacionEventoSubasta(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oEMails As GSServerModel.EMails) As GSServerModel.GSException
End Interface

<ServiceContract()> _
Public Interface IGSFileStreamServer
    'Interface para las acciones de BD con tipo de datos FILESTREAM

    'Archivos de reglas de subasta    
    <OperationContract()> _
    Function ActualizarProcesoSubastaReglas(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal SubastaReglas As GSServerModel.ProcesoSubastaReglas, ByVal oArchivos As GSServerModel.Files) As GSServerModel.GSException    
    <OperationContract()> _
    Function DevolverArchivoSubastaRegla(ByVal Offset As Integer, ByVal ChunkSize As Integer, ByVal ID As Integer) As Byte()

End Interface

<ServiceContract()> _
Public Interface IGSFileTransferServer
    'File Transfer
    <OperationContract()> _
    Function BeginFileTransfer() As String
    <OperationContract()> _
    Function SendFileChunk(ByVal FileName As String, ByVal sFolder As String, ByVal Buffer As Byte()) As Boolean
    <OperationContract()> _
    Function EndFileTransfer(ByVal sFolder As String) As Boolean
End Interface


