﻿Imports System.ServiceModel
Imports Fullstep.FSNLibrary.TiposDeDatos

' NOTE: If you change the class name "IGSMonedasServer" here, you must also update the reference to "IGSMonedasServer" in App.config.
<ServiceContract()> _
Public Interface IGSMonedasServer

    <OperationContract()> _
    Function DevolverTodasLasMonedas(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As DataSet
    <OperationContract()> _
    Function DevolverTodosLosIdiomas() As GSServerModel.Idiomas
    <OperationContract()> _
    Function ActualizarMoneda(ByRef Moneda As GSServerModel.Moneda, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function AnyadirMoneda(ByRef Moneda As GSServerModel.Moneda, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function EliminarMonedas(ByVal Codigos() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sMonedaCentral As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function CambioCodigoMoneda(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer
    <OperationContract()> _
    Function DevolverMoneda(ByVal Moneda As String, ByVal Idioma As String) As GSServerModel.Moneda
    <OperationContract()> _
    Function ComprobarSession(ByVal sUsu As String, ByVal Session As Long) As Boolean
    <OperationContract()> _
    Function CargarDatosUsu(ByVal sUsu As String, ByVal sIdi As String) As GSServerModel.Usuario
    <OperationContract()> _
    Function DevolverPersona(ByVal sCodigo As String) As GSServerModel.Persona
    <OperationContract()> _
    Function CargarParametrosGenerales(ByRef ParametrosGenerales As GSServerModel.ParametrosGenerales, ByRef LongitudesDeCodigo As GSServerModel.LongitudesCodigo) As Object
    <OperationContract()> _
    Function CargarParametrosInstalacion(ByVal ParametrosGenerales As GSServerModel.ParametrosGenerales, ByRef ParametrosInstalacion As GSServerModel.ParametrosInstalacion, ByVal sUsu As String) As Object
    <OperationContract()> _
    Function CargarParametrosIntegracion(ByRef ParametrosIntegracion As GSServerModel.ParametrosIntegracion, ByVal ParametrosGenerales As GSServerModel.ParametrosGenerales) As Object
    <OperationContract()> _
    Function DevolverTodasLasUnidades(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As DataSet
    <OperationContract()> _
    Function AnyadirUnidad(ByRef oUnidad As GSServerModel.Unidad, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function ActualizarUnidad(ByRef oUnidad As GSServerModel.Unidad, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function EliminarUnidades(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function CambioCodigoUnidad(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer
    <OperationContract()> _
    Function RegistrarEntradaOSalida(ByVal sUsuario As String, ByVal iAccion As Integer, ByVal Descripcion As String) As Boolean
    <OperationContract()> _
    Function DevolverTodasLasViasPago(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As DataSet
    <OperationContract()> _
    Function AnyadirViaPago(ByRef oViaPago As GSServerModel.ViaPago, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function ActualizarViaPago(ByRef oViaPago As GSServerModel.ViaPago, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function EliminarViasPago(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function CambioCodigoViaPago(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

    <OperationContract()> _
    Function DevolverTodasLasFormasPago(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As DataSet
    <OperationContract()> _
    Function AnyadirFormaPago(ByRef oFormaPago As GSServerModel.FormaPago, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function ActualizarFormaPago(ByRef oFormaPago As GSServerModel.FormaPago, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function EliminarFormasPago(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function CambioCodigoFormaPago(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

    <OperationContract()> _
    Function DevolverTodosLosPaisesProvincias(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As DataSet
    <OperationContract()> _
    Function DevolverMonedasPais(ByVal sIdioma As String) As DataSet

    <OperationContract()> _
    Function AnyadirPais(ByRef Pais As GSServerModel.Pais, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function AnyadirProvincia(ByRef Provincia As GSServerModel.Provincia, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function ActualizarPais(ByRef Pais As GSServerModel.Pais, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
    <OperationContract()> _
    Function ActualizarProvincia(ByRef Provincia As GSServerModel.Provincia, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException

    <OperationContract()> _
    Function CambioCodigoPais(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer
    <OperationContract()> _
    Function CambioCodigoProvincia(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String, ByVal sPais As String) As Integer
    <OperationContract()> _
    Function EliminarProvincias(ByVal CodigosProvincias() As String, ByVal CodigosPaisProvi() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String, ByVal sIdioma As String) As GSServerModel.GSException
    <OperationContract()> _
    Function EliminarPaises(ByVal CodigosPaises() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String, ByVal sIdioma As String) As GSServerModel.GSException

    'Impuestos
    <OperationContract()> _
    Function DevolverDatosImpuestos(ByVal sIdioma As String) As GSServerModel.ImpuestosData
    <OperationContract()> _
    Function DevolverImpuestos(ByVal sIdioma As String) As GSServerModel.Impuestos
    <OperationContract()> _
    Function InsertarImpuesto(ByRef oImpuesto As GSServerModel.Impuesto) As GSServerModel.GSException
    <OperationContract()> _
    Function EliminarImpuestos(ByVal IDs() As Integer) As GSServerModel.GSException
    <OperationContract()> _
    Function ActualizarImpuesto(ByRef oImpuesto As GSServerModel.Impuesto) As GSServerModel.GSException
    <OperationContract()> _
    Function InsertarImpuestoValor(ByVal IdImpuesto As Integer, ByRef oImpuestoValor As GSServerModel.ImpuestoValor) As GSServerModel.GSException
    <OperationContract()> _
    Function ActualizarImpuestoValor(ByRef oImpuestoValor As GSServerModel.ImpuestoValor) As GSServerModel.GSException
    <OperationContract()> _
    Function EliminarImpuestoValores(ByVal IDs() As Integer) As GSServerModel.GSException

    'Materiales
    <OperationContract()> _
    Function DevolverEstructuraMaterialesImpuestos(ByVal iIdImpuestoValor As Integer, ByVal sIdioma As String, Optional ByVal sCodEqpComprador As String = Nothing, Optional ByVal sCodComprador As String = Nothing, Optional ByVal bOrdPorCod As Boolean = False) As DataSet
    <OperationContract()> _
    Function ActualizarEstructuraMaterialesImpuestos(ByVal _iIdImpuestoValor As Integer, ByVal oAsignar As GSServerModel.GruposMaterial, ByVal oDesasignar As GSServerModel.GruposMaterial, _
        ByVal oArtAsignar As GSServerModel.Articulos, ByVal oArtDesasignar As GSServerModel.Articulos, ByVal oArtIncluir As GSServerModel.Articulos, ByVal oArtExcluir As GSServerModel.Articulos) As GSServerModel.GSException
    <OperationContract()> _
    Function DevolverArticulosGMNImpuestos(ByVal IdImpuestoValor As Integer, ByVal oGMN As GSServerModel.GrupoMaterial) As GSServerModel.Articulos

    'Subastas
    <OperationContract()> _
    Function UTCdeBD() As Date
End Interface
