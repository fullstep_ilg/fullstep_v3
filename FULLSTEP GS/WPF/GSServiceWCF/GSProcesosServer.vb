﻿Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.ComponentModel
Imports System.ServiceModel
Imports System.ServiceModel.Activation

<ServiceBehavior(), AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Required)> _
Public Class GSProcesosServer
    Implements IGSProcesosServer
    Implements IGSFileStreamServer
    Implements IGSFileTransferServer

    'Private _sFolder As String

    'Public Sub New()
    '    'OperationContext.Current.InstanceContext.Host.Extensions.Add(New GSServerModel.CustomBehaviorExtensionElement)
    '    'OperationContext.Current.InstanceContext.Host.Description.Behaviors.Add(New GSServerModel.CustomBehaviorExtensionElement)
    '    For Each o As System.ServiceModel.Description.ServiceEndpoint In OperationContext.Current.InstanceContext.Host.Description.Endpoints
    '        o.Behaviors.Add(New GSServerModel.CustomBehavior)
    '    Next        
    'End Sub

#Region " IGSProcesosServer "

#Region " Proceso "

    '<summary>Devuelve los datos de un proceso</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<returns>Objetos Proceso con los datos del proceso</returns>

    Public Function DevolverProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal DevolverEstructura As Boolean) As GSServerModel.Proceso Implements IGSProcesosServer.DevolverProceso
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverProceso(Anyo, Cod, GMN1, DevolverEstructura)
    End Function
    '<summary>Devuelve el remitente de la empresa de la distribucion del un proceso</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<returns></returns>

    Public Function SacarRemitenteEmpresa(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As String Implements IGSProcesosServer.SacarRemitenteEmpresa
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.SacarRemitenteEmpresa(Anyo, Cod, GMN1)
    End Function
    '<summary>Devuelve la carpeta personalizada de la empresa</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<returns></returns>

    Public Function DevolverCarpetaPlantillaProce(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As String Implements IGSProcesosServer.DevolverCarpetaPlantillaProce
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverCarpetaPlantillaProce(Anyo, Cod, GMN1)
    End Function
    '<summary>Actualiza los datos de un proceso</summary>
    '<param name="oProceso">Datos del proceso</param>        
    '<returns>Objeto de tipo GSException con el error si ha tenido lugar</returns>

    Public Function ActualizarProceso(ByVal oProceso As GSServerModel.Proceso) As GSServerModel.GSException Implements IGSProcesosServer.ActualizarProceso
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.ActualizarProceso(oProceso)
    End Function

    Public Function DevolverPeticionesProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProceProvePetOfertas Implements IGSProcesosServer.DevolverPeticionesProceso
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverPeticionesProceso(Anyo, Cod, GMN1)
    End Function

    Public Function InsertarPeticionesProceso(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal TipoInstalacionWeb As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal CIA As Integer, ByVal oPets As GSServerModel.ProceProvePetOfertas) As GSServerModel.GSException Implements IGSProcesosServer.InsertarPeticionesProceso
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.InsertarPeticionesProceso(Anyo, Proce, GMN1, TipoInstalacionWeb, CIA, oPets)
    End Function

    Public Function ActivarPublicacionProveedores(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal oProves As GSServerModel.ProcesoProveedores, ByVal InsWeb As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, ByVal sFSPSRV As String, ByVal sFSPBD As String) As GSServerModel.GSException Implements IGSProcesosServer.ActivarPublicacionProveedores
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.ActivarPublicacionProveedores(Anyo, Proce, GMN1, oProves, InsWeb, FSP_CIA, sFSPSRV, sFSPBD)
    End Function

    Public Function DesactivarPublicacionProveedores(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal oProves As GSServerModel.ProcesoProveedores, ByVal InsWeb As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal bPremium As Boolean, ByVal FSP_CIA As Integer, ByVal sFSPSRV As String, ByVal sFSPBD As String) As GSServerModel.GSException Implements IGSProcesosServer.DesactivarPublicacionProveedores
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DesactivarPublicacionProveedores(Anyo, Proce, GMN1, oProves, InsWeb, bPremium, FSP_CIA, sFSPSRV, sFSPBD)
    End Function

#End Region

#Region " Proce_Def "

    '<summary>Devuelve los datos de un proceso de la tabla Proce_Def</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Proce">Proce</param> 
    '<param name="GMN1">GMN1</param>
    '<returns>Objeto ProcesoDef con los datos del proceso de la tabla Proce_Def</returns>

    Public Function DevolverProcesoDef(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoDef Implements IGSProcesosServer.DevolverProcesoDef
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverProcesoDef(Anyo, Proce, GMN1)
    End Function

    '<summary>Actualiza los datos de un proceso de la tabla Proce_Def</summary>
    '<param name="oProcesoDef">Objeto ProcesoDef con los datos del proceso de la tabla ProcesoDef</param>             
    '<returns>Objeto GSException con los datos del error si se han producido</returns>

    Public Function ActualizarProcesoDef(ByVal oProcesoDef As GSServerModel.ProcesoDef) As GSServerModel.GSException Implements IGSProcesosServer.ActualizarProcesoDef
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.ActualizarProcesoDef(oProcesoDef)
    End Function

#End Region

#Region " Proceso Subasta "

    '<summary>Devuelve los datos para la pantalla SubastaMonitor</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<returns>Objeto ProcesoSubasta SubastaMonitorData</returns>

    Public Function DevolverDatosSubastaMonitor(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal Idioma As String, Optional ByVal Grupo As Nullable(Of Integer) = Nothing, Optional ByVal Item As Nullable(Of Integer) = Nothing) As GSServerModel.SubastaMonitorData Implements IGSProcesosServer.DevolverDatosSubastaMonitor
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverDatosSubastaMonitor(Anyo, Cod, GMN1, Idioma, Grupo, Item)
    End Function

    '<summary>Devuelve los datos para la pantalla SubastaConf</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<param name="Idioma">Idioma</param>
    '<returns>Objeto ProcesoSubasta SubastaMonitorData</returns>

    Public Function DevolverDatosSubastaConf(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal Idioma As String) As GSServerModel.SubastaConfData Implements IGSProcesosServer.DevolverDatosSubastaConf
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverDatosSubastaConf(Anyo, Cod, GMN1, Idioma)
    End Function

    '<summary>Devuelve los datos de subasta de un proceso</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<returns>Objetos ProcesoSubasta con los datos de la subasta</returns>

    Public Function DevolverProcesoSubasta(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal DevolverEstructura As Boolean) As GSServerModel.ProcesoSubasta Implements IGSProcesosServer.DevolverProcesoSubasta
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverProcesoSubasta(Anyo, Cod, GMN1, DevolverEstructura)
    End Function

    '<summary>Actualiza los datos de subasta de un proceso</summary>
    '<param name="oProceso">Objeto de tipo Proceso con los datos del proceso</param>         
    '<param name="oProcesoSubasta">Objeto de tipo ProcesoSubasta con los datos de las subasta</param> 
    '<param name="arProcesoProves">Array de objetos ProcesoProveedor con los datos de los proveedores</param> 
    '<param name="FileDirectory">Directorio en el que se encuentran los archivos de las reglas nuevas</param> 
    '<returns>Objeto de tipo GSException con el error si ha tenido lugar</returns>

    Public Function ActualizarProcesoSubasta(ByVal oProceso As GSServerModel.Proceso, ByVal oProcesoDef As GSServerModel.ProcesoDef, ByVal oProcesoSubasta As GSServerModel.ProcesoSubasta, ByVal oProcesoProves As GSServerModel.ProcesoProveedores, ByVal oSubastaReglas As GSServerModel.ProcesoSubastaReglas, ByVal oArchivos As GSServerModel.Files) As GSServerModel.GSException Implements IGSProcesosServer.ActualizarProcesoSubasta
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.ActualizarProcesoSubasta(oProceso, oProcesoDef, oProcesoSubasta, oProcesoProves, oSubastaReglas, oArchivos)
    End Function

    '<summary>Devuelve los proveedores asignados a una subasta</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>     
    '<returns>Objeto de tipo ProcesoProveedores</returns>

    Public Function DevolverSubastaProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoProveedores Implements IGSProcesosServer.DevolverSubastaProveedores
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverSubastaProveedores(Anyo, Cod, GMN1)
    End Function

    '<summary>Validación de los atributos obligatorios</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param> 
    '<param name="udtTipoVal">udtTipoVal</param> 
    '<returns>Objeto de tipo ValidacionAtributos con la validación</returns>

    Public Function ValidarAtributosEspObligatorios(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal udtTipoVal As Fullstep.FSNLibrary.TiposDeDatos.TValidacionAtrib) As GSServerModel.ValidacionAtributos Implements IGSProcesosServer.ValidarAtributosEspObligatorios
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.ValidarAtributosEspObligatorios(Anyo, Cod, GMN1, udtTipoVal)
    End Function

    '<summary>Devuelve las pujas de una subasta</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Codigo">Codigo</param> 
    '<param name="GMN1">GMN1</param>     
    '<returns>Objeto de tipo ProcesoSubastaPujas con las pujas de los proveedores</returns>

    Public Function DevolverProcesoSubastaPujas(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoSubastaPujas Implements IGSProcesosServer.DevolverProcesoSubastaPujas
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverProcesoSubastaPujas(Anyo, Codigo, GMN1)
    End Function

    '<summary>Lleva a cabo la acción indicada sobre un proceso de subasta</summary>
    '<param name="Evento">Acción a llevar a cabo sobre la subasta</param>             
    '<returns>Objeto de tipo GSException con el error si se ha producido</returns>

    Public Function AccionSubasta(ByVal Evento As GSServerModel.ProcesoSubastaEvento) As GSServerModel.GSException Implements IGSProcesosServer.AccionSubasta
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.AccionSubasta(Evento)
    End Function

#End Region

#Region " Proceso Proveedor "

    '<summary>Devuelve los datos de proveedores de un proceso</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<returns>Array de objetos ProcesoProveedor con los datos de los proveedores</returns>

    Public Function DevolverProcesoProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoProveedores Implements IGSProcesosServer.DevolverProcesoProveedores
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverProcesoProveedores(Anyo, Cod, GMN1)
    End Function

    '<summary>Actualiza los datos de proveedores de un proceso</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param> 
    '<param name="arProcesoProves">Array de objetos ProcesoProveedor con los datos de los proveedores</param> 
    '<returns>Objeto de tipo GSException con el error si ha tenido lugar</returns>

    Public Function ActualizarProcesoProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oProcesoProves As GSServerModel.ProcesoProveedores) As GSServerModel.GSException Implements IGSProcesosServer.ActualizarProcesoProveedores
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.ActualizarProcesoProveedores(Anyo, Cod, GMN1, oProcesoProves)
    End Function

    '<summary>Devuelve los proveedores a publicar</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param> 
    '<param name="Equipo">Equipo</param> 
    '<param name="Com">Com</param> 
    '<returns>Objeto Proveedores con los datos de los proveedores</returns>

    Public Function DevolverProveedoresAPublicar(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, Optional ByVal Equipo As String = Nothing, Optional ByVal Com As String = Nothing) As GSServerModel.Proveedores Implements IGSProcesosServer.DevolverProveedoresAPublicar
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverProveedoresAPublicar(Anyo, Proce, GMN1, Equipo, Com)
    End Function

#End Region

#Region " ProcesoGrupos "

    '<summary>Devuelve los datos de grupos de un proceso</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<param name="DevolverEstructura">Si hay que devolver los items del grupo o no</param>
    '<returns>Array de objetos ProcesoGrupo con los datos de los grupos</returns>

    Public Function DevolverProcesoGrupos(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal DevolverEstructura As Boolean) As GSServerModel.ProcesoGrupos Implements IGSProcesosServer.DevolverProcesoGrupos
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverProcesoGrupos(Anyo, Cod, GMN1, DevolverEstructura)
    End Function

#End Region

#Region " ProcesoGrupoItem "

    '<summary>Devuelve los items de un grupo de un proceso</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<param name="ID">ID</param>
    '<returns>Array de objetos ProcesoGrupo con los datos de los grupos</returns>

    Public Function DevolverProcesoGrupoItems(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal ID As Integer) As GSServerModel.Items Implements IGSProcesosServer.DevolverProcesoGrupoItems
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverProcesoGrupoItems(Anyo, Cod, GMN1, ID)
    End Function

#End Region

#Region " Proveedores "

    Public Function DevolverProveedorContactos(ByVal Prove As String) As GSServerModel.Contactos Implements IGSProcesosServer.DevolverProveedorContactos
        Dim oProve As New GSServer.ProveedoresRule(GetContext)
        Return oProve.DevolverProveedorContactos(Prove)
    End Function

    Public Function DevolverEstadoProveedorPortal(ByVal Prove As String, ByVal Cia As Integer) As GSServerModel.EstadoProveedorPortal Implements IGSProcesosServer.DevolverEstadoProveedorPortal
        Dim oProve As New GSServer.ProveedoresRule(GetContext)
        Return oProve.DevolverEstadoProveedorPortal(Prove, Cia)
    End Function

    Public Function ComprobarEmailEnPortal(ByVal CodProvePortal As String, ByVal EMail As String, ByVal FSP_SRV As String, ByVal FSP_BD As String) As Boolean Implements IGSProcesosServer.ComprobarEmailEnPortal
        Dim oProve As New GSServer.ProveedoresRule(GetContext)
        Return oProve.ComprobarEmailEnPortal(CodProvePortal, EMail, FSP_SRV, FSP_BD)
    End Function

#End Region

#Region " Asignaciones "

    Public Function DevolverAsignacionesProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal CriterioOrdenacion As Fullstep.FSNLibrary.TiposDeDatos.TipoOrdenacionAsignaciones = -1, Optional ByVal CodEqp As String = Nothing, Optional ByVal CodComp As String = Nothing, Optional ByVal CodProve As String = Nothing, Optional ByVal DevolverEstructura As Boolean = False) As GSServerModel.Asignaciones Implements IGSProcesosServer.DevolverAsignacionesProceso
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverAsignacionesProceso(Anyo, Cod, GMN1, CriterioOrdenacion, CodEqp, CodComp, CodProve, DevolverEstructura)
    End Function

#End Region

#Region " Equipos "

    Public Function DevolverCompradoresEquipo(ByVal Equipo As String, Optional ByVal NumMax As Integer = -1, Optional ByVal CarIniCod As String = Nothing, Optional ByVal CarIniApel As String = Nothing, Optional ByVal NoBajaLog As Boolean = False, Optional ByVal OrderByApe As Boolean = False) As GSServerModel.Compradores Implements IGSProcesosServer.DevolverCompradoresEquipo
        Dim oEqp As New GSServer.EquiposRule(GetContext)
        Return oEqp.DevolverCompradoresEquipo(Equipo, NumMax, CarIniCod, CarIniApel, NoBajaLog, OrderByApe)
    End Function

#End Region

#Region " Reglas de subasta "

    '<summary>Devuelve las datos de reglas de una subasta</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<returns>Array de objetos ProcesoSubastaRegla con los datos de las reglas</returns>

    Public Function DevolverProcesoSubastaReglas(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProcesoSubastaReglas Implements IGSProcesosServer.DevolverProcesoSubastaReglas
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverProcesoSubastaReglas(Anyo, Cod, GMN1)
    End Function

#End Region

#Region " Notificaciones "

    Public Function ComponerCorreosNotificacionSubasta(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String, ByVal InsWEB As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, _
                                                       ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal DirPlantilla As String, ByVal From As String, ByVal MailSubject As String, ByVal URLWeb As String) As GSServerModel.DatosNotifSubasta Implements IGSProcesosServer.ComponerCorreosNotificacionSubasta
        Dim oNotif As New GSNotificador.Notificar(GetContext)
        Return oNotif.ComponerCorreosNotificacionSubasta(Anyo, Codigo, GMN1, InsWEB, FSP_CIA, FSP_SRV, FSP_BD, DirPlantilla, From, MailSubject, URLWeb)
    End Function

    Public Function NotificacionSubasta(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal InsWEB As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal oDatos As GSServerModel.DatosNotifSubasta, ByVal bUsarRemitenteEmpresa As Boolean) As GSServerModel.GSException Implements IGSProcesosServer.NotificacionSubasta
        Dim oNotif As New GSNotificador.Notificar(GetContext)
        Return oNotif.NotificacionSubasta(Anyo, Cod, GMN1, InsWEB, FSP_CIA, FSP_SRV, FSP_BD, oDatos, bUsarRemitenteEmpresa)
    End Function

    Public Function ComponerCorreosNotificacionEventoSubasta(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String, ByVal InsWEB As Fullstep.FSNLibrary.TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal DirPlantilla As String, ByVal sFrom As String, ByVal oEvento As GSServerModel.ProcesoSubastaEvento, ByVal oEveDesc As System.Collections.Generic.Dictionary(Of String, String)) As GSServerModel.EMails Implements IGSProcesosServer.ComponerCorreosNotificacionEventoSubasta
        Dim oNotif As New GSNotificador.Notificar(GetContext)
        Return oNotif.ComponerCorreosNotificacionEventoSubasta(Anyo, Codigo, GMN1, InsWEB, FSP_CIA, FSP_SRV, FSP_BD, DirPlantilla, sFrom, oEvento, oEveDesc)
    End Function

    Public Function NotificacionEventoSubasta(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oEMails As GSServerModel.EMails) As GSServerModel.GSException Implements IGSProcesosServer.NotificacionEventoSubasta
        Dim oNotif As New GSNotificador.Notificar(GetContext)
        Return oNotif.NotificacionEventoSubasta(Anyo, Cod, GMN1, oEMails)
    End Function

#End Region

    '<summary>Recupera la info. de contexto de la cabecera del mensaje y la guarda en la memoria del thread</summary>        

    Private Function GetContext() As GSServerModel.Contexto
        'Obtener el contexto de la cabecera de mensaje
        Dim oCtx As GSServerModel.Contexto
        If OperationContext.Current.IncomingMessageHeaders.FindHeader("Contexto", "urn:FSN.GSHeaderContexto") > -1 Then
            oCtx = OperationContext.Current.IncomingMessageHeaders.GetHeader(Of GSServerModel.Contexto)("Contexto", "urn:FSN.GSHeaderContexto")
        End If

        'Añadir la info. de contexto a la memoria del thread
        'If Not oCtx Is Nothing Then
        '    Dim oSlot As System.LocalDataStoreSlot = System.Threading.Thread.GetNamedDataSlot("Contexto")
        '    System.Threading.Thread.SetData(oSlot, oCtx)
        'End If

        ''Add the content of the header to the IncomingMessageProperties dictionary
        'OperationContext.Current.IncomingMessageProperties.Add("key", oCtx)

        Return oCtx
    End Function

#End Region

#Region "IGSFileStreamServer"

#Region " ProcesoSubastaReglas "

    '<summary>Actualiza las reglas de subasta de un proceso</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param> 
    '<param name="arSubastaReglas">Array de objetos ProcesoSubastaRegla con las reglas</param> 
    '<returns>Objeto de tipo GSException con el error si ha tenido lugar</returns>

    Public Function ActualizarProcesoSubastaReglas(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oSubastaReglas As GSServerModel.ProcesoSubastaReglas, ByVal oArchivos As GSServerModel.Files) As GSServerModel.GSException Implements IGSFileStreamServer.ActualizarProcesoSubastaReglas
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.ActualizarProcesoSubastaReglas(Anyo, Cod, GMN1, oSubastaReglas, oArchivos)
    End Function

    '<summary>Devuelve un chunk del archivo correspondiente a una regla</summary>
    '<param name="Offset">Offset</param>    
    '<param name="ChunkSize">ChunkSize</param>    
    '<param name="ID">ID del archivo</param>        
    '<returns>array de bytes con el trozo de archivo pedido</returns>

    Public Function DevolverArchivoSubastaRegla(ByVal Offset As Integer, ByVal ChunkSize As Integer, ByVal ID As Integer) As Byte() Implements IGSFileStreamServer.DevolverArchivoSubastaRegla
        Dim oProce As New GSServer.ProcesosRule(GetContext)
        Return oProce.DevolverArchivoSubastaRegla(Offset, ChunkSize, ID)
    End Function

#End Region

#End Region

#Region "IGSFileTransferServer "

    Public Function BeginFileTransfer() As String Implements IGSFileTransferServer.BeginFileTransfer
        Dim sRootFolder As String = System.Configuration.ConfigurationManager.AppSettings("UploadFolder")

        Dim oFT As New GSServer.FileTransfer
        '_sFolder = oFT.BeginFileTransfer(sRootFolder)
        'Return _sFolder
        Return oFT.BeginFileTransfer(sRootFolder)
    End Function

    Public Function SendFileChunk(ByVal FileName As String, ByVal sFolder As String, ByVal Buffer() As Byte) As Boolean Implements IGSFileTransferServer.SendFileChunk
        Dim sRootFolder As String = System.Configuration.ConfigurationManager.AppSettings("UploadFolder")
        If Not System.IO.Directory.Exists(sRootFolder) Then System.IO.Directory.CreateDirectory(sRootFolder)
        If Not sRootFolder.EndsWith("\") Then sRootFolder &= "\"

        Dim oFT As New GSServer.FileTransfer
        'Return oFT.SendFileChunk(sRootFolder & _sFolder, FileName, Buffer)
        Return oFT.SendFileChunk(sRootFolder & sFolder, FileName, Buffer)
    End Function

    Public Function EndFileTransfer(ByVal sFolder As String) As Boolean Implements IGSFileTransferServer.EndFileTransfer
        Dim sRootFolder As String = System.Configuration.ConfigurationManager.AppSettings("UploadFolder")
        If Not sRootFolder.EndsWith("\") Then sRootFolder &= "\"

        Dim oFT As New GSServer.FileTransfer
        'Return oFT.EndFileTransfer(sRootFolder & _sFolder)
        Return oFT.EndFileTransfer(sRootFolder & sFolder)
    End Function

#End Region

End Class
