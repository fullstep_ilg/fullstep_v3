﻿Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.ServiceModel
Imports System.ServiceModel.Activation


<ServiceBehavior(), AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Required)> _
Public Class GSMonedasServer
    Implements IGSMonedasServer


#Region "Monedas data access methods"

    Public Function DevolverTodasLasMonedas(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As DataSet Implements IGSMonedasServer.DevolverTodasLasMonedas
        Dim oMonedasRule As New GSServer.MonedasRule()
        Return oMonedasRule.CargarTodasLasMonedas(bMostrarIntegracion, sIdioma)
    End Function

    Public Function DevolverTodosLosIdiomas() As GSServerModel.Idiomas Implements IGSMonedasServer.DevolverTodosLosIdiomas
        Dim oIdiomasRule As New GSServer.IdiomasRule()
        Return oIdiomasRule.CargarTodosLosIdiomas()
    End Function

    Public Function ActualizarMoneda(ByRef Moneda As GSServerModel.Moneda, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.ActualizarMoneda
        Dim oMonedasRule As New GSServer.MonedasRule()
        Return oMonedasRule.ActualizarMoneda(Moneda, bRealizarCambioCodigo, iInstWeb, bActivLog, bIntegracionMonedas, FSP, sUsuario)
    End Function

    Public Function AnyadirMoneda(ByRef Moneda As GSServerModel.Moneda, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.AnyadirMoneda
        Dim oMonedasRule As New GSServer.MonedasRule()
        Return oMonedasRule.AnyadirMoneda(Moneda, iInstWeb, bActivLog, bIntegracionMonedas, FSP, sUsuario)
    End Function

    Public Function EliminarMonedas(ByVal Codigos() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sMonedaCentral As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.EliminarMonedas
        Dim oMonedasRule As New GSServer.MonedasRule()
        Return oMonedasRule.EliminarMonedas(Codigos, iInstWeb, bActivLog, bIntegracionMonedas, FSP, sMonedaCentral, sUsuario)
    End Function

    Function CambioCodigoMoneda(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer Implements IGSMonedasServer.CambioCodigoMoneda
        Dim oMonedasRule As New GSServer.MonedasRule()
        Return oMonedasRule.CambioCodigoMoneda(Codigo, CodigoNuevo, Usuario)
    End Function

    Public Function DevolverMoneda(ByVal Moneda As String, ByVal Idioma As String) As GSServerModel.Moneda Implements IGSMonedasServer.DevolverMoneda
        Dim oMonedasRule As New GSServer.MonedasRule()
        Return oMonedasRule.DevolverMoneda(Moneda, Idioma)
    End Function

#End Region

    Function ComprobarSession(ByVal sUsu As String, ByVal Session As Long) As Boolean Implements IGSMonedasServer.ComprobarSession
        Dim oUsu As New GSServer.UsuarioRule()
        Return oUsu.ComprobarSession(sUsu, Session)
    End Function

    Function CargarDatosUsu(ByVal sUsu As String, ByVal sIdi As String) As GSServerModel.Usuario Implements IGSMonedasServer.CargarDatosUsu
        Dim oUsu As New GSServer.UsuarioRule()
        Return oUsu.CargarDatosUsu(sUsu, sIdi)

    End Function

    Public Function DevolverPersona(ByVal sCodigo As String) As GSServerModel.Persona Implements IGSMonedasServer.DevolverPersona
        Dim oUsu As New GSServer.UsuarioRule()
        Return oUsu.DevolverPersona(sCodigo)
    End Function

    Function CargarParametrosGenerales(ByRef ParametrosGenerales As GSServerModel.ParametrosGenerales, ByRef LongitudesDeCodigo As GSServerModel.LongitudesCodigo) As Object Implements IGSMonedasServer.CargarParametrosGenerales
        Dim oParametrosRule As New GSServer.ParametrosRule()
        Return oParametrosRule.CargarParametrosGenerales(ParametrosGenerales, LongitudesDeCodigo)
    End Function

    Function CargarParametrosInstalacion(ByVal ParametrosGenerales As GSServerModel.ParametrosGenerales, ByRef ParametrosInstalacion As GSServerModel.ParametrosInstalacion, ByVal sUsu As String) As Object Implements IGSMonedasServer.CargarParametrosInstalacion
        Dim oParametrosRule As New GSServer.ParametrosRule()
        Return oParametrosRule.CargarParametrosInstalacion(ParametrosGenerales, ParametrosInstalacion, sUsu)
    End Function

    Function CargarParametrosIntegracion(ByRef ParametrosIntegracion As GSServerModel.ParametrosIntegracion, ByVal ParametrosGenerales As GSServerModel.ParametrosGenerales) As Object Implements IGSMonedasServer.CargarParametrosIntegracion
        Dim oParametrosRule As New GSServer.ParametrosRule()
        Return oParametrosRule.CargarParametrosIntegracion(ParametrosIntegracion, ParametrosGenerales)
    End Function

#Region "Unidades data access methods"

    ''Unidades
    Public Function DevolverTodasLasUnidades(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As DataSet Implements IGSMonedasServer.DevolverTodasLasUnidades
        Dim oUnidadesRule As New GSServer.UnidadesRule()
        Return oUnidadesRule.CargarTodasLasUnidades(bMostrarIntegracion, sIdioma)
    End Function

    Public Function ActualizarUnidad(ByRef oUnidad As GSServerModel.Unidad, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.ActualizarUnidad
        Dim oUnidadesRule As New GSServer.UnidadesRule()
        Return oUnidadesRule.ActualizarUnidad(oUnidad, bRealizarCambioCodigo, bActivLog, bIntegracionUnidades, FSP, sUsuario)
    End Function

    Public Function AnyadirUnidad(ByRef oUnidad As GSServerModel.Unidad, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.AnyadirUnidad
        Dim oUnidadesRule As New GSServer.UnidadesRule()
        Return oUnidadesRule.AnyadirUnidad(oUnidad, bActivLog, bIntegracionUnidades, FSP, sUsuario)
    End Function

    Public Function EliminarUnidades(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.EliminarUnidades
        Dim oUnidadesRule As New GSServer.UnidadesRule()
        Return oUnidadesRule.EliminarUnidades(Codigos, bActivLog, bIntegracionUnidades, FSP, sUsuario)
    End Function
    Public Function CambioCodigoUnidad(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer Implements IGSMonedasServer.CambioCodigoUnidad
        Dim oUnidadesRule As New GSServer.UnidadesRule()
        Return oUnidadesRule.CambioCodigoUnidad(Codigo, CodigoNuevo, Usuario)
    End Function


    Public Function RegistrarEntradaOSalida(ByVal sUsuario As String, ByVal iAccion As Integer, ByVal Descripcion As String) As Boolean Implements IGSMonedasServer.RegistrarEntradaOSalida
        Dim oUsu As New GSServer.UsuarioRule()
        Return oUsu.RegistrarEntradaOSalida(sUsuario, iAccion, Descripcion)

    End Function

#End Region



#Region "Vias de pago data access methods"

    ''Vias de Pago

    Public Function DevolverTodasLasViasPago(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As DataSet Implements IGSMonedasServer.DevolverTodasLasViasPago
        Dim oViaPago As New GSServer.ViasPagoRule()
        Return oViaPago.CargarTodasLasViasPago(bMostrarIntegracion, sIdioma)

    End Function

    Public Function AnyadirViaPago(ByRef oViaPago As GSServerModel.ViaPago, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.AnyadirViaPago
        Dim oViasPagoRule As New GSServer.ViasPagoRule()
        Return oViasPagoRule.AnyadirViaPago(oViaPago, bActivLog, bIntegracionViasPago, FSP, sUsuario)
    End Function

    Public Function ActualizarViaPago(ByRef oViaPago As GSServerModel.ViaPago, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.ActualizarViaPago
        Dim oViasPagoRule As New GSServer.ViasPagoRule()
        Return oViasPagoRule.ActualizarViaPago(oViaPago, bRealizarCambioCodigo, bActivLog, bIntegracionViasPago, FSP, sUsuario)
    End Function

    Public Function EliminarViasPago(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.EliminarViasPago
        Dim oViasPagoRule As New GSServer.ViasPagoRule()
        Return oViasPagoRule.EliminarViasPago(Codigos, bActivLog, bIntegracionViasPago, FSP, sUsuario)
    End Function

    Public Function CambioCodigoViaPago(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer Implements IGSMonedasServer.CambioCodigoViaPago
        Dim oViasPagoRule As New GSServer.ViasPagoRule()
        Return oViasPagoRule.CambioCodigoViaPago(Codigo, CodigoNuevo, Usuario)

    End Function
#End Region
#Region "Formas de pago data access methods"

    Public Function DevolverTodasLasFormasPago(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As DataSet Implements IGSMonedasServer.DevolverTodasLasFormasPago
        Dim oFormaPago As New GSServer.FormasPagoRule()
        Return oFormaPago.CargarTodasLasFormasPago(bMostrarIntegracion, sIdioma)

    End Function

    Public Function AnyadirFormaPago(ByRef oFormaPago As GSServerModel.FormaPago, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.AnyadirFormaPago
        Dim oFormasPagoRule As New GSServer.FormasPagoRule()
        Return oFormasPagoRule.AnyadirFormaPago(oFormaPago, bActivLog, bIntegracionFormasPago, FSP, sUsuario)
    End Function

    Public Function ActualizarFormaPago(ByRef oFormaPago As GSServerModel.FormaPago, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.ActualizarFormaPago
        Dim oFormasPagoRule As New GSServer.FormasPagoRule()
        Return oFormasPagoRule.ActualizarFormaPago(oFormaPago, bRealizarCambioCodigo, bActivLog, bIntegracionFormasPago, FSP, sUsuario)
    End Function

    Public Function EliminarFormasPago(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.EliminarFormasPago
        Dim oFormasPagoRule As New GSServer.FormasPagoRule()
        Return oFormasPagoRule.EliminarFormasPago(Codigos, bActivLog, bIntegracionFormasPago, FSP, sUsuario)
    End Function

    Public Function CambioCodigoFormaPago(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer Implements IGSMonedasServer.CambioCodigoFormaPago
        Dim oFormasPagoRule As New GSServer.FormasPagoRule()
        Return oFormasPagoRule.CambioCodigoFormaPago(Codigo, CodigoNuevo, Usuario)

    End Function

#End Region

    Public Function DevolverTodosLosPaisesProvincias(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As DataSet Implements IGSMonedasServer.DevolverTodosLosPaisesProvincias
        Dim oPaisesRule As New GSServer.PaisesRule()
        Return oPaisesRule.CargarTodosLosPaisesProvincias(bMostrarIntegracion, sIdioma)
    End Function

    Public Function DevolverMonedasPais(ByVal sIdioma As String) As DataSet Implements IGSMonedasServer.DevolverMonedasPais
        Dim oPaisesRule As New GSServer.PaisesRule()
        Return oPaisesRule.CargarTodasLasMonedasPais(sIdioma)
    End Function


    Public Function AnyadirPais(ByRef Pais As GSServerModel.Pais, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.AnyadirPais
        Dim oPaisesRule As New GSServer.PaisesRule()
        Return oPaisesRule.AnyadirPais(Pais, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario)

    End Function

    Public Function AnyadirProvincias(ByRef Provincia As GSServerModel.Provincia, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.AnyadirProvincia
        Dim oPaisesRule As New GSServer.PaisesRule()
        Return oPaisesRule.AnyadirProvincia(Provincia, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario)

    End Function

    Public Function ActualizarPais(ByRef Pais As GSServerModel.Pais, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.ActualizarPais
        Dim oPaisesRule As New GSServer.PaisesRule()
        Return oPaisesRule.ActualizarPais(Pais, bRealizarCambioCodigo, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario)
    End Function
    Public Function ActualizarProvincia(ByRef Provincia As GSServerModel.Provincia, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException Implements IGSMonedasServer.ActualizarProvincia
        Dim oPaisesRule As New GSServer.PaisesRule()
        Return oPaisesRule.ActualizarProvincia(Provincia, bRealizarCambioCodigo, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario)
    End Function

    Public Function CambioCodigoPais(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer Implements IGSMonedasServer.CambioCodigoPais
        Dim oPaisesRule As New GSServer.PaisesRule()
        Return oPaisesRule.CambioCodigoPais(Codigo, CodigoNuevo, Usuario)

    End Function

    Public Function CambioCodigoProvincia(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String, ByVal sPais As String) As Integer Implements IGSMonedasServer.CambioCodigoProvincia
        Dim oPaisesRule As New GSServer.PaisesRule()
        Return oPaisesRule.CambioCodigoProvincia(Codigo, CodigoNuevo, Usuario, sPais)

    End Function

    Public Function EliminarProvincias(ByVal CodigosProvincias() As String, ByVal CodigosPaisProvi() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String, ByVal sIdioma As String) As GSServerModel.GSException Implements IGSMonedasServer.EliminarProvincias
        Dim oPaisesRule As New GSServer.PaisesRule()
        Return oPaisesRule.EliminarProvincias(CodigosProvincias, CodigosPaisProvi, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario, sIdioma)

    End Function
    Public Function EliminarPaises(ByVal CodigosPaises() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String, ByVal sIdioma As String) As GSServerModel.GSException Implements IGSMonedasServer.EliminarPaises
        Dim oPaisesRule As New GSServer.PaisesRule()
        Return oPaisesRule.EliminarPaises(CodigosPaises, iInstWeb, bActivLog, bIntegracionPaises, FSP, sUsuario, sIdioma)
    End Function

#Region " Impuestos "

    Public Function DevolverDatosImpuestos(ByVal sIdioma As String) As GSServerModel.ImpuestosData Implements IGSMonedasServer.DevolverDatosImpuestos
        Dim oImpuestosRule As New GSServer.ImpuestosRule()
        Return oImpuestosRule.DevolverDatosImpuestos(sIdioma)
    End Function

    Public Function DevolverImpuestos(ByVal sIdioma As String) As GSServerModel.Impuestos Implements IGSMonedasServer.DevolverImpuestos
        Dim oImpuestosRule As New GSServer.ImpuestosRule()
        Return oImpuestosRule.DevolverImpuestos(sIdioma)
    End Function

    Public Function InsertarImpuesto(ByRef oImpuesto As GSServerModel.Impuesto) As GSServerModel.GSException Implements IGSMonedasServer.InsertarImpuesto
        Dim oImpuestosRule As New GSServer.ImpuestosRule()
        Return oImpuestosRule.InsertarImpuesto(oImpuesto)
    End Function

    Public Function EliminarImpuestos(ByVal IDs() As Integer) As GSServerModel.GSException Implements IGSMonedasServer.EliminarImpuestos
        Dim oImpuestosRule As New GSServer.ImpuestosRule()
        Return oImpuestosRule.EliminarImpuestos(IDs)
    End Function

    Public Function ActualizarImpuesto(ByRef oImpuesto As GSServerModel.Impuesto) As GSServerModel.GSException Implements IGSMonedasServer.ActualizarImpuesto
        Dim oImpuestosRule As New GSServer.ImpuestosRule()
        Return oImpuestosRule.ActualizarImpuesto(oImpuesto)
    End Function

    Public Function InsertarImpuestoValor(ByVal IdImpuesto As Integer, ByRef oImpuestoValor As GSServerModel.ImpuestoValor) As GSServerModel.GSException Implements IGSMonedasServer.InsertarImpuestoValor
        Dim oImpuestosRule As New GSServer.ImpuestosRule()
        Return oImpuestosRule.InsertarImpuestoValor(IdImpuesto, oImpuestoValor)
    End Function

    Public Function ActualizarImpuestoValor(ByRef oImpuestoValor As GSServerModel.ImpuestoValor) As GSServerModel.GSException Implements IGSMonedasServer.ActualizarImpuestoValor
        Dim oImpuestosRule As New GSServer.ImpuestosRule()
        Return oImpuestosRule.ActualizarImpuestoValor(oImpuestoValor)
    End Function

    Public Function EliminarImpuestoValores(ByVal IDs() As Integer) As GSServerModel.GSException Implements IGSMonedasServer.EliminarImpuestoValores
        Dim oImpuestosRule As New GSServer.ImpuestosRule()
        Return oImpuestosRule.EliminarImpuestoValores(IDs)
    End Function
#End Region

#Region " Materiales "

    Public Function DevolverEstructuraMaterialesImpuestos(ByVal iIdImpuestoValor As Integer, ByVal sIdioma As String, Optional ByVal sCodEqpComprador As String = Nothing, Optional ByVal sCodComprador As String = Nothing, Optional ByVal bOrdPorCod As Boolean = False) As DataSet Implements IGSMonedasServer.DevolverEstructuraMaterialesImpuestos
        Dim oMaterialesRule As New GSServer.MaterialesRule()
        Return oMaterialesRule.DevolverEstructuraMaterialesImpuestos(iIdImpuestoValor, sIdioma, sCodEqpComprador, sCodComprador, bOrdPorCod)
    End Function

    Public Function ActualizarEstructuraMaterialesImpuestos(ByVal _iIdImpuestoValor As Integer, ByVal oAsignar As GSServerModel.GruposMaterial, ByVal oDesasignar As GSServerModel.GruposMaterial, _
        ByVal oArtAsignar As GSServerModel.Articulos, ByVal oArtDesasignar As GSServerModel.Articulos, ByVal oArtIncluir As GSServerModel.Articulos, ByVal oArtExcluir As GSServerModel.Articulos) As GSServerModel.GSException Implements IGSMonedasServer.ActualizarEstructuraMaterialesImpuestos
        Dim oMaterialesRule As New GSServer.MaterialesRule()
        Return oMaterialesRule.ActualizarEstructuraMaterialesImpuestos(_iIdImpuestoValor, oAsignar, oDesasignar, oArtAsignar, oArtDesasignar, oArtIncluir, oArtExcluir)
    End Function

    Public Function DevolverArticulosGMNImpuestos(ByVal IdImpuestoValor As Integer, ByVal oGMN As GSServerModel.GrupoMaterial) As GSServerModel.Articulos Implements IGSMonedasServer.DevolverArticulosGMNImpuestos
        Dim oMaterialesRule As New GSServer.MaterialesRule()
        Return oMaterialesRule.DevolverArticulosGMNImpuestos(IdImpuestoValor, oGMN)
    End Function

#End Region

#Region "Subasta"
    Public Function UTCdeBD() As Date Implements IGSMonedasServer.UTCdeBD
        Dim oParametrosRule As New GSServer.ParametrosRule()
        Return oParametrosRule.UTCdeBD()
    End Function
#End Region

End Class

