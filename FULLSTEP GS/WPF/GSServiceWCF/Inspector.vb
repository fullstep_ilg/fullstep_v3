﻿Imports System.ServiceModel
Imports System.ServiceModel.Dispatcher
Imports System.ServiceModel.Description
Imports System.ServiceModel.Configuration

'Inspector de mensajes
Public Class CustomMessageInspector
    Implements IDispatchMessageInspector

    Public Function AfterReceiveRequest(ByRef request As System.ServiceModel.Channels.Message, ByVal channel As System.ServiceModel.IClientChannel, ByVal instanceContext As System.ServiceModel.InstanceContext) As Object Implements System.ServiceModel.Dispatcher.IDispatchMessageInspector.AfterReceiveRequest
        'Look for my custom header in the request
        Dim oCtx As GSServerModel.Contexto
        If OperationContext.Current.IncomingMessageHeaders.FindHeader("Contexto", "urn:FSN.GSHeaderContexto") > 0 Then
            oCtx = OperationContext.Current.IncomingMessageHeaders.GetHeader(Of GSServerModel.Contexto)("Contexto", "urn:FSN.GSHeaderContexto")
        End If

        ' // Get an XmlDictionaryReader to read the header content
        ' XmlDictionaryReader reader = request.Headers.GetReaderAtHeader(headerPosition); 

        ' // Read it through its static method ReadHeader
        ' CustomHeader header = CustomHeader.ReadHeader(reader); 

        ' // Add the content of the header to the IncomingMessageProperties dictionary
        'OperationContext.Current.IncomingMessageProperties.Add("key", header.Key); 


        Return Nothing
    End Function

    Public Sub BeforeSendReply(ByRef reply As System.ServiceModel.Channels.Message, ByVal correlationState As Object) Implements System.ServiceModel.Dispatcher.IDispatchMessageInspector.BeforeSendReply

    End Sub
End Class

'Behaviour (para añadir el inspector de mensajes)
'<AttributeUsage(AttributeTargets.Class)> _
'Public Class CustomBehavior
'    Implements IServiceBehavior

'    Public Sub AddBindingParameters(ByVal serviceDescription As System.ServiceModel.Description.ServiceDescription, ByVal serviceHostBase As System.ServiceModel.ServiceHostBase, ByVal endpoints As System.Collections.ObjectModel.Collection(Of System.ServiceModel.Description.ServiceEndpoint), ByVal bindingParameters As System.ServiceModel.Channels.BindingParameterCollection) Implements System.ServiceModel.Description.IServiceBehavior.AddBindingParameters
'        Return
'    End Sub

'    Public Sub ApplyDispatchBehavior(ByVal serviceDescription As System.ServiceModel.Description.ServiceDescription, ByVal serviceHostBase As System.ServiceModel.ServiceHostBase) Implements System.ServiceModel.Description.IServiceBehavior.ApplyDispatchBehavior
'        For Each chDisp As ChannelDispatcher In serviceHostBase.ChannelDispatchers
'            For Each epDisp As EndpointDispatcher In chDisp.Endpoints
'                epDisp.DispatchRuntime.MessageInspectors.Add(New CustomMessageInspector())
'                For Each op As DispatchOperation In epDisp.DispatchRuntime.Operations
'                    op.ParameterInspectors.Add(New CustomMessageInspector())
'                Next op
'            Next epDisp
'        Next chDisp

'    End Sub

'    Public Sub Validate(ByVal serviceDescription As System.ServiceModel.Description.ServiceDescription, ByVal serviceHostBase As System.ServiceModel.ServiceHostBase) Implements System.ServiceModel.Description.IServiceBehavior.Validate

'    End Sub
'End Class

<AttributeUsage(AttributeTargets.Class)> _
Public Class CustomBehavior
    Implements IEndpointBehavior

    Public Sub AddBindingParameters(ByVal endpoint As System.ServiceModel.Description.ServiceEndpoint, ByVal bindingParameters As System.ServiceModel.Channels.BindingParameterCollection) Implements System.ServiceModel.Description.IEndpointBehavior.AddBindingParameters

    End Sub

    Public Sub ApplyClientBehavior(ByVal endpoint As System.ServiceModel.Description.ServiceEndpoint, ByVal clientRuntime As System.ServiceModel.Dispatcher.ClientRuntime) Implements System.ServiceModel.Description.IEndpointBehavior.ApplyClientBehavior
        Dim inspector As New CustomMessageInspector()
        clientRuntime.MessageInspectors.Add(inspector)
    End Sub

    Public Sub ApplyDispatchBehavior(ByVal endpoint As System.ServiceModel.Description.ServiceEndpoint, ByVal endpointDispatcher As System.ServiceModel.Dispatcher.EndpointDispatcher) Implements System.ServiceModel.Description.IEndpointBehavior.ApplyDispatchBehavior
        Dim channelDispatcher As ChannelDispatcher = endpointDispatcher.ChannelDispatcher
        If Not channelDispatcher Is Nothing Then
            For Each ed As EndpointDispatcher In channelDispatcher.Endpoints
                Dim inspector As New CustomMessageInspector()
                ed.DispatchRuntime.MessageInspectors.Add(inspector)
            Next
        End If
    End Sub

    Public Sub Validate(ByVal endpoint As System.ServiceModel.Description.ServiceEndpoint) Implements System.ServiceModel.Description.IEndpointBehavior.Validate

    End Sub
End Class

Public Class CustomBehaviorExtensionElement
    Inherits BehaviorExtensionElement

    Public Overrides ReadOnly Property BehaviorType As System.Type
        Get
            Return GetType(CustomBehavior)
        End Get
    End Property

    Protected Overrides Function CreateBehavior() As Object
        Return New CustomBehavior        
    End Function

End Class




'<DataContract()> _
'Public Class InstanceCreationExtension
'    Implements IExtension(Of InstanceContext)

'    Private _oContexto As GSServerModel.Contexto

'    Public Sub New(ByVal Contexto As GSServerModel.Contexto)
'        _oContexto = Contexto
'    End Sub

'    <DataMember()> _
'    Public Property Contexto As GSServerModel.Contexto
'        Get
'            Return _oContexto
'        End Get
'        Set(ByVal value As GSServerModel.Contexto)
'            _oContexto = value
'        End Set
'    End Property

'    Public Sub Attach(ByVal owner As System.ServiceModel.InstanceContext) Implements System.ServiceModel.IExtension(Of System.ServiceModel.InstanceContext).Attach

'    End Sub

'    Public Sub Detach(ByVal owner As System.ServiceModel.InstanceContext) Implements System.ServiceModel.IExtension(Of System.ServiceModel.InstanceContext).Detach

'    End Sub
'End Class

'Public Class InstanceCreationInitializer
'    Implements IInstanceContextInitializer

'    Public Sub Initialize(ByVal instanceContext As System.ServiceModel.InstanceContext, ByVal message As System.ServiceModel.Channels.Message) Implements System.ServiceModel.Dispatcher.IInstanceContextInitializer.Initialize
'        'Add extension which contains the new instance creation index
'        'instanceContext.Extensions.Add(New InstanceCreationExtension(m_InstanceCreationIndex, DateTime.Now))
'    End Sub
'End Class



