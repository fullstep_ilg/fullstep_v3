﻿Imports GSClientServices.GSServerAccessService
Imports System.ServiceModel

Public Class ServiceFactory    

#Region "Singleton"
    Private Shared _instance As New ServiceFactory()

    Public Shared ReadOnly Property Instance() As ServiceFactory
        Get
            Return _instance
        End Get
    End Property

    Private Sub New()

    End Sub
#End Region

    Public Function GetService(Of T)() As T
        Dim result As T

        result = CType(Activator.CreateInstance(GetType(T)), T)

        Return result
    End Function

    Public Function GetFullGSService() As GSClientServices.GSServerAccessService.GSMonedasServerClient
        Return GetService(Of GSClientServices.GSServerAccessService.GSMonedasServerClient)()
        'Dim service As New GSMonedasServerClient()
        'Return service
    End Function

    Public Function GetFullGSProcessService() As GSClientServices.GSWCFReferenceProcesses.GSProcesosServerClient
        Dim oService As GSClientServices.GSWCFReferenceProcesses.GSProcesosServerClient = GetService(Of GSClientServices.GSWCFReferenceProcesses.GSProcesosServerClient)()

        'Obtener el contexto de la memoria del thread        
        Dim oContexto As GSServerModel.Contexto = System.Threading.Thread.GetData(System.Threading.Thread.GetNamedDataSlot("Contexto"))
        If Not oContexto Is Nothing Then        
            '--- Inluir un header en cada llamada con datos del usuario
            Dim oScope As New OperationContextScope(oService.InnerChannel)
            'Añadir el nuevo header con el contexto           
            Dim header As MessageHeader(Of GSServerModel.Contexto) = New MessageHeader(Of GSServerModel.Contexto)(oContexto)
            OperationContext.Current.OutgoingMessageHeaders.Add(header.GetUntypedHeader("Contexto", "urn:FSN.GSHeaderContexto"))
        End If

        Return oService
    End Function

    Public Function GetFullGSFileStreamService() As GSClientServices.GSWCFReferenceProcesses.GSFileStreamServerClient
        Dim oService As GSClientServices.GSWCFReferenceProcesses.GSFileStreamServerClient = GetService(Of GSClientServices.GSWCFReferenceProcesses.GSFileStreamServerClient)()

        'Obtener el contexto de la memoria del thread        
        Dim oContexto As GSServerModel.Contexto = System.Threading.Thread.GetData(System.Threading.Thread.GetNamedDataSlot("Contexto"))
        If Not oContexto Is Nothing Then
            '--- Inluir un header en cada llamada con datos del usuario
            Dim oScope As New OperationContextScope(oService.InnerChannel)
            'Añadir el nuevo header con el contexto           
            Dim header As MessageHeader(Of GSServerModel.Contexto) = New MessageHeader(Of GSServerModel.Contexto)(oContexto)
            OperationContext.Current.OutgoingMessageHeaders.Add(header.GetUntypedHeader("Contexto", "urn:FSN.GSHeaderContexto"))
        End If

        Return oService
    End Function

    Public Function GetFullGSFileTransferService() As GSClientServices.GSWCFReferenceProcesses.GSFileTransferServerClient
        Return GetService(Of GSClientServices.GSWCFReferenceProcesses.GSFileTransferServerClient)()
    End Function

End Class
