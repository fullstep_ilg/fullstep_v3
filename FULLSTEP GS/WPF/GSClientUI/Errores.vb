﻿Imports Fullstep.FSNLibrary.TiposDeDatos
Module Errores

    Public Sub TratarError(ByVal TESErr As GSServerModel.GSException)

        Select TESErr.Number

            Case ErroresGS.TESUsuarioNoValido

                'Mensajes.UsuarioNoAutorizado()

            Case ErroresGS.TESDatoDuplicado

                Mensajes.DatoDuplicado(TESErr.ErrorId)

            Case ErroresGS.TESDatoEliminado

                Mensajes.DatoEliminado(TESErr.ErrorId)

            Case ErroresGS.TESDatoNoValido

                Mensajes.NoValido(TESErr.ErrorId)

            Case ErroresGS.TESImposibleEliminar

                Mensajes.ImposibleEliDatRel(TESErr.ErrorId)

            Case ErroresGS.TESInfModificada

                Mensajes.DatosModificados()

            Case ErroresGS.TESFaltanDatos

                Mensajes.FaltanDatos(TESErr.ErrorId)

                'Case ErroresGS.TESCantDistMayorQueItem

                '    Mensajes.CantDistMayorQueItem()

                'Case ErroresGS.TESValidarSinItems, ErroresGS.TESValidarYaValidado, ErroresGS.TESValidarSelProveSinProve, ErroresGS.TESValidarSelProveYaValidado

                '    Mensajes.ImposibleValidar(TESErr.ErrorId, TESErr.ErrorStr)

                'Case ErroresGS.TESAdjAntDenegada
                '    Mensajes.AdjAnterioresNoValidas()

                'Case ErroresGS.TESVolumenAdjDirSuperado
                '    Mensajes.VolAdjDirectaSuperado(TESErr.ErrorId, TESErr.ErrorStr)

            Case ErroresGS.TESOtroerror

                Mensajes.OtroError(TESErr.ErrorId, TESErr.ErrorStr)

                'Case ErroresGS.TESOfertaAdjudicada
                '    Mensajes.OfertaAdjudicada()

                'Case TESCierreSinPreAdjudicaciones
                '    Mensajes.ImposibleValidar(TESErr.ErrorId)

                'Case ErroresGS.TESImposibleReabrir
                '    Mensajes.ImposibleReabrir(TESErr.ErrorId, TESErr.ErrorStr)

                'Case TESImposibleAdjudicarAProveNoHom
                '    Mensajes.ImposibleValidar(TESErr.ErrorId)

                'Case tesimposiblecambiarfecha
                '    Mensajes.ImposibleCambiarFecha(TESErr.ErrorId)

                'Case ErroresGS.TESProcesoBloqueado
                '    Mensajes.ProcesoBloqueadoPorOtroUsuario(TESErr.ErrorId, TESErr.ErrorStr)

                'Case ErroresGS.TESInfBloqueadaPorOtro
                '    Mensajes.InformacionBloqueadaPorOtroUsuario(TESErr.ErrorId)

                'Case ErroresGS.TESImposibleEliminarAsignacion
                '    Mensajes.ImposibleEliminarAsignacion(TESErr.ErrorId, TESErr.ErrorStr)

                'Case ErroresGS.tesprocesocerrado
                '    Mensajes.ProcesoCerradoPorOtroUsuario()

                'Case ErroresGS.TESImposibleModificarReunion
                '    Mensajes.ImposibleModReunion()

                'Case ErroresGS.TESImposibleAsignarProveedor
                '    Mensajes.ImposibleAsignarProveedor(TESErr.ErrorId)

            Case ErroresGS.TESInfActualModificada
                Mensajes.DatosActualesModificados()

                'Case ErroresGS.TESProvePortalDesautorizado
                '    Mensajes.ImposibleAutorizarProvePortal()

                'Case ErroresGS.TESProvePortalDesautorizadoEnCia
                '    Mensajes.ProvePortalDesautorizadoEnCia()

                'Case ErroresGS.tesciaportaldesautorizada
                '    Mensajes.CiaDesautorizadaEnPortal()

                'Case ErroresGS.TESProveedorHaUsadoElPortal
                '    Mensajes.ProveedorHaUsadoElPortal(TESErr.ErrorId)

                'Case ErroresGS.TESProveedorEsPremium
                '    Mensajes.ProveedorEsPremium(TESErr.ErrorId)

                'Case ErroresGS.TESSelProveFUPProveSinEnlace
                '    Mensajes.FUPExistenProveedoresSinEnlazar(TESErr.ErrorId)

                'Case ErroresGS.TESSelProveBSProveSinEmail
                '    Mensajes.BSExistenProveedoresSinEmail(TESErr.ErrorId)

                'Case ErroresGS.TESDesPublicarPremiumActivo
                '    Mensajes.ImposibleDespublicarProveedorPremiumActivo()

                'Case ErroresGS.TESMail
                '    Select Case TESErr.ErrorId
                '        Case IDSM_ERR_UNDETERMINED
                '            Mensajes.errormail(CargarTextoMensaje(139))
                '        Case IDSM_ERR_NOADDRESSEES
                '            Mensajes.errormail(CargarTexto(84, 10))
                '        Case IDSM_ERR_TOOMANY_ATTACHMENTS
                '            Mensajes.errormail(CargarTextoMensaje(140))
                '        Case IDSM_ERR_INVALID_ATTACHMENT
                '            Mensajes.errormail(CargarTextoMensaje(141))
                '        Case Else

                '            oIdsMail.DecodeError(Err.Number, CInt(TESErr.ErrorStr), iErrDecoded, iErrDecodedAPI, sErrMensaje)
                '            Mensajes.OtroError(iErrDecoded & "-" & iErrDecoded, sErrMensaje)

                '    End Select

                '    'Case erroresgs.TESImposiblePreadjudicar
                '    'Mensajes.ImposiblePreadjudicar TESErr.ErrorId
                'Case ErroresGS.TESImposibleMarcarAprob
                '    Select Case TESErr.ErrorId
                '        Case "USUELI"
                '            Mensajes.ImposibleMarcarAprobador(TESErr.ErrorStr, 226)
                '    End Select
                'Case ErroresGS.TESImposibleEliminarApro
                '    Mensajes.ImposibleEliminarAprobaAprovi(TESErr.ErrorId)

                'Case ErroresGS.TESImposiblePublicarLineaCatProveNoFormaPago
                '    Mensajes.ImposiblePublicarLineaCat(1)

                'Case ErroresGS.TESImposiblePublicarLineaCatProveNoContactoAprov
                '    Mensajes.ImposiblePublicarLineaCat(2)

                'Case ErroresGS.TESImposiblePublicarLineaCatProveNoProvAutorizado
                '    Mensajes.ImposiblePublicarLineaCat(3)

                'Case ErroresGS.TESImposibleAnyadirAdjACatAdjYaEnCat
                '    Mensajes.ImposibleAnyadirAdjACat()

                'Case ErroresGS.TESImposibleEliminarCat
                '    Mensajes.ImposibleEliminarCategoria()

                'Case ErroresGS.TESImposibleAnyadirLineaCatNuevaSubCategoria
                '    Mensajes.ImposibleAnyadirLineaCatNuevaSubCat()

                'Case ErroresGS.TESImposibleAnyadirLineaCatPorSeguridad
                '    Mensajes.ImposibleAnyadirLineaCatPorSeguridad()

                'Case ErroresGS.TESImposibleAbrirProcesoSinPermisoMaterial
                '    Mensajes.ImposibleAbrirProcesoSinPermisoMaterial()

                'Case ErroresGS.TESGrupoCerrado
                '    Mensajes.GrupoCerradoPorOtroUsuario()

                'Case ErroresGS.TESImposibleAnyadirProceFaltanDatosObl
                '    Mensajes.ImposibleAnyadirProceFaltanDatosObl(TESErr.ErrorId)

                'Case ErroresGS.TESImposibleAnyadirProceDesdePlantFaltanDatosObl
                '    Mensajes.ImposibleAnyadirProceDesdePlantFaltanDatosObl(TESErr.ErrorId)

                'Case ErroresGS.TESImposibleEliminarAtributoMantenimiento
                '    Mensajes.ImposibleEliminarAtributoMantenimiento()

                'Case ErroresGS.TESPresupuestoObligatorio
                '    Mensajes.ImposibleModificarConfiguracionDato(TESErr.ErrorId)

                'Case ErroresGS.TESNingunGrupoDef
                '    Mensajes.ImposibleConfigDatoEnGrupo()

                'Case ErroresGS.TESErrorDeTransferenciaDeArchivo
                '    Mensajes.ErrorDeTransferenciaDeArchivo()

                'Case ErroresGS.TESASPUser, ErroresGS.TESASPUserAddUser, ErroresGS.TESASPUserPropiedades
                '    Mensajes.ErrorOriginal(TESErr)

                'Case ErroresGS.TESASPUserRemoteGrupo, ErroresGS.TESADSIExProgramaIni, ErroresGS.TESADSIExHomeDir
                '    Mensajes.ErrorOriginal(TESErr)

                'Case ErroresGS.TESASPUserCambiaPwd, ErroresGS.TESASPUserRemove, ErroresGS.TESASPUserRename
                '    Mensajes.ErrorOriginal(TESErr)

                'Case ErroresGS.TESASPUserCuentaExiste
                '    Mensajes.CuentaUsuarioExistente()

                'Case ErroresGS.TESRunAsUser, ErroresGS.TESMailSMTP
                '    Mensajes.ErrorOriginal(TESErr)
                'Case ErroresGS.TESCambioContrasenya
                '    Mensajes.MensajeOKOnly(568)

                'Case TESCambiaThreshold
                '    Mensajes.MensajeOKOnly(573)

                'Case ErroresGS.TESImposibleAnyadirGrupoFaltanDatosObl
                '    Mensajes.ImposibleAnyadirGrupoFaltanDatosObl(TESErr.ErrorId)

                'Case ErroresGS.TESPedidoProcesoUnoSoloEmitido
                '    Mensajes.ImposibleEmitirPedidoUnoSolo(TESErr.ErrorId, TESErr.ErrorStr)

                'Case ErroresGS.TESTraspasoAdjERP
                '    Mensajes.FaltanDatosTablasERP(TESErr.ErrorId)

                'Case ErroresGS.TESNotifUsuMatQA
                '    Mensajes.FalloNotifUsuMatQA(TESErr.ErrorId)

                'Case ErroresGS.TESValidarPerfilEliminar
                '    Mensajes.ImposibleEliminarPerfil()

                'Case ErroresGS.TESArtGenericoModificado
                '    Mensajes.NoGrabaAperturaGenerico(TESErr.ErrorId)

                'Case ErroresGS.TESProcesoSinMatNoMultimat
                '    Mensajes.ProcesoSinMatNoMultimat()

                'Case ErroresGS.TESAtributoYaMarcadoAPedido
                '    Mensajes.AtributoYaMarcadoAPedido(TESErr.ErrorId)

                'Case ErroresGS.TESImposibleAccederAlArchivoVinculado
                '    Mensajes.ImposibleAccederAlArchivoVinculado(TESErr.ErrorId, TESErr.ErrorStr)

                'Case ErroresGS.TESImposibleValidarPorAtribsOblProvePremium
                '    Mensajes.AtributosEspObl(TESErr.ErrorId, TValidacionAtrib.Publicacion)

                'Case ErroresGS.TESErrorEliminarRecepFacturas
                '    Mensajes.ErrorEliminarRecepFacturas()

        End Select

    End Sub


End Module
