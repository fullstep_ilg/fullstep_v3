﻿Imports System.Windows.Interop
Imports System.Configuration
Imports GSClient

'Imports System
'Imports System.Windows
'Imports System.Deployment
'Imports System.Deployment.Application

Class Application
    Inherits System.Windows.Application

    ' Application events, such as Startup(), Exit(), and DispatcherUnhandledException
    ' can be handled in this file

    Private Sub Application_Startup(ByVal sender As Object, ByVal e As System.Windows.StartupEventArgs) Handles Me.Startup
        Dim theme As ResourceDictionary
        Dim stheme As String
        stheme = "themes\"
        Dim sVersion As String
        sVersion = ConfigurationManager.AppSettings.Get("Version")
        stheme = "themes\" & sVersion & ".xaml"
        Dim themeUri As Uri
        themeUri = New Uri(stheme, UriKind.Relative)
        Try
            theme = CType(Application.LoadComponent(themeUri), ResourceDictionary)
        Catch Ex As Exception
            stheme = "themes\Generic.xaml"
            themeUri = New Uri(stheme, UriKind.Relative)
            theme = CType(Application.LoadComponent(themeUri), ResourceDictionary)

        End Try

        Resources.MergedDictionaries.Add(theme)

        'Dim sUsu As String
        'Dim IdSession As Long
        'Dim sPantalla As String
        'Dim p As String()
        'Dim q As String()
        'Dim queryString As String
        'Dim oUsu As New GSClient.Usuario
        'queryString = BrowserInteropHelper.Source.Query.ToString()
        'queryString = queryString.Trim("?")
        'If (IsNothing(queryString) Or queryString.Length = 0) Then
        '    Application.Current.StartupUri = New Uri("ErrorLogin.xaml", UriKind.Relative)
        '    Exit Sub
        'End If

        'sUsu = ""
        'IdSession = 0
        'sPantalla = ""

        'p = queryString.Split("&")


        'If p.Length <> 3 Then
        '    Application.Current.StartupUri = New Uri("ErrorLogin.xaml", UriKind.Relative)
        '    Exit Sub
        'Else


        '    For i = 0 To p.Count - 1
        '        q = p(i).Split("=")

        '        If q(0).ToString = "session" Then
        '            IdSession = q(1)
        '        End If

        '        If q(0).ToString = "usu" Then
        '            sUsu = q(1)
        '        End If

        '        If q(0).ToString = "pantalla" Then
        '            sPantalla = q(1)
        '        End If

        '    Next i

        '    '            ModulePublic.Escribirlog("------------------------------------------")
        '    '            ModulePublic.Escribirlog("INICIO DE LOG")
        '    If sUsu <> "" And IdSession <> 0 And sPantalla <> "" Then
        '        '               ModulePublic.Escribirlog("Antes de comprobar session en Application")
        '        If oUsu.ComprobarSession(sUsu, IdSession) Then
        '            '                  ModulePublic.Escribirlog("Despues de comprobar session en Application")

        '            ModulePublic.CargarUsu(sUsu)
        '            ModulePublic.CargarParametrosGenerales()

        '            CargarTextos()

        '            Dim theme As ResourceDictionary
        '            Dim stheme As String
        '            stheme = "themes\"
        '            Dim sVersion As String
        '            sVersion = ConfigurationManager.AppSettings.Get("Version")
        '            stheme = "themes\" & sVersion & ".xaml"
        '            Dim themeUri As Uri
        '            themeUri = New Uri(stheme, UriKind.Relative)
        '            Try
        '                theme = CType(Application.LoadComponent(themeUri), ResourceDictionary)
        '            Catch Ex As Exception
        '                stheme = "themes\Generic.xaml"
        '                themeUri = New Uri(stheme, UriKind.Relative)
        '                theme = CType(Application.LoadComponent(themeUri), ResourceDictionary)

        '            End Try

        '            Resources.MergedDictionaries.Add(theme)

        '            Select Case sPantalla

        '                Case "Monedas"
        '                    ModulePublic.iModule = ModuleName.FRM_MANTENIMIENTOMONEDAS
        '                    Application.Current.StartupUri = New Uri("PInicio.xaml", UriKind.Relative)
        '                Case "Paises"
        '                    ModulePublic.iModule = ModuleName.FRM_MANTENIMIENTOPAISESPROVINCIAS
        '                    Application.Current.StartupUri = New Uri("PInicio.xaml", UriKind.Relative)
        '                Case "Unidades"
        '                    ModulePublic.iModule = ModuleName.FRM_MANTENIMIENTOUNIDADES
        '                    Application.Current.StartupUri = New Uri("PInicio.xaml", UriKind.Relative)
        '                Case "ViaPago"
        '                    ModulePublic.iModule = ModuleName.FRM_MANTENIMIENTOVIASPAGO
        '                    Application.Current.StartupUri = New Uri("PInicio.xaml", UriKind.Relative)
        '                Case "FormaPago"
        '                    ModulePublic.iModule = ModuleName.FRM_MANTENIMIENTOFORMASPAGO
        '                    Application.Current.StartupUri = New Uri("PInicio.xaml", UriKind.Relative)
        '                Case "Impuestos"
        '                    ModulePublic.iModule = ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS
        '                    Application.Current.StartupUri = New Uri("PInicio.xaml", UriKind.Relative)

        '                Case Else
        '                    Application.Current.StartupUri = New Uri("ErrorLogin.xaml", UriKind.Relative)
        '            End Select

        '        Else
        '            Application.Current.StartupUri = New Uri("ErrorLogin.xaml", UriKind.Relative)
        '            Exit Sub

        '        End If
        '    Else
        '        Application.Current.StartupUri = New Uri("ErrorLogin.xaml", UriKind.Relative)
        '        Exit Sub
        '    End If

        'End If
        'ModulePublic.Escribirlog("Fin application startup")
    End Sub


    Private Sub CargarTextos()
        Dim ds As System.Data.DataSet
        Dim oTextos As New GSClient.CTextos

        ds = oTextos.DevolverTextos(TiposDeDatos.ModuleName.INFRAGISTICS, ModulePublic.ParametrosInstalacion.gsIdioma)

        If Not ds Is Nothing Then


            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("GroupByArea_Prompt1", ds.Tables(0).Rows(0).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("GroupByArea_Prompt2", ds.Tables(0).Rows(1).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("ConditionGroup_Description", ds.Tables(0).Rows(2).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SpecialFilterOperand_Blanks_DisplayContent", ds.Tables(0).Rows(3).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SpecialFilterOperand_NonBlanks_DisplayContent", ds.Tables(0).Rows(4).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SR_FilterDropDownItem_Custom", ds.Tables(0).Rows(2).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SR_FilterDropDownItem_Blanks", ds.Tables(0).Rows(3).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SpecialFilterOperand_Blanks_DisplayContent", ds.Tables(0).Rows(3).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("SpecialFilterOperand_NonBlanks_DisplayContent", ds.Tables(0).Rows(4).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_TitleDescriptionProperty", ds.Tables(0).Rows(5).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_FieldDescriptionProperty", ds.Tables(0).Rows(6).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_AddConditionLabelProperty", ds.Tables(0).Rows(7).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_RemoveSelectedConditionsLabelProperty", ds.Tables(0).Rows(8).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_AndGroupLegendDescriptionProperty", ds.Tables(0).Rows(9).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_GroupSelectedConditionsAsAndGroupLabelProperty", ds.Tables(0).Rows(10).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_OrGroupLegendDescriptionProperty", ds.Tables(0).Rows(11).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_GroupSelectedConditionsAsOrGroupLabelProperty", ds.Tables(0).Rows(12).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_ToggleOperatorOfSelectedConditionsLabelProperty", ds.Tables(0).Rows(13).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_UngroupSelectedConditionsLabelProperty", ds.Tables(0).Rows(14).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_OperandFieldLabel", ds.Tables(0).Rows(15).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_OperatorFieldLabel", ds.Tables(0).Rows(16).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_OkButtonLabelProperty", ds.Tables(0).Rows(17).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_CancelButtonLabelProperty", ds.Tables(0).Rows(18).Item(0).ToString())
            Infragistics.Windows.DataPresenter.Resources.Customizer.SetCustomizedString("CustomFilterSelectionControl_GroupSelectedLabelProperty", ds.Tables(0).Rows(23).Item(0).ToString())

        End If
    End Sub

End Class
