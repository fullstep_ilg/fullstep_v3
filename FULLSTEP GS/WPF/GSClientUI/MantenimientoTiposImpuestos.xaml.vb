﻿Imports GSClient
Imports System.ComponentModel
Imports GSServerModel
Imports Infragistics.Windows.DataPresenter
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Infragistics.Windows.Editors
Imports System.Data
Imports Infragistics.Windows.DataPresenter.ExcelExporter
Imports Infragistics.Excel
Imports System.Runtime.InteropServices

Partial Public Class MantenimientoTiposImpuestos
    Inherits ControlBase    

    <DllImport("user32.dll")> _
    Protected Shared Function SetForegroundWindow(ByVal hWnd As IntPtr) As Boolean
    End Function

    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)> _
    Protected Shared Function FindWindow(ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
    End Function

    Private _sTextoEliminarImpuestos As String
    Private _sTextoEliminarImpuestoValores As String

    Private _bAdding As Boolean
    Private _bDeshacer As Boolean
    Private _bAllowEdit As Boolean
    Private _bAllowEditAsig As Boolean
    Private _bRecargar As Boolean
    Private _bDobleAlto As Boolean

    Private _oImpEnEdicion As ImpuestoView
    Private _oImpOriginal As ImpuestoView
    Private _oImpValorEnEdicion As ImpuestoValor
    Private _oImpValorOriginal As ImpuestoValor
    Private _bImpRec As Boolean
    Private _bImpValRec As Boolean
    Private _oExportOptions As ExportOptions
    Private WithEvents _ComboPaises As XamComboEditor
    Private WithEvents _ComboProvincias As XamComboEditor
    Private WithEvents _oTextBoxComent As TextBox

    Public Sub New()
        MyBase.New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.CadenaEntrada = "Inicio Pagina Mantenimiento Tipos Impuestos"
        Me.CadenaSalida = "Fin Pagina Mantenimiento Tipos Impuestos"
        Me.AccionEntrada = AccionesGS.ACCImpuestoCon
        Me.AccionSalida = AccionesGS.ACCImpuestoCon

        'Cargar textos
        Mensajes.Idioma = ModulePublic.ParametrosInstalacion.gsIdioma
        MyBase.CargarTextos(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, Me.Grid)
        Dim oTextos As New GSClient.CTextos
        tbiData.Header = oTextos.DevolverTextosMensaje(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, 7)
        _sTextoEliminarImpuestos = oTextos.DevolverTextosMensaje(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, 19)
        _sTextoEliminarImpuestoValores = oTextos.DevolverTextosMensaje(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, 20)

        CargarDatos()
        CargarCombos(CType(Me.GetBindingObject("PaisesProvincias"), DataSet).Tables(0))

        ConfigurarPermisos()

        Me.cmdEliminar.IsEnabled = False
    End Sub

    '<summary>Obtiene los objetos de datos y carga el grid con ellos</summary>        
    '<remarks>Llamada desde: New, cmdRecargar_Click</remarks>

    Private Sub CargarDatos()
        'Obtener objetos de datos        
        Dim oImpRule As New GSClient.ImpuestosRule
        Dim oImpuestosData As GSServerModel.ImpuestosData
        oImpuestosData = oImpRule.DevolverDatosImpuestos(ModulePublic.ParametrosInstalacion.gsIdioma)

        'Data binding
        Me.AddBindingObject(NuevaBindingList(oImpuestosData.Impuestos), "Impuestos")
        Me.AddBindingObject(oImpuestosData.PaisesProvicias, "PaisesProvincias")
        Me.MakeBinding(Me.Grid)
    End Sub

    '<summary>Crea una BindingList a partir de la clase Impuestos. Para que el grid ofrezca la funcionalidad de añadir y borrar registros a todos los niveles
    'los objetos a los que se hace el binding tienen que ser una BindingList y deben tener un constructor sin parámetros</summary>    
    '<param name="oImpuestos">Objeto de tipo Impuestos</param>
    '<remarks>Llamada desde: New</remarks>

    Private Function NuevaBindingList(ByVal oImpuestos As Impuestos) As BindingList(Of ImpuestoView)
        Dim oImpuestosView As New BindingList(Of ImpuestoView)

        For Each oImpuesto As Impuesto In oImpuestos
            oImpuestosView.Add(New ImpuestoView(oImpuesto))
        Next

        Return oImpuestosView
    End Function

    Private Sub ComboPaisesProvincias_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs)
        Dim oCombo As Infragistics.Windows.Editors.XamComboEditor = CType(sender, Infragistics.Windows.Editors.XamComboEditor)
        oCombo.DataContext = CType(Me.GetBindingObject("PaisesProvincias"), System.Data.DataSet).Tables(0)
    End Sub

#Region " Eventos Grid "

    Private Sub XamDGTiposImpuestos_CellActivating(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellActivatingEventArgs) Handles XamDGTiposImpuestos.CellActivating
        Dim oDataRec As DataRecord = CType(XamDGTiposImpuestos.ActiveRecord, DataRecord)

        If ModulePublic.oUsu.Tipo <> Fullstep.FSNLibrary.TiposDeDatos.TipoDeUsuario.Administrador Then
            Select Case oDataRec.FieldLayout.Key
                Case "FL1"
                    If Not _bAllowEdit Then
                        oDataRec.FieldLayout.Fields(e.Cell.Field.Name).Settings.AllowEdit = False
                    Else
                        oDataRec.FieldLayout.Fields(e.Cell.Field.Name).Settings.AllowEdit = True
                    End If

                Case "FL2"
                    If Not _bAllowEdit Then
                        oDataRec.FieldLayout.Fields(e.Cell.Field.Name).Settings.AllowEdit = False
                    Else
                        If e.Cell.Field.Name <> "FechaAct" And e.Cell.Field.Name <> "Asignar" Then
                            oDataRec.FieldLayout.Fields(e.Cell.Field.Name).Settings.AllowEdit = True
                        End If
                    End If
            End Select
        End If
    End Sub

    Private Sub XamDGTiposImpuestos_CellChanged(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellChangedEventArgs) Handles XamDGTiposImpuestos.CellChanged
        Me.cmdDeshacer.IsEnabled = True
    End Sub

    Private Sub XamDGTiposImpuestos_CellDeactivating(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellDeactivatingEventArgs) Handles XamDGTiposImpuestos.CellDeactivating
        Select Case e.Cell.Field.Name
            Case "Pais"
                If e.Cell.Record.IsAddRecord Then
                    If Not _ComboPaises Is Nothing AndAlso _ComboPaises.SelectedItem Is Nothing Then
                        If Not e.Cell.Value Is Nothing Then
                            e.Cell.Value = Nothing
                            _ComboPaises.Text = Nothing
                            _ComboPaises.Value = Nothing
                        End If
                    End If
                End If
            Case "Provincia"
                If e.Cell.Record.IsAddRecord Then
                    If Not _ComboProvincias Is Nothing AndAlso _ComboProvincias.SelectedItem Is Nothing Then
                        If Not e.Cell.Value Is Nothing Then
                            e.Cell.Value = Nothing
                            _ComboProvincias.Text = Nothing
                            _ComboProvincias.Value = Nothing
                        End If
                    End If
                End If
        End Select
    End Sub

    Private Sub XamDGTiposImpuestos_EditModeEnded(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.EditModeEndedEventArgs) Handles XamDGTiposImpuestos.EditModeEnded
        If e.Cell.Field.Name = "Comentario" Then
            'Cambio de tamaño de la fila para devolverla a su tamaño original
            If _bAllowEdit Then
                If _bDobleAlto Then PonerAltoSimpleFila(e.Cell)
            End If
        End If
    End Sub

    Private Sub XamDGTiposImpuestos_EditModeStarted(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.EditModeStartedEventArgs) Handles XamDGTiposImpuestos.EditModeStarted
        If e.Cell.Field.Name = "Comentario" Then
            'Cambio de tamaño de la fila para acomodar 2 filas de comentario
            If _bAllowEdit Then PonerDobleAltoFila(e.Cell)
        End If
    End Sub

    Private Sub PonerDobleAltoFila(ByRef oCell As Cell)
        Dim oDataRec As DataRecord = CType(oCell.Record, DataRecord)
        Dim oRecPres As RecordPresenter = DataRecordPresenter.FromRecord(oDataRec)
        Dim oDRCA As DataRecordCellArea = Infragistics.Windows.Utilities.GetDescendantFromType(oRecPres, GetType(DataRecordCellArea), False)
        _oTextBoxComent = Infragistics.Windows.Utilities.GetDescendantFromType(oDRCA, GetType(TextBox), False)
        If Not _oTextBoxComent Is Nothing Then
            If _oTextBoxComent.LineCount > 1 Then
                _bDobleAlto = True

                'DataRecordPresenter
                oRecPres.Height = 2 * oRecPres.ActualHeight

                'DataRecordCellArea                            
                oDRCA.Height = 2 * oDRCA.ActualHeight

                'CellValuePresenter
                Dim oCVP As CellValuePresenter = CellValuePresenter.FromCell(oCell)
                oCVP.Height = 2 * oCVP.ActualHeight
            End If
        End If
    End Sub

    Private Sub PonerAltoSimpleFila(ByRef oCell As Cell)
        _bDobleAlto = False

        'DataRecordPresenter          
        Dim oDataRec As DataRecord = CType(oCell.Record, DataRecord)
        Dim oRecPres As RecordPresenter = DataRecordPresenter.FromRecord(oDataRec)
        oRecPres.Height = oRecPres.ActualHeight / 2

        'DataRecordCellArea
        Dim o As DataRecordCellArea = Infragistics.Windows.Utilities.GetDescendantFromType(oRecPres, GetType(DataRecordCellArea), False)
        o.Height = o.ActualHeight / 2

        'CellValuePresenter
        Dim oCVP As CellValuePresenter = CellValuePresenter.FromCell(oCell)
        oCVP.Height = oCVP.ActualHeight / 2
    End Sub



    Private Sub XamDGTiposImpuestos_InitializeRecord(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.InitializeRecordEventArgs) Handles XamDGTiposImpuestos.InitializeRecord
        If Not e.Record Is Nothing Then
            If e.Record.FieldLayout.Key = "FL2" Then
                If TypeOf e.Record Is DataRecord Then
                    Dim oRecord As DataRecord = CType(e.Record, DataRecord)
                    oRecord.Cells("Pais").Value = oRecord.Cells("CodigoPais").Value & " - " & oRecord.Cells("DenominacionPais").Value
                    If Not oRecord.Cells("CodigoProvincia").Value Is Nothing Then
                        oRecord.Cells("Provincia").Value = oRecord.Cells("CodigoProvincia").Value & " - " & oRecord.Cells("DenominacionProvincia").Value
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub XamDGTiposImpuestos_InitializeTemplateAddRecord(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.InitializeTemplateAddRecordEventArgs) Handles XamDGTiposImpuestos.InitializeTemplateAddRecord

    End Sub

    Private Sub XamDGTiposImpuestos_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Input.KeyEventArgs) Handles XamDGTiposImpuestos.PreviewKeyDown
        Select Case e.Key
            Case Key.Delete
                cmdEliminar_Click(XamDGTiposImpuestos, e)

            Case Key.Escape
                Dim oDataRec As DataRecord = CType(XamDGTiposImpuestos.ActiveRecord, DataRecord)
                If oDataRec.IsAddRecord Then
                    If Not _ComboProvincias Is Nothing Then _ComboProvincias.Value = Nothing
                    If Not _ComboPaises Is Nothing Then _ComboPaises.Value = Nothing
                End If
        End Select
    End Sub

    Private Sub XamDGTiposImpuestos_RecordActivating(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordActivatingEventArgs) Handles XamDGTiposImpuestos.RecordActivating
        If Not e.Record Is Nothing Then
            Select Case e.Record.FieldLayout.Key
                Case "FL1"
                    If TypeOf e.Record Is DataRecord Then
                        Dim oRecord As DataRecord = CType(e.Record, DataRecord)

                        If Not oRecord.IsAddRecord And Not oRecord.IsSpecialRecord Then
                            'Se guarda una copia del objeto que está detrás del registro actual para poder deshacer los cambios en caso de que se pulse Deshacer
                            'Esto es necesario hacerlo porque una vez que se cambia una celda el cambio pasa al objeto y el command DiscardChangesToActiveRecord no hace nada porque los 
                            'cambios ya se han trasladado al objeto
                            _oImpEnEdicion = CType(oRecord.DataItem, ImpuestoView).Duplicate
                            _oImpOriginal = oRecord.DataItem
                            _bImpRec = True
                            _bImpValRec = False
                        Else
                            _oImpEnEdicion = Nothing
                            _oImpOriginal = Nothing
                            _bImpRec = False
                            _bImpValRec = False
                        End If
                    End If
                Case "FL2"
                    If TypeOf e.Record Is DataRecord Then
                        Dim oRecord As DataRecord = CType(e.Record, DataRecord)

                        If Not oRecord.IsAddRecord And Not oRecord.IsSpecialRecord Then
                            'Se guarda una copia del objeto que está detrás del registro actual para poder deshacer los cambios en caso de que se pulse Deshacer
                            'Esto es necesario hacerlo porque una vez que se cambia una celda el cambio pasa al objeto y el command DiscardChangesToActiveRecord no hace nada porque los 
                            'cambios ya se han trasladado al objeto
                            _oImpValorEnEdicion = CType(oRecord.DataItem, ImpuestoValor).Duplicate
                            _oImpValorOriginal = oRecord.DataItem
                            _bImpRec = False
                            _bImpValRec = True
                        Else
                            _oImpValorEnEdicion = Nothing
                            _oImpValorOriginal = Nothing
                            _bImpRec = False
                            _bImpValRec = False

                            Dim oCvp As CellValuePresenter
                            Dim oBinding As System.Windows.Data.Binding

                            oCvp = CellValuePresenter.FromRecordAndField(oRecord, oRecord.FieldLayout.Fields("Pais"))
                            _ComboPaises = Infragistics.Windows.Utilities.GetDescendantFromType(oCvp, GetType(XamComboEditor), False)
                            'Binding datos
                            'oBinding = New System.Windows.Data.Binding("Value")
                            'oBinding.Source = oRecord.Cells("CodigoPais")
                            'oBinding.Mode = BindingMode.TwoWay
                            '_ComboPaises.SetBinding(FSNComboEditor.ValueProperty, oBinding)

                            oCvp = CellValuePresenter.FromRecordAndField(oRecord, oRecord.FieldLayout.Fields("Provincia"))
                            _ComboProvincias = Infragistics.Windows.Utilities.GetDescendantFromType(oCvp, GetType(XamComboEditor), False)
                            'Binding datos
                            'oBinding = New System.Windows.Data.Binding("Value")
                            'oBinding.Source = oRecord.Cells("CodigoProvincia")
                            'oBinding.Mode = BindingMode.TwoWay
                            '_ComboProvincias.SetBinding(FSNComboEditor.ValueProperty, oBinding)
                        End If
                    End If
            End Select
        End If
    End Sub

    '<summary>Rellena el combo de países</summary>    
    '<param name="dtPaises">Datatable con los países</param>
    '<remarks>Llamada desde: XamDGTiposImpuestos_RecordActivated</remarks>

    Private Sub CargarCombos(ByVal dtPaises As DataTable)
        Dim oTextos As New GSClient.CTextos

        'Concepto impuesto
        Dim oConcepProv As ComboBoxItemsProvider = Me.FindResource("Conceptos")
        oConcepProv.Items.Add(New ComboBoxDataItem(ConceptoImpuesto.Gasto, oTextos.DevolverTextosMensaje(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, 32)))
        oConcepProv.Items.Add(New ComboBoxDataItem(ConceptoImpuesto.Inversion, oTextos.DevolverTextosMensaje(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, 33)))
        oConcepProv.Items.Add(New ComboBoxDataItem(ConceptoImpuesto.Ambos, oTextos.DevolverTextosMensaje(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, 34)))

        'Dim oItemProv As New ComboBoxItemsProvider
        Dim oItemProv As ComboBoxItemsProvider = Me.FindResource("Paises")

        If Not dtPaises Is Nothing AndAlso dtPaises.Rows.Count > 0 Then
            Dim sDenField As String = "DEN_SPA"
            Select Case ModulePublic.ParametrosInstalacion.gsIdioma
                Case "SPA"
                    sDenField = "DEN_SPA"
                Case "ENG"
                    sDenField = "DEN_ENG"
                Case "GER"
                    sDenField = "DEN_GER"
            End Select

            Dim dvPaises As New DataView(dtPaises, Nothing, sDenField, DataViewRowState.CurrentRows)
            For Each oRow As DataRowView In dvPaises
                Dim oNewItem As New ComboBoxDataItem(oRow("PAICOD"), oRow(sDenField))
                oNewItem.Tag = oRow(sDenField)    'Guardo en el tag la denominación 
                oItemProv.Items.Add(oNewItem)
            Next
        End If
    End Sub

    ' <summary>Configuracion de permisos</summary>    
    ' <remarks>Llamada desde: New</remarks>    

    Private Sub ConfigurarPermisos()
        If ModulePublic.oUsu.Tipo <> Fullstep.FSNLibrary.TiposDeDatos.TipoDeUsuario.Administrador Then
            'Cargar Acciones para determinar si puede modificar o no
            If Not (ModulePublic.oUsu.Acciones.Item(CLng(Fullstep.FSNLibrary.TiposDeDatos.AccionesDeSeguridad.TipImpModificar)) Is Nothing) Then
                _bAllowEdit = True
            Else
                _bAllowEdit = False
            End If

            If Not (ModulePublic.oUsu.Acciones.Item(CLng(Fullstep.FSNLibrary.TiposDeDatos.AccionesDeSeguridad.TipImpModifAsig)) Is Nothing) Then
                _bAllowEditAsig = True
            Else
                _bAllowEditAsig = False
            End If

            If Not _bAllowEdit Then
                XamDGTiposImpuestos.FieldSettings.AllowEdit = False

                XamDGTiposImpuestos.FieldLayouts("FL1").Settings.AllowAddNew = False
                XamDGTiposImpuestos.FieldLayouts("FL1").Settings.AllowDelete = False

                XamDGTiposImpuestos.FieldLayouts("FL2").Settings.AllowAddNew = False
                XamDGTiposImpuestos.FieldLayouts("FL2").Settings.AllowDelete = False

                Me.cmdEliminar.Visibility = Windows.Visibility.Collapsed
                Me.cmdDeshacer.Visibility = Windows.Visibility.Collapsed

                If Not _bRecargar Then
                    cmdExportar.Margin = Me.cmdEliminar.Margin
                    cmdListado.Margin = Me.cmdDeshacer.Margin
                End If
            Else
                XamDGTiposImpuestos.FieldLayoutSettings.AddNewRecordLocation = AddNewRecordLocation.OnTopFixed
                XamDGTiposImpuestos.FieldLayoutSettings.AllowDelete = False
            End If
        Else
            _bAllowEdit = True
            _bAllowEditAsig = True
            XamDGTiposImpuestos.FieldLayoutSettings.AddNewRecordLocation = AddNewRecordLocation.OnTopFixed
            cmdEliminar.IsEnabled = True
        End If
    End Sub

    '<summary>Rellena el combo de provincias</summary>    
    '<param name="dtPaises">Datatable con las provincias</param>
    '<remarks>Llamada desde: XamDGTiposImpuestos_RecordActivated</remarks>

    Private Sub RellenarComboProvincias(ByVal sCodPais As String, ByVal dtProvincias As DataTable)
        'Dim oItemProv As New ComboBoxItemsProvider
        Dim oItemProv As ComboBoxItemsProvider = Me.FindResource("Provincias")

        oItemProv.Items.Clear()
        If Not dtProvincias Is Nothing AndAlso dtProvincias.Rows.Count > 0 Then
            Dim sDenField As String = "DEN_SPA"
            Select Case ModulePublic.ParametrosInstalacion.gsIdioma
                Case "SPA"
                    sDenField = "DEN_SPA"
                Case "ENG"
                    sDenField = "DEN_ENG"
                Case "GER"
                    sDenField = "DEN_GER"
            End Select

            Dim dvProvincias As New DataView(dtProvincias, "PROVIPAI='" & sCodPais & "'", sDenField, DataViewRowState.CurrentRows)
            For Each oRow As DataRowView In dvProvincias
                Dim oNewItem As New ComboBoxDataItem(oRow("PROVICOD"), oRow(sDenField))
                oNewItem.Tag = oRow(sDenField)    'Guardo en el tag la denominación 
                oItemProv.Items.Add(oNewItem)
            Next
        End If

        '_ComboProvincias.ItemsProvider = oItemProv
    End Sub

    Private Sub XamDGTiposImpuestos_RecordAdded(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordAddedEventArgs) Handles XamDGTiposImpuestos.RecordAdded
        _bAdding = True
        If e.Record.FieldLayout.Key = "FL1" Then

            Dim oDataRec As DataRecord = CType(e.Record, DataRecord)
            Dim oImpuesto As ImpuestoView = CType(oDataRec.DataItem, ImpuestoView)

            oImpuesto.GrpComp = 1
        End If

    End Sub

    Private Sub XamDGTiposImpuestos_RecordsDeleting(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordsDeletingEventArgs) Handles XamDGTiposImpuestos.RecordsDeleting
        e.DisplayPromptMessage = False
    End Sub

    Private Sub XamDGTiposImpuestos_RecordUpdating(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordUpdatingEventArgs) Handles XamDGTiposImpuestos.RecordUpdating
        If Not e.Record Is Nothing Then
            If Not _bDeshacer Then
                Select Case e.Record.FieldLayout.Key
                    Case "FL1"
                        If _bAdding Then
                            If ComprobarDatosImpuesto(e.Record) Then

                                Dim oImpuestosRule As New GSClient.ImpuestosRule
                                Dim oImpuesto As GSServerModel.Impuesto = CType(e.Record.DataItem, ImpuestoView).ToImpuesto
                                Dim oError As GSServerModel.GSException = oImpuestosRule.InsertarImpuesto(oImpuesto)
                                If oError.Number <> ErroresGS.TESnoerror Then
                                    TratarError(oError)
                                    e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges
                                Else
                                    CType(e.Record.DataItem, ImpuestoView).Id = oImpuesto.Id
                                    CType(e.Record.DataItem, ImpuestoView).FechaAct = oImpuesto.FechaAct
                                End If

                                _bAdding = False
                            Else
                                e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                            End If
                        Else
                            If Not e.Record.IsAddRecord And Not e.Record.IsSpecialRecord Then
                                If ComprobarDatosImpuesto(e.Record) Then
                                    Dim oImpuestosRule As New GSClient.ImpuestosRule
                                    Dim oImpuesto As GSServerModel.Impuesto = CType(e.Record.DataItem, ImpuestoView).ToImpuesto
                                    Dim oError As GSServerModel.GSException = oImpuestosRule.ActualizarImpuesto(oImpuesto)
                                    If oError.Number <> ErroresGS.TESnoerror Then
                                        TratarError(oError)
                                        e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges
                                    Else
                                        CType(e.Record.DataItem, ImpuestoView).FechaAct = oImpuesto.FechaAct
                                    End If
                                Else
                                    e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                                End If
                            End If
                        End If
                    Case "FL2"
                        If _bAdding Then
                            Dim oImpuesto As GSServerModel.Impuesto = CType(e.Record.ParentDataRecord.DataItem, ImpuestoView).ToImpuesto
                            If ComprobarDatosValorImpuesto(oImpuesto.Id, e.Record) Then
                                Dim oImpuestosRule As New GSClient.ImpuestosRule
                                Dim oImpuestoValor As GSServerModel.ImpuestoValor = CType(e.Record.DataItem, ImpuestoValor)
                                Dim oError As GSServerModel.GSException = oImpuestosRule.InsertarImpuestoValor(oImpuesto.Id, oImpuestoValor)
                                If oError.Number <> ErroresGS.TESnoerror Then
                                    TratarError(oError)
                                    e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges
                                Else
                                    CType(e.Record.DataItem, ImpuestoValor).Id = oImpuestoValor.Id
                                    CType(e.Record.DataItem, ImpuestoValor).Impuesto = oImpuestoValor.Impuesto
                                    CType(e.Record.DataItem, ImpuestoValor).FechaAct = oImpuestoValor.FechaAct
                                End If

                                _bAdding = False
                            Else
                                e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                            End If
                        Else
                            If Not e.Record.IsAddRecord And Not e.Record.IsSpecialRecord Then
                                If ComprobarDatosValorImpuesto(CType(e.Record.ParentDataRecord.DataItem, ImpuestoView).Id, e.Record) Then
                                    Dim oImpuestosRule As New GSClient.ImpuestosRule
                                    Dim oImpuestoValor As GSServerModel.ImpuestoValor = CType(e.Record.DataItem, ImpuestoValor)
                                    Dim oError As GSServerModel.GSException = oImpuestosRule.ActualizarImpuestoValor(oImpuestoValor)
                                    If oError.Number <> ErroresGS.TESnoerror Then
                                        TratarError(oError)
                                        e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges
                                    Else
                                        CType(e.Record.DataItem, ImpuestoValor).FechaAct = oImpuestoValor.FechaAct
                                    End If
                                Else
                                    e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                                End If
                            End If
                        End If
                End Select
            End If
        End If
    End Sub

    Private Sub XamDGTiposImpuestos_SelectedItemsChanged(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs) Handles XamDGTiposImpuestos.SelectedItemsChanged
        If _bAllowEdit And XamDGTiposImpuestos.SelectedItems.Count > 0 Then
            cmdEliminar.IsEnabled = True
        Else
            cmdEliminar.IsEnabled = False
        End If
    End Sub

#End Region

    Private Sub _oTextBoxComent_TextChanged(ByVal sender As Object, ByVal e As System.Windows.Controls.TextChangedEventArgs) Handles _oTextBoxComent.TextChanged
        If _oTextBoxComent.LineCount > 1 And Not _bDobleAlto Then
            PonerDobleAltoFila(XamDGTiposImpuestos.ActiveCell)
        ElseIf _oTextBoxComent.LineCount = 1 And _bDobleAlto Then
            PonerAltoSimpleFila(XamDGTiposImpuestos.ActiveCell)
        End If
    End Sub

    Private Sub _ComboPaises_ValueChanged(ByVal sender As Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of Object)) Handles _ComboPaises.ValueChanged
        Me.cmdDeshacer.IsEnabled = True

        If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value OrElse e.NewValue = String.Empty Then
            If Not XamDGTiposImpuestos.ActiveRecord Is Nothing Then
                CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("CodigoPais").Value = Nothing
                CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("DenominacionPais").Value = Nothing
                CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("CodigoProvincia").Value = Nothing
                CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("DenominacionProvincia").Value = Nothing

                'Deshabilitar el dropdown de las provincias
                _ComboProvincias.IsHitTestVisible = False
                _ComboProvincias.Value = Nothing
            End If
        Else
            If Not XamDGTiposImpuestos.ActiveRecord Is Nothing Then
                If Not CType(sender, XamComboEditor).SelectedItem Is Nothing Then
                    CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("CodigoPais").Value = e.NewValue
                    CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("DenominacionPais").Value = CType(CType(sender, XamComboEditor).SelectedItem, ComboBoxDataItem).Tag
                End If
                CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("CodigoProvincia").Value = Nothing
                CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("DenominacionProvincia").Value = Nothing

                'Habilitar el dropdown de las provincias            
                RellenarComboProvincias(e.NewValue, CType(Me.GetBindingObject("PaisesProvincias"), DataSet).Tables(1))
                _ComboProvincias.IsHitTestVisible = (_ComboProvincias.ItemsProvider.Items.Count > 0)
            End If
        End If
    End Sub

    Private Sub _ComboProvincias_ValueChanged(ByVal sender As Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of Object)) Handles _ComboProvincias.ValueChanged
        Me.cmdDeshacer.IsEnabled = True

        If Not XamDGTiposImpuestos.ActiveRecord Is Nothing Then
            If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value OrElse e.NewValue = String.Empty Then
                CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("CodigoProvincia").Value = Nothing
                CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("DenominacionProvincia").Value = Nothing
            Else
                If Not CType(sender, XamComboEditor).SelectedItem Is Nothing Then
                    CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("CodigoProvincia").Value = e.NewValue
                    CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).Cells("DenominacionProvincia").Value = CType(CType(sender, XamComboEditor).SelectedItem, ComboBoxDataItem).Tag
                End If
            End If
        End If
    End Sub

#Region " Eventos Botones "

    Private Sub cmdDeshacer_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Input.KeyEventArgs) Handles cmdDeshacer.PreviewKeyDown
        DeshacerCambios()
    End Sub


    Private Sub cmdDeshacer_PreviewMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles cmdDeshacer.PreviewMouseDoubleClick
        DeshacerCambios()
    End Sub

    Private Sub cmdDeshacer_PreviewMouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles cmdDeshacer.PreviewMouseDown
        DeshacerCambios()
    End Sub

    '' <summary>Lleva a cabo la acción de deshacer los cambios</summary> 
    '' <remarks>Llamada desde: cmdDeshacer_PreviewKeyDown, cmdDeshacer_PreviewMouseDoubleClick, cmdDeshacer_PreviewMouseDown</remarks>

    Private Sub DeshacerCambios()
        Me.Cursor = Cursors.Wait

        _bDeshacer = True

        XamDGTiposImpuestos.ExecuteCommand(DataPresenterCommands.DiscardChangesToActiveRecord)

        If _bImpRec Then
            Dim _oImpuestos As BindingList(Of ImpuestoView) = Me.GetBindingObject("Impuestos")
            Dim oImp As ImpuestoView = _oImpuestos.Item(_oImpuestos.IndexOf(_oImpOriginal))
            oImp.Copy(_oImpEnEdicion)
        End If
        If _bImpValRec Then

            Dim _oImpuestos As BindingList(Of ImpuestoView) = Me.GetBindingObject("Impuestos")
            Dim oImps As IEnumerable(Of ImpuestoView) = _oImpuestos.Where(Function(elemento As ImpuestoView) elemento.Id = _oImpValorOriginal.Impuesto) 'epb
            Dim oImp As ImpuestoView = oImps.ElementAt(0) 'epb
            'Dim oImp As ImpuestoView = _oImpuestos.Item(_oImpuestos.IndexOf(_oImpOriginal)) 'epb
            Dim oImpValor As ImpuestoValor = oImp.Valores.Item(oImp.Valores.IndexOf(_oImpValorOriginal))
            oImpValor.Copy(_oImpValorEnEdicion)
        End If

        If XamDGTiposImpuestos.ActiveRecord.FieldLayout.Key = "FL2" Then
            If TypeOf (XamDGTiposImpuestos.ActiveRecord) Is DataRecord Then
                If CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).IsAddRecord Then
                    'Limpiar los combos. Es necesario porque están en campos no enlazados y no se limpian al hacer DiscardChangesToActiveRecord
                    _ComboPaises.Value = Nothing
                    _ComboProvincias.Value = Nothing
                End If
            End If
        End If

        _bDeshacer = False
        Me.cmdDeshacer.IsEnabled = False
        _bAdding = False

        Me.Cursor = Cursors.Arrow
    End Sub

    Private Sub cmdExportar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdExportar.Click
        Dim sfileName As String
        sfileName = System.IO.Path.GetTempPath() + "\TiposImpuestos.xls"

        Try
            ' Get the instance of the DataPresenterExcelExporter we defined in the Page's resources and call its Export
            ' method passing in the XamDataGrid we want to export, the name of the file we would like
            ' to export to and the format of the Workbook we want to Exporter to create.
            '
            ' NOTE: there are 11 overloads to the Export method that give you control over different
            ' aspects of the exporting process.

            'No exportar la columna "Asignar material/artículos", que no es de datos. Se oculta antes de la exportación y se visualiza después

            XamDGTiposImpuestos.FieldLayouts("FL2").Fields("Asignar").Visibility = Windows.Visibility.Hidden

            Dim exporter As New DataPresenterExcelExporter()
            exporter = CType(Me.Resources("Exporter"), DataPresenterExcelExporter)

            'exporter.Export(Me.XamDGMonedas, fileName, WorkbookFormat.Excel2007, this.ExportOptions);
            exporter.Export(Me.XamDGTiposImpuestos, sfileName, WorkbookFormat.Excel97To2003, ExportOptions)

            XamDGTiposImpuestos.FieldLayouts("FL2").Fields("Asignar").Visibility = Windows.Visibility.Visible

        Catch ex As Exception

        End Try

        Try
            Dim oExcel As Object = CreateObject("Excel.Application")
            oExcel.Visible = True
            oExcel.Workbooks.Open(sfileName)
            Dim handler As IntPtr = FindWindow(Nothing, oExcel.Caption)
            SetForegroundWindow(handler)

        Catch Ex As Exception

            MessageBox.Show("Error al generar el excel", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation)
        End Try
    End Sub

    '' <summary>Funcion de exportar el grid</summary>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>

    Public ReadOnly Property ExportOptions() As ExportOptions
        Get
            If (IsNothing(_oExportOptions)) Then
                _oExportOptions = New ExportOptions()

            End If
            Return _oExportOptions
        End Get
    End Property

    Private Sub cmdRecargar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdRecargar.Click
        Me.Cursor = Cursors.Wait
        _bRecargar = True
        CargarDatos()
        Me.cmdEliminar.IsEnabled = False
        _bRecargar = False
        Me.Cursor = Cursors.Arrow
    End Sub

    Private Sub cmdEliminar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdEliminar.Click
        With XamDGTiposImpuestos
            If _bAllowEdit And .SelectedItems.Count > 0 Then
                If .SelectedItems.Records(0).FieldLayout.Key = "FL1" Then
                    Dim iRespuesta As Integer = Mensajes.EliminarObjeto(_sTextoEliminarImpuestos)
                    If iRespuesta = vbYes Then
                        Me.Cursor = Cursors.Wait

                        Dim ID(.SelectedItems.Count - 1) As Integer
                        For i As Integer = 0 To .SelectedItems.Count - 1
                            ID(i) = CType(.SelectedItems.Records(i), DataRecord).Cells("Id").Value
                        Next

                        Dim oImpuestosRule As New GSClient.ImpuestosRule
                        Dim oError As GSServerModel.GSException = oImpuestosRule.EliminarImpuestos(ID)

                        If oError.Number = ErroresGS.TESnoerror Then
                            .ExecuteCommand(DataPresenterCommands.DeleteSelectedDataRecords)
                        Else
                            If oError.Number = ErroresGS.TESImposibleEliminar Then
                                ImposibleEliminacionMultiple(76, oError)
                                .ExecuteCommand(DataPresenterCommands.ClearAllSelected)
                            Else
                                TratarError(oError)
                            End If
                        End If

                        Me.Cursor = Cursors.Arrow
                    End If
                ElseIf .SelectedItems.Records(0).FieldLayout.Key = "FL2" Then
                    Dim iRespuesta As Integer = Mensajes.EliminarObjeto(_sTextoEliminarImpuestoValores)
                    If iRespuesta = vbYes Then
                        Me.Cursor = Cursors.Wait

                        Dim ID(.SelectedItems.Count - 1) As Integer
                        For i As Integer = 0 To .SelectedItems.Count - 1
                            ID(i) = CType(.SelectedItems.Records(i), DataRecord).Cells("Id").Value
                        Next

                        Dim oImpuestosRule As New GSClient.ImpuestosRule
                        Dim oError As GSServerModel.GSException = oImpuestosRule.EliminarImpuestoValores(ID)

                        If oError.Number = ErroresGS.TESnoerror Then
                            .ExecuteCommand(DataPresenterCommands.DeleteSelectedDataRecords)
                        Else
                            If oError.Number = ErroresGS.TESImposibleEliminar Then
                                ImposibleEliminacionMultiple(76, oError)
                                .ExecuteCommand(DataPresenterCommands.ClearAllSelected)
                            Else
                                TratarError(oError)
                            End If
                        End If

                        Me.Cursor = Cursors.Arrow
                    End If
                End If
            End If
        End With
    End Sub

    Private Sub cmdListado_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdListado.Click
        'Para que el grid jerárquico salga desplegado en el informe
        Dim oTRV As New TabularReportView
        oTRV.ExcludeExpandedState = True
        Me.XamDGTiposImpuestos.ReportView = oTRV

        Dim myReport As New Infragistics.Windows.Reporting.Report()
        Dim myXamDataGridReport As New Infragistics.Windows.Reporting.EmbeddedVisualReportSection(Me.XamDGTiposImpuestos)

        Me.tbiPreview.Visibility = Windows.Visibility.Visible
        Me.tbiPreview.IsSelected = True
        XamReportPreview1.Visibility = Windows.Visibility.Visible
        myReport.Sections.Add(myXamDataGridReport)
        Me.tbiData.Width = Me.tbiPreview.Width
        XamReportPreview1.GeneratePreview(myReport, False, True)
    End Sub

    Private Sub AsignarButton_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs)
        PantallaDeshabilitada.Visibility = Visibility.Visible

        Dim oImpuestoValor As ImpuestoValor = CType(CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).DataItem, ImpuestoValor)
        Dim oImpuesto As ImpuestoView = CType(CType(CType(XamDGTiposImpuestos.ActiveRecord, DataRecord).ParentRecord, ExpandableFieldRecord).ParentRecord, DataRecord).DataItem

        'Añadir el nuevo control y controladores de eventos
        Dim sDenImpuesto As String = String.Empty
        Select Case ModulePublic.ParametrosInstalacion.gsIdioma
            Case "SPA"
                sDenImpuesto = oImpuesto.DenSpa
            Case "ENG"
                sDenImpuesto = oImpuesto.DenEng
            Case "GER"
                sDenImpuesto = oImpuesto.DenGer
        End Select
        Dim oAsigImpuestos As New AsigImpuestos(oImpuestoValor.DenominacionPais, oImpuesto.Codigo, oImpuestoValor.Id, oImpuestoValor.Valor, sDenImpuesto, _bAllowEditAsig)
        AddHandler oAsigImpuestos.CerrarAsigImpuestos, AddressOf CerrarAsigImpuestos_Click

        pnAsigImpuestos.Children.Add(oAsigImpuestos)
        pnAsigImpuestos.Visibility = Visibility.Visible
        pnAsigImpuestos.IsEnabled = True
    End Sub

    '<summary>Acciones al cancelar el evío de correos</summary>    

    Private Sub CerrarAsigImpuestos_Click()
        pnAsigImpuestos.Children.RemoveAt(pnAsigImpuestos.Children.Count - 1)
        PantallaDeshabilitada.Visibility = Visibility.Hidden

        Me.Cursor = Cursors.Arrow
    End Sub

#End Region

    '<summary>Comprueba los datos para el alta de un impuesto</summary>    
    '<returns>Booleano indicando si los datos son correctos</returns>
    '<remarks>Llamada desde: XamDGTiposImpuestos_RecordUpdating</remarks>

    Private Function ComprobarDatosImpuesto(ByVal oDatos As Infragistics.Windows.DataPresenter.DataRecord) As Boolean
        Dim oImpuestos As BindingList(Of ImpuestoView)
        Dim bOk As Boolean = True

        Dim oImpuesto As ImpuestoView = CType(oDatos.DataItem, ImpuestoView)

        If oImpuesto.Codigo Is Nothing Then
            Mensajes.NoValido(oDatos.FieldLayout.Fields("Codigo").Label)
            bOk = False
        ElseIf oImpuesto.DenSpa Is Nothing Then
            Mensajes.NoValido(oDatos.FieldLayout.Fields("DenSpa").Label)
            bOk = False
        ElseIf oImpuesto.DenEng Is Nothing Then
            Mensajes.NoValido(oDatos.FieldLayout.Fields("DenEng").Label)
            bOk = False
        ElseIf oImpuesto.DenGer Is Nothing Then
            Mensajes.NoValido(oDatos.FieldLayout.Fields("DenGer").Label)
            bOk = False
        End If

        If bOk Then
            'Comprobar código duplicado
            oImpuestos = Me.GetBindingObject("Impuestos")
            For Each oImp As ImpuestoView In oImpuestos
                'Cuando se está comprobando esto el nuevo elemento ya está en la colección
                If Not oImpuesto.Equals(oImp) AndAlso oImpuesto.Codigo.ToUpper.Equals(oImp.Codigo.ToUpper) Then
                    Mensajes.DatoDuplicado(oDatos.FieldLayout.Fields("Codigo").Label)
                    bOk = False
                    Exit For
                End If
            Next
        End If

        Return bOk
    End Function

    '<summary>Comprueba los datos para el alta de un valor de un impuesto</summary>   
    '<param name="IdImpuesto">Id. del impuesto</param>
    '<param name="oDatos">Datos del nuevo valor</param>
    '<returns>Booleano indicando si los datos son correctos</returns>
    '<remarks>Llamada desde: XamDGTiposImpuestos_RecordUpdating</remarks>

    Private Function ComprobarDatosValorImpuesto(ByVal IdImpuesto As Integer, ByVal oDatos As Infragistics.Windows.DataPresenter.DataRecord) As Boolean
        Dim oImpuestos As BindingList(Of ImpuestoView)
        Dim bOk As Boolean = True

        Dim oImpuestoValor As ImpuestoValor = CType(oDatos.DataItem, ImpuestoValor)

        If oImpuestoValor.CodigoPais Is Nothing Then
            Mensajes.NoValido(oDatos.FieldLayout.Fields("Pais").Label)
            bOk = False
        End If
        If oImpuestoValor.Valor < 0 Or oImpuestoValor.Valor > 100 Then
            Mensajes.NoValido(oDatos.FieldLayout.Fields("Valor").Label)
            bOk = False
        End If

        If bOk Then
            'Comprobar duplicado
            oImpuestos = Me.GetBindingObject("Impuestos")
            Dim oImps As IEnumerable(Of ImpuestoView) = oImpuestos.Where(Function(elemento As ImpuestoView) elemento.Id = IdImpuesto)
            Dim oImpuesto As ImpuestoView = oImps.ElementAt(0)
            For Each oImpValor As ImpuestoValor In oImpuesto.Valores
                'Cuando se está comprobando esto el nuevo elemento ya está en la colección
                If oImpuestoValor.CodigoProvincia Is Nothing OrElse oImpuestoValor.CodigoProvincia = String.Empty Then
                    If oImpValor.CodigoProvincia Is Nothing OrElse oImpValor.CodigoProvincia = String.Empty Then
                        If Not oImpuestoValor.Equals(oImpValor) AndAlso oImpuestoValor.CodigoPais.Equals(oImpValor.CodigoPais) AndAlso _
                            oImpuestoValor.Valor = oImpValor.Valor Then
                            Mensajes.DatoDuplicado(oDatos.FieldLayout.Fields("Valor").Label)
                            bOk = False
                            Exit For
                        End If
                    End If
                Else
                    If Not oImpValor.CodigoProvincia Is Nothing AndAlso oImpValor.CodigoProvincia <> String.Empty Then
                        If Not oImpuestoValor.Equals(oImpValor) AndAlso oImpuestoValor.CodigoPais.Equals(oImpValor.CodigoPais) AndAlso _
                            oImpuestoValor.CodigoProvincia.Equals(oImpValor.CodigoProvincia) AndAlso oImpuestoValor.Valor = oImpValor.Valor Then
                            Mensajes.DatoDuplicado(oDatos.FieldLayout.Fields("Valor").Label)
                            bOk = False
                            Exit For
                        End If
                    End If
                End If
            Next
        End If

        Return bOk
    End Function

#Region " ImpuestoView Class "

    'Clase para que se puedan hacer operaciones Add/Delete en el grid. Para que el grid permita estas operaciones (y se vea el registro para los nuevos)
    'el objeto al que se haya hecho el binding tiene que heredar de BindingList o implementar IBindingList

    Private Class ImpuestoView
        Implements INotifyPropertyChanged

        Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

        Private _iId As Integer
        Private _sCod As String
        Private _iConcepto As Nullable(Of Integer)
        Private _bRetenido As Boolean
        Private _dtFechaAct As Date
        Private _sDenEng As String
        Private _sDenSpa As String
        Private _sDenGer As String
        Private _sDenFra As String
        Private _sComentario As String
        Private _iGrpComp As Integer
        Private _Valores As BindingList(Of ImpuestoValor)

#Region " Propiedades "

        Property Id() As Integer
            Get
                Id = _iId
            End Get
            Set(ByVal Value As Integer)
                _iId = Value
                NotifyPropertyChanged("Id")
            End Set
        End Property

        Property Codigo() As String
            Get
                Codigo = _sCod
            End Get
            Set(ByVal Value As String)
                _sCod = Value
                NotifyPropertyChanged("Codigo")
            End Set
        End Property

        Property Concepto() As Nullable(Of Integer)
            Get
                Concepto = _iConcepto
            End Get
            Set(ByVal Value As Nullable(Of Integer))
                _iConcepto = Value
                NotifyPropertyChanged("Concepto")
            End Set
        End Property

        Property Retenido() As Boolean
            Get
                Retenido = _bRetenido
            End Get
            Set(ByVal Value As Boolean)
                _bRetenido = Value
                NotifyPropertyChanged("Retenido")
            End Set
        End Property

        Property FechaAct() As Date
            Get
                FechaAct = _dtFechaAct
            End Get
            Set(ByVal Value As Date)
                _dtFechaAct = Value
                NotifyPropertyChanged("FechaAct")
            End Set
        End Property

        Property DenSpa() As String
            Get
                DenSpa = _sDenSpa
            End Get
            Set(ByVal Value As String)
                _sDenSpa = Value
                NotifyPropertyChanged("DenSpa")
            End Set
        End Property

        Property DenEng() As String
            Get
                DenEng = _sDenEng
            End Get
            Set(ByVal Value As String)
                _sDenEng = Value
                NotifyPropertyChanged("DenEng")
            End Set
        End Property

        Property DenGer() As String
            Get
                DenGer = _sDenGer
            End Get
            Set(ByVal Value As String)
                _sDenGer = Value
                NotifyPropertyChanged("DenGer")
            End Set
        End Property

        Property DenFra() As String
            Get
                DenFra = _sDenFra
            End Get
            Set(ByVal Value As String)
                _sDenFra = Value
                NotifyPropertyChanged("DenFra")
            End Set
        End Property

        Property Comentario() As String
            Get
                Comentario = _sComentario
            End Get
            Set(ByVal Value As String)
                _sComentario = Value
                NotifyPropertyChanged("Comentario")
            End Set
        End Property

        Property GrpComp() As Integer
            Get
                GrpComp = _iGrpComp
            End Get
            Set(ByVal Value As Integer)
                _iGrpComp = Value
                NotifyPropertyChanged("GrpComp")
            End Set
        End Property


        Public Property Valores As BindingList(Of ImpuestoValor)
            Get
                Valores = _Valores
            End Get
            Set(ByVal value As BindingList(Of ImpuestoValor))
                _Valores = value
            End Set
        End Property

#End Region

#Region " Constructores "

        '<summary>Es necesario un constructor sin parámetros para las funcionalidades del grid</summary>            

        Public Sub New()
            _Valores = New BindingList(Of ImpuestoValor)
        End Sub

        Public Sub New(ByVal oImpuesto As GSServerModel.Impuesto)
            Me.Id = oImpuesto.Id
            Me.Codigo = oImpuesto.Codigo
            Me.Concepto = oImpuesto.Concepto
            Me.Retenido = oImpuesto.Retenido
            For Each oDen As GSServerModel.Multiidioma In oImpuesto.Denominaciones
                If oDen.Idioma = "SPA" Then Me.DenSpa = oDen.Denominacion
                If oDen.Idioma = "ENG" Then Me.DenEng = oDen.Denominacion
                If oDen.Idioma = "GER" Then Me.DenGer = oDen.Denominacion
                If oDen.Idioma = "FRA" Then Me.DenFra = oDen.Denominacion
            Next
            Me.Comentario = oImpuesto.Comentario
            Me.GrpComp = oImpuesto.GrupoCompatibilidad
            Me.FechaAct = oImpuesto.FechaAct
            If Not oImpuesto.Valores Is Nothing Then
                Me.Valores = New BindingList(Of ImpuestoValor)(oImpuesto.Valores)
            Else
                Me.Valores = New BindingList(Of ImpuestoValor)
            End If
        End Sub

#End Region

        Private Sub NotifyPropertyChanged(ByVal info As String)
            RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(info))
        End Sub

        Public Function ToImpuesto() As GSServerModel.Impuesto
            Dim oImpuesto As New GSServerModel.Impuesto

            oImpuesto.Id = Me.Id
            oImpuesto.Codigo = Me.Codigo
            oImpuesto.Concepto = Me.Concepto
            oImpuesto.Retenido = Me.Retenido
            oImpuesto.Comentario = Me.Comentario
            oImpuesto.GrupoCompatibilidad = Me.GrpComp
            oImpuesto.FechaAct = Me.FechaAct
            'denominaciones
            oImpuesto.Denominaciones = New GSServerModel.Multiidiomas
            If Not Me.DenSpa Is Nothing AndAlso Me.DenSpa <> String.Empty Then
                Dim oDen As New GSServerModel.Multiidioma
                oDen.Idioma = "SPA"
                oDen.Denominacion = Me.DenSpa
                oDen.FecAct = Me.FechaAct
                oImpuesto.Denominaciones.Add(oDen)
            End If
            If Not Me.DenEng Is Nothing AndAlso Me.DenSpa <> String.Empty Then
                Dim oDen As New GSServerModel.Multiidioma
                oDen.Idioma = "ENG"
                oDen.Denominacion = Me.DenEng
                oDen.FecAct = Me.FechaAct
                oImpuesto.Denominaciones.Add(oDen)
            End If
            If Not Me.DenGer Is Nothing AndAlso Me.DenSpa <> String.Empty Then
                Dim oDen As New GSServerModel.Multiidioma
                oDen.Idioma = "GER"
                oDen.Denominacion = Me.DenGer
                oDen.FecAct = Me.FechaAct
                oImpuesto.Denominaciones.Add(oDen)
            End If
            If Not Me.DenFra Is Nothing AndAlso Me.DenSpa <> String.Empty Then
                Dim oDen As New GSServerModel.Multiidioma
                oDen.Idioma = "FRA"
                oDen.Denominacion = Me.DenFra
                oDen.FecAct = Me.FechaAct
                oImpuesto.Denominaciones.Add(oDen)
            End If

            'Valores
            If Not Me.Valores Is Nothing AndAlso Me.Valores.Count > 0 Then
                oImpuesto.Valores = New GSServerModel.ImpuestoValores
                For Each oValor As GSServerModel.ImpuestoValor In Me.Valores
                    oImpuesto.Valores.Add(oValor)
                Next
            End If

            Return oImpuesto
        End Function

        Public Function Duplicate() As ImpuestoView
            Dim oImp As New ImpuestoView
            oImp.Id = _iId
            oImp.Codigo = _sCod
            oImp.Concepto = _iConcepto
            oImp.DenSpa = _sDenSpa
            oImp.DenEng = _sDenEng
            oImp.DenGer = _sDenGer
            oImp.DenFra = _sDenFra
            oImp.FechaAct = _dtFechaAct
            oImp.Retenido = _bRetenido
            oImp.Comentario = _sComentario
            oImp.GrpComp = _iGrpComp
            oImp.Valores = _Valores
            Return oImp
        End Function

        Public Sub Copy(ByVal oImp As ImpuestoView)
            Me.Id = oImp.Id
            Me.Codigo = oImp.Codigo
            Me.Concepto = oImp.Concepto
            Me.DenSpa = oImp.DenSpa
            Me.DenEng = oImp.DenEng
            Me.DenGer = oImp.DenGer
            Me.DenFra = oImp.DenFra
            Me.FechaAct = oImp.FechaAct
            Me.Retenido = oImp.Retenido
            Me.Comentario = oImp.Comentario
            Me.GrpComp = oImp.GrpComp
            Me.Valores = oImp.Valores
        End Sub
    End Class

#End Region

End Class


