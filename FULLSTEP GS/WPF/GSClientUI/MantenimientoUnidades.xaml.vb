﻿Imports System.Data.SqlClient
Imports System.Windows.Markup
Imports System.Windows.Interop
Imports System.Windows.Navigation
Imports Infragistics.Windows.DataPresenter
Imports Infragistics.Windows.DataPresenter.ExcelExporter
Imports System.Diagnostics
Imports Infragistics.Excel
Imports Infragistics.Windows.Editors
Imports System.ComponentModel
Imports Microsoft.Win32
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Infragistics.Windows.Reporting
Imports GSClient
Imports System.Runtime.InteropServices

Partial Public Class MantenimientoUnidades
    <DllImport("user32.dll")> _
    Protected Shared Function SetForegroundWindow(ByVal hWnd As IntPtr) As Boolean
    End Function

    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)> _
    Protected Shared Function FindWindow(ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
    End Function

    Private moIdiomas As GSServerModel.Idiomas
    Private mbAdding As Boolean
    Private msCodigos() As String
    Private mbDeshacer As Boolean
    Private mbActuCodigo As Boolean
    Private mbModifModificar As Boolean
    Private mbModifModificarCod As Boolean
    Private moexportOptions As ExportOptions
    Private mbCancelarUpdate As Boolean
    Private msDatoSincronizado As String
    Private msDatoNoSincronizado As String
    Private msCodigo As String
    Private msDenominacion As String
    Private msFecAct As String
    Private msDeleteSingleRecordPrompt As String
    Private msEstado As String
    Private msEliminarUnidadesTit As String
    Private msEliminarUnidadesTexto As String
    Private msEliminarUnidadTit As String
    Private msEliminarUnidadTexto As String
    Private msNumDecimales As String
    Private mbRecargar As Boolean

    '' <summary>
    '' Generacion de la pantalla MantenimientoUnidades
    '' </summary>
    '' <returns></returns>
    '' <remarks>Llamada desde: Application_Startup  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    '' 
    Public Sub New()
        Dim oIdiomaRule As New GSClient.CIdioma

        InitializeComponent()
        moIdiomas = oIdiomaRule.DevolverIdiomas()
        CargarTextos()
        Cargar()
        Me.cmdEliminar.IsEnabled = False
        Mensajes.Idioma = ModulePublic.ParametrosInstalacion.gsIdioma

    End Sub


    '' <summary>
    '' carga los textos de la pantalla
    '' </summary>
    '' <returns></returns>
    '' <remarks>Llamada desde: New  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    '' 

    Private Sub CargarTextos()
        Dim ds As System.Data.DataSet
        Dim oTextos As New GSClient.CTextos

        ds = oTextos.DevolverTextos(TiposDeDatos.ModuleName.FRM_MANTENIMIENTOUNIDADES, ModulePublic.ParametrosInstalacion.gsIdioma)

        If Not ds Is Nothing Then


            msCodigo = ds.Tables(0).Rows(0).Item(0).ToString()
            msDenominacion = ds.Tables(0).Rows(1).Item(0).ToString()
            msEstado = ds.Tables(0).Rows(2).Item(0).ToString()
            msFecAct = ds.Tables(0).Rows(3).Item(0).ToString()


            Me.cmdEliminar.Tag = ds.Tables(0).Rows(4).Item(0).ToString()
            Me.cmdListado.Tag = ds.Tables(0).Rows(5).Item(0).ToString()
            Me.cmdRecargar.Tag = ds.Tables(0).Rows(6).Item(0).ToString()

            msDatoSincronizado = ds.Tables(0).Rows(7).Item(0).ToString()
            msDatoNoSincronizado = ds.Tables(0).Rows(8).Item(0).ToString()


            msEliminarUnidadesTit = ds.Tables(0).Rows(9).Item(0).ToString()
            msEliminarUnidadesTexto = ds.Tables(0).Rows(10).Item(0).ToString()

            msEliminarUnidadTit = ds.Tables(0).Rows(11).Item(0).ToString()
            msEliminarUnidadTexto = ds.Tables(0).Rows(12).Item(0).ToString() + " XXX " + ds.Tables(0).Rows(13).Item(0).ToString()

            Me.cmdExportar.Tag = ds.Tables(0).Rows(14).Item(0).ToString()
            Me.cmdDeshacer.Tag = ds.Tables(0).Rows(15).Item(0).ToString()

            Me.tbiData.Header = ds.Tables(0).Rows(16).Item(0).ToString()
            Me.tbiPreview.Header = ds.Tables(0).Rows(5).Item(0).ToString()

            msNumDecimales = ds.Tables(0).Rows(17).Item(0).ToString()
            Me.Titulo.Text = ds.Tables(0).Rows(18).Item(0).ToString()
        End If

    End Sub


    '' <summary>
    '' Carga el grid de unidades
    '' </summary>
    '' <returns></returns>
    '' <remarks>Llamada desde: New  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    '' 

    Private Sub Cargar()

        Dim oUnidadesRule As New GSClient.UnidadesRule

        XamDGUnidades.DataSource = oUnidadesRule.DevolverUnidades(GSClient.ParametrosIntegracion.gaExportar(Fullstep.FSNLibrary.TiposDeDatos.EntidadIntegracion.Uni), ModulePublic.ParametrosInstalacion.gsIdioma).Tables(0).DefaultView
        ConfigurarPermisos()
        ConfigurarGrid()

        Me.cmdDeshacer.IsEnabled = False


    End Sub


    '' <summary>
    '' Configura el grid de las unidades
    '' </summary>
    '' <returns></returns>
    '' <remarks>Llamada desde: Cargar  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    '' 

    Private Sub ConfigurarGrid()
        Try

            'Dim st As New Style
            'Dim setter As New Setter(XamTextEditor.FormatProperty, ModulePublic.FormatoFecha)
            'Dim st2 As New Style
            'Dim setter2 As New Setter(XamNumericEditor.FormatProperty, "###.###.##0,##########")
            'Dim setter3 As New Setter(XamNumericEditor.MaskProperty, "")
            Dim stNumdec As New Style

            Dim st3 As New Style
            Dim vc As New ValueConstraint
            Dim vcDenominacion As New ValueConstraint
            Dim Den As String
            Dim oIdi As GSServerModel.Idioma
            Dim sDenominacion As String

            XamDGUnidades.Records.FieldLayout.Fields("COD").Label = msCodigo
            XamDGUnidades.Records.FieldLayout.Fields("FECACT").Label = msFecAct
            XamDGUnidades.Records.FieldLayout.Fields("COD").IsScrollTipField = True
            XamDGUnidades.Records.FieldLayout.Fields("FECACT").Settings.AllowEdit = False


            For Each oIdi In moIdiomas
                sDenominacion = "DEN_"
                sDenominacion = sDenominacion & oIdi.Cod
                XamDGUnidades.Records.FieldLayout.Fields("" & sDenominacion & "").Label = msDenominacion & " " & oIdi.Den
            Next


            'XamDGUnidades.Records.FieldLayout.Fields("FECACT").Settings.EditorType = GetType(XamTextEditor)
            'st.TargetType = GetType(XamTextEditor)
            'st.Setters.Add(setter)
            'XamDGUnidades.Records.FieldLayout.Fields("FECACT").Settings.EditorStyle = st




            vc.MaxLength = LongitudesDeCodigo.giLongCodUNI
            Dim setter4 As New Setter(XamTextEditor.ValueConstraintProperty, vc)
            XamDGUnidades.Records.FieldLayout.Fields("COD").Settings.EditorType = GetType(XamTextEditor)
            st3.TargetType = GetType(XamTextEditor)
            st3.Setters.Add(setter4)
            XamDGUnidades.Records.FieldLayout.Fields("COD").Settings.EditorStyle = st3



            Dim stDenominacion As New Style
            Dim setterDenominacion As New Setter(XamTextEditor.ValueConstraintProperty, vcDenominacion)
            vcDenominacion.MaxLength = ModulePublic.giLongDenMoneda
            stDenominacion.Setters.Add(setterDenominacion)

            For Each oIdi In moIdiomas
                Den = "DEN_"
                Den = Den & oIdi.Cod
                XamDGUnidades.Records.FieldLayout.Fields("" & Den & "").Settings.EditorType = GetType(XamTextEditor)
                stDenominacion.TargetType = GetType(XamTextEditor)
                XamDGUnidades.Records.FieldLayout.Fields("" & Den & "").Settings.EditorStyle = stDenominacion
            Next


            XamDGUnidades.Records.FieldLayout.Fields("COD_ORIG").Visibility = Windows.Visibility.Collapsed
            XamDGUnidades.Records.FieldLayout.Settings.HighlightAlternateRecords = "True"
            XamDGUnidades.FieldLayoutSettings.SupportDataErrorInfo = True

            If GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Uni) Then
                Dim stl As New Style
                stl = Me.FindResource("stsinc")
                XamDGUnidades.Records.FieldLayout.Fields("ESTADOV").Settings.AllowEdit = False
                XamDGUnidades.FieldLayouts(0).Fields("ESTADOV").Settings.EditorStyle = stl
                XamDGUnidades.Records.FieldLayout.Fields("ESTADO").Visibility = Windows.Visibility.Collapsed
                XamDGUnidades.Records.FieldLayout.Fields("ESTADOV").Label = msEstado
            End If


            stNumdec = Me.FindResource("stEntero")
            XamDGUnidades.Records.FieldLayout.Fields("NUMDEC").Settings.EditorStyle = stNumdec
            XamDGUnidades.Records.FieldLayout.Fields("NUMDEC").Label = msNumDecimales


            Dim numIdiomas As Integer
            numIdiomas = moIdiomas.Count

            XamDGUnidades.Records.FieldLayout.Fields("COD").Settings.CellMaxWidth = 75
            XamDGUnidades.Records.FieldLayout.Fields("NUMDEC").Settings.CellMaxWidth = 135
            If numIdiomas < 4 Then
                XamDGUnidades.AutoFit = True
                XamDGUnidades.Records.FieldLayout.Fields("FECACT").Settings.CellMaxWidth = 130
                If GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Uni) Then
                    XamDGUnidades.Records.FieldLayout.Fields("ESTADOV").Settings.CellMaxWidth = 150
                End If
            Else
                For Each oIdi In moIdiomas
                    Den = "DEN_"
                    Den = Den & oIdi.Cod

                    XamDGUnidades.Records.FieldLayout.Fields("" & Den & "").Settings.CellWidth = 300
                Next
            End If


        Catch Ex As Exception

        End Try

    End Sub

    '' <summary>
    '' Configuracion de permisos
    '' </summary>
    '' <returns></returns>
    '' <remarks>Llamada desde: Cargar  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    '' <revision>LTG 30/04/2013</revision>

    Private Sub ConfigurarPermisos()

        If GSClient.ParametrosIntegracion.gaSoloImportar(Fullstep.FSNLibrary.TiposDeDatos.EntidadIntegracion.Uni) Then
            mbModifModificar = False
            mbModifModificarCod = False
            mbActuCodigo = False
            Me.cmdEliminar.IsEnabled = False
            Me.XamDGUnidades.FieldLayoutSettings.AllowAddNew = False
            Me.XamDGUnidades.FieldSettings.AllowEdit = False
            Me.XamDGUnidades.FieldLayoutSettings.AllowDelete = False
            Me.cmdEliminar.Visibility = Windows.Visibility.Collapsed
            Me.cmdDeshacer.Visibility = Windows.Visibility.Collapsed

            If mbRecargar = False Then
                Me.cmdExportar.Margin = Me.cmdEliminar.Margin
                Me.cmdListado.Margin = Me.cmdDeshacer.Margin
            End If
            Me.ButtonBar.Width = 350
            Me.ButtonBorder.Width = 350

        Else            
            'Cargar Acciones para determinar si puede modificar o no
            mbModifModificar = Not (ModulePublic.oUsu.Acciones.Item(CLng(Fullstep.FSNLibrary.TiposDeDatos.AccionesDeSeguridad.UNIModificar)) Is Nothing)
            mbModifModificarCod = Not (ModulePublic.oUsu.Acciones.Item(CLng(Fullstep.FSNLibrary.TiposDeDatos.AccionesDeSeguridad.UNIModificarCodigo)) Is Nothing)

            If Not mbModifModificar Then
                Me.XamDGUnidades.FieldLayoutSettings.AllowAddNew = False
                Me.XamDGUnidades.FieldSettings.AllowEdit = False
                Me.XamDGUnidades.FieldLayoutSettings.AllowDelete = False
                Me.cmdEliminar.Visibility = Windows.Visibility.Collapsed
                Me.cmdDeshacer.Visibility = Windows.Visibility.Collapsed
                If mbRecargar = False Then
                    Me.cmdExportar.Margin = Me.cmdEliminar.Margin
                    Me.cmdListado.Margin = Me.cmdDeshacer.Margin
                End If
            Else
                Me.XamDGUnidades.FieldLayoutSettings.AddNewRecordLocation = AddNewRecordLocation.OnTopFixed
            End If
        End If
    End Sub

    '' <summary>
    '' Evento producido al activar una celda
    '' </summary>
    ' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    '' 
    Private Sub XamDGUnidades_CellActivating(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellActivatingEventArgs) Handles XamDGUnidades.CellActivating        
        If e.Cell.Field.Name = "COD" And mbModifModificarCod Then            
            XamDGUnidades.Records.FieldLayout.Fields("COD").Settings.AllowEdit = True
        Else
            XamDGUnidades.Records.FieldLayout.Fields("COD").Settings.AllowEdit = False
        End If
    End Sub


    '' <summary>
    '' Evento producido al cambiar una celda
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGUnidades_CellChanged(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellChangedEventArgs) Handles XamDGUnidades.CellChanged
        Me.cmdDeshacer.IsEnabled = True
    End Sub


    '' <summary>
    '' Evento producido al añadir un item
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGUnidades_RecordAdded(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordAddedEventArgs) Handles XamDGUnidades.RecordAdded
        mbAdding = True
    End Sub



    '' <summary>
    '' Evento producido al eliminar items
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGUnidades_RecordsDeleting(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordsDeletingEventArgs) Handles XamDGUnidades.RecordsDeleting
        e.DisplayPromptMessage = False
    End Sub



    '' <summary>
    '' Evento producido modificando items
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGUnidades_RecordUpdating(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordUpdatingEventArgs) Handles XamDGUnidades.RecordUpdating

        Dim den As String
        Dim myRecord As DataRecord
        Dim res As Integer
        Dim bSeguir As Boolean
        bSeguir = False

        Dim oUnidadesRule As New GSClient.UnidadesRule

        Dim oUnidad As New GSServerModel.Unidad(False, "", "", True)
        Dim oDenominacion As New GSServerModel.Multiidioma(False, "", "", True)
        Dim oDenominaciones As New GSServerModel.Multiidiomas(False, "", "", True)
        Dim bRealizarCambioCodigo As Boolean

        Dim udtTEsError As New GSServerModel.GSException(False, "", "", True)

        If e.Record.RecordType = RecordType.FilterRecord Then
            Exit Sub
        End If


        If Not mbDeshacer Then
            If mbAdding Then

                'Caso especial 
                If IsDBNull(e.Record.Cells("COD").Value) Then
                    'If IsDBNull(e.Record.Cells("EQUIV").Value) Then
                    For Each oIdi In moIdiomas
                        den = "DEN_"
                        den = den & oIdi.Cod
                        If Not IsDBNull(e.Record.Cells("" & den & "").Value) Then 'Or e.Record.Cells("" & den & "").Value = "" Then
                            bSeguir = True
                            Exit For
                        End If
                    Next
                    If Not bSeguir Then
                        'Significa que ha estado modificando y para salir lo ha dejado vacio
                        ' salimos y en recordUpdated no hace nada
                        e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges
                        Exit Sub
                    End If
                    'End If
                End If


                If IsDBNull(e.Record.Cells("COD").Value) Then ' Or e.Record.Cells("COD").Value = "" Then
                    e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                    Mensajes.NoValido(XamDGUnidades.Records.FieldLayout.Fields("COD").Label)
                    Exit Sub
                End If


                For Each oIdi In moIdiomas
                    den = "DEN_"
                    den = den & oIdi.Cod
                    If IsDBNull(e.Record.Cells("" & den & "").Value) Then 'Or e.Record.Cells("" & den & "").Value = "" Then
                        e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                        Mensajes.CampoVacio(XamDGUnidades.Records.FieldLayout.Fields("" & den & "").Label)
                        Exit Sub
                    End If
                Next

                For Each myRecord In XamDGUnidades.Records
                    If myRecord.IsAddRecord = False Then
                        If myRecord.Cells("COD").Value = e.Record.Cells("COD").Value Then
                            e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                            Mensajes.DatoDuplicado(2)
                            Exit Sub
                        End If
                    End If
                Next
            Else

                If mbCancelarUpdate Then
                    mbCancelarUpdate = False
                    e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateDiscardChanges
                    Exit Sub
                End If

                If IsDBNull(e.Record.Cells("COD").Value) Then 'Or e.Record.Cells("COD").Value = "" Then
                    e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                    Mensajes.CampoVacio(XamDGUnidades.Records.FieldLayout.Fields("COD").Label)
                    Exit Sub
                End If


                For Each oIdi In moIdiomas
                    den = "DEN_"
                    den = den & oIdi.Cod
                    If IsDBNull(e.Record.Cells("" & den & "").Value) Then 'Or e.Record.Cells("" & den & "").Value = "" Then
                        e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                        Mensajes.CampoVacio(XamDGUnidades.Records.FieldLayout.Fields("" & den & "").Label)
                        Exit Sub
                    End If
                Next


                If e.Record.Cells("COD").Value <> e.Record.Cells("COD_ORIG").Value Then
                    If Mensajes.CambioCodigo("Unidad") = vbYes Then

                        res = oUnidadesRule.CambioCodigoUnidad(e.Record.Cells("COD_ORIG").Value, e.Record.Cells("COD").Value, "adm")

                        If res = TiposDeDatos.CambioCodigo.PRESENTE Then
                            Mensajes.CodigoYaPresente("Unidad")
                            e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                            Exit Sub
                        Else
                            If res = TiposDeDatos.CambioCodigo.CAMBIOPENDIENTE Then
                                Mensajes.CambioCodigoPendiente("Unidad")
                                e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                                Exit Sub
                            End If

                        End If



                    Else
                        e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                        Exit Sub
                    End If
                End If

            End If


            If mbDeshacer Then
                Me.cmdDeshacer.IsEnabled = False
                mbDeshacer = False
                Exit Sub
            End If
            If IsDBNull(e.Record.Cells("COD").Value) Then
                mbAdding = False
                Exit Sub
            End If

            If mbAdding Then

                oUnidad.Codigo = e.Record.Cells("COD").Value
                For Each oIdi In moIdiomas
                    den = ""
                    den = "DEN_" & oIdi.Cod

                    oDenominacion = oDenominaciones.Add(oIdi.Cod, e.Record.Cells("" & den & "").Value, DateTime.Now)
                Next
                oUnidad.Denominaciones = oDenominaciones
                If IsDBNull(e.Record.Cells("NUMDEC").Value) Then
                    oUnidad.NumDecimales = Nothing
                Else
                    oUnidad.NumDecimales = e.Record.Cells("NUMDEC").Value
                End If

                udtTEsError = oUnidadesRule.AnyadirUnidad(oUnidad, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(Fullstep.FSNLibrary.TiposDeDatos.EntidadIntegracion.Uni), ModulePublic.sFSP, ModulePublic.oUsu.Cod)

                If udtTEsError.Number <> ErroresGS.TESnoerror Then

                    TratarError(udtTEsError)
                    e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges
                Else
                    e.Record.Cells("FECACT").Value = oUnidad.FecAct
                    e.Record.Cells("COD_ORIG").Value = e.Record.Cells("COD").Value
                    If GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Uni) Then
                        e.Record.Cells("ESTADO").Value = 0
                        e.Record.Cells("ESTADOV").Value = msDatoNoSincronizado
                    End If

                End If


            Else

                oUnidad.Codigo = e.Record.Cells("COD").Value
                oUnidad.FecAct = e.Record.Cells("FECACT").Value
                If e.Record.Cells("COD").Value <> e.Record.Cells("COD_ORIG").Value Then
                    'hacer Cambio de código
                    bRealizarCambioCodigo = True
                End If

                For Each oIdi In moIdiomas
                    den = ""
                    den = "DEN_" & oIdi.Cod
                    oDenominacion = oDenominaciones.Add(oIdi.Cod, e.Record.Cells("" & den & "").Value, e.Record.Cells("FECACT").Value)


                Next

                oUnidad.Denominaciones = oDenominaciones

                If IsDBNull(e.Record.Cells("NUMDEC").Value) Then
                    oUnidad.NumDecimales = Nothing
                Else
                    oUnidad.NumDecimales = e.Record.Cells("NUMDEC").Value
                End If

                udtTEsError = oUnidadesRule.ActualizarUnidad(oUnidad, bRealizarCambioCodigo, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(Fullstep.FSNLibrary.TiposDeDatos.EntidadIntegracion.Uni), ModulePublic.sFSP, ModulePublic.oUsu.Cod)

                If udtTEsError.Number <> ErroresGS.TESnoerror Then

                    TratarError(udtTEsError)
                    e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges

                Else
                    '' Registro de acciones
                    e.Record.Cells("FECACT").Value = oUnidad.FecAct
                    e.Record.Cells("COD_ORIG").Value = e.Record.Cells("COD").Value
                End If


            End If


            Me.cmdDeshacer.IsEnabled = False
            mbAdding = False

        End If
    End Sub


    '' <summary>
    '' Click del boton recargar
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub cmdRecargar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdRecargar.Click
        XamDGUnidades.DataSource = Nothing

        mbRecargar = True
        Cargar()
        Me.cmdEliminar.IsEnabled = False
        mbRecargar = False

    End Sub

    '' <summary>
    '' Click del boton Eliminar
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub cmdEliminar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdEliminar.Click
        Try
            Dim b As DataRecord
            Dim iRespuesta As Integer
            Dim sEliminarUnidadTexto As String

            Try
                If mbModifModificar Then
                    If XamDGUnidades.SelectedItems.Count > 1 Then
                        iRespuesta = Mensajes.EliminarObjeto(msEliminarUnidadesTexto)
                        If iRespuesta = vbNo Then
                            Exit Sub
                        End If
                    Else
                        If XamDGUnidades.SelectedItems.Count = 1 Then
                            'For i = 0 To e.Records.Count - 1

                            b = CType(XamDGUnidades.SelectedItems.Records(0), DataRecord)

                            sEliminarUnidadTexto = msEliminarUnidadTexto.Replace("XXX", b.Cells("COD").Value)

                            iRespuesta = Mensajes.EliminarObjeto(sEliminarUnidadTexto)
                            If iRespuesta = vbNo Then
                                Exit Sub
                            End If
                        Else
                            Exit Sub
                        End If
                    End If
                    Me.XamDGUnidades.FieldLayoutSettings.AllowDelete = True
                    EliminarUnidades()
                    Me.XamDGUnidades.FieldLayoutSettings.AllowDelete = False
                Else
                    Exit Sub
                End If
            Catch ex As Exception
            End Try

        Catch Ex As Exception

        End Try

    End Sub


    '' <summary>
    '' Eliminacion de Unidades
    '' </summary>    
    '' <returns></returns>
    '' <remarks>Llamada desde: cmdEliminarClick  </remarks>
    '' <remarks>Tiempo mÃ¡ximo: 0 </remarks>
    Private Sub EliminarUnidades()

        Dim udtTEsError As New GSServerModel.GSException(False, "", "", True)
        Dim oUnidadesRule As New GSClient.UnidadesRule
        Dim b As DataRecord

        Dim i As Integer

        ReDim msCodigos(XamDGUnidades.SelectedItems.Count - 1)

        For i = 0 To XamDGUnidades.SelectedItems.Count - 1

            b = CType(XamDGUnidades.SelectedItems.Records(i), DataRecord)
            msCodigos(i) = b.Cells("COD").Value
        Next i


        udtTEsError = oUnidadesRule.EliminarUnidades(msCodigos, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(Fullstep.FSNLibrary.TiposDeDatos.EntidadIntegracion.Uni), ModulePublic.sFSP, ModulePublic.oUsu.Cod)

        If udtTEsError.Number <> ErroresGS.TESnoerror Then
            If udtTEsError.Number = ErroresGS.TESImposibleEliminar Then
                ImposibleEliminacionMultiple(9, udtTEsError)
                For i = 0 To udtTEsError.Errores.Count - 1
                    XamDGUnidades.SelectedItems.Records.Item(udtTEsError.Errores(i).Numero).IsSelected = False
                Next i

                XamDGUnidades.ExecuteCommand(DataPresenterCommands.DeleteSelectedDataRecords)
            Else
                TratarError(udtTEsError)
            End If

        Else
            XamDGUnidades.ExecuteCommand(DataPresenterCommands.DeleteSelectedDataRecords)
        End If

    End Sub

    '' <summary>
    '' Evento producido al cambiar los items seleccionados
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub XamDGUnidades_SelectedItemsChanged(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs) Handles XamDGUnidades.SelectedItemsChanged
        If mbModifModificar Then
            If XamDGUnidades.SelectedItems.Count > 0 Then
                cmdEliminar.IsEnabled = True
            Else
                cmdEliminar.IsEnabled = False
            End If
        End If
    End Sub


    '' <summary>
    '' Funcion de exportar el grid
    '' </summary>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>


    Public ReadOnly Property ExportOptions() As ExportOptions
        Get
            If (IsNothing(Me.moexportOptions)) Then
                Me.moexportOptions = New ExportOptions()

            End If
            Return Me.moexportOptions


        End Get
    End Property

    '' <summary>
    '' Click del boton Exportar
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub cmdExportar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdExportar.Click

        Dim sfileName As String
        sfileName = System.IO.Path.GetTempPath() + "\Unidades.xls"
        Try

            ' Get the instance of the DataPresenterExcelExporter we defined in the Page's resources and call its Export
            ' method passing in the XamDataGrid we want to export, the name of the file we would like
            ' to export to and the format of the Workbook we want to Exporter to create.
            '
            ' NOTE: there are 11 overloads to the Export method that give you control over different
            ' aspects of the exporting process.

            Dim exporter As New DataPresenterExcelExporter()
            exporter = CType(Me.Resources("Exporter"), DataPresenterExcelExporter)

            'exporter.Export(Me.XamDGUnidades, fileName, WorkbookFormat.Excel2007, this.ExportOptions);
            exporter.Export(Me.XamDGUnidades, sfileName, WorkbookFormat.Excel97To2003, Me.ExportOptions)

        Catch ex As Exception

            ' It's possible that the exporter does not have permission to write the file, so it's generally
            ' a good idea to account for this possibility.
            'MessageBox.Show(string.Format(Strings.ExportToExcel_Message_ExcelExportError_Text, ex.Message),
            '                                Strings.ExportToExcel_Message_ExcelError_Caption,
            '                               MessageBoxButton.OK,
            '                              MessageBoxImage.Exclamation);
            '
            '              return;
        End Try


        ' Display an 'Export Completed' message.
        'MessageBox.Show(Strings.ExportToExcel_Message_ExportCompleted_Text,
        '					Strings.ExportToExcel_Message_ExportCompleted_Caption,
        '					MessageBoxButton.OK,
        '						MessageBoxImage.Information);

        ' Execute Excel to display the exported workbook.
        Try
            Dim oExcel As Object = CreateObject("Excel.Application")
            oExcel.Visible = True
            oExcel.Workbooks.Open(sfileName)
            Dim handler As IntPtr = FindWindow(Nothing, oExcel.Caption)
            SetForegroundWindow(handler)

        Catch Ex As Exception
            MessageBox.Show("Error al generar el excel","Error",MessageBoxButton.OK, MessageBoxImage.Exclamation)
        End Try

    End Sub




    Private Sub cmdDeshacer_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Input.KeyEventArgs) Handles cmdDeshacer.PreviewKeyDown
        mbDeshacer = True
        XamDGUnidades.ExecuteCommand(DataPresenterCommands.DiscardChangesToActiveRecord)
        mbDeshacer = False
        Me.cmdDeshacer.IsEnabled = False
        mbAdding = False
    End Sub


    Private Sub cmdDeshacer_PreviewMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles cmdDeshacer.PreviewMouseDoubleClick
        mbDeshacer = True
        XamDGUnidades.ExecuteCommand(DataPresenterCommands.DiscardChangesToActiveRecord)
        mbDeshacer = False
        Me.cmdDeshacer.IsEnabled = False
        mbAdding = False
    End Sub

    Private Sub cmdDeshacer_PreviewMouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles cmdDeshacer.PreviewMouseDown
        mbDeshacer = True
        XamDGUnidades.ExecuteCommand(DataPresenterCommands.DiscardChangesToActiveRecord)
        mbDeshacer = False
        Me.cmdDeshacer.IsEnabled = False
        mbAdding = False
    End Sub

    '' <summary>
    '' Click del boton del listado
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub cmdListado_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdListado.Click
        Dim myReport As New Infragistics.Windows.Reporting.Report()
        Dim myXamDataGridReport As New Infragistics.Windows.Reporting.EmbeddedVisualReportSection(Me.XamDGUnidades)

        Me.tbiPreview.Visibility = Windows.Visibility.Visible
        Me.tbiPreview.IsSelected = True
        XamReportPreview1.Visibility = Windows.Visibility.Visible
        myReport.Sections.Add(myXamDataGridReport)
        Me.tbiData.Width = Me.tbiPreview.Width
        XamReportPreview1.GeneratePreview(myReport, False, True)

        'Dim saveDlg As New SaveFileDialog()
        'saveDlg.Filter = "XPS documents|*.xps"
        'If (Not saveDlg.ShowDialog().GetValueOrDefault()) Then
        '    Return
        'End If

        'Dim reportObj As New Report()
        'Dim section As New EmbeddedVisualReportSection(Me.XamDGUnidades)
        'reportObj.Sections.Add(section)
        ''progressInfo.Report = reportObj;
        'reportObj.Export(OutputFormat.XPS, saveDlg.FileName)


    End Sub


    '' <summary>
    '' Evento producido al cambiar una celda del grid
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub XamDGUnidades_CellUpdated(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellUpdatedEventArgs) Handles XamDGUnidades.CellUpdated

        If e.Cell.IsDataChanged And e.Cell.Field.Name = "COD" And Not mbAdding And Not mbActuCodigo Then

            If Mensajes.CambioCodigo("Unidad") = vbYes Then

                Dim oUnidadesRule As New GSClient.UnidadesRule
                Dim res As Integer
                res = oUnidadesRule.CambioCodigoUnidad(e.Record.Cells("COD_ORIG").Value, e.Record.Cells("COD").Value, "adm")
                If res = TiposDeDatos.CambioCodigo.PRESENTE Then
                    Mensajes.CodigoYaPresente("Unidad")
                    mbActuCodigo = True
                    e.Record.Cells("COD").Value = e.Record.Cells("COD_ORIG").Value
                    mbActuCodigo = True
                    mbCancelarUpdate = True
                    Exit Sub
                Else
                    If res = TiposDeDatos.CambioCodigo.CAMBIOPENDIENTE Then
                        Mensajes.CambioCodigoPendiente("Unidad")
                        mbActuCodigo = True
                        e.Record.Cells("COD").Value = e.Record.Cells("COD_ORIG").Value
                        mbActuCodigo = False
                        mbCancelarUpdate = True
                        Exit Sub
                    End If

                End If

                mbActuCodigo = True
                e.Record.Cells("COD").Value = e.Record.Cells("COD_ORIG").Value
                mbActuCodigo = False

            Else
                If Not mbAdding Then
                    mbActuCodigo = True
                    e.Record.Cells("COD").Value = e.Record.Cells("COD_ORIG").Value
                    'e.Record.CancelUpdate()
                    mbCancelarUpdate = True
                    mbActuCodigo = False
                    Exit Sub
                End If
            End If

        End If


    End Sub


    Private Sub MantenimientoUnidades_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        Dim sCadenaEntrada As String
        sCadenaEntrada = "Inicio Pagina Mantenimiento Unidades"

        Dim oRegistro As New GSClient.RegistrosRule()

        oRegistro.RegistrarEntradaOSalida(oUsu.Cod, AccionesGS.ACCUniCon, sCadenaEntrada)

    End Sub


    Protected Overrides Sub Finalize()
        Dim sCadenaEntrada As String
        sCadenaEntrada = "Fin Pagina Mantenimiento Unidades"

        Dim oRegistro As New GSClient.RegistrosRule()

        oRegistro.RegistrarEntradaOSalida(oUsu.Cod, AccionesGS.ACCUniCon, sCadenaEntrada)

        MyBase.Finalize()

    End Sub

    '' <summary>
    '' Funcion que lanza la accion de eliinar en caso de que se pulse el boton suprimir
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo mÃ¡ximo: 0 </remarks>

    Private Sub XamDGUnidades_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Input.KeyEventArgs) Handles XamDGUnidades.KeyDown
        If e.Key = Key.Delete Then
            cmdEliminar_Click(Me.XamDGUnidades, e)
        End If
    End Sub

    '' <summary>
    '' Funcion que oculta los tabs cuando se cierra el del listado
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo mÃ¡ximo: 0 </remarks>
    Private Sub tbiPreview_Closing(ByVal sender As Object, ByVal e As Infragistics.Windows.Controls.Events.TabClosingEventArgs) Handles tbiPreview.Closing
        Me.tbiData.Width = 0
    End Sub

End Class
