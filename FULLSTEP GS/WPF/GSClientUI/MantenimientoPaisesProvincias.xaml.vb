﻿Imports System.Data.SqlClient
Imports System.Windows.Markup
Imports System.Windows.Interop
Imports System.Windows.Navigation
Imports Infragistics.Windows.DataPresenter
Imports Infragistics.Windows.DataPresenter.ExcelExporter
Imports System.Diagnostics
Imports Infragistics.Excel
Imports Infragistics.Windows.Editors
Imports System.ComponentModel
Imports Microsoft.Win32
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Infragistics.Windows.Reporting
Imports GSClient
Imports GSClient.ModulePublic
Imports System.Runtime.InteropServices

Partial Public Class MantenimientoPaisesProvincias
    <DllImport("user32.dll")> _
    Protected Shared Function SetForegroundWindow(ByVal hWnd As IntPtr) As Boolean
    End Function

    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)> _
    Protected Shared Function FindWindow(ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
    End Function

    Private moIdiomas As GSServerModel.Idiomas
    Private mbAdding As Boolean
    Private msCodigos() As String
    Private mbDeshacer As Boolean
    Private mbActuCodigo As Boolean
    Private mbSoloActCambio As Boolean
    Private mbModifModificar As Boolean
    Private mbModifModificarCod As Boolean
    Private moexportOptions As ExportOptions
    Private mbCancelarUpdate As Boolean
    Private msDatoSincronizado As String
    Private msDatoNoSincronizado As String
    Private msCodigo As String
    Private msDenominacion As String
    Private msEquivalencia As String
    Private msFecAct As String
    Private msDeleteSingleRecordPrompt As String
    Private msEstado As String
    Private msMoneda As String
    Private msEliminarPaisTexto As String
    Private msEliminarProvinciaTexto As String

    Private msCodigosProvi() As String
    Private msCodigosPaisProvi() As String
    Private mbRecargar As Boolean

    Public Sub New()
        Dim oIdiomaRule As New GSClient.CIdioma

        InitializeComponent()
        moIdiomas = oIdiomaRule.DevolverIdiomas()
        CargarTextos()
        Cargar()
        Me.cmdEliminar.IsEnabled = False
        Mensajes.Idioma = ModulePublic.ParametrosInstalacion.gsIdioma


    End Sub

    Private Sub Cargar()

        Dim oPaisesProvincias As New GSClient.PaisesProvinciasRule

        Dim ds As New System.Data.DataSet

        ds = oPaisesProvincias.DevolverPaisesProvincias(GSClient.ParametrosIntegracion.gaExportar(Fullstep.FSNLibrary.TiposDeDatos.EntidadIntegracion.Pai), ModulePublic.ParametrosInstalacion.gsIdioma)

        XamDGPaisesProvincias.DataSource = ds.Tables(0).DefaultView
        ConfigurarPermisos()
        ConfigurarGrid()

        'Me.cmdDeshacer.IsEnabled = False

    End Sub

    '' <summary>
    '' carga los textos de la pantalla
    '' </summary>
    '' <returns></returns>
    '' <remarks>Llamada desde: New  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    '' 

    Private Sub CargarTextos()
        Dim ds As System.Data.DataSet
        Dim oTextos As New GSClient.CTextos

        ds = oTextos.DevolverTextos(TiposDeDatos.ModuleName.FRM_MANTENIMIENTOPAISESPROVINCIAS, ModulePublic.ParametrosInstalacion.gsIdioma)

        If Not ds Is Nothing Then

            msCodigo = ds.Tables(0).Rows(0).Item(0).ToString()
            msDenominacion = ds.Tables(0).Rows(1).Item(0).ToString()
            msEstado = ds.Tables(0).Rows(2).Item(0).ToString()
            msMoneda = ds.Tables(0).Rows(3).Item(0).ToString()


            Me.cmdEliminar.Tag = ds.Tables(0).Rows(4).Item(0).ToString()
            Me.cmdListado.Tag = ds.Tables(0).Rows(5).Item(0).ToString()
            Me.cmdRecargar.Tag = ds.Tables(0).Rows(6).Item(0).ToString()

            msDatoSincronizado = ds.Tables(0).Rows(7).Item(0).ToString()
            msDatoNoSincronizado = ds.Tables(0).Rows(8).Item(0).ToString()

            msEliminarPaisTexto = ds.Tables(0).Rows(9).Item(0).ToString() + " XXX " + ds.Tables(0).Rows(11).Item(0).ToString()
            msEliminarProvinciaTexto = ds.Tables(0).Rows(10).Item(0).ToString() + " XXX " + ds.Tables(0).Rows(11).Item(0).ToString()

            Me.cmdExportar.Tag = ds.Tables(0).Rows(12).Item(0).ToString()
            Me.cmdDeshacer.Tag = ds.Tables(0).Rows(13).Item(0).ToString()

            Me.tbiData.Header = ds.Tables(0).Rows(14).Item(0).ToString()
            Me.tbiPreview.Header = ds.Tables(0).Rows(5).Item(0).ToString()

            Me.Titulo.Text = ds.Tables(0).Rows(15).Item(0).ToString()
        End If

    End Sub

    '' <summary>Configuracion de permisos</summary>
    '' <returns></returns>
    '' <remarks>Llamada desde: Cargar  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    '' <revision>LTG 30/04/2013</revision>

    Private Sub ConfigurarPermisos()
        If GSClient.ParametrosIntegracion.gaSoloImportar(Fullstep.FSNLibrary.TiposDeDatos.EntidadIntegracion.Pai) Then
            mbModifModificar = False
            mbModifModificarCod = False
            mbActuCodigo = False
            Me.cmdEliminar.IsEnabled = False
            Me.XamDGPaisesProvincias.FieldLayoutSettings.AllowAddNew = False
            Me.XamDGPaisesProvincias.FieldSettings.AllowEdit = False
            Me.XamDGPaisesProvincias.FieldLayoutSettings.AllowDelete = False
            'Me.cmdEliminar.Visibility = Windows.Visibility.Hidden
            'Me.cmdDeshacer.Visibility = Windows.Visibility.Hidden
            Me.cmdEliminar.Visibility = Windows.Visibility.Collapsed
            Me.cmdDeshacer.Visibility = Windows.Visibility.Collapsed
            If mbRecargar = False Then
                Me.cmdExportar.Margin = Me.cmdEliminar.Margin
                Me.cmdListado.Margin = Me.cmdDeshacer.Margin
            End If
            Me.ButtonBar.Width = 350
            Me.ButtonBorder.Width = 350
        Else            
            'Cargar Acciones para determinar si puede modificar o no
            mbModifModificar = Not (ModulePublic.oUsu.Acciones.Item(CLng(Fullstep.FSNLibrary.TiposDeDatos.AccionesDeSeguridad.PAIModificar)) Is Nothing)
            mbModifModificarCod = Not (ModulePublic.oUsu.Acciones.Item(CLng(Fullstep.FSNLibrary.TiposDeDatos.AccionesDeSeguridad.PAIModificarCodigo)) Is Nothing)

            If Not mbModifModificar Then
                Me.XamDGPaisesProvincias.FieldLayoutSettings.AllowAddNew = False
                Me.XamDGPaisesProvincias.FieldSettings.AllowEdit = False
                Me.XamDGPaisesProvincias.FieldLayoutSettings.AllowDelete = False
                Me.cmdEliminar.Visibility = Windows.Visibility.Collapsed
                Me.cmdDeshacer.Visibility = Windows.Visibility.Collapsed
                If mbRecargar = False Then
                    Me.cmdExportar.Margin = Me.cmdEliminar.Margin
                    Me.cmdListado.Margin = Me.cmdDeshacer.Margin

                End If
            Else
                Me.XamDGPaisesProvincias.FieldLayoutSettings.AddNewRecordLocation = AddNewRecordLocation.OnTopFixed                
            End If
        End If
    End Sub

    '' <summary>
    '' Configura el grid de los paises y provincias
    '' </summary>
    '' <returns></returns>
    '' <remarks>Llamada desde: Cargar  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    '' 

    Private Sub ConfigurarGrid()
        Try

            'Dim st As New Style
            'Dim setter As New Setter(XamTextEditor.FormatProperty, "dd/MM/yyyy hh:mm:ss")
            'Dim st2 As New Style
            'Dim setter2 As New Setter(XamNumericEditor.FormatProperty, "###.###.##0,##########")
            'Dim setter3 As New Setter(XamNumericEditor.MaskProperty, "")
            Dim st3 As New Style
            Dim st4 As New Style
            Dim vc As New ValueConstraint
            Dim vcDenominacion As New ValueConstraint
            Dim Den As String
            Dim oIdi As GSServerModel.Idioma
            Dim sDenominacion As String

            XamDGPaisesProvincias.Records.FieldLayout.Fields("PAICOD").Label = msCodigo
            XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIMON").Label = msMoneda
            XamDGPaisesProvincias.FieldLayouts(1).Fields("PROVICOD").Label = msCodigo

            XamDGPaisesProvincias.Records.FieldLayout.Fields("PAICOD").IsScrollTipField = True
            XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIFECACT").Settings.AllowEdit = False
            XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVIFECACT").Settings.AllowEdit = False

            XamDGPaisesProvincias.FieldLayoutSettings.ExpansionIndicatorDisplayMode = ExpansionIndicatorDisplayMode.CheckOnDisplay


            For Each oIdi In moIdiomas
                sDenominacion = "DEN_"
                sDenominacion = sDenominacion & oIdi.Cod
                XamDGPaisesProvincias.Records.FieldLayout.Fields("" & sDenominacion & "").Label = msDenominacion & " " & oIdi.Den
                XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("" & sDenominacion & "").Label = msDenominacion & " " & oIdi.Den
            Next


            'XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIFECACT").Settings.EditorType = GetType(XamTextEditor)
            'st.TargetType = GetType(XamTextEditor)
            'st.Setters.Add(setter)
            'XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIFECACT").Settings.EditorStyle = st


            vc.MaxLength = LongitudesDeCodigo.giLongCodPAI
            Dim setter4 As New Setter(XamTextEditor.ValueConstraintProperty, vc)
            XamDGPaisesProvincias.Records.FieldLayout.Fields("PAICOD").Settings.EditorType = GetType(XamTextEditor)
            st3.TargetType = GetType(XamTextEditor)
            st3.Setters.Add(setter4)
            XamDGPaisesProvincias.Records.FieldLayout.Fields("PAICOD").Settings.EditorStyle = st3


            vc.MaxLength = LongitudesDeCodigo.giLongCodPROVI
            Dim setter5 As New Setter(XamTextEditor.ValueConstraintProperty, vc)
            XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVICOD").Settings.EditorType = GetType(XamTextEditor)
            st4.TargetType = GetType(XamTextEditor)
            st4.Setters.Add(setter5)
            XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVICOD").Settings.EditorStyle = st4

            Dim stDenominacion As New Style
            Dim setterDenominacion As New Setter(XamTextEditor.ValueConstraintProperty, vcDenominacion)
            vcDenominacion.MaxLength = ModulePublic.giLongDenMoneda
            stDenominacion.Setters.Add(setterDenominacion)

            For Each oIdi In moIdiomas
                Den = "DEN_"
                Den = Den & oIdi.Cod
                XamDGPaisesProvincias.Records.FieldLayout.Fields("" & Den & "").Settings.EditorType = GetType(XamTextEditor)
                XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("" & Den & "").Settings.EditorType = GetType(XamTextEditor)
                stDenominacion.TargetType = GetType(XamTextEditor)
                XamDGPaisesProvincias.Records.FieldLayout.Fields("" & Den & "").Settings.EditorStyle = stDenominacion
                XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("" & Den & "").Settings.EditorStyle = stDenominacion
            Next


            Dim a As New ComboBoxItemsProvider
            Dim oPaisesProvincias As New GSClient.PaisesProvinciasRule

            Dim ds As New System.Data.DataSet

            Dim xamComboEditorStyle As New Style(GetType(XamComboEditor))

            ds = oPaisesProvincias.DevolverMonedasPais(ModulePublic.ParametrosInstalacion.gsIdioma)

            a.ItemsSource = ds.Tables(0).DefaultView

            'Dim itemsProviderSetter As New Setter(XamComboEditor.ItemsProviderProperty, a)
            Dim itemsProviderSetter As New Setter(XamComboEditor.ItemsSourceProperty, ds.Tables(0).DefaultView)
            Dim itemsProviderSetter2 As New Setter(XamComboEditor.DisplayMemberPathProperty, "VISIBLE")
            Dim itemsProviderSetter3 As New Setter(XamComboEditor.ValuePathProperty, "COD")

            xamComboEditorStyle.Setters.Add(itemsProviderSetter)
            xamComboEditorStyle.Setters.Add(itemsProviderSetter2)
            xamComboEditorStyle.Setters.Add(itemsProviderSetter3)

            XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(0).Fields("PAIMON").Settings.EditorStyle = xamComboEditorStyle
            XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIMON").Settings.EditorType = GetType(XamComboEditor)


            XamDGPaisesProvincias.Records.FieldLayout.Fields("COD_PAIORIG").Visibility = Windows.Visibility.Collapsed
            XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIFECACT").Visibility = Windows.Visibility.Collapsed
            XamDGPaisesProvincias.Records.FieldLayout.Fields("MONCOD").Visibility = Windows.Visibility.Collapsed
            XamDGPaisesProvincias.Records.FieldLayout.Fields("MONDEN").Visibility = Windows.Visibility.Collapsed


            XamDGPaisesProvincias.Records.FieldLayout.Settings.HighlightAlternateRecords = "True"
            XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Settings.HighlightAlternateRecords = "True"

            XamDGPaisesProvincias.FieldLayoutSettings.SupportDataErrorInfo = True

            XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("COD_PROVIORIG").Visibility = Windows.Visibility.Collapsed
            XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVIPAI").Visibility = Windows.Visibility.Collapsed
            XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVIFECACT").Visibility = Windows.Visibility.Collapsed

            If GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Pai) Then
                Dim stl As New Style
                stl = Me.FindResource("stsinc")
                XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIESTADOV").Settings.AllowEdit = False
                XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIESTADOV").Settings.EditorStyle = stl
                XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIESTADO").Visibility = Windows.Visibility.Collapsed
                XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIESTADOV").Label = msEstado

                XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVIESTADOV").Settings.EditorStyle = stl
                XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVIESTADO").Visibility = Windows.Visibility.Collapsed
                XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVIESTADOV").Label = msEstado
                XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVIESTADOV").Settings.AllowEdit = False

            End If


            Dim numIdiomas As Integer
            numIdiomas = moIdiomas.Count

            XamDGPaisesProvincias.Records.FieldLayout.Fields("PAICOD").Settings.CellMaxWidth = 75
            XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVICOD").Settings.CellMaxWidth = 75
            If numIdiomas < 4 Then
                XamDGPaisesProvincias.AutoFit = True

                XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIFECACT").Settings.CellMaxWidth = 120
                XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVIFECACT").Settings.CellMaxWidth = 120
                If GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Pai) Then
                    XamDGPaisesProvincias.Records.FieldLayout.Fields("PAIESTADOV").Settings.CellMaxWidth = 150
                    XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("PROVIESTADO").Settings.CellMaxWidth = 150
                End If
            Else
                For Each oIdi In moIdiomas
                    Den = "DEN_"
                    Den = Den & oIdi.Cod

                    XamDGPaisesProvincias.Records.FieldLayout.Fields("" & Den & "").Settings.CellWidth = 300
                    XamDGPaisesProvincias.Records.FieldLayout.DataPresenter.FieldLayouts(1).Fields("" & Den & "").Settings.CellWidth = 330
                Next
            End If


        Catch Ex As Exception

        End Try

    End Sub


    '' <summary>
    '' Evento producido al activar una celda
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    '' 
    Private Sub XamDGPaisesProvincias_CellActivating(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellActivatingEventArgs) Handles XamDGPaisesProvincias.CellActivating        
        If e.Cell.Field.Name = "PAICOD" And mbModifModificarCod And Not GSClient.ParametrosIntegracion.gaSoloImportar(Fullstep.FSNLibrary.TiposDeDatos.EntidadIntegracion.Pai) Then
            XamDGPaisesProvincias.Records.FieldLayout.Fields("PAICOD").Settings.AllowEdit = True            
        Else
            If e.Cell.Field.Name = "PROVICOD" And mbModifModificarCod And Not GSClient.ParametrosIntegracion.gaSoloImportar(Fullstep.FSNLibrary.TiposDeDatos.EntidadIntegracion.Pai) Then
                XamDGPaisesProvincias.FieldLayouts(1).Fields("PROVICOD").Settings.AllowEdit = True                
            Else
                XamDGPaisesProvincias.FieldLayouts(1).Fields("PROVICOD").Settings.AllowEdit = False
                XamDGPaisesProvincias.Records.FieldLayout.Fields("PAICOD").Settings.AllowEdit = False
            End If
        End If        
    End Sub

    '' <summary>
    '' Evento producido al cambiar una celda
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGPaisesProvincias_CellChanged(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellChangedEventArgs) Handles XamDGPaisesProvincias.CellChanged
        Me.cmdDeshacer.IsEnabled = True
    End Sub


    '' <summary>
    '' Evento producido al añadir un item
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGPaisesProvincias_RecordAdded(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordAddedEventArgs) Handles XamDGPaisesProvincias.RecordAdded
        mbAdding = True
    End Sub


    '' <summary>
    '' Evento producido al eliminar items
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGPaisesProvincias_RecordsDeleting(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordsDeletingEventArgs) Handles XamDGPaisesProvincias.RecordsDeleting

        e.DisplayPromptMessage = False

    End Sub


    '' <summary>
    '' Evento producido modificando items
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGPaisesProvincias_RecordUpdating(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordUpdatingEventArgs) Handles XamDGPaisesProvincias.RecordUpdating

        Dim den As String
        Dim myRecord As DataRecord

        Dim bSeguir As Boolean
        bSeguir = False

        Dim oPaisesProvinciasRule As New GSClient.PaisesProvinciasRule
        Dim oMoneda As New GSServerModel.Moneda(False, "", "", True)

        Dim oPais As New GSServerModel.Pais(False, "", "", True)
        Dim oDenominacion As New GSServerModel.Multiidioma(False, "", "", True)
        Dim oDenominaciones As New GSServerModel.Multiidiomas(False, "", "", True)
        Dim bRealizarCambioCodigo As Boolean

        Dim oProvincia As New GSServerModel.Provincia(False, "", "", True)
        Dim udtTEsError As New GSServerModel.GSException(False, "", "", True)

        If e.Record.RecordType = RecordType.FilterRecord Then
            Exit Sub
        End If

        Dim bEsPais As Boolean
        Dim sCad As String
        If Not mbDeshacer Then
            Try
                sCad = e.Record.Cells("PROVICOD").Value
            Catch ex As Exception
                bEsPais = True
            End Try

            If mbAdding Then

                If bEsPais Then

                    If IsDBNull(e.Record.Cells("PAICOD").Value) Then ' Or e.Record.Cells("COD").Value = "" Then
                        e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                        Mensajes.NoValido(XamDGPaisesProvincias.Records.FieldLayout.Fields("PAICOD").Label)

                        Exit Sub
                    End If

                    For Each oIdi In moIdiomas
                        den = "DEN_"
                        den = den & oIdi.Cod
                        If IsDBNull(e.Record.Cells("" & den & "").Value) Then
                            e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                            Mensajes.CampoVacio(XamDGPaisesProvincias.Records.FieldLayout.Fields("" & den & "").Label)
                            Exit Sub
                        End If
                    Next

                    For Each myRecord In XamDGPaisesProvincias.Records
                        If myRecord.IsAddRecord = False Then
                            If myRecord.Cells("PAICOD").Value = e.Record.Cells("PAICOD").Value Then
                                e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                                Mensajes.DatoDuplicado(2)
                                Exit Sub
                            End If
                        End If
                    Next


                Else

                    If IsDBNull(e.Record.Cells("PROVICOD").Value) Then ' Or e.Record.Cells("COD").Value = "" Then
                        e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                        Mensajes.NoValido(XamDGPaisesProvincias.FieldLayouts(1).Fields("PROVICOD").Label)

                        Exit Sub
                    End If

                    For Each oIdi In moIdiomas
                        den = "DEN_"
                        den = den & oIdi.Cod
                        If IsDBNull(e.Record.Cells("" & den & "").Value) Then
                            e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                            Mensajes.CampoVacio(XamDGPaisesProvincias.FieldLayouts(1).Fields("" & den & "").Label)
                            Exit Sub
                        End If
                    Next

                    'For Each myRecord In XamDGPaisesProvincias.Records(e.Record.ParentRecord.Index.).R
                    For Each myRecord In e.Record.ParentCollection
                        If myRecord.IsAddRecord = False Then
                            If myRecord.Cells("PROVICOD").Value = e.Record.Cells("PROVICOD").Value Then
                                e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                                Mensajes.DatoDuplicado(2)
                                Exit Sub
                            End If
                        End If
                    Next

                End If


            Else

                If mbCancelarUpdate Then
                    mbCancelarUpdate = False
                    e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateDiscardChanges
                    Exit Sub
                End If

                If bEsPais Then
                    If IsDBNull(e.Record.Cells("PAICOD").Value) Then
                        e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                        Mensajes.CampoVacio(XamDGPaisesProvincias.FieldLayouts(0).Fields("PAICOD").Label)
                        Exit Sub
                    End If


                    For Each oIdi In moIdiomas
                        den = "DEN_"
                        den = den & oIdi.Cod
                        If IsDBNull(e.Record.Cells("" & den & "").Value) Then 'Or e.Record.Cells("" & den & "").Value = "" Then
                            e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                            Mensajes.CampoVacio(XamDGPaisesProvincias.FieldLayouts(0).Fields("" & den & "").Label)
                            Exit Sub
                        End If
                    Next



                    'For Each myRecord In XamDGPaisesProvincias.Records
                    If e.Record.Cells("PAICOD").Value <> e.Record.Cells("COD_PAIORIG").Value Then
                        If Mensajes.CambioCodigo("Pais") = vbYes Then

                            If oPaisesProvinciasRule.CambioCodigoPais(e.Record.Cells("COD_PAIORIG").Value, e.Record.Cells("PAICOD").Value, "adm") Then
                                Mensajes.CodigoYaPresente("Pais")
                                e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                                Exit Sub

                            Else

                                e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                                Exit Sub
                            End If
                        End If
                    End If


                Else
                    If IsDBNull(e.Record.Cells("PROVICOD").Value) Then
                        e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                        Mensajes.CampoVacio(XamDGPaisesProvincias.FieldLayouts(1).Fields("PROVICOD").Label)
                        Exit Sub
                    End If


                    For Each oIdi In moIdiomas
                        den = "DEN_"
                        den = den & oIdi.Cod
                        If IsDBNull(e.Record.Cells("" & den & "").Value) Then 'Or e.Record.Cells("" & den & "").Value = "" Then
                            e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                            Mensajes.CampoVacio(XamDGPaisesProvincias.FieldLayouts(1).Fields("" & den & "").Label)
                            Exit Sub
                        End If
                    Next



                    'For Each myRecord In XamDGPaisesProvincias.Records
                    If e.Record.Cells("PROVICOD").Value <> e.Record.Cells("COD_PROVIORIG").Value Then
                        If Mensajes.CambioCodigo("Provincia") = vbYes Then

                            If oPaisesProvinciasRule.CambioCodigoProvincia(e.Record.Cells("COD_PROVIORIG").Value, e.Record.Cells("PROVICOD").Value, "adm", e.Record.Cells("PAICOD").Value) Then
                                Mensajes.CodigoYaPresente("Provincia")
                                e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                                Exit Sub
                            Else

                                e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                                Exit Sub
                            End If



                        End If


                    End If


                End If

            End If

        End If

        If mbDeshacer Then

            Me.cmdDeshacer.IsEnabled = False
            mbDeshacer = False
            Exit Sub
        End If

        If mbAdding Then

            If bEsPais Then


                If IsDBNull(e.Record.Cells("PAICOD").Value) Then
                    mbAdding = False
                    Exit Sub
                End If
                oPais.Codigo = e.Record.Cells("PAICOD").Value

                For Each oIdi In moIdiomas
                    den = ""
                    den = "DEN_" & oIdi.Cod
                    oDenominacion = oDenominaciones.Add(oIdi.Cod, e.Record.Cells("" & den & "").Value, DateTime.Now)
                Next
                oPais.Denominaciones = oDenominaciones
                If IsDBNull(e.Record.Cells("PAIMON").Value) Then
                    oMoneda.Codigo = ""
                Else
                    oMoneda.Codigo = e.Record.Cells("PAIMON").Value
                End If
                oPais.Moneda = oMoneda
                udtTEsError = oPaisesProvinciasRule.AnyadirPais(oPais, GSClient.ParametrosGenerales.giINSTWEB, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Pai), ModulePublic.sFSP, ModulePublic.oUsu.Cod)

                If udtTEsError.Number <> ErroresGS.TESnoerror Then

                    TratarError(udtTEsError)
                    e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges
                Else
                    e.Record.Cells("PAIFECACT").Value = oPais.FecAct
                    e.Record.Cells("COD_PAIORIG").Value = e.Record.Cells("PAICOD").Value
                    If GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Pai) Then
                        e.Record.Cells("PAIESTADO").Value = 0
                        e.Record.Cells("PAIESTADOV").Value = msDatoNoSincronizado
                    End If

                End If

            Else

                If IsDBNull(e.Record.Cells("PROVICOD").Value) Then
                    mbAdding = False
                    Exit Sub
                End If
                oProvincia.Codigo = e.Record.Cells("PROVICOD").Value

                For Each oIdi In moIdiomas
                    den = ""
                    den = "DEN_" & oIdi.Cod

                    oDenominacion = oDenominaciones.Add(oIdi.Cod, e.Record.Cells("" & den & "").Value, DateTime.Now)
                Next
                oProvincia.Denominaciones = oDenominaciones
                oPais.Codigo = e.Record.ParentDataRecord.Cells("PAICOD").Value
                oProvincia.Pais = oPais

                udtTEsError = oPaisesProvinciasRule.AnyadirProvincia(oProvincia, GSClient.ParametrosGenerales.giINSTWEB, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Pai), ModulePublic.sFSP, ModulePublic.oUsu.Cod)

                If udtTEsError.Number <> ErroresGS.TESnoerror Then

                    TratarError(udtTEsError)
                    e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges
                Else
                    e.Record.Cells("PROVIFECACT").Value = oProvincia.FecAct
                    e.Record.Cells("COD_PROVIORIG").Value = e.Record.Cells("PROVICOD").Value
                    e.Record.Cells("PROVIPAI").Value = e.Record.ParentDataRecord.Cells("PAICOD").Value
                    If GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Pai) Then
                        e.Record.Cells("PROVIESTADO").Value = 0
                        e.Record.Cells("PROVIESTADOV").Value = msDatoNoSincronizado
                    End If

                End If



            End If




        Else

            If bEsPais Then
                oPais.Codigo = e.Record.Cells("PAICOD").Value
                oPais.FecAct = e.Record.Cells("PAIFECACT").Value
                If IsDBNull(e.Record.Cells("PAIMON").Value) Then
                    oMoneda.Codigo = ""
                Else
                    oMoneda.Codigo = e.Record.Cells("PAIMON").Value
                End If

                oPais.Moneda = oMoneda
                If e.Record.Cells("PAICOD").Value <> e.Record.Cells("COD_PAIORIG").Value Then
                    'hacer Cambio de código
                    bRealizarCambioCodigo = True

                End If

                For Each oIdi In moIdiomas
                    den = ""
                    den = "DEN_" & oIdi.Cod
                    oDenominacion = oDenominaciones.Add(oIdi.Cod, e.Record.Cells("" & den & "").Value, e.Record.Cells("PAIFECACT").Value)

                Next

                oPais.Denominaciones = oDenominaciones
                udtTEsError = oPaisesProvinciasRule.ActualizarPais(oPais, bRealizarCambioCodigo, GSClient.ParametrosGenerales.giINSTWEB, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Pai), ModulePublic.sFSP, ModulePublic.oUsu.Cod)

                If udtTEsError.Number <> ErroresGS.TESnoerror Then

                    TratarError(udtTEsError)
                    e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges

                Else
                    '' Registro de acciones
                    e.Record.Cells("PAIFECACT").Value = oPais.FecAct
                    e.Record.Cells("COD_PAIORIG").Value = e.Record.Cells("PAICOD").Value
                End If
            Else

                oProvincia.Codigo = e.Record.Cells("PROVICOD").Value
                oProvincia.FecAct = e.Record.Cells("PROVIFECACT").Value
                oPais.Codigo = e.Record.Cells("PROVIPAI").Value
                oProvincia.Pais = oPais
                If e.Record.Cells("PROVICOD").Value <> e.Record.Cells("COD_PROVIORIG").Value Then
                    'hacer Cambio de código
                    bRealizarCambioCodigo = True

                End If

                For Each oIdi In moIdiomas
                    den = ""
                    den = "DEN_" & oIdi.Cod
                    oDenominacion = oDenominaciones.Add(oIdi.Cod, e.Record.Cells("" & den & "").Value, e.Record.Cells("PROVIFECACT").Value)

                Next

                oProvincia.Denominaciones = oDenominaciones
                udtTEsError = oPaisesProvinciasRule.ActualizarProvincia(oProvincia, bRealizarCambioCodigo, GSClient.ParametrosGenerales.giINSTWEB, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Pai), ModulePublic.sFSP, ModulePublic.oUsu.Cod)

                If udtTEsError.Number <> ErroresGS.TESnoerror Then

                    TratarError(udtTEsError)
                    e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges

                Else
                    '' Registro de acciones
                    e.Record.Cells("PROVIFECACT").Value = oProvincia.FecAct
                    e.Record.Cells("COD_PROVIORIG").Value = e.Record.Cells("PROVICOD").Value
                End If


            End If


        End If

        Me.cmdDeshacer.IsEnabled = False
        mbAdding = False
    End Sub


    '' <summary>
    '' Click del boton recargar
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub cmdRecargar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdRecargar.Click
        XamDGPaisesProvincias.DataSource = Nothing
        mbRecargar = True
        Cargar()
        Me.cmdEliminar.IsEnabled = False
        mbRecargar = False

    End Sub

    '' <summary>
    '' Click del boton Eliminar
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub cmdEliminar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdEliminar.Click

        Dim b As DataRecord
        Dim iRespuesta As Integer
        Dim bEsPais As Boolean
        Dim sCad As String
        Dim inumpais As Integer
        inumpais = 0
        Dim inumprovi As Integer
        inumprovi = 0
        Dim sEliminarProvinciaTexto As String
        Dim sEliminarPaisTexto As String
        Try
            msCodigos = Nothing
            msCodigosProvi = Nothing
            msCodigosPaisProvi = Nothing

            If mbModifModificar Then
                If XamDGPaisesProvincias.SelectedItems.Count > 1 Then

                    For i = 0 To XamDGPaisesProvincias.SelectedItems.Count - 1
                        bEsPais = False
                        b = CType(XamDGPaisesProvincias.SelectedItems.Records(i), DataRecord)
                        Try
                            sCad = b.Cells("PROVICOD").Value
                        Catch ex As Exception
                            bEsPais = True
                        End Try

                        If bEsPais Then

                            ReDim Preserve msCodigos(inumpais)
                            msCodigos(UBound(msCodigos, 1)) = b.Cells("PAICOD").Value
                            inumpais = inumpais + 1
                        Else
                            If Not b.ParentRecord.IsSelected Then
                                ReDim Preserve msCodigosProvi(inumprovi)
                                ReDim Preserve msCodigosPaisProvi(inumprovi)
                                msCodigosProvi(UBound(msCodigosProvi, 1)) = b.Cells("PROVICOD").Value
                                msCodigosPaisProvi(UBound(msCodigosPaisProvi, 1)) = b.Cells("PROVIPAI").Value
                                inumprovi = inumprovi + 1
                            End If

                        End If

                    Next i

                    If iRespuesta = vbNo Then
                        Exit Sub
                    End If
                Else
                    If XamDGPaisesProvincias.SelectedItems.Count = 1 Then
                        b = CType(XamDGPaisesProvincias.SelectedItems.Records(0), DataRecord)

                        Try
                            sCad = b.Cells("PROVICOD").Value
                        Catch ex As Exception
                            bEsPais = True
                        End Try

                        If bEsPais Then
                            sEliminarPaisTexto = msEliminarPaisTexto.Replace("XXX", b.Cells("PAICOD").Value)
                            iRespuesta = Mensajes.EliminarObjeto(sEliminarPaisTexto)
                            ReDim msCodigos(inumpais)
                            msCodigos(UBound(msCodigos, 1)) = b.Cells("PAICOD").Value
                        Else
                            sEliminarProvinciaTexto = msEliminarProvinciaTexto.Replace("XXX", b.Cells("PROVICOD").Value)
                            iRespuesta = Mensajes.EliminarObjeto(sEliminarProvinciaTexto)
                            ReDim msCodigosProvi(inumprovi)
                            msCodigosProvi(UBound(msCodigosProvi, 1)) = b.Cells("PROVICOD").Value
                            ReDim msCodigosPaisProvi(inumprovi)
                            msCodigosPaisProvi(UBound(msCodigosPaisProvi, 1)) = b.Cells("PROVIPAI").Value
                        End If

                        If iRespuesta = vbNo Then
                            Exit Sub
                        End If
                    Else
                        Exit Sub
                    End If
                End If
                Me.XamDGPaisesProvincias.FieldLayoutSettings.AllowDelete = True
                EliminarPaisesprovincias()
                Me.XamDGPaisesProvincias.FieldLayoutSettings.AllowDelete = False

            Else
                Exit Sub
            End If
        Catch ex As Exception
        End Try

    End Sub

    '' <summary>
    '' Eliminacion de monedas
    '' </summary>    
    '' <returns></returns>
    '' <remarks>Llamada desde: cmdEliminarClick  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub EliminarPaisesprovincias()

        Dim udtTEsErrorPais As New GSServerModel.GSException(False, "", "", True)
        Dim udtTEsErrorProvi As New GSServerModel.GSException(False, "", "", True)
        Dim oPaisesProvinciasRule As New GSClient.PaisesProvinciasRule
        Dim i As Integer
        Dim den As String
        Dim sDenominacion As String = ""
        Dim obj As Object

        If Not msCodigos Is Nothing Then
            'Elimina paises con sus provincias
            den = "DEN_" & ModulePublic.ParametrosInstalacion.gsIdioma
            udtTEsErrorPais = oPaisesProvinciasRule.EliminarPaises(msCodigos, GSClient.ParametrosGenerales.giINSTWEB, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Pai), ModulePublic.sFSP, ModulePublic.oUsu.Cod, ModulePublic.ParametrosInstalacion.gsIdioma)
            If udtTEsErrorPais.Number <> ErroresGS.TESnoerror Then
                If udtTEsErrorPais.Number = ErroresGS.TESImposibleEliminar Then

                    For i = 0 To udtTEsErrorPais.Errores.Count - 1
                        obj = Nothing
                        obj = XamDGPaisesProvincias.SelectedItems.Records.Item(udtTEsErrorPais.Errores(i).Numero).DataPresenter.ActiveDataItem
                        sDenominacion = sDenominacion & "-" & obj.Row("" & den & "")
                        XamDGPaisesProvincias.SelectedItems.Records.Item(udtTEsErrorPais.Errores(i).Numero).IsSelected = False
                        udtTEsErrorPais.Errores(i).Denominacion = sDenominacion
                    Next i
                    ImposibleEliminacionPais(30, udtTEsErrorPais)
                    XamDGPaisesProvincias.ExecuteCommand(DataPresenterCommands.DeleteSelectedDataRecords)

                Else
                    TratarError(udtTEsErrorPais)
                End If
            Else
                XamDGPaisesProvincias.ExecuteCommand(DataPresenterCommands.DeleteSelectedDataRecords)
            End If
        Else

            If Not msCodigosProvi Is Nothing Then
                den = "DEN_" & ModulePublic.ParametrosInstalacion.gsIdioma
                'Elimina provincias dentro de un pais
                udtTEsErrorProvi = oPaisesProvinciasRule.EliminarProvincias(msCodigosProvi, msCodigosPaisProvi, GSClient.ParametrosGenerales.giINSTWEB, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Pai), ModulePublic.sFSP, ModulePublic.oUsu.Cod, ModulePublic.ParametrosInstalacion.gsIdioma)
                If udtTEsErrorProvi.Number <> ErroresGS.TESnoerror Then
                    If udtTEsErrorProvi.Number = ErroresGS.TESImposibleEliminar Then

                        For i = 0 To udtTEsErrorProvi.Errores.Count - 1
                            obj = Nothing
                            obj = XamDGPaisesProvincias.SelectedItems.Records.Item(udtTEsErrorProvi.Errores(i).Numero).DataPresenter.ActiveDataItem
                            sDenominacion = sDenominacion & "-" & obj.Row("" & den & "")
                            XamDGPaisesProvincias.SelectedItems.Records.Item(udtTEsErrorProvi.Errores(i).Numero).IsSelected = False
                            udtTEsErrorProvi.Errores(i).Denominacion = sDenominacion
                        Next i
                        ImposibleEliminacionProvincia(27, udtTEsErrorProvi)
                        XamDGPaisesProvincias.ExecuteCommand(DataPresenterCommands.DeleteSelectedDataRecords)
                    Else
                        TratarError(udtTEsErrorProvi)
                    End If
                Else
                    'Todo sin error
                    XamDGPaisesProvincias.ExecuteCommand(DataPresenterCommands.DeleteSelectedDataRecords)
                End If
            End If
        End If

    End Sub


    '' <summary>
    '' Evento producido al cambiar los items seleccionados
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub XamDGMonedas_SelectedItemsChanged(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs) Handles XamDGPaisesProvincias.SelectedItemsChanged
        If mbModifModificar Then
            If XamDGPaisesProvincias.SelectedItems.Count > 0 Then
                cmdEliminar.IsEnabled = True
            Else
                cmdEliminar.IsEnabled = False
            End If
        End If
    End Sub


    '' <summary>
    '' Funcion de exportar el grid
    '' </summary>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>


    Public ReadOnly Property ExportOptions() As ExportOptions
        Get
            If (IsNothing(Me.moexportOptions)) Then
                Me.moexportOptions = New ExportOptions()

            End If
            Return Me.moexportOptions


        End Get
    End Property

    '' <summary>
    '' Click del boton Exportar
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub cmdExportar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdExportar.Click

        Dim sfileName As String
        sfileName = System.IO.Path.GetTempPath() + "\PaisesProvincias.xls"
        Try

            ' Get the instance of the DataPresenterExcelExporter we defined in the Page's resources and call its Export
            ' method passing in the XamDataGrid we want to export, the name of the file we would like
            ' to export to and the format of the Workbook we want to Exporter to create.
            '
            ' NOTE: there are 11 overloads to the Export method that give you control over different
            ' aspects of the exporting process.

            Dim exporter As New DataPresenterExcelExporter()
            exporter = CType(Me.Resources("Exporter"), DataPresenterExcelExporter)

            'exporter.Export(Me.XamDGMonedas, fileName, WorkbookFormat.Excel2007, this.ExportOptions);
            exporter.Export(Me.XamDGPaisesProvincias, sfileName, WorkbookFormat.Excel97To2003, Me.ExportOptions)

        Catch ex As Exception

            ' It's possible that the exporter does not have permission to write the file, so it's generally
            ' a good idea to account for this possibility.
            'MessageBox.Show(string.Format(Strings.ExportToExcel_Message_ExcelExportError_Text, ex.Message),
            '                                Strings.ExportToExcel_Message_ExcelError_Caption,
            '                               MessageBoxButton.OK,
            '                              MessageBoxImage.Exclamation);
            '
            '              return;
        End Try


        ' Display an 'Export Completed' message.
        'MessageBox.Show(Strings.ExportToExcel_Message_ExportCompleted_Text,
        '					Strings.ExportToExcel_Message_ExportCompleted_Caption,
        '					MessageBoxButton.OK,
        '						MessageBoxImage.Information);

        ' Execute Excel to display the exported workbook.
        Try            
            Dim oExcel As Object = CreateObject("Excel.Application")
            oExcel.Visible = True
            oExcel.Workbooks.Open(sfileName)
            Dim handler As IntPtr = FindWindow(Nothing, oExcel.Caption)
            SetForegroundWindow(handler)

        Catch Ex As Exception
            MessageBox.Show("Error al generar el excel", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation)
        End Try

    End Sub



    Private Sub cmdDeshacer_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Input.KeyEventArgs) Handles cmdDeshacer.PreviewKeyDown
        mbDeshacer = True
        XamDGPaisesProvincias.ExecuteCommand(DataPresenterCommands.DiscardChangesToActiveRecord)
        mbDeshacer = False
        Me.cmdDeshacer.IsEnabled = False
        mbAdding = False
    End Sub


    Private Sub cmdDeshacer_PreviewMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles cmdDeshacer.PreviewMouseDoubleClick
        mbDeshacer = True
        XamDGPaisesProvincias.ExecuteCommand(DataPresenterCommands.DiscardChangesToActiveRecord)
        mbDeshacer = False
        Me.cmdDeshacer.IsEnabled = False
        mbAdding = False
    End Sub

    Private Sub cmdDeshacer_PreviewMouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles cmdDeshacer.PreviewMouseDown
        mbDeshacer = True
        XamDGPaisesProvincias.ExecuteCommand(DataPresenterCommands.DiscardChangesToActiveRecord)
        mbDeshacer = False
        Me.cmdDeshacer.IsEnabled = False
        mbAdding = False
    End Sub

    '' <summary>
    '' Click del boton del listado
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo mÃ¡ximo: 0 </remarks>

    Private Sub cmdListado_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdListado.Click
        'Para que el grid jerárquico salga desplegado en el informe
        Dim oTRV As New TabularReportView
        oTRV.ExcludeExpandedState = True
        Me.XamDGPaisesProvincias.ReportView = oTRV

        Dim myReport As New Infragistics.Windows.Reporting.Report()
        Dim myXamDataGridReport As New Infragistics.Windows.Reporting.EmbeddedVisualReportSection(Me.XamDGPaisesProvincias)

        Me.tbiPreview.Visibility = Windows.Visibility.Visible
        Me.tbiPreview.IsSelected = True
        XamReportPreview1.Visibility = Windows.Visibility.Visible
        myReport.Sections.Add(myXamDataGridReport)
        Me.tbiData.Width = Me.tbiPreview.Width
        XamReportPreview1.GeneratePreview(myReport, False, True)

        'Dim saveDlg As New SaveFileDialog()
        'saveDlg.Filter = "XPS documents|*.xps"
        'If (Not saveDlg.ShowDialog().GetValueOrDefault()) Then
        '    Return
        'End If

        'Dim reportObj As New Report()
        'Dim section As New EmbeddedVisualReportSection(Me.XamDGPaisesProvincias)
        'reportObj.Sections.Add(section)
        ''progressInfo.Report = reportObj;
        'reportObj.Export(OutputFormat.XPS, saveDlg.FileName)

    End Sub


    '' <summary>
    '' Evento producido al cambiar una celda del grid
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub XamDGMonedas_CellUpdated(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellUpdatedEventArgs) Handles XamDGPaisesProvincias.CellUpdated

        Dim oPaisesProvinciasRule As New GSClient.PaisesProvinciasRule
        Dim res As Integer
        If e.Cell.IsDataChanged And Not mbAdding And Not mbActuCodigo Then

            If e.Cell.Field.Name = "PAICOD" Then

                If Mensajes.CambioCodigo("Pais") = vbYes Then

                    res = oPaisesProvinciasRule.CambioCodigoPais(e.Record.Cells("COD_PAIORIG").Value, e.Record.Cells("PAICOD").Value, "adm")
                    'MessageBox.Show("Campo código duplicado", "Error al actualizar un registro", MessageBoxButton.OK, MessageBoxImage.Exclamation)
                    If res = TiposDeDatos.CambioCodigo.PRESENTE Then
                        Mensajes.CodigoYaPresente("Pais")
                        mbActuCodigo = True
                        e.Record.Cells("PAICOD").Value = e.Record.Cells("COD_PAIORIG").Value
                        mbActuCodigo = True
                        mbCancelarUpdate = True
                        Exit Sub
                    Else
                        If res = TiposDeDatos.CambioCodigo.CAMBIOPENDIENTE Then
                            Mensajes.CambioCodigoPendiente("Pais")
                            mbActuCodigo = True
                            e.Record.Cells("PAICOD").Value = e.Record.Cells("COD_PAIORIG").Value
                            mbActuCodigo = False
                            mbCancelarUpdate = True
                            Exit Sub
                        End If

                    End If

                    mbActuCodigo = True
                    e.Record.Cells("PAICOD").Value = e.Record.Cells("COD_PAIORIG").Value
                    mbActuCodigo = False

                Else
                    If Not mbAdding Then
                        mbActuCodigo = True
                        e.Record.Cells("PAICOD").Value = e.Record.Cells("COD_PAIORIG").Value
                        'e.Record.CancelUpdate()
                        mbCancelarUpdate = True
                        mbActuCodigo = False
                        Exit Sub
                    End If
                End If
            Else

                If e.Cell.Field.Name = "PROVICOD" Then

                    If Mensajes.CambioCodigo("Provincia") = vbYes Then

                        res = oPaisesProvinciasRule.CambioCodigoProvincia(e.Record.Cells("COD_PROVIORIG").Value, e.Record.Cells("PROVICOD").Value, "adm", e.Record.Cells("PROVIPAI").Value)
                        'MessageBox.Show("Campo código duplicado", "Error al actualizar un registro", MessageBoxButton.OK, MessageBoxImage.Exclamation)
                        If res = TiposDeDatos.CambioCodigo.PRESENTE Then
                            Mensajes.CodigoYaPresente("Provincia")
                            mbActuCodigo = True
                            e.Record.Cells("PROVICOD").Value = e.Record.Cells("COD_PROVIORIG").Value
                            mbActuCodigo = True
                            mbCancelarUpdate = True
                            Exit Sub
                        Else
                            If res = TiposDeDatos.CambioCodigo.CAMBIOPENDIENTE Then
                                Mensajes.CambioCodigoPendiente("Provincia")
                                mbActuCodigo = True
                                e.Record.Cells("PROVICOD").Value = e.Record.Cells("COD_PROVIORIG").Value
                                mbActuCodigo = False
                                mbCancelarUpdate = True
                                Exit Sub
                            End If

                        End If

                        mbActuCodigo = True
                        e.Record.Cells("PROVICOD").Value = e.Record.Cells("COD_PROVIORIG").Value
                        mbActuCodigo = False

                    Else
                        If Not mbAdding Then
                            mbActuCodigo = True
                            e.Record.Cells("PROVICOD").Value = e.Record.Cells("COD_PROVIORIG").Value
                            'e.Record.CancelUpdate()
                            mbCancelarUpdate = True
                            mbActuCodigo = False
                            Exit Sub
                        End If
                    End If

                End If
            End If
        End If


    End Sub


    Private Sub MantenimientoMonedas_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        Dim sCadenaEntrada As String
        sCadenaEntrada = "Inicio Pagina Mantenimiento Paises y Provincias"

        Dim oRegistro As New GSClient.RegistrosRule()

        oRegistro.RegistrarEntradaOSalida(oUsu.Cod, AccionesGS.ACCPaiCon, sCadenaEntrada)

    End Sub


    Protected Overrides Sub Finalize()
        Dim sCadenaEntrada As String
        sCadenaEntrada = "Fin Pagina Mantenimiento Paises y Provincias"

        Dim oRegistro As New GSClient.RegistrosRule()

        oRegistro.RegistrarEntradaOSalida(oUsu.Cod, AccionesGS.ACCPaiCon, sCadenaEntrada)

        MyBase.Finalize()

    End Sub

    '' <summary>
    '' Funcion que lanza la accion de eliinar en caso de que se pulse el boton suprimir
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub XamDGMonedas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Input.KeyEventArgs) Handles XamDGPaisesProvincias.KeyDown
        If e.Key = Key.Delete Then
            cmdEliminar_Click(Me.XamDGPaisesProvincias, e)
        End If
    End Sub

    '' <summary>
    '' Funcion que oculta los tabs cuando se cierra el del listado
    '' </summary>
    '' <param name="sender">Objeto en el que se produce el evento </param>
    '' <param name="e">Evento </param>
    '' <returns></returns>
    '' <remarks>Llamada desde: Evento  </remarks>
    '' <remarks>Tiempo mÃ¡ximo: 0 </remarks>
    Private Sub tbiPreview_Closing(ByVal sender As Object, ByVal e As Infragistics.Windows.Controls.Events.TabClosingEventArgs) Handles tbiPreview.Closing
        Me.tbiData.Width = 0
    End Sub

End Class


