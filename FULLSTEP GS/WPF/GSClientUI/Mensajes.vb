﻿Imports System
Imports GSClient

Module Mensajes

    Private sMensaje As String
    Public sTexto As String
    Private Textos As New GSClient.CTextos

    Public Idioma As String

    Public Enum TipoIconoMensaje
        None = 0
        Critical = 1
        Exclamation = 2
        Information = 3
        question = 4
    End Enum
    Public Enum TipoBotonesMensaje
        None = 0
        OKOnly = 1
        YesNo = 2
        Otro = 3
    End Enum


    Public Sub NoValido(ByVal Campo As Object)
        sMensaje = CargarTextoMensaje(1)
        sMensaje = sMensaje & " " & Campo

        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Exclamation)


    End Sub

    Public Function CargarTextoMensaje(ByVal MensajeId As Long) As String
        Dim sTexto As String

        On Error Resume Next

        sTexto = Textos.DevolverTextosMensaje(1, MensajeId, Idioma)

        CargarTextoMensaje = sTexto

    End Function


    Public Function CargarTextoTabla(ByVal Modulo As Integer, ByVal MensajeId As Long) As String
        Dim sTexto As String

        On Error Resume Next

        sTexto = Textos.DevolverTextosMensaje(Modulo, MensajeId, Idioma)

        CargarTextoTabla = sTexto

    End Function

    Public Function CambioCodigo(ByVal TipoCambio As String) As Integer

        Select Case TipoCambio

            Case "Moneda"
                sMensaje = CargarTextoMensaje(2) & vbCrLf & CargarTextoMensaje(3) & vbCrLf & CargarTextoMensaje(4)
                Return MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.YesNo, MessageBoxImage.Question)

            Case "Unidad"
                sMensaje = CargarTextoMensaje(21) & vbCrLf & CargarTextoMensaje(3) & vbCrLf & CargarTextoMensaje(4)
                Return MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.YesNo, MessageBoxImage.Question)

            Case "ViaPago"
                sMensaje = CargarTextoMensaje(22) & vbCrLf & CargarTextoMensaje(3) & vbCrLf & CargarTextoMensaje(4)
                Return MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.YesNo, MessageBoxImage.Question)

            Case "FormaPago"
                sMensaje = CargarTextoMensaje(23) & vbCrLf & CargarTextoMensaje(3) & vbCrLf & CargarTextoMensaje(4)
                Return MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.YesNo, MessageBoxImage.Question)

            Case "Pais"
                sMensaje = CargarTextoMensaje(24) & vbCrLf & CargarTextoMensaje(3) & vbCrLf & CargarTextoMensaje(4)
                Return MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.YesNo, MessageBoxImage.Question)

            Case "Provincia"
                sMensaje = CargarTextoMensaje(25) & vbCrLf & CargarTextoMensaje(3) & vbCrLf & CargarTextoMensaje(4)
                Return MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.YesNo, MessageBoxImage.Question)

        End Select

    End Function

    Public Function CodigoYaPresente(ByVal Tipo As String) As Integer

        Select Case Tipo

            Case "Moneda", "Unidad", "ViaPago", "FormaPago", "Pais", "Provincia"
                sMensaje = CargarTextoMensaje(5)
                Return MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Question)

        End Select

    End Function


    Public Function CambioCodigoPendiente(ByVal Tipo As String) As Integer

        Select Case Tipo

            Case "Moneda", "Unidad", "ViaPago", "FormaPago", "Pais", "Provincia"
                sMensaje = CargarTextoMensaje(6)
                Return MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Question)

        End Select

    End Function


    Public Sub CampoVacio(ByVal scad As String)

        sMensaje = CargarTextoMensaje(8)
        MessageBox.Show(sMensaje & scad, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Exclamation)

    End Sub


    'Public Sub ImposibleEliminacionMultiple(ByVal Tabla As Integer, ByVal datos As Object)
    Public Sub ImposibleEliminacionMultiple(ByVal Tabla As Integer, ByVal datos As GSServerModel.GSException)
        '*********************************************************************
        '*** Descripción: Muestra un mensaje de imposibilidad de e***
        '***              En caso de no poder informa de los destinos que  ***
        '***              no pudieron ser eliminados                       ***
        '*** Parámetros:  Tabla ::> Es el ID correspondiente al mensaje    ***
        '***                        en el módulo de MENSAJES de la tabla   ***
        '***                        de idiomas. Despende del elemento      ***
        '***                        múltiple que ha sido imposible         ***
        '***                        eliminar (ej.: Destinos, Monedas, ...  ***
        '***              Datos ::> Array (3xn)                            ***
        '***                        {código, causa error, denominación}    ***
        '*** Valor que devuelve:    ----------                             ***
        '*********************************************************************
        Dim i As Integer

        sMensaje = CargarTextoMensaje(Tabla)
        sMensaje = sMensaje & vbCrLf & CargarTextoMensaje(10)

        For i = 0 To datos.Errores.Count - 1  'desde 0 hasta el máximo
            ' de la 2ª dimensión del array
            sMensaje = sMensaje & vbCrLf & datos.Errores(i).Codigo & _
                   " . " & _
                   CargarTextoMensaje(11) & " " & _
                   CargarTextoTabla(ModuleName.FRM_MENSAJE_TABLAS, datos.Errores(i).idError) & " " & _
                   datos.Errores(i).Descripcion
        Next i


        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Information)

    End Sub

    Public Sub DatoDuplicado(ByVal Dato As Integer)

        sMensaje = CargarTextoMensaje(12) & " " & CargarTextoTabla(ModuleName.OTROS, Dato)
        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Exclamation)

    End Sub

    Public Sub DatoDuplicado(ByVal Dato As String)
        sMensaje = CargarTextoMensaje(12) & " " & Dato
        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Exclamation)
    End Sub

    Public Sub DatoEliminado(ByVal Dato As Object)

        sMensaje = CargarTextoMensaje(13)

        If IsNumeric(Dato) Then
            sMensaje = sMensaje & " " & CargarTextoTabla(ModuleName.OTROS, Dato)
        Else
            sMensaje = sMensaje & " " & Dato
        End If

        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Error)
        
    End Sub

    Public Sub ImposibleEliDatRel(ByVal iDato As Integer)
        sMensaje = CargarTextoMensaje(14) & vbLf & CargarTextoMensaje(15) & CargarTextoTabla(ModuleName.FRM_MENSAJE_TABLAS, iDato)

        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Error)

    End Sub


    Public Sub DatosModificados(Optional ByVal bResum As Boolean = False)
        Dim sMensaje2 As String
        Dim sMensaje3 As String

        If Not bResum Then
            sMensaje2 = CargarTextoMensaje(17)
            sMensaje3 = CargarTextoMensaje(18)
            sMensaje = CargarTextoMensaje(16) & vbCrLf & sMensaje2 & vbCrLf & sMensaje3
        Else
            sMensaje = CargarTextoMensaje(16)
        End If

        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Exclamation)

    End Sub


    Public Sub FaltanDatos(ByVal Campo As Object)
        sMensaje = CargarTextoMensaje(19)

        If IsNumeric(Campo) Then
            sMensaje = sMensaje & vbCrLf & CargarTextoTabla(ModuleName.OTROS, Campo)
        Else
            sMensaje = sMensaje & vbCrLf & Campo
        End If

        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Exclamation)


    End Sub


    Public Sub DatosActualesModificados()

        sMensaje = CargarTextoMensaje(20)
        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Information)

    End Sub

    Public Sub SeleccionarMaterial()
        sMensaje = CargarTextoMensaje(77)
        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Information)
    End Sub

    Public Sub MaterialSinArticulos()
        sMensaje = CargarTextoMensaje(78)
        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Information)
    End Sub

    Public Function EliminarObjeto(ByVal sTexto As String) As Integer
        Return MessageBox.Show(sTexto & Environment.NewLine & CargarTextoTabla(ModuleName.FRM_MENSAJES, 4), "FULLSTEP", MessageBoxButton.YesNo, MessageBoxImage.Information)
    End Function

    Public Sub ImposibleEliminacionProvincia(ByVal Tabla As Integer, ByVal datos As GSServerModel.GSException)
        '    '*********************************************************************
        '    '*** Descripción: Muestra un mensaje de imposibilidad              ***
        '    '***              En caso de no poder informa de las provincias que***
        '    '***              no pudieron ser eliminados                       ***
        '    '*** Parámetros:  Tabla ::> Es el ID correspondiente al mensaje    ***
        '    '***                        en el módulo de MENSAJES de la tabla   ***
        '    '***                        de idiomas. Despende del elemento      ***
        '    '***                        múltiple que ha sido imposible         ***
        '    '***                        eliminar (ej.: Destinos, Monedas, ...  ***
        '    '***              Datos ::> Array (3xn)                            ***
        '    '***                        {código, causa error, denominación}    ***
        '    '*** Valor que devuelve:    ----------                             ***
        '    '*********************************************************************
        Dim i As Integer
        Dim sMensaje2 As String
        Dim sMensaje3 As String

        sMensaje2 = ""
        sMensaje3 = ""

        For i = 0 To datos.Errores.Count - 1 'desde 0 hasta el máximo de la 2ª dimensión del array
            If datos.Errores(i).idError = "0" Then
                sMensaje3 = sMensaje3 & vbCrLf & datos.Errores(i).Codigo & " " & datos.Errores(i).Denominacion
            Else
                sMensaje2 = sMensaje2 & vbCrLf & datos.Errores(i).Codigo & " " & datos.Errores(i).Denominacion & ". " _
                            & CargarTextoMensaje(11) & " " & _
                            CargarTextoTabla(TiposDeDatos.ModuleName.FRM_MENSAJE_TABLAS, datos.Errores(i).idError) _
                            & datos.Errores(i).Descripcion

            End If
        Next i

        If sMensaje2 <> "" Then
            sMensaje2 = CargarTextoMensaje(Tabla) & vbCrLf & CargarTextoMensaje(26) & sMensaje2
        End If


        If sMensaje3 <> "" Then
            If sMensaje2 = "" Then
                sMensaje3 = CargarTextoMensaje(28) & ":" & sMensaje3
            Else
                sMensaje3 = vbCrLf & vbCrLf & CargarTextoMensaje(28) & ":" & sMensaje3
            End If
        End If
        sMensaje = sMensaje2 & sMensaje3


        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Information)

    End Sub


    Public Sub ImposibleEliminacionPais(ByVal Tabla As Integer, ByVal datos As GSServerModel.GSException)
        '    '*********************************************************************
        '    '*** Descripción: Muestra un mensaje de imposibilidad              ***
        '    '***              En caso de no poder informa de las provincias que***
        '    '***              no pudieron ser eliminados                       ***
        '    '*** Parámetros:  Tabla ::> Es el ID correspondiente al mensaje    ***
        '    '***                        en el módulo de MENSAJES de la tabla   ***
        '    '***                        de idiomas. Despende del elemento      ***
        '    '***                        múltiple que ha sido imposible         ***
        '    '***                        eliminar (ej.: Destinos, Monedas, ...  ***
        '    '***              Datos ::> Array (3xn)                            ***
        '    '***                        {código, causa error, denominación}    ***
        '    '*** Valor que devuelve:    ----------                             ***
        '    '*********************************************************************
        Dim i As Integer
        Dim sMensaje2 As String
        Dim sMensaje3 As String

        sMensaje2 = ""
        sMensaje3 = ""

        For i = 0 To datos.Errores.Count - 1 'desde 0 hasta el máximo de la 2ª dimensión del array
            If datos.Errores(i).idError = "0" Then
                sMensaje3 = sMensaje3 & vbCrLf & datos.Errores(i).Codigo & " " & datos.Errores(i).Denominacion
            Else
                sMensaje2 = sMensaje2 & vbCrLf & datos.Errores(i).Codigo & " " & datos.Errores(i).Denominacion & ". " _
                            & CargarTextoMensaje(11) & " " & _
                            CargarTextoTabla(ModuleName.FRM_MENSAJE_TABLAS, datos.Errores(i).idError) _
                            & datos.Errores(i).Descripcion

            End If
        Next i

        If sMensaje2 <> "" Then
            sMensaje2 = CargarTextoMensaje(Tabla) & vbCrLf & CargarTextoMensaje(26) & sMensaje2
        End If


        If sMensaje3 <> "" Then
            If sMensaje2 = "" Then
                sMensaje3 = CargarTextoMensaje(29) & ":" & sMensaje3
            Else
                sMensaje3 = vbCrLf & vbCrLf & CargarTextoMensaje(29) & ":" & sMensaje3
            End If
        End If
        sMensaje = sMensaje2 & sMensaje3


        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Information)

    End Sub

    Public Sub OtroError(ByVal Codigo As String, ByVal Descr As String)
        Dim sMensaje2 As String
        Dim sMensaje3 As String
        Dim sMensaje4 As String

        sMensaje2 = CargarTextoMensaje(32)
        sMensaje3 = CargarTextoMensaje(33)
        sMensaje4 = CargarTextoMensaje(34)
        sMensaje = CargarTextoMensaje(31) & vbLf & sMensaje2 & " " & Codigo & vbLf & sMensaje3 & " " & Descr & vbLf & sMensaje4


        MessageBox.Show(sMensaje, "FULLSTEP", MessageBoxButton.OK, MessageBoxImage.Information)

    End Sub

End Module
