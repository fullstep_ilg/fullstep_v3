﻿Imports System.Data.SqlClient
Imports System.Windows.Markup
Imports System.Windows.Interop
Imports System.Windows.Navigation
Imports Infragistics.Windows.DataPresenter
Imports Infragistics.Windows.DataPresenter.ExcelExporter
Imports System.Diagnostics
Imports Infragistics.Excel
Imports Infragistics.Windows.Editors
Imports System.ComponentModel
Imports Microsoft.Win32
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Infragistics.Windows.Reporting
Imports GSClient
Imports System.Runtime.InteropServices

Partial Public Class MantenimientoMonedas
    <DllImport("user32.dll")> _
    Protected Shared Function SetForegroundWindow(ByVal hWnd As IntPtr) As Boolean
    End Function

    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)> _
    Protected Shared Function FindWindow(ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
    End Function

    Private moIdiomas As GSServerModel.Idiomas
    Private mbAdding As Boolean
    Private msCodigos() As String
    Private mbDeshacer As Boolean
    Private mbActuCodigo As Boolean
    Private mbSoloActCambio As Boolean
    Private mbModifModificar As Boolean
    Private mbModifModificarCod As Boolean
    Private moexportOptions As ExportOptions
    Private mbCancelarUpdate As Boolean
    Private msDatoSincronizado As String
    Private msDatoNoSincronizado As String
    Private msCodigo As String
    Private msDenominacion As String
    Private msEquivalencia As String
    Private msFecAct As String
    Private msDeleteSingleRecordPrompt As String
    Private msEstado As String

    Private msEliminarMonedasTit As String
    Private msEliminarMonedasTexto As String

    Private msEliminarMonedaTit As String
    Private msEliminarMonedaTexto As String

    Private mbRecargar As Boolean


    Private moMonedas As New GSServerModel.Monedas(True, "", "", False)

    ' ''' <summary>
    ' ''' Generacion de la pantalla MonatenimientoMonedas
    ' ''' </summary>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Application_Startup  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>
    ' ''' 
    Public Sub New()
        Dim oIdiomaRule As New GSClient.CIdioma

        InitializeComponent()
        moIdiomas = oIdiomaRule.DevolverIdiomas()
        CargarTextos()
        Cargar()

        Me.cmdEliminar.IsEnabled = False
        Mensajes.Idioma = ModulePublic.ParametrosInstalacion.gsIdioma


    End Sub


    ' ''' <summary>
    ' ''' carga los textos de la pantalla
    ' ''' </summary>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: New  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>
    ' ''' 

    Private Sub CargarTextos()
        Dim ds As System.Data.DataSet
        Dim oTextos As New GSClient.CTextos

        ds = oTextos.DevolverTextos(TiposDeDatos.ModuleName.FRM_MANTENIMIENTOMONEDAS, ModulePublic.ParametrosInstalacion.gsIdioma)

        If Not ds Is Nothing Then

            msCodigo = ds.Tables(0).Rows(0).Item(0).ToString()
            msEquivalencia = ds.Tables(0).Rows(2).Item(0).ToString()
            msEstado = ds.Tables(0).Rows(3).Item(0).ToString()
            msFecAct = ds.Tables(0).Rows(4).Item(0).ToString()

            msDenominacion = ds.Tables(0).Rows(1).Item(0).ToString()

            Me.cmdEliminar.Tag = ds.Tables(0).Rows(5).Item(0).ToString()
            Me.cmdListado.Tag = ds.Tables(0).Rows(6).Item(0).ToString()
            Me.cmdRecargar.Tag = ds.Tables(0).Rows(7).Item(0).ToString()

            msDatoSincronizado = ds.Tables(0).Rows(8).Item(0).ToString()
            msDatoNoSincronizado = ds.Tables(0).Rows(9).Item(0).ToString()

            msEliminarMonedasTit = ds.Tables(0).Rows(10).Item(0).ToString()
            msEliminarMonedasTexto = ds.Tables(0).Rows(11).Item(0).ToString()

            msEliminarMonedaTit = ds.Tables(0).Rows(12).Item(0).ToString()
            msEliminarMonedaTexto = ds.Tables(0).Rows(13).Item(0).ToString() + " XXX " + ds.Tables(0).Rows(14).Item(0).ToString()

            Me.cmdExportar.Tag = ds.Tables(0).Rows(15).Item(0).ToString()
            Me.cmdDeshacer.Tag = ds.Tables(0).Rows(16).Item(0).ToString()

            Me.tbiData.Header = ds.Tables(0).Rows(17).Item(0).ToString()
            Me.tbiPreview.Header = ds.Tables(0).Rows(6).Item(0).ToString()

            Me.Titulo.Text = ds.Tables(0).Rows(18).Item(0).ToString()
        End If

    End Sub


    ' ''' <summary>
    ' ''' Carga el grid de monedas
    ' ''' </summary>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: New  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>
    ' ''' 

    Private Sub Cargar()

        Dim oMonedasRule As New GSClient.MonedasRule
        'ModulePublic.Escribirlog("Inicio de pagina de monedas")
        XamDGMonedas.DataSource = oMonedasRule.DevolverMonedas(GSClient.ParametrosIntegracion.gaExportar(Fullstep.FSNLibrary.TiposDeDatos.EntidadIntegracion.Mon), ModulePublic.ParametrosInstalacion.gsIdioma).Tables(0).DefaultView
        ConfigurarPermisos()
        ConfigurarGrid()

        Me.cmdDeshacer.IsEnabled = False

        'ModulePublic.Escribirlog("Fin de pagina de monedas")
    End Sub


    ' ''' <summary>
    ' ''' Configura el grid de las moneda
    ' ''' </summary>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Cargar  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>
    ' ''' 

    Private Sub ConfigurarGrid()
        Try

            'Dim st As New Style
            'Dim setter As New Setter(XamTextEditor.FormatProperty, ModulePublic.FormatoFecha)
            'Dim st2 As New Style
            'Dim setter2 As New Setter(XamNumericEditor.FormatProperty, "###.###.##0,##########")
            'Dim setter3 As New Setter(XamNumericEditor.MaskProperty, "")
            'Dim st3 As New Style
            Dim st4 As New Style
            Dim vc As New ValueConstraint
            Dim vcDenominacion As New ValueConstraint
            Dim Den As String
            Dim oIdi As GSServerModel.Idioma
            Dim sDenominacion As String
            Dim stEquiv As New Style



            XamDGMonedas.Records.FieldLayout.Fields("COD").Label = msCodigo
            XamDGMonedas.Records.FieldLayout.Fields("COD").IsScrollTipField = True
            XamDGMonedas.Records.FieldLayout.Fields("EQUIV").Label = msEquivalencia
            XamDGMonedas.Records.FieldLayout.Fields("FECACT").Label = msFecAct
            XamDGMonedas.Records.FieldLayout.Fields("FECACT").Settings.AllowEdit = False

            For Each oIdi In moIdiomas
                sDenominacion = "DEN_"
                sDenominacion = sDenominacion & oIdi.Cod
                XamDGMonedas.Records.FieldLayout.Fields("" & sDenominacion & "").Label = msDenominacion & " " & oIdi.Den
            Next


            'XamDGMonedas.Records.FieldLayout.Fields("FECACT").Settings.EditorType = GetType(XamTextEditor)
            'st.TargetType = GetType(XamTextEditor)
            'st.Setters.Add(setter)
            'XamDGMonedas.Records.FieldLayout.Fields("FECACT").Settings.EditorStyle = st


            vc.MaxLength = LongitudesDeCodigo.giLongCodMON
            Dim setter4 As New Setter(XamTextEditor.ValueConstraintProperty, vc)
            XamDGMonedas.Records.FieldLayout.Fields("COD").Settings.EditorType = GetType(XamTextEditor)
            st4.TargetType = GetType(XamTextEditor)
            st4.Setters.Add(setter4)
            XamDGMonedas.Records.FieldLayout.Fields("COD").Settings.EditorStyle = st4



            Dim stDenominacion As New Style
            Dim setterDenominacion As New Setter(XamTextEditor.ValueConstraintProperty, vcDenominacion)
            vcDenominacion.MaxLength = ModulePublic.giLongDenMoneda
            stDenominacion.Setters.Add(setterDenominacion)

            For Each oIdi In moIdiomas
                Den = "DEN_"
                Den = Den & oIdi.Cod
                XamDGMonedas.Records.FieldLayout.Fields("" & Den & "").Settings.EditorType = GetType(XamTextEditor)
                stDenominacion.TargetType = GetType(XamTextEditor)
                XamDGMonedas.Records.FieldLayout.Fields("" & Den & "").Settings.EditorStyle = stDenominacion
            Next


            XamDGMonedas.Records.FieldLayout.Fields("COD_ORIG").Visibility = Windows.Visibility.Collapsed
            XamDGMonedas.Records.FieldLayout.Settings.HighlightAlternateRecords = "True"
            XamDGMonedas.FieldLayoutSettings.SupportDataErrorInfo = True

            'Style s = new Style(typeof(XamTextEditor));
            'ValueConstraint vc = new ValueConstraint();
            'vc.MaxLength = 20;
            'Setter setter = new Setter(XamTextEditor.ValueConstraintProperty,vc);
            's.Setters.Add(setter);

            'XamDGMonedas.FieldLayouts(0).Fields("EQUIV").Settings.EditorType = GetType(XamNumericEditor)
            'st2.TargetType = GetType(XamNumericEditor)            
            'st2.Setters.Add(setter2)
            'st2.Setters.Add(setter3)
            'XamDGMonedas.FieldLayouts(0).Fields("EQUIV").Settings.EditorStyle = st2

            stEquiv = Me.FindResource("stNoWrap")
            XamDGMonedas.Records.FieldLayout.Fields("EQUIV").Settings.EditorStyle = stEquiv


            If GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Mon) Then
                Dim stl As New Style
                stl = Me.FindResource("stsinc")
                XamDGMonedas.Records.FieldLayout.Fields("ESTADOV").Settings.AllowEdit = False
                XamDGMonedas.FieldLayouts(0).Fields("ESTADOV").Settings.EditorStyle = stl
                XamDGMonedas.Records.FieldLayout.Fields("ESTADO").Visibility = Windows.Visibility.Collapsed
                XamDGMonedas.Records.FieldLayout.Fields("ESTADOV").Label = msEstado
            End If

            Dim numIdiomas As Integer
            numIdiomas = moIdiomas.Count

            'XamDGMonedas.Records.FieldLayout.Fields("COD").Settings.CellMaxWidth = 75
            'XamDGMonedas.Records.FieldLayout.Fields("EQUIV").Settings.CellMaxWidth = 100
            'If numIdiomas < 4 Then
            '    XamDGMonedas.AutoFit = True
            '    XamDGMonedas.Records.FieldLayout.Fields("FECACT").Settings.CellMaxWidth = 120
            '    If ParametrosIntegracion.gaExportar(EntidadIntegracion.Mon) Then
            '        XamDGMonedas.Records.FieldLayout.Fields("ESTADOV").Settings.CellMaxWidth = 150
            '    End If
            'Else
            '    For Each oIdi In moIdiomas
            '        Den = "DEN_"
            '        Den = Den & oIdi.Cod

            '        XamDGMonedas.Records.FieldLayout.Fields("" & Den & "").Settings.CellWidth = 300
            '    Next
            'End If



        Catch Ex As Exception

        End Try

    End Sub

    '<summary>Configuracion de permisos</summary>
    '<returns></returns>
    '<remarks>Llamada desde: Cargar  </remarks>
    '<remarks>Tiempo máximo: 0 </remarks>
    '<revision>LTG 30/04/2013</revision>

    Private Sub ConfigurarPermisos()
        Dim den As String
        Me.XamDGMonedas.FieldLayoutSettings.AllowDelete = False
        If GSClient.ParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Mon) Then
            mbModifModificar = False
            mbModifModificarCod = False
            mbActuCodigo = False
            Me.cmdEliminar.IsEnabled = False
            Me.XamDGMonedas.FieldLayoutSettings.AllowAddNew = False
            Me.XamDGMonedas.FieldSettings.AllowEdit = False
            Me.XamDGMonedas.FieldLayoutSettings.AllowDelete = False
            Me.cmdEliminar.Visibility = Windows.Visibility.Collapsed
            Me.cmdDeshacer.Visibility = Windows.Visibility.Collapsed

            If mbRecargar = False Then
                Me.cmdExportar.Margin = Me.cmdEliminar.Margin
                Me.cmdListado.Margin = Me.cmdDeshacer.Margin
            End If
            Me.ButtonBar.Width = 350
            Me.ButtonBorder.Width = 350
        Else
            'Cargar Acciones para determinar si puede modificar o no
            mbModifModificar = Not (ModulePublic.oUsu.Acciones.Item(CLng(AccionesDeSeguridad.MONModificar)) Is Nothing)
            mbModifModificarCod = Not (ModulePublic.oUsu.Acciones.Item(CLng(AccionesDeSeguridad.MONModificarCodigo)) Is Nothing)
            mbSoloActCambio = Not (ModulePublic.oUsu.Acciones.Item(CLng(AccionesDeSeguridad.MONActualizarCambio)) Is Nothing)

            If Not mbModifModificar Then
                Me.XamDGMonedas.FieldLayoutSettings.AllowAddNew = False
                Me.XamDGMonedas.FieldSettings.AllowEdit = False
                Me.XamDGMonedas.FieldLayoutSettings.AllowDelete = False
                Me.cmdEliminar.Visibility = Windows.Visibility.Collapsed
                Me.cmdDeshacer.Visibility = Windows.Visibility.Collapsed

                If mbRecargar = False Then
                    Me.cmdExportar.Margin = Me.cmdEliminar.Margin
                    Me.cmdListado.Margin = Me.cmdDeshacer.Margin
                End If
            Else
                If mbSoloActCambio Then
                    XamDGMonedas.Records.FieldLayout.Fields("EQUIV").Settings.AllowEdit = True
                    XamDGMonedas.Records.FieldLayout.Fields("COD").Settings.AllowEdit = False
                    For Each oIdi2 In moIdiomas
                        den = "DEN_"
                        den = den & oIdi2.Cod
                        XamDGMonedas.Records.FieldLayout.Fields("" & den & "").Settings.AllowEdit = False
                    Next
                    Me.XamDGMonedas.FieldLayoutSettings.AllowAddNew = False
                    Me.XamDGMonedas.FieldLayoutSettings.AllowDelete = False
                    Me.cmdEliminar.Visibility = Windows.Visibility.Collapsed

                    If mbRecargar = False Then
                        Me.cmdListado.Margin = Me.cmdExportar.Margin
                        Me.cmdExportar.Margin = Me.cmdDeshacer.Margin
                        Me.cmdDeshacer.Margin = Me.cmdEliminar.Margin

                    End If

                    Me.ButtonBar.Width = 470
                    Me.ButtonBorder.Width = 470
                Else
                    Me.XamDGMonedas.FieldLayoutSettings.AddNewRecordLocation = AddNewRecordLocation.OnTopFixed
                End If
            End If
        End If
    End Sub

    ' ''' <summary>
    ' ''' Evento producido al activar una celda
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>
    ' ''' 
    Private Sub XamDGMonedas_CellActivating(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellActivatingEventArgs) Handles XamDGMonedas.CellActivating
        If e.Cell.Field.Name = "COD" And mbModifModificarCod And Not mbSoloActCambio Then
            XamDGMonedas.Records.FieldLayout.Fields("COD").Settings.AllowEdit = True
        Else
            XamDGMonedas.Records.FieldLayout.Fields("COD").Settings.AllowEdit = False
        End If
    End Sub


    ' ''' <summary>
    ' ''' Evento producido al cambiar una celda
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGMonedas_CellChanged(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellChangedEventArgs) Handles XamDGMonedas.CellChanged
        Me.cmdDeshacer.IsEnabled = True
    End Sub


    ' ''' <summary>
    ' ''' Evento producido al añadir un item
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGMonedas_RecordAdded(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordAddedEventArgs) Handles XamDGMonedas.RecordAdded
        mbAdding = True
    End Sub




    ' ''' <summary>
    ' ''' Evento producido al eliminar items
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGMonedas_RecordsDeleting(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordsDeletingEventArgs) Handles XamDGMonedas.RecordsDeleting

        e.DisplayPromptMessage = False

    End Sub



    ' ''' <summary>
    ' ''' Evento producido modificando items
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub XamDGMonedas_RecordUpdating(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.RecordUpdatingEventArgs) Handles XamDGMonedas.RecordUpdating

        Dim den As String
        Dim myRecord As DataRecord

        Dim bSeguir As Boolean
        bSeguir = False

        Dim oMonedasRule As New GSClient.MonedasRule

        Dim oMoneda As New GSServerModel.Moneda(False, "", "", True)
        Dim oDenominacion As New GSServerModel.Multiidioma(False, "", "", True)
        Dim oDenominaciones As New GSServerModel.Multiidiomas(False, "", "", True)
        Dim bRealizarCambioCodigo As Boolean

        Dim udtTEsError As New GSServerModel.GSException(False, "", "", True)

        If e.Record.RecordType = RecordType.FilterRecord Then
            Exit Sub
        End If

        If Not mbDeshacer Then
            If mbAdding Then

                'Caso especial 
                If IsDBNull(e.Record.Cells("COD").Value) Then
                    If IsDBNull(e.Record.Cells("EQUIV").Value) Then
                        For Each oIdi In moIdiomas
                            den = "DEN_"
                            den = den & oIdi.Cod
                            If Not IsDBNull(e.Record.Cells("" & den & "").Value) Then 'Or e.Record.Cells("" & den & "").Value = "" Then
                                bSeguir = True
                                Exit For
                            End If
                        Next
                        If Not bSeguir Then
                            'Significa que ha estado modificando y para salir lo ha dejado vacio
                            ' salimos y en recordUpdated no hace nada
                            e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges
                            Exit Sub
                        End If
                    End If
                End If

                If IsDBNull(e.Record.Cells("COD").Value) Then ' Or e.Record.Cells("COD").Value = "" Then
                    e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                    Mensajes.NoValido(XamDGMonedas.Records.FieldLayout.Fields("COD").Label)

                    Exit Sub
                End If


                For Each oIdi In moIdiomas
                    den = "DEN_"
                    den = den & oIdi.Cod
                    If IsDBNull(e.Record.Cells("" & den & "").Value) Then 'Or e.Record.Cells("" & den & "").Value = "" Then
                        e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                        Mensajes.CampoVacio(XamDGMonedas.Records.FieldLayout.Fields("" & den & "").Label)
                        Exit Sub
                    End If
                Next

                If IsDBNull(e.Record.Cells("EQUIV").Value) Then ' Or e.Record.Cells("COD").Value = "" Then
                    e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                    Mensajes.CampoVacio(XamDGMonedas.Records.FieldLayout.Fields("EQUIV").Label)
                    Exit Sub
                End If


                For Each myRecord In XamDGMonedas.Records
                    If myRecord.IsAddRecord = False Then
                        If myRecord.Cells("COD").Value = e.Record.Cells("COD").Value Then
                            e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                            Mensajes.DatoDuplicado(2)
                            Exit Sub
                        End If
                    End If
                Next
            Else

                If mbCancelarUpdate Then
                    mbCancelarUpdate = False
                    e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateDiscardChanges
                    Exit Sub
                End If

                If IsDBNull(e.Record.Cells("COD").Value) Then 'Or e.Record.Cells("COD").Value = "" Then
                    e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                    Mensajes.CampoVacio(XamDGMonedas.Records.FieldLayout.Fields("COD").Label)
                    Exit Sub
                End If


                For Each oIdi In moIdiomas
                    den = "DEN_"
                    den = den & oIdi.Cod
                    If IsDBNull(e.Record.Cells("" & den & "").Value) Then 'Or e.Record.Cells("" & den & "").Value = "" Then
                        e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                        Mensajes.CampoVacio(XamDGMonedas.Records.FieldLayout.Fields("" & den & "").Label)
                        Exit Sub
                    End If
                Next

                If IsDBNull(e.Record.Cells("EQUIV").Value) Then 'Or e.Record.Cells("COD").Value = "" Then
                    e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                    Mensajes.CampoVacio(XamDGMonedas.Records.FieldLayout.Fields("EQUIV").Label)

                    Exit Sub
                End If

                'For Each myRecord In XamDGMonedas.Records
                If e.Record.Cells("COD").Value <> e.Record.Cells("COD_ORIG").Value Then
                    If Mensajes.CambioCodigo("Moneda") = vbYes Then

                        'Dim oMonedasRule As New GSClient.MonedasRule
                        If oMonedasRule.CambioCodigoMoneda(e.Record.Cells("COD_ORIG").Value, e.Record.Cells("COD").Value, "adm") Then
                            Mensajes.CodigoYaPresente("Moneda")
                            e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                            Exit Sub
                        End If

                    Else
                        e.Action = Infragistics.Windows.DataPresenter.RecordUpdatingAction.CancelUpdateRetainChanges
                        Exit Sub
                    End If
                End If

            End If


            If mbDeshacer Then
                Me.cmdDeshacer.IsEnabled = False
                mbDeshacer = False
                Exit Sub
            End If
            If IsDBNull(e.Record.Cells("COD").Value) Then
                mbAdding = False
                Exit Sub
            End If

            If mbAdding Then

                oMoneda.Codigo = e.Record.Cells("COD").Value
                oMoneda.Equivalencia = e.Record.Cells("EQUIV").Value
                For Each oIdi In moIdiomas
                    den = ""
                    den = "DEN_" & oIdi.Cod

                    oDenominacion = oDenominaciones.Add(oIdi.Cod, e.Record.Cells("" & den & "").Value, DateTime.Now)
                Next
                oMoneda.Denominaciones = oDenominaciones
                udtTEsError = oMonedasRule.AnyadirMoneda(oMoneda, GSClient.ParametrosGenerales.giINSTWEB, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Mon), ModulePublic.sFSP, ModulePublic.oUsu.Cod)

                If udtTEsError.Number <> ErroresGS.TESnoerror Then

                    TratarError(udtTEsError)
                    e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges
                Else
                    e.Record.Cells("FECACT").Value = oMoneda.FecAct
                    e.Record.Cells("COD_ORIG").Value = e.Record.Cells("COD").Value
                    If GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Mon) Then
                        e.Record.Cells("ESTADO").Value = 0
                        e.Record.Cells("ESTADOV").Value = msDatoNoSincronizado
                    End If

                End If


            Else

                'Try
                oMoneda.Codigo = e.Record.Cells("COD").Value
                oMoneda.Equivalencia = e.Record.Cells("EQUIV").Value
                oMoneda.FecAct = e.Record.Cells("FECACT").Value
                If e.Record.Cells("COD").Value <> e.Record.Cells("COD_ORIG").Value Then
                    'hacer Cambio de código
                    bRealizarCambioCodigo = True

                End If

                For Each oIdi In moIdiomas
                    den = ""
                    den = "DEN_" & oIdi.Cod
                    oDenominacion = oDenominaciones.Add(oIdi.Cod, e.Record.Cells("" & den & "").Value, e.Record.Cells("FECACT").Value)


                Next

                oMoneda.Denominaciones = oDenominaciones
                udtTEsError = oMonedasRule.ActualizarMoneda(oMoneda, bRealizarCambioCodigo, GSClient.ParametrosGenerales.giINSTWEB, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Mon), ModulePublic.sFSP, ModulePublic.oUsu.Cod)

                If udtTEsError.Number <> ErroresGS.TESnoerror Then

                    TratarError(udtTEsError)
                    e.Action = RecordUpdatingAction.CancelUpdateDiscardChanges

                Else
                    ' Registro de acciones
                    e.Record.Cells("FECACT").Value = oMoneda.FecAct
                    e.Record.Cells("COD_ORIG").Value = e.Record.Cells("COD").Value
                End If

            End If



            Me.cmdDeshacer.IsEnabled = False
            mbAdding = False

        End If
    End Sub


    ' ''' <summary>
    ' ''' Click del boton recargar
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub cmdRecargar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdRecargar.Click
        XamDGMonedas.DataSource = Nothing
        mbRecargar = True
        Cargar()
        Me.cmdEliminar.IsEnabled = False
        mbRecargar = False

    End Sub

    ' ''' <summary>
    ' ''' Click del boton Eliminar
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub cmdEliminar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdEliminar.Click
        Dim b As DataRecord
        Dim iRespuesta As Integer
        Dim sEliminarMonedaTexto As String

        Try
            If mbModifModificar And Not mbSoloActCambio Then
                If XamDGMonedas.SelectedItems.Count > 1 Then
                    iRespuesta = Mensajes.EliminarObjeto(msEliminarMonedasTexto)
                    If iRespuesta = vbNo Then
                        Exit Sub
                    End If
                Else
                    If XamDGMonedas.SelectedItems.Count = 1 Then
                        'For i = 0 To e.Records.Count - 1

                        b = CType(XamDGMonedas.SelectedItems.Records(0), DataRecord)

                        sEliminarMonedaTexto = msEliminarMonedaTexto.Replace("XXX", b.Cells("COD").Value)

                        iRespuesta = Mensajes.EliminarObjeto(sEliminarMonedaTexto)
                        If iRespuesta = vbNo Then
                            Exit Sub
                        End If
                    Else
                        Exit Sub
                    End If
                End If
                Me.XamDGMonedas.FieldLayoutSettings.AllowDelete = True
                EliminarMonedas()
                Me.XamDGMonedas.FieldLayoutSettings.AllowDelete = False
            Else
                Exit Sub
            End If
        Catch ex As Exception
        End Try

    End Sub

    ' ''' <summary>
    ' ''' Eliminacion de monedas
    ' ''' </summary>    
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: cmdEliminarClick  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>
    Private Sub EliminarMonedas()

        Dim udtTEsError As New GSServerModel.GSException(False, "", "", True)
        Dim oMonedasRule As New GSClient.MonedasRule
        Dim b As DataRecord

        Dim i As Integer

        ReDim msCodigos(XamDGMonedas.SelectedItems.Count - 1)

        For i = 0 To XamDGMonedas.SelectedItems.Count - 1

            b = CType(XamDGMonedas.SelectedItems.Records(i), DataRecord)
            msCodigos(i) = b.Cells("COD").Value
        Next i


        udtTEsError = oMonedasRule.EliminarMonedas(msCodigos, GSClient.ParametrosGenerales.giINSTWEB, GSClient.ParametrosGenerales.gbACTIVLOG, GSClient.ParametrosIntegracion.gaExportar(EntidadIntegracion.Mon), ModulePublic.sFSP, GSClient.ParametrosGenerales.gsMONCEN, ModulePublic.oUsu.Cod)

        If udtTEsError.Number <> ErroresGS.TESnoerror Then
            If udtTEsError.Number = ErroresGS.TESImposibleEliminar Then
                ImposibleEliminacionMultiple(9, udtTEsError)
                For i = 0 To udtTEsError.Errores.Count - 1
                    XamDGMonedas.SelectedItems.Records.Item(udtTEsError.Errores(i).Numero).IsSelected = False
                Next i

                XamDGMonedas.ExecuteCommand(DataPresenterCommands.DeleteSelectedDataRecords)
            Else
                TratarError(udtTEsError)
            End If

        Else
            XamDGMonedas.ExecuteCommand(DataPresenterCommands.DeleteSelectedDataRecords)
        End If

    End Sub


    ' ''' <summary>
    ' ''' Evento producido al cambiar los items seleccionados
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub XamDGMonedas_SelectedItemsChanged(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs) Handles XamDGMonedas.SelectedItemsChanged
        If mbModifModificar Then
            If XamDGMonedas.SelectedItems.Count > 0 Then
                cmdEliminar.IsEnabled = True
            Else
                cmdEliminar.IsEnabled = False
            End If
        End If
    End Sub


    ' ''' <summary>
    ' ''' Funcion de exportar el grid
    ' ''' </summary>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>


    Public ReadOnly Property ExportOptions() As ExportOptions
        Get
            If (IsNothing(Me.moexportOptions)) Then
                Me.moexportOptions = New ExportOptions()

            End If
            Return Me.moexportOptions


        End Get
    End Property

    ' ''' <summary>
    ' ''' Click del boton Exportar
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub cmdExportar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdExportar.Click

        Dim sfileName As String
        sfileName = System.IO.Path.GetTempPath() + "\monedas.xls"
        Try

            ' Get the instance of the DataPresenterExcelExporter we defined in the Page's resources and call its Export
            ' method passing in the XamDataGrid we want to export, the name of the file we would like
            ' to export to and the format of the Workbook we want to Exporter to create.
            '
            ' NOTE: there are 11 overloads to the Export method that give you control over different
            ' aspects of the exporting process.

            Dim exporter As New DataPresenterExcelExporter()
            exporter = CType(Me.Resources("Exporter"), DataPresenterExcelExporter)

            'exporter.Export(Me.XamDGMonedas, fileName, WorkbookFormat.Excel2007, this.ExportOptions);
            exporter.Export(Me.XamDGMonedas, sfileName, WorkbookFormat.Excel97To2003, Me.ExportOptions)

        Catch ex As Exception

            ' It's possible that the exporter does not have permission to write the file, so it's generally
            ' a good idea to account for this possibility.
            'MessageBox.Show(string.Format(Strings.ExportToExcel_Message_ExcelExportError_Text, ex.Message),
            '                                Strings.ExportToExcel_Message_ExcelError_Caption,
            '                               MessageBoxButton.OK,
            '                              MessageBoxImage.Exclamation);
            '
            '              return;
        End Try

        Try
            Dim oExcel As Object = CreateObject("Excel.Application")
            oExcel.Visible = True
            oExcel.Workbooks.Open(sfileName)
            Dim handler As IntPtr = FindWindow(Nothing, oExcel.Caption)
            SetForegroundWindow(handler)            

        Catch Ex As Exception
            MessageBox.Show("Error al generar el excel", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation)
        End Try

    End Sub




    Private Sub cmdDeshacer_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Input.KeyEventArgs) Handles cmdDeshacer.PreviewKeyDown
        mbDeshacer = True
        XamDGMonedas.ExecuteCommand(DataPresenterCommands.DiscardChangesToActiveRecord)
        mbDeshacer = False
        Me.cmdDeshacer.IsEnabled = False
        mbAdding = False
    End Sub


    Private Sub cmdDeshacer_PreviewMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles cmdDeshacer.PreviewMouseDoubleClick
        mbDeshacer = True
        XamDGMonedas.ExecuteCommand(DataPresenterCommands.DiscardChangesToActiveRecord)
        mbDeshacer = False
        Me.cmdDeshacer.IsEnabled = False
        mbAdding = False
    End Sub

    Private Sub cmdDeshacer_PreviewMouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles cmdDeshacer.PreviewMouseDown
        mbDeshacer = True
        XamDGMonedas.ExecuteCommand(DataPresenterCommands.DiscardChangesToActiveRecord)
        mbDeshacer = False
        Me.cmdDeshacer.IsEnabled = False
        mbAdding = False
    End Sub

    ' ''' <summary>
    ' ''' Click del boton del listado
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub cmdListado_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles cmdListado.Click

        Dim myReport As New Infragistics.Windows.Reporting.Report()
        Dim myXamDataGridReport As New Infragistics.Windows.Reporting.EmbeddedVisualReportSection(Me.XamDGMonedas)

        Me.tbiPreview.Visibility = Windows.Visibility.Visible
        Me.tbiPreview.IsSelected = True
        XamReportPreview1.Visibility = Windows.Visibility.Visible
        myReport.Sections.Add(myXamDataGridReport)
        Me.tbiData.Width = Me.tbiPreview.Width
        XamReportPreview1.GeneratePreview(myReport, False, True)

        '        Dim saveDlg As New SaveFileDialog()
        '        saveDlg.Filter = "XPS documents|*.xps"
        '        If (Not saveDlg.ShowDialog().GetValueOrDefault()) Then
        ' Return
        'End If

        'Dim reportObj As New Report()
        'Dim section As New EmbeddedVisualReportSection(Me.XamDGMonedas)
        'reportObj.Sections.Add(section)
        'progressInfo.Report = reportObj;
        'reportObj.Export(OutputFormat.XPS, saveDlg.FileName)


    End Sub


    ' ''' <summary>
    ' ''' Evento producido al cambiar una celda del grid
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub XamDGMonedas_CellUpdated(ByVal sender As Object, ByVal e As Infragistics.Windows.DataPresenter.Events.CellUpdatedEventArgs) Handles XamDGMonedas.CellUpdated

        If e.Cell.IsDataChanged And e.Cell.Field.Name = "COD" And Not mbAdding And Not mbActuCodigo Then

            If Mensajes.CambioCodigo("Moneda") = vbYes Then

                Dim oMonedasRule As New GSClient.MonedasRule
                Dim res As Integer
                res = oMonedasRule.CambioCodigoMoneda(e.Record.Cells("COD_ORIG").Value, e.Record.Cells("COD").Value, "adm")
                'MessageBox.Show("Campo código duplicado", "Error al actualizar un registro", MessageBoxButton.OK, MessageBoxImage.Exclamation)
                If res = TiposDeDatos.CambioCodigo.PRESENTE Then
                    Mensajes.CodigoYaPresente("Moneda")
                    mbActuCodigo = True
                    e.Record.Cells("COD").Value = e.Record.Cells("COD_ORIG").Value
                    mbActuCodigo = True
                    mbCancelarUpdate = True
                    Exit Sub
                Else
                    If res = TiposDeDatos.CambioCodigo.CAMBIOPENDIENTE Then
                        Mensajes.CambioCodigoPendiente("Moneda")
                        mbActuCodigo = True
                        e.Record.Cells("COD").Value = e.Record.Cells("COD_ORIG").Value
                        mbActuCodigo = False
                        mbCancelarUpdate = True
                        Exit Sub
                    End If

                End If

                mbActuCodigo = True
                e.Record.Cells("COD").Value = e.Record.Cells("COD_ORIG").Value
                mbActuCodigo = False

            Else
                If Not mbAdding Then
                    mbActuCodigo = True
                    e.Record.Cells("COD").Value = e.Record.Cells("COD_ORIG").Value
                    'e.Record.CancelUpdate()
                    mbCancelarUpdate = True
                    mbActuCodigo = False
                    Exit Sub
                End If
            End If

        End If


    End Sub


    Private Sub MantenimientoMonedas_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        Dim sCadenaEntrada As String
        sCadenaEntrada = "Inicio Pagina Mantenimiento Monedas"

        Dim oRegistro As New GSClient.RegistrosRule()

        oRegistro.RegistrarEntradaOSalida(oUsu.Cod, AccionesGS.ACCMonCon, sCadenaEntrada)

    End Sub


    Protected Overrides Sub Finalize()
        Dim sCadenaEntrada As String
        sCadenaEntrada = "Fin Pagina Mantenimiento Monedas"

        Dim oRegistro As New GSClient.RegistrosRule()

        oRegistro.RegistrarEntradaOSalida(oUsu.Cod, AccionesGS.ACCMonCon, sCadenaEntrada)

        MyBase.Finalize()

    End Sub

    ' ''' <summary>
    ' ''' Funcion que lanza la accion de eliinar en caso de que se pulse el boton suprimir
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo máximo: 0 </remarks>

    Private Sub XamDGMonedas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Input.KeyEventArgs) Handles XamDGMonedas.KeyDown
        If e.Key = Key.Delete Then
            cmdEliminar_Click(Me.XamDGMonedas, e)
        End If
    End Sub

    ' ''' <summary>
    ' ''' Funcion que oculta los tabs cuando se cierra el del listado
    ' ''' </summary>
    ' ''' <param name="sender">Objeto en el que se produce el evento </param>
    ' ''' <param name="e">Evento </param>
    ' ''' <returns></returns>
    ' ''' <remarks>Llamada desde: Evento  </remarks>
    ' ''' <remarks>Tiempo mÃ¡ximo: 0 </remarks>
    Private Sub tbiPreview_Closing(ByVal sender As Object, ByVal e As Infragistics.Windows.Controls.Events.TabClosingEventArgs) Handles tbiPreview.Closing
        Me.tbiData.Width = 0
    End Sub




    'Private Sub TabGeneral_SelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Controls.SelectionChangedEventArgs) Handles TabGeneral.SelectionChanged
    '    Dim selectedTabItem As Infragistics.Windows.Controls.TabItemEx = TabGeneral.SelectedItem
    '    If Not IsDBNull(selectedTabItem) Then
    '        If selectedTabItem.Name = "tbiPreview" Then
    '            tbiPreview.Visibility = Windows.Visibility.Visible
    '            XamReportPreview1.GeneratePreview(oReport, False, True)
    '        End If
    '    End If

    'End Sub





End Class
