﻿Imports System.Windows.Interop
Imports System.Windows.Markup
Imports GSClient

Partial Public Class PInicio
    Inherits GSClient.PageBase

    Public Sub New()
        MyBase.New(BrowserInteropHelper.Source.Query.ToString())

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        Dim sQueryString As String = BrowserInteropHelper.Source.Query.ToString()
        sQueryString = sQueryString.Trim("?")
        Dim arParam() As String = sQueryString.Split("&")

        Select Case m_sUCToOpen

            Case "Monedas"
                Dim ucMonedas As New MantenimientoMonedas()
                Me.GridAplicacion.Children.Add(ucMonedas)
            Case "Paises"
                Dim ucPaisesProvincias As New MantenimientoPaisesProvincias()
                Me.GridAplicacion.Children.Add(ucPaisesProvincias)
            Case "Unidades"
                Dim ucUnidades As New MantenimientoUnidades()
                Me.GridAplicacion.Children.Add(ucUnidades)
            Case "ViaPago"
                Dim ucViasPago As New MantenimientoViaPago()
                Me.GridAplicacion.Children.Add(ucViasPago)
            Case "FormaPago"
                Dim ucFormasPago As New MantenimientoFormaPago()
                Me.GridAplicacion.Children.Add(ucFormasPago)
            Case "Impuestos"
                Dim ucTiposImpuestos As New MantenimientoTiposImpuestos()
                Me.GridAplicacion.Children.Add(ucTiposImpuestos)
            Case Else
                Dim ucErrorLogin As New GSClient.ErrorLogin
                Me.GridAplicacion.Children.Add(ucErrorLogin)
        End Select

        '' This call is required by the Windows Form Designer.
        'InitializeComponent()
        'Me.Language = XmlLanguage.GetLanguage(LanguageTag)
        ''ModulePublic.Escribirlog("Inicio de pagina de inicio")
        'Select Case ModulePublic.iModule

        '    Case ModuleName.FRM_MANTENIMIENTOMONEDAS
        '        'ModulePublic.Escribirlog("Antes de Añadir el user control")
        '        Dim ucMonedas As New MantenimientoMonedas()
        '        Me.GridAplicacion.Children.Add(ucMonedas)
        '        'ModulePublic.Escribirlog("Despues de Añadir el user control")

        '    Case ModuleName.FRM_MANTENIMIENTOUNIDADES
        '        Dim ucUnidades As New MantenimientoUnidades()
        '        Me.GridAplicacion.Children.Add(ucUnidades)

        '    Case ModuleName.FRM_MANTENIMIENTOVIASPAGO
        '        Dim ucViasPago As New MantenimientoViaPago()
        '        Me.GridAplicacion.Children.Add(ucViasPago)

        '    Case ModuleName.FRM_MANTENIMIENTOFORMASPAGO
        '        Dim ucFormasPago As New MantenimientoFormaPago()
        '        Me.GridAplicacion.Children.Add(ucFormasPago)

        '    Case ModuleName.FRM_MANTENIMIENTOPAISESPROVINCIAS
        '        Dim ucPaisesProvincias As New MantenimientoPaisesProvincias()
        '        Me.GridAplicacion.Children.Add(ucPaisesProvincias)

        '    Case ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS
        '        Dim ucTiposImpuestos As New MantenimientoTiposImpuestos()
        '        Me.GridAplicacion.Children.Add(ucTiposImpuestos)

        'End Select



        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Overrides Sub RemoveChild(ByVal oChild As System.Windows.Controls.UserControl)
        Me.GridAplicacion.Children.Remove(oChild)
    End Sub

End Class
