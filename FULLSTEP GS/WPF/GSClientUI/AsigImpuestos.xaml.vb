﻿Imports GSClient
Imports Fullstep.FSNLibrary
Imports Infragistics.Windows.Controls
Imports GSServerModel
Imports System.Data
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.ComponentModel
Imports Infragistics.Windows.Editors

Public Class AsigImpuestos
    Inherits ControlBase

    Public Event CerrarAsigImpuestos()

    Private _iIdImpuestoValor As Integer
    Private _bAllowEdit As Boolean

    Public Shared ReadOnly AllowEditProperty As DependencyProperty = DependencyProperty.Register("AllowEdit", GetType(Boolean), GetType(AsigImpuestos))

    Public Property AllowEdit() As Boolean
        Get
            Return CBool(GetValue(AllowEditProperty))
        End Get
        Set(ByVal value As Boolean)
            SetValue(AllowEditProperty, value)
        End Set
    End Property


    Public Sub New(ByVal sDenPais As String, ByVal sCodImpuesto As String, ByVal iIdImpuestoValor As Integer, ByVal dValor As Double, ByVal sDenImpuesto As String, ByVal bAllowEdit As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.       
        _iIdImpuestoValor = iIdImpuestoValor
        Me.AllowEdit = bAllowEdit

        MyBase.CargarTextos(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, Me.GridPpal)
        Dim oTextos As New GSClient.CTextos
        tbPais.Text &= ": " & sDenPais
        tbImpuesto.Text = tbImpuesto.Text.Replace("@@IMP", sCodImpuesto & " " & dValor.ToString & "% " & sDenImpuesto)
        CType(xamtcAsig.Items(0), TabItemEx).Header = oTextos.DevolverTextosMensaje(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, 23)
        CType(xamtcAsig.Items(1), TabItemEx).Header = oTextos.DevolverTextosMensaje(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, 24)
        If Not bAllowEdit Then
            btnCancelar.Content = oTextos.DevolverTextosMensaje(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, 36)
            btnCancelar.ToolTip = oTextos.DevolverTextosMensaje(ModuleName.FRM_MANTENIMIENTOTIPOSIMPUESTOS, 36)
        End If        

        ConfigurarPermisos()

        CargarDatos(iIdImpuestoValor)
    End Sub

    ' <summary>Configuracion de permisos</summary>    
    ' <remarks>Llamada desde: New</remarks>    

    Private Sub ConfigurarPermisos()
        XamDGArticulos.FieldSettings.AllowEdit = Me.AllowEdit
    End Sub

    '<summary>Obtiene los objetos de datos y los carga en los controles</summary>        
    '<remarks>Llamada desde: New, cmdRecargar_Click</remarks>

    Private Sub CargarDatos(ByVal iIdImpuestoValor As Integer)
        'Obtener objetos de datos        
        Dim oMatRule As New GSClient.MaterialesRule
        Dim oEstrMat As DataSet
        If Not (ModulePublic.oUsu.Acciones.Item(CLng(Fullstep.FSNLibrary.TiposDeDatos.AccionesDeSeguridad.TipImpRestMatUsu)) Is Nothing) Then
            Dim oUsuRule As New UsuarioRule
            Dim oPer As GSServerModel.Persona = oUsuRule.DevolverPersona(ModulePublic.oUsu.Persona)
            oEstrMat = oMatRule.DevolverEstructuraMaterialesImpuestos(iIdImpuestoValor, ModulePublic.ParametrosInstalacion.gsIdioma, oPer.Equipo, oPer.Codigo, True)
        Else
            oEstrMat = oMatRule.DevolverEstructuraMaterialesImpuestos(iIdImpuestoValor, ModulePublic.ParametrosInstalacion.gsIdioma, , , True)
        End If

        'Data binding
        Me.AddBindingObject(oEstrMat, "EstructuraMateriales")

        tvwMateriales.ItemsSource = New EstructuraMateriales(oEstrMat)

        Me.MakeBinding(Me.GridPpal)
    End Sub

    Private Sub CheckAsignado_Changed(ByVal sender As Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of Object))
        Dim oGMN As GMNView = CType(sender, XamCheckEditor).DataContext

        'Dim oBindingExpression As BindingExpression = CType(sender, XamCheckEditor).GetBindingExpression(XamCheckEditor.IsCheckedProperty)
        'Dim oParentBinding As Binding = oBindingExpression.ParentBinding

        If Not oGMN.Desasignando Then oGMN.AsignarRamaMaterial(e.NewValue)
    End Sub

#Region " Botones "

    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnAceptar.Click
        Me.Cursor = Cursors.Wait

        Dim oError As New GSServerModel.GSException(False, "", "", True)

        XamDGArticulos.EndEdit()

        Dim oGestorAcciones As New GestorAcciones(tvwMateriales.ItemsSource)
        oGestorAcciones.CrearAcciones()

        If oGestorAcciones.HayAcciones Then
            Dim oMatRule As New GSClient.MaterialesRule
            'oError = oMatRule.ActualizarEstructuraMaterialesImpuestos(_iIdImpuestoValor, oGestorAcciones.oAsignar, oGestorAcciones.oDesasignar, oGestorAcciones.oIncluir, _
            '                 oGestorAcciones.oExcluir, oGestorAcciones.oArtAsignar, oGestorAcciones.oArtDesasignar, oGestorAcciones.oArtIncluir, oGestorAcciones.oArtExcluir)
            oError = oMatRule.ActualizarEstructuraMaterialesImpuestos(_iIdImpuestoValor, oGestorAcciones.oAsignar, oGestorAcciones.oDesasignar, _
                              oGestorAcciones.oArtAsignar, oGestorAcciones.oArtDesasignar, oGestorAcciones.oArtIncluir, oGestorAcciones.oArtExcluir)
        End If

        Me.Cursor = Cursors.Arrow

        If oError.Number <> ErroresGS.TESnoerror Then
            GSClient.TratarError(oError)
        Else
            RaiseEvent CerrarAsigImpuestos()
        End If
    End Sub

    Private Sub xamtcAsig_SelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Controls.SelectionChangedEventArgs) Handles xamtcAsig.SelectionChanged
        If CType(xamtcAsig.SelectedItem, TabItemEx).Name = tciArticulos.Name Then
            Dim bCancelar As Boolean = False
            Dim oGMN As GMNView

            If tvwMateriales.SelectedItem Is Nothing Then
                bCancelar = True
            Else
                oGMN = CType(tvwMateriales.SelectedItem, GMNView)
                If oGMN.Nivel <> 4 Then bCancelar = True
            End If

            If bCancelar Then
                Mensajes.SeleccionarMaterial()
                xamtcAsig.SelectedIndex = 0
            Else
                If CargarArticulos(oGMN) = 0 Then
                    Mensajes.MaterialSinArticulos()
                    xamtcAsig.SelectedIndex = 0
                End If
            End If
        End If
    End Sub

    Private Function CargarArticulos(ByVal oGMN As GMNView) As Integer
        Dim oArticulosList As BindingList(Of ArticuloView)

        If oGMN.Articulos Is Nothing Then
            Dim oMatRule As New GSClient.MaterialesRule
            Dim oArticulos As Articulos = oMatRule.DevolverArticulosGMNImpuestos(_iIdImpuestoValor, New GrupoMaterial(oGMN.GMNPadre.GMNPadre.GMNPadre.Codigo, oGMN.GMNPadre.GMNPadre.Codigo, oGMN.GMNPadre.Codigo, oGMN.Codigo))
            oArticulosList = NuevaBindingListArticulos(oArticulos, oGMN)
        Else
            oArticulosList = oGMN.Articulos
        End If
        Me.AddBindingObject(oArticulosList, "Articulos")
        oGMN.Articulos = oArticulosList
        Me.MakeBinding(XamDGArticulos)

        Return oArticulosList.Count
    End Function

    '<summary>Crea una BindingList a partir de la clase Articulo. Para que el grid ofrezca la funcionalidad de modificar registros
    'los objetos a los que se hace el binding tienen que ser una BindingList y deben tener un constructor sin parámetros</summary>    
    '<param name="oArticulos">Objeto de tipo Articulos</param>
    '<param name="iAsignado">Indica si el Material de los artículos tiene el impuesto asignado</param>
    '<remarks>Llamada desde: CargarArticulos</remarks>

    Private Function NuevaBindingListArticulos(ByVal oArticulos As Articulos, ByVal oGMN As GMNView) As BindingList(Of ArticuloView)
        Dim oArticulosView As New BindingList(Of ArticuloView)

        For Each oArticulo As Articulo In oArticulos
            oArticulosView.Add(New ArticuloView(oArticulo, oGMN))
        Next

        Return oArticulosView
    End Function

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnCancelar.Click
        RaiseEvent CerrarAsigImpuestos()
    End Sub

#End Region

#Region " Clase GestorAcciones "

    Private Class GestorAcciones
        Public oAsignar As GruposMaterial
        Public oDesasignar As GruposMaterial
        'Ya no son necesarios
        'Public oIncluir As GruposMaterial
        'Public oExcluir As GruposMaterial
        Public oArtAsignar As Articulos
        Public oArtDesasignar As Articulos
        Public oArtIncluir As Articulos
        Public oArtExcluir As Articulos

        Private _oEstrMatView As EstructuraMateriales

        Public Sub New(ByVal oEstrMatView As EstructuraMateriales)
            oAsignar = New GruposMaterial
            oDesasignar = New GruposMaterial
            'Ya no son necesarios
            'oIncluir = New GruposMaterial
            'oExcluir = New GruposMaterial
            oArtAsignar = New Articulos
            oArtDesasignar = New Articulos
            oArtIncluir = New Articulos
            oArtExcluir = New Articulos

            _oEstrMatView = oEstrMatView
        End Sub

        Public Sub CrearAcciones()
            For Each oGMN1 As GMNView In _oEstrMatView
                AsignarAColAcciones(oGMN1.Codigo, Nothing, Nothing, Nothing, Nothing, oGMN1.Accion)

                For Each oGMN2 As GMNView In oGMN1.Materiales
                    AsignarAColAcciones(oGMN1.Codigo, oGMN2.Codigo, Nothing, Nothing, Nothing, oGMN2.Accion)

                    For Each oGMN3 As GMNView In oGMN2.Materiales
                        AsignarAColAcciones(oGMN1.Codigo, oGMN2.Codigo, oGMN3.Codigo, Nothing, Nothing, oGMN3.Accion)

                        For Each oGMN4 As GMNView In oGMN3.Materiales
                            AsignarAColAcciones(oGMN1.Codigo, oGMN2.Codigo, oGMN3.Codigo, oGMN4.Codigo, Nothing, oGMN4.Accion)

                            If Not oGMN4.Articulos Is Nothing AndAlso oGMN4.Articulos.Count > 0 Then
                                For Each oArt As ArticuloView In oGMN4.Articulos
                                    AsignarAColAcciones(oGMN1.Codigo, oGMN2.Codigo, oGMN3.Codigo, oGMN4.Codigo, oArt.Codigo, oArt.Accion)
                                Next
                            End If
                        Next
                    Next
                Next
            Next
        End Sub

        Private Sub AsignarAColAcciones(ByVal sGMN1 As String, ByVal sGMN2 As String, ByVal sGMN3 As String, ByVal sGMN4 As String, ByVal sArt As String, ByVal Accion As GMNView.AccionSobreMaterial)
            Select Case Accion
                Case GMNView.AccionSobreMaterial.SinAccion
                Case GMNView.AccionSobreMaterial.Asignar
                    If sArt Is Nothing Then
                        oAsignar.Add(New GrupoMaterial(sGMN1, sGMN2, sGMN3, sGMN4))
                    Else
                        oArtAsignar.Add(New Articulo(sGMN1, sGMN2, sGMN3, sGMN4, sArt))
                    End If
                Case GMNView.AccionSobreMaterial.Desasignar
                    If sArt Is Nothing Then
                        oDesasignar.Add(New GrupoMaterial(sGMN1, sGMN2, sGMN3, sGMN4))
                    Else
                        oArtDesasignar.Add(New Articulo(sGMN1, sGMN2, sGMN3, sGMN4, sArt))
                    End If

                Case ArticuloView.AccionSobreArticulo.Incluir
                    oArtIncluir.Add(New Articulo(sGMN1, sGMN2, sGMN3, sGMN4, sArt))                    
                Case ArticuloView.AccionSobreArticulo.Excluir                    
                    oArtExcluir.Add(New Articulo(sGMN1, sGMN2, sGMN3, sGMN4, sArt))                    
                Case ArticuloView.AccionSobreArticulo.DesasignarYExcluir                    
                    oArtDesasignar.Add(New Articulo(sGMN1, sGMN2, sGMN3, sGMN4, sArt))
                    oArtExcluir.Add(New Articulo(sGMN1, sGMN2, sGMN3, sGMN4, sArt))                    
                Case ArticuloView.AccionSobreArticulo.AsignarYIncluir                    
                    oArtAsignar.Add(New Articulo(sGMN1, sGMN2, sGMN3, sGMN4, sArt))
                    oArtIncluir.Add(New Articulo(sGMN1, sGMN2, sGMN3, sGMN4, sArt))                    
            End Select
        End Sub

        Public Function HayAcciones() As Boolean
            'Return (oAsignar.Count + oDesasignar.Count + oIncluir.Count + oExcluir.Count + _
            '        oArtAsignar.Count + oArtDesasignar.Count + oArtIncluir.Count + oArtExcluir.Count > 0)
            Return (oAsignar.Count + oDesasignar.Count + oArtAsignar.Count + oArtDesasignar.Count + oArtIncluir.Count + oArtExcluir.Count > 0)
        End Function

    End Class

#End Region

End Class

Public Class EstructuraMateriales
    Inherits EstructuraGMNView

    Public Sub New(ByVal dsData As DataSet)
        MyBase.New()

        Dim sDenField As String
        Select Case ModulePublic.ParametrosInstalacion.gsIdioma
            Case "SPA"
                sDenField = "DEN_SPA"
            Case "ENG"
                sDenField = "DEN_ENG"
            Case "GER"
                sDenField = "DEN_GER"
        End Select
        If Not dsData.Tables(0) Is Nothing Then
            'GMN1                
            For Each oRow As DataRow In dsData.Tables(0).Rows
                Dim iAsig As Nullable(Of Integer) = Nothing
                If Not oRow.IsNull("ASIG") Then iAsig = oRow("ASIG")
                Dim oGMN1 As New GMNView(oRow("COD"), oRow(sDenField), 1, iAsig, oRow("ASIG_COM"))
                Me.Add(oGMN1)


            Next

            'GMN2                
            For Each oRow As DataRow In dsData.Tables(1).Rows
                Dim iAsig As Nullable(Of Integer) = Nothing
                If Not oRow.IsNull("ASIG") Then iAsig = oRow("ASIG")
                Dim oGMN2 As New GMNView(oRow("COD"), oRow(sDenField), 2, iAsig, oRow("ASIG_COM"))
                Me.Find(oRow("GMN1")).AddMaterial(oGMN2)
            Next

            'GMN3                
            For Each oRow As DataRow In dsData.Tables(2).Rows
                Dim iAsig As Nullable(Of Integer) = Nothing
                If Not oRow.IsNull("ASIG") Then iAsig = oRow("ASIG")
                Dim oGMN3 As New GMNView(oRow("COD"), oRow(sDenField), 3, iAsig, oRow("ASIG_COM"))
                Me.Find(oRow("GMN1")).Materiales.Find(oRow("GMN2")).AddMaterial(oGMN3)
            Next

            'GMN4                
            For Each oRow As DataRow In dsData.Tables(3).Rows
                Dim iAsig As Nullable(Of Integer) = Nothing
                If Not oRow.IsNull("ASIG") Then iAsig = oRow("ASIG")
                Dim oGMN4 As New GMNView(oRow("COD"), oRow(sDenField), 4, iAsig, oRow("ASIG_COM"))
                Me.Find(oRow("GMN1")).Materiales.Find(oRow("GMN2")).Materiales.Find(oRow("GMN3")).AddMaterial(oGMN4)
            Next
        End If
    End Sub

    Public Overloads Function Find(ByVal sCod As String) As GMNView
        For Each oItem As GMNView In Me
            If oItem.Codigo = sCod Then
                Return oItem
            End If
        Next
    End Function

End Class

Public Class EstructuraGMNView
    Inherits Lista(Of GMNView)

    Public Sub New()
        MyBase.New(False, String.Empty, String.Empty, False)
    End Sub

    Public Overloads Function Find(ByVal sCod As String) As GMNView
        For Each oItem As GMNView In Me
            If oItem.Codigo = sCod Then
                Return oItem
            End If
        Next
    End Function

End Class

Public Class GMNView
    Implements INotifyPropertyChanged

    Public Enum AccionSobreMaterial
        SinAccion = 0
        Asignar = 1
        Desasignar = 2
        'Al principio se iba a utilizar la tabla IMPUESTO_NO_MAT para contener los materiales no incluidos, con lo que podían darse acciones como incluir, excluir,
        'desasignar y excluir o asignar e incluir. Podía haber una rama de material seleccionada con algún hijo excluido (los contenidos en esa tabla).
        'después de una revisión de dicha funcionalidad, se decidió que esta forma de mostrar los datos no era muy clara para el usuario, ya que se mostraban como
        'seleccionados materiales que podían tener hijos no seleccionados. Se decidió que si se deseleccionaba un material se debía deseleccionar su padre si este
        'estaba seleccionado, con lo que los materiales seleccionados aparecerían todos en IMPUESTO_MAT quedando sin uso la tabla IMPUESTO_NO_MAT para los materiales.
        'La tabla IMPUESTO_NO_MAT sólo contendrá registros de artículos no incluidos para algún material, ya que esta funcionalidad se mantiene para los artículos.
        'Incluir = 3
        'Excluir = 4
        'DesasignarYExcluir = 5
        'AsignarYIncluir = 6
    End Enum

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _sCodigo As String
    Private _sDen As String
    Private _sDesc As String
    Private _iNivel As Integer
    Private _iAsignadoAnt As Nullable(Of Integer)
    Private _iAsignado As Nullable(Of Integer)
    Private _bAsignadoComp As Boolean
    Private _bExpanded As Boolean
    Private _bDesasignando As Boolean
    Private _GMNPadre As GMNView
    Private _Accion As AccionSobreMaterial
    Private _Materiales As EstructuraGMNView
    Private _Articulos As BindingList(Of ArticuloView)    

    Public Sub New(ByVal sCod As String, ByVal sDen As String, ByVal iNivel As Integer, ByVal iAsignado As Nullable(Of Integer), ByVal bAsignadoComp As Boolean)
        _sCodigo = sCod
        _sDen = sDen
        _iNivel = iNivel
        _iAsignadoAnt = iAsignado
        _iAsignado = iAsignado
        _bAsignadoComp = bAsignadoComp        
        _Materiales = New EstructuraGMNView
        _Accion = AccionSobreMaterial.SinAccion
    End Sub

#Region " Propiedades "

    Property Codigo As String
        Get
            Return _sCodigo
        End Get
        Set(ByVal value As String)
            _sCodigo = value
        End Set
    End Property

    Property Denominacion As String
        Get
            Return _sDen
        End Get
        Set(ByVal value As String)
            _sDen = value
        End Set
    End Property

    Public ReadOnly Property Descripcion As String
        Get
            Return _sCodigo & " - " & _sDen
        End Get
    End Property

    Property Nivel As Integer
        Get
            Return _iNivel
        End Get
        Set(ByVal value As Integer)
            _iNivel = value
        End Set
    End Property

    Property Asignado As Nullable(Of Integer)
        Get
            Return _iAsignado
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _iAsignado = value
            NotifyPropertyChanged("Asignado")
        End Set
    End Property

    ReadOnly Property AsignadoAnt As Nullable(Of Integer)
        Get
            Return _iAsignadoAnt
        End Get
    End Property

    Property AsignadoComp As Boolean
        Get
            Return _bAsignadoComp
        End Get
        Set(ByVal value As Boolean)
            _bAsignadoComp = value            
        End Set
    End Property

    Property Expanded As Boolean
        Get
            Return _bExpanded
        End Get
        Set(ByVal value As Boolean)
            _bExpanded = value
            If _bExpanded AndAlso Not _GMNPadre Is Nothing Then If Not _GMNPadre.CheckAsignado Then _GMNPadre.Expanded = True
        End Set
    End Property

    Property Desasignando As Boolean
        Get
            Return _bDesasignando
        End Get
        Set(ByVal value As Boolean)
            _bDesasignando = value            
        End Set
    End Property

    Public Property CheckAsignado As Boolean
        Get
            Return SQLBinaryToBoolean(_iAsignado)
        End Get
        Set(ByVal value As Boolean)
            _iAsignado = BooleanToSQLBinary(value)
            NotifyPropertyChanged("CheckAsignado")
        End Set
    End Property

    Public Property GMNPadre As GMNView
        Get
            Return _GMNPadre
        End Get
        Set(ByVal value As GMNView)
            _GMNPadre = value
        End Set
    End Property

    Public Property Accion As AccionSobreMaterial
        Get
            Return _Accion
        End Get
        Set(ByVal value As AccionSobreMaterial)
            _Accion = value
        End Set
    End Property

    Public Property Materiales As EstructuraGMNView
        Get
            Return _Materiales
        End Get
        Set(ByVal value As EstructuraGMNView)
            _Materiales = value
        End Set
    End Property

    Public Property Articulos As BindingList(Of ArticuloView)
        Get
            Return _Articulos
        End Get
        Set(ByVal value As BindingList(Of ArticuloView))
            _Articulos = value
        End Set
    End Property


#End Region

    Public Sub AddMaterial(ByRef oGMN As GMNView)
        If oGMN.Asignado Is Nothing Then oGMN.Asignado = _iAsignado

        If oGMN.CheckAsignado And Not Me.CheckAsignado Then Me.Expanded = True

        oGMN.GMNPadre = Me
        _Materiales.Add(oGMN)
    End Sub

    Private Sub NotifyPropertyChanged(ByVal info As String)              
        RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(info))
    End Sub

    Public Sub AsignarRamaMaterial(ByVal bAsignado As Boolean)        
        SetAccion()

        If Not _Articulos Is Nothing Then
            For Each oArt As ArticuloView In _Articulos
                oArt.Seleccionado = bAsignado
            Next
        End If

        For Each oMat As GMNView In _Materiales
            oMat.CheckAsignado = bAsignado

            'oMat.AsignarRamaMaterial(bAsignado)
        Next

        'Desasignar material padre si es necesario  
        If Not bAsignado Then DesasignarGMNPadre()        
    End Sub

    Private Sub DesasignarGMNPadre()
        If Not _GMNPadre Is Nothing AndAlso _GMNPadre.CheckAsignado Then
            _GMNPadre.Desasignando = True

            'Desasignar padre
            _GMNPadre.CheckAsignado = False
            _GMNPadre.SetAccion()

            'Asignar materiales hermanos
            For Each oGMNHer As GMNView In _GMNPadre.Materiales
                If oGMNHer.Codigo <> Me.Codigo Then
                    oGMNHer.CheckAsignado = True
                    oGMNHer.SetAccion()
                End If
            Next

            _GMNPadre.DesasignarGMNPadre()

            _GMNPadre.Desasignando = False
        End If
    End Sub

    Private Sub SetAccion()
        'Al principio se iba a utilizar la tabla IMPUESTO_NO_MAT para contener los materiales no incluidos, con lo que podían darse acciones como incluir, excluir,
        'desasignar y excluir o asignar e incluir. Podía haber una rama de material seleccionada con algún hijo excluido (los contenidos en esa tabla).
        'después de una revisión de dicha funcionalidad, se decidió que esta forma de mostrar los datos no era muy clara para el usuario, ya que se mostraban como
        'seleccionados materiales que podían tener hijos no seleccionados. Se decidió que si se deseleccionaba un material se debía deseleccionar su padre si este
        'estaba seleccionado, con lo que los materiales seleccionados aparecerían todos en IMPUESTO_MAT quedando sin uso la tabla IMPUESTO_NO_MAT para los materiales.
        'La tabla IMPUESTO_NO_MAT sólo contendrá registros de artículos no incluidos para algún material, ya que esta funcionalidad se mantiene para los artículos.

        _Accion = AccionSobreMaterial.SinAccion

        If _GMNPadre Is Nothing Then
            'Caso de GMN1
            If _iAsignadoAnt Is Nothing And (Not _iAsignado Is Nothing AndAlso _iAsignado = 1) Then
                _Accion = AccionSobreMaterial.Asignar
            ElseIf _iAsignadoAnt = 1 And (Not _iAsignado Is Nothing AndAlso _iAsignado = 0) Then
                _Accion = AccionSobreMaterial.Desasignar
            End If
        Else
            If _GMNPadre.AsignadoAnt Is Nothing Then
                If _GMNPadre.Asignado Is Nothing Then
                    'Padre sin asignar y sin cambio

                    If _iAsignadoAnt Is Nothing Then
                        If _iAsignado Is Nothing Then
                            'Sin asignar y sin cambio                            
                        Else
                            If _iAsignado = 1 Then
                                _Accion = AccionSobreMaterial.Asignar
                            Else
                                'Sin asignar y sin cambio                               
                            End If
                        End If
                    Else
                        '_iAsignado no puede ser Nothing

                        If _iAsignadoAnt = 1 Then
                            If _iAsignado = 1 Then
                                'Asignado sin cambio                                
                            Else
                                _Accion = AccionSobreMaterial.Desasignar
                            End If
                        Else
                            If _iAsignado = 1 Then
                                _Accion = AccionSobreMaterial.Asignar
                            Else
                                'Deasasignado sin cambio                                
                            End If
                        End If
                    End If
                Else
                    If _GMNPadre.Asignado = 1 Then
                        'Cambio padre a asignado
                        '_iAsignado no puede ser Nothing

                        If _iAsignadoAnt Is Nothing Then
                            If _iAsignado = 1 Then
                                'Se ha asignado la rama del padre, no hay que hacer nada con este                               
                            Else
                                '_Accion = AccionSobreMaterial.Excluir
                                'No puede darse, si se desasigna un hijo se desasigna el padre
                            End If
                        Else
                            If _iAsignadoAnt = 1 Then
                                If _iAsignado = 1 Then
                                    'Si se asigna el padre no hace falta tener asignado este también
                                    _Accion = AccionSobreMaterial.Desasignar
                                Else
                                    '_Accion = AccionSobreMaterial.DesasignarYExcluir
                                    'No puede darse, si se desasigna un hijo se desasigna el padre
                                End If
                            Else
                                'No puede darse, si está asignado el padre los hijos están asignados automaticamente
                                'If _iAsignado = 1 Then
                                '    If _GMNPadre.Accion = AccionSobreMaterial.SinAccion Then
                                '        _Accion = AccionSobreMaterial.Incluir
                                '    End If
                                'Else
                                '    _Accion = AccionSobreMaterial.Excluir
                                'End If
                            End If
                        End If
                    Else
                        If _GMNPadre.Accion = AccionSobreMaterial.SinAccion Then
                            'Padre desasignado sin cambio
                            If _iAsignadoAnt Is Nothing Then
                                If _iAsignado = 1 Then
                                    _Accion = AccionSobreMaterial.Asignar
                                Else
                                    'Desasignado sin cambio                               
                                End If
                            Else
                                If _iAsignadoAnt = 1 Then
                                    If _iAsignado = 1 Then
                                        'Asignado sin cambio                                    
                                    Else
                                        _Accion = AccionSobreMaterial.Desasignar
                                    End If
                                Else
                                    If _iAsignado = 1 Then
                                        _Accion = AccionSobreMaterial.Asignar
                                    Else
                                        'Desasignado sin cambio                                    
                                    End If
                                End If
                            End If
                        Else
                            'Cambio Padre a desasignado o excluido
                            If _iAsignadoAnt Is Nothing Then
                                If _iAsignado = 1 Then
                                    _Accion = AccionSobreMaterial.Asignar
                                Else
                                    'Desasignado sin cambio                               
                                End If
                            Else
                                If _iAsignadoAnt = 1 Then
                                    'No puede darse
                                Else
                                    If _iAsignado = 1 Then
                                        '_Accion = AccionSobreMaterial.AsignarYIncluir
                                        _Accion = AccionSobreMaterial.Asignar
                                    Else
                                        'No puede darse                                    
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Else
                '_GMNPadre.Asignado no puede ser Nothing                
                '_iAsignado no puede ser nothing

                If _GMNPadre.AsignadoAnt = 1 Then
                    If _GMNPadre.Asignado = 1 Then
                        'Padre asignado sin cambio 

                        If _iAsignadoAnt Is Nothing Then
                            If _iAsignado = 1 Then
                                'Asignado sin cambio                                
                            Else
                                '_Accion = AccionSobreMaterial.Excluir
                                'No puede darse, si se desasigna un hijo se desasigna el padre
                            End If
                        Else
                            If _iAsignadoAnt = 1 Then
                                If _iAsignado = 1 Then
                                    'Asignado sin cambio                                    
                                Else
                                    '_Accion = AccionSobreMaterial.Excluir
                                    'No puede darse, si se desasigna un hijo se desasigna el padre
                                End If
                            Else
                                If _iAsignado = 1 Then
                                    '_Accion = AccionSobreMaterial.Incluir
                                    'No puede darse, no puede haber un hijo excluido con un padre asignado
                                Else
                                    'Excluido sin cambio                                    
                                End If
                            End If
                        End If
                    Else
                        'Cambio padre a desasignado

                        If _iAsignadoAnt = 1 Then
                            'No puede darse
                        Else
                            If _iAsignado = 1 Then
                                '_Accion = AccionSobreMaterial.AsignarYIncluir
                                _Accion = AccionSobreMaterial.Asignar
                            Else
                                '_Accion = AccionSobreMaterial.Incluir
                                _Accion = AccionSobreMaterial.Asignar
                            End If
                        End If
                    End If
                Else
                    If _GMNPadre.Asignado = 1 Then
                        'Cambio padre a asignado

                        If _iAsignadoAnt Is Nothing Then
                            If _iAsignado = 1 Then
                                '_Accion = AccionSobreMaterial.Incluir
                                'No puede darse, si el padre está asignado los hijos también
                            Else
                                '_Accion = AccionSobreMaterial.Excluir
                                'No puede darse, si se desasigna un hijo se desasigna el padre
                            End If
                        Else
                            If _iAsignadoAnt = 1 Then
                                If _iAsignado = 1 Then
                                    _Accion = AccionSobreMaterial.Desasignar
                                Else
                                    '_Accion = AccionSobreMaterial.DesasignarYExcluir
                                    'No puede darse, si se desasigna un hijo se desasigna el padre
                                End If
                            Else
                                'No puede darse
                            End If
                        End If
                    Else
                        'Padre desasignado sin cambio

                        If _iAsignadoAnt = 1 Then
                            If _iAsignado = 1 Then
                                'Asignado sin cambio                                
                            Else
                                _Accion = AccionSobreMaterial.Desasignar
                            End If
                        Else
                            If _iAsignado = 1 Then
                                _Accion = AccionSobreMaterial.Asignar
                            Else
                                'Desasignado sin cambio                               
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub
End Class

Public Class ArticuloView
    Implements INotifyPropertyChanged

    Public Enum AccionSobreArticulo
        SinAccion = 0
        Asignar = 1
        Desasignar = 2
        Incluir = 3
        Excluir = 4
        DesasignarYExcluir = 5
        AsignarYIncluir = 6
    End Enum

    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    Private _bSeleccionadoAnt As Boolean
    Private _bSeleccionado As Boolean
    Private _sCod As String
    Private _sDen As String
    Private _Accion As AccionSobreArticulo
    Private _oGMN As GMNView

    Property Seleccionado() As Boolean
        Get
            Return _bSeleccionado
        End Get
        Set(ByVal Value As Boolean)
            _bSeleccionado = Value
            NotifyPropertyChanged("Seleccionado")
            SetAccion()
        End Set
    End Property

    Property Codigo() As String
        Get
            Return _sCod
        End Get
        Set(ByVal Value As String)
            _sCod = Value
            NotifyPropertyChanged("Codigo")
        End Set
    End Property

    Property Denominacion() As String
        Get
            Return _sDen
        End Get
        Set(ByVal Value As String)
            _sDen = Value
            NotifyPropertyChanged("Denominacion")
        End Set
    End Property

    Public Property Accion As AccionSobreArticulo
        Get
            Return _Accion
        End Get
        Set(ByVal value As AccionSobreArticulo)
            _Accion = value
        End Set
    End Property

    Public Property GMN As GMNView
        Get
            Return _oGMN
        End Get
        Set(ByVal value As GMNView)
            _oGMN = value
        End Set
    End Property

#Region " Constructores "

    '<summary>Es necesario un constructor sin parámetros para las funcionalidades del grid</summary>            

    Public Sub New()
    End Sub

    Public Sub New(ByVal oArticulo As GSServerModel.Articulo, ByVal oGMN As GMNView)
        If oArticulo.Asignado Is Nothing Then
            _bSeleccionado = (oGMN.Asignado = 1)
        Else
            _bSeleccionado = (oArticulo.Asignado = 1)
        End If
        _bSeleccionadoAnt = _bSeleccionado
        _sCod = oArticulo.Codigo
        _sDen = oArticulo.Denominacion
        _Accion = AccionSobreArticulo.SinAccion
        _oGMN = oGMN
    End Sub

#End Region

    Private Sub SetAccion()
        _Accion = AccionSobreArticulo.SinAccion

        If _oGMN.AsignadoAnt Is Nothing Then
            If _oGMN.Asignado Is Nothing Then
                'El GMN no está asignado, sin cambio

                If _bSeleccionadoAnt Then
                    If _bSeleccionado Then
                        'Artículo incluido sin cambio
                    Else
                        _Accion = AccionSobreArticulo.Excluir
                    End If
                Else
                    If _bSeleccionado Then
                        _Accion = AccionSobreArticulo.Incluir
                    Else
                        'Artículo no asignado sin cambio
                    End If
                End If
            Else
                If _oGMN.Asignado = 1 Then
                    If _oGMN.Accion = GMNView.AccionSobreMaterial.Asignar Then
                        'Cambio de GMN a asignado

                        If _bSeleccionadoAnt Then
                            If _bSeleccionado Then
                                _Accion = AccionSobreArticulo.Desasignar
                            Else
                                _Accion = AccionSobreArticulo.Excluir
                            End If
                        Else
                            If _bSeleccionado Then
                                'No hay que hacer nada porque ya está seleccionado el material
                            Else
                                _Accion = AccionSobreArticulo.Excluir
                            End If
                        End If
                    Else
                        'GMN asignado porque está asignado su padre

                        If _bSeleccionadoAnt Then
                            If _bSeleccionado Then
                                'No hay que hacer nada porque ya está seleccionado el material
                            Else
                                _Accion = AccionSobreArticulo.Excluir
                            End If
                        Else
                            If _bSeleccionado Then
                                _Accion = AccionSobreArticulo.Incluir
                            Else
                                'Sin cambio
                            End If
                        End If
                    End If
                Else
                    'Cambio de GMN a desasignado

                    If _bSeleccionadoAnt Then
                        If _bSeleccionado Then
                            _Accion = AccionSobreArticulo.Incluir
                        Else
                            _Accion = AccionSobreArticulo.Desasignar
                        End If
                    Else
                        If _bSeleccionado Then
                            _Accion = AccionSobreArticulo.Asignar
                        Else
                            'Sin cambio
                        End If
                    End If
                End If
            End If
        Else
            If _oGMN.AsignadoAnt = 1 Then
                If _oGMN.Asignado Is Nothing Then
                    'No puede darse
                Else
                    If _oGMN.Asignado = 1 Then
                        'GMN seleccionado sin cambio

                        If _bSeleccionadoAnt Then
                            If _bSeleccionado Then
                                'Sin cambio
                            Else
                                _Accion = AccionSobreArticulo.Excluir
                            End If
                        Else
                            If _bSeleccionado Then
                                _Accion = AccionSobreArticulo.Incluir
                            Else
                                'Sin cambio
                            End If
                        End If
                    Else
                        'Cambio de GMN a no seleccionado

                        If _bSeleccionadoAnt Then
                            If _bSeleccionado Then
                                _Accion = AccionSobreArticulo.Asignar
                            Else
                                'No hay que hacer nada porque se ha desasignado el material
                            End If
                        Else
                            If _bSeleccionado Then
                                _Accion = AccionSobreArticulo.AsignarYIncluir
                            Else
                                _Accion = AccionSobreArticulo.Incluir
                            End If
                        End If
                    End If
                End If
            Else
                If _oGMN.Asignado Is Nothing Then
                    'No puede darse
                Else
                    If _oGMN.Asignado = 1 Then
                        'Cambio de GMN a asignado

                        If _bSeleccionadoAnt Then
                            If _bSeleccionado Then
                                _Accion = AccionSobreArticulo.Desasignar
                            Else
                                _Accion = AccionSobreArticulo.DesasignarYExcluir
                            End If
                        Else
                            If _bSeleccionado Then
                                'Sin cambio
                            Else
                                _Accion = AccionSobreArticulo.Excluir
                            End If
                        End If
                    Else
                        'GMN sin asignar sin cambio

                        If _bSeleccionadoAnt Then
                            If _bSeleccionado Then
                                'No hay cambio
                            Else
                                _Accion = AccionSobreArticulo.Desasignar
                            End If
                        Else
                            If _bSeleccionado Then
                                _Accion = AccionSobreArticulo.Asignar
                            Else
                                'Sin cambio
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(info))
    End Sub
End Class

#Region " Converters "

Public Class TreeViewLineConverter
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        Dim oItem As TreeViewItem = CType(value, TreeViewItem)
        Dim oIC As ItemsControl = ItemsControl.ItemsControlFromItemContainer(oItem)
        Return (oIC.ItemContainerGenerator.IndexFromContainer(oItem) = oIC.Items.Count - 1)
    End Function

    Public Function ConvertBack1(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

Public Class NivelToImageConverter
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        Dim image As BitmapImage = New BitmapImage()
        image.BeginInit()

        Select Case CType(value, Integer)
            Case 1
                image.UriSource = New Uri("/GSClientUI;component/Images/carpetaCommodity.gif", System.UriKind.Relative)
            Case 2
                image.UriSource = New Uri("/GSClientUI;component/Images/carpetaFamilia.gif", System.UriKind.Relative)
            Case 3
                image.UriSource = New Uri("/GSClientUI;component/Images/CarpetaSubFamilia.gif", System.UriKind.Relative)
            Case 4
                image.UriSource = New Uri("/GSClientUI;component/Images/carpetaGrupo.gif", System.UriKind.Relative)
            Case Else
                image.UriSource = Nothing
        End Select

        If Not image.UriSource Is Nothing Then image.EndInit()
        Return image
    End Function

    Public Function ConvertBack1(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

Public Class BooleanANDToVisibilityConverter
    Implements IMultiValueConverter

    Public Function Convert(ByVal values() As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IMultiValueConverter.Convert
        Dim bValor As Boolean = (values(0) And values(1))
        If bValor Then
            Return Visibility.Visible
        Else
            Return Visibility.Collapsed
        End If
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetTypes() As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object() Implements System.Windows.Data.IMultiValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

#End Region




