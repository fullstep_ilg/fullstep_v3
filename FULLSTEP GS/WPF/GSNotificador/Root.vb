Imports System.IO
Imports System.Xml
Imports Fullstep.FSNLibrary
Imports GSServerModel
Imports System.Net

<Serializable()> _
Public Class Root
    Inherits GSServer.RuleBase

    Friend mUserCode As String = ""
    Friend mUserPassword As String = ""
    Friend mRemottingServer As Boolean = False
    Friend mIsAuthenticated As Boolean = False    

    Public Sub New(ByVal oCtx As GSServerModel.Contexto)
        MyBase.New(oCtx)
    End Sub

    ''' <summary>
    ''' Si existe el fichero "MailPwd.txt", lo lee y lo desencriptar para el env�o de emails autenticados.
    ''' </summary>
    Public Sub New(ByVal oCtx As GSServerModel.Contexto, ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(oCtx)

        mRemottingServer = remotting
        mUserCode = UserCode
        mUserPassword = UserPassword
        mIsAuthenticated = isAuthenticated
    End Sub

    Friend Sub Authenticate()
        If Not mIsAuthenticated Then Throw New System.Security.SecurityException("User not authenticated!")
    End Sub


    Public Function Get_Notificar() As Notificar
        Dim oNotif As Notificar
        Authenticate()
        oNotif = New Notificar(Me.Contexto, mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)
        Return oNotif
    End Function

    '<summary>
    'Funci�n que realiza el env�o de los emails. Lee los par�metros MAILSENDUSING, MAILAUTENTICATION y MAILUSER del We.Config.
    'Dependiendo de estos par�metros, hace un env�o autenticado, an�nimo o NTLM.
    'En caso de autenticaci�n, coge la contrase�a de un fichero.    
    '</summary>
    '<param name="oEMail">Objeto de tipo EMail con los datos del correo</param>    
    '<param name="iError">C�digo de error (por referencia)</param>
    '<param name="strError">Descripci�n del error (por referencia)</param>    
    '<param name="bGrabaEnvio">Si hay que grabar el env�o en BD</param>
    ''' <param name="CodProve">Proveedor al q se le envia el mail</param>
    ''' <param name="IdMail">Tipo de Notificacion. Es un valor de FSNLibrary.TipoNotificacion</param> 
    ''' <param name="lIdInstancia">Instancia que asociaremos al mail enviado</param> 
    ''' <remarks>Llamada desde: Todas las funciones de Notificaci�n de Notificar.vb. Tiempo m�ximo: 1 sg.</remarks>
    ''' 
    'Public Function EnviarMail(ByVal oEMail As EMail, ByVal Ruta As String, Optional ByVal bGrabaEnvio As Boolean = True, Optional ByVal CodProve As String = Nothing, _
    '                           Optional ByVal IdMail As Integer = 0, Optional ByVal lIdInstancia As Long = 0) As GSServerModel.GSException
    '    'iError
    '    '   0- SmtpMail.Send(MyMail) casco
    '    '   1- Adjunto
    '    '   2- Todo bien
    '    '   3- Casca EventLog

    '    Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
    '    oError.Number = TiposDeDatos.ErroresGS.TESnoerror
    '    oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

    '    Dim SmtpMail As New Mail.SmtpClient
    '    Dim MyMail As New Mail.MailMessage
    '    Dim myAttachment As Mail.Attachment

    '    Dim oFichero As System.IO.File
    '    Dim sPath As String
    '    Dim sTemp As String = ""

    '    oError.Number = 2

    '    Dim GSDBServer As New GSDatabaseServer.Procesos
    '    Try
    '        Dim sSMTPServer As String = String.Empty
    '        Dim iSMTPPort As Integer = 25
    '        Dim iMailAuthentication As Integer = 0
    '        Dim bBasicaGS As Boolean = False
    '        Dim sSMTPUser As String = String.Empty
    '        Dim sSMTPPwd As String = String.Empty

    '        'Obtener datos configuraci�n servicio correo            
    '        Dim dsParamEmail As DataSet = GSDBServer.DevolverParamConfigServicioEMail
    '        If Not dsParamEmail Is Nothing AndAlso dsParamEmail.Tables.Count > 0 AndAlso dsParamEmail.Tables(0).Rows.Count > 0 Then
    '            Dim drConfig As DataRow = dsParamEmail.Tables(0).Rows(0)

    '            sSMTPServer = DBNullToStr(drConfig("SERVIDOR"))
    '            If Not drConfig.IsNull("PUERTO") Then iSMTPPort = drConfig("PUERTO")
    '            If Not drConfig.IsNull("AUTENTICACION") Then iMailAuthentication = drConfig("AUTENTICACION")
    '            If Not drConfig.IsNull("BASICA_GS") Then bBasicaGS = SQLBinaryToBoolean(drConfig("BASICA_GS"))
    '            sSMTPUser = DBNullToStr(drConfig("USU"))
    '            sSMTPPwd = DBNullToStr(drConfig("PWD"))
    '        End If

    '        SmtpMail.Host = sSMTPServer
    '        SmtpMail.Port = iSMTPPort
    '        'If ConfigurationManager.AppSettings("MAILSENDUSING") = Nothing Then
    '        SmtpMail.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
    '        'Else
    '        'Select Case CInt(ConfigurationManager.AppSettings("MAILSENDUSING"))
    '        '    Case 1
    '        '        SmtpMail.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.PickupDirectoryFromIis
    '        '    Case Else
    '        '        SmtpMail.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
    '        'End Select
    '        'End If
    '        Select Case iMailAuthentication
    '            Case 1
    '                SmtpMail.UseDefaultCredentials = False

    '                If bBasicaGS Then
    '                Else
    '                End If

    '                SmtpMail.Credentials = New NetworkCredential(sSMTPUser, sSMTPPwd)
    '                If sSMTPUser = String.Empty Then
    '                    SmtpMail.Credentials = CredentialCache.DefaultNetworkCredentials
    '                End If
    '            Case 2
    '                SmtpMail.UseDefaultCredentials = True
    '            Case Else
    '                SmtpMail.UseDefaultCredentials = False
    '                SmtpMail.Credentials = CredentialCache.DefaultNetworkCredentials
    '        End Select

    '        MyMail.From = New Mail.MailAddress(oEMail.De.EMail)
    '        MyMail.Subject = oEMail.Asunto

    '        For Each oContacto As Contacto In oEMail.Para
    '            MyMail.To.Add(oContacto.EMail)
    '        Next

    '        If Not oEMail.CC Is Nothing AndAlso oEMail.CC.Count > 0 Then
    '            For Each oContacto As Contacto In oEMail.CC
    '                MyMail.CC.Add(oContacto.EMail)
    '            Next
    '        End If
    '        If Not oEMail.BCC Is Nothing AndAlso oEMail.BCC.Count > 0 Then
    '            For Each oContacto As Contacto In oEMail.BCC
    '                MyMail.CC.Add(oContacto.EMail)
    '            Next
    '        End If

    '        MyMail.Body = oEMail.Contenido

    '        If Not oEMail.Adjuntos Is Nothing AndAlso oEMail.Adjuntos.Count > 0 AndAlso NothingToStr(Ruta) <> String.Empty Then
    '            Dim sRutaAdj As String = Ruta
    '            If Not sRutaAdj.EndsWith("\") Then sRutaAdj &= "\"

    '            For Each oAdj As GSServerModel.File In oEMail.Adjuntos
    '                Try
    '                    myAttachment = New Mail.Attachment(sRutaAdj & oAdj.NombreArchivo)
    '                    MyMail.Attachments.Add(myAttachment)
    '                Catch ex As Exception
    '                    EventLog.WriteEntry("FULLSTEP GS", "Notificar.vb (EnviarMail). Algun adjunto ha fallado", EventLogEntryType.Error, 20000)

    '                    oError.ErrorStr = "Alg�n adjunto ha fallado"
    '                    oError.Number = 1

    '                    If bGrabaEnvio Then
    '                        GSDBServer.GrabarEnviarMensaje(oEMail, True, oError.ErrorStr, Ruta, CodProve, IdMail, lIdInstancia)
    '                    End If

    '                    Return oError
    '                End Try
    '            Next
    '        End If

    '        MyMail.Priority = Mail.MailPriority.Normal
    '        MyMail.IsBodyHtml = (oEMail.TipoEmail = Fullstep.FSNLibrary.TiposDeDatos.TipoEmail.HTML)

    '        SmtpMail.Send(MyMail)
    '        If Not MyMail.Attachments Is Nothing AndAlso MyMail.Attachments.Count > 0 Then
    '            For Each oAttachment As Mail.Attachment In MyMail.Attachments
    '                oAttachment.Dispose()  'Para que los docs. no se queden bloqueados
    '            Next
    '        End If

    '        If bGrabaEnvio Then
    '            GSDBServer.GrabarEnviarMensaje(oEMail, False, String.Empty, Ruta, CodProve, IdMail, lIdInstancia)
    '        End If

    '        SmtpMail = Nothing
    '        MyMail = Nothing
    '        myAttachment = Nothing
    '        oFichero = Nothing

    '        Return oError

    '    Catch ex As System.Security.SecurityException
    '        'error en log
    '        oError.ErrorStr = "Error en log"
    '        oError.Number = 3

    '        If bGrabaEnvio Then
    '            GSDBServer.GrabarEnviarMensaje(oEMail, True, oError.ErrorStr, Ruta, CodProve, IdMail)
    '        End If

    '        Return oError

    '    Catch ex As Exception
    '        EventLog.WriteEntry("FULLSTEP GS", ex.Message, EventLogEntryType.Error, 20000)

    '        oError.ErrorStr = ex.Message
    '        oError.Number = 0

    '        If bGrabaEnvio Then
    '            GSDBServer.GrabarEnviarMensaje(oEMail, True, oError.ErrorStr, Ruta, CodProve, IdMail)
    '        End If

    '        Return (oError)

    '    End Try
    'End Function

End Class
