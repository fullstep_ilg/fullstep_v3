﻿Imports Fullstep.FSNLibrary
Imports GSServerModel
Imports System.Globalization

Public Class HTML
    Inherits GSServer.RuleBase

    Private _sHTML As String

    Public Sub New(ByVal oCtx As GSServerModel.Contexto, ByVal Plantilla As String)
        MyBase.New(oCtx)

        Dim oStream As New System.IO.StreamReader(Plantilla, System.Text.Encoding.Default, True)
        _sHTML = oStream.ReadToEnd()
        oStream.Close()
    End Sub

    '<summary>Funcion que devuelve los datos en HTML de la notificación de una subasta</summary>
    '<param name="sPlantilla">Ruta de la plantilla</param>
    '<param name="oProve">Datos del proveedor</param>
    '<param name="oContacto">Datos del contacto</param>
    '<param name="oProceso">Objeto de tipo Proceso con los datos del proceso</param>
    '<param name="URLWeb">URL de la web</param>  
    '<param name="DateFormat">Formato para fechas</param>  
    '<returns>Un texto con los datos a enviar</returns>
    '<remarks>Llamada desde: ComponerCorreoMuestraNotificacionSubasta, Tiempo máximo: 0,4 seg</remarks>

    Public Function NotificacionSubasta(ByVal oProve As Proveedor, ByVal oContacto As Contacto, ByVal oProceso As Proceso, ByVal URLWeb As String) As String
        Dim oProceRule As New GSServer.ProcesosRule(Me.Contexto)
        Dim oComprador As Comprador
        If NothingToStr(oProceso.Equipo) <> String.Empty And NothingToStr(oProceso.Comprador) <> String.Empty Then
            oComprador = oProceRule.DevolverComprador(oProceso.Equipo, oProceso.Comprador)
        End If

        _sHTML = ReplaceV(_sHTML, "@COD_PROVE", oProve.Codigo)
        _sHTML = ReplaceV(_sHTML, "@DEN_PROVE", oProve.Denominacion)
        _sHTML = ReplaceV(_sHTML, "@TFNO_CONTACTO", NothingToStr(oContacto.Telefono))
        _sHTML = ReplaceV(_sHTML, "@TFNO_MOVIL_CONTACTO", NothingToStr(oContacto.TelefonoMovil))
        _sHTML = ReplaceV(_sHTML, "@FAX_CONTACTO", NothingToStr(oContacto.Fax))
        _sHTML = ReplaceV(_sHTML, "@NOM_CONTACTO", NothingToStr(oContacto.Nombre))
        _sHTML = ReplaceV(_sHTML, "@APE_CONTACTO", NothingToStr(oContacto.Apellidos))
        Dim sProceso As String = oProceso.Anyo & "/" & oProceso.GMN1 & "/" & oProceso.Codigo
        _sHTML = ReplaceV(_sHTML, "@NUM_PROCESO", sProceso)
        _sHTML = ReplaceV(_sHTML, "@DEN_PROCESO", oProceso.Denominacion)
        _sHTML = ReplaceV(_sHTML, "@URL", URLWeb)
        Dim oCulture As New CultureInfo(oContacto.LanguageTag)
        Dim sFechaIni As String = CType(oProceso.FechaInicioSubasta, Date).ToString(oCulture.DateTimeFormat) & " (" & CType(Me.TzToUtc(oProceso.FechaInicioSubasta), Date).ToString(oCulture.DateTimeFormat) & " UTC)"
        Dim sFechaFin As String = CType(oProceso.FechaLimiteOfertas, Date).ToString(oCulture.DateTimeFormat) & " (" & CType(Me.TzToUtc(oProceso.FechaLimiteOfertas), Date).ToString(oCulture.DateTimeFormat) & " UTC)"
        _sHTML = ReplaceV(_sHTML, "@FECHAINISUB", sFechaIni)
        _sHTML = ReplaceV(_sHTML, "@FECHA_LIMITE", sFechaFin)
        If Not oComprador Is Nothing Then
            _sHTML = ReplaceV(_sHTML, "@NOM_COMPRADOR", NothingToStr(oComprador.Nombre))
            _sHTML = ReplaceV(_sHTML, "@APE_COMPRADOR", NothingToStr(oComprador.Apellidos))
            _sHTML = ReplaceV(_sHTML, "@TFNO_COMPRADOR", NothingToStr(oComprador.Telefono))
            _sHTML = ReplaceV(_sHTML, "@MAIL_COMPRADOR", NothingToStr(oComprador.EMail))
        Else
            _sHTML = ReplaceV(_sHTML, "@NOM_COMPRADOR", String.Empty)
            _sHTML = ReplaceV(_sHTML, "@APE_COMPRADOR", String.Empty)
            _sHTML = ReplaceV(_sHTML, "@TFNO_COMPRADOR", String.Empty)
            _sHTML = ReplaceV(_sHTML, "@MAIL_COMPRADOR", String.Empty)
        End If

        Return _sHTML
    End Function

    '<summary>Funcion que devuelve los datos en HTML de la notificación de un evento de subasta</summary>
    '<param name="sPlantilla">Ruta de la plantilla</param>
    '<param name="oProve">Datos del proveedor</param>
    '<param name="oContacto">Datos del contacto</param>
    '<param name="oProceso">Objeto de tipo Proceso con los datos del proceso</param>
    '<param name="URLWeb">URL de la web</param>  
    '<param name="DateFormat">Formato para fechas</param>  
    '<returns>Un texto con los datos a enviar</returns>
    '<remarks>Llamada desde: ComponerCorreoMuestraNotificacionSubasta, Tiempo máximo: 0,4 seg</remarks>

    Public Function NotificacionEventoSubasta(ByVal oProve As Proveedor, ByVal oContacto As Contacto, ByVal oProceso As Proceso, ByVal oEvento As GSServerModel.ProcesoSubastaEvento, _
                                              ByVal sDescEvento As String) As String
        Dim oProceRule As New GSServer.ProcesosRule(Me.Contexto)
        Dim oComprador As Comprador
        If NothingToStr(oProceso.Equipo) <> String.Empty And NothingToStr(oProceso.Comprador) <> String.Empty Then
            oComprador = oProceRule.DevolverComprador(oProceso.Equipo, oProceso.Comprador)
        End If

        Dim oCulture As New CultureInfo(oContacto.LanguageTag)

        _sHTML = ReplaceV(_sHTML, "@DESCEVENTO", sDescEvento)
        _sHTML = ReplaceV(_sHTML, "@FECEVE", CType(oEvento.Fecha, Date).ToString(oCulture.DateTimeFormat))
        _sHTML = ReplaceV(_sHTML, "@FECUTCEVE", CType(Me.TzToUtc(oEvento.Fecha), Date).ToString(oCulture.DateTimeFormat))
        _sHTML = ReplaceV(_sHTML, "@CODPROVE", oProve.Codigo)
        _sHTML = ReplaceV(_sHTML, "@DENPROVE", oProve.Denominacion)
        _sHTML = ReplaceV(_sHTML, "@NOM_CON", NothingToStr(oContacto.Nombre))
        _sHTML = ReplaceV(_sHTML, "@APE_CON", NothingToStr(oContacto.Apellidos))
        _sHTML = ReplaceV(_sHTML, "@TFNO1_CON", NothingToStr(oContacto.Telefono))
        _sHTML = ReplaceV(_sHTML, "@EMAIL_CON", NothingToStr(oContacto.EMail))
        _sHTML = ReplaceV(_sHTML, "@ANYO_PROCE", oProceso.Anyo)
        _sHTML = ReplaceV(_sHTML, "@GMN1_PROCE", oProceso.GMN1)
        _sHTML = ReplaceV(_sHTML, "@COD_PROCE", oProceso.Codigo)
        _sHTML = ReplaceV(_sHTML, "@DEN_PROCE", oProceso.Denominacion)
        _sHTML = ReplaceV(_sHTML, "@FEC_APE", CType(oProceso.FechaInicioSubasta, Date).ToString(oCulture.DateTimeFormat))
        _sHTML = ReplaceV(_sHTML, "@FEC_UTC_APE", CType(Me.TzToUtc(oProceso.FechaInicioSubasta), Date).ToString(oCulture.DateTimeFormat))
        _sHTML = ReplaceV(_sHTML, "@FEC_CIERRE", CType(oProceso.FechaLimiteOfertas, Date).ToString(oCulture.DateTimeFormat))
        _sHTML = ReplaceV(_sHTML, "@FEC_UTC_CIERRE", CType(Me.TzToUtc(oProceso.FechaLimiteOfertas), Date).ToString(oCulture.DateTimeFormat))
        If Not oComprador Is Nothing Then
            _sHTML = ReplaceV(_sHTML, "@NOM_COM", NothingToStr(oComprador.Nombre))
            _sHTML = ReplaceV(_sHTML, "@APE_COM", NothingToStr(oComprador.Apellidos))
            _sHTML = ReplaceV(_sHTML, "@EMAIL_COM", NothingToStr(oComprador.EMail))
            _sHTML = ReplaceV(_sHTML, "@TFNO1_COM", NothingToStr(oComprador.Telefono))
            _sHTML = ReplaceV(_sHTML, "@TFNO2_COM", NothingToStr(oComprador.Telefono2))
            _sHTML = ReplaceV(_sHTML, "@FAX_COM", NothingToStr(oComprador.Fax))
            _sHTML = ReplaceV(_sHTML, "@CARGO_COM", NothingToStr(oComprador.Car))
        Else
            _sHTML = ReplaceV(_sHTML, "@NOM_COM", String.Empty)
            _sHTML = ReplaceV(_sHTML, "@APE_COM", String.Empty)
            _sHTML = ReplaceV(_sHTML, "@EMAIL_COM", String.Empty)
            _sHTML = ReplaceV(_sHTML, "@TFNO1_COM", String.Empty)
            _sHTML = ReplaceV(_sHTML, "@TFNO2_COM", String.Empty)
            _sHTML = ReplaceV(_sHTML, "@FAX_COM", String.Empty)
            _sHTML = ReplaceV(_sHTML, "@CARGO_COM", String.Empty)
        End If

        Return _sHTML
    End Function

End Class
