﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Net
Imports Fullstep.FSNLibrary
Imports GSServerModel
Imports Fullstep.FSNLibrary.TiposDeDatos

<Serializable()> _
Public Class Notificar
    Inherits Root

    Private _sSMTPServer As String
    Private _iSMTPPort As Integer
    Private _iMailAuthentication As Integer
    Private _bBasicaGS As Boolean
    Private _sSMTPUser As String
    Private _sSMTPPwd As String
    Private _bSSL As Boolean
    Private _sDominio As String
    Private _iSmtpDeliveryMethod As Integer
    Private _pCrearConexionSMTP As Boolean
    Private _sSmtpCuentaServicio As String

    Private moLongitudesDeCodigos As TiposDeDatos.LongitudesDeCodigos

    Public Sub New(ByVal oCtx As GSServerModel.Contexto, Optional ByVal pCrearConexionSMTP As Boolean = True)
        MyBase.New(oCtx)
        GetEMailParams()
        _pCrearConexionSMTP = pCrearConexionSMTP
    End Sub

    Public Sub New(ByVal oCtx As GSServerModel.Contexto, ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean, _
                   Optional ByVal pCrearConexionSMTP As Boolean = True)
        MyBase.New(oCtx, remotting, UserCode, UserPassword, isAuthenticated)
        GetEMailParams()
        _pCrearConexionSMTP = pCrearConexionSMTP
    End Sub

    Private Sub GetEMailParams()
        Dim GSDBServer As New GSDatabaseServer.Procesos
        Dim dsParamEmail As DataSet = GSDBServer.DevolverParamConfigServicioEMail
        If Not dsParamEmail Is Nothing AndAlso dsParamEmail.Tables.Count > 0 AndAlso dsParamEmail.Tables(0).Rows.Count > 0 Then
            Dim drConfig As DataRow = dsParamEmail.Tables(0).Rows(0)

            _sSMTPServer = DBNullToStr(drConfig("SERVIDOR"))
            If Not drConfig.IsNull("PUERTO") Then _iSMTPPort = drConfig("PUERTO")
            If Not drConfig.IsNull("AUTENTICACION") Then _iMailAuthentication = drConfig("AUTENTICACION")
            _bSSL = SQLBinaryToBoolean(drConfig("SSL"))
            If Not drConfig.IsNull("BASICA_GS") Then _bBasicaGS = SQLBinaryToBoolean(drConfig("BASICA_GS"))
            _iSmtpDeliveryMethod = drConfig("METODO_ENTREGA")
            _sDominio = DBNullToStr(drConfig("DOMINIO"))
            If _iMailAuthentication = 1 Then
                _sSMTPUser = DBNullToStr(drConfig("USU"))
                _sSMTPPwd = Encrypter.Encrypt(drConfig.Item("PWD"), drConfig.Item("USU"), False, Encrypter.TipoDeUsuario.Persona, drConfig.Item("FECPWD"))
            ElseIf _iMailAuthentication = 2 Then
                _sSMTPUser = DBNullToStr(drConfig("CUENTAMAIL"))
                _sSMTPPwd = Encrypter.Encrypt(drConfig.Item("PWD"), drConfig.Item("CUENTAMAIL"), False, Encrypter.TipoDeUsuario.Persona, drConfig.Item("FECPWD"))
            Else
                _sSMTPUser = String.Empty
                _sSMTPPwd = String.Empty
            End If
            _sSmtpCuentaServicio = DBNullToStr(drConfig.Item("CUENTAMAIL"))
        End If
    End Sub

    Private Function ReplaceV(ByVal s1 As String, ByVal s2 As String, ByVal s3 As Object) As String
        ReplaceV = Replace(s1, s2, IIf(IsDBNull(s3), "", s3.ToString))
    End Function

    '<summary>Compone un correo de notificación de subasta por cada proveedor (contacto de subasta)</summary>
    '<param name=""></param>    
    '<remarks>Llamada desde: GSCUIProcesos, Tiempo máximo: 0,2 seg</remarks>

    Public Function ComponerCorreosNotificacionSubasta(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String, ByVal InsWEB As TiposDeDatos.TipoInstWeb, _
                                                       ByVal FSP_CIA As Integer, ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal DirPlantilla As String, _
                                                       ByVal sFrom As String, ByVal MailSubject As String, ByVal URLWeb As String) As DatosNotifSubasta
        Dim oDatos As New DatosNotifSubasta
        Dim oEMails As New GSServerModel.EMails

        Dim oProcRule As New GSServer.ProcesosRule(Me.Contexto)
        Dim oProceso As Proceso = oProcRule.DevolverProceso(Anyo, Codigo, GMN1, False)
        Dim oReglas As ProcesoSubastaReglas = oProcRule.DevolverProcesoSubastaReglas(Anyo, Codigo, GMN1)
        Dim oProceProves As ProcesoProveedores = oProcRule.DevolverSubastaProveedores(Anyo, Codigo, GMN1)
        Dim oProves As New Proveedores
        Dim oContactosComunicar = DevolverContactosProveedoresSubasta(oProceProves, InsWEB, FSP_CIA, FSP_SRV, FSP_BD, oProves)

        For Each oContacto As Contacto In oContactosComunicar
            Dim sNombrePlantilla As String = oContacto.Idioma & "_SubastaPetCartaWeb"
            If oContacto.TipoEMail = Fullstep.FSNLibrary.TiposDeDatos.TipoEmail.HTML Then
                sNombrePlantilla &= ".htm"
            Else
                sNombrePlantilla &= ".txt"
            End If

            'Obtener la plantilla correspondiente
            If Not DirPlantilla.EndsWith("\") Then DirPlantilla &= "\"
            Dim sPlantilla As String = DirPlantilla & sNombrePlantilla

            'Comprobamos que las plantillas son válidas        
            If System.IO.File.Exists(sPlantilla) Then
                Dim oEMail As New EMail

                oEMail.NombreEMail = oContacto.Proveedor
                oEMail.De = New Contacto
                oEMail.De.EMail = sFrom
                oEMail.Para.Add(oContacto)
                oEMail.TipoEmail = oContacto.TipoEMail
                oEMail.Asunto = ComponerAsuntoEMail(MailSubject, oProceso)

                If oContacto.TipoEMail = Fullstep.FSNLibrary.TiposDeDatos.TipoEmail.HTML Then
                    Dim oHTML As New HTML(Me.Contexto, sPlantilla)
                    oEMail.Contenido = oHTML.NotificacionSubasta(oProves.Item(oContacto.Proveedor), oContacto, oProceso, URLWeb)
                Else
                    Dim oTXT As New TXT(Me.Contexto, sPlantilla)
                    oEMail.Contenido = oTXT.NotificacionSubasta(oProves.Item(oContacto.Proveedor), oContacto, oProceso, URLWeb)
                End If

                'Adjuntos
                If Not oReglas Is Nothing AndAlso oReglas.Count > 0 Then
                    For Each oRegla As ProcesoSubastaRegla In oReglas
                        Dim oAdj As New File(oRegla.Nombre, oRegla.Nombre, oRegla.Comentario, oRegla.FecAlta, oRegla.TamañoArchivo)
                        oAdj.ID = oRegla.AdjunProce
                        oEMail.Adjuntos.Add(oAdj)
                    Next
                End If

                oEMails.Add(oEMail)
            End If
        Next

        oDatos.ContactosSubasta = oContactosComunicar
        oDatos.EMailsSubasta = oEMails

        Return oDatos
    End Function

    '<summary>Compone el subject de un correo </summary>
    '<param name="sSubject">Subject original</param>
    '<returns>String con el subject definitivo</returns>
    '<remarks>Llamada desde: ComponerMensaje; Tiempo máximo:0</remarks>   

    Private Function ComponerAsuntoEMail(ByVal sSubject As String, ByVal oProceso As GSServerModel.Proceso) As String
        Dim sAsunto As String = sSubject

        'Búsqueda y cambio de tags
        If sAsunto.ToUpper.Contains("[PROCECOD]") Then
            Dim iPos As Integer = sAsunto.ToUpper.IndexOf("[PROCECOD]")
            While iPos >= 0
                Dim sTag As String = sAsunto.Substring(iPos, 10)
                sAsunto = sAsunto.Replace(sTag, oProceso.Anyo.ToString & "/" & oProceso.GMN1 & "/" & oProceso.Codigo.ToString)
                iPos = sAsunto.ToUpper.IndexOf("[PROCECOD]", iPos + CType((oProceso.Anyo.ToString & "/" & oProceso.GMN1 & "/" & oProceso.Codigo.ToString), String).Length)
            End While
        End If
        If sAsunto.ToUpper.Contains("[PROCEDEN]") Then
            Dim iPos As Integer = sAsunto.ToUpper.IndexOf("[PROCEDEN]")
            While iPos >= 0
                Dim sTag As String = sAsunto.Substring(iPos, 10)
                sAsunto = sAsunto.Replace(sTag, oProceso.Denominacion)
                iPos = sAsunto.ToUpper.IndexOf("[PROCEDEN]", iPos + oProceso.Denominacion.Length)
            End While
        End If

        Return sAsunto
    End Function

    '<summary>Compone un correo de notificación de eventos subasta (el mismo para todos los proveedores)</summary>
    '<param name=""></param>    
    '<remarks>Llamada desde: GSCUIProcesos, Tiempo máximo: 0,2 seg</remarks>

    Public Function ComponerCorreosNotificacionEventoSubasta(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String, ByVal InsWEB As TiposDeDatos.TipoInstWeb, _
                                                       ByVal FSP_CIA As Integer, ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal DirPlantilla As String, _
                                                       ByVal sFrom As String, ByVal oEvento As GSServerModel.ProcesoSubastaEvento, ByVal oEveDesc As Dictionary(Of String, String)) As EMails
        Dim oEMails As New GSServerModel.EMails

        Dim oProcRule As New GSServer.ProcesosRule(Me.Contexto)
        Dim oProceso As Proceso = oProcRule.DevolverProceso(Anyo, Codigo, GMN1, False)
        Dim oProceProves As ProcesoProveedores = oProcRule.DevolverSubastaProveedores(Anyo, Codigo, GMN1)
        Dim oProves As New Proveedores
        Dim oContactosComunicar = DevolverContactosProveedoresSubasta(oProceProves, InsWEB, FSP_CIA, FSP_SRV, FSP_BD, oProves)

        For Each oContacto As Contacto In oContactosComunicar
            Dim sNombrePlantilla As String = oContacto.Idioma & "_NotifEveSubasta"
            If oContacto.TipoEMail = Fullstep.FSNLibrary.TiposDeDatos.TipoEmail.HTML Then
                sNombrePlantilla &= ".htm"
            Else
                sNombrePlantilla &= ".txt"
            End If

            'Obtener la plantilla correspondiente
            If Not DirPlantilla.EndsWith("\") Then DirPlantilla &= "\"
            Dim sPlantilla As String = DirPlantilla & sNombrePlantilla

            'Comprobamos que las plantillas son válidas        
            If System.IO.File.Exists(sPlantilla) Then
                Dim oEMail As New EMail

                oEMail.NombreEMail = oContacto.Proveedor
                oEMail.De = New Contacto
                oEMail.De.EMail = sFrom
                oEMail.Para.Add(oContacto)
                oEMail.TipoEmail = oContacto.TipoEMail

                Dim oTextoRule As New GSServer.TextosRule
                Dim sMailSubject As String = oTextoRule.DevolverTextoWEBTEXT(102, 1, oContacto.Idioma)
                sMailSubject = ReplaceV(sMailSubject, "@DESCEVENTO", oEveDesc(oContacto.Idioma))
                oEMail.Asunto = ComponerAsuntoEMail(sMailSubject, oProceso)

                If oContacto.TipoEMail = Fullstep.FSNLibrary.TiposDeDatos.TipoEmail.HTML Then
                    Dim oHTML As New HTML(Me.Contexto, sPlantilla)
                    oEMail.Contenido = oHTML.NotificacionEventoSubasta(oProves.Item(oContacto.Proveedor), oContacto, oProceso, oEvento, oEveDesc(oContacto.Idioma))
                Else
                    Dim oTXT As New TXT(Me.Contexto, sPlantilla)
                    oEMail.Contenido = oTXT.NotificacionEventoSubasta(oProves.Item(oContacto.Proveedor), oContacto, oProceso, oEvento, oEveDesc(oContacto.Idioma))
                End If

                oEMails.Add(oEMail)
            End If
        Next

        Return oEMails
    End Function

    ' <summary>Procedimiento que notifica la creación de un proceso de subasta</summary>
    ' <param name="Anyo">Anyo del proceso</param>
    ' <param name="Cod">Cod del proceso</param>
    ' <param name="GMN1">GMN1 del proceso</param>
    ' <param name="InsWEB">tipo de instalación web</param>
    ' <param name="FSP_CIA">Compañía Portal</param>
    ' <param name="FSP_SRV">Servidor portal</param>
    ' <param name="FSP_BD">BD Portal</param>
    ' <param name="oEMails">Mails a enviar</param>
    ' <remarks>Llamada desde: GSCUIProcesos, Tiempo máximo: 0,2 seg</remarks>

    Public Function NotificacionSubasta(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal InsWEB As TiposDeDatos.TipoInstWeb,
                                        ByVal FSP_CIA As Integer, ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal oDatos As DatosNotifSubasta, ByVal bUsarRemitenteEmpresa As Boolean) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = TiposDeDatos.ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim oProcRule As New GSServer.ProcesosRule(Me.Contexto)
        Dim oProceso As Proceso = oProcRule.DevolverProceso(Anyo, Cod, GMN1, False)
        Dim oSubasta As ProcesoSubasta = oProcRule.DevolverProcesoSubasta(Anyo, Cod, GMN1, False)
        Dim oProcesoDef As ProcesoDef = oProcRule.DevolverProcesoDef(Anyo, Cod, GMN1)

        'Creación de las peticiones
        Dim dtFecha As DateTime = Me.UtcToTz(Date.UtcNow)
        Dim oPets As New ProceProvePetOfertas
        For Each oContacto As Contacto In oDatos.ContactosSubasta
            Dim oPet As New GSServerModel.ProceProvePetOferta(oProceso.Anyo, oProceso.GMN1, oProceso.Codigo, oContacto.Proveedor, True, False, False, dtFecha,
                                                                oContacto.ID, oContacto.Apellidos, False, 0, oContacto.Nombre, , oContacto.EMail, oContacto.Telefono,
                                                                oContacto.Telefono2, oContacto.Fax, oContacto.TelefonoMovil)
            oPets.Add(oPet)
        Next

        'Comprobacion de atributo de epecificacion obligatorios con validacion en la publicacion 
        If Not oProcesoDef.ValAtrEsp Then
            Dim oVal As GSServerModel.ValidacionAtributos = oProcRule.ValidarAtributosEspObligatorios(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, TiposDeDatos.TValidacionAtrib.Publicacion)
            If Not oVal.AtributosCorrectos Then
                oError.Number = TiposDeDatos.ErroresGS.TESAtribEspObl
                oError.ErrorId = TiposDeDatos.TValidacionAtrib.Publicacion

                Dim sCadena As String = String.Empty
                For Each oAtribEsp As ProcesoAtributoEspecificacion In oVal.AtributosProceso
                    sCadena &= oAtribEsp.Denominacion
                    sCadena &= " @PROCESO" & Environment.NewLine
                Next
                For Each oAtribEsp As ProcesoGrupoAtributoEspecificacion In oVal.AtributosPorcesoGrupo
                    sCadena &= " @GRUPO" & Environment.NewLine
                Next
                For Each oAtribEsp As ItemAtributoEspecificacion In oVal.AtributosItem
                    sCadena &= " @ITEM" & Environment.NewLine
                Next
                oError.ErrorStr = sCadena

                Return oError
            End If
        End If

        'Crea una instancia de la clase Email pasándole la configuración del servidor de correo para los emails que se envíen desde la instancia de notificar.
        Dim oMyEMail As New Fullstep.FSNLibraryCOM.Email(_sSMTPServer, _iSMTPPort, _iMailAuthentication, _bSSL, _iSmtpDeliveryMethod, _sSMTPUser, _sSMTPPwd, _sDominio, True)

        ' Recorremos las peticiones para enviar un mail  
        Dim bObtenerAdjuntos As Boolean = True
        Dim sPathAdjuntos As String = String.Empty
        Dim sTemp As String = System.IO.Path.GetTempPath
        For Each oEMail As GSServerModel.EMail In oDatos.EMailsSubasta
            ' DATOS de COMPRADORES            
            'Dim oAsignaciones As GSServerModel.Asignaciones = oProcRule.DevolverAsignacionesProceso(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, , , , oPet.CodigoProveedor)
            'Dim oCompradores As GSServerModel.Compradores = oEqp.DevolverCompradoresEquipo(oAsignaciones.Item(1).CodigoEquipo, 1, oAsignaciones.Item(1).CodigoComp)

            Dim oPet As ProceProvePetOferta = oPets.Item(oEMail.NombreEMail)    'El código del proveedor es el nombre del email

            'Guardar los adjuntos en el dir. temporal. 
            'Sólo la primera vez. Suponemos que todos los emails llevan los mismos adjuntos            
            If bObtenerAdjuntos Then
                If Not oEMail.Adjuntos Is Nothing AndAlso oEMail.Adjuntos.Count > 0 Then
                    'Crear un dir. temporal
                    Dim sRootFolder As String = System.Configuration.ConfigurationManager.AppSettings("UploadFolder")
                    Dim sTempFolder As String = GenerateRandomPath()
                    sPathAdjuntos = sRootFolder
                    If Not sPathAdjuntos.EndsWith("\") Then sPathAdjuntos &= "\"
                    sPathAdjuntos &= sTempFolder & "\"
                    If Not System.IO.Directory.CreateDirectory(sPathAdjuntos) Is Nothing Then
                        For Each oAdj In oEMail.Adjuntos
                            GuardarArchivoSubastaRegla(sPathAdjuntos, oAdj.NombreArchivo, oAdj.ID)
                        Next
                    End If
                End If

                bObtenerAdjuntos = False
            End If

            Dim sPara As String = String.Empty
            For Each oPara As Contacto In oEMail.Para
                sPara &= oPara.EMail & ";"
            Next
            Dim sCC As String = String.Empty
            For Each oCC As Contacto In oEMail.CC
                sCC &= oCC.EMail & ";"
            Next
            Dim sBCC As String = String.Empty
            For Each oBCC As Contacto In oEMail.BCC
                sBCC &= oBCC.EMail & ";"
            Next
            Dim sAdjuntos As String = String.Empty
            If Not oEMail.Adjuntos Is Nothing AndAlso oEMail.Adjuntos.Count > 0 AndAlso NothingToStr(sPathAdjuntos) <> String.Empty Then
                For Each oAdj As GSServerModel.File In oEMail.Adjuntos
                    sAdjuntos &= oAdj.NombreArchivo & ";"
                Next
            End If

            Dim iError As Integer
            Dim sRemitente As String
            sRemitente = _sSmtpCuentaServicio
            If bUsarRemitenteEmpresa Then
                sRemitente = oEMail.De.EMail
            End If
            oMyEMail.EnviarMail(sRemitente, oEMail.De.EMail, oEMail.Asunto, sPara, oEMail.Contenido, , , sCC, sBCC, sAdjuntos, sPathAdjuntos, (oEMail.TipoEmail = TipoEmail.HTML), , , , , iError)

            'Registrar acción
            If iError = 2 Then
                Dim sUsuCod As String = String.Empty
                If Not Me.Contexto Is Nothing Then
                    sUsuCod = Me.Contexto.UsuCod
                End If

                Dim oGestSeg As New GSDatabaseServer.GestorSeguridad
                oGestSeg.RegistrarAccion(sUsuCod, TiposDeDatos.AccionesGS.ACCNotifSubasta, "Anyo:" & Anyo.ToString & " GMN1:" & GMN1 & " Proce:" & Cod.ToString & " Prove:" & oPet.CodigoProveedor & " Con:" & oPet.ApellidoCon)
            End If
        Next

        oMyEMail.FuerzaFinalizeSmtpClient()

        'Borrar los adjuntos
        'If NothingToStr(sPathAdjuntos) <> String.Empty Then
        '    If IO.Directory.Exists(sPathAdjuntos) Then
        '        IO.Directory.Delete(sPathAdjuntos, True)
        '    End If
        'End If

        ' Almacenamos las peticiones en la BD            
        oProcesoDef.ValAtrEsp = True
        oSubasta.FechaUltPub = dtFecha
        'oError = oProcRule.ActualizarProceso(oProceso)        
        'oError = oProcRule.ActualizarProcesoDef(oProcesoDef)
        oError = oProcRule.ActualizarProcesoSubasta(oProceso, oProcesoDef, oSubasta, Nothing, Nothing, Nothing)
        If oError.Number = TiposDeDatos.ErroresGS.TESnoerror Then
            oError = oProcRule.InsertarPeticionesProceso(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, InsWEB, FSP_CIA, oPets)
        End If

        Return oError
    End Function

    ' <summary>Procedimiento que notifica la creación de un proceso de subasta</summary>
    ' <param name=""></param>    
    ' <param name="oEMail">Mails a enviar</param>
    ' <remarks>Llamada desde: GSCUIProcesos, Tiempo máximo: 0,2 seg</remarks>

    Public Function NotificacionEventoSubasta(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oEMails As GSServerModel.EMails) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = TiposDeDatos.ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        'Crea una instancia de la clase Email pasándole la configuración del servidor de correo para los emails que se envíen desde la instancia de notificar.
        Dim oMyEMail As New Fullstep.FSNLibraryCOM.Email(_sSMTPServer, _iSMTPPort, _iMailAuthentication, _bSSL, _iSmtpDeliveryMethod, _sSMTPUser, _sSMTPPwd, _sDominio, True)

        'Enviamos los correos                 
        For Each oEMail As GSServerModel.EMail In oEMails
            Dim sPara As String = String.Empty
            For Each oPara As Contacto In oEMail.Para
                sPara &= oPara.EMail & ";"
            Next
            Dim sCC As String = String.Empty
            For Each oCC As Contacto In oEMail.CC
                sCC &= oCC.EMail & ";"
            Next
            Dim sBCC As String = String.Empty
            For Each oBCC As Contacto In oEMail.BCC
                sBCC &= oBCC.EMail & ";"
            Next

            Dim iError As Integer

            oMyEMail.EnviarMail(_sSmtpCuentaServicio, oEMail.De.EMail, oEMail.Asunto, sPara, oEMail.Contenido, , , sCC, sBCC, , , (oEMail.TipoEmail = TipoEmail.HTML), , , , , iError)

            'Registrar acción
            If iError = 2 Then
                Dim sUsuCod As String = String.Empty
                If Not Me.Contexto Is Nothing Then
                    sUsuCod = Me.Contexto.UsuCod
                End If

                Dim oGestSeg As New GSDatabaseServer.GestorSeguridad
                oGestSeg.RegistrarAccion(sUsuCod, TiposDeDatos.AccionesGS.ACCNotifEventoSubasta, "Anyo:" & Anyo.ToString & " GMN1:" & GMN1 & " Proce:" & Cod.ToString & " Prove:" & oEMail.NombreEMail)
            End If
        Next

        oMyEMail.FuerzaFinalizeSmtpClient()

        Return oError
    End Function

#Region " Métodos privados "

    '<summary>Devuelve los contactos de los proveedores de una subasta a los que enviar emails</summary>
    '<param name="Anyo">Anyo</param>    
    '<param name="Codigo">Codigo</param>
    '<param name="GMN1">GMN1</param>
    '<param name="InsWEB">Tipo de instalación web</param>
    '<param name="FSP_CIA">Compañía</param>
    '<param name="FSP_SRV">Servidor de BD del Portal</param>
    '<param name="FSP_BD">BD del porta</param>
    '<remarks>Llamada desde: GSCUIProcesos, Tiempo máximo: 0,2 seg</remarks>

    Private Function DevolverContactosProveedoresSubasta(ByVal oProceProves As ProcesoProveedores, ByVal InsWEB As TiposDeDatos.TipoInstWeb, _
                                                         ByVal FSP_CIA As Integer, ByVal FSP_SRV As String, ByVal FSP_BD As String, ByRef oProves As Proveedores) As Contactos
        Dim oContactosComunicar As New Contactos

        Dim bComunicar As Boolean
        For Each oProceProve As ProcesoProveedor In oProceProves
            Dim oContactosProve As New Contactos

            Dim oProve As New Proveedor(oProceProve.Proveedor, oProceProve.Denominacion)
            oProves.Add(oProve)
            Dim oProveRules As New GSServer.ProveedoresRule(Me.Contexto)
            'Obtener contactos de subasta
            Dim oContactos As Contactos = oProveRules.DevolverProveedorContactos(oProve.Codigo, True)
            If oContactos Is Nothing OrElse oContactos.Count = 0 Then
                'Obtener contactos que reciben peticiones de oferta
                oContactos = oProveRules.DevolverProveedorContactos(oProve.Codigo, , True)
                If oContactos Is Nothing OrElse oContactos.Count = 0 Then
                    'Obtener cualquier contacto
                    oContactos = oProveRules.DevolverProveedorContactos(oProve.Codigo)
                    If Not oContactos Is Nothing AndAlso oContactos.Count > 0 Then
                        'Obtener cualquier contacto con email   
                        For Each oContacto As Contacto In oContactos
                            If Not oContacto.EMail Is Nothing Then
                                oContactosProve.Add(oContacto)
                                Exit For
                            End If
                        Next
                    End If
                Else
                    'Obtener cualquier contacto con email     
                    For Each oContacto As Contacto In oContactos
                        If Not oContacto.EMail Is Nothing Then
                            oContactosProve.Add(oContacto)
                            Exit For
                        End If
                    Next
                End If
            Else
                oContactosProve = oContactos
            End If

            'Comprobar que el email existe en los usuarios del portal 
            bComunicar = True
            If InsWEB = TiposDeDatos.TipoInstWeb.ConPortal Then
                Dim oAut As EstadoProveedorPortal = oProveRules.DevolverEstadoProveedorPortal(oProve.Codigo, FSP_CIA)
                If Not oAut Is Nothing Then bComunicar = oAut.Autorizado
            End If

            If bComunicar Then
                For Each oContacto As Contacto In oContactosProve
                    If NothingToStr(oContacto.EMail) <> String.Empty And oContacto.Port Then
                        If oProveRules.ComprobarEmailEnPortal(Fullstep.FSNLibrary.NothingToStr(oProceProve.CodigoPortal), Fullstep.FSNLibrary.NothingToStr(oContacto.EMail), FSP_SRV, FSP_BD) Then
                            If Not oContactosComunicar.Contains(oContacto) Then oContactosComunicar.Add(oContacto)
                        End If
                    End If
                Next
            End If
        Next

        Return oContactosComunicar
    End Function

    '<summary>Guarda el archivo de regla indicado en el directorio indicado</summary>
    '<param name="DownloadFolder">Carpeta destino</param>    
    '<param name="FileName">Nombre del archivo</param>    
    '<param name="ID">ID del archivo</param>        
    '<returns>Boolean indicando si el proceso ha finalizado correctamente</returns>

    Private Function GuardarArchivoSubastaRegla(ByVal DownloadFolder As String, ByVal FileName As String, ByVal ID As Integer) As Boolean
        GuardarArchivoSubastaRegla = False

        Dim oFS As New System.IO.FileStream(DownloadFolder & FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write)
        Dim oProcRule As New GSServer.ProcesosRule(Me.Contexto)

        Dim iOffset As Integer = 0
        Dim Buffer() As Byte = oProcRule.DevolverArchivoSubastaRegla(iOffset, 2000000, ID)
        While Buffer.Length > 0
            oFS.Write(Buffer, 0, Buffer.Length)

            iOffset += Buffer.Length
            Buffer = oProcRule.DevolverArchivoSubastaRegla(iOffset, 2000000, ID)
        End While

        oFS.Close()
        oFS.Dispose()
        oFS = Nothing

        GuardarArchivoSubastaRegla = True
    End Function

#End Region

End Class
