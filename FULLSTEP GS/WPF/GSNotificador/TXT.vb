﻿Imports Fullstep.FSNLibrary
Imports GSServerModel
Imports System.Globalization

Public Class TXT
    Inherits GSServer.RuleBase

    Private _sTXT As String

    Public Sub New(ByVal oCtx As GSServerModel.Contexto, ByVal Plantilla As String)
        MyBase.New(oCtx)

        Dim oStream As New System.IO.StreamReader(Plantilla, System.Text.Encoding.Default, True)
        _sTXT = oStream.ReadToEnd()
        oStream.Close()
    End Sub

    '<summary>Funcion que devuelve el texto con la notificación de la anulación de una solicitud</summary>
    '<param name="sPlantilla">Ruta de la plantilla</param>
    '<param name="oProve">Datos del proveedor</param>
    '<param name="oContacto">Datos del contacto</param>
    '<param name="oProceso">Objeto de tipo Proceso con los datos del proceso</param>
    '<param name="URLWeb">URL de la web</param>  
    '<param name="DateFormat">Formato para fechas</param>  
    '<returns>Un string con el texto que va a enviar</returns>
    '<remarks>Llamada desde: ComponerCorreoMuestraNotificacionSubasta, Tiempo máximo: 0,3 seg</remarks>

    Public Function NotificacionSubasta(ByVal oProve As Proveedor, ByVal oContacto As Contacto, ByVal oProceso As Proceso, ByVal URLWeb As String) As String
        Dim oProceRule As New GSServer.ProcesosRule(Me.Contexto)
        Dim oComprador As Comprador
        If NothingToStr(oProceso.Equipo) <> String.Empty And NothingToStr(oProceso.Comprador) <> String.Empty Then
            oComprador = oProceRule.DevolverComprador(oProceso.Equipo, oProceso.Comprador)
        End If

        _sTXT = ReplaceV(_sTXT, "@COD_PROVE", oProve.Codigo)
        _sTXT = ReplaceV(_sTXT, "@DEN_PROVE", oProve.Denominacion)
        _sTXT = ReplaceV(_sTXT, "@TFNO_CONTACTO", NothingToStr(oContacto.Telefono))
        _sTXT = ReplaceV(_sTXT, "@TFNO_MOVIL_CONTACTO", NothingToStr(oContacto.TelefonoMovil))
        _sTXT = ReplaceV(_sTXT, "@FAX_CONTACTO", NothingToStr(oContacto.Fax))
        _sTXT = ReplaceV(_sTXT, "@NOM_CONTACTO", NothingToStr(oContacto.Nombre))
        _sTXT = ReplaceV(_sTXT, "@APE_CONTACTO", NothingToStr(oContacto.Apellidos))
        Dim sProceso As String = oProceso.Anyo & "/" & oProceso.GMN1 & "/" & oProceso.Codigo
        _sTXT = ReplaceV(_sTXT, "@NUM_PROCESO", sProceso)
        _sTXT = ReplaceV(_sTXT, "@DEN_PROCESO", oProceso.Denominacion)
        _sTXT = ReplaceV(_sTXT, "@URL", URLWeb)
        Dim oCulture As New CultureInfo(oContacto.LanguageTag)
        Dim sFechaIni As String = CType(oProceso.FechaInicioSubasta, Date).ToString(oCulture.DateTimeFormat) & " (" & CType(Me.TzToUtc(oProceso.FechaInicioSubasta), Date).ToString(oCulture.DateTimeFormat) & " UTC)"
        Dim sFechaFin As String = CType(oProceso.FechaLimiteOfertas, Date).ToString(oCulture.DateTimeFormat) & " (" & CType(Me.TzToUtc(oProceso.FechaLimiteOfertas), Date).ToString(oCulture.DateTimeFormat) & " UTC)"
        _sTXT = ReplaceV(_sTXT, "@FECHAINISUB", sFechaIni)
        _sTXT = ReplaceV(_sTXT, "@FECHA_LIMITE", sFechaFin)
        If Not oComprador Is Nothing Then
            _sTXT = ReplaceV(_sTXT, "@NOM_COMPRADOR", NothingToStr(oComprador.Nombre))
            _sTXT = ReplaceV(_sTXT, "@APE_COMPRADOR", NothingToStr(oComprador.Apellidos))
            _sTXT = ReplaceV(_sTXT, "@TFNO_COMPRADOR", NothingToStr(oComprador.Telefono))
            _sTXT = ReplaceV(_sTXT, "@MAIL_COMPRADOR", NothingToStr(oComprador.EMail))
            _sTXT = ReplaceV(_sTXT, "@TFNO2_COMPRADOR", NothingToStr(oComprador.Telefono2))
            _sTXT = ReplaceV(_sTXT, "@FAX_COMPRADOR", NothingToStr(oComprador.Fax))
            _sTXT = ReplaceV(_sTXT, "@CARGO_COMPRADOR", NothingToStr(oComprador.Car))
        Else
            _sTXT = ReplaceV(_sTXT, "@NOM_COMPRADOR", String.Empty)
            _sTXT = ReplaceV(_sTXT, "@APE_COMPRADOR", String.Empty)
            _sTXT = ReplaceV(_sTXT, "@TFNO_COMPRADOR", String.Empty)
            _sTXT = ReplaceV(_sTXT, "@MAIL_COMPRADOR", String.Empty)
            _sTXT = ReplaceV(_sTXT, "@TFNO2_COMPRADOR", String.Empty)
            _sTXT = ReplaceV(_sTXT, "@FAX_COMPRADOR", String.Empty)
            _sTXT = ReplaceV(_sTXT, "@CARGO_COMPRADOR", String.Empty)
        End If

        Return _sTXT
    End Function

    '<summary>Funcion que devuelve el texto con la notificación de la anulación de una solicitud</summary>
    '<param name="sPlantilla">Ruta de la plantilla</param>
    '<param name="oProve">Datos del proveedor</param>
    '<param name="oContacto">Datos del contacto</param>
    '<param name="oProceso">Objeto de tipo Proceso con los datos del proceso</param>
    '<param name="URLWeb">URL de la web</param>  
    '<param name="DateFormat">Formato para fechas</param>  
    '<returns>Un string con el texto que va a enviar</returns>
    '<remarks>Llamada desde: ComponerCorreoMuestraNotificacionSubasta, Tiempo máximo: 0,3 seg</remarks>

    Public Function NotificacionEventoSubasta(ByVal oProve As Proveedor, ByVal oContacto As Contacto, ByVal oProceso As Proceso, ByVal oEvento As GSServerModel.ProcesoSubastaEvento, _
                                              ByVal sDescEvento As String) As String
        Dim oProceRule As New GSServer.ProcesosRule(Me.Contexto)
        Dim oComprador As Comprador
        If NothingToStr(oProceso.Equipo) <> String.Empty And NothingToStr(oProceso.Comprador) <> String.Empty Then
            oComprador = oProceRule.DevolverComprador(oProceso.Equipo, oProceso.Comprador)
        End If

        Dim oCulture As New CultureInfo(oContacto.LanguageTag)

        _sTXT = ReplaceV(_sTXT, "@DESCEVENTO", sDescEvento)
        _sTXT = ReplaceV(_sTXT, "@FECEVE", CType(oEvento.Fecha, Date).ToString(oCulture.DateTimeFormat))
        _sTXT = ReplaceV(_sTXT, "@FECUTCEVE", CType(Me.TzToUtc(oEvento.Fecha), Date).ToString(oCulture.DateTimeFormat))
        _sTXT = ReplaceV(_sTXT, "@CODPROVE", oProve.Codigo)
        _sTXT = ReplaceV(_sTXT, "@DENPROVE", oProve.Denominacion)
        _sTXT = ReplaceV(_sTXT, "@NOM_CON", NothingToStr(oContacto.Nombre))
        _sTXT = ReplaceV(_sTXT, "@APE_CON", NothingToStr(oContacto.Apellidos))
        _sTXT = ReplaceV(_sTXT, "@TFNO1_CON", NothingToStr(oContacto.Telefono))
        _sTXT = ReplaceV(_sTXT, "@EMAIL_CON", NothingToStr(oContacto.EMail))
        _sTXT = ReplaceV(_sTXT, "@ANYO_PROCE", oProceso.Anyo)
        _sTXT = ReplaceV(_sTXT, "@GMN1_PROCE", oProceso.GMN1)
        _sTXT = ReplaceV(_sTXT, "@COD_PROCE", oProceso.Codigo)
        _sTXT = ReplaceV(_sTXT, "@DEN_PROCE", oProceso.Denominacion)
        _sTXT = ReplaceV(_sTXT, "@FEC_APE", CType(oProceso.FechaInicioSubasta, Date).ToString(oCulture.DateTimeFormat))
        _sTXT = ReplaceV(_sTXT, "@FEC_UTC_APE", CType(Me.TzToUtc(oProceso.FechaInicioSubasta), Date).ToString(oCulture.DateTimeFormat))
        _sTXT = ReplaceV(_sTXT, "@FEC_CIERRE", CType(oProceso.FechaLimiteOfertas, Date).ToString(oCulture.DateTimeFormat))
        _sTXT = ReplaceV(_sTXT, "@FEC_UTC_CIERRE", CType(Me.TzToUtc(oProceso.FechaLimiteOfertas), Date).ToString(oCulture.DateTimeFormat))
        If Not oComprador Is Nothing Then
            _sTXT = ReplaceV(_sTXT, "@NOM_COM", NothingToStr(oComprador.Nombre))
            _sTXT = ReplaceV(_sTXT, "@APE_COM", NothingToStr(oComprador.Apellidos))
            _sTXT = ReplaceV(_sTXT, "@EMAIL_COM", NothingToStr(oComprador.EMail))
            _sTXT = ReplaceV(_sTXT, "@TFNO1_COM", NothingToStr(oComprador.Telefono))
        Else
            _sTXT = ReplaceV(_sTXT, "@NOM_COM", String.Empty)
            _sTXT = ReplaceV(_sTXT, "@APE_COM", String.Empty)
            _sTXT = ReplaceV(_sTXT, "@EMAIL_COM", String.Empty)
            _sTXT = ReplaceV(_sTXT, "@TFNO1_COM", String.Empty)
        End If

        Return _sTXT
    End Function

End Class
