﻿Public Class UnidadesRule

    Private mbGrabarLog As Boolean

    Public Property Log() As Boolean
        Get
            Log = mbGrabarLog
        End Get
        Set(ByVal value As Boolean)
            mbGrabarLog = value
        End Set
    End Property

    Public Function CargarTodasLasUnidades(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CargarTodasLasUnidades(bMostrarIntegracion, sIdioma)

    End Function


    Public Function ActualizarUnidad(ByRef Unidad As GSServerModel.Unidad, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim o As New Object
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        mbGrabarLog = GrabarEnLog(Unidad, bActivLog, bIntegracionUnidades)
        Return GSDBServer.ActualizarUnidad(Unidad, bRealizarCambioCodigo, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function AnyadirUnidad(ByRef oUnidad As GSServerModel.Unidad, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim o As New Object
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        mbGrabarLog = GrabarEnLog(oUnidad, bActivLog, bIntegracionUnidades)
        Return GSDBServer.AnyadirUnidad(oUnidad, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function EliminarUnidades(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet
        Dim Unidad As New GSServerModel.Unidad(False, "", "", True)
        mbGrabarLog = GrabarEnLog(Unidad, bActivLog, bIntegracionUnidades)
        Return GSDBServer.EliminarUnidades(Codigos, mbGrabarLog, FSP, Unidad.Origen, sUsuario)


    End Function

    Public Function CambioCodigoUnidad(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CambioCodigoUnidad(Codigo, CodigoNuevo, Usuario)
    End Function

    Private Function GrabarEnLog(ByRef Unidad As GSServerModel.Unidad, ByVal bActivLog As Boolean, ByVal bIntegracionUnidades As Boolean) As Boolean
        '***********************************************************************************
        '*** Descripción: Nos indica si hay que grabar o no el cambio en el log de       ***
        '***              monedas (tabla LOG_MON) y carga en la variable                 ***
        '***              del módulo m_udtOrigen el origen del movimiento                ***
        '*** Parametros:  Ninguno                                                        ***
        '*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
        '***********************************************************************************

        If bActivLog Or bIntegracionUnidades Then
            If bActivLog And bIntegracionUnidades Then
                Unidad.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSIntReg
            Else
                If bActivLog Then
                    Unidad.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSReg
                Else
                    Unidad.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSInt
                End If
            End If
            GrabarEnLog = True
        Else
            GrabarEnLog = False
        End If

    End Function

End Class
