﻿Public Class PaisesRule

    Private mbGrabarLog As Boolean

    Private mbGrabarLogProvincia As Boolean

    Public Property Log() As Boolean
        Get
            Log = mbGrabarLog
        End Get
        Set(ByVal value As Boolean)
            mbGrabarLog = value
        End Set
    End Property

    Public Property Logprovincia() As Boolean
        Get
            Log = mbGrabarLogProvincia
        End Get
        Set(ByVal value As Boolean)
            mbGrabarLogProvincia = value
        End Set
    End Property


    Public Function CargarTodosLosPaisesProvincias(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CargarTodasLosPaisesProvincias(bMostrarIntegracion, sIdioma)

    End Function

    Public Function CargarTodasLasMonedasPais(ByVal sIdioma As String) As System.Data.DataSet
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CargarTodasLasMonedasPais(sIdioma)

    End Function

    Public Function AnyadirPais(ByRef Pais As GSServerModel.Pais, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet
        mbGrabarLog = GrabarEnLog(Pais, bActivLog, bIntegracionPaises)
        Return GSDBServer.AnyadirPais(Pais, iInstWeb, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function ActualizarPais(ByRef Pais As GSServerModel.Pais, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet
        mbGrabarLog = GrabarEnLog(Pais, bActivLog, bIntegracionPaises)
        Return GSDBServer.ActualizarPais(Pais, bRealizarCambioCodigo, iInstWeb, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function CambioCodigoPais(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CambioCodigoPais(Codigo, CodigoNuevo, Usuario)
    End Function


    Public Function AnyadirProvincia(ByRef Provincia As GSServerModel.Provincia, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet
        mbGrabarLog = GrabarEnLogProvincia(Provincia, bActivLog, bIntegracionPaises)
        Return GSDBServer.AnyadirProvincia(Provincia, iInstWeb, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function ActualizarProvincia(ByRef Provincia As GSServerModel.Provincia, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet
        mbGrabarLog = GrabarEnLogProvincia(Provincia, bActivLog, bIntegracionPaises)
        Return GSDBServer.ActualizarProvincia(Provincia, bRealizarCambioCodigo, iInstWeb, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function CambioCodigoProvincia(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String, ByVal sPais As String) As Integer

        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CambioCodigoProvincia(Codigo, CodigoNuevo, Usuario, sPais)
    End Function


    Public Function EliminarProvincias(ByVal CodigosProvincias() As String, ByVal CodigosPaisProvi() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String, ByVal sIdioma As String) As GSServerModel.GSException
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet
        'Dim Moneda As New GSServerModel.Moneda(False, "", "", True)
        mbGrabarLog = True
        'mbGrabarLog = GrabarEnLog(Moneda, bActivLog, bIntegracionMonedas)
        'Return GSDBServer.EliminarPaisesProvincias(CodigosPaises, CodigosProvincias, iInstWeb, mbGrabarLog, FSP, Moneda.Origen, sUsuario)
        Return GSDBServer.EliminarProvincias(CodigosProvincias, CodigosPaisProvi, iInstWeb, mbGrabarLog, FSP, 2, sUsuario, sIdioma)


    End Function

    Public Function EliminarPaises(ByVal CodigosPaises() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean, ByVal FSP As String, ByVal sUsuario As String, ByVal sIdioma As String) As GSServerModel.GSException
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet
        'Dim Moneda As New GSServerModel.Moneda(False, "", "", True)
        'mbGrabarLog = GrabarEnLog(Moneda, bActivLog, bIntegracionMonedas)
        'Return GSDBServer.EliminarPaisesProvincias(CodigosPaises, CodigosProvincias, iInstWeb, mbGrabarLog, FSP, Moneda.Origen, sUsuario)
        Return GSDBServer.EliminarPaises(CodigosPaises, iInstWeb, mbGrabarLog, FSP, 2, sUsuario, sIdioma)

    End Function


    Private Function GrabarEnLog(ByRef Pais As GSServerModel.Pais, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean) As Boolean
        '***********************************************************************************
        '*** Descripción: Nos indica si hay que grabar o no el cambio en el log de       ***
        '***              monedas (tabla LOG_MON) y carga en la variable                 ***
        '***              del módulo m_udtOrigen el origen del movimiento                ***
        '*** Parametros:  Ninguno                                                        ***
        '*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
        '***********************************************************************************

        If bActivLog Or bIntegracionPaises Then
            If bActivLog And bIntegracionPaises Then
                Pais.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSIntReg
            Else
                If bActivLog Then
                    Pais.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSReg
                Else
                    Pais.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSInt
                End If
            End If
            GrabarEnLog = True
        Else
            GrabarEnLog = False
        End If

    End Function


    Private Function GrabarEnLogProvincia(ByRef Provincia As GSServerModel.Provincia, ByVal bActivLog As Boolean, ByVal bIntegracionPaises As Boolean) As Boolean
        '***********************************************************************************
        '*** Descripción: Nos indica si hay que grabar o no el cambio en el log de       ***
        '***              monedas (tabla LOG_MON) y carga en la variable                 ***
        '***              del módulo m_udtOrigen el origen del movimiento                ***
        '*** Parametros:  Ninguno                                                        ***
        '*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
        '***********************************************************************************

        If bActivLog Or bIntegracionPaises Then
            If bActivLog And bIntegracionPaises Then
                Provincia.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSIntReg
            Else
                If bActivLog Then
                    Provincia.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSReg
                Else
                    Provincia.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSInt
                End If
            End If
            GrabarEnLogProvincia = True
        Else
            GrabarEnLogProvincia = False
        End If

    End Function

End Class
