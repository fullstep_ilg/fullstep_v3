﻿Public Class ViasPagoRule

    Private mbGrabarLog As Boolean

    Public Property Log() As Boolean
        Get
            Log = mbGrabarLog
        End Get
        Set(ByVal value As Boolean)
            mbGrabarLog = value
        End Set
    End Property

    Public Function CargarTodasLasViasPago(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CargarTodasLasViasPago(bMostrarIntegracion, sIdioma)

    End Function


    Public Function ActualizarViaPago(ByRef oViaPago As GSServerModel.ViaPago, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim o As New Object
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        mbGrabarLog = GrabarEnLog(oViaPago, bActivLog, bIntegracionViasPago)
        Return GSDBServer.ActualizarViaPago(oViaPago, bRealizarCambioCodigo, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function AnyadirViaPago(ByRef oViaPago As GSServerModel.ViaPago, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim o As New Object
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        mbGrabarLog = GrabarEnLog(oViaPago, bActivLog, bIntegracionViasPago)
        Return GSDBServer.AnyadirViaPago(oViaPago, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function EliminarViasPago(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet
        Dim ViaPago As New GSServerModel.ViaPago(False, "", "", True)
        mbGrabarLog = GrabarEnLog(ViaPago, bActivLog, bIntegracionViasPago)
        Return GSDBServer.EliminarViasPago(Codigos, mbGrabarLog, FSP, ViaPago.Origen, sUsuario)


    End Function

    Public Function CambioCodigoViaPago(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CambioCodigoViaPago(Codigo, CodigoNuevo, Usuario)
    End Function

    Private Function GrabarEnLog(ByRef ViaPago As GSServerModel.ViaPago, ByVal bActivLog As Boolean, ByVal bIntegracionViasPago As Boolean) As Boolean
        '***********************************************************************************
        '*** Descripción: Nos indica si hay que grabar o no el cambio en el log de       ***
        '***              monedas (tabla LOG_MON) y carga en la variable                 ***
        '***              del módulo m_udtOrigen el origen del movimiento                ***
        '*** Parametros:  Ninguno                                                        ***
        '*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
        '***********************************************************************************

        If bActivLog Or bIntegracionViasPago Then
            If bActivLog And bIntegracionViasPago Then
                ViaPago.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSIntReg
            Else
                If bActivLog Then
                    ViaPago.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSReg
                Else
                    ViaPago.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSInt
                End If
            End If
            GrabarEnLog = True
        Else
            GrabarEnLog = False
        End If

    End Function

End Class
