﻿Imports System
Imports System.Data.SqlClient
Imports System.Exception
Imports System.Collections.ObjectModel
'Imports System.ServiceModel
Imports Fullstep.FSNLibrary
Imports GSDatabaseServer


Public Class Monedas
    'Inherits ObservableCollection(Of Moneda)
    Inherits Lista(Of Moneda)
    'Implements IGSMonedasServer


    Public Overloads ReadOnly Property Item(ByVal Cod As String) As Moneda
        Get
            Return Me.Find(Function(elemento As Moneda) elemento.Codigo = Cod)
        End Get
    End Property

    ''' <summary>Añade un elemento CMoneda a la lista</summary>
    ''' <param name="Cod">Código</param>
    ''' <param name="Den">Denominación</param>
    ''' <param name="EQUIV">Equivalencia</param>
    ''' <param name="FECACT">Fecha última actualización</param>
    ''' <returns>Devuelve el elemento CMoneda agregado</returns>
    Public Overloads Function Add(ByVal Cod As String, ByVal Den As String, ByVal EQUIV As Double, ByVal FECACT As Object) As Moneda

        ' * Objetivo: Anyadir una moneda a la coleccion
        ' * Recibe: Datos de la moneda
        ' * Devuelve: Moneda añadida

        Dim objnewmember As Moneda

        ' Creacion de objeto moneda

        'objnewmember = New Moneda(mDBServer, mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)
        objnewmember = New Moneda(mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)

        ' Paso de los parametros al nuevo objeto moneda

        objnewmember.Codigo = Cod
        objnewmember.Denominacion = Den
        objnewmember.Equivalencia = EQUIV
        objnewmember.FecAct = FECACT
        ' objnewmember.Denominaciones = New Multiidiomas(mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)

        MyBase.Add(objnewmember)
        Return objnewmember

    End Function
    'Public Function DevolverTodasLasMonedas() As DataSet Implements IGSMonedasServer.DevolverTodasLasMonedas
    '    Dim rs As New DataSet
    '    Return rs
    'End Function
    Public Function CargarTodasLasMonedasMonedas() As Monedas
        'Dim Client As New MonedasWCF.MonedasServiceClient
        Dim ds As DataSet
        Dim oMoneda As Moneda

        'Client.Open()
        'ds = Client.CargarTodasLasMonedas()
        'Client.Close()
        'ds = GSDatabaseServer.Root
        ' Llenar la coleccion a partir del Dataset
        'Me.Clear()

        For Each fila As DataRow In ds.Tables(0).Rows
            'He modificado los valores que estaban vacios por sus valores por defecto para quitar la clausula Optional del metodo que lo llama
            oMoneda = Me.Add(fila.Item("COD"), "", fila.Item("EQUIV"), fila.Item("FECACT"))
            '            oMoneda.Denominaciones = New Multiidiomas(mRemottingServer, mUserCode, mUserPassword, mIsAuthenticated)
            'For Each child As DataRow In fila.GetChildRows("REL_MON_DEN")
            '    oMoneda.Denominaciones.Add(child.Item("IDIOMA"), child.Item("DEN"), child.Item("FECACT"))
            'Next
        Next

        ds.Clear()
        ds = Nothing

        Return Me
    End Function

    Public Function CargarTodasLasMonedas() As System.Data.DataSet
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CargarTodasLasMonedas

    End Function
    Public Function AnyadirMonedaWCF(ByVal Datos As DataTable) As Boolean
        'Dim Client As New MonedasWCF.MonedasServiceClient
        Dim Result As Boolean
        'Dim WCFMon As New MonedasWCF.MonedaType

        'WCFMon.MonedaRow = Datos
        'Client.Open()
        'Result = Client.AnyadirMoneda(WCFMon)
        'Client.Close()

        Return Result
    End Function

    Public Sub New(ByVal remotting As Boolean, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
        MyBase.New(remotting, UserCode, UserPassword, isAuthenticated)
    End Sub

End Class
