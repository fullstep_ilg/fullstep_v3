﻿Public Class IdiomasRule
    Public Function CargarTodosLosIdiomas() As GSServerModel.Idiomas
        'Dim Client As New MonedasWCF.MonedasServiceClient
        Dim ds As DataSet
        Dim oIdiomas As New GSServerModel.Idiomas(True, "", "", True)
        Dim oIdioma As GSServerModel.Idioma
        Dim GSDBServer As New GSDatabaseServer.Root

        ds = GSDBServer.CargarTodosLosIdiomas()

        For Each fila As DataRow In ds.Tables(0).Rows
            'He modificado los valores que estaban vacios por sus valores por defecto para quitar la clausula Optional del metodo que lo llama
            oIdioma = oIdiomas.Add(fila.Item("COD"), fila.Item("DEN"))
        Next

        ds.Clear()
        ds = Nothing

        Return oIdiomas
    End Function
    'Public Sub Escribirlog(ByVal eventolog As String)

    '    Dim txtFecha As String
    '    Dim fic As String '= "GSregistro.txt"

    '    fic = System.AppDomain.CurrentDomain.BaseDirectory()
    '    fic = fic + "GSRegistro.txt"

    '    'txtFecha = Format$(Now)

    '    txtFecha = Now.ToString("dd/MM/yyyy HH:mm:ss:fff")

    '    Dim sw As New System.IO.StreamWriter(fic, True)
    '    sw.WriteLine(txtFecha & "  -  " & eventolog)
    '    sw.Close()

    'End Sub
End Class
