﻿Imports Fullstep.FSNLibrary.TiposDeDatos
Public Class MonedasRule

    Private mbGrabarLog As Boolean

    Public Property Log() As Boolean
        Get
            Log = mbGrabarLog
        End Get
        Set(ByVal value As Boolean)
            mbGrabarLog = value
        End Set
    End Property

    Public Function ActualizarMoneda(ByRef Moneda As GSServerModel.Moneda, ByVal bRealizarCambioCodigo As Boolean, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim o As New Object
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        mbGrabarLog = GrabarEnLog(Moneda, bActivLog, bIntegracionMonedas)
        Return GSDBServer.ActualizarMoneda(Moneda, bRealizarCambioCodigo, iInstWeb, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function AnyadirMoneda(ByRef Moneda As GSServerModel.Moneda, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet
        mbGrabarLog = GrabarEnLog(Moneda, bActivLog, bIntegracionMonedas)
        Return GSDBServer.AnyadirMoneda(Moneda, iInstWeb, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function CargarTodasLasMonedas(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CargarTodasLasMonedas(bMostrarIntegracion, sIdioma)

    End Function

    Public Function EliminarMonedas(ByVal Codigos() As String, ByVal iInstWeb As Integer, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean, ByVal FSP As String, ByVal sMonedaCentral As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet
        Dim Moneda As New GSServerModel.Moneda(False, "", "", True)
        mbGrabarLog = GrabarEnLog(Moneda, bActivLog, bIntegracionMonedas)
        Return GSDBServer.EliminarMonedas(Codigos, iInstWeb, mbGrabarLog, FSP, sMonedaCentral, Moneda.Origen, sUsuario)


    End Function

    Public Function CambioCodigoMoneda(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CambioCodigoMoneda(Codigo, CodigoNuevo, Usuario)
    End Function

    Private Function GrabarEnLog(ByRef Moneda As GSServerModel.Moneda, ByVal bActivLog As Boolean, ByVal bIntegracionMonedas As Boolean) As Boolean
        '***********************************************************************************
        '*** Descripción: Nos indica si hay que grabar o no el cambio en el log de       ***
        '***              monedas (tabla LOG_MON) y carga en la variable                 ***
        '***              del módulo m_udtOrigen el origen del movimiento                ***
        '*** Parametros:  Ninguno                                                        ***
        '*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
        '***********************************************************************************

        If bActivLog Or bIntegracionMonedas Then
            If bActivLog And bIntegracionMonedas Then
                Moneda.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSIntReg
            Else
                If bActivLog Then
                    Moneda.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSReg
                Else
                    Moneda.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSInt
                End If
            End If
            GrabarEnLog = True
        Else
            GrabarEnLog = False
        End If

    End Function

    '<summary>Devuelve los datos de una moneda</summary>
    '<param name="Moneda">Código de la moneda</param>
    '<param name="sIdioma">Idioma para la denominación</param>    
    '<returns>Objeto Moneda</returns>    

    Public Function DevolverMoneda(ByVal Moneda As String, ByVal Idioma As String) As GSServerModel.Moneda
        Dim oMoneda As New GSServerModel.Moneda

        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As System.Data.DataSet = GSDBServer.DevolverMoneda(Moneda, Idioma)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            oMoneda.Codigo = ds.Tables(0).Rows(0)("COD")
            oMoneda.Denominacion = ds.Tables(0).Rows(0)("DEN")
            oMoneda.Equivalencia = ds.Tables(0).Rows(0)("EQUIV")
            If Not ds.Tables(0).Rows(0).IsNull("FECACT") Then oMoneda.FecAct = ds.Tables(0).Rows(0)("FECACT")
        End If

        Return oMoneda
    End Function

End Class
