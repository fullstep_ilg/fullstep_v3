﻿Public Class FormasPagoRule
    Private mbGrabarLog As Boolean

    Public Property Log() As Boolean
        Get
            Log = mbGrabarLog
        End Get
        Set(ByVal value As Boolean)
            mbGrabarLog = value
        End Set
    End Property

    Public Function CargarTodasLasFormasPago(ByVal bMostrarIntegracion As Boolean, ByVal sIdioma As String) As System.Data.DataSet
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CargarTodasLasFormasPago(bMostrarIntegracion, sIdioma)

    End Function


    Public Function ActualizarFormaPago(ByRef oFormaPago As GSServerModel.FormaPago, ByVal bRealizarCambioCodigo As Boolean, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim o As New Object
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        mbGrabarLog = GrabarEnLog(oFormaPago, bActivLog, bIntegracionFormasPago)
        Return GSDBServer.ActualizarFormaPago(oFormaPago, bRealizarCambioCodigo, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function AnyadirFormaPago(ByRef oFormaPago As GSServerModel.FormaPago, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim o As New Object
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        mbGrabarLog = GrabarEnLog(oFormaPago, bActivLog, bIntegracionFormasPago)
        Return GSDBServer.AnyadirFormaPago(oFormaPago, mbGrabarLog, FSP, sUsuario)

    End Function

    Public Function EliminarFormasPago(ByVal Codigos() As String, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean, ByVal FSP As String, ByVal sUsuario As String) As GSServerModel.GSException
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet
        Dim FormaPago As New GSServerModel.FormaPago(False, "", "", True)
        mbGrabarLog = GrabarEnLog(FormaPago, bActivLog, bIntegracionFormasPago)
        Return GSDBServer.EliminarFormasPago(Codigos, mbGrabarLog, FSP, FormaPago.Origen, sUsuario)


    End Function

    Public Function CambioCodigoFormaPago(ByVal Codigo As String, ByVal CodigoNuevo As String, ByVal Usuario As String) As Integer

        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.CambioCodigoFormaPago(Codigo, CodigoNuevo, Usuario)
    End Function

    Private Function GrabarEnLog(ByRef FormaPago As GSServerModel.FormaPago, ByVal bActivLog As Boolean, ByVal bIntegracionFormasPago As Boolean) As Boolean
        '***********************************************************************************
        '*** Descripción: Nos indica si hay que grabar o no el cambio en el log de       ***
        '***              monedas (tabla LOG_MON) y carga en la variable                 ***
        '***              del módulo m_udtOrigen el origen del movimiento                ***
        '*** Parametros:  Ninguno                                                        ***
        '*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
        '***********************************************************************************

        If bActivLog Or bIntegracionFormasPago Then
            If bActivLog And bIntegracionFormasPago Then
                FormaPago.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSIntReg
            Else
                If bActivLog Then
                    FormaPago.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSReg
                Else
                    FormaPago.Origen = Fullstep.FSNLibrary.TiposDeDatos.OrigenIntegracion.FSGSInt
                End If
            End If
            GrabarEnLog = True
        Else
            GrabarEnLog = False
        End If

    End Function


End Class
