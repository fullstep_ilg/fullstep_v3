﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("GSServer")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("FULLSTEP NETWORKS S.L.")> 
<Assembly: AssemblyProduct("GSServer")> 
<Assembly: AssemblyCopyright("Copyright © FULLSTEP NETWORKS S.L. 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("ea06ac87-e9e9-4723-90ba-db5ef5bc0733")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("32100.4.004.002")>
<Assembly: AssemblyFileVersion("32100.4.004.002")>
