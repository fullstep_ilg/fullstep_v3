﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class ImpuestosRule

    '<summary>Devuelve los datos para el Mnto. de impuestos</summary>   
    '<param name="sIdioma">Idioma de las descripciones de paises y provincias</param>
    '<returns>Objeto Impuestos</returns>    

    Public Function DevolverDatosImpuestos(ByVal sIdioma As String) As GSServerModel.ImpuestosData
        Dim oData As New GSServerModel.ImpuestosData

        oData.Impuestos = DevolverImpuestos(sIdioma)
        Dim oPaisesRule As New PaisesRule
        oData.PaisesProvicias = oPaisesRule.CargarTodosLosPaisesProvincias(False, sIdioma)

        Return oData
    End Function

    '<summary>Devuelve los datos de impuestos</summary>   
    '<param name="sIdioma">Idioma de las descripciones de paises y provincias</param>
    '<returns>Objeto Impuestos</returns>    

    Public Function DevolverImpuestos(ByVal sIdioma As String) As GSServerModel.Impuestos
        Dim oImpuestos As New GSServerModel.Impuestos

        Dim oDBServer As New GSDatabaseServer.Impuestos
        Dim ds As DataSet = oDBServer.DevolverImpuestos(sIdioma)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oImpuesto As New GSServerModel.Impuesto
                    oImpuesto.Id = oRow("ID")
                    oImpuesto.Codigo = oRow("COD")
                    oImpuesto.Retenido = oRow("RETENIDO")
                    oImpuesto.GrupoCompatibilidad = oRow("GRP_COMP")
                    If Not oRow.IsNull("CONCEPTO") Then oImpuesto.Concepto = CType(oRow("CONCEPTO"), Integer)
                    If Not oRow.IsNull("COMMENT") Then oImpuesto.Comentario = oRow("COMMENT")
                    oImpuesto.FechaAct = oRow("FECACT")

                    'Denominaciones
                    If Not ds.Tables(1) Is Nothing AndAlso ds.Tables(1).Rows.Count > 0 Then
                        oImpuesto.Denominaciones = New GSServerModel.Multiidiomas

                        Dim dtView As New DataView(ds.Tables(1), "IMPUESTO=" & oImpuesto.Id.ToString, "IMPUESTO", DataViewRowState.CurrentRows)
                        For Each oRowValor As DataRowView In dtView
                            Dim oDen As New GSServerModel.Multiidioma

                            oDen.Idioma = oRowValor("IDIOMA")
                            oDen.Denominacion = oRowValor("DEN")

                            oImpuesto.Denominaciones.Add(oDen)
                        Next
                    End If

                    'Valores
                    If Not ds.Tables(2) Is Nothing AndAlso ds.Tables(2).Rows.Count > 0 Then
                        oImpuesto.Valores = New GSServerModel.ImpuestoValores

                        Dim dtView As New DataView(ds.Tables(2), "IMPUESTO=" & oImpuesto.Id.ToString, "IMPUESTO", DataViewRowState.CurrentRows)
                        For Each oRowValor As DataRowView In dtView
                            Dim oValor As New GSServerModel.ImpuestoValor
                            oValor.Id = oRowValor("ID")
                            oValor.Impuesto = oRowValor("IMPUESTO")
                            oValor.CodigoPais = oRowValor("PAI")
                            If Not oRowValor.Row.IsNull("PAIDEN") Then oValor.DenominacionPais = oRowValor("PAIDEN")
                            If Not oRowValor.Row.IsNull("PROVI") Then oValor.CodigoProvincia = oRowValor("PROVI")
                            If Not oRowValor.Row.IsNull("PROVIDEN") Then oValor.DenominacionProvincia = oRowValor("PROVIDEN")
                            oValor.Valor = oRowValor("VALOR")
                            oValor.Vigente = SQLBinaryToBoolean(oRowValor("VIGENTE"))
                            oValor.FechaAct = oRowValor("FECACT")

                            oImpuesto.Valores.Add(oValor)
                        Next
                    End If

                    oImpuestos.Add(oImpuesto)
                Next
            End If
        End If

        Return oImpuestos
    End Function

    '<summary>Inserta un nuevo impuesto</summary>
    '<param name="oImpuesto">Objeto Impuesto</param>         
    '<returns>Objeto GSException con el error si se ha producido</returns> 

    Public Function InsertarImpuesto(ByRef oImpuesto As GSServerModel.Impuesto) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        If Not oImpuesto Is Nothing Then
            Dim oGSDBServer As New GSDatabaseServer.Impuestos
            oError = oGSDBServer.InsertarImpuesto(oImpuesto)
        End If

        Return oError
    End Function

    '<summary>Actualiza un impuesto</summary>
    '<param name="oImpuesto">Objeto Impuesto</param>         
    '<returns>Objeto GSException con el error si se ha producido</returns> 

    Public Function ActualizarImpuesto(ByRef oImpuesto As GSServerModel.Impuesto) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        If Not oImpuesto Is Nothing Then
            Dim oGSDBServer As New GSDatabaseServer.Impuestos
            oError = oGSDBServer.ActualizarImpuesto(oImpuesto)
        End If

        Return oError
    End Function

    '<summary>Elimina impuestos</summary>
    '<param name="Ids">Ids de los impuestos a eliminar</param>         
    '<returns>Objeto GSException con el error si se ha producido</returns> 

    Public Function EliminarImpuestos(ByRef Ids() As Integer) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        If Not Ids Is Nothing AndAlso Ids.Count > 0 Then
            Dim oGSDBServer As New GSDatabaseServer.Impuestos
            oError = oGSDBServer.EliminarImpuestos(Ids)
        End If

        Return oError
    End Function

    '<summary>Inserta un nuevo valor de un impuesto</summary>
    '<param name="IdImpuesto">Id del impuesto</param>         
    '<param name="oImpuestoValor">Objeto ImpuestoValor</param>         
    '<returns>Objeto GSException con el error si se ha producido</returns> 

    Public Function InsertarImpuestoValor(ByVal IdImpuesto As Integer, ByRef oImpuestoValor As GSServerModel.ImpuestoValor) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        If Not oImpuestoValor Is Nothing Then
            Dim oGSDBServer As New GSDatabaseServer.Impuestos
            oError = oGSDBServer.InsertarImpuestoValor(IdImpuesto, oImpuestoValor)
        End If

        Return oError
    End Function

    '<summary>Actualiza el valor de un impuesto</summary>
    '<param name="oImpuestoValor">Objeto Impuesto</param>         
    '<returns>Objeto GSException con el error si se ha producido</returns> 

    Public Function ActualizarImpuestoValor(ByRef oImpuestoValor As GSServerModel.ImpuestoValor) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        If Not oImpuestoValor Is Nothing Then
            Dim oGSDBServer As New GSDatabaseServer.Impuestos
            oError = oGSDBServer.ActualizarImpuestoValor(oImpuestoValor)
        End If

        Return oError
    End Function

    '<summary>Elimina valores de un impuesto</summary>
    '<param name="Ids">Ids de los valores a eliminar</param>         
    '<returns>Objeto GSException con el error si se ha producido</returns> 

    Public Function EliminarImpuestoValores(ByRef Ids() As Integer) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        If Not Ids Is Nothing AndAlso Ids.Count > 0 Then
            Dim oGSDBServer As New GSDatabaseServer.Impuestos
            oError = oGSDBServer.EliminarImpuestoValores(Ids)
        End If

        Return oError
    End Function

End Class

