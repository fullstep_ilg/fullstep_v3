﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel
Imports System.ComponentModel
Imports System.IO

Public Class ProcesosRule
    Inherits RuleBase

    Public Sub New(ByVal oCtx As GSServerModel.Contexto)
        MyBase.New(oCtx)
    End Sub

#Region " Proceso "

    '<summary>Devuelve los datos de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Objeto Proceso</returns>    

    Public Function DevolverProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal DevolverEstructura As Boolean = False) As Proceso
        Dim oProceso As New Proceso()

        Dim GSDBServer As New GSDatabaseServer.Procesos
        Dim ds As System.Data.DataSet = GSDBServer.DevolverProceso(Anyo, Cod, GMN1)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim drProce As DataRow = ds.Tables(0).Rows(0)

            oProceso.Anyo = drProce("ANYO")
            oProceso.Codigo = drProce("COD")
            oProceso.GMN1 = drProce("GMN1")
            oProceso.Denominacion = drProce("DEN")
            'oProceso.AdjDir = drProce("ADJDIR")
            'If Not drProce.IsNull("PRES") Then oProceso.Presupuesto = drProce("PRES")
            'If Not drProce.IsNull("FECNEC") Then oProceso.FechaNecesidad = drProce("FECNEC")
            'oProceso.FechaApe = drProce("FECAPE")
            'If Not drProce.IsNull("FECVALSELPROVE") Then oProceso.FechaValSelProve = drProce("FECVALSELPROVE")
            'If Not drProce.IsNull("FECVAL") Then oProceso.FechaValidacion = drProce("FECVAL")
            'If Not drProce.IsNull("FECPRES") Then oProceso.FechaPresentacion = drProce("FECPRES")
            'If Not drProce.IsNull("FECENVPET") Then oProceso.FechaEnvPet = drProce("FECENVPET")
            If Not drProce.IsNull("FECLIMOFE") Then oProceso.FechaLimiteOfertas = Me.UtcToTz(drProce("FECLIMOFE"))
            'If Not drProce.IsNull("FECPROXREU") Then oProceso.FechaProximaReunion = drProce("FECPROXREU")
            'If Not drProce.IsNull("FECULTREU") Then oProceso.FechaUltimaReunion = drProce("FECULTREU")
            'If Not drProce.IsNull("FECCIERRE") Then oProceso.FechaCierre = drProce("FECCIERRE")
            'If Not drProce.IsNull("FECREAPE") Then oProceso.FechaReapertura = drProce("FECREAPE")
            'oProceso.Reudec = drProce("REUDEC")
            'If Not drProce.IsNull("USUVAL") Then oProceso.UsuVal = drProce("USUVAL")
            'If Not drProce.IsNull("USUVALSELPROVE") Then oProceso.UsuValSelProve = drProce("USUVALSELPROVE")
            'If Not drProce.IsNull("USUVALCIERRE") Then oProceso.UsuValCierre = drProce("USUVALCIERRE")
            oProceso.Moneda = drProce("MON")
            'oProceso.Cambio = drProce("CAMBIO")
            'If Not drProce.IsNull("ESP") Then oProceso.Esp = drProce("ESP")
            oProceso.Estado = drProce("EST")
            'If Not drProce.IsNull("ESTULT") Then oProceso.EstUlt = drProce("ESTULT")
            If Not drProce.IsNull("EQP") Then oProceso.Equipo = drProce("EQP")
            If Not drProce.IsNull("COM") Then oProceso.Comprador = drProce("COM")
            'If Not drProce.IsNull("SOLICITUD") Then oProceso.Solicitud = drProce("SOLICITUD")
            'If Not drProce.IsNull("FECNECMANUAL") Then oProceso.FechaNecManual = drProce("FECNECMANUAL")
            'oProceso.PedAprov = drProce("PED_APROV")
            'If Not drProce.IsNull("DEST") Then oProceso.Dest = drProce("DEST")
            'If Not drProce.IsNull("PAG") Then oProceso.Pag = drProce("PAG")
            'If Not drProce.IsNull("FECINI") Then oProceso.FechaIni = drProce("FECINI")
            'If Not drProce.IsNull("FECFIN") Then oProceso.FechaFin = drProce("FECFIN")
            'If Not drProce.IsNull("PROVEACT") Then oProceso.ProveAct = drProce("PROVEACT")
            If Not drProce.IsNull("FECACT") Then oProceso.FechaAct = drProce("FECACT")
            'oProceso.UsuarioApertura = drProce("USUAPER")
            If Not drProce.IsNull("FECINISUB") Then oProceso.FechaInicioSubasta = Me.UtcToTz(drProce("FECINISUB"))
            'If Not drProce.IsNull("SOLICIT") Then oProceso.Solicit = drProce("SOLICIT")
            'oProceso.CalcPend = drProce("CALCPEND")
            'If Not drProce.IsNull("CALCFEC") Then oProceso.FechaCalc = drProce("CALCFEC")
            'If Not drProce.IsNull("ADJUDICADO") Then oProceso.Adjudicado = drProce("ADJUDICADO")
            'If Not drProce.IsNull("PLANTILLA") Then oProceso.Plantilla = drProce("PLANTILLA")
            'oProceso.PlantillaVistas = drProce("PLANTILLA_VISTAS")
            'oProceso.AdjERP = drProce("ADJ_ERP")
            'If Not drProce.IsNull("COMENT_ADJ") Then oProceso.ComentAdj = drProce("COMENT_ADJ")
            'If Not drProce.IsNull("P") Then oProceso.P = drProce("P")
            'If Not drProce.IsNull("W") Then oProceso.W = drProce("W")
            'If Not drProce.IsNull("O") Then oProceso.O = drProce("O")
            'If Not drProce.IsNull("PRESTOTAL") Then oProceso.PresTotal = drProce("PRESTOTAL")
            'If Not drProce.IsNull("CONSUMIDO") Then oProceso.Consumido = drProce("CONSUMIDO")
            'oProceso.DesdeSolicitud = drProce("DESDE_SOLICITUD")
            'If Not drProce.IsNull("PER_APER") Then oProceso.PersonaApertura = drProce("PER_APER")
            'If Not drProce.IsNull("UON1_APER") Then oProceso.UON1Apertura = drProce("UON1_APER")
            'If Not drProce.IsNull("UON2_APER") Then oProceso.UON2Apertura = drProce("UON2_APER")
            'If Not drProce.IsNull("UON3_APER") Then oProceso.UON3Apertura = drProce("UON3_APER")
            'If Not drProce.IsNull("DEP_APER") Then oProceso.DepApertura = drProce("DEP_APER")
            'oProceso.PubMatProve = drProce("PUBMATPROVE")            
            'If Not drProce.IsNull("FECADJ") Then oProceso.FechaAdj = drProce("FECADJ")

            If DevolverEstructura Then oProceso.Grupos = DevolverProcesoGrupos(Anyo, Cod, GMN1, True)
        End If

        Return oProceso
    End Function
    Public Function DevolverCarpetaPlantillaProce(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As String
        Dim GSDBServer As New GSDatabaseServer.Procesos
        Dim sCarpeta As String = GSDBServer.DevolverCarpetaPlantillaProce(Anyo, Cod, GMN1)
        Return sCarpeta
    End Function

    Public Function SacarRemitenteEmpresa(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As String
        Dim GSDBServer As New GSDatabaseServer.Procesos
        Dim sRemitente As String = GSDBServer.SacarRemitenteEmpresa(Anyo, Cod, GMN1)
        Return sRemitente
    End Function
    '<summary>Actualiza los datos de un proceso</summary>
    '<param name="oProceso">Objeto Proceso</param>         

    Public Function ActualizarProceso(ByVal oProceso As GSServerModel.Proceso) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        If Not oProceso Is Nothing Then
            Dim oGSDBServer As New GSDatabaseServer.Procesos

            'Comprobar que existe el proceso
            Dim ds As DataSet = oGSDBServer.DevolverProceso(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim drProce As DataRow = ds.Tables(0).Rows(0)

                'Comprobar que los datos de la subasta no han sido modificados por otra persona
                If Date.Equals(oProceso.FechaAct, drProce("FECACT")) Then
                    'Pasar las fechas a UTC antes de grabar en BD
                    If Not oProceso.FechaLimiteOfertas Is Nothing Then oProceso.FechaLimiteOfertas = Me.TzToUtc(oProceso.FechaLimiteOfertas)
                    If Not oProceso.FechaInicioSubasta Is Nothing Then oProceso.FechaInicioSubasta = Me.TzToUtc(oProceso.FechaInicioSubasta)

                    oError = oGSDBServer.ActualizarProceso(oProceso)
                Else
                    oError.Number = ErroresGS.TESInfActualModificada
                End If
            Else
                oError.Number = ErroresGS.TESProcesoNoExiste
            End If
        End If

        Return oError
    End Function

    '<summary>Devuelve los grupos de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Array de objetos ProcesoGrupo</returns>    

    Public Function DevolverProcesoGrupos(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal DevolverEstructura As Boolean = False) As ProcesoGrupos
        Dim oPGS As New ProcesoGrupoService(Me.Contexto)
        Return oPGS.DevolverGrupos(Anyo, Cod, GMN1, DevolverEstructura)
    End Function

    '<summary>Actualiza los grupos de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="oGrupos">Objeto de tipo ProcesoGrupos con los datos de los grupos</param>
    '<param name="arGrupos">Array de objetos ProcesoGrupo</param>         

    Public Function ActualizarProcesoGrupos(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oGrupos As GSServerModel.ProcesoGrupos) As GSServerModel.GSException
        Dim oPS As New ProcesoService(Me.Contexto)
        Return oPS.ActualizarProcesoGrupos(Anyo, Cod, GMN1, oGrupos)
    End Function

    '<summary>Comprueba que los atributos obligatorios tienen valor</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="udtTipoVal">Validacion en apertura (0) en publicacion(1)</param>
    '<returns>Objeto ValidacionAtributos</returns>    

    Public Function ValidarAtributosEspObligatorios(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal udtTipoVal As TValidacionAtrib) As GSServerModel.ValidacionAtributos
        Dim oVal As New ValidacionAtributos
        oVal.AtributosCorrectos = True

        Dim oPS As New ProcesoService(Me.Contexto)
        oVal.AtributosProceso = oPS.DevolverAtributosProceso(Anyo, Cod, GMN1, True, udtTipoVal, True)
        Dim oPGS As New ProcesoGrupoService(Me.Contexto)
        oVal.AtributosPorcesoGrupo = oPGS.DevolverAtributosGrupo(Anyo, Cod, GMN1, , True, udtTipoVal, True)
        Dim oIS As New ItemService(Me.Contexto)
        oVal.AtributosItem = oIS.DevolverAtributosItem(Anyo, Cod, GMN1, , True, udtTipoVal, True)

        If (Not oVal.AtributosProceso Is Nothing AndAlso oVal.AtributosProceso.Count > 0) Or _
           (Not oVal.AtributosPorcesoGrupo Is Nothing AndAlso oVal.AtributosPorcesoGrupo.Count > 0) Or _
           (Not oVal.AtributosItem Is Nothing AndAlso oVal.AtributosItem.Count > 0) Then
            oVal.AtributosCorrectos = False
        End If

        Return oVal
    End Function

    '<summary>Devuelve las asignaciones de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Proce">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="CriterioOrdenacion">Criterio de ordenación de las asignaciones devueltas</param>
    '<param name="CodEqp">Criterio código equipo</param>
    '<param name="CodComp">criterio código comp.</param>
    '<param name="CodProve">criterio código proveedor</param>
    '<param name="DevolverEstructura">Indica si hay que devolver la estructura del proceso con la asignación</param>
    '<returns>Objeto asignaciones</returns> 

    Public Function DevolverAsignacionesProceso(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, _
                                                Optional ByVal CriterioOrdenacion As Fullstep.FSNLibrary.TiposDeDatos.TipoOrdenacionAsignaciones = -1, _
                                                Optional ByVal CodEqp As String = Nothing, Optional ByVal CodComp As String = Nothing, Optional ByVal CodProve As String = Nothing, _
                                                Optional ByVal DevolverEstructura As Boolean = False) As GSServerModel.Asignaciones
        Dim oAsigs As New Asignaciones

        Dim oGSDBServer As New GSDatabaseServer.Procesos
        Dim dsAsignaciones As DataSet = oGSDBServer.DevolverAsignaciones(Anyo, Proce, GMN1, CriterioOrdenacion, CodEqp, CodComp, CodProve)

        If Not dsAsignaciones Is Nothing AndAlso dsAsignaciones.Tables.Count > 0 Then
            Dim oAsig As New Asignacion

            For Each oRow As DataRow In dsAsignaciones.Tables(0).Rows
                oAsig.CodigoProveedor = oRow("COD")
                oAsig.DenominacionProveedor = oRow("DEN")
                oAsig.CodigoEquipo = oRow("EQP")
                oAsig.CodigoComp = oRow("COM")
                If Not oRow.IsNull("FSP_COD") Then oAsig.CodigoPortal = oRow("FSP_COD")
                oAsig.Premium = oRow("PREMIUM")
                oAsig.PremActivo = oRow("ACTIVO")
                If Not oRow.IsNull("CAL1") Then oAsig.Calif1 = oRow("CAL1")
                If Not oRow.IsNull("CAL2") Then oAsig.Calif2 = oRow("CAL2")
                If Not oRow.IsNull("CAL3") Then oAsig.Calif3 = oRow("CAL3")
                If Not oRow.IsNull("VAL1") Then oAsig.Val1 = oRow("VAL1")
                If Not oRow.IsNull("VAL2") Then oAsig.Val2 = oRow("VAL2")
                If Not oRow.IsNull("VAL3") Then oAsig.Val3 = oRow("VAL3")

                If DevolverEstructura Then oAsig.Grupos = DevolverProcesoGrupos(Anyo, Proce, GMN1, True)

                oAsigs.Add(oAsig)
            Next
        End If

        Return oAsigs
    End Function

    Public Function DevolverPeticionesProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As GSServerModel.ProceProvePetOfertas
        Dim oPets As New ProceProvePetOfertas

        Dim oDBProce As New GSDatabaseServer.Procesos
        Dim dsPeticiones As DataSet = oDBProce.DevolverProcesoPeticiones(Anyo, Cod, GMN1)

        If Not dsPeticiones Is Nothing AndAlso dsPeticiones.Tables.Count > 0 Then
            Dim oPet As New ProceProvePetOferta

            For Each oRow As DataRow In dsPeticiones.Tables(0).Rows
                oPet.Anyo = oRow("ANYO")
                oPet.Proce = oRow("PROCE")
                oPet.GMN1 = oRow("GMN1")
                oPet.CodigoProveedor = oRow("PROVE")
                oPet.ID = oRow("ID")
                oPet.ApellidoCon = oRow("APECON")
                If Not oRow.IsNull("NOMCON") Then oPet.NombreCon = oRow("NOMCON")
                oPet.ViaWeb = SQLBinaryToBoolean(oRow("WEB"))
                oPet.ViaEmail = SQLBinaryToBoolean(oRow("EMAIL"))
                oPet.ViaImpreso = SQLBinaryToBoolean(oRow("IMP"))
                oPet.Fecha = Me.UtcToTz(oRow("FECHA"))
                oPet.TipoPet = oRow("TIPOPET")
                oPet.FechaReu = Me.UtcToTz(oRow("FECHAREU"))
                oPet.Cerrado = SQLBinaryToBoolean(oRow("CERRADO"))

                oPets.Add(oPet)
            Next
        End If

        Return oPets
    End Function

    Public Function InsertarPeticionesProceso(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal TipoInstalacionWeb As TipoInstWeb, ByVal CIA As Integer, ByVal oPets As ProceProvePetOfertas) As GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim arProvesDesautorizados(2, -1) As String
        For Each oPet As ProceProvePetOferta In oPets
            If Not oPet.CancelMail Then
                If oPet.ViaWeb Then
                    If TipoInstalacionWeb = TipoInstWeb.ConPortal Then
                        Dim oProveRule As New GSServer.ProveedoresRule(Me.Contexto)
                        Dim oEstProve As EstadoProveedorPortal = oProveRule.DevolverEstadoProveedorPortal(oPet.CodigoProveedor, CIA)
                        If oEstProve Is Nothing Then
                            ReDim Preserve arProvesDesautorizados(2, arProvesDesautorizados.GetUpperBound(1) + 1)
                            arProvesDesautorizados(0, arProvesDesautorizados.GetUpperBound(1)) = oPet.CodigoProveedor
                            arProvesDesautorizados(1, arProvesDesautorizados.GetUpperBound(1)) = String.Empty
                            arProvesDesautorizados(2, arProvesDesautorizados.GetUpperBound(1)) = "DESAUT"
                            oPets.Remove(oPet)
                        Else
                            If oEstProve.EstadoGS = TipoEstadoProveEnCia.TESPCInActivo Then
                                ReDim Preserve arProvesDesautorizados(2, arProvesDesautorizados.GetUpperBound(1) + 1)
                                arProvesDesautorizados(0, arProvesDesautorizados.GetUpperBound(1)) = oPet.CodigoProveedor
                                arProvesDesautorizados(1, arProvesDesautorizados.GetUpperBound(1)) = oEstProve.CodigoPortal
                                arProvesDesautorizados(2, arProvesDesautorizados.GetUpperBound(1)) = "DESAUT"
                                oPets.Remove(oPet)
                            Else
                                If oEstProve.EstadoPortal <> TipoEstadoCiaEnPortal.TESPActivo Then
                                    ReDim Preserve arProvesDesautorizados(2, arProvesDesautorizados.GetUpperBound(1) + 1)
                                    arProvesDesautorizados(0, arProvesDesautorizados.GetUpperBound(1)) = oPet.CodigoProveedor
                                    arProvesDesautorizados(1, arProvesDesautorizados.GetUpperBound(1)) = oEstProve.CodigoPortal
                                    arProvesDesautorizados(2, arProvesDesautorizados.GetUpperBound(1)) = "DESAUT"
                                    oPets.Remove(oPet)
                                Else
                                    oPet.CodigoProveedorPortal = oEstProve.CodigoPortal
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Next

        'Todas las peticiones son nuevas, luego se obtiene un dataset vacío        
        If oPets.Count > 0 Then
            Dim oDBProce As New GSDatabaseServer.Procesos
            Dim dsPeticiones As DataSet = oDBProce.DevolverProcesoPeticiones(-1, -1, String.Empty)

            'Nuevas peticiones
            For Each oPet As ProceProvePetOferta In oPets
                Dim oNewRow As DataRow = dsPeticiones.Tables(0).NewRow
                oNewRow("ANYO") = oPet.Anyo
                oNewRow("GMN1") = oPet.GMN1
                oNewRow("PROCE") = oPet.Proce
                oNewRow("PROVE") = oPet.CodigoProveedor
                oNewRow("APECON") = oPet.ApellidoCon
                oNewRow("NOMCON") = oPet.NombreCon
                oNewRow("WEB") = BooleanToSQLBinaryDBNull(oPet.ViaWeb)
                oNewRow("EMAIL") = BooleanToSQLBinaryDBNull(oPet.ViaEmail)
                oNewRow("IMP") = BooleanToSQLBinaryDBNull(oPet.ViaImpreso)
                oNewRow("FECHA") = Me.TzToUtc(oPet.Fecha)
                oNewRow("TIPOPET") = oPet.TipoPet
                oNewRow("FECHAREU") = oPet.FechaReu 'oPet.TzToUtc(oPet.FechaReu)
                oNewRow("CERRADO") = BooleanToSQLBinaryDBNull(oPet.Cerrado)
                'CODIGOPORTAL no se corresponde con un campo de la tabla
                If Not oPet.CodigoProveedorPortal Is Nothing Then oNewRow("CODIGOPORTAL") = oPet.CodigoProveedorPortal

                dsPeticiones.Tables(0).Rows.Add(oNewRow)
            Next

            'Inserción de las peticiones
            oError = oDBProce.InsertarProcesoPeticiones(CIA, dsPeticiones, TipoInstalacionWeb)
        End If

        Return oError
    End Function

    '<summary>Devuelve los datos de un comprador</summary>
    '<param name="Equipo">Equipo</param>
    '<param name="Codigo">Código</param>    
    '<returns>Objeto Comprador</returns>    

    Public Function DevolverComprador(ByVal Equipo As String, ByVal Codigo As String) As Comprador
        Dim oComprador As New Comprador()

        Dim GSDBServer As New GSDatabaseServer.Procesos
        Dim ds As System.Data.DataSet = GSDBServer.DevolverComprador(Equipo, Codigo)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim drComp As DataRow = ds.Tables(0).Rows(0)

            oComprador.Equipo = drComp("EQP")
            oComprador.Codigo = drComp("COD")
            oComprador.Apellidos = drComp("APE")
            If Not drComp.IsNull("NOM") Then oComprador.Nombre = drComp("NOM")
            If Not drComp.IsNull("TFNO") Then oComprador.Telefono = drComp("TFNO")
            If Not drComp.IsNull("FAX") Then oComprador.Fax = drComp("FAX")
            If Not drComp.IsNull("EMAIL") Then oComprador.EMail = drComp("EMAIL")
            If Not drComp.IsNull("CAR") Then oComprador.Car = drComp("CAR")
            If Not drComp.IsNull("UON1") Then oComprador.UON1 = drComp("UON1")
            If Not drComp.IsNull("UON2") Then oComprador.UON2 = drComp("UON2")
            If Not drComp.IsNull("UON3") Then oComprador.UON3 = drComp("UON3")
            If Not drComp.IsNull("DEP") Then oComprador.Departamento = drComp("DEP")
            If Not drComp.IsNull("TFNO2") Then oComprador.Telefono2 = drComp("TFNO2")
        End If

        Return oComprador
    End Function

    '<summary>Devuelve los proveedores del proceso a publicar</summary>
    '<param name="Equipo">Equipo</param>
    '<param name="Codigo">Código</param>    
    '<returns>Objeto Proveedores con los datos de los proveedores</returns>    

    Public Function DevolverProveedoresAPublicar(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, Optional ByVal Equipo As String = Nothing, Optional ByVal Com As String = Nothing) As Proveedores
        Dim oProves As New Proveedores

        Dim oDBProce As New GSDatabaseServer.Procesos
        Dim dsProves As DataSet = oDBProce.DevolverProveedoresAPublicar(Anyo, Proce, GMN1, Equipo, Com)
        If Not dsProves Is Nothing AndAlso dsProves.Tables.Count > 0 Then
            For Each oRow As DataRow In dsProves.Tables(0).Rows
                Dim oProve As New Proveedor
                oProve.Codigo = oRow("PROVE")
                oProve.Denominacion = oRow("DEN")

                oProves.Add(oProve)
            Next
        End If

        Return oProves
    End Function

    '<summary>Activa la publicación de los proveedores</summary>
    '<param name="Anyo">Anyo</param>
    '<param name="Proce">Proce</param>    
    '<param name="GMN1">GMN1</param> 
    '<param name="oProves">Proveedores</param>
    '<param name="InsWeb">Tipo instalación web</param>
    '<param name="FSP_CIA">Código compañía en portal</param>
    '<param name="sFSPSRV">Servidor portal</param>
    '<param name="sFSPBD">BD Portal</param>
    '<returns>Objeto GSException con el error si se ha producido</returns>  

    Public Function ActivarPublicacionProveedores(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal oProves As GSServerModel.ProcesoProveedores, ByVal InsWeb As TiposDeDatos.TipoInstWeb, ByVal FSP_CIA As Integer, _
                                                  ByVal sFSPSRV As String, ByVal sFSPBD As String) As GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim oProceso As GSServerModel.Proceso = DevolverProceso(Anyo, Proce, GMN1)        
        If Not oProceso Is Nothing Then
            If oProceso.Estado < TipoEstadoProceso.conadjudicaciones Then
                For Each oProve As GSServerModel.ProcesoProveedor In oProves                    
                    If InsWeb = TiposDeDatos.TipoInstWeb.ConPortal Then
                        'Recuperamos el ESTADO del proveedor enlazado
                        Dim oProveRules As New GSServer.ProveedoresRule(Me.Contexto)
                        Dim oAut As EstadoProveedorPortal = oProveRules.DevolverEstadoProveedorPortal(oProve.Proveedor, FSP_CIA)
                        If oAut.Autorizado Then
                            oProve.CodigoPortal = oAut.CodigoPortal

                            If oAut.EstadoGS = TipoEstadoProveEnCia.TESPCActivo Then
                                If oAut.EstadoPortal <> TipoEstadoCiaEnPortal.TESPActivo Then
                                    oError.Number = ErroresGS.TESProvePortalDesautorizado
                                    Exit For
                                End If
                            Else
                                oError.Number = ErroresGS.TESProvePortalDesautorizadoEnCia
                                Exit For
                            End If
                        Else
                            oError.Number = ErroresGS.TESProvePortalDesautorizado
                            Exit For
                        End If
                    End If
                Next
            Else
                oError.Number = ErroresGS.tesprocesocerrado
            End If
        Else
            oError.Number = ErroresGS.TESDatoEliminado
            oError.ErrorId = 44   'Proceso
        End If

        If oError.Number = ErroresGS.TESnoerror Then
            Dim oDBProce As New GSDatabaseServer.Procesos
            oDBProce.ActivarPublicacionProveedores(Anyo, Proce, GMN1, oProves, InsWeb, sFSPSRV, sFSPBD, FSP_CIA)
        End If

        Return oError
    End Function

    '<summary>Desactiva la publicación de los proveedores</summary>
    '<param name="Anyo">Anyo</param>
    '<param name="Proce">Proce</param>    
    '<param name="GMN1">GMN1</param> 
    '<param name="oProves">Proveedores</param>
    '<param name="InsWeb">Tipo instalación web</param>
    '<param name="bPremium">Valor del parámetro Premium</param>
    '<param name="FSP_CIA">Código compañía en portal</param>
    '<param name="sFSPSRV">Servidor portal</param>
    '<param name="sFSPBD">BD Portal</param>
    '<returns>Objeto GSException con el error si se ha producido</returns>  

    Public Function DesactivarPublicacionProveedores(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal oProves As GSServerModel.ProcesoProveedores, ByVal InsWeb As TiposDeDatos.TipoInstWeb, ByVal bPremium As Boolean, _
                                                     ByVal FSP_CIA As Integer, ByVal sFSPSRV As String, ByVal sFSPBD As String) As GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        Dim oProvesDespub As New ProcesoProveedores
        Dim oProveRule As New ProveedoresRule(Me.Contexto)
        For Each oProcProve As GSServerModel.ProcesoProveedor In oProves
            Dim oProve As Proveedor = oProveRule.DevolverProveedor(oProcProve.Proveedor)

            If InsWeb = TiposDeDatos.TipoInstWeb.ConPortal Then
                'Comprobamos que el proveedor no es premium activo
                If oProve.Premium And oProve.Activo And bPremium Then
                    oError.Number = ErroresGS.TESDesPublicarPremiumActivo
                    Exit For
                End If

                If oProve.FSPCod Is Nothing Then
                    oError.Number = ErroresGS.TESProvePortalNoEnlazado
                    Exit For
                End If
            End If

            oProvesDespub.Add(oProcProve)
        Next

        If oError.Number = ErroresGS.TESnoerror Then
            Dim oDBProce As New GSDatabaseServer.Procesos
            oDBProce.DesactivarPublicacionProveedores(Anyo, Proce, GMN1, oProvesDespub, InsWeb, sFSPSRV, sFSPBD, FSP_CIA)
        End If

        Return oError
    End Function

#End Region

#Region " Proceso_Def "

    '<summary>Devuelve los datos de un proceso de la tabla Proce_Def</summary>
    '<param name="Anyo">Año</param>
    '<param name="Proce">Proce</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Objeto Proceso</returns>    

    Public Function DevolverProcesoDef(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String) As ProcesoDef
        Dim oProcesoDef As New ProcesoDef()

        Dim GSDBServer As New GSDatabaseServer.Procesos
        Dim ds As System.Data.DataSet = GSDBServer.DevolverProcesoDef(Anyo, Proce, GMN1)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim drProceDef As DataRow = ds.Tables(0).Rows(0)

            oProcesoDef.Anyo = drProceDef("ANYO")
            oProcesoDef.GMN1 = drProceDef("GMN1")
            oProcesoDef.Proce = drProceDef("PROCE")
            'oProcesoDef.Dest = drProceDef("DEST")
            'oProcesoDef.Pag = drProceDef("PAG")
            'oProcesoDef.FecSum = drProceDef("FECSUM")
            'oProcesoDef.Prove = drProceDef("PROVE")
            'oProcesoDef.Dist = drProceDef("DIST")
            'oProcesoDef.PresAnu1 = drProceDef("PRESANU1")
            'oProcesoDef.PresAnu2 = drProceDef("PRESANU2")
            'oProcesoDef.Pres1 = drProceDef("PRES1")
            'oProcesoDef.Pres2 = drProceDef("PRES2")
            'oProcesoDef.ProceEsp = SQLBinaryToBoolean(drProceDef("PROCE_ESP"))
            'oProcesoDef.GrupoEsp = SQLBinaryToBoolean(drProceDef("GRUPO_ESP"))
            'oProcesoDef.ItemEsp = SQLBinaryToBoolean(drProceDef("ITEM_ESP"))
            'oProcesoDef.OfeAdjun = SQLBinaryToBoolean(drProceDef("OFE_ADJUN"))
            'oProcesoDef.GrupoAdjun = SQLBinaryToBoolean(drProceDef("GRUPO_ADJUN"))
            'oProcesoDef.ItemAdjun = SQLBinaryToBoolean(drProceDef("ITEM_ADJUN"))
            'oProcesoDef.SolCantMax = SQLBinaryToBoolean(drProceDef("SOLCANTMAX"))
            'oProcesoDef.Ponderar = SQLBinaryToBoolean(drProceDef("PONDERAR"))
            'oProcesoDef.PrecAlter = SQLBinaryToBoolean(drProceDef("PRECALTER"))
            oProcesoDef.Subasta = SQLBinaryToBoolean(drProceDef("SUBASTA"))
            If Not drProceDef.IsNull("FECACT") Then oProcesoDef.FecAct = drProceDef("FECACT")
            'oProcesoDef.CambiarMon = SQLBinaryToBoolean(drProceDef("CAMBIARMON"))
            'oProcesoDef.Solicit = drProceDef("SOLICIT")
            'oProcesoDef.AdminPub = SQLBinaryToBoolean(drProceDef("ADMIN_PUB"))
            'oProcesoDef.MaxOfe = SQLBinaryToBoolean(drProceDef("MAX_OFE"))
            'oProcesoDef.PublicarFinSum = SQLBinaryToBoolean(drProceDef("PUBLICARFINSUM"))
            'If Not drProceDef.IsNull("AVISARDESPUB") Then oProcesoDef.AvisarDesPub = SQLBinaryToBoolean(drProceDef("AVISARDESPUB"))
            'oProcesoDef.UnSoloPedido = SQLBinaryToBoolean(drProceDef("UNSOLOPEDIDO"))
            'oProcesoDef.OblAtribOfe = SQLBinaryToBoolean(drProceDef("OBL_ATRIB_OFE"))
            oProcesoDef.ValAtrEsp = SQLBinaryToBoolean(drProceDef("VAL_ATR_ESP"))
            'oProcesoDef.SubastaTipo = drProceDef("SUBASTATIPO")
            'oProcesoDef.SubastaComunica = SQLBinaryToBoolean(drProceDef("SUBASTACOMUNICA"))
        End If

        Return oProcesoDef
    End Function

    '<summary>Actualiza los datos de un proceso de la tabla Proce_Def</summary>
    '<param name="oProcesoDef">Objeto ProcesoDef</param>         

    Public Function ActualizarProcesoDef(ByVal oProcesoDef As GSServerModel.ProcesoDef) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        If Not oProcesoDef Is Nothing Then
            Dim oGSDBServer As New GSDatabaseServer.Procesos

            'Comprobar que existe el proceso
            Dim ds As DataSet = oGSDBServer.DevolverProcesoDef(oProcesoDef.Anyo, oProcesoDef.Proce, oProcesoDef.GMN1)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim drProceDef As DataRow = ds.Tables(0).Rows(0)

                'Comprobar que los datos no han sido modificados por otra persona
                If Date.Equals(oProcesoDef.FecAct, drProceDef("FECACT")) Then
                    'Actualizar datos
                    ActualizarDRProcesoDef(drProceDef, oProcesoDef)

                    oError = oGSDBServer.ActualizarProcesoDef(ds)
                Else
                    oError.Number = ErroresGS.TESInfActualModificada
                End If
            Else
                oError.Number = ErroresGS.TESProcesoNoExiste
            End If
        End If

        Return oError
    End Function

    '<summary>Traslada los cambios del objeto ProcesoDef al datarow</summary>
    '<param name="drProcedef">DataRow con los datos del proceso</param>         
    '<param name="oProcesoDef">Objeto ProcesoDef</param> 

    Private Sub ActualizarDRProcesoDef(ByRef drProceDef As DataRow, ByVal oProcesoDef As GSServerModel.ProcesoDef)
        'drProceDef("DEST") = oProcesoDef.Dest
        'drProceDef("PAG") = oProcesoDef.Pag
        'drProceDef("FECSUM") = oProcesoDef.FecSum
        'drProceDef("PROVE") = oProcesoDef.Prove
        'drProceDef("DIST") = oProcesoDef.Dist
        'drProceDef("PRESANU1") = oProcesoDef.PresAnu1
        'drProceDef("PRESANU2") = oProcesoDef.PresAnu2
        'drProceDef("PRES1") = oProcesoDef.Pres1
        'drProceDef("PRES2") = oProcesoDef.Pres2
        'drProceDef("PROCE_ESP") = BooleanToSQLBinaryDBNull(oProcesoDef.ProceEsp)
        'drProceDef("GRUPO_ESP") = BooleanToSQLBinaryDBNull(oProcesoDef.GrupoEsp)
        'drProceDef("ITEM_ESP") = BooleanToSQLBinaryDBNull(oProcesoDef.ItemEsp)
        'drProceDef("OFE_ADJUN") = BooleanToSQLBinaryDBNull(oProcesoDef.OfeAdjun)
        'drProceDef("GRUPO_ADJUN") = BooleanToSQLBinaryDBNull(oProcesoDef.GrupoAdjun)
        'drProceDef("ITEM_ADJUN") = BooleanToSQLBinaryDBNull(oProcesoDef.ItemAdjun)
        'drProceDef("SOLCANTMAX") = BooleanToSQLBinaryDBNull(oProcesoDef.SolCantMax)
        'drProceDef("PONDERAR") = BooleanToSQLBinaryDBNull(oProcesoDef.Ponderar)
        'drProceDef("PRECALTER") = BooleanToSQLBinaryDBNull(oProcesoDef.PrecAlter)
        drProceDef("SUBASTA") = BooleanToSQLBinaryDBNull(oProcesoDef.Subasta)
        'drProceDef("CAMBIARMON") = BooleanToSQLBinaryDBNull(oProcesoDef.CambiarMon)
        'drProceDef("SOLICIT") = oProcesoDef.Solicit
        'drProceDef("ADMIN_PUB") = BooleanToSQLBinaryDBNull(oProcesoDef.AdminPub)
        'drProceDef("MAX_OFE") = BooleanToSQLBinaryDBNull(oProcesoDef.MaxOfe)
        'drProceDef("PUBLICARFINSUM") = BooleanToSQLBinaryDBNull(oProcesoDef.PublicarFinSum)
        'drProceDef("AVISARDESPUB") = BooleanToSQLBinaryDBNull(oProcesoDef.AvisarDesPub)
        'drProceDef("UNSOLOPEDIDO") = BooleanToSQLBinaryDBNull(oProcesoDef.UnSoloPedido)
        'drProceDef("OBL_ATRIB_OFE") = BooleanToSQLBinaryDBNull(oProcesoDef.OblAtribOfe)
        'drProceDef("SUBASTATIPO") = oProcesoDef.SubastaTipo
        drProceDef("VAL_ATR_ESP") = BooleanToSQLBinaryDBNull(oProcesoDef.ValAtrEsp)
        'drProceDef("SUBASTACOMUNICA") = BooleanToSQLBinaryDBNull(oProcesoDef.SubastaComunica)
    End Sub

#End Region

#Region " ProcesoSubasta "

    '<summary>Devuelve los datos para la pantalla SubastaMonitor</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<returns>Objeto ProcesoSubasta SubastaMonitorData</returns>

    Public Function DevolverDatosSubastaMonitor(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal Idioma As String, Optional ByVal Grupo As Nullable(Of Integer) = Nothing, Optional ByVal Item As Nullable(Of Integer) = Nothing) As GSServerModel.SubastaMonitorData
        Dim oSubMonData As New SubastaMonitorData

        oSubMonData.Proceso = DevolverProceso(Anyo, Cod, GMN1, True)
        oSubMonData.Subasta = DevolverProcesoSubasta(Anyo, Cod, GMN1)
        oSubMonData.ProceProves = DevolverSubastaProveedoresResumen(Anyo, Cod, GMN1, Grupo, Item)
        oSubMonData.Eventos = DevolverSubastaEventos(Anyo, Cod, GMN1)
        oSubMonData.SubastaPujas = DevolverProcesoSubastaPujas(Anyo, Cod, GMN1, Grupo, Item)
        Dim oMonedaRule As New MonedasRule
        oSubMonData.Moneda = oMonedaRule.DevolverMoneda(oSubMonData.Proceso.Moneda, Idioma)

        Return oSubMonData
    End Function

    '<summary>Devuelve los datos para la pantalla SubastaConf</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Cod">Cod</param> 
    '<param name="GMN1">GMN1</param>
    '<returns>Objeto ProcesoSubasta SubastaConfData</returns>

    Public Function DevolverDatosSubastaConf(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal Idioma As String) As GSServerModel.SubastaConfData
        Dim oSubConfData As New SubastaConfData

        oSubConfData.Proceso = DevolverProceso(Anyo, Cod, GMN1)
        oSubConfData.ProcesoDef = DevolverProcesoDef(Anyo, Cod, GMN1)
        oSubConfData.ProceProves = DevolverProcesoProveedores(Anyo, Cod, GMN1)
        oSubConfData.Subasta = DevolverProcesoSubasta(Anyo, Cod, GMN1, True)
        If Not oSubConfData.Subasta.FechaUltPub Is Nothing Then
            oSubConfData.ProvesComunicados = DevolverSubastaProvesComunicados(Anyo, Cod, GMN1, Me.TzToUtc(oSubConfData.Subasta.FechaUltPub))
        End If
        oSubConfData.SubastaReglas = DevolverProcesoSubastaReglas(Anyo, Cod, GMN1)
        Dim oMonedaRule As New MonedasRule
        oSubConfData.Moneda = oMonedaRule.DevolverMoneda(oSubConfData.Proceso.Moneda, Idioma)
        Dim oIdiomasRule As New IdiomasRule
        oSubConfData.Idiomas = oIdiomasRule.CargarTodosLosIdiomas

        Return oSubConfData
    End Function

    '<summary>Devuelve los datos de subasta un proceso en modo subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>Objeto ProcesoSubasta</returns>    

    Public Function DevolverProcesoSubasta(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal DevolverEstructura As Boolean = False) As ProcesoSubasta
        Dim oSubasta As New ProcesoSubasta()

        Dim GSDBServer As New GSDatabaseServer.Procesos
        Dim ds As System.Data.DataSet = GSDBServer.DevolverProcesoSubasta(Anyo, Cod, GMN1)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim drSub As DataRow = ds.Tables(0).Rows(0)

            oSubasta.Anyo = drSub("ANYO")
            oSubasta.Codigo = drSub("PROCE")
            oSubasta.GMN1 = drSub("GMN1")
            oSubasta.Denominacion = drSub("DEN")
            oSubasta.Tipo = drSub("TIPO")
            oSubasta.Modo = drSub("MODO")
            If Not drSub.IsNull("PUBLICARFEC") Then oSubasta.FechaUltPub = Me.UtcToTz(drSub("PUBLICARFEC"))
            oSubasta.MinEsperaCierre = drSub("MINESPERACIERRE")
            oSubasta.NotificarEventos = SQLBinaryToBoolean(drSub("NOTIFICAREVENTOS"))
            If Not drSub.IsNull("FECAPESOBRE") Then oSubasta.FechaAperturaSobre = Me.UtcToTz(drSub("FECAPESOBRE"))
            If Not drSub.IsNull("MINJAPONESA") Then oSubasta.MinutosJaponesa = CType(drSub("MINJAPONESA"), Integer)
            oSubasta.VerProveGanador = SQLBinaryToBoolean(drSub("VERPROVEGANADOR"))
            oSubasta.VerPrecioGanador = SQLBinaryToBoolean(drSub("VERPRECIOGANADOR"))
            oSubasta.VerDetallePujas = SQLBinaryToBoolean(drSub("VERDETALLEPUJAS"))
            oSubasta.VerDesdePrimeraPuja = SQLBinaryToBoolean(drSub("VERDESDEPRIMERAPUJA"))
            oSubasta.BajadaMinGanador = SQLBinaryToBoolean(drSub("BAJMINGANADOR"))
            oSubasta.BajadaMinGanadorTipo = drSub("BAJMINGANADORTIPO")
            oSubasta.BajadaMinProve = SQLBinaryToBoolean(drSub("BAJMINPROVE"))
            oSubasta.BajadaMinProveTipo = drSub("BAJMINPROVETIPO")
            oSubasta.NotifAutom = drSub("NOTIFAUTOM")
            If Not drSub.IsNull("TEXTOFIN") Then
                oSubasta.TextoFin = drSub("TEXTOFIN")
                oSubasta.TextosFin = DevolverProcesoSubastaTextosFin(oSubasta.TextoFin)
            End If
            If Not drSub.IsNull("FECACT") Then oSubasta.FechaAct = drSub("FECACT")
            If Not drSub.IsNull("PRESTOTAL") Then oSubasta.PresupuestoTotal = drSub("PRESTOTAL")
            If Not drSub.IsNull("PRECSALIDA") Then oSubasta.PrecioSalida = drSub("PRECSALIDA")
            If Not drSub.IsNull("MINPUJGANADOR") Then oSubasta.MinPujaGanadora = drSub("MINPUJGANADOR")
            If Not drSub.IsNull("MINPUJPROVE") Then oSubasta.MinPujaProve = drSub("MINPUJPROVE")
            oSubasta.Estado = drSub("EST")
        End If

        If DevolverEstructura Then
            oSubasta.Grupos = DevolverProcesoGrupos(Anyo, Cod, GMN1, True)

            'Cálculo del presupuesto
            For Each oGrupo As ProcesoGrupo In oSubasta.Grupos
                oSubasta.Presupuesto += oGrupo.Presupuesto
            Next
        End If

        Return oSubasta
    End Function

    '<summary>Actualiza los datos de subasta y de proceso de un proceso</summary>
    '<param name="oProceso">Objeto Proceso</param>         
    '<param name="oProcesoSubasta">Objeto ProcesoSubasta</param> 

    Public Function ActualizarProcesoSubasta(ByVal oProceso As GSServerModel.Proceso, _
                                             ByVal oProcesoDef As GSServerModel.ProcesoDef, _
                                             ByVal oProcesoSubasta As GSServerModel.ProcesoSubasta, _
                                             ByVal oProcesoProves As GSServerModel.ProcesoProveedores, _
                                             ByVal oSubastaReglas As GSServerModel.ProcesoSubastaReglas, _
                                             ByVal oArchivos As GSServerModel.Files) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        If Not oProceso Is Nothing And Not oProcesoSubasta Is Nothing Then
            Dim oGSDBServer As New GSDatabaseServer.Procesos
            Dim dsProc As DataSet = oGSDBServer.DevolverProceso(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
            Dim dsProcDef As DataSet = oGSDBServer.DevolverProcesoDef(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
            Dim dsSub As DataSet = oGSDBServer.DevolverProcesoSubasta(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
            Dim dsProves As DataSet = oGSDBServer.DevolverProveedores(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
            Dim dsReglas As DataSet
            If Not oSubastaReglas Is Nothing Then
                dsReglas = oGSDBServer.DevolverSubastaReglas(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1)
            End If                  

            If Not dsProc Is Nothing AndAlso dsProc.Tables.Count > 0 AndAlso dsProc.Tables(0).Rows.Count > 0 Then
                'Caso de un registro de subasta nuevo
                If Not dsSub Is Nothing AndAlso dsSub.Tables.Count > 0 AndAlso dsSub.Tables(0).Rows.Count = 0 Then
                    Dim oNewRow As DataRow = dsSub.Tables(0).NewRow
                    oNewRow("Anyo") = oProceso.Anyo
                    oNewRow("GMN1") = oProceso.GMN1
                    oNewRow("PROCE") = oProceso.Codigo
                    dsSub.Tables(0).Rows.Add(oNewRow)
                End If

                Dim drProc As DataRow = dsProc.Tables(0).Rows(0)
                Dim drProcDef As DataRow = dsProcDef.Tables(0).Rows(0)
                Dim drSub As DataRow = dsSub.Tables(0).Rows(0)
                Dim dtProves As DataTable
                Dim dtReglas As DataTable
                If Not dsProves Is Nothing AndAlso dsProves.Tables.Count > 0 Then dtProves = dsProves.Tables(0)
                If Not dsReglas Is Nothing AndAlso dsReglas.Tables.Count > 0 Then dtReglas = dsReglas.Tables(0)

                'Comprobar que los datos del proceso y de la subasta no han sido modificados por otra persona
                If ComprobarActualizarProcesoSubasta(drProc, oProceso, drProcDef, oProcesoDef, drSub, oProcesoSubasta, dtProves, oProcesoProves, dtReglas, oSubastaReglas) Then
                    'Actualizar datos

                    'Fechas a UTC
                    'Proceso
                    If Not oProceso.FechaInicioSubasta Is Nothing Then oProceso.FechaInicioSubasta = Me.TzToUtc(oProceso.FechaInicioSubasta)
                    If Not oProceso.FechaLimiteOfertas Is Nothing Then oProceso.FechaLimiteOfertas = Me.TzToUtc(oProceso.FechaLimiteOfertas)
                    'Reglas
                    If Not oSubastaReglas Is Nothing AndAlso oSubastaReglas.Count > 0 Then
                        For Each oRegla As ProcesoSubastaRegla In oSubastaReglas
                            oRegla.FecAlta = Me.TzToUtc(oRegla.FecAlta)
                        Next
                    End If

                    ComprobarBajadasMinimas(oProcesoSubasta)

                    ActualizarDRProcesoDef(drProcDef, oProcesoDef)
                    ActualizarDRSubasta(drSub, oProcesoSubasta)
                    If Not dtProves Is Nothing And Not oProcesoProves Is Nothing Then ActualizarDTProves(dtProves, oProcesoProves)
                    Dim oRS As New SubastaReglaService(Me.Contexto)
                    If Not dtReglas Is Nothing Then oRS.ActualizarDTReglas(dtReglas, oSubastaReglas)

                    'Actualizar grupos
                    Dim dsGrupos As DataSet
                    Dim arItems(-1) As DataSet
                    If Not oProcesoSubasta.Grupos Is Nothing AndAlso oProcesoSubasta.Grupos.Count > 0 Then
                        Dim oPS As New ProcesoService(Me.Contexto)
                        oPS.TrasladarCambiosProcesoGrupos(oProceso.Anyo, oProceso.Codigo, oProceso.GMN1, oProcesoSubasta.Grupos, dsGrupos, arItems)
                    End If

                    'Actualizar textos fin
                    Dim dsTextos As DataSet
                    If Not oProcesoSubasta.TextosFin Is Nothing Then
                        Dim oTM As New TextoMultidiomaService
                        oTM.TrasladarCambiosTextos(oProcesoSubasta.TextoFin, oProcesoSubasta.TextosFin, dsTextos)
                    End If

                    oError = oGSDBServer.ActualizarProcesoSubasta(oProceso, dsProcDef, dsSub, dsGrupos, arItems, dsProves, dsReglas, oArchivos, dsTextos)

                    If oError.Number = ErroresGS.TESnoerror Then
                        'Eliminar archivos de reglas nuevas del dir. temporal
                        If Not oArchivos Is Nothing Then
                            Dim oFT As FileTransfer
                            For Each oArchivo As GSServerModel.File In oArchivos
                                If oArchivo.Nuevo Then
                                    oFT = New FileTransfer
                                    oFT.EndFileTransfer(oArchivo.ServerFileDirectory)
                                End If
                            Next
                        End If

                        'Registrar modificación subasta
                        Dim sUsuCod As String = String.Empty
                        Dim oContexto As Contexto = System.Threading.Thread.GetData(System.Threading.Thread.GetNamedDataSlot("Contexto"))
                        If Not oContexto Is Nothing Then
                            sUsuCod = oContexto.UsuCod
                        End If

                        Dim oGestSeg As New GSDatabaseServer.GestorSeguridad
                        oGestSeg.RegistrarAccion(sUsuCod, TiposDeDatos.AccionesGS.ACCModifSubasta, "Anyo:" & oProceso.Anyo.ToString & " GMN1:" & oProceso.GMN1 & " Proce:" & oProceso.Codigo.ToString)
                    End If
                Else
                    oError.Number = ErroresGS.TESInfActualModificada
                End If
            End If
        End If

        Return oError
    End Function

    '<summary>Comprueba los valores para establecer las bajadas mínimas. Si no se establecen borra los importes</summary>
    '<param name="oProcesoSubasta">Objeto de tipo ProcesoSubasta</param>                 

    Private Sub ComprobarBajadasMinimas(ByRef oProcesoSubasta As GSServerModel.ProcesoSubasta)
        If Not oProcesoSubasta Is Nothing Then
            If Not oProcesoSubasta.BajadaMinGanador Or Not oProcesoSubasta.BajadaMinProve Then
                If Not oProcesoSubasta.BajadaMinGanador Then oProcesoSubasta.MinPujaGanadora = Nothing
                If Not oProcesoSubasta.BajadaMinProve Then oProcesoSubasta.MinPujaProve = Nothing

                If Not oProcesoSubasta.Grupos Is Nothing AndAlso oProcesoSubasta.Grupos.Count > 0 Then
                    For Each oGrupo As GSServerModel.ProcesoGrupo In oProcesoSubasta.Grupos
                        If Not oProcesoSubasta.BajadaMinGanador Then oGrupo.MinPujaGanadora = Nothing
                        If Not oProcesoSubasta.BajadaMinProve Then oGrupo.MinPujaProve = Nothing

                        If Not oGrupo.Items Is Nothing AndAlso oGrupo.Items.Count > 0 Then
                            For Each oItem As GSServerModel.Item In oGrupo.Items
                                If Not oProcesoSubasta.BajadaMinGanador Then oItem.MinPujaGanadora = Nothing
                                If Not oProcesoSubasta.BajadaMinProve Then oItem.MinPujaProve = Nothing
                            Next
                        End If
                    Next
                End If
            End If
        End If
    End Sub

    '<summary>Realiza las comprobaciones necesarias previas a la actualización de la subasta</summary>
    '<param name="drProc">datos proceso</param>         
    '<param name="drSub">Datos subasta</param> 
    '<param name="dsProve">Datos proveedores</param> 
    '<returns"dsProve">Si se puede realizar la actualización</returns> 

    Private Function ComprobarActualizarProcesoSubasta(ByVal drProc As DataRow, ByVal oProceso As GSServerModel.Proceso, _
                                                       ByVal drProcDef As DataRow, ByVal oProcesoDef As GSServerModel.ProcesoDef, _
                                                       ByVal drSub As DataRow, ByVal oProcesoSubasta As GSServerModel.ProcesoSubasta, _
                                                       ByVal dtProve As DataTable, ByVal oProcesoProves As GSServerModel.ProcesoProveedores, _
                                                       ByVal dtReglas As DataTable, ByVal oSubastaReglas As GSServerModel.ProcesoSubastaReglas) As Boolean
        Dim blnOK As Boolean = True

        If Not Date.Equals(oProceso.FechaAct, drProc("FECACT")) Or Not Date.Equals(oProcesoDef.FecAct, drProcDef("FECACT")) Or _
            (drSub.RowState = DataRowState.Modified AndAlso Not Date.Equals(oProcesoSubasta.FechaAct, drSub("FECACT"))) Then
            blnOK = False        
        End If

        Return blnOK
    End Function

    '<summary>Traslada los cambios del objeto subasta al datarow</summary>
    '<param name="drSub">DataRow con los datos de subasta</param>         
    '<param name="oProcesoSubasta">Objeto ProcesoSubasta</param> 

    Private Sub ActualizarDRSubasta(ByRef drSub As DataRow, ByVal oProcesoSubasta As GSServerModel.ProcesoSubasta)
        drSub("TIPO") = oProcesoSubasta.Tipo
        drSub("MODO") = oProcesoSubasta.Modo
        If Not oProcesoSubasta.FechaUltPub Is Nothing Then
            drSub("PUBLICARFEC") = Me.TzToUtc(oProcesoSubasta.FechaUltPub)
        Else
            drSub("PUBLICARFEC") = DBNull.Value
        End If
        drSub("MINESPERACIERRE") = oProcesoSubasta.MinEsperaCierre
        drSub("NOTIFICAREVENTOS") = BooleanToSQLBinaryDBNull(oProcesoSubasta.NotificarEventos)
        If Not oProcesoSubasta.Tipo = TipoSubasta.SobreCerrado Then oProcesoSubasta.FechaAperturaSobre = Nothing
        If Not oProcesoSubasta.FechaAperturaSobre Is Nothing Then
            drSub("FECAPESOBRE") = Me.TzToUtc(oProcesoSubasta.FechaAperturaSobre)
        Else
            drSub("FECAPESOBRE") = DBNull.Value
        End If
        drSub("MINJAPONESA") = IntToSQLIntegerDBNull(oProcesoSubasta.MinutosJaponesa)
        drSub("VERPROVEGANADOR") = BooleanToSQLBinaryDBNull(oProcesoSubasta.VerProveGanador)
        drSub("VERPRECIOGANADOR") = BooleanToSQLBinaryDBNull(oProcesoSubasta.VerPrecioGanador)
        drSub("VERDETALLEPUJAS") = BooleanToSQLBinaryDBNull(oProcesoSubasta.VerDetallePujas)
        drSub("VERDESDEPRIMERAPUJA") = BooleanToSQLBinaryDBNull(oProcesoSubasta.VerDesdePrimeraPuja)
        drSub("BAJMINGANADOR") = BooleanToSQLBinaryDBNull(oProcesoSubasta.BajadaMinGanador)
        drSub("BAJMINGANADORTIPO") = oProcesoSubasta.BajadaMinGanadorTipo
        drSub("BAJMINPROVE") = BooleanToSQLBinaryDBNull(oProcesoSubasta.BajadaMinProve)
        drSub("BAJMINPROVETIPO") = oProcesoSubasta.BajadaMinProveTipo
        drSub("NOTIFAUTOM") = oProcesoSubasta.NotifAutom
        drSub("TEXTOFIN") = IntToSQLIntegerDBNull(oProcesoSubasta.TextoFin)
        drSub("PRECSALIDA") = DoubleToSQLDoubleDBNull(oProcesoSubasta.PrecioSalida)
        drSub("MINPUJGANADOR") = DoubleToSQLDoubleDBNull(oProcesoSubasta.MinPujaGanadora)
        drSub("MINPUJPROVE") = DoubleToSQLDoubleDBNull(oProcesoSubasta.MinPujaProve)
        drSub("EST") = oProcesoSubasta.Estado
    End Sub

    '<summary>Devuelve un resumen de las pujas de los proveedores de una subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<param name="Grupo">Opcional: Grupo</param>
    '<param name="Item">Opcional: Item</param>
    '<returns>array de objetos ProceProveResumenSubasta</returns> 

    Public Function DevolverSubastaProveedoresResumen(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal Grupo As Nullable(Of Integer) = Nothing, Optional ByVal Item As Nullable(Of Integer) = Nothing) As ProceProvesResumenSubasta
        Dim oProceRes As New ProceProvesResumenSubasta

        Dim GSDBServer As New GSDatabaseServer.Procesos
        Dim ds As System.Data.DataSet = GSDBServer.DevolverSubastaProveedoresResumen(Anyo, Cod, GMN1, Grupo, Item)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            For Each drProceProveRes As DataRow In ds.Tables(0).Rows
                Dim oProceProveRes As New ProceProveResumenSubasta
                oProceProveRes.Anyo = drProceProveRes("ANYO")
                oProceProveRes.Proceso = drProceProveRes("PROCE")
                oProceProveRes.GMN1 = drProceProveRes("GMN1")
                oProceProveRes.Proveedor = drProceProveRes("PROVE")
                oProceProveRes.Denominacion = drProceProveRes("DEN")
                If Not drProceProveRes.IsNull("FECULTPUJA") Then oProceProveRes.FechaUltimaPuja = Me.UtcToTz(drProceProveRes("FECULTPUJA"))
                If Not drProceProveRes.IsNull("PRECIO") Then oProceProveRes.Precio = drProceProveRes("PRECIO")
                If Not drProceProveRes.IsNull("CANTIDAD") Then oProceProveRes.Cantidad = drProceProveRes("CANTIDAD")
                If Not drProceProveRes.IsNull("VALORULTPUJA") Then oProceProveRes.ValorUltimaPuja = drProceProveRes("VALORULTPUJA")
                oProceProveRes.NumPujas = drProceProveRes("NUMPUJAS")
                If Not drProceProveRes.IsNull("AHORRO") Then oProceProveRes.Ahorro = drProceProveRes("AHORRO")
                If Not drProceProveRes.IsNull("PORCENTAHORRO") Then oProceProveRes.PorcentAhorro = drProceProveRes("PORCENTAHORRO")
                If Not drProceProveRes.IsNull("POSICIONPUJA") Then oProceProveRes.PosicionPuja = CType(drProceProveRes("POSICIONPUJA"), Integer)
                oProceProveRes.Conectado = SQLBinaryToBoolean(drProceProveRes("CONECTADO"))
                If Not drProceProveRes.IsNull("FECINI") Then oProceProveRes.FechaInicio = Me.UtcToTz(drProceProveRes("FECINI"))
                If Not drProceProveRes.IsNull("FECFIN") Then oProceProveRes.FechaFin = Me.UtcToTz(drProceProveRes("FECFIN"))

                oProceRes.Add(oProceProveRes)
            Next
        End If

        Return oProceRes
    End Function

    '<summary>Devuelve los eventos de una subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>    
    '<returns>array de objetos ProceProveResumenSubasta</returns> 

    Public Function DevolverSubastaEventos(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String) As SubastaEventos
        Dim oEventos As New SubastaEventos

        Dim GSDBServer As New GSDatabaseServer.Procesos
        Dim ds As System.Data.DataSet = GSDBServer.DevolverSubastaEventos(Anyo, Proce, GMN1)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            For Each drEvento As DataRow In ds.Tables(0).Rows
                Dim oEvento As New SubastaEvento

                oEvento.ID = drEvento("ID")
                oEvento.Anyo = drEvento("ANYO")
                oEvento.Proceso = drEvento("PROCE")
                oEvento.GMN1 = drEvento("GMN1")

                oEvento.Accion = CType(drEvento("ACCION"), AccionSubasta)
                oEvento.Fecha = Me.UtcToTz(drEvento("FECHA"))
                If Not drEvento.IsNull("EXTEN") Then oEvento.Extension = drEvento("EXTEN")
                If Not drEvento.IsNull("FECFINHIST") Then oEvento.FechaFinHist = Me.UtcToTz(drEvento("FECFINHIST"))
                If Not drEvento.IsNull("MODREINISUB") Then oEvento.ModoReinicioSubasta = CType(drEvento("MODREINISUB"), ModoReinicioSubasta)
                If Not drEvento.IsNull("MINREINISUB") Then oEvento.MinReinicioSubasta = CType(drEvento("MINREINISUB"), Integer)
                If Not drEvento.IsNull("FECREINISUB") Then oEvento.FechaReinicioSubasta = Me.UtcToTz(drEvento("FECREINISUB"))
                If Not drEvento.IsNull("MODINCTSUB") Then oEvento.ModoIncTiempoSubasta = CType(drEvento("MODINCTSUB"), ModoIncTiempoSubasta)
                If Not drEvento.IsNull("MININCTSUB") Then oEvento.MinIncTiempoSubasta = CType(drEvento("MININCTSUB"), Integer)
                If Not drEvento.IsNull("PROVE") Then oEvento.Proveedor = drEvento("PROVE")

                oEventos.Add(oEvento)
            Next
        End If

        Return oEventos
    End Function

    '<summary>Devuelve las pujas de una subasta</summary>
    '<param name="Anyo">Anyo</param>         
    '<param name="Codigo">Codigo</param> 
    '<param name="GMN1">GMN1</param>     
    '<returns>Objeto de tipo ProcesoSubastaPujas con las pujas de los proveedores</returns>

    Public Function DevolverProcesoSubastaPujas(ByVal Anyo As Integer, ByVal Codigo As Integer, ByVal GMN1 As String, Optional ByVal Grupo As Nullable(Of Integer) = Nothing, Optional ByVal Item As Nullable(Of Integer) = Nothing) As GSServerModel.ProcesoSubastaPujas
        ' ''*************************************************
        ' ''ProceProvePuja: Es una puja. Propiedades: Valor y Fecha
        ' ''ProceProvePujas: Colección de ProceProvePuja. Propiedades: Prove
        ' ''ProcesoSubastaPujas: Colección de ProceProvePujas. Propiedades: Anyo, GMN1, Codigo, GruposPujas (pujas para los grupos, colección de ProceGrupoPujas)
        ' ''ProceGruposPujas: Colección de ProceGrupoPujas
        ' ''ProceGrupoPujas: Colección de ProceProvePujas. Propiedades: Anyo, GMN1, Codigo, ID, ItemsPujas (pujas para los items, colección de ProceItemPujas)
        ' ''ProceItemsPujas: Colección de ProceItemPujas
        ' ''PorceItemPujas: Colección de ProceProvePujas. Propiedades: Anyo, GMN1, Codigo, ID 
        ' ''
        ' ''ProcesoSubastaPujas(Of ProceProvePujas(Of ProceProvePuja))
        ' ''ProcesoSubastaPujas.GruposPujas --> ProceGruposPujas(Of ProceGrupoPujas(Of ProceProvePujas(Of ProceProvePuja))
        ' ''ProcesoSubastaPujas.GruposPujas.ItemsPujas --> ProceItemsPujas(Of ProceItemPujas(Of ProceProvePujas(Of ProceProvePuja))
        ' ''*************************************************

        Dim oSubastaPujas As New ProcesoSubastaPujas
        oSubastaPujas.Anyo = Anyo
        oSubastaPujas.GMN1 = GMN1
        oSubastaPujas.Proce = Codigo

        Dim GSDBServer As New GSDatabaseServer.Procesos
        Dim ds As System.Data.DataSet = GSDBServer.DevolverProcesoSubastaPujas(Anyo, Codigo, GMN1, Grupo, Item)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            'Pujas del proceso
            Dim dtPujasProceso As DataTable = ds.Tables(0)
            If Not dtPujasProceso Is Nothing AndAlso dtPujasProceso.Rows.Count > 0 Then
                Dim strProve As String = dtPujasProceso.Rows(0)("PROVE")
                Dim strDenProve As String = dtPujasProceso.Rows(0)("DEN")
                Dim oProvePujas As New ProceProvePujas
                oProvePujas.Prove = strProve
                oProvePujas.Denominacion = strDenProve
                For Each drPuja As DataRow In dtPujasProceso.Rows
                    'Cambio de proveedor: Añadir las pujas para ese proveedor
                    If Not strProve.Equals(drPuja("PROVE")) Then
                        oSubastaPujas.Add(oProvePujas)

                        strProve = drPuja("PROVE")
                        strDenProve = drPuja("DEN")
                        oProvePujas = New ProceProvePujas
                        oProvePujas.Prove = strProve
                        oProvePujas.Denominacion = strDenProve
                    End If

                    Dim oPuja As New ProceProvePuja
                    oPuja.FechaPuja = Me.UtcToTz(drPuja("FECREC"))
                    If Not drPuja.IsNull("IMPORTE") Then oPuja.Valor = drPuja("IMPORTE")

                    'Añadir la puja a la colección de pujas
                    oProvePujas.Add(oPuja)
                Next
                oSubastaPujas.Add(oProvePujas)
            End If

            'Pujas de los grupos
            Dim dtPujasGrupos As DataTable = ds.Tables(1)
            If Not dtPujasGrupos Is Nothing AndAlso dtPujasGrupos.Rows.Count > 0 Then
                Dim oGruposPujas As New ProceGruposPujas

                Dim oGrupoPujas As New ProceGrupoPujas
                oGrupoPujas.Anyo = Anyo
                oGrupoPujas.Proce = Codigo
                oGrupoPujas.GMN1 = GMN1
                oGrupoPujas.ID = dtPujasGrupos.Rows(0)("GRUPO")
                Dim iGrupo As Integer = dtPujasGrupos.Rows(0)("GRUPO")
                Dim strProve As String = dtPujasProceso.Rows(0)("PROVE")
                Dim strDenProve As String = dtPujasProceso.Rows(0)("DEN")
                Dim oProvePujas As New ProceProvePujas
                oProvePujas.Prove = strProve
                oProvePujas.Denominacion = strDenProve
                For Each drPuja As DataRow In dtPujasGrupos.Rows
                    'Cambio de grupo
                    If iGrupo <> drPuja("GRUPO") Then
                        oGrupoPujas.Add(oProvePujas)
                        oGruposPujas.Add(oGrupoPujas)

                        iGrupo = drPuja("GRUPO")

                        oGrupoPujas = New ProceGrupoPujas
                        oGrupoPujas.Anyo = Anyo
                        oGrupoPujas.Proce = Codigo
                        oGrupoPujas.GMN1 = GMN1
                        oGrupoPujas.ID = iGrupo

                        strProve = drPuja("PROVE")
                        strDenProve = drPuja("DEN")
                        oProvePujas = New ProceProvePujas
                        oProvePujas.Prove = strProve
                        oProvePujas.Denominacion = strDenProve
                    Else
                        If Not strProve.Equals(drPuja("PROVE")) Then
                            oGrupoPujas.Add(oProvePujas)

                            strProve = drPuja("PROVE")
                            strDenProve = drPuja("DEN")
                            oProvePujas = New ProceProvePujas
                            oProvePujas.Prove = strProve
                            oProvePujas.Denominacion = strDenProve
                        End If
                    End If

                    Dim oPuja As New ProceProvePuja
                    oPuja.FechaPuja = Me.UtcToTz(drPuja("FECREC"))
                    If Not drPuja.IsNull("IMPORTE") Then oPuja.Valor = drPuja("IMPORTE")

                    'Añadir la puja a la colección de pujas
                    oProvePujas.Add(oPuja)
                Next
                oGrupoPujas.Add(oProvePujas)
                oGruposPujas.Add(oGrupoPujas)

                oSubastaPujas.GruposPujas = oGruposPujas
            End If

            'Pujas del item
            If Not Item Is Nothing AndAlso ds.Tables.Count > 2 Then
                Dim dtPujasItems As DataTable = ds.Tables(2)
                If Not dtPujasItems Is Nothing AndAlso dtPujasItems.Rows.Count > 0 Then
                    Dim oGrupoPujas As ProceGrupoPujas = oSubastaPujas.GruposPujas.Item(Grupo)

                    Dim oItemsPujas As New ProceItemsPujas
                    Dim oItemPujas As New ProceItemPujas
                    oItemPujas.Anyo = Anyo
                    oItemPujas.Proce = Codigo
                    oItemPujas.GMN1 = GMN1
                    oItemPujas.ID = dtPujasItems.Rows(0)("ITEM")

                    Dim strProve As String = dtPujasItems.Rows(0)("PROVE")
                    Dim strDenProve As String = dtPujasItems.Rows(0)("DEN")
                    Dim oProvePujas As New ProceProvePujas
                    oProvePujas.Prove = strProve
                    oProvePujas.Denominacion = strDenProve
                    For Each drPuja As DataRow In dtPujasItems.Rows
                        If Not strProve.Equals(drPuja("PROVE")) Then
                            oItemPujas.Add(oProvePujas)

                            strProve = drPuja("PROVE")
                            strDenProve = drPuja("DEN")
                            oProvePujas = New ProceProvePujas
                            oProvePujas.Prove = strProve
                            oProvePujas.Denominacion = strDenProve
                        End If

                        Dim oPuja As New ProceProvePuja
                        oPuja.FechaPuja = Me.UtcToTz(drPuja("FECREC"))
                        If Not drPuja.IsNull("IMPORTE") Then oPuja.Valor = drPuja("IMPORTE")

                        'Añadir la puja a la colección de pujas
                        oProvePujas.Add(oPuja)
                    Next
                    oItemPujas.Add(oProvePujas)

                    oItemsPujas.Add(oItemPujas)
                    oGrupoPujas.ItemsPujas = oItemsPujas
                End If
            End If
        End If

        Return oSubastaPujas
    End Function

    '<summary>Lleva a cabo la acción indicada sobre un proceso de subasta</summary>
    '<param name="Evento">Acción a llevar a cabo</param>              
    '<returns>Objeto de tipo GSException con el error si se ha producido</returns>

    Public Function AccionSubasta(ByVal Evento As GSServerModel.ProcesoSubastaEvento) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        If Not Evento Is Nothing Then
            'Fechas a UTC antes de pasar a BD
            Evento.Fecha = Me.TzToUtc(Evento.Fecha)
            If Not Evento.FechaReinicioSub Is Nothing Then Evento.FechaReinicioSub = Me.TzToUtc(Evento.FechaReinicioSub)

            Dim oGSDBServer As New GSDatabaseServer.Procesos
            oError = oGSDBServer.AccionSubasta(Evento)
        End If

        Return oError
    End Function

#End Region

#Region " ProcesoSubastaReglas "

    '<summary>Devuelve las reglas de subasta de un proceso en modo subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>arry de objetos ProcesoSubastaRegla</returns>    

    Public Function DevolverProcesoSubastaReglas(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As ProcesoSubastaReglas
        Dim oSRS As New SubastaReglaService(Me.Contexto)
        Return oSRS.DevolverSubastaReglas(Anyo, Cod, GMN1)
    End Function

    '<summary>Actualiza las reglas de una subasta</summary>
    '<param name="Anyo">Anyo</param>    
    '<param name="Cod">Cod</param>    
    '<param name="GMN1">GMN1</param>    
    '<param name="arSubastaReglas">Array de objetos ProcesoSubastaRegla</param>         
    '<returns>Objeto de tipo GSServerModel.GSException con el error si se ha producido</returns>

    Public Function ActualizarProcesoSubastaReglas(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oSubastaReglas As GSServerModel.ProcesoSubastaReglas, ByVal oArchivos As GSServerModel.Files) As GSServerModel.GSException
        Dim oSRS As New SubastaReglaService(Me.Contexto)
        Return oSRS.ActualizarSubastaReglas(Anyo, Cod, GMN1, oSubastaReglas, oArchivos)
    End Function

    '<summary>Devuelve un chunk del archivo correspondiente a una regla</summary>
    '<param name="Offset">Offset</param>    
    '<param name="ChunkSize">ChunkSize</param>    
    '<param name="ID">ID del archivo</param>        
    '<returns>array de bytes con el trozo de archivo pedido</returns>

    Public Function DevolverArchivoSubastaRegla(ByVal Offset As Integer, ByVal ChunkSize As Integer, ByVal ID As Integer) As Byte()
        Dim oSRS As New SubastaReglaService(Me.Contexto)
        Return oSRS.DevolverArchivoSubastaRegla(Offset, ChunkSize, ID)
    End Function

#End Region

#Region " ProcesoProveedor "

    '<summary>Devuelve los proveedores asignados as un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>array de objetos ProcesoProveedor</returns>    

    Public Function DevolverProcesoProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As ProcesoProveedores
        Dim oPPS As New ProcesoProveedorService
        Return oPPS.DevolverProveedores(Anyo, Cod, GMN1)
    End Function

    '<summary>Devuelve los proveedores a los que se les ha comunicado la subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>array de objetos ProcesoProveedor</returns>

    Public Function DevolverSubastaProvesComunicados(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal dtFechaCom As Date) As ProcesoProveedores
        Dim oPPS As New ProcesoProveedorService
        Return oPPS.DevolverProvesComunicados(Anyo, Cod, GMN1, dtFechaCom)
    End Function

    '<summary>Devuelve los proveedores asignados a una subasta</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>array de objetos ProcesoProveedor</returns>    

    Public Function DevolverSubastaProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String) As ProcesoProveedores
        Dim oPPS As New ProcesoProveedorService
        Return oPPS.DevolverProveedores(Anyo, Cod, GMN1, True)
    End Function

    '<summary>Actualiza los proveedores de un proceso</summary>
    '<param name="arProceProve">Array de objetos ProcesoProveedor</param>         

    Public Function ActualizarProcesoProveedores(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oProceProves As GSServerModel.ProcesoProveedores) As GSServerModel.GSException
        Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
        oError.Number = ErroresGS.TESnoerror
        oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

        If Not oProceProves Is Nothing AndAlso oProceProves.Count > 0 Then
            Dim oGSDBServer As New GSDatabaseServer.Procesos

            Dim ds As DataSet = oGSDBServer.DevolverProveedores(Anyo, Cod, GMN1)

            'Comprobar que existe el proceso
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                'Actualizar datos
                ActualizarDTProves(ds.Tables(0), oProceProves)

                oError = oGSDBServer.ActualizarProcesoProveedores(ds)
            Else
                oError.Number = ErroresGS.TESProcesoNoExiste
            End If
        End If

        Return oError
    End Function

    '<summary>Traslada los cambios del objeto proceso al datarow</summary>
    '<param name="drProce">DataRow con los datos del proceso</param>         
    '<param name="oProceso">Objeto Proceso</param> 

    Private Sub ActualizarDTProves(ByRef dtProves As DataTable, ByVal oProves As GSServerModel.ProcesoProveedores)
        dtProves.DefaultView.Sort = "PROVE"
        For Each oProve As ProcesoProveedor In oProves
            Dim iIndex As Integer = dtProves.DefaultView.Find(oProve.Proveedor)
            If iIndex >= 0 Then
                Dim drProve As DataRow = dtProves.DefaultView(iIndex).Row

                'drProve("EQP") = oProve.
                'drProve("COM") = oProve.
                'drProve("PUB") = oProve.º
                'drProve("FECPUB") = oProve.
                'drProve("OFE") = oProve.
                'drProve("SUPERADA") = oProve.
                'drProve("NUMOBJ") = oProve.
                'drProve("OBJNUEVOS") = oProve.
                'drProve("CERRADO") = oProve.
                'drProve("NO_OFE") = oProve.
                'drProve("FEC_NO_OFE") = oProve.
                'drProve("MOTIVO_NO_OFE") = oProve.
                'drProve("AVISADODESPUB") = oProve.
                'drProve("PORTAL_NO_OFE") = oProve.
                'drProve("USU_NO_OFE") = oProve.
                'drProve("NOMUSU_NO_OFE") = oProve.
                drProve("SUBASTA") = oProve.Subasta
            End If
        Next
    End Sub

#End Region

#Region " ProcesoGrupo "

    '<summary>Devuelve los items asignados al grupo de un proceso</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>array de objetos Item</returns>    

    Public Function DevolverProcesoGrupoItems(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal GRUPO As Integer) As Items
        Dim oIS As New ItemService(Me.Contexto)
        Return oIS.DevolverItems(Anyo, Cod, GMN1, GRUPO)
    End Function

    '<summary>Actualiza los items de un grupo</summary>
    '<param name="arItems">Array de objetos Item</param>         

    Public Function ActualizarProcesoGrupoItems(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal ID As Integer, ByVal oItems As GSServerModel.Items) As GSServerModel.GSException
        Dim oPGS As New ProcesoGrupoService(Me.Contexto)
        Return oPGS.ActualizarProcesoGrupoItems(Anyo, Cod, GMN1, ID, oItems)
    End Function

#End Region

#Region " ProcesoSubastaTextosFin "

    '<summary>Devuelve los textos de fin de subasta</summary>
    '<param name="ID">ID de los textos</param>    
    '<returns>arry de objetos TextoMultiidioma</returns>    

    Public Function DevolverProcesoSubastaTextosFin(ByVal ID As Integer) As TextosMultiidioma
        Dim oSRS As New TextoMultidiomaService
        Return oSRS.DevolverTextos(ID)
    End Function

#End Region

#Region " Clase SubastaReglaService "

    Private Class SubastaReglaService
        Inherits RuleBase

        Public Sub New(ByVal oCtx As GSServerModel.Contexto)
            MyBase.New(oCtx)
        End Sub

        '<summary>Devuelve reglas de subasta</summary>
        '<param name="Anyo">Año</param>
        '<param name="Cod">Código</param>
        '<param name="GMN1">GMN1</param>
        '<returns>Objeto ProcesoSubastaReglas</returns>    

        Public Function DevolverSubastaReglas(Optional ByVal Anyo As Integer = -1, Optional ByVal Cod As Integer = -1, Optional ByVal GMN1 As String = Nothing) As ProcesoSubastaReglas
            Dim oSubastaReglas As New ProcesoSubastaReglas

            Dim GSDBServer As New GSDatabaseServer.Procesos
            Dim ds As System.Data.DataSet = GSDBServer.DevolverSubastaReglas(Anyo, Cod, GMN1)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oRegla As New ProcesoSubastaRegla
                    oRegla.Anyo = oRow("ANYO")
                    oRegla.Proce = oRow("PROCE")
                    oRegla.GMN1 = oRow("GMN1")
                    oRegla.ID = oRow("ID")
                    oRegla.Nombre = oRow("NOM")
                    If Not oRow.IsNull("COM") Then oRegla.Comentario = oRow("COM")
                    If Not oRow.IsNull("FECACT") Then oRegla.FecAct = oRow("FECACT")
                    If Not oRow.IsNull("FECALTA") Then oRegla.FecAlta = Me.UtcToTz(oRow("FECALTA"))
                    If Not oRow.IsNull("ADJUN_PROCE") Then oRegla.AdjunProce = oRow("ADJUN_PROCE")
                    If Not oRow.IsNull("CONT") Then
                        oRegla.Cont = oRow("CONT")
                    Else
                        oRegla.Cont = -1
                    End If
                    If Not oRow.IsNull("DGUID") Then oRegla.DGuid = oRow("DGUID")
                    If Not oRow.IsNull("NumBytes") Then oRegla.TamañoArchivo = oRow("NumBytes")

                    oSubastaReglas.Add(oRegla)
                Next
            End If

            Return oSubastaReglas
        End Function

        '<summary>Devuelve el archivo correspondiente a una regla de subasta</summary>
        '<param name="ID">ID del archivo</param>        
        '<returns></returns>    

        Public Function DevolverArchivoSubastaRegla(ByVal Offset As Integer, ByVal ChunkSize As Integer, ByVal ID As Integer) As Byte()
            Dim Buffer(ChunkSize) As Byte

            Dim GSDBServer As New GSDatabaseServer.ArchivoSubastaReglas
            Dim NumBytes As Integer = GSDBServer.DevolverArchivoSubastaRegla(Buffer, Offset, ChunkSize, ID)
            ReDim Preserve Buffer(NumBytes - 1)

            DevolverArchivoSubastaRegla = Buffer
        End Function

        '<summary>Actualiza las reglas de una subasta</summary>
        '<param name="Anyo">Anyo</param>    
        '<param name="Cod">Cod</param>    
        '<param name="GMN1">GMN1</param>    
        '<param name="arSubastaReglas">Array de objetos ProcesoSubastaRegla</param>         
        '<returns>Objeto de tipo GSServerModel.GSException con el error si se ha producido</returns>

        Public Function ActualizarSubastaReglas(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oSubastaReglas As GSServerModel.ProcesoSubastaReglas, ByVal oArchivos As GSServerModel.Files) As GSServerModel.GSException
            Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
            oError.Number = ErroresGS.TESnoerror
            oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

            If Not oSubastaReglas Is Nothing AndAlso oSubastaReglas.Count > 0 Then
                Dim oGSDBServer As New GSDatabaseServer.Procesos

                Dim ds As DataSet = oGSDBServer.DevolverSubastaReglas(Anyo, Cod, GMN1)

                'Comprobar que hay registros
                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    'Actualizar datos
                    ActualizarDTReglas(ds.Tables(0), oSubastaReglas)

                    oError = oGSDBServer.ActualizarProcesoSubastaReglas(ds, oArchivos)
                Else
                    oError.Number = ErroresGS.TESProcesoNoExiste
                End If
            End If

            Return oError
        End Function

        '<summary>Traslada los cambios del objeto proceso al datarow</summary>
        '<param name="dtReglas">DataTable con las reglas a actualizar</param>         
        '<param name="arReglas">Array de objetos ProcesoSubastaRegla con los nuevos datos</param> 

        Public Sub ActualizarDTReglas(ByRef dtReglas As DataTable, ByVal oReglas As GSServerModel.ProcesoSubastaReglas)
            'Borrados
            For Each oRow As DataRow In dtReglas.Select
                Dim bEncontrado As Boolean = False
                For Each oRegla As GSServerModel.ProcesoSubastaRegla In oReglas
                    If oRow("ANYO") = oRegla.Anyo And _
                        oRow("GMN1") = oRegla.GMN1 And _
                        oRow("PROCE") = oRegla.Proce And _
                        oRow("ID") = oRegla.ID Then
                        bEncontrado = True
                        Exit For
                    End If
                Next

                If Not bEncontrado Then oRow.Delete()
            Next

            'Inserciones y actualizaciones
            dtReglas.DefaultView.Sort = "ID"
            For Each oRegla As ProcesoSubastaRegla In oReglas
                Dim drRegla As DataRow

                Dim iIndex As Integer = dtReglas.DefaultView.Find(oRegla.ID)
                If iIndex >= 0 Then
                    drRegla = dtReglas.DefaultView(iIndex).Row
                Else
                    'Nueva regla
                    drRegla = dtReglas.NewRow
                    drRegla("ANYO") = oRegla.Anyo
                    drRegla("GMN1") = oRegla.GMN1
                    drRegla("PROCE") = oRegla.Proce
                    drRegla("FECALTA") = oRegla.FecAlta
                End If

                drRegla("NOM") = oRegla.Nombre
                If Not oRegla.Comentario Is Nothing Then
                    drRegla("COM") = oRegla.Comentario
                Else
                    drRegla("COM") = DBNull.Value
                End If
                If oRegla.Cont <> -1 Then
                    drRegla("CONT") = oRegla.Cont
                Else
                    drRegla("CONT") = DBNull.Value
                End If
                drRegla("DGUID") = oRegla.DGuid
                'Archivo
                'If Not oRegla.Data Is Nothing Then
                '    Dim sPath As String = oRow("PATHNAME")
                '    Dim arTransactionContext() As Byte = CType(oRow("TRANSACTIONCONTEXT"), Byte()) 'reader.GetSqlBytes(1).Buffer;

                '    'Create the SqlFileStream
                '    Dim oFS As System.IO.Stream = New System.Data.SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Read, FileOptions.SequentialScan, 0)
                'End If

                If Not iIndex >= 0 Then dtReglas.Rows.Add(drRegla)
            Next
        End Sub

    End Class

#End Region

#Region " Clase ProcesoProveedorService "

    Private Class ProcesoProveedorService

        '<summary>Devuelve proveedores</summary>
        '<param name="Anyo">Año</param>
        '<param name="Cod">Código</param>
        '<param name="GMN1">GMN1</param>
        '<returns>array de objetos ProcesoProveedor</returns>    

        Public Function DevolverProveedores(Optional ByVal Anyo As Integer = -1, Optional ByVal Cod As Integer = -1, Optional ByVal GMN1 As String = Nothing, Optional ByVal Subasta As Boolean = False) As ProcesoProveedores
            Dim oProcProves As New ProcesoProveedores

            Dim GSDBServer As New GSDatabaseServer.Procesos
            Dim ds As System.Data.DataSet = GSDBServer.DevolverProveedores(Anyo, Cod, GMN1, Subasta)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oProceProve As New ProcesoProveedor
                    oProceProve.Anyo = oRow("ANYO")
                    oProceProve.Proceso = oRow("PROCE")
                    oProceProve.GMN1 = oRow("GMN1")
                    oProceProve.Proveedor = oRow("PROVE")
                    oProceProve.Denominacion = oRow("DEN")
                    If Not oRow.IsNull("FSP_COD") Then oProceProve.CodigoPortal = oRow("FSP_COD")
                    oProceProve.Subasta = oRow("SUBASTA")

                    oProcProves.Add(oProceProve)
                Next
            End If

            Return oProcProves
        End Function

        '<summary>Devuelve proveedores</summary>
        '<param name="Anyo">Año</param>
        '<param name="Cod">Código</param>
        '<param name="GMN1">GMN1</param>
        '<returns>array de objetos ProcesoProveedor</returns>

        Public Function DevolverProvesComunicados(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal dtFechaCom As Date) As ProcesoProveedores
            Dim oProcProves As New ProcesoProveedores

            Dim GSDBServer As New GSDatabaseServer.Procesos
            Dim ds As System.Data.DataSet = GSDBServer.DevolverProveedoresComunicados(Anyo, Cod, GMN1, dtFechaCom)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oProceProve As New ProcesoProveedor
                    oProceProve.Anyo = oRow("ANYO")
                    oProceProve.Proceso = oRow("PROCE")
                    oProceProve.GMN1 = oRow("GMN1")
                    oProceProve.Proveedor = oRow("PROVE")
                    oProceProve.Denominacion = oRow("DEN")

                    oProcProves.Add(oProceProve)
                Next
            End If

            Return oProcProves
        End Function

    End Class

#End Region

#Region " Clase ProcesoService "

    Private Class ProcesoService
        Inherits RuleBase

        Public Sub New(ByVal oCtx As GSServerModel.Contexto)
            MyBase.New(oCtx)
        End Sub

        '<summary>Actualiza los grupos de un proceso</summary>
        '<param name="arGrupos">Array de objetos ProcesoGrupo</param>         

        Public Function ActualizarProcesoGrupos(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oGrupos As GSServerModel.ProcesoGrupos) As GSServerModel.GSException
            Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
            oError.Number = ErroresGS.TESnoerror
            oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

            If Not oGrupos Is Nothing AndAlso oGrupos.Count > 0 Then
                Dim dsGrupos As DataSet
                Dim arItems(-1) As DataSet
                TrasladarCambiosProcesoGrupos(Anyo, Cod, GMN1, oGrupos, dsGrupos, arItems)

                'Comprobar que existen datos
                If Not dsGrupos Is Nothing AndAlso dsGrupos.Tables.Count > 0 AndAlso dsGrupos.Tables(0).Rows.Count > 0 Then
                    Dim oGSDBServer As New GSDatabaseServer.Procesos
                    oError = oGSDBServer.ActualizarProcesoGrupos(dsGrupos, arItems)
                Else
                    oError.Number = ErroresGS.TESProcesoNoExiste
                End If
            End If

            Return oError
        End Function

        Public Sub TrasladarCambiosProcesoGrupos(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal oGrupos As ProcesoGrupos, ByRef dsGrupos As DataSet, ByRef arItems As DataSet())
            Dim oGSDBServer As New GSDatabaseServer.Procesos
            dsGrupos = oGSDBServer.DevolverGrupos(Anyo, Cod, GMN1)

            'Comprobar que existen datos
            If Not dsGrupos Is Nothing AndAlso dsGrupos.Tables.Count > 0 AndAlso dsGrupos.Tables(0).Rows.Count > 0 Then
                'Actualizar datos de grupos
                ActualizarDTGrupos(dsGrupos.Tables(0), oGrupos)

                'Actualizar items            
                For Each oGrupo As ProcesoGrupo In oGrupos
                    If Not oGrupo.Items Is Nothing AndAlso oGrupo.Items.Count > 0 Then
                        Dim oPGS As New ProcesoGrupoService(Me.Contexto)
                        Dim dsGrupoItems As DataSet = oPGS.TrasladarCambiosProcesoGrupoItems(oGrupo.Anyo, oGrupo.Proce, oGrupo.GMN1, oGrupo.ID, oGrupo.Items)

                        ReDim Preserve arItems(arItems.Length)
                        arItems(arItems.GetUpperBound(0)) = dsGrupoItems
                    End If
                Next
            End If
        End Sub

        '<summary>Traslada los cambios del array de grupos al dattable</summary>
        '<param name="dtGrupos">DataTable con los grupos a actualizar</param>         
        '<param name="arGrupos">Array de objetos ProcesoGrupo con los nuevos datos</param> 

        Private Sub ActualizarDTGrupos(ByRef dtGrupos As DataTable, ByVal oGrupos As GSServerModel.ProcesoGrupos)
            'Borrados
            For Each oRow As DataRow In dtGrupos.Select
                Dim bEncontrado As Boolean = False
                For Each oGrupo As GSServerModel.ProcesoGrupo In oGrupos
                    If oRow("ANYO") = oGrupo.Anyo And _
                        oRow("GMN1") = oGrupo.GMN1 And _
                        oRow("PROCE") = oGrupo.Proce And _
                        oRow("ID") = oGrupo.ID Then
                        bEncontrado = True
                        Exit For
                    End If
                Next

                If Not bEncontrado Then oRow.Delete()
            Next

            'Inserciones y actualizaciones
            dtGrupos.DefaultView.Sort = "ID"
            For Each oGrupo As ProcesoGrupo In oGrupos
                Dim drGrupo As DataRow

                Dim iIndex As Integer = dtGrupos.DefaultView.Find(oGrupo.ID)
                If iIndex >= 0 Then
                    drGrupo = dtGrupos.DefaultView(iIndex).Row
                Else
                    'Nuevo grupo
                    drGrupo = dtGrupos.NewRow
                    drGrupo("ANYO") = oGrupo.Anyo
                    drGrupo("GMN1") = oGrupo.GMN1
                    drGrupo("PROCE") = oGrupo.Proce
                    drGrupo("ID") = oGrupo.ID
                End If

                Dim oPGS As New ProcesoGrupoService(Me.Contexto)
                oPGS.ActualizarDRGrupo(drGrupo, oGrupo)

                'Caso de insercion
                If Not iIndex >= 0 Then dtGrupos.Rows.Add(drGrupo)
            Next
        End Sub

        '<summary>Devuelve los atributos de un proceso</summary>
        '<param name="Anyo">Año</param>
        '<param name="Cod">Código</param>
        '<param name="GMN1">GMN1</param>
        '<param name="udtTipoVal">Validacion en apertura (0) en publicacion(1)</param>
        '<returns>Objeto de tipo ProcesoAtributosEspecificacion</returns>    

        Public Function DevolverAtributosProceso(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, _
                                                 Optional ByVal Oblig As Object = Nothing, Optional ByVal Validacion As Object = Nothing, _
                                                 Optional ByVal Vacios As Object = Nothing) As GSServerModel.ProcesoAtributosEspecificacion
            Dim oAtribs As New GSServerModel.ProcesoAtributosEspecificacion

            Dim oPS As New GSDatabaseServer.Atributos
            Dim ds As DataSet = oPS.DevolverAtributosProceso(Anyo, Cod, GMN1, True, Validacion, True)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oAtrib As New ProcesoAtributoEspecificacion

                    oAtrib.Anyo = oRow("ANYO")
                    oAtrib.GMN1 = oRow("GMN1")
                    oAtrib.Proce = oRow("PROCE")
                    oAtrib.Atrib = oRow("ATRIB")
                    oAtrib.Interno = SQLBinaryToBoolean(oRow("INTERNO"))
                    oAtrib.Pedido = SQLBinaryToBoolean(oRow("PEDIDO"))
                    oAtrib.Orden = oRow("ORDEN")
                    If Not oRow.IsNull("VALOR_NUM") Then oAtrib.ValorNum = oRow("VALOR_NUM")
                    If Not oRow.IsNull("VALOR_TEXT") Then oAtrib.ValorText = oRow("VALOR_TEXT")
                    If Not oRow.IsNull("VALOR_FEC") Then
                        oAtrib.ValorFecha = Me.UtcToTz(oRow("VALOR_FEC"))
                    Else
                        oAtrib.ValorFecha = Date.MinValue
                    End If
                    If Not oRow.IsNull("VALOR_BOOL") Then oAtrib.ValorBool = oRow("VALOR_BOOL")
                    If Not oRow.IsNull("FECACT") Then oAtrib.FechaAct = oRow("FECACT")
                    oAtrib.Oblig = oRow("OBLIG")
                    oAtrib.Validacion = oRow("VALIDACION")
                    oAtrib.Codigo = oRow("COD")
                    oAtrib.Denominacion = oRow("DEN")

                    oAtribs.Add(oAtrib)
                Next
            End If

            Return oAtribs
        End Function

    End Class

#End Region

#Region " Clase ProcesoGrupoService "

    Private Class ProcesoGrupoService
        Inherits RuleBase

        Public Sub New(ByVal oCtx As GSServerModel.Contexto)
            MyBase.New(oCtx)
        End Sub

        '<summary>Devuelve grupos</summary>
        '<param name="Anyo">Año</param>
        '<param name="Cod">Código</param>
        '<param name="GMN1">GMN1</param>
        '<returns>Array de objetos ProcesoGrupo</returns>    

        Public Function DevolverGrupos(Optional ByVal Anyo As Integer = -1, Optional ByVal Cod As Integer = -1, Optional ByVal GMN1 As String = Nothing, _
                                       Optional ByVal DevolverEstructura As Boolean = False) As ProcesoGrupos
            Dim oProcGrupos As New ProcesoGrupos

            Dim GSDBServer As New GSDatabaseServer.Procesos
            Dim ds As System.Data.DataSet = GSDBServer.DevolverGrupos(Anyo, Cod, GMN1)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oProceGrupo As New ProcesoGrupo
                    oProceGrupo.Anyo = oRow("ANYO")
                    oProceGrupo.Proce = oRow("PROCE")
                    oProceGrupo.GMN1 = oRow("GMN1")
                    oProceGrupo.ID = oRow("ID")
                    oProceGrupo.Cod = oRow("COD")
                    oProceGrupo.Denominacion = oRow("DEN")
                    'If Not oRow.IsNull("DESCR") Then oProceGrupo.Descripcion = oRow("DESCR")
                    'If Not oRow.IsNull("DEST") Then oProceGrupo.Dest = oRow("DEST")
                    'If Not oRow.IsNull("PAG") Then oProceGrupo.Pag = oRow("PAG")
                    'If Not oRow.IsNull("FECINI") Then oProceGrupo.FechaInicio = Me.UtcToTz(oRow("FECINI"))
                    'If Not oRow.IsNull("FECFIN") Then oProceGrupo.FechaFin = Me.UtcToTz(oRow("FECFIN"))
                    'If Not oRow.IsNull("PROVEACT") Then oProceGrupo.ProveAct = oRow("PROVEACT")
                    'If Not oRow.IsNull("ESP") Then oProceGrupo.Esp = oRow("ESP")
                    'If Not oRow.IsNull("CERRADO") Then oProceGrupo.Cerrado = SQLBinaryToBoolean(oRow("CERRADO"))
                    'If Not oRow.IsNull("ESP") Then oProceGrupo.Solicitud = oRow("SOLICIT")
                    'If Not oRow.IsNull("SOBRE") Then oProceGrupo.Sobre = oRow("SOBRE")
                    'oProceGrupo.Adjudicado = oRow("ADJUDICADO")
                    If Not oRow.IsNull("FECACT") Then oProceGrupo.FecAct = oRow("FECACT")
                    If Not oRow.IsNull("PRECSALIDA") Then oProceGrupo.PrecioSalida = oRow("PRECSALIDA")
                    If Not oRow.IsNull("MINPUJGANADOR") Then oProceGrupo.MinPujaGanadora = oRow("MINPUJGANADOR")
                    If Not oRow.IsNull("MINPUJPROVE") Then oProceGrupo.MinPujaProve = oRow("MINPUJPROVE")

                    If DevolverEstructura Then
                        Dim oIS As New ItemService(Me.Contexto)
                        oProceGrupo.Items = oIS.DevolverItems(Anyo, Cod, GMN1, oRow("ID"))

                        'Cálculo del presupuesto
                        For Each oItem As Item In oProceGrupo.Items
                            oProceGrupo.Presupuesto += oItem.Presupuesto
                        Next
                    End If

                    oProcGrupos.Add(oProceGrupo)
                Next
            End If

            Return oProcGrupos
        End Function

        '<summary>Traslada los cambios de grupo al datrow</summary>
        '<param name="drGrupo">DataRow el grupo a actualizar</param>         
        '<param name="oGrupo">Objetos ProcesoGrupo con los nuevos datos</param> 

        Public Sub ActualizarDRGrupo(ByRef drGrupo As DataRow, ByVal oGrupo As GSServerModel.ProcesoGrupo)
            'drGrupo("COD") = oGrupo.Cod
            'drGrupo("DEN") = oGrupo.Denominacion
            'drGrupo("DESCR") = strToDBNull(oGrupo.Descripcion)
            'drGrupo("DEST") = strToDBNull(oGrupo.Dest)
            'drGrupo("PAG") = strToDBNull(oGrupo.Pag)
            'drGrupo("FECINI") = DateToSQLDateDBNull(oGrupo.FechaInicio)
            'drGrupo("FECFIN") = DateToSQLDateDBNull(oGrupo.FechaFin)
            'drGrupo("PROVEACT") = strToDBNull(oGrupo.ProveAct)
            'drGrupo("ESP") = strToDBNull(oGrupo.Esp)
            'drGrupo("CERRADO") = BooleanToSQLBinaryDBNull(oGrupo.Cerrado)
            'drGrupo("SOLICIT") = IntToSQLIntegerDBNull(oGrupo.Solicitud)
            'drGrupo("SOBRE") = IntToSQLIntegerDBNull(oGrupo.Sobre)
            'drGrupo("ADJUDICADO") = oGrupo.Adjudicado
            drGrupo("PRECSALIDA") = DoubleToSQLDoubleDBNull(oGrupo.PrecioSalida)
            drGrupo("MINPUJGANADOR") = DoubleToSQLDoubleDBNull(oGrupo.MinPujaGanadora)
            drGrupo("MINPUJPROVE") = DoubleToSQLDoubleDBNull(oGrupo.MinPujaProve)
        End Sub

        '<summary>Actualiza los items de un grupo</summary>
        '<param name="arItems">Array de objetos Item</param>         

        Public Function ActualizarProcesoGrupoItems(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, ByVal ID As Integer, ByVal oItems As GSServerModel.Items) As GSServerModel.GSException
            Dim oError As New GSServerModel.GSException(False, String.Empty, String.Empty, True)
            oError.Number = ErroresGS.TESnoerror
            oError.Errores = New GSServerModel.GSExceptionObjects(False, String.Empty, String.Empty, True)

            If Not oItems Is Nothing AndAlso oItems.Count > 0 Then
                Dim dsItems As DataSet = TrasladarCambiosProcesoGrupoItems(Anyo, Cod, GMN1, ID, oItems)

                'Comprobar que existen datos
                If Not dsItems Is Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                    Dim oGSDBServer As New GSDatabaseServer.Procesos
                    oError = oGSDBServer.ActualizarProcesoGrupoItems(dsItems)
                Else
                    oError.Number = ErroresGS.TESProcesoNoExiste
                End If
            End If

            Return oError
        End Function

        Public Function TrasladarCambiosProcesoGrupoItems(ByVal Anyo As Integer, ByVal Proce As Integer, ByVal GMN1 As String, ByVal ID As Integer, ByVal oItems As Items) As DataSet
            Dim oGSDBServer As New GSDatabaseServer.Procesos
            Dim dsItems As DataSet = oGSDBServer.DevolverItems(Anyo, Proce, GMN1, ID)
            If Not dsItems Is Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                ActualizarDTItems(dsItems.Tables(0), oItems)
            End If

            Return dsItems
        End Function

        '<summary>Traslada los cambios de los objetos item al datatable</summary>
        '<param name="dtItems">DataRow con los datos del proceso</param>         
        '<param name="arItems">Array de objetos Item</param> 

        Private Sub ActualizarDTItems(ByRef dtItems As DataTable, ByVal oItems As GSServerModel.Items)
            'Borrados
            For Each oRow As DataRow In dtItems.Select
                Dim bEncontrado As Boolean = False
                For Each oItem As GSServerModel.Item In oItems
                    If oRow("ANYO") = oItem.Anyo And _
                        oRow("PROCE") = oItem.Proce And _
                        oRow("GMN1_PROCE") = oItem.GMN1_PROCE And _
                        oRow("ID") = oItem.ID Then
                        bEncontrado = True
                        Exit For
                    End If
                Next

                If Not bEncontrado Then oRow.Delete()
            Next

            'Inserciones y actualizaciones
            dtItems.DefaultView.Sort = "ID"
            For Each oItem As Item In oItems
                Dim drItem As DataRow

                Dim iIndex As Integer = dtItems.DefaultView.Find(oItem.ID)
                If iIndex >= 0 Then
                    drItem = dtItems.DefaultView(iIndex).Row
                Else
                    'Nuevo item               
                    drItem = dtItems.NewRow
                    drItem("ANYO") = oItem.Anyo
                    drItem("GMN1_PROCE") = oItem.GMN1_PROCE
                    drItem("PROCE") = oItem.Proce
                    drItem("ID") = oItem.ID
                End If

                Dim oIS As New ItemService(Me.Contexto)
                oIS.ActualizarDRItem(drItem, oItem)

                'Caso de insercion
                If Not iIndex >= 0 Then dtItems.Rows.Add(drItem)
            Next
        End Sub

        '<summary>Devuelve los atributos de un grupo</summary>
        '<param name="Anyo">Año</param>
        '<param name="Cod">Código</param>
        '<param name="GMN1">GMN1</param>
        '<param name="udtTipoVal">Validacion en apertura (0) en publicacion(1)</param>
        '<param name="Vacios">Indica si los valores de los atributos estarán vacíos</param>
        '<returns>Objeto de tipo ProcesoAtributosEspecificacion</returns>    

        Public Function DevolverAtributosGrupo(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal Grupo As Integer = -1, _
                                                  Optional ByVal Oblig As Object = Nothing, Optional ByVal Validacion As Object = Nothing, _
                                                  Optional ByVal Vacios As Object = Nothing) As GSServerModel.ProcesoGrupoAtributosEspecificacion
            Dim oAtribs As New GSServerModel.ProcesoGrupoAtributosEspecificacion

            Dim oPS As New GSDatabaseServer.Atributos
            Dim ds As DataSet = oPS.DevolverAtributosProcesoGrupo(Anyo, Cod, GMN1, , True, Validacion, True)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oAtrib As New ProcesoGrupoAtributoEspecificacion

                    oAtrib.Anyo = oRow("ANYO")
                    oAtrib.GMN1 = oRow("GMN1")
                    oAtrib.Proce = oRow("PROCE")
                    oAtrib.Grupo = oRow("GRUPO")
                    oAtrib.Atrib = oRow("ATRIB")
                    oAtrib.Interno = SQLBinaryToBoolean(oRow("INTERNO"))
                    oAtrib.Pedido = SQLBinaryToBoolean(oRow("PEDIDO"))
                    oAtrib.Orden = oRow("ORDEN")
                    If Not oRow.IsNull("VALOR_NUM") Then oAtrib.ValorNum = oRow("VALOR_NUM")
                    If Not oRow.IsNull("VALOR_TEXT") Then oAtrib.ValorText = oRow("VALOR_TEXT")
                    If Not oRow.IsNull("VALOR_FEC") Then
                        oAtrib.ValorFecha = Me.UtcToTz(oRow("VALOR_FEC"))
                    Else
                        oAtrib.ValorFecha = Date.MinValue
                    End If
                    If Not oRow.IsNull("VALOR_BOOL") Then oAtrib.ValorBool = oRow("VALOR_BOOL")
                    If Not oRow.IsNull("FECACT") Then oAtrib.FechaAct = oRow("FECACT")
                    oAtrib.Oblig = oRow("OBLIG")
                    oAtrib.Validacion = oRow("VALIDACION")
                    oAtrib.Codigo = oRow("COD")
                    oAtrib.Denominacion = oRow("DEN")

                    oAtribs.Add(oAtrib)
                Next
            End If

            Return oAtribs
        End Function

    End Class

#End Region

#Region " Clase ItemService "

    Private Class ItemService
        Inherits RuleBase

        Public Sub New(ByVal oCtx As GSServerModel.Contexto)
            MyBase.New(oCtx)
        End Sub

        '<summary>Devuelve items</summary>
        '<param name="Anyo">Año</param>
        '<param name="Cod">Código</param>
        '<param name="GMN1">GMN1</param>
        '<returns>array de objetos Item</returns>

        Public Function DevolverItems(Optional ByVal Anyo As Integer = -1, Optional ByVal Cod As Integer = -1, Optional ByVal GMN1 As String = Nothing, _
                                      Optional ByVal GRUPO As Integer = -1) As Items
            Dim oItems As New Items

            Dim GSDBServer As New GSDatabaseServer.Procesos
            Dim ds As System.Data.DataSet = GSDBServer.DevolverItems(Anyo, Cod, GMN1, GRUPO)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oItem As New Item
                    oItem.Anyo = oRow("ANYO")
                    oItem.Proce = oRow("PROCE")
                    oItem.GMN1_PROCE = oRow("GMN1_PROCE")
                    oItem.Grupo = oRow("GRUPO")
                    oItem.ID = oRow("ID")
                    If Not oRow.IsNull("ART") Then oItem.Articulo = oRow("ART")
                    If Not oRow.IsNull("DESCR") Then oItem.Descripcion = oRow("DESCR")
                    'If Not oRow.IsNull("GMN1") Then oItem.GMN1 = oRow("GMN1")
                    'If Not oRow.IsNull("GMN2") Then oItem.GMN2 = oRow("GMN2")
                    'If Not oRow.IsNull("GMN3") Then oItem.GMN3 = oRow("GMN3")
                    'If Not oRow.IsNull("GMN4") Then oItem.GMN4 = oRow("GMN4")
                    oItem.Dest = oRow("DEST")
                    oItem.Unidad = oRow("UNI")
                    'oItem.Pag = oRow("PAG")
                    oItem.Cantidad = oRow("CANT")
                    'If Not oRow.IsNull("PREC") Then oItem.Precio = oRow("PREC")
                    If Not oRow.IsNull("PRES") Then oItem.Presupuesto = oRow("PRES")
                    'If Not oRow.IsNull("FECINI") Then oItem.FechaInicio = Me.UtcToTz(oRow("FECINI"))
                    'If Not oRow.IsNull("FECFIN") Then oItem.FechaFin = Me.UtcToTz(oRow("FECFIN"))
                    'If Not oRow.IsNull("OBJ") Then oItem.Obj = oRow("OBJ")
                    'If Not oRow.IsNull("ESP") Then oItem.Esp = oRow("ESP")
                    'oItem.Conf = oRow("CONF")
                    If Not oRow.IsNull("FECACT") Then oItem.FecAct = oRow("FECACT")
                    'If Not oRow.IsNull("FECHAOBJ") Then oItem.FechaObj = Me.UtcToTz(oRow("FECHAOBJ"))
                    'If Not oRow.IsNull("PROVEACT") Then oItem.ProveAct = oRow("PROVEACT")
                    'If Not oRow.IsNull("EST") Then oItem.Est = SQLBinaryToBoolean(oRow("EST"))
                    'If Not oRow.IsNull("SOLICIT") Then oItem.Solicitud = oRow("SOLICIT")
                    'If Not oRow.IsNull("LINEA_SOLICIT") Then oItem.LineaSolicitud = oRow("LINEA_SOLICIT")
                    'If Not oRow.IsNull("FECULTREU") Then oItem.FechaUltReu = Me.UtcToTz(oRow("FECULTREU"))
                    'If Not oRow.IsNull("FECCIERRE") Then oItem.FechaCierre = Me.UtcToTz(oRow("FECCIERRE"))
                    'oItem.AdjCom = SQLBinaryToBoolean(oRow("ADJCOM"))
                    'If Not oRow.IsNull("CAMPO_SOLICIT") Then oItem.CampoSolicitud = oRow("CAMPO_SOLICIT")
                    If Not oRow.IsNull("MINPUJGANADOR") Then oItem.MinPujaGanadora = oRow("MINPUJGANADOR")
                    If Not oRow.IsNull("PRECSALIDA") Then oItem.PrecioSalida = oRow("PRECSALIDA")
                    If Not oRow.IsNull("MINPUJPROVE") Then oItem.MinPujaProve = oRow("MINPUJPROVE")

                    oItems.Add(oItem)
                Next
            End If

            Return oItems
        End Function

        '<summary>Traslada los cambios del objeto item al datarow</summary>
        '<param name="drItem">DataRow con los datos del item</param>         
        '<param name="oItem">Objeto Item</param> 

        Public Sub ActualizarDRItem(ByRef drItem As DataRow, ByVal oItem As GSServerModel.Item)
            'drItem("ART") = strToDBNull(oItem.Articulo)
            'drItem("DESCR") = strToDBNull(oItem.Descripcion)
            'drItem("GMN1") = strToDBNull(oItem.GMN1)
            'drItem("GMN2") = strToDBNull(oItem.GMN2)
            'drItem("GMN3") = strToDBNull(oItem.GMN3)
            'drItem("GMN4") = strToDBNull(oItem.GMN4)
            'drItem("DEST") = oItem.Dest
            'drItem("UNI") = oItem.Unidad
            'drItem("PAG") = oItem.Pag
            'drItem("CANT") = oItem.Cantidad
            'drItem("GMN4") = strToDBNull(oItem.GMN4)
            'drItem("PREC") = DoubleToSQLDoubleDBNull(oItem.Precio)
            'drItem("PRES") = DoubleToSQLDoubleDBNull(oItem.Presupuesto)
            'drItem("FECINI") = oItem.FechaInicio
            'drItem("FECFIN") = oItem.FechaFin
            'drItem("OBJ") = DoubleToSQLDoubleDBNull(oItem.Obj)
            'drItem("ESP") = strToDBNull(oItem.Esp)
            'drItem("CONF") = BooleanToSQLBinaryDBNull(oItem.Conf)
            'drItem("FECHAOBJ") = DateToSQLDateDBNull(oItem.FechaObj)
            'drItem("PROVEACT") = strToDBNull(oItem.ProveAct)
            'drItem("EST") = BooleanToSQLBinaryDBNull(oItem.Est)
            'drItem("GRUPO") = oItem.Grupo
            'drItem("SOLICIT") = IntToSQLIntegerDBNull(oItem.Solicitud)
            'drItem("LINEA_SOLICIT") = IntToSQLIntegerDBNull(oItem.LineaSolicitud)
            'drItem("FECULTREU") = DateToSQLDateDBNull(oItem.FechaUltReu)
            'drItem("FECCIERRE") = DateToSQLDateDBNull(oItem.FechaCierre)
            'drItem("ADJCOM") = BooleanToSQLBinaryDBNull(oItem.AdjCom)
            'drItem("CAMPO_SOLICIT") = IntToSQLIntegerDBNull(oItem.CampoSolicitud)
            drItem("MINPUJGANADOR") = DoubleToSQLDoubleDBNull(oItem.MinPujaGanadora)
            drItem("PRECSALIDA") = DoubleToSQLDoubleDBNull(oItem.PrecioSalida)
            drItem("MINPUJPROVE") = DoubleToSQLDoubleDBNull(oItem.MinPujaProve)
        End Sub

        '<summary>Devuelve los atributos de un item</summary>
        '<param name="Anyo">Año</param>
        '<param name="Cod">Código</param>
        '<param name="GMN1">GMN1</param>
        '<param name="udtTipoVal">Validacion en apertura (0) en publicacion(1)</param>
        '<param name="Vacios">Indica si los valores de los atributos estarán vacíos</param>
        '<returns>Objeto de tipo ProcesoAtributosEspecificacion</returns>    

        Public Function DevolverAtributosItem(ByVal Anyo As Integer, ByVal Cod As Integer, ByVal GMN1 As String, Optional ByVal Item As Integer = -1, _
                                            Optional ByVal Oblig As Object = Nothing, Optional ByVal Validacion As Object = Nothing, _
                                            Optional ByVal Vacios As Object = Nothing) As GSServerModel.ItemAtributosEspecificacion
            Dim oAtribs As New GSServerModel.ItemAtributosEspecificacion

            Dim oPS As New GSDatabaseServer.Atributos
            Dim ds As DataSet = oPS.DevolverAtributosItem(Anyo, Cod, GMN1, , True, Validacion, True)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oAtrib As New ItemAtributoEspecificacion

                    oAtrib.Anyo = oRow("ANYO")
                    oAtrib.GMN1 = oRow("GMN1")
                    oAtrib.Proce = oRow("PROCE")
                    oAtrib.Item = oRow("ITEM")
                    oAtrib.Atrib = oRow("ATRIB")
                    If Not oRow.IsNull("VALOR_NUM") Then oAtrib.ValorNum = oRow("VALOR_NUM")
                    If Not oRow.IsNull("VALOR_TEXT") Then oAtrib.ValorText = oRow("VALOR_TEXT")
                    If Not oRow.IsNull("VALOR_FEC") Then
                        oAtrib.ValorFecha = Me.UtcToTz(oRow("VALOR_FEC"))
                    Else
                        oAtrib.ValorFecha = Date.MinValue
                    End If
                    If Not oRow.IsNull("VALOR_BOOL") Then oAtrib.ValorBool = oRow("VALOR_BOOL")
                    If Not oRow.IsNull("FECACT") Then oAtrib.FechaAct = oRow("FECACT")
                    oAtrib.Art4Sinc = SQLBinaryToBoolean(oRow("ART4_SINC"))
                    If Not oRow.IsNull("OBLIG") Then oAtrib.Oblig = oRow("OBLIG")
                    If Not oRow.IsNull("VALIDACION") Then oAtrib.Validacion = oRow("VALIDACION")
                    oAtrib.Codigo = oRow("COD")
                    oAtrib.Denominacion = oRow("DEN")

                    oAtribs.Add(oAtrib)
                Next
            End If

            Return oAtribs
        End Function


    End Class

#End Region

#Region " Clase TextoMultidiomaService "

    Private Class TextoMultidiomaService

        '<summary>Devuelve los textos de un ID</summary>
        '<param name="ID">ID de los textos</param>        
        '<returns>Objeto TextosMultiidioma con los textos</returns>

        Public Function DevolverTextos(ByVal ID As Integer) As TextosMultiidioma
            Dim oTextos As New TextosMultiidioma

            Dim GSDBServer As New GSDatabaseServer.Procesos
            Dim ds As System.Data.DataSet = GSDBServer.DevolverTextosMultiidioma(ID)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oTexto As New TextoMultiidioma
                    oTexto.ID = oRow("ID")
                    oTexto.Idioma = oRow("IDI")
                    If Not oRow.IsNull("TEXTO") Then oTexto.Texto = oRow("TEXTO")
                    oTexto.Tipo = oRow("TIPO")

                    oTextos.Add(oTexto)
                Next
            End If

            Return oTextos
        End Function

        Public Sub TrasladarCambiosTextos(ByVal ID As Nullable(Of Integer), ByVal oTextos As TextosMultiidioma, ByRef dsTextos As DataSet)
            Dim oGSDBServer As New GSDatabaseServer.Procesos
            If Not ID Is Nothing Then
                dsTextos = oGSDBServer.DevolverTextosMultiidioma(ID)
            Else
                dsTextos = oGSDBServer.DevolverTextosMultiidioma(-1)
            End If


            'Comprobar que existen datos
            If Not dsTextos Is Nothing AndAlso dsTextos.Tables.Count > 0 Then
                Dim dtTexto As DataTable = dsTextos.Tables(0)

                'Borrados
                For Each oRow As DataRow In dtTexto.Select
                    Dim bEncontrado As Boolean = False
                    For Each oTexto As GSServerModel.TextoMultiidioma In oTextos
                        If oRow("ID") = oTexto.ID And oRow("IDI") = oTexto.Idioma Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next

                    If Not bEncontrado Then oRow.Delete()
                Next

                'Inserciones y actualizaciones
                dtTexto.DefaultView.Sort = "IDI"
                For Each oTexto As TextoMultiidioma In oTextos
                    Dim drTexto As DataRow

                    Dim iIndex As Integer = dtTexto.DefaultView.Find(oTexto.Idioma)
                    If iIndex >= 0 Then
                        drTexto = dtTexto.DefaultView(iIndex).Row
                    Else
                        'Nuevo grupo
                        drTexto = dtTexto.NewRow
                        drTexto("ID") = oTexto.ID
                        drTexto("IDI") = oTexto.Idioma
                    End If

                    drTexto("TEXTO") = oTexto.Texto
                    drTexto("TIPO") = oTexto.Tipo

                    'Caso de insercion
                    If Not iIndex >= 0 Then dtTexto.Rows.Add(drTexto)
                Next
            End If
        End Sub

    End Class

#End Region

End Class
