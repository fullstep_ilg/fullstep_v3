﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel
Imports System.ComponentModel

Public Class ProveedoresRule
    Inherits RuleBase

    Public Sub New(ByVal oCtx As GSServerModel.Contexto)
        MyBase.New(oCtx)
    End Sub

#Region " Proveedor "

    '<summary>Devuelve los datos de un proveedor</summary>
    '<param name="Prove">Proveedor</param>      
    '<returns>Objeto Proveedor con los datos del proveedor</returns>    

    Public Function DevolverProveedor(ByVal Prove As String) As Proveedor
        Dim oProve As New Proveedor()

        Dim GSDBServer As New GSDatabaseServer.Proveedores
        Dim ds As System.Data.DataSet = GSDBServer.DevolverProveedor(Prove)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim drProve As DataRow = ds.Tables(0).Rows(0)

            oProve.Codigo = drProve("COD")
            oProve.Denominacion = drProve("DEN")
            If Not drProve.IsNull("FSP_COD") Then oProve.FSPCod = drProve("FSP_COD")
            oProve.Premium = SQLBinaryToBoolean(drProve("PREMIUM"))
            oProve.Activo = SQLBinaryToBoolean(drProve("ACTIVO"))            
        End If

        Return oProve
    End Function

    '<summary>Devuelve la autorización de un proveedor para acceder al portal</summary>
    '<param name="Prove">Proveedor</param>
    '<param name="Cia">Compañía</param>    
    '<returns>Objeto AutorizacionPortal con los datos de la autorización</returns>    

    Public Function DevolverEstadoProveedorPortal(ByVal Prove As String, ByVal Cia As Integer) As EstadoProveedorPortal
        Dim oAut As EstadoProveedorPortal

        Dim GSDBServer As New GSDatabaseServer.Proveedores
        Dim ds As System.Data.DataSet = GSDBServer.DevolverEstadoProveedorPortal(Prove, Cia)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            oAut = New EstadoProveedorPortal(Prove)

            Dim oRow As DataRow = ds.Tables(0).Rows(0)

            oAut.EstadoGS = oRow("ESTGS")
            oAut.EstadoPortal = oRow("ESTP")
            oAut.CodigoPortal = oRow("FSP_COD")
            If oRow("ESTGS") <> TipoEstadoProveEnCia.TESPCInActivo Or oRow("ESTP") = TipoEstadoCiaEnPortal.TESPActivo Then
                oAut.Autorizado = True
            End If
        End If

        Return oAut
    End Function

    '<summary>Comprueba el email del proveedor en el portal</summary>
    '<param name="CodProvePortal">Proveedor</param>
    '<param name="EMail">Email</param>    
    '<returns>Objeto AutorizacionPortal con los datos de la autorización</returns>    

    Public Function ComprobarEmailEnPortal(ByVal CodProvePortal As String, ByVal EMail As String, ByVal FSP_SRV As String, ByVal FSP_BD As String) As Boolean
        Dim GSDBServer As New GSDatabaseServer.Proveedores
        Return GSDBServer.ComprobarEmailEnPortal(CodProvePortal, EMail, FSP_SRV, FSP_BD)
    End Function

#End Region

#Region " ProveedorContactos "

    '<summary>Devuelve los contactos de un proveedor</summary>
    '<param name="Anyo">Año</param>
    '<param name="Cod">Código</param>
    '<param name="GMN1">GMN1</param>
    '<returns>array de objetos ProcesoProveedor</returns>    

    Public Function DevolverProveedorContactos(ByVal Prove As String, Optional ByVal Subasta As Boolean = False, Optional ByVal RecibePeticiones As Boolean = False) As Contactos
        Dim oCS As New ContactoService
        Return oCS.DevolverContactos(Prove, , Subasta, RecibePeticiones)
    End Function

#End Region

#Region " Clase ContactoService "

    Private Class ContactoService

        '<summary>Devuelve contactos</summary>
        '<param name="Anyo">Año</param>
        '<param name="Cod">Código</param>
        '<param name="GMN1">GMN1</param>
        '<returns>array de objetos ProcesoProveedor</returns>    

        Public Function DevolverContactos(Optional ByVal Prove As String = Nothing, Optional ByVal ID As Integer = -1, Optional ByVal Subasta As Boolean = False, _
                                          Optional ByVal RecibePeticiones As Boolean = False) As Contactos
            Dim oContactos As New Contactos

            Dim GSDBServer As New GSDatabaseServer.Proveedores
            Dim ds As System.Data.DataSet = GSDBServer.DevolverContactos(Prove, ID, Subasta, RecibePeticiones)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oContacto As New Contacto
                    oContacto.Proveedor = oRow("PROVE")
                    oContacto.ID = oRow("ID")
                    If Not oRow.IsNull("NOM") Then oContacto.Nombre = oRow("NOM")
                    oContacto.Apellidos = oRow("APE")
                    If Not oRow.IsNull("DEP") Then oContacto.Departamento = oRow("DEP")
                    If Not oRow.IsNull("CAR") Then oContacto.Cargo = oRow("CAR")
                    If Not oRow.IsNull("TFNO") Then oContacto.Telefono = oRow("TFNO")
                    If Not oRow.IsNull("FAX") Then oContacto.Fax = oRow("FAX")
                    If Not oRow.IsNull("EMAIL") Then oContacto.EMail = oRow("EMAIL")
                    oContacto.RecPet = SQLBinaryToBoolean(oRow("RECPET"))
                    If Not oRow.IsNull("FECACT") Then oContacto.FecAct = oRow("FECACT")
                    If Not oRow.IsNull("TFNO2") Then oContacto.Telefono2 = oRow("TFNO2")
                    oContacto.Port = SQLBinaryToBoolean(oRow("PORT"))
                    If Not oRow.IsNull("TFNO_MOVIL") Then oContacto.TelefonoMovil = oRow("TFNO_MOVIL")
                    oContacto.Aprov = SQLBinaryToBoolean(oRow("APROV"))
                    If Not oRow.IsNull("ID_PORT") Then oContacto.IDPort = oRow("ID_PORT")
                    oContacto.Subasta = SQLBinaryToBoolean(oRow("SUBASTA"))
                    If Not oRow.IsNull("IDI") Then oContacto.Idioma = oRow("IDI")
                    If Not oRow.IsNull("TIPOEMAIL") Then oContacto.TipoEMail = CType(oRow("TIPOEMAIL"), Fullstep.FSNLibrary.TiposDeDatos.TipoEmail)
                    oContacto.Calidad = SQLBinaryToBoolean(oRow("CALIDAD"))
                    If Not oRow.IsNull("THOUSANFMT") Then oContacto.ThousanFMT = oRow("THOUSANFMT")
                    If Not oRow.IsNull("DECIMALFMT") Then oContacto.DecimalFMT = oRow("DECIMALFMT")
                    If Not oRow.IsNull("PRECISIONFMT") Then
                        oContacto.PrecisionFMT = oRow("PRECISIONFMT")
                    Else
                        oContacto.PrecisionFMT = -1
                    End If
                    If Not oRow.IsNull("DATEFMT") Then oContacto.DateFMT = oRow("DATEFMT")
                    If Not oRow.IsNull("LANGUAGETAG") Then oContacto.LanguageTag = oRow("LANGUAGETAG")

                    oContactos.Add(oContacto)
                Next
            End If

            Return oContactos
        End Function

    End Class

#End Region

End Class
