﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class MaterialesRule

    '<summary>Devuelve la estructura de materiales</summary>
    '<param name="iIdImpuestoValor">Id. impuesto valor</param>
    '<param name="sIdioma">Idioma</param>
    '<param name="sCodEqpComprador">Cod. equipo comprador</param>
    '<param name="sCodComprador">Cod. comprador</param>
    '<param name="bOrdPorCod">Orden por código</param>
    '<returns>Objeto Impuestos</returns>  
    '<revision>LTG 10/09/2012</revision>

    Public Function DevolverEstructuraMaterialesImpuestos(ByVal iIdImpuestoValor As Integer, ByVal sIdioma As String, Optional ByVal sCodEqpComprador As String = Nothing, _
            Optional ByVal sCodComprador As String = Nothing, Optional ByVal bOrdPorCod As Boolean = False) As DataSet
        Dim oDBServer As New GSDatabaseServer.Materiales
        Dim ds As DataSet = oDBServer.DevolverEstructuraMaterialesImpuestos(iIdImpuestoValor, sIdioma, sCodEqpComprador, sCodComprador, bOrdPorCod)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            Dim tblRelation As System.Data.DataRelation
            tblRelation = New System.Data.DataRelation("GMN1_GMN2", ds.Tables(0).Columns("COD"), ds.Tables(1).Columns("GMN1"))
            ds.Relations.Add(tblRelation)
            tblRelation = New System.Data.DataRelation("GMN1_GMN3", ds.Tables(0).Columns("COD"), ds.Tables(2).Columns("GMN1"))
            ds.Relations.Add(tblRelation)
            tblRelation = New System.Data.DataRelation("GMN1_GMN4", ds.Tables(0).Columns("COD"), ds.Tables(3).Columns("GMN1"))
            ds.Relations.Add(tblRelation)

            tblRelation = New System.Data.DataRelation("GMN2_GMN3", New DataColumn() {ds.Tables(1).Columns("GMN1"), ds.Tables(1).Columns("COD")}, New DataColumn() {ds.Tables(2).Columns("GMN1"), ds.Tables(2).Columns("GMN2")})
            ds.Relations.Add(tblRelation)
            tblRelation = New System.Data.DataRelation("GMN2_GMN4", New DataColumn() {ds.Tables(1).Columns("GMN1"), ds.Tables(1).Columns("COD")}, New DataColumn() {ds.Tables(3).Columns("GMN1"), ds.Tables(3).Columns("GMN2")})
            ds.Relations.Add(tblRelation)

            tblRelation = New System.Data.DataRelation("GMN3_GMN4", New DataColumn() {ds.Tables(2).Columns("GMN1"), ds.Tables(2).Columns("GMN2"), ds.Tables(2).Columns("COD")}, New DataColumn() {ds.Tables(3).Columns("GMN1"), ds.Tables(3).Columns("GMN2"), ds.Tables(3).Columns("GMN3")})
            ds.Relations.Add(tblRelation)
        End If
        Return ds
    End Function

    '<summary>Actualiza los cambios en la asignacion de impuestos a materiales</summary>
    '<param name="iIdImpuestoValor">Id. impuesto valor</param>
    '<param name="oAsignar">Materiales a asignar</param>
    '<param name="oDesasignar">Materiales a desasignar</param>
    '<param name="oIncluir">Materiales a incluir</param>
    '<param name="oExcluir">Materiales a excluir</param>        
    '<param name="oArtAsignar">Artículos a asignar</param>
    '<param name="oArtDesasignar">Artículos a desasignar</param>
    '<param name="oArtIncluir">Artículos a incluir</param>    
    '<param name="oArtExcluir">Artículos a excluir</param>  
    '<returns>Objeto error con el error si se ha producido</returns>  
    '<revision>LTG 13/09/2012</revision>

    Public Function ActualizarEstructuraMaterialesImpuestos(ByVal _iIdImpuestoValor As Integer, ByVal oAsignar As GSServerModel.GruposMaterial, _
            ByVal oDesasignar As GSServerModel.GruposMaterial, ByVal oArtAsignar As GSServerModel.Articulos, ByVal oArtDesasignar As GSServerModel.Articulos, _
            ByVal oArtIncluir As GSServerModel.Articulos, ByVal oArtExcluir As GSServerModel.Articulos) As GSServerModel.GSException
        Dim oDBServer As New GSDatabaseServer.Materiales
        Return oDBServer.ActualizarEstructuraMaterialesImpuestos(_iIdImpuestoValor, oAsignar, oDesasignar, oArtAsignar, oArtDesasignar, oArtIncluir, oArtExcluir)
    End Function

    '<summary>Devuelve las asignaciones a impuestos de los artículos de un material</summary>
    '<param name="IdImpuestoValor">Id. impuesto-valor</param>
    '<param name="oGMN">Material</param>    
    '<returns>Colección con los datos</returns>  
    '<revision>LTG 13/09/2012</revision>

    Public Function DevolverArticulosGMNImpuestos(ByVal IdImpuestoValor As Integer, ByVal oGMN As GSServerModel.GrupoMaterial) As GSServerModel.Articulos
        Dim oArticulos As New GSServerModel.Articulos

        Dim oDBServer As New GSDatabaseServer.Materiales
        Dim ds As DataSet = oDBServer.DevolverArticulosGMNImpuestos(IdImpuestoValor, oGMN)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            If Not ds.Tables(0) Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    Dim oArticulo As New GSServerModel.Articulo
                    oArticulo.GMN1 = oRow("GMN1")
                    oArticulo.GMN1 = oRow("GMN2")
                    oArticulo.GMN1 = oRow("GMN3")
                    oArticulo.GMN1 = oRow("GMN4")
                    oArticulo.Codigo = oRow("COD")
                    oArticulo.Denominacion = oRow("DEN")
                    If Not oRow.IsNull("ASIG") Then oArticulo.Asignado = oRow("ASIG")

                    oArticulos.Add(oArticulo)
                Next
            End If
        End If

        Return oArticulos
    End Function

End Class
