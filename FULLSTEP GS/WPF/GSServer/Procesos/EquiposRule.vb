﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports GSServerModel
Imports System.ComponentModel
Imports System.IO

Public Class EquiposRule    
    Inherits RuleBase

    Public Sub New(ByVal oCtx As GSServerModel.Contexto)
        MyBase.New(oCtx)
    End Sub

    Public Function DevolverCompradoresEquipo(ByVal Equipo As String, Optional ByVal NumMax As Integer = -1, Optional ByVal NoBajaLog As Boolean = False, _
                                              Optional ByVal CarIniCod As String = Nothing, Optional ByVal CarIniApel As String = Nothing, Optional ByVal OrderByApe As Boolean = False) As Compradores
        Dim oCompradores As New Compradores

        Dim GSDBServer As New GSDatabaseServer.Equipos
        Dim ds As System.Data.DataSet = GSDBServer.DevolverCompradoresEquipo(Equipo, NumMax, CarIniCod, CarIniApel, NoBajaLog, OrderByApe)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oComp As New Comprador

            For Each oRow As DataRow In ds.Tables(0).Rows
                oComp.Equipo = oRow("EQP")
                oComp.Codigo = oRow("COD")
                If Not oRow.IsNull("NOM") Then oComp.Nombre = oRow("NOM")
                oComp.Apellidos = oRow("APE")
                If Not oRow.IsNull("TFNO") Then oComp.Telefono = oRow("TFNO")
                If Not oRow.IsNull("FAX") Then oComp.Fax = oRow("FAX")
                If Not oRow.IsNull("EMAIL") Then oComp.EMail = oRow("EMAIL")

                oCompradores.Add(oComp)
            Next
        End If

        Return oCompradores
    End Function

End Class
