﻿Public Class RuleBase
    Private _oCtx As GSServerModel.Contexto

    Public Property Contexto As GSServerModel.Contexto
        Get
            Return _oCtx
        End Get
        Set(ByVal value As GSServerModel.Contexto)
            _oCtx = value
        End Set
    End Property

    Public Sub New(ByVal oCtx As GSServerModel.Contexto)
        If Not oCtx Is Nothing Then
            _oCtx = oCtx
        Else
            _oCtx = New GSServerModel.Contexto
            _oCtx.UserTZ = TimeZoneInfo.Local.Id
        End If
    End Sub

    Public Function UtcToTz(ByVal dtDate As DateTime) As DateTime
        Dim oTZInfo As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(_oCtx.UserTZ)
        Return TimeZoneInfo.ConvertTimeFromUtc(dtDate, oTZInfo)
    End Function

    Public Function TzToUtc(ByVal dtDate As DateTime) As DateTime
        Dim oTZInfo As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(_oCtx.UserTZ)
        Return TimeZoneInfo.ConvertTimeToUtc(dtDate, oTZInfo)
    End Function

End Class
