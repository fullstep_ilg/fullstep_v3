﻿Imports System.IO
Imports Fullstep.FSNLibrary

Public Class FileTransfer    

    '' <summary>Crea un dir. temporal en el que dejar los archivos transferidos</summary>
    '' <param name="RootFolder">Direcotorio raíz para la transferencia de archivos</param>       
    '' <remarks>Llamada desde: New  </remarks>

    Public Function BeginFileTransfer(ByVal RootFolder As String) As String
        'Crear un dir. temporal
        Dim sTempFolder As String = GenerateRandomPath()
        Dim sPath As String = RootFolder
        If Not sPath.EndsWith("\") Then sPath &= "\"        

        If Not System.IO.Directory.CreateDirectory(sPath & sTempFolder) Is Nothing Then
            Return sTempFolder
        End If
    End Function

    '' <summary>Elimina el dir. temporal de descargas y su contenido</summary>
    '' <param name="RootFolder">Direcotorio raíz para la transferencia de archivos</param>       
    '' <remarks>Llamada desde: New  </remarks>

    Public Function EndFileTransfer(ByVal Path As String) As Boolean
        EndFileTransfer = True

        If Directory.Exists(Path) Then
            Try
                For Each sFile As String In Directory.GetFiles(Path)
                    File.Delete(sFile)
                Next

                Directory.Delete(Path)
            Catch ex As Exception
                EndFileTransfer = False
            End Try
        End If        
    End Function

    '' <summary>Recibe un array de bytes y crea un archivo con ellos si no existe o los añade al final si existe</summary>
    '' <param name="FileNme">Nombre del archivo</param>   
    '' <param name="Buffer">Array de bytes</param>       
    '' <remarks>Llamada desde: New  </remarks>

    Public Function SendFileChunk(ByVal Path As String, ByVal FileName As String, ByVal Buffer As Byte()) As Boolean
        SendFileChunk = False

        If Directory.Exists(Path) Then
            Dim sFullFileName As String = Path & "\" & FileName

            Try
                Dim oFS As New FileStream(sFullFileName, FileMode.Append, FileAccess.Write)
                oFS.Write(Buffer, 0, Buffer.Length)
                oFS.Close()

                SendFileChunk = True

            Catch ex As Exception
                File.Delete(sFullFileName)
            End Try
        End If
    End Function

End Class
