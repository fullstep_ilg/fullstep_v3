﻿Public Class TextosRule

    '<summary>Devuelve un texto de la tabla WEBTEXT</summary>
    '<param name="Modulo">Modulo</param>
    '<param name="ID">ID</param>
    '<param name="Idioma">Idioma</param>    
    '<returns>Un string con el texto pedido</returns>
    '<remarks>Llamada desde: GSNotificador, Tiempo máximo: 0,3 seg</remarks>

    Public Function DevolverTextoWEBTEXT(ByVal Modulo As Integer, ByVal ID As Integer, ByVal Idioma As String) As String
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As System.Data.DataSet = GSDBServer.DevolverTextoWEBTEXT(Modulo, ID, Idioma)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows(0)("TEXT_" & Idioma)
        End If
    End Function

End Class
