﻿Imports GSServerModel
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Interface IPeticiones
    Function DevolverProveedoresSinPeticiones(Optional ByVal CodEqp As String = Nothing, Optional ByVal CodComp As String = Nothing, Optional ByVal UsarIndice As Boolean = False, Optional ByVal udtOrden As TipoOrdenacionPeticiones = Nothing) As Proveedores
    Function DevolverPeticiones(ByVal CriterioOrd As TipoOrdenacionPeticiones, Optional ByVal CodEqp As String = Nothing, Optional ByVal CodComp As String = Nothing, Optional ByVal UsarIndice As Boolean = False, Optional ByVal CodProve As String = Nothing) As ProceProvePetOfertas
    Function DevolverPeticionesPotenciales(Optional ByVal CodEqp As String = Nothing, Optional ByVal CodComp As String = Nothing, Optional ByVal UsarIndice As Boolean = False) As ProceProvePetOfertas
    Function DevolverNotificacionesPotenciales(Optional ByVal CodEqp As String = Nothing, Optional ByVal CodComp As String = Nothing, Optional ByVal UsarIndice As Boolean = False) As ProceProvePetOfertas
    Function EliminarPeticiones(ByVal oPets As ProceProvePetOfertas) As ProceProvePetOfertas
    Function RealizarPeticiones(ByVal oPets As ProceProvePetOfertas) As GSException
    Function ModificarFechaLimiteDeOfertas(ByVal Fecha As Object) As GSException
    Function DevolverComunicacionObjPotenciales(Optional ByVal CodEqp As String = Nothing, Optional ByVal CodComp As String = Nothing, Optional ByVal UsarIndice As Boolean = False) As ProceProvePetOfertas
    Function RealizarComunicacion(ByVal oPets As ProceProvePetOfertas) As GSException
End Interface
