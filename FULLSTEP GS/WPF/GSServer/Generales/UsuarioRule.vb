﻿Imports GSServerModel
Imports Fullstep.FSNLibrary

Public Class UsuarioRule

    Function ComprobarSession(ByVal sUsu As String, ByVal Session As Long) As Boolean
        Dim o As New Object
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Return GSDBServer.ComprobarSession(sUsu, Session)

    End Function

    Function CargarDatosUsu(ByVal sUsu As String, ByVal sIdi As String) As GSServerModel.Usuario
        Dim o As New Object
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As New System.Data.DataSet

        Dim oUsuario As New GSServerModel.Usuario(False, "", "", True)
        Dim oAcciones As New GSServerModel.Acciones(False, "", "", True)
        Dim oAccion As New GSServerModel.Accion(False, "", "", True)
        Dim i As Integer

        ds = GSDBServer.CargarDatosUsu(sUsu, sIdi)

        If Not ds Is Nothing Then
            oUsuario.Acciones = New GSServerModel.Acciones(False, "", "", False)
            Dim dtUsu As DataTable = ds.Tables(0)

            If dtUsu.Rows.Count <> 0 Then                
                oUsuario.Cod = dtUsu.Rows(0).Item("COD")                
                Dim sADM As String = Encrypter.Encrypt(dtUsu.Rows(0).Item("ADM"), oUsuario.Cod, False, Fullstep.FSNLibrary.TiposDeDatos.TipoDeUsuario.Administrador, dtUsu.Rows(0).Item("FEC_USU"))
                If sADM.Equals(oUsuario.Cod) Then
                    oUsuario.Tipo = Fullstep.FSNLibrary.TiposDeDatos.TipoDeUsuario.Administrador
                Else
                    oUsuario.Tipo = dtUsu.Rows(0).Item("TIPO")
                End If
                oUsuario.TimeZone = TimeZoneInfo.Local.Id
                If Not dtUsu.Rows(0).IsNull("TIMEZONE") Then oUsuario.TimeZone = dtUsu.Rows(0).Item("TIMEZONE")
                If Not dtUsu.Rows(0).IsNull("LANGUAGETAG") Then oUsuario.LanguageTag = dtUsu.Rows(0).Item("LANGUAGETAG")
                If Not dtUsu.Rows(0).IsNull("IDIOMA") Then oUsuario.Idioma = dtUsu.Rows(0).Item("IDIOMA")
                oUsuario.TipoEmail = dtUsu.Rows(0).Item("TIPOEMAIL")
                If Not dtUsu.Rows(0).IsNull("PER") Then oUsuario.Persona = dtUsu.Rows(0).Item("PER")
                If Not dtUsu.Rows(0).IsNull("FECACT") Then oUsuario.FecAct = dtUsu.Rows(0)("FECACT")

                If IsDBNull(dtUsu.Rows(0).Item("PERFCOD")) Then
                    'Es un usuario normal                
                    ds = GSDBServer.CargarAccionesUsu(oUsuario.Cod)

                    If ds.Tables(0).Rows.Count <> 0 Then
                        For Each oRow As DataRow In ds.Tables(0).Rows
                            oAccion.ID = oRow.Item("ACC")
                            oAccion.DEN = Fullstep.FSNLibrary.DBNullToStr(oRow.Item("DEN"))
                            oAccion.Orden = oRow.Item("ORDEN")

                            oUsuario.Acciones.Add(oAccion.ID, oAccion.DEN, oAccion.Orden)
                        Next
                    End If
                Else
                    'Es Un Perfil
                    oUsuario.Perfil = New GSServerModel.Perfil(False, "", "", True)

                    oUsuario.Perfil.Id = dtUsu.Rows(0).Item("PERFID")
                    oUsuario.Perfil.Cod = dtUsu.Rows(0).Item("PERFCOD") '("PERFCOD")
                    oUsuario.Perfil.Den = dtUsu.Rows(0).Item("PERFDEN") '("PERFDEN")

                    ds = GSDBServer.CargarAccionesPerfil(oUsuario.Perfil.Id, IIf(oUsuario.Idioma Is Nothing, sIdi, oUsuario.Idioma))
                    If ds.Tables(0).Rows.Count <> 0 Then
                        For Each oRow As DataRow In ds.Tables(0).Rows
                            oAccion.ID = oRow.Item("ACC")
                            oAccion.DEN = Fullstep.FSNLibrary.DBNullToStr(oRow.Item("DEN"))
                            oAccion.Orden = oRow.Item("ORDEN")

                            oUsuario.Acciones.Add(oAccion.ID, oAccion.DEN, oAccion.Orden)
                        Next
                    End If
                End If
            End If
        End If

        Return oUsuario
    End Function


    Public Function RegistrarEntradaOSalida(ByVal sUsuario As String, ByVal iAccion As Integer, ByVal Descripcion As String) As Boolean

        Dim GSDBServer As New GSDatabaseServer.Root

        Return (GSDBServer.RegistrarEntradaOSalida(sUsuario, iAccion, Descripcion))

    End Function

    Public Function DevolverPersona(ByVal Codigo As String) As Persona
        Dim oPersona As New Persona()

        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As System.Data.DataSet = GSDBServer.DevolverPersona(Codigo)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim drUsu As DataRow = ds.Tables(0).Rows(0)

            oPersona.Codigo = drUsu("COD")
            oPersona.Apellidos = drUsu("APE")
            If Not drUsu.IsNull("NOM") Then oPersona.Nombre = drUsu("NOM")
            If Not drUsu.IsNull("CAR") Then oPersona.Car = drUsu("CAR")
            If Not drUsu.IsNull("TFNO") Then oPersona.Telefono = drUsu("TFNO")
            If Not drUsu.IsNull("FAX") Then oPersona.Fax = drUsu("FAX")
            If Not drUsu.IsNull("EMAIL") Then oPersona.EMail = drUsu("EMAIL")
            If Not drUsu.IsNull("UON1") Then oPersona.UON1 = drUsu("UON1")
            If Not drUsu.IsNull("UON2") Then oPersona.UON2 = drUsu("UON2")
            If Not drUsu.IsNull("UON3") Then oPersona.UON3 = drUsu("UON3")
            oPersona.Departamento = drUsu("DEP")
            If Not drUsu.IsNull("FECACT") Then oPersona.FecAct = drUsu("FECACT")
            If Not drUsu.IsNull("TFNO2") Then oPersona.Telefono2 = drUsu("TFNO2")
            If Not drUsu.IsNull("EQP") Then oPersona.Equipo = drUsu("EQP")
            oPersona.BajaLog = drUsu("BAJALOG")
        End If

        Return oPersona
    End Function

End Class
