﻿Imports Fullstep.FSNLibrary

Public Class ParametrosRule

    Function CargarParametrosGenerales(ByRef ParametrosGenerales As GSServerModel.ParametrosGenerales, ByRef LongitudesDeCodigo As GSServerModel.LongitudesCodigo) As Object
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As System.Data.DataSet = GSDBServer.CargarParametrosGenerales()

        If Not ds Is Nothing Then

            If ds.Tables(0).Rows.Count <> 0 Then
                ParametrosGenerales.giINSTWEB = ds.Tables(0).Rows(0).Item("INSTWEB")
                ParametrosGenerales.gbIntegracion = ds.Tables(0).Rows(0).Item("INTEGRACION")
                ParametrosGenerales.gbSincronizacionMat = ds.Tables(0).Rows(0).Item("SINC_MAT")
                ParametrosGenerales.gbUsarRemitenteEmpresa = DBNullToDbl(ds.Tables(0).Rows(0).Item("USAR_RMTE_EMP"))
            End If

            If ds.Tables(1).Rows.Count <> 0 Then
                ParametrosGenerales.gbActivarCodProveErp = ds.Tables(1).Rows(0).Item("TABLA_INTERMEDIA")
            End If

            If ds.Tables(2).Rows.Count <> 0 Then
                ParametrosGenerales.gbACTIVLOG = ds.Tables(2).Rows(0).Item("ACTIVLOG")
                ParametrosGenerales.gsURLSUMMIT = ds.Tables(2).Rows(0).Item("URLWEBPROVE")
            End If

            If ds.Tables(3).Rows.Count <> 0 Then
                ParametrosGenerales.gsFSP_SRV = ds.Tables(3).Rows(0).Item("FSP_SRV")
                ParametrosGenerales.gsFSP_BD = ds.Tables(3).Rows(0).Item("FSP_Bd")
                ParametrosGenerales.gsFSP_CIA = ds.Tables(3).Rows(0).Item("FSP_CIA")
            End If

            If ds.Tables(4).Rows.Count <> 0 Then
                ParametrosGenerales.gsMONCEN = ds.Tables(4).Rows(0).Item("MONCEN")
                ParametrosGenerales.gsIdioma = ds.Tables(4).Rows(0).Item("IDIOMA")
            End If

            If ds.Tables(5).Rows.Count <> 0 Then
                ParametrosGenerales.giFSP_CIA = ds.Tables(5).Rows(0).Item("ID")
            End If

            If ds.Tables(6).Rows.Count <> 0 Then
                ParametrosGenerales.gsPetOfeMailSubject = ds.Tables(6).Rows(0).Item("PETOFEMAILSUBJECT")
            End If

            If ds.Tables(7).Rows.Count <> 0 Then
                ParametrosGenerales.gbSMTPUsarCuenta = SQLBinaryToBoolean(ds.Tables(7).Rows(0).Item("USAR_CUENTA"))
                ParametrosGenerales.gsSMTPCuentaMail = DBNullToStr(ds.Tables(7).Rows(0).Item("CUENTAMAIL"))
            End If

            If ds.Tables(8).Rows.Count <> 0 Then
                ParametrosGenerales.gbPremium = ds.Tables(8).Rows(0).Item("PREMIUM")
                ParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS = DBNullToStr(ds.Tables(8).Rows(0).Item("MARCADOR_CARPETA_PLANTILLAS"))
            End If
        End If

        ds = Nothing

        GSDBServer.CargarLongitudesDeCodigo(LongitudesDeCodigo)

        Return 0
    End Function

    Function CargarParametrosInstalacion(ByVal ParametrosGenerales As GSServerModel.ParametrosGenerales, ByRef ParametrosInstalacion As GSServerModel.ParametrosInstalacion, ByVal sUsu As String) As Object
        Dim GSDBServer As New GSDatabaseServer.Root

        'Parámetros instalación
        Dim ds As System.Data.DataSet = GSDBServer.CargarParametrosInstalacion(ParametrosInstalacion, sUsu)
        If Not ds Is Nothing Then
            If ds.Tables(0).Rows.Count > 0 Then
                ParametrosInstalacion.gsIdioma = ds.Tables(0).Rows(0).Item("IDIOMA")
                ParametrosInstalacion.gbMostrarMail = SQLBinaryToBoolean(ds.Tables(0).Rows(0).Item("MAILMOSTRAR"))
            Else
                ParametrosInstalacion.gsIdioma = "SPA"
            End If

            If Not ds.Tables(1) Is Nothing AndAlso ds.Tables(1).Rows.Count > 0 Then
                Dim dvParinsDot As DataView = New DataView(ds.Tables(1))
                dvParinsDot.Sort = "NUM"

                Dim iIndex As Integer = dvParinsDot.Find(44)
                If iIndex >= 0 Then
                    ParametrosInstalacion.gsPetOfeMailSubject = dvParinsDot(iIndex)("PLANTILLA")
                    If ParametrosInstalacion.gsPetOfeMailSubject = String.Empty Then
                        ParametrosInstalacion.gsPetOfeMailSubject = ParametrosGenerales.gsPetOfeMailSubject
                    End If
                End If

                iIndex = dvParinsDot.Find(49)
                If iIndex >= 0 Then
                    ParametrosInstalacion.gsPetOfeSubMailSubject = NothingToStr(dvParinsDot(iIndex)("PLANTILLA"))
                End If
            End If

            If Not ds.Tables(2) Is Nothing AndAlso ds.Tables(2).Rows.Count > 0 Then
                ParametrosInstalacion.gsPetOfeSubWeb = NothingToStr(ds.Tables(2).Rows(0)("SOLICIPATH"))
            End If
            If ParametrosInstalacion.gsPetOfeSubWeb = String.Empty Then
                ParametrosInstalacion.gsPetOfeSubWeb = ParametrosGenerales.gsPETOFEWEBDOT
            End If
        End If

        Return 0
    End Function

    Function CargarParametrosIntegracion(ByRef ParametrosIntegracion As GSServerModel.ParametrosIntegracion, ByVal ParametrosGenerales As GSServerModel.ParametrosGenerales) As Object
        Dim GSDBServer As New GSDatabaseServer.Root

        GSDBServer.CargarParametrosIntegracion(ParametrosIntegracion, ParametrosGenerales)

        Return 0
    End Function

    '<summary>Devuelve el literral indicado en el idioma indicado</summary>
    '<param name="ID">ID</param>
    '<param name="Idioma">Idioma</param>    
    '<returns>Un string con el literal pedido</returns>
    '<remarks>Llamada desde: GSNotificador, Tiempo máximo: 0,3 seg</remarks>

    Public Function DevolverLiteral(ByVal ID As Integer, ByVal Idioma As String) As String
        Dim GSDBServer As New GSDatabaseServer.Root
        Dim ds As System.Data.DataSet = GSDBServer.DevolverLiteral(ID, Idioma)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows(0)("DEN")
        End If
    End Function

    Public Function UTCdeBD() As Date
        Dim GSDBServer As New GSDatabaseServer.Root

        Return GSDBServer.UTCdeBD
    End Function

End Class
