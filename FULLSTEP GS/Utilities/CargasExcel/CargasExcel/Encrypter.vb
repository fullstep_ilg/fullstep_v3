﻿Option Explicit On
Option Strict On

Imports System.Runtime.InteropServices
Imports System.Security.Principal

Public Class Encrypter
    Public Enum TipoDeUsuario
        Administrador = 1
        Persona = 2
    End Enum
    Private Const ClaveSQLtod As String = "aldjñw"

    Private Const ClaveADMpar As String = "pruiop"
    Private Const ClaveADMimp As String = "ljkdag"
    Private Const ClavePar As String = "agkag´"
    Private Const ClaveImpar As String = "h+hlL_"
    Public Shared Function EncriptarAcceso(ByVal sCadena As String, ByVal sDato As String, ByVal Encriptando As Boolean, Optional ByVal Fecha As Date = #3/5/1974#) As String
        If sDato = "" Then Return ""

        Dim Crypt As New FSNCrypt.FSNAES
        Dim sClave As String

        If Len(sCadena) Mod 2 = 0 Then
            sClave = ClavePar
        Else
            sClave = ClaveImpar
        End If

        If Encriptando Then
            EncriptarAcceso = Crypt.EncryptStringToString(Fecha, sClave, sDato)
        Else
            EncriptarAcceso = Crypt.DecryptStringFromString(Fecha, sClave, sDato)
        End If

        Crypt = Nothing
    End Function
    ''' <summary>
    ''' Se utiliza para desencriptar la password que del fichero MailPwd.txt que sirve para enviar correos autenticados.
    ''' Usa como clave "ClaveSQLtod"
    ''' </summary>
    ''' <param name="DatoUsu">"PWD"</param>
    ''' <param name="DatoPwd">El contenido del fichero</param>
    ''' <param name="Encriptando">False</param>
    ''' <returns>La password desencriptada</returns>
    ''' <remarks>Llamada desde: PMNotificador --> Roor.vb --> New</remarks>
    Public Shared Function EncriptarSQLtod(ByVal DatoUsu As String, ByVal DatoPwd As String, ByVal Encriptando As Boolean, Optional ByVal Fecha As Date = #3/5/1974#) As String
        If DatoPwd = "" Then Return ""

        Dim Crypt As New FSNCrypt.FSNAES

        If Encriptando Then
            EncriptarSQLtod = Crypt.EncryptStringToString(Fecha, ClaveSQLtod, DatoPwd)
        Else
            EncriptarSQLtod = Crypt.DecryptStringFromString(Fecha, ClaveSQLtod, DatoPwd)
        End If

        Crypt = Nothing
    End Function

    Public Shared Function Encrypt(ByVal Data As String, Optional ByVal Usu As String = "", Optional ByVal Encrypting As Boolean = True, Optional ByVal UserType As TipoDeUsuario = TipoDeUsuario.Persona, Optional ByVal Fecha As Date = #3/5/1974#) As String
        Dim Crypt As New FSNCrypt.FSNAES
        Dim sKeyString As String
        Dim diacrypt As Integer
        Dim sFecha As String

        sFecha = Format(Fecha, "dd/MM/yyyy HH\:mm\:ss")
        diacrypt = Fecha.Day

        If diacrypt Mod 2 = 0 Then
            If UserType = TipoDeUsuario.Administrador Then
                sKeyString = ClaveADMpar
            Else
                sKeyString = ClavePar
            End If
        Else
            If UserType = TipoDeUsuario.Administrador Then
                sKeyString = ClaveADMimp
            Else
                sKeyString = ClaveImpar
            End If
        End If

        If Encrypting Then
            Encrypt = Crypt.EncryptStringToString(Fecha, Usu & sKeyString, Data)
        Else
            Encrypt = Crypt.DecryptStringFromString(Fecha, Usu & sKeyString, Data)
        End If

        Crypt = Nothing
    End Function

    Public Shared Function EncryptFSP(ByVal Data As String, Optional ByVal Usu As String = "", Optional ByVal Encrypting As Boolean = True, Optional ByVal UserType As TipoDeUsuario = TipoDeUsuario.Persona, Optional ByVal Fecha As Date = #3/5/1974#) As String
        Dim Crypt As New FSNCrypt.FSNAES
        Dim sKeyString As String
        Dim diacrypt As Integer
        Dim sFecha As String

        sFecha = Format(Fecha, "dd/MM/yyyy HH\:mm\:ss")
        diacrypt = Fecha.Day

        If diacrypt Mod 2 = 0 Then
            If UserType = TipoDeUsuario.Administrador Then
                sKeyString = ClaveADMpar
            Else
                sKeyString = ClavePar
            End If
        Else
            If UserType = TipoDeUsuario.Administrador Then
                sKeyString = ClaveADMimp
            Else
                sKeyString = ClaveImpar
            End If
        End If

        If Encrypting Then
            EncryptFSP = Crypt.EncryptStringToString(Fecha, sKeyString, Data)
        Else
            EncryptFSP = Crypt.DecryptStringFromString(Fecha, sKeyString, Data)
        End If

        Crypt = Nothing
    End Function

    Public Shared Function EncriptarAperturaSobre(ByVal sCadena As String, ByVal sDato As String, ByVal Encriptando As Boolean, Optional ByVal Fecha As Date = #3/5/1974#) As String
        If sDato = "" Then Return ""

        Dim Crypt As New FSNCrypt.FSNAES
        Dim sClave As String

        If Len(sCadena) Mod 2 = 0 Then
            sClave = sCadena & ClavePar
        Else
            sClave = sCadena & ClaveImpar
        End If

        If Encriptando Then
            EncriptarAperturaSobre = Crypt.EncryptStringToString(Fecha, sClave, sDato)
        Else
            EncriptarAperturaSobre = Crypt.DecryptStringFromString(Fecha, sClave, sDato)
        End If

        Crypt = Nothing
    End Function
End Class

