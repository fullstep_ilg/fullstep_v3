﻿Imports System.Data.OleDb
Imports System.Configuration
Imports System.Xml
Imports System.IO
Imports System.Data.SqlClient
Public Class Principal

    Private DBConnection As String
    Private DBConnection_Portal As String

    Private Function GetDataExcel(ByVal fileName As String, ByVal source As String) As DataTable

        Try
            Using cnn As New OleDbConnection(
                "Provider=Microsoft.Jet.OLEDB.4.0;" &
                "Extended Properties='Excel 8.0;HDR=Yes';" &
                "Data Source=" & fileName)

                Dim sql As String =
                    String.Format("SELECT * FROM [{0}]", source)

                Dim da As New OleDbDataAdapter(sql, cnn)

                Dim dt As New DataTable()

                da.Fill(dt)

                Return dt

            End Using

        Catch ex As Exception
            Throw

        End Try

    End Function

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        EstablecerConexion()
        EstablecerConexion_Portal()

    End Sub
    Private Sub EstablecerConexion()

        Dim DBLogin As String = ""
        Dim DBPassword As String

        Dim DBName As String = ""
        Dim DBServer As String = ""

        'Comprobar tema de licencias y cargar datos de conexión
        Dim sDB As String = ConfigurationManager.AppSettings("DB:FS_WS")

        If sDB Is Nothing Then
            Err.Raise(10000, , "Configuration file corrupted. Missing FS_WS entry !")
        End If

        'Tenemos que extraer el login y la contraseña de conexión.

        Dim doc As New XmlDocument
        Dim sdblogin As String = ""
        Dim sdbPassword As String = ""

        Try
            doc.Load(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "License.xml"))
        Catch e As Exception
            Err.Raise(10000, , "LICENSE file missing!")
        End Try



        Dim child As XmlNode

        child = doc.SelectSingleNode("/LICENSE/DB_LOGIN")

        If Not (child Is Nothing) Then
            DBLogin = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If

        child = Nothing

        child = doc.SelectSingleNode("/LICENSE/DB_PASSWORD")

        If Not (child Is Nothing) Then
            DBPassword = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        Else
            DBPassword = ""
        End If

        child = doc.SelectSingleNode("/LICENSE/DB_NAME")

        If Not (child Is Nothing) Then
            DBName = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If

        child = Nothing

        child = doc.SelectSingleNode("/LICENSE/DB_SERVER")

        If Not (child Is Nothing) Then
            DBServer = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If


        If IsDBNull(DBServer) Or DBServer = "" Or DBName = "" Or DBLogin = "" Then
            Err.Raise(10000, , "Corrupted LICENSE file !" & "RUTA:" & System.AppDomain.CurrentDomain.BaseDirectory() & "DatosServ:" & DBServer & ",BD:" & DBName)
        End If

        'PENDIENTE METER ENCRIPTACIÓN
        sDB = sDB.Replace("DB_LOGIN", DBLogin)
        sDB = sDB.Replace("DB_PASSWORD", DBPassword)
        sDB = sDB.Replace("DB_NAME", DBName)
        sDB = sDB.Replace("DB_SERVER", DBServer)

        doc = Nothing

        DBConnection = sDB
    End Sub

    Private Sub EstablecerConexion_Portal()

        Dim DBLogin As String = ""
        Dim DBPassword As String

        Dim DBName As String = ""
        Dim DBServer As String = ""

        'Comprobar tema de licencias y cargar datos de conexión
        Dim sDB As String = ConfigurationManager.AppSettings("DB:FS_WS")

        If sDB Is Nothing Then
            Err.Raise(10000, , "Configuration file corrupted. Missing FS_WS entry !")
        End If

        'Tenemos que extraer el login y la contraseña de conexión.

        Dim doc As New XmlDocument
        Dim sdblogin As String = ""
        Dim sdbPassword As String = ""

        Try
            doc.Load(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "License_Portal.xml"))
        Catch e As Exception
            Exit Sub
        End Try



        Dim child As XmlNode

        child = doc.SelectSingleNode("/LICENSE/DB_LOGIN")

        If Not (child Is Nothing) Then
            DBLogin = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If

        child = Nothing

        child = doc.SelectSingleNode("/LICENSE/DB_PASSWORD")

        If Not (child Is Nothing) Then
            DBPassword = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        Else
            DBPassword = ""
        End If

        child = doc.SelectSingleNode("/LICENSE/DB_NAME")

        If Not (child Is Nothing) Then
            DBName = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If

        child = Nothing

        child = doc.SelectSingleNode("/LICENSE/DB_SERVER")

        If Not (child Is Nothing) Then
            DBServer = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If


        If IsDBNull(DBServer) Or DBServer = "" Or DBName = "" Or DBLogin = "" Then
            Err.Raise(10000, , "Corrupted LICENSE file !" & "RUTA:" & System.AppDomain.CurrentDomain.BaseDirectory() & "DatosServ:" & DBServer & ",BD:" & DBName)
        End If

        'PENDIENTE METER ENCRIPTACIÓN
        sDB = sDB.Replace("DB_LOGIN", DBLogin)
        sDB = sDB.Replace("DB_PASSWORD", DBPassword)
        sDB = sDB.Replace("DB_NAME", DBName)
        sDB = sDB.Replace("DB_SERVER", DBServer)

        doc = Nothing

        DBConnection_Portal = sDB
    End Sub
    Private Sub btnLineasCatalogo_Click(sender As Object, e As EventArgs) Handles btnLineasCatalogo.Click
        Try
            Dim sFile As String = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "LineasCatalogo.xls")
            Dim dt As DataTable = GetDataExcel(sFile, "Sheet1$")
            Dim sCodProve As String = ""
            Dim iCATN1 As Integer = 0
            Dim iCATN2 As Integer = 0
            Dim iCATN3 As Integer = 0
            Dim iCATN4 As Integer = 0
            Dim iCATN5 As Integer = 0
            Dim cn As New SqlConnection()
            Dim cm As SqlCommand = New SqlCommand()
            Dim dr As SqlDataReader = Nothing
            Dim i As Integer = 0
            Dim bSaltar As Boolean = False
            Using cn
                cn.ConnectionString = DBConnection
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                For i = 0 To dt.Rows.Count - 1
                    bSaltar = False
                    iCATN1 = 0
                    iCATN2 = 0
                    iCATN3 = 0
                    iCATN4 = 0
                    iCATN5 = 0
                    'Proveedor
                    sCodProve = ""
                    cm.CommandText = "SELECT COD FROM PROVE WHERE NIF ='" & dt.Rows(i)("NIFPROVE") & "'"
                    dr = cm.ExecuteReader()
                    dr.Read()
                    If dr.HasRows Then
                        sCodProve = dr.Item("COD")
                    End If
                    dr.Close()
                    If sCodProve = "" Then
                        MsgBox("Proveedor no encontrado " & dt.Rows(i)("NIFPROVE"))
                        bSaltar = True
                    End If
                    If Not bSaltar Then
                        'Categoria Nivel 1
                        cm.CommandText = "SELECT * FROM CATN1 WHERE COD ='" & dt.Rows(i)("CATN1") & "'"
                        dr = cm.ExecuteReader()
                        dr.Read()
                        iCATN1 = dr.Item("ID")
                        dr.Close()

                        'Categoria Nivel 2
                        cm.CommandText = "SELECT * FROM CATN2 WHERE CAT1 = " & iCATN1 & " AND COD ='" & dt.Rows(i)("CATN2") & "'"
                        dr = cm.ExecuteReader()
                        dr.Read()
                        iCATN2 = dr.Item("ID")
                        dr.Close()

                        'Categoria Nivel 3
                        cm.CommandText = "SELECT * FROM CATN3 WHERE CAT1 = " & iCATN1 & " AND CAT2 = " & iCATN2 & " AND COD ='" & dt.Rows(i)("CATN3") & "'"
                        dr = cm.ExecuteReader()
                        dr.Read()
                        iCATN3 = dr.Item("ID")
                        dr.Close()

                        'Categoria Nivel 4
                        cm.CommandText = "SELECT * FROM CATN4 WHERE CAT1 = " & iCATN1 & " AND CAT2 = " & iCATN2 & " AND CAT3 = " & iCATN3 & " AND COD ='" & dt.Rows(i)("CATN4") & "'"
                        dr = cm.ExecuteReader()
                        dr.Read()
                        iCATN4 = dr.Item("ID")
                        dr.Close()

                        If dt.Rows(i)("CATN5") IsNot DBNull.Value Then
                            'Categoria Nivel 5
                            cm.CommandText = "SELECT * FROM CATN5 WHERE CAT1 = " & iCATN1 & " AND CAT2 = " & iCATN2 & " AND CAT3 = " & iCATN3 & " AND CAT4 = " & iCATN4 & " AND COD ='" & dt.Rows(i)("CATN5") & "'"
                            dr = cm.ExecuteReader()
                            dr.Read()
                            If dr.HasRows Then
                                iCATN5 = dr.Item("ID")
                            Else
                                MsgBox("Nivel 5 no encontrado " & dt.Rows(i)("CATN1") & "-" & dt.Rows(i)("CATN2") & "-" & dt.Rows(i)("CATN3") & "-" & dt.Rows(i)("CATN4") & "-" & dt.Rows(i)("CATN5"))
                                bSaltar = True
                            End If
                            dr.Close()
                        End If

                        Dim sINSERT As String = "INSERT INTO CATALOG_LIN (PROVE, ART_INT, DEST, UC, PRECIO_UC,UP_DEF, FC_DEF,CANT_MIN_DEF, PUB,CAT1,CAT2,CAT3,CAT4,CAT5, MON, DEN, ESP, SEGURIDAD_CATN, SEGURIDAD_NIVEL) VALUES ("
                        sINSERT += "'" + sCodProve + "','" + dt.Rows(i)("ART") + "','" + dt.Rows(i)("DEST") + "','" + dt.Rows(i)("UNIDAD") + "'," + dt.Rows(i)("PRECIO").ToString + ",'" + dt.Rows(i)("UP")
                        sINSERT += "'," + dt.Rows(i)("FC").ToString + "," + IIf(dt.Rows(i)("CANT_MIN_DEF") Is DBNull.Value, "NULL", dt.Rows(i)("CANT_MIN_DEF")) + ",1,"
                        sINSERT += iCATN1.ToString + "," + iCATN2.ToString + "," + iCATN3.ToString + "," + iCATN4.ToString + "," + IIf(iCATN5 = 0, "NULL", iCATN5.ToString) + ",'" + dt.Rows(i)("MON") + "','" + dt.Rows(i)("DEN")
                        sINSERT += "','" + dt.Rows(i)("ESP") + "'," + iCATN1.ToString + ",1)"
                        cm.CommandText = sINSERT
                        cm.ExecuteNonQuery()
                    End If
                Next
            End Using
            MsgBox("Carga Finalizada")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCECOs_Click(sender As Object, e As EventArgs) Handles btnCECOs.Click
        Dim sConsulta As String
        Try
            Dim sFile As String = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "CECOsUsu.xls")
            Dim dt As DataTable = GetDataExcel(sFile, "Sheet1$")
            Dim cn As New SqlConnection()
            Dim cm As SqlCommand = New SqlCommand()
            Dim i As Integer = 0
            Using cn
                cn.ConnectionString = DBConnection
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.Text

                For i = 0 To dt.Rows.Count - 1
                    If dt.Rows(i)("UON4") & "" <> "" And dt.Rows(i)("UON3") & "" <> "" Then
                        sConsulta = "INSERT INTO USU_CC_IMPUTACION (USU, UON1, UON2, UON3, UON4) VALUES ('" & dt.Rows(i)("USUARIO") & "','" & dt.Rows(i)("UON1") & "','" & dt.Rows(i)("UON2") & "','" & dt.Rows(i)("UON3") & "','" & dt.Rows(i)("UON4") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                    End If
                Next
                MsgBox("Carga Finalizada")
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnActMonProvi_Prove_Click(sender As Object, e As EventArgs) Handles btnActMonProvi_Prove.Click
        Dim sConsulta As String
        Dim i As Integer = 0
        Try
            Dim sFile As String = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "Act_prove_MON_PROVI.xls")
            Dim dt As DataTable = GetDataExcel(sFile, "Actualización$")
            Dim cn As New SqlConnection()
            Dim cm As SqlCommand = New SqlCommand()
            Using cn
                cn.ConnectionString = DBConnection
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.Text

                For i = 0 To dt.Rows.Count - 1
                    sConsulta = "UPDATE PROVE SET MON = '" & dt.Rows(i)("MONEDA") & "', PAI = '" & Trim(dt.Rows(i)("PAIS")) & "', PROVI = " & IIf(Trim(dt.Rows(i)("PROVI")) = "", "NULL", "'" & Trim(dt.Rows(i)("PROVI")) & "'") & " WHERE COD = '" & Trim(dt.Rows(i)("Código Proveedor FULLSTEP")) & "'"
                    cm.CommandText = sConsulta
                    cm.ExecuteNonQuery()

                Next
                MsgBox("Carga Finalizada")
            End Using
        Catch ex As Exception
            MsgBox(I)
            Throw ex
        End Try
    End Sub

    Private Sub btnCargarPaisProvi_Click(sender As Object, e As EventArgs) Handles btnCargarPaisProvi.Click
        Dim sConsulta As String
        Dim dr As SqlDataReader = Nothing
        Dim i As Integer = 0
        Dim sDonde As String = ""
        Try
            Dim sFile As String = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "CargaPaisesProvincias.xls")
            Dim dt As DataTable = GetDataExcel(sFile, "carga pais$")
            Dim cn As New SqlConnection()
            Dim cm As SqlCommand = New SqlCommand()

            Using cn
                cn.ConnectionString = DBConnection
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.Text

                For i = 0 To dt.Rows.Count - 1
                    sConsulta = "SELECT COD FROM PAI WITH(NOLOCK) WHERE COD ='" & dt.Rows(i)("codigo pais") & "'"
                    cm.CommandText = sConsulta
                    dr = cm.ExecuteReader()
                    dr.Read()
                    If Not dr.HasRows Then
                        dr.Close()
                        sDonde = "PAI"
                        sConsulta = "INSERT INTO PAI (COD) VALUES ('" & dt.Rows(i)("codigo pais") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                        sDonde = "PAI_DEN SPA"
                        sConsulta = "INSERT INTO PAI_DEN (PAI, IDIOMA, DEN) VALUES ('" & dt.Rows(i)("codigo pais") & "','SPA' ,'" & dt.Rows(i)("den pais spa") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                        sDonde = "PAI_DEN ENG"
                        sConsulta = "INSERT INTO PAI_DEN (PAI, IDIOMA, DEN) VALUES ('" & dt.Rows(i)("codigo pais") & "','ENG' ,'" & dt.Rows(i)("den pais eng") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                    Else
                        dr.Close()
                    End If

                Next

            End Using

            dt = GetDataExcel(sFile, "Carga Provi$")

            Using cn

                cn.ConnectionString = DBConnection
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.Text

                For i = 0 To dt.Rows.Count - 1
                    sDonde = "PROVI"

                    sConsulta = "SELECT COD FROM PROVI WITH(NOLOCK) WHERE PAI = '" & dt.Rows(i)("CODIGO PAIS") & "' AND COD ='" & dt.Rows(i)("PROVI") & "'"
                    cm.CommandText = sConsulta
                    dr = cm.ExecuteReader()
                    dr.Read()
                    If Not dr.HasRows Then
                        dr.Close()
                        sConsulta = "INSERT INTO PROVI (PAI, COD) VALUES ('" & dt.Rows(i)("CODIGO PAIS") & "','" & dt.Rows(i)("PROVI") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                        sDonde = "PROVI_DEN SPA"
                        sConsulta = "INSERT INTO PROVI_DEN (PAI, PROVI, IDIOMA, DEN) VALUES ('" & dt.Rows(i)("CODIGO PAIS") & "', '" & dt.Rows(i)("PROVI") & "','SPA' ,'" & dt.Rows(i)("Denominación SPA") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                        sDonde = "PROVI_DEN ENG"
                        sConsulta = "INSERT INTO PROVI_DEN (PAI, PROVI, IDIOMA, DEN) VALUES ('" & dt.Rows(i)("CODIGO PAIS") & "', '" & dt.Rows(i)("PROVI") & "','ENG' ,'" & dt.Rows(i)("Denominación ENG") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                    Else
                        dr.Close()
                    End If

                Next
            End Using
        Catch ex As Exception
            MsgBox(i)
            Throw ex
        End Try

        '''PORTAL***************************************************************************************
        Try
            Dim sFile As String = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "CargaPaisesProvincias.xls")
            Dim dt As DataTable = GetDataExcel(sFile, "carga pais$")
            Dim cn As New SqlConnection()
            Dim cm As SqlCommand = New SqlCommand()
            Dim IDPAi As Integer
            Using cn
                cn.ConnectionString = DBConnection_Portal
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                sConsulta = "SELECT MAX(ID) NUM FROM PAI WITH(NOLOCK) "
                cm.CommandText = sConsulta
                dr = cm.ExecuteReader()
                dr.Read()
                IDPAi = dr.Item("NUM") + 1
                dr.Close()
                For i = 0 To dt.Rows.Count - 1
                    sConsulta = "SELECT COD FROM PAI WITH(NOLOCK) WHERE COD ='" & dt.Rows(i)("codigo pais") & "'"
                    cm.CommandText = sConsulta
                    dr = cm.ExecuteReader()
                    dr.Read()
                    If Not dr.HasRows Then
                        dr.Close()
                        sDonde = "PAI"
                        sConsulta = "INSERT INTO PAI (ID,COD) VALUES (" & IDPAi & ",'" & dt.Rows(i)("codigo pais") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                        sDonde = "PAI_DEN SPA"
                        sConsulta = "INSERT INTO PAI_DEN (PAI, IDIOMA, DEN) VALUES (" & IDPAi & ",0 ,'" & dt.Rows(i)("den pais spa") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                        sDonde = "PAI_DEN ENG"
                        sConsulta = "INSERT INTO PAI_DEN (PAI, IDIOMA, DEN) VALUES (" & IDPAi & ",1 ,'" & dt.Rows(i)("den pais eng") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                        IDPAi = IDPAi + 1
                    Else
                        dr.Close()
                    End If

                Next

            End Using

            dt = GetDataExcel(sFile, "Carga Provi$")

            Using cn
                Dim IDProvi As Integer
                cn.ConnectionString = DBConnection_Portal
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.Text

                For i = 0 To dt.Rows.Count - 1
                    IDPAi = 0

                    sConsulta = "SELECT ID FROM PAI WITH(NOLOCK) WHERE COD = '" & dt.Rows(i)("CODIGO PAIS") & "'"
                    cm.CommandText = sConsulta
                    dr = cm.ExecuteReader()
                    dr.Read()
                    IDPAi = dr.Item("ID")
                    dr.Close()
                    If IDPAi = 0 Then
                        MsgBox("Sin pais")
                    End If
                    sDonde = "PROVI"
                    sConsulta = "SELECT CASE WHEN MAX(ID) IS NULL THEN 0 ELSE MAX(ID) END NUM FROM PROVI WITH(NOLOCK) WHERE PAI = " & IDPAi
                    cm.CommandText = sConsulta
                    dr = cm.ExecuteReader()
                    dr.Read()
                    IDProvi = dr.Item("NUM") + 1
                    dr.Close()
                    If IDProvi = 0 Then
                        MsgBox("Sin provincia")
                    End If
                    sConsulta = "SELECT COD FROM PROVI WITH(NOLOCK) WHERE PAI = " & IDPAi & " AND COD ='" & dt.Rows(i)("PROVI") & "'"
                    cm.CommandText = sConsulta
                    dr = cm.ExecuteReader()
                    dr.Read()
                    If Not dr.HasRows Then
                        dr.Close()
                        sConsulta = "INSERT INTO PROVI (ID,PAI, COD) VALUES (" & IDProvi & ", " & IDPAi & ",'" & dt.Rows(i)("PROVI") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                        sDonde = "PROVI_DEN SPA"
                        sConsulta = "INSERT INTO PROVI_DEN (PAI, PROVI, IDIOMA, DEN) VALUES (" & IDPAi & ", " & IDProvi & ",0 ,'" & dt.Rows(i)("Denominación SPA") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                        sDonde = "PROVI_DEN ENG"
                        sConsulta = "INSERT INTO PROVI_DEN (PAI, PROVI, IDIOMA, DEN) VALUES (" & IDPAi & ", " & IDProvi & ",1 ,'" & dt.Rows(i)("Denominación ENG") & "')"
                        cm.CommandText = sConsulta
                        cm.ExecuteNonQuery()
                    Else
                        dr.Close()
                    End If

                Next
                MsgBox("Carga Finalizada")


            End Using
        Catch ex As Exception
            MsgBox(i)
        Throw ex
        End Try
    End Sub
End Class
