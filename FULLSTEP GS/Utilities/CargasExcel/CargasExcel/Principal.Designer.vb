﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Principal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnLineasCatalogo = New System.Windows.Forms.Button()
        Me.btnCECOs = New System.Windows.Forms.Button()
        Me.btnActMonProvi_Prove = New System.Windows.Forms.Button()
        Me.btnCargarPaisProvi = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnLineasCatalogo
        '
        Me.btnLineasCatalogo.Location = New System.Drawing.Point(12, 12)
        Me.btnLineasCatalogo.Name = "btnLineasCatalogo"
        Me.btnLineasCatalogo.Size = New System.Drawing.Size(260, 24)
        Me.btnLineasCatalogo.TabIndex = 0
        Me.btnLineasCatalogo.Text = "Cargar Lineas Catálogo"
        Me.btnLineasCatalogo.UseVisualStyleBackColor = True
        '
        'btnCECOs
        '
        Me.btnCECOs.Location = New System.Drawing.Point(12, 42)
        Me.btnCECOs.Name = "btnCECOs"
        Me.btnCECOs.Size = New System.Drawing.Size(260, 23)
        Me.btnCECOs.TabIndex = 1
        Me.btnCECOs.Text = "Cargar CECOs Usuario"
        Me.btnCECOs.UseVisualStyleBackColor = True
        '
        'btnActMonProvi_Prove
        '
        Me.btnActMonProvi_Prove.Location = New System.Drawing.Point(12, 100)
        Me.btnActMonProvi_Prove.Name = "btnActMonProvi_Prove"
        Me.btnActMonProvi_Prove.Size = New System.Drawing.Size(260, 23)
        Me.btnActMonProvi_Prove.TabIndex = 2
        Me.btnActMonProvi_Prove.Text = "Actualizar Moneda y Provincia de los Proveedores"
        Me.btnActMonProvi_Prove.UseVisualStyleBackColor = True
        '
        'btnCargarPaisProvi
        '
        Me.btnCargarPaisProvi.Location = New System.Drawing.Point(12, 71)
        Me.btnCargarPaisProvi.Name = "btnCargarPaisProvi"
        Me.btnCargarPaisProvi.Size = New System.Drawing.Size(260, 23)
        Me.btnCargarPaisProvi.TabIndex = 3
        Me.btnCargarPaisProvi.Text = "Cargar países y provincias"
        Me.btnCargarPaisProvi.UseVisualStyleBackColor = True
        '
        'Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.btnCargarPaisProvi)
        Me.Controls.Add(Me.btnActMonProvi_Prove)
        Me.Controls.Add(Me.btnCECOs)
        Me.Controls.Add(Me.btnLineasCatalogo)
        Me.Name = "Principal"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnLineasCatalogo As Button
    Friend WithEvents btnCECOs As Button
    Friend WithEvents btnActMonProvi_Prove As Button
    Friend WithEvents btnCargarPaisProvi As Button
End Class
