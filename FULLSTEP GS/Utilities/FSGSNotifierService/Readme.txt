'*****************FULLSTEP NETWORKS 25/1/2002******************
INSTALACI�N:

1) Para instalar el servicio ejecutar "c:\Ruta\FSPAuctionService.exe -i"
2) Para arrancar el servicio,acceder al panel de control,herramientas
   administrativas,servicios, ir al servicio y pulsar "Start".
   El servicio no arrancar� hasta que se sincronice con la hora del sistema.
   Esto ser� cuando los minutos de la hora del sistema sean multiplos de 5,
   y los segundos sean 0. Por ejemplo: 12:05 0:00, 12:10:00 etc

3)Requisitos:

 Tener registrados:
 NTSVC.ocx  Microsoft NT Service Control
 Scrrun.dll Microsoft scripting runtime library

DESINSTALACI�N:
 
1) Primero pararlo desde el panel de control,herramientas
   administrativas,servicios, ir al servicio y pulsar "Stop".

2) Ejecutar "c:\Ruta\FSPAuctionService.exe -u"
 
 
****************************************************************

IMPORTANTE:

	No ejecutar el FSPAuctionService.exe sin par�metros de comando
	El programa interpretar� que se le est� llamando desde el 
	service control manager e intentar� responder a la petici�n
	de arranque de servicio.

**************************************************************



