VERSION 5.00
Object = "{E7BC34A0-BA86-11CF-84B1-CBC2DA68BF6C}#1.0#0"; "NTSVC.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmNotifierService 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Servicio de notificaciones"
   ClientHeight    =   11160
   ClientLeft      =   4575
   ClientTop       =   4110
   ClientWidth     =   6555
   Icon            =   "frmNotifierService.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11160
   ScaleWidth      =   6555
   Begin VB.Frame Frame5 
      Height          =   3225
      Left            =   60
      TabIndex        =   34
      Top             =   7800
      Width           =   6330
      Begin VB.TextBox txtConfirmPwd 
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   2100
         MaxLength       =   100
         PasswordChar    =   "*"
         TabIndex        =   17
         Top             =   1860
         Width           =   3345
      End
      Begin VB.TextBox txtPassword 
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   2100
         MaxLength       =   100
         PasswordChar    =   "*"
         TabIndex        =   16
         Top             =   1440
         Width           =   3345
      End
      Begin VB.CheckBox chkAutentif 
         Caption         =   "Requiere autentificaci�n"
         Height          =   375
         Left            =   120
         TabIndex        =   14
         Top             =   630
         Width           =   2085
      End
      Begin VB.TextBox txtUser 
         Height          =   315
         Left            =   2100
         MaxLength       =   100
         TabIndex        =   15
         Top             =   1020
         Width           =   3345
      End
      Begin VB.TextBox txtFromName 
         Height          =   315
         Left            =   2100
         MaxLength       =   50
         TabIndex        =   19
         Top             =   2700
         Width           =   3345
      End
      Begin VB.TextBox txtSMTP 
         Height          =   315
         Left            =   2100
         MaxLength       =   50
         TabIndex        =   13
         Top             =   240
         Width           =   3345
      End
      Begin VB.TextBox txtFrom 
         Height          =   315
         Left            =   2100
         MaxLength       =   50
         TabIndex        =   18
         Top             =   2280
         Width           =   3345
      End
      Begin VB.Label Label18 
         Caption         =   "Confirmar password:"
         Height          =   315
         Left            =   120
         TabIndex        =   45
         Top             =   1950
         Width           =   1935
      End
      Begin VB.Label Label17 
         Caption         =   "Password:"
         Height          =   315
         Left            =   120
         TabIndex        =   44
         Top             =   1530
         Width           =   1935
      End
      Begin VB.Label Label16 
         Caption         =   "Usuario:"
         Height          =   315
         Left            =   120
         TabIndex        =   43
         Top             =   1110
         Width           =   1875
      End
      Begin VB.Label Label9 
         Caption         =   "Nombre remitente e-mail:"
         Height          =   315
         Left            =   120
         TabIndex        =   37
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Label Label8 
         Caption         =   "Servidor SMTP:"
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   300
         Width           =   1515
      End
      Begin VB.Label Label10 
         Caption         =   "Direcci�n remitente e-mail:"
         Height          =   315
         Left            =   120
         TabIndex        =   35
         Top             =   2340
         Width           =   1875
      End
   End
   Begin VB.Frame Frame4 
      Height          =   1155
      Left            =   60
      TabIndex        =   31
      Top             =   6630
      Width           =   6330
      Begin VB.TextBox txtServidor 
         Height          =   315
         Left            =   2100
         MaxLength       =   50
         TabIndex        =   11
         Top             =   240
         Width           =   3315
      End
      Begin VB.TextBox txtBaseDeDatos 
         Height          =   315
         Left            =   2100
         MaxLength       =   50
         TabIndex        =   12
         Top             =   660
         Width           =   3315
      End
      Begin VB.Label Label7 
         Caption         =   "Nombre del servidor:"
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   300
         Width           =   1515
      End
      Begin VB.Label Label1 
         Caption         =   "Base de datos:"
         Height          =   315
         Left            =   120
         TabIndex        =   32
         Top             =   720
         Width           =   1635
      End
   End
   Begin NTService.NTService FSGSNotifierService 
      Left            =   5760
      Top             =   300
      _Version        =   65536
      _ExtentX        =   741
      _ExtentY        =   741
      _StockProps     =   0
      DisplayName     =   "FSGSNotifierService"
      ServiceName     =   "FSGSNotifierService"
      StartMode       =   3
   End
   Begin VB.Frame Frame3 
      Caption         =   "Par�metros del servicio"
      Height          =   2955
      Left            =   60
      TabIndex        =   27
      Top             =   60
      Width           =   6360
      Begin VB.TextBox txtPeriodicidadContrExp 
         Height          =   285
         Left            =   4560
         TabIndex        =   47
         Top             =   1920
         Width           =   915
      End
      Begin MSComCtl2.DTPicker dtCambiosCod 
         Height          =   300
         Left            =   4560
         TabIndex        =   5
         Top             =   2490
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   16449538
         CurrentDate     =   39538
      End
      Begin VB.TextBox txtPeriodicidadExpir 
         Height          =   285
         Left            =   4560
         TabIndex        =   4
         Top             =   1485
         Width           =   915
      End
      Begin VB.TextBox txtPeriodicidadDespub 
         Height          =   285
         Left            =   4560
         TabIndex        =   3
         Top             =   1095
         Width           =   915
      End
      Begin VB.TextBox txtPeriodicidad 
         Height          =   285
         Left            =   4560
         TabIndex        =   2
         Top             =   705
         Width           =   915
      End
      Begin VB.TextBox txtInstancia 
         Height          =   285
         Left            =   1380
         TabIndex        =   1
         Top             =   300
         Width           =   4080
      End
      Begin VB.Label Label20 
         Caption         =   "minutos"
         Height          =   255
         Left            =   5640
         TabIndex        =   48
         Top             =   1920
         Width           =   555
      End
      Begin VB.Label Label19 
         Caption         =   "Periodicidad de monitorizaci�n de avisos de contratos pr�ximos a expirar"
         Height          =   375
         Left            =   120
         TabIndex        =   46
         Top             =   1920
         Width           =   4095
      End
      Begin VB.Label Label15 
         Caption         =   "Hora de ejecuci�n de cambios de c�digo:"
         Height          =   255
         Left            =   120
         TabIndex        =   42
         Top             =   2520
         Width           =   3015
      End
      Begin VB.Label Label14 
         Caption         =   "minutos"
         Height          =   255
         Left            =   5640
         TabIndex        =   41
         Top             =   1500
         Width           =   555
      End
      Begin VB.Label Label13 
         Caption         =   "Periodicidad  de monitorizaci�n de avisos de certificados expirados"
         Height          =   375
         Left            =   135
         TabIndex        =   40
         Top             =   1500
         Width           =   4215
      End
      Begin VB.Label Label12 
         Caption         =   "Periodicidad de monitorizaci�n de avisos de despublicaci�n"
         Height          =   255
         Left            =   135
         TabIndex        =   39
         Top             =   1110
         Width           =   4410
      End
      Begin VB.Label Label11 
         Caption         =   "minutos"
         Height          =   255
         Left            =   5640
         TabIndex        =   38
         Top             =   1110
         Width           =   555
      End
      Begin VB.Label Label3 
         Caption         =   "minutos"
         Height          =   255
         Left            =   5640
         TabIndex        =   30
         Top             =   720
         Width           =   555
      End
      Begin VB.Label lblMinutos 
         Caption         =   "Periodicidad de monitorizaci�n de subasta"
         Height          =   255
         Left            =   135
         TabIndex        =   29
         Top             =   720
         Width           =   3600
      End
      Begin VB.Label lblInstancia 
         Caption         =   "Instancia"
         Height          =   255
         Left            =   135
         TabIndex        =   28
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Cuenta de usuario"
      Height          =   2115
      Left            =   60
      TabIndex        =   23
      Top             =   4470
      Width           =   4215
      Begin VB.TextBox txtConfirm 
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   1740
         PasswordChar    =   "|"
         TabIndex        =   10
         Top             =   1260
         Width           =   1695
      End
      Begin VB.TextBox txtPwd 
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   1740
         PasswordChar    =   "|"
         TabIndex        =   9
         Top             =   840
         Width           =   1695
      End
      Begin VB.TextBox txtUsuario 
         Height          =   315
         Left            =   1740
         TabIndex        =   8
         Top             =   420
         Width           =   1695
      End
      Begin VB.Label Label6 
         Caption         =   "Confirm pwd"
         Height          =   255
         Left            =   300
         TabIndex        =   26
         Top             =   1260
         Width           =   1335
      End
      Begin VB.Label Label5 
         Caption         =   "Pwd"
         Height          =   255
         Left            =   300
         TabIndex        =   25
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label4 
         Caption         =   "Usuario"
         Height          =   255
         Left            =   300
         TabIndex        =   24
         Top             =   420
         Width           =   1275
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Datos del servicio"
      Height          =   1275
      Left            =   60
      TabIndex        =   20
      Top             =   3120
      Width           =   6360
      Begin VB.TextBox txtDisplayName 
         Height          =   285
         Left            =   1860
         TabIndex        =   7
         Text            =   "FSGSNotifierService"
         Top             =   720
         Width           =   2175
      End
      Begin VB.TextBox txtServiceName 
         Height          =   285
         Left            =   1860
         TabIndex        =   6
         Text            =   "FSGSNotifierService"
         Top             =   300
         Width           =   2175
      End
      Begin VB.Label Label2 
         Caption         =   "Nombre descr."
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   720
         Width           =   1395
      End
      Begin VB.Label Nombre 
         Caption         =   "Nombre"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   300
         Width           =   1275
      End
   End
   Begin VB.CommandButton cmdInstalar 
      Caption         =   "Instalar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2115
      Left            =   4410
      TabIndex        =   0
      Top             =   4440
      Width           =   1995
   End
   Begin VB.Timer Timer 
      Enabled         =   0   'False
      Left            =   5460
      Top             =   1260
   End
   Begin VB.Timer TimerSubastas 
      Enabled         =   0   'False
      Left            =   5880
      Top             =   2520
   End
End
Attribute VB_Name = "frmNotifierService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private lPeriodicidad As Long 'Medida en minutos
Private lPeriodicidadDespub As Long 'Medida en minutos
Private lPeriodicidadContrExp As Long 'Medida en minutos
Private a_iHoraCambiosCod(2) As Integer 'Hora, minutos y segundos
Private iMinutos As Integer
Private iSegundos As Integer
Private sInstancia As String
Private iMinutosDespub As Integer
Private iSegundosDespub As Integer
Private iMinutosContrExp As Integer
Private iSegundosContrExp As Integer
Private bNoValidarInstancia As Boolean
Private iSegundosBatch As Integer
Private dSigCambioCod As Date

Private Const ClaveSQLtod = "aldj�w"


''' <summary>
''' Carga de datos necesarios para la ejecuci�n de
''' - los Servicios de notificaciones (subastas y despublicaci�n)
''' - limpieza de reuniones virtuales
''' - cambios de codigo/denominaciones
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub Form_Load()
Dim oFOS As FileSystemObject
Dim ostream As TextStream
Dim aHora As Variant

On Error GoTo Err_Load
    
    'Cargamos los valores de la instancia
    Set oFOS = CreateObject("Scripting.FileSystemObject")
    Set ostream = oFOS.OpenTextFile(App.Path & "\FSGSNotifierService.ini", 1, True)
    
    If Not ostream Is Nothing Then
        If Not ostream.AtEndOfStream Then
            
            sInstancia = ostream.ReadLine
            
            If InStr(1, sInstancia, "INSTANCIA=") Then
                sInstancia = Right(sInstancia, Len(sInstancia) - 10)
                '10 es el n�mero de letras de 'INSTANCIA=' +1
            Else
                sInstancia = ""
            End If
        
        End If
        
        ostream.Close
        Set ostream = Nothing
    
    End If
    
    Set oFOS = Nothing
    
    Select Case Command
        
        Case "-i"
                MostrarValoresInstancia (sInstancia)
                Exit Sub
        Case "-u"
                
                FSGSNotifierService.ServiceName = basRegistry.GetStringSetting("FSGSNotifierService", sInstancia, "Parametros", "NombreDeServicio")
                If FSGSNotifierService.Uninstall Then
                    MsgBox FSGSNotifierService.ServiceName & " uninstalled successfully"
                Else
                    MsgBox FSGSNotifierService.ServiceName & " failed to uninstall"
                End If
                End
        Case Else
            
            If sInstancia = "" Then
                MsgBox "No existe el fichero FSGSNotifierService.ini"
                End
            End If
            
            FSGSNotifierService.ServiceName = basRegistry.GetStringSetting("FSGSNotifierService", sInstancia, "Parametros", "NombreDeServicio")
            lPeriodicidad = Val(basRegistry.GetStringSetting("FSGSNotifierService", sInstancia, "Parametros", "Periodicidad"))
            lPeriodicidadDespub = Val(basRegistry.GetStringSetting("FSGSNotifierService", sInstancia, "Parametros", "PeriodicidadDespub"))
            lPeriodicidadContrExp = Val(basRegistry.GetStringSetting("FSGSNotifierService", sInstancia, "Parametros", "PeriodicidadContrExp"))
            
            aHora = Split(basRegistry.GetStringSetting("FSGSNotifierService", sInstancia, "Parametros", "HoraCambioCodigos", "0:0:0"), ":")
            If UBound(aHora) <> 2 Then
                a_iHoraCambiosCod(0) = 0
                a_iHoraCambiosCod(1) = 0
                a_iHoraCambiosCod(2) = 0
            Else
                a_iHoraCambiosCod(0) = CInt(aHora(0))
                a_iHoraCambiosCod(1) = CInt(aHora(1))
                a_iHoraCambiosCod(2) = CInt(aHora(2))
            End If
            If CDate(a_iHoraCambiosCod(0) & ":" & a_iHoraCambiosCod(1) & ":" & a_iHoraCambiosCod(2)) > CDate(Hour(Now) & ":" & Minute(Now) & ":" & Second(Now)) Then
                dSigCambioCod = Now
            Else
                dSigCambioCod = DateAdd("d", 1, Now)
            End If
            dSigCambioCod = CDate(Day(dSigCambioCod) & "/" & Month(dSigCambioCod) & "/" & Year(dSigCambioCod) & " " & a_iHoraCambiosCod(0) & ":" & a_iHoraCambiosCod(1) & ":" & a_iHoraCambiosCod(2))
    End Select
    
   Dim parmInterval As String
   
   parmInterval = FSGSNotifierService.GetSetting("Parameters", "TimerInterval", "1000")
   Timer.Interval = CLng(parmInterval)
   TimerSubastas.Interval = 5000    '5 segundos por defecto para el timer de revisi�n de subastas
    
    ' enable Pause/Continue. Must be set before StartService
    ' is called or in design mode
    FSGSNotifierService.ControlsAccepted = svcCtrlPauseContinue
    ' connect service to Windows NT services controller
    FSGSNotifierService.StartService
    Me.Hide
    Exit Sub
    
Err_Load:
    If FSGSNotifierService.Interactive Then
        MsgBox "[" & Err.Number & "] " & Err.Description
        End
    Else
        Call FSGSNotifierService.LogEvent(svcMessageError, svcEventError, "[" & Err.Number & "] " & Err.Description)
    End If
    
End Sub

''' <summary>
''' Descripci�n: Recoge los valores de los controles del formulario y :
'''              1. Asigna al NTService los valores account,password,nombre de servicio y el nobre con el que se va a ver
'''              2. Crea en el directorio un archivo .INI que guardar� la instancia
'''              3. Crea una entrada en el registro "FSGSNotifierService" + Nombre instancia + Entradas: Nombre servicio + periodicidad
'''              4. Crea una entrada en el registro "FSGSNotifier" para el componente con el servidor y la base de datos
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub cmdInstalar_Click()

Dim oFOS As Object
Dim ostream As Object

    If txtUsuario.Text = "" Then
       MsgBox "Usuario no v�lido.", vbInformation, "FSGSNotifierService"
       Exit Sub
    End If
    
    If txtPwd.Text <> txtConfirm.Text Then
       MsgBox "Pwd incorrecta", vbInformation, "FSGSNotifierService"
       Exit Sub
    End If
    
    If txtServidor.Text = "" Then
       MsgBox "Servidor de base de datos no v�lido.", vbInformation, "FSGSNotifierService"
       Exit Sub
    End If
    
    If txtBaseDeDatos.Text = "" Then
       MsgBox "Nombre de base de datos no v�lido.", vbInformation, "FSGSNotifierService"
       Exit Sub
    End If
            
    If chkAutentif.Value = 1 Then
        If txtUser = "" Or txtPassword = "" Or txtConfirmPwd = "" Then
            MsgBox "Debe introducir el usuario y el password.", vbExclamation, "Configuraci�n de la instalaci�n"
            Exit Sub
        Else
            If txtPassword <> txtConfirmPwd Then
                MsgBox "El password y su confirmaci�n deben ser iguales.", vbExclamation, "Configuraci�n de la instalaci�n"
                Exit Sub
            End If
        End If
    End If
              
    FSGSNotifierService.Account = txtUsuario.Text
    FSGSNotifierService.Password = txtPwd.Text
    FSGSNotifierService.DisplayName = txtDisplayName.Text
    FSGSNotifierService.ServiceName = txtServiceName.Text
    
    
    FSGSNotifierService.StartMode = svcStartAutomatic
     
    
    If FSGSNotifierService.Install Then
        ' La unidad del intervalo del timer son milisegundos.
        Call FSGSNotifierService.SaveSetting("Parameters", "TimerInterval", "1000")
        '1000 ms = 1 seg
        
        ''' Generamos el .INI con el nombre de la instancia
        Set oFOS = CreateObject("Scripting.FileSystemObject")
        
        Set ostream = oFOS.CreateTextFile(App.Path & "\FSGSNotifierService.ini", True)
        ostream.Write "INSTANCIA=" & txtInstancia
        ostream.Close
        Set ostream = Nothing
        Set oFOS = Nothing
        
        ''' Registry para el servicio
        ''' Graba en Registry con la instancia, el servidor y base de datos
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Parametros", "Periodicidad", txtPeriodicidad.Text
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Parametros", "PeriodicidadDespub", txtPeriodicidadDespub.Text
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Parametros", "PeriodicidadContrExp", txtPeriodicidadContrExp.Text
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Parametros", "HoraCambioCodigos", dtCambiosCod.Hour & ":" & dtCambiosCod.Minute & ":" & dtCambiosCod.Second
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Parametros", "NombreDeServicio", txtServiceName.Text
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Conexion", "Servidor", txtServidor.Text
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Conexion", "BaseDeDatos", txtBaseDeDatos.Text
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Opciones", "SMTPHost", txtSMTP.Text
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Opciones", "Autenticacion", chkAutentif.Value
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Opciones", "User", IIf(chkAutentif.Value = 1, txtUser, "")
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Opciones", "Pwd", IIf(chkAutentif.Value = 1, EncriptarAES("PWD", txtPassword, True), "")
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Opciones", "From", txtFrom.Text
        SaveStringSetting "FSGSNotifierService", txtInstancia.Text, "Opciones", "FromName", txtFromName.Text
         
        MsgBox txtServiceName.Text & " installed successfully"
        
        End
    Else
        
        MsgBox txtServiceName.Text & " failed to install"
        
    End If
       

End Sub

Private Sub FSGSNotifierService_Continue(Success As Boolean)
    
    If CDate(a_iHoraCambiosCod(0) & ":" & a_iHoraCambiosCod(1) & ":" & a_iHoraCambiosCod(2)) > CDate(Hour(Now) & ":" & Minute(Now) & ":" & Second(Now)) Then
        dSigCambioCod = Now
    Else
        dSigCambioCod = DateAdd("d", 1, Now)
    End If
    dSigCambioCod = CDate(Day(dSigCambioCod) & "/" & Month(dSigCambioCod) & "/" & Year(dSigCambioCod) & " " & a_iHoraCambiosCod(0) & ":" & a_iHoraCambiosCod(1) & ":" & a_iHoraCambiosCod(2))
    
    Timer.Enabled = True
    TimerSubastas.Enabled = True
    FSGSNotifierService.LogEvent 4, 110, "Continuamos:" & Time
    
    Success = True
End Sub



Private Sub FSGSNotifierService_Pause(Success As Boolean)
    Success = True
    Timer.Enabled = False
    TimerSubastas.Enabled = False
    FSGSNotifierService.LogEvent 4, 110, "Paramos:" & Time
    
    
End Sub

Private Sub FSGSNotifierService_Start(Success As Boolean)
    
    If CDate(a_iHoraCambiosCod(0) & ":" & a_iHoraCambiosCod(1) & ":" & a_iHoraCambiosCod(2)) > CDate(Hour(Now) & ":" & Minute(Now) & ":" & Second(Now)) Then
        dSigCambioCod = Now
    Else
        dSigCambioCod = DateAdd("d", 1, Now)
    End If
    dSigCambioCod = CDate(Day(dSigCambioCod) & "/" & Month(dSigCambioCod) & "/" & Year(dSigCambioCod) & " " & a_iHoraCambiosCod(0) & ":" & a_iHoraCambiosCod(1) & ":" & a_iHoraCambiosCod(2))
    
    Success = True
       
    Timer.Enabled = True
    TimerSubastas.Enabled = True
    
    FSGSNotifierService.LogEvent 4, 110, "Arrancamos: " & Time
    
    
    
End Sub

Private Sub FSGSNotifierService_Stop()
    
    FSGSNotifierService.LogEvent 4, 110, "Paramos:" & Time
    End
    
End Sub

''' <summary>
''' Ejecuci�n de
''' - monitorizaci�n de proveedores en subasta para desconexi�n
''' - los Servicios de notificaciones (subastas y despublicaci�n)
''' - limpieza de reuniones virtuales
''' - cambios de codigo/denominaciones
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub Timer_Timer()
Dim iError As Integer
Dim oNotificador As Object
Dim oBatch As Object

On Error GoTo Error:
    
    iError = 0
        
   ' Lanzaremos la ejecuci�n cada X minutos. Seg�n lo indicado en un par�metro general
   ' EL TIMER ESTA PUESTO EN segundos
    'Si los parametros de periodicidad son 0 es que no queremos que se notifique
    If lPeriodicidad <> 0 Then
        iSegundos = iSegundos + 1
        If iSegundos = 60 Then
            iMinutos = iMinutos + 1
            iSegundos = 0
        End If
    
        If iMinutos = lPeriodicidad Then
            iMinutos = 0
            'Instanciamos el componente de integraci�n
            Set oNotificador = CreateObject("FSGSNotifier_322020001.CFSGSNotifier")
            Set oNotificador = Nothing
            Set oNotificador = GetObject("queue:/new:FSGSNotifier_322020001.CFSGSNotifier")
            oNotificador.ServicioDeSubastas (sInstancia)
            Set oNotificador = Nothing
                
        End If
    End If
    
    If lPeriodicidadDespub <> 0 Then
        iSegundosDespub = iSegundosDespub + 1
        If iSegundosDespub = 60 Then
            iMinutosDespub = iMinutosDespub + 1
            iSegundosDespub = 0
        End If
        
        If iMinutosDespub = lPeriodicidadDespub Then
            iMinutosDespub = 0
            'Instanciamos el componente de integraci�n
            Set oNotificador = CreateObject("FFSGSNotifier_322020001.CFSGSNotifier")
            Set oNotificador = Nothing
            Set oNotificador = GetObject("queue:/new:FSGSNotifier_322020001.CFSGSNotifier")
            oNotificador.ServicioAvisoDespublicacion (sInstancia)
            Set oNotificador = Nothing
                
        End If
    End If
    
    If lPeriodicidadContrExp <> 0 Then
        iSegundosContrExp = iSegundosContrExp + 1
        If iSegundosContrExp = 60 Then
            iMinutosContrExp = iMinutosContrExp + 1
            iSegundosContrExp = 0
        End If
    
        If iMinutosContrExp = lPeriodicidadContrExp Then
            iMinutosContrExp = 0
            'Instanciamos el componente de integraci�n
            Set oNotificador = CreateObject("FSGSNotifier_322020001.CFSGSNotifier")
            Set oNotificador = Nothing
            Set oNotificador = GetObject("queue:/new:FSGSNotifier_322020001.CFSGSNotifier")
            oNotificador.ServicioContratosProximaExpiracion (sInstancia)
            Set oNotificador = Nothing
                
        End If
    End If
    
        
    iSegundosBatch = iSegundosBatch + 1
    If iSegundosBatch = 60 Then
        iSegundosBatch = 0
        Set oBatch = CreateObject("FSGSBatUpd_322020001.CBatUpd")
        Set oBatch = Nothing
        Set oBatch = GetObject("queue:/new:FSGSBatUpd_322020001.CBatUpd")
        oBatch.LimpiarReunionesVirtuales (sInstancia)
        Set oBatch = Nothing
    End If
     
    
    If Now > dSigCambioCod Then
        dSigCambioCod = DateAdd("d", 1, dSigCambioCod)
        Set oBatch = CreateObject("FSGSBatUpd_322020001.CBatUpd")
        Set oBatch = Nothing
        Set oBatch = GetObject("queue:/new:FSGSBatUpd_322020001.CBatUpd")
        
        'Actualiza los c�digos de: proveedores, art�culos y v�as de pago y denominaciones PM
        oBatch.ActualizarCodigos (sInstancia)
                
        Set oBatch = Nothing
    
    End If
Exit Sub

Error:
        
        FSGSNotifierService.LogEvent 4, 110, "Error de ejecuci�n: N�mero:" & Err.Number & " Descripci�n:" & Err.Description & " Hora:" & Time
        
        Resume Next
        
End Sub

''' <summary>Ejecuci�n de monitorizaci�n de subastas pausadas para rearranque</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>

Private Sub TimerSubastas_Timer()
    Dim iError As Integer
    Dim oBatch As Object
    
    On Error GoTo Error:
    
    iError = 0
        
    Set oBatch = CreateObject("FSGSBatUpd_322020001.CBatUpd")
    Set oBatch = Nothing
    Set oBatch = GetObject("queue:/new:FSGSBatUpd_322020001.CBatUpd")
    oBatch.RevisarSubastas (sInstancia)
    
Salir:
    Set oBatch = Nothing
    Exit Sub
Error:
    FSGSNotifierService.LogEvent 4, 110, "Error de ejecuci�n: N�mero:" & Err.Number & " Descripci�n:" & Err.Description & " Hora:" & Time
    Resume Salir
End Sub

Private Sub txtInstancia_Validate(Cancel As Boolean)
If bNoValidarInstancia Then Exit Sub
MostrarValoresInstancia (Me.txtInstancia.Text)
End Sub

''' <summary>
''' Carga de datos necesarios para la ejecuci�n de
''' - los Servicios de notificaciones (subastas y despublicaci�n)
''' - limpieza de reuniones virtuales
''' - cambios de codigo/denominaciones
''' </summary>
''' <param name="sInstancia">Instancia en la instalamos</param>
''' <remarks>Llamada desde: Form_Load   txtInstancia_Validate; Tiempo m�ximo: 0,1</remarks>
Private Sub MostrarValoresInstancia(ByVal sInstancia As String)
    Dim asHora As Variant
    bNoValidarInstancia = True
    Me.txtInstancia = sInstancia
    bNoValidarInstancia = False
On Error Resume Next
    Me.txtServidor.Text = ""
    Me.txtBaseDeDatos.Text = ""
    Me.txtServiceName.Text = ""
    Me.txtPeriodicidad.Text = ""
    Me.txtPeriodicidadDespub.Text = ""
    Me.txtPeriodicidadContrExp.Text = ""
    Me.txtSMTP.Text = ""
    Me.txtFrom.Text = ""
    Me.txtFromName.Text = ""
    
    Me.txtServidor.Text = GetStringSetting("FSGSNotifierService", sInstancia, "Conexion", "Servidor")
    Me.txtBaseDeDatos.Text = GetStringSetting("FSGSNotifierService", sInstancia, "Conexion", "BaseDeDatos")
    Me.txtServiceName.Text = GetStringSetting("FSGSNotifierService", sInstancia, "Parametros", "NombreDeServicio")
    Me.txtPeriodicidad.Text = Val(basRegistry.GetStringSetting("FSGSNotifierService", sInstancia, "Parametros", "Periodicidad"))
    Me.txtPeriodicidadDespub.Text = Val(basRegistry.GetStringSetting("FSGSNotifierService", sInstancia, "Parametros", "PeriodicidadDespub"))
    Me.txtPeriodicidadContrExp.Text = Val(basRegistry.GetStringSetting("FSGSNotifierService", sInstancia, "Parametros", "PeriodicidadContrExp"))
    
    asHora = Split(GetStringSetting("FSGSNotifierService", sInstancia, "Parametros", "HoraCambioCodigos", "0:0:0"), ":")
    If UBound(asHora) <> 2 Then
        Me.dtCambiosCod.Hour = 0
        Me.dtCambiosCod.Minute = 0
        Me.dtCambiosCod.Second = 0
    Else
        Me.dtCambiosCod.Hour = CInt(asHora(0))
        Me.dtCambiosCod.Minute = CInt(asHora(1))
        Me.dtCambiosCod.Second = CInt(asHora(2))
    End If
    Me.txtSMTP.Text = GetStringSetting("FSGSNotifierService", sInstancia, "Opciones", "SMTPHost")
    Me.chkAutentif.Value = Val(GetStringSetting("FSGSNotifierService", sInstancia, "Opciones", "Autenticacion"))
    Me.txtUser.Text = GetStringSetting("FSGSNotifierService", sInstancia, "Opciones", "User")
    Me.txtPassword.Text = EncriptarAES("PWD", GetStringSetting("FSGSNotifierService", sInstancia, "Opciones", "Pwd"), False)
    Me.txtConfirmPwd = Me.txtPassword
    Me.txtFrom.Text = GetStringSetting("FSGSNotifierService", sInstancia, "Opciones", "From")
    Me.txtFromName.Text = GetStringSetting("FSGSNotifierService", sInstancia, "Opciones", "FromName")
    If txtServiceName.Text = "" Then
        txtServiceName.Text = "FSGSNotifierService"
    End If
End Sub

''' <summary>Realiza la encriptaci�n/desencriptaci�n del password del usuario</summary>
''' <param name="DatoUsu">Semilla para encriptar � desencriptar</param>
''' <param name="DatoPwd">Dato a encriptar � desencriptar</param>
''' <param name="Encriptando">Indica si se trata de una encriptaci�n � desencriptaci�n</param>
''' <param name="Fecha">Fecha para la encriptaci�n</param>
''' <returns>String con el dato encriptado/desencriptado</returns>
''' <remarks>Llamada desde: cmdInstalar_Click   MostrarValoresInstancia; Tiempo m�ximo:1msg</remarks>
Public Function EncriptarAES(ByVal DatoUsu As String, ByVal DatoPwd As String, ByVal Encriptando As Boolean, Optional ByVal Fecha As Variant) As String
    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As cCrypt2
    
    If IsMissing(Fecha) Then
        Fecha = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            Fecha = DateSerial(1974, 3, 5)
        End If
    End If
        
    fechahoracrypt = Fecha
    
    Set oCrypt2 = New cCrypt2
    
    oCrypt2.InBuffer = DatoPwd
        
    oCrypt2.Codigo = DatoUsu & ClaveSQLtod
        
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
    
    EncriptarAES = oCrypt2.OutBuffer
                            
    Set oCrypt2 = Nothing
End Function

