﻿Imports System.Data.SqlClient
Imports System.Runtime.InteropServices
<ComVisibleAttribute(False)> _
Public Class DatabaseServer
#Region "Variables locales"
    Protected mDBConnection As String = ""
#End Region
#Region "Conexión"
    Public Sub New()
    End Sub
    Public Sub New(ByVal connectionString As String)
        mDBConnection = connectionString
    End Sub
#End Region
#Region "Acceso datos"
    ''' <summary>
    ''' Recupera los parámetros generales de la política de contraseñas de la base de datos
    ''' </summary>
    ''' <remarks></remarks>
    Public Function GetParametrosGeneralesPasswordPolicy() As DataTable
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cm.Connection = cn
            cm.CommandText = "FSP_GETPARAMETROS_PASSWORDPOLICY"
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)
            Return ds.Tables(0)
        Catch ex As Exception
            cn.Close()
            cm.Dispose()
            Throw ex
        Finally
            cn.Close()
            cm.Dispose()
        End Try
    End Function
    Public Function GetFechaPasswordActual(ByVal UsuCod As String) As Date
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cm.Connection = cn
            cm.CommandText = "FSP_GETUSUPASSWORD_DATE"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USUCOD", UsuCod)
            da.SelectCommand = cm
            da.Fill(ds)
            Return CType(ds.Tables(0).Rows(0)("FECPWD"), Date)
        Catch ex As Exception
            cn.Close()
            cm.Dispose()
            Throw ex
        Finally
            cn.Close()
            cm.Dispose()
        End Try
    End Function
    Public Function ComprobacionHistoricoPassword(ByVal IdCia As Integer, ByVal IdUsu As Integer, _
                ByVal NumeroPasswordHistorico As Integer, ByVal NewPWD As String) As Boolean
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cm.Connection = cn
            cm.CommandText = "FSP_GETPASSWORD_HISTORY"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDCIA", IdCia)
            cm.Parameters.AddWithValue("@IDUSU", IdUsu)
            cm.Parameters.AddWithValue("@NUM_HISTORY", NumeroPasswordHistorico)
            cm.Parameters.AddWithValue("@NEWPWD", NewPWD)
            da.SelectCommand = cm
            da.Fill(ds)
            Return CType(ds.Tables(0).Rows(0)("FECPWD"), Boolean)
        Catch ex As Exception
            cn.Close()
            cm.Dispose()
            Throw ex
        Finally
            cn.Close()
            cm.Dispose()
        End Try
    End Function
    Public Sub CambiarPwdDB(ByVal IdCia As Integer, ByVal IdUsu As Integer, ByVal NewPassword As String, _
                            ByVal FecNewPassword As Date, ByVal Salt As String)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSP_PASSWORD_CHANGE"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDCIA", IdCia)
            cm.Parameters.AddWithValue("@IDUSU", IdUsu)
            cm.Parameters.AddWithValue("@NEWPWD", NewPassword)
            cm.Parameters.AddWithValue("@FECPWD", FecNewPassword)
            cm.Parameters.AddWithValue("@SALT", Salt)
            cm.ExecuteNonQuery()
            cn.Close()
            cm.Dispose()
        Catch ex As Exception
            cn.Close()
            cm.Dispose()
            Throw ex
        Finally
            cn.Close()
            cm.Dispose()
        End Try
    End Sub
#End Region
End Class


