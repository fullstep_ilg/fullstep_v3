﻿Imports System.DirectoryServices
Imports System.Runtime.InteropServices

<ComClass(AdminUsers.ClassId, AdminUsers.InterfaceId, AdminUsers.EventsId)> _
Public Class AdminUsers

#Region "COM GUIDs"
    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.   
    Public Const ClassId As String = "A6F89247-45AB-4C08-AA33-76BF84D719D4"
    Public Const InterfaceId As String = "D5100218-A8C1-4B5F-A3CC-5B1D1C57135D"
    Public Const EventsId As String = "3E16D510-0CF8-404D-B747-EEEAF994FEA9"
#End Region

#Region "Funciones Creación/Destrucción Objeto AdminUsers"
    ''' <summary>
    ''' A creatable COM class must have a Public Sub New() 
    ''' with no parameters, otherwise, the class will not be 
    ''' registered in the COM registry and cannot be created 
    ''' via CreateObject.
    ''' </summary>
    ''' <remarks>Llamada desde: Gs, Fsp, Ps y Notif GS; Tiempo maximo:0</remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Destruir el objeto 
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb; Tiempo maximo:0</remarks>
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
#End Region

#Region "Definicion Propertys"
    Private mWinSecAdminUser As String
    Private mWinSecAdminPwd As String
    Private mWinSecRemoteGroupName As String

    Private mvarCod As String
    Private mvarPWDDes As String
    Private mInstancia As String
    Private mApe As String

    Private mMustChangePassword As Boolean
    Private mAccountDisabled As Boolean
    Private mAccountLocked As Boolean
    Private mCanChangePassword As Boolean
    Private mNoEjecutarWHOF As Boolean

    Private mvarPWDOld As String

    Private Const ACCOUNT_PASSWD_CANT_CHANGE = &H40
    Private Const ACCOUNT_DONT_EXPIRE_PASSWD = &H10000
    Private Const ACCOUNT_DISABLED = &H2
    Private Const ACCOUNT_LOCK_OUT = &H10
#End Region

#Region "Establecer Propertys"
    ''' <summary>
    '''  Devuelve/Establace la propiedad mWinSecAdminUser
    ''' </summary>
    ''' <remarks>Llamada desde:  CUsuario; Tiempo maximo:0</remarks>
    Property WinSecAdminUser() As String
        Get
            WinSecAdminUser = mWinSecAdminUser
        End Get
        Set(ByVal Value As String)
            mWinSecAdminUser = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mWinSecAdminPwd
    ''' </summary>
    ''' <remarks>Llamada desde: CUsuario; Tiempo maximo:0</remarks>
    Property WinSecAdminPwd() As String
        Get
            WinSecAdminPwd = mWinSecAdminPwd
        End Get
        Set(ByVal Value As String)
            mWinSecAdminPwd = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mWinSecRemoteGroupName
    ''' </summary>
    ''' <remarks>Llamada desde: CUsuario; Tiempo maximo:0</remarks>
    Property WinSecRemoteGroupName() As String
        Get
            WinSecRemoteGroupName = mWinSecRemoteGroupName
        End Get
        Set(ByVal Value As String)
            mWinSecRemoteGroupName = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mvarCod
    ''' </summary>
    ''' <remarks>Llamada desde: CUsuario; Tiempo maximo:0</remarks>
    Property Codigo() As String
        Get
            Codigo = mvarCod
        End Get
        Set(ByVal Value As String)
            mvarCod = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mvarPWDDes
    ''' </summary>
    ''' <remarks>Llamada desde: CUsuario; Tiempo maximo:0</remarks>
    Property PWDDes() As String
        Get
            PWDDes = mvarPWDDes
        End Get
        Set(ByVal Value As String)
            mvarPWDDes = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mInstancia
    ''' </summary>
    ''' <remarks>Llamada desde: CUsuario; Tiempo maximo:0</remarks>
    Property Instancia() As String
        Get
            Instancia = mInstancia
        End Get
        Set(ByVal Value As String)
            mInstancia = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mApe
    ''' </summary>
    ''' <remarks>Llamada desde: CUsuario; Tiempo maximo:0</remarks>
    Property Ape() As String
        Get
            Ape = mApe
        End Get
        Set(ByVal Value As String)
            mApe = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mMustChangePassword
    ''' </summary>
    ''' <remarks>Llamada desde: CUsuario; Tiempo maximo:0</remarks>
    Property MustChangePassword() As Boolean
        Get
            MustChangePassword = mMustChangePassword
        End Get
        Set(ByVal Value As Boolean)
            mMustChangePassword = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mAccountDisabled
    ''' </summary>
    ''' <remarks>Llamada desde: CUsuario; Tiempo maximo:0</remarks>
    Property AccountDisabled() As Boolean
        Get
            AccountDisabled = mAccountDisabled
        End Get
        Set(ByVal Value As Boolean)
            mAccountDisabled = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mAccountLocked
    ''' </summary>
    ''' <remarks>Llamada desde: CUsuario; Tiempo maximo:0</remarks>
    Property AccountLocked() As Boolean
        Get
            AccountLocked = mAccountLocked
        End Get
        Set(ByVal Value As Boolean)
            mAccountLocked = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mvarPWDOld
    ''' </summary>
    ''' <remarks>Llamada desde: CUsuario; Tiempo maximo:0</remarks>
    Property PWDOld() As String
        Get
            PWDOld = mvarPWDOld
        End Get
        Set(ByVal Value As String)
            mvarPWDOld = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mCanChangePassword
    ''' </summary>
    ''' <remarks>Llamada desde: FSGSUserTester/FSGSUserDelete; Tiempo maximo:0</remarks>
    Property CanChangePassword() As Boolean
        Get
            CanChangePassword = mCanChangePassword
        End Get
        Set(ByVal Value As Boolean)
            mCanChangePassword = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mNoEjecutarWHOF
    ''' </summary>
    ''' <remarks>Llamada desde: FSGSUserTester/FSGSUserDelete; Tiempo maximo:0</remarks>
    Property NoEjecutarWHOF() As Boolean
        Get
            NoEjecutarWHOF = mNoEjecutarWHOF
        End Get
        Set(ByVal Value As Boolean)
            mNoEjecutarWHOF = Value
        End Set
    End Property
#End Region

#Region "Sacado de GS"
    ''' <summary>
    ''' Añade el usuario a la máquina
    ''' </summary>
    ''' <param name="StrResultado">texto error de haberlo</param>
    ''' <returns>0-ok   1-error impersonate   2-error añadiendo </returns>
    ''' <remarks>Llamada desde: GS/CUsuario/AddUserToServer; Tiempo maximo: 0</remarks>
    Public Function AddUserToServer(ByRef StrResultado As String) As Integer
        Try
            If impersonateValidUser(mWinSecAdminUser, Environment.MachineName, mWinSecAdminPwd) Then
                Try
                    Dim de As New System.DirectoryServices.DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                    de.RefreshCache()

                    Dim NewUser As System.DirectoryServices.DirectoryEntry = de.Children.Add(mvarCod, "user")
                    NewUser.Invoke("SetPassword", New Object() {mvarPWDDes})
                    NewUser.Invoke("Put", New Object() {"Description", mInstancia})
                    NewUser.Invoke("Put", New Object() {"PasswordExpired", 0})
                    NewUser.CommitChanges()

                    NewUser.Invoke("Put", New Object() {"FullName", mApe})

                    'Esto dos EjecutarFSGSxx antes de q se deshabilite la cuenta o fallara. Pruebas 31900.8 / 2014 / 251
                    If Not NoEjecutarWHOF Then EjecutarFSGSSaveRegistryWHOF()

                    EjecutarFSGSSaveRegistryWREG()

                    Dim newFlags As Object = NewUser.Invoke("Get", New Object() {"userFlags"})
                    If CanChangePassword = True Then
                        newFlags = newFlags Or ACCOUNT_DONT_EXPIRE_PASSWD
                    Else
                        newFlags = newFlags Or ACCOUNT_PASSWD_CANT_CHANGE Or ACCOUNT_DONT_EXPIRE_PASSWD
                    End If

                    If (Not mMustChangePassword) AndAlso (mAccountDisabled) Then
                        newFlags = newFlags Or ACCOUNT_DISABLED
                    End If

                    NewUser.Invoke("Put", New Object() {"userFlags", newFlags})

                    NewUser.CommitChanges()

                    Dim grp As System.DirectoryServices.DirectoryEntry = de.Children.Find(WinSecRemoteGroupName, "group")
                    grp.Invoke("Add", New Object() {NewUser.Path.ToString()})
                    grp.CommitChanges()

                    AddUserToServer = 0
                Catch ex2 As System.Runtime.InteropServices.COMException
                    AddUserToServer = 2

                    If ex2.ErrorCode = -2147022672 Then AddUserToServer = 3

                    StrResultado = ex2.Message

                    EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.AddUserToServer COMException Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)

                Catch ex As Exception
                    AddUserToServer = 2

                    StrResultado = ex.Message

                    EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.AddUserToServer Exception Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
                Finally
                    'undoImpersonation()
                End Try

            Else
                AddUserToServer = 1

                StrResultado = "Error Impersonate"

                EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.AddUserToServer Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
            End If
        Catch ex As Exception
            AddUserToServer = 1

            StrResultado = "Error Impersonate catch" & ex.Message

            EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.AddUserToServer Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
        End Try
    End Function

    ''' <summary>
    ''' Modifica el usuario en la máquina
    ''' </summary>
    ''' <param name="ModifySoloPWD">Aparte de la contraseña si se modif o no las propiedades del usuario</param>
    ''' <param name="bChangeHPath">Si establece el programa de inicio y el home directory y si se modifica el registro para algunas opciones de Excel y Word</param>
    ''' <param name="StrResultado">texto error de haberlo</param>
    ''' <param name="EsCambioLogin">Si vienede CambioPWD.asmx/CambiarLogin o no</param>
    ''' <returns>0-ok   1-error impersonate   2-error modificando </returns>
    ''' <remarks>Llamada desde: GS/CUsuario/ModifyUserInServer           CambiarLogin; Tiempo maximo: 0</remarks>
    Public Function ModifyUserInServer(ByVal ModifySoloPWD As Boolean, ByVal bChangeHPath As Boolean, ByRef StrResultado As String, Optional ByVal EsCambioLogin As Boolean = False) As Integer
        Try
            If impersonateValidUser(mWinSecAdminUser, Environment.MachineName, mWinSecAdminPwd) Then
                Try
                    Dim de As New System.DirectoryServices.DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                    Dim User As System.DirectoryServices.DirectoryEntry = de.Children.Find(mvarCod, "user")

                    If bChangeHPath Then
                        'Esto dos EjecutarFSGSxx antes de q se deshabilite la cuenta o fallara. Pruebas 31900.8 / 2014 / 251
                        EjecutarFSGSSaveRegistryWHOF()
                    End If

                    If Not ModifySoloPWD Then
                        User.Invoke("Put", New Object() {"PasswordExpired", 0})

                        Dim Flags As Object = User.Invoke("Get", New Object() {"userFlags"})
                        Flags = Flags Or ACCOUNT_PASSWD_CANT_CHANGE
                        Flags = Flags Or ACCOUNT_DONT_EXPIRE_PASSWD
                        If mAccountDisabled Then
                            Flags = Flags Or ACCOUNT_DISABLED
                        ElseIf ((Flags And ACCOUNT_DISABLED) = ACCOUNT_DISABLED) Then
                            Flags = Flags Xor ACCOUNT_DISABLED
                        End If
                        If mAccountLocked = False AndAlso ((Flags And ACCOUNT_LOCK_OUT) = ACCOUNT_LOCK_OUT) Then
                            Flags = Flags Xor ACCOUNT_LOCK_OUT
                        End If
                        User.Invoke("Put", New Object() {"userFlags", Flags})
                        User.CommitChanges()
                    End If

                    If mvarPWDOld <> mvarPWDDes Then
                        Dim Flags As Object = User.Invoke("Get", New Object() {"userFlags"})
                        Dim bCambio As Boolean = ((Flags And ACCOUNT_PASSWD_CANT_CHANGE) = ACCOUNT_PASSWD_CANT_CHANGE)
                        If bCambio Then
                            Flags = Flags Xor ACCOUNT_PASSWD_CANT_CHANGE
                            User.Invoke("Put", New Object() {"userFlags", Flags})
                            User.CommitChanges()
                        End If

                        User.Invoke("SetPassword", New Object() {mvarPWDDes})

                        If bCambio Then
                            Flags = User.Invoke("Get", New Object() {"userFlags"})
                            Flags = Flags Or ACCOUNT_PASSWD_CANT_CHANGE
                            User.Invoke("Put", New Object() {"userFlags", Flags})
                        End If
                        User.CommitChanges()
                    End If

                    ModifyUserInServer = 0
                Catch ex As Exception
                    ModifyUserInServer = 2

                    StrResultado = ex.Message

                    EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers." & IIf(EsCambioLogin, "CambiarLogin", "ModifyUserInServer") & " Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
                Finally
                    undoImpersonation()
                End Try
            Else
                ModifyUserInServer = 1

                StrResultado = "Error Impersonate"

                EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers." & IIf(EsCambioLogin, "CambiarLogin", "ModifyUserInServer") & " Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
            End If
        Catch ex As Exception
            ModifyUserInServer = 1

            StrResultado = "Error Impersonate catch" & ex.Message

            EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.AddUserToServer Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
        End Try
    End Function

    ''' <summary>
    ''' Borra el usuario de la máquina
    ''' </summary>
    ''' <param name="StrResultado">texto error de haberlo</param>
    ''' <returns>0-ok   1-error impersonate   2-error borrando </returns>
    ''' <remarks>Llamada desde: GS/CUsuario/RemoveUserFromServer; Tiempo maximo: 0</remarks>
    Public Function RemoveUserFromServer(ByRef StrResultado As String) As Integer
        Try
            If impersonateValidUser(mWinSecAdminUser, Environment.MachineName, mWinSecAdminPwd) Then
                Try
                    Dim de As New System.DirectoryServices.DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                    Dim Users As System.DirectoryServices.DirectoryEntries = de.Children
                    Dim User As System.DirectoryServices.DirectoryEntry = Users.Find(mvarCod, "user")

                    Users.Remove(User)

                    RemoveUserFromServer = 0
                Catch ex As Exception
                    RemoveUserFromServer = 2

                    StrResultado = ex.Message

                    EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.RemoveUserFromServer Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
                Finally
                    undoImpersonation()
                End Try
            Else
                RemoveUserFromServer = 1

                StrResultado = "Error Impersonate"

                EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.RemoveUserFromServer Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
            End If
        Catch ex As Exception
            RemoveUserFromServer = 1

            StrResultado = "Error Impersonate catch" & ex.Message

            EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.AddUserToServer Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
        End Try
    End Function

    ''' <summary>
    ''' Obtener los datos del usuario de la máquina
    ''' </summary>
    ''' <param name="StrResultado">texto error de haberlo</param>
    ''' <returns>0-ok   1-error impersonate   2-error obteniendo datos </returns>
    ''' <remarks>Llamada desde: GS/CUsuario/RetrieveUserProperties; Tiempo maximo: 0</remarks>
    Public Function RetrieveUserProperties(ByRef StrResultado As String) As Integer
        Try
            If impersonateValidUser(mWinSecAdminUser, Environment.MachineName, mWinSecAdminPwd) Then
                Try
                    Dim de As New System.DirectoryServices.DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                    Dim User As System.DirectoryServices.DirectoryEntry = de.Children.Find(mvarCod, "user")

                    Dim Flags As Object = User.Invoke("Get", New Object() {"userFlags"})

                    mAccountDisabled = ((Flags And ACCOUNT_DISABLED) = ACCOUNT_DISABLED)
                    mAccountLocked = ((Flags And ACCOUNT_LOCK_OUT) = ACCOUNT_LOCK_OUT)

                    RetrieveUserProperties = 0
                Catch ex As Exception
                    RetrieveUserProperties = 2

                    StrResultado = ex.Message

                    EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.RetrieveUserProperties Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
                Finally
                    undoImpersonation()
                End Try
            Else
                RetrieveUserProperties = 1

                StrResultado = "Error Impersonate"

                EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.RetrieveUserProperties Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
            End If
        Catch ex As Exception
            RetrieveUserProperties = 1

            StrResultado = "Error Impersonate catch" & ex.Message

            EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.AddUserToServer Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
        End Try
    End Function
#End Region

#Region "Sacado de FSGSBatchUpdates"
    ''' <summary>
    ''' Esta función renombra el usuario de windows
    ''' </summary>
    ''' <param name="sOldCod">Codigo viejo</param>
    ''' <param name="sNewCod">Codigo nuevo</param>
    ''' <param name="StrResultado">texto error de haberlo</param>
    ''' <returns>0-ok   1-error impersonate   2-error renombrando</returns>
    ''' <remarks>Llamada desde: GSBatchUpdates/basActCodigos/ActualizarCodigoUsuario; Tiempo maximo: 0</remarks>
    Public Function RenameUserInServer(ByVal sOldCod As String, ByVal sNewCod As String, ByRef StrResultado As String) As Integer
        Try
            If impersonateValidUser(mWinSecAdminUser, Environment.MachineName, mWinSecAdminPwd) Then
                Try
                    Dim de As New System.DirectoryServices.DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                    Dim User As System.DirectoryServices.DirectoryEntry = de.Children.Find(sOldCod, "user")

                    User.Rename(sNewCod)

                    User.CommitChanges()

                    RenameUserInServer = 0
                Catch ex As Exception
                    RenameUserInServer = 2

                    StrResultado = ex.Message

                    EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.RenameUserInServer Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
                Finally
                    undoImpersonation()
                End Try
            Else
                RenameUserInServer = 1

                StrResultado = "Error Impersonate"

                EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.RenameUserInServer Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
            End If
        Catch ex As Exception
            RenameUserInServer = 1

            StrResultado = "Error Impersonate catch" & ex.Message

            EventLog.WriteEntry("FSNLibraryCOM", "AdminUsers.AddUserToServer Instancia: " & mInstancia & ". Error: " & StrResultado, EventLogEntryType.Error, 20000)
        End Try
    End Function
#End Region

#Region "Sacado de FSNWebServicePWD"
    ''' <summary>
    ''' Esta función cambia password del usuario de windows
    ''' </summary>
    ''' <param name="StrResultado">texto error de haberlo</param>
    ''' <returns>0-ok   10-error impersonate   9-error cambio login</returns>
    ''' <remarks>Llamada desde: FSNWeb/FSNWebServicePWD/CambioPWD.asmx/CambiarLogin; Tiempo maximo: 0</remarks>
    Public Function CambiarLogin(ByRef StrResultado As String) As Integer
        Dim Resul As Integer = ModifyUserInServer(True, False, StrResultado, True)

        If Resul = 1 Then Resul = 10 '1 error impersonate ya estaba cogido, paso a usar 10
        If Resul = 2 Then Resul = 9 ' 9 Devolvía FSNWebServicePWD en caso de error en el cambio

        CambiarLogin = Resul
    End Function

#End Region

#Region "Sacado de FSGSSaveRegistry"

#Region "Variables FSGSSaveRegistry"
    Private Const REG_SZ As Long = 1
    Private Const REG_EXPAND_SZ As Long = 2
    Private Const REG_BINARY As Long = 3
    Private Const REG_DWORD As Long = 4

    Private Const BASE_KEY As String = "SOFTWARE\Microsoft\Office"

    Private Const STANDARD_RIGHTS_ALL As Long = &H1F0000
    Private Const KEY_QUERY_VALUE As Long = &H1
    Private Const KEY_SET_VALUE As Long = &H2
    Private Const KEY_CREATE_SUB_KEY As Long = &H4
    Private Const KEY_ENUMERATE_SUB_KEYS As Long = &H8
    Private Const KEY_NOTIFY As Long = &H10
    Private Const KEY_CREATE_LINK As Long = &H20
    Private Const SYNCHRONIZE As Long = &H100000
    Private Const KEY_ALL_ACCESS As Long = ((STANDARD_RIGHTS_ALL Or _
                                                        KEY_QUERY_VALUE Or _
                                                        KEY_SET_VALUE Or _
                                                        KEY_CREATE_SUB_KEY Or _
                                                        KEY_ENUMERATE_SUB_KEYS Or _
                                                        KEY_NOTIFY Or _
                                                        KEY_CREATE_LINK) _
                                                        And (Not SYNCHRONIZE))

    Private Const ADS_SECURE_AUTHENTICATION As Long = 1

    Private msLauncher As String
    Private msRuta As String
    Private msUnidad As String

    ''' <summary>
    '''  Devuelve/Establace la propiedad msLauncher
    ''' </summary>
    ''' <remarks>Llamada desde: AddUserToServer; Tiempo maximo:0</remarks>
    Property Launcher() As String
        Get
            Launcher = msLauncher
        End Get
        Set(ByVal Value As String)
            msLauncher = Value
        End Set
    End Property
    ''' <summary>
    '''  Devuelve/Establace la propiedad msRuta
    ''' </summary>
    ''' <remarks>Llamada desde: AddUserToServer; Tiempo maximo:0</remarks>
    Property Ruta() As String
        Get
            Ruta = msRuta
        End Get
        Set(ByVal Value As String)
            msRuta = Value
        End Set
    End Property
    ''' <summary>
    '''  Devuelve/Establace la propiedad msUnidad
    ''' </summary>
    ''' <remarks>Llamada desde: AddUserToServer; Tiempo maximo:0</remarks>
    Property Unidad() As String
        Get
            Unidad = msUnidad
        End Get
        Set(ByVal Value As String)
            msUnidad = Value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Establece el programa de inicio y el home directory
    ''' </summary>
    ''' <remarks>Llamada desde: AddUserToServer        ModifyUserInServer; Tiempo maximo: 0</remarks>
    Private Sub EjecutarFSGSSaveRegistryWHOF()
        Dim dso As Object
        Dim User As Object
        Dim str As String

        dso = GetObject("WinNT:")

        str = "WinNT://" & Environment.MachineName & "/" & mvarCod & ",user"
        User = dso.OpenDSObject(DblQuote(str), DblQuote(mvarCod), DblQuote(mvarPWDDes), ADS_SECURE_AUTHENTICATION)

        User.TerminalServicesInitialProgram = msLauncher

        If msUnidad = "NO" Then

            User.TerminalServicesHomeDirectory = msRuta

        Else
            User.TerminalServicesHomeDrive = msUnidad & ":"

            User.TerminalServicesHomeDirectory = msRuta
        End If

        User.SetInfo()

        User = Nothing
        dso = Nothing
    End Sub

    ''' <summary>
    ''' Modifica el registro para algunas opciones de Excel y Word
    ''' </summary>
    ''' <remarks>Llamada desde: AddUserToServer     ModifyUserInServer; Tiempo maximo: 0</remarks>
    Private Sub EjecutarFSGSSaveRegistryWREG()
        DevolverHomeFolder()
        OpcionesExcel()
        OpcionesWord()
    End Sub

    ''' <summary>
    ''' Devolver la ruta del Terminal Services
    ''' </summary>
    ''' <remarks>Llamada desde: EjecutarFSGSSaveRegistryWREG; Tiempo maximo: 0</remarks>
    Private Sub DevolverHomeFolder()
        Dim dso As Object
        Dim User As Object
        Dim str As String

        dso = GetObject("WinNT:")

        str = "WinNT://" & Environment.MachineName & "/" & mvarCod & ",user"
        User = dso.OpenDSObject(DblQuote(str), DblQuote(mvarCod), DblQuote(mvarPWDDes), ADS_SECURE_AUTHENTICATION)

        User.GetInfo()

        If User.TerminalServicesHomeDrive <> "" Then
            msRuta = CStr(User.TerminalServicesHomeDrive)
        Else
            msRuta = CStr(User.TerminalServicesHomeDirectory)
        End If

        User = Nothing
        dso = Nothing
    End Sub

    ''' <summary>
    ''' Transforma los ' por '' para  dso.OpenDSObject
    ''' </summary>
    ''' <param name="texto">texto q puede tener '</param>
    ''' <returns>Texto Transformado</returns>
    ''' <remarks>Llamada desde: DevolverHomeFolder ; Tiempo maximo: 0</remarks>
    Private Function DblQuote(ByVal texto As String) As String
        DblQuote = Replace(texto, "'", "''")
    End Function

    ''' <summary>
    ''' Modifica el registro para algunas opciones de Excel 
    ''' </summary>
    ''' <remarks>Llamada desde: EjecutarFSGSSaveRegistryWREG; Tiempo maximo: 0</remarks>
    Private Sub OpcionesExcel()
        Dim str As String

        str = GetOfficePath("Excel.Application", "Excel.exe")

        Dim sVersionOffice As String = GetProductVersion(str & "Excel.exe")

        If Val(sVersionOffice) >= Val("10.0") Then
            SaveStringSetting(sVersionOffice, "Excel", "Security", "AccessVBOM", 1, 4)
        End If

        SaveStringSetting(sVersionOffice, "Excel", "Security", "Level", 2, 4)

        SaveStringSetting(sVersionOffice, "Excel", "Options", "DefaultPath", msRuta, 2)

    End Sub

    ''' <summary>
    ''' Modifica el registro para algunas opciones de  Word
    ''' </summary>
    ''' <remarks>Llamada desde: EjecutarFSGSSaveRegistryWREG; Tiempo maximo: 0</remarks>
    Private Sub OpcionesWord()
        Dim str As String

        str = GetOfficePath("Word.Application", "Word.exe")

        Dim sVersionOffice As String = GetProductVersion(str & "Word.exe")

        SaveStringSetting(sVersionOffice, "Word", "Options", "DOC-PATH", msRuta, 2)

    End Sub

    ''' <summary>
    ''' Devolver path del programa sProgNom
    ''' </summary>
    ''' <param name="sProgId">Excel.Application/Word.Application</param>
    ''' <param name="sProgNom">Excel.exe/Word.exe</param>
    ''' <returns>path del programa</returns>
    ''' <remarks>Llamada desde: OpcionesExcel     OpcionesWord; Tiempo maximo: 0</remarks>
    Private Function GetOfficePath(ByVal sProgId As String, ByVal sProgNom As String) As String
        Dim rk As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine
        Dim sCLSID As String = rk.OpenSubKey("Software\Classes\" & sProgId & "\CLSID", True).GetValue("")
        rk.Close()
        Dim sPath As String = rk.OpenSubKey("Software\Classes\CLSID\" & sCLSID & "\LocalServer32", True).GetValue("")
        rk.Close()

        If InStr(1, UCase(sPath), UCase(sProgNom)) > 0 Then
            sPath = Left(sPath, InStr(1, UCase(sPath), UCase(sProgNom)) - 1)
        End If
        If Left(sPath, 1) = """" Then
            GetOfficePath = Right(sPath, Len(sPath) - 1)
        Else
            GetOfficePath = sPath
        End If
    End Function

    ''' <summary>
    ''' Devolver version del programa strExeFullPath
    ''' </summary>
    ''' <param name="strExeFullPath">Ruta del programa</param>
    ''' <returns>version del programa</returns>
    ''' <remarks>Llamada desde: OpcionesExcel     OpcionesWord; Tiempo maximo: 0</remarks>
    Private Function GetProductVersion(ByVal strExeFullPath As String) As String
        GetProductVersion = ""

        Dim myFileVersionInfo As FileVersionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(strExeFullPath)
        If Not (myFileVersionInfo Is Nothing) Then
            If myFileVersionInfo.FileMajorPart > 0 Then
                GetProductVersion = myFileVersionInfo.FileMajorPart & "." & myFileVersionInfo.FileMinorPart
            End If
        End If
    End Function

    ''' <summary>
    ''' Own version of VB's SaveStringSetting to store strings under
    ''' HKEY_CURRENT_USER\SOFTWARE instead of
    ''' HKEY_CURRENT_USER\Software\VB and VBA Program Settings
    ''' </summary>
    ''' <param name="sAppName">The first level</param>
    ''' <param name="sInstance">Instance</param>
    ''' <param name="sSection">The second level</param>
    ''' <param name="sKey">The key in the second level</param>
    ''' <param name="sSetting">The new value for the key</param>
    ''' <param name="iType">key Typ</param>
    ''' <remarks>Llamada desde: OpcionesExcel     OpcionesWord; Tiempo maximo: 0</remarks>
    Private Sub SaveStringSetting(ByVal sAppName As String, _
                                 ByVal sInstance As String, _
                                 ByVal sSection As String, _
                                 ByVal sKey As String, _
                                 ByVal sSetting As Object, _
                                 ByVal iType As Integer)
        Dim sNewKey As String
        Dim bRetVal As Boolean

        If Not (IsNumeric(sSetting)) AndAlso sSetting = "" Then
            sSetting = " "
        End If
        If Trim(sAppName) = "" Then
            Err.Raise(vbObjectError + 1000, , "AppName may not be empty")
        End If
        If Trim(sInstance) = "" Then
            Err.Raise(vbObjectError + 1001, , "Instance may not be empty")
        End If
        If Trim(sSection) = "" Then
            Err.Raise(vbObjectError + 1001, , "Section may not be empty")
        End If
        If Trim(sKey) = "" Then
            Err.Raise(vbObjectError + 1002, , "Key may not be empty")
        End If

        sNewKey = BASE_KEY & "\" & Trim(sAppName) & "\" & sInstance & "\" & Trim(sSection)

        Select Case iType
            Case 1
                bRetVal = RegCreateKeyExAndString(sNewKey, REG_SZ, Trim(sSection), KEY_ALL_ACCESS, True)
            Case 2
                bRetVal = RegCreateKeyExAndString(sNewKey, REG_EXPAND_SZ, Trim(sSection), KEY_ALL_ACCESS, True)
            Case 3
                bRetVal = RegCreateKeyExAndString(sNewKey, REG_DWORD, Trim(sSection), KEY_ALL_ACCESS, True)
            Case 4
                bRetVal = RegCreateKeyExAndString(sNewKey, REG_BINARY, Trim(sSection), KEY_ALL_ACCESS, True)
            Case Else
                bRetVal = RegCreateKeyExAndString(sNewKey, vbNullString, Trim(sSection), KEY_ALL_ACCESS, True)
        End Select

        If Not bRetVal Then
            Err.Raise(vbObjectError + 2000, , "Could not open/create registry section")
        End If

        Select Case iType
            Case 1
                bRetVal = RegCreateKeyExAndString(sNewKey, REG_SZ, Trim(sKey), sSetting)
            Case 2
                bRetVal = RegCreateKeyExAndString(sNewKey, REG_EXPAND_SZ, Trim(sKey), sSetting)
            Case 3
                bRetVal = RegCreateKeyExAndString(sNewKey, REG_BINARY, Trim(sKey), sSetting)
            Case 4
                bRetVal = RegCreateKeyExAndString(sNewKey, REG_DWORD, Trim(sKey), sSetting)
            Case Else
                bRetVal = RegCreateKeyExAndString(sNewKey, REG_SZ, Trim(sKey), sSetting)
        End Select

        If Not bRetVal Then
            Err.Raise(vbObjectError + 2000, , "Could not set key value")
        End If
    End Sub

    ''' <summary>
    ''' Crea ruta (si es necesario) y/o establece la entrada (param sSection) de la ruta
    ''' </summary>
    ''' <param name="sKey">Ruta de la entrada. Ejemplo: "SOFTWARE\Microsoft\Office\Excel\14.0\Security</param>
    ''' <param name="ValueKind">Tipo de valor con el q updatar</param>
    ''' <param name="sSection">Entrada a updatar. Ejemplo: AccessVBOM</param>
    ''' <param name="Valor">Valor de la entrada</param>
    ''' <param name="SoloCreate">True->No establecer nada solo mirar existencia de la ruta y crearla si es necesario</param>
    ''' <returns>Si ha ido todo bien o no</returns>
    ''' <remarks>Llamada desde: SaveStringSetting; Tiempo maximo: 0</remarks>
    Private Function RegCreateKeyExAndString(ByVal sKey As String, ByVal ValueKind As Microsoft.Win32.RegistryValueKind, ByVal sSection As String _
                                               , ByVal Valor As Object, Optional ByVal SoloCreate As Boolean = False) As Boolean
        Dim rk As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.CurrentUser
        Try
            If Not (rk.OpenSubKey(sKey, True) Is Nothing) Then
            Else
                Dim RutaNewKey As String = Replace(sKey, "\" & sSection, "")
                rk.OpenSubKey(RutaNewKey, True).CreateSubKey(sSection)
            End If

            If Not SoloCreate Then rk.OpenSubKey(sKey, True).SetValue(sSection, Valor, ValueKind)

            RegCreateKeyExAndString = True
        Catch ex As Exception
            RegCreateKeyExAndString = False
        Finally
            rk.Close()
        End Try
    End Function
#End Region

End Class
