﻿Imports System.ServiceModel

<ComClass(PasswordPolicy.ClassId, PasswordPolicy.InterfaceId, PasswordPolicy.EventsId)> _
Public Class PasswordPolicy
#Region "COM GUIDs"
    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.
    Public Const ClassId As String = "50C14B32-F01A-4228-A9DE-246927F556A9"
    Public Const InterfaceId As String = "DC99D939-6638-4397-B572-CED89BAAACAC"
    Public Const EventsId As String = "9637B259-DE96-45E4-AD4F-0FCF9FECBAB5"
#End Region
#Region "Properties"
    Private _urlServicio As String
    Public Property URLServicio() As String
        Get
            Return _urlServicio
        End Get
        Set(ByVal value As String)
            _urlServicio = value
        End Set
    End Property
    Private _usuImpersonate As String
    Public Property UsuImpersonate() As String
        Get
            Return _usuImpersonate
        End Get
        Set(ByVal value As String)
            _usuImpersonate = value
        End Set
    End Property
    Private _pwdImpersonate As String
    Public Property PWDImpersonate() As String
        Get
            Return _pwdImpersonate
        End Get
        Set(ByVal value As String)
            _pwdImpersonate = value
        End Set
    End Property
    Private _dominioImpersonate As String
    Public Property DominioImpersonate() As String
        Get
            Return _dominioImpersonate
        End Get
        Set(ByVal value As String)
            _dominioImpersonate = value
        End Set
    End Property
    Private _usuId As Integer
    Public Property UsuId() As Integer
        Get
            Return _usuId
        End Get
        Set(ByVal value As Integer)
            _usuId = value
        End Set
    End Property
    Private _usuCia As Integer
    Public Property UsuCia() As Integer
        Get
            Return _usuCia
        End Get
        Set(ByVal value As Integer)
            _usuCia = value
        End Set
    End Property
    Private _usuCod As String
    Public Property UsuCod() As String
        Get
            Return _usuCod
        End Get
        Set(ByVal value As String)
            _usuCod = value
        End Set
    End Property
    Private _usuNewPWD As String
    Public Property UsuNewPWD() As String
        Get
            Return _usuNewPWD
        End Get
        Set(ByVal value As String)
            _usuNewPWD = value
        End Set
    End Property
    Private _fecPWD As Date
    Public Property FecPWD() As Date
        Get
            Return _fecPWD
        End Get
        Set(ByVal value As Date)
            _fecPWD = value
        End Set
    End Property
    Private _salt As String
    Public Property Salt() As String
        Get
            Return _salt
        End Get
        Set(ByVal value As String)
            _salt = value
        End Set
    End Property
    Private _pwd As String
    Public Property PWD() As String
        Get
            Return _pwd
        End Get
        Set(ByVal value As String)
            _pwd = value
        End Set
    End Property
#End Region
    Public Sub New()
        MyBase.New()
    End Sub
    Public Function LlamarWCFChangePasswordPortal() As FSNPasswordPolicy.ResultadoPasswordChange
        Dim resultadoChange As New FSNPasswordPolicy.ResultadoPasswordChange
        Try
            Dim basicHttpBinding As New BasicHttpBinding
            With basicHttpBinding
                With .Security
                    .Mode = BasicHttpSecurityMode.TransportCredentialOnly
                    .Transport.ClientCredentialType = HttpClientCredentialType.Windows
                    .Transport.ProxyCredentialType = HttpProxyCredentialType.None
                    .Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
                End With
            End With
            Dim service As New FSNPasswordPolicyServiceClient(basicHttpBinding, New EndpointAddress(URLServicio))
            With service
                .ClientCredentials.Windows.ClientCredential.UserName = UsuImpersonate
                .ClientCredentials.Windows.ClientCredential.Password = PWDImpersonate
                .ClientCredentials.Windows.ClientCredential.Domain = DominioImpersonate
            End With
            resultadoChange = service.PasswordChangePortal(UsuCia, UsuId, UsuCod, FecPWD, UsuNewPWD)
            If resultadoChange.TipoMensaje = 0 Then
                _pwd = resultadoChange.Password
                _salt = resultadoChange.Salt
            End If
            Return resultadoChange
        Catch ex As Exception
            With resultadoChange
                .TipoMensaje = -55
                .Mensaje = ex.Message
            End With
            Return resultadoChange
        End Try
    End Function
    Public Function ValidarPasswordPoliticaSeguridad() As FSNPasswordPolicy.ResultadoPasswordChange
        Dim resultadoChange As New FSNPasswordPolicy.ResultadoPasswordChange
        Try
            Dim basicHttpBinding As New BasicHttpBinding
            With basicHttpBinding
                With .Security
                    .Mode = BasicHttpSecurityMode.TransportCredentialOnly
                    .Transport.ClientCredentialType = HttpClientCredentialType.Windows
                    .Transport.ProxyCredentialType = HttpProxyCredentialType.None
                    .Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
                End With
            End With

            Dim service As New FSNPasswordPolicyServiceClient(basicHttpBinding, New EndpointAddress(URLServicio))
            With service
                .ClientCredentials.Windows.ClientCredential.UserName = UsuImpersonate
                .ClientCredentials.Windows.ClientCredential.Password = PWDImpersonate
                .ClientCredentials.Windows.ClientCredential.Domain = DominioImpersonate
            End With
            resultadoChange = service.ValidatePasswordPoliticaSeguridad(UsuCod, UsuNewPWD, FecPWD)
            If resultadoChange.TipoMensaje = 0 Then _pwd = resultadoChange.Password
            Return resultadoChange
        Catch ex As Exception
            With resultadoChange
                .TipoMensaje = -55
                .Mensaje = ex.Message
            End With
            Return resultadoChange
        End Try
    End Function
End Class