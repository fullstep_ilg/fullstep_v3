﻿Imports System.Net

<ComClass(Email.ClassId, Email.InterfaceId, Email.EventsId)> _
Public Class Email

#Region "COM GUIDs"
    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.   
    Public Const ClassId As String = "1A11DCB5-0315-4F61-A0F8-2928CC7AA450"
    Public Const InterfaceId As String = "1DB2E537-EE55-4CC8-9467-39FE723EE8C0"
    Public Const EventsId As String = "BF88D227-C113-474A-8F9F-B6724DD2A37B"
#End Region

#Region "Definicion Propertys"
    Private mSmtpServer As String
    Private mSmtpPort As Integer
    Private mSmtpEnableSsl As Boolean
    Private mSmtpDeliveryMethod As Integer
    Private mSmtpAuthentication As Integer
    Private mSmtpUsu As String
    Private mSmtpPwd As String
    Private mSmtpDominio As String
    Private mSmtpClient As Mail.SmtpClient
    Private mCrearConexionSmtp As Boolean

    Private mClaseCom As Integer

    Public Enum Aplicaciones
        EP = &H7FFF
        PM = &HFFFF
        QA = &H1FFFF
        Otra = &H5FFF
    End Enum
#End Region

#Region "Establecer Propertys"
    ''' <summary>
    '''  Devuelve/Establace la propiedad mSmtpServer
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los Notificar (gs, portal, Pm, Int); Tiempo maximo:0</remarks>
    Property SmtpServer() As String
        Get
            SmtpServer = mSmtpServer
        End Get
        Set(ByVal Value As String)
            mSmtpServer = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mSmtpPort
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los Notificar (gs, portal, Pm, Int); Tiempo maximo:0</remarks>
    Property SmtpPort() As Integer
        Get
            SmtpPort = mSmtpPort
        End Get
        Set(ByVal Value As Integer)
            mSmtpPort = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mSmtpUsu
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los Notificar (gs, portal, Pm, Int); Tiempo maximo:0</remarks>
    Property SmtpUsu() As String
        Get
            SmtpUsu = mSmtpUsu
        End Get
        Set(ByVal Value As String)
            mSmtpUsu = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mSmtpPwd
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los Notificar (gs, portal, Pm, Int); Tiempo maximo:0</remarks>
    Property SmtpPwd() As String
        Get
            SmtpPwd = mSmtpPwd
        End Get
        Set(ByVal Value As String)
            mSmtpPwd = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mSmtpDominio
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los Notificar (gs, portal, Pm, Int); Tiempo maximo:0</remarks>
    Property SmtpDominio() As String
        Get
            SmtpDominio = mSmtpDominio
        End Get
        Set(ByVal Value As String)
            mSmtpDominio = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mSmtpEnableSsl
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los Notificar (gs, portal, Pm, Int); Tiempo maximo:0</remarks>
    Property SmtpEnableSsl() As Boolean
        Get
            SmtpEnableSsl = mSmtpEnableSsl
        End Get
        Set(ByVal Value As Boolean)
            mSmtpEnableSsl = Value
        End Set
    End Property

    ''' <summary>
    '''  Devuelve/Establace la propiedad mSmtpDeliveryMethod
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los Notificar (gs, portal, Pm, Int); Tiempo maximo:0</remarks>
    Property SmtpDeliveryMethod() As Integer
        Get
            SmtpDeliveryMethod = mSmtpDeliveryMethod
        End Get
        Set(ByVal Value As Integer)
            mSmtpDeliveryMethod = Value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve/Establace la propiedad mSmtpAuthentication
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los Notificar (gs, portal, Pm, Int); Tiempo maximo:0</remarks>
    Property SmtpAuthentication() As Integer
        Get
            SmtpAuthentication = mSmtpAuthentication
        End Get
        Set(ByVal Value As Integer)
            mSmtpAuthentication = Value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve/Establace la propiedad mClaseCom
    ''' Necesario pq las clases COM no admiten el new con parametros
    ''' 0- No com (valor por defecto)
    ''' 1- com y mCrearConexionSmtp = true           (Sin esto GS/FSP/PS/Notif GS no crea el objeto STMP compartido)
    ''' 2- com y mCrearConexionSmtp = false          (GS/FSP/PS/Notif GS)
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los Notificar (gs, portal, Pm, Int); Tiempo maximo:0</remarks>
    Property ClaseCom() As Integer
        Get
            ClaseCom = mClaseCom
        End Get
        Set(ByVal Value As Integer)
            mClaseCom = Value

            If mClaseCom > 0 Then
                mCrearConexionSmtp = (mClaseCom = 1)
                If mCrearConexionSmtp Then 'Necesario pq no se ha realizado la creacion desde el new
                    CreacionObjetoSTMP()
                End If
            End If
        End Set
    End Property

#End Region

#Region "Funciones Creación/Destrucción Objeto SmtpClient"
    ''' <summary>
    ''' A creatable COM class must have a Public Sub New() 
    ''' with no parameters, otherwise, the class will not be 
    ''' registered in the COM registry and cannot be created 
    ''' via CreateObject.
    ''' </summary>
    ''' <remarks>Llamada desde: Gs, Fsp, Ps y Notif GS; Tiempo maximo:0</remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Destruir el objeto Mail.SmtpClient completamente, liberando memoria
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb; Tiempo maximo:0</remarks>
    Protected Overrides Sub Finalize()
        MyBase.Finalize()

        If mCrearConexionSmtp Then
            mSmtpClient.Dispose()
        End If
    End Sub

    ''' <summary>
    ''' FUERZA al sistema a destruir el objeto Mail.SmtpClient completamente, liberando memoria
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb; Tiempo maximo:0</remarks>
    Public Sub FuerzaFinalizeSmtpClient()
        If mCrearConexionSmtp Then
            mSmtpClient.Dispose()
        End If
    End Sub

    ''' <summary>
    ''' Constructor de la clase con parametros de entrada para crear el SmtpClient
    ''' </summary>
    ''' <param name="pSmtpServer">Servidor</param>
    ''' <param name="pSmtpPort">Puerto</param>
    ''' <param name="pSmtpAuthentication">Autenticación</param>
    ''' <param name="pSmtpEnableSsl">Si se usa ssl/tls o no</param>
    ''' <param name="pSmtpDeliveryMethod">Metodo de entrega</param>
    ''' <param name="pSmtpUsu">Usuario/Cuenta de servicio</param>
    ''' <param name="pSmtpPwd">Password</param>
    ''' <param name="pSmtpDominio">Dominio</param>
    ''' <param name="pCrearConexionSmtp">Crear o no el objeto mail</param>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb; Tiempo maximo:0</remarks>
    Public Sub New(ByVal pSmtpServer As String, ByVal pSmtpPort As Integer, ByVal pSmtpAuthentication As Integer, Optional ByVal pSmtpEnableSsl As Boolean = False _
    , Optional ByVal pSmtpDeliveryMethod As Integer = 0, Optional ByVal pSmtpUsu As String = "", Optional ByVal pSmtpPwd As String = "" _
    , Optional ByVal pSmtpDominio As String = "", Optional ByVal pCrearConexionSmtp As Boolean = True)

        MyBase.New()

        mSmtpServer = pSmtpServer
        mSmtpPort = pSmtpPort
        mSmtpEnableSsl = pSmtpEnableSsl
        mSmtpDeliveryMethod = pSmtpDeliveryMethod
        mSmtpAuthentication = pSmtpAuthentication
        mSmtpUsu = pSmtpUsu
        mSmtpPwd = pSmtpPwd
        mSmtpDominio = pSmtpDominio

        mCrearConexionSmtp = pCrearConexionSmtp
        If mCrearConexionSmtp Then
            CreacionObjetoSTMP()
        End If
    End Sub

    ''' <summary>
    ''' Crea el SmtpClient
    ''' </summary>
    ''' <remarks>Llamada desde: New y EnviarMail; Tiempo maximo:0</remarks>
    Private Sub CreacionObjetoSTMP()
        Try
            mSmtpClient = New Mail.SmtpClient

            mSmtpClient.Host = mSmtpServer
            mSmtpClient.Port = mSmtpPort

            mSmtpClient.EnableSsl = mSmtpEnableSsl

            If mSmtpDeliveryMethod = Nothing Then
                mSmtpClient.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
            Else
                Select Case CInt(mSmtpDeliveryMethod)
                    Case 1
                        mSmtpClient.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.SpecifiedPickupDirectory
                    Case 2
                        mSmtpClient.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.PickupDirectoryFromIis
                    Case Else
                        mSmtpClient.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
                End Select
            End If

            Select Case mSmtpAuthentication
                Case 1 'basica
                    mSmtpClient.UseDefaultCredentials = False
                    mSmtpClient.Credentials = New NetworkCredential(mSmtpUsu, mSmtpPwd)
                    If mSmtpUsu = Nothing OrElse mSmtpUsu = "" Then
                        mSmtpClient.Credentials = CredentialCache.DefaultNetworkCredentials
                    End If
                Case 2 'Windows
                    'impersonateValidUser(mSmtpUsu, mSmtpDominio, mSmtpPwd)

                    mSmtpClient.UseDefaultCredentials = False
                    mSmtpClient.Credentials = New NetworkCredential(mSmtpUsu, mSmtpPwd, mSmtpDominio)

                    'undoImpersonation()
                Case Else 'Anonimo
                    mSmtpClient.UseDefaultCredentials = False
                    If Not mSmtpEnableSsl Then
                        mSmtpClient.Credentials = CredentialCache.DefaultNetworkCredentials
                    Else
                        mSmtpClient.Credentials = New NetworkCredential(mSmtpUsu, mSmtpPwd)
                    End If
            End Select

        Catch ex As Exception
            WriteErrorToLog(ex, "FSNLibraryCOM.Email")
            Throw ex
        End Try
    End Sub
#End Region

#Region "Funciones Envio Mails"
    ''' <summary>
    ''' Función que realiza el envío de los emails
    ''' </summary>
    ''' <param name="CuentaServicio">Cuenta Servicio desde el q se envian todos los mails</param>
    ''' <param name="From">El from del mensaje</param>
    ''' <param name="Subject">El asunto</param>
    ''' <param name="Para">El destinatario</param>
    ''' <param name="Message">El mensaje</param>
    ''' <param name="FromName">Nombre y apellidos del q envia el mail</param>
    ''' <param name="ToName">Nombre y apellidos del q recibe el mail</param>
    ''' <param name="CC">Copia a</param>
    ''' <param name="BCC">Copia oculta</param>
    ''' <param name="Attachments">Cadena con los adjuntos</param>
    ''' <param name="AttachmentsPath">Ruta de los adjuntos</param>
    ''' <param name="isHTML">Si el formato del email es HTML o no</param>
    ''' <param name="EnvioAsincrono">Enviar Asincrono o no</param>
    ''' <param name="AcuseRecibo">Si se pide acuse de recibo</param>
    ''' <param name="iError">Numero error de haberlo</param>
    ''' <param name="strError">Error de haberlo</param>
    ''' <param name="Producto">para envios asincronos. Donde guardo q producto es. Se necesita para grabar envio resto de parametros son del tipo from, subject ...-> del mailmessage </param>
    ''' <param name="Grabar">para envios asincronos. Donde guardo si se guarda o no el envio.</param>
    ''' <remarks>Llamada desde: Todas las funciones de Notificación de Fullstep Web + Gs + Portal + Servicio Notificacion Gs. Tiempo máximo: 1 sg.</remarks>
    Public Sub EnviarMail(ByVal CuentaServicio As String, ByVal From As String, ByVal Subject As String, ByVal Para As String, ByVal Message As String, Optional ByVal FromName As String = "" _
    , Optional ByVal ToName As String = "", Optional ByVal CC As String = "", Optional ByVal BCC As String = "", Optional ByVal Attachments As String = "" _
    , Optional ByVal AttachmentsPath As String = "", Optional ByVal isHTML As Boolean = False, Optional ByVal EnvioAsincrono As Boolean = False _
    , Optional ByVal AcuseRecibo As Boolean = False, Optional ByRef iError As Integer = 2, Optional ByRef strError As String = "" _
    , Optional ByVal Producto As Aplicaciones = Aplicaciones.EP, Optional ByVal Grabar As Boolean = False)

        Dim MyMail As New Mail.MailMessage
        Dim myAttachment As Mail.Attachment
        Dim i As Integer = 0
        Dim sFichero As String
        Dim Num As Integer = 0

        If Not mCrearConexionSmtp Then
            CreacionObjetoSTMP()
        End If

        Try
            iError = 2
            strError = ""

            MyMail.From = New Mail.MailAddress(CuentaServicio)

            MyMail.Subject = Replace(Replace(Replace(Subject, vbCrLf, " "), vbCr, " "), vbLf, " ")

            Para = Replace(Replace(Para, "(", "<"), ")", ">")
            CC = Replace(Replace(CC, "(", "<"), ")", ">")
            BCC = Replace(Replace(BCC, "(", "<"), ")", ">")

            Para = Replace(Replace(Para, "[", "<"), "]", ">")
            CC = Replace(Replace(CC, "[", "<"), "]", ">")
            BCC = Replace(Replace(BCC, "[", "<"), "]", ">")

            Dim strEmails() As String
            Dim strEmail As String

            strEmails = Split(Para, ";")
            For Each strEmail In strEmails
                If strEmail <> "" Then
                    If ValidateEmail(strEmail) Then
                        MyMail.To.Add(Trim(strEmail))
                    Else
                        iError = 4
                        strError = strError & strEmail & ","
                        Num = Num + 1
                    End If
                End If
            Next

            If CC <> Nothing Then
                strEmails = Split(CC, ";")
                For Each strEmail In strEmails
                    If strEmail <> "" Then
                        If ValidateEmail(strEmail) Then
                            MyMail.CC.Add(Trim(strEmail))
                        Else
                            iError = 4
                            strError = strError & strEmail & ","
                            Num = Num + 1
                        End If
                    End If
                Next
            End If

            If BCC <> Nothing Then
                strEmails = Split(BCC, ";")
                For Each strEmail In strEmails
                    If strEmail <> "" Then
                        If ValidateEmail(strEmail) Then
                            MyMail.Bcc.Add(Trim(strEmail))
                        Else
                            iError = 4
                            strError = strError & strEmail & ","
                            Num = Num + 1
                        End If
                    End If
                Next
            End If

            MyMail.Body = Message

            Dim auxAdjuntos As String = Attachments
            If AttachmentsPath <> "" Then
                Try
                    While InStr(auxAdjuntos, ";", CompareMethod.Binary)
                        sFichero = Left(auxAdjuntos, InStr(auxAdjuntos, ";", CompareMethod.Binary) - 1)
Etiqueta:
                        If Not (LCase(Right(sFichero, 4)) = ".pdf") Then
                            myAttachment = New Mail.Attachment(AttachmentsPath & sFichero, System.Net.Mime.MediaTypeNames.Application.Octet)
                        Else
                            'www.codepal.co.uk/show/ASPNET_Net_Mail_attaches_DAT_files_to_emails 
                            myAttachment = New System.Net.Mail.Attachment(AttachmentsPath & sFichero, System.Net.Mime.MediaTypeNames.Application.Pdf)
                        End If

                        If mClaseCom > 0 Then
                            If Strings.InStr(sFichero, "ñ", CompareMethod.Text) > 0 OrElse Strings.InStr(sFichero, "á", CompareMethod.Text) > 0 _
                            OrElse Strings.InStr(sFichero, "é", CompareMethod.Text) > 0 OrElse Strings.InStr(sFichero, "í", CompareMethod.Text) > 0 _
                            OrElse Strings.InStr(sFichero, "ó", CompareMethod.Text) > 0 OrElse Strings.InStr(sFichero, "ú", CompareMethod.Text) > 0 _
                            OrElse Strings.InStr(sFichero, "Ñ", CompareMethod.Text) > 0 OrElse Strings.InStr(sFichero, "Á", CompareMethod.Text) > 0 _
                            OrElse Strings.InStr(sFichero, "É", CompareMethod.Text) > 0 OrElse Strings.InStr(sFichero, "Í", CompareMethod.Text) > 0 _
                            OrElse Strings.InStr(sFichero, "Ó", CompareMethod.Text) > 0 OrElse Strings.InStr(sFichero, "Ú", CompareMethod.Text) > 0 _
                            OrElse Strings.InStr(sFichero, "à", CompareMethod.Text) > 0 OrElse Strings.InStr(sFichero, "è", CompareMethod.Text) > 0 _
                            OrElse Strings.InStr(sFichero, "ì", CompareMethod.Text) > 0 OrElse Strings.InStr(sFichero, "ò", CompareMethod.Text) > 0 _
                            OrElse Strings.InStr(sFichero, "ú", CompareMethod.Text) > 0 OrElse Strings.InStr(sFichero, "À", CompareMethod.Text) > 0 _
                            OrElse Strings.InStr(sFichero, "È", CompareMethod.Text) > 0 OrElse Strings.InStr(sFichero, "Ì", CompareMethod.Text) > 0 _
                            OrElse Strings.InStr(sFichero, "Ò", CompareMethod.Text) > 0 OrElse Strings.InStr(sFichero, "Ù", CompareMethod.Text) > 0 Then
                                sFichero = Replace(Replace(sFichero, "ñ", "n"), "Ñ", "N")
                                sFichero = Replace(Replace(Replace(Replace(sFichero, "á", "a"), "Á", "A"), "à", "a"), "À", "A")
                                sFichero = Replace(Replace(Replace(Replace(sFichero, "é", "e"), "É", "E"), "è", "e"), "È", "E")
                                sFichero = Replace(Replace(Replace(Replace(sFichero, "í", "i"), "Í", "I"), "ì", "i"), "Ì", "I")
                                sFichero = Replace(Replace(Replace(Replace(sFichero, "ó", "o"), "Ó", "O"), "ò", "o"), "Ò", "O")
                                sFichero = Replace(Replace(Replace(Replace(sFichero, "ú", "u"), "Ú", "U"), "ù", "u"), "Ù", "U")
                            End If
                        End If

                        myAttachment.Name = sFichero
                        MyMail.Attachments.Add(myAttachment)

                        auxAdjuntos = Right(auxAdjuntos, auxAdjuntos.Length - InStr(auxAdjuntos, ";", CompareMethod.Binary))
                    End While
                Catch ex As Exception
                    WriteErrorToLog(ex, "FSNLibraryCOM.Email")

                    'Si el error es que el archivo esta abierto, lo copio y repito operacion
                    Dim HResult As Int64 = System.Runtime.InteropServices.Marshal.GetHRForException(ex)
                    If HResult = -2147024864 Then
                        Dim sFile As String = System.IO.Path.GetFileNameWithoutExtension(AttachmentsPath & sFichero)
                        Dim sExt As String = System.IO.Path.GetExtension(AttachmentsPath & sFichero)
                        i = i + 1
                        sFile = sFile & "_" & i & sExt
                        System.IO.File.Copy(AttachmentsPath & sFichero, AttachmentsPath & sFile, True)
                        sFichero = sFile
                        GoTo Etiqueta
                    End If

                    iError = 1
                    strError = "Algun adjunto ha fallado"
                    If Not myAttachment Is Nothing Then myAttachment.Dispose()
                    MyMail = Nothing
                    myAttachment = Nothing

                    'El finally se encarga
                    'If Not mCrearConexionSmtp Then
                    '    mSmtpClient.Dispose()
                    '    mSmtpClient = Nothing
                    'End If

                    Exit Sub
                End Try

            End If

            MyMail.Priority = Mail.MailPriority.Normal
            MyMail.IsBodyHtml = isHTML

            If String.IsNullOrEmpty(From) Then From = CuentaServicio 'Antes se usaba ConfigurationManager.AppSettings("NotificationFrom")

            If AcuseRecibo AndAlso From <> "" Then
                MyMail.Headers.Add("Disposition-Notification-To", "<" & From & ">")
            End If

            If iError = 4 Then strError = strError & "###" & CStr(Num)

            If EnvioAsincrono Then
                Dim mens As New MiMensaje(MyMail)
                mens.Producto = Producto
                mens.Grabar = Grabar

                AddHandler mSmtpClient.SendCompleted, AddressOf EnviomailAsincrono_Complete
                mSmtpClient.SendAsync(mens, mens)
            Else
                mSmtpClient.Send(MyMail)
            End If

            MyMail.Dispose()
            If Not myAttachment Is Nothing Then myAttachment.Dispose()
        Catch ex As System.Security.SecurityException
            WriteErrorToLog(ex, "FSNLibraryCOM.Email")

            'error en log
            strError = "Error en log"
            iError = 3

        Catch ex As Exception
            WriteErrorToLog(ex, "FSNLibraryCOM.Email")

            If Num = 0 Then 'Num>0 cuando detecta mal el mail. Mas explicativo "mal el mail xxÑx@f.com" que "Must define a receipt"
                iError = 0
                strError = ex.Message
            Else
                iError = 5
                strError = strError & "|||" & ex.Message
            End If
        Finally
            If Not MyMail Is Nothing Then MyMail.Dispose()
            If Not myAttachment Is Nothing Then myAttachment.Dispose()
            MyMail = Nothing
            myAttachment = Nothing

            If Not mCrearConexionSmtp Then
                mSmtpClient.Dispose()
                mSmtpClient = Nothing
            End If
        End Try
    End Sub


    ''' <summary>
    ''' Validar cadenas que representan direcciones de correo electrónico 
    ''' </summary>
    ''' <param name="email">direccion de correo electrónico</param>
    ''' <returns>correcto o incorrecto</returns>
    ''' <remarks>Llamada desde:EnviarMail; Tiempo máximo: 0sg</remarks>
    Function ValidateEmail(ByVal email As String) As Boolean
        If Strings.InStr(email, "ñ", CompareMethod.Text) > 0 OrElse Strings.InStr(email, "á", CompareMethod.Text) > 0 _
        OrElse Strings.InStr(email, "é", CompareMethod.Text) > 0 OrElse Strings.InStr(email, "í", CompareMethod.Text) > 0 _
        OrElse Strings.InStr(email, "ó", CompareMethod.Text) > 0 OrElse Strings.InStr(email, "ú", CompareMethod.Text) > 0 Then
            Return False
        End If

        Dim emailRegex As New System.Text.RegularExpressions.Regex(
            "^(?<user>[^@]+)@(?<host>.+)$")
        Dim emailMatch As System.Text.RegularExpressions.Match =
           emailRegex.Match(email)
        Return emailMatch.Success
    End Function

#End Region

#Region "Asincrono"
    ''' <summary>
    ''' Evento de Envio asincrono completado
    ''' </summary>
    ''' <param name="Sender">SmtpClient</param>
    ''' <param name="E">Objeto MiMensaje o Nothing (sin error en envio)</param>
    ''' <remarks>Llamada desde: Envio asincrono completado; Tiempo maximo:0</remarks>
    Public Event FinEnviomailAsincrono(ByVal Sender As Object, ByVal E As EventArgs)

    ''' <summary>
    ''' Tras un Envio asincrono, aqui se sabe si ha ido bien o mal.
    ''' </summary>
    ''' <param name="Sender">SmtpClient</param>
    ''' <param name="E">Resultado del envio. Ok-nothing KO-una excepción</param>
    ''' <remarks>Llamada desde: FSNServer. Envio asincrono</remarks>
    Protected Sub EnviomailAsincrono_Complete(ByVal Sender As Object, ByVal E As EventArgs)
        RaiseEvent FinEnviomailAsincrono(Sender, E)
    End Sub

    Public Class MiMensaje
        Inherits Mail.MailMessage

        Private _Grabar As Boolean
        Private _Producto As Integer

        ''' <summary>
        ''' Establecer/Devolver la propiedad. Se graba mail_registro o no.
        ''' </summary>
        ''' <remarks>Llamada desde: FSNServer y FSNEmail/EnviarMail. Envio asincrono</remarks>
        Public Property Grabar() As Boolean
            Get
                Return _Grabar
            End Get
            Set(ByVal value As Boolean)
                _Grabar = value
            End Set
        End Property

        ''' <summary>
        ''' Establecer/Devolver la propiedad. De q producto es el mail asincrono.
        ''' </summary>
        ''' <remarks>Llamada desde: FSNServer y FSNEmail/EnviarMail. Envio asincrono</remarks>
        Public Property Producto() As Integer
            Get
                Return _Producto
            End Get
            Set(ByVal value As Integer)
                _Producto = value
            End Set
        End Property

        ''' <summary>
        ''' Crea un objeto MiMensaje a partir de un objeto MailMessage
        ''' </summary>
        ''' <param name="Mensaje">Objeto MailMessage a partir del cual crear el objeto MiMensaje</param>
        ''' <remarks>Llamada desde: FSNServer y FSNEmail/EnviarMail. Envio asincrono</remarks>
        Public Sub New(ByVal Mensaje As Mail.MailMessage)
            MyBase.New()
            Me.From = Mensaje.From
            For Each address As Mail.MailAddress In Mensaje.To
                Me.To.Add(address)
            Next
            For Each address As Mail.MailAddress In Mensaje.CC
                Me.CC.Add(address)
            Next
            For Each address As Mail.MailAddress In Mensaje.Bcc
                Me.Bcc.Add(address)
            Next
            Me.ReplyTo = Mensaje.ReplyTo
            Me.Sender = Mensaje.Sender
            For Each attach As Mail.Attachment In Mensaje.Attachments
                Me.Attachments.Add(attach)
            Next
            Me.Subject = Mensaje.Subject
            Me.SubjectEncoding = Mensaje.SubjectEncoding
            Me.Body = Mensaje.Body
            Me.BodyEncoding = Mensaje.BodyEncoding
            Me.IsBodyHtml = Mensaje.IsBodyHtml
            Me.DeliveryNotificationOptions = Mensaje.DeliveryNotificationOptions
            Me.Priority = Mensaje.Priority
        End Sub

    End Class

    Private Sub WriteErrorToLog(ByVal ex As Exception, ByVal Where As String)
        Dim sMessage As String = "[" & Date.Now.ToString & "]" & Environment.NewLine
        sMessage &= "SOURCE: " & Where & Environment.NewLine
        sMessage &= "Error Message:" & Environment.NewLine
        sMessage &= ex.Message & Environment.NewLine
        sMessage &= "STACK TRACE:" & Environment.NewLine & ex.StackTrace

        Dim oFile = New System.IO.StreamWriter("FSNLibraryCOMLog.txt", True)
        oFile.WriteLine(sMessage)
        oFile.Close()
    End Sub

#End Region
End Class
