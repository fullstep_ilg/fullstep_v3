﻿Imports System.Runtime.InteropServices
<ComVisibleAttribute(False)> _
Public Class PasswordPolicyServer
#Region "Properties"
    Private mDBServer As DatabaseServer
    Private ReadOnly Property DBServer() As DatabaseServer
        Get
            If mDBServer Is Nothing Then
                mDBServer = New DatabaseServer(_connectionString)
            End If
            Return mDBServer
        End Get
    End Property
    Private _connectionString As String
    Public Property ConnectionString() As String
        Get
            Return _connectionString
        End Get
        Set(ByVal value As String)
            _connectionString = value
        End Set
    End Property
    Private _EDAD_MAX_PWD As Integer
    Public Property EDAD_MAX_PWD() As Integer
        Get
            Return _EDAD_MAX_PWD
        End Get
        Set(ByVal value As Integer)
            _EDAD_MAX_PWD = value
        End Set
    End Property
    Private _EDAD_MIN_PWD As Integer
    Public Property EDAD_MIN_PWD() As Integer
        Get
            Return _EDAD_MIN_PWD
        End Get
        Set(ByVal value As Integer)
            _EDAD_MIN_PWD = value
        End Set
    End Property
    Private _HISTORICO_PWD As Integer
    Public Property HISTORICO_PWD() As Integer
        Get
            Return _HISTORICO_PWD
        End Get
        Set(ByVal value As Integer)
            _HISTORICO_PWD = value
        End Set
    End Property
    Private _COMPLEJIDAD_PWD As Boolean
    Public Property COMPLEJIDAD_PWD() As Boolean
        Get
            Return _COMPLEJIDAD_PWD
        End Get
        Set(ByVal value As Boolean)
            _COMPLEJIDAD_PWD = value
        End Set
    End Property
    Private _MIN_SIZE_PWD As Integer
    Public Property MIN_SIZE_PWD() As Integer
        Get
            Return _MIN_SIZE_PWD
        End Get
        Set(ByVal value As Integer)
            _MIN_SIZE_PWD = value
        End Set
    End Property
    Private _encrptType As Boolean
    Public Property EncryptType() As Boolean
        Get
            Return _encrptType
        End Get
        Set(ByVal value As Boolean)
            _encrptType = value
        End Set
    End Property
#End Region
#Region "Funciones"
    ''' <summary>
    ''' Recupera los parámetros generales de la política de contraseñas de la base de datos
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetParametrosGeneralesPasswordPolicy()
        Dim dt As DataTable
        dt = DBServer.GetParametrosGeneralesPasswordPolicy()
        With dt.Rows(0)
            _EDAD_MAX_PWD = CType(.Item("EDAD_MAX_PWD"), Integer)
            _EDAD_MIN_PWD = CType(.Item("EDAD_MIN_PWD"), Integer)
            _HISTORICO_PWD = CType(.Item("HISTORICO_PWD"), Integer)
            _COMPLEJIDAD_PWD = CType(.Item("COMPLEJIDAD_PWD"), Boolean)
            _MIN_SIZE_PWD = CType(.Item("MIN_SIZE_PWD"), Integer)
            _encrptType = CType(.Item("ENCRYPT_ONE_WAY"), Boolean)
        End With
    End Sub
    ''' <summary>
    ''' Obtiene la fecha de la password actual
    ''' </summary>
    ''' <param name="UsuCod">Código del usuario del que obtendremos la fecha de la password</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetFechaPasswordActual(ByVal UsuCod As String) As Date
        Return DBServer.GetFechaPasswordActual(UsuCod)
    End Function
    ''' <summary>
    ''' Comprueba que la contraseña introducida no es igual a una de las "NumeroPasswordHistorico" anteriores
    ''' </summary>
    ''' <param name="NumeroPasswordHistorico">Número de contraseñas anteriores a comparar</param>
    ''' <param name="NewPWD">Contraseña introducida como nueva</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComprobacionHistoricoPassword(ByVal IdCia As Integer, ByVal IdUsu As Integer, _
                ByVal NumeroPasswordHistorico As Integer, ByVal NewPWD As String) As Boolean
        Return DBServer.ComprobacionHistoricoPassword(IdCia, IdUsu, NumeroPasswordHistorico, NewPWD)
    End Function
    ''' <summary>
    ''' Función que realiza el cambio de contraseña
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CambioPassword(ByVal IdCia As Integer, ByVal IdUsu As Integer, ByVal NewPassword As String, _
                              ByVal FecNewPwd As Date, ByVal Salt As String)
        DBServer.CambiarPwdDB(IdCia, IdUsu, NewPassword, FecNewPwd, Salt)
    End Sub
#End Region
End Class


