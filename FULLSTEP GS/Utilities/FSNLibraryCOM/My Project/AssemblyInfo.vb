﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("FSNLibraryCOM")> 
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Fullstep Networks SL")>
<Assembly: AssemblyProduct("FSNLibraryCOM")>
<Assembly: AssemblyCopyright("Copyright © Fullstep Networks SL 2017")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(True)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("ed020325-4918-4031-85dc-1c7ec4701776")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("32.100.6.001")>
<Assembly: AssemblyFileVersion("32.100.6.001")>
