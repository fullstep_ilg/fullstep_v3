﻿Option Strict Off
Option Explicit On

Imports System.Runtime.Serialization
Imports System.Runtime.InteropServices

Namespace FSNPasswordPolicy

    <System.Diagnostics.DebuggerStepThroughAttribute(), _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0"), _
     System.Runtime.Serialization.DataContractAttribute(Name:="ResultadoPasswordChange", [Namespace]:="http://schemas.datacontract.org/2004/07/FSNPasswordPolicy")> _
     <ComClass(ResultadoPasswordChange.ClassId, ResultadoPasswordChange.InterfaceId, ResultadoPasswordChange.EventsId)> _
    Partial Public Class ResultadoPasswordChange
        Inherits Object
        Implements System.Runtime.Serialization.IExtensibleDataObject

#Region "COM GUIDs"
        ' These  GUIDs provide the COM identity for this class 
        ' and its COM interfaces. If you change them, existing 
        ' clients will no longer be able to access the class.
        Public Const ClassId As String = "039C4DA0-15C2-43CD-B8AC-278817CB2531"
        Public Const InterfaceId As String = "AF3C00D5-EEEF-4408-A019-14BC674CC522"
        Public Const EventsId As String = "B772AF86-8F74-4425-A510-4FD2B74F19F6"
#End Region
        Private extensionDataField As System.Runtime.Serialization.ExtensionDataObject

        Private FecPasswordField As Date

        Private MensajeField As String

        Private PasswordField As String

        Private SaltField As String

        Private TipoMensajeField As Integer

        Public Property ExtensionData() As System.Runtime.Serialization.ExtensionDataObject Implements System.Runtime.Serialization.IExtensibleDataObject.ExtensionData
            Get
                Return Me.extensionDataField
            End Get
            Set(value As System.Runtime.Serialization.ExtensionDataObject)
                Me.extensionDataField = value
            End Set
        End Property

        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property FecPassword() As Date
            Get
                Return Me.FecPasswordField
            End Get
            Set(value As Date)
                Me.FecPasswordField = value
            End Set
        End Property

        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property Mensaje() As String
            Get
                Return Me.MensajeField
            End Get
            Set(value As String)
                Me.MensajeField = value
            End Set
        End Property

        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property Password() As String
            Get
                Return Me.PasswordField
            End Get
            Set(value As String)
                Me.PasswordField = value
            End Set
        End Property

        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property Salt() As String
            Get
                Return Me.SaltField
            End Get
            Set(value As String)
                Me.SaltField = value
            End Set
        End Property

        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property TipoMensaje() As Integer
            Get
                Return Me.TipoMensajeField
            End Get
            Set(value As Integer)
                Me.TipoMensajeField = value
            End Set
        End Property
    End Class
End Namespace

<System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"), _
 System.ServiceModel.ServiceContractAttribute(ConfigurationName:="IFSNPasswordPolicyService")> _
Public Interface IFSNPasswordPolicyService

    <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IFSNPasswordPolicyService/PasswordChangePortal", ReplyAction:="http://tempuri.org/IFSNPasswordPolicyService/PasswordChangePortalResponse")> _
    Function PasswordChangePortal(ByVal IdCia As Integer, ByVal IdUsu As Integer, ByVal UsuCod As String, ByVal FecPwd As Date, ByVal NewPwd As String) As FSNPasswordPolicy.ResultadoPasswordChange

    <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IFSNPasswordPolicyService/ValidatePasswordPoliticaSeguridad", ReplyAction:="http://tempuri.org/IFSNPasswordPolicyService/ValidatePasswordPoliticaSeguridadRes" & _
        "ponse")> _
    Function ValidatePasswordPoliticaSeguridad(ByVal UsuCod As String, ByVal Pwd As String, ByVal FecPwd As Date) As FSNPasswordPolicy.ResultadoPasswordChange
End Interface

<System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")> _
Public Interface IFSNPasswordPolicyServiceChannel
    Inherits IFSNPasswordPolicyService, System.ServiceModel.IClientChannel
End Interface

<System.Diagnostics.DebuggerStepThroughAttribute(), _
 System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")> _
 <ComVisibleAttribute(False)> _
Partial Public Class FSNPasswordPolicyServiceClient
    Inherits System.ServiceModel.ClientBase(Of IFSNPasswordPolicyService)
    Implements IFSNPasswordPolicyService

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal endpointConfigurationName As String)
        MyBase.New(endpointConfigurationName)
    End Sub

    Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As String)
        MyBase.New(endpointConfigurationName, remoteAddress)
    End Sub

    Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
        MyBase.New(endpointConfigurationName, remoteAddress)
    End Sub

    Public Sub New(ByVal binding As System.ServiceModel.Channels.Binding, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
        MyBase.New(binding, remoteAddress)
    End Sub

    Public Function PasswordChangePortal(ByVal IdCia As Integer, ByVal IdUsu As Integer, ByVal UsuCod As String, ByVal FecPwd As Date, ByVal NewPwd As String) As FSNPasswordPolicy.ResultadoPasswordChange Implements IFSNPasswordPolicyService.PasswordChangePortal
        Return MyBase.Channel.PasswordChangePortal(IdCia, IdUsu, UsuCod, FecPwd, NewPwd)
    End Function

    Public Function ValidatePasswordPoliticaSeguridad(ByVal UsuCod As String, ByVal Pwd As String, ByVal FecPwd As Date) As FSNPasswordPolicy.ResultadoPasswordChange Implements IFSNPasswordPolicyService.ValidatePasswordPoliticaSeguridad
        Return MyBase.Channel.ValidatePasswordPoliticaSeguridad(UsuCod, Pwd, FecPwd)
    End Function
End Class