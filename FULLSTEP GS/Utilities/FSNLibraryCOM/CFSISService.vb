﻿Imports System.ServiceModel
Imports System.Runtime.Remoting.Messaging
Imports System.Runtime.InteropServices
Imports System.ServiceModel.Security
Imports System.ServiceModel.Description
Imports System.Data.OleDb

<ComClass(CFSISService.ClassId, CFSISService.InterfaceId, CFSISService.EventsId)> _
Public Class CFSISService

#Region "COM GUIDs"
    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.   
    Public Const ClassId As String = "ADC38D8C-6773-406F-9FB4-8CF613E7E3F4"
    Public Const InterfaceId As String = "BB069554-C16C-4158-A172-CE7EE2804023"
    Public Const EventsId As String = "F1C928E5-204C-43AF-8C0F-6106C64AB9AA"
#End Region

#Region "Definicion Propertys"
    Private mlIdLog As Long
    Private miEntidad As Integer
    Private mClaseCom As Integer

    Public Enum Aplicaciones
        EP = &H7FFF
        PM = &HFFFF
        QA = &H1FFFF
        Otra = &H5FFF
    End Enum
#End Region

#Region "Establecer Propertys"
    ''' <summary>
    ''' Devuelve/Establace la propiedad mClaseCom
    ''' Necesario pq las clases COM no admiten el new con parametros
    ''' 0- No com (valor por defecto)
    ''' 1- com y mCrearConexionSmtp = true           (Sin esto GS/FSP/PS/Notif GS no crea el objeto STMP compartido)
    ''' 2- com y mCrearConexionSmtp = false          (GS/FSP/PS/Notif GS)
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los Notificar (gs, portal, Pm, Int); Tiempo maximo:0</remarks>
    Property ClaseCom() As Integer
        Get
            ClaseCom = mClaseCom
        End Get
        Set(ByVal Value As Integer)
            mClaseCom = Value
        End Set
    End Property

#End Region


#Region "Funciones"
    ''' <summary>
    ''' A creatable COM class must have a Public Sub New() 
    ''' with no parameters, otherwise, the class will not be 
    ''' registered in the COM registry and cannot be created 
    ''' via CreateObject.
    ''' </summary>
    ''' <remarks>Llamada desde: Gs, Fsp, Ps y Notif GS; Tiempo maximo:0</remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Destruir el objeto Mail.SmtpClient completamente, liberando memoria
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb; Tiempo maximo:0</remarks>
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub


    Public Function ConvertRecodsetToDataset(ByVal oRSGeneral As Object, Optional ByVal oRSCabecera As Object = Nothing, Optional ByVal oRSLineas As Object = Nothing) As DataSet

        Dim myDA As New OleDb.OleDbDataAdapter
        Dim newDataSet As New DataSet("Validaciones")
        Try
            If Not IsNothing(oRSGeneral) Then
                myDA.Fill(newDataSet, oRSGeneral, "InfoGeneral")
            End If

            If Not IsNothing(oRSCabecera) Then
                myDA.Fill(newDataSet, oRSCabecera, "Cabecera")
            End If

            If Not IsNothing(oRSLineas) Then
                myDA.Fill(newDataSet, oRSLineas, "Lineas")
            End If

            Return newDataSet

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function ValidacionFSIS(ByVal sOrigenValidacion As String, Optional iEntidad As Integer = 0, Optional ByVal iServiceBindingType As Integer = 0, Optional ByVal iServiceSecurityMode As Integer = 0, _
       Optional ByVal iClientCredentialType As Integer = 0, Optional ByVal iProxyCredentialType As Integer = 0, Optional ByVal sUserName As String = "", Optional ByVal sUserPassword As String = "", _
       Optional ByVal sRutaServicio As String = "", Optional ByVal sObjetoSerializado As String = "", Optional ByVal oRSGeneral As Object = Nothing, Optional ByVal oRSCabecera As Object = Nothing, _
       Optional ByVal oRSLineas As Object = Nothing, Optional ByVal sIdioma As String = "", Optional ByVal sBloqueDestino As String = "", Optional ByVal iNivel As Integer = 0, Optional ByVal iErp As Integer = 0) As String

        Dim newDataSet As New DataSet
        Dim sError As String

        Try
            sError = ""
            ''Se crea el DataSet de las Validaciones
            newDataSet = ConvertRecodsetToDataset(oRSGeneral, oRSCabecera, oRSLineas)

            sError = Validaciones(sOrigenValidacion, iEntidad, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sUserName, sUserPassword, sRutaServicio, sObjetoSerializado, newDataSet, sIdioma, sBloqueDestino, iNivel, iErp)

            Return sError

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DevolverListaObjetosGS(ByVal sCodEmpresa As String, ByVal IdAtributoListaExterna As Integer, ByVal sCodigo As String, ByVal sDenominacion As String, ByVal sUsuario As String, _
                                         Optional ByVal sUon As String = "", Optional ByVal sXml As String = "", Optional ByVal iServiceBindingType As Integer = 0, _
                                         Optional ByVal iServiceSecurityMode As Integer = 0, Optional ByVal iClientCredentialType As Integer = 0, _
                                         Optional ByVal iProxyCredentialType As Integer = 0, Optional ByVal sUserName As String = "", Optional ByVal sUserPassword As String = "", _
                                         Optional ByVal sRutaServicio As String = "") As ADODB.Recordset

        Try
            sRutaServicio = Split(sRutaServicio, "Exportar")(0) & "Validaciones.svc"

            Dim oListaAtributosGSRequest As DevolverListaObjetosGSRequest
            Dim oListaAtributosGSResponse As DevolverListaObjetosGSResponse

            Dim myChannelFactory As ChannelFactory(Of IValidaciones)
            Dim myEndpoint As New EndpointAddress(sRutaServicio)

            oListaAtributosGSRequest = New DevolverListaObjetosGSRequest(sCodEmpresa, IdAtributoListaExterna, sCodigo, sDenominacion, sUsuario, sUon, sXml)

            Select Case iServiceBindingType
                Case 1 'WSHttpBinding
                    Dim iSecurityMode As SecurityMode = iServiceSecurityMode
                    Dim myBinding As New WSHttpBinding(iSecurityMode)
                    With myBinding
                        .Security.Transport.ClientCredentialType = iClientCredentialType
                        .Security.Transport.ProxyCredentialType = iProxyCredentialType
                        .MaxBufferPoolSize = "2147483647"
                        .MaxReceivedMessageSize = "2147483647"
                        .ReaderQuotas.MaxDepth = "2000000"
                        .ReaderQuotas.MaxStringContentLength = "2147483647"
                        .ReaderQuotas.MaxArrayLength = "2147483647"
                        .ReaderQuotas.MaxBytesPerRead = "2147483647"
                        .ReaderQuotas.MaxNameTableCharCount = "2147483647"
                    End With
                    myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
                Case Else 'BasicHttpBinding
                    Dim iBasicSecurityMode As BasicHttpSecurityMode = iServiceSecurityMode
                    Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
                    With myBinding
                        .Security.Transport.ClientCredentialType = iClientCredentialType
                        .Security.Transport.ProxyCredentialType = iProxyCredentialType
                        .MaxBufferPoolSize = "2147483647"
                        .MaxReceivedMessageSize = "2147483647"
                        .ReaderQuotas.MaxDepth = "2000000"
                        .ReaderQuotas.MaxStringContentLength = "2147483647"
                        .ReaderQuotas.MaxArrayLength = "2147483647"
                        .ReaderQuotas.MaxBytesPerRead = "2147483647"
                        .ReaderQuotas.MaxNameTableCharCount = "2147483647"
                    End With
                    myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
            End Select

            With (myChannelFactory.Credentials)
                Select Case iClientCredentialType
                    Case HttpClientCredentialType.Windows
                        With .Windows.ClientCredential
                            .UserName = sUserName
                            .Password = sUserPassword
                        End With
                    Case HttpClientCredentialType.Basic
                        With .UserName
                            .UserName = sUserName
                            .Password = sUserPassword
                        End With
                    Case HttpClientCredentialType.Certificate
                        .ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine, _
                                                          System.Security.Cryptography.X509Certificates.StoreName.My, _
                                                          System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
                End Select
            End With

            Dim wcfProxyClient As IValidaciones = myChannelFactory.CreateChannel()
            oListaAtributosGSResponse = wcfProxyClient.DevolverListaObjetosGS(oListaAtributosGSRequest)
            myChannelFactory.Close()

            Dim utilities As New Utilities

            ''Se transforma el objeto DataSet en un ADODB.Recordset
            Return utilities.ConvertToRecordset(oListaAtributosGSResponse.DevolverListaObjetosGSResult.Tables(0))

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="lIdLogEntidad"></param>
    ''' <param name="iEntidadIntegracion"></param>
    ''' <param name="strRutaServicio"></param>
    ''' <param name="serviceBindingType">1=WSHttpBinding / BasicHttpBinding</param>
    ''' <param name="serviceSecurityMode">1=Transport / 2=Message / 3=TransportWithMessageCredential / 4=TransportCredentialOnly / 0=None</param>
    ''' <param name="serviceClientCredentialType">1=HttpClientCredentialType.Basic / 2=HttpClientCredentialType.Digest / 3=HttpClientCredentialType.Ntlm / 4=HttpClientCredentialType.Windows / 5=HttpClientCredentialType.Certificate / 0=HttpClientCredentialType.None</param>
    ''' <param name="serviceProxyCredentialType">1=HttpProxyCredentialType.Basic / 2=HttpProxyCredentialType.Digest / 3=HttpProxyCredentialType.Ntlm / 4=HttpProxyCredentialType.Windows / 0=HttpProxyCredentialType.None</param>
    ''' <param name="serviceCredentialUserName"></param>
    ''' <param name="serviceCredentialPassword"></param>
    ''' <remarks></remarks>
    Public Sub Exportar(ByVal lIdLogEntidad As Integer, ByVal iEntidadIntegracion As Integer, ByVal strRutaServicio As String, _
                        Optional ByVal serviceBindingType As Integer = 0, Optional ByVal serviceSecurityMode As Integer = 0, _
                        Optional ByVal serviceClientCredentialType As Integer = 0, Optional ByVal serviceProxyCredentialType As Integer = 0, _
                        Optional ByVal serviceCredentialUserName As String = "", Optional ByVal serviceCredentialPassword As String = "")
        Dim myChannelFactory As ChannelFactory(Of IExportar)
        Dim myEndpoint As New EndpointAddress(strRutaServicio)
        Try
            Select Case serviceBindingType
                Case 1 'WSHttpBinding
                    Dim iSecurityMode As SecurityMode = serviceSecurityMode
                    Dim myBinding As New WSHttpBinding(iSecurityMode)
                    With myBinding
                        .Security.Transport.ClientCredentialType = serviceClientCredentialType
                        .Security.Transport.ProxyCredentialType = serviceProxyCredentialType
                    End With
                    myChannelFactory = New ChannelFactory(Of IExportar)(myBinding, myEndpoint)
                Case Else 'BasicHttpBinding
                    Dim iBasicSecurityMode As BasicHttpSecurityMode = serviceSecurityMode
                    Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
                    With myBinding
                        .Security.Transport.ClientCredentialType = serviceClientCredentialType
                        .Security.Transport.ProxyCredentialType = serviceProxyCredentialType
                    End With
                    myChannelFactory = New ChannelFactory(Of IExportar)(myBinding, myEndpoint)
            End Select

            With (myChannelFactory.Credentials)
                Select Case serviceClientCredentialType
                    Case HttpClientCredentialType.Windows
                        With .Windows.ClientCredential
                            .UserName = serviceCredentialUserName
                            .Password = serviceCredentialPassword
                        End With
                    Case HttpClientCredentialType.Basic
                        With .UserName
                            .UserName = serviceCredentialUserName
                            .Password = serviceCredentialPassword
                        End With
                    Case HttpClientCredentialType.Certificate
                        .ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine, _
                                                          System.Security.Cryptography.X509Certificates.StoreName.My, _
                                                          System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
                End Select
            End With

            Dim wcfProxyClient As IExportar = myChannelFactory.CreateChannel()
            wcfProxyClient.BeginExportar(lIdLogEntidad, iEntidadIntegracion, Nothing, myChannelFactory)
            myChannelFactory.Close()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="lIdLogEntidad"></param>
    ''' <param name="iEntidadIntegracion"></param>
    ''' <param name="strRutaServicio"></param>
    ''' <param name="serviceBindingType">1=WSHttpBinding / BasicHttpBinding</param>
    ''' <param name="serviceSecurityMode">1=Transport / 2=Message / 3=TransportWithMessageCredential / 4=TransportCredentialOnly / 0=None</param>
    ''' <param name="serviceClientCredentialType">1=HttpClientCredentialType.Basic / 2=HttpClientCredentialType.Digest / 3=HttpClientCredentialType.Ntlm / 4=HttpClientCredentialType.Windows / 5=HttpClientCredentialType.Certificate / 0=HttpClientCredentialType.None</param>
    ''' <param name="serviceProxyCredentialType">1=HttpProxyCredentialType.Basic / 2=HttpProxyCredentialType.Digest / 3=HttpProxyCredentialType.Ntlm / 4=HttpProxyCredentialType.Windows / 0=HttpProxyCredentialType.None</param>
    ''' <param name="serviceCredentialUserName"></param>
    ''' <param name="serviceCredentialPassword"></param>
    ''' <remarks></remarks>
    Public Function Validaciones(ByVal sOrigenValidacion As String, Optional iEntidad As Integer = 0, Optional ByVal iServiceBindingType As Integer = 0, Optional ByVal iServiceSecurityMode As Integer = 0, _
       Optional ByVal iClientCredentialType As Integer = 0, Optional ByVal iProxyCredentialType As Integer = 0, Optional ByVal sUserName As String = "", Optional ByVal sUserPassword As String = "", _
       Optional ByVal sRutaServicio As String = "", Optional ByVal sObjetoSerializado As String = "", Optional ByVal oRS As DataSet = Nothing, Optional ByVal sIdioma As String = "", _
       Optional ByVal sBloqueDestino As String = "", Optional ByVal iNivel As Integer = 0, Optional ByVal iErp As Integer = 0) As String
        Try
            sRutaServicio = Split(sRutaServicio, "Exportar")(0) & "Validaciones.svc"

            Dim oValidacionesFSISRequest As ValidacionesFSISRequest
            Dim oValidacionesFSISResponse As ValidacionesFSISResponse

            Dim myChannelFactory As ChannelFactory(Of IValidaciones)
            Dim myEndpoint As New EndpointAddress(sRutaServicio)

            oValidacionesFSISRequest = New ValidacionesFSISRequest(sOrigenValidacion, iEntidad, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sUserName, sUserPassword, sRutaServicio, sObjetoSerializado, oRS, sIdioma, sBloqueDestino, iNivel, iErp)

            Select Case iServiceBindingType
                Case 1 'WSHttpBinding
                    Dim iSecurityMode As SecurityMode = iServiceSecurityMode
                    Dim myBinding As New WSHttpBinding(iSecurityMode)
                    With myBinding
                        .Security.Transport.ClientCredentialType = iClientCredentialType
                        .Security.Transport.ProxyCredentialType = iProxyCredentialType
                    End With
                    myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
                Case Else 'BasicHttpBinding
                    Dim iBasicSecurityMode As BasicHttpSecurityMode = iServiceSecurityMode
                    Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
                    With myBinding
                        .Security.Transport.ClientCredentialType = iClientCredentialType
                        .Security.Transport.ProxyCredentialType = iProxyCredentialType
                    End With
                    myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
            End Select

            With (myChannelFactory.Credentials)
                Select Case iClientCredentialType
                    Case HttpClientCredentialType.Windows
                        With .Windows.ClientCredential
                            .UserName = sUserName
                            .Password = sUserPassword
                        End With
                    Case HttpClientCredentialType.Basic
                        With .UserName
                            .UserName = sUserName
                            .Password = sUserPassword
                        End With
                    Case HttpClientCredentialType.Certificate
                        .ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine, _
                                                          System.Security.Cryptography.X509Certificates.StoreName.My, _
                                                          System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
                End Select
            End With

            Dim wcfProxyClient As IValidaciones = myChannelFactory.CreateChannel()
            oValidacionesFSISResponse = wcfProxyClient.ValidacionesFSIS(oValidacionesFSISRequest)
            myChannelFactory.Close()

            Return oValidacionesFSISResponse.ValidacionesFSISResult
        Catch ex As Exception
            Throw ex
        End Try

    End Function


    Public Sub ExportarLista(ByVal lIdsLogEntidad() As Long, ByVal iEntidadIntegracion As Integer, ByVal strRutaServicio As String, _
                        Optional ByVal serviceBindingType As Integer = 0, Optional ByVal serviceSecurityMode As Integer = 0, _
                        Optional ByVal serviceClientCredentialType As Integer = 0, Optional ByVal serviceProxyCredentialType As Integer = 0, _
                        Optional ByVal serviceCredentialUserName As String = "", Optional ByVal serviceCredentialPassword As String = "")
        Dim myChannelFactory As ChannelFactory(Of IExportar)
        Dim myEndpoint As New EndpointAddress(strRutaServicio)
        Try
            Select Case serviceBindingType
                Case 1 'WSHttpBinding
                    Dim iSecurityMode As SecurityMode = serviceSecurityMode
                    Dim myBinding As New WSHttpBinding(iSecurityMode)
                    With myBinding
                        .Security.Transport.ClientCredentialType = serviceClientCredentialType
                        .Security.Transport.ProxyCredentialType = serviceProxyCredentialType
                    End With
                    myChannelFactory = New ChannelFactory(Of IExportar)(myBinding, myEndpoint)
                Case Else 'BasicHttpBinding
                    Dim iBasicSecurityMode As BasicHttpSecurityMode = serviceSecurityMode
                    Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
                    With myBinding
                        .Security.Transport.ClientCredentialType = serviceClientCredentialType
                        .Security.Transport.ProxyCredentialType = serviceProxyCredentialType
                    End With
                    myChannelFactory = New ChannelFactory(Of IExportar)(myBinding, myEndpoint)
            End Select

            With (myChannelFactory.Credentials)
                Select Case serviceClientCredentialType
                    Case HttpClientCredentialType.Windows
                        With .Windows.ClientCredential
                            .UserName = serviceCredentialUserName
                            .Password = serviceCredentialPassword
                        End With
                    Case HttpClientCredentialType.Basic
                        With .UserName
                            .UserName = serviceCredentialUserName
                            .Password = serviceCredentialPassword
                        End With
                    Case HttpClientCredentialType.Certificate
                        .ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine, _
                                                          System.Security.Cryptography.X509Certificates.StoreName.My, _
                                                          System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
                End Select
            End With

            Dim wcfProxyClient As IExportar = myChannelFactory.CreateChannel()
            wcfProxyClient.BeginExportarLista(lIdsLogEntidad.ToArray, iEntidadIntegracion, Nothing, myChannelFactory)
            myChannelFactory.Close()
            myChannelFactory.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region
End Class

