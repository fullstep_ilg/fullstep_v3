﻿Imports System.Web
Imports System.Security.Principal
Imports System.Runtime.InteropServices
Module Impersonate
    Private LOGON32_LOGON_INTERACTIVE As Integer = 2
    Private LOGON32_PROVIDER_DEFAULT As Integer = 0
    Private impersonationContext As WindowsImpersonationContext
    Declare Function LogonUserA Lib "advapi32.dll" (ByVal lpszUsername As String, _
                            ByVal lpszDomain As String, _
                            ByVal lpszPassword As String, _
                            ByVal dwLogonType As Integer, _
                            ByVal dwLogonProvider As Integer, _
                            ByRef phToken As IntPtr) As Integer
    Declare Auto Function DuplicateToken Lib "advapi32.dll" ( _
                            ByVal ExistingTokenHandle As IntPtr, _
                            ByVal ImpersonationLevel As Integer, _
                            ByRef DuplicateTokenHandle As IntPtr) As Integer
    Declare Auto Function RevertToSelf Lib "advapi32.dll" () As Long
    Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Long
    ''' <summary>
    ''' Suplantacion de identidad
    ''' </summary>
    ''' <returns>suplantacion correcta o no</returns>
    ''' <remarks>Llamada desde:Email.vb/CreacionObjetoSTMP       AdminUsers.vb/AddUserToServer       AdminUsers.vb/ModifyUserInServer       AdminUsers.vb/RemoveUserFromServer
    '''        AdminUsers.vb/RetrieveUserProperties ; Tiempo máximo:0seg.</remarks>
    Public Function impersonateValidUser(ByVal userName As String, ByVal domain As String, ByVal password As String) As Boolean
        impersonateValidUser = False

        If userName = "" Or domain = "" Or password = "" Then
            Exit Function
        End If

        Dim tempWindowsIdentity As WindowsIdentity
        Dim token As IntPtr = IntPtr.Zero
        Dim tokenDuplicate As IntPtr = IntPtr.Zero
        If RevertToSelf() Then
            If LogonUserA(userName, domain, password, LOGON32_LOGON_INTERACTIVE,
                         LOGON32_PROVIDER_DEFAULT, token) <> 0 Then
                If DuplicateToken(token, 2, tokenDuplicate) <> 0 Then
                    tempWindowsIdentity = New WindowsIdentity(tokenDuplicate)
                    impersonationContext = tempWindowsIdentity.Impersonate()
                    If Not impersonationContext Is Nothing Then
                        impersonateValidUser = True
                    End If
                End If
            End If
        End If
        If Not tokenDuplicate.Equals(IntPtr.Zero) Then
            CloseHandle(tokenDuplicate)
        End If
        If Not token.Equals(IntPtr.Zero) Then
            CloseHandle(token)
        End If
    End Function
    ''' <summary>
    ''' Deshacer suplantacion de identidad
    ''' </summary>
    ''' <remarks>Llamada desde:Email.vb/CreacionObjetoSTMP       AdminUsers.vb/AddUserToServer       AdminUsers.vb/ModifyUserInServer       AdminUsers.vb/RemoveUserFromServer
    '''        AdminUsers.vb/RetrieveUserProperties; Tiempo máximo:0seg.</remarks>
    Public Sub undoImpersonation()
        impersonationContext.Undo()
    End Sub
End Module
