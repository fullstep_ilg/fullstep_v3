Attribute VB_Name = "basPublic"
Option Explicit

Public Declare Sub ExitProcess Lib "kernel32" (ByVal uExitCode As Long)

Private bTransaccionEnCurso As Boolean
Private sConsulta As String
Public gRDOCon As RDO.rdoConnection
Public gRDOErs As RDO.rdoErrors

Public moConexion As CConexion
Public oFSGSRaiz As CRaiz
Public gsDEN_UON0 As String

Public Const gcUltimaVersion = "3.00.37.014" 'Versión ACTUAL

Public gsVersionBD As String
Public gdatFecha As Date
Public gLongitudesDeCodigos As basUtilidades.LongitudesDeCodigos

Public gServer As String
Public gBD As String
Public gUID As String
Public gPWD As String
Public gCollation As String
Public gSQLVersion As Integer

Public Enum AccionesSummit
    ACCUON1Anya = 90
    ACCUON1Mod = 91
    ACCUON1Eli = 92
    ACCUON1Det = 93
    ACCUON2Anya = 94
    ACCUON2Mod = 95
    ACCUON2Eli = 96
    ACCUON2Det = 97
    ACCUON3Anya = 98
    ACCUON3Mod = 99
    ACCUON3Eli = 100
    ACCUON3Det = 101
    
    ACCUOCon = 102
'*************************************
End Enum

'Cod de los idiomas que van en las columnas TEXT_SPA,TEXT_ENG,TEXT_GER,TEXT_FRA
Public gIdiomaSPA As String
Public gIdiomaENG As String
Public gIdiomaGER As String
Public gIdiomaFRA As String

Public Function InsertIdiomas() As Boolean
Dim ADOCon As New ADODB.Connection
Dim AdoRes As ADODB.Recordset
Dim sSQL As String
Dim rdoRS As RDO.rdoResultset
Dim rdoCol As RDO.rdoColumn

    On Error GoTo Error:
    
    
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    
    '''- Borrar los registros de ACC_IDIOMA y MENU y volverlos a cargar de FSActualizador.mdb
    frmProgreso.lblDetalle = "Borramos los registros de ACC_IDIOMA, ACC, ACC_DEPEN, MENU, WEBTEXT y los cargamos de FSActualizador.mdb"
    frmProgreso.lblDetalle.Refresh
    On Error Resume Next
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    On Error GoTo 0
    
    sConsulta = "ALTER TABLE ACCMEN NOCHECK CONSTRAINT ALL"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE PERF_ACC NOCHECK CONSTRAINT ALL"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE USU_ACC NOCHECK CONSTRAINT ALL"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "DELETE FROM ACC_IDIOMA"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "DELETE FROM ACC_DEPEN"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "DELETE FROM ACC"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "DELETE FROM MENU"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "DELETE FROM ACCMEN_IDIOMA"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "DELETE FROM ACCMEN"
    ExecuteSQL gRDOCon, sConsulta
    
    'No todos los idiomas van a todas las instalaciones. El campo APLICACION a 0 no sirve por como tenemos Portal (preguntar a Encarni).
    'Para dar de alta un idioma, ejemplo Frances, 1ro a mano se mete en tabla IDIOMAS con campo APLICACION a 1, 2do se pasa un script q crea
    'los filtros y 3ro este actualizador se lanza, solo para crear textos accmen_idioma, acc_idioma, etc
    Dim rdores As rdoResultset
    
    Dim bActivoEng As Boolean
    sConsulta = "SELECT COD FROM IDIOMAS WHERE APLICACION=1 AND COD='ENG'"
    Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    If Not rdores.EOF Then bActivoEng = True
    rdores.Close
    Set rdores = Nothing
    
    Dim bActivoGer As Boolean
    sConsulta = "SELECT COD FROM IDIOMAS WHERE APLICACION=1 AND COD='GER'"
    Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    If Not rdores.EOF Then bActivoGer = True
    rdores.Close
    Set rdores = Nothing
    
    Dim bActivoFra As Boolean
    sConsulta = "SELECT COD FROM IDIOMAS WHERE APLICACION=1 AND COD='FRA'"
    Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    If Not rdores.EOF Then bActivoFra = True
    rdores.Close
    Set rdores = Nothing
        
    'Create the connection.
    ADOCon.Open "Provider=Microsoft.Jet.OLEDB.4.0;" & "Data Source=" & App.Path & "\FSActualizador.mdb;"
    ADOCon.CursorLocation = adUseClient
    
    'Añadido para la version 8
    sSQL = "SELECT * FROM ACT_ACCMEN ORDER BY ID"
    
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO [dbo].[ACCMEN] VALUES (" & AdoRes("ID").Value & "," & StrToSQLNULL(AdoRes("ACCMEN_ID").Value) & "," & AdoRes("MODULO").Value & "," & StrToSQLNULL(AdoRes("ORDEN").Value) & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    Set AdoRes = Nothing
    
    'Añadido para la version 8 TABLA ACCMEN_IDIOMA SPA
    sSQL = "SELECT * FROM ACT_ACCMEN_IDIOMA WHERE IDIOMA='" & gIdiomaSPA & "' ORDER BY ACCMEN_ID"
    
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO [dbo].[ACCMEN_IDIOMA] VALUES (" & AdoRes("ACCMEN_ID").Value & ",'SPA'," & StrToSQLNULL(AdoRes("DEN").Value) & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    Set AdoRes = Nothing
    
     'Añadido para la version 8 TABLA ACCMEN_IDIOMA ENG
    If bActivoEng Then
        sSQL = "SELECT * FROM ACT_ACCMEN_IDIOMA WHERE IDIOMA='" & gIdiomaENG & "' ORDER BY ACCMEN_ID"
        
        Set AdoRes = New ADODB.Recordset
        
        AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
        
        While Not AdoRes.EOF
            sConsulta = "INSERT INTO [dbo].[ACCMEN_IDIOMA] VALUES (" & AdoRes("ACCMEN_ID").Value & ",'ENG'," & StrToSQLNULL(AdoRes("DEN").Value) & ")"
            ExecuteSQL gRDOCon, sConsulta
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
     'Añadido para la version 8 TABLA ACCMEN_IDIOMA GER
    If bActivoGer Then
        sSQL = "SELECT * FROM ACT_ACCMEN_IDIOMA WHERE IDIOMA='" & gIdiomaGER & "' ORDER BY ACCMEN_ID"
        
        Set AdoRes = New ADODB.Recordset
        
        AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
        
        While Not AdoRes.EOF
            sConsulta = "INSERT INTO [dbo].[ACCMEN_IDIOMA] VALUES (" & AdoRes("ACCMEN_ID").Value & ",'GER'," & StrToSQLNULL(AdoRes("DEN").Value) & ")"
            ExecuteSQL gRDOCon, sConsulta
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    'IDIOMA FRA
    If bActivoFra Then
         sSQL = "SELECT * FROM ACT_ACCMEN_IDIOMA WHERE IDIOMA='" & gIdiomaFRA & "' ORDER BY ACCMEN_ID"
        
        Set AdoRes = New ADODB.Recordset
        
        AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
        
        While Not AdoRes.EOF
            sConsulta = "INSERT INTO [dbo].[ACCMEN_IDIOMA] VALUES (" & AdoRes("ACCMEN_ID").Value & ",'FRA'," & StrToSQLNULL(AdoRes("DEN").Value) & ")"
            ExecuteSQL gRDOCon, sConsulta
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    sSQL = "SELECT * FROM ACT_ACC ORDER BY ID"
    
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO [dbo].[ACC] VALUES (" & AdoRes("ID").Value & "," & AdoRes("ORDEN").Value & "," & AdoRes("ACCMEN_ID").Value & "," & AdoRes("UON").Value & "," & AdoRes("UNQA").Value & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    Set AdoRes = Nothing
    
    sSQL = "SELECT * FROM ACT_ACC_DEPEN ORDER BY ACC,EST,ACC_DEPEN"
    
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO [dbo].[ACC_DEPEN] VALUES (" & AdoRes("ACC").Value & "," & AdoRes("EST").Value & "," & AdoRes("ACC_DEPEN").Value & "," & AdoRes("EST_DEPEN").Value & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    Set AdoRes = Nothing
    
    'lo que se introduce con idioma SPA
    sSQL = "SELECT * FROM ACT_ACC_IDIOMA WHERE IDIOMA='" & gIdiomaSPA & "' ORDER BY ID"
    
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO [dbo].[ACC_IDIOMA] VALUES (" & AdoRes("ID").Value & ", 'SPA', " & StrToSQLNULL(NullToStr(AdoRes("DEN").Value)) & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    Set AdoRes = Nothing
    
    'lo que se introduce con idioma ENG
    If bActivoEng Then
        sSQL = "SELECT * FROM ACT_ACC_IDIOMA WHERE IDIOMA='" & gIdiomaENG & "' ORDER BY ID"
        
        Set AdoRes = New ADODB.Recordset
        
        AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
        
        While Not AdoRes.EOF
            sConsulta = "INSERT INTO [dbo].[ACC_IDIOMA] VALUES (" & AdoRes("ID").Value & ", 'ENG', " & StrToSQLNULL(NullToStr(AdoRes("DEN").Value)) & ")"
            ExecuteSQL gRDOCon, sConsulta
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    'lo que se introduce con idioma GER
    If bActivoGer Then
        sSQL = "SELECT * FROM ACT_ACC_IDIOMA WHERE IDIOMA='" & gIdiomaGER & "' ORDER BY ID"
        
        Set AdoRes = New ADODB.Recordset
        
        AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
        
        While Not AdoRes.EOF
            sConsulta = "INSERT INTO [dbo].[ACC_IDIOMA] VALUES (" & AdoRes("ID").Value & ", 'GER', " & StrToSQLNULL(NullToStr(AdoRes("DEN").Value)) & ")"
            ExecuteSQL gRDOCon, sConsulta
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    'FRA
    If bActivoFra Then
        sSQL = "SELECT * FROM ACT_ACC_IDIOMA WHERE IDIOMA='" & gIdiomaFRA & "' ORDER BY ID"
        
        Set AdoRes = New ADODB.Recordset
        
        AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
        
        While Not AdoRes.EOF
            sConsulta = "INSERT INTO [dbo].[ACC_IDIOMA] VALUES (" & AdoRes("ID").Value & ", 'FRA', " & StrToSQLNULL(NullToStr(AdoRes("DEN").Value)) & ")"
            ExecuteSQL gRDOCon, sConsulta
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    sConsulta = "ALTER TABLE ACCMEN CHECK CONSTRAINT ALL"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE PERF_ACC CHECK CONSTRAINT ALL"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE USU_ACC CHECK CONSTRAINT ALL"
    ExecuteSQL gRDOCon, sConsulta
    
    
    'lo que se introduce con idioma SPA
    sSQL = "SELECT * FROM ACT_MENU WHERE IDIOMA = '" & gIdiomaSPA & "' ORDER BY COD"
    
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO [dbo].[MENU] VALUES (" & AdoRes("COD").Value & ", "
        sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU1").Value)) & ", "
        sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU2").Value)) & ", "
        sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU3").Value)) & ", "
        sConsulta = sConsulta & AdoRes("ACC_INI").Value & ", "
        sConsulta = sConsulta & AdoRes("ACC_FIN").Value & ", "
        sConsulta = sConsulta & "CONVERT(DATETIME,'" & Format(Date, "dd/mm/yyyy") & "',103), "
        sConsulta = sConsulta & "'SPA')"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    Set AdoRes = Nothing
    
    'lo que se introduce con idioma ENG
    If bActivoEng Then
        sSQL = "SELECT * FROM ACT_MENU WHERE IDIOMA = '" & gIdiomaENG & "' ORDER BY COD"
        
        Set AdoRes = New ADODB.Recordset
        
        AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
        
        While Not AdoRes.EOF
            sConsulta = "INSERT INTO [dbo].[MENU] VALUES (" & AdoRes("COD").Value & ", "
            sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU1").Value)) & ", "
            sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU2").Value)) & ", "
            sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU3").Value)) & ", "
            sConsulta = sConsulta & AdoRes("ACC_INI").Value & ", "
            sConsulta = sConsulta & AdoRes("ACC_FIN").Value & ", "
            sConsulta = sConsulta & "CONVERT(DATETIME,'" & Format(Date, "dd/mm/yyyy") & "',103), "
            sConsulta = sConsulta & "'ENG')"
            ExecuteSQL gRDOCon, sConsulta
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    'lo que se introduce con idioma GER
    If bActivoGer Then
        sSQL = "SELECT * FROM ACT_MENU WHERE IDIOMA = '" & gIdiomaGER & "' ORDER BY COD"
        
        Set AdoRes = New ADODB.Recordset
        
        AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
        
        While Not AdoRes.EOF
            sConsulta = "INSERT INTO [dbo].[MENU] VALUES (" & AdoRes("COD").Value & ", "
            sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU1").Value)) & ", "
            sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU2").Value)) & ", "
            sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU3").Value)) & ", "
            sConsulta = sConsulta & AdoRes("ACC_INI").Value & ", "
            sConsulta = sConsulta & AdoRes("ACC_FIN").Value & ", "
            sConsulta = sConsulta & "CONVERT(DATETIME,'" & Format(Date, "dd/mm/yyyy") & "',103), "
            sConsulta = sConsulta & "'GER')"
            ExecuteSQL gRDOCon, sConsulta
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    'IDIOMA FRA
    If bActivoFra Then
        sSQL = "SELECT * FROM ACT_MENU WHERE IDIOMA = '" & gIdiomaFRA & "' ORDER BY COD"
        
        Set AdoRes = New ADODB.Recordset
        
        AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
        
        While Not AdoRes.EOF
            sConsulta = "INSERT INTO [dbo].[MENU] VALUES (" & AdoRes("COD").Value & ", "
            sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU1").Value)) & ", "
            sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU2").Value)) & ", "
            sConsulta = sConsulta & StrToSQLNULL(NullToStr(AdoRes("MENU3").Value)) & ", "
            sConsulta = sConsulta & AdoRes("ACC_INI").Value & ", "
            sConsulta = sConsulta & AdoRes("ACC_FIN").Value & ", "
            sConsulta = sConsulta & "CONVERT(DATETIME,'" & Format(Date, "dd/mm/yyyy") & "',103), "
            sConsulta = sConsulta & "'FRA')"
            ExecuteSQL gRDOCon, sConsulta
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    
    'Creamos una tabla temporal, lo insertamos todo y luego pasamos a la de verdad solo lo que  no tiene
    sConsulta = "if exists (select * from tempdb..sysobjects where id = object_id(N'tempdb..#ACT_WTEXT') )" & vbCrLf
    sConsulta = sConsulta & "drop table tempdb..#ACT_WTEXT" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE TABLE #ACT_WTEXT ("
    sConsulta = sConsulta & "[MODULO] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "[ID] [int] NOT NULL " & vbCrLf
    sConsulta = sConsulta & ")"
    ExecuteSQL gRDOCon, sConsulta
    
    
    sSQL = "SELECT * FROM ACT_WEBTEXT_CONS ORDER BY MODULO,ID"
    Set AdoRes = New ADODB.Recordset
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO tempdb..#ACT_WTEXT VALUES (" & AdoRes("MODULO").Value & ", " & AdoRes("ID").Value & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    
    AdoRes.Close
    Set AdoRes = Nothing
    
    sConsulta = "DELETE FROM WEBTEXT WHERE NOT EXISTS "
    sConsulta = sConsulta & " (SELECT * FROM #ACT_WTEXT WHERE WEBTEXT.MODULO=#ACT_WTEXT.MODULO AND WEBTEXT.ID=#ACT_WTEXT.ID)"
    ExecuteSQL gRDOCon, sConsulta
        
    
    sSQL = "SELECT W.* FROM ACT_WEBTEXT W LEFT JOIN ACT_WEBTEXT_CONS A ON W.MODULO = A.MODULO AND W.ID = A.ID WHERE A.MODULO IS NULL ORDER BY W.MODULO, W.ID"
    
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO [dbo].[WEBTEXT] (MODULO,ID,TEXT_SPA,TEXT_ENG,TEXT_GER,TEXT_FRA) VALUES (" & AdoRes("MODULO").Value & ", " & AdoRes("ID").Value & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaSPA).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaENG).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaGER).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaFRA).Value)) & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    
    
    sSQL = "SELECT W.* FROM ACT_WEBTEXT W INNER JOIN ACT_WEBTEXT_CONS A ON W.MODULO = A.MODULO AND W.ID = A.ID ORDER BY W.MODULO, W.ID"
    
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
    
        sConsulta = "IF NOT EXISTS(SELECT * FROM WEBTEXT WHERE MODULO =" & AdoRes("MODULO").Value & " AND ID=" & AdoRes("ID").Value & ")" & vbCrLf _
                  & "INSERT INTO [dbo].[WEBTEXT] (MODULO,ID,TEXT_SPA,TEXT_ENG,TEXT_GER,TEXT_FRA) VALUES (" & AdoRes("MODULO").Value & ", " & AdoRes("ID").Value & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaSPA).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaENG).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaGER).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaFRA).Value)) & ")"
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "SELECT * FROM WEBTEXT WHERE MODULO =" & AdoRes("MODULO").Value & " AND ID=" & AdoRes("ID").Value
        
        sSQL = ""
        Set rdoRS = gRDOCon.OpenResultset(sConsulta, rdOpenDynamic)
        For Each rdoCol In rdoRS.rdoColumns
            Select Case rdoCol.Name
                Case "MODULO", "ID", "OBS"
                
                Case Else
                    If IsNull(rdoCol.Value) Then
                        sSQL = sSQL & "UPDATE WEBTEXT SET " & rdoCol.Name & "=" & StrToSQLNULL(AdoRes.Fields(rdoCol.Name).Value) & " WHERE MODULO =" & AdoRes("MODULO").Value & " AND ID=" & AdoRes("ID").Value & vbCrLf
                    End If
            End Select
        Next
        rdoRS.Close
        Set rdoRS = Nothing
        If sSQL <> "" Then
            ExecuteSQL gRDOCon, sSQL
        End If
        
        AdoRes.MoveNext
    
    
    Wend
    AdoRes.Close
    
    
    'Creamos una tabla temporal, lo insertamos todo y luego pasamos a la de verdad solo lo que  no tiene
    sConsulta = "DELETE FROM #ACT_WTEXT "
    ExecuteSQL gRDOCon, sConsulta
    
    sSQL = "SELECT * FROM ACT_WEBFSEPTEXT_CONS ORDER BY MODULO,ID"
    Set AdoRes = New ADODB.Recordset
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO tempdb..#ACT_WTEXT VALUES (" & AdoRes("MODULO").Value & ", " & AdoRes("ID").Value & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    
    AdoRes.Close
    Set AdoRes = Nothing
    
    sConsulta = "DELETE FROM WEBFSEPTEXT WHERE NOT EXISTS "
    sConsulta = sConsulta & " (SELECT * FROM #ACT_WTEXT WHERE WEBFSEPTEXT.MODULO=#ACT_WTEXT.MODULO AND WEBFSEPTEXT.ID=#ACT_WTEXT.ID)"
    ExecuteSQL gRDOCon, sConsulta
        
    
    sSQL = "SELECT W.* FROM ACT_WEBFSEPTEXT W LEFT JOIN ACT_WEBFSEPTEXT_CONS A ON W.MODULO = A.MODULO AND W.ID = A.ID WHERE A.MODULO IS NULL ORDER BY W.MODULO, W.ID"
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO [dbo].[WEBFSEPTEXT] (MODULO,ID,TEXT_SPA,TEXT_ENG, TEXT_GER, TEXT_FRA) VALUES (" & AdoRes("MODULO").Value & ", " & AdoRes("ID").Value & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaSPA).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaENG).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaGER).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaFRA).Value)) & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    
    sSQL = "SELECT W.* FROM ACT_WEBFSEPTEXT W INNER JOIN ACT_WEBFSEPTEXT_CONS A ON W.MODULO = A.MODULO AND W.ID = A.ID ORDER BY W.MODULO, W.ID"
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
    
        sConsulta = "IF NOT EXISTS(SELECT * FROM WEBFSEPTEXT WHERE MODULO =" & AdoRes("MODULO").Value & " AND ID=" & AdoRes("ID").Value & ")" & vbCrLf _
                  & "INSERT INTO [dbo].[WEBFSEPTEXT] (MODULO,ID,TEXT_SPA,TEXT_ENG,TEXT_GER,TEXT_FRA) VALUES (" & AdoRes("MODULO").Value & ", " & AdoRes("ID").Value & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaSPA).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaENG).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaGER).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaFRA).Value)) & ")"
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "SELECT * FROM WEBFSEPTEXT WHERE MODULO =" & AdoRes("MODULO").Value & " AND ID=" & AdoRes("ID").Value
        
        sSQL = ""
        Set rdoRS = gRDOCon.OpenResultset(sConsulta, rdOpenDynamic)
        For Each rdoCol In rdoRS.rdoColumns
            Select Case rdoCol.Name
                Case "MODULO", "ID", "OBS"
                
                Case Else
                    If IsNull(rdoCol.Value) Then
                        sSQL = sSQL & "UPDATE WEBFSEPTEXT SET " & rdoCol.Name & "=" & StrToSQLNULL(AdoRes.Fields(rdoCol.Name).Value) & " WHERE MODULO =" & AdoRes("MODULO").Value & " AND ID=" & AdoRes("ID").Value & vbCrLf
                    End If
            End Select
        Next
        rdoRS.Close
        Set rdoRS = Nothing
        If sSQL <> "" Then
            ExecuteSQL gRDOCon, sSQL
        End If
        
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    


'WEBFSWSTEXT

    'Creamos una tabla temporal, lo insertamos todo y luego pasamos a la de verdad solo lo que  no tiene
    sConsulta = "DELETE FROM #ACT_WTEXT "
    ExecuteSQL gRDOCon, sConsulta
    
    sSQL = "SELECT * FROM ACT_WEBFSWSTEXT_CONS ORDER BY MODULO,ID"
    Set AdoRes = New ADODB.Recordset
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO tempdb..#ACT_WTEXT VALUES (" & AdoRes("MODULO").Value & ", " & AdoRes("ID").Value & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    
    AdoRes.Close
    Set AdoRes = Nothing
    
    sConsulta = "DELETE FROM WEBFSWSTEXT WHERE NOT EXISTS "
    sConsulta = sConsulta & " (SELECT * FROM #ACT_WTEXT WHERE WEBFSWSTEXT.MODULO=#ACT_WTEXT.MODULO AND WEBFSWSTEXT.ID=#ACT_WTEXT.ID)"
    ExecuteSQL gRDOCon, sConsulta
        
    
    sSQL = "SELECT W.* FROM ACT_WEBFSWSTEXT W LEFT JOIN ACT_WEBFSWSTEXT_CONS A ON W.MODULO = A.MODULO AND W.ID = A.ID WHERE A.MODULO IS NULL ORDER BY W.MODULO, W.ID"
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
    
        sConsulta = "INSERT INTO [dbo].[WEBFSWSTEXT] (MODULO,ID,TEXT_SPA,TEXT_ENG, TEXT_GER, TEXT_FRA) VALUES (" & AdoRes("MODULO").Value & ", " & AdoRes("ID").Value & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaSPA).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaENG).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaGER).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaFRA).Value)) & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    
    sSQL = "SELECT W.* FROM ACT_WEBFSWSTEXT W INNER JOIN ACT_WEBFSWSTEXT_CONS A ON W.MODULO = A.MODULO AND W.ID = A.ID ORDER BY W.MODULO, W.ID"
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
    
        sConsulta = "IF NOT EXISTS(SELECT * FROM WEBFSWSTEXT WHERE MODULO =" & AdoRes("MODULO").Value & " AND ID=" & AdoRes("ID").Value & ")" & vbCrLf _
                  & "INSERT INTO [dbo].[WEBFSWSTEXT] (MODULO,ID,TEXT_SPA,TEXT_ENG,TEXT_GER,TEXT_FRA) VALUES (" & AdoRes("MODULO").Value & ", " & AdoRes("ID").Value & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaSPA).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaENG).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaGER).Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXT_" & gIdiomaFRA).Value)) & ")"
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "SELECT * FROM WEBFSWSTEXT WHERE MODULO =" & AdoRes("MODULO").Value & " AND ID=" & AdoRes("ID").Value
        
        sSQL = ""
        Set rdoRS = gRDOCon.OpenResultset(sConsulta, rdOpenDynamic)
        For Each rdoCol In rdoRS.rdoColumns
            Select Case rdoCol.Name
                Case "MODULO", "ID", "OBS"
                
                Case Else
                    If IsNull(rdoCol.Value) Then
                        sSQL = sSQL & "UPDATE WEBFSWSTEXT SET " & rdoCol.Name & "=" & StrToSQLNULL(AdoRes.Fields(rdoCol.Name).Value) & " WHERE MODULO =" & AdoRes("MODULO").Value & " AND ID=" & AdoRes("ID").Value & vbCrLf
                    End If
            End Select
        Next
        rdoRS.Close
        Set rdoRS = Nothing
        If sSQL <> "" Then
            ExecuteSQL gRDOCon, sSQL
        End If
        
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    

    'TEXTOS_GS
    sSQL = "DELETE FROM TEXTOS_GS"
    ExecuteSQL gRDOCon, sSQL
    
    sSQL = "SELECT * FROM ACT_TEXTOS_GS ORDER BY MODULO,ID"
    Set AdoRes = New ADODB.Recordset
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
    
        sConsulta = "INSERT INTO [dbo].[TEXTOS_GS] (MODULO,ID,TEXT_SPA,TEXT_ENG,TEXT_GER,TEXT_FRA) VALUES (" & AdoRes("MODULO").Value & "," & AdoRes("ID").Value & "," & StrToSQLNULL(AdoRes("TEXT_" & gIdiomaSPA).Value) & "," & StrToSQLNULL(AdoRes("TEXT_" & gIdiomaENG).Value) & "," & StrToSQLNULL(AdoRes("TEXT_" & gIdiomaGER).Value) & "," & StrToSQLNULL(AdoRes("TEXT_" & gIdiomaFRA).Value) & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    
    'TEXTOS_UM
    sSQL = "DELETE FROM TEXTOS_UM"
    ExecuteSQL gRDOCon, sSQL
    
    sSQL = "SELECT * FROM ACT_TEXTOS_UM ORDER BY MODULO,ID"
    Set AdoRes = New ADODB.Recordset
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
    
        sConsulta = "INSERT INTO [dbo].[TEXTOS_UM] (MODULO,ID,TEXT_SPA,TEXT_ENG,TEXT_GER,TEXT_FRA) VALUES (" & AdoRes("MODULO").Value & "," & AdoRes("ID").Value & "," & StrToSQLNULL(AdoRes("TEXT_" & gIdiomaSPA).Value) & "," & StrToSQLNULL(AdoRes("TEXT_" & gIdiomaENG).Value) & "," & StrToSQLNULL(AdoRes("TEXT_" & gIdiomaGER).Value) & "," & StrToSQLNULL(AdoRes("TEXT_" & gIdiomaFRA).Value) & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    
    'FSGA_TEXTOS_MULTIIDIOMA
    sConsulta = "DELETE FROM FSGA_TEXTOS_MULTIIDIOMA "
    ExecuteSQL gRDOCon, sConsulta
        
    
    sSQL = "SELECT * FROM ACT_FSGA_TEXTOS_MULTIIDIOMA ORDER BY ID,IDIOMA"
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO [dbo].[FSGA_TEXTOS_MULTIIDIOMA] (ID,IDIOMA,TEXTO, TIPO) VALUES (" & AdoRes("ID").Value & "," & StrToSQLNULL(NullToStr(AdoRes("IDIOMA").Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TEXTO").Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("TIPO").Value)) & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    
    
    'FSAL_TIPO_EVENTO_DEN
    sConsulta = "DELETE FROM FSAL_TIPO_EVENTO_DEN"
    ExecuteSQL gRDOCon, sConsulta
        
    
    sSQL = "SELECT * FROM ACT_FSAL_TIPO_EVENTO_DEN ORDER BY ID,IDIOMA"
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO [dbo].[FSAL_TIPO_EVENTO_DEN] (ID,IDI,DEN) VALUES (" & AdoRes("ID").Value & "," & StrToSQLNULL(NullToStr(AdoRes("IDIOMA").Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("DEN").Value)) & ")"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    
    
    'FSAL_KPI_DEN
    sConsulta = "DELETE FROM FSAL_KPI_DEN"
    ExecuteSQL gRDOCon, sConsulta
        
    
    sSQL = "SELECT * FROM ACT_FSAL_KPI_DEN ORDER BY ID,IDIOMA"
    Set AdoRes = New ADODB.Recordset
    
    AdoRes.Open sSQL, ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not AdoRes.EOF
        sConsulta = "INSERT INTO [dbo].[FSAL_KPI_DEN] (ID_KPI,IDIOMA,DEN_KPI,FECACT) VALUES (" & AdoRes("ID").Value & "," & StrToSQLNULL(NullToStr(AdoRes("IDIOMA").Value)) & ", " & StrToSQLNULL(NullToStr(AdoRes("DEN").Value)) & ",GETDATE())"
        ExecuteSQL gRDOCon, sConsulta
        AdoRes.MoveNext
    Wend
    AdoRes.Close
    
    
    Set AdoRes.ActiveConnection = Nothing
    Set AdoRes = Nothing
    ADOCon.Close
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
    InsertIdiomas = True
    Exit Function

Error:

    If bTransaccionEnCurso Then
        sConsulta = "ROLLBACK TRANSACTION"
        ExecuteSQL gRDOCon, sConsulta

    End If
     
    MsgBox Err.Description, vbCritical + vbOKOnly
    
    InsertIdiomas = False

End Function


Public Function CrearJobs() As Boolean
    
    Dim bTransaccionEnCurso As Boolean
    
    On Error GoTo Error
    
    '''10.- CADUCIDAD
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

        frmProgreso.lblDetalle = "Actualizaciones para la caducidad de las ofertas"
        frmProgreso.lblDetalle.Refresh
        frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
        
        If Not basCaducidad.CrearJobCaducidad Then
            If bTransaccionEnCurso Then
                gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
            End If
            CrearJobs = False
            Exit Function
        End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
    
    '''10.- ahorros
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
    
        frmProgreso.lblDetalle = "Actualizaciones para el cálculo autómatico de ahorros"
        frmProgreso.lblDetalle.Refresh
        frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
        
        If Not basCaducidad.CrearJobAhorros Then
            If bTransaccionEnCurso Then
                gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
            End If
            CrearJobs = False
            Exit Function
        End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
    
    '''10.- despublicación de certificados
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
    
        frmProgreso.lblDetalle = "Actualizaciones para la despublicación de certificados"
        frmProgreso.lblDetalle.Refresh
        frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
        
        If Not basCaducidad.CrearJobCertificados Then
            If bTransaccionEnCurso Then
                gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
            End If
            CrearJobs = False
            Exit Function
        End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
    
  
    ''''ELIMINAR INSTANCIAS INCOMPLETAS
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
    
    
        frmProgreso.lblDetalle = "Actualizaciones para la eliminación de solicitudes incompletas"
        frmProgreso.lblDetalle.Refresh
        frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
        
        If Not basCaducidad.CrearJobEliminarInstIncomp Then
            If bTransaccionEnCurso Then
                gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
            End If
            CrearJobs = False
            Exit Function
        End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
    
    

    
    
    
    ''''ELIMINAR INSTANCIAS INCOMPLETAS EN QA
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
    
    
        frmProgreso.lblDetalle = "Actualizaciones para la eliminación de solicitudes incompletas en QA"
        frmProgreso.lblDetalle.Refresh
        frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
        
        If Not basCaducidad.CrearJobEliminarInstIncomp_QA Then
            If bTransaccionEnCurso Then
                gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
            End If
            CrearJobs = False
            Exit Function
        End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
    
    
    ''''Contratos: renovación automática y expiración
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Actualizaciones para la renovación y expiración de contratos"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
       
    If Not basCaducidad.CrearJobRenovarExpirarContratos Then
        If bTransaccionEnCurso Then
            gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
        End If
        CrearJobs = False
        Exit Function
    End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
        
    
    ''' Recepciones Automaticas
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Actualizaciones para el RecepcionesAutomaticas"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
        
    If Not basCaducidad.CrearJobRecepcionesAutomaticas Then
        If bTransaccionEnCurso Then
            gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
        End If
        CrearJobs = False
        Exit Function
    End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
    
    ''''Actualizar el estado de los pedidos abiertos
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
    
    
        frmProgreso.lblDetalle = "Actualizaciones para modificar el estado de los pedidos abiertos"
        frmProgreso.lblDetalle.Refresh
        frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
        
        If Not basCaducidad.CrearJobModificarEstadoPedidoAbierto Then
            If bTransaccionEnCurso Then
                gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
            End If
            CrearJobs = False
            Exit Function
        End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
        
    
        ''' TRUNCAR LOGS DE BD
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Actualizaciones para el truncar log"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
        
        If Not basCaducidad.CrearJobTruncarLog Then
            If bTransaccionEnCurso Then
                gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
            End If
            CrearJobs = False
            Exit Function
        End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
    
    CrearJobs = True
    Exit Function
Error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
     
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CrearJobs = False

End Function



Public Sub GenerarThumbnails()
Dim pj As PajantImage
Dim adocom As ADODB.Command
Dim adoRS As ADODB.Recordset
Dim adoRS2 As ADODB.Recordset
Dim imgBD As Variant
Dim imgProve As Variant
Dim lMax As Long
Dim i As Integer
Dim fso As New Scripting.FileSystemObject
Dim ADOCon As ADODB.Connection
Set ADOCon = New ADODB.Connection


frmProgreso.lblDetalle = "Actualizamos los thumbnails"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

If fso.FileExists(App.Path & "\errores.log") Then
    fso.DeleteFile (App.Path & "\errores.log")
End If

Set fso = Nothing

ADOCon.Open "Provider=SQLOLEDB.1;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
ADOCon.CursorLocation = adUseClient
ADOCon.CommandTimeout = 120

lMax = 100

Dim h As Long
Dim W As Long
Dim hO As Long
Dim wO As Long

On Error GoTo Err_


    Set pj = New PajantImage
    pj.Register "FULLSTEP NETWORKS S.L.", "3C91D6B17E242021664AB9C6"

    Set adoRS2 = New ADODB.Recordset
    Set adoRS = New ADODB.Recordset
    adoRS.Open "SELECT ART, PROVE, IMAGEN, DATALENGTH(IMAGEN) DATASIZE FROM PROVE_ART4 WHERE DATALENGTH(IMAGEN)>0", ADOCon, adOpenForwardOnly
    
    While Not adoRS.EOF
    
        imgBD = adoRS.Fields("IMAGEN").GetChunk(adoRS.Fields("DATASIZE").Value)
        pj.ImportFrom PJT_MEMPNG, 0, 0, imgBD, PJT_TONEWIMAGE
        hO = pj.Height
        wO = pj.Width
        
        If hO > wO Then
            h = lMax
            W = wO * lMax / hO
        Else
            W = lMax
            h = hO * lMax / wO
        End If
        imgProve = Null
            
        If hO > lMax Or wO > lMax Then
            pj.ResizeImage W, h, PJT_SIMPLE
        End If
        pj.ExportTo PJT_MEMJPEG, 0, 0, imgProve, PJT_FRAME
        
        i = 1
 
        sConsulta = "SELECT THUMBNAIL FROM PROVE_ART4 WHERE PROVE='" & DblQuote(adoRS.Fields("PROVE").Value) _
        & "' AND ART='" & DblQuote(adoRS.Fields("ART").Value) & "'"

        adoRS2.Open sConsulta, ADOCon, adOpenKeyset, adLockOptimistic
        If Not adoRS2.EOF Then
            If Not IsNull(imgProve) Then
                adoRS2("THUMBNAIL").AppendChunk imgProve
            End If
            adoRS2.Update
        End If
        adoRS2.Close
        i = 2
        If frmProgreso.ProgressBar1.Value > 9900 Then frmProgreso.ProgressBar1.Value = 1
        frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 10
        DoEvents
        adoRS.MoveNext
    Wend
    
    adoRS.Close
    Set adoRS = Nothing
    Set adoRS2 = Nothing
    ADOCon.Close
    Set ADOCon = Nothing
    Set adocom = Nothing
    
    
    Exit Sub
Err_:
    If i = 1 Then
        Open App.Path & "\errores.log" For Append As #1
        Print #1, adoRS.Fields("ART").Value & " " & adoRS.Fields("PROVE").Value
        Print #1, vbTab & Err.Number & "-" & Err.Description
        Close #1
    End If

    Resume Next
    Resume 0

End Sub

Public Function Actualizar_2(ByVal deVersion As String, AVersion As String) As Boolean
    Dim NumVersion As String
    Dim DeVerSinPuntos As String
    Dim AVerSinPuntos As String

    DeVerSinPuntos = Replace(deVersion, ".", "")
    AVerSinPuntos = Replace(AVersion, ".", "")
    ' Precondición
    If CDbl(DeVerSinPuntos) > CDbl(AVerSinPuntos) Then
        Actualizar_2 = True
        Exit Function
    End If

    If CDbl(DeVerSinPuntos) = CDbl(AVerSinPuntos) Then
        Actualizar_2 = True
        Exit Function
    End If

    gLongitudesDeCodigos = basUtilidades.DevolverLongitudesDeCodigos
    
    frmProgreso.lblDetalle = "Actualizando de versión : " & CStr(deVersion) & " a versión " & CStr(AVersion)
    
    frmProgreso.Show
    
    DoEvents
    
    frmProgreso.ProgressBar1.Value = 1

    Select Case deVersion
     Case "3.00.22.026", "3.00.22.027", "3.00.22.028", "3.00.22.029", "3.00.22.030", "3.00.22.031", "3.00.22.032", "3.00.22.033", "3.00.22.034", "3.00.22.035", "3.00.22.036", "3.00.22.037"
        NumVersion = "3.00.23.000"
        If Not CodigoDeActualizacion32100_01_2225_A32100_02_2300 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.000"
        NumVersion = "3.00.23.001"
        If Not CodigoDeActualizacion32100_02_2300_A32100_02_2301 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.001"
        NumVersion = "3.00.23.002"
        If Not CodigoDeActualizacion32100_02_2301_A32100_02_2302 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.002"
        NumVersion = "3.00.23.003"
        If Not CodigoDeActualizacion32100_02_2302_A32100_02_2303 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.003"
        NumVersion = "3.00.23.004"
        If Not CodigoDeActualizacion32100_02_2303_A32100_02_2304 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.004"
        NumVersion = "3.00.23.005"
        If Not CodigoDeActualizacion32100_02_2304_A32100_02_2305 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.005"
        NumVersion = "3.00.23.006"
        If Not CodigoDeActualizacion32100_02_2305_A32100_02_2306 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.006"
        NumVersion = "3.00.23.007"
        If Not CodigoDeActualizacion32100_02_2306_A32100_02_2307 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.007"
        NumVersion = "3.00.23.008"
        If Not CodigoDeActualizacion32100_02_2307_A32100_02_2308 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.008"
        NumVersion = "3.00.23.009"
        If Not CodigoDeActualizacion32100_02_2308_A32100_02_2309 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.009"
        NumVersion = "3.00.23.010"
        If Not CodigoDeActualizacion32100_02_2309_A32100_02_2310 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.010"
        NumVersion = "3.00.23.011"
        If Not CodigoDeActualizacion32100_02_2310_A32100_02_2311 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.011"
        NumVersion = "3.00.23.012"
        If Not CodigoDeActualizacion32100_02_2311_A32100_02_2312 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.012"
        NumVersion = "3.00.23.013"
        If Not CodigoDeActualizacion32100_02_2312_A32100_02_2313 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.013"
        NumVersion = "3.00.23.014"
        If Not CodigoDeActualizacion32100_02_2313_A32100_02_2314 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.014"
        NumVersion = "3.00.23.015"
        If Not CodigoDeActualizacion32100_02_2314_A32100_02_2315 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.015"
        NumVersion = "3.00.23.016"
        If Not CodigoDeActualizacion32100_02_2315_A32100_02_2316 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.016"
        NumVersion = "3.00.23.017"
        If Not CodigoDeActualizacion32100_02_2316_A32100_02_2317 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.017"
        NumVersion = "3.00.23.018"
        If Not CodigoDeActualizacion32100_02_2317_A32100_02_2318 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.018"
        NumVersion = "3.00.23.019"
        If Not CodigoDeActualizacion32100_02_2318_A32100_02_2319 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.019"
        NumVersion = "3.00.23.020"
        If Not CodigoDeActualizacion32100_02_2319_A32100_02_2320 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.020"
        NumVersion = "3.00.23.021"
        If Not CodigoDeActualizacion32100_02_2320_A32100_02_2321 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.23.021", "3.00.23.022", "3.00.23.023"
        NumVersion = "3.00.24.000"
        If Not CodigoDeActualizacion32100_03_2321_A32100_03_2400 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.000"
        NumVersion = "3.00.24.001"
        If Not CodigoDeActualizacion32100_03_2400_A32100_03_2401 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.001"
        NumVersion = "3.00.24.002"
        If Not CodigoDeActualizacion32100_03_2401_A32100_03_2402 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.002"
        NumVersion = "3.00.24.003"
        If Not CodigoDeActualizacion32100_03_2402_A32100_03_2403 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.003"
        NumVersion = "3.00.24.004"
        If Not CodigoDeActualizacion32100_03_2403_A32100_03_2404 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.004"
        NumVersion = "3.00.24.005"
        If Not CodigoDeActualizacion32100_03_2404_A32100_03_2405 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.005"
        NumVersion = "3.00.24.006"
        If Not CodigoDeActualizacion32100_03_2405_A32100_03_2406 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
     Case "3.00.24.006"
        NumVersion = "3.00.24.007"
        If Not CodigoDeActualizacion32100_03_2406_A32100_03_2407 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.007"
        NumVersion = "3.00.24.008"
        If Not CodigoDeActualizacion32100_03_2407_A32100_03_2408 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.008"
        NumVersion = "3.00.24.009"
        If Not CodigoDeActualizacion32100_03_2408_A32100_03_2409 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.009"
        NumVersion = "3.00.24.010"
        If Not CodigoDeActualizacion32100_03_2409_A32100_03_2410 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.010"
        NumVersion = "3.00.24.011"
        If Not CodigoDeActualizacion32100_03_2410_A32100_03_2411 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.011"
        NumVersion = "3.00.24.012"
        If Not CodigoDeActualizacion32100_03_2411_A32100_03_2412 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.012"
        NumVersion = "3.00.24.013"
        If Not CodigoDeActualizacion32100_03_2412_A32100_03_2413 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.013"
        NumVersion = "3.00.24.014"
        If Not CodigoDeActualizacion32100_03_2413_A32100_03_2414 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.24.014", "3.00.24.015", "3.00.24.016", "3.00.24.017", "3.00.24.018", "3.00.24.019", "3.00.24.020", "3.00.24.021", "3.00.24.022", "3.00.24.023", "3.00.24.024", "3.00.24.025"
        NumVersion = "3.00.25.001"
        If Not CodigoDeActualizacion32100_04_2500_A32100_04_2501(True, True, True, True) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "-3.00.25.000"
        NumVersion = "3.00.25.001"
        If Not CodigoDeActualizacion32100_04_2500_A32100_04_2501(False, True, True, True) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "-3.00.25.001"
        NumVersion = "3.00.25.001"
        If Not CodigoDeActualizacion32100_04_2500_A32100_04_2501(False, False, True, True) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "-3.00.25.002"
        NumVersion = "3.00.25.001"
        If Not CodigoDeActualizacion32100_04_2500_A32100_04_2501(False, False, False, True) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.001"
        NumVersion = "3.00.25.002"
        If Not CodigoDeActualizacion32100_04_2501_A32100_04_2502 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.002"
        NumVersion = "3.00.25.003"
        If Not CodigoDeActualizacion32100_04_2502_A32100_04_2503 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.003"
        NumVersion = "3.00.25.004"
        If Not CodigoDeActualizacion32100_04_2503_A32100_04_2504 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.004"
        NumVersion = "3.00.25.005"
        If Not CodigoDeActualizacion32100_04_2504_A32100_04_2505 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.005"
        NumVersion = "3.00.25.006"
        If Not CodigoDeActualizacion32100_04_2505_A32100_04_2506 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.006"
        NumVersion = "3.00.25.007"
        If Not CodigoDeActualizacion32100_04_2506_A32100_04_2507 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.007"
        NumVersion = "3.00.25.008"
        If Not CodigoDeActualizacion32100_04_2507_A32100_04_2508 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.008"
        NumVersion = "3.00.25.009"
        If Not CodigoDeActualizacion32100_04_2508_A32100_04_2509 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.009"
        NumVersion = "3.00.25.010"
        If Not CodigoDeActualizacion32100_04_2509_A32100_04_2510 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.010"
        NumVersion = "3.00.25.011"
        If Not CodigoDeActualizacion32100_04_2510_A32100_04_2511 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.011"
        NumVersion = "3.00.25.012"
        If Not CodigoDeActualizacion32100_04_2511_A32100_04_2512 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.012"
        NumVersion = "3.00.25.013"
        If Not CodigoDeActualizacion32100_04_2512_A32100_04_2513 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.013"
        NumVersion = "3.00.25.014"
        If Not CodigoDeActualizacion32100_04_2513_A32100_04_2514 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.014"
        NumVersion = "3.00.25.015"
        If Not CodigoDeActualizacion32100_04_2514_A32100_04_2515 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.015"
        NumVersion = "3.00.25.016"
        If Not CodigoDeActualizacion32100_04_2515_A32100_04_2516 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.016"
        NumVersion = "3.00.25.017"
        If Not CodigoDeActualizacion32100_04_2516_A32100_04_2517 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.017"
        NumVersion = "3.00.25.018"
        If Not CodigoDeActualizacion32100_04_2517_A32100_04_2518 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.018"
        NumVersion = "3.00.25.019"
        If Not CodigoDeActualizacion32100_04_2518_A32100_04_2519 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.019"
        NumVersion = "3.00.25.020"
        If Not CodigoDeActualizacion32100_04_2519_A32100_04_2520 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.020"
        NumVersion = "3.00.25.021"
        If Not CodigoDeActualizacion32100_04_2520_A32100_04_2521 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.021"
        NumVersion = "3.00.25.022"
        If Not CodigoDeActualizacion32100_04_2521_A32100_04_2522 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.022"
        NumVersion = "3.00.25.023"
        If Not CodigoDeActualizacion32100_04_2522_A32100_04_2523 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.023"
        NumVersion = "3.00.25.024"
        If Not CodigoDeActualizacion32100_04_2523_A32100_04_2524 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.024"
        NumVersion = "3.00.25.025"
        If Not CodigoDeActualizacion32100_04_2524_A32100_04_2525 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.025"
        NumVersion = "3.00.25.026"
        If Not CodigoDeActualizacion32100_04_2525_A32100_04_2526 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.026"
        NumVersion = "3.00.25.027"
        If Not CodigoDeActualizacion32100_04_2526_A32100_04_2527 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.027"
        NumVersion = "3.00.25.028"
        If Not CodigoDeActualizacion32100_04_2527_A32100_04_2528 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.028"
        NumVersion = "3.00.25.029"
        If Not CodigoDeActualizacion32100_04_2528_A32100_04_2529 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.029"
        NumVersion = "3.00.25.030"
        If Not CodigoDeActualizacion32100_04_2529_A32100_04_2530 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.030"
        NumVersion = "3.00.25.031"
        If Not CodigoDeActualizacion32100_04_2530_A32100_04_2531 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.031"
        NumVersion = "3.00.25.032"
        If Not CodigoDeActualizacion32100_04_2531_A32100_04_2532 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.032"
        NumVersion = "3.00.25.033"
        If Not CodigoDeActualizacion32100_04_2532_A32100_04_2533 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.033"
        NumVersion = "3.00.25.034"
        If Not CodigoDeActualizacion32100_04_2533_A32100_04_2534 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.034"
        NumVersion = "3.00.25.035"
        If Not CodigoDeActualizacion32100_04_2534_A32100_04_2535 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.035"
        NumVersion = "3.00.25.036"
        If Not CodigoDeActualizacion32100_04_2535_A32100_04_2536 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.036"
        NumVersion = "3.00.25.037"
        If Not CodigoDeActualizacion32100_04_2536_A32100_04_2537 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.037"
        NumVersion = "3.00.25.038"
        If Not CodigoDeActualizacion32100_04_2537_A32100_04_2538 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.038"
        NumVersion = "3.00.25.039"
        If Not CodigoDeActualizacion32100_04_2538_A32100_04_2539 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.039"
        NumVersion = "3.00.25.040"
        If Not CodigoDeActualizacion32100_04_2539_A32100_04_2540 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.040"
        NumVersion = "3.00.25.041"
        If Not CodigoDeActualizacion32100_04_2540_A32100_04_2541 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.041"
        NumVersion = "3.00.25.042"
        If Not CodigoDeActualizacion32100_04_2541_A32100_04_2542 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.25.042", "3.00.25.043", "3.00.25.044", "3.00.25.045", "3.00.25.046", "3.00.25.047", "3.00.25.048"
        NumVersion = "3.00.26.001"
        If Not CodigoDeActualizacion32100_05_2600_A32100_05_2601 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.001"
        NumVersion = "3.00.26.002"
        If Not CodigoDeActualizacion32100_05_2601_A32100_05_2602 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.002"
        NumVersion = "3.00.26.003"
        If Not CodigoDeActualizacion32100_05_2602_A32100_05_2603 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.003"
        NumVersion = "3.00.26.004"
        If Not CodigoDeActualizacion32100_05_2603_A32100_05_2604 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.004"
        NumVersion = "3.00.26.005"
        If Not CodigoDeActualizacion32100_05_2604_A32100_05_2605 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.005"
        NumVersion = "3.00.26.006"
        If Not CodigoDeActualizacion32100_05_2605_A32100_05_2606 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.006"
        NumVersion = "3.00.26.007"
        If Not CodigoDeActualizacion32100_05_2606_A32100_05_2607 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.007"
        NumVersion = "3.00.26.008"
        If Not CodigoDeActualizacion32100_05_2607_A32100_05_2608 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.008"
        NumVersion = "3.00.26.009"
        If Not CodigoDeActualizacion32100_05_2608_A32100_05_2609 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.009"
        NumVersion = "3.00.26.010"
        If Not CodigoDeActualizacion32100_05_2609_A32100_05_2610 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.010"
        NumVersion = "3.00.26.011"
        If Not CodigoDeActualizacion32100_05_2610_A32100_05_2611 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.011"
        NumVersion = "3.00.26.012"
        If Not CodigoDeActualizacion32100_05_2611_A32100_05_2612 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.012"
        NumVersion = "3.00.26.013"
        If Not CodigoDeActualizacion32100_05_2612_A32100_05_2613 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.013"
        NumVersion = "3.00.26.014"
        If Not CodigoDeActualizacion32100_05_2613_A32100_05_2614 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.014"
        NumVersion = "3.00.26.015"
        If Not CodigoDeActualizacion32100_05_2614_A32100_05_2615 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.26.015", "3.00.26.016"
        NumVersion = "3.00.27.001"
        If Not CodigoDeActualizacion32100_06_2700_A32100_06_2701 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.27.001"
        NumVersion = "3.00.27.002"
        If Not CodigoDeActualizacion32100_06_2701_A32100_06_2702 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.27.002"
        NumVersion = "3.00.27.003"
        If Not CodigoDeActualizacion32100_06_2702_A32100_06_2703 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.27.003"
        NumVersion = "3.00.27.004"
        If Not CodigoDeActualizacion32100_06_2703_A32100_06_2704 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.27.004"
        NumVersion = "3.00.27.005"
        If Not CodigoDeActualizacion32100_06_2704_A32100_06_2705 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.27.005"
        NumVersion = "3.00.27.006"
        If Not CodigoDeActualizacion32100_06_2705_A32100_06_2706 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.27.006"
        NumVersion = "3.00.27.007"
        If Not CodigoDeActualizacion32100_06_2706_A32100_06_2707 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.27.007"
        NumVersion = "3.00.27.008"
        If Not CodigoDeActualizacion32100_06_2707_A32100_06_2708 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.27.008"
        NumVersion = "3.00.27.009"
        If Not CodigoDeActualizacion32100_06_2708_A32100_06_2709 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.27.009"
        NumVersion = "3.00.27.010"
        If Not CodigoDeActualizacion32100_06_2709_A32100_06_2710 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.27.010"
        NumVersion = "3.00.27.011"
        If Not CodigoDeActualizacion32100_06_2710_A32100_06_2711 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.27.011", "3.00.27.012", "3.00.27.013"
        NumVersion = "3.00.28.001"
        If Not CodigoDeActualizacion32100_07_2800_A32100_07_2801 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.001"
        NumVersion = "3.00.28.002"
        If Not CodigoDeActualizacion32100_07_2801_A32100_07_2802 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.002"
        NumVersion = "3.00.28.003"
        If Not CodigoDeActualizacion32100_07_2802_A32100_07_2803 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.003"
        NumVersion = "3.00.28.004"
        If Not CodigoDeActualizacion32100_07_2803_A32100_07_2804 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.004"
        NumVersion = "3.00.28.005"
        If Not CodigoDeActualizacion32100_07_2804_A32100_07_2805 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.005"
        NumVersion = "3.00.28.006"
        If Not CodigoDeActualizacion32100_07_2805_A32100_07_2806 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.006"
        NumVersion = "3.00.28.007"
        If Not CodigoDeActualizacion32100_07_2806_A32100_07_2807 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.007"
        NumVersion = "3.00.28.008"
        If Not CodigoDeActualizacion32100_07_2807_A32100_07_2808 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.008"
        NumVersion = "3.00.28.009"
        If Not CodigoDeActualizacion32100_07_2808_A32100_07_2809 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.009"
        NumVersion = "3.00.28.010"
        If Not CodigoDeActualizacion32100_07_2809_A32100_07_2810 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.010"
        NumVersion = "3.00.28.011"
        If Not CodigoDeActualizacion32100_07_2810_A32100_07_2811 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.011"
        NumVersion = "3.00.28.012"
        If Not CodigoDeActualizacion32100_07_2811_A32100_07_2812 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.28.012", "3.00.28.013", "3.00.28.014", "3.00.28.015", "3.00.28.016"
        NumVersion = "3.00.29.001"
        If Not CodigoDeActualizacion32100_08_2900_A32100_08_2901 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.29.001"
        NumVersion = "3.00.29.002"
        If Not CodigoDeActualizacion32100_08_2901_A32100_08_2902 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.29.002"
        NumVersion = "3.00.29.003"
        If Not CodigoDeActualizacion32100_08_2902_A32100_08_2903 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.29.003"
        NumVersion = "3.00.29.004"
        If Not CodigoDeActualizacion32100_08_2903_A32100_08_2904 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.29.004"
        NumVersion = "3.00.29.005"
        If Not CodigoDeActualizacion32100_08_2904_A32100_08_2905 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.29.005"
        NumVersion = "3.00.29.006"
        If Not CodigoDeActualizacion32100_08_2905_A32100_08_2906 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
     Case "3.00.29.006"
        NumVersion = "3.00.29.007"
        If Not CodigoDeActualizacion32100_08_2906_A32100_08_2907 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.29.007"
        NumVersion = "3.00.29.008"
        If Not CodigoDeActualizacion32100_08_2907_A32100_08_2908 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.29.008"
        NumVersion = "3.00.29.009"
        If Not CodigoDeActualizacion32100_08_3008_A32100_08_3009 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.29.009", "3.00.29.010", "3.00.29.011", "3.00.29.012", "3.00.29.013", "3.00.29.014", "3.00.29.015"  'Aquí empieza el SP2
        NumVersion = "3.00.29.020"
        If Not CodigoDeActualizacion32100_08_3019_A32100_08_3020 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
     Case "3.00.29.020"
        NumVersion = "3.00.29.021"
        If Not CodigoDeActualizacion32100_08_3020_A32100_08_3021 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.29.021", "3.00.29.022", "3.00.29.023", "3.00.29.024", "3.00.29.025", "3.00.29.026" 'Aquí empieza la versión 32100.9
        NumVersion = "3.00.30.001"
        If Not CodigoDeActualizacion32100_09_3000_A32100_09_3001 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.30.001"
        NumVersion = "3.00.30.002"
        If Not CodigoDeActualizacion32100_09_3001_A32100_09_3002 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.30.002"
        NumVersion = "3.00.30.003"
        If Not CodigoDeActualizacion32100_09_3002_A32100_09_3003 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.30.003"
        NumVersion = "3.00.30.004"
        If Not CodigoDeActualizacion32100_09_3003_A32100_09_3004 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.30.004"
        NumVersion = "3.00.30.005"
        If Not CodigoDeActualizacion32100_09_3004_A32100_09_3005 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
     Case "3.00.30.005", "3.00.30.006", "3.00.30.007", "3.00.30.008", "3.00.30.009", "3.00.30.010", "3.00.30.011", "3.00.30.012", "3.00.30.013"
        NumVersion = "3.00.31.001"
        If Not CodigoDeActualizacion32200_01_3100_A32200_01_3101 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
     Case "3.00.31.001"
        NumVersion = "3.00.31.002"
        If Not CodigoDeActualizacion32200_01_3101_A32200_01_3102 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.31.002"
        NumVersion = "3.00.31.003"
        If Not CodigoDeActualizacion32200_01_3102_A32200_01_3103 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.31.003"
        NumVersion = "3.00.31.004"
        If Not CodigoDeActualizacion32200_01_3103_A32200_01_3104 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.31.004"
        NumVersion = "3.00.31.005"
        If Not CodigoDeActualizacion32200_01_3104_A32200_01_3105 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.31.005"
        NumVersion = "3.00.31.006"
        If Not CodigoDeActualizacion32200_01_3105_A32200_01_3106 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.31.006"
        NumVersion = "3.00.31.007"
        If Not CodigoDeActualizacion32200_01_3106_A32200_01_3107 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.31.007"
        NumVersion = "3.00.31.008"
        If Not CodigoDeActualizacion32200_01_3107_A32200_01_3108 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.31.008", "3.00.31.009", "3.00.31.010", "3.00.31.011", "3.00.31.012", "3.00.31.013", "3.00.31.014", "3.00.31.015", "3.00.31.016", "3.00.31.017", _
        "3.00.31.018", "3.00.31.019", "3.00.31.020"
        NumVersion = "3.00.32.001"
        If Not CodigoDeActualizacion32200_02_3200_A32200_02_3201 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.32.001"
        NumVersion = "3.00.32.002"
        If Not CodigoDeActualizacion32200_02_3201_A32200_02_3202 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.32.002"
        NumVersion = "3.00.32.003"
        If Not CodigoDeActualizacion32200_02_3202_A32200_02_3203 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.32.003"
        NumVersion = "3.00.32.004"
        If Not CodigoDeActualizacion32200_02_3203_A32200_02_3204 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.32.004"
        NumVersion = "3.00.32.005"
        If Not CodigoDeActualizacion32200_02_3204_A32200_02_3205 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.32.005"
        NumVersion = "3.00.32.006"
        If Not CodigoDeActualizacion32200_02_3205_A32200_02_3206 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.32.006"
        NumVersion = "3.00.32.007"
        If Not CodigoDeActualizacion32200_02_3206_A32200_02_3207 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.32.007"
        NumVersion = "3.00.32.008"
        If Not CodigoDeActualizacion32200_02_3207_A32200_02_3208 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.32.008"
        NumVersion = "3.00.32.009"
        If Not CodigoDeActualizacion32200_02_3208_A32200_02_3209 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.32.009", "3.00.32.010", "3.00.32.011", "3.00.32.012", "3.00.32.013", "3.00.32.014", "3.00.32.015", "3.00.32.016"
        NumVersion = "3.00.33.001"
        If Not CodigoDeActualizacion32200_03_3300_A32200_03_3301 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.33.001"
        NumVersion = "3.00.33.002"
        If Not CodigoDeActualizacion3_3_3301_A3_3_3302 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.33.002"
        NumVersion = "3.00.33.003"
        If Not CodigoDeActualizacion3_3_3302_A3_3_3303 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.33.003"
        NumVersion = "3.00.33.004"
        If Not CodigoDeActualizacion3_3_3303_A3_3_3304 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.33.004"
        NumVersion = "3.00.33.005"
        If Not CodigoDeActualizacion3_3_3304_A3_3_3305 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.33.005", "3.00.33.006"
        NumVersion = "3.00.34.001"
        If Not CodigoDeActualizacion3_3_3400_A3_3_3401 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.001"
        NumVersion = "3.00.34.002"
        If Not CodigoDeActualizacion3_3_3401_A3_3_3402 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.002"
        NumVersion = "3.00.34.003"
        If Not CodigoDeActualizacion3_3_3402_A3_3_3403 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.003"
        NumVersion = "3.00.34.004"
        If Not CodigoDeActualizacion3_3_3403_A3_3_3404 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.004"
        NumVersion = "3.00.34.005"
        If Not CodigoDeActualizacion3_3_3404_A3_3_3405 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.005"
        NumVersion = "3.00.34.006"
        If Not CodigoDeActualizacion3_3_3405_A3_3_3406 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.006"
        NumVersion = "3.00.34.007"
        If Not CodigoDeActualizacion3_3_3406_A3_3_3407 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.007"
        NumVersion = "3.00.34.008"
        If Not CodigoDeActualizacion3_3_3407_A3_3_3408 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.008"
        NumVersion = "3.00.34.009"
        If Not CodigoDeActualizacion3_3_3408_A3_3_3409 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.009"
        NumVersion = "3.00.34.010"
        If Not CodigoDeActualizacion3_3_3409_A3_3_3410 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.010"
        NumVersion = "3.00.34.011"
        If Not CodigoDeActualizacion3_3_3410_A3_3_3411 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.011"
        NumVersion = "3.00.34.012"
        If Not CodigoDeActualizacion3_3_3411_A3_3_3412 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.012"
        NumVersion = "3.00.34.013"
        If Not CodigoDeActualizacion3_3_3412_A3_3_3413 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.013"
        NumVersion = "3.00.34.014"
        If Not CodigoDeActualizacion3_3_3413_A3_3_3414 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.014"
        NumVersion = "3.00.34.015"
        If Not CodigoDeActualizacion3_3_3414_A3_3_3415 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.015"
        NumVersion = "3.00.34.016"
        If Not CodigoDeActualizacion3_3_3415_A3_3_3416 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.016"
        NumVersion = "3.00.34.017"
        If Not CodigoDeActualizacion3_3_3416_A3_3_3417 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.017"
        NumVersion = "3.00.34.018"
        If Not CodigoDeActualizacion3_3_3417_A3_3_3418 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.34.018", "3.00.34.019", "3.00.34.020", "3.00.34.021", "3.00.34.022", "3.00.34.023", "3.00.34.024", "3.00.34.025", "3.00.34.026", "3.00.34.027", "3.00.34.028", "3.00.34.029"
        NumVersion = "3.00.35.001"
        If Not CodigoDeActualizacion3_3_3500_A3_3_3501 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.35.001"
        NumVersion = "3.00.35.002"
        If Not CodigoDeActualizacion3_3_3501_A3_3_3502 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.35.002"
        NumVersion = "3.00.35.003"
        If Not CodigoDeActualizacion3_3_3502_A3_3_3503 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.35.003"
        NumVersion = "3.00.35.004"
        If Not CodigoDeActualizacion3_3_3503_A3_3_3504 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.35.004"
        NumVersion = "3.00.35.005"
        If Not CodigoDeActualizacion3_3_3504_A3_3_3505 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.35.005"
        NumVersion = "3.00.35.006"
        If Not CodigoDeActualizacion3_3_3505_A3_3_3506 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.35.006"
        NumVersion = "3.00.35.007"
        If Not CodigoDeActualizacion3_3_3506_A3_3_3507 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
     Case "3.00.35.007"
        NumVersion = "3.00.35.008"
        If Not CodigoDeActualizacion3_3_3507_A3_3_3508 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.35.008"
        NumVersion = "3.00.36.001"
        If Not CodigoDeActualizacion3_3_3600_A3_3_3601 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.001"
        NumVersion = "3.00.36.002"
        If Not CodigoDeActualizacion3_3_3601_A3_3_3602 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.002"
        NumVersion = "3.00.36.003"
        If Not CodigoDeActualizacion3_3_3602_A3_3_3603 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.003"
        NumVersion = "3.00.36.004"
        If Not CodigoDeActualizacion3_3_3603_A3_3_3604 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.004"
        NumVersion = "3.00.36.005"
        If Not CodigoDeActualizacion3_3_3604_A3_3_3605 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.005"
        NumVersion = "3.00.36.006"
        If Not CodigoDeActualizacion3_3_3605_A3_3_3606 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.006"
        NumVersion = "3.00.36.007"
        If Not CodigoDeActualizacion3_3_3606_A3_3_3607 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.007"
        NumVersion = "3.00.36.008"
        If Not CodigoDeActualizacion3_3_3607_A3_3_3608 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.008"
        NumVersion = "3.00.36.009"
        If Not CodigoDeActualizacion3_3_3608_A3_3_3609 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.009"
        NumVersion = "3.00.36.010"
        If Not CodigoDeActualizacion3_3_3609_A3_3_3610 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.010"
        NumVersion = "3.00.36.011"
        If Not CodigoDeActualizacion3_3_3610_A3_3_3611 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.011"
        NumVersion = "3.00.36.012"
        If Not CodigoDeActualizacion3_3_3611_A3_3_3612 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.012"
        NumVersion = "3.00.36.013"
        If Not CodigoDeActualizacion3_3_3612_A3_3_3613 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.013"
        NumVersion = "3.00.36.014"
        If Not CodigoDeActualizacion3_3_3613_A3_3_3614 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
     Case "3.00.36.014"
        NumVersion = "3.00.36.015"
        If Not CodigoDeActualizacion3_3_3614_A3_3_3615 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.015"
        NumVersion = "3.00.36.016"
        If Not CodigoDeActualizacion3_3_3615_A3_3_3616 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.016"
        NumVersion = "3.00.36.017"
        If Not CodigoDeActualizacion3_3_3616_A3_3_3617 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.017"
        NumVersion = "3.00.36.018"
        If Not CodigoDeActualizacion3_3_3617_A3_3_3618 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.018", "3.00.36.019"
        NumVersion = "3.00.36.030"
        If Not CodigoDeActualizacion3_3_3618_A3_3_3630 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.030"
        NumVersion = "3.00.36.031"
        If Not CodigoDeActualizacion3_3_3630_A3_3_3631 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.031"
        NumVersion = "3.00.36.032"
        If Not CodigoDeActualizacion3_3_3631_A3_3_3632 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.032"
        NumVersion = "3.00.36.033"
        If Not CodigoDeActualizacion3_3_3632_A3_3_3633 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.033"
        NumVersion = "3.00.36.034"
        If Not CodigoDeActualizacion3_3_3633_A3_3_3634 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.034"
        NumVersion = "3.00.36.035"
        If Not CodigoDeActualizacion3_3_3634_A3_3_3635 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.035"
        NumVersion = "3.00.36.036"
        If Not CodigoDeActualizacion3_3_3635_A3_3_3636 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.036"
        NumVersion = "3.00.36.037"
        If Not CodigoDeActualizacion3_3_3636_A3_3_3637 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.037"
        NumVersion = "3.00.36.038"
        If Not CodigoDeActualizacion3_3_3637_A3_3_3638 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.038"
        NumVersion = "3.00.36.039"
        If Not CodigoDeActualizacion3_3_3638_A3_3_3639 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.039"
        NumVersion = "3.00.36.040"
        If Not CodigoDeActualizacion3_3_3639_A3_3_3640 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.040"
        NumVersion = "3.00.36.041"
        If Not CodigoDeActualizacion3_3_3640_A3_3_3641 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.041"
        NumVersion = "3.00.36.042"
        If Not CodigoDeActualizacion3_3_3641_A3_3_3642 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.042"
        NumVersion = "3.00.36.043"
        If Not CodigoDeActualizacion3_3_3642_A3_3_3643 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.043"
        NumVersion = "3.00.36.044"
        If Not CodigoDeActualizacion3_3_3643_A3_3_3644 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.044"
        NumVersion = "3.00.36.045"
        If Not CodigoDeActualizacion3_3_3644_A3_3_3645 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.045"
        NumVersion = "3.00.36.046"
        If Not CodigoDeActualizacion3_3_3645_A3_3_3646 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.046"
        NumVersion = "3.00.36.050"
        If Not CodigoDeActualizacion3_3_3646_A3_3_3650 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.050"
        NumVersion = "3.00.36.051"
        If Not CodigoDeActualizacion3_3_3650_A3_3_3651 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.051"
        NumVersion = "3.00.36.052"
        If Not CodigoDeActualizacion3_3_3651_A3_3_3652 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.052"
        NumVersion = "3.00.36.053"
        If Not CodigoDeActualizacion3_3_3652_A3_3_3653 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.053"
        NumVersion = "3.00.36.054"
        If Not CodigoDeActualizacion3_3_3653_A3_3_3654 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.053", "3.00.36.054", "3.00.36.055", "3.00.36.056", "3.00.36.057"
        NumVersion = "3.00.36.060"
        If Not CodigoDeActualizacion3_3_3653_A3_3_3660 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.060"
        NumVersion = "3.00.36.061"
        If Not CodigoDeActualizacion3_3_3660_A3_3_3661 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.061"
        NumVersion = "3.00.36.062"
        If Not CodigoDeActualizacion3_3_3661_A3_3_3662 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.062"
        NumVersion = "3.00.36.063"
        If Not CodigoDeActualizacion3_3_3662_A3_3_3663 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.063"
        NumVersion = "3.00.36.064"
        If Not CodigoDeActualizacion3_3_3663_A3_3_3664 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.36.064"
        NumVersion = "3.00.37.001"
        If Not CodigoDeActualizacion3_3_3700_A3_3_3701 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.001"
        NumVersion = "3.00.37.002"
        If Not CodigoDeActualizacion3_3_3701_A3_3_3702 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.002"
        NumVersion = "3.00.37.003"
        If Not CodigoDeActualizacion3_3_3702_A3_3_3703 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.003"
        NumVersion = "3.00.37.004"
        If Not CodigoDeActualizacion3_3_3703_A3_3_3704 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.004"
        NumVersion = "3.00.37.005"
        If Not CodigoDeActualizacion3_3_3704_A3_3_3705 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.005"
        NumVersion = "3.00.37.006"
        If Not CodigoDeActualizacion3_3_3705_A3_3_3706 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.006"
        NumVersion = "3.00.37.007"
        If Not CodigoDeActualizacion3_3_3706_A3_3_3707 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.007"
        NumVersion = "3.00.37.008"
        If Not CodigoDeActualizacion3_3_3707_A3_3_3708 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.008"
        NumVersion = "3.00.37.009"
        If Not CodigoDeActualizacion3_3_3708_A3_3_3709 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.009"
        NumVersion = "3.00.37.010"
        If Not CodigoDeActualizacion3_3_3709_A3_3_3710 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.010"
        NumVersion = "3.00.37.011"
        If Not CodigoDeActualizacion3_3_3710_A3_3_3711 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.011"
        NumVersion = "3.00.37.012"
        If Not CodigoDeActualizacion3_3_3711_A3_3_3712 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.012"
        NumVersion = "3.00.37.013"
        If Not CodigoDeActualizacion3_3_3712_A3_3_3713 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    Case "3.00.37.013"
        NumVersion = "3.00.37.014"
        If Not CodigoDeActualizacion3_3_3713_A3_3_3714 Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
        If Not Actualizar_2(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versión " & AVersion & vbLf & "Ultima actualización:" & deVersion
            Actualizar_2 = False
            Exit Function
        End If
    End Select
        
    Actualizar_2 = True
End Function
