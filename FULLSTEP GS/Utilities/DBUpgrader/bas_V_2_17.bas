Attribute VB_Name = "bas_V_2_17"
Option Explicit
Private oADOConnection As ADODB.Connection

Public Function CodigoDeActualizacion2_16_05_11A2_17_00_00() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificar tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_17_Tablas_0

frmProgreso.lblDetalle = "Modificar triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_17_Triggers_0


frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_17_Storeds_0


sConsulta = "UPDATE VERSION SET NUM ='2.17.00.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_05_11A2_17_00_00 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_11A2_17_00_00 = False

End Function

Private Sub V_2_17_Tablas_0()
    Dim sConsulta As String
    'LOG_PRES_ART
sConsulta = "CREATE TABLE [dbo].[LOG_PRES_ART] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ACCION] [varchar] (1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [COD] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ORIGEN] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [USU] [varchar] (" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE TRIGGER TG_LOG_PRES_ART_INS ON dbo.LOG_PRES_ART" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO LOG_GRAL" & vbCrLf
sConsulta = sConsulta & "       (ID_TABLA, TABLA, ORIGEN,FECACT)" & vbCrLf
sConsulta = sConsulta & "       SELECT L.ID,16, L.ORIGEN,CASE WHEN L.FECACT IS NULL THEN GETDATE() ELSE L.FECACT END" & vbCrLf
sConsulta = sConsulta & "       FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN LOG_PRES_ART L ON L.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "    UPDATE LOG_PRES_ART " & vbCrLf
sConsulta = sConsulta & "        SET FECACT = CASE WHEN L.FECACT IS NULL THEN GETDATE() ELSE L.FECACT END" & vbCrLf
sConsulta = sConsulta & "        FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN LOG_PRES_ART L ON L.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    'PARGEN_PED
sConsulta = "CREATE TABLE [dbo].[PARGEN_PED] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ELIMLINCATALOG] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBLHOMPEDIDOS] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBLCODPEDIDO] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAT_ADJESP_ART] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAT_ADJESP_PROVE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBLHOMPEDDIR] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBL_DIST_PEDIR] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [MAX_ADJUN_PED] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PEDPRES1] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PEDPRES2] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PEDPRES3] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PEDPRES4] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEFPRESITEM] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [SINCPRESITEM] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBLPRES1] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBLPRES2] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBLPRES3] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBLPRES4] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NIVPRES1] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NIVPRES2] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NIVPRES3] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NIVPRES4] [tinyint] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[PARGEN_PED] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PARGEN_PED] UNIQUE  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[PARGEN_PED] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_ELIMLINCATALOG] DEFAULT (0) FOR [ELIMLINCATALOG]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_OBLHOMPEDIDOS] DEFAULT (0) FOR [OBLHOMPEDIDOS]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_OBLCODPEDIDO] DEFAULT (0) FOR [OBLCODPEDIDO]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_CAT_ADJESP_ART] DEFAULT (1) FOR [CAT_ADJESP_ART]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_CAT_ADJESP_PROVE] DEFAULT (1) FOR [CAT_ADJESP_PROVE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_OBLHOMPEDDIR] DEFAULT (0) FOR [OBLHOMPEDDIR]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_OBLDISTCOMPRAPEDIR] DEFAULT (0) FOR [OBL_DIST_PEDIR]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_PEDPRES1] DEFAULT (0) FOR [PEDPRES1]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_PEDPRES2] DEFAULT (0) FOR [PEDPRES2]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_PEDPRES3] DEFAULT (0) FOR [PEDPRES3]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_PEDPRES4] DEFAULT (0) FOR [PEDPRES4]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_DEFPRESITEM] DEFAULT (0) FOR [DEFPRESITEM]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_SINCPRESITEM] DEFAULT (0) FOR [SINCPRESITEM]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_OBLPRES1] DEFAULT (0) FOR [OBLPRES1]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_OBLPRES2] DEFAULT (0) FOR [OBLPRES2]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_OBLPRES3] DEFAULT (0) FOR [OBLPRES3]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_OBLPRES4] DEFAULT (0) FOR [OBLPRES4]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_NIVPRES1] DEFAULT (0) FOR [NIVPRES1]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_NIVPRES2] DEFAULT (0) FOR [NIVPRES2]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_NIVPRES3] DEFAULT (0) FOR [NIVPRES3]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_PED_NIVPRES4] DEFAULT (0) FOR [NIVPRES4]" & vbCrLf


sConsulta = "INSERT INTO  PARGEN_PED (ID,ELIMLINCATALOG,OBLHOMPEDIDOS,OBLCODPEDIDO,CAT_ADJESP_ART,CAT_ADJESP_PROVE,OBLHOMPEDDIR,OBL_DIST_PEDIR,MAX_ADJUN_PED,PEDPRES1,PEDPRES2,PEDPRES3,PEDPRES4,DEFPRESITEM,SINCPRESITEM,OBLPRES1,OBLPRES2,OBLPRES3,OBLPRES4,NIVPRES1,NIVPRES2,NIVPRES3,NIVPRES4)" & vbCrLf
sConsulta = sConsulta & "SELECT ID,ELIMLINCATALOG,OBLHOMPEDIDOS,OBLCODPEDIDO,CAT_ADJESP_ART,CAT_ADJESP_PROVE,OBLHOMPEDDIR,OBL_DIST_PEDIR,NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM PARGEN_GEST" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "UPDATE PARGEN_PED SET MAX_ADJUN_PED=PD.MAX_ADJUN_PED FROM PARGEN_PED PP INNER JOIN PARGEN_DEF PD ON PP.ID=PD.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "UPDATE PARGEN_PED SET OBLCODPEDIDO=0 WHERE OBLCODPEDIDO=2"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE PARGEN_GEST DROP " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_ELIMLINCATALOG] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLHOMPEDIDOS] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLCODPEDIDO] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CAT_ADJESP_ART] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CAT_ADJESP_PROVE] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLHOMPEDDIR] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLDISTCOMPRAPEDIR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE PARGEN_GEST DROP " & vbCrLf
sConsulta = sConsulta & "COLUMN ELIMLINCATALOG," & vbCrLf
sConsulta = sConsulta & "COLUMN OBLHOMPEDIDOS," & vbCrLf
sConsulta = sConsulta & "COLUMN  OBLCODPEDIDO," & vbCrLf
sConsulta = sConsulta & "COLUMN CAT_ADJESP_ART," & vbCrLf
sConsulta = sConsulta & "COLUMN CAT_ADJESP_PROVE," & vbCrLf
sConsulta = sConsulta & "COLUMN OBLHOMPEDDIR," & vbCrLf
sConsulta = sConsulta & "COLUMN OBL_DIST_PEDIR" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE PARGEN_DEF DROP COLUMN MAX_ADJUN_PED" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    'PARGEN_DEF y USU
sConsulta = "  ALTER TABLE [dbo].[PARGEN_DEF] ADD" & vbCrLf
sConsulta = sConsulta & "     [TSRUTALOCAL] [varchar] (255) NULL," & vbCrLf
sConsulta = sConsulta & "     [TSRUTAUNIDAD] [varchar] (255) NULL,"
sConsulta = sConsulta & "     [TSUNIDAD] [varchar] (1) NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[USU] ADD" & vbCrLf
sConsulta = sConsulta & "     [TSRUTALOCAL] [varchar] (255) NULL," & vbCrLf
sConsulta = sConsulta & "     [TSRUTAUNIDAD] [varchar] (255) NULL,"
sConsulta = sConsulta & "     [TSUNIDAD] [varchar] (1) NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE PARGEN_DEF SET TSRUTAUNIDAD='\\tsclient\C',TSUNIDAD='Z'"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE USU SET TSRUTAUNIDAD='\\tsclient\C',TSUNIDAD='Z'"
ExecuteSQL gRDOCon, sConsulta

    'ACC_IDIOMA
sConsulta = "  ALTER TABLE [dbo].[ACC_IDIOMA] ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "     [DEN] [varchar] (200) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    'LINEAS_PEDIDO_PRESx
sConsulta = "CREATE TABLE [dbo].[LINEAS_PEDIDO_PRES1] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PORCEN] [float] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE TABLE [dbo].[LINEAS_PEDIDO_PRES2] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PORCEN] [float] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE TABLE [dbo].[LINEAS_PEDIDO_PRES3] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PORCEN] [float] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE TABLE [dbo].[LINEAS_PEDIDO_PRES4] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PORCEN] [float] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LINEAS_PEDIDO_PRES1] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_LINEAS_PEDIDO_PRES1] CHECK (((not([PRES1] is null))) and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and ((not([PRES2] is null))) and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and ((not([PRES3] is null))) and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and ((not([PRES4] is null))))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LINEAS_PEDIDO_PRES2] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_LINEAS_PEDIDO_PRES2] CHECK (((not([PRES1] is null))) and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and ((not([PRES2] is null))) and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and ((not([PRES3] is null))) and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and ((not([PRES4] is null))))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LINEAS_PEDIDO_PRES3] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_LINEAS_PEDIDO_PRES3] CHECK (((not([PRES1] is null))) and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and ((not([PRES2] is null))) and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and ((not([PRES3] is null))) and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and ((not([PRES4] is null))))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LINEAS_PEDIDO_PRES4] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_LINEAS_PEDIDO_PRES4] CHECK (((not([PRES1] is null))) and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and ((not([PRES2] is null))) and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and ((not([PRES3] is null))) and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and ((not([PRES4] is null))))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    'LOG_LINEAS_PEDIDO_PRESx
sConsulta = "CREATE TABLE [dbo].[LOG_PEDIDO_PRES1] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LOG_ORDEN_ENTREGA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LOG_LINEAS_PEDIDO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PORCEN] [float] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE TABLE [dbo].[LOG_PEDIDO_PRES2] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LOG_ORDEN_ENTREGA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LOG_LINEAS_PEDIDO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PORCEN] [float] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE TABLE [dbo].[LOG_PEDIDO_PRES3] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LOG_ORDEN_ENTREGA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LOG_LINEAS_PEDIDO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PORCEN] [float] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE TABLE [dbo].[LOG_PEDIDO_PRES4] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LOG_ORDEN_ENTREGA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LOG_LINEAS_PEDIDO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PORCEN] [float] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES1] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_LOG_PEDIDO_PRES1] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES2] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_LOG_PEDIDO_PRES2] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES3] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_LOG_PEDIDO_PRES3] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES4] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_LOG_PEDIDO_PRES4] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES1] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_LOG_PEDIDO_PRES1] CHECK (((not([PRES1] is null))) and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and ((not([PRES2] is null))) and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and ((not([PRES3] is null))) and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and ((not([PRES4] is null))))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES2] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_LOG_PEDIDO_PRES2] CHECK (((not([PRES1] is null))) and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and ((not([PRES2] is null))) and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and ((not([PRES3] is null))) and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and ((not([PRES4] is null))))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES3] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_LOG_PEDIDO_PRES3] CHECK (((not([PRES1] is null))) and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and ((not([PRES2] is null))) and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and ((not([PRES3] is null))) and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and ((not([PRES4] is null))))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES4] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_LOG_PEDIDO_PRES4] CHECK (((not([PRES1] is null))) and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and ((not([PRES2] is null))) and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and ((not([PRES3] is null))) and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and ((not([PRES4] is null))))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
 sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES1] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LOG_PEDIDO_PRES1_LOG_LINEAS_PEDIDO] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID_LOG_LINEAS_PEDIDO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[LOG_LINEAS_PEDIDO] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LOG_PEDIDO_PRES1_LOG_ORDEN_ENTREGA] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID_LOG_ORDEN_ENTREGA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[LOG_ORDEN_ENTREGA] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES2] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LOG_PEDIDO_PRES2_LOG_LINEAS_PEDIDO] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID_LOG_LINEAS_PEDIDO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[LOG_LINEAS_PEDIDO] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LOG_PEDIDO_PRES2_LOG_ORDEN_ENTREGA] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID_LOG_ORDEN_ENTREGA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[LOG_ORDEN_ENTREGA] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES3] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LOG_PEDIDO_PRES3_LOG_LINEAS_PEDIDO] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID_LOG_LINEAS_PEDIDO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[LOG_LINEAS_PEDIDO] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LOG_PEDIDO_PRES3_LOG_ORDEN_ENTREGA] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID_LOG_ORDEN_ENTREGA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[LOG_ORDEN_ENTREGA] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[LOG_PEDIDO_PRES4] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LOG_PEDIDO_PRES4_LOG_LINEAS_PEDIDO] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID_LOG_LINEAS_PEDIDO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[LOG_LINEAS_PEDIDO] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LOG_PEDIDO_PRES4_LOG_ORDEN_ENTREGA] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID_LOG_ORDEN_ENTREGA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[LOG_ORDEN_ENTREGA] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_ENTREGA_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_ENTREGA_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ORDEN_ENTREGA_TG_UPD ON dbo.ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEAID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESPUB AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_PED AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_ADJ AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT1 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT2 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT3 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT4 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT5 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEG AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT_LIN_ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_ENTREGA_Upd CURSOR FOR SELECT ID,EST,TIPO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_ENTREGA_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_ENTREGA_Upd INTO @ID,@EST,@TIPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(EST) " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "  --Al emitir actualizar la cantidad pedida en ITEM_ADJ y despublicar las lineas de cat�logo si se ha llegado a la cantidad adjudicada" & vbCrLf
sConsulta = sConsulta & "   IF @EST=2 " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @DESPUB=(SELECT ELIMLINCATALOG FROM PARGEN_PED)" & vbCrLf
sConsulta = sConsulta & "       DECLARE cur_LineasOrden CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,PROVE,ID,CANT_PED*FC  FROM LINEAS_PEDIDO WHERE ORDEN=@ID AND EST=1 AND ITEM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       OPEN cur_LineasOrden" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM cur_LineasOrden INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@LINEAID,@CANT" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM_ADJ SET CANT_PED=CANT_PED + @CANT WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SELECT  @CANT_PED= CANT_PED, @CANT_ADJ=CANT_ADJ FROM ITEM_ADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "           IF @DESPUB=1 AND (@CANT_ADJ<=@CANT_PED)" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "               SELECT @CAT_LIN_ID = ID   FROM CATALOG_LIN  " & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO=@ANYO     AND GMN1=@GMN1    AND PROCE=@PROCE   AND ITEM=@ITEM    AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "               DELETE FROM CAT_LIN_MIN    WHERE LINEA=@CAT_LIN_ID " & vbCrLf
sConsulta = sConsulta & "               DELETE FROM CATALOG_LIN_ESP    WHERE LINEA=@CAT_LIN_ID " & vbCrLf
sConsulta = sConsulta & "               DELETE FROM CATALOG_LIN   WHERE ID=@CAT_LIN_ID " & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM cur_LineasOrden INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@LINEAID,@CANT" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       Close cur_LineasOrden" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE cur_LineasOrden" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   --Dar baja logica a las categorias marcadas despues de que se cierran todos los pedidos " & vbCrLf
sConsulta = sConsulta & "   IF @TIPO=1 AND (@EST >=  6) " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE cur_LineasOrden1 CURSOR FOR SELECT DISTINCT CAT1,CAT2,CAT3,CAT4,CAT5 FROM LINEAS_PEDIDO WHERE LINEAS_PEDIDO.ORDEN=@ID " & vbCrLf
sConsulta = sConsulta & "     OPEN cur_LineasOrden1" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM cur_LineasOrden1 INTO @CAT1,@CAT2,@CAT3,@CAT4,@CAT5" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @CAT5 IS NOT NULL AND (SELECT BAJALOG FROM CATN5 WHERE ID=@CAT5) = 1" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT5=@CAT5 AND ORDEN_ENTREGA.EST<6)=0 " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "             UPDATE CATN5 SET BAJALOG=2 WHERE ID=@CAT5   " & vbCrLf
sConsulta = sConsulta & "             SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "             SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT5 AND NIVEL=5" & vbCrLf
sConsulta = sConsulta & "             IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG              " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "       IF @CAT4 IS NOT NULL AND (SELECT BAJALOG FROM CATN4 WHERE ID=@CAT4) = 1" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT4=@CAT4 AND ORDEN_ENTREGA.EST<6)=0 " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE CATN4 SET BAJALOG=2 WHERE ID=@CAT4" & vbCrLf
sConsulta = sConsulta & "              SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "              SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT4 AND NIVEL=4" & vbCrLf
sConsulta = sConsulta & "              IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "       IF @CAT3 IS NOT NULL AND (SELECT BAJALOG FROM CATN3 WHERE ID=@CAT3) = 1" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT3=@CAT3 AND ORDEN_ENTREGA.EST<6)=0 " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE CATN3 SET BAJALOG=2 WHERE ID=@CAT3" & vbCrLf
sConsulta = sConsulta & "               SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "               SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT3 AND NIVEL=3" & vbCrLf
sConsulta = sConsulta & "               IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       IF @CAT2 IS NOT NULL AND (SELECT BAJALOG FROM CATN2 WHERE ID=@CAT2) = 1 " & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT2=@CAT2 AND ORDEN_ENTREGA.EST<6)=0 " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE CATN2 SET BAJALOG=2 WHERE ID=@CAT2" & vbCrLf
sConsulta = sConsulta & "               SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "               SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT2 AND NIVEL=2" & vbCrLf
sConsulta = sConsulta & "               IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       IF @CAT1 IS NOT NULL AND (SELECT BAJALOG FROM CATN1 WHERE ID=@CAT1) = 1" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT1=@CAT1 AND ORDEN_ENTREGA.EST<6)=0 " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "              UPDATE CATN1 SET BAJALOG=2 WHERE ID=@CAT1" & vbCrLf
sConsulta = sConsulta & "              SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "              SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT1 AND NIVEL=1" & vbCrLf
sConsulta = sConsulta & "              IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM cur_LineasOrden1 INTO @CAT1,@CAT2,@CAT3,@CAT4,@CAT5" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "     Close cur_LineasOrden1" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE cur_LineasOrden1" & vbCrLf
sConsulta = sConsulta & "   END " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_ENTREGA_Upd INTO @ID,@EST,@TIPO" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "Close curTG_ORDEN_ENTREGA_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_ENTREGA_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "UPDATE ORDEN_ENTREGA SET IMPORTE=IMPORTE / CAMBIO"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE PROCE SET W=(SELECT CASE WHEN COUNT(DISTINCT PROVE)=0 THEN NULL ELSE COUNT(DISTINCT PROVE) END FROM PROCE_OFE WHERE PROCE_OFE.ANYO=PROCE.ANYO AND PROCE_OFE.GMN1=PROCE.GMN1  AND PROCE_OFE.PROCE=PROCE.COD AND PROCE_OFE.ENVIADA=1 AND PROCE_OFE.PORTAL=1) WHERE W <>0"
ExecuteSQL gRDOCon, sConsulta


End Sub
Private Sub V_2_17_Triggers_0()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_OFE_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_OFE_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_OFE_TG_DEL ON dbo.PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PORTAL AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXOFE AS INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO AS FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE2 AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @OFE2 AS SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @IDENT AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIOMONCEN FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFE AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_OFE_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE,PORTAL FROM DELETED " & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE ,@PORTAL" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & " IF (SELECT SUBASTA FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE) = 1 " & vbCrLf
sConsulta = sConsulta & " BEGIN " & vbCrLf
sConsulta = sConsulta & "     --Si existen precios en la oferta eliminada que eran m�nimas en la puja se deber�n recalcular los nuevos precios m�nimos " & vbCrLf
sConsulta = sConsulta & "         /*Obtiene todos los items para los cuales ese proveedor tiene la oferta m�nima*/ " & vbCrLf
sConsulta = sConsulta & "         DECLARE C CURSOR FOR SELECT ITEM FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         OPEN C " & vbCrLf
sConsulta = sConsulta & "         FETCH NEXT FROM C INTO @ITEM " & vbCrLf
sConsulta = sConsulta & "         WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "             SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVE2=A.PROVE,@OFE2=A.NUM" & vbCrLf
sConsulta = sConsulta & "                   FROM ITEM_OFE A INNER JOIN PROCE_OFE B ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE AND A.NUM=B.OFE" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN PROCE C ON A.ANYO=C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE = C.COD" & vbCrLf
sConsulta = sConsulta & "             WHERE A.ANYO=@ANYO AND A.GMN1=@GMN1 AND A.PROCE=@PROCE AND A.ITEM=@ITEM AND A.ULT=1 AND A.PRECIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC" & vbCrLf
sConsulta = sConsulta & "             IF @PRECIO IS NULL " & vbCrLf
sConsulta = sConsulta & "             BEGIN " & vbCrLf
sConsulta = sConsulta & "                 UPDATE OFEMIN SET PRECIO=NULL, PROVE=NULL, OFE=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND ITEM = @ITEM " & vbCrLf
sConsulta = sConsulta & "             End " & vbCrLf
sConsulta = sConsulta & "             Else " & vbCrLf
sConsulta = sConsulta & "             BEGIN " & vbCrLf
sConsulta = sConsulta & "                 /*Actualiza la tabla OFEMIN con la oferta m�nima para el proceso e item*/ " & vbCrLf
sConsulta = sConsulta & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2, ULTOFE=0 WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "             End " & vbCrLf
sConsulta = sConsulta & "             FETCH NEXT FROM C INTO @ITEM " & vbCrLf
sConsulta = sConsulta & "         End " & vbCrLf
sConsulta = sConsulta & "         Close C " & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "     EXEC SP_CALCULAR_DETALLE_PUJAS @ANYO, @GMN1, @PROCE" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "     SET @NUMOFE=(SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ENVIADA=1)" & vbCrLf
sConsulta = sConsulta & "     IF (@NUMOFE IS NULL ) " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "             UPDATE PROCE SET O=(O-1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "     IF @PORTAL=1 --Si la oferta eliminada era de portal puede que tenga que actualizar el campo W de PROCE" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "          SET @NUMOFE=NULL" & vbCrLf
sConsulta = sConsulta & "          SET @NUMOFE=(SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ENVIADA=1 AND PORTAL=1)" & vbCrLf
sConsulta = sConsulta & "          IF (@NUMOFE IS NULL )  " & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE SET W=(W-1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ENVIADA=1 AND OFE <> -1 ) " & vbCrLf
sConsulta = sConsulta & "     IF (@MAXOFE IS NULL ) " & vbCrLf
sConsulta = sConsulta & "     BEGIN " & vbCrLf
sConsulta = sConsulta & "     /* Si se han insertado ofertas, actualizamos el estado del proceso  si es que este era inferior a 7 */ " & vbCrLf
sConsulta = sConsulta & "         IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) = 7  /* 7= Con ofertas */ " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM PROCE_PROVE_PET WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) = 0" & vbCrLf
sConsulta = sConsulta & "               UPDATE PROCE SET EST = 5  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "               UPDATE PROCE SET EST = 6  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "         End " & vbCrLf
sConsulta = sConsulta & "     End " & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE ,@PORTAL" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TG_LOG_PRES_ART_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[TG_LOG_PRES_ART_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER TG_LOG_PRES_ART_INS ON dbo.LOG_PRES_ART" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO LOG_GRAL" & vbCrLf
sConsulta = sConsulta & "       (ID_TABLA, TABLA, ORIGEN,FECACT)" & vbCrLf
sConsulta = sConsulta & "       SELECT L.ID,16, L.ORIGEN,CASE WHEN L.FECACT IS NULL THEN GETDATE() ELSE L.FECACT END" & vbCrLf
sConsulta = sConsulta & "       FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN LOG_PRES_ART L ON L.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "    UPDATE LOG_PRES_ART " & vbCrLf
sConsulta = sConsulta & "        SET FECACT = CASE WHEN L.FECACT IS NULL THEN GETDATE() ELSE L.FECACT END" & vbCrLf
sConsulta = sConsulta & "        FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN LOG_PRES_ART L ON L.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_EST_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_EST_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ORDEN_EST_TG_INS ON dbo.ORDEN_EST " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_EST_Ins CURSOR LOCAL FOR SELECT EST,ID,ORDEN,PER,COMENT,EXTERNO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXTERNO AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVLOG AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORIGEN AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMENT AS VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_ORDEN_ENTREGA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAG AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTEGRACION AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACCION AS VARCHAR(2)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_PED_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOGRABAR AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADO_APROV_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_ERP AS VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @EXTERNO IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF (@EST = 2 OR @EST = 3 OR @EST = 4 OR @EST = 20 OR @EST = 21) " & vbCrLf
sConsulta = sConsulta & "      BEGIN -- Si es uno de los estados que nos interesan para grabar en LOG_ORDEN_ENTREGA (emitido,aceptado,rechazado,anulado,en camino)" & vbCrLf
sConsulta = sConsulta & "        -- Recuperamos el tipo de orden de entrega para ver si est� activada su integraci�n" & vbCrLf
sConsulta = sConsulta & "        SET @COD_ERP=NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT  @TIPO=TIPO,@PROVE=PROVE, @COD_ERP=COD_PROVE_ERP FROM ORDEN_ENTREGA WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "        SELECT  @ACTIVLOG = ACTIVLOG FROM PARGEN_GEST WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "        SELECT  @INTEGRACION =INTEGRACION, @TRASLADO_APROV_ERP=TRASLADO_APROV_ERP, @TRASPASO_PED_ERP= TRASPASO_PED_ERP FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @INTEGRACION = 0" & vbCrLf
sConsulta = sConsulta & "             SET @ACTIVA = 0" & vbCrLf
sConsulta = sConsulta & "        ELSE " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO = 0 --Pedidos directos" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 13" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 11" & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF (@ACTIVA = 1 AND (@SENTIDO=3 OR @SENTIDO=1)) OR @ACTIVLOG = 1 OR @TRASPASO_PED_ERP=1 OR @TRASLADO_APROV_ERP = 1" & vbCrLf
sConsulta = sConsulta & "          -- si el sentido es = 2  (solo entrada) o no est� activa la integraci�n  no grabamos en log" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SET @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "            IF (@TIPO=0 AND @TRASPASO_PED_ERP=1)  OR (@TIPO = 1 AND @TRASLADO_APROV_ERP = 1) --emisi�n de pedidos directos y de aprovisionamiento" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                 IF @TIPO = 0  and @TRASPASO_PED_ERP=1" & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                     SET @ERP=1" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVLOG = 1 -- log de actividad" & vbCrLf
sConsulta = sConsulta & "                         SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                         SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                 IF @TIPO = 1  and @TRASLADO_APROV_ERP=1" & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                     SET @ERP=1" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVLOG = 1 -- log de actividad" & vbCrLf
sConsulta = sConsulta & "                         SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                         SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                begin" & vbCrLf
sConsulta = sConsulta & "                SET @ERP=0" & vbCrLf
sConsulta = sConsulta & "                IF @ACTIVLOG = 1 " & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVA  = 1" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 2 -- Integracion y log de actividad" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 1 -- solo log de actividad" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                     BEGIN" & vbCrLf
sConsulta = sConsulta & "                      IF @ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "                          SET @ORIGEN = 0 -- solo integracion" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                          SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                end" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                SET @USU = NULL" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                SET @USU = (SELECT COD FROM USU WHERE PER = @PER)" & vbCrLf
sConsulta = sConsulta & "            SET @PAG = (SELECT PAG FROM PROVE WHERE COD = @PROVE)" & vbCrLf
sConsulta = sConsulta & "            -- la acci�n ser� E - Cambio de estado o I Creaci�n orden de entrega)" & vbCrLf
sConsulta = sConsulta & "            IF @EST = 2  " & vbCrLf
sConsulta = sConsulta & "              SET @ACCION='I'" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @EST=20 " & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='D'" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='E'" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA,OBS)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,@ORIGEN,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA,ORDEN_ENTREGA.OBS FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                -- En el caso de Emisi�n de Pedido Directo, todav�a no estar�n creadas las lineas de pedido, se hace en el trigger LINEAS_PEDIDO_TG_INS" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "      IF @ACCION='I' AND @ORIGEN<>1 AND @TIPO = 0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "                    -- Presupuestos" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES1 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES1 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID  INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES2 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES2 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID  INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES3 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES3 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID   INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES4 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES4 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID   INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            IF @ERP=1 " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA,OBS)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,0,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA,ORDEN_ENTREGA.OBS FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    -- Presupuestos" & vbCrLf
sConsulta = sConsulta & "                   IF @ACCION='I'  AND @TIPO = 0" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES1 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES1 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID   INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES2 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES2 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES3 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES3 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES4 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES4 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "          END --IF ACTIVA" & vbCrLf
sConsulta = sConsulta & "      END -- IF EST" & vbCrLf
sConsulta = sConsulta & "   END -- IF @EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  UPDATE ORDEN_ENTREGA SET ORDEN_EST = @ID WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_17_Storeds_0()

Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CADUCIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CADUCIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_CADUCIDAD AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INT,@ANYO AS INT,@GMN1 AS VARCHAR(3),@PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTWEB AS INT,@SERVER AS VARCHAR(50),@BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM AS INT,@NUMNUE AS INT,@NUMAREV AS INT,@CODPORTAL AS VARCHAR(20),@ID AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONEX  AS VARCHAR(100),@FSP_CIA AS VARCHAR(20),@CONEX2 AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA AS FLOAT,@APROV AS INT,@CERRAR AS INT" & vbCrLf
sConsulta = sConsulta & "declare @FECHA AS DATETIME" & vbCrLf
sConsulta = sConsulta & "declare @FECHAPUB AS DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECHAPUB=getdate()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECHA = CONVERT(DATETIME,CONVERT(VARCHAR,DAY(GETDATE())) + '/' + CONVERT(VARCHAR,MONTH(GETDATE())) + '/' + CONVERT(VARCHAR,YEAR(GETDATE())) ,103)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INSTWEB=(SELECT INSTWEB FROM PARGEN_INTERNO)" & vbCrLf
sConsulta = sConsulta & "IF @INSTWEB<>0 " & vbCrLf
sConsulta = sConsulta & "  IF @INSTWEB=2" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SERVER=(SELECT FSP_SRV FROM PARGEN_PORT)" & vbCrLf
sConsulta = sConsulta & "      SET @BD=(SELECT FSP_BD FROM PARGEN_PORT)" & vbCrLf
sConsulta = sConsulta & "      SET @FSP_CIA =(SELECT FSP_CIA FROM PARGEN_PORT WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "      SET @CONEX2= @SERVER + '.' + @BD + '.dbo.SP_SELECT_ID'" & vbCrLf
sConsulta = sConsulta & "      EXEC @ID = @CONEX2  @FSP_CIA" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR " & vbCrLf
sConsulta = sConsulta & " SELECT PROCE_PROVE.PROCE,PROCE_PROVE.ANYO,PROCE_PROVE.GMN1,PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "   FROM PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "                   ON PROCE_PROVE.PROCE= PROCE.COD " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE_PROVE.ANYO=PROCE.ANYO " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE_PROVE.GMN1=PROCE.GMN1 " & vbCrLf
sConsulta = sConsulta & "  WHERE PROCE_PROVE.PUB = 1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE.FECLIMOFE <= @FECHAPUB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @PROCE,@ANYO,@GMN1,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_PROVE SET PUB=0,FECPUB=NULL WHERE PROCE=@PROCE  AND ANYO= @ANYO AND GMN1= @GMN1 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "    IF  @INSTWEB=2" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @CODPORTAL=(SELECT FSP_COD FROM PROVE WHERE COD = @PROVE)" & vbCrLf
sConsulta = sConsulta & "        EXECUTE NUM_PROCE_PUB @PROVE,@NUM OUTPUT,@NUMNUE OUTPUT,@NUMAREV OUTPUT" & vbCrLf
sConsulta = sConsulta & "        IF @NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @NUM=0" & vbCrLf
sConsulta = sConsulta & "        IF @NUMNUE IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @NUMNUE=0" & vbCrLf
sConsulta = sConsulta & "        IF @NUMAREV IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @NUMAREV=0" & vbCrLf
sConsulta = sConsulta & "        SET @CONEX=@SERVER + '.' + @BD + '.dbo.SP_ACT_PROCE_PUB'" & vbCrLf
sConsulta = sConsulta & "        EXECUTE @CONEX @ID,@CODPORTAL,@NUM,@NUMNUE,@NUMAREV" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @PROCE,@ANYO,@GMN1,@PROVE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @APROV=(SELECT APROVISION FROM PARGEN_INTERNO) " & vbCrLf
sConsulta = sConsulta & "IF @APROV<>0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @CERRAR=1" & vbCrLf
sConsulta = sConsulta & "    DECLARE D CURSOR FOR SELECT ID FROM CATALOG_LIN  WHERE PUB = 1 AND FECHA_DESPUB <= @FECHA" & vbCrLf
sConsulta = sConsulta & "    OPEN D" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM D INTO @LINEA" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        UPDATE CATALOG_LIN SET PUB=0,FECHA_DESPUB=NULL WHERE ID=@LINEA" & vbCrLf
sConsulta = sConsulta & "        UPDATE FAVORITOS_ORDEN_ENTREGA " & vbCrLf
sConsulta = sConsulta & "               SET IMPORTE = (SELECT ISNULL(SUM(FLP.PREC_UP * FLP.CANT_PED),0) " & vbCrLf
sConsulta = sConsulta & "               FROM FAVORITOS_LINEAS_PEDIDO FLP WHERE  FAVORITOS_ORDEN_ENTREGA.ID = FLP.ORDEN " & vbCrLf
sConsulta = sConsulta & "               AND FLP.ORDEN IN (SELECT FLP2.ORDEN FROM FAVORITOS_LINEAS_PEDIDO FLP2 WHERE FLP2.LINEA = @LINEA) AND FLP.LINEA<> @LINEA) " & vbCrLf
sConsulta = sConsulta & "              FROM FAVORITOS_ORDEN_ENTREGA INNER JOIN FAVORITOS_LINEAS_PEDIDO FLP " & vbCrLf
sConsulta = sConsulta & "                  ON FAVORITOS_ORDEN_ENTREGA.ID=FLP.ORDEN AND FLP.ORDEN IN " & vbCrLf
sConsulta = sConsulta & "                  (SELECT FLP2.ORDEN FROM FAVORITOS_LINEAS_PEDIDO FLP2 WHERE FLP2.LINEA = @LINEA)" & vbCrLf
sConsulta = sConsulta & "         DELETE FROM FAVORITOS_LINEAS_PEDIDO_ADJUN FROM FAVORITOS_LINEAS_PEDIDO_ADJUN FA INNER JOIN FAVORITOS_LINEAS_PEDIDO FV ON FA.LINEA=FV.ID AND FA.USU=FV.USU WHERE FV.LINEA = @LINEA" & vbCrLf
sConsulta = sConsulta & "         DELETE FROM FAVORITOS_LINEAS_PEDIDO WHERE LINEA = @LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM D INTO @LINEA" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    CLOSE D" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE D" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_CALCULAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_CALCULAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_AR_CALCULAR AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INT,@ANYO AS INT,@GMN1 AS VARCHAR(3),@PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "declare @RES AS INT    " & vbCrLf
sConsulta = sConsulta & "declare @NumCal as int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @NumCal=0" & vbCrLf
sConsulta = sConsulta & "DECLARE C_Cal CURSOR LOCAL FOR  SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD   FROM PROCE" & vbCrLf
sConsulta = sConsulta & "  WHERE PROCE.CALCPEND = 1  AND EST >=11 AND EST <20" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C_Cal" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C_Cal INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     begin distributed tran" & vbCrLf
sConsulta = sConsulta & "     set xact_abort on" & vbCrLf
sConsulta = sConsulta & "     EXEC SP_AR_AHORROS_CALCULAR @ANYO,@GMN1,@PROCE,@RES OUTPUT" & vbCrLf
sConsulta = sConsulta & "     if @res =0 " & vbCrLf
sConsulta = sConsulta & "              begin" & vbCrLf
sConsulta = sConsulta & "             commit  tran" & vbCrLf
sConsulta = sConsulta & "             set @NumCal= @NumCal + 1" & vbCrLf
sConsulta = sConsulta & "             set xact_abort off" & vbCrLf
sConsulta = sConsulta & "             end" & vbCrLf
sConsulta = sConsulta & "     else" & vbCrLf
sConsulta = sConsulta & "             if @@trancount >0" & vbCrLf
sConsulta = sConsulta & "                 rollback  tran" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM C_Cal INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C_Cal " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C_Cal" & vbCrLf
sConsulta = sConsulta & "IF @NumCal >0 " & vbCrLf
sConsulta = sConsulta & "    UPDATE PARGEN_DEF SET FEC_ACT_AHORROS=GETDATE()" & vbCrLf
sConsulta = sConsulta & "set xact_abort off" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ORDENES @MAXLINEAS integer, @DESDEID varchar(50)=null, @APROBADOR varchar(50) = null,@APROVISIONADOR varchar(50) = null, @ARTINT varchar(50) = null, @ARTDEN varchar(500) = null, @ARTEXT varchar(500) = null, @DESDEFECHA varchar(10) = null, @HASTAFECHA varchar(10) = null, @PROVECOD varchar(50) = null, @PROVEDEN varchar(500) = null,@NUMPEDPROVE varchar(100) = null,@ANYO int = null,@PEDIDO int = null,@ORDEN int = null,@EST int  = null, @ID int = null ,@RECINCORRECTO int = null ,@COINCIDENCIA int =null,@REFERENCIA varchar(120) = null ,@CAMPO1 int = null ,@CAMPO2 int = null, @VALORCAMPO1 varchar(150) = null, @VALORCAMPO2 varchar(150)=null , @CATN1 int = null, @CATN2 int = null, @CATN3 int = null, @CATN4 int = null , @CATN5 int = null, @EMPRESA INT = NULL, @RECEPTOR VARCHAR(100) = NULL, @COD_ERP  VARCHAR(100) = NULL, @ENTIDAD VARCHAR(100) = 'ORDEN', @TIPO TINYINT = 1 AS " & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(1000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL1 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'PROVE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT DISTINCT P.COD, P.DEN , P.NIF, P.DIR, P.CP, P.PAI, P.POB, P.PROVI'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT DISTINCT E.ID, E.NIF, E.DEN, E.DIR, E.CP, E.POB, E.PROVI, E.PAI, PAI.DEN PAIDEN, PROVI.DEN PROVIDEN '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT TOP ' + CONVERT(varchar,@MAXLINEAS) + ' CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID), " & vbCrLf
sConsulta = sConsulta & "       OE.ID, OE.ANYO, PE.NUM NUMPEDIDO, OE.NUM NUMORDEN, OE.PROVE PROVECOD, P.DEN PROVEDEN, P.NIF," & vbCrLf
sConsulta = sConsulta & "       OE.NUMEXT, OE.FECHA FECHA, OE.EST EST, OE.IMPORTE IMPORTE, " & vbCrLf
sConsulta = sConsulta & "       P1.APE + '', '' + P1.NOM APROVISIONADOR,P1.APE,P1.NOM,OE.OBS," & vbCrLf
sConsulta = sConsulta & "       OES.COMENT, OES.FECHA FECEST,OE.IMPORTE,OE.PEDIDO,OE.TIPO, OE.INCORRECTA, PE.PER,OE.REFERENCIA,T3.ADJUNTOS, CP.COMENT COMENTPROVE, CP.EST ESTCOMENT,OE.MON, OE.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, OE.COD_PROVE_ERP, OE.RECEPTOR ,PER.NOM + '' '' + PER.APE NOMRECEPTOR, PER.TFNO TFNORECEPTOR, PER.EMAIL EMAILRECEPTOR, ISNULL(PER.UON3,ISNULL(PER.UON2,PER.UON1)) UONRECEPTOR, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) DENUONRECEPTOR, PE.TIPO, PR.FECHA FECHARECEP'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (EMP E LEFT JOIN PAI ON E.PAI = PAI.COD LEFT JOIN PROVI ON E.PAI = PROVI.PAI AND E.PROVI = PROVI.COD) ON OE.EMPRESA = E.ID'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN EMP E ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (PER LEFT JOIN UON1 ON PER.UON1 = UON1.COD LEFT JOIN UON2 ON PER.UON1 = UON2.UON1 AND PER.UON2 = UON2.COD LEFT JOIN UON3 ON PER.UON1 = UON3.UON1 AND PER.UON2 = UON3.UON2 AND PER.UON3 = UON3.COD) ON OE.RECEPTOR = PER.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT OE.ID, OE.PEDIDO, OE.ORDEN, OE.EST, OE.COMENT " & vbCrLf
sConsulta = sConsulta & "                  FROM ORDEN_EST OE " & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN (SELECT PEDIDO, ORDEN, MAX(ID) ID" & vbCrLf
sConsulta = sConsulta & "                                      FROM ORDEN_EST  WHERE EST IN (3, 21) " & vbCrLf
sConsulta = sConsulta & "                                    GROUP BY PEDIDO, ORDEN ) MO " & vbCrLf
sConsulta = sConsulta & "                                ON MO.ID = OE.ID) CP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = CP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID " & vbCrLf
sConsulta = sConsulta & "                                           AND LP.NIVEL_APROB = 0) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (SELECT COUNT(*) AS ADJUNTOS,ORDEN  FROM orden_entrega_adjun GROUP BY orden_entrega_adjun.ORDEN) T3 ON OE.ID= T3.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN (PEDIDO_RECEP PR INNER JOIN (SELECT PEDIDO, ORDEN, MAX(ID) ID FROM PEDIDO_RECEP GROUP BY PEDIDO,ORDEN)  PR2 ON PR.ID=PR2.ID )" & vbCrLf
sConsulta = sConsulta & "             ON PR.ORDEN= OE.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null or @CATN1 is not null or @CATN2 is not null or @CATN3 is not null or @CATN4 is not null or @CATN5 is not null or @CAMPO1 is not null or @CAMPO2 is not null or @VALORCAMPO1 is not null or @VALORCAMPO2 is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO1 = @CAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO2 = @CAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR1 = @VALORCAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR2 = @VALORCAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT1 = @CATN1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT2 = @CATN2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT3 = @CATN3 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT4 = @CATN4 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT5 = @CATN5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR LP.ART_INT + I.ART=@ARTINT)'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like + ''%'' + @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE =@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "   IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (PA.COD_EXT like ''% ''  + @ARTEXT + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like @ARTEXT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like ''% '' + @ARTEXT ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR PA.COD_EXT =@ARTEXT)'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like + ''%'' + @ARTEXT + ''%'''  " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 1            " & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @TIPO= 0" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=0'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.REFERENCIA = @REFERENCIA '" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.EMPRESA = @EMPRESA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.RECEPTOR = @RECEPTOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.COD_PROVE_ERP = @COD_ERP '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL1 = ''" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = ''" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "     set @SQL1=' OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "IF @RECINCORRECTO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "  IF @RECINCORRECTO=1" & vbCrLf
sConsulta = sConsulta & "   set @SQL2 = ' OE.INCORRECTA=1'  " & vbCrLf
sConsulta = sConsulta & "IF @SQL1<>'' and @SQL2<>'' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND (' + @SQL1 + ' OR ' + @SQL2 + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "if @SQL1!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL1 " & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "if @SQL2!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL2" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @ID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.ID=@ID '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "if @DESDEID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)>=@DESDEID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'PROVE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL =  @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    ORDER BY P.COD '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL =  @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    ORDER BY E.NIF '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @DESDEID varchar(50), @EST int, @ID int,@REFERENCIA varchar(120) , @CAMPO1 int ,@CAMPO2 int, @VALORCAMPO1 varchar(150) ,@VALORCAMPO2 varchar(150) ,@CATN1 int ,@CATN2 int ,@CATN3 int, @CATN4 int , @CATN5 int, @EMPRESA INT, @RECEPTOR VARCHAR(100), @COD_ERP VARCHAR(100) '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT =@ARTEXT , @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO,@DESDEID=@DESDEID, @EST=@EST, @ID = @ID, @REFERENCIA=@REFERENCIA , @CAMPO1=@CAMPO1 , @CAMPO2=@CAMPO2 ,@VALORCAMPO1=@VALORCAMPO1 , @VALORCAMPO2=@VALORCAMPO2 , @CATN1=@CATN1 , @CATN2=@CATN2 ,@CATN3=@CATN3 ,@CATN4=@CATN4, @CATN5=@CATN5 , @EMPRESA = @EMPRESA, @RECEPTOR = @RECEPTOR, @COD_ERP = @COD_ERP" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_CALCULAR_PRES1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_CALCULAR_PRES1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_AR_CALCULAR_PRES1(@ANYO SMALLINT,@GMN1 VARCHAR(20),@PROCE INTEGER,@ITEM INTEGER, @RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "IF @RES IS NULL SET @RES=0" & vbCrLf
sConsulta = sConsulta & "Return @RES" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_CALCULAR_PRES2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_CALCULAR_PRES2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_AR_CALCULAR_PRES2(@ANYO SMALLINT,@GMN1 VARCHAR(20),@PROCE INTEGER, @ITEM INTEGER,@RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "IF @RES IS NULL SET @RES=0" & vbCrLf
sConsulta = sConsulta & "Return @RES" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_CALCULAR_PRES3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_CALCULAR_PRES3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_AR_CALCULAR_PRES3(@ANYO SMALLINT,@GMN1 VARCHAR(20),@PROCE INTEGER, @ITEM INTEGER,@RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PRESCON3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "IF @RES IS NULL SET @RES=0" & vbCrLf
sConsulta = sConsulta & "Return @RES" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_CALCULAR_PRES4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_CALCULAR_PRES4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_AR_CALCULAR_PRES4(@ANYO SMALLINT,@GMN1 VARCHAR(20),@PROCE INTEGER, @ITEM INTEGER,@RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PRESCON4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "IF @RES IS NULL SET @RES=0" & vbCrLf
sConsulta = sConsulta & "Return @RES" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[USU_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_TG_NUEVOUSU]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[USU_TG_NUEVOUSU]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  TRIGGER USU_TG_NUEVOUSU ON dbo.USU" & vbCrLf
sConsulta = sConsulta & "FOR INSERT,UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Aprob_Lim_Del CURSOR LOCAL FOR SELECT COD,PER FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Aprob_Lim_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Aprob_Lim_Del INTO @COD,@PER" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF (SELECT COUNT(*) AS APROB FROM APROB_LIM WHERE PER=@PER)>0 " & vbCrLf
sConsulta = sConsulta & "        IF (SELECT COUNT(*) AS APROB FROM APROB_LIM A1 INNER JOIN APROB_LIM A2 ON A1.PADRE=A2.ID WHERE A2.PER =@PER )=0 -- No es aprobador tipo2" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT COUNT(*) FROM SEGURIDAD WHERE APROB_TIPO1 =@PER)=0 -- No es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "     SET @TIPO=1" & vbCrLf
sConsulta = sConsulta & "            ELSE --Es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "     SET @TIPO=3" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @TIPO=3" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM SEGURIDAD WHERE APROB_TIPO1 =@PER)>0 -- No es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "     SET @TIPO=2" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "     SET @TIPO=0" & vbCrLf
sConsulta = sConsulta & "    UPDATE USU SET FSEPTIPO=@TIPO,FECACT=GETDATE()  WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Aprob_Lim_Del INTO @COD,@PER" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_Aprob_Lim_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Aprob_Lim_Del" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_17_00_00A2_17_00_01() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificar tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_17_Tablas_1

frmProgreso.lblDetalle = "Modificar triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_17_Triggers_1


frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_17_Storeds_1


iRespuesta = MsgBox("�Desea activar la asignaci�n de proveedores a grupos?", vbYesNo, "FULLSTEP GS Actualizador")
If iRespuesta = vbYes Then
    ExecuteSQL gRDOCon, "UPDATE PARGEN_INTERNO SET PROCE_PROVE_GRUPOS=1"
    sConsulta = "INSERT INTO PROCE_PROVE_GRUPOS (ANYO,GMN1,PROCE,PROVE,GRUPO) SELECT PP.ANYO,PP.GMN1,PP.PROCE,PP.PROVE,PG.COD FROM PROCE_PROVE PP"
    sConsulta = sConsulta & " INNER JOIN PROCE_GRUPO PG ON PP.ANYO=PG.ANYO AND PP.GMN1=PG.GMN1 AND PP.PROCE=PG.PROCE"
    ExecuteSQL gRDOCon, sConsulta
End If

sConsulta = "UPDATE VERSION SET NUM ='2.17.00.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_17_00_00A2_17_00_01 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_17_00_00A2_17_00_01 = False

End Function

Private Sub V_2_17_Tablas_1()
Dim sConsulta As String
    
    sConsulta = "  ALTER TABLE [dbo].[PARGEN_INTERNO] ADD" & vbCrLf
    sConsulta = sConsulta & "     [PROCE_PROVE_GRUPOS] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_PROCE_PROVE_GRUPOS] DEFAULT 0" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE TABLE [dbo].[PROCE_PROVE_GRUPOS] (" & vbCrLf
    sConsulta = sConsulta & "   [ANYO] [smallint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [PROCE] [smallint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [PROVE] [varchar] (" & gLongitudesDeCodigos.giLongCodPROVE & ") NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [GRUPO] [varchar] (" & gLongitudesDeCodigos.giLongCodGRUPO & ") NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "ALTER TABLE [dbo].[PROCE_PROVE_GRUPOS] WITH NOCHECK ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [PK_PROCE_PROVE_GRUPOS] PRIMARY KEY  CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ANYO]," & vbCrLf
    sConsulta = sConsulta & "       [GMN1]," & vbCrLf
    sConsulta = sConsulta & "       [PROCE]," & vbCrLf
    sConsulta = sConsulta & "       [PROVE]," & vbCrLf
    sConsulta = sConsulta & "       [GRUPO]" & vbCrLf
    sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "ALTER TABLE [dbo].[PROCE_PROVE_GRUPOS] ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [FK_PROCE_PROVE_GRUPOS_PROCE_GRUPO] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ANYO]," & vbCrLf
    sConsulta = sConsulta & "       [GMN1]," & vbCrLf
    sConsulta = sConsulta & "       [PROCE]," & vbCrLf
    sConsulta = sConsulta & "       [GRUPO]" & vbCrLf
    sConsulta = sConsulta & "   ) REFERENCES [dbo].[PROCE_GRUPO] (" & vbCrLf
    sConsulta = sConsulta & "       [ANYO]," & vbCrLf
    sConsulta = sConsulta & "       [GMN1]," & vbCrLf
    sConsulta = sConsulta & "       [PROCE]," & vbCrLf
    sConsulta = sConsulta & "       [COD]" & vbCrLf
    sConsulta = sConsulta & "   )," & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [FK_PROCE_PROVE_GRUPOS_PROCE_PROVE] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ANYO]," & vbCrLf
    sConsulta = sConsulta & "       [GMN1]," & vbCrLf
    sConsulta = sConsulta & "       [PROCE]," & vbCrLf
    sConsulta = sConsulta & "       [PROVE]" & vbCrLf
    sConsulta = sConsulta & "   ) REFERENCES [dbo].[PROCE_PROVE] (" & vbCrLf
    sConsulta = sConsulta & "       [ANYO]," & vbCrLf
    sConsulta = sConsulta & "       [GMN1]," & vbCrLf
    sConsulta = sConsulta & "       [PROCE]," & vbCrLf
    sConsulta = sConsulta & "       [PROVE]" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE TRIGGER PROCE_PROVE_GRUPOS_TG_INSUPD ON dbo.PROCE_PROVE_GRUPOS" & vbCrLf
    sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@PROVE VARCHAR(50),@GRUPO VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "DECLARE curTG_PROCE_GRUPO_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,PROVE,GRUPO FROM INSERTED" & vbCrLf
    sConsulta = sConsulta & "OPEN curTG_PROCE_GRUPO_Ins" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GRUPO_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE,@GRUPO" & vbCrLf
    sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "UPDATE PROCE_PROVE_GRUPOS SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND GRUPO=@GRUPO" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GRUPO_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE,@GRUPO" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "Close curTG_PROCE_GRUPO_Ins" & vbCrLf
    sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GRUPO_Ins" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_17_Storeds_1()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ELIMINAR_GRUPO (@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@GRUPO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @RESTVISTAS AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEVISTAS AS INT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @PROCEVISTAS=0" & vbCrLf
sConsulta = sConsulta & "  SET @RESTVISTAS=0" & vbCrLf
sConsulta = sConsulta & "  SELECT @RESTVISTAS= COUNT(CONF_VISTAS_PROCE_ATRIB.ATRIB) FROM CONF_VISTAS_PROCE_ATRIB INNER JOIN PROCE_ATRIB " & vbCrLf
sConsulta = sConsulta & "          ON CONF_VISTAS_PROCE_ATRIB.ATRIB =PROCE_ATRIB.ID WHERE PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE AND PROCE_ATRIB.GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "        AND CONF_VISTAS_PROCE_ATRIB.POS IS NOT NULL AND CONF_VISTAS_PROCE_ATRIB.POS<>0" & vbCrLf
sConsulta = sConsulta & "  IF @RESTVISTAS>0" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "           SET @PROCEVISTAS=1" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "           CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "           FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_PROCE_ATRIB FROM CONF_VISTAS_PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROCE_ATRIB ON CONF_VISTAS_PROCE_ATRIB.ATRIB =PROCE_ATRIB.ID AND PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE AND PROCE_ATRIB.GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_GRUPO_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @RESTVISTAS=0" & vbCrLf
sConsulta = sConsulta & "  SELECT @RESTVISTAS= COUNT(CONF_VISTAS_ALL_ATRIB.ATRIB) FROM CONF_VISTAS_ALL_ATRIB INNER JOIN PROCE_ATRIB " & vbCrLf
sConsulta = sConsulta & "          ON CONF_VISTAS_ALL_ATRIB.ATRIB =PROCE_ATRIB.ID WHERE PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE AND PROCE_ATRIB.GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "        AND CONF_VISTAS_ALL_ATRIB.POSICION IS NOT NULL AND CONF_VISTAS_ALL_ATRIB.POSICION<>0" & vbCrLf
sConsulta = sConsulta & "  IF @RESTVISTAS>0" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "           SET @PROCEVISTAS=1   " & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_ALL SET " & vbCrLf
sConsulta = sConsulta & "                PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                FROM CONF_VISTAS_ALL V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=0 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_ALL_ATRIB FROM CONF_VISTAS_ALL_ATRIB" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB ON CONF_VISTAS_ALL_ATRIB.ATRIB =PROCE_ATRIB.ID AND PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE AND PROCE_ATRIB.GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "   IF @PROCEVISTAS=1   " & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 " & vbCrLf
sConsulta = sConsulta & "                   begin" & vbCrLf
sConsulta = sConsulta & "                      UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE                  " & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_PRES1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_PRES2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_PRES3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_PRES4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GRUPO_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_ITEM_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE C CURSOR FOR SELECT ID FROM ITEM WHERE ANYO=@ANYO AND PROCE=@PROCE AND GMN1=@GMN1 AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   OPEN C" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM OFE_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM OFE_ITEM_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_OFE_WEB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_ADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_ADJREU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_OBJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_PRES WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_PRESCON WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE C" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM OFE_GR_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM GRUPO_OFE_REU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM GRUPO_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_PROVE_GRUPOS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_ATRIB_POND FROM PROCE_ATRIB_POND" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_ATRIB ON PROCE_ATRIB_POND.ATRIB_ID =PROCE_ATRIB.ID AND PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE AND PROCE_ATRIB.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_ATRIB_LISTA FROM PROCE_ATRIB_LISTA" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROCE_ATRIB ON PROCE_ATRIB_LISTA.ATRIB_ID =PROCE_ATRIB.ID AND PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE  AND PROCE_ATRIB.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND COD=@GRUPO" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ITEMS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_ITEMS  @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@GRUPO VARCHAR(50)=null, @PROVE varchar(50), @OFE smallint, @INTERNO TINYINT = NULL, @TODOS TINYINT = 0, @MON varchar(50) = null  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPROC FLOAT  " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOATRIB tinyint" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODATRIB varchar(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROCEATRIB int" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAMDEF NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ALTER NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSERT NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE_GRUPOS AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVE_GRUPOS = PROCE_PROVE_GRUPOS FROM PARGEN_INTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIVPROC = CAMBIO, @PROCEEST =EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "              AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              AND OFE=@OFE)" & vbCrLf
sConsulta = sConsulta & "IF @CAM IS NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT @CAM =  EQUIV / @EQUIVPROC" & vbCrLf
sConsulta = sConsulta & "    FROM MON " & vbCrLf
sConsulta = sConsulta & "   WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "declare C1 CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ID, DA.COD, DA.TIPO" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #ATRIBS (ATANYO int NULL ," & vbCrLf
If gCollation = "" Then
    sConsulta = sConsulta & "   ATGMN1 varchar (50) NULL ," & vbCrLf
Else
    sConsulta = sConsulta & "   ATGMN1 varchar (50) COLLATE " & gCollation & " NULL ," & vbCrLf
End If
sConsulta = sConsulta & "   ATPROCE int NULL ," & vbCrLf
sConsulta = sConsulta & "   ATITEM int NULL " & vbCrLf
sConsulta = sConsulta & ") " & vbCrLf
sConsulta = sConsulta & "SET @INSERT = 'INSERT INTO #ATRIBS (ATANYO, ATGMN1, ATPROCE, ATITEM, '" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT ANYO, GMN1,  PROCE, ITEM, '" & vbCrLf
sConsulta = sConsulta & "set @ALTER = 'ALTER TABLE #ATRIBS ADD '" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @IDPROCEATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    SET @INSERT = @INSERT + '[AT_' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "    IF @TIPOATRIB = 1 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] VARCHAR(800), '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_TEXT else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 2" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] FLOAT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_NUM else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 3" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] DATETIME, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_FEC else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER + '[AT_' + @CODATRIB + '] TINYINT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_BOOL else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @IDPROCEATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "if @INSERT != 'INSERT INTO #ATRIBS (ATANYO, ATGMN1, ATPROCE, ATITEM, '" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    set @SQL = LEFT(@SQL,LEN(@SQL)-1)" & vbCrLf
sConsulta = sConsulta & "    set @INSERT = LEFT(@INSERT,LEN(@INSERT)-1)" & vbCrLf
sConsulta = sConsulta & "    set @ALTER = LEFT(@ALTER,LEN(@ALTER)-1)" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @ALTER " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @INSERT + ')" & vbCrLf
sConsulta = sConsulta & "    ' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ATRIB IOA" & vbCrLf
sConsulta = sConsulta & "     WHERE IOA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND IOA.GMN1 = @GMN1  " & vbCrLf
sConsulta = sConsulta & "       AND IOA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND IOA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "       AND IOA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "    GROUP BY ANYO, GMN1,  PROCE, ITEM'" & vbCrLf
sConsulta = sConsulta & "    SET @PARAMDEF = '@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT,@GRUPO VARCHAR(50), @PROVE varchar(50), @OFE smallint'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @SQL, @PARAMDEF, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO, @PROVE = @PROVE, @OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "--   Se devuelve siempre en la moneda de la oferta" & vbCrLf
sConsulta = sConsulta & "SET @SQL='" & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO, I.GMN1, I.PROCE, I.GRUPO, I.ID, I.GMN2, I.GMN3, I.GMN4, I.ART, I.DESCR + A.DEN AS DESCR, " & vbCrLf
sConsulta = sConsulta & "       I.DEST, I.UNI,UNI.DEN UNIDEN, I.CANT, I.PREC*@CAM as PREC, I.PRES*@CAM as PRES," & vbCrLf
sConsulta = sConsulta & "       I.PAG, PAG.DEN DENPAG, I.FECINI, I.FECFIN, I.ESP, I.CONF, I.EST," & vbCrLf
sConsulta = sConsulta & "       I.OBJ*@CAM AS OBJ , I.FECHAOBJ, " & vbCrLf
sConsulta = sConsulta & "       O.PROVE MINPROVE, P2.DEN MINPROVEDEN, O.PRECIO*@EQUIVPROC*@CAM MINPRECIO," & vbCrLf
sConsulta = sConsulta & "       CASE WHEN @PROVE = O.PROVE THEN 1 ELSE 0 END PROPIA," & vbCrLf
sConsulta = sConsulta & "       CASE  WHEN NUMESP > 0 THEN 1 WHEN NOT I.ESP  IS NULL THEN 1 ELSE 0 END ESPECADJ ," & vbCrLf
sConsulta = sConsulta & "       ITO.PRECIO, ITO.CANTMAX, ITO.ULT, ITO.PRECIO2, ITO.PRECIO3,ITO.USAR,ITO.FECACT," & vbCrLf
sConsulta = sConsulta & "       IA.NUMADJUN, ITO.COMENT1, ITO.COMENT2, ITO.COMENT3, @EQUIVPROC*@CAM CAMBIO, cast(ITO.OBSADJUN as varchar(2000)) OBSADJUN, @EQUIVPROC EQUIVPROC, ATR.*" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM I" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ART4 A" & vbCrLf
sConsulta = sConsulta & "               ON I.ART = A.COD " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN4 = A.GMN4 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN3 = A.GMN3 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN2 = A.GMN2" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = A.GMN1" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ITEM_OFE ITO" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = ITO.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = ITO.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = ITO.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = ITO.ITEM" & vbCrLf
sConsulta = sConsulta & "              AND @PROVE = ITO.PROVE" & vbCrLf
sConsulta = sConsulta & "              AND @OFE = ITO.NUM" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN (OFEMIN O " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN PROVE P2 " & vbCrLf
sConsulta = sConsulta & "                             ON O.PROVE = P2.COD" & vbCrLf
sConsulta = sConsulta & "                   )" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = O.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = O.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = O.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = O.ITEM" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN #ATRIBS ATR" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = ATR.ATANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = ATR.ATGMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = ATR.ATPROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = ATR.ATITEM        LEFT JOIN (SELECT IE.ANYO, IE.GMN1, IE.PROCE, IE.ITEM, COUNT(IE.ID) NUMESP " & vbCrLf
sConsulta = sConsulta & "                     FROM ITEM_ESP IE" & vbCrLf
sConsulta = sConsulta & "                    WHERE IE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                      AND IE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                      AND IE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                 GROUP BY IE.ANYO, IE.GMN1, IE.PROCE, IE.ITEM) IE" & vbCrLf
sConsulta = sConsulta & "               ON I.ID = IE.ITEM " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = IE.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = IE.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ANYO = IE.ANYO " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN (SELECT IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM, COUNT(IA.ID) NUMADJUN" & vbCrLf
sConsulta = sConsulta & "                     FROM OFE_ITEM_ADJUN IA" & vbCrLf
sConsulta = sConsulta & "                    WHERE IA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                      AND IA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                      AND IA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                      AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                      AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                 GROUP BY IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM) IA" & vbCrLf
sConsulta = sConsulta & "               ON I.ID = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ANYO = IA.ANYO " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PAG " & vbCrLf
sConsulta = sConsulta & "               ON I.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN UNI " & vbCrLf
sConsulta = sConsulta & "               ON I.UNI = UNI.COD" & vbCrLf
sConsulta = sConsulta & "WHERE I.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND I.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND I.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND I.CONF=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVE_GRUPOS=1 AND @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + '  AND I.GRUPO IN (SELECT GRUPO FROM PROCE_PROVE_GRUPOS PVG WHERE PVG.ANYO=@ANYO AND PVG.GMN1=@GMN1 AND PVG.PROCE=@PROCE AND " & vbCrLf
sConsulta = sConsulta & "           PVG.PROVE=@PROVE)'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + '  AND I.GRUPO =case when @GRUPO is null then i.grupo else @GRUPO END'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + '   AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN CASE WHEN @TODOS=1 THEN I.EST ELSE 0 END ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "                            ORDER BY I.ART,DESCR'" & vbCrLf
sConsulta = sConsulta & " SET @PARAMDEF = '@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT,@GRUPO VARCHAR(50), @PROVE varchar(50), @OFE smallint,@CAM float,@EQUIVPROC float,@PROCEEST SMALLINT,@TODOS TINYINT'" & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL  @SQL, @PARAMDEF, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO, @PROVE = @PROVE, @OFE = @OFE,@CAM=@CAM,@EQUIVPROC=@EQUIVPROC,@PROCEEST=@PROCEEST,@TODOS=@TODOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_ASIGNACION_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_ASIGNACION_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ELIMINAR_ASIGNACION_GRUPO(@ANYO AS SMALLINT,@GMN1 AS VARCHAR(50), @PROCE AS SMALLINT,@PROVE AS VARCHAR(50),@GRUPO AS VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PPARAM NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM CONF_VISTAS_GRUPO_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM CONF_VISTAS_ITEM_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DELETE ITEM_ADJREU  FROM ITEM_ADJREU IR INNER JOIN ITEM I ON IR.ANYO=I.ANYO AND IR.GMN1=I.GMN1 AND IR.PROCE=I.PROCE AND IR.ITEM =I.ID WHERE IR.ANYO=@ANYO AND IR.GMN1=@GMN1 AND IR.PROCE=@PROCE AND IR.PROVE=@PROVE AND I.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE ITEM_ADJ  FROM ITEM_ADJ IR INNER JOIN ITEM I ON IR.ANYO=I.ANYO AND IR.GMN1=I.GMN1 AND IR.PROCE=I.PROCE AND IR.ITEM =I.ID WHERE IR.ANYO=@ANYO AND IR.GMN1=@GMN1 AND IR.PROCE=@PROCE AND IR.PROVE=@PROVE AND I.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE OFEMIN FROM OFEMIN IR INNER JOIN ITEM I ON IR.ANYO=I.ANYO AND IR.GMN1=I.GMN1 AND IR.PROCE=I.PROCE AND IR.ITEM =I.ID WHERE IR.ANYO=@ANYO AND IR.GMN1=@GMN1 AND IR.PROCE=@PROCE AND IR.PROVE=@PROVE AND I.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE P_OFE_ITEM_ATRIB  FROM P_OFE_ITEM_ATRIB PR INNER JOIN P_PROCE_OFE P ON PR.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE=@PROVE AND PR.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE P_OFE_ITEM_ADJUN  FROM OFE_ITEM_ADJUN PR INNER JOIN P_PROCE_OFE P ON PR.ID=P.ID INNER JOIN ITEM I ON P.ANYO=I.ANYO AND P.GMN1=I.GMN1 AND P.PROCE=I.PROCE AND PR.ITEM =I.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE=@PROVE AND I.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE P_ITEM_OFE  FROM P_ITEM_OFE PR INNER JOIN P_PROCE_OFE P ON PR.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE=@PROVE AND PR.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE OFE_ITEM_ATRIB  FROM OFE_ITEM_ATRIB IR INNER JOIN ITEM I ON IR.ANYO=I.ANYO AND IR.GMN1=I.GMN1 AND IR.PROCE=I.PROCE AND IR.ITEM =I.ID WHERE IR.ANYO=@ANYO AND IR.GMN1=@GMN1 AND IR.PROCE=@PROCE AND IR.PROVE=@PROVE AND I.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE OFE_ITEM_ADJUN  FROM OFE_ITEM_ADJUN IR INNER JOIN ITEM I ON IR.ANYO=I.ANYO AND IR.GMN1=I.GMN1 AND IR.PROCE=I.PROCE AND IR.ITEM =I.ID WHERE IR.ANYO=@ANYO AND IR.GMN1=@GMN1 AND IR.PROCE=@PROCE AND IR.PROVE=@PROVE AND I.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    UPDATE ITEM_OFE SET PRECIO=NULL,PRECIO2=NULL,PRECIO3=NULL,CANTMAX=NULL,PREC_VALIDO=NULL,OBSADJUN=NULL,COMENT1=NULL,COMENT2=NULL,COMENT3=NULL  FROM ITEM_OFE IR INNER JOIN ITEM I ON IR.ANYO=I.ANYO AND IR.GMN1=I.GMN1 AND IR.PROCE=I.PROCE AND IR.ITEM =I.ID WHERE IR.ANYO=@ANYO AND IR.GMN1=@GMN1 AND IR.PROCE=@PROCE AND IR.PROVE=@PROVE AND I.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT @LNKSERVER=FSP_SRV + '.' + FSP_BD +'.dbo.' FROM PARGEN_PORT" & vbCrLf
sConsulta = sConsulta & "    SET @LNKSERVER=@LNKSERVER + 'sp_executesql'" & vbCrLf
sConsulta = sConsulta & "    SET @PPARAM = N'@ANYO AS SMALLINT,@GMN1 AS VARCHAR(50), @PROCE AS SMALLINT,@PROVE AS VARCHAR(50),@GRUPO AS VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING = N'DELETE OFE_ITEM_ADJUN  FROM OFE_ITEM_ADJUN IR INNER JOIN PROCE_OFE P ON P.ID=IR.ID INNER JOIN ITEM_OFE IO ON IO.ID=IR.ID AND IR.ITEM=IO.ITEM WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE =@PROVE AND IO.GRUPO=@GRUPO'" & vbCrLf
sConsulta = sConsulta & "    EXEC @LNKSERVER @SQLSTRING, @PPARAM, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @PROVE = @PROVE, @GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING = N'DELETE OFE_ITEM_ATRIB  FROM OFE_ITEM_ATRIB IR INNER JOIN PROCE_OFE P ON P.ID=IR.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE =@PROVE AND IR.GRUPO=@GRUPO'" & vbCrLf
sConsulta = sConsulta & "    EXEC @LNKSERVER @SQLSTRING, @PPARAM, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @PROVE = @PROVE, @GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING = N'UPDATE ITEM_OFE SET PRECIO=NULL,PRECIO2=NULL,PRECIO3=NULL,CANTMAX=NULL,OBSADJUN=NULL,COMENT1=NULL,COMENT2=NULL,COMENT3=NULL,ADJUN=0  FROM ITEM_OFE IR INNER JOIN PROCE_OFE P ON P.ID=IR.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE =@PROVE AND IR.GRUPO=@GRUPO'" & vbCrLf
sConsulta = sConsulta & "    EXEC @LNKSERVER @SQLSTRING, @PPARAM, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @PROVE = @PROVE, @GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING = N'DELETE OFE_GR_ADJUN  FROM OFE_GR_ADJUN GR INNER JOIN PROCE_OFE P ON P.ID=GR.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE =@PROVE AND GR.GRUPO=@GRUPO'" & vbCrLf
sConsulta = sConsulta & "    EXEC @LNKSERVER @SQLSTRING, @PPARAM, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @PROVE = @PROVE, @GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING = N'DELETE OFE_GR_ATRIB  FROM OFE_GR_ATRIB GR INNER JOIN PROCE_OFE P ON P.ID=GR.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE =@PROVE AND GR.GRUPO=@GRUPO'" & vbCrLf
sConsulta = sConsulta & "    EXEC @LNKSERVER @SQLSTRING, @PPARAM, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @PROVE = @PROVE, @GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING = N'UPDATE  GRUPO_OFE  SET OBSADJUN=NULL,ADJUN=0 FROM GRUPO_OFE GR INNER JOIN PROCE_OFE P ON P.ID=GR.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE =@PROVE AND GR.GRUPO=@GRUPO'" & vbCrLf
sConsulta = sConsulta & "    EXEC @LNKSERVER @SQLSTRING, @PPARAM, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @PROVE = @PROVE, @GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DELETE  P_OFE_GR_ATRIB  FROM P_OFE_GR_ATRIB PR INNER JOIN P_PROCE_OFE P ON PR.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE=@PROVE AND PR.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE  P_OFE_GR_ADJUN  FROM P_OFE_GR_ADJUN PR INNER JOIN P_PROCE_OFE P ON PR.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE=@PROVE AND PR.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE  P_GRUPO_OFE  FROM P_GRUPO_OFE PR INNER JOIN P_PROCE_OFE P ON PR.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND P.PROVE=@PROVE AND PR.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM GRUPO_OFE_REU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM OFE_GR_ADJUN  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    UPDATE GRUPO_OFE SET CONSUMIDO=NULL,ADJUDICADO=NULL, AHORRADO=NULL,AHORRADO_PORCEN=NULL,ABIERTO=NULL,IMPORTE=NULL,AHORRO_OFE=NULL, AHORRO_OFE_PORCEN=NULL,OBSADJUN=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM PROCE_PROVE_GRUPOS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND GRUPO=@GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CARGAR_CONFIG_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CARGAR_CONFIG_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_CARGAR_CONFIG_PROCE @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @PROVE VARCHAR(50)=null, @DEFDEST tinyint  = null OUTPUT, @DEFPAG tinyint = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFFECSUM tinyint  = null OUTPUT, @DEFPROCEESP tinyint  = null OUTPUT, @DEFGRUPOESP  tinyint = null OUTPUT, @DEFITEMESP tinyint = null OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFOFEADJUN tinyint = null OUTPUT, @DEFGRUPOADJUN tinyint = null OUTPUT, @DEFITEMADJUN tinyint = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFSUBASTA tinyint = null OUTPUT, @DEFSUVERPROVE tinyint = null OUTPUT, @DEFSUVERPUJAS tinyint = null OUTPUT, @DEFSUMINUTOS int = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFPRECALTER tinyint = null OUTPUT, @DEFCANTMAX tinyint = null OUTPUT, @HAYATRIBUTOS AS tinyint = null OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @CAMBIARMON tinyint = 1 OUTPUT, @ADMINPUB tinyint=null OUTPUT, @NUMMAXOFE int = 0 OUTPUT, @PUBLICARFINSUM tinyint = null OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE @PISUBASTA TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE_GRUPOS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT  @PISUBASTA=SUBASTA, @PROVE_GRUPOS=PROCE_PROVE_GRUPOS FROM PARGEN_INTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROCEEST = EST   FROM PROCE  WHERE ANYO = @ANYO   AND GMN1 = @GMN1   AND COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @DEFDEST=PD.DEST, @DEFPAG=PD.PAG, @DEFFECSUM=PD.FECSUM, @DEFPROCEESP=PD.PROCE_ESP, @DEFGRUPOESP = PD.GRUPO_ESP, @DEFITEMESP  = PD.ITEM_ESP, " & vbCrLf
sConsulta = sConsulta & "       @DEFOFEADJUN = PD.OFE_ADJUN, @DEFGRUPOADJUN=PD.GRUPO_ADJUN, @DEFITEMADJUN= PD.ITEM_ADJUN, @DEFCANTMAX=PD.SOLCANTMAX, @DEFPRECALTER = PD.PRECALTER, " & vbCrLf
sConsulta = sConsulta & "       @DEFSUBASTA = CASE WHEN @PISUBASTA=1 THEN PD.SUBASTA ELSE 0 END, @DEFSUVERPROVE = PD.SUBASTAPROVE, @DEFSUVERPUJAS=PD.SUBASTAPUJAS, @DEFSUMINUTOS = PD.SUBASTAESPERA," & vbCrLf
sConsulta = sConsulta & "       @HAYATRIBUTOS = ISNULL(PA.ATRIB,0),@CAMBIARMON=PD.CAMBIARMON, @ADMINPUB = ADMIN_PUB, @NUMMAXOFE = PD.MAX_OFE , @PUBLICARFINSUM = PD.PUBLICARFINSUM" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN (SELECT ANYO, GMN1, PROCE, COUNT(ATRIB ) ATRIB" & vbCrLf
sConsulta = sConsulta & "                        FROM PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "                    GROUP BY ANYO, GMN1, PROCE) PA" & vbCrLf
sConsulta = sConsulta & "                  ON PD.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PD.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PD.PROCE = PA.PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE PD.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND PD.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PD.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NOT NULL AND @PROVE_GRUPOS=1 " & vbCrLf
sConsulta = sConsulta & "  select PG.anyo, PG.gmn1, PG.proce, PG.COD , PG.DEN, COUNT(PA.ATRIB) ATRIB, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'ESP' THEN 1 else 0 end) as ESP," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES1' THEN 1 ELSE 0 END) AS PRES1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES2' THEN 1 ELSE 0 END) AS PRES2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU1' THEN 1 ELSE 0 END) AS PRESANU1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU2' THEN 1 ELSE 0 END) AS PRESANU2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DIST' THEN 1 ELSE 0 END) AS DIST," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PROVE' THEN 1 ELSE 0 END) AS PROVE," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'FECSUM' THEN 1 ELSE 0 END) AS FECSUM," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PAG' THEN 1 ELSE 0 END) AS PAG," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DEST' THEN 1 ELSE 0 END) AS DEST," & vbCrLf
sConsulta = sConsulta & "       ISNULL(IT.NUMITEMS,0) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "  from PROCE_GRUPO PG " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PROCE_PROVE_GRUPOS PPG" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PPG.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PPG.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PPG.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = PPG.GRUPO" & vbCrLf
sConsulta = sConsulta & "                 AND PPG.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN PROCE_GRUPO_DEF PGD" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PGD.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PGD.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PGD.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = PGD.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN (SELECT ANYO, GMN1, PROCE, ISNULL(GRUPO,'#T#') GRUPO, ATRIB " & vbCrLf
sConsulta = sConsulta & "                        FROM PROCE_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND AMBITO = 2) PA" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = CASE WHEN PA.GRUPO ='#T#' THEN PG.COD ELSE PA.GRUPO END" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN (SELECT I.GRUPO, count(I.id) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "                        FROM ITEM I " & vbCrLf
sConsulta = sConsulta & "                      WHERE I.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND I.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND I.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                        AND I.CONF = 1" & vbCrLf
sConsulta = sConsulta & "                        AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "                     GROUP BY I.GRUPO) IT" & vbCrLf
sConsulta = sConsulta & "                  ON PG.COD = IT.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN SOBRE S" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = S.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = S.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = S.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.SOBRE = S.SOBRE" & vbCrLf
sConsulta = sConsulta & "  where PG.anyo = @ANYO" & vbCrLf
sConsulta = sConsulta & "  and PG.gmn1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "  and PG.proce = @PROCE" & vbCrLf
sConsulta = sConsulta & "  and isnull(pg.cerrado,0) = 0" & vbCrLf
sConsulta = sConsulta & "  group by PG.anyo, PG.gmn1, PG.proce, PG.cod, PG.DEN, IT.NUMITEMS, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS" & vbCrLf
sConsulta = sConsulta & "  ORDER BY S.SOBRE,PG.COD" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  select PG.anyo, PG.gmn1, PG.proce, PG.COD , PG.DEN, COUNT(PA.ATRIB) ATRIB, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'ESP' THEN 1 else 0 end) as ESP," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES1' THEN 1 ELSE 0 END) AS PRES1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES2' THEN 1 ELSE 0 END) AS PRES2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU1' THEN 1 ELSE 0 END) AS PRESANU1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU2' THEN 1 ELSE 0 END) AS PRESANU2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DIST' THEN 1 ELSE 0 END) AS DIST," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PROVE' THEN 1 ELSE 0 END) AS PROVE," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'FECSUM' THEN 1 ELSE 0 END) AS FECSUM," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PAG' THEN 1 ELSE 0 END) AS PAG," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DEST' THEN 1 ELSE 0 END) AS DEST," & vbCrLf
sConsulta = sConsulta & "       ISNULL(IT.NUMITEMS,0) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "  from PROCE_GRUPO PG " & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN PROCE_GRUPO_DEF PGD" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PGD.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PGD.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PGD.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = PGD.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN (SELECT ANYO, GMN1, PROCE, ISNULL(GRUPO,'#T#') GRUPO, ATRIB " & vbCrLf
sConsulta = sConsulta & "                        FROM PROCE_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND AMBITO = 2) PA" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = CASE WHEN PA.GRUPO ='#T#' THEN PG.COD ELSE PA.GRUPO END" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN (SELECT I.GRUPO, count(I.id) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "                        FROM ITEM I " & vbCrLf
sConsulta = sConsulta & "                      WHERE I.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND I.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND I.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                        AND I.CONF = 1" & vbCrLf
sConsulta = sConsulta & "                        AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "                     GROUP BY I.GRUPO) IT" & vbCrLf
sConsulta = sConsulta & "                  ON PG.COD = IT.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN SOBRE S" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = S.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = S.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = S.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.SOBRE = S.SOBRE" & vbCrLf
sConsulta = sConsulta & "  where PG.anyo = @ANYO" & vbCrLf
sConsulta = sConsulta & "  and PG.gmn1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "  and PG.proce = @PROCE" & vbCrLf
sConsulta = sConsulta & "  and isnull(pg.cerrado,0) = 0" & vbCrLf
sConsulta = sConsulta & "  group by PG.anyo, PG.gmn1, PG.proce, PG.cod, PG.DEN, IT.NUMITEMS, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS" & vbCrLf
sConsulta = sConsulta & "  ORDER BY S.SOBRE,PG.COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_ESP @ANYO INT, @GMN1 VARCHAR(20), @PROCE INT, @PROVE VARCHAR(50)=null AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE_GRUPOS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVE_GRUPOS=PROCE_PROVE_GRUPOS FROM PARGEN_INTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NOT NULL AND @PROVE_GRUPOS=1 " & vbCrLf
sConsulta = sConsulta & "   SELECT ANYO, GMN1, PROCE, NULL GRUPO, NULL ITEM,  ID, NOM, COM, FECACT, DATALENGTH(DATA) TAMANYO FROM PROCE_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT PGE.ANYO, PGE.GMN1, PGE.PROCE, PGE.GRUPO, NULL ITEM,  PGE.ID, PGE.NOM, PGE.COM, PGE.FECACT, DATALENGTH(DATA) TAMANYO " & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_GRUPO_ESP PGE" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE_PROVE_GRUPOS PPG ON PGE.ANYO=PPG.ANYO AND PGE.GMN1=PPG.GMN1 AND PGE.PROCE=PPG.PROCE AND PGE.GRUPO=PPG.GRUPO AND PPG.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "     WHERE PGE.ANYO=@ANYO AND PGE.GMN1=@GMN1 AND PGE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT IE.ANYO, IE.GMN1, IE.PROCE, I.GRUPO, IE.ITEM, IE.ID, IE.NOM, IE.COM, IE.FECACT, DATALENGTH(IE.DATA) TAMANYO " & vbCrLf
sConsulta = sConsulta & "     FROM  ITEM_ESP IE" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN ITEM I   ON IE.ANYO = I.ANYO    AND IE.GMN1 = I.GMN1     AND IE.PROCE = I.PROCE      AND IE.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROCE_PROVE_GRUPOS PPG ON I.ANYO=PPG.ANYO AND I.GMN1=PPG.GMN1 AND I.PROCE=PPG.PROCE AND I.GRUPO=PPG.GRUPO AND PPG.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "     WHERE IE.ANYO=@ANYO AND IE.GMN1=@GMN1 AND IE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT ANYO, GMN1, PROCE, NULL GRUPO, NULL ITEM,  ID, NOM, COM, FECACT, DATALENGTH(DATA) TAMANYO FROM PROCE_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT ANYO, GMN1, PROCE, GRUPO, NULL ITEM,  ID, NOM, COM, FECACT, DATALENGTH(DATA) TAMANYO FROM PROCE_GRUPO_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT IE.ANYO, IE.GMN1, IE.PROCE, I.GRUPO, IE.ITEM, IE.ID, IE.NOM, IE.COM, IE.FECACT, DATALENGTH(IE.DATA) TAMANYO " & vbCrLf
sConsulta = sConsulta & "     FROM  ITEM_ESP IE" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN ITEM I   ON IE.ANYO = I.ANYO    AND IE.GMN1 = I.GMN1     AND IE.PROCE = I.PROCE      AND IE.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & "     WHERE IE.ANYO=@ANYO AND IE.GMN1=@GMN1 AND IE.PROCE=@PROCE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_17_Triggers_1()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PROVE_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_PROVE_TG_INS ON dbo.PROCE_PROVE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPOS AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @GRUPOS=PROCE_PROVE_GRUPOS FROM PARGEN_INTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PROVE_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Inserta los registros correspondientes en las tablas de prove-grupos*/" & vbCrLf
sConsulta = sConsulta & "IF @GRUPOS=1 " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PROCE_PROVE_GRUPOS (ANYO, GMN1, PROCE, PROVE, GRUPO) SELECT ANYO,GMN1,PROCE,@PROVE,COD FROM PROCE_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*Inserta los registros correspondientes en las tablas de configuraci�n de la comparativa*/" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_PROCE_PROVE (ANYO,GMN1,PROCE,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_ALL_PROVE (ANYO,GMN1,PROCE,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_ALL WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_GRUPO_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,GRUPO,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_ITEM_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,GRUPO,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET P= case when P is null then 1 else (P+1) end  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* Si se han insertado proveedores, actualizamos el estado del proceso  si es que este era inferior a */" & vbCrLf
sConsulta = sConsulta & "IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) < 4  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST = 4  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GRUPO_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GRUPO_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_GRUPO_TG_INS ON dbo.PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @BGRUPOS AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @BGRUPOS = PROCE_PROVE_GRUPOS FROM PARGEN_INTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Inserta en USAR_GR_ATRIB los atributos aplicables al precio del total de grupo que haya para el proceso" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GRUPO_Ins CURSOR  LOCAL FOR SELECT ANYO,GMN1,PROCE,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GRUPO_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO USAR_GR_ATRIB (ANYO,GMN1,PROCE,GRUPO,ATRIB,USAR_PREC) SELECT ANYO,GMN1,PROCE,@GRUPO,ID,DEF_PREC " & vbCrLf
sConsulta = sConsulta & "   FROM  PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC=1 AND GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO GRUPO_OFE (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE) SELECT ANYO,GMN1,PROCE,@GRUPO,PROVE,OFE " & vbCrLf
sConsulta = sConsulta & "    FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "    IF @BGRUPOS =1" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO PROCE_PROVE_GRUPOS (ANYO,GMN1,PROCE,PROVE,GRUPO) SELECT ANYO,GMN1,PROCE,PROVE,@GRUPO FROM PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM curTG_PROCE_GRUPO_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion2_17_00_01A2_17_00_02() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Insertar registro integraci�n"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "INSERT INTO TABLAS_INTEGRACION (ID,TABLA,ACTIVA) VALUES (16,16,0)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE VERSION SET NUM ='2.17.00.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_17_00_01A2_17_00_02 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_17_00_01A2_17_00_02 = False

End Function

Public Function CodigoDeActualizacion2_17_00_02A2_17_00_03() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_17_Storeds_3


sConsulta = "UPDATE VERSION SET NUM ='2.17.00.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_17_00_02A2_17_00_03 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_17_00_02A2_17_00_03 = False

End Function


Private Sub V_2_17_Storeds_3()
Dim sConsulta As String


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SDEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR (50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_GRUPOS SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " IF (SELECT SINC_MAT FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @IDI=(SELECT IDIOMAPORT FROM PARINS_GEST WHERE USU=@USUARIO)" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_SPA'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET @PARAM = N'@VAR_COD VARCHAR(50), @VAR_DEN VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & " SET @SQL='" & vbCrLf
sConsulta = sConsulta & " DECLARE C CURSOR FOR SELECT COD,' + @SDEN + ' FROM GMN1 WHERE COD=''' + @OLD + '''" & vbCrLf
sConsulta = sConsulta & " OPEN C" & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & " IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " INSERT INTO LOG_GMN (ACCION,GMN1,DEN,COD_NEW,ORIGEN,USU) VALUES (''C'',@VAR_COD,@VAR_DEN,''' + @NEW + ''',''' + @INT_ORIGEN + ''',''' + @USUARIO + ''')" & vbCrLf
sConsulta = sConsulta & " End" & vbCrLf
sConsulta = sConsulta & " Close C" & vbCrLf
sConsulta = sConsulta & " DEALLOCATE C'" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "  EXEC SP_EXECUTESQL @SQL,@PARAM, @VAR_COD = @VAR_COD, @VAR_DEN=@VAR_DEN" & vbCrLf
sConsulta = sConsulta & " End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROCE_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROCE_COD (@OLD SMALLINT,@NEW SMALLINT,@ANYO INT,@GMN1 VARCHAR (50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @AN AS INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE D CURSOR FOR SELECT ANYO,GMN1,COD FROM PROCE WHERE ANYO=@ANYO AND COD=@OLD AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "OPEN D " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM D INTO @AN,@GMN,@VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_SOBRE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD  " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_GRUPOS SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL  " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close D " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE D " & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROVE_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROVE_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON " & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM PROVE WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP_PORTAL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "--2.12" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN1_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN2_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN3_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN4_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN5_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EMPRESA_COD_ERP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_GRUPOS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close C " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GRUPO_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROCE_GRUPO_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROCE_GRUPO_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@ANYO INT,@GMN1 VARCHAR (50),@PROCE INT)  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @AN AS INT ,@PROC AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON " & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE D CURSOR FOR SELECT ANYO,GMN1,PROCE,COD FROM PROCE_GRUPO WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN D " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM D INTO @AN,@GMN,@PROC,@VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_GRUPOS SET GRUPO=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@OLD " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close D " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE D " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_17_00_03A2_17_00_04() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

'Cambio de la comprativa, quito las adjudicaciones 0
sConsulta = "DELETE FROM ITEM_ADJREU WHERE PORCEN=0"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "DELETE FROM ITEM_ADJ WHERE PORCEN=0"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE VERSION SET NUM ='2.17.00.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_17_00_03A2_17_00_04 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_17_00_03A2_17_00_04 = False

End Function

Public Function CodigoDeActualizacion2_17_00_04A2_17_00_05() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_17_Storeds_5

sConsulta = "UPDATE VERSION SET NUM ='2.17.00.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_17_00_04A2_17_00_05 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_17_00_04A2_17_00_05 = False

End Function


Private Sub V_2_17_Storeds_5()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRASPASAR_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRASPASAR_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_TRASPASAR_OFERTA @ID int , @NUM INT OUTPUT, @NUMNUE INT OUTPUT, @NUMAREV INT OUTPUT, @ERROR SMALLINT = NULL OUTPUT, @COMPROBARATRIBS TINYINT = 0  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NEWOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEWEB AS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJ INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJGS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO = ANYO, @GMN1= GMN1, @PROCE = PROCE, @PROVE = PROVE, @MON = MON, @NUMOBJ = NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUMOBJGS = (SELECT NUMOBJ FROM PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "IF @NUMOBJGS  <=@NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE_PROVE SET OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV FLOAT " & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIV = (MON.EQUIV / PROCE.CAMBIO) " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "             ON PROCE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "            AND MON.COD=@MON " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SUBASTA = SUBASTA " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBASTA = 1 OR @COMPROBARATRIBS  = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    DECLARE @OFE INT" & vbCrLf
sConsulta = sConsulta & "    SET @OFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE!=-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_GR_ATRIB (ID, GRUPO, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.GRUPO, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_GR_ATRIB T WHERE T.ID = @ID AND T.GRUPO = O.GRUPO AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ITEM_ATRIB (ID, GRUPO, ITEM, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, I.GRUPO, IA.ITEM, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ATRIB IA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                   ON I.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "                  AND I.ID = IA.ITEM" & vbCrLf
sConsulta = sConsulta & "         WHERE IA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND IA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = IA.ATRIB_ID AND T.ITEM = IA.ITEM)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ATRIB (ID, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEGRUPOATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEITEMATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @PAID INT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @AMBITO SMALLINT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE CATRIBS CURSOR LOCAL FORWARD_ONLY  FOR " & vbCrLf
sConsulta = sConsulta & "      SELECT PA.ID, AMBITO, GRUPO, TIPO" & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                 ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "         AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "         AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "         AND OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      OPEN CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "      SET @OFEATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEGRUPOATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "      WHILE @@FETCH_STATUS=0 AND @OFEATRIB=0 AND @OFEGRUPOATRIB=0 AND @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEATRIB =1" & vbCrLf
sConsulta = sConsulta & "              SELECT @OFEATRIB = CASE " & vbCrLf
sConsulta = sConsulta & "                        WHEN CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NULL " & vbCrLf
sConsulta = sConsulta & "                        THEN 1 " & vbCrLf
sConsulta = sConsulta & "                        ELSE 0 " & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                FROM P_OFE_ATRIB" & vbCrLf
sConsulta = sConsulta & "               WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEGRUPOATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL) <> (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE)" & vbCrLf
sConsulta = sConsulta & "                   SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  =0 " & vbCrLf
sConsulta = sConsulta & "                       SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEITEMATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                     END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE                    " & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                         AND ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                          AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      CLOSE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      IF @OFEATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 2" & vbCrLf
sConsulta = sConsulta & "      IF @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 3" & vbCrLf
sConsulta = sConsulta & "      IF @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 4" & vbCrLf
sConsulta = sConsulta & "      IF @ERROR>0" & vbCrLf
sConsulta = sConsulta & "        GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE  @ESTWEB VARCHAR(3) " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @ESTWEB=ESTWEB FROM PARGEN_DEF INNER JOIN PARGEN_GEST ON PARGEN_DEF.ID=PARGEN_GEST.ID  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET ULT= 0, LEIDO = 1  WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ULT = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NEWOFE = ISNULL(MAX(OFE),0) + 1 FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE != -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE(ANYO, GMN1, PROCE, PROVE, OFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, LEIDO, ULT, ENVIADA, OBSADJUN, PORTAL,USU,NOMUSU)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, @ESTWEB, getdate(), FECVAL, MON, CAMBIO, OBS, 0, 1, 1, OBSADJUN, PORTAL,USU,NOMUSU" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & "   SET OBSADJUN = PGO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE GO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN P_GRUPO_OFE PGO" & vbCrLf
sConsulta = sConsulta & "              ON GO.GRUPO = PGO.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE GO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND GO.OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "   SET PRECIO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       CANTMAX = PIO.CANTMAX," & vbCrLf
sConsulta = sConsulta & "       PRECIO2 = PIO.PRECIO2," & vbCrLf
sConsulta = sConsulta & "       PRECIO3 = PIO.PRECIO3," & vbCrLf
sConsulta = sConsulta & "       PREC_VALIDO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       OBSADJUN = PIO.OBSADJUN," & vbCrLf
sConsulta = sConsulta & "       COMENT1 = PIO.COMENT1," & vbCrLf
sConsulta = sConsulta & "       COMENT2 = PIO.COMENT2," & vbCrLf
sConsulta = sConsulta & "       COMENT3 = PIO.COMENT3" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN P_ITEM_OFE PIO" & vbCrLf
sConsulta = sConsulta & "             ON IO.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN ITEM I ON IO.ANYO=I.ANYO AND IO.GMN1=I.GMN1 AND IO.PROCE=I.PROCE AND IO.ITEM= I.ID " & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND (I.EST=0 OR I.EST IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ADJUN R" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN P_ITEM_OFE PIO" & vbCrLf
sConsulta = sConsulta & "               ON PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "              AND R.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ADJUN = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB P  " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ITEM = R.ITEM" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_GR_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND CAST(P.GRUPO AS VARCHAR(50)) = CAST(R.GRUPO AS VARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ADJUN R" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN P_GRUPO_OFE PGO" & vbCrLf
sConsulta = sConsulta & "            ON PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "           AND PGO.GRUPO = R.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ADJUN = 1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT ADJUN FROM P_PROCE_OFE WHERE ID = @ID) = 1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS FROM P_PROCE_OFE_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_PROCE_OFE_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_ADJUN(ANYO, GMN1, PROCE, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, POA.IDGS, POA.NOM, POA.COM, POA.IDPORTAL, POA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE_ADJUN POA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON POA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE POA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, GRUPO FROM P_OFE_GR_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_GR_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OGA.GRUPO, @PROVE, @NEWOFE, OGA.IDGS, OGA.NOM, OGA.COM, OGA.IDPORTAL, OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OGA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & " WHERE OGA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, ITEM FROM P_OFE_ITEM_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_ITEM_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ADJUN(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OIA.ITEM, @PROVE, @NEWOFE, OIA.IDGS, OIA.NOM, OIA.COM, OIA.IDPORTAL, OIA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OIA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE OIA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, PO.GRUPO, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ATRIB PO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                ON PA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "               AND PO.ATRIB_ID  = PA.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " WHERE PO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIB(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, PO.ITEM, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ATRIB PO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                ON PA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "               AND PO.ATRIB_ID  = PA.ID" & vbCrLf
sConsulta = sConsulta & " WHERE PO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ATRIB(ANYO, GMN1, PROCE, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ATRIB PO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                ON PA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "               AND PO.ATRIB_ID  = PA.ID" & vbCrLf
sConsulta = sConsulta & " WHERE PO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND PA.AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE AND OFE = 0" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEWEB = isnull(MAX(OFEWEB),0)+1 FROM PROCE_OFE_WEB WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB (ANYO, GMN1, PROCE, PROVE, OFEWEB, OFE, FECREC, FECVAL, MON)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, OFE, FECREC,FECVAL,MON " & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE_WEB (ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NUMOFEWEB, PRECIO, CANTMAX" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB_ADJUN (ANYO, GMN1, PROCE, PROVE, OFEWEB, ID, NOM, COM, IDPORTAL)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, ID, NOM, COM, IDPORTAL" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET ULT = 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE =@PROCE AND PROVE=@PROVE AND NUM!=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "COMMIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 100" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ROLLBACK" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERRORSINTRAN_:" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 200" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELIMINAR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC NUM_PROCE_PUB @PROVE =@PROVE ,@NUM = @NUM OUTPUT,@NUMNUE = @NUMNUE OUTPUT ,@NUMAREV = @NUMAREV OUTPUT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_GRUPO_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_ITEM_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ADJUN WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_17_00_05A2_17_00_06() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_17_Storeds_6

sConsulta = "UPDATE VERSION SET NUM ='2.17.00.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_17_00_05A2_17_00_06 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_17_00_05A2_17_00_06 = False

End Function

Private Sub V_2_17_Storeds_6()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_PERSONAS_ACCION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_PERSONAS_ACCION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_PERSONAS_ACCION @ACC int AS" & vbCrLf
sConsulta = sConsulta & "SELECT P.COD, P.NOM, P.APE " & vbCrLf
sConsulta = sConsulta & "  FROM USU U" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PER P " & vbCrLf
sConsulta = sConsulta & "               ON U.PER  = P.COD " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN USU_ACC UA" & vbCrLf
sConsulta = sConsulta & "               ON U.COD =  UA.USU" & vbCrLf
sConsulta = sConsulta & "              AND @ACC = UA.ACC" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PERF_ACC PA" & vbCrLf
sConsulta = sConsulta & "               ON U.PERF = PA.PERF" & vbCrLf
sConsulta = sConsulta & "              AND @ACC = PA.ACC" & vbCrLf
sConsulta = sConsulta & "WHERE ISNULL(UA.ACC,PA.ACC) IS NOT NULL AND U.BAJA=0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_ELIMINAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_ELIMINAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_ELIMINAR  @TABLA SMALLINT,@ATRIB INTEGER  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ELIM_V SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 0" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROVE_ART4_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      DELETE  FROM ART4_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM GMN4_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PLANTILLA_ATRIB_LISTA  WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      DELETE  FROM PLANTILLA_ATRIB_POND  WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB  FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB INNER JOIN PLANTILLA_ATRIB" & vbCrLf
sConsulta = sConsulta & "             ON PLANTILLA_CONF_VISTAS_PROCE_ATRIB.ATRIB=PLANTILLA_ATRIB.ID AND PLANTILLA_ATRIB. ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB  FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB INNER JOIN PLANTILLA_ATRIB" & vbCrLf
sConsulta = sConsulta & "             ON PLANTILLA_CONF_VISTAS_GRUPO_ATRIB.ATRIB=PLANTILLA_ATRIB.ID AND PLANTILLA_ATRIB. ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB  FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB INNER JOIN PLANTILLA_ATRIB" & vbCrLf
sConsulta = sConsulta & "             ON PLANTILLA_CONF_VISTAS_ALL_ATRIB.ATRIB=PLANTILLA_ATRIB.ID AND PLANTILLA_ATRIB. ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PLANTILLA_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "          " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM DEF_ATRIB_LISTA WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM DEF_ATRIB_POND WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "       DELETE   FROM DEF_ATRIB  WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "         IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @ELIM_V=0" & vbCrLf
sConsulta = sConsulta & "           ---Si existe el atributo en la pesta�a de ALL" & vbCrLf
sConsulta = sConsulta & "           DECLARE C_Vistas CURSOR LOCAL FOR SELECT DISTINCT ANYO,GMN1,PROCE FROM CONF_VISTAS_ALL_ATRIB WHERE  ATRIB=@ATRIB AND POSICION IS NOT NULL AND POSICION<>0" & vbCrLf
sConsulta = sConsulta & "           OPEN C_Vistas" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C_Vistas INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "           WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_ALL SET " & vbCrLf
sConsulta = sConsulta & "                PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                FROM CONF_VISTAS_ALL V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=0 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 " & vbCrLf
sConsulta = sConsulta & "                 IF (SELECT count(*)  FROM PLANTILLA_ATRIB LA INNER JOIN PROCE P ON P.PLANTILLA=LA.PLANT INNER JOIN PROCE_ATRIB PA ON PA.ANYO=P.ANYO AND PA.GMN1 =P.GMN1 AND PA.PROCE=P.COD AND PA.ATRIB=LA.ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.COD=@PROCE AND PA.ID=@ATRIB)=1 " & vbCrLf
sConsulta = sConsulta & "                      SET @ELIM_V=1        " & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C_Vistas INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "          Close C_Vistas" & vbCrLf
sConsulta = sConsulta & "          DEALLOCATE C_Vistas" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          DELETE FROM CONF_VISTAS_ALL_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "          IF @ELIM_V =1 " & vbCrLf
sConsulta = sConsulta & "                      UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE          " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          ---Si existe el atributo en la pesta�a de Grupo" & vbCrLf
sConsulta = sConsulta & "           SET @ELIM_V=0" & vbCrLf
sConsulta = sConsulta & "           DECLARE C_Vistas2 CURSOR LOCAL FOR SELECT DISTINCT ANYO,GMN1,PROCE,GRUPO FROM CONF_VISTAS_GRUPO_ATRIB WHERE  ATRIB=@ATRIB AND POSICION IS NOT NULL AND POSICION<>0" & vbCrLf
sConsulta = sConsulta & "           OPEN C_Vistas2" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C_Vistas2 INTO @ANYO,@GMN1,@PROCE,@GRUPO" & vbCrLf
sConsulta = sConsulta & "           WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                FROM CONF_VISTAS_GRUPO V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=1 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 " & vbCrLf
sConsulta = sConsulta & "                 IF (SELECT count(*)  FROM PLANTILLA_ATRIB LA INNER JOIN PROCE P ON P.PLANTILLA=LA.PLANT INNER JOIN PROCE_ATRIB PA ON PA.ANYO=P.ANYO AND PA.GMN1 =P.GMN1 AND PA.PROCE=P.COD AND PA.ATRIB=LA.ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.COD=@PROCE AND PA.ID=@ATRIB)=1 " & vbCrLf
sConsulta = sConsulta & "                      SET @ELIM_V=1        " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM C_Vistas2 INTO @ANYO,@GMN1,@PROCE,@GRUPO" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          Close C_Vistas2" & vbCrLf
sConsulta = sConsulta & "          DEALLOCATE C_Vistas2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          DELETE FROM CONF_VISTAS_GRUPO_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "          IF @ELIM_V =1 " & vbCrLf
sConsulta = sConsulta & "                      UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE          " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          ---Si existe el atributo en la pesta�a de PROCE" & vbCrLf
sConsulta = sConsulta & "           SET @ELIM_V = 0" & vbCrLf
sConsulta = sConsulta & "           DECLARE C_Vistas3 CURSOR LOCAL FOR SELECT DISTINCT ANYO,GMN1,PROCE FROM CONF_VISTAS_PROCE_ATRIB WHERE  ATRIB=@ATRIB AND POS IS NOT NULL AND POS<>0" & vbCrLf
sConsulta = sConsulta & "           OPEN C_Vistas3" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C_Vistas3 INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "           WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "           CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "           FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 " & vbCrLf
sConsulta = sConsulta & "                 IF (SELECT count(*)  FROM PLANTILLA_ATRIB LA INNER JOIN PROCE P ON P.PLANTILLA=LA.PLANT INNER JOIN PROCE_ATRIB PA ON PA.ANYO=P.ANYO AND PA.GMN1 =P.GMN1 AND PA.PROCE=P.COD AND PA.ATRIB=LA.ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.COD=@PROCE AND PA.ID=@ATRIB)=1 " & vbCrLf
sConsulta = sConsulta & "                      SET @ELIM_V=1        " & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C_Vistas3 INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "          Close C_Vistas3" & vbCrLf
sConsulta = sConsulta & "          DEALLOCATE C_Vistas3" & vbCrLf
sConsulta = sConsulta & "          DELETE FROM CONF_VISTAS_PROCE_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "          IF @ELIM_V =1 " & vbCrLf
sConsulta = sConsulta & "                      UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE          " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM CONF_VISTAS_ITEM_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM OFE_ATRIB WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM OFE_GR_ATRIB WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM OFE_ITEM_ATRIB WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM PROCE_ATRIB_POND WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM PROCE_ATRIB_LISTA WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM PROCE_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "             IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       --Si existe el atributo en la pesta�a de ALL" & vbCrLf
sConsulta = sConsulta & "        DECLARE C_Vistas CURSOR LOCAL FOR SELECT PLANT FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE  ATRIB=@ATRIB AND POSICION IS NOT NULL AND POSICION<>0" & vbCrLf
sConsulta = sConsulta & "        OPEN C_Vistas" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM C_Vistas INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "        WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL SET " & vbCrLf
sConsulta = sConsulta & "                    PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1," & vbCrLf
sConsulta = sConsulta & "                    CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                                 FETCH NEXT FROM C_Vistas INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "       Close C_Vistas" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE C_Vistas" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --Si existe el atributo en la pesta�a de grupo" & vbCrLf
sConsulta = sConsulta & "       DECLARE C_Vistas2 CURSOR LOCAL FOR SELECT PLANT FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE  ATRIB=@ATRIB AND POSICION IS NOT NULL AND POSICION<>0" & vbCrLf
sConsulta = sConsulta & "        OPEN C_Vistas2" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM C_Vistas2 INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "        WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                    PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1," & vbCrLf
sConsulta = sConsulta & "                    CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                                 FETCH NEXT FROM C_Vistas2 INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "       Close C_Vistas2" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE C_Vistas2" & vbCrLf
sConsulta = sConsulta & "                           DELETE FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       ---Si existe el atributo en la pesta�a de PROCE" & vbCrLf
sConsulta = sConsulta & "            DECLARE C_Vistas3 CURSOR LOCAL FOR SELECT PLANT FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE  ATRIB=@ATRIB AND POS IS NOT NULL AND POS<>0" & vbCrLf
sConsulta = sConsulta & "        OPEN C_Vistas3" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM C_Vistas3 INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "        WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "               CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0" & vbCrLf
sConsulta = sConsulta & "               , ADJ_POS=3, ADJ_WIDTH=850,ADJ_VISIBLE=1,AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "               , AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ,AHORR_ADJ_PORCEN_VISIBLE=0" & vbCrLf
sConsulta = sConsulta & "               , AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "               , AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                                   FETCH NEXT FROM C_Vistas3 INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "                          END " & vbCrLf
sConsulta = sConsulta & "         Close C_Vistas3" & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE C_Vistas3" & vbCrLf
sConsulta = sConsulta & "                  DELETE FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        DELETE FROM PLANTILLA_ATRIB_POND WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PLANTILLA_ATRIB_LISTA WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PLANTILLA_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_ADJUDICACIONES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_ADJUDICACIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ELIMINAR_ADJUDICACIONES(@ANYO AS SMALLINT,@GMN1 AS VARCHAR(50), @PROCE AS SMALLINT,@PROVE AS VARCHAR(50)=null) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAY_REU INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECREU DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF  (SELECT COUNT(*) FROM ITEM_ADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE) >0 " & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "      SET @EST = (SELECT EST FROM PROCE  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "      IF @EST>=9 AND @EST<=11" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "         SET @HAY_REU=(SELECT COUNT(*) FROM REU_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF @EST=11 " & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_ADJ FROM ITEM_ADJ IA INNER JOIN ITEM IT ON IA.ANYO=IT.ANYO AND IA.GMN1=IT.GMN1 AND IA.PROCE=IT.PROCE AND IA.ITEM=IT.ID AND IT.EST=0 WHERE IA.ANYO=@ANYO  AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_ADJ WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         UPDATE GRUPO_OFE SET CONSUMIDO=NULL,ADJUDICADO=NULL,AHORRADO=NULL,AHORRADO_PORCEN=NULL WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @HAY_REU>0 " & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF @EST=11 " & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_ADJREU FROM ITEM_ADJREU IA INNER JOIN ITEM IT ON IA.ANYO=IT.ANYO AND IA.GMN1=IT.GMN1 AND IA.PROCE=IT.PROCE AND IA.ITEM=IT.ID AND IT.EST=0 WHERE IA.ANYO=@ANYO  AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_ADJREU WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             UPDATE GRUPO_OFE_REU SET CONSUMIDO=NULL,ADJUDICADO=NULL,AHORRADO=NULL,AHORRADO_PORCEN=NULL WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        UPDATE PROCE SET ADJUDICADO=NULL,CONSUMIDO=NULL,FECULTREU=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "        IF @EST<>11" & vbCrLf
sConsulta = sConsulta & "          IF @EST = 9 " & vbCrLf
sConsulta = sConsulta & "              UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE SET EST=7  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       --Llama al stored que calcula el precio v�lido y el abierto de GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_CALCULAR_PREC_VALIDO @ANYO, @GMN1, @PROCE   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @HAY_REU>0" & vbCrLf
sConsulta = sConsulta & "         BEGIN              " & vbCrLf
sConsulta = sConsulta & "           DECLARE CUR_REU CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "               SELECT FECHA FROM REU_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           OPEN CUR_REU" & vbCrLf
sConsulta = sConsulta & "                    FETCH NEXT FROM CUR_REU INTO @FECREU" & vbCrLf
sConsulta = sConsulta & "                              WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                                  BEGIN" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_CALCULAR_PREC_VALIDO_REU @ANYO, @GMN1, @PROCE, @FECREU" & vbCrLf
sConsulta = sConsulta & "                                        FETCH NEXT FROM CUR_REU INTO @FECREU" & vbCrLf
sConsulta = sConsulta & "                                  END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "         CLOSE CUR_REU" & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE CUR_REU" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE --CON PROVE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF  (SELECT COUNT(*) FROM ITEM_ADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE) >0 " & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "      SET @EST = (SELECT EST FROM PROCE  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "      IF @EST>=9 AND @EST<=11" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "          SET @HAY_REU=(SELECT COUNT(*) FROM REU_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF @EST=11 " & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_ADJ FROM ITEM_ADJ IA INNER JOIN ITEM IT ON IA.ANYO=IT.ANYO AND IA.GMN1=IT.GMN1 AND IA.PROCE=IT.PROCE AND IA.ITEM=IT.ID AND IT.EST=0 WHERE IA.ANYO=@ANYO  AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE AND IA.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_ADJ WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         UPDATE GRUPO_OFE SET CONSUMIDO=NULL,ADJUDICADO=NULL,AHORRADO=NULL,AHORRADO_PORCEN=NULL WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         IF @HAY_REU>0 " & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF @EST=11 " & vbCrLf
sConsulta = sConsulta & "               DELETE FROM ITEM_ADJREU FROM ITEM_ADJREU IA INNER JOIN ITEM IT ON IA.ANYO=IT.ANYO AND IA.GMN1=IT.GMN1 AND IA.PROCE=IT.PROCE AND IA.ITEM=IT.ID AND IT.EST=0 WHERE IA.ANYO=@ANYO  AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE AND IA.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "               DELETE FROM ITEM_ADJREU WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             UPDATE GRUPO_OFE_REU SET CONSUMIDO=NULL,ADJUDICADO=NULL,AHORRADO=NULL,AHORRADO_PORCEN=NULL WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "        UPDATE PROCE SET ADJUDICADO=NULL,CONSUMIDO=NULL,FECULTREU=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "        IF @EST<>11" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM ITEM_ADJ  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE)=0" & vbCrLf
sConsulta = sConsulta & "              IF @EST = 9 " & vbCrLf
sConsulta = sConsulta & "                   UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                   UPDATE PROCE SET EST=7  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       --Llama al stored que calcula el precio v�lido y el abierto de GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_CALCULAR_PREC_VALIDO @ANYO, @GMN1, @PROCE   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         IF @HAY_REU>0" & vbCrLf
sConsulta = sConsulta & "         BEGIN              " & vbCrLf
sConsulta = sConsulta & "           DECLARE CUR_REU CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "               SELECT FECHA FROM REU_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           OPEN CUR_REU" & vbCrLf
sConsulta = sConsulta & "                    FETCH NEXT FROM CUR_REU INTO @FECREU" & vbCrLf
sConsulta = sConsulta & "                              WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       EXEC SP_CALCULAR_PREC_VALIDO_REU @ANYO, @GMN1, @PROCE, @FECREU" & vbCrLf
sConsulta = sConsulta & "                                        FETCH NEXT FROM CUR_REU INTO @FECREU" & vbCrLf
sConsulta = sConsulta & "                                  END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "         CLOSE CUR_REU" & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE CUR_REU" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_2_17_Triggers_6()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_ADJ_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_ADJ_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ITEM_ADJ_TG_INSUPD ON [ITEM_ADJ]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_ADJ IO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND IO.ITEM = I.ITEM" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROVE = I.PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_OFE_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_OFE_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ITEM_OFE_TG_INSUPD ON dbo.ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN INSERTED I " & vbCrLf
sConsulta = sConsulta & "             ON IO.ANYO  = I.ANYO " & vbCrLf
sConsulta = sConsulta & "            AND IO.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND IO.PROCE = I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND IO.ITEM = I.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND IO.PROVE=I.PROVE " & vbCrLf
sConsulta = sConsulta & "            AND IO.NUM = I.NUM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_ADJREU_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_ADJREU_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ITEM_ADJREU_TG_INSUPD ON [ITEM_ADJREU]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_ADJREU IO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND IO.ITEM = I.ITEM" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROVE = I.PROVE" & vbCrLf
sConsulta = sConsulta & "               AND IO.FECREU=I.FECREU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GRUPO_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GRUPO_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_GRUPO_TG_INSUPD ON dbo.PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO IO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND IO.COD = I.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ITEM_TG_INSUPD ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM  IO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND IO.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ART4_ADJ_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ART4_ADJ_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ART4_ADJ_TG_INSUPD ON dbo.ART4_ADJ" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ART4_ADJ A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND A.GMN2=I.GMN2" & vbCrLf
sConsulta = sConsulta & "               AND A.GMN3=I.GMN3" & vbCrLf
sConsulta = sConsulta & "               AND A.GMN4=I.GMN4" & vbCrLf
sConsulta = sConsulta & "               AND A.ART=I.ART" & vbCrLf
sConsulta = sConsulta & "               AND A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ITEM_TG_UPD ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ART AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINI AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECFIN AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJ AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJOLD AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECNECMANUAL  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ID,GMN2,GMN3,GMN4,ART,CANT,FECINI,FECFIN,OBJ FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@FECFIN,@OBJ" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET  @FECNECMANUAL=(SELECT PROCE.FECNECMANUAL FROM PROCE  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "IF @FECNECMANUAL=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE)  WHERE COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(PRES) OR UPDATE(CONF)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     UPDATE PROCE SET PRESTOTAL=(SELECT SUM(PRES) FROM ITEM WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND CONF=1 GROUP BY ANYO,GMN1,PROCE )   ," & vbCrLf
sConsulta = sConsulta & "     CONSUMIDO=(select sum(item.prec*(item.cant*(porcen/100)))  from item_adj  " & vbCrLf
sConsulta = sConsulta & "     inner join item on item_adj.anyo=item.anyo and item_adj.gmn1=item.gmn1 and item_adj.proce=item.proce and item_adj.item=item.id where item_adj.anyo=@ANYO and item_adj.gmn1=@GMN1 and item_adj.proce=@PROCE )" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(OBJ)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EST = (SELECT EST FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "SET @OBJOLD = (SELECT OBJ FROM DELETED WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ID)" & vbCrLf
sConsulta = sConsulta & "IF @OBJ IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Si estamos en estado 8 y borramos el �ltimo objetivo entonces pasamos a estado 7*/" & vbCrLf
sConsulta = sConsulta & "IF @EST = 8 AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=7 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 9  AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=10 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD IS NOT NULL AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE_PROVE SET NUMOBJ=0,OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD <> @OBJ" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "_FETCH:" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@FECFIN,@OBJ" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_17_00_06A2_17_00_07() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificar tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_17_Tablas_7

frmProgreso.lblDetalle = "Modificar triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_17_Triggers_7

frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_17_Storeds_7

sConsulta = "UPDATE VERSION SET NUM ='2.17.00.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_17_00_06A2_17_00_07 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_17_00_06A2_17_00_07 = False

End Function

Private Sub V_2_17_Tablas_7()
Dim sConsulta As String
    
    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[USU]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'MODIFPREC')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[USU] ADD" & vbCrLf
    sConsulta = sConsulta & "     [MODIFPREC] [tinyint] NOT NULL CONSTRAINT [DF_USU_MODIFPREC] DEFAULT 0" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[CATN1]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'MODIFPREC')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[CATN1] ADD" & vbCrLf
    sConsulta = sConsulta & "     [MODIFPREC] [tinyint] NOT NULL CONSTRAINT [DF_CATN1_MODIFPREC] DEFAULT 0," & vbCrLf
    sConsulta = sConsulta & "     [ACTUALIZARPREC] [tinyint] NOT NULL CONSTRAINT [DF_CATN1_ACTUALIZARPREC] DEFAULT 0"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[CATN2]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'MODIFPREC')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[CATN2] ADD" & vbCrLf
    sConsulta = sConsulta & "     [MODIFPREC] [tinyint] NOT NULL CONSTRAINT [DF_CATN2_MODIFPREC] DEFAULT 0," & vbCrLf
    sConsulta = sConsulta & "     [ACTUALIZARPREC] [tinyint] NOT NULL CONSTRAINT [DF_CATN2_ACTUALIZARPREC] DEFAULT 0"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[CATN3]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'MODIFPREC')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[CATN3] ADD" & vbCrLf
    sConsulta = sConsulta & "     [MODIFPREC] [tinyint] NOT NULL CONSTRAINT [DF_CATN3_MODIFPREC] DEFAULT 0," & vbCrLf
    sConsulta = sConsulta & "     [ACTUALIZARPREC] [tinyint] NOT NULL CONSTRAINT [DF_CATN3_ACTUALIZARPREC] DEFAULT 0"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[CATN4]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'MODIFPREC')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[CATN4] ADD" & vbCrLf
    sConsulta = sConsulta & "     [MODIFPREC] [tinyint] NOT NULL CONSTRAINT [DF_CATN4_MODIFPREC] DEFAULT 0," & vbCrLf
    sConsulta = sConsulta & "     [ACTUALIZARPREC] [tinyint] NOT NULL CONSTRAINT [DF_CATN4_ACTUALIZARPREC] DEFAULT 0"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[CATN5]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'MODIFPREC')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[CATN5] ADD" & vbCrLf
    sConsulta = sConsulta & "     [MODIFPREC] [tinyint] NOT NULL CONSTRAINT [DF_CATN5_MODIFPREC] DEFAULT 0," & vbCrLf
    sConsulta = sConsulta & "     [ACTUALIZARPREC] [tinyint] NOT NULL CONSTRAINT [DF_CATN5_ACTUALIZARPREC] DEFAULT 0"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[FAVORITOS_LINEAS_PEDIDO]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'PREC_CAMBIADO')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[FAVORITOS_LINEAS_PEDIDO] ADD" & vbCrLf
    sConsulta = sConsulta & "     [PREC_CAMBIADO] [tinyint] NOT NULL CONSTRAINT [DF_FAVORITOS_LINEAS_PEDIDO_PREC_CAMBIADO] DEFAULT 0"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_PROCE_PLANTILLA]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
    sConsulta = sConsulta & "    ALTER TABLE [dbo].[PROCE] ADD CONSTRAINT [FK_PROCE_PLANTILLA] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "     ([PLANTILLA]) REFERENCES [dbo].[PLANTILLA] ([ID])"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[LINEAS_PEDIDO]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'LINCAT')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[LINEAS_PEDIDO] ADD" & vbCrLf
    sConsulta = sConsulta & "     [LINCAT] [int] NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
End Sub

Private Sub V_2_17_Triggers_7()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_MAT1_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_MAT1_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PRES_MAT1_TG_INSUPD ON dbo.PRES_MAT1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM PRES_MAT1 PM" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON PM.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PM.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_MAT2_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_MAT2_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PRES_MAT2_TG_INSUPD ON dbo.PRES_MAT2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM PRES_MAT2 IO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN2 = I.GMN2" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_MAT3_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_MAT3_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PRES_MAT3_TG_INSUPD ON dbo.PRES_MAT3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM PRES_MAT3 IO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN2 = I.GMN2" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN3 = I.GMN3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FAVORITOS_LINEAS_PEDIDO_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[FAVORITOS_LINEAS_PEDIDO_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER FAVORITOS_LINEAS_PEDIDO_TG_INS ON dbo.FAVORITOS_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "FOR INSERT,UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM FAVORITOS_LINEAS_PEDIDO A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FAVORITOS_LINEAS_PEDIDO_ADJUN_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[FAVORITOS_LINEAS_PEDIDO_ADJUN_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER FAVORITOS_LINEAS_PEDIDO_ADJUN_TG_INSUPD ON dbo.FAVORITOS_LINEAS_PEDIDO_ADJUN" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO_ADJUN SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM FAVORITOS_LINEAS_PEDIDO_ADJUN A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FAVORITOS_ORDEN_ENTREGA_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[FAVORITOS_ORDEN_ENTREGA_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER FAVORITOS_ORDEN_ENTREGA_TG_INSUPD ON dbo.FAVORITOS_ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM FAVORITOS_ORDEN_ENTREGA A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FAVORITOS_ORDEN_ENTREGA_ADJUN_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[FAVORITOS_ORDEN_ENTREGA_ADJUN_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER FAVORITOS_ORDEN_ENTREGA_ADJUN_TG_INSUPD ON dbo.FAVORITOS_ORDEN_ENTREGA_ADJUN" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA_ADJUN SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM FAVORITOS_ORDEN_ENTREGA_ADJUN A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LINEAS_PEDIDO_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[LINEAS_PEDIDO_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER LINEAS_PEDIDO_TG_INS ON dbo.LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM LINEAS_PEDIDO A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID=I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LINEAS_PEDIDO_ADJUN_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[LINEAS_PEDIDO_ADJUN_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER LINEAS_PEDIDO_ADJUN_TG_INSUPD ON dbo.LINEAS_PEDIDO_ADJUN" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO_ADJUN SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM LINEAS_PEDIDO_ADJUN A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_ENTREGA_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_ENTREGA_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ORDEN_ENTREGA_TG_INSUPD ON dbo.ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_ENTREGA_ADJUN_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_ENTREGA_ADJUN_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ORDEN_ENTREGA_ADJUN_TG_INSUPD ON dbo.ORDEN_ENTREGA_ADJUN" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA_ADJUN SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA_ADJUN A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CATN1_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CATN1_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER CATN1_TG_INSUPD ON dbo.CATN1 " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN1 SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM CATN1 A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CATN2_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CATN2_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER CATN2_TG_INSUPD ON dbo.CATN2 " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN2 SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM CATN2 A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CATN3_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CATN3_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER CATN3_TG_INSUPD ON dbo.CATN3 " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN3 SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM CATN3 A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CATN4_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CATN4_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER CATN4_TG_INSUPD ON dbo.CATN4 " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN4 SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM CATN4 A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CATN5_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CATN5_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER CATN5_TG_INSUPD ON dbo.CATN5 " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN5 SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM CATN5 A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_2_17_Storeds_7()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ARTICULOS_CATALOGO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ARTICULOS_CATALOGO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ARTICULOS_CATALOGO @COD VARCHAR(40) = NULL, @DEN VARCHAR(300) = NULL, @PROVE VARCHAR(300) = NULL," & vbCrLf
sConsulta = sConsulta & "@CATN1 VARCHAR(2000) = NULL, @CATN2 VARCHAR(2000) = NULL, @CATN3 VARCHAR(2000) = NULL, @CATN4 VARCHAR(2000) = NULL, " & vbCrLf
sConsulta = sConsulta & "@CATN5 VARCHAR(2000) = NULL, @ID varchar(200),@PRECIOMAX FLOAT = NULL,@PER VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@MAXLINEAS INTEGER,@COINCIDENCIA INTEGER=NULL,@DEST varchar(50)=null, @ORDER VARCHAR(3) = 'COD', @DIREC VARCHAR(4) = 'ASC'," & vbCrLf
sConsulta = sConsulta & "@CODEXT VARCHAR(50) = NULL   AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'COD'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING='SELECT DISTINCT TOP ' + CONVERT(VARCHAR,@MAXLINEAS) +' CL.art_int + I.ART +  convert(varchar,CL.ID) VID , CL.art_int + I.ART TOOLTIP,'" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'DEN'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING='SELECT DISTINCT TOP ' + CONVERT(VARCHAR,@MAXLINEAS) +' ISNULL(ART_AUX.DEN,A.DEN) + I.DESCR  +  convert(varchar,CL.ID) VID , ISNULL(ART_AUX.DEN,A.DEN) + I.DESCR TOOLTIP,'" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'PRO'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING='SELECT DISTINCT TOP ' + CONVERT(VARCHAR,@MAXLINEAS) +' PROVE.DEN +  convert(varchar,CL.ID) VID , PROVE.DEN TOOLTIP,'" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'PRE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING='SELECT DISTINCT TOP ' + CONVERT(VARCHAR,@MAXLINEAS) +' LEFT(''0000000000000000000000000'', 25 - LEN(CAST(CAST(PRECIO_UC AS DECIMAL(25,5)) AS VARCHAR(25)))) + CAST(CAST(PRECIO_UC AS DECIMAL(25,5)) AS VARCHAR(25)) + CAST(CL.ID AS VARCHAR) VID , PRECIO_UC TOOLTIP, '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "CL.ID,CL.ANYO,CL.PROCE,CL.ITEM," & vbCrLf
sConsulta = sConsulta & "CL.PROVE,ART_INT + I.ART COD_ITEM," & vbCrLf
sConsulta = sConsulta & "CASE WHEN ART_AUX.DEN IS NULL THEN A.DEN ELSE ART_AUX.DEN END + I.DESCR ART_DEN," & vbCrLf
sConsulta = sConsulta & "CL.DEST,UC,PA4.COD_EXT," & vbCrLf
sConsulta = sConsulta & "CANT_ADJ,PRECIO_UC,UP_DEF,FC_DEF," & vbCrLf
sConsulta = sConsulta & "CANT_MIN_DEF,PUB,FECHA_DESPUB,CL.CAT1,CL.CAT2,CL.CAT3,CL.CAT4,CL.CAT5,CL.SEGURIDAD," & vbCrLf
sConsulta = sConsulta & "PROVE.DEN  AS PROVEDEN," & vbCrLf
sConsulta = sConsulta & "CL.GMN1,CL.GMN2,CL.GMN3,CL.GMN4,APROB_LIM.MOD_DEST, DATALENGTH(PA4.THUMBNAIL) THUMBNAIL, CL.ESP," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "  case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "      case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "        c1.cod" & vbCrLf
sConsulta = sConsulta & "      else" & vbCrLf
sConsulta = sConsulta & "        c2.cod" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "    else" & vbCrLf
sConsulta = sConsulta & "      c3.cod" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "  else" & vbCrLf
sConsulta = sConsulta & "    c4.cod" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "  c5.cod" & vbCrLf
sConsulta = sConsulta & "end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "  case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c3.cod is null then  " & vbCrLf
sConsulta = sConsulta & "      case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "        ''('' + c1.cod + '') '' + c1.den" & vbCrLf
sConsulta = sConsulta & "      else" & vbCrLf
sConsulta = sConsulta & "        ''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "    else" & vbCrLf
sConsulta = sConsulta & "      ''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den + '' - ('' + c3.cod + '') '' + c3.den" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "  else" & vbCrLf
sConsulta = sConsulta & "    ''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den + '' - ('' + c3.cod + '') '' + c3.den + '' - ('' + c4.cod + '') '' + c4.den" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "  ''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den + '' - ('' + c3.cod + '') '' + c3.den + '' - ('' + c4.cod + '') '' + c4.den + '' - ('' + c5.cod + '') '' + c5.den" & vbCrLf
sConsulta = sConsulta & "end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "       case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "           case when c2.cod is null then  c1.MODIFPREC  " & vbCrLf
sConsulta = sConsulta & "           else  c2.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & "       else  c3.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & "    else  c4.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & " else  c5.MODIFPREC end MODIF_PREC," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "       case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "           case when c2.cod is null then  c1.ACTUALIZARPREC  " & vbCrLf
sConsulta = sConsulta & "           else  c2.ACTUALIZARPREC end" & vbCrLf
sConsulta = sConsulta & "       else  c3.ACTUALIZARPREC end" & vbCrLf
sConsulta = sConsulta & "    else  c4.ACTUALIZARPREC end" & vbCrLf
sConsulta = sConsulta & " else  c5.ACTUALIZARPREC end ACTUALIZAR_PREC" & vbCrLf
sConsulta = sConsulta & " '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & " FROM CATALOG_LIN CL " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "            ON CL.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "       ON CL.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CL.ANYO=I.ANYO " & vbCrLf
sConsulta = sConsulta & "      AND CL.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "      AND CL.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 A" & vbCrLf
sConsulta = sConsulta & "       ON CL.GMN1=A.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CL.GMN2=A.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND CL.GMN3=A.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND CL.GMN4=A.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND CL.ART_INT + I.ART=A.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "       ON I.GMN1=ART_AUX.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND I.GMN2=ART_AUX.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND I.GMN3=ART_AUX.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND I.GMN4=ART_AUX.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND I.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ART4 PA4" & vbCrLf
sConsulta = sConsulta & "       ON CL.GMN1=PA4.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CL.GMN2=PA4.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND CL.GMN3=PA4.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND CL.GMN4=PA4.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND CASE WHEN CL.ART_INT IS NULL THEN I.ART ELSE CL.ART_INT  END =PA4.ART " & vbCrLf
sConsulta = sConsulta & "      AND CL.PROVE=PA4.PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN1 C1   ON CL.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN2 C2   ON CL.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN3 C3   ON CL.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN4 C4   ON CL.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN5 C5   ON CL.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN APROB_LIM " & vbCrLf
sConsulta = sConsulta & "                  ON CL.SEGURIDAD=APROB_LIM.SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "                 AND APROB_LIM.PER=@PER'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING +'  " & vbCrLf
sConsulta = sConsulta & " WHERE PUB=1" & vbCrLf
sConsulta = sConsulta & "   AND CL.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "IF @PROVE is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "           AND PROVE.COD = @PROVE'" & vbCrLf
sConsulta = sConsulta & "IF @COD is not null and @cod!=''" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @COINCIDENCIA=0 " & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING= @SQLSTRING +' AND ISNULL(CL.ART_INT,'''') + ISNULL(I.ART,'''') =  @COD '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING= @SQLSTRING +' AND ISNULL(CL.ART_INT,'''') + ISNULL(I.ART,'''') LIKE ''%'' + @COD + ''%'''" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "IF @DEN is not null and @den!=''" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=0 " & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING = @SQLSTRING + ' AND ISNULL(I.DESCR,'''') + ISNULL(A.DEN,'''') + ISNULL(ART_AUX.DEN,'''') =  @DEN '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING = @SQLSTRING + ' AND ISNULL(I.DESCR,'''') + ISNULL(A.DEN,'''') + ISNULL(ART_AUX.DEN,'''') LIKE ''%'' + @DEN + ''%'''" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CL.CAT1 in (' + @CATN1 + ')'" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CL.CAT2 in (' + @CATN2 + ')'" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CL.CAT3 in (' + @CATN3 + ')'" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CL.CAT4 in (' + @CATN4 + ')'" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CL.CAT5 in (' + @CATN5 + ')'" & vbCrLf
sConsulta = sConsulta & "IF @PRECIOMAX is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CL.PRECIO_UC<=@PRECIOMAX'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEST IS NOT NULL" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CL.DEST = @DEST '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  IF @COINCIDENCIA = 1" & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "        AND PA4.COD_EXT LIKE ''%'' + @CODEXT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "        AND PA4.COD_EXT =  @CODEXT '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @OP VARCHAR(2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DIREC='ASC'" & vbCrLf
sConsulta = sConsulta & "  SET @OP = '>='" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @OP = '<='" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID <> '0'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'COD'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING = @SQLSTRING + ' AND isnull(CL.art_int,'''') + isnull(I.ART,'''') +  convert(varchar,CL.ID)  ' + @OP + '@ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'DEN'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING = @SQLSTRING + ' AND ISNULL(ART_AUX.DEN,isnull(A.DEN,'''')) + isnull(I.DESCR,'''') +  convert(varchar,CL.ID)  ' + @OP + '@ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'PRO'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING = @SQLSTRING + ' AND PROVE.DEN +  convert(varchar,CL.ID)  ' + @OP + '@ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'PRE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING = @SQLSTRING + ' AND LEFT(''0000000000000000000000000'', 25 - LEN(CAST(CAST(PRECIO_UC AS DECIMAL(25,5)) AS VARCHAR(25)))) + CAST(CAST(PRECIO_UC AS DECIMAL(25,5)) AS VARCHAR(25))  + CAST(CL.ID AS VARCHAR) ' + @OP + '@ID'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'COD'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING = @SQLSTRING + ' ORDER BY CL.art_int + I.ART +  convert(varchar,CL.ID) '  + @DIREC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'DEN'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING = @SQLSTRING + ' ORDER BY ISNULL(ART_AUX.DEN,A.DEN) + I.DESCR +  convert(varchar,CL.ID) ' + @DIREC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'PRO'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING = @SQLSTRING + ' ORDER BY PROVE.DEN +  convert(varchar,CL.ID) ' + @DIREC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDER = 'PRE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING = @SQLSTRING + ' ORDER BY LEFT(''0000000000000000000000000'', 25 - LEN(CAST(CAST(PRECIO_UC AS DECIMAL(25,5)) AS VARCHAR(25)))) + CAST(CAST(PRECIO_UC AS DECIMAL(25,5)) AS VARCHAR(25))  + CAST(CL.ID AS VARCHAR) ' + @DIREC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = '@COD VARCHAR(40), @DEN VARCHAR(300), @PROVE VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@PRECIOMAX FLOAT,@PER VARCHAR(50),@ID VARCHAR(200), @DEST VARCHAR(50),@CODEXT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD=@COD, " & vbCrLf
sConsulta = sConsulta & "@DEN=@DEN, " & vbCrLf
sConsulta = sConsulta & "@PROVE=@PROVE," & vbCrLf
sConsulta = sConsulta & "@PRECIOMAX=@PRECIOMAX, " & vbCrLf
sConsulta = sConsulta & "@PER=@PER," & vbCrLf
sConsulta = sConsulta & "@ID=@ID," & vbCrLf
sConsulta = sConsulta & "@DEST = @DEST," & vbCrLf
sConsulta = sConsulta & "@CODEXT = @CODEXT" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_VALIDAR_USUARIO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_VALIDAR_USUARIO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  FSEP_VALIDAR_USUARIO (@USU VARCHAR(80),@PWD VARCHAR(255),@BLOQUEO INTEGER)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM_INTENTOS AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMARTICULOS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTETOTAL FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMARTICULOS = COUNT(*) , @IMPORTETOTAL = ISNULL(SUM(isnull(CANT,0)*isnull(PREC,0)*isnull(FC,1)),0)" & vbCrLf
sConsulta = sConsulta & "  FROM CESTA " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN USU " & vbCrLf
sConsulta = sConsulta & "            ON USU.PER = CESTA.PER" & vbCrLf
sConsulta = sConsulta & " WHERE USU.COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "If @BLOQUEO > 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @NUM_INTENTOS = (SELECT USU.BLOQ FROM USU WHERE USU.COD=@USU AND USU.PWD=@PWD AND USU.BLOQ<@BLOQUEO)" & vbCrLf
sConsulta = sConsulta & "    IF (@NUM_INTENTOS IS NULL)" & vbCrLf
sConsulta = sConsulta & "       UPDATE USU SET BLOQ=BLOQ+1 WHERE USU.COD=@USU" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @NUM_INTENTOS < @BLOQUEO" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT USU.COD, PER.COD AS PER, PER.NOM, USU.THOUSANFMT, USU.DECIMALFMT, USU.PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                   USU.MOSTRARFMT,USU.DATEFMT,FSEP,USU.MON,USU.FSEPTIPO,MON.EQUIV EQUIV,USU.TIPOEMAIL,USU.IDIOMA," & vbCrLf
sConsulta = sConsulta & "                   USU.DEST,USU.DEST_OTRAS_UO,USU.SOL_COMPRA, MOSTIMGART, MOSTATRIB, MOSTCANTMIN, MOSTCODPROVE, MOSTCODART ," & vbCrLf
sConsulta = sConsulta & "                   @NUMARTICULOS NUMARTICULOS, @IMPORTETOTAL IMPORTETOTAL, ORDEN, DIREC, PEDLIBRE,PED_OTRAS_EMP,MODIFPREC" & vbCrLf
sConsulta = sConsulta & "              FROM USU INNER JOIN PER ON USU.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN MON ON USU.MON =MON.COD" & vbCrLf
sConsulta = sConsulta & "             WHERE USU.COD =@USU AND USU.PWD=@PWD AND FSEP>0 AND BAJA = 0" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "            UPDATE USU SET BLOQ=0 WHERE USU.COD=@USU" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT USU.COD, PER.COD AS PER, PER.NOM, USU.THOUSANFMT, USU.DECIMALFMT, USU.PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "    USU.MOSTRARFMT,USU.DATEFMT,FSEP,USU.MON,USU.FSEPTIPO,MON.EQUIV EQUIV, USU.TIPOEMAIL, USU.IDIOMA," & vbCrLf
sConsulta = sConsulta & "    USU.DEST,USU.DEST_OTRAS_UO,USU.SOL_COMPRA, MOSTIMGART, MOSTATRIB, MOSTCANTMIN, MOSTCODPROVE, MOSTCODART ," & vbCrLf
sConsulta = sConsulta & "    @NUMARTICULOS NUMARTICULOS, @IMPORTETOTAL IMPORTETOTAL, ORDEN, DIREC, PEDLIBRE,PED_OTRAS_EMP,MODIFPREC" & vbCrLf
sConsulta = sConsulta & "     From USU INNER JOIN PER ON USU.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "     inner join MON ON USU.MON =MON.COD" & vbCrLf
sConsulta = sConsulta & "    WHERE USU.COD =@USU AND USU.PWD=@PWD AND FSEP>0 AND BAJA = 0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_CARGAR_CESTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_CARGAR_CESTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_CARGAR_CESTA @PER VARCHAR(50), @IDIOMA VARCHAR(3) = 'SPA', @ORDER VARCHAR(50) = 'TIPO, ART_DEN' AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEST VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTDEF VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @DEST = DEST" & vbCrLf
sConsulta = sConsulta & "  FROM USU " & vbCrLf
sConsulta = sConsulta & " WHERE USU.PER = @PER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @DESTDEF = DESTDEF" & vbCrLf
sConsulta = sConsulta & "  FROM PARGEN_DEF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = N'SELECT T.*, C1.DEN DEN1, C1.OBL OBL1, C2.DEN DEN2, C2.OBL OBL2 FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT C.ID, C.LINEA, CL.PROVE, P.DEN PROVEDEN, ART_INT + I.ART COD_ITEM,CASE WHEN ART_AUX.DEN IS NULL THEN ART4.DEN ELSE ART_AUX.DEN END + I.DESCR ART_DEN, " & vbCrLf
sConsulta = sConsulta & "C.CANT, ISNULL(C.UP,CL.UP_DEF) UP, ISNULL(C.PREC,CL.PRECIO_UC) PREC,  ISNULL(C.FC,CL.FC_DEF) FC, ISNULL(CLM.CANT_MIN,CL.CANT_MIN_DEF) CANT_MIN_DEF, C.TIPO, " & vbCrLf
sConsulta = sConsulta & "CL.GMN1,CL.GMN2,CL.GMN3,CL.GMN4, CASE DEST_USU WHEN 1 THEN ISNULL(@DEST,CL.DEST) ELSE CL.DEST END DEST,CL.CAT1, CL.CAT2, CL.CAT3, CL.CAT4, CL.CAT5," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.campo1 else c2.campo1 end else c3.campo1 end else c4.campo1 end else c5.campo1 end CAMPO1," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.campo2 else c2.campo2 end else c3.campo2 end else c4.campo2 end else c5.campo2 end CAMPO2," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.modifprec else c2.modifprec end else c3.modifprec end else c4.modifprec end else c5.modifprec end MODIF_PREC" & vbCrLf
sConsulta = sConsulta & "FROM CESTA C" & vbCrLf
sConsulta = sConsulta & "INNER JOIN (CATALOG_LIN CL" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM I ON CL.GMN1=I.GMN1 AND CL.ANYO=I.ANYO AND CL.PROCE=I.PROCE AND CL.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 ON CL.GMN1=ART4.GMN1 AND CL.GMN2=ART4.GMN2 AND CL.GMN3=ART4.GMN3 AND CL.GMN4=ART4.GMN4 AND CL.ART_INT + I.ART=ART4.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 ART_AUX ON I.GMN1=ART_AUX.GMN1 AND I.GMN2=ART_AUX.GMN2 AND I.GMN3=ART_AUX.GMN3 AND I.GMN4=ART_AUX.GMN4 AND I.ART=ART_AUX.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN1 C1 ON CL.CAT1 = C1.ID LEFT JOIN CATN2 C2 ON CL.CAT2 = C2.ID LEFT JOIN CATN3 C3 ON CL.CAT3 = C3.ID LEFT JOIN CATN4 C4 ON CL.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN5 C5 ON CL.CAT5 = C5.ID INNER JOIN PROVE P ON Cl.PROVE=P.COD" & vbCrLf
sConsulta = sConsulta & ") ON C.LINEA = CL.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAT_LIN_MIN CLM ON C.LINEA = CLM.LINEA AND C.UP = CLM.UP WHERE PER=@PER AND TIPO = 0" & vbCrLf
sConsulta = sConsulta & "union SELECT C.ID, NULL LINEA, C.PROVE, P.DEN PROVEDEN, NULL COD_ITEM, DESCR ART_DEN,C.CANT, C.UP, C.PREC, 1 FC, NULL CANT_MIN_DEF, C.TIPO, NULL GMN1, NULL GMN2,NULL GMN3,NULL GMN4, ISNULL(@DEST,@DESTDEF) DEST," & vbCrLf
sConsulta = sConsulta & "C.CAT1, C.CAT2, C.CAT3, C.CAT4, C.CAT5," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.campo1 else c2.campo1 end else c3.campo1 end else c4.campo1 end else c5.campo1 end CAMPO1," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.campo2 else c2.campo2 end else c3.campo2 end else c4.campo2 end else c5.campo2 end CAMPO2," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.modifprec else c2.modifprec end else c3.modifprec end else c4.modifprec end else c5.modifprec end MODIF_PREC" & vbCrLf
sConsulta = sConsulta & "FROM CESTA C LEFT JOIN CATN1 C1 ON C.CAT1 = C1.ID LEFT JOIN CATN2 C2 ON C.CAT2 = C2.ID LEFT JOIN CATN3 C3 ON C.CAT3 = C3.ID LEFT JOIN CATN4 C4 ON C.CAT4 = C4.ID LEFT JOIN CATN5 C5 ON C.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE P ON C.PROVE=P.COD WHERE PER=@PER AND TIPO = 1" & vbCrLf
sConsulta = sConsulta & ") t LEFT JOIN CATALOG_CAMPOS C1 on T.CAMPO1 = C1.ID AND C1.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS C2 ON T.CAMPO2 = C2.ID AND C2.IDIOMA = @IDIOMA " & vbCrLf
sConsulta = sConsulta & "ORDER BY ' + @ORDER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@DEST varchar(50), @IDIOMA VARCHAR(3), @PER VARCHAR(50),@DESTDEF varchar(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @DEST= @DEST, @IDIOMA = @IDIOMA, @PER = @PER, @DESTDEF=@DESTDEF" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_ACTUALIZAR_CATALOGO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_ACTUALIZAR_CATALOGO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_ACTUALIZAR_CATALOGO(@PEDIDO AS INTEGER,@ORDEN AS INTEGER,@PER AS VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINPED AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PREC FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FC FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINCAT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT1 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT2 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT3 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT4 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT5 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @MODIFPREC AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTUALIZAR AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF ISNULL((SELECT MODIFPREC FROM USU WHERE PER = @PER),0) = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     /*Selecciona para las lineas*/" & vbCrLf
sConsulta = sConsulta & "    DECLARE C1 CURSOR LOCAL FOR SELECT DISTINCT L.ID,L.PREC_UP PREC,L.FC,CL.ID,CL.CAT1,CL.CAT2,CL.CAT3,CL.CAT4,CL.CAT5 FROM LINEAS_PEDIDO L " & vbCrLf
sConsulta = sConsulta & "                INNER JOIN ORDEN_ENTREGA O ON O.ID=L.ORDEN AND O.EST=2" & vbCrLf
sConsulta = sConsulta & "                INNER JOIN CATALOG_LIN CL ON L.LINCAT=CL.ID" & vbCrLf
sConsulta = sConsulta & "                     WHERE L.ORDEN=@ORDEN" & vbCrLf
sConsulta = sConsulta & "    OPEN C1" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @LINPED,@PREC,@FC,@LINCAT,@CAT1,@CAT2,@CAT3,@CAT4,@CAT5" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @CAT5 IS NULL" & vbCrLf
sConsulta = sConsulta & "                IF @CAT4 IS NULL" & vbCrLf
sConsulta = sConsulta & "                    IF @CAT3 IS NULL" & vbCrLf
sConsulta = sConsulta & "                         IF @CAT2 IS NULL" & vbCrLf
sConsulta = sConsulta & "                                SELECT @MODIFPREC=MODIFPREC,@ACTUALIZAR=ACTUALIZARPREC FROM CATN1 WHERE ID=@CAT1 " & vbCrLf
sConsulta = sConsulta & "                         ELSE" & vbCrLf
sConsulta = sConsulta & "                                SELECT @MODIFPREC=MODIFPREC,@ACTUALIZAR=ACTUALIZARPREC FROM CATN2 WHERE ID=@CAT2" & vbCrLf
sConsulta = sConsulta & "                    ELSE" & vbCrLf
sConsulta = sConsulta & "                          SELECT @MODIFPREC=MODIFPREC,@ACTUALIZAR=ACTUALIZARPREC FROM CATN3 WHERE ID=@CAT3" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                     SELECT @MODIFPREC=MODIFPREC,@ACTUALIZAR=ACTUALIZARPREC FROM CATN4 WHERE ID=@CAT4 " & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                  SELECT @MODIFPREC=MODIFPREC,@ACTUALIZAR=ACTUALIZARPREC FROM CATN5 WHERE ID=@CAT5" & vbCrLf
sConsulta = sConsulta & "           IF @MODIFPREC=1 AND @ACTUALIZAR=1         " & vbCrLf
sConsulta = sConsulta & "                   IF  (SELECT PRECIO_UC FROM CATALOG_LIN WHERE ID=@LINCAT) <> (@PREC/@FC) " & vbCrLf
sConsulta = sConsulta & "                   begin" & vbCrLf
sConsulta = sConsulta & "                   UPDATE CATALOG_LIN SET PRECIO_UC=@PREC/@FC  WHERE ID=@LINCAT" & vbCrLf
sConsulta = sConsulta & "                   UPDATE FAVORITOS_LINEAS_PEDIDO SET PREC_UP=@PREC WHERE LINEA=@LINCAT AND PREC_CAMBIADO=0" & vbCrLf
sConsulta = sConsulta & "                   end " & vbCrLf
sConsulta = sConsulta & "          FETCH NEXT FROM C1 INTO @LINPED,@PREC,@FC,@LINCAT,@CAT1,@CAT2,@CAT3,@CAT4,@CAT5" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    CLOSE C1" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     /*Selecciona para las lineas*/" & vbCrLf
sConsulta = sConsulta & "    DECLARE C1 CURSOR LOCAL FOR SELECT DISTINCT L.ID,L.PREC_UP PREC,L.FC,CL.ID,CL.CAT1,CL.CAT2,CL.CAT3,CL.CAT4,CL.CAT5 FROM LINEAS_PEDIDO L " & vbCrLf
sConsulta = sConsulta & "                INNER JOIN ORDEN_ENTREGA O ON O.ID=L.ORDEN AND O.EST=2" & vbCrLf
sConsulta = sConsulta & "                INNER JOIN CATALOG_LIN CL ON L.LINCAT=CL.ID" & vbCrLf
sConsulta = sConsulta & "                     WHERE L.PEDIDO=@PEDIDO" & vbCrLf
sConsulta = sConsulta & "    OPEN C1" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @LINPED,@PREC,@FC,@LINCAT,@CAT1,@CAT2,@CAT3,@CAT4,@CAT5" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @CAT5 IS NULL" & vbCrLf
sConsulta = sConsulta & "                IF @CAT4 IS NULL" & vbCrLf
sConsulta = sConsulta & "                    IF @CAT3 IS NULL" & vbCrLf
sConsulta = sConsulta & "                         IF @CAT2 IS NULL" & vbCrLf
sConsulta = sConsulta & "                                SELECT @MODIFPREC=MODIFPREC,@ACTUALIZAR=ACTUALIZARPREC FROM CATN1 WHERE ID=@CAT1 " & vbCrLf
sConsulta = sConsulta & "                         ELSE" & vbCrLf
sConsulta = sConsulta & "                                SELECT @MODIFPREC=MODIFPREC,@ACTUALIZAR=ACTUALIZARPREC FROM CATN2 WHERE ID=@CAT2" & vbCrLf
sConsulta = sConsulta & "                    ELSE" & vbCrLf
sConsulta = sConsulta & "                          SELECT @MODIFPREC=MODIFPREC,@ACTUALIZAR=ACTUALIZARPREC FROM CATN3 WHERE ID=@CAT3" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                     SELECT @MODIFPREC=MODIFPREC,@ACTUALIZAR=ACTUALIZARPREC FROM CATN4 WHERE ID=@CAT4 " & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                  SELECT @MODIFPREC=MODIFPREC,@ACTUALIZAR=ACTUALIZARPREC FROM CATN5 WHERE ID=@CAT5" & vbCrLf
sConsulta = sConsulta & "           IF @MODIFPREC=1 AND @ACTUALIZAR=1 " & vbCrLf
sConsulta = sConsulta & "                   IF  (SELECT PRECIO_UC FROM CATALOG_LIN WHERE ID=@LINCAT) <> (@PREC/@FC) " & vbCrLf
sConsulta = sConsulta & "                   begin" & vbCrLf
sConsulta = sConsulta & "                   UPDATE CATALOG_LIN SET PRECIO_UC=@PREC/@FC  WHERE ID=@LINCAT" & vbCrLf
sConsulta = sConsulta & "                   UPDATE FAVORITOS_LINEAS_PEDIDO SET PREC_UP=@PREC WHERE LINEA=@LINCAT AND PREC_CAMBIADO=0" & vbCrLf
sConsulta = sConsulta & "                   end " & vbCrLf
sConsulta = sConsulta & "          FETCH NEXT FROM C1 INTO @LINPED,@PREC,@FC,@LINCAT,@CAT1,@CAT2,@CAT3,@CAT4,@CAT5" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    CLOSE C1" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ARTICULO_FAVORITO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ARTICULO_FAVORITO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ARTICULO_FAVORITO @ID INT ,@PER VARCHAR(50),@IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEST AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTDEF AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @DEST =DEST  FROM USU  WHERE USU.PER=@PER" & vbCrLf
sConsulta = sConsulta & "SELECT @DESTDEF =DESTDEF FROM PARGEN_DEF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select T.*,C1.DEN DEN1, C1.OBL OBL1, C2.DEN DEN2, C2.OBL OBL2" & vbCrLf
sConsulta = sConsulta & "from " & vbCrLf
sConsulta = sConsulta & "(SELECT CL.ID,CASE DEST_USU WHEN 1 THEN ISNULL(@DEST,CL.DEST) else CL.DEST END DEST," & vbCrLf
sConsulta = sConsulta & "CL.PROVE,CL.GMN1,CL.GMN2,CL.GMN3,CL.GMN4,CL.CAT1, CL.CAT2, CL.CAT3, CL.CAT4, CL.CAT5," & vbCrLf
sConsulta = sConsulta & "P.DEN PROVEDEN,CL.UP_DEF UP, CL.PRECIO_UC PREC,  CL.FC_DEF FC,ART_INT + I.ART COD_ITEM," & vbCrLf
sConsulta = sConsulta & "CASE WHEN ART_AUX.DEN IS NULL THEN ART4.DEN ELSE ART_AUX.DEN END + I.DESCR ART_DEN," & vbCrLf
sConsulta = sConsulta & "ISNULL(CLM.CANT_MIN,CL.CANT_MIN_DEF) CANT_MIN_DEF," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "  case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "      case when c2.cod is null then " & vbCrLf
sConsulta = sConsulta & "        c1.campo1 else c2.campo1 end" & vbCrLf
sConsulta = sConsulta & "    else c3.campo1 end" & vbCrLf
sConsulta = sConsulta & "  else c4.campo1 end" & vbCrLf
sConsulta = sConsulta & "else c5.campo1 end CAMPO1," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "  case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "      case when c2.cod is null then " & vbCrLf
sConsulta = sConsulta & "        c1.campo2 else c2.campo2 end" & vbCrLf
sConsulta = sConsulta & "    else c3.campo2 end" & vbCrLf
sConsulta = sConsulta & "  else c4.campo2 end" & vbCrLf
sConsulta = sConsulta & "else c5.campo2 end CAMPO2," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "       case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "           case when c2.cod is null then  c1.MODIFPREC  " & vbCrLf
sConsulta = sConsulta & "           else  c2.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & "       else  c3.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & "    else  c4.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & " else  c5.MODIFPREC end MODIF_PREC," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "       case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "           case when c2.cod is null then  c1.ACTUALIZARPREC  " & vbCrLf
sConsulta = sConsulta & "           else  c2.ACTUALIZARPREC end" & vbCrLf
sConsulta = sConsulta & "       else  c3.ACTUALIZARPREC end" & vbCrLf
sConsulta = sConsulta & "    else  c4.ACTUALIZARPREC end" & vbCrLf
sConsulta = sConsulta & " else  c5.ACTUALIZARPREC end ACTUALIZAR_PREC" & vbCrLf
sConsulta = sConsulta & "FROM CATALOG_LIN CL " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM I ON CL.GMN1=I.GMN1 AND CL.ANYO=I.ANYO AND CL.PROCE=I.PROCE AND CL.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "  ON CL.GMN1=ART4.GMN1 AND CL.GMN2=ART4.GMN2 AND CL.GMN3=ART4.GMN3 AND CL.GMN4=ART4.GMN4 AND CL.ART_INT + I.ART=ART4.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "  ON I.GMN1=ART_AUX.GMN1 AND I.GMN2=ART_AUX.GMN2 AND I.GMN3=ART_AUX.GMN3 AND I.GMN4=ART_AUX.GMN4 AND I.ART=ART_AUX.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN1 C1 ON CL.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN2 C2 ON CL.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN3 C3 ON CL.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN4 C4 ON CL.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN5 C5 ON CL.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE P ON Cl.PROVE=P.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAT_LIN_MIN CLM ON CL.ID = CLM.LINEA AND CL.UP_DEF = CLM.UP" & vbCrLf
sConsulta = sConsulta & ") T" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS C1" & vbCrLf
sConsulta = sConsulta & "        on T.CAMPO1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "       AND C1.IDIOMA = @IDI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS C2" & vbCrLf
sConsulta = sConsulta & "       ON T.CAMPO2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "       AND C2.IDIOMA = @IDI" & vbCrLf
sConsulta = sConsulta & "where T.ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN_FAVORITOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN_FAVORITOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_LINEAS_DE_ORDEN_FAVORITOS(@ORDENID INTEGER,@IDI varchar(20) = 'SPA')  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID LINEAID,LP.USU," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + I.ART CODART," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "                LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + I.DESCR  + ART_AUX.DEN + + LP.DESCR_LIBRE DENART," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "                D.DEN DESTDEN," & vbCrLf
sConsulta = sConsulta & "                LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "                U.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS, " & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "                C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "                C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "                C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "                C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "                C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "                I.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "                I.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                I.ID ITEM,LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "                LP.VALOR1,LP.VALOR2,CC1.DEN AS DENCAMPO1,CC2.DEN AS DENCAMPO2,CC1.OBL AS OBL1,CC2.OBL AS OBL2,CC1.ID AS CAMPO1,CC2.ID AS CAMPO2," & vbCrLf
sConsulta = sConsulta & "       PROCE.DEN AS DENPROCE,LP.OBSADJUN, LP.LINEA, ISNULL(CLM.CANT_MIN,CL.CANT_MIN_DEF) CANT_MIN, ISNULL(LP.PREC_UP,CL.PRECIO_UC*CL.FC_DEF) PRECIO_UP, LP.FC," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.MODIFPREC" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.MODIFPREC" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.MODIFPREC" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.MODIFPREC" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.MODIFPREC" & vbCrLf
sConsulta = sConsulta & "                    end MODIF_PREC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " FROM FAVORITOS_LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DEST D" & vbCrLf
sConsulta = sConsulta & "             ON LP.DEST = D.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN UNI U" & vbCrLf
sConsulta = sConsulta & "             ON LP.UP = U.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA " & vbCrLf
sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM FAVORITOS_LINEAS_PEDIDO_ADJUN GROUP BY FAVORITOS_LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC1 " & vbCrLf
sConsulta = sConsulta & "       ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC2 " & vbCrLf
sConsulta = sConsulta & "            ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "       ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_LIN CL" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEA = CL.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CAT_LIN_MIN CLM" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEA = CLM.LINEA" & vbCrLf
sConsulta = sConsulta & "            AND LP.UP = CLM.UP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_COMPARAR_ARTICULOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_COMPARAR_ARTICULOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_COMPARAR_ARTICULOS AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOATRIB tinyint" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODATRIB varchar(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDATRIB int" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAMDEF NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ALTER NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSERT NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT PAA.ATRIB, DA.COD, DA.TIPO" & vbCrLf
sConsulta = sConsulta & "  FROM PROVE_ART4_ATRIB PAA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN #ARTS_COMP T" & vbCrLf
sConsulta = sConsulta & "               ON PAA.PROVE = T.PROVE" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN1 = T.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN2 = T.GMN2" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN3 = T.GMN3" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN4 = T.GMN4" & vbCrLf
sConsulta = sConsulta & "              AND PAA.ART = T.ART" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PAA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #ATRIBS (ATID INT NULL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS(SELECT @@VERSION WHERE @@VERSION LIKE '%2000%')" & vbCrLf
sConsulta = sConsulta & "SET @SQL='" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE #ATRIBS ADD " & vbCrLf
sConsulta = sConsulta & "   ATPROVE VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN1 varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN2 varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN3 varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN4 varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ," & vbCrLf
sConsulta = sConsulta & "   ATART varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL " & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL='" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE #ATRIBS ADD " & vbCrLf
sConsulta = sConsulta & "   ATPROVE VARCHAR(50) NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN1 varchar (50) NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN2 varchar (50) NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN3 varchar (50) NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN4 varchar (50) NULL ," & vbCrLf
sConsulta = sConsulta & "   ATART varchar (50) NULL" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL  @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INSERT = 'INSERT INTO #ATRIBS (ATID, ATPROVE, ATGMN1, ATGMN2, ATGMN3, ATGMN4, ATART, '" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT T.ID, PAA.PROVE, PAA.GMN1, PAA.GMN2, PAA.GMN3, PAA.GMN4, PAA.ART, '" & vbCrLf
sConsulta = sConsulta & "set @ALTER = 'ALTER TABLE #ATRIBS ADD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @IDATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    SET @INSERT = @INSERT + '[AT_' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "    IF @TIPOATRIB = 1 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] VARCHAR(800), '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case PAA.ATRIB when ' + cast(@IDATRIB as varchar(100)) + ' THEN VALOR_TEXT else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 2" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] FLOAT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case PAA.ATRIB when ' + cast(@IDATRIB as varchar(100)) + ' THEN VALOR_NUM else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 3" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] DATETIME, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case PAA.ATRIB when ' + cast(@IDATRIB as varchar(100)) + ' THEN VALOR_FEC else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER + '[AT_' + @CODATRIB + '] TINYINT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case PAA.ATRIB when ' + cast(@IDATRIB as varchar(100)) + ' THEN VALOR_BOOL else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @IDATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "if @INSERT != 'INSERT INTO #ATRIBS (ATID, ATPROVE, ATGMN1, ATGMN2, ATGMN3, ATGMN4, ATART, '" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    set @SQL = LEFT(@SQL,LEN(@SQL)-1)" & vbCrLf
sConsulta = sConsulta & "    set @INSERT = LEFT(@INSERT,LEN(@INSERT)-1)" & vbCrLf
sConsulta = sConsulta & "    set @ALTER = LEFT(@ALTER,LEN(@ALTER)-1)" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @ALTER " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @INSERT + ')" & vbCrLf
sConsulta = sConsulta & "    ' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "      FROM PROVE_ART4_ATRIB PAA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN #ARTS_COMP T" & vbCrLf
sConsulta = sConsulta & "               ON PAA.PROVE = T.PROVE" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN1 = T.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN2 = T.GMN2" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN3 = T.GMN3" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN4 = T.GMN4" & vbCrLf
sConsulta = sConsulta & "              AND PAA.ART = T.ART" & vbCrLf
sConsulta = sConsulta & "    GROUP BY T.ID, PAA.PROVE, PAA.GMN1, PAA.GMN2, PAA.GMN3, PAA.GMN4, PAA.ART'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @SQL" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SELECT AC.ID, P.DEN PROVEDEN,  CASE WHEN ART_AUX.DEN IS NULL THEN A4.DEN ELSE ART_AUX.DEN END + I.DESCR  ARTDEN, CL.PRECIO_UC, CL.UP_DEF, CL.FC_DEF,CL.CANT_MIN_DEF, DATALENGTH(PA.IMAGEN) DATASIZE,  A.*," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "       case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "           case when c2.cod is null then  c1.MODIFPREC  " & vbCrLf
sConsulta = sConsulta & "           else  c2.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & "       else  c3.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & "    else  c4.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & " else  c5.MODIFPREC end MODIF_PREC" & vbCrLf
sConsulta = sConsulta & "  FROM #ARTS_COMP AC" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN" & vbCrLf
sConsulta = sConsulta & "              (CATALOG_LIN CL " & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                           ON CL.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.ANYO=I.ANYO " & vbCrLf
sConsulta = sConsulta & "                          AND CL.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "                          AND CL.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN ART4 A4" & vbCrLf
sConsulta = sConsulta & "                           ON CL.GMN1=A4.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN2=A4.GMN2 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN3=A4.GMN3 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN4=A4.GMN4 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.ART_INT + I.ART=A4.COD " & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "                           ON I.GMN1=ART_AUX.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND I.GMN2=ART_AUX.GMN2 " & vbCrLf
sConsulta = sConsulta & "                          AND I.GMN3=ART_AUX.GMN3 " & vbCrLf
sConsulta = sConsulta & "                          AND I.GMN4=ART_AUX.GMN4 " & vbCrLf
sConsulta = sConsulta & "                          AND I.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "                           ON CL.GMN1=PA.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN2=PA.GMN2 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN3=PA.GMN3 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN4=PA.GMN4 " & vbCrLf
sConsulta = sConsulta & "                          AND CASE WHEN CL.ART_INT IS NULL THEN I.ART ELSE CL.ART_INT  END =PA.ART " & vbCrLf
sConsulta = sConsulta & "                          AND CL.PROVE=PA.PROVE" & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN CATN1 C1   ON CL.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN CATN2 C2   ON CL.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN CATN3 C3   ON CL.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN CATN4 C4   ON CL.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN CATN5 C5   ON CL.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "             ON AC.ID = CL.ID" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN #ATRIBS A" & vbCrLf
sConsulta = sConsulta & "           ON AC.ID = A.ATID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "           ON AC.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "set nocount off" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ARTICULO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ARTICULO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ARTICULO @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT catalog_lin.art_int + ITEM.ART +  convert(varchar,catalog_lin.ID) VID , catalog_lin.art_int + ITEM.ART TOOLTIP," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.ID,CATALOG_LIN.ANYO,CATALOG_LIN.PROCE,CATALOG_LIN.ITEM," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.PROVE,ART_INT + ITEM.ART COD_ITEM," & vbCrLf
sConsulta = sConsulta & "CASE WHEN ART_AUX.DEN IS NULL THEN ART4.DEN ELSE ART_AUX.DEN END + ITEM.DESCR ART_DEN," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.DEST,UC,PROVE_ART4.COD_EXT," & vbCrLf
sConsulta = sConsulta & "CANT_ADJ,PRECIO_UC,UP_DEF,FC_DEF," & vbCrLf
sConsulta = sConsulta & "CANT_MIN_DEF,PUB,FECHA_DESPUB,CATALOG_LIN.CAT1,CATALOG_LIN.CAT2,CATALOG_LIN.CAT3,CATALOG_LIN.CAT4,CATALOG_LIN.CAT5,CATALOG_LIN.SEGURIDAD," & vbCrLf
sConsulta = sConsulta & "PROVE.DEN  AS PROVEDEN," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.GMN1,CATALOG_LIN.GMN2,CATALOG_LIN.GMN3,CATALOG_LIN.GMN4,APROB_LIM.MOD_DEST, DATALENGTH(PROVE_ART4.THUMBNAIL) THUMBNAIL, CATALOG_LIN.ESP, " & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "  case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "      case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "        c1.cod" & vbCrLf
sConsulta = sConsulta & "      else" & vbCrLf
sConsulta = sConsulta & "        c2.cod" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "    else" & vbCrLf
sConsulta = sConsulta & "      c3.cod" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "  else" & vbCrLf
sConsulta = sConsulta & "    c4.cod" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "  c5.cod" & vbCrLf
sConsulta = sConsulta & "end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "  case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c3.cod is null then  " & vbCrLf
sConsulta = sConsulta & "      case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "        '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "      else" & vbCrLf
sConsulta = sConsulta & "        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "    else" & vbCrLf
sConsulta = sConsulta & "      '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "  else" & vbCrLf
sConsulta = sConsulta & "    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "    case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "       case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "           case when c2.cod is null then  c1.MODIFPREC  " & vbCrLf
sConsulta = sConsulta & "           else  c2.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & "       else  c3.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & "    else  c4.MODIFPREC end" & vbCrLf
sConsulta = sConsulta & " else  c5.MODIFPREC end MODIF_PREC" & vbCrLf
sConsulta = sConsulta & " FROM CATALOG_LIN " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.SEGURIDAD=APROB_LIM.SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND CATALOG_LIN.ANYO=ITEM.ANYO " & vbCrLf
sConsulta = sConsulta & "           AND CATALOG_LIN.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND CATALOG_LIN.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND CATALOG_LIN.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND CATALOG_LIN.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND CATALOG_LIN.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "           AND CATALOG_LIN.ART_INT + ITEM.ART=ART4.COD " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "            ON ITEM.GMN1=ART_AUX.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND ITEM.GMN2=ART_AUX.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND ITEM.GMN3=ART_AUX.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND ITEM.GMN4=ART_AUX.GMN4 " & vbCrLf
sConsulta = sConsulta & "           AND ITEM.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROVE_ART4 " & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.GMN1=PROVE_ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND CATALOG_LIN.GMN2=PROVE_ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND CATALOG_LIN.GMN3=PROVE_ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND CATALOG_LIN.GMN4=PROVE_ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "           AND CASE WHEN CATALOG_LIN.ART_INT IS NULL THEN ITEM.ART ELSE CATALOG_LIN.ART_INT  END =PROVE_ART4.ART " & vbCrLf
sConsulta = sConsulta & "           AND CATALOG_LIN.PROVE=PROVE_ART4.PROVE" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & " WHERE PUB=1" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_17_00_07A2_17_00_08() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificar tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_17_Tablas_8

frmProgreso.lblDetalle = "Modificar triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_17_Triggers_8

frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_17_Storeds_8

sConsulta = "UPDATE VERSION SET NUM ='2.17.00.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_17_00_07A2_17_00_08 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_17_00_07A2_17_00_08 = False

End Function


Private Sub V_2_17_Storeds_8()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_RECEPTORES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_RECEPTORES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_RECEPTORES @PER VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @UON1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "declare @UON2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "declare @UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT DEST_OTRAS_UO FROM USU WHERE PER = @PER)=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    select @UON1 = uon1, @UON2 = uon2, @UON3 = uon3 from per where cod = @PER" & vbCrLf
sConsulta = sConsulta & "    IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "      IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                   ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "         WHERE U.FSEP = 1  and P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "           AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "      ORDER BY P.NOM, P.APE" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                   ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "         WHERE U.FSEP = 1 and P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "           AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON2 = @UON2" & vbCrLf
sConsulta = sConsulta & "      ORDER BY P.NOM, P.APE" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                 ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "       WHERE U.FSEP = 1 and P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "         AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "         AND P.UON2 = @UON2" & vbCrLf
sConsulta = sConsulta & "         AND P.UON3 = @UON3" & vbCrLf
sConsulta = sConsulta & "    ORDER BY P.NOM, P.APE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "             ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "   WHERE U.FSEP = 1 and P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "  ORDER BY P.NOM, P.APE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CARGAR_ADJUDICACIONES_GRUPOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CARGAR_ADJUDICACIONES_GRUPOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_CARGAR_ADJUDICACIONES_GRUPOS @ANYO int, @GMN1 varchar(50), @PROCE int, @FECREU datetime=NULL,@EQP VARCHAR(50)=NULL, @COM VARCHAR(50)=NULL, @ITEM INT =NULL AS" & vbCrLf
sConsulta = sConsulta & "IF @COM IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @ITEM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM_ADJ.OFE,ITEM_ADJ.PORCEN,ITEM.GRUPO, ITEM_ADJ.CATALOGO, ITEM_ADJ.PEDIDO FROM ITEM_ADJ INNER JOIN PROCE_PROVE ON ITEM_ADJ.ANYO=PROCE_PROVE.ANYO AND ITEM_ADJ.GMN1=PROCE_PROVE.GMN1 AND ITEM_ADJ.PROCE=PROCE_PROVE.PROCE AND ITEM_ADJ.PROVE=PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID " & vbCrLf
sConsulta = sConsulta & "                       WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE  AND ITEM_ADJ.ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "                                    AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "                SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM_ADJ.OFE,ITEM_ADJ.PORCEN,ITEM.GRUPO, ITEM_ADJ.CATALOGO, ITEM_ADJ.PEDIDO FROM ITEM_ADJ INNER JOIN PROCE_PROVE ON ITEM_ADJ.ANYO=PROCE_PROVE.ANYO AND ITEM_ADJ.GMN1=PROCE_PROVE.GMN1 AND ITEM_ADJ.PROCE=PROCE_PROVE.PROCE AND ITEM_ADJ.PROVE=PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID " & vbCrLf
sConsulta = sConsulta & "                       WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "      IF @EQP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF @ITEM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                       SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM_ADJ.OFE,ITEM_ADJ.PORCEN ,ITEM.GRUPO, ITEM_ADJ.CATALOGO, ITEM_ADJ.PEDIDO FROM ITEM_ADJ INNER JOIN PROCE_PROVE ON ITEM_ADJ.ANYO=PROCE_PROVE.ANYO AND ITEM_ADJ.GMN1=PROCE_PROVE.GMN1 AND ITEM_ADJ.PROCE=PROCE_PROVE.PROCE AND ITEM_ADJ.PROVE=PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE   AND ITEM_ADJ.ITEM=ITEM.ID " & vbCrLf
sConsulta = sConsulta & "                              WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND PROCE_PROVE.EQP=@EQP" & vbCrLf
sConsulta = sConsulta & "                               AND ITEM_ADJ.ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                       SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM_ADJ.OFE,ITEM_ADJ.PORCEN ,ITEM.GRUPO, ITEM_ADJ.CATALOGO, ITEM_ADJ.PEDIDO FROM ITEM_ADJ INNER JOIN PROCE_PROVE ON ITEM_ADJ.ANYO=PROCE_PROVE.ANYO AND ITEM_ADJ.GMN1=PROCE_PROVE.GMN1 AND ITEM_ADJ.PROCE=PROCE_PROVE.PROCE AND ITEM_ADJ.PROVE=PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID " & vbCrLf
sConsulta = sConsulta & "                              WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND PROCE_PROVE.EQP=@EQP" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "     ELSE " & vbCrLf
sConsulta = sConsulta & "            IF @FECREU IS NULL    --Devolvemos las actuales del proceso" & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 IF @ITEM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                         SELECT ITEM,PROVE,OFE,PORCEN,ITEM.GRUPO, ITEM_ADJ.CATALOGO, ITEM_ADJ.PEDIDO FROM ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID " & vbCrLf
sConsulta = sConsulta & "                            WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE  AND ITEM_ADJ.ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                         SELECT ITEM,PROVE,OFE,PORCEN,ITEM.GRUPO, ITEM_ADJ.CATALOGO, ITEM_ADJ.PEDIDO FROM ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID " & vbCrLf
sConsulta = sConsulta & "                             WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "                BEGIN      --Devolvemos las ocurridas en esa reuni�n" & vbCrLf
sConsulta = sConsulta & "                IF @ITEM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                         SELECT ITEM_ADJREU.ITEM,ITEM_ADJREU.PROVE,ITEM_ADJREU.OFE,ITEM_ADJREU.PORCEN,ITEM.GRUPO, ITEM_ADJ.CATALOGO, ITEM_ADJ.PEDIDO FROM ITEM_ADJREU " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM ON ITEM_ADJREU.ANYO=ITEM.ANYO AND ITEM_ADJREU.GMN1=ITEM.GMN1 AND ITEM_ADJREU.PROCE=ITEM.PROCE AND ITEM_ADJREU.ITEM=ITEM.ID " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN ITEM_ADJ ON ITEM_ADJREU.ANYO=ITEM_ADJ.ANYO AND ITEM_ADJREU.GMN1=ITEM_ADJ.GMN1 AND ITEM_ADJREU.PROCE=ITEM_ADJ.PROCE AND ITEM_ADJREU.ITEM=ITEM_ADJ.ITEM AND ITEM_ADJREU.PROVE=ITEM_ADJ.PROVE" & vbCrLf
sConsulta = sConsulta & "                             WHERE ITEM_ADJREU.ANYO=@ANYO AND ITEM_ADJREU.GMN1=@GMN1 AND ITEM_ADJREU.PROCE=@PROCE AND ITEM_ADJREU.FECREU=@FECREU" & vbCrLf
sConsulta = sConsulta & "                                 AND ITEM_ADJREU.ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                         SELECT ITEM_ADJREU.ITEM,ITEM_ADJREU.PROVE,ITEM_ADJREU.OFE,ITEM_ADJREU.PORCEN,ITEM.GRUPO, ITEM_ADJ.CATALOGO, ITEM_ADJ.PEDIDO FROM ITEM_ADJREU " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN ITEM ON ITEM_ADJREU.ANYO=ITEM.ANYO AND ITEM_ADJREU.GMN1=ITEM.GMN1 AND ITEM_ADJREU.PROCE=ITEM.PROCE  AND ITEM_ADJREU.ITEM=ITEM.ID " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN ITEM_ADJ ON ITEM_ADJREU.ANYO=ITEM_ADJ.ANYO AND ITEM_ADJREU.GMN1=ITEM_ADJ.GMN1 AND ITEM_ADJREU.PROCE=ITEM_ADJ.PROCE AND ITEM_ADJREU.ITEM=ITEM_ADJ.ITEM AND ITEM_ADJREU.PROVE=ITEM_ADJ.PROVE" & vbCrLf
sConsulta = sConsulta & "                             WHERE ITEM_ADJREU.ANYO=@ANYO AND ITEM_ADJREU.GMN1=@GMN1 AND ITEM_ADJREU.PROCE=@PROCE AND ITEM_ADJREU.FECREU=@FECREU" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_MODIFICAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_MODIFICAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_MODIFICAR  @TABLA SMALLINT,@ID INTEGER,@DEN VARCHAR(100)=NULL,@TIPO SMALLINT=NULL, @PREFBAJO TINYINT , @INTRO TINYINT, @MINNUM FLOAT=NULL,@MINFEC DATETIME=NULL,@MAXNUM FLOAT=NULL,@MAXFEC DATETIME=NULL,@ATRIB INTEGER=NULL,@INTERNO  TINYINT=NULL,@AMBITO TINYINT=NULL,@OBLIG  TINYINT=NULL,@OP_PREC VARCHAR(2)=NULL,@APLIC_PREC VARCHAR(50)=NULL,@DEF_PREC TINYINT=NULL,@ANYO VARCHAR(50)=NULL,@GMN1 VARCHAR(50)=NULL,@PROCE VARCHAR(50)=NULL,@GRUPO VARCHAR(50)=NULL , @FECACT DATETIME OUTPUT,@ORDEN SMALLINT=NULL OUTPUT,@PLANT INTEGER=NULL , @DESCR NVARCHAR(4000)=NULL AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITOOLD AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLI_PREC_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIB_PREC AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_PREC AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRIMERO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOOLD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTAS AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=0  " & vbCrLf
sConsulta = sConsulta & "     BEGIN --@tabla=0" & vbCrLf
sConsulta = sConsulta & "            SELECT   @POND=POND,@TIPOOLD=TIPO  FROM DEF_ATRIB  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            IF @POND = 1 AND @TIPO<> 2 and @TIPO<>3  " & vbCrLf
sConsulta = sConsulta & "                         SET @POND = 0" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                          IF @POND = 2 AND (@TIPO=4 OR @INTRO=0)" & vbCrLf
sConsulta = sConsulta & "                                       SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                        IF  @POND = 3 AND @TIPO<> 2 " & vbCrLf
sConsulta = sConsulta & "                                                   SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                        ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    IF @POND =5 AND @TIPO<>4" & vbCrLf
sConsulta = sConsulta & "                                                            begin" & vbCrLf
sConsulta = sConsulta & "                                                            SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                                            end" & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "            IF @TIPOOLD <> @TIPO " & vbCrLf
sConsulta = sConsulta & "                  begin" & vbCrLf
sConsulta = sConsulta & "                  IF @TIPOOLD=2" & vbCrLf
sConsulta = sConsulta & "                        BEGIN" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PROCE_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PLANTILLA_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        END " & vbCrLf
sConsulta = sConsulta & "                  end" & vbCrLf
sConsulta = sConsulta & "            UPDATE DEF_ATRIB SET DEN=@DEN,TIPO=@TIPO,PREFBAJO=@PREFBAJO,POND=@POND,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ,MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10),@MINFEC,111) , DESCR=@DESCR WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            SELECT @FECACT=FECACT FROM DEF_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "      END --@tabla=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "    BEGIN -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "       IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN --@orden=null" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ATRIB_PREC =ATRIB  FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ORDEN=ORDEN FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLIC_PREC AND" & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                      " & vbCrLf
sConsulta = sConsulta & "                 SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                 SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                 SET @PRIMERO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 WHILE @ORDEN_V=0 and @APLI_PREC_V<=3" & vbCrLf
sConsulta = sConsulta & "                         BEGIN -- while" & vbCrLf
sConsulta = sConsulta & "                             SELECT  @ORDEN_V = MAX(ORDEN) FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                             IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                               begin --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                                 IF @APLIC_PREC=3" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                       SET @ORDEN_V=1 " & vbCrLf
sConsulta = sConsulta & "                                       SET @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "                                 ELSE                   " & vbCrLf
sConsulta = sConsulta & "                                   begin" & vbCrLf
sConsulta = sConsulta & "                                      SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                      SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                               end  --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                         END -- while" & vbCrLf
sConsulta = sConsulta & "                if @ORDEN_V IS NULL  SET @ORDEN_V=0" & vbCrLf
sConsulta = sConsulta & "                      begin" & vbCrLf
sConsulta = sConsulta & "                      if @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=@ORDEN_V+1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID<>@ID" & vbCrLf
sConsulta = sConsulta & "               SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "             END --@orden=null" & vbCrLf
sConsulta = sConsulta & "       SELECT @POND=POND,@AMBITOOLD=AMBITO,@USAR_PREC=USAR_PREC FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "       IF @DEF_PREC=1 SET @USAR_PREC=1" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                  MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                  MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                  OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC,USAR_PREC=@USAR_PREC," & vbCrLf
sConsulta = sConsulta & "                  DEF_PREC=@DEF_PREC,POND=@POND WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       SELECT @FECACT=FECACT FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF   @ATRIB IS NOT  NULL" & vbCrLf
sConsulta = sConsulta & "             BEGIN --@atrib not null" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                       UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM ," & vbCrLf
sConsulta = sConsulta & "                           MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                           MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "             END--@atrib not null" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "               BEGIN --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     IF @AMBITOOLD<3 " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                             IF (SELECT count(*) FROM CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POS IS NOT NULL AND POS<>0)>0" & vbCrLf
sConsulta = sConsulta & "                                    SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT count(*) FROM CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 " & vbCrLf
sConsulta = sConsulta & "                                     SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT count(*) FROM CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 " & vbCrLf
sConsulta = sConsulta & "                                     SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 AND @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                           begin" & vbCrLf
sConsulta = sConsulta & "                           UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     SELECT @COUNT=COUNT(*) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "                              IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                                     begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT > 0" & vbCrLf
sConsulta = sConsulta & "                                                begin" & vbCrLf
sConsulta = sConsulta & "                                                DELETE OFE_ITEM_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                                end" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                    SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                            begin                                                              " & vbCrLf
sConsulta = sConsulta & "                                                  IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "                                                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                                                        UPDATE CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                        UPDATE CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                                                              PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                               ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                               FROM CONF_VISTAS_GRUPO V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=1 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                    END" & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    BEGIN      " & vbCrLf
sConsulta = sConsulta & "                                                          UPDATE CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                          UPDATE CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                                                                PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                  ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                  FROM CONF_VISTAS_GRUPO V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=1 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE AND V.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                     END" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_ALL SET " & vbCrLf
sConsulta = sConsulta & "                             PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                       ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                     FROM CONF_VISTAS_ALL V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=0 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                            end -- vista" & vbCrLf
sConsulta = sConsulta & "                                     end  --old=3" & vbCrLf
sConsulta = sConsulta & "                              ELSE --ser�a old=2" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                          IF @COUNT>0 " & vbCrLf
sConsulta = sConsulta & "                                              BEGIN" & vbCrLf
sConsulta = sConsulta & "                                              IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                              ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                              END " & vbCrLf
sConsulta = sConsulta & "                                          IF @VISTAS=1 " & vbCrLf
sConsulta = sConsulta & "                                              BEGIN                  " & vbCrLf
sConsulta = sConsulta & "                                              UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "               CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "               FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              END             " & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "                                IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                                     BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID            " & vbCrLf
sConsulta = sConsulta & "                                                   end" & vbCrLf
sConsulta = sConsulta & "                                          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                                                   begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                                                          DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                                            --pasa de ambito 1 a ambito 3, el @grupo es siempre null, para todos los grupos" & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                    SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                   SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                   SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE              " & vbCrLf
sConsulta = sConsulta & "                                                  end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                               begin                               " & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "               CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "               FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                end --vistas" & vbCrLf
sConsulta = sConsulta & "                                     END --old=1" & vbCrLf
sConsulta = sConsulta & "                                ELSE " & vbCrLf
sConsulta = sConsulta & "                                     BEGIN  --old es 2, nuevo es 3" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                                               begin" & vbCrLf
sConsulta = sConsulta & "                                               IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                    DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                    end" & vbCrLf
sConsulta = sConsulta & "                                                ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     begin" & vbCrLf
sConsulta = sConsulta & "                                                     DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID " & vbCrLf
sConsulta = sConsulta & "                                                     end" & vbCrLf
sConsulta = sConsulta & "                                               end" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                    INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                    INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                        SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                    end" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     begin" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                     end" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                  SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                               begin" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                          UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "                              CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                          ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                           FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              end --vistas" & vbCrLf
sConsulta = sConsulta & "                                     END--old es 2" & vbCrLf
sConsulta = sConsulta & "               END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "         END -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ATRIB_PREC =ATRIB  FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ORDEN=ORDEN FROM PLANTILLA_ATRIB WHERE PLANT=@PLANT AND APLIC_PREC = @APLIC_PREC AND OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                     IF @ORDEN IS NULL" & vbCrLf
sConsulta = sConsulta & "                         BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                           SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                           WHILE @APLI_PREC_V < 3  AND @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                                     SELECT  @ORDEN_V = MAX(ORDEN) FROM PLANTILLA_ATRIB WHERE  PLANT=@PLANT AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                                      IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                                         begin" & vbCrLf
sConsulta = sConsulta & "                                              SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                              SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                         end" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                          UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                          SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "                        END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "            begin" & vbCrLf
sConsulta = sConsulta & "                        IF @APLIC_PREC IS not  NULL   " & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                            UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SELECT @POND=POND, @AMBITOOLD=AMBITO FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "           IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "           IF @DEF_PREC=1 SET @USAR_PREC=1" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                    MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                    MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC,USAR_PREC=@USAR_PREC," & vbCrLf
sConsulta = sConsulta & "                    DEF_PREC=@DEF_PREC,POND=@POND,ORDEN=@ORDEN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          SELECT @FECACT=FECACT FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          IF   @ATRIB IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                        UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ," & vbCrLf
sConsulta = sConsulta & "                            MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                            MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND, DESCR=@DESCR WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "       BEGIN --@ambito<>ambitoold           " & vbCrLf
sConsulta = sConsulta & "           IF @AMBITOOLD<3 " & vbCrLf
sConsulta = sConsulta & "               begin" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POS IS NOT NULL AND POS<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "              begin" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "             begin" & vbCrLf
sConsulta = sConsulta & "                IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                   begin --old=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                              " & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO PLANTILLA_CONF_VISTAS_PROCE_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                SELECT @PLANT,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_PROCE WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                         IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                         begin" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600  WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,  CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT  AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,  CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                   end  --old=3" & vbCrLf
sConsulta = sConsulta & "   ELSE --Ser�a ambitoold=2" & vbCrLf
sConsulta = sConsulta & "        begin" & vbCrLf
sConsulta = sConsulta & "                          if @VISTAS=1 " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "       UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                         end" & vbCrLf
sConsulta = sConsulta & "        end" & vbCrLf
sConsulta = sConsulta & "            end --@AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "             IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                  begin --old=1" & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "               begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                               DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                               IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                             SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                             SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT         " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                      SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "                            IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                            begin" & vbCrLf
sConsulta = sConsulta & "                UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "            UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                            end" & vbCrLf
sConsulta = sConsulta & "                 end --old=1" & vbCrLf
sConsulta = sConsulta & "             ELSE " & vbCrLf
sConsulta = sConsulta & "                 BEGIN  --old es 2, nuevo es 3" & vbCrLf
sConsulta = sConsulta & "                      DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                begin" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                      INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                             SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                      IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                       begin                       " & vbCrLf
sConsulta = sConsulta & "                         UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                 UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                       end" & vbCrLf
sConsulta = sConsulta & "                END--old es 2" & vbCrLf
sConsulta = sConsulta & "       END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub

Private Sub V_2_17_Tablas_8()
Dim sConsulta As String
    
    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PROVE]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'PORTAL_NO_OFE')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[PROCE_PROVE] ADD" & vbCrLf
    sConsulta = sConsulta & "     [PORTAL_NO_OFE] [tinyint] NOT NULL CONSTRAINT [DF_PROCE_PROVE_PORTAL_NO_OFE] DEFAULT 0," & vbCrLf
    sConsulta = sConsulta & "     [USU_NO_OFE] [varchar] (50) NULL," & vbCrLf
    sConsulta = sConsulta & "     [NOMUSU_NO_OFE] [varchar] (200) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[PARGEN_GEST]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'CAMBIARMON')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[PARGEN_GEST] ADD" & vbCrLf
    sConsulta = sConsulta & "     [CAMBIARMON] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_GEST_CAMBIARMON] DEFAULT 1" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[PARINS_PROCE]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'CAMBIARMON')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[PARINS_PROCE] ADD" & vbCrLf
    sConsulta = sConsulta & "     [CAMBIARMON] [tinyint] NOT NULL CONSTRAINT [DF_PARINS_PROCE_CAMBIARMON] DEFAULT 1" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists(select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ADJUN]') and OBJECTPROPERTY(id, N'IsTable')=1 )" & vbCrLf
    sConsulta = sConsulta & " BEGIN" & vbCrLf
    sConsulta = sConsulta & " CREATE TABLE [dbo].[PROCE_ADJUN] (" & vbCrLf
    sConsulta = sConsulta & "   [ANYO] [smallint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [PROCE] [smallint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [ID] [smallint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [NOM] [varchar] (300) NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [COM] [varchar] (500) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [DATA] [image] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [RUTA] [varchar] (2000) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [DATASIZE] [int] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " CREATE  CLUSTERED  INDEX [TEMP_PK_PROCE_ADJUN] ON [dbo].[PROCE_ADJUN]([ANYO], [PROCE], [GMN1], [ID]) WITH  FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & " ALTER TABLE [dbo].[PROCE_ADJUN] ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [PK_PROCE_ADJUN] PRIMARY KEY  NONCLUSTERED " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ANYO]," & vbCrLf
    sConsulta = sConsulta & "       [PROCE]," & vbCrLf
    sConsulta = sConsulta & "       [GMN1]," & vbCrLf
    sConsulta = sConsulta & "       [ID]" & vbCrLf
    sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & " ALTER TABLE [dbo].[PROCE_ADJUN] ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [FK_PROCE_ADJUN_PROCE] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ANYO]," & vbCrLf
    sConsulta = sConsulta & "       [GMN1]," & vbCrLf
    sConsulta = sConsulta & "       [PROCE]" & vbCrLf
    sConsulta = sConsulta & "   ) REFERENCES [dbo].[PROCE] (" & vbCrLf
    sConsulta = sConsulta & "       [ANYO]," & vbCrLf
    sConsulta = sConsulta & "       [GMN1]," & vbCrLf
    sConsulta = sConsulta & "       [COD]" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & " END"
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "if not exists(select * from dbo.sysobjects where id = object_id(N'[dbo].[LOG_CIERRE]') and OBJECTPROPERTY(id, N'IsTable')=1 )" & vbCrLf
    sConsulta = sConsulta & " BEGIN" & vbCrLf
    sConsulta = sConsulta & "CREATE TABLE [dbo].[LOG_CIERRE] (" & vbCrLf
    sConsulta = sConsulta & "   [ANYO] [smallint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [PROCE] [smallint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [USU] [varchar] (50) NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [PER] [varchar] (50) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [APE] [varchar] (100) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [NOM] [varchar] (50) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [EMAIL] [varchar] (100) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [FECHA] [datetime] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [TIPO] [tinyint] NOT NULL " & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & " CREATE  INDEX [IX_LOG_CIERRE] ON [dbo].[LOG_CIERRE]([ANYO], [GMN1], [PROCE]) ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & " END "
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_ADJ]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'PEDIDO')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[ITEM_ADJ] ADD" & vbCrLf
    sConsulta = sConsulta & "     [PEDIDO] [varchar] (50) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ANYOPED INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PEDIDO INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ORDEN INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "declare c1 cursor local for select DISTINCT lp.anyo, lp.gmn1, lp.proce, lp.item,lp.prove" & vbCrLf
    sConsulta = sConsulta & " from lineas_pedido lp   where lp.item is not null " & vbCrLf
    sConsulta = sConsulta & "OPEN c1" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM c1 INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE" & vbCrLf
    sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "  declare C2 cursor local for SELECT p.anyo , p.num ,  o.num FROM LINEAS_PEDIDO LP " & vbCrLf
    sConsulta = sConsulta & "  inner join pedido p on p.id=lp.pedido inner join orden_entrega o on o.id=lp.orden " & vbCrLf
    sConsulta = sConsulta & "  where LP.ANYO=@ANYO AND LP.GMN1=@GMN1 AND LP.PROCE=@PROCE AND  lp.item =@ITEM AND LP.PROVE=@PROVE order by lp.fecemision desc" & vbCrLf
    sConsulta = sConsulta & "    OPEN C2" & vbCrLf
    sConsulta = sConsulta & "    FETCH NEXT FROM C2 INTO @ANYOPED,@PEDIDO,@ORDEN" & vbCrLf
    sConsulta = sConsulta & "    WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "    BEGIN" & vbCrLf
    sConsulta = sConsulta & "      update item_ADJ set PEDIDO=CAST(@ANYOPED AS VARCHAR(4)) + '/' + CAST(@PEDIDO AS VARCHAR(15))  + '/' +  CAST(@ORDEN AS VARCHAR(15)) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE" & vbCrLf
    sConsulta = sConsulta & "    END " & vbCrLf
    sConsulta = sConsulta & "    CLOSE C2" & vbCrLf
    sConsulta = sConsulta & "    DEALLOCATE C2" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM c1 INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE" & vbCrLf
    sConsulta = sConsulta & "end " & vbCrLf
    sConsulta = sConsulta & "CLOSE C1" & vbCrLf
    sConsulta = sConsulta & ""
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists(select * from dbo.ACC where ID = 12050)" & vbCrLf
    sConsulta = sConsulta & " BEGIN" & vbCrLf
    sConsulta = sConsulta & " INSERT INTO [dbo].[ACC] VALUES (12050,12050)" & vbCrLf
    sConsulta = sConsulta & " insert into USU_ACC (USU,ACC) SELECT USU,12050 FROM USU_ACC WHERE ACC=12031" & vbCrLf
    sConsulta = sConsulta & " insert into PERF_ACC (PERF,ACC) SELECT PERF,12050 FROM PERF_ACC WHERE ACC=12031" & vbCrLf
    sConsulta = sConsulta & " END"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists(select * from dbo.ACC where ID = 20125)" & vbCrLf
    sConsulta = sConsulta & " BEGIN" & vbCrLf
    sConsulta = sConsulta & " INSERT INTO [dbo].[ACC] VALUES (20125,20125)" & vbCrLf
    sConsulta = sConsulta & " insert into USU_ACC (USU,ACC) SELECT USU,20125 FROM USU_ACC WHERE ACC=20102" & vbCrLf
    sConsulta = sConsulta & " insert into PERF_ACC (PERF,ACC) SELECT PERF,20125 FROM PERF_ACC WHERE ACC=20102" & vbCrLf
    sConsulta = sConsulta & " END"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists(select * from dbo.ACC where ID = 20234)" & vbCrLf
    sConsulta = sConsulta & " BEGIN" & vbCrLf
    sConsulta = sConsulta & " INSERT INTO [dbo].[ACC] VALUES (20234,20234)" & vbCrLf
    sConsulta = sConsulta & " insert into USU_ACC (USU,ACC) SELECT USU,20234 FROM USU_ACC WHERE ACC=20223" & vbCrLf
    sConsulta = sConsulta & " insert into PERF_ACC (PERF,ACC) SELECT PERF,20234 FROM PERF_ACC WHERE ACC=20223" & vbCrLf
    sConsulta = sConsulta & " END"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists(select * from dbo.ACC where ID = 20235)" & vbCrLf
    sConsulta = sConsulta & " BEGIN" & vbCrLf
    sConsulta = sConsulta & " INSERT INTO [dbo].[ACC] VALUES (20235,20235)" & vbCrLf
    sConsulta = sConsulta & " insert into USU_ACC (USU,ACC) SELECT USU,20235 FROM USU_ACC WHERE ACC=20223" & vbCrLf
    sConsulta = sConsulta & " insert into PERF_ACC (PERF,ACC) SELECT PERF,20235 FROM PERF_ACC WHERE ACC=20223" & vbCrLf
    sConsulta = sConsulta & " END"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists(select * from dbo.IDIOMAS where COD ='GER')" & vbCrLf
    sConsulta = sConsulta & " INSERT INTO [dbo].[IDIOMAS] VALUES ('GER','Deutsch',1000,0)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_2_17_Triggers_8()
Dim sConsulta As String


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ADJUN_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ADJUN_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = " CREATE TRIGGER PROCE_ADJUN_TG_INSUPD ON dbo.PROCE_ADJUN" & vbCrLf
    sConsulta = sConsulta & " FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & " AS" & vbCrLf
    sConsulta = sConsulta & " UPDATE PROCE_ADJUN SET FECACT = GETDATE()" & vbCrLf
    sConsulta = sConsulta & "  FROM PROCE_ADJUN IO" & vbCrLf
    sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
    sConsulta = sConsulta & "                ON IO.ANYO = I.ANYO" & vbCrLf
    sConsulta = sConsulta & "               AND IO.GMN1=I.GMN1" & vbCrLf
    sConsulta = sConsulta & "               AND IO.PROCE = I.PROCE" & vbCrLf
    sConsulta = sConsulta & "               AND IO.ID = I.ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LINEAS_PEDIDO_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[LINEAS_PEDIDO_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER LINEAS_PEDIDO_TG_DEL ON dbo.LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYOPED AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PED AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_LINEAS_PEDIDO_Upd CURSOR LOCAL FOR SELECT I.ID,P.ANYO,P.NUM CES ,O.NUM PED,I.ANYO,I.GMN1,I.PROCE,I.ITEM,I.PROVE FROM DELETED I " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PEDIDO P ON I.PEDIDO=P.ID INNER JOIN ORDEN_ENTREGA O ON O.ID=I.ORDEN" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_LINEAS_PEDIDO_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LINEAS_PEDIDO_Upd INTO @ID,@ANYOPED,@PEDIDO,@ORDEN,@ANYO,@GMN1,@PROCE,@ITEM,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @ITEM IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                    UPDATE ITEM_ADJ SET PEDIDO = NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                    DECLARE C1 CURSOR LOCAL FOR  SELECT P.ANYO , P.NUM , O.NUM FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN PEDIDO P ON P.ID=LP.PEDIDO INNER JOIN ORDEN_ENTREGA O ON O.ID=LP.ORDEN " & vbCrLf
sConsulta = sConsulta & "                                 WHERE LP. ANYO=@ANYO AND LP.GMN1=@GMN1 AND LP.PROCE=@PROCE AND LP.ID=@ITEM AND LP.PROVE=@PROVE ORDER BY LP.FECEMISION DESC" & vbCrLf
sConsulta = sConsulta & "                    OPEN C1" & vbCrLf
sConsulta = sConsulta & "                    FETCH NEXT FROM C1 INTO @ANYOPED,@PEDIDO,@ORDEN" & vbCrLf
sConsulta = sConsulta & "                    WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @PED=CAST(@ANYOPED AS VARCHAR(4)) + '/' + CAST(@PEDIDO AS VARCHAR(15))  + '/' +  CAST(@ORDEN AS VARCHAR(15))" & vbCrLf
sConsulta = sConsulta & "                           UPDATE ITEM_ADJ SET PEDIDO = @PED WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                    END" & vbCrLf
sConsulta = sConsulta & "                    CLOSE C1" & vbCrLf
sConsulta = sConsulta & "                    DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LINEAS_PEDIDO_Upd INTO @ID,@ANYOPED,@PEDIDO,@ORDEN,@ANYO,@GMN1,@PROCE,@ITEM,@PROVE" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_LINEAS_PEDIDO_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_LINEAS_PEDIDO_Upd" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LINEAS_PEDIDO_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[LINEAS_PEDIDO_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER LINEAS_PEDIDO_TG_INS ON dbo.LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYOPED AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PED AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_LINEAS_PEDIDO_Upd CURSOR LOCAL FOR SELECT I.ID,P.ANYO,P.NUM CES ,O.NUM PED,I.ANYO,I.GMN1,I.PROCE,I.ITEM,I.PROVE FROM INSERTED I " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PEDIDO P ON I.PEDIDO=P.ID INNER JOIN ORDEN_ENTREGA O ON O.ID=I.ORDEN" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_LINEAS_PEDIDO_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LINEAS_PEDIDO_Upd INTO @ID,@ANYOPED,@PEDIDO,@ORDEN,@ANYO,@GMN1,@PROCE,@ITEM,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @ITEM IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PED=CAST(@ANYOPED AS VARCHAR(4)) + '/' + CAST(@PEDIDO AS VARCHAR(15))  + '/' +  CAST(@ORDEN AS VARCHAR(15))" & vbCrLf
sConsulta = sConsulta & "                   UPDATE ITEM_ADJ SET PEDIDO = @PED WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "   UPDATE LINEAS_PEDIDO SET FECACT=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LINEAS_PEDIDO_Upd INTO @ID,@ANYOPED,@PEDIDO,@ORDEN,@ANYO,@GMN1,@PROCE,@ITEM,@PROVE" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_LINEAS_PEDIDO_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_LINEAS_PEDIDO_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_17_00_08A2_17_00_09() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificar tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_17_Tablas_9

frmProgreso.lblDetalle = "Modificar triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_17_Triggers_9

frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_17_Storeds_9

sConsulta = "UPDATE VERSION SET NUM ='2.17.00.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_17_00_08A2_17_00_09 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_17_00_08A2_17_00_09 = False

End Function

Private Sub V_2_17_Tablas_9()
Dim sConsulta As String
Dim oRes As RDO.rdoResultset
Dim bHacer As Boolean
    
bHacer = False
sConsulta = "select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[PARGEN_PED]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'OBLCODPEDDIR'"
Set oRes = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
If oRes.eof Then
    bHacer = True
End If
oRes.Close
Set oRes = Nothing
If bHacer Then
    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[PARGEN_PED]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'OBLCODPEDDIR')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[PARGEN_PED] ADD" & vbCrLf
    sConsulta = sConsulta & "     [OBLCODPEDDIR] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_PED_OBLCODPEDDIR] DEFAULT 0" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "INSERT INTO PARGEN_LIT SELECT 36, I.COD, PL.DEN FROM IDIOMAS I LEFT JOIN PARGEN_LIT PL ON PL.IDI=I.COD AND PL.ID=31"
    sConsulta = sConsulta & " WHERE NOT EXISTS (SELECT * FROM PARGEN_LIT PALI WHERE PALI.ID=36 AND PALI.IDI=I.COD)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_ENTREGA]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'NUM_PED_ERP')" & vbCrLf
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[ORDEN_ENTREGA] ADD" & vbCrLf
    sConsulta = sConsulta & "     [NUM_PED_ERP] [varchar] (100) NULL," & vbCrLf
    sConsulta = sConsulta & "     [PAG] [varchar] (" & gLongitudesDeCodigos.giLongCodPAG & ") NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if exists(select * from syscolumns where id = (select id from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_ENTREGA]') and OBJECTPROPERTY(id, N'IsTable')=1 ) and name = 'REFERENCIA')" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "  UPDATE ORDEN_ENTREGA SET NUM_PED_ERP=REFERENCIA" & vbCrLf
    sConsulta = sConsulta & "  IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_ORDEN_ENTREGA_01')"
    sConsulta = sConsulta & "  DROP INDEX [dbo].[ORDEN_ENTREGA].[IX_ORDEN_ENTREGA_01]"
    sConsulta = sConsulta & "  CREATE  INDEX [IX_ORDEN_ENTREGA_01] ON [dbo].[ORDEN_ENTREGA]([NUM_PED_ERP]) WITH  FILLFACTOR = 90 ON [PRIMARY]"
    sConsulta = sConsulta & "  ALTER TABLE [dbo].[ORDEN_ENTREGA] DROP COLUMN REFERENCIA" & vbCrLf
    sConsulta = sConsulta & "  END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE TABLE [dbo].[ORDEN_ENTREGA_ATRIB] (" & vbCrLf
    sConsulta = sConsulta & "   [ORDEN] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [ATRIB] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [INTERNO] [tinyint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [INTRO] [tinyint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [MINNUM] [float] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [MAXNUM] [float] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [MINFEC] [datetime] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [MAXFEC] [datetime] NULL " & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[ORDEN_ENTREGA_ATRIB] WITH NOCHECK ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [PK_ORDEN_ENTREGA_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ORDEN]," & vbCrLf
    sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
    sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[ORDEN_ENTREGA_ATRIB] ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [DF_ORDEN_ENTREGA_ATRIB_INTERNO] DEFAULT (0) FOR [INTERNO]," & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [DF_ORDEN_ENTREGA_ATRIB_INTRO] DEFAULT (0) FOR [INTRO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[ORDEN_ENTREGA_ATRIB] ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [FK_ORDEN_ENTREGA_ATRIB_DEF_ATRIB] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
    sConsulta = sConsulta & "   ) REFERENCES [dbo].[DEF_ATRIB] (" & vbCrLf
    sConsulta = sConsulta & "       [ID]" & vbCrLf
    sConsulta = sConsulta & "   )," & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [FK_ORDEN_ENTREGA_ATRIB_ORDEN_ENTREGA] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ORDEN]" & vbCrLf
    sConsulta = sConsulta & "   ) REFERENCES [dbo].[ORDEN_ENTREGA] (" & vbCrLf
    sConsulta = sConsulta & "       [ID]" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "CREATE TRIGGER ORDEN_ENTREGA_ATRIB_TG_INSUPD ON dbo.ORDEN_ENTREGA_ATRIB" & vbCrLf
    sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA_ATRIB SET FECACT = GETDATE()" & vbCrLf
    sConsulta = sConsulta & "  FROM ORDEN_ENTREGA_ATRIB O" & vbCrLf
    sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
    sConsulta = sConsulta & "                ON O.ORDEN = I.ORDEN" & vbCrLf
    sConsulta = sConsulta & "               AND O.ATRIB=I.ATRIB" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE TABLE [dbo].[ORDEN_ENTREGA_ATRIB_LISTA] (" & vbCrLf
    sConsulta = sConsulta & "   [ORDEN] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [ATRIB] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [NUMORDEN] [smallint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[ORDEN_ENTREGA_ATRIB_LISTA] WITH NOCHECK ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [PK_ORDEN_ENTREGA_ATRIB_LISTA] PRIMARY KEY  CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ORDEN]," & vbCrLf
    sConsulta = sConsulta & "       [ATRIB]," & vbCrLf
    sConsulta = sConsulta & "       [NUMORDEN]" & vbCrLf
    sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[ORDEN_ENTREGA_ATRIB_LISTA] ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [FK_ORDEN_ENTREGA_ATRIB_LISTA_ORDEN_ENTREGA_ATRIB] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ORDEN]," & vbCrLf
    sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
    sConsulta = sConsulta & "   ) REFERENCES [dbo].[ORDEN_ENTREGA_ATRIB] (" & vbCrLf
    sConsulta = sConsulta & "       [ORDEN]," & vbCrLf
    sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "CREATE TRIGGER ORDEN_ENTREGA_ATRIB_LISTA_TG_INSUPD ON dbo.ORDEN_ENTREGA_ATRIB_LISTA" & vbCrLf
    sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA_ATRIB_LISTA SET FECACT=GETDATE() " & vbCrLf
    sConsulta = sConsulta & " FROM ORDEN_ENTREGA_ATRIB_LISTA OEA" & vbCrLf
    sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
    sConsulta = sConsulta & "                ON OEA.ORDEN = I.ORDEN" & vbCrLf
    sConsulta = sConsulta & "               AND OEA.ATRIB=I.ATRIB" & vbCrLf
    sConsulta = sConsulta & "               AND OEA.NUMORDEN = I.NUMORDEN" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE TABLE [dbo].[LOG_ORDEN_ENTREGA_ATRIB] (" & vbCrLf
    sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [ID_LOG_ORDEN_ENTREGA] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [ATRIB] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[LOG_ORDEN_ENTREGA_ATRIB] WITH NOCHECK ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [PK_LOG_ORDEN_ENTREGA_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ID]" & vbCrLf
    sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[LOG_ORDEN_ENTREGA_ATRIB] ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [FK_LOG_ORDEN_ENTREGA_ATRIB_LOG_ORDEN_ENTREGA] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ID_LOG_ORDEN_ENTREGA]" & vbCrLf
    sConsulta = sConsulta & "   ) REFERENCES [dbo].[LOG_ORDEN_ENTREGA] (" & vbCrLf
    sConsulta = sConsulta & "       [ID]" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "CREATE TRIGGER TG_LOG_ORDEN_ENTREGA_ATRIB_INS ON dbo.LOG_ORDEN_ENTREGA_ATRIB" & vbCrLf
    sConsulta = sConsulta & "FOR INSERT" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "  UPDATE LOG_ORDEN_ENTREGA_ATRIB" & vbCrLf
    sConsulta = sConsulta & "        SET FECACT=CASE WHEN L.FECACT IS NULL THEN GETDATE() ELSE L.FECACT END" & vbCrLf
    sConsulta = sConsulta & "        FROM INSERTED I" & vbCrLf
    sConsulta = sConsulta & "        INNER JOIN LOG_ORDEN_ENTREGA_ATRIB L ON L.ID = I.ID" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE TABLE [dbo].[LINEAS_PEDIDO_ATRIB] (" & vbCrLf
    sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [ATRIB] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [INTERNO] [tinyint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [INTRO] [tinyint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [MINNUM] [float] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [MAXNUM] [float] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [MINFEC] [datetime] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [MAXFEC] [datetime] NULL " & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[LINEAS_PEDIDO_ATRIB] WITH NOCHECK ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [PK_LINEAS_PEDIDO_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [LINEA]," & vbCrLf
    sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
    sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[LINEAS_PEDIDO_ATRIB] ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [DF_LINEAS_PEDIDO_ATRIB_INTERNO] DEFAULT (0) FOR [INTERNO]," & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [DF_LINEAS_PEDIDO_ATRIB_INTRO] DEFAULT (0) FOR [INTRO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[LINEAS_PEDIDO_ATRIB] ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [FK_LINEAS_PEDIDO_ATRIB_DEF_ATRIB] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
    sConsulta = sConsulta & "   ) REFERENCES [dbo].[DEF_ATRIB] (" & vbCrLf
    sConsulta = sConsulta & "       [ID]" & vbCrLf
    sConsulta = sConsulta & "   )," & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [FK_LINEAS_PEDIDO_ATRIB_LINEAS_PEDIDO1] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [LINEA]" & vbCrLf
    sConsulta = sConsulta & "   ) REFERENCES [dbo].[LINEAS_PEDIDO] (" & vbCrLf
    sConsulta = sConsulta & "       [ID]" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "CREATE TRIGGER LINEAS_PEDIDO_ATRIB_TG_INSUPD ON dbo.LINEAS_PEDIDO_ATRIB" & vbCrLf
    sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO_ATRIB SET FECACT = GETDATE()" & vbCrLf
    sConsulta = sConsulta & "  FROM LINEAS_PEDIDO_ATRIB O" & vbCrLf
    sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
    sConsulta = sConsulta & "                ON O.LINEA = I.LINEA" & vbCrLf
    sConsulta = sConsulta & "               AND O.ATRIB=I.ATRIB" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "CREATE TABLE [dbo].[LINEAS_PEDIDO_ATRIB_LISTA] (" & vbCrLf
    sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [ATRIB] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [NUMORDEN] [smallint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[LINEAS_PEDIDO_ATRIB_LISTA] WITH NOCHECK ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [PK_LINEAS_PEDIDO_ATRIB_LISTA] PRIMARY KEY  CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [LINEA]," & vbCrLf
    sConsulta = sConsulta & "       [ATRIB]," & vbCrLf
    sConsulta = sConsulta & "       [NUMORDEN]" & vbCrLf
    sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[LINEAS_PEDIDO_ATRIB_LISTA] ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [FK_LINEAS_PEDIDO_ATRIB_LISTA_LINEAS_PEDIDO_ATRIB] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [LINEA]," & vbCrLf
    sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
    sConsulta = sConsulta & "   ) REFERENCES [dbo].[LINEAS_PEDIDO_ATRIB] (" & vbCrLf
    sConsulta = sConsulta & "       [LINEA]," & vbCrLf
    sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "CREATE TRIGGER LINEAS_PEDIDO_ATRIB_LISTA_TG_INSUPD ON dbo.LINEAS_PEDIDO_ATRIB_LISTA" & vbCrLf
    sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO_ATRIB_LISTA SET FECACT=GETDATE() " & vbCrLf
    sConsulta = sConsulta & " FROM LINEAS_PEDIDO_ATRIB_LISTA OEA" & vbCrLf
    sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
    sConsulta = sConsulta & "                ON OEA.LINEA = I.LINEA" & vbCrLf
    sConsulta = sConsulta & "               AND OEA.ATRIB=I.ATRIB" & vbCrLf
    sConsulta = sConsulta & "               AND OEA.NUMORDEN = I.NUMORDEN" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE TABLE [dbo].[LOG_LINEAS_PEDIDO_ATRIB] (" & vbCrLf
    sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [ID_LOG_LINEA] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [ATRIB] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL ," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[LOG_LINEAS_PEDIDO_ATRIB] WITH NOCHECK ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [PK_LOG_LINEAS_PEDIDO_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ID]" & vbCrLf
    sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[LOG_LINEAS_PEDIDO_ATRIB] ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [FK_LOG_LINEAS_PEDIDO_ATRIB_LOG_LINEAS_PEDIDO] FOREIGN KEY " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ID_LOG_LINEA]" & vbCrLf
    sConsulta = sConsulta & "   ) REFERENCES [dbo].[LOG_LINEAS_PEDIDO] (" & vbCrLf
    sConsulta = sConsulta & "       [ID]" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "CREATE TRIGGER TG_LOG_LINEAS_PEDIDO_ATRIB_INS ON dbo.LOG_LINEAS_PEDIDO_ATRIB" & vbCrLf
    sConsulta = sConsulta & "FOR INSERT" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "UPDATE LOG_LINEAS_PEDIDO_ATRIB" & vbCrLf
    sConsulta = sConsulta & "   SET FECACT=CASE WHEN L.FECACT IS NULL THEN GETDATE() ELSE L.FECACT END" & vbCrLf
    sConsulta = sConsulta & "   FROM INSERTED I" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_LINEAS_PEDIDO_ATRIB L ON L.ID = I.ID" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "ALTER TABLE CONF_VISOR ALTER COLUMN [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NULL "
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE CONF_VISOR ALTER COLUMN [GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ") NULL "
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE CONF_VISOR ALTER COLUMN [GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ") NULL "
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE CONF_VISOR ALTER COLUMN [GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ") NULL "
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE CONF_VISOR ALTER COLUMN [EQP] [varchar] (" & gLongitudesDeCodigos.giLongCodEQP & ") NULL "
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE CONF_VISOR ALTER COLUMN [COM] [varchar] (" & gLongitudesDeCodigos.giLongCodCOM & ") NULL "
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE CONF_VISOR ALTER COLUMN [UON1] [varchar] (" & gLongitudesDeCodigos.giLongCodUON1 & ") NULL "
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE CONF_VISOR ALTER COLUMN [UON2] [varchar] (" & gLongitudesDeCodigos.giLongCodUON2 & ") NULL "
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "ALTER TABLE CONF_VISOR ALTER COLUMN [UON3] [varchar] (" & gLongitudesDeCodigos.giLongCodUON3 & ") NULL "
    ExecuteSQL gRDOCon, sConsulta
    
End If

End Sub

Private Sub V_2_17_Triggers_9()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_EST_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_EST_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE TRIGGER ORDEN_EST_TG_INS ON dbo.ORDEN_EST " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_EST_Ins CURSOR LOCAL FOR SELECT EST,ID,ORDEN,PER,COMENT,EXTERNO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXTERNO AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVLOG AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORIGEN AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMENT AS VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_ORDEN_ENTREGA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAG AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTEGRACION AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACCION AS VARCHAR(2)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_PED_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOGRABAR AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADO_APROV_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_ERP AS VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @EXTERNO IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF (@EST = 2 OR @EST = 3 OR @EST = 4 OR @EST = 20 OR @EST = 21) " & vbCrLf
sConsulta = sConsulta & "      BEGIN -- Si es uno de los estados que nos interesan para grabar en LOG_ORDEN_ENTREGA (emitido,aceptado,rechazado,anulado,en camino)" & vbCrLf
sConsulta = sConsulta & "        -- Recuperamos el tipo de orden de entrega para ver si est� activada su integraci�n" & vbCrLf
sConsulta = sConsulta & "        SET @COD_ERP=NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT  @TIPO=TIPO,@PROVE=PROVE, @COD_ERP=COD_PROVE_ERP FROM ORDEN_ENTREGA WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "        SELECT  @ACTIVLOG = ACTIVLOG FROM PARGEN_GEST WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "        SELECT  @INTEGRACION =INTEGRACION, @TRASLADO_APROV_ERP=TRASLADO_APROV_ERP, @TRASPASO_PED_ERP= TRASPASO_PED_ERP FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @INTEGRACION = 0" & vbCrLf
sConsulta = sConsulta & "             SET @ACTIVA = 0" & vbCrLf
sConsulta = sConsulta & "        ELSE " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO = 0 --Pedidos directos" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 13" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 11" & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF (@ACTIVA = 1 AND (@SENTIDO=3 OR @SENTIDO=1)) OR @ACTIVLOG = 1 OR @TRASPASO_PED_ERP=1 OR @TRASLADO_APROV_ERP = 1" & vbCrLf
sConsulta = sConsulta & "          -- si el sentido es = 2  (solo entrada) o no est� activa la integraci�n  no grabamos en log" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SET @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "            IF (@TIPO=0 AND @TRASPASO_PED_ERP=1)  OR (@TIPO = 1 AND @TRASLADO_APROV_ERP = 1) --emisi�n de pedidos directos y de aprovisionamiento" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                 IF @TIPO = 0  and @TRASPASO_PED_ERP=1" & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                     SET @ERP=1" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVLOG = 1 -- log de actividad" & vbCrLf
sConsulta = sConsulta & "                         SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                         SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                 IF @TIPO = 1  and @TRASLADO_APROV_ERP=1" & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                     SET @ERP=1" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVLOG = 1 -- log de actividad" & vbCrLf
sConsulta = sConsulta & "                         SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                         SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                begin" & vbCrLf
sConsulta = sConsulta & "                SET @ERP=0" & vbCrLf
sConsulta = sConsulta & "                IF @ACTIVLOG = 1 " & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVA  = 1" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 2 -- Integracion y log de actividad" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 1 -- solo log de actividad" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                     BEGIN" & vbCrLf
sConsulta = sConsulta & "                      IF @ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "                          SET @ORIGEN = 0 -- solo integracion" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                          SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                end" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                SET @USU = NULL" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                SET @USU = (SELECT COD FROM USU WHERE PER = @PER)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            -- la acci�n ser� E - Cambio de estado o I Creaci�n orden de entrega)" & vbCrLf
sConsulta = sConsulta & "            IF @EST = 2  " & vbCrLf
sConsulta = sConsulta & "              SET @ACCION='I'" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @EST=20 " & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='D'" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='E'" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA,OBS)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,ORDEN_ENTREGA.PAG,@EST,INCORRECTA,@COMENT,@ORIGEN,@PER,@USU, ORDEN_ENTREGA.MON,ORDEN_ENTREGA.CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,ORDEN_ENTREGA.NUM_PED_ERP,ORDEN_ENTREGA.OBS " & vbCrLf
sConsulta = sConsulta & "                        FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "      IF @ACCION='I' AND @ORIGEN<>1 AND @TIPO = 0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "                    -- Presupuestos" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES1 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES1 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID  INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES2 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES2 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID  INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES3 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES3 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID   INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES4 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES4 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID   INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "          -- Atributos" & vbCrLf
sConsulta = sConsulta & "          INSERT INTO LOG_ORDEN_ENTREGA_ATRIB ( ID_LOG_ORDEN_ENTREGA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "                        SELECT @ID_LOG_ORDEN_ENTREGA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL FROM ORDEN_ENTREGA_ATRIB WHERE ORDEN=@ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "          INSERT INTO LOG_LINEAS_PEDIDO_ATRIB ( ID_LOG_LINEA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "                        SELECT LLP.ID,A.ATRIB,A.VALOR_NUM,A.VALOR_TEXT,A.VALOR_FEC,A.VALOR_BOOL FROM LINEAS_PEDIDO_ATRIB  A" & vbCrLf
sConsulta = sConsulta & "                                INNER JOIN LOG_LINEAS_PEDIDO LLP ON LLP.ID_LINEAS_PEDIDO  =A.LINEA WHERE LLP.ID_LOG_ORDEN_ENTREGA=@ID_LOG_ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            IF @ERP=1 " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA,OBS)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,ORDEN_ENTREGA.PAG,@EST,INCORRECTA,@COMENT,0,@PER,@USU, ORDEN_ENTREGA.MON,ORDEN_ENTREGA.CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,ORDEN_ENTREGA.NUM_PED_ERP,ORDEN_ENTREGA.OBS" & vbCrLf
sConsulta = sConsulta & "                                  FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    -- Presupuestos" & vbCrLf
sConsulta = sConsulta & "                   IF @ACCION='I'  AND @TIPO = 0" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES1 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES1 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID   INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES2 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES2 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES3 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES3 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES4 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES4 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "          -- Atributos" & vbCrLf
sConsulta = sConsulta & "          INSERT INTO LOG_ORDEN_ENTREGA_ATRIB ( ID_LOG_ORDEN_ENTREGA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "                        SELECT @ID_LOG_ORDEN_ENTREGA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL FROM ORDEN_ENTREGA_ATRIB WHERE ORDEN=@ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "          INSERT INTO LOG_LINEAS_PEDIDO_ATRIB ( ID_LOG_LINEA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "                        SELECT LLP.ID,A.ATRIB,A.VALOR_NUM,A.VALOR_TEXT,A.VALOR_FEC,A.VALOR_BOOL FROM LINEAS_PEDIDO_ATRIB  A" & vbCrLf
sConsulta = sConsulta & "                                INNER JOIN LOG_LINEAS_PEDIDO LLP ON LLP.ID_LINEAS_PEDIDO  =A.LINEA WHERE LLP.ID_LOG_ORDEN_ENTREGA=@ID_LOG_ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "          END --IF ACTIVA" & vbCrLf
sConsulta = sConsulta & "      END -- IF EST" & vbCrLf
sConsulta = sConsulta & "   END -- IF @EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  UPDATE ORDEN_ENTREGA SET ORDEN_EST = @ID WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_17_Storeds_9()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_INSERTAR_LISTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_INSERTAR_LISTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_INSERTAR_LISTA  @TIPO SMALLINT,@ATRIB INTEGER,@ORDEN SMALLINT,@VALORPOND FLOAT=NULL,@ENDEFINICION INTEGER,@VALORTEXT VARCHAR(100)=NULL, @VALORNUM FLOAT=NULL, @VALORFEC DATETIME=NULL, @ID INT=NULL AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @INTRO AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIBDEF AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN " & vbCrLf
sConsulta = sConsulta & "      SELECT @ATRIBDEF=ATRIB FROM PROCE_ATRIB WHERE ID= @ATRIB" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO PROCE_ATRIB_LISTA (ATRIB_ID, ORDEN,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "      VALUES (@ATRIB, @ORDEN, cast(@VALORNUM AS VARCHAR(15)), @VALORTEXT, CONVERT( VARCHAR(10),@VALORFEC ,111),cast(@VALORPOND AS VARCHAR(15)) )" & vbCrLf
sConsulta = sConsulta & "  END    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 2" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "      SELECT @ATRIBDEF=ATRIB FROM PLANTILLA_ATRIB WHERE ID= @ATRIB" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO PLANTILLA_ATRIB_LISTA (ATRIB_ID, ORDEN,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "      VALUES  (@ATRIB, @ORDEN, cast(@VALORNUM AS VARCHAR(15)), @VALORTEXT, CONVERT( VARCHAR(10),@VALORFEC ,111),cast(@VALORPOND AS VARCHAR(15)))" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 0 " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO  DEF_ATRIB_LISTA ( ATRIB, ORDEN, VALOR_NUM, VALOR_TEXT, VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "           VALUES (@ATRIB, @ORDEN, cast(@VALORNUM AS VARCHAR(15)), @VALORTEXT, CONVERT( VARCHAR(10),@VALORFEC ,111),cast(@VALORPOND AS VARCHAR(15)) )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 3" & vbCrLf
sConsulta = sConsulta & "  BEGIN " & vbCrLf
sConsulta = sConsulta & "      INSERT INTO ORDEN_ENTREGA_ATRIB_LISTA (ORDEN,ATRIB, NUMORDEN,VALOR_NUM,VALOR_TEXT,VALOR_FEC) " & vbCrLf
sConsulta = sConsulta & "      VALUES (@ID,@ATRIB, @ORDEN, cast(@VALORNUM AS VARCHAR(15)), @VALORTEXT, CONVERT( VARCHAR(10),@VALORFEC ,111) )" & vbCrLf
sConsulta = sConsulta & "  END    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 4" & vbCrLf
sConsulta = sConsulta & "  BEGIN " & vbCrLf
sConsulta = sConsulta & "      INSERT INTO LINEAS_PEDIDO_ATRIB_LISTA (LINEA,ATRIB, NUMORDEN,VALOR_NUM,VALOR_TEXT,VALOR_FEC) " & vbCrLf
sConsulta = sConsulta & "      VALUES (@ID,@ATRIB, @ORDEN, cast(@VALORNUM AS VARCHAR(15)), @VALORTEXT, CONVERT( VARCHAR(10),@VALORFEC ,111) )" & vbCrLf
sConsulta = sConsulta & "  END    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO <> 0 AND @ENDEFINICION=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @INTRO=INTRO FROM DEF_ATRIB WHERE ID=@ATRIBDEF" & vbCrLf
sConsulta = sConsulta & "       IF @INTRO=1 " & vbCrLf
sConsulta = sConsulta & "           INSERT INTO  DEF_ATRIB_LISTA ( ATRIB, ORDEN, VALOR_NUM, VALOR_TEXT, VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "               VALUES (@ATRIBDEF, @ORDEN, cast(@VALORNUM AS VARCHAR(15)), @VALORTEXT, CONVERT( VARCHAR(10),@VALORFEC ,111),cast(@VALORPOND AS VARCHAR(15)) )" & vbCrLf
sConsulta = sConsulta & "   END    " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_APROB_DEVOLVER_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_APROB_DEVOLVER_ORDEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_APROB_DEVOLVER_ORDEN(@ID INTEGER, @APROBADOR VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT ORDEN_ENTREGA.ID AS ORDENID,ORDEN_ENTREGA.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM_PED_ERP AS REFERENCIA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE + ' ' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.FECHA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.EST" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + ',' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.OBS" & vbCrLf
sConsulta = sConsulta & ",case WHEN LP.NIVEL_APROB=0 THEN 1 WHEN LP.NIVEL_APROB > 0 THEN 2 END  MOTIVO" & vbCrLf
sConsulta = sConsulta & ",COUNT(ORDEN_ENTREGA_ADJUN.ID)  ARCH" & vbCrLf
sConsulta = sConsulta & ", ORDEN_ENTREGA.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, ORDEN_ENTREGA.COD_PROVE_ERP, ORDEN_ENTREGA.RECEPTOR ,R.NOM + ' ' + R.APE NOMRECEPTOR, R.TFNO TFNORECEPTOR, R.EMAIL EMAILRECEPTOR, ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) UONRECEPTOR, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) DENUONRECEPTOR" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PEDIDO ON PEDIDO.ID=ORDEN_ENTREGA.PEDIDO AND ORDEN_ENTREGA.ID=@ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER ON PER.COD=PEDIDO.PER" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE ON PROVE.COD=ORDEN_ENTREGA.PROVE AND ORDEN_ENTREGA.ID=@ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN (" & vbCrLf
sConsulta = sConsulta & "     SELECT ORDEN,MIN(NIVEL_APROB) NIVEL_APROB FROM LINEAS_PEDIDO L " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD S ON S.ID= L.SEGURIDAD AND L.NIVEL_APROB=0 AND L.EST=0" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM AP ON AP.SEGURIDAD=L.SEGURIDAD AND AP.ID=L.APROB_LIM AND AP.NIVEL=L.NIVEL_APROB" & vbCrLf
sConsulta = sConsulta & "      WHERE L.ORDEN=@ID AND L.EST=0  AND (AP.PER=@APROBADOR OR S.APROB_TIPO1=@APROBADOR) GROUP BY ORDEN) LP" & vbCrLf
sConsulta = sConsulta & " ON LP.ORDEN=ORDEN_ENTREGA.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ORDEN_ENTREGA_ADJUN ON ORDEN_ENTREGA.ID=ORDEN_ENTREGA_ADJUN.ORDEN" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN EMP E ON ORDEN_ENTREGA.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (PER R LEFT JOIN UON1 ON R.UON1 = UON1.COD LEFT JOIN UON2 ON R.UON1 = UON2.UON1 AND R.UON2 = UON2.COD LEFT JOIN UON3 ON R.UON1 = UON3.UON1 AND R.UON2 = UON3.UON2 AND R.UON3 = UON3.COD) ON ORDEN_ENTREGA.RECEPTOR = R.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GROUP BY    ORDEN_ENTREGA.ID ,ORDEN_ENTREGA.ANYO " & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM_PED_ERP " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE + ' ' + PROVE.DEN " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE " & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.FECHA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.EST" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + ',' + PER.NOM " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.OBS,LP.NIVEL_APROB" & vbCrLf
sConsulta = sConsulta & ", ORDEN_ENTREGA.EMPRESA, E.DEN , E.NIF , ORDEN_ENTREGA.COD_PROVE_ERP, ORDEN_ENTREGA.RECEPTOR ,R.NOM + ' ' + R.APE , R.TFNO , R.EMAIL , ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) , ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_APROB_DEVOLVER_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_APROB_DEVOLVER_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

 sConsulta = "CREATE PROCEDURE FSEP_APROB_DEVOLVER_ORDENES(@APROBADOR VARCHAR(50), @MAXLINEAS INTEGER, @DESDEID INTEGER," & vbCrLf
sConsulta = sConsulta & "@ANYO INTEGER, @NUMPEDIDO INTEGER, @ORDEN INTEGER, @APROBADOS INTEGER,@DENEGADOS INTEGER,@CODPROVE VARCHAR(50),@NUMPEDIDOPROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & ",@DESDEFECHA VARCHAR(10), @APROVISIONADOR VARCHAR(50), @REFERENCIA varchar(120) = null ,@CAMPO1 int = null ,@CAMPO2 int = null, " & vbCrLf
sConsulta = sConsulta & "@VALORCAMPO1 varchar(150) = null, @VALORCAMPO2 varchar(150)=null , @CATN1 int = null, @CATN2 int = null, @CATN3 int = null, @CATN4 int = null , @CATN5 int = null ," & vbCrLf
sConsulta = sConsulta & " @EMPRESA INT = NULL, @RECEPTOR VARCHAR(100) = NULL, @COD_ERP  VARCHAR(100) = NULL, @HASTAFECHA VARCHAR(10))  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INDICE INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= 'SELECT DISTINCT OE.ID AS ORDENID" & vbCrLf
sConsulta = sConsulta & ",OE.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",OE.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",OE.NUM_PED_ERP AS REFERENCIA" & vbCrLf
sConsulta = sConsulta & ",OE.PROVE + '' '' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",OE.PROVE AS PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN AS PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",OE.FECHA" & vbCrLf
sConsulta = sConsulta & ",OE.EST" & vbCrLf
sConsulta = sConsulta & ",OE.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + '','' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & ",OE.OBS" & vbCrLf
sConsulta = sConsulta & ",1 MOTIVO" & vbCrLf
sConsulta = sConsulta & ",COUNT(ORDEN_ENTREGA_ADJUN.ID)  ARCH" & vbCrLf
sConsulta = sConsulta & ", OE.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, OE.COD_PROVE_ERP, OE.RECEPTOR ,R.NOM + '' '' + R.APE NOMRECEPTOR, R.TFNO TFNORECEPTOR, R.EMAIL EMAILRECEPTOR, ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) UONRECEPTOR, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) DENUONRECEPTOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "INNER JOIN SEGURIDAD ON LINEAS_PEDIDO.SEGURIDAD=SEGURIDAD.ID AND SEGURIDAD.APROB_TIPO1='''+ @APROBADOR +''' AND LINEAS_PEDIDO.NIVEL_APROB=0'" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE            " & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA OE ON LINEAS_PEDIDO.ORDEN=OE.ID'" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING= @SQLSTRING + ' AND OE.NUM=' + CONVERT(VARCHAR,@ORDEN ) " & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.NUM_PED_ERP = ''' + @REFERENCIA + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON OE.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  INNER JOIN PROVE ON OE.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  LEFT JOIN ORDEN_ENTREGA_ADJUN ON OE.ID=ORDEN_ENTREGA_ADJUN.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + ' LEFT JOIN EMP E ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (PER R LEFT JOIN UON1 ON R.UON1 = UON1.COD LEFT JOIN UON2 ON R.UON1 = UON2.UON1 AND R.UON2 = UON2.COD LEFT JOIN UON3 ON R.UON1 = UON3.UON1 AND R.UON2 = UON3.UON2 AND R.UON3 = UON3.COD) ON OE.RECEPTOR = R.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + ' WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO1 is NOT NULL and @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO1 = ' + CONVERT(VARCHAR,@CAMPO1) + N'   AND LINEAS_PEDIDO.VALOR1 = ''' + @VALORCAMPO1 +''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO2 is NOT NULL and @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO2 = ' + CONVERT(VARCHAR,@CAMPO2) + N'   AND LINEAS_PEDIDO.VALOR2 = ''' + @VALORCAMPO2 + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT1 = ' + CONVERT(VARCHAR,@CATN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT2 = ' + CONVERT(VARCHAR,@CATN2) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT3 = ' + CONVERT(VARCHAR,@CATN3) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT4 = ' + CONVERT(VARCHAR,@CATN4) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT5 = ' + CONVERT(VARCHAR,@CATN5) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>'' " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.FECHA >=CONVERT(DATETIME,''' + @DESDEFECHA + ''',103) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA  IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.FECHA <= CONVERT(datetime,''' + @HASTAFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.EMPRESA=' +CONVERT(VARCHAR,@EMPRESA )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.RECEPTOR  = ''' + @RECEPTOR + '''' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.COD_PROVE_ERP = ''' +  @COD_ERP + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' GROUP BY OE.ID, OE.ANYO ,PEDIDO.NUM ,OE.NUM " & vbCrLf
sConsulta = sConsulta & ",OE.NUM_PED_ERP ,OE.PROVE + '' '' + PROVE.DEN ,OE.PROVE ,PROVE.DEN " & vbCrLf
sConsulta = sConsulta & ",OE.FECHA ,OE.EST ,OE.IMPORTE   ,PER.APE + '','' + PER.NOM ,OE.OBS,  OE.EMPRESA, E.DEN , E.NIF , OE.COD_PROVE_ERP, OE.RECEPTOR ,R.NOM + '' '' + R.APE , R.TFNO , R.EMAIL , ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) , ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN))  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' UNION SELECT DISTINCT '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' OE.ID AS ORDENID,OE.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",OE.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",OE.NUM_PED_ERP AS REFERENCIA" & vbCrLf
sConsulta = sConsulta & ",OE.PROVE + '' '' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",OE.PROVE AS PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN AS PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",OE.FECHA" & vbCrLf
sConsulta = sConsulta & ",OE.EST" & vbCrLf
sConsulta = sConsulta & ",OE.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + '','' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & ",OE.OBS" & vbCrLf
sConsulta = sConsulta & ",2 MOTIVO" & vbCrLf
sConsulta = sConsulta & ",COUNT(ORDEN_ENTREGA_ADJUN.ID)  ARCH" & vbCrLf
sConsulta = sConsulta & ", OE.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, OE.COD_PROVE_ERP, OE.RECEPTOR ,R.NOM + '' '' + R.APE NOMRECEPTOR, R.TFNO TFNORECEPTOR, R.EMAIL EMAILRECEPTOR, ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) UONRECEPTOR, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) DENUONRECEPTOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO INNER JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.APROB_LIM=APROB_LIM.ID AND APROB_LIM.PER='''+ @APROBADOR +''''" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0    " & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA OE ON LINEAS_PEDIDO.ORDEN=OE.ID '" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING= @SQLSTRING + ' AND OE.NUM=' + CONVERT(VARCHAR,@ORDEN ) " & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.NUM_PED_ERP = ''' + @REFERENCIA + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON OE.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  LEFT JOIN ORDEN_ENTREGA_ADJUN ON OE.ID=ORDEN_ENTREGA_ADJUN.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + ' INNER JOIN PROVE ON OE.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + ' LEFT JOIN EMP E ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (PER R LEFT JOIN UON1 ON R.UON1 = UON1.COD LEFT JOIN UON2 ON R.UON1 = UON2.UON1 AND R.UON2 = UON2.COD LEFT JOIN UON3 ON R.UON1 = UON3.UON1 AND R.UON2 = UON3.UON2 AND R.UON3 = UON3.COD) ON OE.RECEPTOR = R.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  WHERE NOT EXISTS (SELECT* FROM LINEAS_PEDIDO LP  INNER JOIN SEGURIDAD ON LP.SEGURIDAD=SEGURIDAD.ID AND SEGURIDAD.APROB_TIPO1='''+ @APROBADOR +''' AND LP.NIVEL_APROB=0 AND LP.EST=0 AND LP.ID=LINEAS_PEDIDO.ID)'" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO1 is NOT NULL and @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO1 = ' + CONVERT(VARCHAR,@CAMPO1) + N'   AND LINEAS_PEDIDO.VALOR1 = ''' + @VALORCAMPO1 +''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO2 is NOT NULL and @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO2 = ' + CONVERT(VARCHAR,@CAMPO2) + N'   AND LINEAS_PEDIDO.VALOR2 = ''' + @VALORCAMPO2 + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT1 = ' + CONVERT(VARCHAR,@CATN1) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT2 = ' + CONVERT(VARCHAR,@CATN2) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT3 = ' + CONVERT(VARCHAR,@CATN3) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT4 = ' + CONVERT(VARCHAR,@CATN4) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT5 = ' + CONVERT(VARCHAR,@CATN5) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.FECHA >=CONVERT(DATETIME,''' + @DESDEFECHA + ''',103) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA  IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.FECHA <= CONVERT(datetime,''' + @HASTAFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.EMPRESA=' +CONVERT(VARCHAR,@EMPRESA )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.RECEPTOR  = ''' + @RECEPTOR + '''' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.COD_PROVE_ERP = ''' +  @COD_ERP + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' GROUP BY OE.ID, OE.ANYO ,PEDIDO.NUM ,OE.NUM " & vbCrLf
sConsulta = sConsulta & ",OE.NUM_PED_ERP ,OE.PROVE + '' '' + PROVE.DEN ,OE.PROVE ,PROVE.DEN " & vbCrLf
sConsulta = sConsulta & ",OE.FECHA ,OE.EST ,OE.IMPORTE ,PER.APE + '','' + PER.NOM " & vbCrLf
sConsulta = sConsulta & ",OE.OBS, OE.EMPRESA, E.DEN , E.NIF , OE.COD_PROVE_ERP, OE.RECEPTOR ,R.NOM + '' '' + R.APE , R.TFNO , R.EMAIL , ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) , ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN))  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--PRINT @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "SET ROWCOUNT @MAXLINEAS" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "SET ROWCOUNT 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_CONTAR_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_CONTAR_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_CONTAR_ORDENES(@APROBADOR VARCHAR(50), @MAXLINEAS INTEGER, @DESDEID INTEGER," & vbCrLf
sConsulta = sConsulta & "@ANYO INTEGER, @NUMPEDIDO INTEGER, @ORDEN INTEGER, @APROBADOS INTEGER,@DENEGADOS INTEGER,@CODPROVE VARCHAR(50),@NUMPEDIDOPROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & ",@DESDEFECHA VARCHAR(10), @APROVISIONADOR VARCHAR(50) ,@REFERENCIA varchar(120) = null ,@CAMPO1 int = null ,@CAMPO2 int = null," & vbCrLf
sConsulta = sConsulta & " @VALORCAMPO1 varchar(150) = null, @VALORCAMPO2 varchar(150)=null , @CATN1 int = null, @CATN2 int = null, @CATN3 int = null, @CATN4 int = null , @CATN5 int = null ," & vbCrLf
sConsulta = sConsulta & " @EMPRESA INT = NULL, @RECEPTOR VARCHAR(100) = NULL, @COD_ERP  VARCHAR(100) = NULL, @HASTAFECHA VARCHAR(10))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INDICE INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= 'SELECT DISTINCT ORDEN_ENTREGA.ID FROM LINEAS_PEDIDO INNER JOIN SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.SEGURIDAD=SEGURIDAD.ID AND APROB_TIPO1='''+ @APROBADOR +''' AND LINEAS_PEDIDO.NIVEL_APROB=0'" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE            " & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO1 is NOT NULL and @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO1 = ' + CONVERT(VARCHAR,@CAMPO1) + N'   AND LINEAS_PEDIDO.VALOR1 = ''' + @VALORCAMPO1 +''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO2 is NOT NULL and @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO2 = ' + CONVERT(VARCHAR,@CAMPO2) + N'   AND LINEAS_PEDIDO.VALOR2 = ''' + @VALORCAMPO2 + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT1 = ' + CONVERT(VARCHAR,@CATN1) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT2 = ' + CONVERT(VARCHAR,@CATN2) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT3 = ' + CONVERT(VARCHAR,@CATN3) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT4 = ' + CONVERT(VARCHAR,@CATN4) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT5 = ' + CONVERT(VARCHAR,@CATN5) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID'" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' +CONVERT(VARCHAR,@ORDEN )" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + '   AND ORDEN_ENTREGA.NUM_PED_ERP = ''' + @REFERENCIA + ''''" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>'' " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >= CONVERT(DATETIME,''' +  @DESDEFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA is not null" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.FECHA <= CONVERT(datetime,''' + @HASTAFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.EMPRESA=' +CONVERT(VARCHAR,@EMPRESA )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.RECEPTOR  = ''' + @RECEPTOR + '''' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.COD_PROVE_ERP = ''' +  @COD_ERP + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' UNION SELECT DISTINCT ORDEN_ENTREGA.ID FROM LINEAS_PEDIDO INNER JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.APROB_LIM=APROB_LIM.ID AND APROB_LIM.PER='''+ @APROBADOR +''''" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0    " & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO1 is NOT NULL and @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO1 = ' + CONVERT(VARCHAR,@CAMPO1) + N'   AND LINEAS_PEDIDO.VALOR1 = ''' + @VALORCAMPO1 +''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO2 is NOT NULL and @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO2 = ' + CONVERT(VARCHAR,@CAMPO2) + N'   AND LINEAS_PEDIDO.VALOR2 = ''' + @VALORCAMPO2 + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT1 = ' + CONVERT(VARCHAR,@CATN1) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT2 = ' + CONVERT(VARCHAR,@CATN2) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT3 = ' + CONVERT(VARCHAR,@CATN3) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT4 = ' + CONVERT(VARCHAR,@CATN4)  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT5 = ' + CONVERT(VARCHAR,@CATN5) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID '" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' +CONVERT(VARCHAR,@ORDEN )" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + '   AND ORDEN_ENTREGA.NUM_PED_ERP = ''' + @REFERENCIA + ''''" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER WHERE 1=1 '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >= CONVERT(DATETIME,''' + @DESDEFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA is not null" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.FECHA <= CONVERT(datetime,''' + @HASTAFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.EMPRESA=' +CONVERT(VARCHAR,@EMPRESA )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.RECEPTOR  = ''' + @RECEPTOR + '''' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.COD_PROVE_ERP = ''' +  @COD_ERP + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ID_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ID_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ID_ORDENES @MAXLINEAS integer, @APROBADOR varchar(50) =null,@APROVISIONADOR varchar(50)  =null,@ARTINT varchar(50)  =null, @ARTDEN varchar(500)  =null, @ARTEXT varchar(500)  =null, @DESDEFECHA varchar(10)  =null, @HASTAFECHA varchar(10)  =null, @PROVECOD varchar(50)  =null,@PROVEDEN varchar(500)  =null,@NUMPEDPROVE varchar(100)  =null,@ANYO int  =null,@PEDIDO int  =null,@ORDEN int  =null,@EST int  =null ,@COINCIDENCIA int =null,@REFERENCIA varchar(120)  =null,@CAMPO1 int=null ,@CAMPO2 int=null, @VALORCAMPO1 varchar(150)=null,@VALORCAMPO2 varchar(150)=null,@CATN1 int = NULL, @CATN2 int = NULL, @CATN3 int = NULL, @CATN4 int = NULL ,@CATN5 int = NULL, @EMPRESA INT = NULL, @RECEPTOR VARCHAR(100) = NULL, @COD_ERP  VARCHAR(100) = NULL, @TIPO TINYINT = 1  AS" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(1000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "set  @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID " & vbCrLf
sConsulta = sConsulta & "                                           AND LP.NIVEL_APROB = 0) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null or @CATN1 is not null or @CATN2 is not null or @CATN3 is not null or @CATN4 is not null or @CATN5 is not null or @CAMPO1 is not null or @CAMPO2 is not null or @VALORCAMPO1 is not null or @VALORCAMPO2 is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART'             " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO1 = @CAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO2 = @CAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR1 = @VALORCAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR2 = @VALORCAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT1 = @CATN1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT2 = @CATN2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT3 = @CATN3 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT4 = @CATN4 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT5 = @CATN5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR LP.ART_INT + I.ART=@ARTINT)'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like + ''%'' + @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE =@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "   IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (PA.COD_EXT like ''% ''  + @ARTEXT + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like @ARTEXT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like ''% '' + @ARTEXT ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR PA.COD_EXT =@ARTEXT)'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like + ''%'' + @ARTEXT + ''%'''  " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 1            " & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @TIPO= 0" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=0'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM_PED_ERP = @REFERENCIA '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.EMPRESA = @EMPRESA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.RECEPTOR = @RECEPTOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.COD_PROVE_ERP = @COD_ERP '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "   set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = '" & vbCrLf
sConsulta = sConsulta & "create table #IDS (id varchar(200))" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "DECLARE @miID varchar(200)" & vbCrLf
sConsulta = sConsulta & "declare @cont int" & vbCrLf
sConsulta = sConsulta & "set @cont=1" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "if @cont = 1" & vbCrLf
sConsulta = sConsulta & " insert into #IDS values (@miID)" & vbCrLf
sConsulta = sConsulta & "set @cont = @cont + 1        " & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "if @cont = ' + CONVERT(VARCHAR,@MAXLINEAS) + ' + 1" & vbCrLf
sConsulta = sConsulta & " set @cont = 1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "set nocount off" & vbCrLf
sConsulta = sConsulta & "select * from #ids" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @EST int,@REFERENCIA varchar(120), @CAMPO1 int ,@CAMPO2 int, @VALORCAMPO1 varchar(150) ,@VALORCAMPO2 varchar(150) ,@CATN1 int ,@CATN2 int ,@CATN3 int, @CATN4 int , @CATN5 int, @EMPRESA INT, @RECEPTOR VARCHAR(100), @COD_ERP VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL2, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT=@ARTEXT, @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO, @EST=@EST,@REFERENCIA=@REFERENCIA , @CAMPO1=@CAMPO1 , @CAMPO2=@CAMPO2 ,@VALORCAMPO1=@VALORCAMPO1 , @VALORCAMPO2=@VALORCAMPO2 , @CATN1=@CATN1 , @CATN2=@CATN2 ,@CATN3=@CATN3 ,@CATN4=@CATN4, @CATN5=@CATN5, @EMPRESA = @EMPRESA, @RECEPTOR = @RECEPTOR, @COD_ERP = @COD_ERP" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

 sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ORDENES @MAXLINEAS integer, @DESDEID varchar(50)=null, @APROBADOR varchar(50) = null,@APROVISIONADOR varchar(50) = null, @ARTINT varchar(50) = null, @ARTDEN varchar(500) = null, @ARTEXT varchar(500) = null, @DESDEFECHA varchar(10) = null, @HASTAFECHA varchar(10) = null, @PROVECOD varchar(50) = null, @PROVEDEN varchar(500) = null,@NUMPEDPROVE varchar(100) = null,@ANYO int = null,@PEDIDO int = null,@ORDEN int = null,@EST int  = null, @ID int = null ,@RECINCORRECTO int = null ,@COINCIDENCIA int =null,@REFERENCIA varchar(120) = null ,@CAMPO1 int = null ,@CAMPO2 int = null, @VALORCAMPO1 varchar(150) = null, @VALORCAMPO2 varchar(150)=null , @CATN1 int = null, @CATN2 int = null, @CATN3 int = null, @CATN4 int = null , @CATN5 int = null, @EMPRESA INT = NULL, @RECEPTOR VARCHAR(100) = NULL, @COD_ERP  VARCHAR(100) = NULL, @ENTIDAD VARCHAR(100) = 'ORDEN', @TIPO TINYINT = null AS " & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(1000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL1 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'PROVE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT DISTINCT P.COD, P.DEN , P.NIF, P.DIR, P.CP, P.PAI, P.POB, P.PROVI'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT DISTINCT E.ID, E.NIF, E.DEN, E.DIR, E.CP, E.POB, E.PROVI, E.PAI, PAI.DEN PAIDEN, PROVI.DEN PROVIDEN '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT TOP ' + CONVERT(varchar,@MAXLINEAS) + ' CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID), " & vbCrLf
sConsulta = sConsulta & "       OE.ID, OE.ANYO, PE.NUM NUMPEDIDO, OE.NUM NUMORDEN, OE.PROVE PROVECOD, P.DEN PROVEDEN, P.NIF," & vbCrLf
sConsulta = sConsulta & "       OE.NUMEXT, OE.FECHA FECHA, OE.EST EST, OE.IMPORTE IMPORTE,OE.CAMBIO, " & vbCrLf
sConsulta = sConsulta & "       P1.APE + '', '' + P1.NOM APROVISIONADOR,P1.APE,P1.NOM,OE.OBS," & vbCrLf
sConsulta = sConsulta & "       OES.COMENT, OES.FECHA FECEST,OE.IMPORTE,OE.PEDIDO,OE.TIPO, OE.INCORRECTA, PE.PER,OE.NUM_PED_ERP AS REFERENCIA,T3.ADJUNTOS, CP.COMENT COMENTPROVE, CP.EST ESTCOMENT,OE.MON," & vbCrLf
sConsulta = sConsulta & "       OE.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, OE.COD_PROVE_ERP, " & vbCrLf
sConsulta = sConsulta & "       OE.RECEPTOR ,PER.NOM + '' '' + PER.APE NOMRECEPTOR, PER.TFNO TFNORECEPTOR, PER.EMAIL EMAILRECEPTOR, ISNULL(PER.UON3,ISNULL(PER.UON2,PER.UON1)) UONRECEPTOR, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) DENUONRECEPTOR," & vbCrLf
sConsulta = sConsulta & "      PE.TIPO, PR.FECHA FECHARECEP, OE.CAMBIO,MON.DEN AS MONDEN,OE.PAG,PAG.DEN AS PAGDEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (EMP E LEFT JOIN PAI ON E.PAI = PAI.COD LEFT JOIN PROVI ON E.PAI = PROVI.PAI AND E.PROVI = PROVI.COD) ON OE.EMPRESA = E.ID'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN EMP E ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (PER LEFT JOIN UON1 ON PER.UON1 = UON1.COD LEFT JOIN UON2 ON PER.UON1 = UON2.UON1 AND PER.UON2 = UON2.COD LEFT JOIN UON3 ON PER.UON1 = UON3.UON1 AND PER.UON2 = UON3.UON2 AND PER.UON3 = UON3.COD) ON OE.RECEPTOR = PER.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT OE.ID, OE.PEDIDO, OE.ORDEN, OE.EST, OE.COMENT " & vbCrLf
sConsulta = sConsulta & "                  FROM ORDEN_EST OE " & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN (SELECT PEDIDO, ORDEN, MAX(ID) ID" & vbCrLf
sConsulta = sConsulta & "                                      FROM ORDEN_EST  WHERE EST IN (3, 21) " & vbCrLf
sConsulta = sConsulta & "                                    GROUP BY PEDIDO, ORDEN ) MO " & vbCrLf
sConsulta = sConsulta & "                                ON MO.ID = OE.ID) CP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = CP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID " & vbCrLf
sConsulta = sConsulta & "                                           AND LP.NIVEL_APROB = 0) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (SELECT COUNT(*) AS ADJUNTOS,ORDEN  FROM orden_entrega_adjun GROUP BY orden_entrega_adjun.ORDEN) T3 ON OE.ID= T3.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN (PEDIDO_RECEP PR INNER JOIN (SELECT PEDIDO, ORDEN, MAX(ID) ID FROM PEDIDO_RECEP GROUP BY PEDIDO,ORDEN)  PR2 ON PR.ID=PR2.ID )" & vbCrLf
sConsulta = sConsulta & "             ON PR.ORDEN= OE.ID'" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN MON  ON MON.COD= OE.MON" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PAG  ON PAG.COD= OE.PAG'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null or @CATN1 is not null or @CATN2 is not null or @CATN3 is not null or @CATN4 is not null or @CATN5 is not null or @CAMPO1 is not null or @CAMPO2 is not null or @VALORCAMPO1 is not null or @VALORCAMPO2 is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO1 = @CAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO2 = @CAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR1 = @VALORCAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR2 = @VALORCAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT1 = @CATN1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT2 = @CATN2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT3 = @CATN3 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT4 = @CATN4 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT5 = @CATN5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR LP.ART_INT + I.ART=@ARTINT)'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like + ''%'' + @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE =@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "   IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (PA.COD_EXT like ''% ''  + @ARTEXT + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like @ARTEXT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like ''% '' + @ARTEXT ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR PA.COD_EXT =@ARTEXT)'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like + ''%'' + @ARTEXT + ''%'''  " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 1            " & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @TIPO= 0" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=0'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM_PED_ERP = @REFERENCIA '" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.EMPRESA = @EMPRESA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.RECEPTOR = @RECEPTOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.COD_PROVE_ERP = @COD_ERP '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL1 = ''" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = ''" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "     set @SQL1=' OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "IF @RECINCORRECTO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "  IF @RECINCORRECTO=1" & vbCrLf
sConsulta = sConsulta & "   set @SQL2 = ' OE.INCORRECTA=1'  " & vbCrLf
sConsulta = sConsulta & "IF @SQL1<>'' and @SQL2<>'' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND (' + @SQL1 + ' OR ' + @SQL2 + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "if @SQL1!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL1 " & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "if @SQL2!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL2" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @ID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.ID=@ID '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "if @DESDEID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)>=@DESDEID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'PROVE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL =  @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    ORDER BY P.COD '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL =  @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    ORDER BY E.NIF '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @DESDEID varchar(50), @EST int, @ID int,@REFERENCIA varchar(120) , @CAMPO1 int ,@CAMPO2 int, @VALORCAMPO1 varchar(150) ,@VALORCAMPO2 varchar(150) ,@CATN1 int ,@CATN2 int ,@CATN3 int, @CATN4 int , @CATN5 int, @EMPRESA INT, @RECEPTOR VARCHAR(100), @COD_ERP VARCHAR(100) '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT =@ARTEXT , @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO,@DESDEID=@DESDEID, @EST=@EST, @ID = @ID, @REFERENCIA=@REFERENCIA , @CAMPO1=@CAMPO1 , @CAMPO2=@CAMPO2 ,@VALORCAMPO1=@VALORCAMPO1 , @VALORCAMPO2=@VALORCAMPO2 , @CATN1=@CATN1 , @CATN2=@CATN2 ,@CATN3=@CATN3 ,@CATN4=@CATN4, @CATN5=@CATN5 , @EMPRESA = @EMPRESA, @RECEPTOR = @RECEPTOR, @COD_ERP = @COD_ERP" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ATRIBUTOS_DE_LINEA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ATRIBUTOS_DE_LINEA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ATRIBUTOS_DE_LINEA(@LINEA INTEGER)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT DA.COD, DA.DEN, DA.TIPO, PA.ATRIB, PA.INTRO,PA.INTERNO,PA.MINNUM,PA.MAXNUM,PA.MINFEC,PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "  PA.VALOR_TEXT, PA.VALOR_NUM,PA.VALOR_FEC,PA.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PEDIDO_ATRIB PA INNER JOIN DEF_ATRIB DA ON DA.ID=PA.ATRIB" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.LINEA= @LINEA" & vbCrLf
sConsulta = sConsulta & "   ORDER BY DA.COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ATRIBUTOS_DE_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ATRIBUTOS_DE_ORDEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ATRIBUTOS_DE_ORDEN(@ORDENID INTEGER)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT DA.COD, DA.DEN, DA.TIPO, PA.ATRIB, PA.INTRO,PA.INTERNO,PA.MINNUM,PA.MAXNUM,PA.MINFEC,PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "  PA.VALOR_TEXT, PA.VALOR_NUM,PA.VALOR_FEC,PA.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM ORDEN_ENTREGA_ATRIB PA INNER JOIN DEF_ATRIB DA ON DA.ID=PA.ATRIB" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ORDEN= @ORDENID" & vbCrLf
sConsulta = sConsulta & "   ORDER BY DA.COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
