Attribute VB_Name = "bas_V_31600_6_2"
Public Function CodigoDeActualizacion31600_06_0143A_06_0144() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Actualiza tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
frmProgreso.ProgressBar1.Refresh
V_31600_Tablas_44


frmProgreso.lblDetalle = "Actualiza storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
frmProgreso.ProgressBar1.Refresh
V_31600_Storeds_44
V_31600_Storeds_44_2
V_31600_Storeds_44_3


frmProgreso.lblDetalle = "Actualiza triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
frmProgreso.ProgressBar1.Refresh
V_31600_Triggers_44

sConsulta = "UPDATE VERSION SET NUM ='3.00.01.44'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31600.7'"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion31600_06_0143A_06_0144 = True
Exit Function

Error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31600_06_0143A_06_0144 = False
End Function


Private Sub V_31600_Tablas_44()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_PM_CONDICIONES_BLOQUEO_CONDICIONES_PM_CONDICIONES_BLOQUEO]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PM_CONDICIONES_BLOQUEO_CONDICIONES] DROP FK_PM_CONDICIONES_BLOQUEO_CONDICIONES_PM_CONDICIONES_BLOQUEO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PM_CONDICIONES_BLOQUEO_CONDICIONES] ADD " & vbCrLf
sConsulta = sConsulta & "            CONSTRAINT [FK_PM_CONDICIONES_BLOQUEO_CONDICIONES_PM_CONDICIONES_BLOQUEO] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "            (" & vbCrLf
sConsulta = sConsulta & "                        [ID_COD]" & vbCrLf
sConsulta = sConsulta & "            ) REFERENCES [dbo].[PM_CONDICIONES_BLOQUEO] (" & vbCrLf
sConsulta = sConsulta & "                        [ID]" & vbCrLf
sConsulta = sConsulta & "            ) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_PM_COPIA_CONDICIONES_BLOQUEO_CONDICIONES_PM_COPIA_CONDICIONES_BLOQUEO]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PM_COPIA_CONDICIONES_BLOQUEO_CONDICIONES] DROP FK_PM_COPIA_CONDICIONES_BLOQUEO_CONDICIONES_PM_COPIA_CONDICIONES_BLOQUEO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PM_COPIA_CONDICIONES_BLOQUEO_CONDICIONES] ADD " & vbCrLf
sConsulta = sConsulta & "            CONSTRAINT [FK_PM_COPIA_CONDICIONES_BLOQUEO_CONDICIONES_PM_COPIA_CONDICIONES_BLOQUEO] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "            (" & vbCrLf
sConsulta = sConsulta & "                        [ID_COD]" & vbCrLf
sConsulta = sConsulta & "            ) REFERENCES [dbo].[PM_COPIA_CONDICIONES_BLOQUEO] (" & vbCrLf
sConsulta = sConsulta & "                        [ID]" & vbCrLf
sConsulta = sConsulta & "            ) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub




Private Sub V_31600_Storeds_44()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTANCIAS_PADRE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTANCIAS_PADRE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTANCIAS_PADRE @IDI VARCHAR(10),@MON AS VARCHAR(3)=NULL, @id as INT=0,@DEN VARCHAR(200) = NULL, @USU VARCHAR(10)=null, @TIPO as int = 0,@UON1 VARCHAR(3)=null,@UON2 VARCHAR(3)=null,@UON3 VARCHAR(3)=null,@FORMATOFECHA varchar(10)='dd/mm/yyyy', @FORMID INT=0,@INSTANCIA INT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFROM  AS NVARCHAR(4000) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SFROMIMPORTE NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE     AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SMON AS VARCHAR(3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOFECHA AS VARCHAR(3)" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=101" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=105" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=110" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=104" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--OBTIENE LA MONEDA DEL FORMULARIO DE LA INSTANCIA" & vbCrLf
sConsulta = sConsulta & "IF @MON IS NULL" & vbCrLf
sConsulta = sConsulta & "   IF @FORMID = 0" & vbCrLf
sConsulta = sConsulta & "   IF @INSTANCIA IS NULL " & vbCrLf
sConsulta = sConsulta & "               SET @MON = 'EUR'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SELECT @SMON= F.MON FROM INSTANCIA I INNER JOIN SOLICITUD S WITH (NOLOCK) ON S.ID = I.SOLICITUD INNER JOIN FORMULARIO F WITH (NOLOCK) ON F.ID = S.FORMULARIO WHERE I.ID = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "        SET @MON = @SMON  " & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @SMON=MON FROM FORMULARIO WHERE ID = @FORMID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @MON = @SMON" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SFROM =N'SELECT DISTINCT MAX(I.ID) ID_INS" & vbCrLf
sConsulta = sConsulta & ",CASE WHEN MAX(INSTANCIA_VISORES_PM.TITULO_FEC) IS NULL THEN MAX(INSTANCIA_VISORES_PM.TITULO_TEXT) ELSE CONVERT(VARCHAR ,MAX(INSTANCIA_VISORES_PM.TITULO_FEC),103) END DESCR" & vbCrLf
sConsulta = sConsulta & ",MAX(I.FEC_ALTA) Fecha,MAX(I.IMPORTE/I.CAMBIO/MON.EQUIV) Importe,CASE WHEN SUM(T.ACUM) IS NULL THEN MAX(I.IMPORTE/I.CAMBIO/MON.EQUIV) ELSE CASE WHEN (MAX(I.IMPORTE*I.CAMBIO/MON.EQUIV) - SUM(T.ACUM)) > 0 THEN MAX(I.IMPORTE/I.CAMBIO/MON.EQUIV) - SUM(T.ACUM) ELSE 0  END END  Disponible ,SUM(T.ACUM) Acumulado, @MON Moneda'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SFROM = @SFROM + ' FROM INSTANCIA I WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER ON I.PETICIONARIO=PER.COD INNER JOIN DEP ON PER.DEP=DEP.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN INSTANCIA_VISORES_PM ON I.ID=INSTANCIA_VISORES_PM.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--DISPONIBLE" & vbCrLf
sConsulta = sConsulta & "SET @SFROMIMPORTE = ' LEFT JOIN ( " & vbCrLf
sConsulta = sConsulta & "SELECT  CC.VALOR_NUM,(CC2.VALOR_NUM * I2.CAMBIO) ACUM" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I  WITH (NOLOCK)ON I.ID = CC.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD  WITH (NOLOCK) ON CCD.ID = CC.COPIA_CAMPO_DEF AND CCD.TIPO_CAMPO_GS=128" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC2  WITH (NOLOCK) ON CC2.INSTANCIA = CC.INSTANCIA AND CC2.NUM_VERSION=(SELECT MAX(NUM_VERSION) FROM VERSION_INSTANCIA WITH (NOLOCK) WHERE INSTANCIA=I.ID)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD2 WITH (NOLOCK) ON CCD2.ID = CC2.COPIA_CAMPO_DEF AND CCD2.IMPORTE_WORKFL=1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I2 WITH (NOLOCK) ON I2.ID = CC.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "WHERE CC.NUM_VERSION=(SELECT MAX(NUM_VERSION) FROM VERSION_INSTANCIA WITH (NOLOCK) WHERE INSTANCIA=I.ID)" & vbCrLf
sConsulta = sConsulta & "AND (I.ESTADO = 0 OR I.ESTADO = 2 OR I.ESTADO = 100  OR I.ESTADO = 101  OR I.ESTADO = 104) AND I.NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & ") T  ON T.VALOR_NUM = I.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MON WITH (NOLOCK) ON MON.COD = @MON '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = ' WHERE 1 = 1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO <> 0 " & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE +  ' AND S.ID= @TIPO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID <> 0 " & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE +  ' AND I.ID= @ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET  @SWHERE = @SWHERE + ' AND I.PETICIONARIO = @USU'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @UON1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND PER.UON1 = @UON1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @UON2 IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND PER.UON2 = @UON2'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @UON3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND PER.UON3 = @UON3'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET  @SWHERE = @SWHERE +    ' AND CASE WHEN C1.SUBTIPO = 3 THEN CONVERT(VARCHAR ,INSTANCIA_VALORES_PM.TITULO_FEC,'+@TIPOFECHA+')'  +  ' ELSE INSTANCIA_VALORES_PM.TITULO_TEXT END LIKE ' +''''+'%' + @DEN +  '%' + ''''" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE +  ' AND I.ESTADO IN (100,101,104) AND I.NUM IS NOT NULL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE +  ' AND EXISTS (SELECT 1 FROM COPIA_CAMPO CC3  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CCD3 WITH (NOLOCK) ON CCD3.ID = CC3.COPIA_CAMPO_DEF AND TIPO_CAMPO_GS = 127" & vbCrLf
sConsulta = sConsulta & "       WHERE CC3.INSTANCIA = I.ID) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SFROM + @SFROMIMPORTE + @SWHERE  +  ' GROUP BY I.ID  ORDER BY ID_INS DESC  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,N' @MON AS VARCHAR(3)=''EUR'',@id as INT=0,@DEN VARCHAR(200) = NULL,@USU VARCHAR(10)=null, @TIPO as int = 0,@UON1 VARCHAR(3)=null,@UON2 VARCHAR(3)=null,@UON3 VARCHAR(3)=null,@FORMATOFECHA varchar(10)=''dd/mm/yyyy''', @MON=@MON,@ID=@ID,@DEN=@DEN,@USU=@USU, @TIPO=@TIPO,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@FORMATOFECHA=@FORMATOFECHA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETWORKFLOW_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETWORKFLOW_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETWORKFLOW_PROVE @PRV VARCHAR(50), @IDI VARCHAR(50), @TI INT =0 , @FEC  VARCHAR(10)=NULL, @INS INT=0, @FILAESTADO INT =0, @FORMATOFECHA VARCHAR(20)='' AS" & vbCrLf
sConsulta = sConsulta & "--Carga los procesos que requieren la intervenci�n del usuario y el hist�rico de procesos en los que particip�:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE  AS NVARCHAR(4000) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM AS NVARCHAR(4000) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM1 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM2 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM3 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM4 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE2  AS NVARCHAR(4000) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOFECHA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='dd/MM/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=101" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=105" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=110" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=104" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = '' --WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "IF @TI <> 0" & vbCrLf
sConsulta = sConsulta & "   IF  @SWHERE=''" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE =  ' WHERE I.SOLICITUD=@TI'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SWHERE = @SWHERE +  ' AND I.SOLICITUD=@TI'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   IF @SWHERE=''" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE = ' WHERE I.FEC_ALTA>=CONVERT(datetime,@FEC,103)'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE = @SWHERE + ' AND I.FEC_ALTA>=CONVERT(datetime,@FEC,103)'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @INS<>0" & vbCrLf
sConsulta = sConsulta & "   IF @SWHERE=''" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE =  ' WHERE I.ID=@INS'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE = @SWHERE +  ' AND I.ID=@INS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SWHERE=''" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = ' WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------PROCESOS QUE REQUIEREN LA INTERVENCI�N DEL USUARIO (trasladados m�s los propios del rol)----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT COUNT(*) AS PENDIENTES " & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA I WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE  WITH (NOLOCK) ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND INSTANCIA_BLOQUE.BLOQ<>1 AND INSTANCIA_BLOQUE.TRASLADADA=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE WITH (NOLOCK)  ON INSTANCIA_BLOQUE.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL WITH (NOLOCK)  ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID AND PM_COPIA_ROL.PROVE=@PRV'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' UNION " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA I WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE WITH (NOLOCK)  ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND INSTANCIA_BLOQUE.BLOQ<>1 AND INSTANCIA_BLOQUE.TRASLADADA=1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN (" & vbCrLf
sConsulta = sConsulta & "   SELECT MAX(ID) ID, BLOQUE " & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_EST " & vbCrLf
sConsulta = sConsulta & "   GROUP BY BLOQUE" & vbCrLf
sConsulta = sConsulta & "   ) M ON INSTANCIA_BLOQUE.ID=M.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST  WITH (NOLOCK) ON M.ID=INSTANCIA_EST.ID AND INSTANCIA_EST.DESTINATARIO_PROV =@PRV'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE + ' AND I.ESTADO=2) SUMA1 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @TI INT, @FEC VARCHAR(10), @INS  INT', @PRV = @PRV,@TI=@TI,@FEC=@FEC,@INS=@INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------PROCESOS EN LOS QUE PARTICIP� EL USUARIO--------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- 2.- En curso" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT COUNT(*) AS EN_CURSO " & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I  WITH (NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND (INSTANCIA_EST.PROVE=@PRV OR DESTINATARIO_PROV=@PRV) AND I.ESTADO=2'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE + ') SUMA2 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @TI INT, @FEC VARCHAR(10), @INS  INT', @PRV = @PRV,@TI=@TI,@FEC=@FEC,@INS=@INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- 3.- Rechazadas" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT COUNT(*) AS RECHAZADAS " & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I  WITH (NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND (INSTANCIA_EST.PROVE=@PRV OR DESTINATARIO_PROV=@PRV) AND I.ESTADO=6' " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE + ') SUMA3 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @TI INT, @FEC VARCHAR(10), @INS  INT', @PRV = @PRV,@TI=@TI,@FEC=@FEC,@INS=@INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- 4.- Anuladas" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT COUNT(*) AS ANULADAS " & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I  WITH (NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND (INSTANCIA_EST.PROVE=@PRV OR DESTINATARIO_PROV=@PRV) AND I.ESTADO=8'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE + ' ) SUMA4 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @TI INT, @FEC VARCHAR(10), @INS  INT', @PRV = @PRV,@TI=@TI,@FEC=@FEC,@INS=@INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- 4.- Finalizadas" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT COUNT(*) AS FINALIZADAS " & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I  WITH (NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND (INSTANCIA_EST.PROVE=@PRV OR DESTINATARIO_PROV=@PRV) AND I.ESTADO>=100'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE + ' ) SUMA5 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @TI INT, @FEC VARCHAR(10), @INS  INT', @PRV = @PRV,@TI=@TI,@FEC=@FEC,@INS=@INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------Devuelve las instancias que cumplen las condiciones:--------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--T�tulo: campo normal o atributo" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM2 =N'SELECT DISTINCT S.ID ID_SOLIC,S.COD,CASE WHEN C1.TITULO_FEC IS NULL THEN C1.TITULO_TEXT ELSE CONVERT(VARCHAR ,C1.TITULO_FEC,@TIPOFECHA) END DESCR, I.ID ID_INS,IE.FECHA, " & vbCrLf
sConsulta = sConsulta & "CASE WHEN IE.PER IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "   E.NOM + '' '' + E.APE " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "   PR.DEN " & vbCrLf
sConsulta = sConsulta & "END NOM_USU," & vbCrLf
sConsulta = sConsulta & "D.DEN ORIGEN,I.ESTADO," & vbCrLf
sConsulta = sConsulta & "CASE WHEN M.PARALELO>1 THEN " & vbCrLf
sConsulta = sConsulta & "   NULL " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "   ISNULL(CB.DEN,'' '') " & vbCrLf
sConsulta = sConsulta & "END DENETAPA_ACTUAL " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA I  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK)  ON I.SOLICITUD=S.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VERSION_INSTANCIA VI  WITH (NOLOCK) ON I.ID=VI.INSTANCIA AND NUM_VERSION=(SELECT MAX(NUM_VERSION) FROM VERSION_INSTANCIA WHERE INSTANCIA=I.ID)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN " & vbCrLf
sConsulta = sConsulta & "      (" & vbCrLf
sConsulta = sConsulta & "      SELECT INSTANCIA, COUNT(*) AS PARALELO,MIN(BLOQUE) AS BLOQUE " & vbCrLf
sConsulta = sConsulta & "      FROM INSTANCIA_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      WHERE ESTADO=1 AND BLOQ<>1 " & vbCrLf
sConsulta = sConsulta & "      GROUP BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & "      ) M ON I.ID=M.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN INSTANCIA_VISORES_PM C1 WITH (NOLOCK)  ON VI.INSTANCIA=C1.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CREATE TABLE #GRID_INSTANCIAS2 (ID_SOLIC INT, COD VARCHAR(50) ,DESCR VARCHAR(500) , ID_INS INT, FECHA DATETIME, NOM_USU VARCHAR(500),ORIGEN VARCHAR(100), ESTADO INT, DENETAPA_ACTUAL VARCHAR(500))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILAESTADO=0 --Pendientes:" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL   + ' ORDER BY I.ID'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQLFROM2 + ' " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK)  ON M.INSTANCIA= IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=0" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE CRB  WITH (NOLOCK) ON IB.BLOQUE=CRB.BLOQUE " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R  WITH (NOLOCK) ON CRB.ROL=R.ID AND R.PROVE=@PRV" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA_CAMINO  WITH (NOLOCK) ON INSTANCIA_BLOQUE_DESTINO=IB.ID " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) ON INSTANCIA_CAMINO.INSTANCIA_EST=IE.ID " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE_DEN CB WITH (NOLOCK)  ON IB.BLOQUE=CB.BLOQUE AND CB.IDI= @IDI " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE PR WITH (NOLOCK)  ON IE.PROVE=PR.COD " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER E WITH (NOLOCK)  ON IE.PER=E.COD " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP D  WITH (NOLOCK) ON E.DEP=D.COD' " & vbCrLf
sConsulta = sConsulta & "   + @SWHERE + ' AND I.ESTADO=2'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' UNION ' + @SQLFROM2 + ' " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK)  ON M.INSTANCIA= IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN " & vbCrLf
sConsulta = sConsulta & "       (" & vbCrLf
sConsulta = sConsulta & "       SELECT MAX(ID) ID, BLOQUE " & vbCrLf
sConsulta = sConsulta & "       FROM INSTANCIA_EST " & vbCrLf
sConsulta = sConsulta & "       GROUP BY BLOQUE" & vbCrLf
sConsulta = sConsulta & "       ) P ON IB.ID=P.BLOQUE " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) ON P.ID=IE.ID AND IE.DESTINATARIO_PROV =@PRV " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE_DEN CB  WITH (NOLOCK) ON IB.BLOQUE=CB.BLOQUE AND CB.IDI=@IDI " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE PR  WITH (NOLOCK) ON IE.PROVE=PR.COD " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER E  WITH (NOLOCK) ON IE.PER=E.COD " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP D  WITH (NOLOCK) ON E.DEP=D.COD' " & vbCrLf
sConsulta = sConsulta & "   + @SWHERE + ' AND I.ESTADO=2'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL   + ' ORDER BY I.ID'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@PRV VARCHAR(50),@IDI VARCHAR(20),@TI INT,@FEC VARCHAR(10),@INS INT,@TIPOFECHA INT',@PRV = @PRV,@IDI=@IDI,@TI=@TI,@FEC=@FEC,@INS=@INS,@TIPOFECHA=@TIPOFECHA   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   IF  @FILAESTADO=2   --2 Rechazadas" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE = @SWHERE + ' AND I.ESTADO=6'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "       IF  @FILAESTADO=3   --3 Anuladas " & vbCrLf
sConsulta = sConsulta & "           SET @SWHERE = @SWHERE + ' AND I.ESTADO=8'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF  @FILAESTADO=4   ---4 finalizadas " & vbCrLf
sConsulta = sConsulta & "               SET @SWHERE = @SWHERE + ' AND I.ESTADO>=100'" & vbCrLf
sConsulta = sConsulta & "           ELSE  --1 En curso" & vbCrLf
sConsulta = sConsulta & "               SET @SWHERE = @SWHERE + ' AND I.ESTADO=2'" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "        end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= N'" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_BLOQUE IB  WITH (NOLOCK) ON M.INSTANCIA= IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PM_COPIA_BLOQUE_DEN CB  WITH (NOLOCK) ON IB.BLOQUE=CB.BLOQUE AND CB.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) ON I.ID=IE.INSTANCIA AND (IE.PROVE=@PRV OR IE.DESTINATARIO_PROV=@PRV)" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE PR WITH (NOLOCK)  ON IE.PROVE=PR.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER E WITH (NOLOCK)  ON IE.PER=E.COD " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP D WITH (NOLOCK)  ON E.DEP=D.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM2 = @SQLFROM2 + @SQL  + @SWHERE" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLFROM2,N'@PRV VARCHAR(50),@IDI VARCHAR(20),@TI INT,@FEC VARCHAR(10),@INS INT,@TIPOFECHA INT',@PRV = @PRV,@IDI=@IDI,@TI=@TI,@FEC=@FEC,@INS=@INS,@TIPOFECHA=@TIPOFECHA  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETINSTANCIAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETINSTANCIAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_GETINSTANCIAS @IDI AS VARCHAR(20), @USU AS VARCHAR(50),@ID AS INT=NULL,@TIPO AS INT=NULL,@FECHA AS VARCHAR(10)=NULL,@ESTADO VARCHAR(100)=NULL, @NUEVO_WORKFL TINYINT=0,@PARTICIPANTE TINYINT=0,@FORMATOFECHA VARCHAR(50)=NULL  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFROM  AS NVARCHAR(4000) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE  AS NVARCHAR(4000) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOFECHA AS INT" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=101" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=105" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=110" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=104" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SFROM =N'SELECT DISTINCT S.ID ID_SOLIC,S.COD, CASE WHEN C1.TITULO_FEC IS NULL THEN C1.TITULO_TEXT ELSE CONVERT(VARCHAR ,C1.TITULO_FEC,@TIPOFECHA) END DESCR, I.ID ID_INS '" & vbCrLf
sConsulta = sConsulta & "SET @SFROM = @SFROM + ', " & vbCrLf
sConsulta = sConsulta & "CASE WHEN I.ESTADO=0 THEN " & vbCrLf
sConsulta = sConsulta & "   I.FEC_ALTA" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   CASE WHEN I.ESTADO=2 THEN " & vbCrLf
sConsulta = sConsulta & "       MR.FECHA " & vbCrLf
sConsulta = sConsulta & "   ELSE " & vbCrLf
sConsulta = sConsulta & "       IE.FECHA " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END FECHA" & vbCrLf
sConsulta = sConsulta & ",I.ESTADO, CASE WHEN I.ESTADO=2 THEN CASE WHEN R.PER IS NOT NULL THEN R.PER END ELSE CASE WHEN IE.PER IS NULL THEN I.PETICIONARIO ELSE IE.PER END END PER, CASE WHEN I.ESTADO=2 THEN PROVE.COD ELSE NULL END PROVE," & vbCrLf
sConsulta = sConsulta & " CASE WHEN I.ESTADO=2 THEN CASE WHEN R.PER IS NOT NULL THEN P3.NOM  + '' '' + P3.APE ELSE PROVE.DEN END ELSE CASE WHEN IE.PER IS NULL THEN PER.NOM  +'' '' + PER.APE ELSE P2.NOM  + '' '' + P2.APE END END NOM_USU, CASE WHEN I.ESTADO=2 THEN CASE WHEN R.PER IS NOT NULL THEN D3.DEN ELSE NULL END ELSE CASE WHEN IE.PER IS NULL " & vbCrLf
sConsulta = sConsulta & "THEN DEP.DEN ELSE D2.DEN END END DEPARTAMENTO,  S.WORKFLOW, CASE WHEN M.PARALELO>1 THEN NULL ELSE IB.BLOQUE END ID_ETAPA_ACTUAL  ,CASE WHEN M.PARALELO>1 THEN NULL ELSE ISNULL(CB.DEN,'' '') END DEN_ETAPA_ACTUAL, I.EN_PROCESO " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA I WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S  WITH (NOLOCK) ON I.SOLICITUD=S.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VERSION_INSTANCIA VI  WITH (NOLOCK) ON I.ID=VI.INSTANCIA AND NUM_VERSION=(SELECT MAX(NUM_VERSION) FROM VERSION_INSTANCIA VI WHERE INSTANCIA=I.ID) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER WITH (NOLOCK)  ON I.PETICIONARIO=PER.COD INNER JOIN DEP ON PER.DEP=DEP.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN INSTANCIA_VISORES_PM C1  WITH (NOLOCK) ON VI.INSTANCIA=C1.INSTANCIA " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN INSTANCIA_EST IE  WITH (NOLOCK) ON I.INSTANCIA_EST=IE.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER P2  WITH (NOLOCK) ON IE.PER=P2.COD LEFT JOIN DEP D2 ON P2.DEP=D2.COD " & vbCrLf
sConsulta = sConsulta & "INNER JOIN " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   SELECT INSTANCIA, COUNT(*) PARALELO,MIN(BLOQUE) BLOQUE " & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE ESTADO=1 AND BLOQ<>1 GROUP BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   ) M ON I.ID=M.INSTANCIA  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB ON M.INSTANCIA= IB.INSTANCIA AND M.BLOQUE=IB.BLOQUE AND IB.ESTADO=1 AND IB.BLOQ<>1  " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   SELECT INSTANCIA_BLOQUE_DESTINO,MAX(FECHA) FECHA " & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_CAMINO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   GROUP BY INSTANCIA_BLOQUE_DESTINO" & vbCrLf
sConsulta = sConsulta & "   ) MR ON IB.ID=MR.INSTANCIA_BLOQUE_DESTINO " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   SELECT BLOQUE,MAX(ROL) ROL " & vbCrLf
sConsulta = sConsulta & "   FROM PM_COPIA_ROL_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   GROUP BY BLOQUE" & vbCrLf
sConsulta = sConsulta & "   ) ROL_BLOQUE ON IB.BLOQUE=ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON ROL_BLOQUE.ROL=R.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN CB WITH (NOLOCK)  ON IB.BLOQUE=CB.BLOQUE AND IDI=@IDI " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER P3 WITH (NOLOCK)  ON R.PER=P3.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEP D3 WITH (NOLOCK)  ON P3.DEP=D3.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE WITH (NOLOCK)  ON R.PROVE=PROVE.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PARTICIPANTE=1 " & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "   SET @SFROM=@SFROM + ' INNER JOIN INSTANCIA_EST  ON I.ID=INSTANCIA_EST.INSTANCIA AND INSTANCIA_EST.PER=@USU AND INSTANCIA_EST.BLOQUE IS NOT NULL'" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE=' WHERE I.PETICIONARIO<> @USU'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE=' WHERE I.PETICIONARIO= @USU'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO <> 0 " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE +  ' AND S.ID= @TIPO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID <> 0 " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE +  ' AND I.ID= @ID'" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "IF @FECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + '  AND I.FEC_ALTA>= CONVERT(datetime,''' + @FECHA + ''',@TIPOFECHA)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE +  ' AND I.ESTADO IN (' + @ESTADO + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE +  ' AND I.ESTADO<>301'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SFROM + @SWHERE + ' AND I.NUM IS NOT NULL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO = '0' OR @ESTADO IS NULL" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL +  ' UNION '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT S.ID ID_SOLIC,S.COD, S.DEN_SPA  DESCR, I.ID ID_INS " & vbCrLf
sConsulta = sConsulta & "   , I.FEC_ALTA  FECHA, I.ESTADO, NULL PER, NULL PROVE,NULL NOM_USU, NULL DEPARTAMENTO, S.WORKFLOW, NULL ID_ETAPA_ACTUAL, NULL DEN_ETAPA_ACTUAL, I.EN_PROCESO " & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA I WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH (NOLOCK)  ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NULL  AND I.EN_PROCESO = 1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ID <> 0 " & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND I.ID =@ID '" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' ORDER BY ID_INS  DESC  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID INT, @TIPO INT,@IDI VARCHAR(20),@TIPOFECHA INT', @USU = @USU,@ID=@ID,@TIPO=@TIPO,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SAVE_INSTANCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SAVE_INSTANCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_SAVE_INSTANCE @INSTANCIA INT, @INSTANCIABLOQUE INT=NULL, @PER_MODIF VARCHAR(50) = NULL, @PROVE VARCHAR(50)=NULL,@CERTIFICADO INT =NULL, @NOCONFORMIDAD INT=NULL,@USUNOM NVARCHAR(500)=NULL , @IMPORTE FLOAT =NULL, @ERRORVAR INT=0 OUTPUT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM_VERSION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OLD_VERSION AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHA DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @BLOQUE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ROL INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Versi�n actual:" & vbCrLf
sConsulta = sConsulta & "--SELECT @OLD_VERSION = MAX(NUM_VERSION) FROM VERSION_INSTANCIA WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "SET @OLD_VERSION = (SELECT TOP 1 NUM_VERSION FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL T ON T.CAMPO = CC.ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Versi�n nueva:" & vbCrLf
sConsulta = sConsulta & "SET @NUM_VERSION = (SELECT ISNULL(MAX(NUM_VERSION),0) + 1 FROM VERSION_INSTANCIA WITH (NOLOCK) WHERE INSTANCIA = @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Sentido integraci�n PM" & vbCrLf
sConsulta = sConsulta & "SET @SENTIDO = (SELECT SENTIDO FROM TABLAS_INTEGRACION WITH (NOLOCK)  WHERE ID=18 AND ACTIVA = 1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECHA = GETDATE()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO VERSION_INSTANCIA(INSTANCIA, NUM_VERSION, PROVE, PER_MODIF, FECHA_MODIF, TIPO, CERTIFICADO,USUPORT_NOM)" & vbCrLf
sConsulta = sConsulta & "  VALUES (@INSTANCIA, @NUM_VERSION, @PROVE, @PER_MODIF,@FECHA , CASE WHEN (@CERTIFICADO IS NULL OR @CERTIFICADO=0) AND (@NOCONFORMIDAD IS NULL OR @NOCONFORMIDAD=0) THEN 0 ELSE CASE WHEN @PROVE IS NULL THEN 2 ELSE 1 END END, @CERTIFICADO,@USUNOM)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IMPORTE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  UPDATE INSTANCIA  SET IMPORTE = @IMPORTE  WHERE ID = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADADA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTINATARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTINATARIO_PROV  AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*SELECT @TRASLADADA=TRASLADADA, @DESTINATARIO=CASE WHEN DESTINATARIO IS NULL THEN PER ELSE DESTINATARIO END , @DESTINATARIO_PROV =DESTINATARIO_PROV " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN INSTANCIA_EST WITH (NOLOCK)  ON INSTANCIA.INSTANCIA_EST=INSTANCIA_EST.ID AND DESTINATARIO IS NOT NULL  " & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA.ID=@INSTANCIA*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @TRASLADADA=IB.TRASLADADA, @DESTINATARIO=CASE WHEN IE.DESTINATARIO IS NULL THEN PER ELSE IE.DESTINATARIO END, @DESTINATARIO_PROV =IE.DESTINATARIO_PROV " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   SELECT TOP 1 * FROM " & vbCrLf
sConsulta = sConsulta & "   INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE INSTANCIA=@INSTANCIA AND BLOQUE = @INSTANCIABLOQUE" & vbCrLf
sConsulta = sConsulta & "   ORDER BY ID DESC" & vbCrLf
sConsulta = sConsulta & "   ) IE ON IE.BLOQUE = IB.ID" & vbCrLf
sConsulta = sConsulta & "WHERE IB.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PER_MODIF IS NOT NULL AND @TRASLADADA=1 AND @PER_MODIF= @DESTINATARIO --Es el usuario al que se le ha trasladado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT TOP 1  @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_BLOQUE IB  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE WITH (NOLOCK) ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE  " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL WITH (NOLOCK)  ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) ON IB.INSTANCIA=IE.INSTANCIA AND IE.DESTINATARIO=@PER_MODIF AND IE.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @PROVE IS NOT NULL AND @TRASLADADA=1 AND @PROVE= @DESTINATARIO_PROV --Es el proveedor al que se le ha trasladado" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT TOP 1 @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "       FROM INSTANCIA_BLOQUE IB  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_BLOQUE  WITH (NOLOCK) ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL  WITH (NOLOCK) ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)  ON IB.INSTANCIA=IE.INSTANCIA AND IE.DESTINATARIO_PROV=@PROVE AND PM_COPIA_ROL.ID=IE.ROL" & vbCrLf
sConsulta = sConsulta & "       WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1" & vbCrLf
sConsulta = sConsulta & "   END    " & vbCrLf
sConsulta = sConsulta & "   ELSE  --No est� en traslado:" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PER_MODIF IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SELECT @BLOQUE=IB.BLOQUE, @ROL=R.ID " & vbCrLf
sConsulta = sConsulta & "           FROM INSTANCIA_BLOQUE IB  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID " & vbCrLf
sConsulta = sConsulta & "           WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND R.PER=@PER_MODIF" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SELECT @BLOQUE=IB.BLOQUE, @ROL=R.ID " & vbCrLf
sConsulta = sConsulta & "           FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID " & vbCrLf
sConsulta = sConsulta & "           WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND R.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ROL = 0 OR @BLOQUE = 0 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ERRORVAR = 1" & vbCrLf
sConsulta = sConsulta & "   RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Crea tablas temporales para tener la relaci�n de los campos almacenados en la temporal con el id nuevo:" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TEMPORAL_CAMPOS (CAMPO_OLD INT, CAMPO_NEW INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TEMPORAL_CAMPO_ADJUN (ADJUN_OLD INT, ADJUN_NEW INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TEMP_CAMPO_ADJUN (ADJUN_OLD INT, ADJUN_NEW INT, LINEA INT)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_  " & vbCrLf
sConsulta = sConsulta & "--Inserta en copia_campo con los valores de la �ltima versi�n:" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CAMPO (INSTANCIA, NUM_VERSION, SUCIO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, COPIA_CAMPO_DEF)" & vbCrLf
sConsulta = sConsulta & "SELECT @INSTANCIA, @NUM_VERSION, 0, C.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & ",/* CASE WHEN CCD.TIPO_CAMPO_GS <> 119 OR CCD.TIPO_CAMPO_GS IS NULL THEN C.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "  ELSE CASE WHEN CCD.ANYADIR_ART =0 and CCB.VISIBLE = 0 AND NOT EXISTS(select ART4.COD " & vbCrLf
sConsulta = sConsulta & "   from  #TEMPORAL TMAT WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN dic GMN1 WITH (NOLOCK)  on gmn1.id = 80" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN dic GMN2 WITH (NOLOCK)  on GMN2.id = 90" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN dic GMN3 WITH (NOLOCK)  on GMN3.id = 100" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN dic GMN4 WITH (NOLOCK)  on GMN4.id = 110" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO CC WITH (NOLOCK)  ON CC.ID=TMAT.CAMPO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO_DEF  CCD2 WITH (NOLOCK)  ON CCD2.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 WITH (NOLOCK)  ON CCB2.CAMPO=CC.COPIA_CAMPO_DEF AND CCB2.ROL=@ROL AND CCB2.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ART4 WITH (NOLOCK)  ON ART4.GMN1=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                          AND ART4.GMN2=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD, GMN2.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                          AND ART4.GMN3=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD, GMN3.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                          AND ART4.GMN4=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD+GMN3.LONGITUD, GMN4.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "           WHERE ART4.COD=C.VALOR_TEXT AND CCD2.TIPO_CAMPO_GS=103 AND CCB2.ORDEN = (CCB.ORDEN-1)" & vbCrLf
sConsulta = sConsulta & "                and CC.INSTANCIA= @INSTANCIA  AND CC.NUM_VERSION = @OLD_VERSION ) then" & vbCrLf
sConsulta = sConsulta & "               null                " & vbCrLf
sConsulta = sConsulta & "   ELSE*/ C.VALOR_TEXT   /*END END*/" & vbCrLf
sConsulta = sConsulta & ", C.VALOR_FEC, C.VALOR_BOOL, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF  CCD WITH (NOLOCK)  ON CCD.ID = C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA= @INSTANCIA  AND C.NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "ORDER BY CCD.GRUPO, CCD.ES_SUBCAMPO, CCB.ORDEN" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Crea la temporal con el id de la versi�n anterior y el nuevo:" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #TEMPORAL_CAMPOS (CAMPO_OLD, CAMPO_NEW)" & vbCrLf
sConsulta = sConsulta & "SELECT COLD.ID CAMPO_OLD, CNEW.ID CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO COLD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CNEW  WITH (NOLOCK) ON COLD.COPIA_CAMPO_DEF = CNEW.COPIA_CAMPO_DEF AND COLD.INSTANCIA = CNEW.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "WHERE COLD.INSTANCIA = @INSTANCIA AND COLD.NUM_VERSION = @OLD_VERSION AND CNEW.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------ Ahora actualiza los id de todas las tablas temporales creadas desde el PM/QA: ------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL SET CAMPO = TC.CAMPO_NEW FROM #TEMPORAL T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS TC  WITH (NOLOCK)  ON T.CAMPO = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL_ADJUN SET CAMPO = TC.CAMPO_NEW FROM #TEMPORAL_ADJUN T  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS TC  WITH (NOLOCK) ON T.CAMPO = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL_DESGLOSE SET CAMPO_PADRE = TCP.CAMPO_NEW, CAMPO_HIJO = TCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "FROM #TEMPORAL_DESGLOSE T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS TCP WITH (NOLOCK)  ON T.CAMPO_PADRE = TCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS TCH WITH (NOLOCK)  ON T.CAMPO_HIJO = TCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL_DESGLOSE_ADJUN SET CAMPO_PADRE = TCP.CAMPO_NEW, CAMPO_HIJO = TCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "FROM #TEMPORAL_DESGLOSE_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS TCP WITH (NOLOCK)  ON T.CAMPO_PADRE = TCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS TCH WITH (NOLOCK)  ON T.CAMPO_HIJO = TCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Inserta los datos en todas las tablas (con los mismos valores que en la versi�n anterior) :" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT CA.ADJUN, TC.CAMPO_NEW, CA.NOM, CA.IDIOMA, CA.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_ADJUN CA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK)  ON CA.CAMPO = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO  #TEMPORAL_CAMPO_ADJUN (ADJUN_OLD ,ADJUN_NEW )" & vbCrLf
sConsulta = sConsulta & "SELECT CCA_OLD.ID ,CCA_NEW.ID   " & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_ADJUN CCA_OLD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_ADJUN CCA_NEW WITH (NOLOCK)  ON CCA_OLD.ADJUN = CCA_NEW.ADJUN " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS TC  WITH (NOLOCK) ON CCA_OLD.CAMPO = TC.CAMPO_old AND CCA_NEW.CAMPO = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL_ADJUN SET ADJUN = TC.ADJUN_NEW FROM #TEMPORAL_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPO_ADJUN  TC WITH (NOLOCK)  ON T.ADJUN = TC.ADJUN_OLD" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_DESGLOSE (CAMPO_PADRE, CAMPO_HIJO)  SELECT DISTINCT CCP.CAMPO_NEW, CCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_DESGLOSE CD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS CCP WITH (NOLOCK)  ON CD.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK)  ON CD.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Inserta en COPIA_LINEA_DESGLOSE los campos que hay en la temporal con LINEA la antigua" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT CL.LINEA, CCP.CAMPO_NEW, CCH.CAMPO_NEW, CL.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & ", /*CASE WHEN CCD.TIPO_CAMPO_GS <> 119 OR CCD.TIPO_CAMPO_GS is null then CL.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "  ELSE  CASE WHEN CCD.ANYADIR_ART =0  and ccb.visible = 0 and NOT EXISTS(" & vbCrLf
sConsulta = sConsulta & "   SELECT ART4.COD FROM ART4 WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_LINEA_DESGLOSE TMAT WITH (NOLOCK)   ON TMAT.CAMPO_PADRE = (CL.CAMPO_PADRE)   AND TMAT.LINEA = CL.LINEA " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN1 WITH (NOLOCK)  on GMN1.ID = 80" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN2 WITH (NOLOCK)  on GMN2.ID = 90" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN3 WITH (NOLOCK)  on GMN3.ID = 100" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN4  WITH (NOLOCK) on GMN4.ID = 110" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO CC WITH (NOLOCK)  ON CC.ID = TMAT.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO_DEF CCD2 WITH (NOLOCK)  ON CCD2.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 WITH (NOLOCK)  ON CCB2.CAMPO = cc.copia_campo_def AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "           WHERE CCB2.ORDEN = (CCB.ORDEN - 1)" & vbCrLf
sConsulta = sConsulta & "               and COD = CL.VALOR_TEXT COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "               AND ART4.GMN1=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "               AND ART4.GMN2=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud, GMN2.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "               AND ART4.GMN3=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud+GMN2.longitud, GMN3.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "               AND ART4.GMN4=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud+GMN2.longitud+GMN3.longitud, GMN4.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "               and CCD2.TIPO_CAMPO_GS=103 ) THEN" & vbCrLf
sConsulta = sConsulta & "           NULL            " & vbCrLf
sConsulta = sConsulta & "   ELSE*/ CL.VALOR_TEXT /*END END*/" & vbCrLf
sConsulta = sConsulta & ", CL.VALOR_FEC, CL.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS CCP  WITH (NOLOCK) ON CL.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK)  ON CL.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK)  ON CC.ID = CL.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK)  ON CCD.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON CCB.CAMPO = cc.copia_campo_def AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--los que ya estaban en desgloses" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT CCP.CAMPO_NEW, CCH.CAMPO_NEW, CL.LINEA, CL.ADJUN,CL.NOM, CL.IDIOMA, CL.DATASIZE, CL.PER, CL.COMENT, CL.FECALTA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE_ADJUN CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS CCP  WITH (NOLOCK) ON CL.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK)  ON CL.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Trazabilidad lineas PM con los ERP" & vbCrLf
sConsulta = sConsulta & "IF @SENTIDO = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "----Inserta en COPIA_LINEA  los campos que hay en la temporal con LINEA la antigua" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA (INSTANCIA, NUM_VERSION, CAMPO_PADRE, LINEA, NUM_LINEA_SAP)" & vbCrLf
sConsulta = sConsulta & "   SELECT @INSTANCIA, @NUM_VERSION,  CCP.CAMPO_NEW, CL.LINEA, CL.NUM_LINEA_SAP" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCP  WITH (NOLOCK) ON CL.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM #TEMPORAL_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Guarda los ID de COPIA_LINEA_DESGLOSE_ADJUN para los adjuntos que ya existian" & vbCrLf
sConsulta = sConsulta & "INSERT INTO  #TEMP_CAMPO_ADJUN (ADJUN_OLD ,ADJUN_NEW,LINEA )" & vbCrLf
sConsulta = sConsulta & "SELECT CCA_OLD.ID ,CCA_NEW.ID,CCA_NEW.LINEA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE_ADJUN CCA_OLD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CCA_NEW  WITH (NOLOCK)  ON CCA_OLD.ADJUN = CCA_NEW.ADJUN " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_CAMPOS TC  WITH (NOLOCK)  ON CCA_OLD.CAMPO_HIJO = TC.CAMPO_old AND CCA_NEW.CAMPO_HIJO = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL_DESGLOSE_ADJUN SET ADJUN = TC.ADJUN_NEW " & vbCrLf
sConsulta = sConsulta & "FROM #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMP_CAMPO_ADJUN  TC WITH (NOLOCK)  ON T.ADJUN = TC.ADJUN_OLD AND T.LINEA = TC.LINEA" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------Ahora actualiza las tablas con los valores modificados desde el PM/QA-----------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Actualizamos los campos con los valores del formulario modificables por el usuario --La Actualizacion del campo ARTICULO, (TIPO_CAMPO_GS = 119) tiene que ver con el DOC. DES_PRT_PM_ ID20" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_CAMPO SET VALOR_NUM = T.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & ", VALOR_TEXT =" & vbCrLf
sConsulta = sConsulta & "/*CASE WHEN CD.TIPO_CAMPO_GS <> 119 OR CD.TIPO_CAMPO_GS is NULL THEN  T.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "    ELSE CASE WHEN CD.ANYADIR_ART = 0  and CCB.VISIBLE = 0 AND NOT EXISTS (SELECT 1 FROM #TEMPORAL TMAT" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DIC GMN1 ON GMN1.id = 80" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN DIC GMN2 ON GMN2.id = 90" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN DIC GMN3 ON GMN3.id = 100" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN DIC GMN4 ON GMN4.id = 110" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CC ON CC.ID=TMAT.CAMPO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF  CCD2 ON CCD2.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 ON CCB2.CAMPO=CC.COPIA_CAMPO_DEF AND CCB2.ROL=@ROL AND CCB2.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ART4 ON  ART4.COD=T.VALOR_TEXT COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "             and ART4.GMN1=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "            AND ART4.GMN2=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD, GMN2.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "            AND ART4.GMN3=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD, GMN3.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "            AND ART4.GMN4=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD+GMN3.LONGITUD, GMN4.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "   WHERE CCD2.TIPO_CAMPO_GS=103 AND CCB2.ORDEN = (CCB.ORDEN-1) ) then" & vbCrLf
sConsulta = sConsulta & "       null                " & vbCrLf
sConsulta = sConsulta & "   ELSE*/ T.VALOR_TEXT   /*END END*/" & vbCrLf
sConsulta = sConsulta & ", VALOR_FEC = T.VALOR_FEC, VALOR_BOOL=T.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL T WITH (NOLOCK)  ON C.ID = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND (ESCRITURA=1  OR (ESCRITURA=0 AND CD.FORMULA IS NOT NULL) OR (VISIBLE = 0 AND( CD.TIPO_CAMPO_GS=119 or  CD.TIPO_CAMPO_GS=103  or  CD.TIPO_CAMPO_GS=105))  OR (ESCRITURA=0 AND CD.TIPO_CAMPO_GS=118))" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Actualizamos a sucio los campos de versiones anteriores que han cambiado con la nueva versi�n:" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_CAMPO SET SUCIO = 1 FROM COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN (SELECT * FROM COPIA_CAMPO WITH (NOLOCK)  WHERE INSTANCIA = @INSTANCIA AND NUM_VERSION= @NUM_VERSION) CNEW" & vbCrLf
sConsulta = sConsulta & "ON C.COPIA_CAMPO_DEF = CNEW.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "AND (C.VALOR_NUM<>CNEW.VALOR_NUM OR C.VALOR_FEC <> CNEW.VALOR_FEC OR C.VALOR_TEXT <>CNEW.VALOR_TEXT OR C.VALOR_BOOL <> CNEW.VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Eliminamos los adjuntos de los campos tipo form que ya no est�n en esta nueva versi�n" & vbCrLf
sConsulta = sConsulta & "--Solo eliminamos los que no estan en #temporal_adjun y ademas son visibles y modificables" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN FROM COPIA_CAMPO_ADJUN CCA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL T WITH (NOLOCK)  ON CCA.CAMPO = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO C  WITH (NOLOCK) on CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND ESCRITURA=1  AND  VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_ADJUN TA WITH (NOLOCK)  WHERE CCA.ID = TA.ADJUN AND TA.TIPO = 1 and ta.portal=0 ) and not exists (SELECT * FROM #TEMPORAL_ADJUN TA WITH (NOLOCK) WHERE CCA.ADJUN = TA.ADJUN AND TA.TIPO = 1 and ta.portal=1 ) " & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Insertamos los adjuntos de los campos tipo form (Insertados desde el PM)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT CA.ID, T.CAMPO, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_ADJUN CA  WITH (NOLOCK) INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK)  ON CA.ID = T.ADJUN " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO C WITH (NOLOCK)  ON T.CAMPO=C.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND ESCRITURA=1" & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 2 AND T.PORTAL = 0" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Insertamos los adjuntos metidos en el portal por el proveedor" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, COMENT, FECALTA, PROVE)" & vbCrLf
sConsulta = sConsulta & "SELECT CA.ID, T.CAMPO, CA.NOM, 'SPA', CCA.DATASIZE, CA.COMENT, CA.FECALTA, CA.PROVE" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN (P_COPIA_CAMPO_ADJUN CCA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & " inner join p_copia_campo c  WITH (NOLOCK) on cca.campo = c.id and c.instancia = @instancia" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK)  ON CCA.ADJUN_PORTAL = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO   WITH (NOLOCK) ON T.CAMPO=COPIA_CAMPO.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB  WITH (NOLOCK) ON COPIA_CAMPO.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND ESCRITURA=1" & vbCrLf
sConsulta = sConsulta & ") ON CA.ADJUN_ORIGEN = CCA.id  AND CA.ADJUN_PORTAL = CCA.ADJUN_PORTAL" & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 2 AND T.PORTAL = 1 and ca.portal = 1 and ca.prove=cca.prove" & vbCrLf
sConsulta = sConsulta & "--Ojo el id que da el portal para los adjuntos no tiene en cuenta el id que da el PM -> Portal dice 143 PM dice 143 pero uno es 'Archivo.txt' y otro es 'Otro Archivo.txt' -> controla el prove" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- ELIMINAMOS LOS ADJUNTOS QUE HAYAN SIDO ELIMINADOS" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN " & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK)  ON CLDA.CAMPO_PADRE = TD.CAMPO_PADRE AND CLDA.CAMPO_HIJO = TD.CAMPO_HIJO AND CLDA.LINEA = TD.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "inner join copia_campo cc WITH (NOLOCK)  on cc.id = clda.campo_hijo" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB  WITH (NOLOCK) ON CCB.CAMPO=cc.copia_campo_def AND ROL=@ROL AND BLOQUE=@BLOQUE  AND ESCRITURA=1  AND  VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT  * FROM #TEMPORAL_DESGLOSE_ADJUN TDA  WITH (NOLOCK) WHERE CLDA.ID = TDA.ADJUN AND TDA.TIPO = 1  AND TDA.PORTAL = 0  AND TDA.LINEA = CLDA.LINEA) " & vbCrLf
sConsulta = sConsulta & "AND NOT EXISTS (SELECT  * FROM #TEMPORAL_DESGLOSE_ADJUN TDA  WITH (NOLOCK) WHERE CLDA.ADJUN = TDA.ADJUN AND TDA.TIPO = 1  AND TDA.PORTAL = 1)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN " & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK)  ON CLDA.CAMPO_PADRE = TD.CAMPO_PADRE AND CLDA.CAMPO_HIJO = TD.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_DESGLOSE TD2  WITH (NOLOCK) WHERE CLDA.CAMPO_PADRE=TD2.CAMPO_PADRE AND CLDA.CAMPO_HIJO = TD2.CAMPO_HIJO AND CLDA.LINEA = TD2.LINEA_OLD)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Elimina los campos de las lineas que no est�n en la temporal por el numero de linea que ten�an= lineas eliminadas" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK)  ON CLD.CAMPO_PADRE = TD.CAMPO_PADRE AND CLD.CAMPO_HIJO = TD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_DESGLOSE TD2 WITH (NOLOCK)  WHERE CLD.CAMPO_PADRE=TD2.CAMPO_PADRE AND CLD.CAMPO_HIJO = TD2.CAMPO_HIJO AND CLD.LINEA = TD2.LINEA_OLD)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Trazabilidad l�neas PM con los ERP" & vbCrLf
sConsulta = sConsulta & "IF @SENTIDO = 1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Elimina de COPIA_LINEA las l�neas que no est�n en la nueva versi�n" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM COPIA_LINEA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK)  ON CL.CAMPO_PADRE = TD.CAMPO_PADRE " & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_DESGLOSE TD2 WITH (NOLOCK)  WHERE CL.CAMPO_PADRE=TD2.CAMPO_PADRE AND CL.LINEA = TD2.LINEA_OLD)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COPIA_LINEA_DESGLOSE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COPIA_LINEA_DESGLOSE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--La Actualizacion del campo ARTICULO, (TIPO_CAMPO_GS = 119) tiene que ver con el DOC. DES_PRT_PM_ ID20 (Cuando unicamente este visible el Material)" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "SET VALOR_TEXT = " & vbCrLf
sConsulta = sConsulta & "/*CASE WHEN CD.TIPO_CAMPO_GS <> 119 OR CD.TIPO_CAMPO_GS is NULL THEN  T.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "   ELSE CASE WHEN CD.ANYADIR_ART = 0  and ccb.visible = 0 AND NOT EXISTS (SELECT ART4.COD FROM ART4  WITH (NOLOCK)            " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN  #TEMPORAL_DESGLOSE TMAT  WITH (NOLOCK)  ON TMAT.CAMPO_PADRE = (T.CAMPO_PADRE) AND TMAT.LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN1 WITH (NOLOCK)  ON GMN1.id = 80" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN2 WITH (NOLOCK)  ON GMN2.id = 90" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN3 WITH (NOLOCK)  ON GMN3.id = 100" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN4 WITH (NOLOCK)  ON GMN4.id = 110" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO CC WITH (NOLOCK)  ON CC.ID = TMAT.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO_DEF CcD2 WITH (NOLOCK)  ON CC.COPIA_CAMPO_DEF=CcD2.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 WITH (NOLOCK)  ON CCB2.CAMPO = CC.COPIA_CAMPO_DEF AND CCB2.ROL=@ROL AND CCB2.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "           WHERE  CCB2.ORDEN = (CCB.ORDEN - 1)" & vbCrLf
sConsulta = sConsulta & "                   and COD = T.VALOR_TEXT COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.GMN1=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.GMN2=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD, GMN2.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                   AND ART4.GMN3=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD, GMN3.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                   AND ART4.GMN4=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD+GMN3.LONGITUD, GMN4.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                   and CCD2.TIPO_CAMPO_GS= 103) THEN   NULL" & vbCrLf
sConsulta = sConsulta & "ELSE*/  T.VALOR_TEXT  /*END  END*/" & vbCrLf
sConsulta = sConsulta & ", VALOR_NUM = T.VALOR_NUM,VALOR_FEC = T.VALOR_FEC,VALOR_BOOL = T.VALOR_BOOL,LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_DESGLOSE T  WITH (NOLOCK) ON CLD.CAMPO_PADRE = T.CAMPO_PADRE AND CLD.CAMPO_HIJO = T.CAMPO_HIJO AND CLD.LINEA = T.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO C WITH (NOLOCK)  ON T.CAMPO_HIJO=C.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND (ESCRITURA=1 OR (ESCRITURA=0 AND FORMULA IS NOT NULL)  OR (VISIBLE = 0 AND( CD.TIPO_CAMPO_GS=119 or  CD.TIPO_CAMPO_GS=103  or  CD.TIPO_CAMPO_GS=105))  OR (ESCRITURA=0 AND CD.TIPO_CAMPO_GS=118))" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_LINEA_DESGLOSE_ADJUN SET LINEA = TDA.LINEA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE_ADJUN CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL_DESGLOSE_ADJUN TDA WITH (NOLOCK)  ON CLD.CAMPO_PADRE = TDA.CAMPO_PADRE AND CLD.CAMPO_HIJO = TDA.CAMPO_HIJO AND ((CLD.ADJUN = TDA.ADJUN) OR (CLD.ID = TDA.ADJUN))" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COPIA_LINEA_DESGLOSE CHECK CONSTRAINT ALL ALTER TABLE COPIA_LINEA_DESGLOSE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Trazabilidad l�neas PM con los ERP" & vbCrLf
sConsulta = sConsulta & "IF @SENTIDO = 1" & vbCrLf
sConsulta = sConsulta & "--Integraci�n con ERP: Trazabilidad l�neas solicitudes " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE COPIA_LINEA NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE COPIA_LINEA SET LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA CL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE T WITH (NOLOCK)  ON CL.CAMPO_PADRE = T.CAMPO_PADRE AND CL.LINEA = T.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE COPIA_LINEA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT T.LINEA, T.CAMPO_PADRE, T.CAMPO_HIJO, T.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & ",/*CASE WHEN CD.TIPO_CAMPO_GS <> 119 OR CD.TIPO_CAMPO_GS is NULL THEN  T.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "  ELSE CASE WHEN CD.ANYADIR_ART = 0  AND CCB.VISIBLE = 0 AND NOT EXISTS (SELECT ART4.COD FROM ART4 WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER  JOIN #TEMPORAL_DESGLOSE TMAT WITH (NOLOCK)   ON TMAT.CAMPO_PADRE = (T.CAMPO_PADRE) AND TMAT.LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN1 WITH (NOLOCK)  ON GMN1.id = 80" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN2 WITH (NOLOCK)  ON GMN2.id = 90" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN3 WITH (NOLOCK)  ON GMN3.id = 100" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN4 WITH (NOLOCK)  ON GMN4.id = 110" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CC.ID = TMAT.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO_DEF CCD2 WITH (NOLOCK)  ON CC.COPIA_CAMPO_DEF=CcD2.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 WITH (NOLOCK)  ON CCB2.CAMPO = CC.COPIA_CAMPO_DEF AND CCB2.ROL=@ROL AND CCB2.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "           WHERE  CCB2.ORDEN = (CCB.ORDEN-1) " & vbCrLf
sConsulta = sConsulta & "               AND COD = T.VALOR_TEXT COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.GMN1=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.GMN2=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud, GMN2.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                   AND ART4.GMN3=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud+GMN2.longitud, GMN3.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                   AND ART4.GMN4=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud+GMN2.longitud+GMN3.longitud, GMN4.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                   AND CCD2.TIPO_CAMPO_GS= 103 ) then" & vbCrLf
sConsulta = sConsulta & "           NULL" & vbCrLf
sConsulta = sConsulta & "   ELSE*/  T.VALOR_TEXT  /*END END */" & vbCrLf
sConsulta = sConsulta & ", T.VALOR_FEC, T.VALOR_BOOL " & vbCrLf
sConsulta = sConsulta & "FROM #TEMPORAL_DESGLOSE T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO C WITH (NOLOCK)  ON T.CAMPO_HIJO=C.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT * FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK)  WHERE T.LINEA=CLD.LINEA AND T.CAMPO_PADRE = CLD.CAMPO_PADRE AND T.CAMPO_HIJO = CLD.CAMPO_HIJO)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT T.CAMPO_PADRE, T.CAMPO_HIJO,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               ON CA.ID = CLDA.ADJUN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMP_CAMPO_ADJUN TC WITH (NOLOCK)  ON TC.ADJUN_NEW = CLDA.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              ON T.ADJUN = TC.ADJUN_OLD" & vbCrLf
sConsulta = sConsulta & "              AND T.CAMPO_PADRE = CLDA.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "              AND T.CAMPO_HIJO = CLDA.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT * FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA2 WITH (NOLOCK)  WHERE T.CAMPO_PADRE = CLDA2.CAMPO_PADRE AND T.CAMPO_HIJO = CLDA2.CAMPO_HIJO AND T.LINEA = CLDA2.LINEA) AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA,ADJUN, NOM, IDIOMA, DATASIZE, COMENT, FECALTA, PROVE)" & vbCrLf
sConsulta = sConsulta & "SELECT T.CAMPO_PADRE, T.CAMPO_HIJO, T.LINEA, CA.ID, CA.NOM, 'SPA', CCA.DATASIZE, CA.COMENT, CA.FECALTA, CA.PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN P_COPIA_LINEA_DESGLOSE_ADJUN CCA WITH (NOLOCK) ON  CA.ADJUN_PORTAL = CCA.ADJUN_PORTAL AND CA.ADJUN_ORIGEN = CCA.id AND CCA.PROVE=CA.PROVE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) ON CCA.ADJUN_PORTAL = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 2 AND T.PORTAL = 1 and ca.portal = 1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT T.CAMPO_PADRE, T.CAMPO_HIJO,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) ON CA.ID = T.ADJUN " & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 2   AND T.PORTAL <> 1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Trazabilidad l�neas PM con los ERP" & vbCrLf
sConsulta = sConsulta & "IF @SENTIDO = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA (INSTANCIA, NUM_VERSION, LINEA, CAMPO_PADRE)" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT @INSTANCIA, @NUM_VERSION, T.LINEA, T.CAMPO_PADRE " & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_DESGLOSE T WITH (NOLOCK)  WHERE NOT EXISTS (SELECT * FROM COPIA_LINEA CL WITH (NOLOCK)  WHERE T.LINEA=CL.LINEA AND T.CAMPO_PADRE = CL.CAMPO_PADRE)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--  Guarda los participantes que haya que poner en esta etapa" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL " & vbCrLf
sConsulta = sConsulta & "SET PER=CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CASE WHEN T.PER='' THEN NULL ELSE T.PER END ELSE U.FSWS_SUSTITUCION END, " & vbCrLf
sConsulta = sConsulta & "    PROVE=CASE WHEN T.PROVE=''  THEN NULL ELSE T.PROVE END," & vbCrLf
sConsulta = sConsulta & "    CON=CASE WHEN T.CON=0  THEN NULL ELSE T.CON END" & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_PARTICIPANTES T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   left join USU U WITH (NOLOCK) on T.PER = U.PER" & vbCrLf
sConsulta = sConsulta & "   inner join PM_COPIA_ROL PCR WITH (NOLOCK) on PCR.ID = T.ROL" & vbCrLf
sConsulta = sConsulta & "   inner join INSTANCIA WITH (NOLOCK)  ON PCR.INSTANCIA=INSTANCIA.ID AND (INSTANCIA.ESTADO=0 or INSTANCIA.ESTADO=2 or INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Por �ltimo comprueba si se est� generando por primera vez la instancia y si es as� inserta en INSTANCIA_BLOQUE el bloque del peticionario:" & vbCrLf
sConsulta = sConsulta & "--IF (SELECT COUNT(*) FROM INSTANCIA_BLOQUE I INNER JOIN PM_COPIA_BLOQUE CB ON I.BLOQUE=CB.ID WHERE I.INSTANCIA=@INSTANCIA AND I.BLOQUE=@BLOQUE AND CB.TIPO=1 AND I.ESTADO=1)=0" & vbCrLf
sConsulta = sConsulta & "--  INSERT INTO INSTANCIA_BLOQUE(INSTANCIA,BLOQUE,ESTADO)  " & vbCrLf
sConsulta = sConsulta & "--     SELECT @INSTANCIA,PM_COPIA_BLOQUE.ID , 1 FROM PM_COPIA_BLOQUE WHERE INSTANCIA=@INSTANCIA AND TIPO=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Actualizar la tabla PM_COPIA_ROL, para establecer roles mediante campos" & vbCrLf
sConsulta = sConsulta & "--Roles de tipo Comprador o usuario" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "SET PER = CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CC.VALOR_TEXT ELSE U.FSWS_SUSTITUCION END" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "left join USU U WITH (NOLOCK) on CC.VALOR_TEXT = U.PER" & vbCrLf
sConsulta = sConsulta & "inner join PM_COPIA_ROL PCR WITH (NOLOCK) on PCR.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA WITH (NOLOCK)  ON PCR.INSTANCIA=INSTANCIA.ID AND (INSTANCIA.ESTADO=0 or INSTANCIA.ESTADO=2 or INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "WHERE (PCR.TIPO = 1 OR PCR.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "   AND PCR.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND PCR.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "   AND PCR.BLOQUE IN (SELECT BLOQUE" & vbCrLf
sConsulta = sConsulta & "                   FROM INSTANCIA_BLOQUE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   WHERE ESTADO = 1" & vbCrLf
sConsulta = sConsulta & "                   AND INSTANCIA = @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "   AND PCR.COMO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "   AND CC.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CC.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Roles de tipo Proveedor" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "SET PROVE = CC.VALOR_TEXT, @PROVE = CC.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "WHERE PM_COPIA_ROL.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "   AND PM_COPIA_ROL.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND PM_COPIA_ROL.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "   AND PM_COPIA_ROL.BLOQUE IN (SELECT BLOQUE" & vbCrLf
sConsulta = sConsulta & "                   FROM INSTANCIA_BLOQUE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   WHERE ESTADO = 1" & vbCrLf
sConsulta = sConsulta & "                   AND INSTANCIA = @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "   AND PM_COPIA_ROL.COMO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "   AND PM_COPIA_ROL.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   AND CC.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CC.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--JGA 10/12/2007 INI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TITULO_ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_CAMPO_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TITULO_TEXT VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TITULO_FEC DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBTIPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FEC_NECESIDAD DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIOMA VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtener el IDIOMA de PER_MODIF" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA = (SELECT TOP 1 IDIOMA FROM USU WITH (NOLOCK)  WHERE PER = @PER_MODIF)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Buscar si alg�n campo de la instancia es de fecha de necesidad" & vbCrLf
sConsulta = sConsulta & "SET @FEC_NECESIDAD = (SELECT TOP 1 VALOR_FEC FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID WHERE TIPO_CAMPO_GS = 5 And INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Diferenciar casos en funci�n de la existencia de t�tulo" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET (SELECT DISTINCT @TITULO_ID = CCD.ID, @TIPO_CAMPO_GS = CCD.TIPO_CAMPO_GS, @VALOR_TEXT = CC.VALOR_TEXT, @SUBTIPO = CCD.SUBTIPO FROM COPIA_CAMPO CC  WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID WHERE TITULO = 1 AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TITULO_ID IS NOT NULL  -- Si existe T�tulo" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO_CAMPO_GS IS NOT NULL -- Si el T�tulo es un campo de tipo GS" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 100 -- Tipo Proveedor" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PR.COD + ' - ' + PR.DEN FROM COPIA_CAMPO CC  WITH (NOLOCK) INNER JOIN PROVE PR  WITH (NOLOCK) ON CC.VALOR_TEXT = PR.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION )" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 102 -- Tipo Moneda" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT M.DEN_SPA FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN MON M WITH (NOLOCK) ON CC.VALOR_TEXT = M.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT M.DEN_ENG FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN MON M WITH (NOLOCK) ON CC.VALOR_TEXT = M.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT M.DEN_GER FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN MON M WITH (NOLOCK) ON CC.VALOR_TEXT = M.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 107 -- Tipo Pais" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PA.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN PAI PA WITH (NOLOCK) ON CC.VALOR_TEXT = PA.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 108 -- Tipo Provincia" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PRO.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN PROVI PRO WITH (NOLOCK) ON CC.VALOR_TEXT = PRO.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 109 -- Tipo Dest" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA='SPA'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT D.DEN_SPA FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEST D WITH (NOLOCK) ON CC.VALOR_TEXT = D.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA='ENG'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT D.DEN_ENG FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEST D WITH (NOLOCK) ON CC.VALOR_TEXT = D.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA='GER'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT D.DEN_GER FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEST D WITH (NOLOCK) ON CC.VALOR_TEXT = D.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 115 -- Tipo Persona" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PE.NOM + ' ' + PE.APE FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN PER PE WITH (NOLOCK) ON CC.VALOR_TEXT = PE.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 122 -- Tipo Departamento" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT DE.COD + ' - ' + DE.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEP DE WITH (NOLOCK) ON CC.VALOR_TEXT = DE.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end      " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 123 -- Tipo Org Compras" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT O.COD + ' - ' + O.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN ORGCOMPRAS O WITH (NOLOCK) ON CC.VALOR_TEXT = O.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 124 -- Tipo Centro" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT CE.COD + ' - ' + CE.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN CENTROS CE WITH (NOLOCK) ON CC.VALOR_TEXT = CE.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 125 -- Tipo Almacen" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT AL.COD + ' - ' + AL.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN ALMACEN AL WITH (NOLOCK) ON CC.VALOR_TEXT = AL.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS IN (5,6,7) -- FecNecesidad, IniSuministro, FinSuministro" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_FEC = (SELECT CC.VALOR_FEC FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 103 -- Tipo Material" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA='SPA'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT =  (SELECT CASE WHEN CC.VALOR_TEXT IS NOT NULL THEN  REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') + ' - ' +  (SELECT DEN_SPA FROM GMN4 WITH (NOLOCK)  WHERE GMN1=  REPLACE   (SUBSTRING(CC.VALOR_TEXT,1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')) ,' ' ,'') AND GMN2=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN2')),' ' ,'') AND GMN3=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN3')),' ','') AND COD=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1, ("
sConsulta = sConsulta & "   SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') ) " & vbCrLf
sConsulta = sConsulta & "   ELSE CC.VALOR_TEXT END FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA='ENG'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT CASE WHEN CC.VALOR_TEXT IS NOT NULL THEN  REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') + ' - ' +  (SELECT DEN_ENG FROM GMN4 WITH (NOLOCK)  WHERE GMN1=  REPLACE   (SUBSTRING(CC.VALOR_TEXT,1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')) ,' ' ,'') AND GMN2=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN2')),' ' ,'') AND GMN3=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN3')),' ','') AND COD=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1, ("
sConsulta = sConsulta & "   SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') ) " & vbCrLf
sConsulta = sConsulta & "   ELSE CC.VALOR_TEXT END FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA='GER'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT CASE WHEN CC.VALOR_TEXT IS NOT NULL THEN  REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') + ' - ' +  (SELECT DEN_GER FROM GMN4 WITH (NOLOCK)  WHERE GMN1=  REPLACE   (SUBSTRING(CC.VALOR_TEXT,1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')) ,' ' ,'') AND GMN2=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')+1,(SELECT LONGITUD FROM DIC  WITH (NOLOCK) WHERE NOMBRE= 'GMN2')),' ' ,'') AND GMN3=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN3')),' ','') AND COD=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1, ("
sConsulta = sConsulta & "   SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') ) " & vbCrLf
sConsulta = sConsulta & "   ELSE CC.VALOR_TEXT END FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "      ELSE  -- En Cualquier otro caso" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "      end end end end end end end end end end end end" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   ELSE  -- TIPO_CAMPO_GS es NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      IF @SUBTIPO = 3 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_FEC = (SELECT CC.VALOR_FEC FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)     " & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = @VALOR_TEXT           " & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "ELSE  -- Sin Campo T�tulo   " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT TOP 1 VALOR_TEXT FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID WHERE TIPO_CAMPO_GS = 1 And INSTANCIA = @INSTANCIA AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   IF @TITULO_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "     SET @TITULO_TEXT = (SELECT DEN_SPA FROM SOLICITUD S WITH (NOLOCK)  INNER JOIN INSTANCIA I WITH (NOLOCK)  ON S.ID = I.SOLICITUD WHERE I.ID = @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Actualizaci�n del registro" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_VISORES_PM SET TITULO_TEXT = @TITULO_TEXT, TITULO_FEC = @TITULO_FEC, FEC_NECESIDAD = @FEC_NECESIDAD WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--JGA 10/12/2007 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
sConsulta = sConsulta & "SET @ERRORVAR = @@ERROR" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SAVE_NEW_INSTANCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SAVE_NEW_INSTANCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_SAVE_NEW_INSTANCE @ID INT, @SOLICITUD INT, @FORMULARIO INT, @WORKFLOW INT, @IMPORTE_PEDIDO_DIRECTO FLOAT,@PETICIONARIO VARCHAR(50), @COMPRADOR VARCHAR(50) = NULL, @MON VARCHAR(50), @GUARDAR TINYINT =0, @IMPORTE FLOAT =NULL, @PEDIDO_AUT TINYINT=0, @ESTADO INT=0 OUTPUT,@COMENT_ALTA NVARCHAR(500)=NULL, @ACCION INT=0 OUTPUT, @ERRORVAR INT=0 OUTPUT  AS" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHA AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM_VERSION AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_SOLICIT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @BLOQUE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS2 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS3 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS4 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PUB TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CERTIF INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_NEW INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OLD_VERSION AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @OLD_VERSION = (SELECT TOP 1 NUM_VERSION FROM COPIA_CAMPO CC" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #TEMPORAL T ON CCD.CAMPO_ORIGEN = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "set @OLD_VERSION = isnull(@OLD_VERSION,0)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Versi�n nueva:" & vbCrLf
sConsulta = sConsulta & "SET @NUM_VERSION = (SELECT ISNULL(MAX(NUM_VERSION),0) + 1 FROM VERSION_INSTANCIA WHERE INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "IF @OLD_VERSION = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --ESTE ES EL CASO DE QUE DESDE ALTA HAYAN PULSADO EN GUARDAR POR 1� VEZ" & vbCrLf
sConsulta = sConsulta & "   --GUARDAMOS LA ESTRUCTURA DEL WORKFLOW" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS=N''" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS2=N''" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS3=N''" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS4=N''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS = @IDIS + N' A.DEN_ENG, A.DEN_GER, A.DEN_SPA, '" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS2 = @IDIS2 + N' A.AYUDA_ENG, A.AYUDA_GER, A.AYUDA_SPA, '" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS3 = @IDIS3 + N' CVL.VALOR_TEXT_ENG, CVL.VALOR_TEXT_GER, CVL.VALOR_TEXT_SPA, '" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS4 = @IDIS4 + N' VALOR_TEXT_ENG, VALOR_TEXT_GER, VALOR_TEXT_SPA, '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'INSERT INTO COPIA_GRUPO (INSTANCIA, ' + @IDIS + ' ORDEN, GRUPO_ORIGEN) SELECT @ID,' + @IDIS + ' ORDEN, A.ID FROM FORM_GRUPO A  WITH (NOLOCK) WHERE FORMULARIO = @FORMULARIO'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @FORMULARIO INT', @ID=@ID, @FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'INSERT INTO COPIA_CAMPO_DEF (GRUPO, ' + @IDIS + ' ' + @IDIS2 + ' TIPO, SUBTIPO, INTRO, MINNUM, MAXNUM, MINFEC, MAXFEC, ORDEN, ES_SUBCAMPO,TIPO_CAMPO_GS, ID_ATRIB_GS, GMN1, GMN2, GMN3, GMN4, LINEAS_PRECONF, ID_CALCULO, FORMULA, ORDEN_CALCULO, IMPORTE_WORKFL, ORIGEN_CALC_DESGLOSE,CAMPO_ORIGEN,POPUP,ANYADIR_ART,TITULO,FECHA_VALOR_DEFECTO,IDREFSOLICITUD,AVISOBLOQUEOIMPACUM,CARGAR_ULT_ADJ) " & vbCrLf
sConsulta = sConsulta & "                SELECT CG.ID, ' + @IDIS + ' ' + @IDIS2 + ' A.TIPO, A.SUBTIPO, A.INTRO, A.MINNUM, A.MAXNUM, A.MINFEC, A.MAXFEC, A.ORDEN, A.ES_SUBCAMPO, A.TIPO_CAMPO_GS, A.ID_ATRIB_GS, A.GMN1, A.GMN2, A.GMN3, A.GMN4, A.LINEAS_PRECONF, A.ID_CALCULO, A.FORMULA, A.ORDEN_CALCULO, A.IMPORTE_WORKFL, A.ORIGEN_CALC_DESGLOSE, A.ID , A.POPUP,A.ANYADIR_ART, A.TITULO, A.FECHA_VALOR_DEFECTO,A.IDREFSOLICITUD,A.AVISOBLOQUEOIMPACUM,A.CARGAR_ULT_ADJ" & vbCrLf
sConsulta = sConsulta & "                  FROM FORM_CAMPO A  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN FORM_GRUPO FG  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN COPIA_GRUPO CG  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                 ON FG.ID = CG.GRUPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "                 WHERE FG.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "                   AND CG.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      ORDER BY CG.ID, A.ES_SUBCAMPO, A.ORDEN'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @FORMULARIO INT', @ID=@ID, @FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'INSERT INTO COPIA_CAMPO_VALOR_LISTA (CAMPO_DEF, ORDEN, VALOR_NUM, ' + @IDIS4 + ' VALOR_FEC) " & vbCrLf
sConsulta = sConsulta & "                SELECT CCD.ID, CVL.ORDEN, CVL.VALOR_NUM, ' + @IDIS3 + ' CVL.VALOR_FEC" & vbCrLf
sConsulta = sConsulta & "                  FROM COPIA_CAMPO_DEF CCD WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN FORM_CAMPO A WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                 ON CCD.CAMPO_ORIGEN = A.ID" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN CAMPO_VALOR_LISTA CVL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ID = CVL.CAMPO" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN FORM_GRUPO FG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON A.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON FG.ID = CG.GRUPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "                                AND CCD.GRUPO = CG.ID" & vbCrLf
sConsulta = sConsulta & "                 WHERE FG.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "                   AND CG.INSTANCIA = @ID'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @FORMULARIO INT', @ID=@ID, @FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE COPIA_CAMPO_DEF SET ORIGEN_CALC_DESGLOSE = CCD2.ID" & vbCrLf
sConsulta = sConsulta & "    FROM COPIA_CAMPO_DEF CCD  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN (SELECT CCD.* FROM COPIA_CAMPO_DEF CCD WITH (NOLOCK) INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) ON CG.ID = CCD.GRUPO WHERE CG.INSTANCIA = @ID) CCD2" & vbCrLf
sConsulta = sConsulta & "                 ON CCD.ORIGEN_CALC_DESGLOSE = CCD2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_GRUPO CG WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 ON CCD.GRUPO = CG.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE CG.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   ------------------------------------------------ Aqu� todas las tablas para el nuevo workflow: ------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_BLOQUE (INSTANCIA,TIPO,[TOP],[LEFT],WIDTH,HEIGHT,BLOQUE_ORIGEN) " & vbCrLf
sConsulta = sConsulta & "     SELECT @ID,TIPO,[TOP],[LEFT],WIDTH,HEIGHT,ID FROM PM_BLOQUE WITH (NOLOCK) WHERE WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_BLOQUE_DEN (BLOQUE,IDI,DEN)" & vbCrLf
sConsulta = sConsulta & "     SELECT B.ID,PM_BLOQUE_DEN.IDI, PM_BLOQUE_DEN.DEN FROM PM_BLOQUE_DEN  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK) ON PM_BLOQUE_DEN.BLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      WHERE  B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ACCIONES (BLOQUE,CUMP_OBL_ROL,GUARDA,TIPO_RECHAZO,TIPO,ACCION_ORIGEN,SOLO_RECHAZADAS,INCREMENTAR_ID)" & vbCrLf
sConsulta = sConsulta & "     SELECT B.ID, PM_ACCIONES.CUMP_OBL_ROL,PM_ACCIONES.GUARDA,PM_ACCIONES.TIPO_RECHAZO,PM_ACCIONES.TIPO,PM_ACCIONES.ID,SOLO_RECHAZADAS,PM_ACCIONES.INCREMENTAR_ID" & vbCrLf
sConsulta = sConsulta & "     FROM PM_ACCIONES   WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          ON PM_ACCIONES.BLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "     WHERE  B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ACCIONES_DEN (ACCION,IDI,DEN)" & vbCrLf
sConsulta = sConsulta & "     SELECT A.ID,PM_ACCIONES_DEN.IDI, PM_ACCIONES_DEN.DEN " & vbCrLf
sConsulta = sConsulta & "     FROM PM_ACCIONES_DEN  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ACCIONES A WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          ON PM_ACCIONES_DEN.ACCION=A.ACCION_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          ON A.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "     WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ROL(INSTANCIA,DEN,TIPO,CUANDO_ASIGNAR,BLOQUE,COMO_ASIGNAR,RESTRINGIR,CAMPO,ROL,PER,PROVE,CONF_BLOQUE_DEF,UON1,UON2,UON3,DEP,EQP,CON,ROL_ORIGEN,PER_ORIGINAL,VER_FLUJO)" & vbCrLf
sConsulta = sConsulta & "     SELECT @ID, P.DEN,P.TIPO,P.CUANDO_ASIGNAR,CB1.ID,P.COMO_ASIGNAR,P.RESTRINGIR, CAMPO_DEF.ID,P.ROL_ASIGNA,CASE WHEN USU.FSWS_SUSTITUCION IS NOT NULL THEN USU.FSWS_SUSTITUCION ELSE P.PER END,P.PROVE,CB2.ID,P.UON1,P.UON2,P.UON3,P.DEP,P.EQP,P.CON,P.ID,P.PER,P.VER_FLUJO " & vbCrLf
sConsulta = sConsulta & "     FROM PM_ROL P  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PM_COPIA_BLOQUE CB1  WITH (NOLOCK) ON P.BLOQUE_ASIGNA=CB1.BLOQUE_ORIGEN AND CB1.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PM_COPIA_BLOQUE CB2  WITH (NOLOCK) ON P.CONF_BLOQUE_DEF=CB2.BLOQUE_ORIGEN  AND CB2.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN USU  WITH (NOLOCK)                                   ON P.PER=USU.PER" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN  FROM COPIA_CAMPO_DEF  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_GRUPO  WITH (NOLOCK)   ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "          ON P.CAMPO=CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "     WHERE WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM PM_COPIA_ROL  WITH (NOLOCK) WHERE ROL IS NOT NULL and instancia=@id) >0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @ROL_ORIGEN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "       DECLARE @ROL AS INTEGER" & vbCrLf
sConsulta = sConsulta & "       DECLARE C1 CURSOR FOR SELECT ID,ROL_ORIGEN FROM PM_COPIA_ROL WHERE INSTANCIA=@ID AND ROL_ORIGEN IS NOT NULL AND ROL_ORIGEN IN (SELECT ROL FROM PM_COPIA_ROL WHERE INSTANCIA=@ID AND ROL IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "       OPEN C1" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM C1 INTO @ROL,@ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "            UPDATE PM_COPIA_ROL SET ROL = @ROL WHERE INSTANCIA = @ID AND ROL=@ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "            IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C1 INTO @ROL,@ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "       CLOSE C1" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   -- Actualizo los Roles Peticionario, para que en el Campo PER tenga el codigo del la persona Peticionaria, " & vbCrLf
sConsulta = sConsulta & "   -- s�lo actualizo si no existe un rol peticionbario que tenga la persona peticionaria" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM PM_COPIA_ROL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PER P WITH (NOLOCK) ON ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') AND " & vbCrLf
sConsulta = sConsulta & "              ISNULL(P.UON3,'') = ISNULL(PM_COPIA_ROL.UON3,'') AND P.DEP=PM_COPIA_ROL.DEP AND P.COD=PM_COPIA_ROL.PER" & vbCrLf
sConsulta = sConsulta & "        WHERE P.COD = @PETICIONARIO AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID)=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "          --Actualizo los roles de la UON y DEP de la persona peticionaria" & vbCrLf
sConsulta = sConsulta & "       UPDATE PM_COPIA_ROL  SET PER = P.COD FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "         WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') AND ISNULL(P.UON3,'') = " & vbCrLf
sConsulta = sConsulta & "               ISNULL(PM_COPIA_ROL.UON3,'') AND P.DEP=PM_COPIA_ROL.DEP AND P.COD = @PETICIONARIO AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "          UPDATE PM_COPIA_ROL   SET PER = P.COD   FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') AND ISNULL(P.UON3,'') = " & vbCrLf
sConsulta = sConsulta & "                 ISNULL(PM_COPIA_ROL.UON3,'') AND P.COD = @PETICIONARIO AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND  PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "          IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol ni UON3" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              UPDATE PM_COPIA_ROL   SET PER = P.COD   FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') AND PM_COPIA_ROL.UON3 IS NULL AND " & vbCrLf
sConsulta = sConsulta & "                 PM_COPIA_ROL.DEP IS NULL AND P.COD = @PETICIONARIO AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND  PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "              IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol ni UON3" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                UPDATE PM_COPIA_ROL   SET PER = P.COD   FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND PM_COPIA_ROL.UON2 IS NULL AND PM_COPIA_ROL.UON3 IS NULL AND PM_COPIA_ROL.DEP IS NULL" & vbCrLf
sConsulta = sConsulta & "                         AND P.COD = @PETICIONARIO AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND  PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM PM_PARTICIPANTES P  WITH (NOLOCK) INNER JOIN PM_ROL R  WITH (NOLOCK) ON P.ROL=R.ID WHERE R.WORKFLOW=@WORKFLOW) >0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO PM_COPIA_PARTICIPANTES(ID,ROL, PER,GMN1,GMN2,GMN3,GMN4)" & vbCrLf
sConsulta = sConsulta & "        SELECT PM_PARTICIPANTES.ID,PM_COPIA_ROL.ID, PM_PARTICIPANTES.PER,PM_PARTICIPANTES.GMN1,PM_PARTICIPANTES.GMN2,PM_PARTICIPANTES.GMN3,PM_PARTICIPANTES.GMN4 " & vbCrLf
sConsulta = sConsulta & "         FROM PM_PARTICIPANTES  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN PM_COPIA_ROL   WITH (NOLOCK) ON PM_PARTICIPANTES.ROL = PM_COPIA_ROL.ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "             WHERE PM_COPIA_ROL.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ROL_BLOQUE(ROL,BLOQUE)" & vbCrLf
sConsulta = sConsulta & "     SELECT PM_COPIA_ROL.ID,PM_COPIA_BLOQUE.ID FROM PM_ROL_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON PM_ROL_BLOQUE.BLOQUE=PM_COPIA_BLOQUE.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ROL  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON PM_ROL_BLOQUE.ROL=PM_COPIA_ROL.ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "     WHERE PM_COPIA_BLOQUE.INSTANCIA=@ID AND PM_COPIA_ROL.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   --Aqui copiamos los listados para cada rol y bloque" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ROL_LISPER(ROL,BLOQUE,LISPER)" & vbCrLf
sConsulta = sConsulta & "      SELECT R.ID,B.ID,L.LISPER FROM PM_ROL_LISPER L WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON L.BLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ROL R   WITH (NOLOCK) ON L.ROL=R.ROL_ORIGEN " & vbCrLf
sConsulta = sConsulta & "      WHERE  B.INSTANCIA=@ID and R.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ROL_LISPER_DEN(ROL,BLOQUE,LISPER,IDI,ARCHIVO_RPT,DEN)" & vbCrLf
sConsulta = sConsulta & "      SELECT R.ID, B.ID, LD.LISPER, LD.IDI, LD.ARCHIVO_RPT, LD.DEN FROM PM_ROL_LISPER_DEN LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON LD.BLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON LD.ROL=R.ROL_ORIGEN " & vbCrLf
sConsulta = sConsulta & "      WHERE  B.INSTANCIA=@ID and R.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ROL_ACCION(ROL,BLOQUE,ACCION)" & vbCrLf
sConsulta = sConsulta & "   SELECT R.ID,B.ID,A.ID FROM PM_ROL_ACCION  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE B   WITH (NOLOCK) ON PM_ROL_ACCION.BLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ROL R   WITH (NOLOCK) ON PM_ROL_ACCION.ROL=R.ROL_ORIGEN " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ACCIONES A   WITH (NOLOCK) ON PM_ROL_ACCION.ACCION=A.ACCION_ORIGEN AND A.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "      WHERE  B.INSTANCIA=@ID and R.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   --Aqui deben ir las precondiciones" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ACCION_PRECOND(ORDEN,COD,ACCION,TIPO,FORMULA,PRECOND_ORIGEN)" & vbCrLf
sConsulta = sConsulta & "      SELECT AP.ORDEN, AP.COD, CA.ID, AP.TIPO, AP.FORMULA, AP.ID FROM PM_ACCION_PRECOND AP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_ACCIONES CA  WITH (NOLOCK)    ON AP.ACCION = CA.ACCION_ORIGEN " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK)   ON CA.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "      WHERE  B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ACCION_PRECOND_DEN(ACCION_PRECOND, IDI, DEN)" & vbCrLf
sConsulta = sConsulta & "      SELECT CAP.ID, APD.IDI, APD.DEN  FROM PM_ACCION_PRECOND_DEN APD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ACCION_PRECOND CAP  WITH (NOLOCK)    ON APD.ACCION_PRECOND = CAP.PRECOND_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ACCIONES CA    WITH (NOLOCK) ON CAP.ACCION = CA.ID" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK)  ON CA.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "      WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ACCION_CONDICIONES(ACCION_PRECOND,COD,TIPO_CAMPO,CAMPO,OPERADOR,TIPO_VALOR,CAMPO_VALOR,VALOR_TEXT,VALOR_NUM,VALOR_FEC,VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "     SELECT CAP.ID,AC.COD,AC.TIPO_CAMPO,CAMPO_DEF.ID,AC.OPERADOR,AC.TIPO_VALOR,CAMPO_DEF2.ID,AC.VALOR_TEXT,AC.VALOR_NUM,AC.VALOR_FEC,AC.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "     FROM PM_ACCION_CONDICIONES AC   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ACCION_PRECOND CAP    WITH (NOLOCK)      ON AC.ACCION_PRECOND=CAP.PRECOND_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ACCIONES CA                   WITH (NOLOCK)         ON CAP.ACCION = CA.ID" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE CB                     WITH (NOLOCK)          ON CA.BLOQUE=CB.ID " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "                          FROM COPIA_CAMPO_DEF  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_GRUPO  WITH (NOLOCK)   ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "       ON AC.CAMPO=CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "              FROM COPIA_CAMPO_DEF  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN COPIA_GRUPO   WITH (NOLOCK) ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF2" & vbCrLf
sConsulta = sConsulta & "          ON AC.CAMPO_VALOR=CAMPO_DEF2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "     WHERE CB.INSTANCIA=@ID " & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ENLACE(BLOQUE_ORIGEN,BLOQUE_DESTINO,ACCION,FORMULA,BLOQUEA,ENLACE_ORIGEN)" & vbCrLf
sConsulta = sConsulta & "     SELECT B1.ID,B2.ID,PM_COPIA_ACCIONES.ID,FORMULA,BLOQUEA,PM_ENLACE.ID" & vbCrLf
sConsulta = sConsulta & "     FROM PM_ENLACE   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE B1      WITH (NOLOCK)   ON PM_ENLACE.BLOQUE_ORIGEN=B1.BLOQUE_ORIGEN " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE B2     WITH (NOLOCK)     ON PM_ENLACE.BLOQUE_DESTINO=B2.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ACCIONES      WITH (NOLOCK)      ON PM_ENLACE.ACCION=PM_COPIA_ACCIONES.ACCION_ORIGEN " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PM_COPIA_BLOQUE B3       WITH (NOLOCK)    ON PM_COPIA_ACCIONES.BLOQUE=B3.ID " & vbCrLf
sConsulta = sConsulta & "     WHERE B1.INSTANCIA=@ID AND B2.INSTANCIA=@ID AND B3.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ENLACE_EXTRAPOINTS(ENLACE, ORDEN, X,Y)" & vbCrLf
sConsulta = sConsulta & "     SELECT E.ID, ORDEN, X,Y" & vbCrLf
sConsulta = sConsulta & "     FROM PM_ENLACE_EXTRAPOINTS X  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ENLACE E      WITH (NOLOCK)   ON X.ENLACE=E.ENLACE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE CB   WITH (NOLOCK)   ON E.BLOQUE_ORIGEN=CB.ID " & vbCrLf
sConsulta = sConsulta & "     WHERE CB.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ENLACE_CONDICION(ENLACE,COD,TIPO_CAMPO,CAMPO,OPERADOR,TIPO_VALOR,CAMPO_VALOR,VALOR_TEXT,VALOR_NUM,VALOR_FEC,VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "     SELECT CE.ID,COD,TIPO_CAMPO,CAMPO_DEF.ID,OPERADOR,TIPO_VALOR,CAMPO_DEF2.ID,VALOR_TEXT,VALOR_NUM,VALOR_FEC,VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "     FROM PM_ENLACE_CONDICIONES   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ENLACE CE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON PM_ENLACE_CONDICIONES.ENLACE=CE.ENLACE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON CE.BLOQUE_ORIGEN=CB.ID " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "                         FROM COPIA_CAMPO_DEF   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN COPIA_GRUPO   WITH (NOLOCK)  ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "          ON PM_ENLACE_CONDICIONES.CAMPO=CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "                           FROM COPIA_CAMPO_DEF   WITH (NOLOCK)  INNER JOIN COPIA_GRUPO   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                          ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF2" & vbCrLf
sConsulta = sConsulta & "          ON PM_ENLACE_CONDICIONES.CAMPO_VALOR=CAMPO_DEF2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHERE CB.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_CONF_CUMP_BLOQUE(BLOQUE,ROL,CAMPO,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN) " & vbCrLf
sConsulta = sConsulta & "     SELECT CB.ID,CR.ID,CCD.ID,PM_CONF_CUMP_BLOQUE.VISIBLE,PM_CONF_CUMP_BLOQUE.ESCRITURA,PM_CONF_CUMP_BLOQUE.OBLIGATORIO,PM_CONF_CUMP_BLOQUE.ORDEN" & vbCrLf
sConsulta = sConsulta & "     FROM PM_CONF_CUMP_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON PM_CONF_CUMP_BLOQUE.BLOQUE=CB.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ROL CR  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON PM_CONF_CUMP_BLOQUE.ROL=CR.ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN COPIA_CAMPO_DEF CCD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON PM_CONF_CUMP_BLOQUE.CAMPO=CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN COPIA_GRUPO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON CCD.GRUPO=COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "     WHERE  CB.INSTANCIA=@ID  AND CR.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_NOTIFICADO_ACCION(ACCION, TIPO_NOTIFICADO, PER , ROL) " & vbCrLf
sConsulta = sConsulta & "        SELECT CA.ID,PM_NOTIFICADO_ACCION.TIPO_NOTIFICADO,PM_NOTIFICADO_ACCION.PER, CR.ID" & vbCrLf
sConsulta = sConsulta & "        FROM PM_NOTIFICADO_ACCION   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCIONES    CA   WITH (NOLOCK)  ON PM_NOTIFICADO_ACCION.ACCION=CA.ACCION_ORIGEN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE       CB    WITH (NOLOCK) ON CA.BLOQUE=CB.ID" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN PM_COPIA_ROL                 CR   WITH (NOLOCK)  ON PM_NOTIFICADO_ACCION.ROL=CR.ROL_ORIGEN  AND CR.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "        WHERE CB.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_NOTIFICADO_ENLACE(ENLACE, TIPO_NOTIFICADO, PER , ROL) " & vbCrLf
sConsulta = sConsulta & "        SELECT CE.ID,PM_NOTIFICADO_ENLACE.TIPO_NOTIFICADO,PM_NOTIFICADO_ENLACE.PER, CR.ID" & vbCrLf
sConsulta = sConsulta & "        FROM PM_NOTIFICADO_ENLACE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ENLACE CE   WITH (NOLOCK)  ON PM_NOTIFICADO_ENLACE.ENLACE=CE.ENLACE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE CB   WITH (NOLOCK)  ON CE.BLOQUE_ORIGEN=CB.ID " & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN PM_COPIA_ROL CR        WITH (NOLOCK)      ON PM_NOTIFICADO_ENLACE.ROL=CR.ROL_ORIGEN AND CR.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "        WHERE CB.INSTANCIA=@ID    " & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ------------------------------------- Aqu� van las condiciones de bloqueo --------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_BLOQUEO_ETAPA (BLOQUEO_APERTURA, BLOQUEO_ADJUDICACION, BLOQUEO_PEDIDOSDIRECTOS, BLOQUEO_EP, IDBLOQUE, IDBLOQUEO_ORIGEN)" & vbCrLf
sConsulta = sConsulta & "     SELECT BLOQUEO_APERTURA, BLOQUEO_ADJUDICACION, BLOQUEO_PEDIDOSDIRECTOS, BLOQUEO_EP, B.ID, PM_BLOQUEO_ETAPA.ID" & vbCrLf
sConsulta = sConsulta & "     FROM PM_BLOQUEO_ETAPA   WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          ON PM_BLOQUEO_ETAPA.IDBLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "    WHERE  B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_CONDICIONES_BLOQUEO (COD,TIPO,MENSAJE_SPA,MENSAJE_ENG,MENSAJE_GER,TIPO_BLOQUEO,INSTANCIA,FECACT,FORMULA, CONDICIONES_ORIGEN) " & vbCrLf
sConsulta = sConsulta & "    SELECT COD,TIPO,MENSAJE_SPA,MENSAJE_ENG,MENSAJE_GER,TIPO_BLOQUEO,@ID,FECACT,FORMULA,ID FROM PM_CONDICIONES_BLOQUEO WITH (NOLOCK) WHERE WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_CONDICIONES_BLOQUEO_CONDICIONES (COD, TIPO_CAMPO, CAMPO, OPERADOR, TIPO_VALOR, CAMPO_VALOR, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL, FECACT, ID_COD)" & vbCrLf
sConsulta = sConsulta & "     SELECT PCBC.COD, PCBC.TIPO_CAMPO, CAMPO_DEF.ID, PCBC.OPERADOR, PCBC.TIPO_VALOR, CAMPO_DEF2.ID, PCBC.VALOR_TEXT, PCBC.VALOR_NUM, PCBC.VALOR_FEC, PCBC.VALOR_BOOL, PCBC.FECACT, PCCB.ID" & vbCrLf
sConsulta = sConsulta & "     FROM PM_CONDICIONES_BLOQUEO_CONDICIONES PCBC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PM_CONDICIONES_BLOQUEO PCB WITH (NOLOCK) ON PCBC.ID_COD = PCB.ID " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PM_COPIA_CONDICIONES_BLOQUEO PCCB WITH (NOLOCK) ON PCCB.CONDICIONES_ORIGEN = PCB.ID " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_CAMPO_DEF  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO  WITH (NOLOCK) ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "     ON PCBC.CAMPO=CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "               FROM COPIA_CAMPO_DEF  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO   WITH (NOLOCK) ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF2" & vbCrLf
sConsulta = sConsulta & "     ON PCBC.CAMPO_VALOR=CAMPO_DEF2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "     WHERE PCB.WORKFLOW = @WORKFLOW AND PCCB.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --AHORA INSERTAMOS LOS DATOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SENTIDO = (SELECT SENTIDO FROM TABLAS_INTEGRACION WHERE ID=18)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @FECHA=GETDATE()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Ahora inserta los campos, copiando los del formulario. Si @GUARDAR=1 despu�s actualizar� los campos modificables desde el PM/QA:" & vbCrLf
sConsulta = sConsulta & "   SET @NUM_VERSION = 1" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO VERSION_INSTANCIA(INSTANCIA,NUM_VERSION, PER_MODIF, FECHA_MODIF)" & vbCrLf
sConsulta = sConsulta & "    VALUES (@ID, @NUM_VERSION, @PETICIONARIO,GETDATE())" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @GUARDAR=1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT top 1  @BLOQUE = CB.ID,@ROL = CR.ID" & vbCrLf
sConsulta = sConsulta & "   FROM PM_CONF_CUMP_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON PM_CONF_CUMP_BLOQUE.BLOQUE=CB.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ROL CR  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON PM_CONF_CUMP_BLOQUE.ROL=CR.ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   WHERE  CB.INSTANCIA=@ID AND CB.TIPO = 1  AND CR.INSTANCIA=@ID AND CR.PER = @PETICIONARIO AND CR.TIPO = 4" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO (INSTANCIA, NUM_VERSION, SUCIO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, COPIA_CAMPO_DEF)" & vbCrLf
sConsulta = sConsulta & "   SELECT @ID, @NUM_VERSION, 0, T.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "   , /*CASE WHEN FC.TIPO_CAMPO_GS <> 119 OR FC.TIPO_CAMPO_GS IS NULL THEN  T.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "   ELSE CASE  WHEN CCB.VISIBLE = 0 AND  NOT EXISTS(SELECT ART4.COD FROM  #TEMPORAL TMAT" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN DIC GMN1 on GMN1.ID = 80" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN DIC GMN2 on GMN2.ID = 90" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN3 on GMN3.ID = 100" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DIC GMN4 on GMN4.ID = 110" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORM_CAMPO FC2 ON  FC2.ID = TMAT.CAMPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CCD2 ON CCD2.CAMPO_ORIGEN = FC2.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 ON CCB2.CAMPO=CCD2.ID  AND CCB2.ROL=@ROL AND CCB2.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ART4 ON ART4.GMN1=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                      AND ART4.GMN2=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD, GMN2.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                      AND ART4.GMN3=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD, GMN3.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                      AND ART4.GMN4=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD+GMN3.LONGITUD, GMN4.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "       WHERE ART4.COD=T.VALOR_TEXT COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "           AND CCD2.TIPO_CAMPO_GS=103 AND CCB2.ORDEN =(CCB.ORDEN-1)) THEN" & vbCrLf
sConsulta = sConsulta & "       CASE WHEN  not exists(SELECT ART4.COD FROM ART4 WHERE ART4.COD = T.VALOR_TEXT COLLATE SQL_Latin1_General_CP1_CI_AS) THEN" & vbCrLf
sConsulta = sConsulta & "           CASE WHEN FC.ANYADIR_ART = 0 THEN NULL ELSE T.VALOR_TEXT END" & vbCrLf
sConsulta = sConsulta & "       ELSE NULL END" & vbCrLf
sConsulta = sConsulta & "   ELSE*/ T.VALOR_TEXT /*END END*/" & vbCrLf
sConsulta = sConsulta & "   , T.VALOR_FEC, T.VALOR_BOOL, CCD.ID " & vbCrLf
sConsulta = sConsulta & "     FROM FORM_CAMPO FC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN #TEMPORAL T WITH (NOLOCK)   ON FC.ID  = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCD WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)  ON CCD.GRUPO = CGP.ID AND @ID = CGP.INSTANCIA)  ON CCD.CAMPO_ORIGEN= FC.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_copia_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON CCB.CAMPO= CCD.ID  AND CCB.ROL=@ROL AND CCB.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "    ORDER BY CGP.ID, FC.ES_SUBCAMPO, FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ID, CC.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK)  ON CA.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO_DEF CCD  WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID) ON CCD.CAMPO_ORIGEN = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "   WHERE CC.INSTANCIA = @ID AND T.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @MAXID INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @MAXID= (SELECT ISNULL(MAX(ID),0) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, PER, COMENT,ADJUN_ORIGEN)                " & vbCrLf
sConsulta = sConsulta & "    SELECT A.DATA, GETDATE(), A.NOM, @PETICIONARIO, A.COMENT, A.ID" & vbCrLf
sConsulta = sConsulta & "      FROM CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN #TEMPORAL_ADJUN T  WITH (NOLOCK) ON A.ID = T.ADJUN AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ID, CC.ID, CA.NOM, A.IDIOMA, A.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA, A.RUTA" & vbCrLf
sConsulta = sConsulta & "     FROM CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   ON A.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_ADJUN CA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   ON A.ID = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "                  AND T.ADJUN = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "                  AND CA.ID > @MAXID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                   ON CC.COPIA_CAMPO_DEF = CCD.ID)" & vbCrLf
sConsulta = sConsulta & "                   ON CCD.CAMPO_ORIGEN = T.CAMPO " & vbCrLf
sConsulta = sConsulta & "   WHERE CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "     AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_DESGLOSE (CAMPO_PADRE, CAMPO_HIJO) " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT CCP.ID, CCH.ID " & vbCrLf
sConsulta = sConsulta & "     FROM (SELECT D.CAMPO_PADRE, D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "             FROM DESGLOSE D  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN FORM_CAMPO FC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                            ON D.CAMPO_PADRE = FC.ID" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN FORM_GRUPO FG  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                            ON FC.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "            WHERE FG.FORMULARIO = @FORMULARIO) D" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN (COPIA_CAMPO_DEF CCDP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDP.ID=CCP.COPIA_CAMPO_DEF " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDP.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "                    ON CCDP.CAMPO_ORIGEN = D.CAMPO_PADRE " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN (COPIA_CAMPO_DEF CCDH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDH.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDH.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "                    ON CCDH.CAMPO_ORIGEN = D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --*********************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "   --INSERTAR LAS NUEVAS LINEAS DE DESGLOSE QUE SE INTRODUZCAN DESDE EL PM" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   SELECT  T.LINEA, CCP.ID,CCH.ID, T.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "   , /*CASE  WHEN CCDHIJO.TIPO_CAMPO_GS <> 119 OR CCDHIJO.tipo_campo_gs is null then  T.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "     ELSE  CASE  WHEN  CCB.visible = 0 AND not exists (SELECT ART4.COD FROM ART4" & vbCrLf
sConsulta = sConsulta & "       INNER  JOIN #TEMPORAL_DESGLOSE TMAT  ON TMAT.CAMPO_PADRE = (T.CAMPO_PADRE) AND TMAT.LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DIC GMN1 ON GMN1.id = 80" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DIC GMN2 ON GMN2.id = 90" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DIC GMN3 ON GMN3.id = 100" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DIC GMN4 ON GMN4.id = 110       " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF) ON CCDHIJO.CAMPO_ORIGEN = TMAT.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 WITH (NOLOCK)  ON CCB2.CAMPO = CCDHIJO.ID AND CCB2.ROL=@ROL AND CCB2.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "       WHERE  CCB2.ORDEN =(CCB.ORDEN-1)" & vbCrLf
sConsulta = sConsulta & "           AND ART4.COD = T.VALOR_TEXT COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "               AND ART4.GMN1=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "               AND ART4.GMN2=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud, GMN2.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "               AND ART4.GMN3=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud+GMN2.longitud, GMN3.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "               AND ART4.GMN4=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud+GMN2.longitud+GMN3.longitud, GMN4.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "               AND CCDHIJO.TIPO_CAMPO_GS= 103 ) THEN" & vbCrLf
sConsulta = sConsulta & "       CASE WHEN  not exists(SELECT ART4.COD FROM ART4 WHERE ART4.COD = T.VALOR_TEXT  COLLATE SQL_Latin1_General_CP1_CI_AS) THEN" & vbCrLf
sConsulta = sConsulta & "           CASE WHEN CCDHIJO.ANYADIR_ART = 0 THEN NULL ELSE T.VALOR_TEXT END" & vbCrLf
sConsulta = sConsulta & "       ELSE  NULL END" & vbCrLf
sConsulta = sConsulta & "      ELSE */   T.VALOR_TEXT /*END END*/" & vbCrLf
sConsulta = sConsulta & "   , T.VALOR_FEC, T.VALOR_BOOL " & vbCrLf
sConsulta = sConsulta & "     FROM #TEMPORAL_DESGLOSE T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_copia_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON CCB.CAMPO=CCDHIJO.ID AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @MAXID= (SELECT ISNULL(MAX(ID),0) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Cambios para trazabilidad l�neas solicitudes con los ERP" & vbCrLf
sConsulta = sConsulta & "   if @sentido = 1 " & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA (INSTANCIA, NUM_VERSION,  LINEA, CAMPO_PADRE)" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT @ID, @NUM_VERSION, T.LINEA, CCP.ID" & vbCrLf
sConsulta = sConsulta & "     FROM #TEMPORAL_DESGLOSE T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA AND CCDPADRE.TIPO_CAMPO_GS=106)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, PER, COMENT,ADJUN_ORIGEN)                " & vbCrLf
sConsulta = sConsulta & "    SELECT A.DATA, GETDATE(), A.NOM, @PETICIONARIO, A.COMENT, A.ID" & vbCrLf
sConsulta = sConsulta & "      FROM LINEA_DESGLOSE_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN (select DISTINCT ADJUN, TIPO FROM #TEMPORAL_DESGLOSE_ADJUN  WITH (NOLOCK) ) T" & vbCrLf
sConsulta = sConsulta & "                    ON A.ID = T.ADJUN AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CCP.ID, CCH.ID,  T.LINEA, CA.ID, CA.NOM, 'SPA', A.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA, A.RUTA" & vbCrLf
sConsulta = sConsulta & "     FROM LINEA_DESGLOSE_ADJUN A  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   ON A.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_ADJUN CA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   ON A.ID = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "                  AND T.ADJUN = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "                  AND CA.ID>@MAXID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CCP.ID, CCH.ID,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   ON CA.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL " & vbCrLf
sConsulta = sConsulta & "   SET PER=CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CASE WHEN T.PER='' THEN NULL ELSE T.PER END ELSE U.FSWS_SUSTITUCION END, " & vbCrLf
sConsulta = sConsulta & "       PROVE=CASE WHEN T.PROVE=''  THEN NULL ELSE T.PROVE END," & vbCrLf
sConsulta = sConsulta & "       CON=CASE WHEN T.CON=0  THEN NULL ELSE T.CON END" & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_PARTICIPANTES T WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "      left join USU U WITH (NOLOCK) on T.PER = U.PER" & vbCrLf
sConsulta = sConsulta & "      inner join PM_COPIA_ROL PCR WITH (NOLOCK) on PCR.ROL_ORIGEN = T.ROL" & vbCrLf
sConsulta = sConsulta & "      inner join INSTANCIA WITH (NOLOCK)  ON PCR.INSTANCIA=INSTANCIA.ID AND (INSTANCIA.ESTADO=0 or INSTANCIA.ESTADO=2 or INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "   WHERE PCR.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ELSE  --No guarda los campos modificados desde el QA , simplemente hace una copia del formulario" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "     INSERT INTO COPIA_CAMPO (INSTANCIA, NUM_VERSION, SUCIO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, COPIA_CAMPO_DEF)" & vbCrLf
sConsulta = sConsulta & "     SELECT @ID, @NUM_VERSION, 0, FC.VALOR_NUM, FC.VALOR_TEXT, FC.VALOR_FEC, FC.VALOR_BOOL, CCD.ID " & vbCrLf
sConsulta = sConsulta & "     FROM FORM_CAMPO FC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN (COPIA_CAMPO_DEF CCD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)  ON CCD.GRUPO = CGP.ID AND @ID = CGP.INSTANCIA)  ON FC.ID = CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @MAXID= (SELECT ISNULL(MAX(ID),0) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, PER, COMENT,ADJUN_ORIGEN)                " & vbCrLf
sConsulta = sConsulta & "    SELECT A.DATA, GETDATE(), A.NOM, @PETICIONARIO, A.COMENT, A.ID" & vbCrLf
sConsulta = sConsulta & "      FROM CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN FORM_CAMPO FC WITH (NOLOCK)  ON A.CAMPO=FC.ID" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN (COPIA_CAMPO_DEF  CCD WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)  ON CCD.GRUPO = CGP.ID AND @ID = CGP.INSTANCIA)  ON FC.ID = CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ID, CC.ID, CA.NOM, A.IDIOMA, A.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA, A.RUTA" & vbCrLf
sConsulta = sConsulta & "     FROM CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_ADJUN CA WITH (NOLOCK)  ON A.ID = CA.ADJUN_ORIGEN  AND CA.ID > @MAXID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK)  ON CC.COPIA_CAMPO_DEF = CCD.ID)" & vbCrLf
sConsulta = sConsulta & "                   ON CCD.CAMPO_ORIGEN = A.CAMPO" & vbCrLf
sConsulta = sConsulta & "   WHERE CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_DESGLOSE (CAMPO_PADRE, CAMPO_HIJO) " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT CCP.ID, CCH.ID " & vbCrLf
sConsulta = sConsulta & "     FROM (SELECT D.CAMPO_PADRE, D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "             FROM DESGLOSE D  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN FORM_CAMPO FC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                            ON D.CAMPO_PADRE = FC.ID" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN FORM_GRUPO FG  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                            ON FC.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "            WHERE FG.FORMULARIO = @FORMULARIO) D" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN (COPIA_CAMPO_DEF CCDP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDP.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDP.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA) " & vbCrLf
sConsulta = sConsulta & "                    ON CCDP.CAMPO_ORIGEN = D.CAMPO_PADRE " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN (COPIA_CAMPO_DEF CCDH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDH.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDH.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "                    ON CCDH.CAMPO_ORIGEN = D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   SELECT  L.LINEA, CCP.ID,CCH.ID, L.VALOR_NUM, L.VALOR_TEXT, L.VALOR_FEC, L.VALOR_BOOL " & vbCrLf
sConsulta = sConsulta & "     FROM LINEA_DESGLOSE L  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON L.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON L.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @MAXID= (SELECT ISNULL(MAX(ID),0) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Cambios para trazabilidad l�neas solicitudes con los ERP" & vbCrLf
sConsulta = sConsulta & "   if @sentido = 1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA (INSTANCIA, NUM_VERSION, LINEA, CAMPO_PADRE)" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT @ID, @NUM_VERSION, L.LINEA, CCP.ID" & vbCrLf
sConsulta = sConsulta & "     FROM LINEA_DESGLOSE L  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA AND CCDPADRE.TIPO_CAMPO_GS=106)" & vbCrLf
sConsulta = sConsulta & "               ON L.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, PER, COMENT,ADJUN_ORIGEN)                " & vbCrLf
sConsulta = sConsulta & "    SELECT A.DATA, GETDATE(), A.NOM, @PETICIONARIO, A.COMENT, A.ID" & vbCrLf
sConsulta = sConsulta & "      FROM LINEA_DESGLOSE_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN FORM_CAMPO FC WITH (NOLOCK)  ON A.CAMPO_HIJO=FC.ID" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN (COPIA_CAMPO_DEF CCD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)  ON CCD.GRUPO = CGP.ID AND @ID = CGP.INSTANCIA)  ON FC.ID = CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CCP.ID, CCH.ID,  A.LINEA, CA.ID, CA.NOM, 'SPA', A.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA, A.RUTA" & vbCrLf
sConsulta = sConsulta & "     FROM LINEA_DESGLOSE_ADJUN A WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_ADJUN CA WITH (NOLOCK)  ON A.ID = CA.ADJUN_ORIGEN  AND CA.ID>@MAXID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON A.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON A.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Devuelve el id de la acci�n de copia_campo: " & vbCrLf
sConsulta = sConsulta & "   IF @ACCION<>0  --Si no est� guardando:" & vbCrLf
sConsulta = sConsulta & "   SET @ACCION=(SELECT PM_COPIA_ACCIONES.ID FROM PM_COPIA_ACCIONES  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK) ON PM_COPIA_ACCIONES.BLOQUE=B.ID WHERE B.INSTANCIA=@ID AND  ACCION_ORIGEN=@ACCION)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --------------------------------------------- Pone a null los campos origen  --------------------------------------------- " & vbCrLf
sConsulta = sConsulta & "   /*" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_BLOQUE SET BLOQUE_ORIGEN=NULL WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL SET ROL_ORIGEN=NULL WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ACCIONES SET ACCION_ORIGEN=NULL FROM PM_COPIA_ACCIONES A WITH (NOLOCK)  INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK)  ON A.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ENLACE SET ENLACE_ORIGEN=NULL FROM PM_COPIA_ENLACE E  WITH (NOLOCK) INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK)  ON E.BLOQUE_ORIGEN=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ACCION_PRECOND SET PRECOND_ORIGEN = NULL FROM PM_COPIA_ACCION_PRECOND AP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ACCIONES A WITH (NOLOCK)  ON AP.ACCION = A.ID" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK)  ON A.BLOQUE = B.ID" & vbCrLf
sConsulta = sConsulta & "      WHERE B.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "   */" & vbCrLf
sConsulta = sConsulta & "   ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Comprueba el tipo de la solicitud para saber si tiene que emitir un certificado o una no conformidad" & vbCrLf
sConsulta = sConsulta & "   SELECT @TIPO_SOLICIT=TP.TIPO FROM SOLICITUD WITH (NOLOCK)  INNER JOIN TIPO_SOLICITUDES TP WITH (NOLOCK)  ON SOLICITUD.TIPO=TP.ID WHERE SOLICITUD.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   --Inserta en INSTANCIA_BLOQUE el bloque del peticionario:" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO INSTANCIA_BLOQUE(INSTANCIA,BLOQUE,ESTADO) " & vbCrLf
sConsulta = sConsulta & "   SELECT @ID,PM_COPIA_BLOQUE.ID, 1 FROM PM_COPIA_BLOQUE WITH (NOLOCK)  WHERE INSTANCIA=@ID AND TIPO=1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Actualizar la tabla PM_COPIA_ROL, para establecer roles mediante campos" & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Comprador o usuario" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "   SET PER = CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CC.VALOR_TEXT ELSE U.FSWS_SUSTITUCION END" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   left join USU U WITH (NOLOCK) on CC.VALOR_TEXT = U.PER" & vbCrLf
sConsulta = sConsulta & "   inner join PM_COPIA_ROL PCR WITH (NOLOCK) on PCR.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   inner join INSTANCIA WITH (NOLOCK)  ON PCR.INSTANCIA=INSTANCIA.ID AND (INSTANCIA.ESTADO=0 or INSTANCIA.ESTADO=2 or INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "   WHERE (PCR.TIPO = 1 OR PCR.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "      AND PCR.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND PCR.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND PCR.BLOQUE IN (SELECT BLOQUE" & vbCrLf
sConsulta = sConsulta & "                      FROM INSTANCIA_BLOQUE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                      WHERE ESTADO = 1 " & vbCrLf
sConsulta = sConsulta & "                      AND INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "      AND PCR.COMO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND PCR.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "      AND CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND CC.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Proveedor" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "   SET PROVE = CC.VALOR_TEXT, @PROVE = CC.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE PM_COPIA_ROL.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PM_COPIA_ROL.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND PM_COPIA_ROL.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND PM_COPIA_ROL.BLOQUE IN (SELECT BLOQUE" & vbCrLf
sConsulta = sConsulta & "                      FROM INSTANCIA_BLOQUE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                      WHERE ESTADO = 1" & vbCrLf
sConsulta = sConsulta & "                      AND INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "      AND PM_COPIA_ROL.COMO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND PM_COPIA_ROL.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "      AND CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND CC.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --HHF 31/10/2007 INI" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --INSERT INTO INSTANCIA_PM (INSTANCIA) VALUES (@ID)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --SET @NUM = (SELECT NUM FROM INSTANCIA_PM WITH (NOLOCK)  WHERE INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE INSTANCIA SET PEDIDO_AUT = @PEDIDO_AUT, IMPORTE = @IMPORTE,  PEDIDO_DIRECTO =  @IMPORTE_PEDIDO_DIRECTO, NUM = @ID WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --HHF 31/10/2007 FIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Sentido integraci�n PM" & vbCrLf
sConsulta = sConsulta & "   SET @SENTIDO = (SELECT SENTIDO FROM TABLAS_INTEGRACION WHERE ID=18 AND ACTIVA = 1)" & vbCrLf
sConsulta = sConsulta & "   SET @FECHA = GETDATE()" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO VERSION_INSTANCIA(INSTANCIA, NUM_VERSION, PROVE, PER_MODIF, FECHA_MODIF, TIPO, CERTIFICADO)" & vbCrLf
sConsulta = sConsulta & "     VALUES (@ID, @NUM_VERSION, @PROVE, @PETICIONARIO,@FECHA , 0,0)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @IMPORTE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "     UPDATE INSTANCIA  SET IMPORTE = @IMPORTE  WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      IF @PETICIONARIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT @BLOQUE=IB.BLOQUE, @ROL=R.ID FROM INSTANCIA_BLOQUE IB  WITH (NOLOCK) INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID WHERE IB.INSTANCIA=@ID AND IB.ESTADO=1 AND R.PER=@PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT @BLOQUE=IB.BLOQUE, @ROL=R.ID FROM INSTANCIA_BLOQUE IB WITH (NOLOCK)  INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID WHERE IB.INSTANCIA=@ID AND IB.ESTADO=1 AND R.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Crea tablas temporales para tener la relaci�n de los campos almacenados en la temporal con el id nuevo:" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #TEMPORAL_CAMPOS (CAMPO_OLD INT, CAMPO_NEW INT)" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #TEMPORAL_CAMPO_ADJUN (ADJUN_OLD INT, ADJUN_NEW INT)" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #TEMP_CAMPO_ADJUN (ADJUN_OLD INT, ADJUN_NEW INT, LINEA INT)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Inserta en copia_campo con los valores de la �ltima versi�n:" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO (INSTANCIA, NUM_VERSION, SUCIO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, COPIA_CAMPO_DEF)" & vbCrLf
sConsulta = sConsulta & "   SELECT @ID, @NUM_VERSION, 0, C.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "   ,/* CASE WHEN CCD.TIPO_CAMPO_GS <> 119 OR CCD.TIPO_CAMPO_GS IS NULL THEN C.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "     ELSE CASE WHEN CCD.ANYADIR_ART =0 and CCB.VISIBLE = 0 AND NOT EXISTS(select ART4.COD from  #TEMPORAL TMAT" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN dic GMN1 on gmn1.id = 80" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN dic GMN2 on GMN2.id = 90" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN dic GMN3 on GMN3.id = 100" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN dic GMN4 on GMN4.id = 110" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN COPIA_CAMPO CC ON CC.ID=TMAT.CAMPO" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN COPIA_CAMPO_DEF  CCD2 ON CCD2.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 ON CCB2.CAMPO=CC.COPIA_CAMPO_DEF AND CCB2.ROL=@ROL AND CCB2.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ART4 ON ART4.GMN1=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                             AND ART4.GMN2=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD, GMN2.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                             AND ART4.GMN3=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD, GMN3.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                             AND ART4.GMN4=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD+GMN3.LONGITUD, GMN4.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "              WHERE ART4.COD=C.VALOR_TEXT AND CCD2.TIPO_CAMPO_GS=103 AND CCB2.ORDEN = (CCB.ORDEN-1)" & vbCrLf
sConsulta = sConsulta & "                   and CC.INSTANCIA= @ID  AND CC.NUM_VERSION = @OLD_VERSION ) then" & vbCrLf
sConsulta = sConsulta & "                  null                " & vbCrLf
sConsulta = sConsulta & "      ELSE*/ C.VALOR_TEXT /*  END END*/" & vbCrLf
sConsulta = sConsulta & "   , C.VALOR_FEC, C.VALOR_BOOL, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF  CCD ON CCD.ID = C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "   WHERE C.INSTANCIA= @ID  AND C.NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "   ORDER BY CCD.GRUPO, CCD.ES_SUBCAMPO, CCB.ORDEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Crea la temporal con el id de la versi�n anterior y el nuevo:" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO #TEMPORAL_CAMPOS (CAMPO_OLD, CAMPO_NEW)" & vbCrLf
sConsulta = sConsulta & "   SELECT COLD.ID CAMPO_OLD, CNEW.ID CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO COLD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CNEW  WITH (NOLOCK) ON COLD.COPIA_CAMPO_DEF = CNEW.COPIA_CAMPO_DEF AND COLD.INSTANCIA = CNEW.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   WHERE COLD.INSTANCIA = @ID AND COLD.NUM_VERSION = @OLD_VERSION AND CNEW.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   ------------------------------------------------------ Ahora actualiza los id de todas las tablas temporales creadas desde el PM/QA: ------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL SET CAMPO = TC.CAMPO_NEW " & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON T.CAMPO = CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CCD.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK) ON CC.ID = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_ADJUN SET CAMPO = TC.CAMPO_NEW " & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON T.CAMPO = CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CCD.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK) ON CC.ID = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_DESGLOSE SET CAMPO_PADRE = TCP.CAMPO_NEW, CAMPO_HIJO = TCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_DESGLOSE T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCDP WITH (NOLOCK) ON T.CAMPO_PADRE = CCDP.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CCP WITH (NOLOCK) ON CCDP.ID = CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TCP WITH (NOLOCK)  ON CCP.ID = TCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCDH WITH (NOLOCK) ON T.CAMPO_HIJO = CCDH.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CCH WITH (NOLOCK) ON CCDH.ID = CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TCH WITH (NOLOCK)  ON CCH.ID = TCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_DESGLOSE_ADJUN SET CAMPO_PADRE = TCP.CAMPO_NEW, CAMPO_HIJO = TCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_DESGLOSE_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCDP WITH (NOLOCK) ON T.CAMPO_PADRE = CCDP.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CCP WITH (NOLOCK) ON CCDP.ID = CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TCP WITH (NOLOCK)  ON CCP.ID = TCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCDH WITH (NOLOCK) ON T.CAMPO_HIJO = CCDH.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CCH WITH (NOLOCK) ON CCDH.ID = CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TCH WITH (NOLOCK)  ON CCH.ID = TCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "    ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   --Inserta los datos en todas las tablas (con los mismos valores que en la versi�n anterior) :" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ADJUN, TC.CAMPO_NEW, CA.NOM, CA.IDIOMA, CA.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO_ADJUN CA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK)  ON CA.CAMPO = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO  #TEMPORAL_CAMPO_ADJUN (ADJUN_OLD ,ADJUN_NEW )" & vbCrLf
sConsulta = sConsulta & "   SELECT CCA_OLD.ID ,CCA_NEW.ID   " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO_ADJUN CCA_OLD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_ADJUN CCA_NEW WITH (NOLOCK)  ON CCA_OLD.ADJUN = CCA_NEW.ADJUN " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TC  WITH (NOLOCK) ON CCA_OLD.CAMPO = TC.CAMPO_old AND CCA_NEW.CAMPO = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_ADJUN SET ADJUN = TC.ADJUN_NEW FROM #TEMPORAL_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPO_ADJUN  TC WITH (NOLOCK)  ON T.ADJUN = TC.ADJUN_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_DESGLOSE (CAMPO_PADRE, CAMPO_HIJO)  SELECT DISTINCT CCP.CAMPO_NEW, CCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_DESGLOSE CD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCP WITH (NOLOCK)  ON CD.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK)  ON CD.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Inserta en COPIA_LINEA_DESGLOSE los campos que hay en la temporal con LINEA la antigua" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   SELECT CL.LINEA, CCP.CAMPO_NEW, CCH.CAMPO_NEW, CL.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "   , /*CASE WHEN CCD.TIPO_CAMPO_GS <> 119 OR CCD.TIPO_CAMPO_GS is null then CL.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "     ELSE  CASE WHEN CCD.ANYADIR_ART =0  and ccb.visible = 0 and NOT EXISTS(SELECT ART4.COD FROM ART4" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN COPIA_LINEA_DESGLOSE TMAT  ON TMAT.CAMPO_PADRE = (CL.CAMPO_PADRE)   AND TMAT.LINEA = CL.LINEA " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN1 on GMN1.ID = 80" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN2 on GMN2.ID = 90" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN3 on GMN3.ID = 100" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN4 on GMN4.ID = 110" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN COPIA_CAMPO CC WITH (NOLOCK)  ON CC.ID = TMAT.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN COPIA_CAMPO_DEF CCD2 ON CCD2.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 WITH (NOLOCK)  ON CCB2.CAMPO = cc.copia_campo_def AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "              WHERE CCB2.ORDEN = (CCB.ORDEN - 1)" & vbCrLf
sConsulta = sConsulta & "                  and COD = CL.VALOR_TEXT COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                  AND ART4.GMN1=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                  AND ART4.GMN2=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud, GMN2.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                  AND ART4.GMN3=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud+GMN2.longitud, GMN3.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                  AND ART4.GMN4=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud+GMN2.longitud+GMN3.longitud, GMN4.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                  and CCD2.TIPO_CAMPO_GS=103 ) THEN" & vbCrLf
sConsulta = sConsulta & "              NULL            " & vbCrLf
sConsulta = sConsulta & "      ELSE*/ CL.VALOR_TEXT /*END END*/" & vbCrLf
sConsulta = sConsulta & "   , CL.VALOR_FEC, CL.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCP  WITH (NOLOCK) ON CL.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK)  ON CL.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CC WITH (NOLOCK)  ON CC.ID = CL.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK)  ON CCD.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON CCB.CAMPO = cc.copia_campo_def AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --los que ya estaban en desgloses" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CCP.CAMPO_NEW, CCH.CAMPO_NEW, CL.LINEA, CL.ADJUN,CL.NOM, CL.IDIOMA, CL.DATASIZE, CL.PER, CL.COMENT, CL.FECALTA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE_ADJUN CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCP  WITH (NOLOCK) ON CL.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK)  ON CL.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Trazabilidad lineas PM con los ERP" & vbCrLf
sConsulta = sConsulta & "   IF @SENTIDO = 1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   ----Inserta en COPIA_LINEA  los campos que hay en la temporal con LINEA la antigua" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO COPIA_LINEA (INSTANCIA, NUM_VERSION, CAMPO_PADRE, LINEA, NUM_LINEA_SAP)" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID, @NUM_VERSION,  CCP.CAMPO_NEW, CL.LINEA, CL.NUM_LINEA_SAP" & vbCrLf
sConsulta = sConsulta & "      FROM COPIA_LINEA CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN #TEMPORAL_CAMPOS CCP  WITH (NOLOCK) ON CL.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM #TEMPORAL_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   -- Guarda los ID de COPIA_LINEA_DESGLOSE_ADJUN para los adjuntos que ya existian" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO  #TEMP_CAMPO_ADJUN (ADJUN_OLD ,ADJUN_NEW,LINEA )" & vbCrLf
sConsulta = sConsulta & "   SELECT CCA_OLD.ID ,CCA_NEW.ID,CCA_NEW.LINEA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE_ADJUN CCA_OLD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CCA_NEW  WITH (NOLOCK)  ON CCA_OLD.ADJUN = CCA_NEW.ADJUN " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TC  WITH (NOLOCK)  ON CCA_OLD.CAMPO_HIJO = TC.CAMPO_old AND CCA_NEW.CAMPO_HIJO = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_DESGLOSE_ADJUN SET ADJUN = TC.ADJUN_NEW " & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMP_CAMPO_ADJUN  TC WITH (NOLOCK)  ON T.ADJUN = TC.ADJUN_OLD AND T.LINEA = TC.LINEA" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   ------------------------------------------------Ahora actualiza las tablas con los valores modificados desde el PM/QA----------------------------------------------------------------------------------------------------------------------------------------- " & vbCrLf
sConsulta = sConsulta & "   --Actualizamos los campos con los valores del formulario modificables por el usuario --La Actualizacion del campo ARTICULO, (TIPO_CAMPO_GS = 119) tiene que ver con el DOC. DES_PRT_PM_ ID20" & vbCrLf
sConsulta = sConsulta & "   UPDATE COPIA_CAMPO SET VALOR_NUM = T.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "   , VALOR_TEXT =" & vbCrLf
sConsulta = sConsulta & "   /*CASE WHEN CD.TIPO_CAMPO_GS <> 119 OR CD.TIPO_CAMPO_GS is NULL THEN  T.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "       ELSE CASE WHEN CD.ANYADIR_ART = 0  and CCB.VISIBLE = 0 AND NOT EXISTS (SELECT 1 FROM #TEMPORAL TMAT" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN DIC GMN1 ON GMN1.id = 80" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN DIC GMN2 ON GMN2.id = 90" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN DIC GMN3 ON GMN3.id = 100" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN DIC GMN4 ON GMN4.id = 110" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN COPIA_CAMPO CC ON CC.ID=TMAT.CAMPO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN COPIA_CAMPO_DEF  CCD2 ON CCD2.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 ON CCB2.CAMPO=CC.COPIA_CAMPO_DEF AND CCB2.ROL=@ROL AND CCB2.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN ART4 ON  ART4.COD=T.VALOR_TEXT COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                and ART4.GMN1=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "               AND ART4.GMN2=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD, GMN2.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "               AND ART4.GMN3=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD, GMN3.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "               AND ART4.GMN4=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD+GMN3.LONGITUD, GMN4.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "      WHERE CCD2.TIPO_CAMPO_GS=103 AND CCB2.ORDEN = (CCB.ORDEN-1) ) then" & vbCrLf
sConsulta = sConsulta & "          null                " & vbCrLf
sConsulta = sConsulta & "      ELSE*/ T.VALOR_TEXT   /*END END*/" & vbCrLf
sConsulta = sConsulta & "   , VALOR_FEC = T.VALOR_FEC, VALOR_BOOL=T.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL T WITH (NOLOCK)  ON C.ID = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND (ESCRITURA=1  OR (ESCRITURA=0 AND CD.FORMULA IS NOT NULL) OR (VISIBLE = 0 AND( CD.TIPO_CAMPO_GS=119 or  CD.TIPO_CAMPO_GS=103  or  CD.TIPO_CAMPO_GS=105))  OR (ESCRITURA=0 AND CD.TIPO_CAMPO_GS=118))" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Actualizamos a sucio los campos de versiones anteriores que han cambiado con la nueva versi�n:" & vbCrLf
sConsulta = sConsulta & "   UPDATE COPIA_CAMPO SET SUCIO = 1 FROM COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN (SELECT * FROM COPIA_CAMPO WITH (NOLOCK)  WHERE INSTANCIA = @ID AND NUM_VERSION= @NUM_VERSION) CNEW" & vbCrLf
sConsulta = sConsulta & "   ON C.COPIA_CAMPO_DEF = CNEW.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   WHERE C.INSTANCIA = @ID AND C.NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "   AND (C.VALOR_NUM<>CNEW.VALOR_NUM OR C.VALOR_FEC <> CNEW.VALOR_FEC OR C.VALOR_TEXT <>CNEW.VALOR_TEXT OR C.VALOR_BOOL <> CNEW.VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Eliminamos los adjuntos de los campos tipo form que ya no est�n en esta nueva versi�n" & vbCrLf
sConsulta = sConsulta & "   --Solo eliminamos los que no estan en #temporal_adjun y ademas son visibles y modificables" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM COPIA_CAMPO_ADJUN FROM COPIA_CAMPO_ADJUN CCA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL T WITH (NOLOCK)  ON CCA.CAMPO = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C  WITH (NOLOCK) on CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND ESCRITURA=1  AND  VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_ADJUN TA WITH (NOLOCK)  WHERE CCA.ID = TA.ADJUN AND TA.TIPO = 1 and ta.portal=0 ) and not exists (SELECT * FROM #TEMPORAL_ADJUN TA WITH (NOLOCK) WHERE CCA.ADJUN = TA.ADJUN AND TA.TIPO = 1 and ta.portal=1 ) " & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Insertamos los adjuntos de los campos tipo form (Insertados desde el PM)" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ID, T.CAMPO, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_ADJUN CA  WITH (NOLOCK) INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK)  ON CA.ID = T.ADJUN " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH (NOLOCK)  ON T.CAMPO=C.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND ESCRITURA=1" & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 2 AND T.PORTAL = 0" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Insertamos los adjuntos metidos en el portal por el proveedor" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, COMENT, FECALTA, PROVE)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ID, T.CAMPO, CA.NOM, 'SPA', CCA.DATASIZE, CA.COMENT, CA.FECALTA, CA.PROVE" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN (P_COPIA_CAMPO_ADJUN CCA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    inner join p_copia_campo c  WITH (NOLOCK) on cca.campo = c.id and c.instancia = @ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK)  ON CCA.ADJUN_PORTAL = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO   WITH (NOLOCK) ON T.CAMPO=COPIA_CAMPO.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB  WITH (NOLOCK) ON COPIA_CAMPO.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND ESCRITURA=1" & vbCrLf
sConsulta = sConsulta & "   ) ON CA.ADJUN_ORIGEN = CCA.id  AND CA.ADJUN_PORTAL = CCA.ADJUN_PORTAL" & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 2 AND T.PORTAL = 1 and ca.portal = 1 and ca.prove=cca.prove" & vbCrLf
sConsulta = sConsulta & "   --Ojo el id que da el portal para los adjuntos no tiene en cuenta el id que da el PM -> Portal dice 143 PM dice 143 pero uno es 'Archivo.txt' y otro es 'Otro Archivo.txt' -> controla el prove" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   -- ELIMINAMOS LOS ADJUNTOS QUE HAYAN SIDO ELIMINADOS" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK)  ON CLDA.CAMPO_PADRE = TD.CAMPO_PADRE AND CLDA.CAMPO_HIJO = TD.CAMPO_HIJO AND CLDA.LINEA = TD.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "   inner join copia_campo cc WITH (NOLOCK)  on cc.id = clda.campo_hijo" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB  WITH (NOLOCK) ON CCB.CAMPO=cc.copia_campo_def AND ROL=@ROL AND BLOQUE=@BLOQUE  AND ESCRITURA=1  AND  VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT  * FROM #TEMPORAL_DESGLOSE_ADJUN TDA  WITH (NOLOCK) WHERE CLDA.ID = TDA.ADJUN AND TDA.TIPO = 1  AND TDA.PORTAL = 0  AND TDA.LINEA = CLDA.LINEA) " & vbCrLf
sConsulta = sConsulta & "   AND NOT EXISTS (SELECT  * FROM #TEMPORAL_DESGLOSE_ADJUN TDA  WITH (NOLOCK) WHERE CLDA.ADJUN = TDA.ADJUN AND TDA.TIPO = 1  AND TDA.PORTAL = 1)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK)  ON CLDA.CAMPO_PADRE = TD.CAMPO_PADRE AND CLDA.CAMPO_HIJO = TD.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_DESGLOSE TD2  WITH (NOLOCK) WHERE CLDA.CAMPO_PADRE=TD2.CAMPO_PADRE AND CLDA.CAMPO_HIJO = TD2.CAMPO_HIJO AND CLDA.LINEA = TD2.LINEA_OLD)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Elimina los campos de las lineas que no est�n en la temporal por el numero de linea que ten�an= lineas eliminadas" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK)  ON CLD.CAMPO_PADRE = TD.CAMPO_PADRE AND CLD.CAMPO_HIJO = TD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_DESGLOSE TD2 WITH (NOLOCK)  WHERE CLD.CAMPO_PADRE=TD2.CAMPO_PADRE AND CLD.CAMPO_HIJO = TD2.CAMPO_HIJO AND CLD.LINEA = TD2.LINEA_OLD)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Trazabilidad l�neas PM con los ERP" & vbCrLf
sConsulta = sConsulta & "   IF @SENTIDO = 1 " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Elimina de COPIA_LINEA las l�neas que no est�n en la nueva versi�n" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM COPIA_LINEA" & vbCrLf
sConsulta = sConsulta & "      FROM COPIA_LINEA CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK)  ON CL.CAMPO_PADRE = TD.CAMPO_PADRE " & vbCrLf
sConsulta = sConsulta & "      WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_DESGLOSE TD2 WITH (NOLOCK)  WHERE CL.CAMPO_PADRE=TD2.CAMPO_PADRE AND CL.LINEA = TD2.LINEA_OLD)" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE COPIA_LINEA_DESGLOSE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE COPIA_LINEA_DESGLOSE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   --La Actualizacion del campo ARTICULO, (TIPO_CAMPO_GS = 119) tiene que ver con el DOC. DES_PRT_PM_ ID20 (Cuando unicamente este visible el Material)" & vbCrLf
sConsulta = sConsulta & "   UPDATE COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   SET VALOR_TEXT = " & vbCrLf
sConsulta = sConsulta & "   /*CASE WHEN CD.TIPO_CAMPO_GS <> 119 OR CD.TIPO_CAMPO_GS is NULL THEN  T.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "      ELSE CASE WHEN CD.ANYADIR_ART = 0  and ccb.visible = 0 AND NOT EXISTS (SELECT ART4.COD FROM ART4            " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN  #TEMPORAL_DESGLOSE TMAT  ON TMAT.CAMPO_PADRE = (T.CAMPO_PADRE) AND TMAT.LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN1 ON GMN1.id = 80" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN2 ON GMN2.id = 90" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN3 ON GMN3.id = 100" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN4 ON GMN4.id = 110" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN COPIA_CAMPO CC WITH (NOLOCK)  ON CC.ID = TMAT.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN COPIA_CAMPO_DEF CcD2 WITH (NOLOCK)  ON CC.COPIA_CAMPO_DEF=CcD2.ID" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 WITH (NOLOCK)  ON CCB2.CAMPO = CC.COPIA_CAMPO_DEF AND CCB2.ROL=@ROL AND CCB2.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "              WHERE  CCB2.ORDEN = (CCB.ORDEN - 1)" & vbCrLf
sConsulta = sConsulta & "                      and COD = T.VALOR_TEXT COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                      AND ART4.GMN1=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                      AND ART4.GMN2=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD, GMN2.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                      AND ART4.GMN3=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD, GMN3.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                      AND ART4.GMN4=LTRIM(RTRIM(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.LONGITUD+GMN2.LONGITUD+GMN3.LONGITUD, GMN4.LONGITUD))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                      and CCD2.TIPO_CAMPO_GS= 103) THEN   NULL" & vbCrLf
sConsulta = sConsulta & "   ELSE*/  T.VALOR_TEXT  /*END  END*/" & vbCrLf
sConsulta = sConsulta & "   , VALOR_NUM = T.VALOR_NUM,VALOR_FEC = T.VALOR_FEC,VALOR_BOOL = T.VALOR_BOOL,LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE T  WITH (NOLOCK) ON CLD.CAMPO_PADRE = T.CAMPO_PADRE AND CLD.CAMPO_HIJO = T.CAMPO_HIJO AND CLD.LINEA = T.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH (NOLOCK)  ON T.CAMPO_HIJO=C.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND (ESCRITURA=1 OR (ESCRITURA=0 AND FORMULA IS NOT NULL)  OR (VISIBLE = 0 AND( CD.TIPO_CAMPO_GS=119 or  CD.TIPO_CAMPO_GS=103  or  CD.TIPO_CAMPO_GS=105))  OR (ESCRITURA=0 AND CD.TIPO_CAMPO_GS=118))" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   UPDATE COPIA_LINEA_DESGLOSE_ADJUN SET LINEA = TDA.LINEA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE_ADJUN CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE_ADJUN TDA WITH (NOLOCK)  ON CLD.CAMPO_PADRE = TDA.CAMPO_PADRE AND CLD.CAMPO_HIJO = TDA.CAMPO_HIJO AND ((CLD.ADJUN = TDA.ADJUN) OR (CLD.ID = TDA.ADJUN))" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE COPIA_LINEA_DESGLOSE CHECK CONSTRAINT ALL ALTER TABLE COPIA_LINEA_DESGLOSE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   --Trazabilidad l�neas PM con los ERP" & vbCrLf
sConsulta = sConsulta & "   IF @SENTIDO = 1" & vbCrLf
sConsulta = sConsulta & "   --Integraci�n con ERP: Trazabilidad l�neas solicitudes " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      ALTER TABLE COPIA_LINEA NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      UPDATE COPIA_LINEA SET LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "      FROM COPIA_LINEA CL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN #TEMPORAL_DESGLOSE T WITH (NOLOCK)  ON CL.CAMPO_PADRE = T.CAMPO_PADRE AND CL.LINEA = T.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      ALTER TABLE COPIA_LINEA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   SELECT T.LINEA, T.CAMPO_PADRE, T.CAMPO_HIJO, T.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "   ,/*CASE WHEN CD.TIPO_CAMPO_GS <> 119 OR CD.TIPO_CAMPO_GS is NULL THEN  T.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "     ELSE CASE WHEN CD.ANYADIR_ART = 0  AND CCB.VISIBLE = 0 AND NOT EXISTS (SELECT ART4.COD FROM ART4" & vbCrLf
sConsulta = sConsulta & "              INNER  JOIN #TEMPORAL_DESGLOSE TMAT  ON TMAT.CAMPO_PADRE = (T.CAMPO_PADRE) AND TMAT.LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN1 ON GMN1.id = 80" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN2 ON GMN2.id = 90" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN3 ON GMN3.id = 100" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN DIC GMN4 ON GMN4.id = 110" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CC.ID = TMAT.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN COPIA_CAMPO_DEF CCD2 WITH (NOLOCK)  ON CC.COPIA_CAMPO_DEF=CcD2.ID" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB2 WITH (NOLOCK)  ON CCB2.CAMPO = CC.COPIA_CAMPO_DEF AND CCB2.ROL=@ROL AND CCB2.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "              WHERE  CCB2.ORDEN = (CCB.ORDEN-1) " & vbCrLf
sConsulta = sConsulta & "                  AND COD = T.VALOR_TEXT COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                      AND ART4.GMN1=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1, GMN1.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                      AND ART4.GMN2=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud, GMN2.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                      AND ART4.GMN3=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud+GMN2.longitud, GMN3.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS " & vbCrLf
sConsulta = sConsulta & "                      AND ART4.GMN4=LTRIM(rtrim(SUBSTRING(TMAT.VALOR_TEXT, 1+GMN1.longitud+GMN2.longitud+GMN3.longitud, GMN4.longitud))) COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf
sConsulta = sConsulta & "                      AND CCD2.TIPO_CAMPO_GS= 103 ) then" & vbCrLf
sConsulta = sConsulta & "              NULL" & vbCrLf
sConsulta = sConsulta & "      ELSE*/  T.VALOR_TEXT  /*END END */" & vbCrLf
sConsulta = sConsulta & "   , T.VALOR_FEC, T.VALOR_BOOL " & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_DESGLOSE T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH (NOLOCK)  ON T.CAMPO_HIJO=C.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT * FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK)  WHERE T.LINEA=CLD.LINEA AND T.CAMPO_PADRE = CLD.CAMPO_PADRE AND T.CAMPO_HIJO = CLD.CAMPO_HIJO)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT T.CAMPO_PADRE, T.CAMPO_HIJO,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, GETDATE()" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                  ON CA.ID = CLDA.ADJUN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN #TEMP_CAMPO_ADJUN TC WITH (NOLOCK)  ON TC.ADJUN_NEW = CLDA.ID" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON T.ADJUN = TC.ADJUN_OLD" & vbCrLf
sConsulta = sConsulta & "                 AND T.CAMPO_PADRE = CLDA.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "                 AND T.CAMPO_HIJO = CLDA.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT * FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA2 WITH (NOLOCK)  WHERE T.CAMPO_PADRE = CLDA2.CAMPO_PADRE AND T.CAMPO_HIJO = CLDA2.CAMPO_HIJO AND T.LINEA = CLDA2.LINEA) AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA,ADJUN, NOM, IDIOMA, DATASIZE, COMENT, FECALTA, PROVE)" & vbCrLf
sConsulta = sConsulta & "   SELECT T.CAMPO_PADRE, T.CAMPO_HIJO, T.LINEA, CA.ID, CA.NOM, 'SPA', CCA.DATASIZE, CA.COMENT, CA.FECALTA, CA.PROVE" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN P_COPIA_LINEA_DESGLOSE_ADJUN CCA WITH (NOLOCK) ON  CA.ADJUN_PORTAL = CCA.ADJUN_PORTAL AND CA.ADJUN_ORIGEN = CCA.id AND CCA.PROVE=CA.PROVE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) ON CCA.ADJUN_PORTAL = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 2 AND T.PORTAL = 1 and ca.portal = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT T.CAMPO_PADRE, T.CAMPO_HIJO,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, GETDATE()" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) ON CA.ID = T.ADJUN " & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 2   AND T.PORTAL <> 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Trazabilidad l�neas PM con los ERP" & vbCrLf
sConsulta = sConsulta & "   IF @SENTIDO = 1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO COPIA_LINEA (INSTANCIA, NUM_VERSION, LINEA, CAMPO_PADRE)" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT @ID, @NUM_VERSION, T.LINEA, T.CAMPO_PADRE " & vbCrLf
sConsulta = sConsulta & "      FROM #TEMPORAL_DESGLOSE T WITH (NOLOCK)  WHERE NOT EXISTS (SELECT * FROM COPIA_LINEA CL WITH (NOLOCK)  WHERE T.LINEA=CL.LINEA AND T.CAMPO_PADRE = CL.CAMPO_PADRE)" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   --  Guarda los participantes que haya que poner en esta etapa" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL " & vbCrLf
sConsulta = sConsulta & "   SET PER=CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CASE WHEN T.PER='' THEN NULL ELSE T.PER END ELSE U.FSWS_SUSTITUCION END, " & vbCrLf
sConsulta = sConsulta & "       PROVE=CASE WHEN T.PROVE=''  THEN NULL ELSE T.PROVE END," & vbCrLf
sConsulta = sConsulta & "       CON=CASE WHEN T.CON=0  THEN NULL ELSE T.CON END" & vbCrLf
sConsulta = sConsulta & "      FROM #TEMPORAL_PARTICIPANTES T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      left join USU U WITH (NOLOCK) on T.PER = U.COD" & vbCrLf
sConsulta = sConsulta & "      inner join PM_COPIA_ROL PCR WITH (NOLOCK) on PCR.ID = T.ROL" & vbCrLf
sConsulta = sConsulta & "      inner join INSTANCIA WITH (NOLOCK)  ON PCR.INSTANCIA=INSTANCIA.ID AND (INSTANCIA.ESTADO=0 or INSTANCIA.ESTADO=2 or INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Por �ltimo comprueba si se est� generando por primera vez la instancia y si es as� inserta en INSTANCIA_BLOQUE el bloque del peticionario:" & vbCrLf
sConsulta = sConsulta & "   --IF (SELECT COUNT(*) FROM INSTANCIA_BLOQUE I INNER JOIN PM_COPIA_BLOQUE CB ON I.BLOQUE=CB.ID WHERE I.INSTANCIA=@ID AND I.BLOQUE=@BLOQUE AND CB.TIPO=1 AND I.ESTADO=1)=0" & vbCrLf
sConsulta = sConsulta & "   --  INSERT INTO INSTANCIA_BLOQUE(INSTANCIA,BLOQUE,ESTADO)  " & vbCrLf
sConsulta = sConsulta & "   --     SELECT @ID,PM_COPIA_BLOQUE.ID , 1 FROM PM_COPIA_BLOQUE WHERE INSTANCIA=@ID AND TIPO=1" & vbCrLf
sConsulta = sConsulta & "   --Actualizar la tabla PM_COPIA_ROL, para establecer roles mediante campos" & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Comprador o usuario" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "   SET PER = CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CC.VALOR_TEXT ELSE U.FSWS_SUSTITUCION END" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   left join USU U WITH (NOLOCK) on CC.VALOR_TEXT = U.COD" & vbCrLf
sConsulta = sConsulta & "   inner join PM_COPIA_ROL PCR WITH (NOLOCK) on PCR.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   inner join INSTANCIA WITH (NOLOCK)  ON PCR.INSTANCIA=INSTANCIA.ID AND (INSTANCIA.ESTADO=0 or INSTANCIA.ESTADO=2 or INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "   WHERE (PCR.TIPO = 1 OR PCR.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "      AND PCR.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND PCR.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND PCR.BLOQUE IN (SELECT BLOQUE" & vbCrLf
sConsulta = sConsulta & "                      FROM INSTANCIA_BLOQUE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                      WHERE ESTADO = 1" & vbCrLf
sConsulta = sConsulta & "                      AND INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "      AND PCR.COMO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND CC.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Proveedor" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "   SET PROVE = CC.VALOR_TEXT, @PROVE = CC.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE PM_COPIA_ROL.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PM_COPIA_ROL.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND PM_COPIA_ROL.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND PM_COPIA_ROL.BLOQUE IN (SELECT BLOQUE" & vbCrLf
sConsulta = sConsulta & "                      FROM INSTANCIA_BLOQUE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                      WHERE ESTADO = 1" & vbCrLf
sConsulta = sConsulta & "                      AND INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "      AND PM_COPIA_ROL.COMO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND PM_COPIA_ROL.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "      AND CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND CC.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Devuelve el id de la acci�n de copia_campo: " & vbCrLf
sConsulta = sConsulta & "   IF @ACCION<>0  --Si no est� guardando:" & vbCrLf
sConsulta = sConsulta & "   SET @ACCION=(SELECT PM_COPIA_ACCIONES.ID FROM PM_COPIA_ACCIONES  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK) ON PM_COPIA_ACCIONES.BLOQUE=B.ID WHERE B.INSTANCIA=@ID AND  ACCION_ORIGEN=@ACCION)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--HHF 31/10/2007 FIN" & vbCrLf
sConsulta = sConsulta & "--JGA 10/12/2007 INI" & vbCrLf
sConsulta = sConsulta & "DECLARE @TITULO_ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_CAMPO_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TITULO_TEXT VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TITULO_FEC DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBTIPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FEC_NECESIDAD DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIOMA VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtener el IDIOMA de PER_MODIF" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA = (SELECT TOP 1 IDIOMA FROM USU WHERE PER = @PETICIONARIO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Buscar si alg�n campo de la instancia es de fecha de necesidad" & vbCrLf
sConsulta = sConsulta & "SET @FEC_NECESIDAD = (SELECT TOP 1 VALOR_FEC FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID WHERE TIPO_CAMPO_GS = 5 And INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "--Diferenciar casos en funci�n de la existencia de t�tulo" & vbCrLf
sConsulta = sConsulta & "SET (SELECT DISTINCT @TITULO_ID = CCD.ID, @TIPO_CAMPO_GS = CCD.TIPO_CAMPO_GS, @VALOR_TEXT = CC.VALOR_TEXT, @SUBTIPO = CCD.SUBTIPO FROM COPIA_CAMPO CC  WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID WHERE TITULO = 1 AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "IF @TITULO_ID IS NOT NULL  -- Si existe T�tulo" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO_CAMPO_GS IS NOT NULL -- Si el T�tulo es un campo de tipo GS" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 100 -- Tipo Proveedor" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PR.COD + ' - ' + PR.DEN FROM COPIA_CAMPO CC  WITH (NOLOCK) INNER JOIN PROVE PR  WITH (NOLOCK) ON CC.VALOR_TEXT = PR.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION )" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 102 -- Tipo Moneda" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT M.DEN_SPA FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN MON M WITH (NOLOCK) ON CC.VALOR_TEXT = M.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT M.DEN_ENG FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN MON M WITH (NOLOCK) ON CC.VALOR_TEXT = M.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT M.DEN_GER FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN MON M WITH (NOLOCK) ON CC.VALOR_TEXT = M.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 107 -- Tipo Pais" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PA.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN PAI PA WITH (NOLOCK) ON CC.VALOR_TEXT = PA.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 108 -- Tipo Provincia" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PRO.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN PROVI PRO WITH (NOLOCK) ON CC.VALOR_TEXT = PRO.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 109 -- Tipo Dest" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT D.DEN_SPA FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEST D WITH (NOLOCK) ON CC.VALOR_TEXT = D.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT D.DEN_ENG FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEST D WITH (NOLOCK) ON CC.VALOR_TEXT = D.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT D.DEN_GER FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEST D WITH (NOLOCK) ON CC.VALOR_TEXT = D.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 115 -- Tipo Persona" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PE.NOM + ' ' + PE.APE FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN PER PE WITH (NOLOCK) ON CC.VALOR_TEXT = PE.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 122 -- Tipo Departamento" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT DE.COD + ' - ' + DE.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEP DE WITH (NOLOCK) ON CC.VALOR_TEXT = DE.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end      " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 123 -- Tipo Org Compras" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT O.COD + ' - ' + O.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN ORGCOMPRAS O WITH (NOLOCK) ON CC.VALOR_TEXT = O.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 124 -- Tipo Centro" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT CE.COD + ' - ' + CE.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN CENTROS CE WITH (NOLOCK) ON CC.VALOR_TEXT = CE.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 125 -- Tipo Almacen" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT AL.COD + ' - ' + AL.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN ALMACEN AL WITH (NOLOCK) ON CC.VALOR_TEXT = AL.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS IN (5,6,7) -- FecNecesidad, IniSuministro, FinSuministro" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_FEC = (SELECT CC.VALOR_FEC FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 103 -- Tipo Material" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT CASE WHEN CC.VALOR_TEXT IS NOT NULL THEN  REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') + ' - ' +  (SELECT DEN_SPA FROM GMN4 WITH (NOLOCK)  WHERE GMN1=  REPLACE   (SUBSTRING(CC.VALOR_TEXT,1,(SELECT LONGITUD FROM DIC WHERE NOMBRE= 'GMN1')) ,' ' ,'') AND GMN2=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN2')),' ' ,'') AND GMN3=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN3')),' ','') AND COD=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1, ("
sConsulta = sConsulta & "   SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') ) ELSE CC.VALOR_TEXT END FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT CASE WHEN CC.VALOR_TEXT IS NOT NULL THEN  REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') + ' - ' +  (SELECT DEN_ENG FROM GMN4 WITH (NOLOCK)  WHERE GMN1=  REPLACE   (SUBSTRING(CC.VALOR_TEXT,1,(SELECT LONGITUD FROM DIC WHERE NOMBRE= 'GMN1')) ,' ' ,'') AND GMN2=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN2')),' ' ,'') AND GMN3=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN3')),' ','') AND COD=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1, ("
sConsulta = sConsulta & "   SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') ) ELSE CC.VALOR_TEXT END FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT CASE WHEN CC.VALOR_TEXT IS NOT NULL THEN  REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') + ' - ' +  (SELECT DEN_GER FROM GMN4 WITH (NOLOCK)  WHERE GMN1=  REPLACE   (SUBSTRING(CC.VALOR_TEXT,1,(SELECT LONGITUD FROM DIC WHERE NOMBRE= 'GMN1')) ,' ' ,'') AND GMN2=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN2')),' ' ,'') AND GMN3=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN3')),' ','') AND COD=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1, ("
sConsulta = sConsulta & "   SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') ) ELSE CC.VALOR_TEXT END FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE  -- En Cualquier otro caso" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "      end end end end end end end end end end end end" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   ELSE  -- TIPO_CAMPO_GS es NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      IF @SUBTIPO = 3 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_FEC = (SELECT CC.VALOR_FEC FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)     " & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = @VALOR_TEXT           " & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "ELSE  -- Sin Campo T�tulo   " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT TOP 1 VALOR_TEXT FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID WHERE TIPO_CAMPO_GS = 1 And INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   IF @TITULO_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "     SET @TITULO_TEXT = (SELECT DEN_SPA FROM SOLICITUD S INNER JOIN INSTANCIA I ON S.ID = I.SOLICITUD WHERE I.ID = @ID)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "IF @OLD_VERSION = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Inserci�n en la tabla" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO INSTANCIA_VISORES_PM (INSTANCIA, TITULO_TEXT, TITULO_FEC, FEC_NECESIDAD) VALUES (@ID, @TITULO_TEXT, @TITULO_FEC, @FEC_NECESIDAD)" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Actualizaci�n del registro" & vbCrLf
sConsulta = sConsulta & "   UPDATE INSTANCIA_VISORES_PM SET TITULO_TEXT = @TITULO_TEXT, TITULO_FEC = @TITULO_FEC, FEC_NECESIDAD = @FEC_NECESIDAD WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "--JGA 10/12/2007 END" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
sConsulta = sConsulta & "   SET @ERRORVAR = @@ERROR" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Private Sub V_31600_Storeds_44_2()
Dim sConsulta As String


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SAVE_VERSION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SAVE_VERSION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_SAVE_VERSION @INSTANCIA INT , @NUM_VERSION INT , @OLD_VERSION INT,@NOCONFORMIDAD INT=NULL,@PROVE VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @REVISOR_OLD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAMOS LOS CAMPOS DEL TIPO FORMULARIO" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_CAMPO SET VALOR_NUM = T.VALOR_NUM, VALOR_TEXT = T.VALOR_TEXT, VALOR_FEC = T.VALOR_FEC, VALOR_BOOL=T.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON C.ID = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAMOS A SUCIO LOS CAMPOS DE VERSIONES ANTERIORES QUE HAN CAMBIADO CON ESTA NUEVA VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_CAMPO SET SUCIO = 1 " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (SELECT * FROM COPIA_CAMPO WITH (NOLOCK)  WHERE INSTANCIA = @INSTANCIA AND NUM_VERSION= @NUM_VERSION) CNEW" & vbCrLf
sConsulta = sConsulta & "                   ON C.COPIA_CAMPO_DEF = CNEW.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "  AND (C.VALOR_NUM<>CNEW.VALOR_NUM OR C.VALOR_FEC <> CNEW.VALOR_FEC OR C.VALOR_TEXT <>CNEW.VALOR_TEXT OR C.VALOR_BOOL <> CNEW.VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ELIMINAMOS LOS ADJUNTOS DE LOS CAMPOS TIPO FORM QUE YA NO EST�N EN ESTA NUEVA VERSI�N" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN " & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN #TEMPORAL T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               ON CCA.CAMPO = T.CAMPO" & vbCrLf
sConsulta = sConsulta & " WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_ADJUN TA WITH (NOLOCK)  WHERE CCA.ID = TA.ADJUN AND TA.TIPO = 1 and ta.portal=0 ) and not exists (SELECT * FROM #TEMPORAL_ADJUN TA WITH (NOLOCK)  WHERE CCA.ADJUN = TA.ADJUN AND TA.TIPO = 1 and ta.portal=1 ) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INSERTAMOS LOS NUEVOS ADJUNTOS DE TIPO FORM (INSERTADOS DESDE EL PM)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT CA.ID, T.CAMPO, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON CA.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 2" & vbCrLf
sConsulta = sConsulta & " AND T.PORTAL = 0" & vbCrLf
sConsulta = sConsulta & "AND CA.PORTAL=0" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "--INSERTAMOS LOS NUEVOS ADJUNTOS DE TIPO FORM (INSERTADOS DESDE EL PORTAL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ESTA PRIMERA INSERT TNEGO QUE REVISARLA. NO TIENE MUCHO SENTIDO. ��INSERTAR ADJUNTOS QUE ESTABAN EN LA INSTANCIA AL SER TRASPASADA AL PORTAL Y QUE UNA VEZ DEVUELTA SIGUEN ESTANDO....PARA QUE???" & vbCrLf
sConsulta = sConsulta & "/*INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, COMENT, FECALTA, PROVE)" & vbCrLf
sConsulta = sConsulta & "SELECT CA.ID, T.CAMPO, CA.NOM, 'SPA', CCA.DATASIZE, CA.COMENT, CA.FECALTA, CA.PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN P_COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "                ON CA.ADJUN_ORIGEN = CCA.ADJUN" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_ADJUN T" & vbCrLf
sConsulta = sConsulta & "                ON CA.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 1 AND T.PORTAL = 1*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ADJUNTOS METIDOS EN EL PORTAL POR EL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, COMENT, FECALTA, PROVE)" & vbCrLf
sConsulta = sConsulta & "SELECT CA.ID, T.CAMPO, CA.NOM, 'SPA', CCA.DATASIZE, CA.COMENT, CA.FECALTA, CA.PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN (P_COPIA_CAMPO_ADJUN CCA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                       inner join p_copia_campo c WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                               on cca.campo = c.id" & vbCrLf
sConsulta = sConsulta & "                              and c.instancia = @instancia" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                               ON CCA.ADJUN_PORTAL = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "                ON CA.ADJUN_ORIGEN = CCA.id  AND CA.ADJUN_PORTAL=CCA.ADJUN_PORTAL " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 2 AND T.PORTAL = 1 and ca.portal = 1 AND CA.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- ADJUNTOS DE DESGLOSE... " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- ELIMINAMOS LOS ADJUNTOS QUE HAYAN SIDO ELIMINADOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON CLDA.CAMPO_PADRE = TD.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "                AND CLDA.CAMPO_HIJO = TD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "                AND CLDA.LINEA = TD.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT  * FROM #TEMPORAL_DESGLOSE_ADJUN TDA WITH (NOLOCK)  WHERE CLDA.ID = TDA.ADJUN AND TDA.TIPO = 1  AND TDA.PORTAL = 0) " & vbCrLf
sConsulta = sConsulta & "  AND NOT EXISTS (SELECT  * FROM #TEMPORAL_DESGLOSE_ADJUN TDA WITH (NOLOCK)  WHERE CLDA.ADJUN = TDA.ADJUN AND TDA.TIPO = 1  AND TDA.PORTAL = 1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON CLDA.CAMPO_PADRE = TD.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "                AND CLDA.CAMPO_HIJO = TD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_DESGLOSE TD2 WITH (NOLOCK)  WHERE CLDA.CAMPO_PADRE=TD2.CAMPO_PADRE AND CLDA.CAMPO_HIJO = TD2.CAMPO_HIJO AND CLDA.LINEA = TD2.LINEA_OLD)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON CLD.CAMPO_PADRE = TD.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "                AND CLD.CAMPO_HIJO = TD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & " WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_DESGLOSE TD2 WITH (NOLOCK)  WHERE CLD.CAMPO_PADRE=TD2.CAMPO_PADRE AND CLD.CAMPO_HIJO = TD2.CAMPO_HIJO AND CLD.LINEA = TD2.LINEA_OLD)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COPIA_LINEA_DESGLOSE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COPIA_LINEA_DESGLOSE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   SET VALOR_TEXT = T.VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "       VALOR_NUM = T.VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "       VALOR_FEC = T.VALOR_FEC," & vbCrLf
sConsulta = sConsulta & "       VALOR_BOOL = T.VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "       LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN #TEMPORAL_DESGLOSE T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON CLD.CAMPO_PADRE = T.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "                AND CLD.CAMPO_HIJO = T.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "                AND CLD.LINEA = T.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "   SET LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN #TEMPORAL_DESGLOSE T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               ON CLD.CAMPO_PADRE = T.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "              AND CLD.CAMPO_HIJO = T.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "              AND CLD.LINEA = T.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COPIA_LINEA_DESGLOSE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COPIA_LINEA_DESGLOSE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT T.LINEA, T.CAMPO_PADRE, T.CAMPO_HIJO, T.VALOR_NUM, T.VALOR_TEXT, T.VALOR_FEC, T.VALOR_BOOL " & vbCrLf
sConsulta = sConsulta & "  FROM #TEMPORAL_DESGLOSE T WITH (NOLOCK)  WHERE NOT EXISTS (SELECT * FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK)  WHERE T.LINEA=CLD.LINEA AND T.CAMPO_PADRE = CLD.CAMPO_PADRE AND T.CAMPO_HIJO = CLD.CAMPO_HIJO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT T.CAMPO_PADRE, T.CAMPO_HIJO,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON CA.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 2 AND T.PORTAL=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT T.CAMPO_PADRE, T.CAMPO_HIJO,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               ON CA.ID = CLDA.ADJUN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               ON T.ADJUN = CLDA.ID" & vbCrLf
sConsulta = sConsulta & "              AND T.CAMPO_PADRE = CLDA.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "              AND T.CAMPO_HIJO = CLDA.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT * FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA2 WITH (NOLOCK)  WHERE T.CAMPO_PADRE = CLDA2.CAMPO_PADRE AND T.CAMPO_HIJO = CLDA2.CAMPO_HIJO AND T.LINEA = CLDA2.LINEA AND T.ADJUN=CLDA2.ID) AND CA.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA,ADJUN, NOM, IDIOMA, DATASIZE, COMENT, FECALTA, PROVE)" & vbCrLf
sConsulta = sConsulta & "SELECT T.CAMPO_PADRE, T.CAMPO_HIJO, T.LINEA, CA.ID, CA.NOM, 'SPA', CCA.DATASIZE, CA.COMENT, CA.FECALTA, CA.PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN P_COPIA_LINEA_DESGLOSE_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "                ON CA.ADJUN_ORIGEN = CCA.ADJUN" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T" & vbCrLf
sConsulta = sConsulta & "                ON CA.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 1 AND T.PORTAL = 1*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA,ADJUN, NOM, IDIOMA, DATASIZE, COMENT, FECALTA, PROVE)" & vbCrLf
sConsulta = sConsulta & "SELECT T.CAMPO_PADRE, T.CAMPO_HIJO, T.LINEA, CA.ID, CA.NOM, 'SPA', CCA.DATASIZE, CA.COMENT, CA.FECALTA, CA.PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN P_COPIA_LINEA_DESGLOSE_ADJUN CCA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON CA.ADJUN_ORIGEN = CCA.id  AND CA.ADJUN_PORTAL = CCA.ADJUN AND CA.PROVE=CCA.PROVE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON CCA.ADJUN_PORTAL = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 2 AND T.PORTAL = 1 and ca.portal = 1 AND CA.PROVE=@PROVE  AND CA.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NOCONFORMIDAD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   UPDATE NOCONF_SOLICIT_ACC SET SOLICITAR = T.SOLICITAR, FECHA_LIMITE = T.FECHA_LIMITE" & vbCrLf
sConsulta = sConsulta & "   FROM NOCONF_SOLICIT_ACC NC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_NOCONF_SOLICIT_ACC T WITH (NOLOCK)  ON NC.COPIA_CAMPO=T.COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--MODIFICADO PARA Q TB ACTUALICE EL REVISOR" & vbCrLf
sConsulta = sConsulta & "  IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    SET @REVISOR_OLD = (SELECT REVISOR FROM NOCONFORMIDAD WHERE NOCONFORMIDAD.ID=@NOCONFORMIDAD) --GUARDAMOS EL REVISOR VIEJO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    UPDATE NOCONFORMIDAD SET REVISOR= T.REVISOR, FEC_LIM_RESOL= CONVERT(DATETIME,T.FEC_LIM_RESOL,110) FROM NOCONFORMIDAD N WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN #TEMPORAL_NOCONFORMIDAD T WITH (NOLOCK)  ON N.ID=T.ID  WHERE N.ID=@NOCONFORMIDAD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     --como se ha modificado el revisor puede ser q tb haga falta modificar el campo accion de la tabla INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "    IF @REVISOR_OLD<> (SELECT REVISOR FROM #TEMPORAL_NOCONFORMIDAD WHERE ID=@NOCONFORMIDAD)" & vbCrLf
sConsulta = sConsulta & "     UPDATE INSTANCIA_EST SET ACCION=1 WHERE INSTANCIA=@INSTANCIA AND (ACCION=9 OR ACCION=10)" & vbCrLf
sConsulta = sConsulta & "  end  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                DELETE FROM NOCONF_ACC WHERE INSTANCIA= @INSTANCIA AND NOCONFORMIDAD = @NOCONFORMIDAD AND  VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO NOCONF_ACC (CAMPO_PADRE,  LINEA, NOCONFORMIDAD, VERSION, INSTANCIA, ESTADO, ESTADO_INT, COMENT)" & vbCrLf
sConsulta = sConsulta & "      SELECT CAMPO_PADRE, LINEA, @NOCONFORMIDAD, @NUM_VERSION,  @INSTANCIA, ESTADO , ESTADO_INT, COMENT " & vbCrLf
sConsulta = sConsulta & "      FROM #TEMPORAL_NOCONF_ACC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_CREATE_NEW_INSTANCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_CREATE_NEW_INSTANCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_CREATE_NEW_INSTANCE @SOLICITUD INT, @PETICIONARIO VARCHAR(50), @COMPRADOR VARCHAR(50) = NULL, @MON VARCHAR(50), @ENVIAR TINYINT =0, @ID INT OUTPUT, @IMPORTE FLOAT =NULL, @PEDIDO_AUT TINYINT=0, @ESTADO INT=0 OUTPUT,@COMENT_ALTA NVARCHAR(500)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS2 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS3 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS4 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FORMULARIO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @WORKFLOW INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTE_PEDIDO_DIRECTO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CUMP_PROVE TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PUB TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_SOLICIT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CERTIF INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_NEW INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHA DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FORMULARIO = FORMULARIO, @WORKFLOW = WORKFLOW, @IMPORTE_PEDIDO_DIRECTO = PEDIDO_DIRECTO , @CUMP_PROVE=CUMP_PROVE " & vbCrLf
sConsulta = sConsulta & "FROM SOLICITUD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "WHERE ID = @SOLICITUD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECHA=GETDATE()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO INSTANCIA (SOLICITUD, ESTADO, FEC_ALTA, COMPRADOR, PETICIONARIO, MON, CAMBIO, PEDIDO_AUT, IMPORTE, PEDIDO_DIRECTO, CUMP_PROVE)" & vbCrLf
sConsulta = sConsulta & "SELECT @SOLICITUD, 0,@FECHA, @COMPRADOR, @PETICIONARIO, @MON , EQUIV , @PEDIDO_AUT, @IMPORTE, @IMPORTE_PEDIDO_DIRECTO , @CUMP_PROVE " & vbCrLf
sConsulta = sConsulta & "FROM MON WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID = (SELECT MAX(ID) " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA " & vbCrLf
sConsulta = sConsulta & "WHERE FEC_ALTA=@FECHA AND PETICIONARIO=@PETICIONARIO AND SOLICITUD=@SOLICITUD)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE  CIDIOMA CURSOR LOCAL FOR SELECT COD FROM IDIOMAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "SET @IDIS=N''" & vbCrLf
sConsulta = sConsulta & "SET @IDIS2=N''" & vbCrLf
sConsulta = sConsulta & "SET @IDIS3=N''" & vbCrLf
sConsulta = sConsulta & "SET @IDIS4=N''" & vbCrLf
sConsulta = sConsulta & "OPEN CIDIOMA " & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CIDIOMA INTO @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @IDIS = @IDIS + N' A.DEN_' + @COD + ', '" & vbCrLf
sConsulta = sConsulta & "    SET @IDIS2 = @IDIS2 + N' A.AYUDA_' + @COD + ', '" & vbCrLf
sConsulta = sConsulta & "    SET @IDIS3 = @IDIS3 + N' CVL.VALOR_TEXT_' + @COD + ', '" & vbCrLf
sConsulta = sConsulta & "    SET @IDIS4 = @IDIS4 + N' VALOR_TEXT_' + @COD + ', '" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM CIDIOMA INTO @COD" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE CIDIOMA" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CIDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO COPIA_GRUPO (INSTANCIA, ' + @IDIS + ' ORDEN, GRUPO_ORIGEN) SELECT @ID,' + @IDIS + ' ORDEN, A.ID FROM FORM_GRUPO A  WITH (NOLOCK) WHERE FORMULARIO = @FORMULARIO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @FORMULARIO INT', @ID=@ID, @FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO COPIA_CAMPO_DEF (GRUPO, ' + @IDIS + ' ' + @IDIS2 + ' TIPO, SUBTIPO, INTRO, MINNUM, MAXNUM, MINFEC, MAXFEC, ORDEN, ES_SUBCAMPO, TIPO_CAMPO_GS, ID_ATRIB_GS, GMN1, GMN2, GMN3, GMN4, LINEAS_PRECONF, ID_CALCULO, FORMULA, ORDEN_CALCULO, IMPORTE_WORKFL, ORIGEN_CALC_DESGLOSE, CAMPO_ORIGEN,POPUP,ANYADIR_ART,titulo,IDREFSOLICITUD,AVISOBLOQUEOIMPACUM,CARGAR_ULT_ADJ) " & vbCrLf
sConsulta = sConsulta & "             SELECT CG.ID, ' + @IDIS + ' ' + @IDIS2 + ' A.TIPO, A.SUBTIPO, A.INTRO, A.MINNUM, A.MAXNUM, A.MINFEC, A.MAXFEC, A.ORDEN, A.ES_SUBCAMPO, A.TIPO_CAMPO_GS, A.ID_ATRIB_GS, A.GMN1, A.GMN2, A.GMN3, A.GMN4, A.LINEAS_PRECONF, A.ID_CALCULO, A.FORMULA, A.ORDEN_CALCULO, A.IMPORTE_WORKFL, A.ORIGEN_CALC_DESGLOSE, A.ID , A.POPUP,A.ANYADIR_ART, a.titulo,A.IDREFSOLICITUD,A.AVISOBLOQUEOIMPACUM,A.CARGAR_ULT_ADJ" & vbCrLf
sConsulta = sConsulta & "               FROM FORM_CAMPO A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN FORM_GRUPO FG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                              ON A.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                              ON FG.ID = CG.GRUPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "              WHERE FG.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "                AND CG.INSTANCIA = @ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @FORMULARIO INT', @ID=@ID, @FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO COPIA_CAMPO_VALOR_LISTA (CAMPO_DEF, ORDEN, VALOR_NUM, ' + @IDIS4 + ' VALOR_FEC) " & vbCrLf
sConsulta = sConsulta & "             SELECT CCD.ID, CVL.ORDEN, CVL.VALOR_NUM, ' + @IDIS3 + ' CVL.VALOR_FEC" & vbCrLf
sConsulta = sConsulta & "               FROM COPIA_CAMPO_DEF CCD WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN FORM_CAMPO A WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              ON CCD.CAMPO_ORIGEN = A.ID" & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN CAMPO_VALOR_LISTA CVL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              ON A.ID = CVL.CAMPO" & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN FORM_GRUPO FG  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              ON A.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN COPIA_GRUPO CG  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              ON FG.ID = CG.GRUPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "                             AND CCD.GRUPO = CG.ID" & vbCrLf
sConsulta = sConsulta & "              WHERE FG.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "                AND CG.INSTANCIA = @ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @FORMULARIO INT', @ID=@ID, @FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_CAMPO_DEF SET ORIGEN_CALC_DESGLOSE = CCD2.ID" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_DEF CCD  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN (SELECT CCD.* FROM COPIA_CAMPO_DEF CCD WITH (NOLOCK) INNER JOIN COPIA_GRUPO CG  WITH (NOLOCK) ON CG.ID = CCD.GRUPO WHERE CG.INSTANCIA = @ID) CCD2" & vbCrLf
sConsulta = sConsulta & "              ON CCD.ORIGEN_CALC_DESGLOSE = CCD2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN COPIA_GRUPO CG WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON CCD.GRUPO = CG.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CG.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_PASO (INSTANCIA, DEN_BLOQUE, LIMITE, ORDEN, PASO_ORIGEN) SELECT @ID, BS.DEN, P.LIMITE, P.ORDEN, P.ID FROM PASO P WITH (NOLOCK) INNER JOIN BLOQUE_SEGURIDAD BS WITH (NOLOCK) ON P.BLOQUE_SEGURIDAD = BS.ID WHERE WORKFLOW = @WORKFLOW" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_SUBPASO (PASO, PER, LIMITE, ORDEN, UON1, UON2, UON3, DEP, PER_ORIGINAL) " & vbCrLf
sConsulta = sConsulta & "SELECT CP.ID, A.PER, A.LIMITE, A.ORDEN , A.UON1, A.UON2, A.UON3, A.DEP, A.PER" & vbCrLf
sConsulta = sConsulta & "  FROM APROBADORES A  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PASO P WITH (NOLOCK) ON P.BLOQUE_SEGURIDAD = A.BLOQUE_SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN COPIA_PASO CP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "             ON P.ID = CP.PASO_ORIGEN" & vbCrLf
sConsulta = sConsulta & " WHERE P.WORKFLOW = @WORKFLOW AND CP.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_PASO_NOTIF (PASO,PER) SELECT COPIA_PASO.ID,PASO_NOTIF.PER FROM COPIA_PASO  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PASO_NOTIF  WITH (NOLOCK) ON COPIA_PASO.PASO_ORIGEN=PASO_NOTIF.PASO WHERE COPIA_PASO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CONF_CUMPLIMENTACION (INSTANCIA, COPIA_CAMPO, COPIA_PASO, SOLIC_PROVE, VISIBLE, ESCRITURA, OBLIGATORIO, ORDEN,ES_ESTADO) " & vbCrLf
sConsulta = sConsulta & "SELECT @ID,  CC2.ID COPIA_CAMPO, CP.ID COPIA_PASO, CC.SOLIC_PROVE, CC.VISIBLE, CC.ESCRITURA, CC.OBLIGATORIO, CC.ORDEN,CC.ES_ESTADO" & vbCrLf
sConsulta = sConsulta & "  FROM CONF_CUMPLIMENTACION CC  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (PASO P  WITH (NOLOCK) INNER JOIN COPIA_PASO CP  WITH (NOLOCK) ON CP.PASO_ORIGEN = P.ID AND CP.INSTANCIA=@ID) " & vbCrLf
sConsulta = sConsulta & "             ON  CC.PASO = P.ID  " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN (FORM_CAMPO C  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN (COPIA_CAMPO_DEF CC2  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_GRUPO CGP  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                       ON CC2.GRUPO = CGP.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND @ID = CGP.INSTANCIA ) " & vbCrLf
sConsulta = sConsulta & "                              ON CC2.CAMPO_ORIGEN = C.ID ) " & vbCrLf
sConsulta = sConsulta & "             ON  CC.CAMPO = C.ID   " & vbCrLf
sConsulta = sConsulta & " WHERE SOLICITUD = @SOLICITUD" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM_VERSION INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET @NUM_VERSION = (SELECT ISNULL(MAX(NUM_VERSION),0) + 1 FROM COPIA_CAMPO WHERE INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "SET @NUM_VERSION=1" & vbCrLf
sConsulta = sConsulta & "INSERT INTO VERSION_INSTANCIA(INSTANCIA,NUM_VERSION, PER_MODIF, FECHA_MODIF)" & vbCrLf
sConsulta = sConsulta & " VALUES (@ID, @NUM_VERSION, @PETICIONARIO,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CAMPO (INSTANCIA, NUM_VERSION, SUCIO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, COPIA_CAMPO_DEF)" & vbCrLf
sConsulta = sConsulta & "SELECT @ID, @NUM_VERSION, 0, T.VALOR_NUM, T.VALOR_TEXT, T.VALOR_FEC, T.VALOR_BOOL, CCD.ID " & vbCrLf
sConsulta = sConsulta & "  FROM FORM_CAMPO FC  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN #TEMPORAL T  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           ON FC.ID  = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (COPIA_CAMPO_DEF CCD  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCD.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                 AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "            ON FC.ID = CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT CA.ID, CC.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                ON CA.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN (COPIA_CAMPO CC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                ON CC.COPIA_CAMPO_DEF = CCD.ID)" & vbCrLf
sConsulta = sConsulta & "                ON CCD.CAMPO_ORIGEN = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "  AND T.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXID INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MAXID= (SELECT ISNULL(MAX(ID),0) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, PER, COMENT,ADJUN_ORIGEN)                " & vbCrLf
sConsulta = sConsulta & " SELECT A.DATA, GETDATE(), A.NOM, @PETICIONARIO, A.COMENT, A.ID" & vbCrLf
sConsulta = sConsulta & "   FROM CAMPO_ADJUN A WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 ON A.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & " AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" & vbCrLf
sConsulta = sConsulta & "SELECT CA.ID, CC.ID, CA.NOM, A.IDIOMA, A.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA, A.RUTA" & vbCrLf
sConsulta = sConsulta & "  FROM CAMPO_ADJUN A WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_ADJUN CA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "               AND T.ADJUN = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "               AND CA.ID > @MAXID" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN (COPIA_CAMPO CC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                ON CC.COPIA_CAMPO_DEF = CCD.ID)" & vbCrLf
sConsulta = sConsulta & "                ON CCD.CAMPO_ORIGEN = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "  AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_DESGLOSE (CAMPO_PADRE, CAMPO_HIJO) " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CCP.ID, CCH.ID " & vbCrLf
sConsulta = sConsulta & "  FROM (SELECT D.CAMPO_PADRE, D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "          FROM DESGLOSE D  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN FORM_CAMPO FC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                         ON D.CAMPO_PADRE = FC.ID" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN FORM_GRUPO FG  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                         ON FC.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "         WHERE FG.FORMULARIO = @FORMULARIO) D" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN (COPIA_CAMPO_DEF CCDP  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDP.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDP.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                 AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "                 ON CCDP.CAMPO_ORIGEN = D.CAMPO_PADRE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN (COPIA_CAMPO_DEF CCDH  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDH.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDH.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                 AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "                 ON CCDH.CAMPO_ORIGEN = D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT  T.LINEA, CCP.ID,CCH.ID, T.VALOR_NUM, T.VALOR_TEXT, T.VALOR_FEC, T.VALOR_BOOL " & vbCrLf
sConsulta = sConsulta & "  FROM #TEMPORAL_DESGLOSE T  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                 AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "            ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                 AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "            ON T.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MAXID= (SELECT ISNULL(MAX(ID),0) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, PER, COMENT,ADJUN_ORIGEN)                " & vbCrLf
sConsulta = sConsulta & " SELECT A.DATA, GETDATE(), A.NOM, @PETICIONARIO, A.COMENT, A.ID" & vbCrLf
sConsulta = sConsulta & "   FROM LINEA_DESGLOSE_ADJUN A WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN (select DISTINCT ADJUN, TIPO FROM #TEMPORAL_DESGLOSE_ADJUN WITH (NOLOCK)) T" & vbCrLf
sConsulta = sConsulta & "                 ON A.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & " AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" & vbCrLf
sConsulta = sConsulta & "SELECT CCP.ID, CCH.ID,  T.LINEA, CA.ID, CA.NOM, 'SPA', A.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA, A.RUTA" & vbCrLf
sConsulta = sConsulta & "  FROM LINEA_DESGLOSE_ADJUN A  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_ADJUN CA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "               AND T.ADJUN = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "               AND CA.ID>@MAXID" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                 AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "            ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                 AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "            ON T.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT CCP.ID, CCH.ID,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_ADJUN CA  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                ON CA.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                 AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "            ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                 AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "            ON T.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE T.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_PASO SET PASO_ORIGEN = NULL WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ESTADO=0" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "--Comprueba el tipo de la solicitud para saber si tiene que emitir un certificado o una no conformidad" & vbCrLf
sConsulta = sConsulta & "SELECT @TIPO_SOLICIT=TP.TIPO FROM SOLICITUD WITH (NOLOCK) INNER JOIN TIPO_SOLICITUDES TP WITH (NOLOCK) ON SOLICITUD.TIPO=TP.ID WHERE SOLICITUD.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "                      " & vbCrLf
sConsulta = sConsulta & "IF @ENVIAR = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT TOP 1 @APROB = SP.ID FROM COPIA_SUBPASO SP WITH (NOLOCK) INNER JOIN COPIA_PASO P  WITH (NOLOCK) ON SP.PASO = P.ID" & vbCrLf
sConsulta = sConsulta & "     WHERE P.INSTANCIA = @ID AND ISNULL(P.LIMITE,@IMPORTE-1)<@IMPORTE AND ISNULL(SP.LIMITE,@IMPORTE-1)<@IMPORTE ORDER BY P.ORDEN, SP.ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @APROB IS NULL" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @PEDIDO_AUT=1 " & vbCrLf
sConsulta = sConsulta & "        begin" & vbCrLf
sConsulta = sConsulta & "            IF @IMPORTE_PEDIDO_DIRECTO>@IMPORTE" & vbCrLf
sConsulta = sConsulta & "              SET @ESTADO = 1 --hay que emitir un pedido" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "             begin" & vbCrLf
sConsulta = sConsulta & "                   IF @TIPO_SOLICIT=2 " & vbCrLf
sConsulta = sConsulta & "                          SET @ESTADO = 200 --Estado de certificado" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "            IF @TIPO_SOLICIT=3 " & vbCrLf
sConsulta = sConsulta & "                             SET @ESTADO = 300 --Estado de No conformidad" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                           SET @ESTADO = 100 --va a compras" & vbCrLf
sConsulta = sConsulta & "                    end" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "          end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        begin" & vbCrLf
sConsulta = sConsulta & "          IF @TIPO_SOLICIT=2 " & vbCrLf
sConsulta = sConsulta & "             SET @ESTADO = 200 --Estado de certificado" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "             begin" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO_SOLICIT=3 " & vbCrLf
sConsulta = sConsulta & "                       SET @ESTADO = 300 --Estado de No conformidad" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                       SET @ESTADO = 100 -- va a compras" & vbCrLf
sConsulta = sConsulta & "             end" & vbCrLf
sConsulta = sConsulta & "         end" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "     SET @ESTADO=2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  --Si es un certificado o es una solicitud k va a compras o pedido directo (y el tipo de solicitud no tiene gestor) entonces emite la solicitud:" & vbCrLf
sConsulta = sConsulta & "  IF @TIPO_SOLICIT=2 OR @TIPO_SOLICIT=3 OR @ESTADO=1 OR (@ESTADO=100 AND (SELECT GESTOR FROM SOLICITUD  WITH (NOLOCK) INNER JOIN INSTANCIA WITH (NOLOCK) ON SOLICITUD.ID=INSTANCIA.SOLICITUD WHERE INSTANCIA.ID=@ID) IS NULL)" & vbCrLf
sConsulta = sConsulta & "        begin" & vbCrLf
sConsulta = sConsulta & "           IF @TIPO_SOLICIT=3  --Es una no conformidad" & vbCrLf
sConsulta = sConsulta & "               INSERT INTO INSTANCIA_EST (INSTANCIA, PER, FECHA, EST, ACCION,COMENT) VALUES (@ID, @PETICIONARIO, GETDATE(), @ESTADO,1,@COMENT_ALTA)" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               INSERT INTO INSTANCIA_EST (INSTANCIA, PER, FECHA, EST, ACCION) VALUES (@ID, @PETICIONARIO, GETDATE(), @ESTADO,1)" & vbCrLf
sConsulta = sConsulta & "           UPDATE INSTANCIA SET APROB = @APROB, ESTADO=@ESTADO WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "        end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO_SOLICIT=2  --Es una solicitud de tipo certificado,se genera el certificado" & vbCrLf
sConsulta = sConsulta & "      Begin" & vbCrLf
sConsulta = sConsulta & "              IF @ESTADO=2" & vbCrLf
sConsulta = sConsulta & "                  SET @PUB=0" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                  SET @PUB=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @PUB=1 --Si se publica la versi�n directamente se pondr�" & vbCrLf
sConsulta = sConsulta & "      UPDATE VERSION_INSTANCIA SET TIPO=2 WHERE INSTANCIA=@ID AND NUM_VERSION=@NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE C1 CURSOR LOCAL FOR SELECT PROVE FROM #TEMPORAL_CERTIFICADO T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   OPEN C1" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C1 INTO @PROVE" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          INSERT INTO CERTIFICADO (PROVE,CONTACTO,PUBLICADA,NUM_VERSION, FEC_PUBLICACION,INSTANCIA,USU,PER,DEP,UON1,UON2,UON3,FEC_DESPUB,FEC_SOLICITUD,TIPO_SOLICITUD) " & vbCrLf
sConsulta = sConsulta & "                SELECT T.PROVE, T.CONTACTO,@PUB,@NUM_VERSION ,CASE WHEN @PUB=1 THEN GETDATE() ELSE NULL END , @ID,USU.COD,PER.COD,PER.DEP,PER.UON1,PER.UON2,PER.UON3" & vbCrLf
sConsulta = sConsulta & "                ,CONVERT(DATETIME,T.FEC_DESPUB ,110), GETDATE(), 2" & vbCrLf
sConsulta = sConsulta & "                FROM #TEMPORAL_CERTIFICADO T WITH (NOLOCK) INNER JOIN PER  WITH (NOLOCK) ON T.PETICIONARIO=PER.COD INNER JOIN USU WITH (NOLOCK) ON PER.COD=USU.PER WHERE T.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         IF  @PUB=1  --Si se publica directamente sin pasar por el workflow actualizamos prove_certificado.Si no se har� cuando la solicitud pase el workflow." & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @ID_CERTIF=NULL" & vbCrLf
sConsulta = sConsulta & "           SELECT @ID_NEW=MAX(ID) FROM CERTIFICADO WHERE PROVE=@PROVE AND INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "           SELECT @ID_CERTIF=ID_CERTIF FROM PROVE_CERTIFICADO  WITH (NOLOCK) WHERE PROVE=@PROVE AND TIPO_CERTIF=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "           IF @ID_CERTIF IS NULL " & vbCrLf
sConsulta = sConsulta & "                  INSERT INTO PROVE_CERTIFICADO(PROVE,TIPO_CERTIF,ID_CERTIF)  VALUES (@PROVE,@SOLICITUD,@ID_NEW)" & vbCrLf
sConsulta = sConsulta & "           ELSE    " & vbCrLf
sConsulta = sConsulta & "               begin" & vbCrLf
sConsulta = sConsulta & "                 UPDATE PROVE_CERTIFICADO SET ID_CERTIF=@ID_NEW WHERE PROVE=@PROVE AND TIPO_CERTIF=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "                 UPDATE CERTIFICADO SET PUBLICADA=0 WHERE ID=@ID_CERTIF" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            --Inserta en la tabla que guarda las versiones de los certificados y las instancias:" & vbCrLf
sConsulta = sConsulta & "            INSERT INTO VERSION_CERTIF(INSTANCIA,NUM_VERSION,CERTIFICADO) VALUES (@ID,@NUM_VERSION,@ID_NEW)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            --Actualiza los contadores de certificados nuevos del portal" & vbCrLf
sConsulta = sConsulta & "            EXEC  FSQA_ACT_REL_CIAS_NEW_CERTIF @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM C1 INTO @PROVE" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "      CLOSE C1" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "      End" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @TIPO_SOLICIT=3  --Es una No conformidad" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "         DECLARE  @EST_NC INT" & vbCrLf
sConsulta = sConsulta & "         IF @ENVIAR = 1" & vbCrLf
sConsulta = sConsulta & "             SET @EST_NC=1" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "   SET @EST_NC=0" & vbCrLf
sConsulta = sConsulta & "              INSERT INTO INSTANCIA_EST (INSTANCIA, PER, FECHA, EST, ACCION,COMENT) VALUES (@ID, @PETICIONARIO, GETDATE(), 301,0,@COMENT_ALTA)" & vbCrLf
sConsulta = sConsulta & "              UPDATE INSTANCIA SET ESTADO=301 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         SET @SQL = N'INSERT INTO COPIA_ESTADOS (INSTANCIA, COD,' + @IDIS + ' NOTIFICAR) SELECT @ID,COD,' + @IDIS + ' NOTIFICAR FROM SOLICITUD_ESTADOS A WITH (NOLOCK) WHERE SOLICITUD = @SOLICITUD'" & vbCrLf
sConsulta = sConsulta & "         EXEC SP_EXECUTESQL @SQL, N'@ID INT, @SOLICITUD INT', @ID=@ID, @SOLICITUD = @SOLICITUD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         INSERT INTO NOCONFORMIDAD(PROVE,CONTACTO,INSTANCIA,VERSION,FEC_LIM_RESOL,ESTADO,FECALTA,USU,PER,DEP,UON1,UON2,UON3, REVISOR)" & vbCrLf
sConsulta = sConsulta & "         SELECT T.PROVE, T.CONTACTO,@ID,@NUM_VERSION ,CONVERT(DATETIME,T.FEC_LIM_RESOL,110),  @EST_NC,GETDATE(),USU.COD,PER.COD,PER.DEP,PER.UON1,PER.UON2,PER.UON3,T.REVISOR" & vbCrLf
sConsulta = sConsulta & "         FROM #TEMPORAL_NOCONFORMIDAD T WITH (NOLOCK) INNER JOIN PER WITH (NOLOCK) ON T.PETICIONARIO=PER.COD INNER JOIN USU WITH (NOLOCK) ON PER.COD=USU.PER" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "         SELECT @ID_NEW=MAX(ID) FROM NOCONFORMIDAD WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         INSERT INTO NOCONF_SOLICIT_ACC(NOCONFORMIDAD,COPIA_CAMPO,SOLICITAR,FECHA_LIMITE)" & vbCrLf
sConsulta = sConsulta & "         SELECT @ID_NEW,CC.ID, T.SOLICITAR,CONVERT(DATETIME,T.FECHA_LIMITE,110) FROM #TEMPORAL_NOCONF_SOLICIT_ACC T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK)  ON T.COPIA_CAMPO=CCD.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CCD.ID=CC.COPIA_CAMPO_DEF WHERE CC.INSTANCIA=@ID AND CC.NUM_VERSION=@NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO NOCONF_ACC (CAMPO_PADRE,  LINEA, NOCONFORMIDAD, VERSION, INSTANCIA, ESTADO, ESTADO_INT, COMENT)" & vbCrLf
sConsulta = sConsulta & "           SELECT CC.ID, T.LINEA, @ID_NEW, @NUM_VERSION,  @ID, T.ESTADO, T.ESTADO_INT,T.COMENT" & vbCrLf
sConsulta = sConsulta & "           FROM #TEMPORAL_NOCONF_ACC T  WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON T.CAMPO_PADRE=CCD.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CCD.ID=CC.COPIA_CAMPO_DEF WHERE CC.INSTANCIA=@ID AND CC.NUM_VERSION=@NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         IF @ESTADO=300  --Si se ha enviado directamente sin pasar por el workflow actualiza los contadores del portal:" & vbCrLf
sConsulta = sConsulta & "              begin" & vbCrLf
sConsulta = sConsulta & "                  UPDATE VERSION_INSTANCIA SET TIPO=2 WHERE INSTANCIA=@ID AND NUM_VERSION=@NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "                  SELECT @PROVE=PROVE FROM NOCONFORMIDAD  WITH (NOLOCK) WHERE ID=@ID_NEW" & vbCrLf
sConsulta = sConsulta & "                  EXEC  FSQA_ACT_REL_CIAS_NEW_NOCONFORMIDAD @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_CREATE_VERSION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_CREATE_VERSION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_CREATE_VERSION @INSTANCIA INT, @PROVE VARCHAR(50) = NULL, @PER_MODIF VARCHAR(50) = NULL, @OLD_VERSION INT = NULL, @CERTIFICADO INT =NULL,  @NUM_VERSION INT =NULL OUTPUT, @NOCONFORMIDAD INT=NULL,@USUNOM NVARCHAR(500)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OLD_VERSION IS NULL" & vbCrLf
sConsulta = sConsulta & "  IF @CERTIFICADO IS NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT @OLD_VERSION = MAX(NUM_VERSION) FROM VERSION_INSTANCIA WHERE INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT @OLD_VERSION = MAX(NUM_VERSION) FROM VERSION_INSTANCIA  WHERE INSTANCIA = @INSTANCIA AND (CERTIFICADO = @CERTIFICADO OR CERTIFICADO IS NULL) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM_VERSION = (SELECT ISNULL(MAX(NUM_VERSION),0) + 1 FROM VERSION_INSTANCIA WHERE INSTANCIA = @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHA DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECHA = GETDATE()" & vbCrLf
sConsulta = sConsulta & "INSERT INTO VERSION_INSTANCIA(INSTANCIA, NUM_VERSION, PROVE, PER_MODIF, FECHA_MODIF, TIPO, CERTIFICADO,USUPORT_NOM)" & vbCrLf
sConsulta = sConsulta & "  VALUES (@INSTANCIA, @NUM_VERSION, @PROVE, @PER_MODIF,@FECHA , CASE WHEN @CERTIFICADO IS NULL AND @NOCONFORMIDAD IS NULL THEN 0 ELSE CASE WHEN @PROVE IS NULL THEN 2 ELSE 1 END END, @CERTIFICADO,@USUNOM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CERTIFICADO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  IF @PROVE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   UPDATE CERTIFICADO SET NUM_VERSION = @NUM_VERSION, FEC_RESPUESTA =@FECHA WHERE ID = @CERTIFICADO" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "   UPDATE CERTIFICADO SET NUM_VERSION = @NUM_VERSION WHERE ID = @CERTIFICADO" & vbCrLf
sConsulta = sConsulta & "  INSERT INTO VERSION_CERTIF(INSTANCIA,NUM_VERSION,CERTIFICADO) VALUES (@INSTANCIA,@NUM_VERSION,@CERTIFICADO)" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @NOCONFORMIDAD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "     begin" & vbCrLf
sConsulta = sConsulta & "       --A�adimos q actualize tb el campo del revisor" & vbCrLf
sConsulta = sConsulta & "        IF @PROVE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   UPDATE NOCONFORMIDAD SET VERSION = @NUM_VERSION, FEC_RESPUESTA =@FECHA WHERE ID = @NOCONFORMIDAD" & vbCrLf
sConsulta = sConsulta & "   end " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE NOCONFORMIDAD SET VERSION = @NUM_VERSION, REVISOR=(SELECT REVISOR FROM #TEMPORAL_NOCONFORMIDAD) WHERE ID = @NOCONFORMIDAD" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TEMPORAL_CAMPOS (CAMPO_OLD INT, CAMPO_NEW INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TEMPORAL_CAMPO_ADJUN (ADJUN_OLD INT, ADJUN_NEW INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CAMPO (INSTANCIA, NUM_VERSION, SUCIO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, COPIA_CAMPO_DEF)" & vbCrLf
sConsulta = sConsulta & "SELECT @INSTANCIA, @NUM_VERSION, 0, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA= @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "   AND C.NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #TEMPORAL_CAMPOS (CAMPO_OLD, CAMPO_NEW)" & vbCrLf
sConsulta = sConsulta & "SELECT COLD.ID CAMPO_OLD, CNEW.ID CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_CAMPO COLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN COPIA_CAMPO CNEW  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                    ON COLD.COPIA_CAMPO_DEF = CNEW.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                   AND COLD.INSTANCIA = CNEW.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "WHERE COLD.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND COLD.NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "  AND CNEW.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL SET CAMPO = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "  FROM #TEMPORAL T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON T.CAMPO = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL_ADJUN SET CAMPO = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "  FROM #TEMPORAL_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON T.CAMPO = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL_DESGLOSE SET CAMPO_PADRE = TCP.CAMPO_NEW, CAMPO_HIJO = TCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "  FROM #TEMPORAL_DESGLOSE T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN #TEMPORAL_CAMPOS TCP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON T.CAMPO_PADRE = TCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN #TEMPORAL_CAMPOS TCH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON T.CAMPO_HIJO = TCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL_DESGLOSE_ADJUN SET CAMPO_PADRE = TCP.CAMPO_NEW, CAMPO_HIJO = TCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "  FROM #TEMPORAL_DESGLOSE_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN #TEMPORAL_CAMPOS TCP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON T.CAMPO_PADRE = TCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN #TEMPORAL_CAMPOS TCH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON T.CAMPO_HIJO = TCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @NOCONFORMIDAD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "    UPDATE #TEMPORAL_NOCONF_SOLICIT_ACC SET COPIA_CAMPO = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "    FROM #TEMPORAL_NOCONF_SOLICIT_ACC T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK)  ON T.COPIA_CAMPO = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_NOCONF_ACC SET CAMPO_PADRE=TC.CAMPO_NEW " & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_NOCONF_ACC T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK)  ON T.CAMPO_PADRE = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT CA.ADJUN, TC.CAMPO_NEW, CA.NOM, CA.IDIOMA, CA.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_CAMPO_ADJUN CA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              ON CA.CAMPO = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO  #TEMPORAL_CAMPO_ADJUN (ADJUN_OLD ,ADJUN_NEW )" & vbCrLf
sConsulta = sConsulta & "SELECT CCA_OLD.ID ,CCA_NEW.ID   " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_CAMPO_ADJUN CCA_OLD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO_ADJUN CCA_NEW  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON CCA_OLD.ADJUN = CCA_NEW.ADJUN " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_CAMPOS TC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON CCA_OLD.CAMPO = TC.CAMPO_old " & vbCrLf
sConsulta = sConsulta & "               AND CCA_NEW.CAMPO = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL_ADJUN SET ADJUN = TC.ADJUN_NEW" & vbCrLf
sConsulta = sConsulta & "  FROM #TEMPORAL_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN #TEMPORAL_CAMPO_ADJUN  TC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON T.ADJUN = TC.ADJUN_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_DESGLOSE (CAMPO_PADRE, CAMPO_HIJO) " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CCP.CAMPO_NEW, CCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_DESGLOSE CD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN #TEMPORAL_CAMPOS CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                  ON CD.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                  ON CD.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT CL.LINEA, CCP.CAMPO_NEW, CCH.CAMPO_NEW, CL.VALOR_NUM, CL.VALOR_TEXT, CL.VALOR_FEC, CL.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_LINEA_DESGLOSE CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN #TEMPORAL_CAMPOS CCP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON CL.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON CL.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "SELECT CCP.CAMPO_NEW, CCH.CAMPO_NEW, CL.LINEA, CL.ADJUN,CL.NOM, CL.IDIOMA, CL.DATASIZE, CL.PER, CL.COMENT, CL.FECALTA" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_LINEA_DESGLOSE_ADJUN CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_CAMPOS CCP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON CL.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON CL.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM #TEMPORAL_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO  #TEMPORAL_CAMPO_ADJUN (ADJUN_OLD ,ADJUN_NEW )" & vbCrLf
sConsulta = sConsulta & "SELECT CCA_OLD.ID ,CCA_NEW.ID   " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CCA_OLD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CCA_NEW  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON CCA_OLD.ADJUN = CCA_NEW.ADJUN " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_CAMPOS TC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON CCA_OLD.CAMPO_HIJO = TC.CAMPO_old " & vbCrLf
sConsulta = sConsulta & "               AND CCA_NEW.CAMPO_HIJO = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE #TEMPORAL_DESGLOSE_ADJUN SET ADJUN = TC.ADJUN_NEW" & vbCrLf
sConsulta = sConsulta & "  FROM #TEMPORAL_DESGLOSE_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN #TEMPORAL_CAMPO_ADJUN  TC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON T.ADJUN = TC.ADJUN_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--IF @NOCONFORMIDAD IS NOT NULL  --Si es una no conformidad guardar� tb las acciones" & vbCrLf
sConsulta = sConsulta & " --BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO NOCONF_SOLICIT_ACC(NOCONFORMIDAD,COPIA_CAMPO,SOLICITAR,FECHA_LIMITE) " & vbCrLf
sConsulta = sConsulta & "   SELECT @NOCONFORMIDAD ,CCP.CAMPO_NEW,N.SOLICITAR,N.FECHA_LIMITE " & vbCrLf
sConsulta = sConsulta & "   FROM NOCONF_SOLICIT_ACC N WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCP WITH (NOLOCK)  ON N.COPIA_CAMPO=CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   WHERE N.NOCONFORMIDAD =@NOCONFORMIDAD" & vbCrLf
sConsulta = sConsulta & " --END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---YA VEREMOS SI ESTA INSERT QUE VIENE SE MANTIENE... POR QUE PARA QUE INSERTAR ESTAS FILAS" & vbCrLf
sConsulta = sConsulta & "---CUANDO AL FINAL VOY A INSERTAR LAS QUE ME VIENEN EN #TEMPORAL_NOCONF_ACC Y BORRAR LAS QUE HAB�A." & vbCrLf
sConsulta = sConsulta & "/*INSERT INTO NOCONF_ACC (CAMPO_PADRE, LINEA, NOCONFORMIDAD, INSTANCIA, VERSION, ESTADO, ESTADO_INT,COMENT)" & vbCrLf
sConsulta = sConsulta & "SELECT CCP.CAMPO_NEW,LINEA, NOCONFORMIDAD,INSTANCIA,@NUM_VERSION,ESTADO,ESTADO_INT,COMENT" & vbCrLf
sConsulta = sConsulta & " FROM  NOCONF_ACC NA " & vbCrLf
sConsulta = sConsulta & "   INNER  JOIN  #TEMPORAL_CAMPOS CCP ON NA.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & " WHERE NA.NOCONFORMIDAD= @NOCONFORMIDAD AND NA.INSTANCIA = @INSTANCIA AND NA.VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ESTA S�" & vbCrLf
sConsulta = sConsulta & "IF @NOCONFORMIDAD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_NOCONF_ACC SET CAMPO_PADRE = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "     FROM #TEMPORAL_NOCONF_ACC T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON T.CAMPO_PADRE = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --UPDATE NOCONF_ACC SET VERSION=@NUM_VERSION,ESTADO=T.ESTADO,ESTADO_INT=T.ESTADO_INT,COMENT=T.COMENT " & vbCrLf
sConsulta = sConsulta & "   --FROM #TEMPORAL_NOCONF_ACC T " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_SAVE_INSTANCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_SAVE_INSTANCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_SAVE_INSTANCE @INSTANCIA INT, @PETICIONARIO VARCHAR(50)=NULL, @COMPRADOR VARCHAR(50) = NULL, @MON VARCHAR(50) = NULL, @ENVIAR TINYINT =0, @IMPORTE FLOAT =NULL, @PEDIDO_AUT TINYINT=0, @PROVE VARCHAR(50) = NULL,@CERTIFICADO INT=NULL,@NOCONFORMIDAD INT=NULL , @ESTADO INT=0 OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS2 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FORMULARIO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @WORKFLOW INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTE_PEDIDO_DIRECTO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM_VERSION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OLD_VERSION INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO_ACTUAL INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CERT_VERSION INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTE_PEDIDO_DIRECTO = PEDIDO_DIRECTO, @ESTADO_ACTUAL=ESTADO FROM INSTANCIA WITH (NOLOCK) WHERE ID = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CERTIFICADO IS NOT NULL AND @CERTIFICADO<>0  --Es un certificado" & vbCrLf
sConsulta = sConsulta & "begin " & vbCrLf
sConsulta = sConsulta & "     SELECT @OLD_VERSION = MAX(NUM_VERSION) FROM VERSION_INSTANCIA WHERE INSTANCIA = @INSTANCIA AND (CERTIFICADO=@CERTIFICADO OR CERTIFICADO IS NULL)" & vbCrLf
sConsulta = sConsulta & "     SELECT @PER = PER_MODIF,@CERT_VERSION=CERTIFICADO FROM VERSION_INSTANCIA  WITH (NOLOCK) WHERE INSTANCIA = @INSTANCIA AND NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "     IF ISNULL(@PER,'') <> @PETICIONARIO OR @PROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          EXEC FSWS_CREATE_VERSION @INSTANCIA = @INSTANCIA, @PER_MODIF = @PETICIONARIO, @PROVE = @PROVE,@CERTIFICADO=@CERTIFICADO, @NUM_VERSION=@NUM_VERSION OUTPUT" & vbCrLf
sConsulta = sConsulta & "    ELSE --Si es el mismo usuario y hay m�s de un proveedor para esa instancia hay que crear una nueva version" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(PROVE) FROM CERTIFICADO WITH (NOLOCK)  WHERE INSTANCIA=@INSTANCIA)>0" & vbCrLf
sConsulta = sConsulta & "                 EXEC FSWS_CREATE_VERSION @INSTANCIA = @INSTANCIA, @PER_MODIF = @PETICIONARIO, @PROVE = @PROVE,@CERTIFICADO=@CERTIFICADO, @NUM_VERSION=@NUM_VERSION OUTPUT" & vbCrLf
sConsulta = sConsulta & "         ELSE --Si es el mismo usuario y hay m�s de un proveedor para esa instancia hay que crear una nueva version" & vbCrLf
sConsulta = sConsulta & "               IF @CERT_VERSION IS NULL AND (SELECT COUNT(PROVE) FROM CERTIFICADO WITH (NOLOCK)  WHERE INSTANCIA=@INSTANCIA)>1" & vbCrLf
sConsulta = sConsulta & "                      EXEC FSWS_CREATE_VERSION @INSTANCIA = @INSTANCIA, @PER_MODIF = @PETICIONARIO, @PROVE = @PROVE,@CERTIFICADO=@CERTIFICADO, @NUM_VERSION=@NUM_VERSION OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  IF @NOCONFORMIDAD IS NOT NULL AND @NOCONFORMIDAD<>0  --Es una no conformidad" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "     SELECT @OLD_VERSION = MAX(NUM_VERSION) FROM VERSION_INSTANCIA WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "     SELECT @PER = PER_MODIF FROM VERSION_INSTANCIA WITH (NOLOCK)  WHERE INSTANCIA = @INSTANCIA AND NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "     IF ISNULL(@PER,'') <> @PETICIONARIO OR @PROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         EXEC FSWS_CREATE_VERSION @INSTANCIA = @INSTANCIA, @PER_MODIF = @PETICIONARIO, @PROVE = @PROVE, @NOCONFORMIDAD=@NOCONFORMIDAD ,  @NUM_VERSION=@NUM_VERSION OUTPUT" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " /* ELSE  --Es una solicitud" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SELECT @OLD_VERSION = MAX(NUM_VERSION) FROM VERSION_INSTANCIA WHERE INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "      SELECT @PER = PER_MODIF FROM VERSION_INSTANCIA WHERE INSTANCIA = @INSTANCIA AND NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     --CREAMOS UNA NUEVA VERSION SI EL QUE GUARDA LA INSTANCIA NO ES EL MISMO QUE EL �LTIMO QUE LA GUARDO (ESTO CAMBIAR� CUANDO SE HAGA EL TEMA DE VARIAS PERSONAS ACTUALIZANDO UNA MISMA INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "      IF ISNULL(@PER,'') <> @PETICIONARIO OR @PROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        EXEC FSWS_CREATE_VERSION @INSTANCIA = @INSTANCIA, @PER_MODIF = @PETICIONARIO, @PROVE = @PROVE, @NUM_VERSION=@NUM_VERSION OUTPUT" & vbCrLf
sConsulta = sConsulta & "  end*/" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM_VERSION IS NULL " & vbCrLf
sConsulta = sConsulta & "  SELECT @NUM_VERSION=MAX(NUM_VERSION) FROM VERSION_INSTANCIA WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA  SET PEDIDO_AUT = @PEDIDO_AUT ,  IMPORTE = @IMPORTE  WHERE ID = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC FSPM_SAVE_VERSION @INSTANCIA =@INSTANCIA , @NUM_VERSION =@NUM_VERSION ,@OLD_VERSION=@OLD_VERSION,@NOCONFORMIDAD=@NOCONFORMIDAD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENVIAR = 1 --Si tiene que enviar la solicitud comprueba si va a compras o pedido directo y si es as� si el gestor del tipo de solicitud es nulo.Si es as� la emite,sino no." & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "      SELECT TOP 1 @APROB = SP.ID FROM COPIA_SUBPASO SP WITH (NOLOCK)   INNER JOIN COPIA_PASO P WITH (NOLOCK)   ON SP.PASO = P.ID" & vbCrLf
sConsulta = sConsulta & "      WHERE P.INSTANCIA = @INSTANCIA  AND ISNULL(P.LIMITE,@IMPORTE-1)<@IMPORTE   AND ISNULL(SP.LIMITE,@IMPORTE-1)<@IMPORTE ORDER BY P.ORDEN, SP.ORDEN" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "      IF @NOCONFORMIDAD IS NOT NULL  --Si es una no conformidad guardada que se va a emitir:" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @APROB IS NULL" & vbCrLf
sConsulta = sConsulta & "                SET @ESTADO = 300 " & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "                SET @ESTADO=2  --Va a workflow" & vbCrLf
sConsulta = sConsulta & "           UPDATE NOCONFORMIDAD SET ESTADO=1,FECALTA=GETDATE() WHERE ID=@NOCONFORMIDAD" & vbCrLf
sConsulta = sConsulta & "           UPDATE INSTANCIA SET ESTADO=@ESTADO, APROB=@APROB WHERE ID=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "           INSERT INTO INSTANCIA_EST (INSTANCIA, PER, FECHA, EST, ACCION) VALUES (@INSTANCIA, @PETICIONARIO, GETDATE(), @ESTADO,1)" & vbCrLf
sConsulta = sConsulta & "           IF @ESTADO=300  --Si se envia directamente sin pasar por el workflow actualiza los contadores del portal:" & vbCrLf
sConsulta = sConsulta & "              begin" & vbCrLf
sConsulta = sConsulta & "                  SELECT @NUM_VERSION= MAX(NUM_VERSION) FROM VERSION_INSTANCIA WHERE INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "                  UPDATE VERSION_INSTANCIA SET TIPO=2 WHERE INSTANCIA=@INSTANCIA AND NUM_VERSION=@NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "                  SELECT @PROVE=PROVE FROM NOCONFORMIDAD WITH (NOLOCK) WHERE ID=@NOCONFORMIDAD" & vbCrLf
sConsulta = sConsulta & "                  EXEC  FSQA_ACT_REL_CIAS_NEW_NOCONFORMIDAD @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "      ELSE  --Es una solicitud:" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "         IF @APROB IS NULL  --Si no hay aprobaci�n superior va a compras o a pedido directo:" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "   IF @PEDIDO_AUT=1 " & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "           IF @IMPORTE_PEDIDO_DIRECTO>@IMPORTE" & vbCrLf
sConsulta = sConsulta & "               SET @ESTADO = 1 --hay que emitir un pedido" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "                 SET @ESTADO = 100 --va a compras" & vbCrLf
sConsulta = sConsulta & "                end" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "         SET @ESTADO = 100 -- va a compras" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT GESTOR FROM SOLICITUD  WITH (NOLOCK) INNER JOIN INSTANCIA  WITH (NOLOCK) ON SOLICITUD.ID=INSTANCIA.SOLICITUD WHERE INSTANCIA.ID=@INSTANCIA) IS NULL" & vbCrLf
sConsulta = sConsulta & "               begin" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO INSTANCIA_EST (INSTANCIA, PER,FECHA, EST, ACCION) VALUES (@INSTANCIA, @PETICIONARIO,GETDATE(), @ESTADO,1)" & vbCrLf
sConsulta = sConsulta & "         UPDATE INSTANCIA SET ESTADO=@ESTADO WHERE ID=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CARGAR_PRECOFE_GRUPOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CARGAR_PRECOFE_GRUPOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_CARGAR_PRECOFE_GRUPOS @ANYO int, @GMN1 varchar(50), @PROCE int, @FECREU DATETIME=NULL AS" & vbCrLf
sConsulta = sConsulta & "IF @FECREU IS NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "        SELECT DISTINCT ITEM_OFE.PROVE,ITEM_OFE.NUM,ITEM.ANYO,ITEM.GMN1_PROCE,ITEM.PROCE,ITEM.ID,ITEM.ART," & vbCrLf
sConsulta = sConsulta & "        ITEM.DESCR ,ITEM.DEST,ITEM.UNI,ITEM.CANT,ITEM.PREC,ITEM.PRES," & vbCrLf
sConsulta = sConsulta & "        ITEM.PAG,ITEM.FECINI,ITEM.FECFIN,ITEM_OFE.CANTMAX,ITEM.EST AS ESTADO,PROVE_ART4.HOM," & vbCrLf
sConsulta = sConsulta & "        ITEM_OFE.PRECIO,ITEM_OFE.PRECIO2,ITEM_OFE.PRECIO3,ITEM_OFE.USAR,ITEM_OFE.PREC_VALIDO AS PRECOFE,ITEM.GRUPO," & vbCrLf
sConsulta = sConsulta & "   ITEM_OFE.COMENT1,ITEM_OFE.COMENT2,ITEM_OFE.COMENT3,CAST(ITEM_OFE.OBSADJUN AS VARCHAR(4000)) AS OBSADJUNTOS,IA.NUMADJUN" & vbCrLf
sConsulta = sConsulta & "        FROM ITEM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ITEM_OFE WITH (NOLOCK) ON ITEM.ANYO=ITEM_OFE.ANYO AND ITEM.GMN1_PROCE=ITEM_OFE.GMN1 AND ITEM.PROCE=ITEM_OFE.PROCE AND ITEM.ID=ITEM_OFE.ITEM " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_OFE WITH (NOLOCK) ON  ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE" & vbCrLf
sConsulta = sConsulta & "                                                           AND ITEM_OFE.PROVE =PROCE_OFE.PROVE AND ITEM_OFE.NUM=PROCE_OFE.OFE AND PROCE_OFE.ENVIADA=1" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_PROVE WITH (NOLOCK) ON PROCE_OFE.PROVE=PROCE_PROVE.PROVE AND PROCE_OFE.ANYO=PROCE_PROVE.ANYO AND PROCE_OFE.GMN1=PROCE_PROVE.GMN1 AND PROCE_OFE.PROCE=PROCE_PROVE.PROCE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN OFEEST WITH (NOLOCK) ON PROCE_OFE.EST=OFEEST.COD AND OFEEST.COMP= 1" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PROVE_ART4 WITH (NOLOCK) ON PROVE_ART4.ART=ITEM.ART AND PROVE_ART4.PROVE=PROCE_OFE.PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM,IA.PROVE,IA.OFE,COUNT(IA.ID) NUMADJUN FROM OFE_ITEM_ADJUN IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          WHERE IA.ANYO=@ANYO AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE GROUP BY IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM,IA.PROVE,IA.OFE) " & vbCrLf
sConsulta = sConsulta & "       IA ON ITEM.ID = IA.ITEM AND ITEM.GMN1_PROCE = IA.GMN1 AND ITEM.PROCE = IA.PROCE AND ITEM.ANYO = IA.ANYO AND PROCE_OFE.PROVE=IA.PROVE AND PROCE_OFE.OFE=IA.OFE" & vbCrLf
sConsulta = sConsulta & "        WHERE PROCE_OFE.ANYO=@ANYO AND PROCE_OFE.GMN1=@GMN1 AND PROCE_OFE.PROCE=@PROCE AND PROCE_OFE.ULT=1 " & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT ITEM_OFE.PROVE,ITEM_OFE.NUM,ITEM.ANYO,ITEM.GMN1_PROCE,ITEM.PROCE,ITEM.ID,ITEM.ART," & vbCrLf
sConsulta = sConsulta & "        ITEM.DESCR,ITEM.DEST,ITEM.UNI,ITEM.CANT,ITEM.PREC,CASE WHEN IP.PRES IS NULL THEN ITEM.PRES ELSE IP.PRES END PRES," & vbCrLf
sConsulta = sConsulta & "        ITEM.PAG,ITEM.FECINI,ITEM.FECFIN,ITEM_OFE.CANTMAX,ITEM.EST AS ESTADO,PROVE_ART4.HOM," & vbCrLf
sConsulta = sConsulta & "        ITEM_OFE.PRECIO,ITEM_OFE.PRECIO2,ITEM_OFE.PRECIO3,ITEM_OFE.USAR,ITEM_OFE.PREC_VALIDO AS PRECOFE,ITEM.GRUPO," & vbCrLf
sConsulta = sConsulta & "   ITEM_OFE.COMENT1,ITEM_OFE.COMENT2,ITEM_OFE.COMENT3,CAST(ITEM_OFE.OBSADJUN AS VARCHAR(4000)) AS OBSADJUNTOS,IA.NUMADJUN" & vbCrLf
sConsulta = sConsulta & "        FROM ITEM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ITEM_PRES IP WITH (NOLOCK) ON ITEM.ANYO = IP.ANYO AND ITEM.GMN1_PROCE = IP.GMN1 AND ITEM.PROCE = IP.PROCE AND ITEM.ID = IP.ITEM AND IP.FECREU=@FECREU" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN ITEM_OFE WITH (NOLOCK) ON ITEM.ANYO=ITEM_OFE.ANYO AND ITEM.GMN1_PROCE=ITEM_OFE.GMN1 AND ITEM.PROCE=ITEM_OFE.PROCE AND ITEM.ID=ITEM_OFE.ITEM " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_OFE WITH (NOLOCK) ON  ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE " & vbCrLf
sConsulta = sConsulta & "                                                         AND ITEM_OFE.PROVE =PROCE_OFE.PROVE AND ITEM_OFE.NUM=PROCE_OFE.OFE AND PROCE_OFE.ENVIADA=1" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN VIEW_PROCE_OFE_REU VPOR WITH (NOLOCK) ON PROCE_OFE.ANYO=VPOR.ANYO AND PROCE_OFE.GMN1=VPOR.GMN1 AND PROCE_OFE.PROCE=VPOR.PROCE AND PROCE_OFE.PROVE = VPOR.PROVE AND PROCE_OFE.OFE = VPOR.OFE and VPOR.FECREU=@FECREU" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_PROVE WITH (NOLOCK) ON PROCE_OFE.PROVE=PROCE_PROVE.PROVE AND PROCE_OFE.ANYO=PROCE_PROVE.ANYO AND PROCE_OFE.GMN1=PROCE_PROVE.GMN1 AND PROCE_OFE.PROCE=PROCE_PROVE.PROCE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN OFEEST WITH (NOLOCK) ON PROCE_OFE.EST=OFEEST.COD AND OFEEST.COMP= 1" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PROVE_ART4 WITH (NOLOCK) ON PROVE_ART4.ART=ITEM.ART AND PROVE_ART4.PROVE=PROCE_OFE.PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM,IA.PROVE,IA.OFE,COUNT(IA.ID) NUMADJUN FROM OFE_ITEM_ADJUN IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          WHERE IA.ANYO=@ANYO AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE GROUP BY IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM,IA.PROVE,IA.OFE) " & vbCrLf
sConsulta = sConsulta & "       IA ON ITEM.ID = IA.ITEM AND ITEM.GMN1_PROCE = IA.GMN1 AND ITEM.PROCE = IA.PROCE AND ITEM.ANYO = IA.ANYO AND PROCE_OFE.PROVE=IA.PROVE AND PROCE_OFE.OFE=IA.OFE" & vbCrLf
sConsulta = sConsulta & "        WHERE PROCE_OFE.ANYO=@ANYO AND PROCE_OFE.GMN1=@GMN1 AND PROCE_OFE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[POND_CARGAR_LISTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[POND_CARGAR_LISTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE POND_CARGAR_LISTA  @TIPO SMALLINT,@ATRIB INTEGER  AS" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 0" & vbCrLf
sConsulta = sConsulta & "   SELECT DEF_ATRIB.POND,DEF_ATRIB.FORMULA,DEF_ATRIB.NUM,POND_SI,DEF_ATRIB.POND_NO, 1 AS TIP" & vbCrLf
sConsulta = sConsulta & "                           ,DEF_ATRIB_LISTA.ORDEN,DEF_ATRIB_LISTA.VALOR_NUM,DEF_ATRIB_LISTA.VALOR_TEXT,DEF_ATRIB_LISTA.VALOR_FEC" & vbCrLf
sConsulta = sConsulta & "             ,null DESDENUM,null HASTANUM,null DESDEFEC,null HASTAFEC,DEF_ATRIB_LISTA.VALOR_POND AS VALOR_POND_LISTA" & vbCrLf
sConsulta = sConsulta & "             ,DEF_ATRIB.INTRO" & vbCrLf
sConsulta = sConsulta & "      FROM DEF_ATRIB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN DEF_ATRIB_LISTA WITH (NOLOCK) ON DEF_ATRIB_LISTA.ATRIB=DEF_ATRIB.ID     " & vbCrLf
sConsulta = sConsulta & "   WHERE DEF_ATRIB.ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT DEF_ATRIB.POND,DEF_ATRIB.FORMULA,DEF_ATRIB.NUM,POND_SI,DEF_ATRIB.POND_NO,2 AS TIP,NULL,NULL,NULL,NULL" & vbCrLf
sConsulta = sConsulta & "                           ,DEF_ATRIB_POND.DESDE_NUM,DEF_ATRIB_POND.HASTA_NUM,DEF_ATRIB_POND.DESDE_FEC" & vbCrLf
sConsulta = sConsulta & "                           ,DEF_ATRIB_POND.HASTA_FEC,DEF_ATRIB_POND.VALOR_POND AS VALOR_POND_LISTA,DEF_ATRIB.INTRO" & vbCrLf
sConsulta = sConsulta & "      FROM DEF_ATRIB  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN DEF_ATRIB_POND WITH (NOLOCK) ON DEF_ATRIB_POND.ATRIB=DEF_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE DEF_ATRIB.ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   ORDER BY TIP" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "                                        SELECT PROCE_ATRIB.POND,PROCE_ATRIB.OP_POND AS FORMULA,PROCE_ATRIB.NUM_POND AS NUM" & vbCrLf
sConsulta = sConsulta & "                                                      ,PROCE_ATRIB.POND_SI,PROCE_ATRIB.POND_NO, 1 AS TIP,PROCE_ATRIB_LISTA.ORDEN" & vbCrLf
sConsulta = sConsulta & "                                                      ,PROCE_ATRIB_LISTA.VALOR_NUM,PROCE_ATRIB_LISTA.VALOR_TEXT,PROCE_ATRIB_LISTA.VALOR_FEC" & vbCrLf
sConsulta = sConsulta & "                                                      ,null DESDENUM,null HASTANUM,null DESDEFEC,null HASTAFEC" & vbCrLf
sConsulta = sConsulta & "                                                      ,PROCE_ATRIB_LISTA.VALOR_POND AS VALOR_POND_LISTA,DEF_ATRIB.INTRO" & vbCrLf
sConsulta = sConsulta & "                                           FROM PROCE_ATRIB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                          INNER JOIN DEF_ATRIB WITH (NOLOCK) ON PROCE_ATRIB.ATRIB = DEF_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "                                             LEFT JOIN PROCE_ATRIB_LISTA WITH (NOLOCK) ON PROCE_ATRIB.ID=PROCE_ATRIB_LISTA.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "                                        WHERE  PROCE_ATRIB.ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                               union" & vbCrLf
sConsulta = sConsulta & "                                        SELECT PROCE_ATRIB.POND,PROCE_ATRIB.OP_POND AS FORMULA,PROCE_ATRIB.NUM_POND AS NUM" & vbCrLf
sConsulta = sConsulta & "                                                     ,PROCE_ATRIB.POND_SI,PROCE_ATRIB.POND_NO,2 AS TIP,NULL,NULL,NULL,NULL" & vbCrLf
sConsulta = sConsulta & "                                                     ,PROCE_ATRIB_POND.DESDE_NUM,PROCE_ATRIB_POND.HASTA_NUM,PROCE_ATRIB_POND.DESDE_FEC" & vbCrLf
sConsulta = sConsulta & "                                                    ,PROCE_ATRIB_POND.HASTA_FEC,PROCE_ATRIB_POND.VALOR_POND AS VALOR_POND_LISTA,DEF_ATRIB.INTRO" & vbCrLf
sConsulta = sConsulta & "                                           FROM PROCE_ATRIB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                          INNER JOIN DEF_ATRIB  WITH (NOLOCK) ON PROCE_ATRIB.ATRIB = DEF_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "                                             LEFT JOIN PROCE_ATRIB_POND WITH (NOLOCK) ON PROCE_ATRIB.ID=PROCE_ATRIB_POND.ATRIB_ID    " & vbCrLf
sConsulta = sConsulta & "                                        WHERE PROCE_ATRIB.ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                                        ORDER BY TIP" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO = 2" & vbCrLf
sConsulta = sConsulta & "                                                     SELECT PLANTILLA_ATRIB.POND,PLANTILLA_ATRIB.OP_POND AS FORMULA,PLANTILLA_ATRIB.NUM_POND AS NUM" & vbCrLf
sConsulta = sConsulta & "                                                                   ,PLANTILLA_ATRIB.POND_SI,PLANTILLA_ATRIB.POND_NO, 1 AS TIP,PLANTILLA_ATRIB_LISTA.ORDEN" & vbCrLf
sConsulta = sConsulta & "                                                                   ,PLANTILLA_ATRIB_LISTA.VALOR_NUM,PLANTILLA_ATRIB_LISTA.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "                                                                   ,PLANTILLA_ATRIB_LISTA.VALOR_FEC,null DESDENUM,null HASTANUM,null DESDEFEC,null HASTAFEC" & vbCrLf
sConsulta = sConsulta & "                                                                   ,PLANTILLA_ATRIB_LISTA.VALOR_POND AS VALOR_POND_LISTA,DEF_ATRIB.INTRO" & vbCrLf
sConsulta = sConsulta & "                                                        FROM PLANTILLA_ATRIB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                                        INNER JOIN DEF_ATRIB WITH (NOLOCK) ON PLANTILLA_ATRIB.ATRIB = DEF_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "                                                          LEFT JOIN PLANTILLA_ATRIB_LISTA WITH (NOLOCK) ON PLANTILLA_ATRIB.ID=PLANTILLA_ATRIB_LISTA.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "                                                    WHERE PLANTILLA_ATRIB.ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                                              UNION " & vbCrLf
sConsulta = sConsulta & "                                                   SELECT PLANTILLA_ATRIB.POND,PLANTILLA_ATRIB.OP_POND AS FORMULA,PLANTILLA_ATRIB.NUM_POND AS NUM" & vbCrLf
sConsulta = sConsulta & "                                                                 ,PLANTILLA_ATRIB.POND_SI,PLANTILLA_ATRIB.POND_NO,2 AS TIP,NULL,NULL,NULL,NULL" & vbCrLf
sConsulta = sConsulta & "                                                                 ,PLANTILLA_ATRIB_POND.DESDE_NUM,PLANTILLA_ATRIB_POND.HASTA_NUM" & vbCrLf
sConsulta = sConsulta & "                                                                 ,PLANTILLA_ATRIB_POND.DESDE_FEC,PLANTILLA_ATRIB_POND.HASTA_FEC" & vbCrLf
sConsulta = sConsulta & "                                                                 ,PLANTILLA_ATRIB_POND.VALOR_POND AS VALOR_POND_LISTA,DEF_ATRIB.INTRO" & vbCrLf
sConsulta = sConsulta & "                                                      FROM PLANTILLA_ATRIB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                                     INNER JOIN DEF_ATRIB WITH (NOLOCK) ON PLANTILLA_ATRIB.ATRIB = DEF_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "                                                       LEFT JOIN PLANTILLA_ATRIB_POND WITH (NOLOCK) ON PLANTILLA_ATRIB.ID=PLANTILLA_ATRIB_POND.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "                                                   WHERE PLANTILLA_ATRIB.ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                                                    ORDER BY TIP" & vbCrLf
ExecuteSQL gRDOCon, sConsulta




sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETDESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETDESGLOSE @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL,@PER VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDI IS NULL " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT ISNULL(CC.ORDEN,C.ORDEN),C.*, LINEA.LINEA ,ISNULL(CC.ESCRITURA,1) ESCRITURA, ISNULL(CC.VISIBLE ,1) VISIBLE, CC.OBLIGATORIO,DEF_ATRIB.VALIDACION_ERP" & vbCrLf
sConsulta = sConsulta & "      FROM FORM_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             INNER JOIN FORM_GRUPO G WITH (NOLOCK)  ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH (NOLOCK)  ON G.FORMULARIO=S.FORMULARIO AND S.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_BLOQUE WITH (NOLOCK)  ON S.WORKFLOW=PM_BLOQUE.WORKFLOW AND PM_BLOQUE.TIPO=1 " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_ROL WITH (NOLOCK)  ON S.WORKFLOW=PM_ROL.WORKFLOW AND PM_ROL.TIPO=4" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN (SELECT TOP 1 * FROM VIEW_ROL_PETICIONARIO WITH (NOLOCK)  WHERE COD = @PER AND SOLICITUD = @SOLICITUD) VW ON VW.ID=PM_ROL.ID AND VW.SOLICITUD=S.ID AND VW.COD=@PER" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_ROL_BLOQUE WITH (NOLOCK)  ON PM_ROL.ID=PM_ROL_BLOQUE.ROL AND PM_BLOQUE.ID=PM_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "             LEFT JOIN PM_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON C.ID = CC.CAMPO AND CC.BLOQUE=PM_BLOQUE.ID AND CC.ROL =  PM_ROL.ID" & vbCrLf
sConsulta = sConsulta & "             LEFT JOIN DEF_ATRIB WITH (NOLOCK)  ON DEF_ATRIB.ID = C.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN DESGLOSE D WITH (NOLOCK)  ON C.ID = D.CAMPO_HIJO , (SELECT MAX(LINEA) LINEA FROM LINEA_DESGLOSE WHERE CAMPO_PADRE = @ID) LINEA " & vbCrLf
sConsulta = sConsulta & "     WHERE D.CAMPO_PADRE = @ID AND C.ES_SUBCAMPO=1  " & vbCrLf
sConsulta = sConsulta & "     ORDER BY CC.ORDEN,CC.CAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT L.* FROM CAMPO_VALOR_LISTA L WITH (NOLOCK)  INNER JOIN FORM_CAMPO C WITH (NOLOCK)  ON L.CAMPO = C.ID INNER JOIN DESGLOSE D WITH (NOLOCK)  ON C.ID = D.CAMPO_HIJO  WHERE D.CAMPO_PADRE = @ID AND C.ES_SUBCAMPO=1 ORDER BY L.CAMPO, L.ORDEN" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT C.ID, C.GRUPO, C.DEN_' + @IDI + ', AYUDA_' + @IDI + ', C.TIPO, C.SUBTIPO, C.INTRO, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.MINNUM, C.MAXNUM, C.MINFEC, C.MAXFEC, C.ID_ATRIB_GS, C.TIPO_CAMPO_GS, LINEA.LINEA , ISNULL(CC.ESCRITURA,1) ESCRITURA, ISNULL(CC.VISIBLE ,1) VISIBLE, CC.OBLIGATORIO,ISNULL(CC.ORDEN,C.ORDEN) ORDEN, C.ID_CALCULO, C.FORMULA, C.ORDEN_CALCULO,C.ANYADIR_ART,DEF_ATRIB.VALIDACION_ERP,C.CARGAR_ULT_ADJ" & vbCrLf
sConsulta = sConsulta & "                   FROM FORM_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN FORM_GRUPO G WITH (NOLOCK)  ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN SOLICITUD S WITH (NOLOCK)  ON G.FORMULARIO=S.FORMULARIO AND S.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PM_BLOQUE WITH (NOLOCK)  ON S.WORKFLOW=PM_BLOQUE.WORKFLOW AND PM_BLOQUE.TIPO=1 " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PM_ROL WITH (NOLOCK)  ON S.WORKFLOW=PM_ROL.WORKFLOW AND PM_ROL.TIPO=4" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN (SELECT TOP 1 * FROM VIEW_ROL_PETICIONARIO WITH (NOLOCK)  WHERE COD = @PER AND SOLICITUD = @SOLICITUD) VW ON VW.ID=PM_ROL.ID AND VW.SOLICITUD=S.ID AND VW.COD=@PER" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN PM_ROL_BLOQUE WITH (NOLOCK)  ON PM_ROL.ID=PM_ROL_BLOQUE.ROL AND PM_BLOQUE.ID=PM_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "                         LEFT JOIN PM_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON C.ID = CC.CAMPO AND CC.BLOQUE=PM_BLOQUE.ID AND CC.ROL =  PM_ROL.ID" & vbCrLf
sConsulta = sConsulta & "               LEFT JOIN DEF_ATRIB WITH (NOLOCK)  ON DEF_ATRIB.ID = C.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN DESGLOSE D WITH (NOLOCK)  ON C.ID = D.CAMPO_HIJO  , (SELECT MAX(LINEA) LINEA FROM LINEA_DESGLOSE WHERE CAMPO_PADRE = @ID) LINEA  " & vbCrLf
sConsulta = sConsulta & "                  WHERE D.CAMPO_PADRE = @ID AND C.ES_SUBCAMPO=1 ORDER BY CC.ORDEN,CC.CAMPO'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@ID INT, @SOLICITUD INT,@PER VARCHAR(50)', @ID=@ID,@SOLICITUD=@SOLICITUD,@PER=@PER" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT  L.ORDEN, L.VALOR_NUM, L.VALOR_TEXT_' + @IDI + ', L.VALOR_FEC , L.CAMPO, L.CAMPO ID FROM CAMPO_VALOR_LISTA L WITH (NOLOCK)  INNER JOIN FORM_CAMPO C WITH (NOLOCK)  ON L.CAMPO = C.ID INNER JOIN DESGLOSE D WITH (NOLOCK)  ON C.ID = D.CAMPO_HIJO  WHERE D.CAMPO_PADRE = @ID AND C.ES_SUBCAMPO=1 ORDER BY L.CAMPO, L.ORDEN'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LD.*" & vbCrLf
sConsulta = sConsulta & ", case when c.tipo_campo_gs = 118 then " & vbCrLf
sConsulta = sConsulta & "(SELECT ART4.GENERICO FROM ART4 WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEA_DESGLOSE LDART WITH (NOLOCK)  ON LDART.CAMPO_HIJO = LD.CAMPO_HIJO-1 AND LDART.LINEA = LD.LINEA" & vbCrLf
sConsulta = sConsulta & "  WHERE COD = LDART.VALOR_TEXT) end GENERICO,C.TIPO_CAMPO_GS" & vbCrLf
sConsulta = sConsulta & " FROM LINEA_DESGLOSE LD WITH (NOLOCK)  INNER JOIN FORM_CAMPO C WITH (NOLOCK)  ON C.ID = LD.CAMPO_HIJO WHERE LD.CAMPO_PADRE=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LDA.ID, LDA.CAMPO_PADRE, LDA.CAMPO_HIJO, LDA.LINEA,  P.NOM + ' ' + P.APE NOMBRE, LDA.NOM , LDA.IDIOMA, LDA.DATASIZE, LDA.PER, LDA.COMENT, LDA.FECALTA " & vbCrLf
sConsulta = sConsulta & "  FROM LINEA_DESGLOSE_ADJUN LDA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "  LEFT JOIN PER P  WITH (NOLOCK)  ON LDA.PER = P.COD " & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_PADRE = @ID ORDER BY CAMPO_HIJO, LINEA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GET_PRECONDICIONES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GET_PRECONDICIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GET_PRECONDICIONES @BSOLICITUD BIT, @IDACC INTEGER, @IDI VARCHAR(3) = 'SPA', @INSTANCIA INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @BSOLICITUD = 1 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = 'SELECT ID, ORDEN, COD, ACCION, TIPO, FORMULA, DEN " & vbCrLf
sConsulta = sConsulta & "    FROM PM_ACCION_PRECOND P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN PM_ACCION_PRECOND_DEN PD WITH (NOLOCK)  ON PD.ACCION_PRECOND = P.ID AND IDI = @IDI " & vbCrLf
sConsulta = sConsulta & "    WHERE ACCION = @IDACC'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@IDACC INT, @IDI VARCHAR(3)', @IDACC=@IDACC, @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = 'SELECT * " & vbCrLf
sConsulta = sConsulta & "   FROM PM_ACCION_CONDICIONES WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE ACCION_PRECOND IN " & vbCrLf
sConsulta = sConsulta & "       (" & vbCrLf
sConsulta = sConsulta & "       SELECT ID " & vbCrLf
sConsulta = sConsulta & "       FROM PM_ACCION_PRECOND WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       WHERE ACCION = @IDACC" & vbCrLf
sConsulta = sConsulta & "       )'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@IDACC INT', @IDACC=@IDACC" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = 'SELECT ID, ORDEN, COD, ACCION, TIPO, FORMULA, DEN " & vbCrLf
sConsulta = sConsulta & "    FROM PM_COPIA_ACCION_PRECOND P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN PM_COPIA_ACCION_PRECOND_DEN PD WITH (NOLOCK)  ON PD.ACCION_PRECOND = P.ID AND IDI = @IDI " & vbCrLf
sConsulta = sConsulta & "    WHERE ACCION = @IDACC'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@IDACC INT, @IDI VARCHAR(3)', @IDACC=@IDACC, @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = 'SELECT CAC.ID, CAC.ACCION_PRECOND, CAC.COD, CAC.TIPO_CAMPO, CC1.ID AS CAMPO, CAC.OPERADOR, CAC.TIPO_VALOR, CC2.ID AS CAMPO_VALOR, CAC.VALOR_TEXT, CAC.VALOR_NUM, CAC.VALOR_FEC, CAC.VALOR_BOOL, CAC.FECACT " & vbCrLf
sConsulta = sConsulta & "       FROM PM_COPIA_ACCION_CONDICIONES CAC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN COPIA_CAMPO CC1 WITH (NOLOCK)  ON CAC.CAMPO = CC1.COPIA_CAMPO_DEF AND CC1.NUM_VERSION = (SELECT MAX(NUM_VERSION) FROM VERSION_INSTANCIA WITH (NOLOCK)  WHERE INSTANCIA = @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN COPIA_CAMPO CC2 WITH (NOLOCK)  ON CAC.CAMPO_VALOR = CC2.COPIA_CAMPO_DEF AND CC2.NUM_VERSION = (SELECT MAX(NUM_VERSION) FROM VERSION_INSTANCIA WITH (NOLOCK)  WHERE INSTANCIA = @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "       WHERE ACCION_PRECOND IN (SELECT ID FROM PM_COPIA_ACCION_PRECOND WITH (NOLOCK)  WHERE ACCION = @IDACC)'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@IDACC INT, @INSTANCIA INT', @IDACC=@IDACC, @INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTINVISIBLEFIELDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTINVISIBLEFIELDS @IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL, @IDI VARCHAR(50) = 'SPA'    AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VERSION INT" & vbCrLf
sConsulta = sConsulta & "--DECLARE @TRASLADADA AS TINYINT " & vbCrLf
sConsulta = sConsulta & "--DECLARE @ESTADO AS INT" & vbCrLf
sConsulta = sConsulta & "--DECLARE @PETICIONARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE  @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE  @SQLCOMMON NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ROL INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @BLOQUE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENBLOQUE_ACT TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @INSTANCIA =INSTANCIA,  @VERSION =NUM_VERSION FROM COPIA_CAMPO WITH (NOLOCK) WHERE ID = @IDCAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SELECT @ESTADO= ESTADO,@TRASLADADA=TRASLADADA, @PETICIONARIO = PETICIONARIO  FROM INSTANCIA WHERE INSTANCIA.ID=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ROL =NULL" & vbCrLf
sConsulta = sConsulta & "SET @BLOQUE=NULL" & vbCrLf
sConsulta = sConsulta & "SET @ENBLOQUE_ACT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-------------Ahora obtiene el rol y bloque del que se va a obtener la configuraci�n de la cumplimentaci�n-----------------------------------------" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "--1� Comprueba si el usuario o prove es un rol de un bloque que est� activo:" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT TOP 1 @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE FROM INSTANCIA_BLOQUE IB " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_ROL_BLOQUE ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_ROL ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID '" & vbCrLf
sConsulta = sConsulta & "IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' AND PM_COPIA_ROL.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' AND PM_COPIA_ROL.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND IB.TRASLADADA=0'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ROL IS NULL and @USU IS NOT NULL --No est� asignado al rol, comprueba si esta en la posible lista de asignados (COMO_ASIGNAR=3)" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=N'SELECT TOP 1 @ROL= R.ID, @BLOQUE=RB.BLOQUE FROM INSTANCIA_BLOQUE IB " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_ROL_BLOQUE RB ON IB.BLOQUE=RB.BLOQUE " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_ROL R ON RB.ROL=R.ID AND R.COMO_ASIGNAR=3 AND R.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN PM_COPIA_PARTICIPANTES PT ON PT.ROL= R.ID  AND PT.PER=@USU" & vbCrLf
sConsulta = sConsulta & "                    WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND IB.TRASLADADA=0'" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ROL IS NULL --No es un rol de un bloque activo. Comprueba si es un rol de cualquier bloque:" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "        SET @SQL=N'SELECT TOP 1  @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE FROM INSTANCIA_BLOQUE IB " & vbCrLf
sConsulta = sConsulta & "                INNER JOIN PM_COPIA_ROL_BLOQUE ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "                INNER JOIN PM_COPIA_ROL ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID  WHERE IB.INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL +' AND PM_COPIA_ROL.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @SQL=@SQL +' AND PM_COPIA_ROL.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT ', @INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @ROL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "            SET @ENBLOQUE_ACT=0" & vbCrLf
sConsulta = sConsulta & "       ELSE --No es un rol, ser� un usuario al que se le ha trasladado o devuelto la solicitud:" & vbCrLf
sConsulta = sConsulta & "          begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT TOP 1 @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE FROM INSTANCIA_BLOQUE IB " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PM_COPIA_ROL_BLOQUE ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PM_COPIA_ROL ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN (SELECT MAX(ID) ID ,INSTANCIA,BLOQUE FROM INSTANCIA_EST WHERE DESTINATARIO IS NOT NULL OR DESTINATARIO_PROV IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                     GROUP BY INSTANCIA,BLOQUE) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN INSTANCIA_EST IE ON T.ID=IE.ID'" & vbCrLf
sConsulta = sConsulta & "          IF @USU IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                  SET @SQL=@SQL + ' AND  IE.DESTINATARIO=@USU'" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL + ' AND IE.DESTINATARIO_PROV=@PROVE'" & vbCrLf
sConsulta = sConsulta & "          SET @SQL=@SQL + ' WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND IB.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "          EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               IF @ROL IS NULL --No es el usuario activo de un traslado o devoluci�n,ser� uno intermedio o uno que intervino en alg�n momento." & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "                    SET @ENBLOQUE_ACT=0" & vbCrLf
sConsulta = sConsulta & "                    SET @SQL=N'SELECT TOP 1 @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE FROM INSTANCIA_BLOQUE IB " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PM_COPIA_ROL_BLOQUE ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PM_COPIA_ROL ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN (SELECT MAX(ID) ID ,INSTANCIA,BLOQUE,ROL FROM INSTANCIA_EST'" & vbCrLf
sConsulta = sConsulta & "          IF @USU IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                  SET @SQL=@SQL + ' WHERE  DESTINATARIO=@USU'" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL + ' WHERE DESTINATARIO_PROV=@PROVE'" & vbCrLf
sConsulta = sConsulta & "          SET @SQL=@SQL + ' GROUP BY INSTANCIA,BLOQUE,ROL) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE AND T.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "          WHERE IB.INSTANCIA=@INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "          EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "                 end" & vbCrLf
sConsulta = sConsulta & "          end" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC FSPM_IDENTIFICAR_ROL @USU=@USU, @PROVE=@PROVE, @INSTANCIA=@INSTANCIA, @ROL=@ROL OUTPUT , @BLOQUE= @BLOQUE OUTPUT,@ENBLOQUE_ACT=@ENBLOQUE_ACT OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT C.ID, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.VALOR_NUM, ISNULL(CD.ID_ATRIB_GS,0) ID_ATRIB_GS, ISNULL(CD.TIPO_CAMPO_GS,0) TIPO_CAMPO_GS,CD.GRUPO, ISNULL(D.VALIDACION_ERP,0) VALIDACION_ERP , ISNULL(CD.SUBTIPO,0) SUBTIPO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CD.ID " & vbCrLf
sConsulta = sConsulta & "               LEFT JOIN DEF_ATRIB D WITH (NOLOCK)  ON D.ID=CD.ID_ATRIB_GS '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQLCOMMON=' INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON CD.ID=CC.CAMPO AND CC.BLOQUE=@BLOQUE AND CC.ROL=@ROL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0 AND  CC.VISIBLE=0' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50),@BLOQUE INT,@ROL INT ', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE,@BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* ,CC.VISIBLE, ISNULL(CD.ID_ATRIB_GS,0) ID_ATRIB_GS, ISNULL(CD.TIPO_CAMPO_GS,0) TIPO_CAMPO_GS,CD.GRUPO, ISNULL(D.VALIDACION_ERP,0) VALIDACION_ERP,CVL.VALOR_TEXT_' + @IDI + ' LISTA_TEXT " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON LD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON CD.ID = C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_DESGLOSE CDESG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON C.ID = CDESG.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN DEF_ATRIB D  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      ON D.ID=CD.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN COPIA_CAMPO_VALOR_LISTA CVL  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   ON CVL.CAMPO_DEF=C.COPIA_CAMPO_DEF AND CVL.ORDEN=LD.VALOR_NUM '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=1 AND ISNULL(CD.TIPO_CAMPO_GS,0)<>103 AND ISNULL(CD.TIPO_CAMPO_GS,0)<>105'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50),@BLOQUE INT,@ROL INT', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE,@BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN" & vbCrLf
sConsulta = sConsulta & "           (SELECT DISTINCT C.ID" & vbCrLf
sConsulta = sConsulta & "              FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         ON C.COPIA_CAMPO_DEF=CD.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=1 AND ISNULL(CD.TIPO_CAMPO_GS,0)<>103 AND ISNULL(CD.TIPO_CAMPO_GS,0)<>105'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ') DESG ON LD.CAMPO_PADRE = DESG.ID' " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50),@BLOQUE INT,@ROL INT', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE,@BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINVISIBLEFIELDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE FSPM_GETINVISIBLEFIELDS @ID INT,@PER VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FORMULARIO AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FORMULARIO = (SELECT FORMULARIO FROM SOLICITUD WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CAMPOS SIMPLES INVISIBLES" & vbCrLf
sConsulta = sConsulta & "SELECT C.ID, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & ", ISNULL(C.ID_ATRIB_GS,0) ID_ATRIB_GS, ISNULL(C.TIPO_CAMPO_GS,0) TIPO_CAMPO_GS,C.GRUPO, ISNULL(D.VALIDACION_ERP,0) VALIDACION_ERP, ISNULL(C.SUBTIPO,0) SUBTIPO" & vbCrLf
sConsulta = sConsulta & "FROM FORM_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G WITH (NOLOCK)  ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK)  ON G.FORMULARIO=S.FORMULARIO AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_BLOQUE WITH (NOLOCK)  ON S.WORKFLOW=PM_BLOQUE.WORKFLOW AND PM_BLOQUE.TIPO=1 " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL WITH (NOLOCK)  ON S.WORKFLOW=PM_ROL.WORKFLOW AND PM_ROL.TIPO=4" & vbCrLf
sConsulta = sConsulta & "INNER JOIN VIEW_ROL_PETICIONARIO VW WITH (NOLOCK)  ON VW.ID=PM_ROL.ID AND VW.SOLICITUD=S.ID AND VW.COD=@PER" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL_BLOQUE WITH (NOLOCK)  ON PM_ROL.ID=PM_ROL_BLOQUE.ROL AND PM_BLOQUE.ID=PM_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONF_CUMP_BLOQUE CC  WITH (NOLOCK)  ON C.ID = CC.CAMPO AND CC.BLOQUE=PM_BLOQUE.ID AND CC.ROL =  PM_ROL.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEF_ATRIB D WITH (NOLOCK)  ON D.ID=C.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "WHERE G.FORMULARIO = @FORMULARIO AND C.ES_SUBCAMPO=0  AND CC.VISIBLE=0 " & vbCrLf
sConsulta = sConsulta & "ORDER BY G.ID, CC.ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--LINEAS DE DESGLOSE DE CAMPOS INVISIBLES QUE EST�N EN DESGLOSES" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* ,CC.VISIBLE,ISNULL(C.TIPO_CAMPO_GS,0) TIPO_CAMPO_GS" & vbCrLf
sConsulta = sConsulta & ", ISNULL(C.ID_ATRIB_GS,0) ID_ATRIB_GS,C.GRUPO, ISNULL(D.VALIDACION_ERP,0) VALIDACION_ERP,CVL.VALOR_TEXT_SPA LISTA_TEXT" & vbCrLf
sConsulta = sConsulta & "FROM LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH (NOLOCK)  ON C.ID = LD.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G WITH (NOLOCK)  ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK)  ON G.FORMULARIO=S.FORMULARIO AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_BLOQUE WITH (NOLOCK)  ON S.WORKFLOW=PM_BLOQUE.WORKFLOW AND PM_BLOQUE.TIPO=1 " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL WITH (NOLOCK)  ON S.WORKFLOW=PM_ROL.WORKFLOW AND PM_ROL.TIPO=4" & vbCrLf
sConsulta = sConsulta & "INNER JOIN VIEW_ROL_PETICIONARIO VW WITH (NOLOCK)  ON VW.ID=PM_ROL.ID AND VW.SOLICITUD=S.ID AND VW.COD=@PER" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL_BLOQUE WITH (NOLOCK) ON PM_ROL.ID=PM_ROL_BLOQUE.ROL AND PM_BLOQUE.ID=PM_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONF_CUMP_BLOQUE CC  WITH (NOLOCK)  ON C.ID = CC.CAMPO AND CC.BLOQUE=PM_BLOQUE.ID AND CC.ROL =  PM_ROL.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEF_ATRIB D WITH (NOLOCK)  ON D.ID=C.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAMPO_VALOR_LISTA CVL WITH (NOLOCK)  ON CVL.CAMPO=C.ID AND CVL.ORDEN=LD.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "WHERE G.FORMULARIO = @FORMULARIO AND C.ES_SUBCAMPO=1 AND ISNULL(C.TIPO_CAMPO_GS,0)<>103  AND ISNULL(C.TIPO_CAMPO_GS,0)<>105" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CAMPOS DE LOS DESGLOSES INVISIBLES" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* " & vbCrLf
sConsulta = sConsulta & "FROM LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(SELECT C.ID" & vbCrLf
sConsulta = sConsulta & "FROM FORM_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G WITH (NOLOCK)  ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK)  ON G.FORMULARIO=S.FORMULARIO AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_BLOQUE WITH (NOLOCK)  ON S.WORKFLOW=PM_BLOQUE.WORKFLOW AND PM_BLOQUE.TIPO=1 " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL WITH (NOLOCK)  ON S.WORKFLOW=PM_ROL.WORKFLOW AND PM_ROL.TIPO=4" & vbCrLf
sConsulta = sConsulta & "INNER JOIN VIEW_ROL_PETICIONARIO VW WITH (NOLOCK)  ON VW.ID=PM_ROL.ID AND VW.SOLICITUD=S.ID AND VW.COD=@PER" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL_BLOQUE WITH (NOLOCK)  ON PM_ROL.ID=PM_ROL_BLOQUE.ROL AND PM_BLOQUE.ID=PM_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON C.ID = CC.CAMPO AND CC.BLOQUE=PM_BLOQUE.ID AND CC.ROL =  PM_ROL.ID" & vbCrLf
sConsulta = sConsulta & "WHERE G.FORMULARIO = @FORMULARIO AND C.ES_SUBCAMPO=0 AND CC.VISIBLE=0 AND C.SUBTIPO = 9" & vbCrLf
sConsulta = sConsulta & ") DESG ON LD.CAMPO_PADRE = DESG.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADFIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADFIELDSCALC @FORMULARIO INT,  @IDI VARCHAR(50)='SPA', @SOLICITUD INT ,@PER VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT FC.*, C.GRUPO GRUPO_ORIGEN_CALC_DESGLOSE, ISNULL(CC.ESCRITURA,1) ESCRITURA,  CASE WHEN CCV.VISIBLE=0 THEN 0 ELSE ISNULL(CC.VISIBLE,1)  END AS VISIBLE" & vbCrLf
sConsulta = sConsulta & "  FROM FORM_CAMPO FC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN FORM_GRUPO FG WITH (NOLOCK)   ON FC.GRUPO=FG.ID  AND FG.FORMULARIO=@FORMULARIO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN SOLICITUD S WITH (NOLOCK)  ON FG.FORMULARIO=S.FORMULARIO AND S.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_BLOQUE WITH (NOLOCK)  ON S.WORKFLOW=PM_BLOQUE.WORKFLOW AND PM_BLOQUE.TIPO=1 " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_ROL WITH (NOLOCK)  ON S.WORKFLOW=PM_ROL.WORKFLOW AND PM_ROL.TIPO=4" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN VIEW_ROL_PETICIONARIO VW WITH (NOLOCK)  ON VW.ID=PM_ROL.ID AND VW.SOLICITUD=S.ID AND VW.COD=@PER" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_ROL_BLOQUE WITH (NOLOCK)  ON PM_ROL.ID=PM_ROL_BLOQUE.ROL AND PM_BLOQUE.ID=PM_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN FORM_CAMPO C WITH (NOLOCK)  ON FC.ORIGEN_CALC_DESGLOSE=C.ID      " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PM_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON FC.ID = CC.CAMPO AND CC.BLOQUE=PM_BLOQUE.ID AND CC.ROL =  PM_ROL.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PM_CONF_CUMP_BLOQUE CCV WITH (NOLOCK)  ON  CCV.CAMPO=C.ORIGEN_CALC_DESGLOSE AND CCV.BLOQUE=PM_BLOQUE.ID  AND CCV.ROL =PM_ROL.ID " & vbCrLf
sConsulta = sConsulta & "WHERE FC.ES_SUBCAMPO = 0   AND FC.ID_CALCULO IS NOT NULL  AND (FC.SUBTIPO =2 OR FC.TIPO =3) " & vbCrLf
sConsulta = sConsulta & "ORDER BY FC.IMPORTE_WORKFL,FC.ORDEN_CALCULO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADFIELDSDESGLOSECALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADFIELDSDESGLOSECALC @CAMPO INT, @SOLICITUD INT,@PER VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT C.* , CC.VISIBLE" & vbCrLf
sConsulta = sConsulta & "  FROM FORM_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN FORM_GRUPO G WITH (NOLOCK)  ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN SOLICITUD S WITH (NOLOCK)  ON G.FORMULARIO=S.FORMULARIO AND S.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_BLOQUE WITH (NOLOCK)  ON S.WORKFLOW=PM_BLOQUE.WORKFLOW AND PM_BLOQUE.TIPO=1 " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_ROL WITH (NOLOCK)  ON S.WORKFLOW=PM_ROL.WORKFLOW AND PM_ROL.TIPO=4" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN VIEW_ROL_PETICIONARIO VW WITH (NOLOCK)  ON VW.ID=PM_ROL.ID AND VW.SOLICITUD=S.ID AND VW.COD=@PER" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_ROL_BLOQUE WITH (NOLOCK)  ON PM_ROL.ID=PM_ROL_BLOQUE.ROL AND PM_BLOQUE.ID=PM_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN DESGLOSE D WITH (NOLOCK)   ON C.ID=D.CAMPO_HIJO  AND D.CAMPO_PADRE=@CAMPO" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PM_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON C.ID = CC.CAMPO AND CC.BLOQUE=PM_BLOQUE.ID AND CC.ROL =  PM_ROL.ID" & vbCrLf
sConsulta = sConsulta & " WHERE C.ID_CALCULO IS NOT NULL    AND (C.SUBTIPO =2 OR C.TIPO =3) " & vbCrLf
sConsulta = sConsulta & "ORDER BY C.ORDEN_CALCULO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* FROM LINEA_DESGLOSE LD INNER JOIN FORM_CAMPO C ON C.ID = LD.CAMPO_HIJO WHERE LD.CAMPO_PADRE=@CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTFIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADINSTFIELDSCALC @INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50)='SPA',@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ROL INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @BLOQUE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENBLOQUE_ACT TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ROL =NULL" & vbCrLf
sConsulta = sConsulta & "SET @BLOQUE=NULL" & vbCrLf
sConsulta = sConsulta & "SET @ENBLOQUE_ACT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC FSPM_IDENTIFICAR_ROL @USU=@USU, @PROVE=@PROVE, @INSTANCIA=@INSTANCIA, @ROL=@ROL OUTPUT , @BLOQUE= @BLOQUE OUTPUT,@ENBLOQUE_ACT=@ENBLOQUE_ACT OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT C.ID ID_CAMPO, CD.*, CC2.ID ID_CAMPO_ORIGEN_CALC_DESGLOSE, CCD2.GRUPO GRUPO_ORIGEN_CALC_DESGLOSE, ISNULL(CCV.VISIBLE,CC.VISIBLE)  AS VISIBLE, C.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_CAMPO_DEF CD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            ON CD.GRUPO =CG.ID" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            ON C.COPIA_CAMPO_DEF = CD.ID" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (COPIA_CAMPO CC2 WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN COPIA_CAMPO_DEF CCD2 WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         ON CC2.COPIA_CAMPO_DEF = CCD2.ID" & vbCrLf
sConsulta = sConsulta & "                        AND CC2.NUM_VERSION=@VERSION)" & vbCrLf
sConsulta = sConsulta & "            ON CC2.COPIA_CAMPO_DEF = CD.ORIGEN_CALC_DESGLOSE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON CD.ID = CC.CAMPO AND CC.BLOQUE=@BLOQUE  AND CC.ROL =@ROL  '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_CONF_CUMP_BLOQUE CCV WITH (NOLOCK)  ON CD.ORIGEN_CALC_DESGLOSE = CCV.CAMPO AND CCV.BLOQUE=@BLOQUE  AND CCV.ROL =@ROL  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +" & vbCrLf
sConsulta = sConsulta & "' WHERE (CG.INSTANCIA = @INSTANCIA  " & vbCrLf
sConsulta = sConsulta & " AND CD.ID_CALCULO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & " AND (CD.SUBTIPO = 2 OR CD.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & " AND CD.ES_SUBCAMPO=0" & vbCrLf
sConsulta = sConsulta & "  AND C.NUM_VERSION = @VERSION)" & vbCrLf
sConsulta = sConsulta & "ORDER BY CD.IMPORTE_WORKFL,CD.ORDEN_CALCULO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--  AND CD.ES_SUBCAMPO=0" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL, @BLOQUE INT , @ROL INT', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@BLOQUE=@BLOQUE, @ROL=@ROL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADINSTFIELDSDESGLOSECALC @CAMPO INT , @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ROL INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @BLOQUE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENBLOQUE_ACT TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ROL =NULL" & vbCrLf
sConsulta = sConsulta & "SET @BLOQUE=NULL" & vbCrLf
sConsulta = sConsulta & "SET @ENBLOQUE_ACT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC FSPM_IDENTIFICAR_ROL @USU=@USU, @PROVE=@PROVE, @INSTANCIA=@INSTANCIA, @ROL=@ROL OUTPUT , @BLOQUE= @BLOQUE OUTPUT,@ENBLOQUE_ACT=@ENBLOQUE_ACT OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL = '" & vbCrLf
sConsulta = sConsulta & "SELECT C.ID ID_CAMPO, CCD.*, CC.VISIBLE" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_CAMPO_DEF CCD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN COPIA_DESGLOSE CD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           ON C.ID = CD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "                          AND CD.CAMPO_PADRE = @CAMPO)" & vbCrLf
sConsulta = sConsulta & "            ON CCD.ID = C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON CCD.ID = CC.CAMPO AND CC.BLOQUE=@BLOQUE  AND CC.ROL =@ROL  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & " WHERE CCD.ID_CALCULO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   AND (CCD.SUBTIPO = 2 or CCD.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "ORDER BY CCD.ORDEN_CALCULO" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @CAMPO INT,@BLOQUE INT, @ROL INT', @INSTANCIA=@INSTANCIA, @CAMPO = @CAMPO,@BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* FROM COPIA_LINEA_DESGLOSE LD WITH (NOLOCK)   WHERE LD.CAMPO_PADRE=@CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETDESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_GETDESGLOSE @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDI IS NULL " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SELECT C.ID, C.GRUPO, C.DEN_ENG,C.DEN_SPA,C.DEN_GER, C.AYUDA_ENG,  C.AYUDA_SPA, C.AYUDA_GER,C.TIPO, C.SUBTIPO, C.INTRO, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.MINNUM, C.MAXNUM, C.MINFEC, C.MAXFEC" & vbCrLf
sConsulta = sConsulta & "           , C.ID_ATRIB_GS, C.TIPO_CAMPO_GS, C.ID_CALCULO, C.FORMULA, C.ORDEN_CALCULO" & vbCrLf
sConsulta = sConsulta & "       , LINEA.LINEA ,ISNULL(CC.ESCRITURA,1) ESCRITURA, ISNULL(CC.VISIBLE ,1) VISIBLE, CC.OBLIGATORIO, CC.ES_ESTADO,ISNULL(CC.ORDEN,C.ORDEN) ORDEN1,C.ANYADIR_ART,DEF_ATRIB.VALIDACION_ERP,C.CARGAR_ULT_ADJ" & vbCrLf
sConsulta = sConsulta & "        FROM FORM_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CONF_CUMPLIMENTACION CC WITH (NOLOCK)  ON C.ID = CC.CAMPO AND CC.PASO IS NULL  AND CC.SOLIC_PROVE = 1 AND CC.SOLICITUD=@SOLICITUD " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEF_ATRIB WITH (NOLOCK)  ON DEF_ATRIB.ID = C.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "    inner JOIN DESGLOSE D WITH (NOLOCK)  ON C.ID = D.CAMPO_HIJO , " & vbCrLf
sConsulta = sConsulta & "   (SELECT MAX(LINEA) LINEA FROM LINEA_DESGLOSE WITH (NOLOCK)  WHERE CAMPO_PADRE = @ID) LINEA  " & vbCrLf
sConsulta = sConsulta & "       WHERE D.CAMPO_PADRE = @ID AND C.ES_SUBCAMPO=1  " & vbCrLf
sConsulta = sConsulta & "    UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT C.ID, C.GRUPO, (SELECT TEXT_ENG FROM WEBFSWSTEXT WHERE MODULO=44 AND ID=11) DEN_ENG,(SELECT TEXT_SPA FROM WEBFSWSTEXT WHERE MODULO=44 AND ID=11) DEN_SPA,(SELECT TEXT_GER FROM WEBFSWSTEXT WHERE MODULO=44 AND ID=11) DEN_GER, C.AYUDA_ENG,  C.AYUDA_SPA, C.AYUDA_GER,C.TIPO, 6 SUBTIPO, C.INTRO, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.MINNUM, C.MAXNUM, C.MINFEC, C.MAXFEC" & vbCrLf
sConsulta = sConsulta & "               , C.ID_ATRIB_GS, 42 TIPO_CAMPO_GS, C.ID_CALCULO, C.FORMULA, C.ORDEN_CALCULO" & vbCrLf
sConsulta = sConsulta & "               ,(SELECT MAX(LINEA) LINEA FROM LINEA_DESGLOSE WHERE CAMPO_PADRE = @ID) LINEA ,ISNULL(CC.ESCRITURA,1) ESCRITURA, ISNULL(CC.VISIBLE ,1) VISIBLE, CC.OBLIGATORIO, " & vbCrLf
sConsulta = sConsulta & "       CC.ES_ESTADO,ISNULL(CC.ORDEN,C.ORDEN) ORDEN1,C.ANYADIR_ART,DEF_ATRIB.VALIDACION_ERP,C.CARGAR_ULT_ADJ" & vbCrLf
sConsulta = sConsulta & "        FROM CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN FORM_CAMPO C WITH (NOLOCK)  ON C.ID=CC.CAMPO" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN DEF_ATRIB WITH (NOLOCK)  ON DEF_ATRIB.ID = C.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "       WHERE CC.SOLICITUD=@SOLICITUD AND CC.ES_ESTADO=1 AND  CC.CAMPO = @ID AND CC.SOLIC_PROVE=1" & vbCrLf
sConsulta = sConsulta & "       ORDER BY ORDEN1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT L.* FROM CAMPO_VALOR_LISTA L WITH (NOLOCK)  INNER JOIN FORM_CAMPO C WITH (NOLOCK)  ON L.CAMPO = C.ID INNER JOIN DESGLOSE D WITH (NOLOCK)  ON C.ID = D.CAMPO_HIJO  WHERE D.CAMPO_PADRE = @ID AND C.ES_SUBCAMPO=1 ORDER BY L.CAMPO, L.ORDEN" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT C.ID, C.GRUPO, C.DEN_' + @IDI + ', AYUDA_' + @IDI + ', C.TIPO, C.SUBTIPO, C.INTRO, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.MINNUM, C.MAXNUM, C.MINFEC, C.MAXFEC, C.ID_ATRIB_GS, C.TIPO_CAMPO_GS, LINEA.LINEA , ISNULL(CC.ESCRITURA,1) ESCRITURA, ISNULL(CC.VISIBLE ,1) VISIBLE, CC.OBLIGATORIO, CC.ES_ESTADO,ISNULL(CC.ORDEN,C.ORDEN) ORDEN1, C.ID_CALCULO, C.FORMULA, C.ORDEN_CALCULO,C.ANYADIR_ART,DEF_ATRIB.VALIDACION_ERP,C.CARGAR_ULT_ADJ" & vbCrLf
sConsulta = sConsulta & "                         FROM FORM_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                LEFT JOIN CONF_CUMPLIMENTACION CC WITH (NOLOCK)  ON C.ID = CC.CAMPO AND CC.PASO IS NULL   AND CC.SOLIC_PROVE = 1 AND CC.SOLICITUD=@SOLICITUD " & vbCrLf
sConsulta = sConsulta & "                                   LEFT OUTER JOIN DEF_ATRIB WITH (NOLOCK)  ON DEF_ATRIB.ID = C.ID_ATRIB_GS INNER JOIN DESGLOSE D ON C.ID = D.CAMPO_HIJO  " & vbCrLf
sConsulta = sConsulta & "              , (SELECT MAX(LINEA) LINEA FROM LINEA_DESGLOSE WITH (NOLOCK)  WHERE CAMPO_PADRE = @ID) LINEA  " & vbCrLf
sConsulta = sConsulta & "                          WHERE D.CAMPO_PADRE = @ID AND C.ES_SUBCAMPO=1 " & vbCrLf
sConsulta = sConsulta & "         UNION " & vbCrLf
sConsulta = sConsulta & "               SELECT C.ID, C.GRUPO, (SELECT TEXT_' + @IDI + ' FROM WEBFSWSTEXT WHERE MODULO=44 AND ID=11) DEN_' + @IDI + ', AYUDA_' + @IDI + ', C.TIPO, 6 SUBTIPO, C.INTRO, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.MINNUM, C.MAXNUM, C.MINFEC, C.MAXFEC,  C.ID_ATRIB_GS, 42 TIPO_CAMPO_GS," & vbCrLf
sConsulta = sConsulta & "               (SELECT MAX(LINEA) LINEA FROM LINEA_DESGLOSE WHERE CAMPO_PADRE = @ID) LINEA ,ISNULL(CC.ESCRITURA,1) ESCRITURA, ISNULL(CC.VISIBLE ,1) VISIBLE, CC.OBLIGATORIO, CC.ES_ESTADO,ISNULL(CC.ORDEN,C.ORDEN) ORDEN1, C.ID_CALCULO, C.FORMULA, C.ORDEN_CALCULO ,C.ANYADIR_ART,DEF_ATRIB.VALIDACION_ERP,C.CARGAR_ULT_ADJ" & vbCrLf
sConsulta = sConsulta & "                   FROM CONF_CUMPLIMENTACION CC WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "               LEFT JOIN FORM_CAMPO C WITH (NOLOCK)  ON C.ID=CC.CAMPO" & vbCrLf
sConsulta = sConsulta & "               LEFT OUTER JOIN DEF_ATRIB WITH (NOLOCK)  ON DEF_ATRIB.ID = C.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "               WHERE CC.SOLICITUD=@SOLICITUD  AND CC.ES_ESTADO=1 AND  CC.CAMPO = @ID AND CC.SOLIC_PROVE=1" & vbCrLf
sConsulta = sConsulta & "                   ORDER BY ORDEN1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@ID INT, @SOLICITUD INT', @ID=@ID,@SOLICITUD=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT  L.ORDEN, L.VALOR_NUM, L.VALOR_TEXT_' + @IDI + ', L.VALOR_FEC , L.CAMPO, L.CAMPO ID FROM CAMPO_VALOR_LISTA L WITH (NOLOCK)  INNER JOIN FORM_CAMPO C WITH (NOLOCK)  ON L.CAMPO = C.ID INNER JOIN DESGLOSE D WITH (NOLOCK)  ON C.ID = D.CAMPO_HIJO  WHERE D.CAMPO_PADRE = @ID AND C.ES_SUBCAMPO=1 ORDER BY L.CAMPO, L.ORDEN'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* , C.TIPO_CAMPO_GS" & vbCrLf
sConsulta = sConsulta & ", CASE WHEN C.TIPO_CAMPO_GS = 118 THEN " & vbCrLf
sConsulta = sConsulta & "(SELECT ART4.GENERICO FROM ART4 WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEA_DESGLOSE LDART WITH (NOLOCK)  ON LDART.CAMPO_HIJO = LD.CAMPO_HIJO-1 AND LDART.LINEA = LD.LINEA" & vbCrLf
sConsulta = sConsulta & "WHERE COD = LDART.VALOR_TEXT   ) end GENERICO" & vbCrLf
sConsulta = sConsulta & "FROM LINEA_DESGLOSE LD WITH (NOLOCK)  INNER JOIN FORM_CAMPO C WITH (NOLOCK)  ON C.ID = LD.CAMPO_HIJO WHERE LD.CAMPO_PADRE=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LDA.ID, LDA.CAMPO_PADRE, LDA.CAMPO_HIJO, LDA.LINEA,  P.NOM + ' ' + P.APE NOMBRE, LDA.NOM , LDA.IDIOMA, LDA.DATASIZE, LDA.PER, LDA.COMENT, LDA.FECALTA " & vbCrLf
sConsulta = sConsulta & "  FROM LINEA_DESGLOSE_ADJUN LDA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN PER P  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON LDA.PER = P.COD " & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_PADRE = @ID ORDER BY CAMPO_HIJO, LINEA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETINSTFIELD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETINSTFIELD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_GETINSTFIELD  @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT DISTINCT S.DEN_' + @IDI + '  DENSOL_' + @IDI + ', CG.DEN_' + @IDI + ' DENGRUPO_' + @IDI + ', CG.INSTANCIA INSTANCIA, CCD.ID, CG.ID GRUPO, CCD.DEN_' + @IDI + ', CCD.AYUDA_' + @IDI + ', CCD.TIPO, CCD.SUBTIPO, CCD.INTRO, CC.VALOR_NUM, CC.VALOR_TEXT, CC.VALOR_FEC, CC.VALOR_BOOL, CCD.MINNUM, CCD.MAXNUM, CCD.MINFEC, CCD.MAXFEC, CCD.ORDEN, CCD.ID_ATRIB_GS, CCD.TIPO_CAMPO_GS, CCD.LINEAS_PRECONF, CCD.FORMULA, cc2.id ORIGEN_CALC_DESGLOSE, CD.CAMPO_PADRE, CC.COPIA_CAMPO_DEF, DEF_ATRIB.VALIDACION_ERP" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_CAMPO_DEF CCD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN COPIA_GRUPO CG WITH (NOLOCK)  ON CCD.GRUPO = CG.ID" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN COPIA_CAMPO CC WITH (NOLOCK)  ON CCD.ID = CC.COPIA_CAMPO_DEF AND CC.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN INSTANCIA WITH (NOLOCK)  ON CC.INSTANCIA=INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN SOLICITUD S WITH (NOLOCK)  ON INSTANCIA.SOLICITUD=S.ID    " & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN COPIA_CAMPO CC2 WITH (NOLOCK)  ON CC2.COPIA_CAMPO_DEF = CCD.ORIGEN_CALC_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN COPIA_DESGLOSE CD WITH (NOLOCK)  ON CC.ID = CD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "             LEFT JOIN DEF_ATRIB WITH (NOLOCK)  ON DEF_ATRIB.ID = CCD.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "                   WHERE CG.INSTANCIA = @INSTANCIA  AND CC.ID = @ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@ID INT, @INSTANCIA INT', @ID=@ID, @INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT L.CAMPO_DEF, L.ORDEN, L.VALOR_NUM, L.VALOR_TEXT_' + @IDI + ', L.VALOR_FEC, L.FECACT  " & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_CAMPO_VALOR_LISTA L  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN COPIA_CAMPO_DEF C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON L.CAMPO_DEF = C.ID " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                               ON C.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                              AND CC.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "                  WHERE CC.ID = @ID " & vbCrLf
sConsulta = sConsulta & "                  ORDER BY L.ORDEN '" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@ID INT, @INSTANCIA INT', @ID=@ID, @INSTANCIA=@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETINSTINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETINSTINVISIBLEFIELDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_GETINSTINVISIBLEFIELDS @IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL    AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VERSION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADADA AS TINYINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTINATARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PETICIONARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCRITURA_CERTIF AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYCONFCUMP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CUMP_PROVE TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @INSTANCIA =INSTANCIA,  @VERSION =NUM_VERSION FROM COPIA_CAMPO WITH (NOLOCK) WHERE ID = @IDCAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADO= ESTADO," & vbCrLf
sConsulta = sConsulta & "       @TRASLADADA=TRASLADADA," & vbCrLf
sConsulta = sConsulta & "       @DESTINATARIO=CASE WHEN DESTINATARIO IS NULL THEN PER ELSE DESTINATARIO END , " & vbCrLf
sConsulta = sConsulta & "       @PETICIONARIO = PETICIONARIO," & vbCrLf
sConsulta = sConsulta & "       @CUMP_PROVE = CUMP_PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN INSTANCIA_EST  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON INSTANCIA.INSTANCIA_EST=INSTANCIA_EST.ID " & vbCrLf
sConsulta = sConsulta & "               AND DESTINATARIO IS NOT NULL  " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA.ID=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE  @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE  @SQLCOMMON NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT C.ID, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.VALOR_NUM, CD.TIPO_CAMPO_GS, ISNULL(CD.SUBTIPO,0) SUBTIPO  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON C.COPIA_CAMPO_DEF=CD.ID '" & vbCrLf
sConsulta = sConsulta & "set @SQLCOMMON=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO = 0" & vbCrLf
sConsulta = sConsulta & "    SET @SQLCOMMON = @SQLCOMMON + '   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                  ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.INSTANCIA=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1-- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "          SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'   " & vbCrLf
sConsulta = sConsulta & "          ELSE-- ha sido trasladada al proveedor y la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            IF @CUMP_PROVE = 1 --usar configuracion de cumplimentaci�n del proveedor" & vbCrLf
sConsulta = sConsulta & "              SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON IE.INSTANCIA = INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.ID = INSTANCIA.INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.DESTINATARIO_PROV = @PROVE" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE =2'   " & vbCrLf
sConsulta = sConsulta & "            ELSE --usar la configuragcion de cumplimentacion del aprobador que ha trasladado la instancia" & vbCrLf
sConsulta = sConsulta & "              SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "      ELSE --no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB -- a no ser que el peticionario sea aprobador, sacar�a la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "          ELSE -- configuracion del peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo el aprobador por lo que sacampos la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "          SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "    ELSE -- NO EST� PENDIENTE DE APROBAR" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200 or @ESTADO=300" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL  --  -- Se est� cargando como certificado por el proveedor" & vbCrLf
sConsulta = sConsulta & "          SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE =2'    " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1'    " & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo otra persona " & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        ELSE --Est� en otro estado" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO --la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo un aprobador por lo que se coje la conf-cumplimentacion que ten�a cuando la aprob�" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                  INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON ISNULL(INSTANCIA.APROB, (SELECT TOP 1 APROB FROM INSTANCIA_EST WITH (NOLOCK)  WHERE INSTANCIA = @ID AND PER = @USU))=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0 AND  CC.VISIBLE=0' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* ,CC.VISIBLE, CD.TIPO_CAMPO_GS,CVL.VALOR_TEXT_SPA LISTA_TEXT " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON LD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON CD.ID = C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_DESGLOSE CDESG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON C.ID = CDESG.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN COPIA_CAMPO_VALOR_LISTA CVL  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      ON CVL.CAMPO_DEF=C.COPIA_CAMPO_DEF AND CVL.ORDEN=LD.VALOR_NUM '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=1' " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN" & vbCrLf
sConsulta = sConsulta & "           (SELECT DISTINCT C.ID" & vbCrLf
sConsulta = sConsulta & "              FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         ON C.COPIA_CAMPO_DEF=CD.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=1' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ') DESG ON LD.CAMPO_PADRE = DESG.ID' " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETINVISIBLEFIELDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_GETINVISIBLEFIELDS @ID INT,@IDI AS  VARCHAR(50) = 'SPA'    AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FORMULARIO AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FORMULARIO = (SELECT FORMULARIO FROM SOLICITUD WITH (NOLOCK) WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "--CAMPOS SIMPLES INVISIBLES" & vbCrLf
sConsulta = sConsulta & "SELECT C.ID, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.TIPO_CAMPO_GS, ISNULL(C.SUBTIPO,0) SUBTIPO" & vbCrLf
sConsulta = sConsulta & "  FROM FORM_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN FORM_GRUPO G  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON C.ID = CC.CAMPO " & vbCrLf
sConsulta = sConsulta & "            AND CC.PASO IS NULL" & vbCrLf
sConsulta = sConsulta & "            AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "            AND CC.SOLICITUD=@ID" & vbCrLf
sConsulta = sConsulta & " WHERE G.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   AND C.ES_SUBCAMPO=0 " & vbCrLf
sConsulta = sConsulta & "   AND CC.VISIBLE=0 " & vbCrLf
sConsulta = sConsulta & "ORDER BY G.ID, CC.ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--LINEAS DE DESGLOSE DE CAMPOS INVISIBLES QUE EST�N EN DESGLOSES" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* ,CC.VISIBLE, C.TIPO_CAMPO_GS,CVL.VALOR_TEXT_SPA LISTA_TEXT" & vbCrLf
sConsulta = sConsulta & "  FROM LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN FORM_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            ON C.ID = LD.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN FORM_GRUPO G  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON C.ID = CC.CAMPO " & vbCrLf
sConsulta = sConsulta & "            AND CC.PASO IS NULL" & vbCrLf
sConsulta = sConsulta & "            AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "            AND CC.SOLICITUD=@ID" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN CAMPO_VALOR_LISTA CVL  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   ON CVL.CAMPO=C.ID AND CVL.ORDEN=LD.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & " WHERE G.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   AND C.ES_SUBCAMPO=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CAMPOS DE LOS DESGLOSES INVISIBLES" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* " & vbCrLf
sConsulta = sConsulta & "  FROM LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN" & vbCrLf
sConsulta = sConsulta & "           (SELECT C.ID" & vbCrLf
sConsulta = sConsulta & "              FROM FORM_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN FORM_GRUPO G  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         ON C.ID = CC.CAMPO " & vbCrLf
sConsulta = sConsulta & "                        AND CC.PASO IS NULL" & vbCrLf
sConsulta = sConsulta & "                        AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                        AND CC.SOLICITUD=@ID" & vbCrLf
sConsulta = sConsulta & "             WHERE G.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "               AND C.ES_SUBCAMPO=0 " & vbCrLf
sConsulta = sConsulta & "               AND CC.VISIBLE=0 " & vbCrLf
sConsulta = sConsulta & "               AND C.SUBTIPO = 9) DESG" & vbCrLf
sConsulta = sConsulta & "            ON LD.CAMPO_PADRE = DESG.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_LOADFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_LOADFIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_LOADFIELDSCALC @FORMULARIO INT,  @IDI VARCHAR(50)='SPA', @SOLICITUD INT  AS" & vbCrLf
sConsulta = sConsulta & "SELECT FC.*, C.GRUPO GRUPO_ORIGEN_CALC_DESGLOSE, ISNULL(CC.ESCRITURA,1) ESCRITURA, ISNULL(CC.VISIBLE,1) VISIBLE" & vbCrLf
sConsulta = sConsulta & "  FROM FORM_CAMPO FC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             INNER JOIN FORM_GRUPO FG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                     ON FC.GRUPO=FG.ID " & vbCrLf
sConsulta = sConsulta & "                    AND FG.FORMULARIO=@FORMULARIO" & vbCrLf
sConsulta = sConsulta & "              LEFT JOIN FORM_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                     ON FC.ORIGEN_CALC_DESGLOSE=C.ID  " & vbCrLf
sConsulta = sConsulta & "              LEFT JOIN CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                     ON FC.ID = CC.CAMPO " & vbCrLf
sConsulta = sConsulta & "                    AND CC.PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                    AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                    AND CC.SOLICITUD=@SOLICITUD " & vbCrLf
sConsulta = sConsulta & "WHERE FC.ES_SUBCAMPO = 0 " & vbCrLf
sConsulta = sConsulta & "   AND FC.ID_CALCULO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   AND (FC.SUBTIPO =2 OR FC.TIPO =3) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ORDER BY FC.ORDEN_CALCULO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_LOADFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_LOADFIELDSDESGLOSECALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_LOADFIELDSDESGLOSECALC @CAMPO INT, @SOLICITUD INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT C.* , CC.VISIBLE" & vbCrLf
sConsulta = sConsulta & "  FROM FORM_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN DESGLOSE D WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              ON C.ID=D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "             AND D.CAMPO_PADRE=@CAMPO" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              ON C.ID = CC.CAMPO " & vbCrLf
sConsulta = sConsulta & "             AND CC.PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "             AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "             AND CC.SOLICITUD=@SOLICITUD " & vbCrLf
sConsulta = sConsulta & " WHERE C.ID_CALCULO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   AND (C.SUBTIPO =2 OR C.TIPO =3) " & vbCrLf
sConsulta = sConsulta & "ORDER BY C.ORDEN_CALCULO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* FROM LINEA_DESGLOSE LD INNER JOIN FORM_CAMPO C WITH (NOLOCK)  ON C.ID = LD.CAMPO_HIJO WHERE LD.CAMPO_PADRE=@CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_LOADINSTFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_LOADINSTFIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_LOADINSTFIELDSCALC @INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50)='SPA',@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADADA AS TINYINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTINATARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PETICIONARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCRITURA_CERTIF AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYCONFCUMP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CUMP_PROVE TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @APROB = (select CS.PER FROM COPIA_SUBPASO CS WITH (NOLOCK)  INNER JOIN INSTANCIA I WITH (NOLOCK)  ON CS.ID = I.APROB WHERE I.ID= @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los campo de la instancia:" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADO= ESTADO," & vbCrLf
sConsulta = sConsulta & "       @TRASLADADA=TRASLADADA," & vbCrLf
sConsulta = sConsulta & "       @DESTINATARIO=CASE WHEN DESTINATARIO IS NULL THEN PER ELSE DESTINATARIO END , " & vbCrLf
sConsulta = sConsulta & "       @PETICIONARIO = PETICIONARIO," & vbCrLf
sConsulta = sConsulta & "       @CUMP_PROVE = CUMP_PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN INSTANCIA_EST  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON INSTANCIA.INSTANCIA_EST=INSTANCIA_EST.ID " & vbCrLf
sConsulta = sConsulta & "               AND DESTINATARIO IS NOT NULL  " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA.ID=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='" & vbCrLf
sConsulta = sConsulta & "SELECT C.ID ID_CAMPO, CD.*, CC2.ID ID_CAMPO_ORIGEN_CALC_DESGLOSE, CCD2.GRUPO GRUPO_ORIGEN_CALC_DESGLOSE, CC.VISIBLE, C.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_CAMPO_DEF CD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            ON CD.GRUPO =CG.ID" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            ON C.COPIA_CAMPO_DEF = CD.ID" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (COPIA_CAMPO CC2 WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN COPIA_CAMPO_DEF CCD2" & vbCrLf
sConsulta = sConsulta & "                         ON CC2.COPIA_CAMPO_DEF = CCD2.ID" & vbCrLf
sConsulta = sConsulta & "                        AND CC2.NUM_VERSION=@VERSION)" & vbCrLf
sConsulta = sConsulta & "            ON CC2.COPIA_CAMPO_DEF = CD.ORIGEN_CALC_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO = 0" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                  ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.INSTANCIA=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1-- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'   " & vbCrLf
sConsulta = sConsulta & "          ELSE-- ha sido trasladada al proveedor y la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            IF @CUMP_PROVE = 1 --usar configuracion de cumplimentaci�n del proveedor" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON IE.INSTANCIA = INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.ID = INSTANCIA.INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.DESTINATARIO_PROV = @PROVE" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE =2'   " & vbCrLf
sConsulta = sConsulta & "            ELSE --usar la configuragcion de cumplimentacion del aprobador que ha trasladado la instancia" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "      ELSE --no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB -- a no ser que el peticionario sea aprobador, sacar�a la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "          ELSE -- configuracion del peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo el aprobador por lo que sacampos la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "    ELSE -- NO EST� PENDIENTE DE APROBAR" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL  --  -- Se est� cargando como certificado por el proveedor" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE =2'    " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1'    " & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo otra persona " & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "      ELSE  --est� en otro estado" & vbCrLf
sConsulta = sConsulta & "        IF @ESTADO = 300 -- es una no conformidad" & vbCrLf
sConsulta = sConsulta & "          IF @USU = null --la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =2'    " & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1'    " & vbCrLf
sConsulta = sConsulta & "        ELSE --Est� en otro estado" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO --la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo un aprobador por lo que se coje la conf-cumplimentacion que ten�a cuando la aprob�" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                  INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON ISNULL(INSTANCIA.APROB, (SELECT TOP 1 APROB FROM INSTANCIA_EST WITH (NOLOCK)  WHERE INSTANCIA = @ID AND PER = @USU))=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +" & vbCrLf
sConsulta = sConsulta & "' WHERE CG.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND CD.ES_SUBCAMPO=0" & vbCrLf
sConsulta = sConsulta & "  AND CD.ID_CALCULO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  AND (CD.SUBTIPO = 2 OR CD.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "  AND C.NUM_VERSION = @VERSION" & vbCrLf
sConsulta = sConsulta & "ORDER BY CD.ORDEN_CALCULO'" & vbCrLf
sConsulta = sConsulta & "print @sql" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETINSTINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETINSTINVISIBLEFIELDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_GETINSTINVISIBLEFIELDS @IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL    AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VERSION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADADA AS TINYINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTINATARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PETICIONARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCRITURA_CERTIF AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYCONFCUMP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CUMP_PROVE TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @INSTANCIA =INSTANCIA,  @VERSION =NUM_VERSION FROM COPIA_CAMPO WITH (NOLOCK) WHERE ID = @IDCAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADO= ESTADO," & vbCrLf
sConsulta = sConsulta & "       @TRASLADADA=TRASLADADA," & vbCrLf
sConsulta = sConsulta & "       @DESTINATARIO=CASE WHEN DESTINATARIO IS NULL THEN PER ELSE DESTINATARIO END , " & vbCrLf
sConsulta = sConsulta & "       @PETICIONARIO = PETICIONARIO," & vbCrLf
sConsulta = sConsulta & "       @CUMP_PROVE = CUMP_PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN INSTANCIA_EST  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON INSTANCIA.INSTANCIA_EST=INSTANCIA_EST.ID " & vbCrLf
sConsulta = sConsulta & "               AND DESTINATARIO IS NOT NULL  " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA.ID=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE  @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE  @SQLCOMMON NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT C.ID, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.VALOR_NUM, CD.TIPO_CAMPO_GS, ISNULL(CD.SUBTIPO,0) SUBTIPO  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON C.COPIA_CAMPO_DEF=CD.ID '" & vbCrLf
sConsulta = sConsulta & "set @SQLCOMMON=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO = 0" & vbCrLf
sConsulta = sConsulta & "    SET @SQLCOMMON = @SQLCOMMON + '   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                  ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.INSTANCIA=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1-- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "          SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'   " & vbCrLf
sConsulta = sConsulta & "          ELSE-- ha sido trasladada al proveedor y la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            IF @CUMP_PROVE = 1 --usar configuracion de cumplimentaci�n del proveedor" & vbCrLf
sConsulta = sConsulta & "              SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON IE.INSTANCIA = INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.ID = INSTANCIA.INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.DESTINATARIO_PROV = @PROVE" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE =2'   " & vbCrLf
sConsulta = sConsulta & "            ELSE --usar la configuragcion de cumplimentacion del aprobador que ha trasladado la instancia" & vbCrLf
sConsulta = sConsulta & "              SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "      ELSE --no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB -- a no ser que el peticionario sea aprobador, sacar�a la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "          ELSE -- configuracion del peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo el aprobador por lo que sacampos la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "          SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "    ELSE -- NO EST� PENDIENTE DE APROBAR" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200 or @ESTADO=300" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL  --  -- Se est� cargando como certificado por el proveedor" & vbCrLf
sConsulta = sConsulta & "          SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE =2'    " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1'    " & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo otra persona " & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        ELSE --Est� en otro estado" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO --la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQLCOMMON = @SQLCOMMON + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo un aprobador por lo que se coje la conf-cumplimentacion que ten�a cuando la aprob�" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLCOMMON = @SQLCOMMON + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                  INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON ISNULL(INSTANCIA.APROB, (SELECT TOP 1 APROB FROM INSTANCIA_EST WITH (NOLOCK)  WHERE INSTANCIA = @ID AND PER = @USU))=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0 AND  CC.VISIBLE=0' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* ,CC.VISIBLE, CD.TIPO_CAMPO_GS,CVL.VALOR_TEXT_SPA LISTA_TEXT " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON LD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON CD.ID = C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_DESGLOSE CDESG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON C.ID = CDESG.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN COPIA_CAMPO_VALOR_LISTA CVL  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      ON CVL.CAMPO_DEF=C.COPIA_CAMPO_DEF AND CVL.ORDEN=LD.VALOR_NUM '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=1' " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN" & vbCrLf
sConsulta = sConsulta & "           (SELECT DISTINCT C.ID" & vbCrLf
sConsulta = sConsulta & "              FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         ON C.COPIA_CAMPO_DEF=CD.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=1' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ') DESG ON LD.CAMPO_PADRE = DESG.ID' " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETINVISIBLEFIELDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_GETINVISIBLEFIELDS @ID INT,@IDI AS  VARCHAR(50) = 'SPA'    AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FORMULARIO AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FORMULARIO = (SELECT FORMULARIO FROM SOLICITUD WITH (NOLOCK) WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "--CAMPOS SIMPLES INVISIBLES" & vbCrLf
sConsulta = sConsulta & "SELECT C.ID, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.TIPO_CAMPO_GS, ISNULL(C.SUBTIPO,0) SUBTIPO" & vbCrLf
sConsulta = sConsulta & "  FROM FORM_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN FORM_GRUPO G  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON C.ID = CC.CAMPO " & vbCrLf
sConsulta = sConsulta & "            AND CC.PASO IS NULL" & vbCrLf
sConsulta = sConsulta & "            AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "            AND CC.SOLICITUD=@ID" & vbCrLf
sConsulta = sConsulta & " WHERE G.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   AND C.ES_SUBCAMPO=0 " & vbCrLf
sConsulta = sConsulta & "   AND CC.VISIBLE=0 " & vbCrLf
sConsulta = sConsulta & "ORDER BY G.ID, CC.ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--LINEAS DE DESGLOSE DE CAMPOS INVISIBLES QUE EST�N EN DESGLOSES" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* ,CC.VISIBLE, C.TIPO_CAMPO_GS,CVL.VALOR_TEXT_SPA LISTA_TEXT" & vbCrLf
sConsulta = sConsulta & "  FROM LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN FORM_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            ON C.ID = LD.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN FORM_GRUPO G  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON C.ID = CC.CAMPO " & vbCrLf
sConsulta = sConsulta & "            AND CC.PASO IS NULL" & vbCrLf
sConsulta = sConsulta & "            AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "            AND CC.SOLICITUD=@ID" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN CAMPO_VALOR_LISTA CVL  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   ON CVL.CAMPO=C.ID AND CVL.ORDEN=LD.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & " WHERE G.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   AND C.ES_SUBCAMPO=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CAMPOS DE LOS DESGLOSES INVISIBLES" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* " & vbCrLf
sConsulta = sConsulta & "  FROM LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN" & vbCrLf
sConsulta = sConsulta & "           (SELECT C.ID" & vbCrLf
sConsulta = sConsulta & "              FROM FORM_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN FORM_GRUPO G  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         ON C.GRUPO = G.ID " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         ON C.ID = CC.CAMPO " & vbCrLf
sConsulta = sConsulta & "                        AND CC.PASO IS NULL" & vbCrLf
sConsulta = sConsulta & "                        AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                        AND CC.SOLICITUD=@ID" & vbCrLf
sConsulta = sConsulta & "             WHERE G.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "               AND C.ES_SUBCAMPO=0 " & vbCrLf
sConsulta = sConsulta & "               AND CC.VISIBLE=0 " & vbCrLf
sConsulta = sConsulta & "               AND C.SUBTIPO = 9) DESG" & vbCrLf
sConsulta = sConsulta & "            ON LD.CAMPO_PADRE = DESG.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_LOADFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_LOADFIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_LOADFIELDSCALC @FORMULARIO INT,  @IDI VARCHAR(50)='SPA', @SOLICITUD INT  AS" & vbCrLf
sConsulta = sConsulta & "SELECT FC.*, C.GRUPO GRUPO_ORIGEN_CALC_DESGLOSE, ISNULL(CC.ESCRITURA,1) ESCRITURA, ISNULL(CC.VISIBLE,1) VISIBLE" & vbCrLf
sConsulta = sConsulta & "  FROM FORM_CAMPO FC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             INNER JOIN FORM_GRUPO FG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                     ON FC.GRUPO=FG.ID " & vbCrLf
sConsulta = sConsulta & "                    AND FG.FORMULARIO=@FORMULARIO" & vbCrLf
sConsulta = sConsulta & "              LEFT JOIN FORM_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                     ON FC.ORIGEN_CALC_DESGLOSE=C.ID  " & vbCrLf
sConsulta = sConsulta & "              LEFT JOIN CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                     ON FC.ID = CC.CAMPO " & vbCrLf
sConsulta = sConsulta & "                    AND CC.PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                    AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                    AND CC.SOLICITUD=@SOLICITUD " & vbCrLf
sConsulta = sConsulta & "WHERE FC.ES_SUBCAMPO = 0 " & vbCrLf
sConsulta = sConsulta & "   AND FC.ID_CALCULO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   AND (FC.SUBTIPO =2 OR FC.TIPO =3) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ORDER BY FC.ORDEN_CALCULO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_LOADFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_LOADFIELDSDESGLOSECALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_LOADFIELDSDESGLOSECALC @CAMPO INT, @SOLICITUD INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT C.* , CC.VISIBLE" & vbCrLf
sConsulta = sConsulta & "  FROM FORM_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN DESGLOSE D WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              ON C.ID=D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "             AND D.CAMPO_PADRE=@CAMPO" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              ON C.ID = CC.CAMPO " & vbCrLf
sConsulta = sConsulta & "             AND CC.PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "             AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "             AND CC.SOLICITUD=@SOLICITUD " & vbCrLf
sConsulta = sConsulta & " WHERE C.ID_CALCULO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   AND (C.SUBTIPO =2 OR C.TIPO =3) " & vbCrLf
sConsulta = sConsulta & "ORDER BY C.ORDEN_CALCULO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* FROM LINEA_DESGLOSE LD INNER JOIN FORM_CAMPO C WITH (NOLOCK)  ON C.ID = LD.CAMPO_HIJO WHERE LD.CAMPO_PADRE=@CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_LOADINSTFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_LOADINSTFIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_LOADINSTFIELDSCALC @INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50)='SPA',@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADADA AS TINYINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTINATARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PETICIONARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCRITURA_CERTIF AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYCONFCUMP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CUMP_PROVE TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @APROB = (select CS.PER FROM COPIA_SUBPASO CS WITH (NOLOCK)  INNER JOIN INSTANCIA I WITH (NOLOCK)  ON CS.ID = I.APROB WHERE I.ID= @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los campo de la instancia:" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADO= ESTADO," & vbCrLf
sConsulta = sConsulta & "       @TRASLADADA=TRASLADADA," & vbCrLf
sConsulta = sConsulta & "       @DESTINATARIO=CASE WHEN DESTINATARIO IS NULL THEN PER ELSE DESTINATARIO END , " & vbCrLf
sConsulta = sConsulta & "       @PETICIONARIO = PETICIONARIO," & vbCrLf
sConsulta = sConsulta & "       @CUMP_PROVE = CUMP_PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN INSTANCIA_EST  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON INSTANCIA.INSTANCIA_EST=INSTANCIA_EST.ID " & vbCrLf
sConsulta = sConsulta & "               AND DESTINATARIO IS NOT NULL  " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA.ID=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='" & vbCrLf
sConsulta = sConsulta & "SELECT C.ID ID_CAMPO, CD.*, CC2.ID ID_CAMPO_ORIGEN_CALC_DESGLOSE, CCD2.GRUPO GRUPO_ORIGEN_CALC_DESGLOSE, CC.VISIBLE, C.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_CAMPO_DEF CD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            ON CD.GRUPO =CG.ID" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            ON C.COPIA_CAMPO_DEF = CD.ID" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (COPIA_CAMPO CC2 WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN COPIA_CAMPO_DEF CCD2" & vbCrLf
sConsulta = sConsulta & "                         ON CC2.COPIA_CAMPO_DEF = CCD2.ID" & vbCrLf
sConsulta = sConsulta & "                        AND CC2.NUM_VERSION=@VERSION)" & vbCrLf
sConsulta = sConsulta & "            ON CC2.COPIA_CAMPO_DEF = CD.ORIGEN_CALC_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO = 0" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                  ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.INSTANCIA=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1-- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'   " & vbCrLf
sConsulta = sConsulta & "          ELSE-- ha sido trasladada al proveedor y la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            IF @CUMP_PROVE = 1 --usar configuracion de cumplimentaci�n del proveedor" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON IE.INSTANCIA = INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.ID = INSTANCIA.INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.DESTINATARIO_PROV = @PROVE" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE =2'   " & vbCrLf
sConsulta = sConsulta & "            ELSE --usar la configuragcion de cumplimentacion del aprobador que ha trasladado la instancia" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "      ELSE --no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB -- a no ser que el peticionario sea aprobador, sacar�a la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "          ELSE -- configuracion del peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo el aprobador por lo que sacampos la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "    ELSE -- NO EST� PENDIENTE DE APROBAR" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL  --  -- Se est� cargando como certificado por el proveedor" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE =2'    " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1'    " & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo otra persona " & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "      ELSE  --est� en otro estado" & vbCrLf
sConsulta = sConsulta & "        IF @ESTADO = 300 -- es una no conformidad" & vbCrLf
sConsulta = sConsulta & "          IF @USU = null --la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =2'    " & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1'    " & vbCrLf
sConsulta = sConsulta & "        ELSE --Est� en otro estado" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO --la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo un aprobador por lo que se coje la conf-cumplimentacion que ten�a cuando la aprob�" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                  INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON ISNULL(INSTANCIA.APROB, (SELECT TOP 1 APROB FROM INSTANCIA_EST WITH (NOLOCK)  WHERE INSTANCIA = @ID AND PER = @USU))=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +" & vbCrLf
sConsulta = sConsulta & "' WHERE CG.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND CD.ES_SUBCAMPO=0" & vbCrLf
sConsulta = sConsulta & "  AND CD.ID_CALCULO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  AND (CD.SUBTIPO = 2 OR CD.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "  AND C.NUM_VERSION = @VERSION" & vbCrLf
sConsulta = sConsulta & "ORDER BY CD.ORDEN_CALCULO'" & vbCrLf
sConsulta = sConsulta & "print @sql" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_LOADINSTFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_LOADINSTFIELDSDESGLOSECALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_LOADINSTFIELDSDESGLOSECALC @CAMPO INT , @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PASO INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADADA AS TINYINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTINATARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PETICIONARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCRITURA_CERTIF AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CUMP_PROVE TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @APROB = (select CS.PER FROM COPIA_SUBPASO CS WITH (NOLOCK)  INNER JOIN INSTANCIA I WITH (NOLOCK)  ON CS.ID = I.APROB WHERE I.ID= @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los campo de la instancia:" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADO= ESTADO," & vbCrLf
sConsulta = sConsulta & "       @TRASLADADA=TRASLADADA," & vbCrLf
sConsulta = sConsulta & "       @DESTINATARIO=CASE WHEN DESTINATARIO IS NULL THEN PER ELSE DESTINATARIO END , " & vbCrLf
sConsulta = sConsulta & "       @PETICIONARIO = PETICIONARIO," & vbCrLf
sConsulta = sConsulta & "       @CUMP_PROVE = CUMP_PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN INSTANCIA_EST  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON INSTANCIA.INSTANCIA_EST=INSTANCIA_EST.ID " & vbCrLf
sConsulta = sConsulta & "               AND DESTINATARIO IS NOT NULL  " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA.ID=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PASO = CS.PASO , @APROB = CS.PER FROM COPIA_SUBPASO CS WITH (NOLOCK)  INNER JOIN INSTANCIA I WITH (NOLOCK)  ON CS.ID = I.APROB WHERE I.ID = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL = '" & vbCrLf
sConsulta = sConsulta & "SELECT C.ID ID_CAMPO, CCD.*, CC.VISIBLE" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_CAMPO_DEF CCD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN COPIA_DESGLOSE CD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           ON C.ID = CD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "                          AND CD.CAMPO_PADRE = @CAMPO)" & vbCrLf
sConsulta = sConsulta & "            ON CCD.ID = C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO = 0" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                  ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.INSTANCIA=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1-- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "          ELSE-- ha sido trasladada al proveedor y la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            IF @CUMP_PROVE = 1 --usar configuracion de cumplimentaci�n del proveedor" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON IE.INSTANCIA = INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.ID = INSTANCIA.INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.DESTINATARIO_PROV = @PROVE" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE =2'   " & vbCrLf
sConsulta = sConsulta & "            ELSE --usar la configuragcion de cumplimentacion del aprobador que ha trasladado la instancia" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "      ELSE --no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB -- a no ser que el peticionario sea aprobador, sacar�a la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "          ELSE -- configuracion del peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo el aprobador por lo que sacampos la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "    ELSE -- NO EST� PENDIENTE DE APROBAR" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL  --  -- Se est� cargando como certificado por el proveedor" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE =2'    " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1'    " & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo otra persona (pilla la configuracion de cumplimentacion del peticionario" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "      ELSE " & vbCrLf
sConsulta = sConsulta & "        IF @ESTADO = 300 -- es una no conformidad" & vbCrLf
sConsulta = sConsulta & "          IF @USU = null --la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =2'    " & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1'    " & vbCrLf
sConsulta = sConsulta & "        ELSE --Est� en otro estado" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO --la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID'" & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo un aprobador por lo que se coje la conf-cumplimentacion que ten�a cuando la aprob�" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON ISNULL(INSTANCIA.APROB, (SELECT TOP 1 APROB FROM INSTANCIA_EST WITH (NOLOCK)  WHERE INSTANCIA = @INSTANCIA AND PER = @USU))=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                         ON CCD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & " WHERE CCD.ID_CALCULO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   AND (CCD.SUBTIPO = 2 or CCD.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "ORDER BY CCD.ORDEN_CALCULO" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PASO INT,@PETICIONARIO VARCHAR(50),@USU VARCHAR(50), @PROVE VARCHAR(50), @CAMPO INT', @INSTANCIA=@INSTANCIA, @PASO = @PASO,@PETICIONARIO=@PETICIONARIO,@USU=@USU,@PROVE = @PROVE, @CAMPO = @CAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* FROM COPIA_LINEA_DESGLOSE LD WITH (NOLOCK)   WHERE LD.CAMPO_PADRE=@CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Private Sub V_31600_Storeds_44_3()
Dim sConsulta As String


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_ETAPA_ACTUAL @INSTANCIA INT ,@IDI VARCHAR(50), @PER VARCHAR(50)=NULL ,@PROVE VARCHAR(50) =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*Obtendr� la etapa actual en la que se encuentra la instancia bas�ndose en la persona o proveedor que se pasa por par�metro:" & vbCrLf
sConsulta = sConsulta & " 1- Puede que sea el rol de un bloque activo" & vbCrLf
sConsulta = sConsulta & " 2- Puede que sea un rol de tipo lista autoasignable" & vbCrLf
sConsulta = sConsulta & " 3- Puede que no sea un rol, sino un usuario/proveedor al que se le ha trasladado la solicitud, o que est� a la espera de que se la devuelvan" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=' SELECT DISTINCT IB.BLOQUE,R.ID AS ROL,R.VER_FLUJO,PM_COPIA_BLOQUE_DEN.DEN AS BLOQUE_DEN ,IB.ID,IB.TRASLADADA,NULL AS CUMPLIMENTADOR,IB.BLOQ,R.COMO_ASIGNAR, R.PER PER_ROL" & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PM_COPIA_BLOQUE_DEN WITH (NOLOCK)  ON IB.BLOQUE=PM_COPIA_BLOQUE_DEN.BLOQUE AND PM_COPIA_BLOQUE_DEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "    WHERE IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '  AND R.PER=@PER'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '  AND R.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Es un rol de lista autoasignable" & vbCrLf
sConsulta = sConsulta & "IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '  " & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT IB.BLOQUE,R.ID AS ROL,R.VER_FLUJO,PM_COPIA_BLOQUE_DEN.DEN AS BLOQUE_DEN ,IB.ID,IB.TRASLADADA,NULL AS CUMPLIMENTADOR,IB.BLOQ,R.COMO_ASIGNAR, R.PER PER_ROL" & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID AND R.PER IS NULL AND R.COMO_ASIGNAR=3" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_PARTICIPANTES PT WITH (NOLOCK)  ON R.ID=PT.ROL AND PT.PER=@PER" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PM_COPIA_BLOQUE_DEN WITH (NOLOCK)  ON IB.BLOQUE=PM_COPIA_BLOQUE_DEN.BLOQUE AND PM_COPIA_BLOQUE_DEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   WHERE IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + '  " & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT IB.BLOQUE,R.ID AS ROL,R.VER_FLUJO, PM_COPIA_BLOQUE_DEN.DEN AS BLOQUE_DEN ,IB.ID,IB.TRASLADADA,CASE WHEN IE.DESTINATARIO IS NULL THEN IE.DESTINATARIO_PROV ELSE IE.DESTINATARIO END AS CUMPLIMENTADOR ,IB.BLOQ,R.COMO_ASIGNAR, R.PER PER_ROL" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN WITH (NOLOCK)  ON IB.BLOQUE=PM_COPIA_BLOQUE_DEN.BLOQUE AND PM_COPIA_BLOQUE_DEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   SELECT MAX(ID) ID ,INSTANCIA,BLOQUE,ROL " & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_EST WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE  (DESTINATARIO IS NOT NULL OR DESTINATARIO_PROV IS NOT NULL)  " & vbCrLf
sConsulta = sConsulta & "   GROUP BY INSTANCIA,BLOQUE,ROL" & vbCrLf
sConsulta = sConsulta & "   ) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE AND R.ID=T.ROL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)  ON T.ID=IE.ID" & vbCrLf
sConsulta = sConsulta & "WHERE IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.INSTANCIA=@INSTANCIA AND IB.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '  AND IE.PER=@PER'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + '  AND IE.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + '  " & vbCrLf
sConsulta = sConsulta & "UNION " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT IB.BLOQUE,R.ID AS ROL,R.VER_FLUJO, PM_COPIA_BLOQUE_DEN.DEN AS BLOQUE_DEN ,IB.ID,IB.TRASLADADA,CASE WHEN IE.DESTINATARIO IS NULL THEN IE.DESTINATARIO_PROV ELSE IE.DESTINATARIO END AS CUMPLIMENTADOR ,IB.BLOQ,R.COMO_ASIGNAR, R.PER PER_ROL" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN WITH (NOLOCK)  ON IB.BLOQUE=PM_COPIA_BLOQUE_DEN.BLOQUE AND PM_COPIA_BLOQUE_DEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   SELECT MAX(ID) ID ,INSTANCIA,BLOQUE,ROL " & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_EST  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE  (DESTINATARIO IS NOT NULL OR DESTINATARIO_PROV IS NOT NULL)  " & vbCrLf
sConsulta = sConsulta & "   GROUP BY INSTANCIA,BLOQUE,ROL" & vbCrLf
sConsulta = sConsulta & "   ) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE  AND R.ID=T.ROL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)  ON T.ID=IE.ID " & vbCrLf
sConsulta = sConsulta & "WHERE IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.INSTANCIA=@INSTANCIA AND IB.TRASLADADA=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '  AND IE.DESTINATARIO=@PER'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + '  AND IE.DESTINATARIO_PROV=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDI VARCHAR(50), @PER VARCHAR(50), @PROVE VARCHAR(50) ', @INSTANCIA=@INSTANCIA,@IDI=@IDI,@PER = @PER,@PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_DEVOLVER_APROBADOR_ACTUAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_DEVOLVER_APROBADOR_ACTUAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_DEVOLVER_APROBADOR_ACTUAL @INSTANCIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*Devuelve las instancias que requieren la aprobaci�n del usuario*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- 0.- Requiere aprobaci�n" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT COPIA_SUBPASO.PER AS APROBADOR,COPIA_SUBPASO.FECACT AS FECHA_ASIG" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_SUBPASO WITH (NOLOCK)  ON  INSTANCIA.APROB=COPIA_SUBPASO.ID " & vbCrLf
sConsulta = sConsulta & "WHERE  INSTANCIA.ID=@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ROL_PARTICIPANTES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ROL_PARTICIPANTES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ROL_PARTICIPANTES @BLOQUE INT, @ROL INT, @INSTANCIA INT,@IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSTANCIA<>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "       SELECT R.DEN RDEN,R.ID ROL,R.TIPO,R.RESTRINGIR, CASE WHEN R.PER IS NULL THEN R.PROVE ELSE R.PER END PART," & vbCrLf
sConsulta = sConsulta & "   CASE WHEN R.PER IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "       PER.NOM + ' ' + PER.APE+ ' (' + PER.EMAIL + ')' " & vbCrLf
sConsulta = sConsulta & "   ELSE " & vbCrLf
sConsulta = sConsulta & "       (CASE WHEN R.PROVE IS NULL THEN " & vbCrLf
sConsulta = sConsulta & "           NULL " & vbCrLf
sConsulta = sConsulta & "       ELSE " & vbCrLf
sConsulta = sConsulta & "           PROVE.DEN + ' (' + CON.EMAIL + ')' " & vbCrLf
sConsulta = sConsulta & "       END) " & vbCrLf
sConsulta = sConsulta & "   END PARTNOM, CON.ID ID_CONTACTO" & vbCrLf
sConsulta = sConsulta & "       FROM PM_COPIA_ROL R WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER WITH (NOLOCK)  ON PER.COD=R.PER " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE WITH (NOLOCK)  ON PROVE.COD=R.PROVE " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON WITH (NOLOCK)  ON PROVE.COD=CON.PROVE AND CON.ID=R.CON" & vbCrLf
sConsulta = sConsulta & "   WHERE R.INSTANCIA=@INSTANCIA AND R.BLOQUE=@BLOQUE AND R.ROL=@ROL AND R.COMO_ASIGNAR=2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT RB.ROL,RB.BLOQUE,B.DEN BDEN" & vbCrLf
sConsulta = sConsulta & "   FROM PM_COPIA_ROL R WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.ROL= R.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE_DEN B WITH (NOLOCK)  ON  B.BLOQUE=RB.BLOQUE " & vbCrLf
sConsulta = sConsulta & "   WHERE R.INSTANCIA=@INSTANCIA AND R.BLOQUE=@BLOQUE AND R.ROL=@ROL AND R.COMO_ASIGNAR=2 AND B.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "    SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT R.DEN RDEN, R.ID ROL, R.TIPO, R.RESTRINGIR, " & vbCrLf
sConsulta = sConsulta & "   CASE WHEN R.PER IS NULL THEN " & vbCrLf
sConsulta = sConsulta & "       R.PROVE " & vbCrLf
sConsulta = sConsulta & "   ELSE " & vbCrLf
sConsulta = sConsulta & "       R.PER " & vbCrLf
sConsulta = sConsulta & "   END PART," & vbCrLf
sConsulta = sConsulta & "               CASE WHEN R.PER IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "       PER.NOM + ' ' + PER.APE+ ' (' + PER.EMAIL + ')' " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "       (CASE WHEN R.PROVE IS NULL THEN " & vbCrLf
sConsulta = sConsulta & "           NULL " & vbCrLf
sConsulta = sConsulta & "       ELSE " & vbCrLf
sConsulta = sConsulta & "           PROVE.DEN + ' (' + CON.EMAIL + ')' " & vbCrLf
sConsulta = sConsulta & "       END) " & vbCrLf
sConsulta = sConsulta & "   END PARTNOM, CON.ID ID_CONTACTO" & vbCrLf
sConsulta = sConsulta & "   FROM PM_ROL R WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER WITH (NOLOCK)  ON PER.COD = R.PER" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE WITH (NOLOCK)  ON PROVE.COD=R.PROVE " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON WITH (NOLOCK)  ON PROVE.COD=CON.PROVE AND CON.ID=R.CON" & vbCrLf
sConsulta = sConsulta & "   WHERE R.BLOQUE_ASIGNA=@BLOQUE AND R.ROL_ASIGNA=@ROL AND R.COMO_ASIGNAR=2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT RB.ROL,RB.BLOQUE,B.DEN BDEN " & vbCrLf
sConsulta = sConsulta & "   FROM PM_ROL R  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.ROL= R.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_BLOQUE_DEN B WITH (NOLOCK)  ON  B.BLOQUE=RB.BLOQUE " & vbCrLf
sConsulta = sConsulta & "   WHERE R.BLOQUE_ASIGNA=@BLOQUE AND R.ROL_ASIGNA=@ROL AND R.COMO_ASIGNAR=2 AND B.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ACCIONES_ROL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ACCIONES_ROL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ACCIONES_ROL @BLOQUE INT, @ROL INT, @IDI VARCHAR(50), @COPIA TINYINT=0,@RECHAZOS TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COPIA=1" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=N'" & vbCrLf
sConsulta = sConsulta & "   SELECT A.*, AD.*  " & vbCrLf
sConsulta = sConsulta & "   FROM PM_COPIA_ACCIONES A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES_DEN AD WITH (NOLOCK)  ON A.ID = AD.ACCION AND @IDI = IDI" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH (NOLOCK) ON RA.ACCION = A.ID'" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'" & vbCrLf
sConsulta = sConsulta & "   SELECT A.*, AD.* " & vbCrLf
sConsulta = sConsulta & "   FROM PM_ACCIONES A  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_DEN AD WITH (NOLOCK)  ON A.ID = AD.ACCION AND @IDI = IDI" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_ROL_ACCION RA WITH (NOLOCK)  ON RA.ACCION = A.ID'" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + '  WHERE A.BLOQUE = @BLOQUE  AND RA.ROL = @ROL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECHAZOS=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' AND A.SOLO_RECHAZADAS=1'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' AND A.SOLO_RECHAZADAS=0'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @ROL INT,@IDI VARCHAR(50)', @BLOQUE = @BLOQUE,@ROL=@ROL,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LISTADOS_ROL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LISTADOS_ROL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LISTADOS_ROL @BLOQUE INT, @ROL INT, @IDI VARCHAR(3), @COPIA TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COPIA=1" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT ARCHIVO_RPT, DEN  " & vbCrLf
sConsulta = sConsulta & "   FROM PM_COPIA_ROL_LISPER_DEN WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT ARCHIVO_RPT, DEN  " & vbCrLf
sConsulta = sConsulta & "   FROM PM_ROL_LISPER_DEN WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + '  " & vbCrLf
sConsulta = sConsulta & "WHERE BLOQUE = @BLOQUE  AND ROL = @ROL AND IDI = @IDI AND LEN(DEN) >1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @ROL INT,@IDI VARCHAR(3)', @BLOQUE = @BLOQUE,@ROL=@ROL,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_ETAPAS_BLOQUEO @BLOQUE INT,@IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PM_COPIA_BLOQUE_DEN.BLOQUE, PM_COPIA_BLOQUE_DEN.DEN" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE IB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ENLACE WITH (NOLOCK) ON IB.BLOQUE=PM_COPIA_ENLACE.BLOQUE_DESTINO AND PM_COPIA_ENLACE.BLOQUEA=2" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE WITH (NOLOCK) ON PM_COPIA_ENLACE.BLOQUE_ORIGEN=INSTANCIA_BLOQUE.BLOQUE AND INSTANCIA_BLOQUE.ESTADO=1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE_DEN WITH (NOLOCK) ON INSTANCIA_BLOQUE.BLOQUE=PM_COPIA_BLOQUE_DEN.BLOQUE AND PM_COPIA_BLOQUE_DEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "WHERE IB.BLOQUE=@BLOQUE AND IB.BLOQ=2 AND IB.ESTADO=1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_getRequests]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_getRequests]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_getRequests @IDI AS VARCHAR(20), @USU AS VARCHAR(50), @COMBO TINYINT=0,@TIPO INT=NULL,@DESDESEGUIMIENTO INT=NULL,@NUEVO_WORKFL TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SPET AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
sConsulta = sConsulta & "  SET @SPET='VIEW_ROL_PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SPET='VIEW_PETICIONARIOS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COMBO=1 " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "  IF @DESDESEGUIMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT NULL AS ID, NULL AS COD, NULL AS DEN UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN FROM SOLICITUD S " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN (' + @SPET + ' P INNER JOIN USU U ON U.PER = P.COD) ON S.ID = P.SOLICITUD" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN TIPO_SOLICITUDES TS ON S.TIPO=TS.ID WHERE S.PUB = 1 AND S.BAJA = 0 AND U.COD = @USU '        " & vbCrLf
sConsulta = sConsulta & "   IF @TIPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL= @SQL + ' AND TS.TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "             SET @SQL= @SQL + ' UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN FROM SOLICITUD S INNER JOIN INSTANCIA I ON S.ID =I.SOLICITUD WHERE I.PETICIONARIO=@USU'" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL= @SQL + ' AND TS.TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "             SET @SQL= @SQL + '  GROUP BY  S.ID, S.COD, S.DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "     begin" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT NULL AS ID, NULL AS COD, NULL AS DEN UNION SELECT S.ID, S.COD, S.DEN_' + @IDI + ' DEN FROM SOLICITUD S " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN TIPO_SOLICITUDES TS ON S.TIPO=TS.ID WHERE S.PUB = 1 AND S.BAJA = 0  '        " & vbCrLf
sConsulta = sConsulta & "       IF @TIPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND TS.TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL= @SQL + '  GROUP BY  S.ID, S.COD, S.DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN, CASE WHEN S.DESCR_' + @IDI + ' IS NOT  NULL THEN 1 ELSE ISNULL(MAX(SA.ID),0) END  ADJUN  " & vbCrLf
sConsulta = sConsulta & "  FROM SOLICITUD S  INNER JOIN FORM_GRUPO FG ON S.FORMULARIO = FG.FORMULARIO INNER JOIN FORM_CAMPO FC ON FG.ID = FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "  LEFT JOIN SOLICITUD_ADJUN SA ON S.ID = SA.SOLICITUD " & vbCrLf
sConsulta = sConsulta & "  INNER JOIN (' + @SPET + ' P INNER JOIN USU U ON U.PER = P.COD) ON S.ID = P.SOLICITUD" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN TIPO_SOLICITUDES TS ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "  WHERE S.PUB = 1 AND S.BAJA = 0 AND U.COD = @USU' " & vbCrLf
sConsulta = sConsulta & "  IF @TIPO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + '  AND TS.TIPO=@TIPO' " & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @SQL= @SQL + '  AND TS.TIPO<>2 AND TS.TIPO<>3' " & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + '  GROUP BY  S.ID, S.COD, S.DEN_' + @IDI + ', S.DESCR_' + @IDI " & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + '  ORDER BY S.COD'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50),@TIPO INT', @USU = @USU,@TIPO=@TIPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_FIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_FIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_FIELDSCALC @GRUPO INT, @CASO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CASO = 0 " & vbCrLf
sConsulta = sConsulta & "--Creacion no hay instancia" & vbCrLf
sConsulta = sConsulta & "       SELECT COUNT(*) FROM FORM_CAMPO WITH (NOLOCK)  WHERE TIPO=3  AND  GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT COUNT(*) FROM COPIA_CAMPO_DEF WITH (NOLOCK)  WHERE TIPO=3  AND  GRUPO=@GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_DATOS_CERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_DATOS_CERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_DATOS_CERTIFICADO @INSTANCIA INT=0 ,@SOLOPROV TINYINT=0, @CERTIFICADO INT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SOLOPROV=0" & vbCrLf
sConsulta = sConsulta & "   SELECT TOP 1 FEC_PUBLICACION,FEC_DESPUB " & vbCrLf
sConsulta = sConsulta & "   FROM CERTIFICADO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA WITH (NOLOCK)  ON CERTIFICADO.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "   WHERE INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSTANCIA<>0" & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE AS COD ,PROVE.DEN,CERTIFICADO.ID,TIPO_SOLICITUD  " & vbCrLf
sConsulta = sConsulta & "   FROM CERTIFICADO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA WITH (NOLOCK)  ON CERTIFICADO.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE WITH (NOLOCK)  ON CERTIFICADO.PROVE=PROVE.COD " & vbCrLf
sConsulta = sConsulta & "   WHERE INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE AS COD ,PROVE.DEN,CERTIFICADO.ID,TIPO_SOLICITUD  " & vbCrLf
sConsulta = sConsulta & "   FROM CERTIFICADO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE WITH (NOLOCK)  ON CERTIFICADO.PROVE=PROVE.COD " & vbCrLf
sConsulta = sConsulta & "   WHERE CERTIFICADO.ID=@CERTIFICADO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_DATOS_NOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_DATOS_NOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_DATOS_NOCONFORMIDAD @INSTANCIA INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT NC.ID, NC.FECALTA,NC.FEC_LIM_RESOL, NC.PER, P.NOM  + ' ' + P.APE NOMBRE, P.EMAIL, NC.FEC_RESPUESTA, NC.VERSION,NC.USU, NC.DEP, NC.UON1, NC.UON2, NC.UON3,NC.PROVE,PROVE.DEN AS DEN_PROVE" & vbCrLf
sConsulta = sConsulta & ", NC.CONTACTO,CON.NOM + ' ' + CON.APE AS NOM_CONTACTO, NC.ESTADO" & vbCrLf
sConsulta = sConsulta & "FROM NOCONFORMIDAD NC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK)  ON NC.INSTANCIA=I.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE WITH (NOLOCK)  ON NC.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CON WITH (NOLOCK)  ON NC.PROVE=CON.PROVE AND NC.CONTACTO=CON.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER P WITH (NOLOCK)  ON NC.PER=P.COD" & vbCrLf
sConsulta = sConsulta & "WHERE NC.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETFIELDS_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETFIELDS_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_GETFIELDS_INSTANCIA @ID INT, @IDI VARCHAR(20) = NULL, @VERSION INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADADA AS TINYINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTINATARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PETICIONARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCRITURA_CERTIF AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYCONFCUMP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CUMP_PROVE TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCRITURA_NOCONF AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los datos de la instancia" & vbCrLf
sConsulta = sConsulta & "SELECT * FROM INSTANCIA WITH (NOLOCK)  WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @APROB = (select CS.PER FROM COPIA_SUBPASO CS  WITH (NOLOCK)  INNER JOIN INSTANCIA I  WITH (NOLOCK)  ON CS.ID = I.APROB WHERE I.ID= @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los campos de la instancia:" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADO= ESTADO," & vbCrLf
sConsulta = sConsulta & "       @TRASLADADA=TRASLADADA," & vbCrLf
sConsulta = sConsulta & "       @DESTINATARIO=CASE WHEN DESTINATARIO IS NULL THEN PER ELSE DESTINATARIO END , " & vbCrLf
sConsulta = sConsulta & "       @PETICIONARIO = PETICIONARIO," & vbCrLf
sConsulta = sConsulta & "       @CUMP_PROVE = CUMP_PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM INSTANCIA   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN INSTANCIA_EST   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON INSTANCIA.INSTANCIA_EST=INSTANCIA_EST.ID " & vbCrLf
sConsulta = sConsulta & "               AND DESTINATARIO IS NOT NULL  " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA.ID=@ID" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO=200  --Si  es un certificado comprueba que la instancia y versi�n que se pasan por par�metro sean las actuales del certificado.Si el certificado est� publicado no se podr�n modificar los datos" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    SELECT @ESCRITURA_CERTIF=PUBLICADA " & vbCrLf
sConsulta = sConsulta & "      FROM CERTIFICADO C WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN INSTANCIA I  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                       ON C.INSTANCIA=I.ID " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN PROVE_CERTIFICADO PV  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                       ON C.PROVE=PV.PROVE " & vbCrLf
sConsulta = sConsulta & "                      AND I.SOLICITUD=PV.TIPO_CERTIF " & vbCrLf
sConsulta = sConsulta & "                      AND C.ID=PV.ID_CERTIF " & vbCrLf
sConsulta = sConsulta & "    WHERE C.INSTANCIA=@ID AND C.NUM_VERSION=@VERSION AND C.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "    IF @ESCRITURA_CERTIF IS NULL" & vbCrLf
sConsulta = sConsulta & "      SET @ESCRITURA_CERTIF=1" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "   IF @ESTADO=300 OR @ESTADO=301" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        IF (SELECT ESTADO FROM NOCONFORMIDAD NC  WITH (NOLOCK)  WHERE NC.INSTANCIA=@ID)<2  AND (SELECT VERSION FROM NOCONFORMIDAD WITH (NOLOCK)   WHERE INSTANCIA=@ID)=@VERSION  --Si es una no conformidad y est�za cerrada  no se podr�n modificar los campos." & vbCrLf
sConsulta = sConsulta & "   SET @ESCRITURA_NOCONF=1" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @ESCRITURA_NOCONF=0" & vbCrLf
sConsulta = sConsulta & "      end " & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM COPIA_CONF_CUMPLIMENTACION  WITH (NOLOCK)  WHERE INSTANCIA = @ID)>0" & vbCrLf
sConsulta = sConsulta & "  SET @HAYCONFCUMP = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @HAYCONFCUMP = 0" & vbCrLf
sConsulta = sConsulta & "--Obtiene los grupos:" & vbCrLf
sConsulta = sConsulta & "IF @IDI IS NULL" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT F.* FROM COPIA_GRUPO F WITH (NOLOCK)   WHERE F.INSTANCIA =  @ID ORDER BY F.ORDEN'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT F.ID, F.INSTANCIA, F.DEN_' + @IDI + ', COUNT(CD.ID) NUMCAMPOS FROM COPIA_GRUPO F WITH (NOLOCK)   '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)   ON F.ID=CD.GRUPO' " & vbCrLf
sConsulta = sConsulta & "IF @HAYCONFCUMP = 1" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO = 0" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                  ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.INSTANCIA=F.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1-- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          ELSE-- ha sido trasladada al proveedor y la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            IF @CUMP_PROVE = 1 --usar configuracion de cumplimentaci�n del proveedor" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON IE.INSTANCIA = INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.ID = INSTANCIA.INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.DESTINATARIO_PROV = @PROVE" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE =2'   " & vbCrLf
sConsulta = sConsulta & "            ELSE --usar la configuragcion de cumplimentacion del aprobador que ha trasladado la instancia" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "      ELSE --no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB -- a no ser que el peticionario sea aprobador, sacar�a la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "          ELSE -- configuracion del peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1" & vbCrLf
sConsulta = sConsulta & "                                          AND F.INSTANCIA=INSTANCIA.ID  '" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo el aprobador por lo que sacampos la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "    ELSE -- NO EST� PENDIENTE DE APROBAR" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200 or @ESTADO = 300 or @ESTADO=301 --Es un certificado o una no conformidad" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL  --  -- Se est� cargando como certificado por el proveedor" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE =2 AND CC.ES_ESTADO=0 '    " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1 AND CC.ES_ESTADO=0 '    " & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo otra persona " & vbCrLf
sConsulta = sConsulta & "   IF @PETICIONARIO IS NULL --Pero es un certificado sin peticionario. Es decir, creado por el proveedor." & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)    " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1 AND CC.ES_ESTADO=0 '    " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1" & vbCrLf
sConsulta = sConsulta & "                                          AND F.INSTANCIA=INSTANCIA.ID  AND CC.ES_ESTADO=0 '" & vbCrLf
sConsulta = sConsulta & "        ELSE --Est� en otro estado" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO --la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND F.INSTANCIA=INSTANCIA.ID  '" & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo un aprobador por lo que se coje la conf-cumplimentacion que ten�a cuando la aprob�" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                         ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                  INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON ISNULL(INSTANCIA.APROB, (SELECT TOP 1 APROB FROM INSTANCIA_EST  WHERE INSTANCIA = @ID AND PER = @USU))=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' AND CC.VISIBLE = 1 AND CD.ES_SUBCAMPO=0'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N' WHERE F.INSTANCIA =  @ID GROUP BY F.ID, F.ORDEN,F.INSTANCIA, F.DEN_' + @IDI + ' ORDER BY F.ORDEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @ID=@ID, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "--- OBTIENE LOS CAMPOS:" & vbCrLf
sConsulta = sConsulta & "IF @IDI IS NULL " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT CD.*,C.ID AS ID_CAMPO, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL'   " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT DISTINCT(C.ID) AS ID_CAMPO,CD.ID, CD.GRUPO, CD.DEN_' + @IDI + ', AYUDA_' + @IDI + ', CD.TIPO, CD.SUBTIPO, CD.INTRO, C.VALOR_NUM, LEFT(C.VALOR_TEXT, 3980) VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, CD.MINNUM, CD.MAXNUM, CD.MINFEC, CD.MAXFEC, CD.ORDEN, CD.ID_ATRIB_GS, CD.TIPO_CAMPO_GS, CD.ID_CALCULO, CD.FORMULA, CD.ORDEN_CALCULO, CD.ORIGEN_CALC_DESGLOSE, CD.POPUP,CD.ANYADIR_ART,CD.IDREFSOLICITUD,CD.AVISOBLOQUEOIMPACUM,CD.CARGAR_ULT_ADJ '" & vbCrLf
sConsulta = sConsulta & "IF @HAYCONFCUMP = 0" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO=0 " & vbCrLf
sConsulta = sConsulta & "    IF @USU = @PETICIONARIO --La instancia est� guardada y es el propio cumplimentador el que accede a ella" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "    ELSE --La instancia est� guardada, pendiente de enviar y la ve otro ?�" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ', 0 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1 -- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, 1 VISIBLE , 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE , 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "      ELSE -- no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo el aprobador" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200 --Certificado publicado" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "              IF @USU=@PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "                    IF @ESCRITURA_CERTIF=1" & vbCrLf
sConsulta = sConsulta & "                        SET @SQL = @SQL + ', 0 ESCRITURA,1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                    ELSE" & vbCrLf
sConsulta = sConsulta & "                        SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + ', 0 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "       ELSE " & vbCrLf
sConsulta = sConsulta & "        IF @ESTADO = 300 OR @ESTADO = 301-- es una no conformidad" & vbCrLf
sConsulta = sConsulta & "              IF @USU IS NULL  --UN PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "                 SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                IF @USU=@PETICIONARIO -- la est� viendo el peticionario " & vbCrLf
sConsulta = sConsulta & "                    IF @ESCRITURA_NOCONF=1" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                    ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                ELSE -- la est� viendo otra persona" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "        ELSE --est� en otro estado" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO=0 " & vbCrLf
sConsulta = sConsulta & "    IF @USU = @PETICIONARIO --La instancia est� guardada y es el propio cumplimentador el que accede a ella" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "    ELSE --La instancia est� guardada, pendiente de enviar y la ve otro ?�" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1 -- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE , CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE -- ha sido trasladada al proveedor" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE , CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "      ELSE -- no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB -- a no ser que el peticionario sea aprobador" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE -- la est� consultando el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB  -- la est� viendo el aprobador" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE  --la esta viendo otra persona (imagino que por que la podr� ver... pero modificar no)" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "    ELSE -- no est� pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200 --Certificado publicado" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL  --UN PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU=@PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            IF @ESCRITURA_CERTIF=1  --est� publicada, por tanto el peticionario no puede modificar" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE -- la est� viendo otra persona" & vbCrLf
sConsulta = sConsulta & "   IF @PETICIONARIO IS NULL --Si es otra persona.Pero pq es un certificado creado por proveedor, lo que implica peticionario nulo." & vbCrLf
sConsulta = sConsulta & "       IF @ESCRITURA_CERTIF=1  --est� publicada, por tanto el peticionario no puede modificar" & vbCrLf
sConsulta = sConsulta & "                     SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                     SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'    " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "      ELSE --est� en otro estado" & vbCrLf
sConsulta = sConsulta & "        IF @ESTADO = 300 OR @ESTADO = 301 -- es una no conformidad" & vbCrLf
sConsulta = sConsulta & "              IF @USU IS NULL  --UN PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "                 SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                IF @USU=@PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "                   IF @ESCRITURA_NOCONF=1" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                    ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                ELSE -- la est� viendo otra persona" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @ESTADO=6 AND @USU=@PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @HAYCONFCUMP=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',CC.ES_ESTADO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@ESTADO=0 OR @ESTADO=2) AND @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  IF @HAYCONFCUMP=1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL +  ' ,G.ID, CC.ORDEN'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL +  ' ,G.ID, CD.ORDEN'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL +  ' ,G.ID, CD.ORDEN'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C WITH (NOLOCK)   INNER JOIN COPIA_CAMPO_DEF CD  ON C.COPIA_CAMPO_DEF=CD.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK)   ON CD.GRUPO = G.ID' " & vbCrLf
sConsulta = sConsulta & "IF @HAYCONFCUMP = 1" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO = 0" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                  ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.SOLIC_PROVE = 1" & vbCrLf
sConsulta = sConsulta & "                                 AND CC.INSTANCIA=C.INSTANCIA   '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1-- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          ELSE-- ha sido trasladada al proveedor y la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            IF @CUMP_PROVE = 1 --usar configuracion de cumplimentaci�n del proveedor" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON IE.INSTANCIA = INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.ID = INSTANCIA.INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.DESTINATARIO_PROV = @PROVE" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE =2'   " & vbCrLf
sConsulta = sConsulta & "            ELSE --usar la configuragcion de cumplimentacion del aprobador que ha trasladado la instancia" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "      ELSE --no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB -- a no ser que el peticionario sea aprobador, sacar�a la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "          ELSE -- configuracion del peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1" & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID '" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo el aprobador por lo que sacampos la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "    ELSE -- NO EST� PENDIENTE DE APROBAR" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200 or @ESTADO=300 OR @ESTADO = 301" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL  --  -- Se est� cargando como certificado por el proveedor" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE =2 AND CC.ES_ESTADO=0'    " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)    " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1 AND CC.ES_ESTADO=0'    " & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo otra persona " & vbCrLf
sConsulta = sConsulta & "   IF @PETICIONARIO IS NULL --Pero es un certificado hecho por el proveedor. Lo que implica peticionario nulo" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)    " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID    " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1 AND CC.ES_ESTADO=0'                " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID  AND CC.ES_ESTADO=0 '" & vbCrLf
sConsulta = sConsulta & "        ELSE --Est� en otro estado" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO --la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1" & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID  '" & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo un aprobador por lo que se coje la conf-cumplimentacion que ten�a cuando la aprob�" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                  INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON ISNULL(INSTANCIA.APROB, (SELECT TOP 1 APROB FROM INSTANCIA_EST  WHERE INSTANCIA = @ID AND PER = @USU))=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @ID AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@ESTADO=0 OR @ESTADO=2) AND @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  IF @HAYCONFCUMP=1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL +  ' ORDER BY G.ID, CC.ORDEN'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL +  ' ORDER BY G.ID, CD.ORDEN'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL +  ' ORDER BY G.ID, CD.ORDEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @ID=@ID, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los valores de listas:" & vbCrLf
sConsulta = sConsulta & "IF @IDI IS NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT L.ORDEN, C.GRUPO, L.* '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT L.ORDEN,C.GRUPO ,  L.CAMPO_DEF, L.VALOR_NUM, L.VALOR_TEXT_' + @IDI + ', L.VALOR_FEC'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' FROM COPIA_CAMPO_VALOR_LISTA L WITH (NOLOCK)   INNER JOIN COPIA_CAMPO_DEF C WITH (NOLOCK)  ON L.CAMPO_DEF = C.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK)   ON C.GRUPO=G.ID WHERE G.INSTANCIA = @ID  AND C.ES_SUBCAMPO=0 ORDER BY C.GRUPO, L.CAMPO_DEF, L.ORDEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
sConsulta = sConsulta & "--Obtiene los adjuntos" & vbCrLf
sConsulta = sConsulta & "SELECT CD.GRUPO, A.ID, A.CAMPO, A.NOM, A.COMENT, A.IDIOMA, A.DATASIZE, A.ADJUN FROM COPIA_CAMPO_ADJUN A WITH (NOLOCK)   INNER JOIN COPIA_CAMPO C WITH (NOLOCK)   ON A.CAMPO = C.ID INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)   ON C.COPIA_CAMPO_DEF=CD.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK)   ON CD.GRUPO=G.ID " & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA=@ID AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0 ORDER BY CD.GRUPO,A.CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETFIELDS_INSTANCIA_31600]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETFIELDS_INSTANCIA_31600]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_GETFIELDS_INSTANCIA_31600 @ID INT, @IDI VARCHAR(20) = NULL, @VERSION INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ROL INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @BLOQUE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENBLOQUE_ACT TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ROL =NULL" & vbCrLf
sConsulta = sConsulta & "SET @BLOQUE=NULL" & vbCrLf
sConsulta = sConsulta & "SET @ENBLOQUE_ACT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los datos de la instancia" & vbCrLf
sConsulta = sConsulta & "SELECT * FROM INSTANCIA WITH (NOLOCK)   WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-------------Ahora obtiene el rol y bloque del que se va a obtener la configuraci�n de la cumplimentaci�n-----------------------------------------" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "--1� Comprueba si el usuario o prove es un rol de un bloque que est� activo:" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT TOP 1 @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE FROM INSTANCIA_BLOQUE IB " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_ROL_BLOQUE ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_ROL ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID '" & vbCrLf
sConsulta = sConsulta & "IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' AND PM_COPIA_ROL.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' AND PM_COPIA_ROL.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' WHERE IB.INSTANCIA=@ID AND IB.ESTADO=1 AND IB.TRASLADADA=0'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @ID=@ID,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ROL IS NULL and @USU IS NOT NULL --No est� asignado al rol, comprueba si esta en la posible lista de asignados (COMO_ASIGNAR=3)" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=N'SELECT TOP 1 @ROL= R.ID, @BLOQUE=RB.BLOQUE FROM INSTANCIA_BLOQUE IB " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_ROL_BLOQUE RB ON IB.BLOQUE=RB.BLOQUE " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_ROL R ON RB.ROL=R.ID AND R.COMO_ASIGNAR=3 AND R.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN PM_COPIA_PARTICIPANTES PT ON PT.ROL= R.ID  AND PT.PER=@USU" & vbCrLf
sConsulta = sConsulta & "                    WHERE IB.INSTANCIA=@ID AND IB.ESTADO=1 AND IB.TRASLADADA=0'" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL, N'@ID INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @ID=@ID,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ROL IS NULL --No es un rol de un bloque activo. Comprueba si es un rol de cualquier bloque:" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "        SET @SQL=N'SELECT TOP 1  @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE FROM INSTANCIA_BLOQUE IB " & vbCrLf
sConsulta = sConsulta & "                INNER JOIN PM_COPIA_ROL_BLOQUE ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "                INNER JOIN PM_COPIA_ROL ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID  WHERE IB.INSTANCIA=@ID'" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL +' AND PM_COPIA_ROL.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @SQL=@SQL +' AND PM_COPIA_ROL.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@ID INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT ', @ID=@ID,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @ROL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "            SET @ENBLOQUE_ACT=0" & vbCrLf
sConsulta = sConsulta & "       ELSE --No es un rol, ser� un usuario al que se le ha trasladado o devuelto la solicitud:" & vbCrLf
sConsulta = sConsulta & "          begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT TOP 1 @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE FROM INSTANCIA_BLOQUE IB " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PM_COPIA_ROL_BLOQUE ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PM_COPIA_ROL ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN (SELECT MAX(ID) ID ,INSTANCIA,BLOQUE FROM INSTANCIA_EST WHERE DESTINATARIO IS NOT NULL OR DESTINATARIO_PROV IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                     GROUP BY INSTANCIA,BLOQUE) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN INSTANCIA_EST IE ON T.ID=IE.ID'" & vbCrLf
sConsulta = sConsulta & "          IF @USU IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                  SET @SQL=@SQL + ' AND  IE.DESTINATARIO=@USU'" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL + ' AND IE.DESTINATARIO_PROV=@PROVE'" & vbCrLf
sConsulta = sConsulta & "          SET @SQL=@SQL + ' WHERE IB.INSTANCIA=@ID AND IB.ESTADO=1 AND IB.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "          EXEC SP_EXECUTESQL @SQL, N'@ID INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @ID=@ID,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               IF @ROL IS NULL --No es el usuario activo de un traslado o devoluci�n,ser� uno intermedio o uno que intervino en alg�n momento." & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "                    SET @ENBLOQUE_ACT=0" & vbCrLf
sConsulta = sConsulta & "                    SET @SQL=N'SELECT TOP 1 @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE FROM INSTANCIA_BLOQUE IB " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PM_COPIA_ROL_BLOQUE ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PM_COPIA_ROL ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN (SELECT MAX(ID) ID ,INSTANCIA,BLOQUE,ROL FROM INSTANCIA_EST'" & vbCrLf
sConsulta = sConsulta & "          IF @USU IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                  SET @SQL=@SQL + ' WHERE  DESTINATARIO=@USU'" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL + ' WHERE DESTINATARIO_PROV=@PROVE'" & vbCrLf
sConsulta = sConsulta & "          SET @SQL=@SQL + ' GROUP BY INSTANCIA,BLOQUE,ROL) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE AND T.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "          WHERE IB.INSTANCIA=@ID '" & vbCrLf
sConsulta = sConsulta & "          EXEC SP_EXECUTESQL @SQL, N'@ID INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @ID=@ID,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "                 end" & vbCrLf
sConsulta = sConsulta & "          end" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC FSPM_IDENTIFICAR_ROL @USU=@USU, @PROVE=@PROVE, @INSTANCIA=@ID, @ROL=@ROL OUTPUT, @BLOQUE=@BLOQUE OUTPUT,@ENBLOQUE_ACT=@ENBLOQUE_ACT OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los grupos:" & vbCrLf
sConsulta = sConsulta & "IF @IDI IS NULL" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT F.* FROM COPIA_GRUPO F WITH (NOLOCK)  WHERE F.INSTANCIA =  @ID ORDER BY F.ORDEN'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT F.ID, F.INSTANCIA, F.DEN_' + @IDI + ', COUNT(CD.ID) NUMCAMPOS FROM COPIA_GRUPO F WITH (NOLOCK)   '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)   ON F.ID=CD.GRUPO" & vbCrLf
sConsulta = sConsulta & " INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CC WITH (NOLOCK)   ON CD.ID = CC.CAMPO AND CC.BLOQUE=@BLOQUE  AND CC.ROL =@ROL " & vbCrLf
sConsulta = sConsulta & "WHERE F.INSTANCIA =  @ID AND CC.VISIBLE = 1 AND CD.ES_SUBCAMPO=0" & vbCrLf
sConsulta = sConsulta & "  GROUP BY F.ID, F.ORDEN,F.INSTANCIA, F.DEN_' + @IDI + ' ORDER BY F.ORDEN'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50),@ROL INT, @BLOQUE INT', @ID=@ID, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE, @ROL=@ROL, @BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--- OBTIENE LOS CAMPOS:" & vbCrLf
sConsulta = sConsulta & "IF @IDI IS NULL " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT CD.*,C.ID AS ID_CAMPO, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL'   " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT C.ID AS ID_CAMPO,CD.ID, CD.GRUPO, CD.DEN_' + @IDI + ', AYUDA_' + @IDI + ', CD.TIPO, CD.SUBTIPO, CD.INTRO, C.VALOR_NUM, LEFT(C.VALOR_TEXT, 3980) VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, CD.MINNUM, CD.MAXNUM, CD.MINFEC, CD.MAXFEC, CD.ID_ATRIB_GS, CD.TIPO_CAMPO_GS, CD.ID_CALCULO, CD.FORMULA, CD.ORDEN_CALCULO, CD.ORIGEN_CALC_DESGLOSE, CD.POPUP,CD.ANYADIR_ART" & vbCrLf
sConsulta = sConsulta & ",CD.IDREFSOLICITUD,CD.AVISOBLOQUEOIMPACUM,CD.CARGAR_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENBLOQUE_ACT=0  --No est� en el bloque actual, por lo que no puede modificar:" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "ELSE  --Es un rol del bloque actual o una persona/prov al que se le ha trasladado, podr� modificar dependiendo de la cumplimentaci�n:" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  ' ,G.ID grupo, ISNULL(CC.ORDEN,CD.ORDEN) ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C WITH (NOLOCK)   INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)   ON C.COPIA_CAMPO_DEF=CD.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK)   ON CD.GRUPO = G.ID" & vbCrLf
sConsulta = sConsulta & " INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CC WITH (NOLOCK)   ON CD.ID = CC.CAMPO AND CC.BLOQUE=@BLOQUE  AND CC.ROL =@ROL  '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @ID AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0' " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  ' ORDER BY G.ID, CC.ORDEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50),@ROL INT, @BLOQUE INT', @ID=@ID, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE, @ROL=@ROL, @BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "--Obtiene los valores de listas: " & vbCrLf
sConsulta = sConsulta & "IF @IDI IS NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT L.ORDEN, C.GRUPO, L.* '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT L.ORDEN,C.GRUPO ,  L.CAMPO_DEF, L.VALOR_NUM, L.VALOR_TEXT_' + @IDI + ', L.VALOR_FEC'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' FROM COPIA_CAMPO_VALOR_LISTA L WITH (NOLOCK)   INNER JOIN COPIA_CAMPO_DEF C WITH (NOLOCK)   ON L.CAMPO_DEF = C.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK)   ON C.GRUPO=G.ID WHERE G.INSTANCIA = @ID  AND C.ES_SUBCAMPO=0 ORDER BY C.GRUPO, L.CAMPO_DEF, L.ORDEN'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los adjuntos" & vbCrLf
sConsulta = sConsulta & "SELECT CD.GRUPO, A.ID, A.CAMPO, A.NOM, A.COMENT, A.IDIOMA, A.DATASIZE, A.ADJUN FROM COPIA_CAMPO_ADJUN A WITH (NOLOCK)   INNER JOIN COPIA_CAMPO C WITH (NOLOCK)   ON A.CAMPO = C.ID INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)   ON C.COPIA_CAMPO_DEF=CD.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK)   ON CD.GRUPO=G.ID " & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA=@ID AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0 ORDER BY CD.GRUPO,A.CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_IDENTIFICAR_ROL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_IDENTIFICAR_ROL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE FSPM_IDENTIFICAR_ROL @USU VARCHAR(50), @PROVE VARCHAR(50), @INSTANCIA INT, @ROL INT OUTPUT, @BLOQUE INT OUTPUT, @ENBLOQUE_ACT TINYINT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtiene el rol y bloque del que se va a obtener la configuraci�n de la cumplimentaci�n" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ROL =NULL" & vbCrLf
sConsulta = sConsulta & "SET @BLOQUE=NULL" & vbCrLf
sConsulta = sConsulta & "SET @ENBLOQUE_ACT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--1� Comprueba si el usuario o prove es un rol de un bloque que est� activo:" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'" & vbCrLf
sConsulta = sConsulta & "SELECT TOP 1 @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE WITH (NOLOCK) ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL WITH (NOLOCK) ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' AND PM_COPIA_ROL.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' AND PM_COPIA_ROL.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND IB.TRASLADADA=0'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ROL IS NULL and @USU IS NOT NULL --No est� asignado al rol, comprueba si esta en la posible lista de asignados (COMO_ASIGNAR=3)" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT TOP 1 @ROL= R.ID, @BLOQUE=RB.BLOQUE " & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK) ON IB.BLOQUE=RB.BLOQUE " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON RB.ROL=R.ID AND R.COMO_ASIGNAR=3 AND R.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_PARTICIPANTES PT WITH (NOLOCK) ON PT.ROL= R.ID  AND PT.PER=@USU" & vbCrLf
sConsulta = sConsulta & "   WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND IB.TRASLADADA=0'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ROL IS NULL --No es un rol de un bloque activo." & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "        --SET @SQL=N'SELECT TOP 1  @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE FROM INSTANCIA_BLOQUE IB " & vbCrLf
sConsulta = sConsulta & "   --             INNER JOIN PM_COPIA_ROL_BLOQUE ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "     --           INNER JOIN PM_COPIA_ROL ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID  WHERE IB.INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "        --IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          --       SET @SQL=@SQL +' AND PM_COPIA_ROL.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "        --ELSE" & vbCrLf
sConsulta = sConsulta & "          --  SET @SQL=@SQL +' AND PM_COPIA_ROL.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "       --EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT ', @INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     --  IF @ROL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       --     SET @ENBLOQUE_ACT=0" & vbCrLf
sConsulta = sConsulta & "       --ELSE " & vbCrLf
sConsulta = sConsulta & "         -- begin" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             --No es un rol activo, ser� un usuario al que se le ha trasladado o devuelto la solicitud??:" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'" & vbCrLf
sConsulta = sConsulta & "   SELECT TOP 1 @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE WITH (NOLOCK) ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL WITH (NOLOCK) ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN " & vbCrLf
sConsulta = sConsulta & "       (" & vbCrLf
sConsulta = sConsulta & "       SELECT MAX(ID) ID ,INSTANCIA,BLOQUE " & vbCrLf
sConsulta = sConsulta & "       FROM INSTANCIA_EST  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       WHERE DESTINATARIO IS NOT NULL OR DESTINATARIO_PROV IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       GROUP BY INSTANCIA,BLOQUE" & vbCrLf
sConsulta = sConsulta & "       ) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) ON T.ID=IE.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @USU IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND  IE.DESTINATARIO=@USU'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND IE.DESTINATARIO_PROV=@PROVE'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND IB.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               IF @ROL IS NULL  --Comprueba si es un rol de cualquier bloque:" & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "                    SET @ENBLOQUE_ACT=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          SET @SQL=N'SELECT TOP 1  @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "       FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                INNER JOIN PM_COPIA_ROL_BLOQUE WITH (NOLOCK)  ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "                INNER JOIN PM_COPIA_ROL WITH (NOLOCK) ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE IB.INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "          IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL +' AND PM_COPIA_ROL.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL +' AND PM_COPIA_ROL.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "                  EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT ', @INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    IF @ROL IS NULL  --No es el usuario activo de un traslado o devoluci�n,ser� uno intermedio o uno que intervino en alg�n momento." & vbCrLf
sConsulta = sConsulta & "           begin       " & vbCrLf
sConsulta = sConsulta & "                       SET @SQL=N'SELECT TOP 1 @ROL= PM_COPIA_ROL.ID, @BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "           FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL_BLOQUE WITH (NOLOCK) ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL WITH (NOLOCK)  ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (SELECT MAX(ID) ID ,INSTANCIA,BLOQUE,ROL " & vbCrLf
sConsulta = sConsulta & "           FROM INSTANCIA_EST WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @USU IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                  SET @SQL=@SQL + ' WHERE  DESTINATARIO=@USU'" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL + ' WHERE DESTINATARIO_PROV=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL + ' " & vbCrLf
sConsulta = sConsulta & "           GROUP BY INSTANCIA,BLOQUE,ROL) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE AND T.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE IB.INSTANCIA=@INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT, @BLOQUE INT OUTPUT', @INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT ,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "                      end" & vbCrLf
sConsulta = sConsulta & "                 end" & vbCrLf
sConsulta = sConsulta & "          " & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_HISTORICO_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_HISTORICO_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_HISTORICO_INSTANCIA @INSTANCIA INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT MAX(IB1.ID) BLOQUE1, MAX(IB2.ID) AS BLOQUE2, MAX(ISNULL(BD1.DEN,' ')) AS DEN_BLOQUE1, MAX(PM_COPIA_ROL.DEN) AS ROL" & vbCrLf
sConsulta = sConsulta & ", MAX(PER.COD) AS PER, MAX(PROVE.COD) AS PROVE" & vbCrLf
sConsulta = sConsulta & ", CASE WHEN MAX(PER.COD) IS NOT NULL THEN MAX(PER.NOM) + ' ' + MAX(PER.APE) ELSE MAX(PROVE.DEN) END AS USUARIO" & vbCrLf
sConsulta = sConsulta & ", MAX(ISNULL(PM_COPIA_ACCIONES_DEN.DEN,' ')) AS ACCION" & vbCrLf
sConsulta = sConsulta & ",INSTANCIA_EST.FECHA,   MAX(ISNULL(BD2.DEN,' ')) AS DEN_BLOQUE2, MAX(INSTANCIA_EST.COMENT) AS COMENT, NULL AS ESTADO" & vbCrLf
sConsulta = sConsulta & ",NULL AS ID_OTRA_ACCION ,NULL AS DESTINATARIO ,COUNT(*) AS ETAPAS_PARALELO, MAX(INSTANCIA_EST.ID) AS ID" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_CAMINO WITH (NOLOCK) ON INSTANCIA_EST.ID=INSTANCIA_CAMINO.INSTANCIA_EST " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB1 WITH (NOLOCK) ON INSTANCIA_CAMINO.INSTANCIA_BLOQUE_ORIGEN=IB1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB2 WITH (NOLOCK) ON INSTANCIA_CAMINO.INSTANCIA_BLOQUE_DESTINO=IB2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE B1 WITH (NOLOCK) ON IB1.BLOQUE=B1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE B2 WITH (NOLOCK) ON IB2.BLOQUE=B2.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN AS BD1 WITH (NOLOCK) ON B1.ID=BD1.BLOQUE AND BD1.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN AS BD2 WITH (NOLOCK) ON B2.ID=BD2.BLOQUE AND BD2.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL WITH (NOLOCK) ON INSTANCIA_CAMINO.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ACCIONES WITH (NOLOCK) ON INSTANCIA_CAMINO.ACCION=PM_COPIA_ACCIONES.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_ACCIONES_DEN WITH (NOLOCK) ON PM_COPIA_ACCIONES.ID=PM_COPIA_ACCIONES_DEN.ACCION AND PM_COPIA_ACCIONES_DEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER WITH (NOLOCK) ON INSTANCIA_EST.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE  WITH (NOLOCK) ON PM_COPIA_ROL.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "WHERE IB1.INSTANCIA=@INSTANCIA AND IB2.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA_CAMINO.INSTANCIA_BLOQUE_ORIGEN,INSTANCIA_EST.FECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UNION  --Es un traslado o una devoluci�n:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT NULL BLOQUE1, NULL AS BLOQUE2, ISNULL(BD1.DEN,' ') AS DEN_BLOQUE1, PM_COPIA_ROL.DEN AS ROL" & vbCrLf
sConsulta = sConsulta & ", PER.COD AS PER, PROVE.COD AS PROVE, CASE WHEN PER.COD IS NOT NULL THEN PER.NOM + ' ' + PER.APE ELSE PROVE.DEN END AS USUARIO" & vbCrLf
sConsulta = sConsulta & ",NULL AS ACCION,INSTANCIA_EST.FECHA,  NULL AS DEN_BLOQUE2, INSTANCIA_EST.COMENT, INSTANCIA_EST.EST AS ESTADO, INSTANCIA_EST.ACCION AS ID_OTRA_ACCION" & vbCrLf
sConsulta = sConsulta & ",CASE WHEN (INSTANCIA_EST.ACCION=3  or INSTANCIA_EST.ACCION=4 ) THEN CASE WHEN DESTINATARIO IS NOT NULL THEN PER2.NOM + ' ' + PER2.APE ELSE PROVE2.DEN END ELSE NULL END AS DESTINATARIO" & vbCrLf
sConsulta = sConsulta & ",NULL AS ETAPAS_PARALELO, INSTANCIA_EST.ID" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_ROL WITH (NOLOCK) ON INSTANCIA_EST.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE WITH (NOLOCK) ON INSTANCIA_EST.BLOQUE=INSTANCIA_BLOQUE.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN BD1 WITH (NOLOCK) ON INSTANCIA_BLOQUE.BLOQUE=BD1.BLOQUE AND BD1.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER WITH (NOLOCK) ON INSTANCIA_EST.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE WITH (NOLOCK) ON INSTANCIA_EST.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER PER2 WITH (NOLOCK) ON INSTANCIA_EST.DESTINATARIO=PER2.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE PROVE2 WITH (NOLOCK) ON INSTANCIA_EST.DESTINATARIO_PROV=PROVE2.COD" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA_EST.INSTANCIA=@INSTANCIA AND (INSTANCIA_EST.ACCION=3 OR INSTANCIA_EST.ACCION=4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UNION  --las que est�n pendientes:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT MAX(INSTANCIA_CAMINO.INSTANCIA_BLOQUE_ORIGEN) BLOQUE1, INSTANCIA_BLOQUE.ID AS BLOQUE2" & vbCrLf
sConsulta = sConsulta & ", MAX(ISNULL(BD1.DEN,' ')) AS DEN_BLOQUE1, MAX(PM_COPIA_ROL.DEN) AS ROL" & vbCrLf
sConsulta = sConsulta & ", MAX(PER.COD) AS PER, MAX(PROVE.COD) AS PROVE, CASE WHEN MAX(PER.COD) IS NOT NULL THEN MAX(PER.NOM) + ' ' + MAX(PER.APE) ELSE MAX(PROVE.DEN) END AS USUARIO" & vbCrLf
sConsulta = sConsulta & ",NULL AS ACCION,NULL AS FECHA,  NULL AS DEN_BLOQUE2, NULL AS COMENT,  MAX(INSTANCIA_BLOQUE.ESTADO), NULL AS ID_OTRA_ACCION,NULL AS DESTINATARIO" & vbCrLf
sConsulta = sConsulta & ",NULL AS ETAPAS_PARALELO, MAX(INSTANCIA_EST.ID)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_CAMINO WITH (NOLOCK) ON INSTANCIA_BLOQUE.ID=INSTANCIA_CAMINO.INSTANCIA_BLOQUE_DESTINO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST WITH (NOLOCK) ON INSTANCIA_CAMINO.INSTANCIA_EST=INSTANCIA_EST.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN BD1 WITH (NOLOCK) ON INSTANCIA_BLOQUE.BLOQUE=BD1.BLOQUE AND BD1.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT MAX(ROL) AS ROL,BLOQUE FROM PM_COPIA_ROL_BLOQUE WITH (NOLOCK) GROUP BY BLOQUE) M ON INSTANCIA_BLOQUE.BLOQUE=M.BLOQUE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_ROL WITH (NOLOCK) ON M.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA WITH (NOLOCK) ON INSTANCIA_BLOQUE.INSTANCIA=INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER WITH (NOLOCK) ON PM_COPIA_ROL.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE WITH (NOLOCK) ON PM_COPIA_ROL.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA_EST.INSTANCIA=@INSTANCIA AND INSTANCIA_BLOQUE .ESTADO=1 AND INSTANCIA.ESTADO<>8 AND INSTANCIA.ESTADO<>6" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA_BLOQUE.ID" & vbCrLf
sConsulta = sConsulta & " UNION --Las anuladas (no provocan cambio de etapa)" & vbCrLf
sConsulta = sConsulta & "SELECT NULL BLOQUE1, NULL AS BLOQUE2, ISNULL(BD1.DEN,' ') AS DEN_BLOQUE1, PM_COPIA_ROL.DEN AS ROL" & vbCrLf
sConsulta = sConsulta & ", PER.COD AS PER, PROVE.COD AS PROVE, CASE WHEN PER.COD IS NOT NULL THEN PER.NOM + ' ' + PER.APE ELSE PROVE.DEN END AS USUARIO" & vbCrLf
sConsulta = sConsulta & ",ISNULL(PM_COPIA_ACCIONES_DEN.DEN,' ') AS ACCION,INSTANCIA_EST.FECHA,  NULL AS DEN_BLOQUE2, INSTANCIA_EST.COMENT, INSTANCIA_EST.EST AS ESTADO, NULL AS ID_OTRA_ACCION" & vbCrLf
sConsulta = sConsulta & ",NULL DESTINATARIO ,NULL AS ETAPAS_PARALELO, INSTANCIA_EST.ID" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL WITH (NOLOCK) ON INSTANCIA_EST.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE WITH (NOLOCK) ON INSTANCIA_EST.BLOQUE=INSTANCIA_BLOQUE.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN BD1 WITH (NOLOCK) ON INSTANCIA_BLOQUE.BLOQUE=BD1.BLOQUE AND BD1.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ACCIONES WITH (NOLOCK) ON INSTANCIA_EST.PM_ACCION=PM_COPIA_ACCIONES.ID AND TIPO_RECHAZO=3" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_ACCIONES_DEN WITH (NOLOCK) ON PM_COPIA_ACCIONES.ID=PM_COPIA_ACCIONES_DEN.ACCION AND PM_COPIA_ACCIONES_DEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER WITH (NOLOCK) ON INSTANCIA_EST.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE WITH (NOLOCK) ON PM_COPIA_ROL.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA_EST.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UNION -- Acciones desde el GS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select NULL as BLOQUE1, NULL as BLOQUE2, NULL as DEN_BLOQUE1, NULL as ROL, " & vbCrLf
sConsulta = sConsulta & "PER.COD AS PER, PROVE.COD AS PROVE, CASE WHEN PER.COD IS NOT NULL THEN PER.NOM + ' ' + PER.APE ELSE ISNULL(PROVE.DEN, ' ') END AS USUARIO, NULL AS ACCION, INSTANCIA_EST.FECHA AS FECHA, NULL AS DEN_BLOQUE2, INSTANCIA_EST.COMENT AS COMENT,  INSTANCIA_EST.EST as ESTADO, INSTANCIA_EST.ACCION AS ID_OTRA_ACCION, NULL AS DESTINATARIO, NULL AS ETAPAS_PARALELO, INSTANCIA_EST.ID" & vbCrLf
sConsulta = sConsulta & "from INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER WITH (NOLOCK) ON INSTANCIA_EST.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE WITH (NOLOCK) ON INSTANCIA_EST.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "where INSTANCIA_EST.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "and INSTANCIA_EST.EST > 100" & vbCrLf
sConsulta = sConsulta & "and INSTANCIA_EST.ACCION is null" & vbCrLf
sConsulta = sConsulta & "and INSTANCIA_EST.ROL is null" & vbCrLf
sConsulta = sConsulta & "and INSTANCIA_EST.PM_ACCION is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ORDER BY INSTANCIA_EST.ID, INSTANCIA_EST.FECHA DESC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_VER_PUNTUACION_CALIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_VER_PUNTUACION_CALIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE FSQA_VER_PUNTUACION_CALIDAD @PROVE NVARCHAR(100) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAT AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SELECT @MAT=VARCAL_MAT FROM PARGEN_INTERNO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Creamos una tabla temporal  para guardar las variables recuperadas hasta ahora" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_VARIABLES (NIVEL INT NOT NULL, IDVAR1 INT NOT NULL,  IDVAR2 INT NOT NULL, IDVAR3 INT NOT NULL, IDVAR4 INT NOT NULL, IDVAR5 INT NOT NULL,DEN VARCHAR(500)COLLATE SQL_Latin1_General_CP1_CI_AS NULL, PUNT INT,CAL  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV1  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV2  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV3  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV4  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV5  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,PUB_PORTAL INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_VARIABLES1 (NIVEL INT NOT NULL, IDVAR1 INT NOT NULL,  IDVAR2 INT NOT NULL, IDVAR3 INT NOT NULL, IDVAR4 INT NOT NULL, IDVAR5 INT NOT NULL,DEN VARCHAR(500)COLLATE SQL_Latin1_General_CP1_CI_AS NULL, PUNT INT,CAL  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV1  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV2  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV3  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV4  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV5  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,PUB_PORTAL INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_VARIABLES2 (NIVEL INT NOT NULL, IDVAR1 INT NOT NULL,  IDVAR2 INT NOT NULL, IDVAR3 INT NOT NULL, IDVAR4 INT NOT NULL, IDVAR5 INT NOT NULL,DEN VARCHAR(500)COLLATE SQL_Latin1_General_CP1_CI_AS NULL, PUNT INT,CAL  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV1  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV2  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV3  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV4  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV5  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,PUB_PORTAL INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_VARIABLES3 (NIVEL INT NOT NULL, IDVAR1 INT NOT NULL,  IDVAR2 INT NOT NULL, IDVAR3 INT NOT NULL, IDVAR4 INT NOT NULL, IDVAR5 INT NOT NULL,DEN VARCHAR(500)COLLATE SQL_Latin1_General_CP1_CI_AS NULL, PUNT INT,CAL  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV1  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV2  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV3  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV4  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV5  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,PUB_PORTAL INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_VARIABLES4 (NIVEL INT NOT NULL, IDVAR1 INT NOT NULL,  IDVAR2 INT NOT NULL, IDVAR3 INT NOT NULL, IDVAR4 INT NOT NULL, IDVAR5 INT NOT NULL,DEN VARCHAR(500)COLLATE SQL_Latin1_General_CP1_CI_AS NULL, PUNT INT,CAL  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV1  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV2  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV3  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV4  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,CODV5  VARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS NULL,PUB_PORTAL INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Las de nivel 1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL='SELECT DISTINCT 1 AS NIVEL, V1.ID AS IDVAR1, 0 AS IDVAR2, 0 AS IDVAR3, 0 AS IDVAR4,0 AS IDVAR5, V1.DEN, PP.PUNT,CP.CAL,V1.COD AS CODV1,NULL AS CODV2,NULL AS CODV3,NULL AS CODV4,NULL AS CODV5,V1.PUB_PORTAL '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' FROM VAR_CAL1 V1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "     begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                 (SELECT DISTINCT V.ID_VAR_CAL1, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal2 v on vg.id=v.id and vg.nivel=2   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4 PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND prove=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union '--las compuestas " & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL +'select DISTINCT V.ID_VAR_CAL1, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal3 v on vg.id=v.id and vg.nivel=3   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND prove=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 4" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4  AND V.MATERIAL_QA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5  AND V.MATERIAL_QA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 2    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL2 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  ) VBLES_PROVE ON  VBLES_PROVE.ID_VAR_CAL1 = V1.ID'" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' LEFT JOIN VAR_PROVE_PUNT PP ON V1.ID=PP.VARCAL AND PP.NIVEL=1 AND PP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CAL_PUNTUACION CP ON PP.VARCAL=CP.VARCAL AND PP.NIVEL=CP.NIVEL AND PP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "     WHERE  V1.PUB_PORTAL = 1 '" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N' INSERT INTO #GRID_VARIABLES ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  --Las de nivel 2" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT DISTINCT 2 AS NIVEL,V2.ID_VAR_CAL1 AS IDVAR1, V2.ID  AS IDVAR2, 0 AS IDVAR3,0 AS IDVAR4,0 AS IDVAR5,''   '' + V2.DEN AS DEN, PP.PUNT,CP.CAL,V1.COD AS CODV1,V2.COD AS CODV2,NULL AS CODV3,NULL AS CODV4,NULL AS CODV5,V2.PUB_PORTAL   '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' FROM VAR_CAL2 V2 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN VAR_CAL1 V1 ON V1.ID=V2.ID_VAR_CAL1 '" & vbCrLf
sConsulta = sConsulta & "   IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "    begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                 (select DISTINCT VG.ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal2 v on vg.id=v.id and vg.nivel=2  AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union ' --las compuestas" & vbCrLf
sConsulta = sConsulta & "                SET @SQL=@SQL +'select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT from VAR_CAL_GMN4 VG " & vbCrLf
sConsulta = sConsulta & "                 inner join var_cal3 v on vg.id=v.id and vg.nivel=3   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4 PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 4" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 2    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID, 1 MAT From VAR_CAL2 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                ) VBLES_PROVE ON VBLES_PROVE.ID=V2.ID'" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' LEFT JOIN VAR_PROVE_PUNT PP ON V2.ID=PP.VARCAL AND PP.NIVEL=2 AND PP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CAL_PUNTUACION CP ON PP.VARCAL=CP.VARCAL AND PP.NIVEL=CP.NIVEL AND PP.PUNT BETWEEN CP.INF AND CP.SUP" & vbCrLf
sConsulta = sConsulta & "WHERE  V2.PUB_PORTAL = 1 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  --Las de nivel 3" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT DISTINCT 3 AS NIVEL,V3.ID_VAR_CAL1 AS IDVAR1, V3.ID_VAR_CAL2  AS IDVAR2, V3.ID AS IDVAR3,0 AS IDVAR4,0 AS IDVAR5,''      '' +V3.DEN AS DEN,PP.PUNT,CP.CAL,V1.COD AS CODV1,V2.COD AS CODV2,V3.COD AS CODV3,NULL AS CODV4,NULL AS CODV5,V3.PUB_PORTAL  ' " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' FROM var_cal3 V3'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN var_cal1 V1 ON V1.ID=V3.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN var_cal2 V2 ON V2.ID=V3.ID_VAR_CAL2'" & vbCrLf
sConsulta = sConsulta & "   IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN (Select DISTINCT V.ID, 1 MAT from VAR_CAL_GMN4 VG " & vbCrLf
sConsulta = sConsulta & "                  inner join var_cal3 v on vg.id=v.id and vg.nivel=3  AND V.MATERIAL_QA IS NOT NULL  " & vbCrLf
sConsulta = sConsulta & "                  inner join prove_gmn4 PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 4" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  ) VBLES_PROVE ON VBLES_PROVE.ID=V3.ID'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & " SET @SQL=@SQL + ' LEFT JOIN VAR_PROVE_PUNT PP ON V3.ID=PP.VARCAL AND PP.NIVEL=3 AND PP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON PP.VARCAL=CP.VARCAL AND PP.NIVEL=CP.NIVEL AND PP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "WHERE V3.PUB_PORTAL=1 '" & vbCrLf
sConsulta = sConsulta & " SET @SQL = N' INSERT INTO #GRID_VARIABLES ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " --Las de nivel 4" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT DISTINCT 4 AS NIVEL,V4.ID_VAR_CAL1 AS IDVAR1, V4.ID_VAR_CAL2  AS IDVAR2, V4.ID_VAR_CAL3  AS IDVAR3,V4.ID AS IDVAR4,0 AS IDVAR5,''          '' +V4.DEN AS DEN,PP.PUNT,CP.CAL,V1.COD AS CODV1,V2.COD AS CODV2,V3.COD AS CODV3,V4.COD AS CODV4,NULL AS CODV5,V4.PUB_PORTAL  ' " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' FROM var_cal4 V4'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN var_cal1 V1 ON V1.ID=V4.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN var_cal2 V2 ON V2.ID=V4.ID_VAR_CAL2'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN var_cal3 V3 ON V3.ID=V4.ID_VAR_CAL3'" & vbCrLf
sConsulta = sConsulta & "   IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN (select DISTINCT V.ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "      union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL4 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL4 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  ) VBLES_PROVE ON VBLES_PROVE.ID=V4.ID'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & " SET @SQL=@SQL + ' LEFT JOIN VAR_PROVE_PUNT PP ON V4.ID=PP.VARCAL AND PP.NIVEL=4 AND PP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON PP.VARCAL=CP.VARCAL AND PP.NIVEL=CP.NIVEL AND PP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "WHERE V4.PUB_PORTAL=1 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " --Las de nivel 5" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT DISTINCT 5 AS NIVEL,V5.ID_VAR_CAL1 AS IDVAR1, V5.ID_VAR_CAL2  AS IDVAR2, V5.ID_VAR_CAL3  AS IDVAR3,V5.ID_VAR_CAL4 AS IDVAR4,V5.ID AS IDVAR5,''             '' +V5.DEN AS DEN,PP.PUNT,CP.CAL,V1.COD AS CODV1,V2.COD AS CODV2,V3.COD AS CODV3,V4.COD AS CODV4,V5.COD AS CODV5,V4.PUB_PORTAL  ' " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' FROM var_cal5 V5'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN var_cal1 V1 ON V1.ID=V5.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN var_cal2 V2 ON V2.ID=V5.ID_VAR_CAL2'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN var_cal3 V3 ON V3.ID=V5.ID_VAR_CAL3'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN var_cal4 V4 ON V4.ID=V5.ID_VAR_CAL4'" & vbCrLf
sConsulta = sConsulta & "   IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN (select DISTINCT V.ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  ) VBLES_PROVE ON VBLES_PROVE.ID=V5.ID'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & " SET @SQL=@SQL + ' LEFT JOIN VAR_PROVE_PUNT PP ON V5.ID=PP.VARCAL AND PP.NIVEL=5 AND PP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON PP.VARCAL=CP.VARCAL AND PP.NIVEL=CP.NIVEL AND PP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "WHERE V5.PUB_PORTAL=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- devolvemos las lineas que tenemos hasta ahora + las madres q no estan publicadas pero que tienen hijas q si" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'SELECT * FROM #GRID_VARIABLES '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--madres de nivel 1 que tiene hijas publicas en el  nivel 2" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT 1 AS NIVEL,V1.ID AS IDVAR1,0 AS IDVAR2,0 AS IDVAR3,0 AS IDVAR4,0 AS IDVAR5,V1.DEN,VP.PUNT,CP.CAL,V1.COD AS CODV1,NULL AS CODV2,NULL AS CODV3,NULL AS CODV4,NULL AS CODV5, V1.PUB_PORTAL FROM VAR_CAL1 V1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                 (SELECT DISTINCT V.ID_VAR_CAL1, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal2 v on vg.id=v.id and vg.nivel=2   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4 PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND prove=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union '--las compuestas " & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL +'select DISTINCT V.ID_VAR_CAL1, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal3 v on vg.id=v.id and vg.nivel=3   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND prove=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 4" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4  AND V.MATERIAL_QA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5  AND V.MATERIAL_QA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 2    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL2 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  ) VBLES_PROVE ON  VBLES_PROVE.ID_VAR_CAL1 = V1.ID'" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & " SET @SQL=@SQL + ' LEFT JOIN VAR_PROVE_PUNT VP ON V1.ID = VP.VARCAL AND  VP.NIVEL=1 AND VP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON CP.VARCAL=VP.VARCAL  AND VP.NIVEL=CP.NIVEL AND VP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VAR_CAL2 V2 ON V2.ID_VAR_CAL1= V1.ID AND V2.PUB_PORTAL=1" & vbCrLf
sConsulta = sConsulta & "WHERE V1.PUB_PORTAL=0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES1 ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--madres de nivel 1 que tienen hijas en nivel 3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'  SELECT DISTINCT 1 AS NIVEL,V1.ID AS IDVAR1,0 AS IDVAR2,0 AS IDVAR3,0 AS IDVAR4,0 AS IDVAR5,V1.DEN,VP.PUNT,CP.CAL, V1.COD AS CODV1,NULL AS CODV2,NULL AS CODV3,NULL AS CODV4,NULL AS CODV5,V1.PUB_PORTAL FROM VAR_CAL1 V1'" & vbCrLf
sConsulta = sConsulta & "IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & " SET @SQL=@SQL + ' INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                 (SELECT DISTINCT V.ID_VAR_CAL1, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal2 v on vg.id=v.id and vg.nivel=2   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4 PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND prove=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union '--las compuestas " & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL +'select DISTINCT V.ID_VAR_CAL1, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal3 v on vg.id=v.id and vg.nivel=3   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND prove=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 4" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4  AND V.MATERIAL_QA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5  AND V.MATERIAL_QA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 2    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL2 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  ) VBLES_PROVE ON  VBLES_PROVE.ID_VAR_CAL1 = V1.ID'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' LEFT JOIN VAR_PROVE_PUNT VP ON V1.ID = VP.VARCAL AND  VP.NIVEL=1 AND VP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON CP.VARCAL = VP.VARCAL  AND VP.NIVEL=CP.NIVEL AND VP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VAR_CAL3 V3 ON V3.ID_VAR_CAL1 = V1.ID AND V3.PUB_PORTAL =1" & vbCrLf
sConsulta = sConsulta & "WHERE V1.PUB_PORTAL=0  '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+ ' ORDER BY CODV1, CODV2,CODV3,CODV4,CODV5'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES1 ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--madres de nivel 1 que tienen hijas en nivel 4" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'  SELECT DISTINCT 1 AS NIVEL,V1.ID AS IDVAR1,0 AS IDVAR2,0 AS IDVAR3,0 AS IDVAR4,0 AS IDVAR5,V1.DEN,VP.PUNT,CP.CAL, V1.COD AS CODV1,NULL AS CODV2,NULL AS CODV3,NULL AS CODV4,NULL AS CODV5,V1.PUB_PORTAL FROM VAR_CAL1 V1'" & vbCrLf
sConsulta = sConsulta & "IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & " SET @SQL=@SQL + ' INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                 (SELECT DISTINCT V.ID_VAR_CAL1, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal2 v on vg.id=v.id and vg.nivel=2   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4 PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND prove=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union '--las compuestas " & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL +'select DISTINCT V.ID_VAR_CAL1, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal3 v on vg.id=v.id and vg.nivel=3   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND prove=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 4" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4  AND V.MATERIAL_QA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5  AND V.MATERIAL_QA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 2    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL2 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  ) VBLES_PROVE ON  VBLES_PROVE.ID_VAR_CAL1 = V1.ID'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' LEFT JOIN VAR_PROVE_PUNT VP ON V1.ID = VP.VARCAL AND  VP.NIVEL=1 AND VP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON CP.VARCAL = VP.VARCAL  AND VP.NIVEL=CP.NIVEL AND VP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VAR_CAL4 V4 ON V4.ID_VAR_CAL1 = V1.ID AND V4.PUB_PORTAL =1" & vbCrLf
sConsulta = sConsulta & "WHERE V1.PUB_PORTAL=0   '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+ ' ORDER BY CODV1, CODV2,CODV3,CODV4,CODV5'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES1 ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--madres de nivel 1 que tienen hijas en nivel 5" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'  SELECT DISTINCT 1 AS NIVEL,V1.ID AS IDVAR1,0 AS IDVAR2,0 AS IDVAR3,0 AS IDVAR4,0 AS IDVAR5,V1.DEN,VP.PUNT,CP.CAL, V1.COD AS CODV1,NULL AS CODV2,NULL AS CODV3,NULL AS CODV4,NULL AS CODV5,V1.PUB_PORTAL FROM VAR_CAL1 V1'" & vbCrLf
sConsulta = sConsulta & "IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & " SET @SQL=@SQL + ' INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                 (SELECT DISTINCT V.ID_VAR_CAL1, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal2 v on vg.id=v.id and vg.nivel=2   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4 PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND prove=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union '--las compuestas " & vbCrLf
sConsulta = sConsulta & "                 SET @SQL=@SQL +'select DISTINCT V.ID_VAR_CAL1, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal3 v on vg.id=v.id and vg.nivel=3   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND prove=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 4" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4  AND V.MATERIAL_QA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5  AND V.MATERIAL_QA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 2    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL2 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL1 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  ) VBLES_PROVE ON  VBLES_PROVE.ID_VAR_CAL1 = V1.ID'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' LEFT JOIN VAR_PROVE_PUNT VP ON V1.ID = VP.VARCAL AND  VP.NIVEL=1 AND VP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON CP.VARCAL = VP.VARCAL  AND VP.NIVEL=CP.NIVEL AND VP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VAR_CAL5 V5 ON V5.ID_VAR_CAL1 = V1.ID AND V5.PUB_PORTAL =1" & vbCrLf
sConsulta = sConsulta & "WHERE V1.PUB_PORTAL=0  '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+ ' ORDER BY CODV1, CODV2,CODV3,CODV4,CODV5'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES1 ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---MADRES DE NIVEL2 QUE TIENEN HIJAS EN NIVEL 3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'  SELECT DISTINCT 2 AS NIVEL,V2.ID_VAR_CAL1 AS IDVAR1,V2.ID AS IDVAR2,0 AS IDVAR3,0 AS IDVAR4,0 AS IDVAR5,''   ''+V2.DEN AS DEN,VP.PUNT,CP.CAL,V1.COD AS CODV1,V2.COD AS CODV2,NULL AS CODV3,NULL AS CODV4,NULL AS CODV5,V2.PUB_PORTAL FROM VAR_CAL2 V2'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN VAR_CAL1 V1 ON V1.ID=V2.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & " IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                 (select DISTINCT VG.ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal2 v on vg.id=v.id and vg.nivel=2  AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union ' --las compuestas" & vbCrLf
sConsulta = sConsulta & "                SET @SQL=@SQL +'select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT from VAR_CAL_GMN4 VG " & vbCrLf
sConsulta = sConsulta & "                 inner join var_cal3 v on vg.id=v.id and vg.nivel=3   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4 PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 4" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 2    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID, 1 MAT From VAR_CAL2 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                ) VBLES_PROVE ON VBLES_PROVE.ID=V2.ID'" & vbCrLf
sConsulta = sConsulta & "  end " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+' LEFT JOIN VAR_PROVE_PUNT VP ON V2.ID=VP.VARCAL AND VP.NIVEL=1 AND VP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON CP.VARCAL=VP.VARCAL  AND VP.NIVEL=CP.NIVEL AND VP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VAR_CAL3 V3 ON V3.ID_VAR_CAL2 = V2.ID AND V3.PUB_PORTAL =1" & vbCrLf
sConsulta = sConsulta & "WHERE V2.PUB_PORTAL=0  '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+ ' ORDER BY CODV1, CODV2,CODV3,CODV4,CODV5'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES2 ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---MADRES DE NIVEL2 QUE TIENEN HIJAS EN NIVEL 4" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'  SELECT DISTINCT 2 AS NIVEL,V2.ID_VAR_CAL1 AS IDVAR1,V2.ID AS IDVAR2,0 AS IDVAR3,0 AS IDVAR4,0 AS IDVAR5,''   ''+V2.DEN AS DEN,VP.PUNT,CP.CAL,V1.COD AS CODV1,V2.COD AS CODV2,NULL AS CODV3,NULL AS CODV4,NULL AS CODV5,V2.PUB_PORTAL FROM VAR_CAL2 V2'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN VAR_CAL1 V1 ON V1.ID=V2.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & " IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                 (select DISTINCT VG.ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal2 v on vg.id=v.id and vg.nivel=2  AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union ' --las compuestas" & vbCrLf
sConsulta = sConsulta & "                SET @SQL=@SQL +'select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT from VAR_CAL_GMN4 VG " & vbCrLf
sConsulta = sConsulta & "                 inner join var_cal3 v on vg.id=v.id and vg.nivel=3   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4 PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 4" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 2    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID, 1 MAT From VAR_CAL2 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                ) VBLES_PROVE ON VBLES_PROVE.ID=V2.ID'" & vbCrLf
sConsulta = sConsulta & "  end " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+' LEFT JOIN VAR_PROVE_PUNT VP ON V2.ID=VP.VARCAL AND VP.NIVEL=1 AND VP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON CP.VARCAL=VP.VARCAL  AND VP.NIVEL=CP.NIVEL AND VP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VAR_CAL4 V4 ON V4.ID_VAR_CAL2 = V2.ID AND V4.PUB_PORTAL =1" & vbCrLf
sConsulta = sConsulta & "WHERE V2.PUB_PORTAL=0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+ ' ORDER BY CODV1, CODV2,CODV3,CODV4,CODV5'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES2 ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---MADRES DE NIVEL2 QUE TIENEN HIJAS EN NIVEL 5" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'  SELECT DISTINCT 2 AS NIVEL,V2.ID_VAR_CAL1 AS IDVAR1,V2.ID AS IDVAR2,0 AS IDVAR3,0 AS IDVAR4,0 AS IDVAR5,''   ''+V2.DEN AS DEN,VP.PUNT,CP.CAL,V1.COD AS CODV1,V2.COD AS CODV2,NULL AS CODV3,NULL AS CODV4,NULL AS CODV5,V2.PUB_PORTAL FROM VAR_CAL2 V2'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN VAR_CAL1 V1 ON V1.ID=V2.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & " IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                 (select DISTINCT VG.ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal2 v on vg.id=v.id and vg.nivel=2  AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union ' --las compuestas" & vbCrLf
sConsulta = sConsulta & "                SET @SQL=@SQL +'select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT from VAR_CAL_GMN4 VG " & vbCrLf
sConsulta = sConsulta & "                 inner join var_cal3 v on vg.id=v.id and vg.nivel=3   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4 PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 4" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 2    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID, 1 MAT From VAR_CAL2 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL2 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                ) VBLES_PROVE ON VBLES_PROVE.ID=V2.ID'" & vbCrLf
sConsulta = sConsulta & "  end " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+' LEFT JOIN VAR_PROVE_PUNT VP ON V2.ID=VP.VARCAL AND VP.NIVEL=1 AND VP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON CP.VARCAL=VP.VARCAL  AND VP.NIVEL=CP.NIVEL AND VP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VAR_CAL5 V5 ON V5.ID_VAR_CAL2 = V2.ID AND V5.PUB_PORTAL =1" & vbCrLf
sConsulta = sConsulta & "WHERE V2.PUB_PORTAL=0  '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+ ' ORDER BY CODV1, CODV2,CODV3,CODV4,CODV5'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES2 ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---MADRES DE NIVEL3 QUE TIENEN HIJAS EN NIVEL 4" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'  SELECT DISTINCT 3 AS NIVEL,V3.ID_VAR_CAL1 AS IDVAR1,V3.ID_VAR_CAL2 AS IDVAR2,V3.ID AS IDVAR3,0 AS IDVAR4,0 AS IDVAR5,''      ''+V3.DEN AS DEN,VP.PUNT,CP.CAL,V1.COD AS CODV1,V2.COD AS CODV2,V3.COD AS CODV3,NULL AS CODV4,NULL AS CODV5,V3.PUB_PORTAL FROM VAR_CAL3 V3'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN VAR_CAL1 V1 ON V1.ID=V3.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN VAR_CAL2 V2 ON V2.ID=V3.ID_VAR_CAL2 and V2.ID_VAR_CAL1=V3.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & " IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                 (select DISTINCT VG.ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal3 v on vg.id=v.id and vg.nivel=3  AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union ' --las compuestas" & vbCrLf
sConsulta = sConsulta & "                SET @SQL=@SQL +'select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5 " & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                ) VBLES_PROVE ON VBLES_PROVE.ID=V3.ID'" & vbCrLf
sConsulta = sConsulta & "  end " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+' LEFT JOIN VAR_PROVE_PUNT VP ON V3.ID=VP.VARCAL AND VP.NIVEL=1 AND VP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON CP.VARCAL=VP.VARCAL  AND VP.NIVEL=CP.NIVEL AND VP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VAR_CAL4 V4 ON V4.ID_VAR_CAL3 = V3.ID AND V4.PUB_PORTAL =1" & vbCrLf
sConsulta = sConsulta & "WHERE V3.PUB_PORTAL=0  '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+ ' ORDER BY CODV1, CODV2,CODV3,CODV4,CODV5'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES3 ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "---MADRES DE NIVEL3 QUE TIENEN HIJAS EN NIVEL 5" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'  SELECT DISTINCT 3 AS NIVEL,V3.ID_VAR_CAL1 AS IDVAR1,V3.ID_VAR_CAL2 AS IDVAR2,V3.ID AS IDVAR3,0 AS IDVAR4,0 AS IDVAR5,''      ''+V3.DEN AS DEN,VP.PUNT,CP.CAL,V1.COD AS CODV1,V2.COD AS CODV2,V3.COD AS CODV3,NULL AS CODV4,NULL AS CODV5,V3.PUB_PORTAL FROM VAR_CAL3 V3'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN VAR_CAL1 V1 ON V1.ID=V3.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN VAR_CAL2 V2 ON V2.ID=V3.ID_VAR_CAL2 and V2.ID_VAR_CAL1=V3.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & " IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                 (select DISTINCT VG.ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal3 v on vg.id=v.id and vg.nivel=3  AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union ' --las compuestas" & vbCrLf
sConsulta = sConsulta & "                SET @SQL=@SQL +'select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                 union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 3    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID, 1 MAT From VAR_CAL3 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL3 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                ) VBLES_PROVE ON VBLES_PROVE.ID=V3.ID'" & vbCrLf
sConsulta = sConsulta & "  end " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+' LEFT JOIN VAR_PROVE_PUNT VP ON V3.ID=VP.VARCAL AND VP.NIVEL=1 AND VP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON CP.VARCAL=VP.VARCAL  AND VP.NIVEL=CP.NIVEL AND VP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VAR_CAL5 V5 ON V5.ID_VAR_CAL3 = V3.ID AND V5.PUB_PORTAL =1" & vbCrLf
sConsulta = sConsulta & "WHERE V3.PUB_PORTAL=0  '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+ ' ORDER BY CODV1, CODV2,CODV3,CODV4,CODV5'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES3 ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---MADRES DE NIVEL4 QUE TIENEN HIJAS EN NIVEL 5" & vbCrLf
sConsulta = sConsulta & "SET @SQL =N'  SELECT DISTINCT 4 AS NIVEL,V4.ID_VAR_CAL1 AS IDVAR1,V4.ID_VAR_CAL2 AS IDVAR2,V4.ID_VAR_CAL3 AS IDVAR3,V4.ID AS IDVAR4,0 AS IDVAR5,''         ''+V4.DEN AS DEN,VP.PUNT,CP.CAL,V1.COD AS CODV1,V2.COD AS CODV2,V3.COD AS CODV3,V4.COD AS CODV4,NULL AS CODV5,V4.PUB_PORTAL FROM VAR_CAL4 V4'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN VAR_CAL1 V1 ON V1.ID=V4.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN VAR_CAL2 V2 ON V2.ID=V4.ID_VAR_CAL2 and V2.ID_VAR_CAL1=V4.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN VAR_CAL3 V3 ON V3.ID=V4.ID_VAR_CAL3 and V3.ID_VAR_CAL2=V4.ID_VAR_CAL2 and V3.ID_VAR_CAL1=V4.ID_VAR_CAL1'" & vbCrLf
sConsulta = sConsulta & " IF @MAT=1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' INNER JOIN (select DISTINCT V.ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal4 v on vg.id=v.id and vg.nivel=4   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "      union --las compuestas 5" & vbCrLf
sConsulta = sConsulta & "                 select DISTINCT V.ID_VAR_CAL4 ID, 1 MAT" & vbCrLf
sConsulta = sConsulta & "                 from VAR_CAL_GMN4 VG inner join var_cal5 v on vg.id=v.id and vg.nivel=5   AND V.MATERIAL_QA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                 inner join  prove_gmn4  PM ON PM.GMN1=VG.GMN1 AND PM.GMN2=VG.GMN2 AND PM.GMN3=VG.GMN3 and PM.GMN4=VG.GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 4    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID, 1 MAT From VAR_CAL4 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "       union--puntuable todos 5    " & vbCrLf
sConsulta = sConsulta & "       select DISTINCT V.ID_VAR_CAL4 ID, 1 MAT From VAR_CAL5 V WHERE V.MATERIAL_QA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  ) VBLES_PROVE ON VBLES_PROVE.ID=V4.ID'" & vbCrLf
sConsulta = sConsulta & "  end " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+' LEFT JOIN VAR_PROVE_PUNT VP ON V4.ID=VP.VARCAL AND VP.NIVEL=1 AND VP.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAL_PUNTUACION CP ON CP.VARCAL=VP.VARCAL  AND VP.NIVEL=CP.NIVEL AND VP.PUNT BETWEEN CP.INF AND CP.SUP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN VAR_CAL5 V5 ON V5.ID_VAR_CAL4 = V4.ID AND V5.PUB_PORTAL =1" & vbCrLf
sConsulta = sConsulta & "WHERE V4.PUB_PORTAL=0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+ ' ORDER BY CODV1, CODV2,CODV3,CODV4,CODV5'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_VARIABLES4 ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)', @PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'SELECT * FROM #GRID_VARIABLES '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' UNION SELECT * FROM #GRID_VARIABLES1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' UNION SELECT * FROM #GRID_VARIABLES2'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' UNION SELECT * FROM #GRID_VARIABLES3'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' UNION SELECT * FROM #GRID_VARIABLES4'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+ ' ORDER BY CODV1, CODV2,CODV3,CODV4,CODV5'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50)',@PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT VP.PUNT, CP.CAL FROM VAR_PROVE_PUNT VP " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CAL_PUNTUACION CP ON VP.VARCAL = CP.VARCAL AND VP.NIVEL=CP.NIVEL AND (VP.PUNT BETWEEN CP.INF AND CP.SUP)" & vbCrLf
sConsulta = sConsulta & "       WHERE VP.PROVE=@PROVE AND VP.VARCAL=0 AND VP.NIVEL=0'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE NVARCHAR(100)',@PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--borramos la tabla temporal" & vbCrLf
sConsulta = sConsulta & "DROP TABLE  #GRID_VARIABLES" & vbCrLf
sConsulta = sConsulta & "DROP TABLE  #GRID_VARIABLES1" & vbCrLf
sConsulta = sConsulta & "DROP TABLE  #GRID_VARIABLES2" & vbCrLf
sConsulta = sConsulta & "DROP TABLE  #GRID_VARIABLES3" & vbCrLf
sConsulta = sConsulta & "DROP TABLE  #GRID_VARIABLES4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CALCULAR_DETALLE_PUJAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CALCULAR_DETALLE_PUJAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_CALCULAR_DETALLE_PUJAS @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @ITEM INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAP1 DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAP2 DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ITEM IS NULL" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM DETALLE_PUJAS  WHERE ANYO = @ANYO   AND GMN1 = @GMN1   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM DETALLE_PUJAS  WHERE ANYO = @ANYO   AND GMN1 = @GMN1   AND PROCE = @PROCE   AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT IO.ANYO, IO.GMN1, IO.PROCE, IO.ITEM, IO.PROVE, IO.PRECIO * PO.CAMBIO PRECIO INTO #PRECIOS_MAXIMOS" & vbCrLf
sConsulta = sConsulta & "     FROM (SELECT IO.ANYO, IO.GMN1, IO.PROCE, IO.ITEM, IO.PROVE, MAX(IO.NUM) OFE" & vbCrLf
sConsulta = sConsulta & "                  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN PROCE_OFE PO ON IO.ANYO = PO.ANYO AND IO.GMN1 = PO.GMN1 AND IO.PROCE = PO.PROCE AND IO.PROVE = PO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                  WHERE PO.ANYO = @ANYO   AND PO.GMN1 = @GMN1   AND PO.PROCE = @PROCE   AND PO.ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "                                AND IO.ITEM = CASE WHEN @ITEM IS NULL THEN IO.ITEM ELSE @ITEM END" & vbCrLf
sConsulta = sConsulta & "                 GROUP BY  IO.ANYO, IO.GMN1, IO.PROCE, IO.ITEM, IO.PROVE ) MAXOFES" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN ITEM_OFE IO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             ON MAXOFES.ANYO = IO.ANYO AND MAXOFES.GMN1 = IO.GMN1 AND MAXOFES.PROCE = IO.PROCE AND MAXOFES.ITEM = IO.ITEM" & vbCrLf
sConsulta = sConsulta & "            AND MAXOFES.PROVE = IO.PROVE AND MAXOFES.OFE = IO.NUM" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROCE_OFE PO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              ON IO.ANYO = PO.ANYO AND IO.GMN1 = PO.GMN1 AND IO.PROCE = PO.PROCE AND IO.PROVE = PO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR SELECT IO.ITEM, IO.PROVE, IO.NUM, IO.PRECIO * PO.CAMBIO PRECIO, PO.FECREC" & vbCrLf
sConsulta = sConsulta & "      FROM ITEM_OFE IO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROCE_OFE PO WITH (NOLOCK)  ON IO.ANYO = PO.ANYO AND IO.GMN1 = PO.GMN1 AND IO.PROCE = PO.PROCE AND IO.PROVE = PO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "       WHERE PO.ANYO = @ANYO   AND PO.GMN1 = @GMN1   AND PO.PROCE = @PROCE   AND PO.ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "                     AND IO.PRECIO * PO.CAMBIO >= (SELECT PRECIO FROM #PRECIOS_MAXIMOS PM WITH (NOLOCK) WHERE PO.ANYO = PM.ANYO AND PO.GMN1 = PM.GMN1 AND PO.PROCE = PM.PROCE AND PO.PROVE = PM.PROVE AND IO.ITEM = PM.ITEM)" & vbCrLf
sConsulta = sConsulta & "                     AND IO.ITEM  = CASE WHEN @ITEM IS NULL THEN IO.ITEM ELSE @ITEM END" & vbCrLf
sConsulta = sConsulta & "       ORDER BY  IO.ITEM, IO.PRECIO  DESC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT IO.ITEM, IO.PROVE, IO.NUM, IO.PRECIO * PO.CAMBIO PRECIO, PO.FECREC" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROCE_OFE PO WITH (NOLOCK) ON IO.ANYO = PO.ANYO AND IO.GMN1 = PO.GMN1 AND IO.PROCE = PO.PROCE AND IO.PROVE = PO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "      WHERE PO.ANYO = @ANYO   AND PO.GMN1 = @GMN1   AND PO.PROCE = @PROCE   AND PO.ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "                    AND IO.PRECIO * PO.CAMBIO >= (SELECT PRECIO FROM #PRECIOS_MAXIMOS PM WITH (NOLOCK) WHERE PO.ANYO = PM.ANYO AND PO.GMN1 = PM.GMN1 AND PO.PROCE = PM.PROCE AND PO.PROVE = PM.PROVE AND IO.ITEM = PM.ITEM)" & vbCrLf
sConsulta = sConsulta & "                   AND IO.ITEM  = CASE WHEN @ITEM IS NULL THEN IO.ITEM ELSE @ITEM END" & vbCrLf
sConsulta = sConsulta & "      ORDER BY  IO.ITEM, IO.PRECIO  DESC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEANT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIOANT FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEMANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAANT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHA DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXIDPUJA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @BSUPERA TINYINT" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @ITEM, @PROVE, @OFE, @PRECIO,@FECHA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @ITEM != ISNULL(@ITEMANT,@ITEM + 1)" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @PROVEANT = NULL" & vbCrLf
sConsulta = sConsulta & "        SET @PRECIOANT = NULL" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    IF @PRECIO<=ISNULL(@PRECIOANT,@PRECIO+1)" & vbCrLf
sConsulta = sConsulta & "      IF @PROVE!=ISNULL(@PROVEANT,@PROVE + '_X')" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @PRECIO = ISNULL(@PRECIOANT,@PRECIO+1)" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @BSUPERA = 0" & vbCrLf
sConsulta = sConsulta & "              SET @FECHAP1 = (SELECT MIN(FECREC) FROM PROCE_OFE PO" & vbCrLf
sConsulta = sConsulta & "                                               INNER JOIN ITEM_OFE IO ON PO.ANYO = IO.ANYO AND PO.GMN1 = IO.GMN1 AND PO.PROCE = IO.PROCE " & vbCrLf
sConsulta = sConsulta & "                                               WHERE PO.ANYO = @ANYO AND PO.GMN1=@GMN1 AND PO.PROCE = @PROCE AND PO.PROVE = @PROVEANT AND IO.ITEM = @ITEMANT" & vbCrLf
sConsulta = sConsulta & "                                                             AND IO.PRECIO = @PRECIOANT)" & vbCrLf
sConsulta = sConsulta & "              SET @FECHAP2 = (SELECT MIN(FECREC) FROM PROCE_OFE PO" & vbCrLf
sConsulta = sConsulta & "                                               INNER JOIN ITEM_OFE IO ON PO.ANYO = IO.ANYO AND PO.GMN1 = IO.GMN1 AND PO.PROCE = IO.PROCE" & vbCrLf
sConsulta = sConsulta & "                                               WHERE PO.ANYO = @ANYO AND PO.GMN1=@GMN1 AND PO.PROCE = @PROCE AND PO.PROVE = @PROVE AND IO.ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "                                                              AND IO.PRECIO = @PRECIO)" & vbCrLf
sConsulta = sConsulta & "              IF @FECHAP1>@FECHAP2" & vbCrLf
sConsulta = sConsulta & "                        SET @BSUPERA=1" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "              SET @BSUPERA = 1" & vbCrLf
sConsulta = sConsulta & "          IF @BSUPERA = 1" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SELECT @MAXIDPUJA = ISNULL(MAX(ID),0)  + 1                 FROM DETALLE_PUJAS" & vbCrLf
sConsulta = sConsulta & "                    WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "              INSERT INTO DETALLE_PUJAS (ANYO, GMN1, PROCE, ITEM, ID, OFE, PROVE, FECHA, PRECIO)" & vbCrLf
sConsulta = sConsulta & "                       VALUES (@ANYO, @GMN1, @PROCE, @ITEM, @MAXIDPUJA , @OFE, @PROVE, @FECHA, @PRECIO)" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "        END " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        IF @PRECIO < ISNULL(@PRECIOANT,@PRECIO+1) " & vbCrLf
sConsulta = sConsulta & "          UPDATE DETALLE_PUJAS " & vbCrLf
sConsulta = sConsulta & "             SET OFE = @OFE," & vbCrLf
sConsulta = sConsulta & "                 PROVE = @PROVE," & vbCrLf
sConsulta = sConsulta & "                 FECHA = @FECHA," & vbCrLf
sConsulta = sConsulta & "                 PRECIO = @PRECIO" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "             AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "             AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "             AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "             AND ID = @MAXIDPUJA" & vbCrLf
sConsulta = sConsulta & "    SET @ITEMANT = @ITEM " & vbCrLf
sConsulta = sConsulta & "    IF @BSUPERA = 1" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @PROVEANT = @PROVE" & vbCrLf
sConsulta = sConsulta & "      SET @PRECIOANT = @PRECIO" & vbCrLf
sConsulta = sConsulta & "      SET @FECHAANT = @FECHA" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "    SET @BSUPERA = 0" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @ITEM, @PROVE, @OFE, @PRECIO,@FECHA" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_MON_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_MON_OFE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_MODIF_MON_OFE @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50) ,@OFE INT,@CAMANT FLOAT, @CAMBIO FLOAT AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE ITEM_OFE " & vbCrLf
sConsulta = sConsulta & "           SET PRECIO=(PRECIO/@CAMANT)*@CAMBIO, " & vbCrLf
sConsulta = sConsulta & "               PREC_VALIDO=(PREC_VALIDO/@CAMANT)*@CAMBIO, " & vbCrLf
sConsulta = sConsulta & "               PRECIO2= (PRECIO2/@CAMANT)*@CAMBIO, " & vbCrLf
sConsulta = sConsulta & "               PRECIO3 = (PRECIO3/@CAMANT)*@CAMBIO" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND NUM=@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "           SET VALOR_NUM=(VALOR_NUM/@CAMANT)*@CAMBIO" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ATRIB WITH (NOLOCK) INNER JOIN PROCE_ATRIB WITH (NOLOCK) ON OFE_ATRIB.ATRIB_ID=PROCE_ATRIB.ID AND PROCE_ATRIB.OP_PREC IN ('+','-')" & vbCrLf
sConsulta = sConsulta & "    WHERE OFE_ATRIB.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ATRIB.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ATRIB.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ATRIB.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ATRIB.OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "           SET VALOR_NUM=(VALOR_NUM/@CAMANT)*@CAMBIO" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB WITH (NOLOCK) INNER JOIN PROCE_ATRIB WITH (NOLOCK) ON OFE_GR_ATRIB.ATRIB_ID=PROCE_ATRIB.ID AND PROCE_ATRIB.OP_PREC IN ('+','-')" & vbCrLf
sConsulta = sConsulta & "    WHERE OFE_GR_ATRIB.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND OFE_GR_ATRIB.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND OFE_GR_ATRIB.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_GR_ATRIB.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_GR_ATRIB.OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "           SET VALOR_NUM=(VALOR_NUM/@CAMANT)*@CAMBIO" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ATRIB WITH (NOLOCK) INNER JOIN PROCE_ATRIB WITH (NOLOCK) ON OFE_ITEM_ATRIB.ATRIB_ID=PROCE_ATRIB.ID AND PROCE_ATRIB.OP_PREC IN ('+','-')" & vbCrLf
sConsulta = sConsulta & "    WHERE OFE_ITEM_ATRIB.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ITEM_ATRIB.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ITEM_ATRIB.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ITEM_ATRIB.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ITEM_ATRIB.OFE=@OFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_NUEVA_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_NUEVA_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_NUEVA_OFERTA @ANYO int, @GMN1 varchar(50), @PROCE int, @PROVE varchar(50), @OFE int, @ULT SMALLINT,@MONNEW VARCHAR(50),@CAMBIONEW FLOAT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIOOLD FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONOLD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "--  ESTA SP SOLO SE PUEDE LLAMAR DESDE EL TRIGGER DE INSERCION DE PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFE=-1" & vbCrLf
sConsulta = sConsulta & "    SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE < @OFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@MAXOFE > 0 ) --NO ES LA PRIMERA OFERTA QUE METE EL PROVEEDOR AS� QUE COPIAMOS LA ANTERIOR" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "         --    Leo la moneda de la oferta que voy a copiar" & vbCrLf
sConsulta = sConsulta & "         SELECT @MONOLD=MON,@CAMBIOOLD=CAMBIO FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO PROCE_OFE_ADJUN (ANYO, GMN1, PROCE, PROVE, OFE, ID,NOM, COM, IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO, GMN1, PROCE, PROVE, @OFE, ID,NOM ,COM, IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "          FROM PROCE_OFE_ADJUN WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_ATRIB (ANYO,GMN1,PROCE,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND " & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO GRUPO_OFE (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,OBSADJUN" & vbCrLf
sConsulta = sConsulta & "          FROM GRUPO_OFE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_GR_ADJUN (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ADJUN WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_GR_ATRIB (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND )" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND " & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ATRIB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @ULT = 1 " & vbCrLf
sConsulta = sConsulta & "               UPDATE ITEM_OFE SET ULT=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--        IF @MONOLD = @MONNEW " & vbCrLf
sConsulta = sConsulta & "/*            INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,ULT,COMENT1,COMENT2,COMENT3)" & vbCrLf
sConsulta = sConsulta & "            SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,@ULT,COMENT1,COMENT2,COMENT3" & vbCrLf
sConsulta = sConsulta & "             FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "             WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "     INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,ULT,COMENT1,COMENT2,COMENT3,PREC_VALIDO)" & vbCrLf
sConsulta = sConsulta & "     SELECT ITEM_OFE.ANYO,ITEM_OFE.GMN1,ITEM_OFE.PROCE,ITEM_OFE.ITEM,ITEM_OFE.PROVE,@OFE,ITEM_OFE.PRECIO,ITEM_OFE.PRECIO2,ITEM_OFE.PRECIO3,ITEM_OFE.USAR,ITEM_OFE.CANTMAX,ITEM_OFE.OBSADJUN,@ULT,ITEM_OFE.COMENT1,ITEM_OFE.COMENT2,ITEM_OFE.COMENT3,CASE WHEN ITEM.EST=1 THEN ITEM_OFE.PREC_VALIDO ELSE NULL END" & vbCrLf
sConsulta = sConsulta & "     FROM ITEM_OFE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN ITEM WITH (NOLOCK) ON ITEM_OFE.ANYO=ITEM.ANYO AND ITEM_OFE.GMN1=ITEM.GMN1_PROCE AND ITEM_OFE.PROCE = ITEM.PROCE AND ITEM_OFE.ITEM = ITEM.ID" & vbCrLf
sConsulta = sConsulta & "     WHERE ITEM_OFE.ANYO=@ANYO AND ITEM_OFE.GMN1=@GMN1 AND ITEM_OFE.PROCE=@PROCE AND ITEM_OFE.PROVE=@PROVE AND ITEM_OFE.NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*        ELSE" & vbCrLf
sConsulta = sConsulta & "            INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,ULT)" & vbCrLf
sConsulta = sConsulta & "            SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,((PRECIO/@CAMBIOOLD)*@CAMBIONEW),((PRECIO2/@CAMBIOOLD)*@CAMBIONEW),((PRECIO3/@CAMBIOOLD)*@CAMBIONEW),USAR,CANTMAX,OBSADJUN,@ULT" & vbCrLf
sConsulta = sConsulta & "             FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "             WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_ITEM_ADJUN (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ADJUN WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_ITEM_ATRIB (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ATRIB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @MONOLD <> @MONNEW " & vbCrLf
sConsulta = sConsulta & "            EXEC SP_MODIF_MON_OFE @ANYO, @GMN1, @PROCE, @PROVE, @OFE, @CAMBIOOLD,@CAMBIONEW" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "        INSERT INTO GRUPO_OFE (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ANYO,@GMN1,@PROCE,COD, @PROVE,@OFE" & vbCrLf
sConsulta = sConsulta & "          FROM PROCE_GRUPO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,CANTMAX,ULT,FECACT)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ANYO,@GMN1,@PROCE,ID,@PROVE,@OFE,NULL,NULL,NULL,NULL,@ULT,NULL" & vbCrLf
sConsulta = sConsulta & "          FROM ITEM WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         UPDATE PROCE SET O=(O+1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_A_DESPUB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESOS_A_DESPUB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_A_DESPUB(@EQP VARCHAR(50), @COM VARCHAR(50),@DESDES INT ,@DESADJ INT,@RESPADJ TINYINT,@RESPPUB TINYINT) AS" & vbCrLf
sConsulta = sConsulta & "IF @RESPADJ=0  AND @RESPPUB=0" & vbCrLf
sConsulta = sConsulta & "   if @DESADJ=0 " & vbCrLf
sConsulta = sConsulta & "            SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , '' AS GMN2, '' AS GMN3, '' AS GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "           WHERE (PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <=convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC ,Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "   else" & vbCrLf
sConsulta = sConsulta & "        if @DESDES=0" & vbCrLf
sConsulta = sConsulta & "            SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , '' AS GMN2, '' AS GMN3, '' AS GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "           WHERE     " & vbCrLf
sConsulta = sConsulta & "            (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.EST,PROCE.FECAPE ,PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "        else" & vbCrLf
sConsulta = sConsulta & "            SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , '' AS GMN2,'' AS GMN3, '' AS GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "           WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <=convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111))" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "           OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.EST,PROCE.FECAPE ,PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "  IF @RESPADJ=0  AND @RESPPUB=1" & vbCrLf
sConsulta = sConsulta & "    if @DESADJ=0" & vbCrLf
sConsulta = sConsulta & "        SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "           PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , '' AS GMN2, '' AS GMN3, '' AS GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           WHERE PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <= convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EQP=@EQP AND PROCE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "          SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "             PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "             Proce.FECNEC , Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "             From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "             Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "             AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "             WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <=convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "             AND PROCE.EQP=@EQP AND PROCE.COM=@COM)" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "             OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day,@DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "             AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "               GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.EST,PROCE.FECAPE, PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "             ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "  Else" & vbCrLf
sConsulta = sConsulta & "    IF @RESPADJ=1  AND @RESPPUB=0" & vbCrLf
sConsulta = sConsulta & "       IF @DESDES=0" & vbCrLf
sConsulta = sConsulta & "         SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "           PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "           WHERE convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EQP=@EQP AND PROCE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.EST,PROCE.FECAPE, PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "           PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , '' AS GMN2, '' AS GMN3, '' AS GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "           WHERE ( (PROCE_PROVE.PUB=1 AND convert(char,getdate(),111)<=convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111) )" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "           OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EQP=@EQP AND PROCE.COM=@COM))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC ,  Proce.EST,PROCE.FECAPE, PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "    IF @RESPADJ=1  AND @RESPPUB=1" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "     SELECT  PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "     PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "     Proce.FECNEC , '' AS GMN2, '' AS GMN3, '' AS GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "     From Proce" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "     Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "     AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "     WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111)<=convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111))" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)))" & vbCrLf
sConsulta = sConsulta & "     AND PROCE.EQP=@EQP AND PROCE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "     AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "      GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.EST,PROCE.FECAPE, PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "      ORDER BY PROCE.FECLIMOFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Private Sub V_31600_Triggers_44()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VERSION_INSTANCIA_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[VERSION_INSTANCIA_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER VERSION_INSTANCIA_TG_INS ON dbo.VERSION_INSTANCIA" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA SET ULT_VERSION = I.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "  FROM INSTANCIA  INS" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON INS.id = I.INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_UON3_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_UON3_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf

sConsulta = sConsulta & "CREATE TRIGGER PROCE_UON3_TG_INS ON dbo.PROCE_UON3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@UON1 VARCHAR(50),@UON2 VARCHAR(50),@UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT,@PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_UON3_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,UON1,UON2,UON3,PORCEN FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_UON3_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_UON3_Ins INTO @ANYO,@GMN1,@PROCE,@UON1,@UON2,@UON3,@PORCEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE  ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "         WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                            INSERT INTO ITEM_UON3 (ANYO,GMN1,PROCE,ITEM,UON1,UON2,UON3,PORCEN) VALUES (@ANYO,@GMN1,@PROCE, @ITEM,@UON1,@UON2,@UON3,@PORCEN)" & vbCrLf
sConsulta = sConsulta & "                            FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "               End" & vbCrLf
sConsulta = sConsulta & "         Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_UON3_Ins INTO @ANYO,@GMN1,@PROCE,@UON1,@UON2,@UON3,@PORCEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_UON3_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_UON3_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ADJUN_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ADJUN_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ADJUN_TG_INS ON dbo.ADJUN" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ADJUN SET FECALTA=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM ADJUN A" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON A.ID = I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ART4_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ART4_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ART4_TG_INS ON dbo.ART4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50),@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ART4_Ins CURSOR LOCAL  FOR SELECT GMN1,GMN2,GMN3,GMN4,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ART4_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ART4_Ins INTO @GMN1,@GMN2,@GMN3,@GMN4,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_ART4 (PROVE,GMN1,GMN2,GMN3,GMN4,ART,HOM) SELECT PROVE,@GMN1,@GMN2,@GMN3,@GMN4,@COD,0 FROM PROVE_GMN4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ART4_ATRIB(GMN1,GMN2,GMN3,GMN4,ART,ATRIB) SELECT GMN1,GMN2,GMN3,GMN4,@COD,ATRIB FROM GMN4_ATRIB WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ART4_Ins INTO @GMN1,@GMN2,@GMN3,@GMN4,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ART4_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ART4_Ins*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50),@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ART4_Ins CURSOR LOCAL  FOR SELECT COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ART4_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ART4_Ins INTO @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_ART4 (PROVE,ART,HOM) SELECT PROVE,@COD,0 FROM PROVE_GMN4 WITH (NOLOCK) WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ART4_ATRIB(ART,ATRIB) SELECT @COD,ATRIB FROM GMN4_ATRIB WITH (NOLOCK) WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ART4_Ins INTO @COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ART4_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ART4_Ins" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DEF_ATRIB_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[DEF_ATRIB_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER DEF_ATRIB_TG_INSUPD ON dbo.DEF_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE DEF_ATRIB SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON DA.ID = I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_ADJ_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_ADJ_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
  sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ITEM_ADJ_TG_INS ON [ITEM_ADJ]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADOPROCESO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_ADJ_Ins CURSOR FOR SELECT I.ANYO,I.GMN1,I.PROCE,I.ITEM,I.PROVE FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ITEM IT WITH (NOLOCK) ON I.ITEM=IT.ID AND I.ANYO=IT.ANYO AND I.GMN1=IT.GMN1_PROCE AND I.PROCE=IT.PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_ADJ_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_ADJ_Ins INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @ESTADOPROCESO = (SELECT PROCE.EST FROM PROCE WITH (NOLOCK) WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "   IF @ESTADOPROCESO <= 7  /* 7= Con ofertas */" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE SET EST =10   WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "   IF @ESTADOPROCESO = 8" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE SET EST =9   WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "_FETCH:" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_ADJ_Ins INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_ADJ_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_ADJ_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_ADJ_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_ADJ_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ITEM_ADJ_TG_UPD  ON [ITEM_ADJ] " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CATALOGO INTEGER" & vbCrLf
sConsulta = sConsulta & "--Variables para pasar PROVE de Potencial a Real:" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARGEN_QA_ADJ AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_ADJ_Upd CURSOR FOR SELECT I.ANYO, IT.GMN1,IT.GMN2, IT.GMN3, IT.GMN4, I.PROCE, I.ITEM, I.PROVE, I.CATALOGO FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ITEM IT WITH (NOLOCK) ON I.ITEM=IT.ID AND I.ANYO=IT.ANYO AND I.GMN1=IT.GMN1_PROCE AND I.PROCE=IT.PROCE" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_ADJ_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_ADJ_Upd INTO @ANYO,@GMN1, @GMN2, @GMN3, @GMN4,@PROCE,@ITEM,@PROVE,@CATALOGO" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(CATALOGO)  AND @CATALOGO=1 " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT PED_APROV FROM PROCE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) = 0 " & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE SET PED_APROV =1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " --Pasar PROVE de Potencial a Real" & vbCrLf
sConsulta = sConsulta & "  IF UPDATE(CANT_ADJ)" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @PARGEN_QA_ADJ = (SELECT TOP 1 POT_A_VAL_ADJUDICACION FROM PARGEN_QA WITH (NOLOCK))" & vbCrLf
sConsulta = sConsulta & "   IF @PARGEN_QA_ADJ = 1 -- Pasamos de Potencial a Real" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "       --Pero solo si el PROVE es Pot y el Material esta relacionado con Mat QA" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROVE SET CALIDAD_POT = 2 WHERE COD=@PROVE AND CALIDAD_POT = 1 " & vbCrLf
sConsulta = sConsulta & "           AND (SELECT COUNT(PROVE) FROM PROVE_GMN4 WHERE PROVE=@PROVE AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4) > 0" & vbCrLf
sConsulta = sConsulta & "           AND (SELECT COUNT(MATERIAL_QA) FROM MATERIAL_QA_GMN4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4) > 0" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_ADJ_Upd INTO @ANYO,@GMN1,@GMN2, @GMN3, @GMN4,@PROCE,@ITEM,@PROVE,@CATALOGO" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_ADJ_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_ADJ_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_ESP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_ESP_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ITEM_ESP_TG_INS ON dbo.ITEM_ESP" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET FECALTA=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_ESP IE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON IE.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND IE.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND IE.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND IE.ITEM = I.ITEM" & vbCrLf
sConsulta = sConsulta & "               AND IE.ID = I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_ESP_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_ESP_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ITEM_ESP_TG_INSUPD ON dbo.ITEM_ESP" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_ESP IE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON IE.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND IE.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND IE.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND IE.ITEM = I.ITEM" & vbCrLf
sConsulta = sConsulta & "               AND IE.ID = I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_OFE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_OFE_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ITEM_OFE_TG_UPD ON dbo.ITEM_OFE " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE " & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO AS FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEINS varchar(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEINS smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @ULTINS smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVENEW AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @OFENEW AS SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @USUOLD AS VARCHAR(300)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_OFE_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,PROVE,NUM, ULT FROM INSERTED " & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_OFE_Upd " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVEINS,@OFEINS,@ULTINS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & " IF (SELECT SUBASTA FROM PROCE_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE) = 1 AND" & vbCrLf
sConsulta = sConsulta & "    (SELECT ENVIADA FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVEINS AND OFE = @OFEINS) =1" & vbCrLf
sConsulta = sConsulta & " BEGIN " & vbCrLf
sConsulta = sConsulta & "     IF UPDATE(PRECIO) " & vbCrLf
sConsulta = sConsulta & "     BEGIN " & vbCrLf
sConsulta = sConsulta & "        --Obtenemos el precio mas bajo ofertado para ese ITEM, el proveedor y numero de oferta" & vbCrLf
sConsulta = sConsulta & "        set @precio = null" & vbCrLf
sConsulta = sConsulta & "        set @provenew = null" & vbCrLf
sConsulta = sConsulta & "        SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVENEW=A.PROVE,@OFENEW=A.NUM " & vbCrLf
sConsulta = sConsulta & "          FROM ITEM_OFE A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN PROCE_OFE B WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           ON A.ANYO=B.ANYO " & vbCrLf
sConsulta = sConsulta & "                          AND A.GMN1=B.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND A.PROCE=B.PROCE " & vbCrLf
sConsulta = sConsulta & "                          AND A.PROVE=B.PROVE " & vbCrLf
sConsulta = sConsulta & "                          AND A.NUM=B.OFE" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN PROCE C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           ON A.ANYO=C.ANYO " & vbCrLf
sConsulta = sConsulta & "                          AND A.GMN1=C.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND A.PROCE = C.COD" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN PROCE_PROVE PP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           ON A.ANYO = PP.ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND A.GMN1 = PP.GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND A.PROCE = PP.PROCE" & vbCrLf
sConsulta = sConsulta & "                          AND A.PROVE = PP.PROVE" & vbCrLf
sConsulta = sConsulta & "                          " & vbCrLf
sConsulta = sConsulta & "         WHERE A.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "           AND A.GMN1=@GMN1  " & vbCrLf
sConsulta = sConsulta & "           AND A.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "           AND A.ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "           AND A.ULT=1 " & vbCrLf
sConsulta = sConsulta & "           AND A.PRECIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           AND B.ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "           AND PP.PUB = 1" & vbCrLf
sConsulta = sConsulta & "      ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE @ULTOFE smallint" & vbCrLf
sConsulta = sConsulta & "      IF @ULTINS=1 AND @PROVEINS=@PROVENEW AND @OFEINS=@OFENEW" & vbCrLf
sConsulta = sConsulta & "          set @ULTOFE=1" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "          set @ULTOFE=0" & vbCrLf
sConsulta = sConsulta & "      --Para obtener el ususario del mail, COJO EL QUE ESTABA COMO PUJA MINIMA" & vbCrLf
sConsulta = sConsulta & "     set @USUOLD=null" & vbCrLf
sConsulta = sConsulta & "     SELECT  @USUOLD=B.NOMUSU" & vbCrLf
sConsulta = sConsulta & "          FROM OFEMIN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   LEFT JOIN PROCE_OFE B WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           ON A.ANYO=B.ANYO " & vbCrLf
sConsulta = sConsulta & "                          AND A.GMN1=B.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND A.PROCE=B.PROCE " & vbCrLf
sConsulta = sConsulta & "                          AND A.PROVE=B.PROVE                           " & vbCrLf
sConsulta = sConsulta & "                          AND A.OFE=B.OFE                       " & vbCrLf
sConsulta = sConsulta & "         WHERE A.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "           AND A.GMN1=@GMN1  " & vbCrLf
sConsulta = sConsulta & "           AND A.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "           AND A.ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      --Actualizamos OFEMIN con los datos de la oferta m�s baja                       " & vbCrLf
sConsulta = sConsulta & "      UPDATE OFEMIN " & vbCrLf
sConsulta = sConsulta & "         SET PRECIO=@PRECIO," & vbCrLf
sConsulta = sConsulta & "             PROVE=@PROVENEW," & vbCrLf
sConsulta = sConsulta & "             OFE=@OFENEW," & vbCrLf
sConsulta = sConsulta & "             ULTOFE = @ULTOFE," & vbCrLf
sConsulta = sConsulta & "             USUCOM=@USUOLD" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_CALCULAR_DETALLE_PUJAS @ANYO, @GMN1, @PROCE, @ITEM " & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "  END  " & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVEINS,@OFEINS,@ULTINS" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_OFE_Upd " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_OFE_Upd" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
  sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ITEM_TG_UPD ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1  AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ART AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINI AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECFIN AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJ AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJOLD AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECNECMANUAL  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Upd CURSOR FOR SELECT ANYO,GMN1_PROCE,PROCE,ID,ART,CANT,FECINI,FECFIN,OBJ FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@ART,@CANT,@FECINI,@FECFIN,@OBJ" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET  @FECNECMANUAL=(SELECT PROCE.FECNECMANUAL FROM PROCE WITH (NOLOCK) WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "IF @FECNECMANUAL=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE ANYO=@ANYO  AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE)  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(PRES) OR UPDATE(CONF)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     UPDATE PROCE SET PRESTOTAL=(SELECT SUM(PRES) FROM ITEM WITH (NOLOCK) WHERE ANYO= @ANYO  AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND CONF=1 GROUP BY ANYO,GMN1_PROCE, PROCE )   ," & vbCrLf
sConsulta = sConsulta & "     CONSUMIDO=(select sum(item.prec*(item.cant*(porcen/100)))  from item_adj WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     inner join item WITH (NOLOCK) on item_adj.anyo=item.anyo and item_adj.gmn1=item.gmn1_proce and item_adj.proce=item.proce and item_adj.item=item.id and item.conf=1  where item_adj.anyo=@ANYO and item_adj.gmn1=@GMN1 and item_adj.proce=@PROCE )" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(OBJ)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EST = (SELECT EST FROM PROCE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "SET @OBJOLD = (SELECT OBJ FROM DELETED WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ID)" & vbCrLf
sConsulta = sConsulta & "IF @OBJ IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Si estamos en estado 8 y borramos el �ltimo objetivo entonces pasamos a estado 7*/" & vbCrLf
sConsulta = sConsulta & "IF @EST = 8 AND ((SELECT COUNT(*) FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=7 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 9  AND ((SELECT COUNT(*) FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=10 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD IS NOT NULL AND ((SELECT COUNT(*) FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE_PROVE SET NUMOBJ=0,OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD <> @OBJ" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "_FETCH:" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@ART,@CANT,@FECINI,@FECFIN,@OBJ" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_TG_UPDATE]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_TG_UPDATE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ITEM_TG_UPDATE ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT,@GMN1 VARCHAR(5),@PROCE SMALLINT,@GRUPO VARCHAR(20),@ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICITOLD INT ,@SOLICITNEW INT,@SOLICIT_NEW_EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Update CURSOR LOCAL FOR " & vbCrLf
sConsulta = sConsulta & "       SELECT I.ANYO,I.GMN1_PROCE,I.PROCE,I.GRUPO,I.ID,I.SOLICIT,D.SOLICIT" & vbCrLf
sConsulta = sConsulta & "       FROM DELETED D INNER JOIN INSERTED I ON D.ANYO=I.ANYO AND D.GMN1_PROCE=I.GMN1_PROCE AND D.PROCE=I.PROCE AND D.ID=I.ID" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Update" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Update INTO @ANYO,@GMN1,@PROCE,@GRUPO,@ITEM, @SOLICITNEW ,@SOLICITOLD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SOLICIT" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@SOLICITOLD,'')<>ISNULL(@SOLICITNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   SET @SOLICIT_NEW_EST=(SELECT ESTADO FROM INSTANCIA WITH (NOLOCK) WHERE ID=@SOLICITNEW)" & vbCrLf
sConsulta = sConsulta & "   IF @SOLICIT_NEW_EST = 100" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE INSTANCIA SET ESTADO=101 WHERE ID=@SOLICITNEW" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO INSTANCIA_EST (INSTANCIA,FECHA , EST) VALUES(@SOLICITNEW,GETDATE(),101)" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Update INTO @ANYO,@GMN1,@PROCE,@GRUPO,@ITEM, @SOLICITNEW ,@SOLICITOLD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Update" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Update" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OFE_ATRIB_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[OFE_ATRIB_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER OFE_ATRIB_TG_INSUPD ON dbo.OFE_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ATRIB OA" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON OA.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND OA.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND OA.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND OA.PROVE = I.PROVE" & vbCrLf
sConsulta = sConsulta & "               AND OA.OFE = I.OFE" & vbCrLf
sConsulta = sConsulta & "               AND OA.ATRIB_ID = I.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OFE_GR_ATRIB_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[OFE_GR_ATRIB_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER OFE_GR_ATRIB_TG_INSUPD ON dbo.OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ATRIB OA" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON OA.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND OA.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND OA.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND OA.PROVE = I.PROVE" & vbCrLf
sConsulta = sConsulta & "               AND OA.OFE = I.OFE" & vbCrLf
sConsulta = sConsulta & "               AND OA.ATRIB_ID = I.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "               AND OA.GRUPO = I.GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OFE_ITEM_ATRIB_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[OFE_ITEM_ATRIB_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER OFE_ITEM_ATRIB_TG_INSUPD ON dbo.OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ATRIB OIA" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON OIA.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND OIA.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND OIA.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND OIA.ITEM = I.ITEM" & vbCrLf
sConsulta = sConsulta & "               AND OIA.PROVE = I.PROVE" & vbCrLf
sConsulta = sConsulta & "               AND OIA.OFE = I.OFE" & vbCrLf
sConsulta = sConsulta & "               AND OIA.ATRIB_ID = I.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OFEMIN_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[OFEMIN_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER OFEMIN_TG_UPD ON [OFEMIN] " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEOLD varchar(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYOOLD int" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1OLD varchar(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEOLD int" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEMOLD int" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIOOLD float" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEOLD smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVENEW varchar(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYONEW int" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1NEW varchar(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCENEW int" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEMNEW int" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIONEW float" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFENEW smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDENT int" & vbCrLf
sConsulta = sConsulta & "DECLARE @ULTOFE smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @AHORA DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @MARGEN SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECLIMITE DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @USUOLD VARCHAR(300)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF COLUMNS_UPDATED()<>2 and COLUMNS_UPDATED()<>4 and  COLUMNS_UPDATED()<>8  " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE curTG_OFEMIN_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM, PROVE, PRECIO,OFE, ULTOFE,USUCOM FROM INSERTED " & vbCrLf
sConsulta = sConsulta & "   OPEN curTG_OFEMIN_Upd " & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM curTG_OFEMIN_Upd INTO @ANYONEW,@GMN1NEW,@PROCENEW,@ITEMNEW,@PROVENEW,@PRECIONEW,@OFENEW,@ULTOFE,@USUOLD" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ANYOOLD=ANYO, @GMN1OLD=GMN1, @PROCEOLD=PROCE, @PROVEOLD=PROVE,@ITEMOLD=ITEM, @PRECIOOLD=PRECIO, @OFEOLD=OFE" & vbCrLf
sConsulta = sConsulta & "       FROM DELETED" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYONEW AND GMN1=@GMN1NEW AND PROCE=@PROCENEW AND ITEM=@ITEMNEW" & vbCrLf
sConsulta = sConsulta & "      IF @PRECIONEW IS NOT NULL AND ISNULL(@PROVEOLD,@PROVENEW)!=@PROVENEW AND @ULTOFE = 1" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "        ----LAS NOTIFICACIONES" & vbCrLf
sConsulta = sConsulta & "        DECLARE @CONEX  AS VARCHAR(100),@FSP_CIA AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "        DECLARE @BD AS VARCHAR(100),@SERVER AS VARCHAR(100) " & vbCrLf
sConsulta = sConsulta & "        DECLARE @EMAIL AS VARCHAR(100) ,@PROVEDEN AS VARCHAR(500) , @CONTACTO AS VARCHAR(500) , @IDUSU_PORT INT" & vbCrLf
sConsulta = sConsulta & "        DECLARE @PROCEDEN VARCHAR(500),@ITEMDEN VARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "        DECLARE @INST smallint" & vbCrLf
sConsulta = sConsulta & "        SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "        SELECT @PROCEDEN=PROCE.DEN,@ITEMDEN=ITEM.DESCR + ART4.DEN " & vbCrLf
sConsulta = sConsulta & "           FROM PROCE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ITEM WITH (NOLOCK) ON PROCE.ANYO=ITEM.ANYO AND PROCE.GMN1 = ITEM.GMN1_PROCE And PROCE.COD = ITEM.PROCE" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN ART4 WITH (NOLOCK) ON ITEM.ART=ART4.COD  " & vbCrLf
sConsulta = sConsulta & "           WHERE PROCE.ANYO=@ANYONEW AND PROCE.GMN1=@GMN1NEW AND PROCE.COD=@PROCENEW AND ITEM.ID=@ITEMNEW " & vbCrLf
sConsulta = sConsulta & "        --Obtenemos el contacto" & vbCrLf
sConsulta = sConsulta & "        SET @EMAIL = NULL --Primero el contacto que puj� por �ltma vez" & vbCrLf
sConsulta = sConsulta & "         IF @USUOLD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SELECT @EMAIL = CON.EMAIL, @IDUSU_PORT=ID_PORT,@PROVEDEN=PROVE.DEN,@CONTACTO = CON.NOM + ' ' + CON.APE" & vbCrLf
sConsulta = sConsulta & "                 FROM PROVE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN CON WITH (NOLOCK) ON PROVE.COD = CON.PROVE WHERE PROVE.COD = @PROVEOLD AND CON.NOM + ' ' + CON.APE=@USUOLD" & vbCrLf
sConsulta = sConsulta & "        IF @EMAIL IS NULL  --Luego el contacto de subasta" & vbCrLf
sConsulta = sConsulta & "        SELECT @EMAIL = CON.EMAIL, @IDUSU_PORT=ID_PORT,@PROVEDEN=PROVE.DEN,@CONTACTO = CON.NOM + ' ' + CON.APE" & vbCrLf
sConsulta = sConsulta & "           FROM PROVE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN CON WITH (NOLOCK) ON PROVE.COD = CON.PROVE WHERE PROVE.COD = @PROVEOLD AND CON.SUBASTA = 1" & vbCrLf
sConsulta = sConsulta & "        IF @EMAIL IS NULL  --Luego el primer contacto comunicado en GS que sea de portal" & vbCrLf
sConsulta = sConsulta & "          SELECT TOP 1 @EMAIL = CON.EMAIL, @IDUSU_PORT=ID_PORT,@PROVEDEN=PROVE.DEN,@CONTACTO = CON.NOM + ' ' + CON.APE" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             INNER JOIN PROCE_PROVE WITH (NOLOCK) ON PROCE.ANYO = PROCE_PROVE.ANYO AND PROCE.GMN1 = PROCE_PROVE.GMN1 " & vbCrLf
sConsulta = sConsulta & "                        AND PROCE.COD = PROCE_PROVE.PROCE AND PROCE_PROVE.PUB=1" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN PROVE WITH (NOLOCK)  ON PROVE.COD=PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "             INNER JOIN CON WITH (NOLOCK) ON PROVE.COD=CON.PROVE AND CON.PORT=1" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN PROCE_PROVE_PET PPP WITH (NOLOCK) ON PROCE_PROVE.ANYO = PPP.ANYO AND PROCE_PROVE.GMN1=PPP.GMN1 AND PROCE_PROVE.PROCE = PPP.PROCE " & vbCrLf
sConsulta = sConsulta & "                      AND PROCE_PROVE.PROVE = PPP.PROVE AND CON.APE = PPP.APECON AND CON.NOM = PPP.NOMCON AND PPP.TIPOPET IN (0,1)" & vbCrLf
sConsulta = sConsulta & "             WHERE PROCE.ANYO = @ANYONEW AND PROCE.GMN1=@GMN1NEW AND PROCE.COD=@PROCENEW AND PROCE_PROVE.PROVE=@PROVEOLD " & vbCrLf
sConsulta = sConsulta & "        IF @EMAIL IS NULL  --Luego el primer contacto comunicado en GS que sea de portal, por si no lo encuentra por nombre y apellido" & vbCrLf
sConsulta = sConsulta & "          SELECT TOP 1 @EMAIL = C.EMAIL, @IDUSU_PORT=ID_PORT,@PROVEDEN=PROVE.DEN,@CONTACTO = C.NOM + ' ' + C.APE" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             INNER JOIN PROCE_PROVE WITH (NOLOCK) ON PROCE.ANYO = PROCE_PROVE.ANYO AND PROCE.GMN1 = PROCE_PROVE.GMN1 AND PROCE.COD = PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & "                        AND PROCE_PROVE.PUB=1" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN PROVE WITH (NOLOCK) ON PROVE.COD=PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "             LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "                 SELECT C1.PROVE, C1.ID, C1.APE,C1.NOM, C1.EMAIL  , C1.ID_PORT" & vbCrLf
sConsulta = sConsulta & "                  FROM (SELECT PROVE, MAX(ID) ID FROM CON WHERE CON.RECPET=1 AND CON.PORT = 1 GROUP BY PROVE) C2 INNER JOIN CON C1 WITH (NOLOCK) ON C2.PROVE = C1.PROVE AND C2.ID = C1.ID" & vbCrLf
sConsulta = sConsulta & "                  WHERE C1.RECPET=1 AND C1.PORT = 1) C ON PROVE.COD = C.PROVE" & vbCrLf
sConsulta = sConsulta & "             WHERE PROCE.ANYO = @ANYONEW AND PROCE.GMN1=@GMN1NEW AND PROCE.COD=@PROCENEW AND PROCE_PROVE.PROVE=@PROVEOLD " & vbCrLf
sConsulta = sConsulta & "                  AND EXISTS (SELECT ID FROM PROCE_PROVE_PET PPP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                        WHERE PPP.ANYO = PROCE_PROVE.ANYO AND PPP.GMN1=PROCE_PROVE.GMN1 AND PPP.PROCE = PROCE_PROVE.PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND PPP.PROVE = PROCE_PROVE.PROVE AND PPP.TIPOPET IN (0,1))" & vbCrLf
sConsulta = sConsulta & "        IF @EMAIL IS NULL  --Sino el primero que sea de portal" & vbCrLf
sConsulta = sConsulta & "           SELECT TOP 1 @EMAIL = CON.EMAIL, @IDUSU_PORT=ID_PORT,@PROVEDEN=PROVE.DEN,@CONTACTO = CON.NOM + ' ' + CON.APE" & vbCrLf
sConsulta = sConsulta & "             FROM PROVE WITH (NOLOCK) INNER JOIN CON WITH (NOLOCK) ON PROVE.COD = CON.PROVE" & vbCrLf
sConsulta = sConsulta & "             WHERE PROVE.COD = @PROVEOLD AND CON.PORT = 1" & vbCrLf
sConsulta = sConsulta & "        IF @EMAIL IS NULL  --Sino el primero que tenga email" & vbCrLf
sConsulta = sConsulta & "           SELECT TOP 1 @EMAIL = CON.EMAIL, @IDUSU_PORT=ID_PORT,@PROVEDEN=PROVE.DEN,@CONTACTO = CON.NOM + ' ' + CON.APE" & vbCrLf
sConsulta = sConsulta & "             FROM PROVE WITH (NOLOCK) INNER JOIN CON WITH (NOLOCK) ON PROVE.COD = CON.PROVE" & vbCrLf
sConsulta = sConsulta & "             WHERE PROVE.COD = @PROVEOLD AND EMAIL IS NOT NULL AND CON.APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        --Obtenemos el servidor,BD y el identificador de la Cia del portal" & vbCrLf
sConsulta = sConsulta & "        SELECT @SERVER = FSP_SRV, @BD = FSP_BD , @FSP_CIA = FSP_CIA FROM PARGEN_PORT WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        --Ejecutamos el store procedure de notificaci�n que se encuentra en el portal." & vbCrLf
sConsulta = sConsulta & "          SET @CONEX=@SERVER + '.' + @BD + '.dbo.SP_ACTUALIZAR_PEND_NOTIF' " & vbCrLf
sConsulta = sConsulta & "          EXECUTE @CONEX @FSP_CIA,@ANYONEW,@GMN1NEW,@PROCENEW,@ITEMNEW,@PROVEOLD,@PROCEDEN,@ITEMDEN,@EMAIL,@PROVEDEN,@CONTACTO,@IDUSU_PORT " & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "      ----FIN DE NOTIFICACIONES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      -- AMPLIAMOS EL PLAZO PARA PUJAR" & vbCrLf
sConsulta = sConsulta & "      IF @PROVENEW!=@PROVEOLD AND @PRECIONEW<@PRECIOOLD" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "        SELECT @FECLIMITE =  P.FECLIMOFE, @MARGEN = PD.SUBASTAESPERA" & vbCrLf
sConsulta = sConsulta & "          FROM PROCE P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             INNER JOIN PROCE_DEF PD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                     ON P.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                    AND P.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                    AND P.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "         WHERE P.ANYO = @ANYONEW" & vbCrLf
sConsulta = sConsulta & "           AND P.GMN1 = @GMN1NEW" & vbCrLf
sConsulta = sConsulta & "           AND P.COD = @PROCENEW" & vbCrLf
sConsulta = sConsulta & "        SET @AHORA = GETDATE()" & vbCrLf
sConsulta = sConsulta & "        IF @AHORA > dateadd(mi,@MARGEN*-1,@FECLIMITE) AND @AHORA < @FECLIMITE" & vbCrLf
sConsulta = sConsulta & "          UPDATE PROCE" & vbCrLf
sConsulta = sConsulta & "             SET FECLIMOFE = DATEADD(mi,@MARGEN , @AHORA)" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYONEW" & vbCrLf
sConsulta = sConsulta & "             AND GMN1=@GMN1NEW" & vbCrLf
sConsulta = sConsulta & "             AND COD=@PROCENEW" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_OFEMIN_Upd INTO @ANYONEW,@GMN1NEW,@PROCENEW,@ITEMNEW,@PROVENEW,@PRECIONEW,@OFENEW,@ULTOFE,@USUOLD" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "Close curTG_OFEMIN_Upd " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_OFEMIN_Upd " & vbCrLf
sConsulta = sConsulta & "if (select count(*) from ofemin WITH (NOLOCK) where ANYO=@ANYONEW and GMN1=@GMN1NEW and PROCE=@PROCENEW and PROVE!=@PROVENEW AND PROVE IS NOT NULL)=0" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "       SET SUPERADA = case when a.prove=@PROVENEW then 0 else 1 end" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_PROVE A" & vbCrLf
sConsulta = sConsulta & "     WHERE A.ANYO= @ANYONEW " & vbCrLf
sConsulta = sConsulta & "       AND A.GMN1=@GMN1NEW " & vbCrLf
sConsulta = sConsulta & "       AND A.PROCE=@PROCENEW " & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "       SET SUPERADA = 1" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_PROVE A" & vbCrLf
sConsulta = sConsulta & "     WHERE A.ANYO= @ANYONEW" & vbCrLf
sConsulta = sConsulta & "       AND A.GMN1=@GMN1NEW " & vbCrLf
sConsulta = sConsulta & "       AND A.PROCE=@PROCENEW" & vbCrLf
sConsulta = sConsulta & "END --Columns updated" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ATRIB_LISTA_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ATRIB_LISTA_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_ATRIB_LISTA_TG_INSUPD ON dbo.PROCE_ATRIB_LISTA" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB_LISTA SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_ATRIB_LISTA PAL" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON PAL.ATRIB_ID = I.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "               AND PAL.ORDEN=I.ORDEN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ATRIB_POND_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ATRIB_POND_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_ATRIB_POND_TG_INSUPD ON dbo.PROCE_ATRIB_POND" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB_POND SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_ATRIB_POND PAP" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON PAP.ATRIB_ID = I.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "               AND PAP.ID=I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ATRIB_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ATRIB_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_ATRIB_TG_INS ON dbo.PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE  @ATRIB INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE  @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE  @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE  @AMBITO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_ATRIBS_ins CURSOR LOCAL FOR SELECT ATRIB,GRUPO,ANYO,GMN1,PROCE,ID,AMBITO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_ATRIBS_ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_ins INTO @ATRIB,@GRUPO,@ANYO,@GMN1,@PROCE,@ID,@AMBITO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        SELECT  @COUNT=COUNT(*) FROM PROCE_ATRIB WITH (NOLOCK) WHERE ATRIB=@ATRIB  AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT  @COUNT=COUNT(*) FROM PROCE_ATRIB WITH (NOLOCK) WHERE ATRIB=@ATRIB   AND ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "   IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "               RAISERROR ('Atributo existente',16,2)" & vbCrLf
sConsulta = sConsulta & "   IF @AMBITO= 1 OR @AMBITO=2" & vbCrLf
sConsulta = sConsulta & "             SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA, ATRIB)" & vbCrLf
sConsulta = sConsulta & "                         (SELECT ANYO,GMN1,PROCE,USU,VISTA,@ID FROM CONF_VISTAS_PROCE WITH (NOLOCK) WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA, ATRIB)" & vbCrLf
sConsulta = sConsulta & "                         (SELECT ANYO,GMN1,PROCE,GRUPO,USU,VISTA,@ID FROM CONF_VISTAS_GRUPO WITH (NOLOCK) WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO)" & vbCrLf
sConsulta = sConsulta & "         SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA, ATRIB)" & vbCrLf
sConsulta = sConsulta & "                         (SELECT ANYO,GMN1,PROCE,USU,VISTA,@ID FROM CONF_VISTAS_ALL WITH (NOLOCK) WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "                            AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "         SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU, VISTA,ATRIB)" & vbCrLf
sConsulta = sConsulta & "                         (SELECT ANYO,GMN1,PROCE,GRUPO,USU,VISTA,@ID FROM CONF_VISTAS_ITEM WITH (NOLOCK) WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO)" & vbCrLf
sConsulta = sConsulta & "        END    " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_ins INTO @ATRIB,@GRUPO,@ANYO,@GMN1,@PROCE,@ID,@AMBITO" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_ATRIBS_ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_ATRIBS_ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ATRIB_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ATRIB_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_ATRIB_TG_INSUPD ON dbo.PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLIC_PRECNEW INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEF_PREC INTEGER" & vbCrLf
sConsulta = sConsulta & "declare @EST AS INTEGER" & vbCrLf
sConsulta = sConsulta & "declare @SOLOORDEN AS VARBINARY(50) " & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "SET @SOLOORDEN = 0x00000002" & vbCrLf
sConsulta = sConsulta & "if COLUMNS_UPDATED()!= @SOLOORDEN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_ATRIB PA " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "               ON PA.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(APLIC_PREC) OR UPDATE(DEF_PREC)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_ATRIBS_insUPS CURSOR LOCAL FOR " & vbCrLf
sConsulta = sConsulta & "SELECT I.ID,I.ANYO,I.GMN1,I.PROCE,I.GRUPO,I.APLIC_PREC,I.DEF_PREC FROM  INSERTED I" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_ATRIBS_insUPS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_insUPS INTO @ID,@ANYO,@GMN1,@PROCE,@GRUPO,@APLIC_PRECNEW,@DEF_PREC" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "SET @EST=(SELECT EST FROM PROCE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "IF @APLIC_PRECNEW = 1" & vbCrLf
sConsulta = sConsulta & "   IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT  @COUNT = COUNT(*) FROM USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "            IF  @COUNT > 0 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @EST <=  7" & vbCrLf
sConsulta = sConsulta & "                  UPDATE USAR_GR_ATRIB SET USAR_PREC=@DEF_PREC  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                END            " & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO USAR_GR_ATRIB (ANYO,GMN1,PROCE,GRUPO,ATRIB,USAR_PREC)" & vbCrLf
sConsulta = sConsulta & "                        SELECT DISTINCT @ANYO,@GMN1,@PROCE,COD,@ID,@DEF_PREC FROM PROCE_GRUPO WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         END " & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "             SELECT  @COUNT = COUNT(*) FROM USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "             IF  @COUNT > 0 " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 IF @EST <= 7" & vbCrLf
sConsulta = sConsulta & "                  UPDATE USAR_GR_ATRIB SET USAR_PREC=@DEF_PREC  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                  INSERT INTO USAR_GR_ATRIB  VALUES (@ANYO,@GMN1,@PROCE, @GRUPO,@ID,@DEF_PREC)" & vbCrLf
sConsulta = sConsulta & "        END          " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "        DELETE USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB =@ID " & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "        DELETE USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB =@ID AND GRUPO =@GRUPO                 " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_insUPS INTO @ID,@ANYO,@GMN1,@PROCE,@GRUPO,@APLIC_PRECNEW,@DEF_PREC" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_ATRIBS_insUPS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_ATRIBS_insUPS" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_DEF_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_DEF_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_DEF_TG_INSUPD ON dbo.PROCE_DEF" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON PD.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PD.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PD.PROCE = I.PROCE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_DEF_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_DEF_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
  sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_DEF_TG_UPD ON dbo.PROCE_DEF" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEOLD TINYINT,@PROVENEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DISTOLD TINYINT,@DISTNEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESANU1OLD TINYINT,@PRESANU1NEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESANU2OLD TINYINT,@PRESANU2NEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1OLD TINYINT,@PRES1NEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2OLD TINYINT,@PRES2NEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE_ESPOLD TINYINT,@PROCE_ESPNEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO_ESPOLD TINYINT,@GRUPO_ESPNEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM_ESPOLD TINYINT,@ITEM_ESPNEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTAOLD TINYINT,@SUBASTANEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @BAJMINPUJAOLD TINYINT,@BAJMINPUJANEW  TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_DEF_Upd CURSOR LOCAL FOR " & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO,I.GMN1,I.PROCE,D.DIST,I.DIST,D.PRESANU1,I.PRESANU1,D.PRESANU2,I.PRESANU2,D.PRES1,I.PRES1,D.PRES2,I.PRES2,D.SUBASTA,I.SUBASTA,D.PROVE,I.PROVE,D.PROCE_ESP,I.PROCE_ESP,D.GRUPO_ESP,I.GRUPO_ESP,D.ITEM_ESP,I.ITEM_ESP, D.SUBASTABAJMINPUJA, I.SUBASTABAJMINPUJA " & vbCrLf
sConsulta = sConsulta & "FROM DELETED D " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "ON D.ANYO=I.ANYO AND D.GMN1=I.GMN1 AND D.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_DEF_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_DEF_Upd INTO @ANYO,@GMN1,@PROCE,@DISTOLD,@DISTNEW,@PRESANU1OLD,@PRESANU1NEW,@PRESANU2OLD,@PRESANU2NEW,@PRES1OLD,@PRES1NEW,@PRES2OLD,@PRES2NEW,@SUBASTAOLD,@SUBASTANEW,@PROVEOLD,@PROVENEW,@PROCE_ESPOLD,@PROCE_ESPNEW,@GRUPO_ESPOLD,@GRUPO_ESPNEW,@ITEM_ESPOLD,@ITEM_ESPNEW,@BAJMINPUJAOLD,@BAJMINPUJANEW" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--PROVE" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PROVEOLD,'')<>ISNULL(@PROVENEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PROVEOLD=1 AND @PROVENEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      UPDATE PROCE SET PROVEACT=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "      UPDATE ITEM SET PROVEACT=NULL WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @PROVEOLD=2 AND @PROVENEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='PROVE'" & vbCrLf
sConsulta = sConsulta & "      UPDATE PROCE_GRUPO SET PROVEACT=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      UPDATE ITEM SET PROVEACT=NULL WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--DISTRIBUICION DE UO" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@DISTOLD,'')<>ISNULL(@DISTNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @DISTOLD=1 AND @DISTNEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @DISTOLD=2 AND @DISTNEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='DIST'" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PRESUPUESTO ANUAL1" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PRESANU1OLD,'')<>ISNULL(@PRESANU1NEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PRESANU1OLD=1 AND @PRESANU1NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_PRES1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @PRESANU1OLD=2 AND @PRESANU1NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='PRESANU1'" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_PRES1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PRESUPUESTO ANUAL2" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PRESANU2OLD,'')<>ISNULL(@PRESANU2NEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PRESANU2OLD=1 AND @PRESANU2NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_PRES2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @PRESANU2OLD=2 AND @PRESANU2NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='PRESANU2'" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_PRES2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PRESUPUESTO 1" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PRES1OLD,'')<>ISNULL(@PRES1NEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PRES1OLD=1 AND @PRES1NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_PRES3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @PRES1OLD=2 AND @PRES1NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='PRES1'" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_PRES3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PRESUPUESTO 2" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PRES2OLD,'')<>ISNULL(@PRES2NEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PRES2OLD=1 AND @PRES2NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_PRES4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @PRES2OLD=2 AND @PRES2NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='PRES2'" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_PRES4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PROCE_ESP" & vbCrLf
sConsulta = sConsulta & "IF @PROCE_ESPOLD=1 AND @PROCE_ESPNEW=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE SET ESP=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM PROCE_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--GRUPO_ESP" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO_ESPOLD=1 AND @GRUPO_ESPNEW=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE_GRUPO SET ESP=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM PROCE_GRUPO_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--ITEM_ESP" & vbCrLf
sConsulta = sConsulta & "IF @ITEM_ESPOLD=1 AND @ITEM_ESPNEW=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET ESP=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--SUBASTA" & vbCrLf
sConsulta = sConsulta & "IF @SUBASTANEW!=@SUBASTAOLD" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "    IF @SUBASTANEW=1" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFEMIN (ANYO,GMN1,PROCE,ITEM,PROVE,PRECIO) " & vbCrLf
sConsulta = sConsulta & "        SELECT @ANYO,@GMN1,@PROCE, ID,NULL,NULL" & vbCrLf
sConsulta = sConsulta & "          FROM ITEM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND GMN1_PROCE = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "--BAJADA M�NIMA DE PUJAS" & vbCrLf
sConsulta = sConsulta & "IF @BAJMINPUJANEW=0 AND @BAJMINPUJAOLD=1" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE ITEM SET LIMITE=NULL WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_DEF_Upd INTO @ANYO,@GMN1,@PROCE,@DISTOLD,@DISTNEW,@PRESANU1OLD,@PRESANU1NEW,@PRESANU2OLD,@PRESANU2NEW,@PRES1OLD,@PRES1NEW,@PRES2OLD,@PRES2NEW,@SUBASTAOLD, @SUBASTANEW,@PROVEOLD,@PROVENEW,@PROCE_ESPOLD,@PROCE_ESPNEW,@GRUPO_ESPOLD,@GRUPO_ESPNEW,@ITEM_ESPOLD,@ITEM_ESPNEW,@BAJMINPUJAOLD,@BAJMINPUJANEW" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_DEF_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_DEF_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ESP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ESP_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_ESP_TG_INS ON [dbo].[PROCE_ESP] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET FECALTA=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_ESP PE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON PE.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PE.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PE.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND PE.ID = I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ESP_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ESP_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_ESP_TG_INSUPD ON dbo.PROCE_ESP" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_ESP PE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON PE.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PE.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PE.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND PE.ID = I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GR_PRES1_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GR_PRES1_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GR_PRES1_TG_DEL ON dbo.PROCE_GR_PRES1" & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATO VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GR_PRES1_Del CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,GRUPO,PRES1,PRES2,PRES3,PRES4 FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GR_PRES1_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES1_Del INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRESANU1'" & vbCrLf
sConsulta = sConsulta & "   IF @DATO IS NOT NULL --Definido el presupuesto anual 1 a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PRES1 IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "            DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                     OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "                           FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                        End" & vbCrLf
sConsulta = sConsulta & "                     Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                 DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES2=@PRES2" & vbCrLf
sConsulta = sConsulta & "                     FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                   End" & vbCrLf
sConsulta = sConsulta & "                Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "                 IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "              OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "              FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "              WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES3=@PRES3" & vbCrLf
sConsulta = sConsulta & "                    FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                 End" & vbCrLf
sConsulta = sConsulta & "              Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "              DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                      IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                         DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                         OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                         FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                         WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                        BEGIN" & vbCrLf
sConsulta = sConsulta & "                   DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES4=@PRES4" & vbCrLf
sConsulta = sConsulta & "                               FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                            End" & vbCrLf
sConsulta = sConsulta & "                         Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                         DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES1_Del INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GR_PRES1_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GR_PRES1_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GR_PRES1_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GR_PRES1_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GR_PRES1_TG_INS ON dbo.PROCE_GR_PRES1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GR_PRES1_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,GRUPO,PRES1,PRES2,PRES3,PRES4,PORCEN FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GR_PRES1_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES1_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @PRES1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESPROY (ANYO,GMN1,PROCE,ITEM,PRES1,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES1,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESPROY (ANYO,GMN1,PROCE,ITEM,PRES2,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES2,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESPROY (ANYO,GMN1,PROCE,ITEM,PRES3,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES3,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESPROY (ANYO,GMN1,PROCE,ITEM,PRES4,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES4,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES1_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GR_PRES1_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GR_PRES1_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GR_PRES2_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GR_PRES2_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GR_PRES2_TG_DEL ON dbo.PROCE_GR_PRES2" & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATO VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GR_PRES2_Del CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,GRUPO,PRES1,PRES2,PRES3,PRES4 FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GR_PRES2_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES2_Del INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRESANU2'" & vbCrLf
sConsulta = sConsulta & "       IF @DATO IS NOT NULL --Definido el presupuesto anual 2 a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @PRES1 IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "             DELETE FROM ITEM_PRESCON WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                   End" & vbCrLf
sConsulta = sConsulta & "                 Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                 DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "              IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "            DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                     OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_PRESCON WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES2=@PRES2" & vbCrLf
sConsulta = sConsulta & "                           FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                        End" & vbCrLf
sConsulta = sConsulta & "                     Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                  IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                   OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                   FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                 DELETE FROM ITEM_PRESCON WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "                         FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                      End" & vbCrLf
sConsulta = sConsulta & "                   Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                   DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                      IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "                DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                        OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                        FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                        WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                  DELETE FROM ITEM_PRESCON WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES4=@PRES4" & vbCrLf
sConsulta = sConsulta & "                              FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                           End" & vbCrLf
sConsulta = sConsulta & "                        Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                        DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES2_Del INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GR_PRES2_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GR_PRES2_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GR_PRES2_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GR_PRES2_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GR_PRES2_TG_INS ON dbo.PROCE_GR_PRES2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GR_PRES2_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,GRUPO,PRES1,PRES2,PRES3,PRES4,PORCEN FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GR_PRES2_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES2_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @PRES1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON (ANYO,GMN1,PROCE,ITEM,PRES1,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES1,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON (ANYO,GMN1,PROCE,ITEM,PRES2,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES2,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON (ANYO,GMN1,PROCE,ITEM,PRES3,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES3,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON (ANYO,GMN1,PROCE,ITEM,PRES4,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES4,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES2_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GR_PRES2_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GR_PRES2_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GR_PRES3_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GR_PRES3_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GR_PRES3_TG_DEL ON dbo.PROCE_GR_PRES3" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATO VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GR_PRES3_Del CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,GRUPO,PRES1,PRES2,PRES3,PRES4 FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GR_PRES3_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES3_Del INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRES1'" & vbCrLf
sConsulta = sConsulta & "       IF @DATO IS NOT NULL --Definido el presupuesto 1 a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @PRES1 IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "            DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                     OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "                           FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                        End" & vbCrLf
sConsulta = sConsulta & "                     Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "              IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "            DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                     OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES2=@PRES2" & vbCrLf
sConsulta = sConsulta & "                           FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                        End" & vbCrLf
sConsulta = sConsulta & "                     Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                  IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "                DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                         OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                         FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                         WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                        BEGIN" & vbCrLf
sConsulta = sConsulta & "                   DELETE FROM ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES3=@PRES3" & vbCrLf
sConsulta = sConsulta & "                                FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                             End" & vbCrLf
sConsulta = sConsulta & "                         Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                         DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                      IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "                   DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                           OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                            FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                            WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                          BEGIN" & vbCrLf
sConsulta = sConsulta & "                     DELETE FROM ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES4=@PRES4" & vbCrLf
sConsulta = sConsulta & "                                  FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                               End" & vbCrLf
sConsulta = sConsulta & "                            Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                            DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES3_Del INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GR_PRES3_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GR_PRES3_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GR_PRES3_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GR_PRES3_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
  sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GR_PRES3_TG_INS ON dbo.PROCE_GR_PRES3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GR_PRES3_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,GRUPO,PRES1,PRES2,PRES3,PRES4,PORCEN FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GR_PRES3_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES3_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @PRES1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON3 (ANYO,GMN1,PROCE,ITEM,PRES1,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES1,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON3 (ANYO,GMN1,PROCE,ITEM,PRES2,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES2,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON3 (ANYO,GMN1,PROCE,ITEM,PRES3,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES3,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND  GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON3 (ANYO,GMN1,PROCE,ITEM,PRES4,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES4,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES3_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GR_PRES3_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GR_PRES3_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GR_PRES4_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GR_PRES4_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GR_PRES4_TG_DEL ON dbo.PROCE_GR_PRES4" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATO VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GR_PRES4_Del CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,GRUPO,PRES1,PRES2,PRES3,PRES4 FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GR_PRES4_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES4_Del INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRES2'" & vbCrLf
sConsulta = sConsulta & "       IF @DATO IS NOT NULL --Definido el presupuesto 2 a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @PRES1 IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "            DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND  GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                     OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "                           FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                        End" & vbCrLf
sConsulta = sConsulta & "                     Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "              IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "            DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                     OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES2=@PRES2" & vbCrLf
sConsulta = sConsulta & "                           FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                        End" & vbCrLf
sConsulta = sConsulta & "                     Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                     DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                  IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                        OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                        FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                        WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                  DELETE FROM ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES3=@PRES3" & vbCrLf
sConsulta = sConsulta & "                              FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                           End" & vbCrLf
sConsulta = sConsulta & "                        Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                        DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                      IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "                DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                        OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                        FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                        WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                  DELETE FROM ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES4=@PRES4" & vbCrLf
sConsulta = sConsulta & "                              FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "                           End" & vbCrLf
sConsulta = sConsulta & "                        Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "                        DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES4_Del INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GR_PRES4_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GR_PRES4_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GR_PRES4_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GR_PRES4_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GR_PRES4_TG_INS ON dbo.PROCE_GR_PRES4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GR_PRES4_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,GRUPO,PRES1,PRES2,PRES3,PRES4,PORCEN FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GR_PRES4_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES4_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @PRES1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON4 (ANYO,GMN1,PROCE,ITEM,PRES1,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES1,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON4 (ANYO,GMN1,PROCE,ITEM,PRES2,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES2,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON4 (ANYO,GMN1,PROCE,ITEM,PRES3,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES3,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON4 (ANYO,GMN1,PROCE,ITEM,PRES4,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES4,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GR_PRES4_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GR_PRES4_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GR_PRES4_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GRUPO_ESP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GRUPO_ESP_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GRUPO_ESP_TG_INS ON dbo.PROCE_GRUPO_ESP" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET FECALTA=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO_ESP PGE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON PGE.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PGE.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PGE.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND PGE.GRUPO = I.GRUPO" & vbCrLf
sConsulta = sConsulta & "               AND PGE.ID = I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GRUPO_ESP_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GRUPO_ESP_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GRUPO_ESP_TG_INSUPD ON dbo.PROCE_GRUPO_ESP" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO_ESP PGE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON PGE.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PGE.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PGE.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND PGE.GRUPO = I.GRUPO" & vbCrLf
sConsulta = sConsulta & "               AND PGE.ID = I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GRUPO_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GRUPO_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GRUPO_TG_INS ON dbo.PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @BGRUPOS AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICIT INT,@SOLICIT_EST INT" & vbCrLf
sConsulta = sConsulta & "SELECT @BGRUPOS = PROCE_PROVE_GRUPOS FROM PARGEN_INTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Inserta en USAR_GR_ATRIB los atributos aplicables al precio del total de grupo que haya para el proceso" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GRUPO_Ins CURSOR  LOCAL FOR SELECT ANYO,GMN1,PROCE,COD,SOLICIT FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GRUPO_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@SOLICIT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO USAR_GR_ATRIB (ANYO,GMN1,PROCE,GRUPO,ATRIB,USAR_PREC) SELECT ANYO,GMN1,PROCE,@GRUPO,ID,DEF_PREC " & vbCrLf
sConsulta = sConsulta & "   FROM  PROCE_ATRIB WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC=1 AND GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO GRUPO_OFE (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE) SELECT ANYO,GMN1,PROCE,@GRUPO,PROVE,OFE " & vbCrLf
sConsulta = sConsulta & "    FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "    IF @BGRUPOS =1" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO PROCE_PROVE_GRUPOS (ANYO,GMN1,PROCE,PROVE,GRUPO) SELECT ANYO,GMN1,PROCE,PROVE,@GRUPO FROM PROCE_PROVE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF  @SOLICIT <> NULL " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SOLICIT_EST=(SELECT ESTADO FROM INSTANCIA WITH (NOLOCK) WHERE ID=@SOLICIT)" & vbCrLf
sConsulta = sConsulta & "   IF @SOLICIT_EST=100" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE INSTANCIA SET ESTADO=101 WHERE ID=@SOLICIT" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO INSTANCIA_EST (INSTANCIA,FECHA , EST) VALUES(@SOLICIT,GETDATE(),101)" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM curTG_PROCE_GRUPO_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@SOLICIT" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GRUPO_TG_UPD_DATVAR]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GRUPO_TG_UPD_DATVAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_GRUPO_TG_UPD_DATVAR ON dbo.PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT,@GMN1 VARCHAR(5),@PROCE SMALLINT,@GRUPO VARCHAR(20),@DATO VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTOLD VARCHAR(20),@DESTNEW VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAGOLD VARCHAR(20),@PAGNEW VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINIOLD DATETIME,@FECININEW DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECFINOLD DATETIME,@FECFINNEW DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEACTOLD VARCHAR(50), @PROVEACTNEW VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICITOLD INT ,@SOLICITNEW INT,@SOLICIT_NEW_EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GRUPO_Upd_DATVAR CURSOR LOCAL FOR " & vbCrLf
sConsulta = sConsulta & "       SELECT I.ANYO,I.GMN1,I.PROCE,I.COD,D.DEST,I.DEST,D.PAG,I.PAG,D.FECINI,I.FECINI,D.FECFIN,I.FECFIN,D.PROVEACT,I.PROVEACT ,D.SOLICIT,I.SOLICIT" & vbCrLf
sConsulta = sConsulta & "       FROM DELETED D INNER JOIN INSERTED I ON D.ANYO=I.ANYO AND D.GMN1=I.GMN1 AND D.PROCE=I.PROCE AND D.COD=I.COD" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GRUPO_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GRUPO_Upd_DATVAR INTO @ANYO,@GMN1,@PROCE,@GRUPO,@DESTOLD,@DESTNEW,@PAGOLD,@PAGNEW,@FECINIOLD,@FECININEW,@FECFINOLD,@FECFINNEW,@PROVEACTOLD,@PROVEACTNEW, @SOLICITOLD ,@SOLICITNEW " & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--DEST" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@DESTOLD,'')<>ISNULL(@DESTNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='DEST'" & vbCrLf
sConsulta = sConsulta & "         IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET DEST=@DESTNEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PAG" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PAGOLD,'')<>ISNULL(@PAGNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PAG'" & vbCrLf
sConsulta = sConsulta & "         IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET PAG=@PAGNEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--FECINI" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@FECINIOLD,'')<>ISNULL(@FECININEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='FECSUM'" & vbCrLf
sConsulta = sConsulta & "         IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET FECINI=@FECININEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--FECFIN" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@FECFINOLD,'')<>ISNULL(@FECFINNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='FECSUM'" & vbCrLf
sConsulta = sConsulta & "         IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET FECFIN=@FECFINNEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PROVEACT" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PROVEACTOLD,'')<>ISNULL(@PROVEACTNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PROVE'" & vbCrLf
sConsulta = sConsulta & "         IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET PROVEACT=@PROVEACTNEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--SOLICIT" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@SOLICITOLD,'')<>ISNULL(@SOLICITNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "           SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='SOLICIT'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET SOLICIT=@SOLICITNEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "   SET @SOLICIT_NEW_EST=(SELECT ESTADO FROM INSTANCIA WITH (NOLOCK) WHERE ID=@SOLICITNEW)" & vbCrLf
sConsulta = sConsulta & "   IF @SOLICIT_NEW_EST = 100" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE INSTANCIA SET ESTADO=101 WHERE ID=@SOLICITNEW" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO INSTANCIA_EST (INSTANCIA,FECHA , EST) VALUES(@SOLICITNEW,GETDATE(),101)" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GRUPO_Upd_DATVAR INTO @ANYO,@GMN1,@PROCE,@GRUPO,@DESTOLD,@DESTNEW,@PAGOLD,@PAGNEW,@FECINIOLD,@FECININEW,@FECFINOLD,@FECFINNEW,@PROVEACTOLD,@PROVEACTNEW, @SOLICITOLD ,@SOLICITNEW " & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GRUPO_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GRUPO_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_OFE_ADJUN_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_OFE_ADJUN_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_OFE_ADJUN_TG_DEL ON PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "declare @IDBORRADO int" & vbCrLf
sConsulta = sConsulta & "declare @vCONT int" & vbCrLf
sConsulta = sConsulta & "declare @vWEB bit" & vbCrLf
sConsulta = sConsulta & "DECLARE curPROCE_OFE_ADJUN_TG_DEL CURSOR FOR SELECT ID FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curPROCE_OFE_ADJUN_TG_DEL" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curPROCE_OFE_ADJUN_TG_DEL INTO @IDBORRADO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @vCONT=CONT,@vWEB=WEB FROM ADJUN WITH (NOLOCK) WHERE ID=@IDBORRADO" & vbCrLf
sConsulta = sConsulta & "IF @vWEB=0 and @vCONT=1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ADJUN WHERE ID=@IDBORRADO" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "UPDATE ADJUN SET CONT=CONT-1 WHERE ID=@IDBORRADO" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curPROCE_OFE_ADJUN_TG_DEL INTO @IDBORRADO" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curPROCE_OFE_ADJUN_TG_DEL" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curPROCE_OFE_ADJUN_TG_DEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_OFE_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_OFE_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_OFE_TG_DEL ON dbo.PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PORTAL AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXOFE AS INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO AS FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE2 AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @OFE2 AS SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @IDENT AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIOMONCEN FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFE AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_OFE_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE,PORTAL FROM DELETED " & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE ,@PORTAL" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & " IF (SELECT SUBASTA FROM PROCE_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE) = 1 " & vbCrLf
sConsulta = sConsulta & " BEGIN " & vbCrLf
sConsulta = sConsulta & "     --Si existen precios en la oferta eliminada que eran m�nimas en la puja se deber�n recalcular los nuevos precios m�nimos " & vbCrLf
sConsulta = sConsulta & "         /*Obtiene todos los items para los cuales ese proveedor tiene la oferta m�nima*/ " & vbCrLf
sConsulta = sConsulta & "         DECLARE C CURSOR FOR SELECT ITEM FROM OFEMIN WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         OPEN C " & vbCrLf
sConsulta = sConsulta & "         FETCH NEXT FROM C INTO @ITEM " & vbCrLf
sConsulta = sConsulta & "         WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "             SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVE2=A.PROVE,@OFE2=A.NUM" & vbCrLf
sConsulta = sConsulta & "                   FROM ITEM_OFE A WITH (NOLOCK) INNER JOIN PROCE_OFE B WITH (NOLOCK) ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE AND A.NUM=B.OFE" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN PROCE C WITH (NOLOCK) ON A.ANYO=C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE = C.COD" & vbCrLf
sConsulta = sConsulta & "             WHERE A.ANYO=@ANYO AND A.GMN1=@GMN1 AND A.PROCE=@PROCE AND A.ITEM=@ITEM AND A.ULT=1 AND A.PRECIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC" & vbCrLf
sConsulta = sConsulta & "             IF @PRECIO IS NULL " & vbCrLf
sConsulta = sConsulta & "             BEGIN " & vbCrLf
sConsulta = sConsulta & "                 UPDATE OFEMIN SET PRECIO=NULL, PROVE=NULL, OFE=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND ITEM = @ITEM " & vbCrLf
sConsulta = sConsulta & "             End " & vbCrLf
sConsulta = sConsulta & "             Else " & vbCrLf
sConsulta = sConsulta & "             BEGIN " & vbCrLf
sConsulta = sConsulta & "                 /*Actualiza la tabla OFEMIN con la oferta m�nima para el proceso e item*/ " & vbCrLf
sConsulta = sConsulta & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2, ULTOFE=0 WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "             End " & vbCrLf
sConsulta = sConsulta & "             FETCH NEXT FROM C INTO @ITEM " & vbCrLf
sConsulta = sConsulta & "         End " & vbCrLf
sConsulta = sConsulta & "         Close C " & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "     EXEC SP_CALCULAR_DETALLE_PUJAS @ANYO, @GMN1, @PROCE" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "     SET @NUMOFE=(SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ENVIADA=1)" & vbCrLf
sConsulta = sConsulta & "     IF (@NUMOFE IS NULL ) " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "             UPDATE PROCE SET O=(O-1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "     IF @PORTAL=1 --Si la oferta eliminada era de portal puede que tenga que actualizar el campo W de PROCE" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "          SET @NUMOFE=NULL" & vbCrLf
sConsulta = sConsulta & "          SET @NUMOFE=(SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ENVIADA=1 AND PORTAL=1)" & vbCrLf
sConsulta = sConsulta & "          IF (@NUMOFE IS NULL )  " & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE SET W=(W-1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ENVIADA=1 AND OFE <> -1 ) " & vbCrLf
sConsulta = sConsulta & "     IF (@MAXOFE IS NULL ) " & vbCrLf
sConsulta = sConsulta & "     BEGIN " & vbCrLf
sConsulta = sConsulta & "     /* Si se han insertado ofertas, actualizamos el estado del proceso  si es que este era inferior a 7 */ " & vbCrLf
sConsulta = sConsulta & "         IF (SELECT PROCE.EST FROM PROCE WITH (NOLOCK) WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) = 7  /* 7= Con ofertas */ " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM PROCE_PROVE_PET WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) = 0" & vbCrLf
sConsulta = sConsulta & "               UPDATE PROCE SET EST = 5  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "               UPDATE PROCE SET EST = 6  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "         End " & vbCrLf
sConsulta = sConsulta & "     End " & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE ,@PORTAL" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_OFE_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_OFE_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_OFE_TG_INS ON dbo.PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXOFE AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ULT AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST int" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECLIMITE datetime" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA tinyint" & vbCrLf
sConsulta = sConsulta & "DECLARE @MARGEN int" & vbCrLf
sConsulta = sConsulta & "DECLARE @AHORA datetime" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_OFE_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,PROVE,OFE,ULT,MON,CAMBIO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_OFE_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_OFE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE,@OFE,@ULT,@MON,@CAMBIO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_NUEVA_OFERTA @ANYO = @ANYO, @GMN1=@GMN1, @PROCE = @PROCE, @PROVE = @PROVE, @OFE=@OFE,@ULT=@ULT,@MONNEW=@MON,@CAMBIONEW=@CAMBIO" & vbCrLf
sConsulta = sConsulta & "    /* Actualizamos el campo OFE de PROCE_PROVE que indica si el proveedor ha presentado ofertas para ese proceso*/" & vbCrLf
sConsulta = sConsulta & "    IF @OFE>0 " & vbCrLf
sConsulta = sConsulta & "      UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "    /* Si se han insertado ofertas, actualizamos el estado del proceso  si es que este era inferior a 7 */" & vbCrLf
sConsulta = sConsulta & "    SELECT @PROCEEST = P.EST, @FECLIMITE= P.FECLIMOFE,@SUBASTA = PD.SUBASTA, @MARGEN = PD.SUBASTAESPERA  " & vbCrLf
sConsulta = sConsulta & "      FROM PROCE P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN PROCE_DEF PD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                      ON P.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                     AND P.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                     AND P.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "     WHERE P.ANYO=@ANYO  " & vbCrLf
sConsulta = sConsulta & "       AND P.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "       AND P.COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    IF @PROCEEST < 7  /* 7= Con ofertas */" & vbCrLf
sConsulta = sConsulta & "        UPDATE PROCE SET EST = 7  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM curTG_PROCE_OFE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE,@OFE,@ULT,@MON,@CAMBIO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_PROCE_OFE_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_OFE_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PRES1_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PRES1_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PRES1_TG_INS ON dbo.PROCE_PRES1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PRES1_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PRES1_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PRES1_Ins INTO @ANYO,@GMN1,@PROCE,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @PRES1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESPROY (ANYO,GMN1,PROCE,ITEM,PRES1,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES1,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                   IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO ITEM_PRESPROY (ANYO,GMN1,PROCE,ITEM,PRES2,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES2,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                         IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                               INSERT INTO ITEM_PRESPROY (ANYO,GMN1,PROCE,ITEM,PRES3,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES3,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                     INSERT INTO ITEM_PRESPROY (ANYO,GMN1,PROCE,ITEM,PRES4,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES4,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PRES1_Ins INTO @ANYO,@GMN1,@PROCE,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PRES1_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PRES1_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PRES2_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PRES2_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PRES2_TG_INS ON dbo.PROCE_PRES2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PRES2_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PRES2_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PRES2_Ins INTO @ANYO,@GMN1,@PROCE,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @PRES1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON (ANYO,GMN1,PROCE,ITEM,PRES1,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES1,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                   IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO ITEM_PRESCON (ANYO,GMN1,PROCE,ITEM,PRES2,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES2,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                         IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                               INSERT INTO ITEM_PRESCON (ANYO,GMN1,PROCE,ITEM,PRES3,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES3,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                     INSERT INTO ITEM_PRESCON (ANYO,GMN1,PROCE,ITEM,PRES4,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES4,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PRES2_Ins INTO @ANYO,@GMN1,@PROCE,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PRES2_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PRES2_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
 
 sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PRES3_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PRES3_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PRES3_TG_INS ON dbo.PROCE_PRES3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PRES3_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PRES3_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PRES3_Ins INTO @ANYO,@GMN1,@PROCE,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @PRES1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON3 (ANYO,GMN1,PROCE,ITEM,PRES1,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES1,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                   IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO ITEM_PRESCON3 (ANYO,GMN1,PROCE,ITEM,PRES2,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES2,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                         IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                               INSERT INTO ITEM_PRESCON3 (ANYO,GMN1,PROCE,ITEM,PRES3,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES3,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                     INSERT INTO ITEM_PRESCON3 (ANYO,GMN1,PROCE,ITEM,PRES4,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES4,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PRES3_Ins INTO @ANYO,@GMN1,@PROCE,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PRES3_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PRES3_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PRES4_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PRES4_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PRES4_TG_INS ON dbo.PROCE_PRES4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PRES4_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PRES4_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PRES4_Ins INTO @ANYO,@GMN1,@PROCE,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @PRES1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_PRESCON4 (ANYO,GMN1,PROCE,ITEM,PRES1,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES1,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                   IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO ITEM_PRESCON4 (ANYO,GMN1,PROCE,ITEM,PRES2,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES2,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                         IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                               INSERT INTO ITEM_PRESCON4 (ANYO,GMN1,PROCE,ITEM,PRES3,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES3,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                     INSERT INTO ITEM_PRESCON4 (ANYO,GMN1,PROCE,ITEM,PRES4,PORCEN) SELECT @ANYO,@GMN1,@PROCE,ID,@PRES4,@PORCEN FROM ITEM WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PRES4_Ins INTO @ANYO,@GMN1,@PROCE,@PRES1,@PRES2,@PRES3,@PRES4,@PORCEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PRES4_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PRES4_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_GRUPOS_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PROVE_GRUPOS_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PROVE_GRUPOS_TG_INSUPD ON dbo.PROCE_PROVE_GRUPOS" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_GRUPOS SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_PROVE_GRUPOS PPG" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON PPG.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PPG.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PPG.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND PPG.PROVE = I.PROVE" & vbCrLf
sConsulta = sConsulta & "          AND PPG.GRUPO = I.GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PROVE_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PROVE_TG_DEL ON dbo.PROCE_PROVE" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PROVE_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PROVE_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "/*Borramos los registros de ese proveedor para las tablas de configuraci�n de la comparativa */" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CONF_VISTAS_PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CONF_VISTAS_GRUPO_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CONF_VISTAS_ITEM_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CONF_VISTAS_ALL_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET P=(P-1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* Si se han insertado proveedores, actualizamos el estado del proceso  si es que este era inferior a */" & vbCrLf
sConsulta = sConsulta & "IF (SELECT EST FROM PROCE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) =4 /* 4=Con proveedores asignados */" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM PROCE_PROVE WITH (NOLOCK) WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST = 3  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PROVE_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PROVE_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PROVE_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PROVE_TG_INS ON dbo.PROCE_PROVE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPOS AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @GRUPOS=PROCE_PROVE_GRUPOS FROM PARGEN_INTERNO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PROVE_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Inserta los registros correspondientes en las tablas de prove-grupos*/" & vbCrLf
sConsulta = sConsulta & "IF @GRUPOS=1 " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PROCE_PROVE_GRUPOS (ANYO, GMN1, PROCE, PROVE, GRUPO) SELECT ANYO,GMN1,PROCE,@PROVE,COD FROM PROCE_GRUPO WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*Inserta los registros correspondientes en las tablas de configuraci�n de la comparativa*/" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_PROCE_PROVE (ANYO,GMN1,PROCE,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_PROCE WITH (NOLOCK) WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_ALL_PROVE (ANYO,GMN1,PROCE,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_ALL WITH (NOLOCK) WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_GRUPO_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,GRUPO,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_GRUPO WITH (NOLOCK) WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_ITEM_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,GRUPO,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_ITEM WITH (NOLOCK) WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--UPDATE PROCE SET P= case when P is null then 1 else (P+1) end  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET P= (SELECT count(*)   FROM PROCE_PROVE WITH (NOLOCK) WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE)  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* Si se han insertado proveedores, actualizamos el estado del proceso  si es que este era inferior a */" & vbCrLf
sConsulta = sConsulta & "IF (SELECT PROCE.EST FROM PROCE WITH (NOLOCK) WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) < 4  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST = 4  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_TG_INSUPD ON dbo.PROCE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@COD SMALLINT,@SOLICIT INT,@SOLICIT_EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_Ins CURSOR FOR SELECT ANYO,GMN1,COD,SOLICIT FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Ins INTO @ANYO,@GMN1,@COD,@SOLICIT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "IF  @SOLICIT <> NULL " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SOLICIT_EST=(SELECT ESTADO FROM INSTANCIA WITH (NOLOCK) WHERE ID=@SOLICIT)" & vbCrLf
sConsulta = sConsulta & "   IF @SOLICIT_EST=100" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE INSTANCIA SET ESTADO=101 WHERE ID=@SOLICIT" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO INSTANCIA_EST (INSTANCIA,FECHA , EST) VALUES(@SOLICIT,GETDATE(),101)" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Ins INTO @ANYO,@GMN1,@COD,@SOLICIT" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if update(USUAPER) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE I.USUAPER IS NOT NULL AND NOT EXISTS (SELECT * FROM (SELECT COD FROM USU WITH (NOLOCK) UNION SELECT USU FROM ADM WITH (NOLOCK)) U WHERE I.USUAPER = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de apertura no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "if update(USUVALSELPROVE) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE I.USUVALSELPROVE IS NOT NULL AND NOT EXISTS (SELECT * FROM (SELECT COD FROM USU WITH (NOLOCK) UNION SELECT USU FROM ADM WITH (NOLOCK)) U WHERE I.USUVALSELPROVE = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de validaci�n de selecci�n de proveedores no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if update(USUVALCIERRE) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE I.USUVALCIERRE IS NOT NULL AND NOT EXISTS (SELECT * FROM (SELECT COD FROM USU WITH (NOLOCK) UNION SELECT USU FROM ADM WITH (NOLOCK)) U WHERE I.USUVALCIERRE = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de cierre de proceso no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "if update(USUVAL) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE  I.USUVAL IS NOT NULL AND NOT EXISTS (SELECT * FROM (SELECT COD FROM USU WITH (NOLOCK) UNION SELECT USU FROM ADM WITH (NOLOCK)) U WHERE I.USUVAL = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de validaci�n de apertura de proceso no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_TG_UPD ON dbo.PROCE" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@COD SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CALCPEND TINYINT, @CALCPEND_ANT TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICIT_OLD INT, @SOLICIT_NEW INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICIT_EST_NEW INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COM_OLD VARCHAR(50),@COM_NEW VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PVISTAS_OLD TINYINT,@PVISTAS_NEW TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_Upd CURSOR LOCAL FOR SELECT I.ANYO,I.GMN1,I.COD,I.CALCPEND,I.SOLICIT,D.SOLICIT, D.CALCPEND,I.COM,I.PLANTILLA_VISTAS,D.COM,D.PLANTILLA_VISTAS FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DELETED D ON I.ANYO=D.ANYO AND I.GMN1=D.GMN1 AND I.COD=D.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd INTO @ANYO,@GMN1,@COD,@CALCPEND,@SOLICIT_NEW, @SOLICIT_OLD, @CALCPEND_ANT, @COM_NEW,@PVISTAS_NEW, @COM_OLD, @PVISTAS_OLD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF UPDATE(CALCPEND)" & vbCrLf
sConsulta = sConsulta & "   IF @CALCPEND = 0 AND @CALCPEND_ANT=1" & vbCrLf
sConsulta = sConsulta & "                  UPDATE PROCE SET CALCFEC=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF ISNULL(@SOLICIT_OLD,0) <> ISNULL(@SOLICIT_NEW,0) " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SOLICIT_EST_NEW=(SELECT ESTADO FROM INSTANCIA WITH (NOLOCK) WHERE ID=@SOLICIT_NEW)" & vbCrLf
sConsulta = sConsulta & "       IF @SOLICIT_EST_NEW = 100" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE INSTANCIA SET ESTADO=101 WHERE ID=@SOLICIT_NEW" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO INSTANCIA_EST (INSTANCIA,FECHA , EST) VALUES(@SOLICIT_NEW,GETDATE(),101)" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   --Si cambia el responsable o se desvinculan las vistas de plantilla eliminamos de vista por defecto a los usuarios que tuvieran las vistas de responsable o de plantilla " & vbCrLf
sConsulta = sConsulta & "  -- como vista por defecto" & vbCrLf
sConsulta = sConsulta & "   IF UPDATE(COM)" & vbCrLf
sConsulta = sConsulta & "             IF (@COM_OLD<>@COM_NEW) OR (@COM_NEW IS NULL AND @COM_OLD IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "                     DELETE FROM CONF_VISTAS_DEFECTO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@COD AND TIPO_VISTA=1" & vbCrLf
sConsulta = sConsulta & "   IF UPDATE(PLANTILLA_VISTAS)" & vbCrLf
sConsulta = sConsulta & "             IF @PVISTAS_OLD=1 AND @PVISTAS_NEW=0 " & vbCrLf
sConsulta = sConsulta & "                     DELETE FROM CONF_VISTAS_DEFECTO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@COD AND TIPO_VISTA=2" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd INTO  @ANYO,@GMN1,@COD,@CALCPEND,@SOLICIT_NEW, @SOLICIT_OLD, @CALCPEND_ANT, @COM_NEW,@PVISTAS_NEW, @COM_OLD, @PVISTAS_OLD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_TG_UPD_DATVAR]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_TG_UPD_DATVAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_TG_UPD_DATVAR ON dbo.PROCE" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT,@GMN1 VARCHAR(5),@PROCE SMALLINT,@DATO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTOLD VARCHAR(20),@DESTNEW VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAGOLD VARCHAR(20),@PAGNEW VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINIOLD DATETIME,@FECININEW DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECFINOLD DATETIME,@FECFINNEW DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEACTOLD VARCHAR(50)DECLARE @PROVEACTNEW VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICITOLD INT,@SOLICITNEW INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_Upd_DATVAR CURSOR LOCAL FOR " & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO,I.GMN1,I.COD,D.DEST,I.DEST,D.PAG,I.PAG,D.FECINI,I.FECINI,D.FECFIN,I.FECFIN,D.PROVEACT,I.PROVEACT,D.SOLICIT,I.SOLICIT" & vbCrLf
sConsulta = sConsulta & "FROM DELETED D " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "ON D.ANYO=I.ANYO AND D.GMN1=I.GMN1 AND D.COD=I.COD" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd_DATVAR INTO @ANYO,@GMN1,@PROCE, @DESTOLD,@DESTNEW,@PAGOLD,@PAGNEW,@FECINIOLD,@FECININEW,@FECFINOLD,@FECFINNEW,@PROVEACTOLD,@PROVEACTNEW,@SOLICITOLD,@SOLICITNEW" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--DEST" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@DESTOLD,'')<>ISNULL(@DESTNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DEST FROM PROCE_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET DEST=@DESTNEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PAG" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PAGOLD,'')<>ISNULL(@PAGNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=PAG FROM PROCE_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET PAG=@PAGNEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--FECINI" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@FECINIOLD,'')<>ISNULL(@FECININEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=FECSUM FROM PROCE_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET FECINI=@FECININEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--FECFIN" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@FECFINOLD,'')<>ISNULL(@FECFINNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=FECSUM FROM PROCE_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET FECFIN=@FECFINNEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PROVEACT" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PROVEACTOLD,'')<>ISNULL(@PROVEACTNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=PROVE FROM PROCE_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET PROVEACT=@PROVEACTNEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--SOLICIT" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@SOLICITOLD,'')<>ISNULL(@SOLICITNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=SOLICIT FROM PROCE_DEF WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET SOLICIT=@SOLICITNEW WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd_DATVAR INTO @ANYO,@GMN1,@PROCE, @DESTOLD,@DESTNEW,@PAGOLD,@PAGNEW,@FECINIOLD,@FECININEW,@FECFINOLD,@FECFINNEW,@PROVEACTOLD,@PROVEACTNEW,@SOLICITOLD,@SOLICITNEW" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_UON1_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_UON1_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_UON1_TG_INS ON dbo.PROCE_UON1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@UON1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT,@PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_UON1_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,UON1,PORCEN FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_UON1_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_UON1_Ins INTO @ANYO,@GMN1,@PROCE,@UON1,@PORCEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE  ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "          WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO ITEM_UON1 (ANYO,GMN1,PROCE,ITEM,UON1,PORCEN) VALUES (@ANYO,@GMN1,@PROCE,@ITEM,@UON1,@PORCEN)" & vbCrLf
sConsulta = sConsulta & "                    FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "               End" & vbCrLf
sConsulta = sConsulta & "   Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_UON1_Ins INTO @ANYO,@GMN1,@PROCE,@UON1,@PORCEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_UON1_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_UON1_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_UON2_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_UON2_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_UON2_TG_INS ON dbo.PROCE_UON2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@UON1 VARCHAR(50),@UON2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT,@PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_UON2_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,UON1,UON2,PORCEN FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_UON2_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_UON2_Ins INTO @ANYO,@GMN1,@PROCE,@UON1,@UON2,@PORCEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WITH (NOLOCK) WHERE  ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   OPEN curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "         WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                            INSERT INTO ITEM_UON2 (ANYO,GMN1,PROCE,ITEM,UON1,UON2,PORCEN) VALUES (@ANYO,@GMN1,@PROCE,@ITEM,@UON1,@UON2,@PORCEN)" & vbCrLf
sConsulta = sConsulta & "                            FETCH NEXT FROM curTG_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "               End" & vbCrLf
sConsulta = sConsulta & "         Close curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE curTG_ITEM" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_UON2_Ins INTO @ANYO,@GMN1,@PROCE,@UON1,@UON2,@PORCEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_UON2_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_UON2_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion31600_06_0144A_06_0145() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualiza triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
frmProgreso.ProgressBar1.Refresh
V_31600_Triggers_45

sConsulta = "UPDATE VERSION SET NUM ='3.00.01.45'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31600.7'"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion31600_06_0144A_06_0145 = True
Exit Function

Error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31600_06_0144A_06_0145 = False
End Function


Private Sub V_31600_Triggers_45()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_EST_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_EST_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER ORDEN_EST_TG_INS ON dbo.ORDEN_EST " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_EST_Ins CURSOR LOCAL FOR SELECT EST,ID,ORDEN,PER,COMENT,EXTERNO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXTERNO AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVLOG AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORIGEN AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMENT AS VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_ORDEN_ENTREGA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAG AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTEGRACION AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACCION AS VARCHAR(2)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_PED_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOGRABAR AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADO_APROV_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_ERP AS VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDDIRENVMAIL  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @EXTERNO IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF (@EST = 2 OR @EST = 3 OR @EST = 4 OR @EST = 20 OR @EST = 21) " & vbCrLf
sConsulta = sConsulta & "      BEGIN -- Si es uno de los estados que nos interesan para grabar en LOG_ORDEN_ENTREGA (emitido,aceptado,rechazado,anulado,en camino)" & vbCrLf
sConsulta = sConsulta & "        -- Recuperamos el tipo de orden de entrega para ver si est� activada su integraci�n" & vbCrLf
sConsulta = sConsulta & "        SET @COD_ERP=NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT  @TIPO=TIPO,@PROVE=PROVE, @COD_ERP=COD_PROVE_ERP FROM ORDEN_ENTREGA WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "        SELECT  @ACTIVLOG = ACTIVLOG FROM PARGEN_GEST WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "        SELECT  @INTEGRACION =INTEGRACION, @TRASLADO_APROV_ERP=TRASLADO_APROV_ERP, @TRASPASO_PED_ERP= TRASPASO_PED_ERP, @PEDDIRENVMAIL =PED_DIR_ENVIO_EMAIL FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @INTEGRACION = 0" & vbCrLf
sConsulta = sConsulta & "             SET @ACTIVA = 0" & vbCrLf
sConsulta = sConsulta & "        ELSE " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO = 0 --Pedidos directos" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 13" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 11" & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF (@ACTIVA = 1 AND (@SENTIDO=3 OR @SENTIDO=1)) OR @ACTIVLOG = 1 OR @TRASPASO_PED_ERP=1 OR @TRASLADO_APROV_ERP = 1" & vbCrLf
sConsulta = sConsulta & "          -- si el sentido es = 2  (solo entrada) o no est� activa la integraci�n  no grabamos en log" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SET @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "            IF (@TIPO=0 AND @TRASPASO_PED_ERP=1)  OR (@TIPO = 1 AND @TRASLADO_APROV_ERP = 1) --emisi�n de pedidos directos y de aprovisionamiento" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                 IF @TIPO = 0  and @TRASPASO_PED_ERP=1" & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                    SET @ERP=1" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVLOG = 1 -- log de actividad" & vbCrLf
sConsulta = sConsulta & "                         SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                         SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                 IF @TIPO = 1  and @TRASLADO_APROV_ERP=1" & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                     SET @ERP=1" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVLOG = 1 -- log de actividad" & vbCrLf
sConsulta = sConsulta & "                         SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                         SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                begin" & vbCrLf
sConsulta = sConsulta & "                SET @ERP=0" & vbCrLf
sConsulta = sConsulta & "                IF @ACTIVLOG = 1 " & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVA  = 1" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 2 -- Integracion y log de actividad" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 1 -- solo log de actividad" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                     BEGIN" & vbCrLf
sConsulta = sConsulta & "                      IF @ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "                          SET @ORIGEN = 0 -- solo integracion" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                          SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                end" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                SET @USU = NULL" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                SET @USU = (SELECT COD FROM USU WHERE BAJA=0 AND PER = @PER)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            -- la acci�n ser� E - Cambio de estado o I Creaci�n orden de entrega)" & vbCrLf
sConsulta = sConsulta & "            IF @PEDDIRENVMAIL=0 and @TIPO=0 --El estado cuando se emite es 3 en pedido directo" & vbCrLf
sConsulta = sConsulta & "                 IF @EST = 3 " & vbCrLf
sConsulta = sConsulta & "                   SET @ACCION='I'" & vbCrLf
sConsulta = sConsulta & "                 ELSE " & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                     IF @EST=20 " & vbCrLf
sConsulta = sConsulta & "                       SET @ACCION='D'" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                       SET @ACCION='E'" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "            else" & vbCrLf
sConsulta = sConsulta & "                 IF @EST = 2  " & vbCrLf
sConsulta = sConsulta & "                   SET @ACCION='I'" & vbCrLf
sConsulta = sConsulta & "                 ELSE " & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                     IF @EST=20 " & vbCrLf
sConsulta = sConsulta & "                       SET @ACCION='D'" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                       SET @ACCION='E'" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA,OBS,ORGCOMPRAS,ERP)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,ORDEN_ENTREGA.PAG,@EST,INCORRECTA,@COMENT,@ORIGEN,@PER,@USU, ORDEN_ENTREGA.MON,ORDEN_ENTREGA.CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,ORDEN_ENTREGA.NUM_PED_ERP,ORDEN_ENTREGA.OBS,ORDEN_ENTREGA.ORGCOMPRAS," & vbCrLf
sConsulta = sConsulta & "                        ERP = CASE WHEN ERP_ORGCOMPRAS.ERP IS NULL THEN ERP_SOCIEDAD.ERP ELSE ERP_ORGCOMPRAS.ERP END" & vbCrLf
sConsulta = sConsulta & "                        FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO " & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN EMP ON EMP.ID = ORDEN_ENTREGA.EMPRESA" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN ERP_SOCIEDAD ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN ERP_ORGCOMPRAS ON ERP_ORGCOMPRAS.ORGCOMPRAS = EMP.ORG_COMPRAS" & vbCrLf
sConsulta = sConsulta & "           WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN,CENTROS, ALMACEN,PEDIDOABIERTO)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN,CENTROS, ALMACEN,PEDIDOABIERTO FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "      IF @ACCION='I' AND @ORIGEN<>1 AND @TIPO = 0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "                    -- Presupuestos" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES1 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES1 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID  INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES2 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES2 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID  INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES3 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES3 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID   INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES4 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES4 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID   INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN<>1 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "          -- Atributos" & vbCrLf
sConsulta = sConsulta & "          INSERT INTO LOG_ORDEN_ENTREGA_ATRIB ( ID_LOG_ORDEN_ENTREGA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "                        SELECT @ID_LOG_ORDEN_ENTREGA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL FROM ORDEN_ENTREGA_ATRIB WHERE ORDEN=@ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "          INSERT INTO LOG_LINEAS_PEDIDO_ATRIB ( ID_LOG_LINEA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "                        SELECT LLP.ID,A.ATRIB,A.VALOR_NUM,A.VALOR_TEXT,A.VALOR_FEC,A.VALOR_BOOL FROM LINEAS_PEDIDO_ATRIB  A" & vbCrLf
sConsulta = sConsulta & "                                INNER JOIN LOG_LINEAS_PEDIDO LLP ON LLP.ID_LINEAS_PEDIDO  =A.LINEA WHERE LLP.ID_LOG_ORDEN_ENTREGA=@ID_LOG_ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            IF @ERP=1 " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "              INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA,OBS,ORGCOMPRAS,ERP)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,ORDEN_ENTREGA.PAG,@EST,INCORRECTA,@COMENT,0,@PER,@USU,ORDEN_ENTREGA.MON,ORDEN_ENTREGA.CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,ORDEN_ENTREGA.NUM_PED_ERP,ORDEN_ENTREGA.OBS,ORDEN_ENTREGA.ORGCOMPRAS," & vbCrLf
sConsulta = sConsulta & "                        ERP = CASE WHEN ERP_ORGCOMPRAS.ERP IS NULL THEN ERP_SOCIEDAD.ERP ELSE ERP_ORGCOMPRAS.ERP END" & vbCrLf
sConsulta = sConsulta & "                        FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO " & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN EMP ON EMP.ID = ORDEN_ENTREGA.EMPRESA" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN ERP_SOCIEDAD ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN ERP_ORGCOMPRAS ON ERP_ORGCOMPRAS.ORGCOMPRAS = EMP.ORG_COMPRAS" & vbCrLf
sConsulta = sConsulta & "           WHERE ORDEN_ENTREGA.ID = @ID_ORDEN                    " & vbCrLf
sConsulta = sConsulta & "           SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN,CENTROS,ALMACEN,PEDIDOABIERTO)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN,CENTROS,ALMACEN,PEDIDOABIERTO FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    -- Presupuestos" & vbCrLf
sConsulta = sConsulta & "                   IF @ACCION='I'  AND @TIPO = 0" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES1 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES1 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID   INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES2 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES2 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES3 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES3 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID  AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_PEDIDO_PRES4 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ID_LOG_ORDEN_ENTREGA,LLP.ID,LP1.LINEA,LP1.PRES1,LP1.PRES2,LP1.PRES3,LP1.PRES4,LP1.PORCEN FROM LINEAS_PEDIDO_PRES4 LP1  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LOG_LINEAS_PEDIDO LLP ON LP1.LINEA=LLP.ID_LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN LINEAS_PEDIDO LP ON LP1.LINEA=LP.ID INNER JOIN LOG_ORDEN_ENTREGA LOE ON LLP.ID_LOG_ORDEN_ENTREGA=LOE.ID AND LOE.ORIGEN=0 WHERE  LP.ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "          -- Atributos" & vbCrLf
sConsulta = sConsulta & "          INSERT INTO LOG_ORDEN_ENTREGA_ATRIB ( ID_LOG_ORDEN_ENTREGA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "                        SELECT @ID_LOG_ORDEN_ENTREGA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL FROM ORDEN_ENTREGA_ATRIB WHERE ORDEN=@ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "          INSERT INTO LOG_LINEAS_PEDIDO_ATRIB ( ID_LOG_LINEA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "                        SELECT LLP.ID,A.ATRIB,A.VALOR_NUM,A.VALOR_TEXT,A.VALOR_FEC,A.VALOR_BOOL FROM LINEAS_PEDIDO_ATRIB  A" & vbCrLf
sConsulta = sConsulta & "                                INNER JOIN LOG_LINEAS_PEDIDO LLP ON LLP.ID_LINEAS_PEDIDO  =A.LINEA WHERE LLP.ID_LOG_ORDEN_ENTREGA=@ID_LOG_ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "          END --IF ACTIVA" & vbCrLf
sConsulta = sConsulta & "      END -- IF EST" & vbCrLf
sConsulta = sConsulta & "   END -- IF @EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  UPDATE ORDEN_ENTREGA SET ORDEN_EST = @ID WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_EST_Ins" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CONF_CUMPLIMENTACION_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CONF_CUMPLIMENTACION_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""

sConsulta = sConsulta & "CREATE TRIGGER CONF_CUMPLIMENTACION_TG_INS ON dbo.CONF_CUMPLIMENTACION " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_CUMPLIMENTACION SET OBLIGATORIO=1 FROM CONF_CUMPLIMENTACION CC INNER JOIN INSERTED I ON CC.ID = I.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON I.CAMPO=FC.ID WHERE FC.TIPO_CAMPO_GS=31 OR FC.TIPO_CAMPO_GS=33 OR (I.SOLIC_PROVE=2 AND (FC.TIPO_CAMPO_GS=22 OR FC.TIPO_CAMPO_GS=23 ))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_CUMPLIMENTACION SET VISIBLE=1,ESCRITURA=0 FROM CONF_CUMPLIMENTACION CC INNER JOIN INSERTED I ON CC.ID = I.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON I.CAMPO=FC.ID WHERE FC.TIPO_CAMPO_GS=34 AND I.SOLIC_PROVE=1 AND I.ES_ESTADO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SOLICITUD_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[SOLICITUD_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""

sConsulta = sConsulta & "CREATE TRIGGER SOLICITUD_TG_INS ON dbo.SOLICITUD " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* IF VERSION >= 31600  THEN*/" & vbCrLf
sConsulta = sConsulta & "DECLARE @WORKFLOW INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FORM INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_SOLICI_Ins CURSOR LOCAL FOR SELECT DISTINCT I.WORKFLOW,I.FORMULARIO FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_SOLICI_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_SOLICI_Ins INTO @WORKFLOW,@FORM" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if not exists " & vbCrLf
sConsulta = sConsulta & "(   SELECT 1" & vbCrLf
sConsulta = sConsulta & "    FROM PM_ROL_BLOQUE RB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PM_BLOQUE B WITH (NOLOCK) ON RB.BLOQUE = B.ID AND B.WORKFLOW = @WORKFLOW" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PM_ROL R WITH (NOLOCK) ON RB.ROL = R.ID AND R.WORKFLOW = @WORKFLOW" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.FORMULARIO = @FORM" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PM_CONF_CUMP_BLOQUE CCB WITH (NOLOCK) ON CCB.BLOQUE = RB.BLOQUE AND CCB.ROL = RB.ROL AND CCB.CAMPO = FC.ID )" & vbCrLf
sConsulta = sConsulta & "            --Insertar las cumplimentaciones por si venimos de copiar un workflow" & vbCrLf
sConsulta = sConsulta & "            INSERT INTO PM_CONF_CUMP_BLOQUE (BLOQUE,ROL,CAMPO,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN)" & vbCrLf
sConsulta = sConsulta & "            SELECT RB.BLOQUE, RB.ROL, FC.ID, 1 AS VISIBLE, CASE WHEN FC.TIPO=3 THEN 0 ELSE 1 END AS ESCRITURA, 0 AS OBLIGATORIO,FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "            FROM PM_ROL_BLOQUE RB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN PM_BLOQUE B WITH (NOLOCK) ON RB.BLOQUE = B.ID AND B.WORKFLOW = @WORKFLOW" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN PM_ROL R WITH (NOLOCK) ON RB.ROL = R.ID AND R.WORKFLOW = @WORKFLOW" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.FORMULARIO = @FORM" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "            ORDER BY FG.ORDEN,FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM curTG_SOLICI_Ins INTO @WORKFLOW,@FORM" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "/* END IF */" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SOLICITUD_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[SOLICITUD_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""

sConsulta = sConsulta & "CREATE TRIGGER SOLICITUD_TG_UPD ON dbo.SOLICITUD" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @WORKFLOW INT,@FORM INT,@SOLIC INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @WORKFLOW_NEW INT, @FORM_NEW INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MODIF AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOSOL AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* IF VERSION >= 31600 */" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENLACE AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACCION AS INT" & vbCrLf
sConsulta = sConsulta & "/*END IF*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_SOLICI_Upd CURSOR LOCAL FOR SELECT DISTINCT I.ID,I.WORKFLOW,I.FORMULARIO,D.WORKFLOW,D.FORMULARIO,I.TIPO FROM INSERTED I  INNER JOIN DELETED D ON I.ID=D.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_SOLICI_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_SOLICI_Upd INTO @SOLIC,@WORKFLOW_NEW,@FORM_NEW,@WORKFLOW,@FORM,@TIPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @MODIF=0" & vbCrLf
sConsulta = sConsulta & "   IF UPDATE(WORKFLOW) " & vbCrLf
sConsulta = sConsulta & "     begin" & vbCrLf
sConsulta = sConsulta & "         IF ISNULL(@WORKFLOW_NEW,0)<>ISNULL(@WORKFLOW,0)" & vbCrLf
sConsulta = sConsulta & "                 SET @MODIF=1" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF UPDATE (FORMULARIO)" & vbCrLf
sConsulta = sConsulta & "     begin" & vbCrLf
sConsulta = sConsulta & "         if @FORM_NEW<>@FORM " & vbCrLf
sConsulta = sConsulta & "                begin" & vbCrLf
sConsulta = sConsulta & "                SET @MODIF=2" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "                    --Elimina las ponderaciones de variables que tengan esa solicitud como origen de datos si ha cambiado el formulario (solo vbles de tipo certificado, por ahora)" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM VAR_CAL_PCERT_LISTA FROM VAR_CAL_PCERT_LISTA VPL WITH (NOLOCK) INNER JOIN VAR_CAL_PCERT VP WITH (NOLOCK) ON VPL.VARCAL=VP.VARCAL AND VPL.NIVEL=VP.NIVEL AND VPL.CAMPO=VP.CAMPO AND VP.NIVEL=2" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN VAR_CAL2 V2 WITH (NOLOCK) ON V2.ID=VP.VARCAL AND V2.ORIGEN=@SOLIC AND V2.SUBTIPO=3" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM VAR_CAL_PCERT FROM VAR_CAL_PCERT VP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN VAR_CAL2 V2 WITH (NOLOCK) ON V2.ID=VP.VARCAL WHERE  V2.ORIGEN=@SOLIC AND VP.NIVEL=2 AND V2.SUBTIPO=3" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM VAR_CAL_PCERT_LISTA FROM VAR_CAL_PCERT_LISTA VPL WITH (NOLOCK) INNER JOIN VAR_CAL_PCERT VP WITH (NOLOCK) ON VPL.VARCAL=VP.VARCAL AND VPL.NIVEL=VP.NIVEL AND VPL.CAMPO=VP.CAMPO AND VP.NIVEL=3" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN VAR_CAL3 V3 WITH (NOLOCK) ON V3.ID=VP.VARCAL AND V3.ORIGEN=@SOLIC AND V3.TIPO=3" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM VAR_CAL_PCERT FROM VAR_CAL_PCERT VP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN VAR_CAL3 V3 WITH (NOLOCK) ON V3.ID=VP.VARCAL WHERE  V3.ORIGEN=@SOLIC AND VP.NIVEL=3 AND V3.TIPO=3" & vbCrLf
sConsulta = sConsulta & "                    end" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @MODIF=1 OR @MODIF=2" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         SELECT @TIPOSOL=T.TIPO FROM TIPO_SOLICITUDES T WITH (NOLOCK) WHERE T.ID=@TIPO" & vbCrLf
sConsulta = sConsulta & "         IF @TIPOSOL=2 OR @TIPOSOL=3" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "         /*Borra la configuraci�n de la cumplimentaci�n para el tipo de solicitud*/" & vbCrLf
sConsulta = sConsulta & "         DELETE FROM CONF_CUMPLIMENTACION WHERE SOLICITUD=@SOLIC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "         /*inserta para los proveedores*/" & vbCrLf
sConsulta = sConsulta & "         INSERT INTO CONF_CUMPLIMENTACION (SOLICITUD,CAMPO,PASO,SOLIC_PROVE,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN)" & vbCrLf
sConsulta = sConsulta & "             SELECT @SOLIC AS SOLICITUD ,FC.ID , NULL AS PASO ,2 AS SOLIC_PROVE, 1 AS VISIBLE, CASE WHEN FC.TIPO=3 THEN 0 ELSE 1 END AS ESCRITURA, CASE WHEN FC.TIPO_CAMPO_GS=22 OR  FC.TIPO_CAMPO_GS=23 THEN 1 ELSE 0 END AS OBLIGATORIO,FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "         FROM INSERTED S INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.GRUPO=FG.ID  WHERE S.ID = @SOLIC ORDER BY FG.ORDEN,FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "         /*inserta para el solicitante*/" & vbCrLf
sConsulta = sConsulta & "         INSERT INTO CONF_CUMPLIMENTACION (SOLICITUD,CAMPO,PASO,SOLIC_PROVE,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN)" & vbCrLf
sConsulta = sConsulta & "             SELECT @SOLIC AS SOLICITUD ,FC.ID , NULL AS PASO ,1 AS SOLIC_PROVE, 1 AS VISIBLE, CASE WHEN FC.TIPO=3 THEN 0 ELSE 1 END AS ESCRITURA, 0 AS OBLIGATORIO,FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "         FROM INSERTED S  INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.GRUPO=FG.ID WHERE S.ID = @SOLIC ORDER BY FG.ORDEN,FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "             /* Inserta la cumplimentacion para el campo estado actual*/" & vbCrLf
sConsulta = sConsulta & "             INSERT INTO CONF_CUMPLIMENTACION (SOLICITUD,CAMPO,PASO,SOLIC_PROVE,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN,ES_ESTADO)" & vbCrLf
sConsulta = sConsulta & "                        SELECT S.ID ,FC.ID , NULL AS PASO ,2 AS SOLIC_PROVE, 1 AS VISIBLE,1 AS ESCRITURA ,0 AS OBLIGATORIO, Z.ORDENC, 1 AS ES_ESTADO " & vbCrLf
sConsulta = sConsulta & "                        FROM SOLICITUD S WITH (NOLOCK) INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.GRUPO=FG.ID AND FC.TIPO_CAMPO_GS=34" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN (SELECT MAX(CF.ORDEN)+1 AS ORDENC,D.CAMPO_PADRE AS CAMPO FROM CONF_CUMPLIMENTACION CF " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN DESGLOSE D ON D.CAMPO_HIJO=CF.CAMPO  AND CF.SOLICITUD=@SOLIC GROUP BY D.CAMPO_PADRE) AS Z ON Z.CAMPO=FC.ID" & vbCrLf
sConsulta = sConsulta & "                    WHERE S.ID=@SOLIC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             INSERT INTO CONF_CUMPLIMENTACION (SOLICITUD,CAMPO,PASO,SOLIC_PROVE,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN,ES_ESTADO)" & vbCrLf
sConsulta = sConsulta & "                        SELECT S.ID ,FC.ID , NULL AS PASO ,1 AS SOLIC_PROVE, 1 AS VISIBLE,1 AS ESCRITURA ,0 AS OBLIGATORIO, Z.ORDENC, 1 AS ES_ESTADO " & vbCrLf
sConsulta = sConsulta & "                        FROM SOLICITUD S WITH (NOLOCK) INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.GRUPO=FG.ID AND FC.TIPO_CAMPO_GS=34" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN (SELECT MAX(CF.ORDEN)+1 AS ORDENC,D.CAMPO_PADRE AS CAMPO FROM CONF_CUMPLIMENTACION CF " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN DESGLOSE D ON D.CAMPO_HIJO=CF.CAMPO  AND CF.SOLICITUD=@SOLIC GROUP BY D.CAMPO_PADRE) AS Z ON Z.CAMPO=FC.ID" & vbCrLf
sConsulta = sConsulta & "                    WHERE S.ID=@SOLIC" & vbCrLf
sConsulta = sConsulta & "         END " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "               --Borrar las cumplimentaciones del formulario anterior" & vbCrLf
sConsulta = sConsulta & "             IF @MODIF<>1" & vbCrLf
sConsulta = sConsulta & "             begin" & vbCrLf
sConsulta = sConsulta & "                   DELETE FROM PM_CONF_CUMP_BLOQUE " & vbCrLf
sConsulta = sConsulta & "                      WHERE BLOQUE IN (SELECT ID FROM PM_BLOQUE WITH (NOLOCK) WHERE WORKFLOW = @WORKFLOW_NEW)" & vbCrLf
sConsulta = sConsulta & "                      AND ROL IN (SELECT ID FROM PM_ROL WITH (NOLOCK) WHERE WORKFLOW = @WORKFLOW_NEW)" & vbCrLf
sConsulta = sConsulta & "             end" & vbCrLf
sConsulta = sConsulta & "                   --Cambiar los Roles asignados por campos del formulario" & vbCrLf
sConsulta = sConsulta & "                   --Pasaran a asignarse por el peticionario en la etapa PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "               UPDATE PM_ROL SET" & vbCrLf
sConsulta = sConsulta & "                   BLOQUE_ASIGNA = (SELECT TOP 1 ID FROM PM_BLOQUE WITH (NOLOCK) WHERE WORKFLOW = @WORKFLOW_NEW AND TIPO = 1)," & vbCrLf
sConsulta = sConsulta & "                   COMO_ASIGNAR = 2 , -- Por Rol" & vbCrLf
sConsulta = sConsulta & "                   CAMPO = NULL," & vbCrLf
sConsulta = sConsulta & "                   ROL_ASIGNA = (SELECT TOP 1 ID FROM PM_ROL WITH (NOLOCK) WHERE WORKFLOW = @WORKFLOW_NEW AND TIPO = 4)" & vbCrLf
sConsulta = sConsulta & "                   WHERE WORKFLOW = @WORKFLOW_NEW AND COMO_ASIGNAR = 1 AND CAMPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   --Eliminar los Enlaces que tengan Condiciones con Campos del formulario" & vbCrLf
sConsulta = sConsulta & "                         DECLARE curTG_SOLICI_Upd2 CURSOR LOCAL FOR SELECT DISTINCT EC.ENLACE " & vbCrLf
sConsulta = sConsulta & "                                                                                     FROM PM_ENLACE_CONDICIONES EC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                                                                     INNER JOIN PM_ENLACE E WITH (NOLOCK) ON EC.ENLACE = E.ID" & vbCrLf
sConsulta = sConsulta & "                                                                                     INNER JOIN PM_BLOQUE B WITH (NOLOCK) ON E.BLOQUE_ORIGEN = B.ID" & vbCrLf
sConsulta = sConsulta & "                                                                                     WHERE B.WORKFLOW = @WORKFLOW_NEW" & vbCrLf
sConsulta = sConsulta & "                                                                                     AND (EC.TIPO_CAMPO = 1 OR EC.TIPO_VALOR = 1)" & vbCrLf
sConsulta = sConsulta & "                        OPEN curTG_SOLICI_Upd2" & vbCrLf
sConsulta = sConsulta & "                        FETCH NEXT FROM curTG_SOLICI_Upd2 INTO @ENLACE" & vbCrLf
sConsulta = sConsulta & "                        WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                        BEGIN" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PM_ENLACE_CONDICIONES WHERE ENLACE = @ENLACE" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PM_NOTIFICADO_ENLACE WHERE ENLACE = @ENLACE" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PM_ENLACE_EXTRAPOINTS WHERE ENLACE = @ENLACE" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PM_ENLACE WHERE ID = @ENLACE" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "                            FETCH NEXT FROM curTG_SOLICI_Upd2 INTO @ENLACE" & vbCrLf
sConsulta = sConsulta & "                          END " & vbCrLf
sConsulta = sConsulta & "                       CLOSE curTG_SOLICI_Upd2" & vbCrLf
sConsulta = sConsulta & "                       DEALLOCATE curTG_SOLICI_Upd2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   --Eliminar las Acciones que tengan Condiciones con Campos del formulario" & vbCrLf
sConsulta = sConsulta & "                   DECLARE curTG_SOLICI_Upd3 CURSOR LOCAL FOR SELECT DISTINCT A.ID " & vbCrLf
sConsulta = sConsulta & "                                                                  FROM PM_ACCIONES A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                                                  INNER JOIN PM_ACCION_PRECOND AP WITH (NOLOCK) ON AP.ACCION = A.ID" & vbCrLf
sConsulta = sConsulta & "                                                                  INNER JOIN PM_ACCION_CONDICIONES AC WITH (NOLOCK) ON AC.ACCION_PRECOND = AP.ID" & vbCrLf
sConsulta = sConsulta & "                                                                  INNER JOIN PM_BLOQUE B WITH (NOLOCK) ON A.BLOQUE = B.ID" & vbCrLf
sConsulta = sConsulta & "                                                                  WHERE B.WORKFLOW = @WORKFLOW_NEW" & vbCrLf
sConsulta = sConsulta & "                                                                  AND (AC.TIPO_CAMPO = 1 OR AC.TIPO_VALOR = 1)" & vbCrLf
sConsulta = sConsulta & "                   OPEN curTG_SOLICI_Upd3" & vbCrLf
sConsulta = sConsulta & "                   FETCH NEXT FROM curTG_SOLICI_Upd3 INTO @ACCION" & vbCrLf
sConsulta = sConsulta & "                   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PM_ACCION_CONDICIONES FROM PM_ACCION_CONDICIONES AC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                      INNER JOIN PM_ACCION_PRECOND AP WITH (NOLOCK) ON AC.ACCION_PRECOND = AP.ID" & vbCrLf
sConsulta = sConsulta & "                                     WHERE AP.ACCION = @ACCION" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PM_NOTIFICADO_ACCION WHERE ACCION = @ACCION" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PM_ACCION_PRECOND_DEN FROM PM_ACCION_PRECOND_DEN APD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                      INNER JOIN PM_ACCION_PRECOND AP WITH (NOLOCK) ON APD.ACCION_PRECOND = AP.ID " & vbCrLf
sConsulta = sConsulta & "                                      WHERE AP.ACCION = @ACCION" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PM_ACCION_PRECOND WHERE ACCION = @ACCION" & vbCrLf
sConsulta = sConsulta & "                            /*DELETE FROM PM_ACCIONES_DEN WHERE ACCION = @ACCION" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PM_ENLACE WHERE ACCION = @ACCION" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PM_ACCIONES WHERE ID = @ACCION*/" & vbCrLf
sConsulta = sConsulta & "                            " & vbCrLf
sConsulta = sConsulta & "                            FETCH NEXT FROM curTG_SOLICI_Upd3 INTO @ACCION" & vbCrLf
sConsulta = sConsulta & "                   END " & vbCrLf
sConsulta = sConsulta & "                   CLOSE curTG_SOLICI_Upd3" & vbCrLf
sConsulta = sConsulta & "                   DEALLOCATE curTG_SOLICI_Upd3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              IF @MODIF<>1" & vbCrLf
sConsulta = sConsulta & "              begin                " & vbCrLf
sConsulta = sConsulta & "                  --Insertar las cumplimentaciones del nuevo formulario" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO PM_CONF_CUMP_BLOQUE (BLOQUE,ROL,CAMPO,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN)" & vbCrLf
sConsulta = sConsulta & "                     SELECT RB.BLOQUE, RB.ROL, FC.ID, 1 AS VISIBLE, CASE WHEN FC.TIPO_CAMPO_GS in (10, 11, 12) THEN 0 ELSE CASE WHEN FC.TIPO=3 THEN 0 ELSE 1 END END AS ESCRITURA, 0 AS OBLIGATORIO,FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "                     FROM PM_ROL_BLOQUE RB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN PM_BLOQUE B WITH (NOLOCK) ON RB.BLOQUE = B.ID AND B.WORKFLOW = @WORKFLOW_NEW" & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN PM_ROL R WITH (NOLOCK) ON RB.ROL = R.ID AND R.WORKFLOW = @WORKFLOW_NEW" & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.FORMULARIO = @FORM_NEW" & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "                     ORDER BY FG.ORDEN,FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_SOLICI_Upd INTO @SOLIC,@WORKFLOW_NEW,@FORM_NEW,@WORKFLOW,@FORM,@TIPO" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Close curTG_SOLICI_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_SOLICI_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TG_PM_ROL_BLOQUE_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[TG_PM_ROL_BLOQUE_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""

sConsulta = sConsulta & "CREATE TRIGGER TG_PM_ROL_BLOQUE_INS ON [dbo].[PM_ROL_BLOQUE] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @BLOQUE AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ROL AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONF_BLOQUE_DEF AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXISTE_CUMPL_DEF AS BIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PM_ROL_BLOQUE_INS CURSOR LOCAL FOR SELECT I.ROL,I.BLOQUE,R.CONF_BLOQUE_DEF, " & vbCrLf
sConsulta = sConsulta & "                   CASE WHEN (SELECT COUNT(*) FROM PM_CONF_CUMP_BLOQUE CCB WHERE CCB.BLOQUE = R.CONF_BLOQUE_DEF AND CCB.ROL = I.ROL ) > 0 THEN 1 ELSE 0 END AS EXISTE" & vbCrLf
sConsulta = sConsulta & "                   FROM INSERTED I " & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN PM_ROL R ON I.ROL = R.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PM_ROL_BLOQUE_INS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PM_ROL_BLOQUE_INS INTO @ROL,@BLOQUE,@CONF_BLOQUE_DEF,@EXISTE_CUMPL_DEF" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF (ISNULL(@CONF_BLOQUE_DEF,@BLOQUE) = @BLOQUE) OR (@EXISTE_CUMPL_DEF = 0)" & vbCrLf
sConsulta = sConsulta & "    --Si no hay Bloque por defecto o el Bloque por defecto es el asignado, o el Bloque por defecto todav�a no tiene Cumplimentacion" & vbCrLf
sConsulta = sConsulta & "    --Establecemos una configuraci�n estandar" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO PM_CONF_CUMP_BLOQUE (BLOQUE,ROL,CAMPO,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN)" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT @BLOQUE AS BLOQUE, @ROL AS ROL, FC.ID, 1 AS VISIBLE, CASE WHEN FC.TIPO_CAMPO_GS IN (10, 11, 12,127) THEN 0 ELSE CASE WHEN FC.TIPO=3 THEN 0 ELSE 1 END END AS ESCRITURA, 0 AS OBLIGATORIO,FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "      FROM PM_BLOQUE B WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN WORKFLOW W WITH (NOLOCK) ON W.ID = B.WORKFLOW" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN SOLICITUD S WITH (NOLOCK) ON S.WORKFLOW = W.ID" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.FORMULARIO = S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "      WHERE B.ID = @BLOQUE ORDER BY FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    --Si hay bloque por defecto y tiene Cumplimentacion" & vbCrLf
sConsulta = sConsulta & "    --Establecemos su configuraci�n" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO PM_CONF_CUMP_BLOQUE (BLOQUE,ROL,CAMPO,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN)" & vbCrLf
sConsulta = sConsulta & "      SELECT @BLOQUE AS BLOQUE, @ROL AS ROL, C.CAMPO, C.VISIBLE, C.ESCRITURA, C.OBLIGATORIO, C.ORDEN" & vbCrLf
sConsulta = sConsulta & "      FROM PM_CONF_CUMP_BLOQUE C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      WHERE C.BLOQUE = @CONF_BLOQUE_DEF AND ROL = @ROL" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_PM_ROL_BLOQUE_INS INTO @ROL,@BLOQUE,@CONF_BLOQUE_DEF,@EXISTE_CUMPL_DEF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_PM_ROL_BLOQUE_INS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PM_ROL_BLOQUE_INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


