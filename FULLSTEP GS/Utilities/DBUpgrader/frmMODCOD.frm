VERSION 5.00
Begin VB.Form frmMODCOD 
   BackColor       =   &H00808000&
   Caption         =   "Administrador de GS"
   ClientHeight    =   2130
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4275
   BeginProperty Font 
      Name            =   "Lucida Sans"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMODCOD.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2130
   ScaleWidth      =   4275
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtCodNue 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1725
      TabIndex        =   2
      Top             =   1215
      Width           =   1590
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1065
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1695
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   2160
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1695
      Width           =   1005
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Introduzca el c�digo de la persona que realizar� las funciones de administrador de la plataforma FULLSTEP GS"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1035
      Left            =   90
      TabIndex        =   4
      Top             =   60
      Width           =   3945
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "C�digo de persona:"
      ForeColor       =   &H8000000E&
      Height          =   285
      Left            =   75
      TabIndex        =   3
      Top             =   1245
      Width           =   1620
   End
End
Attribute VB_Name = "frmMODCOD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
    If txtCodNue.Text = "" Then
        MsgBox "Debe introducir el c�digo de la persona administrador. Debe ser una persona existente en la plataforma.", vbCritical, "FULLSTEP GS DB Updater"
        Exit Sub
    Else
        If Not ComprobarPersona(txtCodNue.Text) Then
            MsgBox "Debe introducir el c�digo de la persona administrador. Debe ser una persona existente en la plataforma.", vbCritical, "FULLSTEP GS DB Updater"
            Exit Sub
        End If
    End If
    
    bas_V_31900_8.g_sPersona = txtCodNue.Text

    Unload Me
End Sub

Private Function ComprobarPersona(ByVal sCodPer As String) As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    Dim oCom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim rsPer As ADODB.Recordset
    
    ComprobarPersona = False
    
    'Comprobar que la persona existe en la BD
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    Set oCom = New ADODB.Command
    With oCom
        sConsulta = "SELECT COD FROM PER WITH (NOLOCK) WHERE COD=?"
        
        Set .ActiveConnection = oADOConnection
        .CommandType = adCmdText
        .CommandText = sConsulta
        Set oParam = .CreateParameter("PER", adVarChar, adParamInput, 50, sCodPer)
        .Parameters.Append oParam
        
        Set rsPer = .Execute
    End With
    
    If Not rsPer Is Nothing Then
        If rsPer.RecordCount > 0 Then ComprobarPersona = True
    End If
    
    oADOConnection.Close
    Set oADOConnection = Nothing
    Set oCom = Nothing
    Set oParam = Nothing
    Set rsPer = Nothing
End Function

Private Sub cmdCancelar_Click()
    bas_V_31900_8.g_sPersona = ""
    Unload Me
End Sub

Private Sub Form_Load()
    
    Me.Top = 7950 / 2 - Me.Height / 2
    Me.Left = 11940 / 2 - Me.Width / 2
    
End Sub

